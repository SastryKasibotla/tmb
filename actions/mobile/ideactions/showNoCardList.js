function preshowMBCardProductLink(){
	changeStatusBarColor();
	//var imgcard = "https://dev.tau2904.com/tmb/ImageRender?crmId=&&personalizedId=&billerId=cardnewissue&modIdentifier=PRODUCTPACKAGEIMG&dummy="
	var imgcard = getImageSRC(imageIssueCardName);
	frmMBCardProductLink.lblCardList.text = kony.i18n.getLocalizedString("Menu_keyCard");
	
	frmMBCardProductLink.lblHeaderCreditCard.text = kony.i18n.getLocalizedString("keyCreditCard");
	frmMBCardProductLink.lblHeaderDebitCard.text = kony.i18n.getLocalizedString("keyDebitCard");
	frmMBCardProductLink.lblHeaderReadyCash.text = kony.i18n.getLocalizedString("keyReadyCashCard");
	
	frmMBCardProductLink.lblCreditCard1.text = kony.i18n.getLocalizedString("GetCreditCard");
	frmMBCardProductLink.btnCCReadMore.text = kony.i18n.getLocalizedString("ReadMore");
	
	frmMBCardProductLink.lblDebitCard1.text = kony.i18n.getLocalizedString("GetDebitCard");
	frmMBCardProductLink.lblDebitCard2.text = kony.i18n.getLocalizedString("keyOr");
	frmMBCardProductLink.btnDCOpenAC.text = kony.i18n.getLocalizedString("OpenAcc");
	frmMBCardProductLink.btnDCReadMore.text = kony.i18n.getLocalizedString("ReadMore");
	
	frmMBCardProductLink.lblReadyCash1.text = kony.i18n.getLocalizedString("GetReadyCard");
	frmMBCardProductLink.btnRCReadMore.text = kony.i18n.getLocalizedString("ReadMore");
	
	frmMBCardProductLink.imgDebitCard.src = imgcard;
	frmMBCardProductLink.imgCreditCard.src = imgcard;
	frmMBCardProductLink.imgReadyCash.src = imgcard;
	
}
function getLinkCardURL(urlType){
	var url = "";
	
	if(urlType == "Credit Card"){
		url = kony.i18n.getLocalizedString("CreditCardReadMore");
	} else if(urlType == "Debit Card"){
		url = kony.i18n.getLocalizedString("DebitCardReadMore");
	} else if(urlType == "Ready Cash"){
		url = kony.i18n.getLocalizedString("ReadyCashReadMore");
	} 
	return url;
}
function openURLfrmMBCardProductURL(urlType){
	var url = "";
	tempUrlType = urlType;
	if(urlType == "Open Account") {
		if(checkMBUserStatus()){
			OnClickOpenNewAccount();
		}	
	} else {
		url = getLinkCardURL(urlType);
		frmMBCardMoreDetail.browserCard.requestURLConfig = {
		     URL: url,
		     requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
		}
		frmMBCardMoreDetail.buttonBack.text = kony.i18n.getLocalizedString("Back");
		frmMBCardMoreDetail.lblCardTitle.text = kony.i18n.getLocalizedString("Menu_keyCard");
		
		frmMBCardMoreDetail.show(); 
	}

}

function preshowMBCardProductURL(){
	changeStatusBarColor();
	url = getLinkCardURL(tempUrlType);
	frmMBCardMoreDetail.browserCard.requestURLConfig = {
	     URL: url,
	     requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
	}
	frmMBCardMoreDetail.buttonBack.text = kony.i18n.getLocalizedString("Back");
	frmMBCardMoreDetail.lblCardTitle.text = kony.i18n.getLocalizedString("Menu_keyCard");
}