function displayPreMenu()
{
			
			var segMenuTable;
			if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
				segMenuTable = [{
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices")
						}
				]
			}else{
				segMenuTable = [{
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates")
					}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices")
					},{
						lblMenuItem: ""
					}
				]
			}
		frmPreMenu.lblmenuLogin.text = kony.i18n.getLocalizedString("login");
		frmPreMenu.btnThai.text=kony.i18n.getLocalizedString("languageThai");
		frmPreMenu.btnEng.text=kony.i18n.getLocalizedString("languageEng");
		var locale = kony.i18n.getCurrentLocale();
		if (locale == "en_US")
	    {
			frmPreMenu.btnEng.skin = btnEngMenu;
			frmPreMenu.btnEng.zIndex = 2;
			frmPreMenu.btnThai.skin = btnThaiMenu;
			frmPreMenu.btnThai.zIndex = 1;
		}
		else
		{
			frmPreMenu.btnEng.skin = btnThaiMenu;
			frmPreMenu.btnEng.zIndex=1;
			frmPreMenu.btnThai.skin = btnEngMenu;
			frmPreMenu.btnThai.zIndex=2;
		}
			
		frmPreMenu.lblversion.text=kony.i18n.getLocalizedString("keyVersion")+" "+ appConfig.appVersion;
		frmPreMenu.lblCopyright.text=getCopyRightText();
		frmPreMenu.segPreLogin.setData(segMenuTable);
		frmPreMenu.show();
}

function onClickMenuPreMenu()
{
	var selectedIndexVal = frmPreMenu.segPreLogin.selectedIndex[1];
	if(selectedIndexVal == 0){
				assignGlobalForMenuClick("frmATMBranch");
				gblLatitude = "";
				gblLongitude = "";
				gblCurrenLocLat = "";
				gblCurrenLocLon = "";
				provinceID = "";
				districtID = "";
				gblProvinceData = [];
				gblDistrictData = [];
				frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
				frmATMBranch.btnCombDistrict.text =  kony.i18n.getLocalizedString("FindTMB_District");
				frmATMBranch.txtKeyword.text = "";
				onClickATM();
			}else if(selectedIndexVal == 1){
				assignGlobalForMenuClick("");
				onClickExchangeRates();
			}else if(selectedIndexVal == 2){
				assignGlobalForMenuClick("frmAppTour");
				//App Tour
				showAppTourForm();
			}
			else if (selectedIndexVal == 3) {
				assignGlobalForMenuClick("frmContactUsMB");
				conatctUsForm();
			}else if (selectedIndexVal == 4) {
				assignGlobalForMenuClick("");
				onClickDownloadTnC('pdf', 'TermsAndCondtionsFooter');
			}else if (selectedIndexVal == 5) {
				assignGlobalForMenuClick("");
				onClickSecurityAdvices();
			}
	
}

function onClickClosebuttonMenu()
{
	gblCallPrePost = false;
	var displayform=kony.application.getPreviousForm();
	displayform.show();
}

function assignGlobalForMenuClick(menuClickForm) {
	if(gblCurrentForm == menuClickForm) {
		gblCallPrePost = false;
	}
}

function assignGlobalForMenuPostshow() {
	gblCallPrePost = true;
	gblCurrentForm = kony.application.getCurrentForm().id;
	try{
		changeStatusBarColor();
		kony.application.getCurrentForm().scrollboxMain.scrollToEnd();
	}catch(error){
	}
}