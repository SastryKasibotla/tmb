//var jtext = '{"tknid":"67255B28F25BB4E40E5726481824E75EE90F99F07FCF4E7E190B67967F729AA8","opstatus":"0","debitCardRec":[],"maxDayBeforeExpire":7,"creditCardRec":[{"ICON_ID":"ICON-07","cardStatus":"Not Activated","dayLeft":"","ProductNameEng":"Credit Card Gold","acctStatus":"BLCK J","cardType":"Credit Card","accountNoFomatted":"4966-94XX-XXXX-4008","productID":"011","IMG_CARD_ID":"VisaDefault","allowCardActivation":"Y","defaultCurrentNickNameEN":"Credit Card Gold 4008","ProductNameThai":"บัตรเครดิต โกลด์","SortId":2,"defaultCurrentNickNameTH":"บัตรเครดิต โกลด์ 4008","allowRequestNewPin":"Y","accType":"CCA","cardRefId":"829001"}],"maxDebitCard":1,"readyCashRec":[],"statusCode":"0"}';
//var jtext = '';
var imageIssueCardName = "blankcard";
var tempUrlType = "";

function preShowMBCardList() {
    showLoadingScreen();
    gblCardList = "";
    gblSelectedCard = "";
    gblManageCardFlow = "";
    clearMBCardList();
    getCustomerCardList();
}

function preShowMBDebitCardList() { // use for test
    gblSelectedCard = "";
    clearMBDebitCardList();
    var obj = JSON.parse(jtext);
    getCallbackShowDebitCardList(400, obj);
}

function callCreditReadyCardActivateInqService(cardRefId) {
    var inputparam = {};
    gblCardList = "";
    gblSelectedCard = "";
    gblManageCardFlow = "";
    if (gblUserLockStatusMB == "03") {
        showTranPwdLockedPopup();
    } else {
        inputparam = {};
        inputparam["cardFlag"] = "1";
        inputparam["cardRefId"] = cardRefId;
        showLoadingScreen();
        invokeServiceSecureAsync("customerCardList", inputparam, getCallbackShowCreditCardList);
    }
}

function preShowMBDebitCardListfromAccntSummary() {
    gblCardList = "";
    gblSelectedCard = "";
    gblManageCardFlow = "";
    clearMBDebitCardList();
    gblCardList = gblDebitCardResult;
    if (gblUserLockStatusMB == "03") {
        showTranPwdLockedPopup();
    } else {
        getCallbackShowDebitCardList(400, gblCardList);
    }
}

function preShowMBCreditCardFromAccDetail() {
    showLoadingScreen();
    gblCardList = "";
    gblSelectedCard = "";
    gblManageCardFlow = "";
    if (gblUserLockStatusMB == "03") {
        showTranPwdLockedPopup();
    } else {
        getCustomerCCACardInfo();
    }
}

function getCustomerCardList() {
    inputparam = {};
    inputparam["cardFlag"] = "3";
    invokeServiceSecureAsync("customerCardList", inputparam, getCallbackShowCardList);
}

function getCustomerAccountDebitCardList() {
    inputparam = {};
    inputparam["cardFlag"] = "2";
    inputparam["acctId"] = "00000019051655";
    invokeServiceSecureAsync("customerCardList", inputparam, getCallbackShowDebitCardList);
}

function getCustomerCCACardInfo() {
    inputparam = {};
    inputparam["cardFlag"] = "1";
    inputparam["cardRef"] = "813001";
    invokeServiceSecureAsync("customerCardList", inputparam, getCallbackShowManageCard);
}

function performShowActivateCard(eventobject, x, y) {
    gblSelectedCard = getCardInfo(eventobject);
    var inCardRefId = gblSelectedCard["cardRefId"];
    var inCardNumber = gblSelectedCard["cardNo"];
    var inNickName = gblSelectedCard["cardName"];
    var inCardType = gblSelectedCard["cardType"];
    if (gblSelectedCard["allowCardActivation"] == "N") {
        if (gblSelectedCard["cardType"] == "Debit Card") {
            showAlert(kony.i18n.getLocalizedString("ActDB_Err_LockDB"), kony.i18n.getLocalizedString("info"));
        } else {
            showAlert(kony.i18n.getLocalizedString("ActCC_Err_LockCC"), kony.i18n.getLocalizedString("info"));
        }
    } else {
        cardManagementActivationCall(inCardType, inCardNumber, inCardRefId, inNickName);
    }
};

function performShowManageCard(eventobject, x, y) {
    gblSelectedCard = getCardInfo(eventobject);
    gotoManageCard(gblSelectedCard);
};

function performIssueNewCard() {
    if (gblCardList["allowIssueNewCard"] == 'Y') {
        frmMBNewTncNewDCShow();
    } else if (gblCardList["allowIssueNewCard"] == 'W') {
        showAlert(kony.i18n.getLocalizedString("NotIssueCard"), kony.i18n.getLocalizedString("info"));
    } else {
        // do nothing
    }
};

function getCardInfo(eventobject) {
    if (eventobject == undefined || eventobject == null) return;
    var currentFormId = kony.application.getCurrentForm().id;
    var selectedCard = "";
    var objID = eventobject.id;
    var objName = eventobject.id.substring(7, objID.length);
    var cardType = objName.substring(0, 2);
    var index = objName.substring(2, objName.length);
    var cardName = "";
    if (currentFormId == "frmMBCardList") {
        cardNo = frmMBCardList["lblCardNo" + objName].text;
        imgSrc = frmMBCardList["imgCard" + objName].src;
        cardName = frmMBCardList["lblCardNickName" + objName].text;
    } else if (currentFormId == "frmMBListDebitCard") {
        cardNo = frmMBListDebitCard["lblCardNo" + objName].text;
        imgSrc = frmMBListDebitCard["imgCard" + objName].src;
        cardName = frmMBListDebitCard["lblCardNickName" + objName].text;
    } else if (currentFormId == "frmMBListCreditCard") {
        cardNo = frmMBListCreditCard["lblCardNo" + objName].text;
        imgSrc = frmMBListCreditCard["imgCard" + objName].src;
        cardName = frmMBListCreditCard["lblCardNickName" + objName].text;
    }
    if (cardType == "CC") {
        selectedCard = gblCardList["creditCardRec"][index];
        selectedCard["imgSrc"] = imgSrc;
        selectedCard["cardNo"] = cardNo;
    } else if (cardType == "DC") {
        selectedCard = gblCardList["debitCardRec"][index];
        selectedCard["imgSrc"] = imgSrc;
        selectedCard["cardNo"] = cardNo;
    } else if (cardType == "RC") {
        selectedCard = gblCardList["readyCashRec"][index];
        selectedCard["imgSrc"] = imgSrc;
        selectedCard["cardNo"] = cardNo;
    }
    selectedCard["cardName"] = cardName;
    return selectedCard;
}

function setCardId(cardType) {
    var cardId = "";
    if (cardType == "Credit Card") {
        cardId = "CC"; // i18n
    } else if (cardType == "Debit Card") {
        cardId = "DC";
    } else if (cardType == "Ready Cash") {
        cardId = "RC";
    }
    return cardId;
}

function getCardHeaderText(cardType) {
    var cardHeaderTxt = "";
    if (cardType == "Credit Card") {
        cardHeaderTxt = kony.i18n.getLocalizedString("keyCreditCard"); // 
    } else if (cardType == "Debit Card") {
        cardHeaderTxt = kony.i18n.getLocalizedString("keyDebitCard"); // keyDebitCard
    } else if (cardType == "Ready Cash") {
        cardHeaderTxt = kony.i18n.getLocalizedString("keyReadyCashCard"); //keyReadyCashCard
    }
    return cardHeaderTxt;
}

function getLinkTextCard(cardType, cardStatus, allowManageCard) {
    var linkTextCard = "";
    if (cardType == "Debit Card") {
        if (cardStatus == "Active" && allowManageCard == "Y") {
            linkTextCard = kony.i18n.getLocalizedString("DBA_ManageCard"); //DBA_ManageCard 
        } else if (cardStatus == "Not Activated") {
            linkTextCard = kony.i18n.getLocalizedString("DBA01_Instruction"); //DBA01_Instruction
        }
    } else {
        if (cardStatus == "Active") {
            linkTextCard = kony.i18n.getLocalizedString("DBA_ManageCard"); //DBA_ManageCard 
        } else if (cardStatus == "Not Activated") {
            linkTextCard = kony.i18n.getLocalizedString("DBA01_Instruction"); //DBA01_Instruction
        }
    }
    return linkTextCard;
}

function getRibbonTextCard(cardStatus, dayLeft) {
    var ribbonTextCard = "";
    var maxExpired = parseInt(gblCardList["maxDayBeforeExpire"]);
    if (maxExpired < 0) maxExpired = 7;
    if (cardStatus == "Not Activated") {
        if (dayLeft > 0 && dayLeft <= maxExpired) {
            ribbonTextCard = dayLeft + " " + kony.i18n.getLocalizedString("keyribbondayleft");; //keyribbondayleft
        } else {
            ribbonTextCard = kony.i18n.getLocalizedString("keyribbonnew"); //keyribbonnew
        }
    } else if (cardStatus == "Blocked") {
        ribbonTextCard = kony.i18n.getLocalizedString("keyribbonblocked"); //keyribbonblocked
    }
    return ribbonTextCard;
}

function getRibbonSkinCard(cardStatus, dayLeft) {
    var ribbonSkinCard = "";
    var maxExpired = parseInt(gblCardList["maxDayBeforeExpire"]);
    if (maxExpired < 0) maxExpired = 7;
    if (cardStatus == "Not Activated") {
        if (dayLeft > 0 && dayLeft <= maxExpired) {
            ribbonSkinCard = "lblRibbonOrange";
        } else {
            ribbonSkinCard = "lblRibbonWhite"
        }
    } else if (cardStatus == "lblRibbonRed") {
        ribbonSkinCard = "lblRibbonRed";
    }
    return ribbonSkinCard;
}

function getLinkEventCard(cardType, cardStatus, allowManageCard) {
    var performEventCard = null;
    if (cardType == "Debit Card") {
        if (cardStatus == "Active" && allowManageCard == "Y") {
            performEventCard = performShowManageCard;
        } else if (cardStatus == "Not Activated") {
            performEventCard = performShowActivateCard;
        }
    } else {
        if (cardStatus == "Active") {
            performEventCard = performShowManageCard;
        } else if (cardStatus == "Not Activated") {
            performEventCard = performShowActivateCard;
        }
    }
    return performEventCard;
}

function getCardNoSkin() {
    var cardNoSkin = "lblWhiteCard24px";
    var gblDeviceInfo = kony.os.deviceInfo();
    var screenwidth = gblDeviceInfo["deviceWidth"];
    var screenheight = gblDeviceInfo["deviceHeight"];
    if (screenwidth > 640) {
        cardNoSkin = "lblWhiteCard28px";
    } else {
        cardNoSkin = "lblWhiteCard24px";
    }
    return cardNoSkin;
}

function setCardHeader(cardCode) {
    var lblSectionHeader = "";
    var flexSectionHeader = "";
    var cardid = "";
    cardid = setCardId(cardCode);
    lblSectionHeader = new kony.ui.Label({
        "id": "lblSectionHeader" + cardid,
        "top": "0dp",
        "left": "10%",
        "zIndex": 1,
        "isVisible": true,
        "text": getCardHeaderText(cardCode),
        "skin": "lblblackbig"
    }, {
        "padding": [0, 3, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    flexSectionHeader = new kony.ui.FlexContainer({
        "id": "flexSectionHeader" + cardid,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "height": "40dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,0]",
        "skin": "slFbox",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});
    flexSectionHeader.setDefaultUnit(kony.flex.DP);
    flexSectionHeader.add(lblSectionHeader);
    frmMBCardList.FlexMain.setDefaultUnit(kony.flex.DP);
    frmMBCardList.FlexMain.add(flexSectionHeader);
}

function dynamicListCard(cardArray) {
    var imgCard = "";
    var lblRibbon = "";
    var flexCard = "";
    var lblLinkText = "";
    var flexLink = "";
    var flexContent = "";
    var lblCardNo = "";
    var lblCardNickName = "";
    var btnCard = "";
    var imgSrc = "";
    var ribbonText = "";
    var linkText = "";
    var cardEvent = "";
    var cardNickName = "";
    var cardNo = "";
    var ribbonSkin = "";
    var cardType = "";
    var cardStatus = "";
    var cardFrmID = "";
    var allowManageCard = "";
    var imageName = "";
    var cardheight = "40%";
    var cardNoSkin = "lblWhiteCard24px";
    var imgSrcFailed = "";
    var gblDeviceInfo = kony.os.deviceInfo();
    var deviceName = gblDeviceInfo["name"];
    var screenwidth = gblDeviceInfo["deviceWidth"];
    var screenheight = gblDeviceInfo["deviceHeight"];
    if (deviceName == "android") {
        if ((screenwidth == 1080 && screenheight < 1920) || screenwidth == 480 || (screenwidth == 720 && screenheight < 1280)) {
            cardheight = "43%";
        } else if (screenwidth == 768 && screenheight < 1280) {
            cardheight = "46%";
        } else {
            cardheight = "40%";
        }
    } else {
        cardheight = "40%";
    }
    cardNoSkin = getCardNoSkin();
    if (cardArray.length < 1) {
        return;
    }
    for (var index = 0; index < cardArray.length; index++) {
        cardType = cardArray[index]["cardType"];
        cardFrmID = setCardId(cardType) + index;
        cardStatus = cardArray[index]["cardStatus"];
        if (cardType == "Debit Card") {
            allowManageCard = cardArray[index]["allowManageCard"];
            imgSrcFailed = "debitdefault.png";
        } else {
            if (cardType == "Credit Card") {
                if (cardArray[index]["accountNoFomatted"][0] == '5') {;
                    imgSrcFailed = "masterdefault.png";
                } else if (cardArray[index]["accountNoFomatted"][0] == '4') {
                    imgSrcFailed = "visadefault.png";
                }
            } else if (cardType == "Ready Cash") {
                imgSrcFailed = "readycard.png";
            }
        }
        imageName = cardArray[index]["IMG_CARD_ID"];
        imgSrc = getImageSRC(imageName);
        kony.print(" imageName : " + imageName + " imgSrc : " + imgSrc);
        if (undefined != cardArray[index]["dayLeft"]) {
            ribbonText = getRibbonTextCard(cardStatus, cardArray[index]["dayLeft"]);
            ribbonSkin = getRibbonSkinCard(cardStatus, cardArray[index]["dayLeft"]);
        } else {
            if (cardStatus == "Blocked") {
                ribbonText = kony.i18n.getLocalizedString("keyribbonblocked");
                ribbonSkin = "lblRibbonRed";
            } else {
                ribbonText = "";
            }
        }
        if (cardStatus == "Not Activated") {
            cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
        } else {
            if (cardArray[index]["acctNickName"] != undefined) {
                cardNickName = cardArray[index]["acctNickName"];
                if (cardNickName == "") {
                    if (kony.i18n.getCurrentLocale() == "en_US") {
                        cardNickName = cardArray[index]["defaultCurrentNickNameEN"];
                    } else {
                        cardNickName = cardArray[index]["defaultCurrentNickNameTH"];
                    }
                }
            } else {
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = cardArray[index]["defaultCurrentNickNameEN"];
                } else {
                    cardNickName = cardArray[index]["defaultCurrentNickNameTH"];
                }
            }
        }
        cardNo = cardArray[index]["accountNoFomatted"].split("-").join("  ");
        linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
        cardEvent = getLinkEventCard(cardType, cardStatus, allowManageCard);
        btnCard = new kony.ui.Button({
            "id": "btnCard" + cardFrmID,
            "top": "0%",
            "left": "0",
            "width": "100%",
            "height": "100%",
            "zIndex": 5,
            "isVisible": true,
            "text": null,
            "skin": "noSkinButtonNormal",
            "focusSkin": "noSkinButtonNormal",
            "onClick": cardEvent
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "glowEffect": false,
            "showProgressIndicator": true
        });
        imgCard = new kony.ui.Image2({
            "id": "imgCard" + cardFrmID,
            "top": "-8.3%", //-4% good for nexus , -8% good for sam
            "width": "100%",
            "height": "100.0%",
            "zIndex": 3,
            "isVisible": true,
            "src": imgSrc,
            "imageWhenFailed": imgSrcFailed,
            "imageWhileDownloading": "empty.png"
        }, {
            "padding": [0, 0, 0, 0],
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {});
        if (ribbonText != "") {
            lblRibbon = new kony.ui.Label({
                "id": "lblRibbon" + cardFrmID,
                "top": "10%", //10.41
                "right": "-12.0%", //-11.20
                "width": "50%",
                "height": "14.0%",
                "zIndex": 3,
                "isVisible": true,
                "text": ribbonText,
                "skin": ribbonSkin
            }, {
                "padding": [0, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {
                "textCopyable": false
            });
            addRibbon(lblRibbon)
        }
        lblLinkText = new kony.ui.Label({
            "id": "lblLinkText" + cardFrmID,
            "top": "0dp",
            "centerX": "50%",
            "zIndex": 3,
            "isVisible": true,
            "text": linkText,
            "skin": "lblbluemedium200"
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "textCopyable": false
        });
        flexLink = new kony.ui.FlexContainer({
            "id": "flexLink" + cardFrmID,
            "bottom": "0dp",
            "left": "0dp",
            "width": "100%",
            "height": "35dp",
            "zIndex": 3,
            "isVisible": true,
            "clipBounds": true,
            "Location": "[0,217]",
            "skin": "slFbox",
            "layoutType": kony.flex.FREE_FORM
        }, {
            "padding": [0, 0, 0, 0]
        }, {});;
        flexLink.setDefaultUnit(kony.flex.DP)
        flexLink.add(lblLinkText);
        lblCardNo = new kony.ui.Label({
            "id": "lblCardNo" + cardFrmID,
            "top": "0%",
            "left": "10%",
            "width": "90%",
            "zIndex": 3,
            "isVisible": true,
            "text": cardNo,
            "skin": cardNoSkin
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "textCopyable": false
        });
        lblCardNickName = new kony.ui.Label({
            "id": "lblCardNickName" + cardFrmID,
            "top": "45%",
            "left": "10%",
            "width": "90%",
            "zIndex": 3,
            "isVisible": true,
            "text": cardNickName,
            "skin": "lblWhiteCard22px"
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "textCopyable": false
        });
        flexContent = new kony.ui.FlexContainer({
            "id": "flexContent" + cardFrmID,
            "top": "0",
            "left": "0",
            "width": "100%",
            "height": "30%",
            "centerX": "50%",
            "centerY": "62%",
            "zIndex": 3,
            "isVisible": true,
            "clipBounds": true,
            "Location": "[0,107]",
            "skin": "slFbox",
            "layoutType": kony.flex.FREE_FORM
        }, {
            "padding": [0, 0, 0, 0]
        }, {});;
        flexContent.setDefaultUnit(kony.flex.DP)
        flexContent.add(lblCardNo, lblCardNickName);
        flexCard = new kony.ui.FlexContainer({
            "id": "flexCard" + cardFrmID,
            "top": "5%",
            "left": "10.89%",
            "width": "83%",
            "height": cardheight,
            "centerX": "50.0%",
            "zIndex": 2,
            "isVisible": true,
            "clipBounds": true,
            "Location": "[39,202]",
            "skin": "noSkinFlexContainer",
            "layoutType": kony.flex.FREE_FORM
        }, {
            "padding": [0, 0, 0, 0]
        }, {});
        flexCard.setDefaultUnit(kony.flex.DP)
        if (ribbonText != "") {
            flexCard.add(btnCard, imgCard, lblRibbon, flexContent, flexLink);
        } else {
            flexCard.add(btnCard, imgCard, flexContent, flexLink);
        }
        frmMBCardList.FlexMain.add(flexCard);
    }
    var flexSession = new kony.ui.FlexContainer({
        "id": "flexSession" + cardType,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "height": "30dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,200]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    frmMBCardList.FlexMain.add(flexSession);
}

function dynamicDebitListCard(cardArray, currentFrm) {
    var imgCard = "";
    var lblRibbon = "";
    var flexCard = "";
    var lblLinkText = "";
    var flexLink = "";
    var flexContent = "";
    var lblCardNo = "";
    var lblCardNickName = "";
    var btnCard = "";
    var imgSrc = "";
    var ribbonText = "";
    var linkText = "";
    var cardEvent = "";
    var cardNickName = "";
    var cardNo = "";
    var ribbonSkin = "";
    var cardType = "";
    var cardStatus = "";
    var cardFrmID = "";
    var allowManageCard = "";
    var imageName = "";
    var cardheight = "46%";
    var cardNoSkin = "lblWhiteCard24px";
    var imgSrcFailed = "";
    var gblDeviceInfo = kony.os.deviceInfo();
    var deviceName = gblDeviceInfo["name"];
    var screenwidth = gblDeviceInfo["deviceWidth"];
    var screenheight = gblDeviceInfo["deviceHeight"];
    if (deviceName == "android") {
        if ((screenwidth == 1080 && screenheight < 1920) || screenwidth == 480 || (screenwidth == 720 && screenheight < 1280)) {
            cardheight = "51.5%";
        } else if (screenwidth == 768 && screenheight < 1280) {
            cardheight = "55%";
        } else {
            cardheight = "47.5%";
        }
    } else {
        cardheight = "47.5%";
    }
    cardNoSkin = getCardNoSkin();
    if (cardArray.length < 1) {
        return;
    }
    for (var index = 0; index < cardArray.length; index++) {
        cardType = cardArray[index]["cardType"];
        cardFrmID = setCardId(cardType) + index;
        cardStatus = cardArray[index]["cardStatus"];
        if (cardType == "Debit Card") {
            allowManageCard = cardArray[index]["allowManageCard"];
            imgSrcFailed = "debitdefault.png";
        } else {
            if (cardType == "Credit Card") {
                if (cardArray[index]["accountNoFomatted"][0] == '5') {;
                    imgSrcFailed = "masterdefault.png";
                } else if (cardArray[index]["accountNoFomatted"][0] == '4') {
                    imgSrcFailed = "visadefault.png";
                }
            } else if (cardType == "Ready Cash") {
                imgSrcFailed = "readycard.png";
            }
        }
        imageName = cardArray[index]["IMG_CARD_ID"];
        imgSrc = getImageSRC(imageName);
        if (undefined != cardArray[index]["dayLeft"]) {
            ribbonText = getRibbonTextCard(cardStatus, cardArray[index]["dayLeft"]);
            ribbonSkin = getRibbonSkinCard(cardStatus, cardArray[index]["dayLeft"]);
        } else {
            if (cardStatus == "Blocked") {
                ribbonText = kony.i18n.getLocalizedString("keyribbonblocked");
                ribbonSkin = "lblRibbonRed";
            } else {
                ribbonText = "";
            }
        }
        if (cardStatus == "Not Activated") {
            cardNickName = kony.i18n.getLocalizedString("keywaitactivate");
        } else {
            if (cardArray[index]["acctNickName"] != undefined) {
                cardNickName = cardArray[index]["acctNickName"];
                if (cardNickName == "") {
                    if (kony.i18n.getCurrentLocale() == "en_US") {
                        cardNickName = cardArray[index]["defaultCurrentNickNameEN"];
                    } else {
                        cardNickName = cardArray[index]["defaultCurrentNickNameTH"];
                    }
                }
            } else {
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    cardNickName = cardArray[index]["defaultCurrentNickNameEN"];
                } else {
                    cardNickName = cardArray[index]["defaultCurrentNickNameTH"];
                }
            }
        }
        cardNo = cardArray[index]["accountNoFomatted"].split("-").join("  ");
        linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
        cardEvent = getLinkEventCard(cardType, cardStatus, allowManageCard);
        btnCard = new kony.ui.Button({
            "id": "btnCard" + cardFrmID,
            "top": "0%",
            "left": "0",
            "width": "100%",
            "height": "100%",
            "zIndex": 5,
            "isVisible": true,
            "text": null,
            "skin": "noSkinButtonNormal",
            "focusSkin": "noSkinButtonNormal",
            "onClick": cardEvent
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "glowEffect": false,
            "showProgressIndicator": true
        });
        imgCard = new kony.ui.Image2({
            "id": "imgCard" + cardFrmID,
            "top": "-10.3%", //-4% good for nexus , -8% good for sam
            "width": "100%",
            "height": "100.0%",
            "zIndex": 3,
            "isVisible": true,
            "src": imgSrc,
            "imageWhenFailed": imgSrcFailed,
            "imageWhileDownloading": "empty.png"
        }, {
            "padding": [0, 0, 0, 0],
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {});
        if (ribbonText != "") {
            lblRibbon = new kony.ui.Label({
                "id": "lblRibbon" + cardFrmID,
                "top": "10%", //10.41
                "right": "-12.0%", //-11.20
                "width": "50%",
                "height": "14.0%",
                "zIndex": 3,
                "isVisible": true,
                "text": ribbonText,
                "skin": ribbonSkin
            }, {
                "padding": [0, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {
                "textCopyable": false
            });
            addRibbon(lblRibbon)
        }
        lblLinkText = new kony.ui.Label({
            "id": "lblLinkText" + cardFrmID,
            "top": "0dp",
            "centerX": "50%",
            "zIndex": 3,
            "isVisible": true,
            "text": linkText,
            "skin": "lblbluemedium200"
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "textCopyable": false
        });
        flexLink = new kony.ui.FlexContainer({
            "id": "flexLink" + cardFrmID,
            "bottom": "3%",
            "left": "0dp",
            "width": "100%",
            "height": "35dp",
            "zIndex": 3,
            "isVisible": true,
            "clipBounds": true,
            "Location": "[0,217]",
            "skin": "slFbox",
            "layoutType": kony.flex.FREE_FORM
        }, {
            "padding": [0, 0, 0, 0]
        }, {});;
        flexLink.setDefaultUnit(kony.flex.DP)
        flexLink.add(lblLinkText);
        lblCardNo = new kony.ui.Label({
            "id": "lblCardNo" + cardFrmID,
            "top": "0%",
            "left": "10%",
            "width": "90%",
            "zIndex": 3,
            "isVisible": true,
            "text": cardNo,
            "skin": cardNoSkin
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "textCopyable": false
        });
        lblCardNickName = new kony.ui.Label({
            "id": "lblCardNickName" + cardFrmID,
            "top": "45%",
            "left": "10%",
            "width": "90%",
            "zIndex": 3,
            "isVisible": true,
            "text": cardNickName,
            "skin": "lblWhiteCard22px"
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "textCopyable": false
        });
        flexContent = new kony.ui.FlexContainer({
            "id": "flexContent" + cardFrmID,
            "top": "0",
            "left": "0",
            "width": "100%",
            "height": "30%",
            "centerX": "50%",
            "centerY": "59%",
            "zIndex": 3,
            "isVisible": true,
            "clipBounds": true,
            "Location": "[0,107]",
            "skin": "slFbox",
            "layoutType": kony.flex.FREE_FORM
        }, {
            "padding": [0, 0, 0, 0]
        }, {});;
        flexContent.setDefaultUnit(kony.flex.DP)
        flexContent.add(lblCardNo, lblCardNickName);
        flexCard = new kony.ui.FlexContainer({
            "id": "flexCard" + cardFrmID,
            "top": "5%",
            "left": "10.89%",
            "width": "83%",
            "height": cardheight,
            "centerX": "50.0%",
            "zIndex": 2,
            "isVisible": true,
            "clipBounds": true,
            "Location": "[39,202]",
            "skin": "noSkinFlexContainer",
            "layoutType": kony.flex.FREE_FORM
        }, {
            "padding": [0, 0, 0, 0]
        }, {});
        flexCard.setDefaultUnit(kony.flex.DP)
        if (ribbonText != "") {
            flexCard.add(btnCard, imgCard, lblRibbon, flexContent, flexLink);
        } else {
            flexCard.add(btnCard, imgCard, flexContent, flexLink);
        }
        if (currentFrm == "frmMBListDebitCard") {
            frmMBListDebitCard.FlexMain.add(flexCard);
        } else if (currentFrm == "frmMBListCreditCard") {
            frmMBListCreditCard.FlexMain.add(flexCard);
        }
    }
}

function clearMBCardList() {
    frmMBCardList.FlexMain.removeAll();
}

function clearMBDebitCardList() {
    frmMBListDebitCard.FlexMain.removeAll();
}

function clearMBCreditCardList() {
    frmMBListCreditCard.FlexMain.removeAll();
}

function postShowRibbonAll() {
    var gblDeviceInfo = kony.os.deviceInfo();
    var deviceName = gblDeviceInfo["name"];
    postShowRibbon("CC", gblCardList["creditCardRec"]);
    postShowRibbon("DC", gblCardList["debitCardRec"]);
    postShowRibbon("RC", gblCardList["readyCashRec"]);
}

function postShowDebitCardRibbon() {
    var trans100 = kony.ui.makeAffineTransform();
    for (var i = 0; i < gblCardList["debitCardRec"].length; i++) {
        if (undefined != frmMBListDebitCard["lblRibbon" + "DC" + i]) {
            trans100.rotate(-45);
            frmMBListDebitCard["lblRibbon" + "DC" + i].animate(kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans100
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {});
        }
    }
}

function postShowCreditCardRibbon() {
    var trans100 = kony.ui.makeAffineTransform();
    for (var i = 0; i < gblCardList["creditCardRec"].length; i++) {
        if (undefined != frmMBListCreditCard["lblRibbon" + "CC" + i]) {
            trans100.rotate(-45);
            frmMBListCreditCard["lblRibbon" + "CC" + i].animate(kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans100
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {});
        }
    }
    for (var i = 0; i < gblCardList["readyCashRec"].length; i++) {
        if (undefined != frmMBListCreditCard["lblRibbon" + "RC" + i]) {
            trans100.rotate(-45);
            frmMBListCreditCard["lblRibbon" + "RC" + i].animate(kony.ui.createAnimation({
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "transform": trans100
                }
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.25
            }, {});
        }
    }
}

function postShowRibbon(cardType, gblCardListCC) {
    for (var i = 0; i < gblCardListCC.length; i++) {
        addRibbon("lblRibbon" + cardType + i);
    }
}

function addRibbon(lblRibbon) {
    var trans100 = kony.ui.makeAffineTransform();
    if (undefined != frmMBCardList[lblRibbon]) {
        trans100.rotate(-45);
        frmMBCardList[lblRibbon].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {});
    }
}

function addRibbonListCreditCard(lblRibbon) {
    var trans100 = kony.ui.makeAffineTransform();
    if (undefined != frmMBListCreditCard[lblRibbon]) {
        trans100.rotate(-45);
        frmMBListCreditCard[lblRibbon].animate(kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {});
    }
}

function canIssueCard() {
    var nocard = 0;
    var maxDebit = 0;
    var allowList = gblCardList["allowIssueNewCardProductList"];
    var productCode = gblAccountTable["custAcctRec"][gblIndex]["productID"];
    //productCode = 210; //"200,203,205,212,219,220,222,225",
    kony.print("productCode " + productCode);
    kony.print("allowList " + allowList);
    kony.print("productCode.indexOf(allowList) " + productCode.indexOf(allowList));
    if (allowList.indexOf(productCode) < 0) {
        return false;
    }
    if (gblCardList["allowIssueNewCard"] != 'N') {
        return true;
    }
    return false;
}

function dynamicIssueNewCard() {
    var imgSrc = "";
    imgSrc = getImageSRC(imageIssueCardName);
    kony.print(" imgSrc : " + imgSrc);
    var imgIssueCard = new kony.ui.Image2({
        "id": "imgIssueCard",
        "top": "0%",
        "left": "-1dp",
        "width": "100%",
        "height": "100.0%",
        "zIndex": 1,
        "isVisible": true,
        "src": imgSrc,
        "imageWhenFailed": "empty.png",
        "imageWhileDownloading": "empty.png",
        "skin": "slImage"
    }, {
        "padding": [0, 0, 0, 0],
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
    });
    var btnIssueCard = new kony.ui.Button({
        "id": "btnIssueCard",
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "height": "100%",
        "centerX": "50%",
        "centerY": "50%",
        "zIndex": 10,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyIssueNC"),
        "skin": "btnBlue28pxTransBackgrnd",
        "focusSkin": "btnBlue28pxTransBackgrnd",
        "onClick": performIssueNewCard
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "glowEffect": false,
        "showProgressIndicator": true
    });
    var flexIssue = new kony.ui.FlexContainer({
        "id": "flexIssue",
        "top": "0",
        "left": "0",
        "width": "100%",
        "height": "91.67%",
        "centerX": "50%",
        "centerY": "50%",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[0,6]",
        "skin": "slFbox",
        "layoutType": kony.flex.FLOW_VERTICAL
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexIssue.setDefaultUnit(kony.flex.DP)
    flexIssue.add(btnIssueCard);
    var flexIssueCard = new kony.ui.FlexContainer({
        "id": "FlexIssueCard",
        "top": "0dp",
        "left": "10.89%",
        "width": "83%",
        "height": "40%",
        "centerX": "50.0%",
        "zIndex": 2,
        "isVisible": true,
        "clipBounds": true,
        "Location": "[39,186]",
        "skin": "noSkinFlexContainer",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flexIssueCard.setDefaultUnit(kony.flex.DP)
    flexIssueCard.add(imgIssueCard, flexIssue);
    frmMBListDebitCard.FlexMain.add(flexIssueCard);
}

function getCallbackShowDebitCardList(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblCardList = resulttable;
            dismissLoadingScreen();
            if (resulttable["debitCardRec"].length > 0) {
                dynamicDebitListCard(resulttable["debitCardRec"], "frmMBListDebitCard");
            }
            if (canIssueCard()) {
                var flexSession = new kony.ui.FlexContainer({
                    "id": "flexSession",
                    "top": "0dp",
                    "left": "0dp",
                    "width": "100%",
                    "height": "30dp",
                    "zIndex": 1,
                    "isVisible": true,
                    "clipBounds": true,
                    "Location": "[0,200]",
                    "skin": "noSkinFlexContainer",
                    "layoutType": kony.flex.FREE_FORM
                }, {
                    "padding": [0, 0, 0, 0]
                }, {});;
                frmMBListDebitCard.FlexMain.add(flexSession);
                dynamicIssueNewCard();
            }
            frmMBListDebitCard.show();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}

function getCallbackShowCardList(status, resulttable) {
    var cardType = "";
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            if (resulttable["errCode"] == undefined) {
                if (resulttable["creditCardRec"].length > 0 || resulttable["debitCardRec"].length > 0 || resulttable["readyCashRec"].length > 0) {
                    gblCardList = resulttable;
                    if (resulttable["creditCardRec"].length > 0) {
                        cardType = resulttable["creditCardRec"][0]["cardType"];
                        setCardHeader(cardType);
                        dynamicListCard(resulttable["creditCardRec"]);
                    }
                    if (resulttable["debitCardRec"].length > 0) {
                        cardType = resulttable["debitCardRec"][0]["cardType"];
                        setCardHeader(cardType);
                        dynamicListCard(resulttable["debitCardRec"]);
                    }
                    if (resulttable["readyCashRec"].length > 0) {
                        cardType = resulttable["readyCashRec"][0]["cardType"];
                        setCardHeader(cardType);
                        dynamicListCard(resulttable["readyCashRec"]);
                    }
                    frmMBCardList.show();
                } else {
                    frmMBCardProductLink.show();
                }
            } else {
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}

function getCallbackShowCreditCardList(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblCardList = resulttable;
            clearMBCreditCardList();
            dismissLoadingScreen();
            if (resulttable["creditCardRec"].length > 0) {
                dynamicDebitListCard(resulttable["creditCardRec"], "frmMBListCreditCard");
            }
            if (resulttable["readyCashRec"].length > 0) {
                dynamicDebitListCard(resulttable["readyCashRec"], "frmMBListCreditCard");
            }
            frmMBListCreditCard.show();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}

function setGblSelectedCardData() {
    var imageName = "";
    var imgSrc = "";
    var cardNo = "";
    var cardNickName = "";
    imageName = gblSelectedCard["IMG_CARD_ID"];
    imgSrc = getImageSRC(imageName);
    cardNo = gblSelectedCard["accountNoFomatted"].split("-").join("  ");
    if (gblSelectedCard["acctNickName"] != undefined) {
        cardNickName = gblSelectedCard["acctNickName"];
        if (cardNickName == "") {
            if (kony.i18n.getCurrentLocale() == "en_US") {
                cardNickName = gblSelectedCard["defaultCurrentNickNameEN"];
            } else {
                cardNickName = gblSelectedCard["defaultCurrentNickNameTH"];
            }
        }
    } else {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            cardNickName = gblSelectedCard["defaultCurrentNickNameEN"];
        } else {
            cardNickName = gblSelectedCard["defaultCurrentNickNameTH"];
        }
    }
    gblSelectedCard["imgSrc"] = imgSrc;
    gblSelectedCard["cardNo"] = cardNo;
    gblSelectedCard["cardName"] = cardNickName;
}

function gotoManageCard(cardInfo) {
    gblManageCardFlow = kony.application.getCurrentForm().id;
    gblCACardRefNumber = cardInfo["cardRefId"];
    frmMBManageCard.lblCardNumber.text = cardInfo["cardNo"];
    frmMBManageCard.lblCardAccountName.text = cardInfo["cardName"];
    frmMBManageCard.imgCard.src = cardInfo["imgSrc"];
    frmMBManageCard.show();
}

function onClickBackOfDebitCardScreen() {
    //frmAccountDetailsMB.show();
    if (isNotBlank(gblIndex)) {
        showAccountDetails({
            "id": "hboxSwipe" + gblIndex
        });
    } else {
        frmAccountDetailsMB.show();
    }
}

function getImageSRC(imageName) {
    var randomnum = Math.floor((Math.random() * 10000) + 1);
    var screendpiservice = "";
    getDeviceDpi();
    var card_image_url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + imageName + screendpiservice + "&modIdentifier=CARDPRODUCTIMG&dummy=" + randomnum;
    kony.print("getImageSRC " + card_image_url);
    return card_image_url;
}