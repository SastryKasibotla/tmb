/**
 * typeFlag 1 means OTP , 2 means access pin and 3 means transaction pwd
 * @returns {}
 */
function showOTPPopupSpa(lblText, refNo, mobNO, callBackConfirm, typeFlag) {
    popOtpSpa.hbxtoken.setVisibility(false);
    popOtpSpa.hboxotp.setVisibility(true);
    popOtpSpa.containerWeight = 94;
    //Please DO NOT KEEP THIS FUNCTION OUTSIDE. This is inner function assigned to popupTractPwd.btnPopupTractConf.onClick
    function onConfClick() {
        //popupTractPwd.dismiss();
        var enteredText = "";
        if (typeFlag == 1) {
            //enteredText = popupTractPwd.tbxPopupTractPwdtxt.text;
            enteredText = popOtpSpa.txtOTP.text;
            if (popOtpSpa.hbxIncorrectOTP.isVisible == true) {
                enteredText = popOtpSpa.txtIncorrectOTP.text;
            }
        }
        callBackConfirm(enteredText);
    }
    popOtpSpa.btnPopupTractConf.onClick = onConfClick;
    popOtpSpa.txtIncorrectOTP.text = "";
    popOtpSpa.txtOTP.text = "";
    //popOtpSpa.lblPopTractPwdtxt.text = lblText;
    popOtpSpa.lblOTP.text = lblText;
    popOtpSpa.hbxIncorrectOTP.isVisible = false;
    if (typeFlag == 1) {
        popOtpSpa.lblPopupTract1.isVisible = true;
        popOtpSpa.hbxPopupTractlblHoldSpa.isVisible = true;
        popOtpSpa.lblPopupTract5.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        popOtpSpa.lblPopupTract5.isVisible = true;
        popOtpSpa.lblPopupTract4Spa.text = mobNO;
        popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
        popOtpSpa.btnPopUpTractCancel.focusSkin = btnDisabledGray;
        popOtpSpa.btnPopUpTractCancel.setEnabled(false);
        //popOtpSpa.btnPopUpTractCancel.onClick = onClickOTPRequest;
        popOtpSpa.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyRequest");
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            //popOtpSpa.lblPopTractPwdtxt.containerWeight = 25;
            popOtpSpa.lblOTP.containerWeight = 25;
        } else {
            //popOtpSpa.lblPopTractPwdtxt.containerWeight = 12;
            popOtpSpa.lblOTP.containerWeight = 12;
        }
        //popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno")+gblRefNum; Commented as added in onClickOTPRequest
        //popOtpSpa.hbxPoupOTP.isVisible = true;
        popOtpSpa.hbxOTP.isVisible = true;
        //popOtpSpa.lblPopTractPwdtxt.text = lblText;
        popOtpSpa.lblOTP.text = lblText;
        //gblOTPFlag = true;
        //onClickOTPRequest();
        //popOtpSpa.tbxPopupTractPwdtxt.text = "";
        //popOtpSpa.tbxPopupTractPwdtxt.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC; 
        //popOtpSpa.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
        //popOtpSpa.txtIncorrectOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
        popOtpSpa.txtOTP.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
        popOtpSpa.txtIncorrectOTP.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    }
    if (spaChnage == "activation") {
        popOtpSpa.txtOTP.placeholder = ""
        popOtpSpa.txtIncorrectOTP.placeholder = ""
    } else {
        //popOtpSpa.txtOTP.placeholder="Enter OTP"
        popOtpSpa.txtIncorrectOTP.placeholder = "Enter OTP"
    }
    popOtpSpa.show();
    document.getElementById('popOtpSpa_txtOTP').type = "tel"
    document.getElementById('popOtpSpa_txtIncorrectOTP').type = "tel"

    function onConfClickSpa() {
        //popOtpSpa.dismiss();
        var enteredText = "";
        if (typeFlag == 1) {
            //enteredText = popOtpSpa.tbxPopupTractPwdtxt.text;
            enteredText = popOtpSpa.txtOTP.text;
            if (popOtpSpa.hbxIncorrectOTP.isVisible == true) {
                enteredText = popOtpSpa.txtIncorrectOTP.text;
            }
        } else if (typeFlag == 2) {
            enteredText = popOtpSpa.tbxPopupTractPwdtxtAccPin.text;
        } else if (typeFlag == 3) {
            enteredText = popOtpSpa.tbxPopupTractPwdtxtTranscPwd.text;
        }
        callBackConfirm(enteredText);
    }
}
/**
 * description
 * Cancelling the otpTimer
 * @returns {}
 */
function otpTimerCallBackSpa() {
    popOtpSpa.btnPopUpTractCancel.skin = btnLightBlue;
    popOtpSpa.btnPopUpTractCancel.setEnabled(true);
    //popOtpSpa.btnPopUpTractCancel.onClick = onClickActiRequestOtpSpa;
    if (spaChnage == "activation") {
        onClickOTPSpaAactivate()
    } else {
        //onClickOTPRequestSpa()
        generateOtpUserSpa()
    }
    gblOTPFlag = true;
    try {
        kony.timer.cancel("otpTimer")
    } catch (e) {}
}
/*
*************************************************************************************
		Module	: onClickActiRequestOtp
		Author  : Kony
		Purpose : Defining onclick for Request button for OTP pop up
****************************************************************************************
*/
function onClickActiRequestOtpSpa() {
    gblOTPFlag = true;
    gblOnClickReq = true;
    popOtpSpa.btnPopUpTractCancel.setEnabled(false);
    onClickOTPRequestSpa();
}
/*
*************************************************************************************
		Module	: onClickOTPRequest
		Author  : Kony
		Purpose : Invoking request OTP kony service
****************************************************************************************
*/
function onClickOTPRequestSpa() {
    spaTknfalg = false;
    if (gblSwitchToken == false && gblTokenSwitchFlag == false && gblSpaTokenServFalg == true) {
        spaTknfalg = true;
        checkTokenFlagSpa();
    }
    if (spaTknfalg == false) {
        spaTokenDisplay();
    }
}

function spaTokenDisplay() {
    if (gblTokenSwitchFlag == false && gblSwitchToken == false) {
        //alert("why here")
        popOtpSpa.hbxtoken.setVisibility(false);
        popOtpSpa.hboxotp.setVisibility(true);
        generateOtpUserSpa();
    } else if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        popOtpSpa.hbxtoken.setVisibility(true);
        popOtpSpa.hboxotp.setVisibility(false);
        popOtpSpa.txttokenspa.text = "";
        dismissLoadingScreen();
        popOtpSpa.show();
    } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
        popOtpSpa.hbxtoken.setVisibility(false);
        popOtpSpa.hboxotp.setVisibility(true);
        generateOtpUserSpa();
    }
}

function checkTokenFlagSpa() {
    var inputParam = [];
    showLoadingScreen();
    gblSpaTokenServFalg = false;
    invokeServiceSecureAsync("tokenSwitching", inputParam, SpaTokenFlagCallbackfunction);
}

function SpaTokenFlagCallbackfunction(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            dismissLoadingScreen();
            if (callbackResponse["deviceFlag"].length == 0) {
                //return
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus == '02') {
                    popOtpSpa.hbxtoken.setVisibility(true);
                    popOtpSpa.hboxotp.setVisibility(false);
                    gblTokenSwitchFlag = true;
                } else {
                    popOtpSpa.hbxtoken.setVisibility(false);
                    popOtpSpa.hboxotp.setVisibility(true);
                    gblTokenSwitchFlag = false;
                }
            }
        }
        if (spaTknfalg) spaTokenDisplay();
        else {
            dismissLoadingScreen();
        }
    }
}

function CallbackonClickOTPRequestSpa(status, resulttable) {
    gblSpaAmount = "";
    gblSpaBillerName = "";
    gblSpaRef1Value = "";
    gblToSpaAccountNumber = "";
    gblToSpaAccountName = "";
    gblAccountDetails = "";
    if (resulttable["errCode"] == "GenOTPRtyErr00002") {
        kony.application.dismissLoadingScreen();
        gblOTPFlag = false;
        popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
        popOtpSpa.btnPopUpTractCancel.setEnabled(false);
        popOtpSpa.dismiss();
        kony.timer.cancel("ProfileOTPTimer");
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (resulttable["errCode"] == "GenOTPRtyErr00001") {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (status == 400) {
        if (resulttable["errCode"] == "GenOTPRtyErr00001") {
            //alert("Test1");
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (resulttable["opstatus"] == 0) {
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            kony.timer.schedule("otpTimer", otpTimerCallBackSpa, reqOtpTimer, false);
            popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
            popOtpSpa.btnPopUpTractCancel.setEnabled(false);
            if (gblOnClickReq == false) {
                showOTPPopupSpa(kony.i18n.getLocalizedString("keyOTP"), "ABCD", "xxx-xxx-" + gblPHONENUMBER.substring(6, 10), otpValidationspa, 1)
                    //(kony.i18n.getLocalizedString("keyOTP"), "ABCD","xxx-xxx-" + gblPHONENUMBER.substring(6, 10),otpValidationspa, 1)
            }
            var refVal = "";
            for (var d = 0; d < resulttable["Collection1"].length; d++) {
                if (resulttable["Collection1"][d]["keyName"] == "pac") {
                    refVal = resulttable["Collection1"][d]["ValueString"];
                    break;
                }
            }
            popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + " " + refVal;
            if (flowSpa) {
                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("keyotpmsg");
                popOtpSpa.lblPopupTract7.text = "";
                popOtpSpa.lblPopupTract4Spa.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            } else {
                popOtpSpa.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
                popOtpSpa.lblPopupTract7.text = "";
                popOtpSpa.lblPopupTract4.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            }
            gblOTPFlag = false;
        } else {
            /*
            	var refVal="";
            	for(var d=0;d < resulttable["Collection1"].length;d++){
            		if(resulttable["Collection1"][d]["keyName"] == "pac"){
            				refVal=resulttable["Collection1"][d]["ValueString"];
            				break;
            			}
            	}*/
            popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + " " + refVal;
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            kony.timer.schedule("otpTimer", otpTimerCallBack, reqOtpTimer, false);
            if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                gblOTPFlag = false;
                return false;
            } else {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                gblOTPFlag = false;
                return false;
            }
        }
        kony.application.dismissLoadingScreen();
    }
}
/**
 * description
 * Cancelling the otpTimer
 * @returns {}
 */
function otpTimerCallBack() {
    popOtpSpa.btnPopUpTractCancel.skin = btnLightBlue;
    popOtpSpa.btnPopUpTractCancel.setEnabled(true);
    popOtpSpa.btnPopUpTractCancel.onClick = onClickActiRequestOtpSpa;
    gblOTPFlag = true;
    try {
        kony.timer.cancel("otpTimer")
    } catch (e) {}
}
/**
 * description
 * Identifing common variables to make as a OTP generic function.
 */
function generateOtpUserSpa() {
    var inputParam = {};
    if (gblOTPFlag) {
        inputParam["Channel"] = gblSpaChannel;
        if (spaChnage == "addrecipients" || spaChnage == "editProfile") {
            inputParam["userAccountNum"] = gblToSpaAccountName;
        }
        inputParam["userAccountName"] = gblToSpaAccountName;
        inputParam["AccDetailMsg"] = gblAccountDetails;
        inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
        if (gblSpaChannel == "OpenAccountOnline") {
            //For Activity Logging
            var platformChannel = gblDeviceInfo.name;
            if (platformChannel == "thinclient") inputParam["channelId"] = GLOBAL_IB_CHANNEL;
            else inputParam["channelId"] = GLOBAL_MB_CHANNEL;
            inputParam["logLinkageId"] = "";
            inputParam["deviceNickName"] = "";
            inputParam["activityFlexValues2"] = "";
            if (gblSelOpenActProdCode == "200" || gblSelOpenActProdCode == "220" || gblSelOpenActProdCode == "222") {
                inputParam["activityTypeID"] = "039";
                inputParam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text;
                inputParam["activityFlexValues3"] = frmOpenAccountNSConfirmation.lblNickNameVal.text;
                inputParam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
                inputParam["activityFlexValues5"] = "";
                //activityLogServiceCall("039", "", "00", "", gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text, "",frmOpenAccountNSConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", ""); //Savings 
            } else if (gblSelOpenActProdCode == "221") {
                inputParam["activityTypeID"] = "042";
                inputParam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text;
                inputParam["activityFlexValues3"] = frmOpenAccountNSConfirmation.lblNickNameVal.text;
                inputParam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
                inputParam["activityFlexValues5"] = "";
                //activityLogServiceCall("042", "", "00", "", gblSelOpenActProdCode + "+" + frmOpenAccountNSConfirmation.lblProdNSConfName.text, "",frmOpenAccountNSConfirmation.lblNickNameVal.text, gblFinActivityLogOpenAct["transferAmount"], "", "");
            } else if (gblSelOpenActProdCode == "206") {
                var transferAmount = frmOpenActDSConfirm.lblOADSAmtVal.text;
                var targetAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
                var bhat = kony.i18n.getLocalizedString("currencyThaiBaht");
                transferAmount = transferAmount.replace(bhat, "");
                targetAmount = targetAmount.replace(bhat, "").replace(/,/g, "")
                frmOpenActDSAck.label475124774164.text = kony.i18n.getLocalizedString("Complete");
                transferAmount = transferAmount.replace(/,/g, "");
                inputParam["activityTypeID"] = "040";
                inputParam["activityFlexValues1"] = frmOpenActDSConfirm.lblOADMnthVal.text;
                inputParam["activityFlexValues3"] = frmOpenActDSConfirm.lblOAMnthlySavNum.text;
                inputParam["activityFlexValues4"] = transferAmount;
                inputParam["activityFlexValues5"] = gblspaDreamName + "+" + targetAmount;
                /*if(flowSpa)
                {
                activityLogServiceCall("040", "", "00", "", frmOpenActDSConfirm.lblOADMnthVal.text, "",frmOpenActDSConfirm.lblOAMnthlySavNum.text, transferAmount, (gblspaDreamName + "+" + targetAmount), ""); //Dream Savings
                }
                else
                {
                	
                activityLogServiceCall("040", "", "00", "", frmOpenActDSConfirm.lblOADMnthVal.text, "",frmOpenActDSConfirm.lblOAMnthlySavNum.text, transferAmount, (frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].imgDreamName + "+" + targetAmount), ""); //Dream Savings
                }*/
            } else if (gblSelOpenActProdCode == "203") {
                var befPer = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal1.text;
                var befPerVthSym = "";
                befPer = befPer.replace("%", "");
                var befName = frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm1.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal1.text + " " + befPer;
                if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm2.isVisible == true) {
                    befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal2.text;
                    befPerVthSym = befPerVthSym.replace("%", "");
                    befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm2.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal2.text + " " + befPerVthSym;
                }
                if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm3.isVisible == true) {
                    befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal3.text;
                    befPerVthSym = befPerVthSym.replace("%", "");
                    befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm3.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal3.text + " " + befPerVthSym;
                }
                if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm4.isVisible == true) {
                    befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal4.text;
                    befPerVthSym = befPerVthSym.replace("%", "");
                    befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm4.text + " " + frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal4.text + " " + befPerVthSym;
                }
                if (frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm5.isVisible == true) {
                    befPerVthSym = frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text;
                    befPerVthSym = befPerVthSym.replace("%", "");
                    befName = befName + "," + frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm5.text + " " + frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text + " " + befPerVthSym;
                }
                inputParam["activityTypeID"] = "081";
                inputParam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmOpenActSavingCareCnfNAck.lblOASCTitle.text;
                inputParam["activityFlexValues3"] = frmOpenActSavingCareCnfNAck.lblOASCNicNamVal.text;
                inputParam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
                inputParam["activityFlexValues5"] = befName;
                //activityLogServiceCall("081", "", "00", "", gblSelOpenActProdCode + "+" + frmOpenActSavingCareCnfNAck.lblOASCTitle.text, "",frmOpenActSavingCareCnfNAck.lblOASCNicNamVal.text, gblFinActivityLogOpenAct["transferAmount"], befName, ""); //Savings Care
            } else if (gblSelProduct == "ForTerm") {
                inputParam["activityTypeID"] = "041";
                inputParam["activityFlexValues1"] = gblSelOpenActProdCode + "+" + frmOpenActTDConfirm.lblTDCnfmTitle.text;
                inputParam["activityFlexValues3"] = frmOpenActTDConfirm.lblOATDNickNameVal.text;
                inputParam["activityFlexValues4"] = gblFinActivityLogOpenAct["transferAmount"];
                inputParam["activityFlexValues5"] = "";
            }
        } else if (gblSpaChannel == "BillPayment") {
            inputParam["billerCategoryID"] = gblBillerCategoryID;
        } else if (gblSpaChannel == "ActivateCard") {}
        invokeServiceSecureAsync("generateOTPWithUser", inputParam, CallbackonClickOTPRequestSpa);
    }
}

function generateOtpUserSpaNewMob() {
    var inputParam = {};
    if (gblOTPFlag) {
        inputParam["Channel"] = gblSpaChannel;
        inputParam["locale"] = kony.i18n.getCurrentLocale();
        inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
        invokeServiceSecureAsync("generateOTP", inputParam, CallbackonClickOTPRequestSpanewMob);
    }
}

function CallbackonClickOTPRequestSpanewMob(status, resulttable) {
    gblSpaAmount = "";
    gblSpaBillerName = "";
    gblSpaRef1Value = "";
    gblToSpaAccountNumber = "";
    gblToSpaAccountName = "";
    gblAccountDetails = "";
    if (resulttable["errCode"] == "GenOTPRtyErr00002") {
        kony.application.dismissLoadingScreen();
        gblOTPFlag = false;
        popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
        popOtpSpa.btnPopUpTractCancel.setEnabled(false);
        popOtpSpa.dismiss();
        kony.timer.cancel("ProfileOTPTimer");
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (resulttable["errCode"] == "GenOTPRtyErr00001") {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (status == 400) {
        if (resulttable["errCode"] == "GenOTPRtyErr00001") {
            //alert("Test1");
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (resulttable["opstatus"] == 0) {
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            kony.timer.schedule("otpTimer", otpTimerCallBackSpa, reqOtpTimer, false);
            popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
            popOtpSpa.btnPopUpTractCancel.setEnabled(false);
            if (gblOnClickReq == false) {
                showOTPPopupSpa(kony.i18n.getLocalizedString("keyOTP"), "ABCD", maskedNewMob.substring(6, 10), otpValidationspa, 1)
                    //(kony.i18n.getLocalizedString("keyOTP"), "ABCD","xxx-xxx-" + gblPHONENUMBER.substring(6, 10),otpValidationspa, 1)
            }
            var refVal = "";
            for (var d = 0; d < resulttable["Collection1"].length; d++) {
                if (resulttable["Collection1"][d]["keyName"] == "pac") {
                    refVal = resulttable["Collection1"][d]["ValueString"];
                    break;
                }
            }
            popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + " " + refVal;
            if (flowSpa) {
                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("keyotpmsg");
                popOtpSpa.lblPopupTract7.text = "";
                popOtpSpa.lblPopupTract4Spa.text = "xxx-xxx-" + maskedNewMob.substring(6, 10);
            } else {
                popOtpSpa.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
                popOtpSpa.lblPopupTract7.text = "";
                popOtpSpa.lblPopupTract4.text = "xxx-xxx-" + maskedNewMob.substring(6, 10);
            }
            gblOTPFlag = false;
        } else {
            /*
            	var refVal="";
            	for(var d=0;d < resulttable["Collection1"].length;d++){
            		if(resulttable["Collection1"][d]["keyName"] == "pac"){
            				refVal=resulttable["Collection1"][d]["ValueString"];
            				break;
            			}
            	}*/
            popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + " " + refVal;
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            kony.timer.schedule("otpTimer", otpTimerCallBack, reqOtpTimer, false);
            if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                gblOTPFlag = false;
                return false;
            } else {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                gblOTPFlag = false;
                return false;
            }
        }
        kony.application.dismissLoadingScreen();
    }
}

function popOtpSpaOnClickOfRequestOTPButton() {
    if (spaChnage == "activation") {
        onClickOTPSpaAactivate()
    } else {
        //onClickOTPRequestSpa()
        if (spaChnage == "editmyprofile") {
            if (caseTrans == "Number") {
                generateOtpUserSpaNewMob();
            } else {
                generateOtpUserSpa()
                popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
                popOtpSpa.btnPopUpTractCancel.focusSkin = btnDisabledGray;
                popOtpSpa.btnPopUpTractCancel.setEnabled(false);
            }
        } else {
            generateOtpUserSpa()
            popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
            popOtpSpa.btnPopUpTractCancel.focusSkin = btnDisabledGray;
            popOtpSpa.btnPopUpTractCancel.setEnabled(false);
        }
    }
}