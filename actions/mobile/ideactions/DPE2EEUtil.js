function encryptForDPAndroid(cleartext){

	var e2eeDPResult = E2EEEncryptDP.encryptDP("", cleartext, gblDPPk, gblDPRandNumber);
	
	kony.print("E2EEDP Return Code = "+e2eeDPResult[1]);
	kony.print("E2EEDP RPIN = "+e2eeDPResult[0]);
	
	return e2eeDPResult[0];
}

function encryptForDPForIphone(cleartext){
	
	var e2eeDPResult = E2EEEncryptDP.encryptDPFuncForiPhone(cleartext, gblDPPk, gblDPRandNumber);
	return e2eeDPResult;
}

function encryptForDPForDesktopWeb(cleartext){
	//#ifdef desktopweb
		var cipherparams_hash='{\"hash\":true,\"hashAlgo\":\"SHA-256\"}';
		try{
			var e2eeDPResult = amdp.encrypt(cipherparams_hash,gblDPPk,gblDPRandNumber,cleartext);
		}catch (e) {
	        alert(e);
	        throw e;
	    }
	    return e2eeDPResult;
    //#endif
}

function encryptDPUsingE2EE(cleartext){
	var e2eeDPResult="";
	if(gblDPPk!="" && gblDPRandNumber!=""){
	
		//#ifdef android
			e2eeDPResult = encryptForDPAndroid(cleartext);
		//#endif

		//#ifdef iphone
			e2eeDPResult = encryptForDPForIphone(cleartext);
		//#endif

		//#ifdef desktopweb
			e2eeDPResult = encryptForDPForDesktopWeb(cleartext);
		//#endif
		

		if(undefined != e2eeDPResult){
			return e2eeDPResult;
		}else{
			//Error handling when encryption fail
			return "";
		}	
	}else{
		//Error handling when init service failed
		return e2eeDPResult;
	}
}