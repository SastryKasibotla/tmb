maxLenChangePin = 4;
remainAttempt = 3;

function onClickChangePin() {
    if (checkMBUserStatus()) {
        initializeCPPinAssignNew();
        initChangePINScreen();
        clearExistChangePIN();
        frmMBChangePINEnterExistsPin.show();
    }
}

function initChangePINScreen() {
    changePINBoxSkin("slFbox");
    frmMBChangePINEnterExistsPin.flxPinPad.setVisibility(true);
    frmMBChangePINEnterExistsPin.flxnewpinlink.setVisibility(false);
}

function onTouchPIN() {
    if (!frmMBChangePINEnterExistsPin.flxPinPad.isVisible) {
        initChangePINScreen();
    }
}

function gotoEnterConfirmChangePIN() {
    //if(status == "0"){ //success 
    // show confirm change PIN Screen
    //initialChangePINParam();
    //clearChangePINMBNew();
    initializeCPPinAssignNew();
    frmMBChangePINConfirm.show();
    //}
}
// Confirmation PIN
function gotoChangePinSuccess() {
    frmMBChangePinSuccess.show();
}

function btnCancelEventExistChangePin() {
    frmMBManageCard.show();
    frmMBChangePINEnterExistsPin.destroy();
}

function changeBtnPinClick(eventobject) {
    if (glbPin.length < maxLenChangePin) {
        assignChangePINRender(eventobject["text"]);
    }
}

function assignChangePINRender(pin) {
    if (isNotBlank(pin)) {
        addChangePin(pin, ++glbIndex);
    } else {
        renderChangePIN();
    }
}

function addChangePin(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderChangePIN();
    if (glbPin.length == maxLenChangePin) {
        kony.print("glbPin----------->" + glbPin);
        initOnlineChangePin();
    }
}

function verifyChangeExistingPIN() {}

function clearExistChangePIN() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    assignChangePINRender("");
}

function renderChangePIN() {
    var btnObject = [frmMBChangePINEnterExistsPin.imgPin1, frmMBChangePINEnterExistsPin.imgPin2, frmMBChangePINEnterExistsPin.imgPin3, frmMBChangePINEnterExistsPin.imgPin4];
    var emptyPinDis = "pin_input.png";
    var emptyPin = "pin_input_active.png";
    var fillPin = "pin_input_filled.png";
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].src = fillPin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].src = emptyPin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].src = emptyPinDis;
        }
    }
}

function deleteExistChangePIN() {
    if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenChangePin) {
        glbPin = glbPinArr[0];
        glbIndex = glbPinArr[0].length;
        glbPinArr = [];
    }
    if (isNotBlank(glbPin)) {
        decreaseChangePINVal();
    }
}

function decreaseChangePINVal() {
    --glbIndex;
    glbPin = glbPin.substring(0, glbPin.length - 1);
    assignChangePINRender("");
}

function encryptChangePinUsingE2EE(pin) {
    var e2eeResult = "";
    if (gblPk != "" && gblRand != "" && gblAlgorithm != "") {
        e2eeResult = encryptForATMPinAndroid(pin);
    }
    return e2eeResult;
}

function onClickCloseChangePINLocked() {
    frmMBChangePINEnterExistsPin.destroy();
    frmMBManageCard.show();
}

function onClickForgotPINToRequestNewPIN() {
    if (gblSelectedCard["allowRequestNewPin"] == "N") {
        showAlert(kony.i18n.getLocalizedString("msgNotAllowRequestNewPIN"), kony.i18n.getLocalizedString("info"));
        return false;
    } else {
        onClickOfRequestNewPinMenu();
        frmMBChangePINEnterExistsPin.destroy();
    }
}

function onClickManageOfChangePINSuccess() {
    frmMBChangePINEnterExistsPin.destroy();
    gblisCardActivationCompleteFlow = true;
    onClickBackManageListCard();
}

function onClickManageThisCardOfChangePINSuccess() {
    gotoManageCard(gblSelectedCard);
}