/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Set toggle on or off according to gblTouchStatus
 *     Params  : None
 ******************************************************************/
function setEnableDisableTouchLogin() {
    if (gblTouchStatus == "Y") {
        //frmTouchIdSettings.lblChkBox.text=kony.i18n.getLocalizedString("keyDisableTouchId");;
        //frmTouchIdSettings.btnPwdOn.skin = btnOnFocus;
        //frmTouchIdSettings.btnPwdOff.skin = btnOffFocus;
        if (gblDeviceInfo["name"] == "iPhone") {
            frmTouchIdSettings.chkBoxGrp.selectedIndex = 0;
        } else if (gblDeviceInfo["name"] == "android") {
            frmFPSetting.imgED.src = "android_sel.png";
            imgEDSelected = 0;
            showFPFlex(false);
        }
    } else if (gblTouchStatus == "N") {
        //frmTouchIdSettings.lblChkBox.text=kony.i18n.getLocalizedString("keyEnableTouchId");;
        //frmTouchIdSettings.btnPwdOn.skin = btnOnNormal;
        //frmTouchIdSettings.btnPwdOff.skin = btnOffNorm;
        if (gblDeviceInfo["name"] == "iPhone") {
            frmTouchIdSettings.chkBoxGrp.selectedIndex = 1;
        } else if (gblDeviceInfo["name"] == "android") {
            frmFPSetting.imgED.src = "android_unsel.png";
            imgEDSelected = 1;
            showFPFlex(false);
        }
    }
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Show Touch Id Info Pop Up
 *     Params  : None
 ******************************************************************/
function onEnableTouchLogin() {
    kony.print("FPRINT: IN onEnableTouchLogin");
    setTouchPwdNormal();
    popUpTouchIdTransPwd.lblTouchPopupErrMsg.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
    popUpTouchIdTransPwd.lblTouchPopupErrMsg.skin = "lblPopupLabelTxt";
    popUpTouchIdTransPwd.btnPopTouchConf.text = kony.i18n.getLocalizedString("keyConfirm");
    popUpTouchIdTransPwd.btnPopTouchCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popUpTouchIdTransPwd.show();
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : On selecting Touch Id from menu , to show touch id form
 *     Params  : None
 ******************************************************************/
function onClickTouchIdSettings() {
    frmTouchIdSettings.btnTouchID.setVisibility(false);
    frmTouchIdSettings.show();
}

function onClickTouchIdAndroidSettings() {
    frmFPSetting.btnReturnHome.setVisibility(false);
    frmFPSetting.show();
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Called from Login callback . To show Touch Id intermediate form
 *     Params  : resulttable
 ******************************************************************/
function getValidTouchCounter() {
    kony.print("FPRINT: IN getValidTouchCounter()");
    //if(gblDeviceInfo["name"] == "iPhone" && gblTouchDevice == true){
    if ((gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android") && gblTouchDevice == true) {
        var count = kony.store.getItem("TouchCounter");
        kony.print("FPRINT: TouchCounter=" + count);
        var maxLimit = GBL_TOUCH_ID_STATUS_COUNT;
        kony.print("FPRINT: maxLimit=" + maxLimit);
        if (count != null || count != undefined) {
            if (gblNewCrmId != null && gblNewCrmId == "YES") {
                count--;
                gblNewCrmId = "NO";
                kony.print("FPRINT IN NEW CRMID COUNT=" + count);
            }
            if (count < maxLimit) {
                count++;
                kony.store.setItem("TouchCounter", count);
                return true;
            } else {
                return false;
            }
        } else {
            kony.store.setItem("TouchCounter", 1);
            var count1 = kony.store.getItem("TouchCounter");
            return true;
        }
    } else {
        return false
    }
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Function on clcik of cancel button of transaction password popup
 *     Params  : None
 ******************************************************************/
function onCancelTouchTransPwd() {
    popUpTouchIdTransPwd.tbxPopTouchPwd.text = "";
    setEnableDisableTouchLogin();
    popUpTouchIdTransPwd.dismiss();
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Function on clcik of confirm button of transaction password popup. To get device id
 *     Params  : Password
 ******************************************************************/
function onConfirmGetDeviceID() {
    kony.print("FPRINT: IN onConfirmGetDeviceID");
    showLoadingScreen();
    GBL_FLOW_ID_CA = 7;
    //TrusteerDeviceId();
    getUniqueID();
    setTouchPwdNormal();
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : callback for recieving device id
 *     Params  : Password
 ******************************************************************/
function callTouchIdStatusService(deviceID) {
    kony.print("FPRINT: IN callTouchIdStatusService");
    var pwd = "";
    var selectedKey = "";
    if (gblDeviceInfo["name"] == "iPhone") {
        selectedKey = frmTouchIdSettings.chkBoxGrp.selectedIndex;
    } else if (gblDeviceInfo["name"] == "android") {
        selectedKey = imgEDSelected;
        kony.print("FPRINT: IN ANDROID selectedKey=" + selectedKey);
    }
    // alert("selectedKey :"+selectedKey);
    if (selectedKey == 1) {
        kony.print("FPRINT: IN her1 ");
        pwd = "";
    } else {
        kony.print("FPRINT: IN her0 ");
        pwd = popUpTouchIdTransPwd.tbxPopTouchPwd.text;
        kony.print("FPRINT: pwd=" + pwd);
        if (pwd == null || pwd == undefined || pwd == "") {
            kony.print("FPRINT: pwd1");
            dismissLoadingScreen();
            popUpTouchIdTransPwd.lblTouchPopupErrMsg.text = kony.i18n.getLocalizedString("emptyMBTransPwd");
            popUpTouchIdTransPwd.lblTouchPopupErrMsg.skin = "lblPopUpErr";
            //alert(kony.i18n.getLocalizedString("keyPasswordRequired"));
            return;
        } else {
            kony.print("FPRINT: pwd2");
            popUpTouchIdTransPwd.lblTouchPopupErrMsg.text = kony.i18n.getLocalizedString("keyTransPwdMsg"); //kony.i18n.getLocalizedString("keyPasswordRequired");
            popUpTouchIdTransPwd.lblTouchPopupErrMsg.skin = "lblPopupLabelTxt";
        }
    }
    inputParam = {};
    inputParam["password"] = pwd;
    inputParam["appID"] = appConfig.appId;
    inputParam["deviceId"] = deviceID;
    if (selectedKey == 1) {
        kony.print("FPRINT: setting  touchIdStatus=N");
        inputParam["touchIdStatus"] = "N";
    } else {
        kony.print("FPRINT: setting  touchIdStatus=Y");
        inputParam["touchIdStatus"] = "Y";
    }
    kony.print("FPRINT: CALLING TouchIdStatusUpdate SERVICE");
    invokeServiceSecureAsync("TouchIdStatusUpdate", inputParam, callBackPasswordVerification);
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : callback of LoginTouchCompositeService. Update touch status in db
 *     Params  : Password
 ******************************************************************/
function callBackPasswordVerification(status, resulttable) {
    dismissLoadingScreen();
    popUpTouchIdTransPwd.tbxPopTouchPwd.text = "";
    var selectedKey = "";
    if (gblDeviceInfo["name"] == "iPhone") {
        selectedKey = frmTouchIdSettings.chkBoxGrp.selectedIndex;
    } else if (gblDeviceInfo["name"] == "android") {
        selectedKey = imgEDSelected;
    }
    kony.print("FPRINT: IN callBackPasswordVerification");
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            /*if (selectedKey == 1){
            	//frmTouchIdSettings.btnPwdOn.skin = btnOnNormal
            	//frmTouchIdSettings.btnPwdOff.skin = btnOffNorm
            	//frmTouchIdSettings.lblChkBox.text=kony.i18n.getLocalizedString("keyEnableTouchId");
            	gblTouchStatus = "N";
            }else {
            	//frmTouchIdSettings.btnPwdOn.skin = btnOnFocus;
            	//frmTouchIdSettings.btnPwdOff.skin = btnOffFocus;
            	//frmTouchIdSettings.lblChkBox.text=kony.i18n.getLocalizedString("keyDisableTouchId");
            	gblTouchStatus = "Y"
            }*/
            if (resulttable["tchIdStatus"] != null && resulttable["tchIdStatus"] != "") {
                gblTouchStatus = resulttable["tchIdStatus"];
                if (gblTouchStatus == "Y") {
                    if (gblDeviceInfo["name"] == "iPhone") {
                        frmTouchIdSettings.chkBoxGrp.selectedIndex = 0;
                    } else if (gblDeviceInfo["name"] == "android") {
                        frmFPSetting.imgED.src = "android_sel.png";
                        imgEDSelected = 0;
                    }
                } else {
                    if (gblDeviceInfo["name"] == "iPhone") {
                        frmTouchIdSettings.chkBoxGrp.selectedIndex = 1;
                    } else if (gblDeviceInfo["name"] == "android") {
                        frmFPSetting.imgED.src = "android_unsel.png";
                        imgEDSelected = 1;
                    }
                }
            } else {
                gblTouchStatus = "N";
                if (gblDeviceInfo["name"] == "iPhone") {
                    frmTouchIdSettings.chkBoxGrp.selectedIndex = 1;
                } else if (gblDeviceInfo["name"] == "android") {
                    frmFPSetting.imgED.src = "android_unsel.png";
                    imgEDSelected = 1;
                }
            }
            kony.store.setItem("usesTouchId", gblTouchStatus);
            if (gblDeviceInfo["name"] == "iPhone") {
                frmTouchIdSettings.btnTouchID.setVisibility(true);
            } else if (gblDeviceInfo["name"] == "android") {
                kony.print("FPRINT: MAKING btnReturnHome VISIBLE");
                frmFPSetting.btnReturnHome.setVisibility(true);
            }
            gblTouchActSum = "Y";
            kony.print("FPRINT: SUCESSFULL!!!!");
            popUpTouchIdTransPwd.dismiss();
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTouchPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
            return false;
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003" || resulttable["errCode"] == "VrfyPWDErrGen") {
            popUpTouchIdTransPwd.dismiss();
            showTranPwdLockedPopup();
            return false;
        } else if (resulttable["opstatus"] == "-1") {
            dismissLoadingScreen();
            //alert(kony.i18n.getLocalizedString(resulttable["errCode"])); //commented due to DEF896 
            alert(kony.i18n.getLocalizedString("keyOpenActGenErr"));
            return false;
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            return false;
        }
    } else {
        alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
    }
}

function showAcntSummary() {
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    showAccuntSummaryScreen();
    gblActivationFlow = false;
}
/*****************************************************************
 *     Module  : Touch ID
 *     Purpose : Function on clcik of cancel button of transaction password popup
 *     Params  : Password
 ******************************************************************/
function onCancelOfTouchFrm() {
    callCustomerAccountService("");
    gblActivationFlow = false;
}

function showFPSettingForm() {
    displayAnnoucementtoUser = false;
    frmFPSetting.btnReturnHome.setVisibility(false);
    frmFPSetting.show();
}

function showTouchIdSettingForm() {
    displayAnnoucementtoUser = false;
    frmTouchIdSettings.btnTouchID.setVisibility(false);
    frmTouchIdSettings.show();
}

function preShowTouchIdSetting() {
    changeStatusBarColor();
    if (!(LocaleController.isFormUpdatedWithLocale(frmTouchIdSettings.id))) {
        frmTouchIdSettings.lblHdrTxt.text = kony.i18n.getLocalizedString("keyTouchIDMenu");
        frmTouchIdSettings.lblTouchTitle.text = kony.i18n.getLocalizedString("keyLoginUsingTouchId");
        frmTouchIdSettings.lblTouchInfo.text = kony.i18n.getLocalizedString("keyTouchInfo");
        frmTouchIdSettings.lblTouchInfo1.text = kony.i18n.getLocalizedString("keyTouchInfoone");
        frmTouchIdSettings.btnTouchID.text = kony.i18n.getLocalizedString("actSumry")
            //if(gblTouchStatus == "N")
        frmTouchIdSettings.lblChkBox.text = kony.i18n.getLocalizedString("keyEnableTouchId");
        //else
        //	frmTouchIdSettings.lblChkBox.text = kony.i18n.getLocalizedString("keyDisableTouchId");
    }
}

function preShowfrmTouchIdIntermediateLogin() {
    changeStatusBarColor();
    //imageSize();
    kony.print("FPRINT: IN preShowfrmTouchIdIntermediateLogin");
    var locale = kony.i18n.getCurrentLocale();
    if (gblDeviceInfo["name"] == "iPhone") {
        kony.print("FPRINT: DISPLAYING IMAGES FOR IPHONE");
        if (gblDeviceInfo["model"] == "iPhone 6 Plus") {
            //alert("referenceHeight--->"+frmTouchIdIntermediateLogin.imgTouch.referenceHeight+" referenceWidth--->"+frmTouchIdIntermediateLogin.imgTouch.referenceWidth);
            frmTouchIdIntermediateLogin.imgTouch.referenceHeight = 620; //1026
            frmTouchIdIntermediateLogin.imgTouch.referenceWidth = 385;
        } else if (gblDeviceInfo["model"] == "iPhone 6") {
            //alert("referenceHeight--new->"+frmTouchIdIntermediateLogin.imgTouch.referenceHeight+" referenceWidth--->"+frmTouchIdIntermediateLogin.imgTouch.referenceWidth);
            frmTouchIdIntermediateLogin.imgTouch.referenceHeight = 560; //1026
            frmTouchIdIntermediateLogin.imgTouch.referenceWidth = 350;
        } else if (gblDeviceInfo["model"] == "iPhone 5S") {
            //alert("referenceHeight--->"+frmTouchIdIntermediateLogin.imgTouch.referenceHeight+" referenceWidth--->"+frmTouchIdIntermediateLogin.imgTouch.referenceWidth);
            frmTouchIdIntermediateLogin.imgTouch.referenceHeight = 467; //1026
            frmTouchIdIntermediateLogin.imgTouch.referenceWidth = 300;
        }
        //hbx6above
        if (locale == "en_US") {
            // imageSize();
            frmTouchIdIntermediateLogin.imgTouch.src = "aw_main_screen_eng.png";
        } else {
            // imageSize();
            frmTouchIdIntermediateLogin.imgTouch.src = "aw_main_screen_th.png";
        }
    } else if (gblDeviceInfo["name"] == "android") {
        /* kony.print("FPRINT: DISPLAYING IMAGES FOR ANDROID");
		 if(locale == "en_US"){
		   // imageSize();
		   frmTouchIdIntermediateLogin.imgTouch.src = "welcome_en.png";
		 }else{
		  // imageSize();
		   frmTouchIdIntermediateLogin.imgTouch.src = "welcome_th.png";
		 }  	 */
    }
    if (!(LocaleController.isFormUpdatedWithLocale(frmTouchIdIntermediateLogin.id))) {
        frmTouchIdIntermediateLogin.btnContinue.text = kony.i18n.getLocalizedString("keyNotNow");
        frmTouchIdIntermediateLogin.btnNotNow.text = kony.i18n.getLocalizedString("keyCancelButton");
    }
}

function setTouchPwdFailedError(errMsg) {
    dismissLoadingScreen();
    if (errMsg != null && errMsg != "") {
        popUpTouchIdTransPwd.lblTouchPopupErrMsg.text = errMsg;
        popUpTouchIdTransPwd.lblTouchPopupErrMsg.skin = "lblPopUpErr";
    } else {
        popUpTouchIdTransPwd.lblTouchPopupErrMsg.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popUpTouchIdTransPwd.lblTouchPopupErrMsg.skin = "lblPopupLabelTxt";
    }
    //popUpTouchIdTransPwd.lblTouchPopupErrMsg.skin = lblRedBold;
    popUpTouchIdTransPwd.tbxPopTouchPwd.skin = txtErrorBG;
    popUpTouchIdTransPwd.tbxPopTouchPwd.focusSkin = txtErrorBG;
    popUpTouchIdTransPwd.tbxPopTouchPwd.text = "";
}

function setTouchPwdNormal() {
    // below commented two lines are moved to on click of ON and OFF  
    //popUpTouchIdTransPwd.lblTouchPopupErrMsg.text = "";
    //popUpTouchIdTransPwd.lblTouchPopupErrMsg.setVisibility(false);
    popUpTouchIdTransPwd.tbxPopTouchPwd.skin = tbxPopupBlue;
    popUpTouchIdTransPwd.tbxPopTouchPwd.focusSkin = tbxPopupBlue;
    //popUpTouchIdTransPwd.tbxPopTouchPwd.text = "";
}