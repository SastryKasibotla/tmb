function verifyTransferMB() {
    var frmAcct;
    var fromAcctID;
    var inputParam = {};
    var fromData;
    showLoadingScreen();
    if (gblCRMProfileData[0]["mbFlowStatusIdRs"] == "03") {
        popTransferConfirmOTPLock.show();
        dismissLoadingScreen()
        return false;
    }
    var frequency = frmTransferConfirm.lblRepeatValue.text;
    var toAcctid = frmTransferConfirm.lblHiddenToAccount.text;
    var transferAmtTmp = frmTransferLanding.txtTranLandAmt.text;
    transferAmtTmp = kony.string.replace(transferAmtTmp, kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    var transferAmt = parseFloat(removeCommos(transferAmtTmp)).toFixed(2);
    var timesTransfer = frmTransferConfirm.lblHiddenVariableForScheduleEnd.text;
    fromData = frmTransferLanding.segTransFrm.data;
    fromAcctID = frmTransferLanding.segTransFrm.selectedItems[0].lblActNoval;
    frmAcct = fromAcctID.replace(/-/g, "");
    var toAcct = toAcctid.replace(/-/g, "");
    var date = frmTransferConfirm.lblTransCnfmDateVal.text;
    date = date.substr(0, 10);
    //for date format should be dd/mm/yy DEF7481 
    var currentDate = date.toString();
    var dateTD = currentDate.substr(0, 2);
    var monthTD = currentDate.substr(3, 2);
    var yearTD = currentDate.substr(8, 2);
    date = dateTD + "/" + monthTD + "/" + yearTD;
    if (!gblPaynow) {
        var splitResult = [];
        inputParam["transferOrderDate"] = frmTransferConfirm.lblTransCnfmDateVal.text;
        //inputParam["recurring"] = frmTransferConfirm.lblRepeatValue.text;
        inputParam["endDate"] = frmTransferConfirm.lblEndOnValue.text;
        // inputParam["source"] = "EditFutureTransfer"
        //inputParam["recipientMobile"] =  "xxx-xxx-"+frmTransferLanding.txtTransLndSms.text.substring(6, 10)
        //if split transfer  
        //if(frmTransferConfirm.segTransCnfmSplit.isVisible == true){
        if (frmTransferConfirm.hbxAmountFeeSplit.isVisible == true) {
            var segData = frmTransferConfirm.segTransCnfmSplit.data;
            for (i = 0; i < segData.length; i++) {
                var splitAmtFeeVal = kony.string.split(segData[i]["lblTransCnfmSplitAmtVal"], "(");
                var splitAmtVal = kony.string.replace(splitAmtFeeVal[1], kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                var splitFeeVal = kony.string.replace(splitAmtFeeVal[2], kony.i18n.getLocalizedString("currencyThaiBaht") + ")", "");
                //var splitAmtVal = kony.string.replace(segData[i]["lblTransCnfmSplitAmtVal"], kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                //var splitFeeVal = kony.string.replace(segData[i]["lblTransCnfmSplitFeeVal"], kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                splitAmtVal = splitAmtVal.replace(/,/g, "");
                splitFeeVal = splitFeeVal.replace(/,/g, "");
                var tempRec = {
                    "amount": commaFormatted(splitAmtVal) + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    "fee": commaFormatted(splitFeeVal) + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    "transRefId": segData[i]["lblTransCnfmSplitRefNoVal"]
                }
                splitResult.push(tempRec);
            }
            var feeVal = removeSquareBrackets(frmTransferConfirm.lblTransCnfmTotFeeVal.text);
            inputParam["totalAmount"] = commaFormatted(parseFloat(removeCommos(feeVal)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
            inputParam["totalFee"] = commaFormatted(parseFloat(removeCommos(feeVal)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
            inputParam["splitResult"] = JSON.stringify(splitResult);
        }
        //if normal transfer
        else {
            var feeVal = removeSquareBrackets(frmTransferConfirm.lblTransCnfmTotFeeVal.text);
            var tempRec = {
                "amount": commaFormatted(parseFloat(removeCommos(frmTransferConfirm.lblTransCnfmTotVal.text)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht"),
                "fee": commaFormatted(parseFloat(removeCommos(feeVal)).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht"),
                "transRefId": frmTransferConfirm.lblTransCnfmRefNumVal.text
            }
            splitResult.push(tempRec);
            inputParam["splitResult"] = JSON.stringify(splitResult);
        }
        var transferSchedule = "";
        if (frmTransferConfirm.lblRepeatValue.text == "Once") {
            transferSchedule = frmTransferConfirm.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmTransferConfirm.lblRepeatValue.text;
        } else if (frmTransferConfirm.lblExecuteValue.text == "-") {
            transferSchedule = frmTransferConfirm.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmTransferConfirm.lblRepeatValue.text;
        } else {
            transferSchedule = frmTransferConfirm.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmTransferConfirm.lblEndOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmTransferConfirm.lblRepeatValue.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmTransferConfirm.lblExecuteValue.text;
        }
        inputParam["PaymentSchedule"] = transferSchedule;
    }
    var businessDate = frmTransferConfirm.lblTransCnfmDateVal.text;
    businessDate = kony.string.replace(businessDate, "/", "");
    businessDate = businessDate.substring(0, 8);
    inputParam["loginModuleId"] = "MB_TxPwd";
    inputParam["password"] = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    gblBalAfterXfer = gblBalAfterXfer.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    gblBalAfterXfer = kony.string.replace(gblBalAfterXfer, "?", "");
    gblBalAfterXfer = kony.string.replace(gblBalAfterXfer, ",", "");
    var toNickname = frmTransferConfirm.lblTransCnfmToBankName.text;
    if (gblSelTransferMode == 2) { // Always send Mobile Number
        toNickname = kony.i18n.getLocalizedString("MIB_P2PMob");
    } else if (gblSelTransferMode == 3) {
        toNickname = kony.i18n.getLocalizedString("MIB_P2PCiti");
    } else {
        if (isNotBlank(toNickname)) {
            if (toNickname.length > 20) {
                toNickname = toNickname.substr(0, 20);
            }
        }
    }
    if (gblSelTransferMode == 3 || gblSelTransferMode == 2) {
        inputParam["merchantNo"] = removeHyphenIB(frmTransferConfirm.lblTransCnfmToNum.text);
    }
    inputParam["fromAcctNo"] = frmAcct; //k			
    inputParam["toAcctNo"] = toAcct; //kk
    inputParam["dueDateForEmail"] = frmTransferConfirm.lblStartOnValue.text;
    inputParam["memo"] = frmTransferLanding.txtTranLandMyNote.text; //k
    inputParam["transferAmt"] = transferAmt; //k
    inputParam["toAcctBank"] = gblBANKREF
        //if(gblTransSMART == 1 || gblTrasORFT == 1 || (Recp_category == 1 && !gblPaynow)){
    if (gblTransEmail == 1) {
        inputParam["notificationType"] = "1";
        inputParam["recipientEmailAddr"] = frmTransferLanding.txtTransLndSmsNEmail.text;
        inputParam["RemailId"] = frmTransferLanding.txtTransLndSmsNEmail.text;
        inputParam["receipientNote"] = frmTransferLanding.textRecNoteEmail.text;
    } else if (gblTrasSMS == 1) {
        inputParam["notificationType"] = "0";
        inputParam["recipientMobileNbr"] = removeHyphenIB(frmTransferLanding.txtTransLndSms.text);
        inputParam["phoneNo"] = "xxx-xxx-" + removeHyphenIB(frmTransferLanding.txtTransLndSms.text).substring(6, 10);
        inputParam["recipientMobile"] = removeHyphenIB(frmTransferLanding.txtTransLndSms.text);
        inputParam["receipientNote"] = frmTransferLanding.txtTranLandRecNote.text;
    }
    //	}
    if (!gblPaynow) {
        inputParam["dueDate"] = changeDateFormatForService(frmTransferConfirm.lblStartOnValue.text); //k
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
            inputParam["freq"] = "Daily";
            inputParam["recurring"] = "keyDaily";
        } else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
            inputParam["freq"] = "Weekly";
            inputParam["recurring"] = "keyWeekly";
        } else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
            inputParam["freq"] = "Monthly";
            inputParam["recurring"] = "keyMonthly";
        } else if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
            inputParam["freq"] = "Annually";
            inputParam["recurring"] = "keyYearly";
        } else {
            inputParam["freq"] = "once";
            inputParam["recurring"] = "keyOnce";
        }
        inputParam["locale"] = kony.i18n.getCurrentLocale();
        inputParam["RecipentName"] = gblSelectedRecipentName; //kk
        if (timesTransfer == "1") inputParam["NumInsts"] = frmTransferConfirm.lblExecuteValue.text.split(" ")[0].trim(); //k
        else if (timesTransfer == "2") inputParam["FinalDueDt"] = changeDateFormatForService(frmTransferConfirm.lblEndOnValue.text); //k
        else if (timesTransfer == "0") {
            // do something?
        }
        var timesNumber = "";
        if (timesTransfer == "3") {
            timesNumber = "2";
            inputParam["FinalDueDt"] = changeDateFormatForService(frmTransferConfirm.lblEndOnValue.text); //k
        } else if (timesTransfer == "2") {
            timesNumber = "1";
            inputParam["NumInsts"] = frmTransferConfirm.lblExecuteValue.text.split(" ")[0].trim();
        } else if (timesTransfer == "1") {
            timesNumber = "0";
        }
        //inputParam["times"] = timesTransfer;//k
        inputParam["times"] = timesNumber; //k
    } else if (gblTransSMART == 1) {
        //ENH_129 SMART Transfer Add Date & Time
        inputParam["dueDate"] = frmTransferConfirm.lblSmartDateVal.text;
    }
    var fourthDigit;
    if (frmAcct.length == 14) {
        fourthDigit = frmAcct.charAt(7);
    } else {
        fourthDigit = frmAcct.charAt(3);
    }
    if (fourthDigit == "3") {
        var depositeNo = gblTDDeposit;
        inputParam["depositeNo"] = depositeNo;
    }
    inputParam["toFIIdent"] = gblisTMB; //orft 		
    //inputParam["acctTitle"] = frmTransferConfirm.lblTransCnfmToName.text //orft kk
    inputParam["transferDate"] = date;
    inputParam["finTxnMemo"] = frmTransferLanding.txtTranLandMyNote.text + ""; //frmIBTranferLP.txtArMn.text + ""; //financial		
    inputParam["frmAcctName"] = frmTransferConfirm.lblTransCnfmCustomerName.text + "";
    inputParam["fromAcctNickname"] = frmTransferConfirm.lblTransCnfmFrmName.text + "";
    inputParam["toAcctName"] = frmTransferConfirm.lblTransCnfmToAccountName.text + "";
    inputParam["toAcctNickname"] = toNickname; //financial	
    inputParam["toBankId"] = gblisTMB; //k
    inputParam["custName"] = gblCustomerName;
    inputParam["toAcctBank"] = gblBANKREF;
    inputParam["RecipentName"] = gblSelectedRecipentName;
    inputParam["Recp_category"] = Recp_category;
    inputParam["gblBalAfterXfer"] = gblBalAfterXfer; //financial		
    inputParam["gblPaynow"] = gblPaynow; //k
    inputParam["gblisTMB"] = gblisTMB;
    inputParam["gblTrasORFT"] = gblTrasORFT; //k
    inputParam["gblTransSMART"] = gblTransSMART; //k
    inputParam["gblBANKREF"] = gblBANKREF; //future
    inputParam["bankShortName"] = getBankShortName(gblisTMB);
    inputParam["gblTrasSMS"] = gblTrasSMS; //future
    inputParam["gblTransEmail"] = gblTransEmail; //future
    //MIB-2096 - Adding log for tracking one time transfer, Date: 23-Jun-16
    //if(frmTransferLanding.hbxRecipientNumberAndName.isVisible) {
    if (frmTransferLanding.tbxAccountNumber.isVisible) {
        inputParam["oneTimeTransfer"] = "AccountNumber";
    } else if (frmTransferLanding.hbxITMXBank.isVisible) {
        inputParam["oneTimeTransfer"] = "MobileNumber";
    }
    //inputParam["gblTokenSwitchFlag"] = "false"; //future
    invokeServiceSecureAsync("executeTransfer", inputParam, callbackVerifyTransferMB)
}

function callbackVerifyTransferMB(status, resultTable) {
    if (status == 400) {
        if (resultTable["opstatus"] == 0) {
            if (resultTable["StatusCode"] == 0 || (resultTable["Transfer"] != undefined && resultTable["Transfer"].length > 0)) {
                gblSplitTransfer = resultTable;
                var fee = 0;
                var transferResultList = resultTable["Transfer"];
                for (var i = 0; i < transferResultList.length; i++) {
                    if (transferResultList[i]["status"] == 0) {
                        var transferFee = transferResultList[i]["fee"] + "";
                        fee = fee + parseFloat(transferFee);
                    }
                }
                var enteredAmount = 0;
                frmTransfersAck.lblTransNPbAckDateVal.text = resultTable["serverDate"];
                resetHbxTransferCompDetails();
                displayRecipientBox();
                frmTransfersAck.lblTransNPbAckFrmName.text = frmTransferConfirm.lblTransCnfmFrmName.text;
                frmTransfersAck.lblTransNPbAckFrmNum.text = frmTransferConfirm.lblTransCnfmFrmNum.text;
                frmTransfersAck.lblTransNPbAckFrmCustomerName.text = frmTransferLanding.segTransFrm.selectedItems[0].custAcctName;
                if (gblTransSMART == 1) {
                    frmTransfersAck.lblSmartDateVal.skin = lblbluemedium142;
                } else {
                    if (gblPaynow) {
                        frmTransfersAck.lblSmartDateVal.text = frmTransfersAck.lblTransNPbAckDateVal.text;
                    }
                    frmTransfersAck.lblSmartDateVal.skin = lblGreyRegular142;
                }
                gblBalAfterXfer = resultTable["availBal"];
                if (gblBalAfterXfer == null || gblBalAfterXfer == undefined || gblBalAfterXfer == "") {
                    gblBalAfterXfer = frmTransferConfirm.lblTransCnfmBalBefAmt.text;
                } else {
                    gblBalAfterXfer = commaFormatted(gblBalAfterXfer) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                frmTransfersAck.lblTransNPbAckFrmBal.text = gblBalAfterXfer;
                frmTransfersAck.imgTransNPbAckFrm.src = frmTransferConfirm.imgTransCnfmFrm.src;
                frmTransfersAck.lblTransNPbAckToAccountName.text = "";
                frmTransfersAck.lblTransNPbAckToAccountName.text = frmTransferConfirm.lblTransCnfmToAccountName.text;
                frmTransfersAck.lblTransNPbAckToName.text = frmTransferConfirm.lblTransCnfmToBankName.text;
                if (gblSelTransferMode == 2) {
                    frmTransfersAck.lblTransNPbAckToNum.text = maskMobileNumber(frmTransferConfirm.lblTransCnfmToNum.text);
                } else if (gblSelTransferMode == 3) {
                    frmTransfersAck.lblTransNPbAckToNum.text = maskCitizenID(frmTransferConfirm.lblTransCnfmToNum.text);
                } else {
                    frmTransfersAck.lblTransNPbAckToNum.text = frmTransferConfirm.lblTransCnfmToNum.text;
                }
                frmTransfersAck.lblTransNPbAckTotVal.text = frmTransferConfirm.lblTransCnfmTotVal.text;
                //frmTransfersAck.lblTransNPbAckTotFeeVal.text = frmTransferConfirm.lblTransCnfmTotFeeVal.text;
                frmTransfersAck.lblTransNPbAckTotFeeVal.text = "(" + commaFormatted(fee + "") + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
                frmTransfersAck.lblTransNPbAckRefDes.text = frmTransferConfirm.lblTransCnfmRefNumVal.text;
                if (gblTDDateFlag) {
                    displayTDTransferCompDetails();
                    frmTransfersAck.lblTransNPbAckPrincipalAmtVal.text = frmTransferConfirm.lblPrincipalAmntVal.text;
                    frmTransfersAck.lblTransNPbAckPenaltyAmtVal.text = frmTransferConfirm.lblTransCnfmPenaltyAmtVal.text;
                    frmTransfersAck.lblTransNPbAckIntAmtVal.text = frmTransferConfirm.lblTransCnfmIntAmtVal.text;
                    frmTransfersAck.lblTransNPbAckTaxAmtVal.text = frmTransferConfirm.lblTransCnfmTaxAmtVal.text;
                    frmTransfersAck.lblTransNPbAckNetAmtVal.text = frmTransferConfirm.lblTransCnfmNetAmtVal.text;
                    frmTransfersAck.lblTransNPbAckRefDes2.text = frmTransferConfirm.lblTransCnfmRefNumVal2.text;
                    gblTDDateFlag = false;
                }
                //async call to send notify Sender
                //TranferNotificationADDSuccessMB();
                if (gblPaynow) {
                    if (frmTransferLanding.hboxTD.isVisible) {
                        displayTDTransferCompDetails();
                    } else {
                        displayNormalTransferComp(false); // isMega = false;
                        //frmTransfersAck.lblTransNPbAckTotVal2.text = frmTransferConfirm.lblTransCnfmTotVal2.text;
                        frmTransfersAck.lblTransNPbAckTotVal2.text = frmTransferConfirm.lblTransCnfmTotVal.text + " " + "(" + commaFormatted(fee + "") + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
                    }
                } else {
                    displayScheduleTransferComp(false);
                    frmTransfersAck.lblTransNPbAckTotVal2.text = frmTransferConfirm.lblTransCnfmTotVal2.text;
                    frmTransfersAck.lblStartOn.text = frmTransferConfirm.lblStartOn.text;
                    frmTransfersAck.lblStartOnValue.text = gblStartBPDate;
                    frmTransfersAck.lblEndOnValue.text = gblEndBPDate;
                    frmTransfersAck.lblRepeatValue.text = gblScheduleRepeatBP;
                    frmTransfersAck.lblExecuteValue.text = gblGiveToConfirmationLabel;
                    if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "none")) {
                        frmTransfersAck.lblEndOnValue.text = frmTransfersAck.lblStartOnValue.text;
                        frmTransfersAck.lblExecuteValue.text = "1 times";
                    }
                    if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")) {
                        frmTransfersAck.lblEndOnValue.text = "-";
                        frmTransfersAck.lblExecuteValue.text = "-";
                    }
                }
                popupTractPwd.dismiss();
                //code change for IOS9
                gblRetryCountRequestOTP = "0";
                enableSegmentTransferMB(resultTable);
                dismissLoadingScreen();
                if ((resultTable["repeatCallORFT"] != null && kony.string.equalsIgnoreCase(resultTable["repeatCallORFT"], "yes")) || resultTable["repeatCallTransferAdd"] != null && kony.string.equalsIgnoreCase(resultTable["repeatCallTransferAdd"], "yes")) {
                    popGeneralMsg.lblMsg.text = kony.i18n.getLocalizedString("TRErr_ORFTDuplicate");
                    popGeneralMsg.btnClose.text = kony.i18n.getLocalizedString("keyOK");
                    popGeneralMsg.btnClose.onClick = onDuplicateTransferErrPopUp;
                    popGeneralMsg.show();
                    return false;
                }
                frmTransfersAck.show();
            } else {
                popupTractPwd.dismiss();
                if (resultTable["errMsg"] != undefined) {
                    alert(resultTable["errMsg"]);
                } else {
                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                }
                dismissLoadingScreen();
            } //statuscode !=0
        } //opstatus
        else if (resultTable["opstatus"] == 1) {
            if (resultTable["ServerStatusCode"] == "XB240071" || resultTable["ServerStatusCode"] == "XB240072") {
                alert(kony.i18n.getLocalizedString("MIB_TRErrNoDest"));
            } else {
                if (resultTable["ServerStatusCode"] != undefined) {
                    alert(kony.i18n.getLocalizedString("ECGenericError") + " (" + resultTable["ServerStatusCode"] + ")");
                } else {
                    alert(kony.i18n.getLocalizedString("ECGenericError"));
                }
            }
            dismissLoadingScreen();
        } //opstatus
        else {
            if (resultTable["errCode"] == "VrfyTxPWDErr00003") {
                showTranPwdLockedPopup();
            } else setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"))
        }
        kony.application.dismissLoadingScreen();
        //InvalidPasswordFlow(resultTable)
    } //status 400
}

function displayRecipientBox() {
    //if(frmTransferLanding.hbxRecipientNumberAndName.isVisible && 
    //	gblSelTransferMode == 1){
    if (frmTransferLanding.tbxAccountNumber.isVisible && gblSelTransferMode == 1) {
        frmTransfersAck.hbxImageAddRecipient.setVisibility(true);
    } else {
        frmTransfersAck.hbxImageAddRecipient.setVisibility(false);
    }
}

function enableSegmentTransferMB(resultTable) {
    var transferAckListTbl = resultTable["Transfer"];
    var gblRemainingFreeTransactionCount = 0;
    gblTransFail = true;
    var allSuccess = "0";
    for (var i = 0; i < transferAckListTbl.length; i++) {
        if (transferAckListTbl[i]["image"] == "wrong.png") {
            frmTransfersAck.imgTransNBpAckComplete.src = "iconnotcomplete.png";
            //frmTransfersAck.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
            //frmTransfersAck.hbxAdv.setVisibility(false);
        } else {
            //frmTransfersAck.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete")
            frmTransfersAck.imgTransNBpAckComplete.src = "completeico.png";
            break;
        }
    }
    frmTransfersAck.segTransAckSplit.removeAll();
    //Chaek all Transfer transaction to control share button
    for (var i = 0; i < transferAckListTbl.length; i++) {
        if (transferAckListTbl[i]["image"] == "correct.png") {
            gblTransFail = false;
            break;
        }
    }
    if (gblTransFail) { // need to show 0;
        frmTransfersAck.lblTransNPbAckTotVal.text = commaFormatted("0") + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmTransfersAck.lblTransNPbAckTotFeeVal.text = "(" + commaFormatted("0") + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")"
    }
    if (transferAckListTbl.length > 1) {
        resetHbxTransferCompDetails();
        if (gblPaynow) {
            displayNormalTransferComp(true);
        } else {
            displayScheduleTransferComp(true);
        }
        //frmTransfersAck.lblTransNPbAckTotVal3.text = frmTransferConfirm.lblTransCnfmTotVal3.text;		
        frmTransfersAck.segTransAckSplit.widgetDataMap = {
            lblSegTrans: "lblSegTrans",
            lblSegAmount: "lblSegAmount",
            lblSegFee: "lblSegFee",
            lblSegTransRefNo: "lblSegTransRefNo",
            lblsegAmountDes: "lblsegAmountDes",
            lblsegFeeDes: "lblsegFeeDes",
            imgMegaTransStatus: "imgMegaTransStatus"
        }
        var tempData = [];
        var transferNoToDiaplay = "";
        var image = "";
        var totalAmountSuccess = "0";
        var totalFeeSuccess = "0";
        for (var i = 0; i < transferAckListTbl.length; i++) {
            transferNoToDiaplay = transferAckListTbl[i]["refId"];
            if (transferAckListTbl[i]["status"] == 0) {
                image = "completeico_sm.png";
                totalAmountSuccess = parseFloat(totalAmountSuccess) + parseFloat(transferAckListTbl[i]["amount"]);
                totalFeeSuccess = parseFloat(totalFeeSuccess) + parseFloat(transferAckListTbl[i]["fee"]);
                gblRemainingFreeTransactionCount++;
            } else {
                image = "failico_sm.png";
                allSuccess = "1";
            }
            var splitAmntFee = commaFormatted(transferAckListTbl[i]["amount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + " (" + commaFormatted(transferAckListTbl[i]["fee"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
            var temp = {
                lblSegTrans: kony.i18n.getLocalizedString("keyTransactionRefNo"),
                lblSegAmount: kony.i18n.getLocalizedString("amount"),
                lblSegFee: kony.i18n.getLocalizedString("keyXferFee"),
                lblSegTransRefNo: transferNoToDiaplay,
                lblsegAmountDes: splitAmntFee,
                lblsegFeeDes: commaFormatted(transferAckListTbl[i]["fee"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                imgMegaTransStatus: image
            }
            tempData.push(temp);
            frmTransfersAck.lblTransNPbAckTotVal.text = commaFormatted(totalAmountSuccess + "") + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmTransfersAck.lblTransNPbAckTotFeeVal.text = "(" + commaFormatted(totalFeeSuccess + "") + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")"
        }
        frmTransfersAck.lblTransNPbAckTotVal3.text = frmTransferConfirm.lblTransCnfmTotVal.text + " " + "(" + commaFormatted(totalFeeSuccess + "") + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
        frmTransfersAck.segTransAckSplit.data = tempData;
        frmTransfersAck.segTransAckSplit.setVisibility(true);
        frmTransfersAck.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
        frmTransfersAck.imgMore.src = "arrow_up.png";
    } else {
        //frmTransfersAck.lblTransNPbAckTotAmt.text = kony.i18n.getLocalizedString("keyAmount")
        //frmTransfersAck.lblTransNPbAckTotFee.text = kony.i18n.getLocalizedString("keyXferFee")
        frmTransfersAck.segTransAckSplit.setVisibility(false);
        //frmTransfersAck.hbxTransNPbAckRef.setVisibility(true);
        frmTransfersAck.lblTransNPbAckRefDes.text = frmTransferConfirm.lblTransCnfmRefNumVal.text;
        if (gblisTMB == gblTMBBankCD) { //frmIBTransferNowCompletion.lblremFreeTranVAlue.text
            gblRemainingFreeTransactionCount = resultTable["remainingFree"];
        } else if (transferAckListTbl[0]["status"] == 0) {
            gblRemainingFreeTransactionCount++;
        }
    }
    frmTransfersAck.lblFreeTransValue.text = "";
    frmTransfersAck.lblAllSuccess.text = allSuccess;
    if (gblTransFail && gblisTMB == gblTMBBankCD) {
        frmTransfersAck.lblFreeTransValue.text = frmTransferConfirm.lblFreeTransValue.text;
    } else if (gblisTMB == gblTMBBankCD && resultTable["remainingFree"] != null && resultTable["remainingFree"] != undefined) {
        frmTransfersAck.lblFreeTransValue.text = resultTable["remainingFree"];
    } else if (gblTrasORFT && resultTable["RemainingFeeORFT"] != undefined && resultTable["RemainingFeeORFT"] != null) {
        frmTransfersAck.lblFreeTransValue.text = resultTable["RemainingFeeORFT"];
    } else if (resultTable["RemainingFeeSmart"] != undefined && resultTable["RemainingFeeSmart"] != null) frmTransfersAck.lblFreeTransValue.text = resultTable["RemainingFeeSmart"];
}
/**************************************************************************************
		Module	: callBackGetAccountListInquiry
		Author  : Kony
		Date    : May 05, 2013
		Purpose : Fetches  From Accounts List from AccountListInquiry service
****************************************************************************************/
function callBackTransferFromAccounts(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            loadBankListForTransfers(resulttable["BankList"]); // transfer REdesign calling bank list in biggining and string in global variable
            var setyourIDTransfer = resulttable["setyourIDTransfer"];
            ITMX_TRANSFER_ENABLE = resulttable["ITMX_TRANSFER_ENABLE"];
            ITMX_TRANSFER_FEE_LIMITS = resulttable["ITMX_TRANSFER_FEE_LIMITS"];
            deviceContactRefreshServerValue = resulttable["DEVICE_CONTACT_REFRESH_VALUE"];
            var fromData = []
            var j = 1
            gbltranFromSelIndex = [0, 0];
            /*** below are configurable params driving from Backend and get Cached. **/
            gblMaxTransferORFT = resulttable.ORFTTransLimit;
            gblMaxTransferSMART = resulttable.SMARTTransLimit;
            gblLimitORFTPerTransaction = resulttable.ORFTTransSplitAmnt;
            gblLimitSMARTPerTransaction = resulttable.SMARTTransAmnt;
            var ORFTRange1Lower = resulttable.ORFTRange1Lower;
            var ORFTRange1Higher = resulttable.ORFTRange1Higher;
            var SMARTRange1Higher = resulttable.SMARTRange1Higher;
            var SMARTRange1Lower = resulttable.SMARTRange1Lower;
            var ORFTRange2Lower = resulttable.ORFTRange2Lower;
            var ORFTRange2Higher = resulttable.ORFTRange2Higher;
            var SMARTRange2Higher = resulttable.SMARTRange2Higher;
            var SMARTRange2Lower = resulttable.SMARTRange2Lower;
            var ORFTSPlitFeeAmnt1 = resulttable.ORFTSPlitFeeAmnt1;
            var ORFTSPlitFeeAmnt2 = resulttable.ORFTSPlitFeeAmnt2;
            var SMARTSPlitFeeAmnt1 = resulttable.SMARTSPlitFeeAmnt1;
            var SMARTSPlitFeeAmnt2 = resulttable.SMARTSPlitFeeAmnt2;
            var WithinBankOwnAccLimit = resulttable.WithinBankOwnAccLimit;
            var WithinBankOwnAccLimitTransaction = resulttable.WithinBankOwnAccLimitTransaction;
            GLOBAL_TODAY_DATE = resulttable["TODAY_DATE"];
            var date = new Date(GLOBAL_TODAY_DATE);
            var curMnth = date.getMonth() + 1;
            var curDate = date.getDate();
            if ((curMnth.toString().length) == 1) {
                curMnth = "0" + curMnth;
            }
            if ((curDate.toString().length) == 1) {
                curDate = "0" + curDate;
            }
            var datetime = "" + curDate + "/" + curMnth + "/" + date.getFullYear();
            //frmTransferLanding.lblSchedSel.text=datetime;
            if (gblTransferFromRecipient) {
                gblSelTransferMode = 1;
                ResetTransferHomePage();
                onClickTransferFromRecipientsMB();
            }
            gblXerSplitData = [];
            var temp1 = [];
            var temp = {
                ORFTRange1Lower: resulttable.ORFTRange1Lower,
                ORFTRange1Higher: resulttable.ORFTRange1Higher,
                SMARTRange1Higher: resulttable.SMARTRange1Higher,
                SMARTRange1Lower: resulttable.SMARTRange1Lower,
                ORFTRange2Lower: resulttable.ORFTRange2Lower,
                ORFTRange2Higher: resulttable.ORFTRange2Higher,
                SMARTRange2Higher: resulttable.SMARTRange2Higher,
                SMARTRange2Lower: resulttable.SMARTRange2Lower,
                ORFTSPlitFeeAmnt1: resulttable.ORFTSPlitFeeAmnt1,
                ORFTSPlitFeeAmnt2: resulttable.ORFTSPlitFeeAmnt2,
                SMARTSPlitFeeAmnt1: resulttable.SMARTSPlitFeeAmnt1,
                SMARTSPlitFeeAmnt2: resulttable.SMARTSPlitFeeAmnt2,
                WithinBankOwnAccLimit: resulttable.WithinBankOwnAccLimit,
                WithinBankOwnAccLimitTransaction: resulttable.WithinBankOwnAccLimitTransaction
            }
            kony.table.insert(temp1, temp)
            gblXerSplitData = temp1;
            /*** till hereee  ***/
            gblTransfersOtherAccounts = [];
            var nonCASAAct = 0;
            for (var i = 0; i < resulttable.custAcctRec.length; i++) {
                var accountStatus = resulttable["custAcctRec"][i].acctStatus;
                if (accountStatus.indexOf("Active") == -1) {
                    nonCASAAct = nonCASAAct + 1;
                }
                var joinType = resulttable.custAcctRec[i].partyAcctRelDesc;
                var AccStatCde = resulttable.custAcctRec[i].personalisedAcctStatusCode;
                var anyIDEligible = true;
                // if p2 p show only anyID eligible
                if (gblSelTransferMode == 2 || gblSelTransferMode == 3) {
                    anyIDEligible = resulttable.custAcctRec[i].anyIdAllowed == "Y";
                }
                if (accountStatus.indexOf("Active") >= 0) {
                    if (AccStatCde == undefined || AccStatCde == "01" || AccStatCde == "") {
                        if (joinType == "PRIJNT" || joinType == "SECJAN" || joinType == "OTHJNT" || joinType == "SECJNT") {
                            var temp = [{
                                custName: resulttable.custAcctRec[i].accountName,
                                acctNo: addHyphenMB(resulttable.custAcctRec[i].accId),
                                nickName: resulttable.custAcctRec[i].acctNickName,
                                productName: resulttable.custAcctRec[i].productNmeEN,
                                productNameTH: resulttable.custAcctRec[i].productNmeTH,
                                bankCD: resulttable.custAcctRec[i].bankCD,
                                productID: resulttable.custAcctRec[i].productID
                            }];
                            kony.table.insert(gblTransfersOtherAccounts, temp[0]);
                        } else {
                            var icon = "";
                            var iconcategory = "";
                            icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + "NEW_" + resulttable.custAcctRec[i]["ICON_ID"] + "&modIdentifier=PRODICON";
                            iconcategory = resulttable.custAcctRec[i]["ICON_ID"];
                            // added new From account widget
                            if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
                                var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew1, icon);
                            } else if (iconcategory == "ICON-03") {
                                var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew2, icon);
                            } else if (iconcategory == "ICON-04") {
                                var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew3, icon);
                            }
                            if (anyIDEligible) kony.table.insert(fromData, temp[0]);
                        }
                    } // if end
                } //for
            }
            // for other accounts
            if (fromData.length == 0 && (gblSelTransferMode == 2 || gblSelTransferMode == 3)) {
                ResetTransferHomePage();
                dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("TRErr_NoFromAcc"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            if (nonCASAAct == resulttable.custAcctRec.length) {
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
                return false;
            }
            if (fromData.length == 0) {
                //alert(kony.i18n.getLocalizedString("noEligibleFromAccount"));
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
                return false;
                /*ResetTransferHomePage();
                recipientAddFromTransfer=false;
                gblTransferFromRecipient=false;
                callBackTransferFromAccounts(status, resulttable);
                return false;*/
            }
            for (var i = 0; i < resulttable.OtherAccounts.length; i++) {
                var prdCode = resulttable.OtherAccounts[i].productID;
                if (prdCode == 290 || prdCode == "otherbank") {
                    var accountId = kony.string.replace(resulttable.OtherAccounts[i].accId, "-", "");
                    if (accountId.length == 14) {
                        resulttable.OtherAccounts[i].accId = accountId.substring(4, 14);
                    }
                    var productName = "";
                    var nickNme = "";
                    if (resulttable.OtherAccounts[i].acctNickName == undefined) {
                        var locale = kony.i18n.getCurrentLocale();
                        if (locale == "en_US") nickNme = resulttable.OtherAccounts[i].productNmeEN
                        else nickNme = resulttable.OtherAccounts[i].productNmeTH
                    } else {
                        nickNme = resulttable.OtherAccounts[i].acctNickName
                    }
                    var temp = [{
                        custName: resulttable.OtherAccounts[i].accountName,
                        acctNo: addHyphenMB(resulttable.OtherAccounts[i].accId),
                        nickName: nickNme,
                        productName: resulttable.OtherAccounts[i].productNmeEN,
                        productID: resulttable.OtherAccounts[i].productID,
                        bankCD: resulttable.OtherAccounts[i].bankCD
                    }]
                    kony.table.insert(gblTransfersOtherAccounts, temp[0])
                }
            }
            if (isNotBlank(gblSelTransferFromAcctNo)) {
                getSelectedIdxTransferFromAcct(fromData);
            }
            frmTransferLanding.segTransFrm.widgetDataMap = {
                lblACno: "lblACno",
                lblAcntType: "lblAcntType",
                img1: "img1",
                lblCustName: "lblCustName",
                lblBalance: "lblBalance",
                lblActNoval: "lblActNoval",
                lblDummy: "lblDummy",
                lblSliderAccN1: "lblSliderAccN1",
                lblSliderAccN2: "lblSliderAccN2",
                lblRemainFeeValue: "lblRemainFeeValue",
                lblRemainFee: "lblRemainFee"
            }
            frmTransferLanding.segTransFrm.data = fromData;
            frmTransferLanding.segTransFrm.selectedIndex = gbltranFromSelIndex;
            /*** decide whether the current selected from is TD accnt */
            dismissLoadingScreen();
            gbltdFlag = fromData[0].tdFlag;
            toggleTDMaturityCombobox(gbltdFlag);
        } else if (resulttable["opstatus"] == 1) {
            showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
            return false;
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            }
        }
    }
    frmTransferLanding.show();
    //Auto Populate TMB if from Account is only eligible to transfer to TMB
    fromAccountIsOnlyAllowedForTMB();
    resetTransferLandingBasedOnToAccount();
    if (gblTransferFromRecipient || recipientAddFromTransfer) {
        assignBankSelectDetails(gblisTMB);
        if (frmTransferLanding.lblTranLandToAccountNumber.text != "") {
            frmTransferLanding.tbxAccountNumber.text = frmTransferLanding.lblTranLandToAccountNumber.text;
        }
        //frmTransferLanding.lineRecipientDetails.skin = lineBlue;
        frmTransferLanding.txtTranLandAmt.setFocus(true);
    } else {
        //frmTransferLanding.tbxAccountNumber.setFocus(true);
        if (gblSelTransferMode == 2) {
            frmTransferLanding.txtOnUsMobileNo.setFocus(true);
        }
        if (gblSelTransferMode == 3) {
            frmTransferLanding.txtCitizenID.setFocus(true);
        }
    }
}

function getSelectedIdxTransferFromAcct(fromData) {
    for (var i = 0; i < fromData.length; i++) {
        if (kony.string.equals(gblSelTransferFromAcctNo, fromData[i].accountNum)) {
            gbltranFromSelIndex = [0, i];
            break;
        }
    }
}
/**************************************************************************************
		Module	: createSegmentRecord
		Author  : Kony
		Date    : May 22, 2013
		Purpose : utility function for reuse
*****************************************************************************************/
function createSegmentRecord(record, hbxSliderindex, icon) {
    var accId = ""
    var flag = false;
    var frmid = ""
    var ramainfee = ""
    var ramainfeeValue = ""
    var productName = ""
    var locale = kony.i18n.getCurrentLocale();
    var accountName = record.acctNickName
        //code changes for to show only 10 digits insted of 14 digits from account id
    if (record.accId.length == 14) accId = record.accId;
    else accId = "0000" + record.accId;
    if (accId.length == 14) {
        if (accId.charAt(0) == 0 && accId.charAt(1) == 0 && accId.charAt(2) == 0 && accId.charAt(3) == 0) {
            flag = true;
        }
        if (flag) {
            accId = accId.substr(4, 10)
        }
    }
    if (gblSMART_FREE_TRANS_CODES.indexOf(record.productID) >= 0) {
        ramainfee = kony.i18n.getLocalizedString("keylblNoOfFreeTrans");
        ramainfeeValue = parseFloat(record.remainingFee) < 0 ? 0 : record.remainingFee;
    } else {
        ramainfee = " ";
        ramainfeeValue = " ";
    }
    if (accountName == "" || accountName == undefined) {
        var acctnum = record.accId
        if (record.accType == kony.i18n.getLocalizedString("Saving") || record.accType == kony.i18n.getLocalizedString("termDeposit")) {
            acctnum = acctnum.substr(10, 4)
            accountName = record.productNmeEN + " " + acctnum
        } else {
            acctnum = acctnum.substr(6, 4)
            accountName = record.productNmeEN + " " + acctnum
        }
    }
    if (locale == "en_US") productName = record.productNmeEN
    else productName = record.productNmeTH
    var temp = [{
        lblACno: kony.i18n.getLocalizedString("AccountNo"),
        img1: icon,
        lblCustName: accountName,
        lblBalance: commaFormatted(record.availableBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        lblActNoval: addHyphenMB(accId),
        lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
        lblSliderAccN2: productName,
        lblDummy: " ",
        tdFlag: record.accType,
        fromFIIdent: record.fiident,
        remainingFee: record.remainingFee,
        lblRemainFee: ramainfee,
        lblRemainFeeValue: ramainfeeValue,
        prodCode: record.productID,
        nickName: record.acctNickName,
        accountNum: addHyphenMB(accId),
        template: hbxSliderindex,
        custAcctName: record.accountName,
        nicknameEn: record.acctNickNameEn,
        nicknameTh: record.acctNickNameTh,
        isOtherBankAllowed: record.isOtherBankAllowed,
        isOtherTMBAllowed: record.isOtherTMBAllowed,
        isAllowedSA: record.allowedSA,
        isAllowedCA: record.allowedCA,
        isAllowedTD: record.allowedTD
    }]
    return temp;
}
/**************************************************************************************
		Module	: addHyphenMB
		Author  : Kony
		Date    : May 22, 2013
		Purpose : adding hyphen to FROM account number
*****************************************************************************************/
function addHyphenMB(accno) {
    if (accno != "") {
        var hyphenText = "";
        for (i = 0; i < accno.length; i++) {
            hyphenText += accno[i];
            if (i == 2 || i == 3 || i == 8) {
                hyphenText += '-';
            }
        }
    }
    return hyphenText;
}
/**************************************************************************************
		Module	: callBackTDAccount
		Author  : Kony
		Date    : May 05, 2013
		Purpose : Fetches  TD Account details from tDDetailinq service
*****************************************************************************************/
function callBackTDAccount(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var tdData = [];
            for (var i = 0; i < resulttable.tdDetailsRec.length; i++) {
                var temp = {
                    lblData: kony.i18n.getLocalizedString("keyDate") + ":" + dateformat(resulttable.tdDetailsRec[i].maturityDate) + " " + kony.i18n.getLocalizedString("keyPrincipal") + commaFormatted(resulttable.tdDetailsRec[i].depositAmtVal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                    maturityDate: dateformat(resulttable.tdDetailsRec[i].maturityDate),
                    lblDepositeNo: "",
                    amount: commaFormatted(resulttable.tdDetailsRec[i].depositAmtVal),
                    depositeNo: resulttable.tdDetailsRec[i].depositNo,
                    imgTD: "bg_arrow_right_grayb.png"
                }
                kony.table.insert(tdData, temp)
            }
            tdData.sort(function(a, b) {
                var dateA = a.maturityDate;
                var dateB = b.maturityDate;
                return kony.os.compareDates(dateA, dateB, "dd/mm/yyyy") //default return value (no sorting)
            })
            popTransfersTDAccount.segTransfersTD.data = tdData;
            if (popTransfersTDAccount.segTransfersTD.data != null && popTransfersTDAccount.segTransfersTD.data.length == 1) {
                onSelectTDDetailsPOP(true);
            } else {
                dismissLoadingScreen();
            }
        } else {
            dismissLoadingScreen();
        }
    } else {
        dismissLoadingScreen();
    }
}

function dateformat(date) {
    var currentDate = date.toString();
    var yearTD = currentDate.substr(0, 4);
    var dateTD = currentDate.substr(8, 2);
    var monthTD = currentDate.substr(5, 2);
    var date1 = dateTD + "/" + monthTD + "/" + yearTD;
    return date1;
}

function getBankNameMB(bankCD) {
    var locale = kony.i18n.getCurrentLocale();
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (globalSelectBankData[i][0] == bankCD) {
            if (locale == 'th_TH') {
                return globalSelectBankData[i][8];
            } else {
                return globalSelectBankData[i][7];
            }
        }
    }
}
/**************************************************************************************
		Module	: callBackgetTransferFeeMB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : getting transfer fee for Smart and Orft
*****************************************************************************************/
function callBackgetTransferFeeMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var orftFee = resulttable["orftFee"];
            var smartFee = resulttable["smartFee"];
            var cutoffInd = resulttable["cutoffInd"];
            var currentDate = resulttable["orftFeeDate"];
            smartDate = resulttable["smartFeeDate"];
            // changed/added below code to fix DEF1068
            var smartDateNew = resulttable["smartFeeDateNew"];
            var smartfuturetime = resulttable["smartFutureNew"];
            var orftFutureTime = resulttable["orftFuture"];
            orftFee = commaFormatted(orftFee);
            smartFee = commaFormatted(smartFee);
            //ENH_129 SMART Transfer Add Date & Time
            var orftFlag = getORFTFlag(gblisTMB);
            var smartFlag = getSMARTFlag(gblisTMB);
            if (orftFlag == "N") {
                frmTransferLanding.btnTransLndORFT.setVisibility(false);
            } else {
                frmTransferLanding.btnTransLndORFT.setEnabled(true);
                frmTransferLanding.btnTransLndORFT.setVisibility(true)
            }
            if (smartFlag == "N") {
                frmTransferLanding.btnTransLndSmart.setVisibility(false);
            } else {
                if (orftFlag == "N") {
                    frmTransferLanding.btnTransLndORFT.setEnabled(false);
                    frmTransferLanding.btnTransLndSmart.setEnabled(false);
                    frmTransferLanding.hbxTransLndFee.setVisibility(false);
                    frmTransferLanding.hbxFeeTransfer.setVisibility(true);
                    makeWidgetsBelowTransferFeeButtonVisible(true);
                    displayHbxNotifyRecipient(true);
                    if (frmTransferLanding.txtTransLndSmsNEmail.isVisible || frmTransferLanding.txtTransLndSms.isVisible) {
                        frmTransferLanding.lineNotifyRecipientButton.setVisibility(true);
                    } else {
                        frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
                    }
                    frmTransferLanding.lblITMXFee.text = smartFee;
                    frmTransferLanding.lblFeeTransfer.text = kony.i18n.getLocalizedString("TREnter_SMART");
                    frmTransferLanding.lblFeeTransfer.skin = "lblGrey36px";
                    frmTransferLanding.lblITMXFee.setVisibility(true);
                    var smartDateTime = smartDate.split(" ");
                    if (smartDateTime.length > 1) {
                        frmTransferLanding.lblRecievedBy.text = smartDateTime[0];
                        frmTransferLanding.lblRecievedByValue.text = smartDateTime[1];
                    }
                    frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_02");
                    frmTransferLanding.lineRecipientNote.setVisibility(true);
                    frmTransferLanding.btnTransLndSmart.setVisibility(true);
                    frmTransferLanding.btnTransLndORFT.setVisibility(true);
                    frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
                    frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
                    gblTrasORFT = 0;
                    gblTransSMART = 1;
                } else {
                    frmTransferLanding.btnTransLndSmart.setEnabled(true);
                    frmTransferLanding.btnTransLndSmart.setVisibility(true);
                    if (!frmTransferLanding.hbxTransLndFee.isVisible) { //reset if smart only selected already and change to orft allowed bank
                        gblTrasORFT = 0;
                        gblTransSMART = 0;
                        frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
                        frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
                        frmTransferLanding.lblRecievedBy.text = "-";
                        frmTransferLanding.lblRecievedByValue.text = "-";
                    } else {
                        if (gblTransSMART == 1 && !frmTransferLanding.hbxTranLandShec.isVisible) {
                            gblTransSMART = 0;
                            gblTrasORFT = 0;
                            ehFrmTransferLanding_btnTransLndSmart_onClick();
                        } else if (gblTrasORFT == 1 && !frmTransferLanding.hbxTranLandShec.isVisible) {
                            gblTrasORFT = 0;
                            gblTransSMART = 0;
                            ehFrmTransferLanding_btnTransLndORFT_onClick();
                        }
                    }
                    frmTransferLanding.hbxTransLndFee.setVisibility(true);
                    frmTransferLanding.hbxFeeTransfer.setVisibility(false);
                }
            }
            if (smartFlag == "N" && orftFlag == "N" && gblisTMB != gblTMBBankCD) {
                alert("selected To account is not eligible for transfers");
                frmTransferLanding.btnTranLandNext.setEnabled(false);
            } else {
                frmTransferLanding.btnTranLandNext.setEnabled(true);
            }
            frmTransferLanding.btnTransLndSmart.text = smartFee + " \n" + kony.i18n.getLocalizedString("TREnter_SMART");
            frmTransferLanding.btnTransLndORFT.text = orftFee + " \n" + kony.i18n.getLocalizedString("TREnter_ORFT");
            var currentDate = currentDate.toString();
            var dateTD = currentDate.substr(0, 2);
            var yearTD = currentDate.substr(6, 4);
            var monthTD = currentDate.substr(3, 2);
            var completeDate = yearTD + "-" + monthTD + "-" + dateTD;
            gblTransferDate = completeDate;
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            setEnabledTransferLandingPage(true);
        }
    } else {
        setEnabledTransferLandingPage(true);
    }
}

function getORFTFlag(bankCd) {
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (globalSelectBankData[i][0] == bankCd) return globalSelectBankData[i][3];
    }
}

function getSMARTFlag(bankCd) {
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (globalSelectBankData[i][0] == bankCd) return globalSelectBankData[i][4];
    }
}
/*************************************************************************************
		Module	: callBackCrmProfileInq
		Author  : Kony
		Date    : May 05, 2013
		Purpose : cheking channel limit
*************************************************************************************/
function callBackCrmProfileInq(status, resulttable) {
    if (status == 400) {
        //For Encryption MIB-1759 --Setting and clearing
        gblDPPk = undefined != resulttable["pk"] ? resulttable["pk"] : "";
        gblDPRandNumber = undefined != resulttable["randomNumber"] ? resulttable["randomNumber"] : "";
        var reload = undefined != resulttable["reload"] ? resulttable["reload"] : "";
        if (resulttable["opstatus"] == 0) {
            //please do not right any code above this check, can cause issues. - MIB-2281
            if ("true" == reload) {
                checkCrmProfileInq();
                return false;
            }
            /// check channel limit and available balance
            var StatusCode = resulttable["statusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            gblEmailId = resulttable["emailAddr"];
            frmTransferConfirm.lblTransCnfmDateVal.text = resulttable["serverDate"];
            if (StatusCode == 0) {
                if (resulttable["mbFlowStatusIdRs"] == "03") {
                    dismissLoadingScreen();
                    alert("" + kony.i18n.getLocalizedString("keyErrTxnPasslock"));
                    setEnabledTransferLandingPage(true);
                    return false;
                }
                gblTransferRefNo = resulttable["acctIdentValue"]
                gblXferLnkdAccnts = resulttable["p2pLinkedAcct"]
                var temp1 = [];
                var temp = {
                    ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"],
                    mbFlowStatusIdRs: resulttable["mbFlowStatusIdRs"]
                }
                kony.table.insert(temp1, temp);
                gblCRMProfileData = temp1;
                dismissLoadingScreen()
                if (gblTransEmail == 1 && gblTrasSMS == 1 && gblisTMB == gblTMBBankCD) {
                    if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) checkTDBusinessHoursMB();
                    else {
                        if (!verifyExceedAvalibleBalance()) {
                            setEnabledTransferLandingPage(true);
                            return false;
                        }
                        checkDepositAccountInq();
                    }
                } else {
                    if (!verifyExceedAvalibleBalance()) {
                        setEnabledTransferLandingPage(true);
                        return false;
                    }
                    var UsageLimit;
                    var DailyLimit = resulttable["ebMaxLimitAmtCurrent"];
                    DailyLimit = parseFloat(DailyLimit)
                    if (resulttable["ebAccuUsgAmtDaily"] == "" && resulttable["ebAccuUsgAmtDaily"].length == 0) {
                        UsageLimit = 0;
                    } else UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"]);
                    var amtVal = kony.string.replace(frmTransferLanding.txtTranLandAmt.text, ",", "");
                    amtVal = kony.string.replace(amtVal, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                    var transferAmt = parseFloat(amtVal);
                    var channelLimit = DailyLimit - UsageLimit;
                    if (channelLimit < 0) {
                        channelLimit = 0;
                    }
                    var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
                    dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(channelLimit) + ""));
                    //P2P MIB-521 Check To Mobile TAB //
                    if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && !isOwnAccountP2P && gblisTMB != gblTMBBankCD && gblPaynow == true) { //Case Transfer to other bank by Prompt Pay		
                        var p2pTranLimit = 0;
                        if (isNotBlank(resulttable["P2P_TRANSACTION_LIMIT"])) {
                            p2pTranLimit = parseFloat(resulttable["P2P_TRANSACTION_LIMIT"]);
                        }
                        var p2pTranlimitExceedMsg = kony.i18n.getLocalizedString("MIB_P2PTRErr_TxnLimit")
                        p2pTranlimitExceedMsg = p2pTranlimitExceedMsg.replace("{transaction limit}", commaFormatted(p2pTranLimit + ""));
                        //Compare base on minimum value					
                        if (channelLimit < p2pTranLimit) {
                            //Display DailyLimit
                            if (transferAmt > channelLimit) {
                                dismissLoadingScreen();
                                showAlert(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                        }
                        if (p2pTranLimit < channelLimit) {
                            //Display p2pTranLimit
                            if (transferAmt > p2pTranLimit) {
                                dismissLoadingScreen();
                                showAlert(p2pTranlimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                        }
                        if (p2pTranLimit == channelLimit) {
                            //Display p2pTranLimit
                            if (transferAmt > p2pTranLimit) {
                                dismissLoadingScreen();
                                showAlert(p2pTranlimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                        }
                    } else if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && isOwnAccountP2P) { //Transfer Own by Prompt Pay
                    } else if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && gblisTMB == gblTMBBankCD && gblPaynow == true) { //Transfer to other TMB by Prompt Pay	
                        //Check only daily limit
                        if (channelLimit < transferAmt) {
                            dismissLoadingScreen();
                            showAlert(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                            setEnabledTransferLandingPage(true);
                            return false;
                        }
                    } else { //end P2P MIB-521
                        //Check To Account TAB //
                        //checkMaxLimitForTransfer//
                        // Transfer to other TMB						
                        if (!(gblTransEmail == 1 && gblTrasSMS == 1) && gblisTMB == gblTMBBankCD) {
                            if (channelLimit < transferAmt && gblPaynow == true) {
                                dismissLoadingScreen();
                                showAlert(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                        }
                        // Transfer ORFT
                        if (gblisTMB != gblTMBBankCD && gblTrasORFT == 1) {
                            var maxTransferORFT = parseFloat(gblMaxTransferORFT).toFixed(2);
                            if ((channelLimit < maxTransferORFT) && (channelLimit < transferAmt) && gblPaynow == true) {
                                dismissLoadingScreen();
                                showAlert(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            //For schedule ORFT								
                            if ((DailyLimit < transferAmt) && gblPaynow == false) {
                                dismissLoadingScreen();
                                var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
                                dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(DailyLimit) + ""));
                                showAlert(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            //For schedule ORFT
                            if ((maxTransferORFT < transferAmt) && gblPaynow == false) {
                                dismissLoadingScreen();
                                var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferORFT + ""));
                                showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackAmountFields);
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            if ((maxTransferORFT < channelLimit) && (maxTransferORFT < transferAmt)) {
                                dismissLoadingScreen();
                                var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferORFT + ""));
                                showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackAmountFields);
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            if ((maxTransferORFT == channelLimit) && (maxTransferORFT < transferAmt)) {
                                dismissLoadingScreen();
                                var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferORFT + ""));
                                showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackAmountFields);
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                        }
                        // Transfer SMART					
                        if (gblisTMB != gblTMBBankCD && gblTransSMART == 1) {
                            var maxTransferSMART = parseFloat(gblMaxTransferSMART).toFixed(2);
                            if ((channelLimit < maxTransferSMART) && (channelLimit < transferAmt) && gblPaynow == true) {
                                dismissLoadingScreen();
                                showAlert(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            //For schedule SMART								
                            if ((DailyLimit < transferAmt) && gblPaynow == false) {
                                dismissLoadingScreen();
                                var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
                                dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(DailyLimit) + ""));
                                showAlert(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            //For schedule SMART
                            if ((maxTransferSMART < transferAmt) && gblPaynow == false) {
                                dismissLoadingScreen();
                                var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferSMART + ""));
                                showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackAmountFields);
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            if ((maxTransferSMART < channelLimit) && (maxTransferSMART < transferAmt)) {
                                dismissLoadingScreen();
                                var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferSMART + ""));
                                showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackAmountFields);
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                            if ((maxTransferSMART == channelLimit) && (maxTransferSMART < transferAmt)) {
                                dismissLoadingScreen();
                                var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferSMART + ""));
                                showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackAmountFields);
                                setEnabledTransferLandingPage(true);
                                return false;
                            }
                        }
                    } //end To Account TAB												
                    dismissLoadingScreen();
                    if (gblisTMB == null && gblisTMB == "" && gblisTMB == undefined) {
                        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                        setEnabledTransferLandingPage(true);
                        return false;
                    }
                    var transferAmt = frmTransferLanding.txtTranLandAmt.text;
                    transferAmt = kony.string.replace(transferAmt, ",", "");
                    transferAmt = kony.string.replace(transferAmt, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
                    if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && gblisTMB != gblTMBBankCD) {
                        checkTransferTypeMB();
                    } else if (gblisTMB != gblTMBBankCD && gblTrasORFT == 1) { //other Bank Inquiry
                        var transORFTSplitAmnt = parseFloat(gblLimitORFTPerTransaction)
                        if (transferAmt > transORFTSplitAmnt) transferAmt = transORFTSplitAmnt + "";
                        else transferAmt = transferAmt;
                        checkOrftAccountInq(transferAmt);
                    } else if (gblisTMB != gblTMBBankCD && gblTransSMART == 1) {
                        if (getORFTFlag(gblisTMB) != "Y") {
                            checkTransferTypeMB();
                        } else {
                            var transORFTSplitAmnt = parseFloat(gblLimitORFTPerTransaction)
                            if (transferAmt > transORFTSplitAmnt) {
                                transferAmt = transORFTSplitAmnt + "";
                            } else {
                                transferAmt = transferAmt;
                            }
                            checkOrftAccountInq(transferAmt);
                        }
                        //frmTransferConfirm.lblTransCnfmToName.text = frmTransferLanding.lblTranLandToName.text;
                        //frmTransferConfirm.lblTransCnfmToBankName.text = frmTransferLanding.lblTranLandToBankName.text;
                    } else if (gblisTMB == gblTMBBankCD) //TMB Banking
                    {
                        checkDepositAccountInq();
                    } else if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                        checkAccountWithdrawInq();
                    } else {
                        checkTransferTypeMB();
                    }
                }
            } else {
                dismissLoadingScreen();
                showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
                setEnabledTransferLandingPage(true);
                return false;
            }
        } else {
            dismissLoadingScreen();
            if (resulttable["opstatus"] == "8005") {
                showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
                setEnabledTransferLandingPage(true);
            } else {
                showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
                setEnabledTransferLandingPage(true);
            }
        }
    }
}
/*************************************************************************************
		Module	: callBackOrftAccountInq
		Author  : Kony
		Date    : May 05, 2013
		Purpose : cheking Account Name limit
***************************************************************************************/
function callBackOrftAccountInq(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            ///fetching To Account Name for ORFT Inq
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            if (StatusCode != "0") {
                dismissLoadingScreen();
                showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
                setEnabledTransferLandingPage(true);
                return false;
            } else {
                var ToAccountName = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];
                frmTransferConfirm.lblTransCnfmToAccountName.text = "";
                //frmTransferConfirm.lblTransCnfmToName.text = frmTransferLanding.lblTranLandToName.text;
                frmTransferConfirm.lblTransCnfmToAccountName.text = ToAccountName;
                //frmTransferConfirm.lblTransCnfmToBankName.text = frmTransferLanding.lblTranLandToBankName.text;
                dismissLoadingScreen();
                // commenting below code to allow evem account name is blank  Ticket#28938
                /*if (ToAccountName == null || ToAccountName == "") {
                	dismissLoadingScreen();
                	alert(" " + kony.i18n.getLocalizedString("keyNoAccountFoundwiththegivenNumber"));
                	return false;
                }*/
                /** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
                if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                    checkAccountWithdrawInq();
                } else {
                    checkTransferTypeMB();
                }
            }
        } else {
            dismissLoadingScreen();
            showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
            setEnabledTransferLandingPage(true);
        }
    } else {
        setEnabledTransferLandingPage(true);
    }
}
/*************************************************************************************
		Module	: checkDepositAccountInq
		Author  : Kony
		Date    : May 05, 2013
		Purpose : cheking TMB Account Name
*************************************************************************************/
function callBackDepositAccountInq(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            // fetching To Account Name for TMB Inq
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            dismissLoadingScreen();
            if (StatusCode != "0") {
                //showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), kony.i18n.getLocalizedString("info"),"info");
                showAlertWithCallBack(kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
                setEnabledTransferLandingPage(true);
                return false;
            } else {
                var ToAccountName = resulttable["accountTitle"]
                frmTransferConfirm.lblTransCnfmToAccountName.text = "";
                if (ToAccountName != null && ToAccountName.length > 0) {
                    //happy Path;(ToAccountName)
                    //frmTransferConfirm.lblTransCnfmToName.text = frmTransferLanding.lblTranLandToName.text;
                    frmTransferConfirm.lblTransCnfmToAccountName.text = ToAccountName;
                    //frmTransferConfirm.lblTransCnfmToBankName.text = frmTransferLanding.lblTranLandToBankName.text;
                } else {
                    //showAlertRcMB(kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), kony.i18n.getLocalizedString("info"),"info");
                    showAlertWithCallBack(kony.i18n.getLocalizedString("Receipent_alert_correctdetails"), kony.i18n.getLocalizedString("info"), callBackAccountNumberField);
                    setEnabledTransferLandingPage(true);
                    return false;
                }
                /** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
                if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                    checkAccountWithdrawInq();
                } else {
                    /*** invoking fundTransferInquiry service to get the Fee Amnt fo Transaction.  */
                    checkFundTransferInqMB();
                }
            }
        } else {
            dismissLoadingScreen();
            setEnabledTransferLandingPage(true);
            showAlertWithCallBack(resulttable["errMsg"], resulttable["XPServerStatCode"], callBackAccountNumberField);
        }
    } else {
        setEnabledTransferLandingPage(true);
    }
}
/*************************************************************************************
		Module	: callBackAccountWithdrawInq
		Author  : Kony
		Date    : May 05, 2013
		Purpose : cheking AccountPreWithdrawInq
**************************************************************************************/
function callBackAccountWithdrawInq(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            dismissLoadingScreen();
            if (StatusCode != "0") {
                showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
                setEnabledTransferLandingPage(true);
                return false;
            } else {
                var totalInterest = resulttable["AcctWithdrawalInqRs"][0].totalInterest;
                var penalityAmt = resulttable["AcctWithdrawalInqRs"][0].penaltyAmt;
                var taxAmt = resulttable["AcctWithdrawalInqRs"][0].taxAmt;
                var netAmt = resulttable["AcctWithdrawalInqRs"][0].OutstandingBalAmt;
                gblTDDateFlag = true;
                var totInt = "";
                if (parseFloat(totalInterest) < 0) {
                    totInt = "0";
                } else {
                    totInt = totalInterest;
                }
                frmTransferConfirm.lblTransCnfmIntAmtVal.text = commaFormatted(totInt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmTransferConfirm.lblTransCnfmPenaltyAmtVal.text = commaFormatted(penalityAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmTransferConfirm.lblTransCnfmTaxAmtVal.text = commaFormatted(taxAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmTransferConfirm.lblTransCnfmNetAmtVal.text = commaFormatted(netAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                gblWithdrawalTotInterest = (parseFloat(totalInterest).toFixed(2)) + "";
                gblWithdrawalTaxAmt = (parseFloat(taxAmt).toFixed(2)) + "";
                if (gblisTMB == gblTMBBankCD) {
                    checkFundTransferInqMB()
                } else {
                    checkTransferTypeMB();
                }
            }
        } else {
            dismissLoadingScreen();
            showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
            setEnabledTransferLandingPage(true);
        }
    } else {
        setEnabledTransferLandingPage(true);
    }
}
/**************************************************************************************
		Module	: callBackTDBussinessHours
		Author  : Kony
		Date    : May 05, 2013
		Purpose : callback function for callBackTDBussinessHours
*****************************************************************************************/
function callBackTDBussinessHours(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var tdflag = resulttable["tdBusHrsFlag"];
            if (tdflag == "false") {
                alert(" " + kony.i18n.getLocalizedString("keyTdBusinessHours"));
                setEnabledTransferLandingPage(true);
                return false;
            } else checkDepositAccountInq();
        } else {
            dismissLoadingScreen();
            showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
            setEnabledTransferLandingPage(true);
        }
    } else {
        setEnabledTransferLandingPage(true);
    }
}
/**************************************************************************************
		Module	: callBackFundTransferInqMB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : callback function for callBackFundTransferInqMB
****************************************************************************************
*/
function callBackFundTransferInqMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            // fetching To Account Name for TMB Inq
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            var fee = resulttable["transferFee"];
            var FlagFeeReg = resulttable["FlagFeeReg"];
            var tranCode = resulttable["TranCode"]
            var temp1 = [];
            var temp = {
                FlagFeeReg: resulttable["FlagFeeReg"],
                tranCode: resulttable["TranCode"]
            }
            kony.table.insert(temp1, temp)
            gblFundXferData = temp1;
            if (fee == "" && fee.length == 0) fee = "0.00"
            dismissLoadingScreen();
            if (StatusCode != "0") {
                showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
                setEnabledTransferLandingPage(true);
                return false;
            } else {
                if (FlagFeeReg == "I") frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + parseFloat(fee).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
                else frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
                checkTransferTypeMB()
            }
        } else {
            dismissLoadingScreen();
            showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
            setEnabledTransferLandingPage(true);
        }
    } else {
        setEnabledTransferLandingPage(true);
    }
}
/*************************************************************************

	Module	: onSelectTDDetailsPOP
	Author  : Kony
	Purpose : selected TD Account details

****/
function onSelectTDDetailsPOP(oneTDMaturityData) {
    if (oneTDMaturityData) {
        var selectedTDMaturityData = popTransfersTDAccount.segTransfersTD.data[0];
    } else {
        var selectedTDMaturityData = popTransfersTDAccount.segTransfersTD.selectedItems[0];
    }
    var amt = selectedTDMaturityData.amount;
    frmTransferLanding.txtTranLandAmt.text = amt;
    displayP2PITMXFee(true, amt);
    //frmTransferLanding.lineAmount.skin = lineBlue;
    //frmTransferConfirm.lblTransCnfmBalAftrAmt.text = popTransfersTDAccount.segTransfersTD.selectedItems[0].maturityDate;
    frmTransferConfirm.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
    frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + selectedTDMaturityData.maturityDate + ")";
    gblTDTransfer = selectedTDMaturityData.maturityDate;
    gblTDDeposit = selectedTDMaturityData.depositeNo;
    frmTransferLanding.lblMaturityDateValue.text = gblTDTransfer;
    frmTransferLanding.lblPrincipalAmountValue.text = amt;
    frmTransferLanding.lblPrincipalAmount.text = kony.i18n.getLocalizedString("TRConfirm_Principal");
    frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_01");
    frmTransferLanding.hbxMaturityPrincipal.setVisibility(true);
    frmTransferLanding.lblSelectMaturity.setVisibility(false);
    enableTransferTextBoxAmount(false);
    //frmTransferLanding.btnSchedTo.onClick = onClickCalenderTD;
    popTransfersTDAccount.dismiss();
    setOnClickSetCanlenderBtn(false);
    makeWidgetsBelowTransferFeeButtonVisible(true);
    frmTransferLanding.hbxTranLandShec.setVisibility(true);
    frmTransferLanding.lineTransferSchedule.setVisibility(true);
    //frmTransferLanding.lineTD.skin = lineBlue;
    //getTransferFeeMB();
    dismissLoadingScreen();
}
/*************************************************************************

	Module	: transAmountOnDone
	Author  : Kony
	Purpose : Calculating the fee for ORFT and SMART based on the amount entered by the user

*************************************************************************/
function transAmountOnDone(isFromTextBox, isServiceCallNeeded) {
    if (isSignedUser) {
        gblAmountChange = false;
        var availableBal;
        var i = gbltranFromSelIndex[1];
        var fromData = frmTransferLanding.segTransFrm.data;
        availableBal = fromData[i].lblBalance;
        availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
        availableBal = kony.string.replace(availableBal, ",", "");
        availableBal = parseFloat(availableBal.trim());
        var findDot;
        var isCrtFormt;
        enteredAmount = frmTransferLanding.txtTranLandAmt.text;
        isCrtFormt = amountValidationMB(enteredAmount);
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        isAvoidDuplicateError = false;
        if (!isCrtFormt) {
            showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"), callBackAmountFields);
            frmTransferLanding.hbxTransLndFee.setVisibility(false);
            frmTransferLanding.hbxFeeTransfer.setVisibility(true);
            //frmTransferLanding.hboxFeeType.setVisibility(false);
            frmTransferLanding.hbxFeeTransfer.setVisibility(true);
            frmTransferLanding.lblITMXFee.text = "";
            frmTransferLanding.lblFeeTransfer.skin = "lblWhite36px";
            frmTransferLanding.txtTranLandAmt.text = "";
            isAvoidDuplicateError = true;
            setEnabledTransferLandingPage(true);
            return false;
        }
        frmTransferLanding.txtTranLandAmt.text = numberWithCommas(fixedToTwoDecimal(enteredAmount));
        enteredAmount = fixedToTwoDecimal(enteredAmount);
        if (gblSelTransferMode == 2) {
            var mobileNumber = removeHyphenIB(frmTransferLanding.txtOnUsMobileNo.text);
            if (!isNotBlank(frmTransferLanding.lblMobileNoTemp.text)) {
                frmTransferLanding.lblMobileNoTemp.text = "";
            }
            if (!isNotBlank(enteredAmount) || parseFloat(enteredAmount, 10) == 0) {
                displayP2PITMXFee(false, enteredAmount);
            } else {
                if (parseFloat(enteredAmount, 10) > 0 && isNotBlank(mobileNumber)) {
                    if (kony.string.equalsIgnoreCase(frmTransferLanding.lblMobileNoTemp.text, frmTransferLanding.txtOnUsMobileNo.text)) {
                        displayP2PITMXFee(true, enteredAmount);
                    } else {
                        if (!recipientMobileVal(mobileNumber)) {
                            showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyEnteredMobileNumberisnotvalid"), kony.i18n.getLocalizedString("info"), callBackMobileNoFieldsP2P);
                            return false;
                        } else {
                            invokeServiceP2PITMX(false);
                        }
                    }
                }
            }
        } else if (gblSelTransferMode == 3) {
            var citizenID = removeHyphenIB(frmTransferLanding.txtCitizenID.text);
            if (!isNotBlank(frmTransferLanding.lblMobileNoTemp.text)) {
                frmTransferLanding.lblMobileNoTemp.text = "";
            }
            if (!isNotBlank(enteredAmount) || parseFloat(enteredAmount, 10) == 0) {
                displayP2PITMXFee(false, enteredAmount);
                //frmTransferLanding.lineAmount.skin = "linePopupBlack";
            } else {
                if (parseFloat(enteredAmount, 10) > 0 && isNotBlank(citizenID)) {
                    if (kony.string.equalsIgnoreCase(frmTransferLanding.lblMobileNoTemp.text, frmTransferLanding.txtCitizenID.text)) {
                        displayP2PITMXFee(true, enteredAmount);
                        //frmTransferLanding.lineAmount.skin = "lineBlue";
                    } else {
                        if (!checkCitizenID(citizenID)) {
                            showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PkeyCIisnotvalid"), kony.i18n.getLocalizedString("info"), callBackCitizenIDFieldsP2P);
                            return false;
                        } else {
                            invokeServiceP2PITMX(false);
                        }
                    }
                }
            }
        }
        if (!isNotBlank(gblisTMB)) {
            setEnabledTransferLandingPage(true);
            return false;
        }
        if (gblDeviceInfo["name"] == "iPhone") {
            recheckEnteredAcctInRecipientListForNotify();
        }
        if (gblisTMB == gblTMBBankCD) { // Transfer To TMB
            gblTrasORFT = 0;
            gblTransSMART = 0;
            frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
            frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
            var date = new Date(GLOBAL_TODAY_DATE);
            var curMnth = date.getMonth() + 1;
            var curDate = date.getDate();
            if ((curMnth.toString().length) == 1) {
                curMnth = "0" + curMnth;
            }
            if ((curDate.toString().length) == 1) {
                curDate = "0" + curDate;
            }
            var datetime = "" + curDate + "/" + curMnth + "/" + date.getFullYear();
            frmTransferLanding.lblRecievedBy.text = datetime;
            frmTransferLanding.lblRecievedByValue.text = kony.i18n.getLocalizedString("keyNOW");
            if (enteredAmount == "") {
                return false;
            }
            makeWidgetsBelowTransferFeeButtonVisible(true);
            if (gblSelTransferMode == 1) {
                if (gblTransEmail == 1 && gblTrasSMS == 1) {
                    displayHbxNotifyRecipient(false);
                    frmTransferLanding.lineMyNote.setVisibility(false);
                } else {
                    frmTransferLanding.lineMyNote.setVisibility(true);
                    displayHbxNotifyRecipient(true);
                }
                displayP2PITMXFee(true, enteredAmount);
            } else if (gblSelTransferMode == 2) {
                displayOwnerNotifyRecipientP2P();
            } else if (gblSelTransferMode == 3) {
                frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(false);
            }
            frmTransferLanding.hbxTransLndFee.setVisibility(false);
            frmTransferLanding.lineRecipientNote.setVisibility(true);
            if (enteredAmount.indexOf(".") > -1) {
                frmTransferLanding.txtTranLandAmt.text = numberWithCommas(fixedToTwoDecimal(enteredAmount));
            }
        } else { // Transfer To Other Bank
            if (enteredAmount == "") {
                return false;
            }
            if (enteredAmount.indexOf(".") > -1) {
                frmTransferLanding.txtTranLandAmt.text = numberWithCommas(fixedToTwoDecimal(enteredAmount));
            }
            if (gblSelTransferMode == 2 || gblSelTransferMode == 3) {
                return false;
            }
            if (isServiceCallNeeded || isFromTextBox) {
                getTransferFeeMB();
            }
        }
    }
    if (!isNotBlank(frmTransferLanding.txtTranLandAmt.text) || !isSignedUser) {
        if (!isFromTextBox) {
            frmTransferLanding.txtTranLandAmt.setFocus(true);
            setEnabledTransferLandingPage(true);
            return false;
        }
    }
}

function displayOwnerNotifyRecipientP2P() {
    frmTransferLanding.lineMyNote.setVisibility(false);
    frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(false);
    if (!isOwnAccountP2P) {
        frmTransferLanding.lineMyNote.setVisibility(true);
        frmTransferLanding.hbxOnUsNotifyRecipient.setVisibility(true);
    }
}

function displayDebitReceivedNotify(isShow) {
    resetDefaultDisplayAfterTDP2P(true);
}

function amountValidationMB(amount) {
    if (amount != null && amount != "") {
        if (kony.string.containsOnlyGivenChars(amount, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."])) {
            amount = amount.replace(/,/g, "");
            amount = amount.replace(".", "");
            //ADDED os.tonumber(amount)== nil TO CHECK IF THE USER ONLY ENTER "." or ","
            if (kony.os.toNumber(amount) <= 0 || kony.os.toNumber(amount) == null) {
                return false;
            } else return true;
        } else return false;
    } else if (amount == "") {
        return true;
    } else return false;
}

function numberWithCommas(amount) {
    return (amount + "").replace(/\b(\d+)((\.\d+)*)\b/g, function(a, b, c) {
        return (b.charAt(0) > 0 && !(c || ".").lastIndexOf(".") ? b.replace(/(\d)(?=(\d{3})+$)/g, "$1,") : b) + c;
    });
}