gblCustomerBBMBInqRs = [];
gblcustomerAccountsListBBMB = [];
gblmasterBillerAndTopupBBMB = [];
gblCustomerBillListBB = [];
gblBillerCategoriesBBMB = [];
gblNoOfFromAcsBB = 0;
gblRef2masterData = "";
executionFlowBBMB = false;
gblBBRefNoMB = "";
gblBBorBillers = true;
gblToUpdateBBList = false;
gblBillerMethodBBMB = "";
gblBillerCompCodeBBMB = ""
isToUpdateAccListMB = true;
gblPmtStatusMB = "";
isRefreshCalenderMB = "";
gblOnClickLocaleChangeBB = false;
gblUpdateMasterData = false;
gblBillersForSearchMB = [];
gblSugBillersForSearchMB = [];
gblBBMoreMB = 0;
gblSugBBMoreMB = 0;
messageObj = {};

function checkUserStatusBBMB() {
    var inputParam = {
        rqUUId: "",
        channelName: "MB-INQ"
    }
    showLoadingScreen();
    // callBackUSerStatusDSM();
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackUSerStatusBBMB)
}
//function callBackUSerStatusDSM() {
function callBackUSerStatusBBMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            var MBUserStatus = callBackResponse["mbFlowStatusIdRs"];
            var IBUserStatus = callBackResponse["ibUserStatusIdTr"];
            if (flowSpa) {
                if (IBUserStatus == "04") {
                    gblIBFlowStatus = "04";
                    dismissLoadingScreen();
                    // frmDreamSavingEdit.show();
                    //  showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"),kony.i18n.getLocalizedString("info"))
                    popTransferConfirmOTPLock.show();
                } else {
                    gblIBFlowStatus = IBUserStatus;
                    //  alert("success")
                    //dismissLoadingScreen();
                    getSelectBillerCategoryServiceBBMB();
                    //onSelectBBSubMenuMB();
                    // DreamonClickAgreeOpenDSActs()
                }
            } else {
                if (MBUserStatus == "03") {
                    gblUserLockStatusMB = "03";
                    dismissLoadingScreen();
                    // frmDreamSavingEdit.show();
                    //  showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"),kony.i18n.getLocalizedString("info"))
                    popTransferConfirmOTPLock.show();
                    //getSelectBillerCategoryServiceBBMB();
                } else {
                    gblUserLockStatusMB = MBUserStatus;
                    //  alert("success")
                    //dismissLoadingScreen();
                    getSelectBillerCategoryServiceBBMB();
                    //onSelectBBSubMenuMB();
                    // DreamonClickAgreeOpenDSActs()
                }
            }
        } else {
            gblUserLockStatusMB = "03";
            dismissLoadingScreen();
            //showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"),kony.i18n.getLocalizedString("info"))
            popTransferConfirmOTPLock.show();
        }
    }
    // DreamcheckOpenActBusHrs();
}

function getSelectBillerCategoryServiceBBMB() {
    showLoadingScreen();
    var inputParams = {
        BillerCategoryGroupType: gblBillerCategoryGroupType
            //clientDate:getCurrentDate()not working on IE 8
    };
    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, getSelectBillerListServiceAsyncCallbackBBMB);
}

function getSelectBillerListServiceAsyncCallbackBBMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            onSelectBBSubMenuMB();
        }
    }
}

function onSelectBBSubMenuMB() {
    gblMyBillerTopUpBB = 2;
    gblRetryCountRequestOTP = "0";
    executionFlowBBMB = false;
    isToUpdateAccListMB = true;
    gblToUpdateBBList = false;
    frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString("keyMyBB");
    if (gblmasterBillerAndTopupBBMB.length == 0) {
        inquireMasterBillerAndTopupMB();
    } else {
        populateMySuggestListBBMB(gblmasterBillerAndTopupBBMB);
        updateAccountListBBMB();
    }
}

function onSelectRef2BB() {
    var masterData = [];
    var tempData = "";
    var ref2List = gblRef2masterData.split(",");
    masterData.push({
        "lblCategory": {
            "text": kony.i18n.getLocalizedString("keySelectRef2BB")
        },
        "BillerCategoryID": {
            "text": "0"
        }
    });
    for (var i = 0; i < ref2List.length; i++) {
        tempData = {
            "lblCategory": {
                "text": ref2List[i]
            },
            "BillerCategoryID": {
                "text": i + 1
            }
        }
        masterData.push(tempData);
    }
    popUpMyBillers.segPop.setData(masterData);
    popUpMyBillers.show();
}

function searchSuggestedBillersBBMB() {
    var search = "";
    var currForm = kony.application.getCurrentForm();
    if (currForm.id == 'frmAddTopUpToMB') return false;
    search = currForm.txbSearch.text;
    var tmpCatSelected = currentCatSelected;
    //alert(search.length + " search.length " + currentCatSelected)
    if (search.length < 3 && (tmpCatSelected == null || tmpCatSelected == 0)) {
        currForm.lblSuggestedBillersText.setVisibility(true);
        currForm.lblErrorMsg.setVisibility(false);
        currForm.segBillersList.removeAll();
        currForm.segSuggestedBillers.removeAll();
        if (gblBBorBillers == true) {
            if (gblCustomerBBMBInqRs.length > 0) {
                frmMyTopUpList.lblNoBill.setVisibility(false);
                currForm.lblMyBillers.setVisibility(true);
                currForm.hbxMoreBB.setVisibility(true);
                //currForm.segBillersList.setData(gblCustomerBBMBInqRs);
                //populateMyBillsBBMB(gblCustomerBBMBInqRs);
                gblBillersForSearchMB = [];
                gblBBMoreMB = 0;
                gblBillersForSearchMB = gblCustomerBBMBInqRs;
                onMyBBSelectBillersMoreDynamicMB();
            } else {
                frmMyTopUpList.lblNoBill.setVisibility(true);
            }
        } else {
            //alert(gblCustomerBillListBB.length + " gblCustomerBillListBB.leng")
            if (gblCustomerBillListBB.length > 0) {
                frmMyTopUpList.lblNoBill.setVisibility(false);
                currForm.lblMyBillers.setVisibility(true);
                currForm.hbxMoreBB.setVisibility(true);
                //currForm.segBillersList.setData(gblCustomerBillListBB);
                //populateMyBillsBBMB(gblCustomerBillListBB);
                gblBillersForSearchMB = [];
                gblBBMoreMB = 0;
                gblBillersForSearchMB = gblCustomerBillListBB;
                onMyBBSelectBillersMoreDynamicMB();
            } else {
                frmMyTopUpList.lblNoBill.setVisibility(true);
            }
        }
        populateMySuggestListBBMB(gblmasterBillerAndTopupBBMB);
        return;
    }
    if (search.length >= 3 || tmpCatSelected != null) {
        currForm.lblSuggestedBillersText.setVisibility(true);
        getBillerLPSuggestListBBMB();
    }
}

function getBillerLPSuggestListBBMB() {
    var currForm = kony.application.getCurrentForm()
    var search = currForm.txbSearch.text;
    var tmpCatSelected = currentCatSelected;
    if (tmpCatSelected == null) tmpCatSelected = 0;
    var tempMaterBillerTopupBB = [];
    var tempBeepAndBills = [];
    var lenMasterBiller = gblmasterBillerAndTopupBBMB.length;
    if (gblBBorBillers == true) {
        var lenBBList = gblCustomerBBMBInqRs.length;
    } else {
        var lenBBList = gblCustomerBillListBB.length;
    }
    if (search.length >= 3 && tmpCatSelected != 0) {
        var lowCaseSearch = search.toLowerCase();
        for (var i = 0; i < lenMasterBiller; i++) {
            var tmpString = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"];
            tmpString = tmpString.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            if (gblmasterBillerAndTopupBBMB[i]["BillerCategoryID"] == tmpCatSelected && (flag != "-1" || flag >= 0)) {
                tempMaterBillerTopupBB.push(gblmasterBillerAndTopupBBMB[i]);
            }
        }
        for (var i = 0; i < lenBBList; i++) {
            if (gblBBorBillers == true) {
                var tmpString = gblCustomerBBMBInqRs[i]["lblBillerNickname"];
                var bbCompcode = gblCustomerBBMBInqRs[i]["lblBillerName"];
            } else {
                var tmpString = gblCustomerBillListBB[i]["lblBillerNickname"].text;
                var bbCompcode = gblCustomerBillListBB[i]["lblBillerName"].text;
            }
            if (tmpString == undefined) tmpString = "";
            if (bbCompcode == undefined) bbCompcode = "";
            tmpString = tmpString.toLowerCase();
            bbCompcode = bbCompcode.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            var flag1 = bbCompcode.search(lowCaseSearch);
            if (gblBBorBillers == true) {
                if (gblCustomerBBMBInqRs[i]["hdnBillerCategoryID"] == tmpCatSelected && (flag != "-1" || flag >= 0 || flag1 != "-1" || flag1 >= 0)) {
                    tempBeepAndBills.push(gblCustomerBBMBInqRs[i]);
                }
            } else {
                if (gblCustomerBillListBB[i]["BillerCategoryID"].text == tmpCatSelected && (flag != "-1" || flag >= 0 || flag1 != "-1" || flag1 >= 0)) {
                    tempBeepAndBills.push(gblCustomerBillListBB[i]);
                }
            }
        }
    } else if (search.length < 3 && tmpCatSelected != 0) {
        for (var i = 0; i < lenMasterBiller; i++) {
            if (gblmasterBillerAndTopupBBMB[i]["BillerCategoryID"] == tmpCatSelected) tempMaterBillerTopupBB.push(gblmasterBillerAndTopupBBMB[i]);
        }
        for (var i = 0; i < lenBBList; i++) {
            if (gblBBorBillers == true) {
                if (gblCustomerBBMBInqRs[i]["hdnBillerCategoryID"] == tmpCatSelected) {
                    tempBeepAndBills.push(gblCustomerBBMBInqRs[i]);
                }
            } else {
                if (gblCustomerBillListBB[i]["BillerCategoryID"].text == tmpCatSelected) {
                    tempBeepAndBills.push(gblCustomerBillListBB[i]);
                }
            }
        }
    } else if (search.length >= 3 && tmpCatSelected == 0) {
        var lowCaseSearch = search.toLowerCase();
        for (var i = 0; i < lenMasterBiller; i++) {
            var tmpString = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"];
            tmpString = tmpString.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            //alert(tmpString + " " + lowCaseSearch + " " + upperCaseSearch)
            if (flag != "-1" || flag >= 0) tempMaterBillerTopupBB.push(gblmasterBillerAndTopupBBMB[i]);
        }
        //alert("lenBBList = " + lenBBList);
        for (var i = 0; i < lenBBList; i++) {
            if (gblBBorBillers == true) {
                var tmpString = gblCustomerBBMBInqRs[i]["lblBillerNickname"];
                var bbCompcode = gblCustomerBBMBInqRs[i]["lblBillerName"];
            } else {
                var tmpString = gblCustomerBillListBB[i]["lblBillerNickname"].text;
                var bbCompcode = gblCustomerBillListBB[i]["lblBillerName"].text;
            }
            //alert(tmpString + " " + bbCompcode)
            if (tmpString == undefined) tmpString = "";
            if (bbCompcode == undefined) bbCompcode = "";
            tmpString = tmpString.toLowerCase();
            bbCompcode = bbCompcode.toLowerCase();
            var flag = tmpString.search(lowCaseSearch);
            var flag1 = bbCompcode.search(lowCaseSearch);
            if (flag != "-1" || flag >= 0 || flag1 != "-1" || flag1 >= 0) {
                if (gblBBorBillers == true) {
                    tempBeepAndBills.push(gblCustomerBBMBInqRs[i]);
                } else {
                    tempBeepAndBills.push(gblCustomerBillListBB[i]);
                }
            }
        }
    } else {
        currForm.lblSuggestedBillersText.setVisibility(false);
        currForm.lblMyBillers.setVisibility(false);
        currForm.segBillersList.removeAll();
        currForm.segSuggestedBillers.removeAll();
    }
    if (tempMaterBillerTopupBB.length > 0) {
        currForm.lblSuggestedBillersText.setVisibility(true);
        currForm.hbxSugMoreBB.setVisibility(true);
        currForm.segSuggestedBillers.removeAll();
        populateMySuggestListBBMB(tempMaterBillerTopupBB);
    } else {
        currForm.lblSuggestedBillersText.setVisibility(false);
        currForm.segSuggestedBillers.removeAll();
        currForm.hbxSugMoreBB.setVisibility(false);
    }
    //alert(tempBeepAndBills.length + " " + tempMaterBillerTopupBB.length )
    if (tempBeepAndBills.length > 0) {
        currForm.lblMyBillers.setVisibility(true);
        currForm.segBillersList.removeAll();
        currForm.hbxMoreBB.setVisibility(true);
        //populateMyBillsBBMB(tempBeepAndBills);
        gblBillersForSearchMB = [];
        gblBBMoreMB = 0;
        gblBillersForSearchMB = tempBeepAndBills;
        onMyBBSelectBillersMoreDynamicMB();
    } else {
        currForm.lblMyBillers.setVisibility(false);
        currForm.segBillersList.removeAll();
        currForm.hbxMoreBB.setVisibility(false);
    }
    if (tempBeepAndBills.length == 0 && tempMaterBillerTopupBB.length == 0) {
        //showAlert(kony.i18n.getLocalizedString("keybillernotfound"),kony.i18n.getLocalizedString("info"));
        currForm.lblNoBill.setVisibility(true);
        currForm.lblErrorMsg.setVisibility(false);
        currForm.hbxSugMoreBB.setVisibility(false);
        currForm.lblSuggestedBillersText.setVisibility(false);
        currForm.lblMyBillers.setVisibility(false);
        currForm.hbxMoreBB.setVisibility(false);
    }
    if (lenBBList == 0 && tempMaterBillerTopupBB.length > 0) {
        currForm.lblNoBill.setVisibility(true);
        currForm.lblErrorMsg.setVisibility(false);
    } else if (lenBBList == 0 && tempMaterBillerTopupBB.length == 0) {
        currForm.lblNoBill.setVisibility(false);
        currForm.lblErrorMsg.setVisibility(true);
    }
    if (lenBBList > 0 && tempBeepAndBills.length == 0 && tempMaterBillerTopupBB.length == 0) {
        currForm.lblNoBill.setVisibility(false);
        currForm.lblErrorMsg.setVisibility(true);
    } else if (lenBBList > 0 && (tempBeepAndBills.length > 0 || tempMaterBillerTopupBB.length > 0)) {
        currForm.lblNoBill.setVisibility(false);
        currForm.lblErrorMsg.setVisibility(false);
    }
}

function inquireMasterBillerAndTopupMB() {
    var inputParams = {
        IsActive: "1",
        BillerGroupType: "0"
    };
    inputParams["flagBillerList"] = "BB";
    showLoadingScreen();
    gblmasterBillerAndTopupBBMB = [];
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, getMasterBillerForBBMB);
}

function getMasterBillerForBBMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0 || callBackResponse["opstatus"] == "0") {
            if (callBackResponse["StatusCode"] == 0 || callBackResponse["StatusCode"] == "0") {
                var length = callBackResponse["MasterBillerInqRs"].length;
                //alert(length + "  topups length");	
                for (var i = 0; i < length; i++) {
                    var noOfChannels = callBackResponse["MasterBillerInqRs"][i]["ValidChannel"].length;
                    for (var j = 0; j < noOfChannels; j++) {
                        if (callBackResponse["MasterBillerInqRs"][i]["ValidChannel"][j]["ChannelCode"] == "51") {
                            gblmasterBillerAndTopupBBMB.push(callBackResponse["MasterBillerInqRs"][i]);
                            break;
                        }
                    }
                }
                for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
                    if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"] + ":";
                    if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"] + ":";
                    for (var j = 0; j < gblmasterBillerAndTopupBBMB[i]["BillerMiscData"].length; j++) {
                        if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                        if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                            if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                        }
                    }
                }
                populateBillerCategory();
                if (gblUpdateMasterData == false) {
                    showLoadingScreen();
                    updateAccountListBBMB();
                } else {
                    gblUpdateMasterData = false;
                    var inputParam = {};
                    inputParam["rmNum"] = "";
                    inputParam["PIN"] = messageObj["pinCode"];
                    inputParam["finTxnAmount"] = messageObj["finTxnAmount"];
                    inputParam["ref1"] = messageObj["ref1"];
                    inputParam["BillerName"] = getBillerNameMB(messageObj["BBCompcode"]);
                    showLoadingScreen();
                    invokeServiceSecureAsync("CustomerBBPaymentStatusInquiry", inputParam, customerBBPayemntStatusInquiryMBCallBack);
                }
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function populateBillerCategory() {
    gblBillerCategoriesBBMB = [];
    var tempData;
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            tempData = {
                "lblCategory": {
                    "text": gblmasterBillerAndTopupBBMB[i]["BillerCatgoryNameEN"]
                },
                "BillerCategoryID": {
                    "text": gblmasterBillerAndTopupBBMB[i]["BillerCategoryID"]
                }
            }
        } else {
            tempData = {
                "lblCategory": {
                    "text": gblmasterBillerAndTopupBBMB[i]["BillerCatgoryNameTH"]
                },
                "BillerCategoryID": {
                    "text": gblmasterBillerAndTopupBBMB[i]["BillerCategoryID"]
                }
            }
        }
        var isExist = false;
        for (var j = 0; j < gblBillerCategoriesBBMB.length; j++) {
            //alert(gblBillerCategoriesBBMB[j]["BillerCategoryID"] + " " + tempData["BillerCategoryID"]);
            if (gblBillerCategoriesBBMB[j]["BillerCategoryID"]["text"] == tempData["BillerCategoryID"]["text"]) {
                isExist = true;
                break;
            }
        }
        if (isExist == false) gblBillerCategoriesBBMB.push(tempData);
    }
    gblBillerCategoriesBBMB.sort(function(a, b) {
        if (a["lblCategory"]["text"] > b["lblCategory"]["text"]) return 1;
        if (a["lblCategory"]["text"] < b["lblCategory"]["text"]) return -1;
        return 0;
    });
    gblBillerCategoriesBBMB.unshift({
        "lblCategory": {
            "text": kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")
        },
        "BillerCategoryID": {
            "text": "0"
        }
    });
}

function onlinePaymentInqBB() {
    var inputParam = {};
    inputParam["TrnId"] = "";
    inputParam["BankRefId"] = "";
    inputParam["BankId"] = "011";
    inputParam["BranchId"] = "0001";
    inputParam["EffDt"] = "";
    inputParam["Ref1"] = frmAddTopUpToMB.txtRef1.text;
    if (frmAddTopUpToMB.hbxref2.isVisible == true) {
        if (frmAddTopUpToMB.txtRef2.isVisible == true) inputParam["Ref2"] = frmAddTopUpToMB.txtRef1.text;
        else inputParam["Ref2"] = frmAddTopUpToMB.btnRef2Dropdown.text;
    } else {
        inputParam["Ref2"] = "";
    }
    inputParam["Ref3"] = "";
    inputParam["Ref4"] = "";
    inputParam["MobileNumber"] = frmAddTopUpToMB.txtRef1.text;
    //inputParam["compCode"] = frmIBBeepAndBillApplyCW.lblAddBillerCompCode.text;
    inputParam["compCode"] = gblBillerCompCodeBBMB;
    inputParam["Amt"] = "0.0";
    inputParam["isChannel"] = "MB";
    inputParam["commandType"] = "Inquiry";
    inputParam["BillerGroupType"] = "0";
    showLoadingScreen();
    invokeServiceSecureAsync("onlinePaymentInq", inputParam, onlinePaymentBBServiceAsyncCallback);
}

function onlinePaymentBBServiceAsyncCallback(status, result) {
    var ind = "";
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] == 0) {
                dismissLoadingScreen();
                if (checkDuplicateCompCodeRef1Ref2() == true) {
                    if (checkDuplicateNicknameOnBeepAndBill() == true) {
                        addBeepAndBillerToMBNext()
                    }
                }
                return true;
            } else {
                dismissLoadingScreen();
                //ind = result["additionalDS"].length;
                frmAddTopUpToMB.txtRef1.skin = txtErrorBG;
                frmAddTopUpToMB.txtRef1.focusSkin = txtErrorBG;
                showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function validateCreditCardBB() {
    var inputparam = [];
    inputparam["cardId"] = "00000000" + removeHyphenIB(frmAddTopUpToMB.txtRef1.text);
    inputparam["waiverCode"] = "";
    inputparam["tranCode"] = TRANSCODEUN;
    inputparam["postedDt"] = getTodaysDate(); // current date you inq for smt
    inputparam["rqUUId"] = "";
    showLoadingScreen();
    var status = invokeServiceSecureAsync("creditcardDetailsInqNonSec", inputparam, validateCreditCardCallBackForBB);
}

function validateCreditCardCallBackForBB(status, result) {
    var ind;
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            //var StatusCode = result["statusCode"];
            //				/** checking for Soap status below  */
            //				
            if (result["StatusCode"] != 0) {
                dismissLoadingScreen();
                //ind = result["additionalDS"].length;
                showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreen();
                if (checkDuplicateCompCodeRef1Ref2() == true) {
                    if (checkDuplicateNicknameOnBeepAndBill() == true) {
                        addBeepAndBillerToMBNext()
                    }
                }
            }
        } else {
            dismissLoadingScreen();
            showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function validateLoanAccBB() {
    var inputParam = {};
    //for TMB Loan
    var ref1AccountIDLoan = frmAddTopUpToMB.txtRef1.text;
    inputParam["acctId"] = "0" + ref1AccountIDLoan + "001";
    inputParam["acctType"] = kony.i18n.getLocalizedString("Loan");
    inputParam["rqUUId"] = "";
    showLoadingScreen();
    invokeServiceSecureAsync("doLoanAcctInqNonSec", inputParam, validateLoanAccCallBackForBB);
}

function validateLoanAccCallBackForBB(status, result) {
    var ind = "";
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            /** checking for Soap status below  */
            var balAmount;
            if (StatusCode != "0") {
                dismissLoadingScreen();
                //ind = result["additionalDS"].length;
                showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreen();
                if (checkDuplicateCompCodeRef1Ref2() == true) {
                    if (checkDuplicateNicknameOnBeepAndBill() == true) {
                        TMBUtil.DestroyForm(frmBBConfirmAndComplete);
                        addBeepAndBillerToMBNext()
                    }
                }
            }
        } else {
            dismissLoadingScreen();
            showAlert(result["errMsg"], kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function checkDuplicateNicknameOnBeepAndBill() {
    var nickname = frmAddTopUpToMB.txbNickName.text;
    var check = "";
    var val = "";
    var len = gblCustomerBBMBInqRs.length;
    for (var i = 0; i < len; i++) {
        val = gblCustomerBBMBInqRs[i]["lblBillerNickname"];
        if (nickname == val) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
    return true;
}

function checkDuplicateCompCodeRef1Ref2() {
    //var BBList = frmMyTopUpList.segBillersList.data;
    var compCode = frmAddTopUpToMB.lblAddbillerName.text;
    var ref1 = frmAddTopUpToMB.txtRef1.text;
    var ref2 = "";
    if (frmAddTopUpToMB.hbxref2.isVisible && frmAddTopUpToMB.txtRef2.isVisible) {
        ref2 = frmAddTopUpToMB.txtRef2.text;
    } else {
        ref2 = frmAddTopUpToMB.btnRef2Dropdown.text;
    }
    //
    var custBBInqLen = gblCustomerBBMBInqRs.length;
    var tmpCompCode = "";
    var tmpRef1 = "";
    var tmpRef2 = "";
    for (var i = 0; i < custBBInqLen; i++) {
        tmpCompCode = gblCustomerBBMBInqRs[i].BillerCompCode;
        tmpRef1 = gblCustomerBBMBInqRs[i].lblRef1Value;
        if (frmAddTopUpToMB.hbxref2.isVisible) {
            tmpRef2 = gblCustomerBBMBInqRs[i].lblRef2Value;
        }
        //alert(tmpCompCode + " " + tmpRef1 + " " + tmpRef2 + " == " + gblBillerCompCodeBBMB + " " + ref1 + " " + ref2);
        //if (frmAddTopUpToMB.hbxref2.isVisible) {
        if (false) {
            if (tmpCompCode == gblBillerCompCodeBBMB && tmpRef1 == ref1 && tmpRef2 == ref2) {
                alert("Combination of CompCode Ref1 and Ref2(if applicable) should be unique");
                return false;
            }
        } else {
            if (tmpCompCode == gblBillerCompCodeBBMB && tmpRef1 == ref1) {
                alert("Combination of CompCode and Ref1 should be unique");
                return false;
            }
        }
    }
    return true;
}

function addBeepAndBillerToMBNext() {
    frmBBConfirmAndComplete.imgBB.src = frmAddTopUpToMB.imgAddedBiller.src;
    frmBBConfirmAndComplete.lblNickName.text = frmAddTopUpToMB.txbNickName.text;
    frmBBConfirmAndComplete.lblAddedRef1.text = frmAddTopUpToMB.lblAddedRef1.text;
    frmBBConfirmAndComplete.lblAddedRef2.text = frmAddTopUpToMB.lblAddedRef2.text;
    frmBBConfirmAndComplete.lblAddedRef1Val.text = frmAddTopUpToMB.txtRef1.text;
    if (frmAddTopUpToMB.txtRef2.isVisible == true) frmBBConfirmAndComplete.lblAddedRef2Val.text = frmAddTopUpToMB.txtRef2.text;
    else frmBBConfirmAndComplete.lblAddedRef2Val.text = frmAddTopUpToMB.btnRef2Dropdown.text;
    frmBBConfirmAndComplete.lblBillerCompcode.text = frmAddTopUpToMB.lblAddbillerName.text;
    frmBBConfirmAndComplete.btnRightComplete.setVisibility(false);
    frmBBConfirmAndComplete.btnRightConfirm.setVisibility(true);
    if (flowSpa) {
        gblnormSelIndex = gbltranFromSelIndex[1];
        gblmbSpaselectedData = frmAddTopUpToMB.segSliderSpa.data[gblnormSelIndex];
        frmBBConfirmAndComplete.lblAccType.text = gblmbSpaselectedData.lblSliderAccN2;
        var accNo = gblmbSpaselectedData.lblActNoval;
        accNo = removeHyphenIB(accNo);
        accNo = accNo.substring(accNo.length - 10, accNo.length);
        frmBBConfirmAndComplete.lblAccNo.text = addHyphenIB(accNo);
        frmBBConfirmAndComplete.lblAccHolderName.text = gblmbSpaselectedData.lblCustName;
        frmBBConfirmAndComplete.lblBalVal.text = gblmbSpaselectedData.lblBalance;
        frmBBConfirmAndComplete.imgBBAcc.src = gblmbSpaselectedData.img1;
    } else {
        //var segData = frmAddTopUpToMB.segSlider.data;
        //var selectedItems = frmAddTopUpToMB.segSlider.selectedIndex[1];
        frmBBConfirmAndComplete.lblAccType.text = frmAddTopUpToMB["segSlider"]["selectedItems"][0].lblSliderAccN2;
        var accNo = frmAddTopUpToMB["segSlider"]["selectedItems"][0].lblActNoval;;
        accNo = removeHyphenIB(accNo);
        accNo = accNo.substring(accNo.length - 10, accNo.length)
        frmBBConfirmAndComplete.lblAccNo.text = addHyphenIB(accNo);
        frmBBConfirmAndComplete.lblAccHolderName.text = frmAddTopUpToMB["segSlider"]["selectedItems"][0].lblCustName;
        frmBBConfirmAndComplete.lblBalVal.text = frmAddTopUpToMB["segSlider"]["selectedItems"][0].lblBalance;
        frmBBConfirmAndComplete.imgBBAcc.src = frmAddTopUpToMB["segSlider"]["selectedItems"][0].img1;
    }
    //frmAddTopUpBillerconfrmtn.segConfirmationList.setData(newSegData);
    frmBBConfirmAndComplete.btnRightComplete.containerWeight = 1;
    frmBBConfirmAndComplete.btnRightConfirm.containerWeight = 45;
    frmBBConfirmAndComplete.imgHeaderMiddle.src = "arrowtop.png"
    frmBBConfirmAndComplete.imgHeaderRight.src = "empty.png"
    frmBBConfirmAndComplete.btnRightComplete.skin = "btnShare";
    frmBBConfirmAndComplete.hbxAddDelOfflineMsg.setVisibility(false)
    frmBBConfirmAndComplete.show();
    frmBBConfirmAndComplete.hbxBalShow.isVisible = true;
    frmBBConfirmAndComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
    //frmBBConfirmAndComplete.hbxArrow.setVisibility(false)
    frmBBConfirmAndComplete.hboxSaveCamEmail.setVisibility(false);
    frmBBConfirmAndComplete.hbxIconComplete.setVisibility(false);
    frmBBConfirmAndComplete.hbxAdv.setVisibility(false);
    frmBBConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
    if (flowSpa) {
        frmBBConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmBBConfirmAndComplete.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
    } else {
        frmBBConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmBBConfirmAndComplete.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    }
    if (frmAddTopUpToMB.hbxref2.isVisible == false) {
        frmBBConfirmAndComplete.hbxRef2.setVisibility(false)
        frmAddTopUpToMB.lblAddedRef2.text = "";
    } else {
        frmBBConfirmAndComplete.hbxRef2.setVisibility(true)
    }
    var temp = frmAddTopUpToMB.lblAddbillerName.text;
    gblBillerCompCodeBBMB = temp.substring(temp.length - 5, temp.length - 1);
    //frmBBConfirmAndComplete.btnRight.skin = btnEdit;
    //TMBUtil.DestroyForm(frmAddTopUpToMB);
}

function updateAccountListBBMB() {
    isToUpdateAccListMB = true;
    callBillPaymentCustomerBBAccountService();
}

function showAccountListBBMB() {
    isToUpdateAccListMB = false;
    callBillPaymentCustomerBBAccountService();
}

function callBillPaymentCustomerBBAccountService() {
    var inputParam = {}
        //hard coding into 11 as always customer will have TMB accounts in from list
    inputParam["bankCd"] = "11";
    inputParam["rqUID"] = "";
    inputParam["crmId"] = "";
    showLoadingScreen();
    invokeServiceSecureAsync("customerBBAccountInquiry", inputParam, billPaymentCustomerBBAccountCallBack);
}

function billPaymentCustomerBBAccountCallBack(status, resulttable) {
    var selectedIndex;
    var countAcc = 0;
    if (status == 400) //success responce
    {
        if (resulttable["opstatus"] == 0) {
            /** checking for Soap status below  */
            //var StatusCode = resulttable["statusCode"];
            //
            //	if( resulttable["StatusCode"] !=null &&   resulttable["StatusCode"] == "0"){
            var fromData = []
            var j = 1
            gbltranFromSelIndex = [0, 0];
            //alert("upto 1")
            if (flowSpa) {
                gblmbSpatransflow = false;
                gblspaSelIndex = 0;
            }
            countAcc = 0;
            if (resulttable.custAcctRec == undefined) {
                resulttable.custAcctRec = [];
            }
            gblNoOfFromAcsBB = resulttable.custAcctRec.length
            TopUpNoOfAccounts = resulttable.custAcctRec.length;
            billpaymentNoOfAccounts = resulttable.custAcctRec.length;
            for (var i = 0; i < gblNoOfFromAcsBB; i++) {
                if (glb_accId == resulttable["custAcctRec"][i]["accId"]) {
                    selectedIndex = i;
                    break;
                }
            }
            //alert("upto 2")
            if (selectedIndex) gbltranFromSelIndex = [0, selectedIndex];
            else gbltranFromSelIndex = [0, 0];
            gblcustomerAccountsListBBMB = [];
            gblcustomerAccountsListBBMB = resulttable["custAcctRec"];
            if (isToUpdateAccListMB == false) {
                //alert("upto 3")
                for (var i = 0; i < resulttable.custAcctRec.length; i++) {
                    if (resulttable["custAcctRec"][i]["personalizedAcctStatus"] != "02") {
                        countAcc++;
                        //var icon = accountImageBBMB(resulttable["custAcctRec"][i]);
                        var icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["custAcctRec"][i]["ICON_ID"] + "&modIdentifier=PRODICON";
                        //
                        //fiident		
                        /*if (j == 1) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp1, icon)
                        } else if (j == 2) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp2, icon)
                        } else if (j == 3) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp3, icon)
                            j = 0;
                        }*/
                        if (j == 1) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp1, icon)
                        } else if (j == 2) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp2, icon)
                        } else if (j == 3) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp3, icon)
                        } else if (j == 4) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp4, icon)
                        } else if (j == 5) {
                            var temp = createSegmentRecordBB(resulttable.custAcctRec[i], hbxSliderTemp5, icon)
                            j = 0;
                        }
                        var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc
                        if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {} else {
                            kony.table.insert(fromData, temp[0])
                        }
                        //
                        //kony.table.insert(fromData,temp[0]);
                        j++;
                        //
                    } //if
                } //for
                //alert("upto 4")
                //flowSpa = false;
                gblNoOfFromAcsBB = countAcc;
                if (gblMyBillerTopUpBB == 2) {
                    if (flowSpa) {
                        frmAddTopUpToMB.segSliderSpa.widgetDataMap = [];
                        frmAddTopUpToMB.segSliderSpa.widgetDataMap = {
                            lblACno: "lblACno",
                            lblAcntType: "lblAcntType",
                            img1: "img1",
                            lblCustName: "lblCustName",
                            lblBalance: "lblBalance",
                            lblActNoval: "lblActNoval",
                            lblDummy: "lblDummy",
                            lblSliderAccN1: "lblSliderAccN1",
                            lblSliderAccN2: "lblSliderAccN2",
                            lblRemainFee: "lblRemainFee",
                            lblRemainFeeValue: "lblRemainFeeValue"
                        }
                        frmAddTopUpToMB.segSliderSpa.data = [];
                        frmAddTopUpToMB.segSliderSpa.data = fromData;
                        frmAddTopUpToMB.segSliderSpa.selectedIndex = gbltranFromSelIndex;
                    } else {
                        //frmAddTopUpToMB.segSlider.widgetDataMap = [];
                        frmAddTopUpToMB.segSlider.widgetDataMap = {
                            lblACno: "lblACno",
                            lblAcntType: "lblAcntType",
                            img1: "img1",
                            lblCustName: "lblCustName",
                            lblBalance: "lblBalance",
                            lblActNoval: "lblActNoval",
                            lblDummy: "lblDummy",
                            lblSliderAccN1: "lblSliderAccN1",
                            lblSliderAccN2: "lblSliderAccN2",
                            lblRemainFee: "lblRemainFee",
                            lblRemainFeeValue: "lblRemainFeeValue"
                        }
                        if (fromData.length == 1) { //If the user has single account to display in cover flow
                            frmAddTopUpToMB.segSlider.viewConfig = {
                                coverflowConfig: {
                                    projectionAngle: 60,
                                    rowItemRotationAngle: 45,
                                    spaceBetweenRowItems: 0,
                                    rowItemWidth: 80,
                                    isCircular: false
                                }
                            }
                            frmAddTopUpToMB.btnLtArrow.setVisibility(false); //Code to make the visibility of left and right to false
                            frmAddTopUpToMB.btnRtArrow.setVisibility(false);
                        } else if (fromData.length > 1) { //If the user has more than one account to display in cover flow
                            frmAddTopUpToMB.segSlider.viewConfig = {
                                coverflowConfig: {
                                    projectionAngle: 60,
                                    rowItemRotationAngle: 45,
                                    spaceBetweenRowItems: 0,
                                    rowItemWidth: 80,
                                    isCircular: true
                                }
                            }
                            frmAddTopUpToMB.btnLtArrow.setVisibility(true); //Code to make the visibility of left and right to true
                            frmAddTopUpToMB.btnRtArrow.setVisibility(true);
                        }
                        if (gblDeviceInfo["name"] == "android") {
                            if (fromData.length == 1) {
                                frmAddTopUpToMB.segSlider.viewConfig = {
                                    "coverflowConfig": {
                                        "rowItemRotationAngle": 0,
                                        "isCircular": false,
                                        "projectionAngle": 60,
                                        "rowItemWidth": 80
                                    }
                                };
                            } else {
                                frmAddTopUpToMB.segSlider.viewConfig = {
                                    "coverflowConfig": {
                                        "rowItemRotationAngle": 0,
                                        "isCircular": true,
                                        "projectionAngle": 60,
                                        "rowItemWidth": 80
                                    }
                                };
                            }
                        }
                        frmAddTopUpToMB.segSlider.data = [];
                        frmAddTopUpToMB.segSlider.data = fromData;
                        gbltranFromSelIndex = [0, 0];
                        frmAddTopUpToMB.segSlider.selectedIndex = gbltranFromSelIndex;
                    }
                }
            }
            //alert("upto 5")
            gblcustomerAccountsListBBMB = [];
            gblcustomerAccountsListBBMB = resulttable["custAcctRec"];
            if (isToUpdateAccListMB == false) {
                if (countAcc == 0) {
                    alert(kony.i18n.getLocalizedString("keyNoEligibleAct"));
                } else {
                    frmAddTopUpToMB.show();
                }
                dismissLoadingScreen();
                return;
            }
            if (executionFlowBBMB == false && isToUpdateAccListMB == true && gblToUpdateBBList == false) {
                showBeepAndBillList();
                populateMySuggestListBBMB(gblmasterBillerAndTopupBBMB);
            } else if (executionFlowBBMB == true) {
                updateBeepAndBillList();
            }
            //dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            showAlert(" " + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }
    } //status 400
}

function createSegmentRecordBB(record, hbxSliderindex, icon) {
    //var remFeeValue;
    //var remFeeLbl;
    var accId = ""
    var flag = false;
    var frmid = ""
    var remainfee = ""
    var remainfeeValue = ""
    var productName = ""
    var locale = kony.i18n.getCurrentLocale();
    var accountName = record.accountName
    if (record.accId.length == 14) accId = record.accId;
    else accId = "0000" + record.accId;
    if (accId.length == 14) {
        if (accId.charAt(0) == 0 && accId.charAt(1) == 0 && accId.charAt(2) == 0 && accId.charAt(3) == 0) {
            flag = true;
        }
        if (flag) {
            accId = accId.substr(4, 10)
        }
    }
    var noFeeFlag = record.VIEW_NAME;
    var accNickName = record.acctNickName;
    if (noFeeFlag == "PRODUCT_CODE_NOFEESAVING_TABLE") {
        remainfee = kony.i18n.getLocalizedString("keyremainingFree") + ": ";
        remainfeeValue = record.remainingFee;
    } else {
        remainfee = "";
        remainfeeValue = "";
    }
    var last4DigitAccNo = record.accId;
    last4DigitAccNo = last4DigitAccNo.substr(last4DigitAccNo.length - 4, last4DigitAccNo.length)
    if (accountName == "" || accountName == undefined) {
        accountName = record.ProductNameEng + " " + last4DigitAccNo
    }
    if (locale == "en_US") productName = record.ProductNameEng
    else productName = record.ProductNameThai
    if (accNickName == undefined || accNickName == "" || accNickName == null) {
        accNickName = productName + " " + last4DigitAccNo
    }
    var temp = [{
        lblACno: kony.i18n.getLocalizedString("AccountNo"),
        img1: icon,
        lblCustName: accNickName,
        lblBalance: commaFormatted(record.availableBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        lblActNoval: addHyphenMB(accId),
        lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
        lblSliderAccN2: productName,
        lblDummy: " ",
        tdFlag: record.accType,
        fromFIIdent: record.fiident,
        remainingFee: record.remainingFee,
        lblRemainFee: remainfee,
        lblRemainFeeValue: remainfeeValue,
        prodCode: record.productID,
        nickName: record.acctNickName,
        accountNum: addHyphenMB(accId),
        template: hbxSliderindex,
        custAcctName: record.accountName
    }]
    return temp;
}

function billerValidationsMBBB() {
    //alert(" Biller Method = " + gblBillerMethodBBMB + " " + gblBillerCompCodeBBMB);
    if (gblBillerMethodBBMB == 1) {
        onlinePaymentInqBB();
    } else if (gblBillerMethodBBMB == 2) {
        validateCreditCardBB();
    } else if (gblBillerMethodBBMB == 3) {
        validateLoanAccBB();
    } else {
        if (checkDuplicateCompCodeRef1Ref2() == true) {
            if (checkDuplicateNicknameOnBeepAndBill() == true) {
                addBeepAndBillerToMBNext()
            }
        }
    }
    return true;
}
/*function callBackCRMPUpdateForBB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            
        } else {
            
            showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }
        kony.application.dismissLoadingScreen();
    }
}*/
function invokeApplyBeenAndBill() {
    //kony.application.showLoadingScreen(frmLoading, "", d.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    var tempAccNo = frmBBConfirmAndComplete.lblAccNo.text;
    var accNo = "";
    for (var i = 0; i < tempAccNo.length; i++) {
        if (tempAccNo[i] != '-') accNo = accNo + tempAccNo[i]
    }
    accNo = accNo.substring(accNo.length - 10, accNo.length)
    inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["rqUUId"] = "";
    inputParam["BBNickname"] = frmBBConfirmAndComplete.lblNickName.text;
    inputParam["BBCompCode"] = gblBillerCompCodeBBMB;
    inputParam["Ref1"] = frmBBConfirmAndComplete.lblAddedRef1Val.text;
    if (frmBBConfirmAndComplete.hbxRef2.isVisible == false) {
        inputParam["Ref2"] = "";
    } else {
        inputParam["Ref2"] = frmBBConfirmAndComplete.lblAddedRef2Val.text;
    }
    inputParam["AcctIdentValue"] = accNo;
    //inputParam["mobileNumber"] = gblPHONENUMBER;
    inputParam["ActionFlag"] = "A";
    inputParam["MIBFlag"] = "N";
    inputParam["Channel"] = "MIB";
    //inputParam["ApplyDate"] = "2013-9-27";
    inputParam["ApplyDate"] = getTodaysDate();
    inputParam["ApplyTime"] = "155712";
    showLoadingScreen();
    invokeServiceSecureAsync("customerBBAdd", inputParam, applyBeepAndBillCallBack);
}

function applyBeepAndBillCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (result["additionalStatus"][0]["statusCode"] == 0) {
                frmBBConfirmAndComplete.hbxAdv.setVisibility(true);
                frmBBConfirmAndComplete.hbxAdv.isVisible = true;
                frmBBConfirmAndComplete.hbxArrow.setVisibility(true);
                frmBBConfirmAndComplete.imgHeaderRight.src = "empty.png";
                frmBBConfirmAndComplete.imgHeaderMiddle.src = "arrowtop.png";
                frmBBConfirmAndComplete.btnRightComplete.containerWeight = 45;
                frmBBConfirmAndComplete.btnRightConfirm.containerWeight = 1;
                frmBBConfirmAndComplete.hboxSaveCamEmail.setVisibility(false);
                frmBBConfirmAndComplete.hbxIconComplete.setVisibility(true);
                frmBBConfirmAndComplete.btnRightConfirm.setVisibility(false);
                //frmBBConfirmAndComplete.btnRightConfirm.isVisible = false;
                frmBBConfirmAndComplete.btnRightComplete.setVisibility(true);
                frmBBConfirmAndComplete.btnRightComplete.containerWeight = 16;
                //frmBBConfirmAndComplete.btnRightComplete.isVisible = true;
                frmBBConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete")
                if (flowSpa) {
                    frmBBConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyReturn");
                    frmBBConfirmAndComplete.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyApplyMore");
                } else {
                    frmBBConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
                    frmBBConfirmAndComplete.btnConfirm.text = kony.i18n.getLocalizedString("keyApplyMore");
                }
                dismissLoadingScreen();
                activityLogServiceCall("032", "", "01", "", frmBBConfirmAndComplete.lblBillerCompcode.text, frmBBConfirmAndComplete.lblAccNo.text, gblPHONENUMBER, frmBBConfirmAndComplete.lblAddedRef1Val.text, "", "");
                if (gblBillerMethodBBMB != 1) {
                    //showAlert(kony.i18n.getLocalizedString("keyBBDelAddOfflineMsg"), kony.i18n.getLocalizedString("info"));
                    frmBBConfirmAndComplete.hbxAddDelOfflineMsg.setVisibility(true);
                } else {
                    frmBBConfirmAndComplete.hbxAddDelOfflineMsg.setVisibility(false);
                }
                sendNotificationForBBApplyMB();
            } else {
                kony.application.dismissLoadingScreen();
                activityLogServiceCall("032", "", "02", "", frmBBConfirmAndComplete.lblBillerCompcode.text, frmBBConfirmAndComplete.lblAccNo.text, gblPHONENUMBER, frmBBConfirmAndComplete.lblAddedRef1Val.text, "", "");
                showAlert(result["additionalStatus"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
/*function sendNotificationForBBExecuteMB() {
    var inputparam = [];
    inputparam["channelName"] = "Mobile Banking";
    inputparam["notificationType"] = "Email"; // check this : which value we have to pass
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["emailId"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["fromAccount"] = frmBBExecuteConfirmAndComplete.lblAccountNum.text;
    // need partial masked account number, 5-9 digits unmasked
    inputparam["fromAcctNick"] = frmBBExecuteConfirmAndComplete.lblAccountName.text;
    inputparam["billerNick"] = frmBBExecuteConfirmAndComplete.lblBillerNickname.text;
    inputparam["billerNameCompCode"] = frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text;
    inputparam["ref1Label"] = frmBBExecuteConfirmAndComplete.lblRef1.text;
    inputparam["ref1Value"] = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
    if (frmBBExecuteConfirmAndComplete.hbxRef2.isVisible == true) {
        inputparam["ref2Labe1"] = frmBBExecuteConfirmAndComplete.lblRef2.text;
        inputparam["ref2Value"] = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
    } else {
        inputparam["ref2Labe1"] = "";
        inputparam["ref2Value"] = "";
    }
    inputparam["amount"] = frmBBExecuteConfirmAndComplete.lblAmountVal.text;
    inputparam["fee"] = frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text;
    inputparam["initiationDt"] = frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text;
    inputparam["todayDate"] = getTodaysDate();
    inputparam["todayTime"] = ""; // why blank ?
    inputparam["transactoinRefNum"] = frmBBExecuteConfirmAndComplete.lblTransRefNoVal.text;
    inputparam["channelID"] = "01";
    inputparam["source"] = "sendEmailForBBExecute";
    inputparam["Locale"] = kony.i18n.getCurrentLocale();
    invokeServiceSecureAsync("NotificationAdd", inputparam, sendNotificationCallBackForBB)
}*/
//For Apply
function sendNotificationForBBApplyMB() {
    var inputparam = [];
    inputparam["channelName"] = "Mobile Banking";
    inputparam["notificationType"] = "Email"; // check this : which value we have to pass
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["emailId"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["fromAccount"] = frmBBConfirmAndComplete.lblAccNo.text;
    // need partial masked account number, 5-9 digits unmasked
    inputparam["fromAcctNick"] = frmBBConfirmAndComplete.lblAccHolderName.text;
    inputparam["billerCompCode"] = frmBBConfirmAndComplete.lblBillerCompcode.text;
    inputparam["billerName"] = frmBBConfirmAndComplete.lblNickName.text;
    inputparam["ref1Label"] = frmBBConfirmAndComplete.lblAddedRef1.text;
    inputparam["ref1Value"] = frmBBConfirmAndComplete.lblAddedRef1Val.text;
    // check for ref2 exists
    if (frmAddTopUpToMB.lblAddedRef2.isVisible == true) {
        inputparam["ref2Label"] = frmBBConfirmAndComplete.lblAddedRef2.text;
        inputparam["ref2Value"] = frmBBConfirmAndComplete.lblAddedRef2Val.text;
    }
    inputparam["todayDate"] = getTodaysDate();
    inputparam["todayTime"] = ""; // why blank ?
    inputparam["channelID"] = "02";
    inputparam["source"] = "sendEmailForBBApply";
    inputparam["Locale"] = kony.i18n.getCurrentLocale();
    invokeServiceSecureAsync("NotificationAdd", inputparam, sendNotificationCallBackForBB)
}

function sendNotificationCallBackForBB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var responseData = result["notificationAddRs"];
            //if (responseData.length > 0) {
            //	
            //}
        } else {}
    } else {
        //alert("Notification Failed");
    }
}

function updateBeepAndBillList() {
    gblToUpdateBBList = true;
    inquireCustomerBeepAndBillList();
}

function showBeepAndBillList() {
    gblToUpdateBBList = false;
    inquireCustomerBeepAndBillList();
}

function inquireCustomerBeepAndBillList() {
    //kony.application.showLoadingScreen(frmLoading, "", d.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    gblBBorBillers = true;
    inputParam = {};
    tempRec = [];
    index = 0;
    inputParam["rmNum"] = "";
    showLoadingScreen();
    invokeServiceSecureAsync("customerBBInquiry", inputParam, inquireCustomerBeepAndBillCallBack);
}
tempRec = [];
index = 0;
recLength = 0;

function inquireCustomerBeepAndBillCallBack(status, result) {
    var tmpRef2BB = "";
    var tmpRef2Type = "";
    var tmpRef2Data = "";
    var labelRef1EN = "";
    var labelRef1TH = "";
    var labelRef2EN = "";
    var labelRef2TH = "";
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var statusCode = result["custBBInqRs"][0]["statusCode"];
            var severity = result["custBBInqRs"][0]["severity"];
            var statusDesc = result["custBBInqRs"][0]["statusDesc"];
            //
            //
            if (statusCode != 0) {
                kony.application.dismissLoadingScreen();
                showAlert(result["custBBInqRs"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                var i = 0;
                recLength = result["custBBInqRs"].length;
                if (recLength == 1) {
                    if (gblOnClickLocaleChangeBB == false) frmBBApplyNow.show();
                    //TMBUtil.DestroyForm(frmMyTopUpList);
                    gblCustomerBBMBInqRs = [];
                    dismissLoadingScreen();
                    gblOnClickLocaleChangeBB = false;
                } else {
                    for (i = 1; i < recLength; i++) {
                        var tempRecLocal = {};
                        tempRecLocal["lblRef1Value"] = result["custBBInqRs"][i]["ref1"];
                        tempRecLocal["lblRef2Value"] = result["custBBInqRs"][i]["ref2"];
                        tempRecLocal["lblBillerName"] = result["custBBInqRs"][i]["bbCompCode"];
                        tempRecLocal["lblBillerNickname"] = result["custBBInqRs"][i]["bbNickName"];
                        tempRecLocal["BillerCompCode"] = result["custBBInqRs"][i]["bbCompCode"];
                        tempRecLocal["hdnBillerCompcode"] = result["custBBInqRs"][i]["bbCompCode"];
                        tempRecLocal["hdnAccNo"] = result["custBBInqRs"][i]["acctIdentValue"];
                        tempRecLocal["hdnStatus"] = result["custBBInqRs"][i]["applStatus"];
                        tempRecLocal["imgBillerLogo"] = {
                                "src": BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + result["custBBInqRs"][i]["bbCompCode"] + "&modIdentifier=MyBillers"
                            }
                            //["src"] = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + result["custBBInqRs"][i]["bbCompCode"];
                        tempRecLocal["imgBillersRtArrow"] = {
                            "src": "bg_arrow_right.png"
                        }
                        for (var j = 0; j < gblmasterBillerAndTopupBBMB.length; j++) {
                            if (gblmasterBillerAndTopupBBMB[j]["BillerCompcode"] == result["custBBInqRs"][i]["bbCompCode"]) {
                                tmpRef2BB = "N";
                                tmpRef2Type = "";
                                tmpRef2Data = "";
                                if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"].length > 0) {
                                    for (var k = 0; k < gblmasterBillerAndTopupBBMB[j]["BillerMiscData"].length; k++) {
                                        if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscName"] == "BB.IsRequiredRefNumber2") {
                                            tmpRef2BB = gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscText"];
                                            //alert(result["custBBInqRs"][i]["bbCompCode"] + " " + tmpRef2BB)
                                        }
                                        if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                                            tmpRef2Type = gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                                            tmpRef2Data = gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref1.LABEL.EN") {
                                            labelRef1EN = gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.LABEL.EN") {
                                            labelRef2EN = gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref1.LABEL.TH") {
                                            labelRef1TH = gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                        if (gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.LABEL.TH") {
                                            labelRef2TH = gblmasterBillerAndTopupBBMB[j]["BillerMiscData"][k]["MiscText"];
                                        }
                                    }
                                }
                                tempRecLocal["hdnIsReqRef2Add"] = gblmasterBillerAndTopupBBMB[j]["IsRequiredRefNumber2Add"];
                                tempRecLocal["hdnIsReqRef2Pay"] = gblmasterBillerAndTopupBBMB[j]["IsRequiredRefNumber2Pay"];
                                tempRecLocal["hdnBillerMethod"] = gblmasterBillerAndTopupBBMB[j]["BillerMethod"];
                                tempRecLocal["hdnBillerCategoryID"] = gblmasterBillerAndTopupBBMB[j]["BillerCategoryID"];
                                tempRecLocal["hdnBillerNameEN"] = gblmasterBillerAndTopupBBMB[j]["BillerNameEN"];
                                tempRecLocal["hdnBillerNameTH"] = gblmasterBillerAndTopupBBMB[j]["BillerNameTH"];
                                if (labelRef1EN == "") {
                                    labelRef1EN = gblmasterBillerAndTopupBBMB[j]["LabelReferenceNumber1EN"];
                                    labelRef1TH = gblmasterBillerAndTopupBBMB[j]["LabelReferenceNumber1TH"];
                                    labelRef2EN = gblmasterBillerAndTopupBBMB[j]["LabelReferenceNumber2EN"];
                                    labelRef2TH = gblmasterBillerAndTopupBBMB[j]["LabelReferenceNumber2TH"];
                                }
                                //alert("Before " + labelRef1EN + " " + labelRef1TH + " " + labelRef2EN + " " + labelRef2TH)
                                if (isEndsWithColon(labelRef1EN) == false) labelRef1EN = labelRef1EN + ":";
                                if (isEndsWithColon(labelRef1TH) == false) labelRef1TH = labelRef1TH + ":";
                                if (isEndsWithColon(labelRef2EN) == false) labelRef2EN = labelRef2EN + ":";
                                if (isEndsWithColon(labelRef2TH) == false) labelRef2TH = labelRef2TH + ":";
                                //     alert("Before " + labelRef1EN + " " + labelRef1TH + " " + labelRef2EN + " " + labelRef2TH) 
                                tempRecLocal["hdnLabelRefNum1EN"] = labelRef1EN;
                                tempRecLocal["hdnLabelRefNum1TH"] = labelRef1TH;
                                tempRecLocal["hdnLabelRefNum2EN"] = labelRef2EN;
                                tempRecLocal["hdnLabelRefNum2TH"] = labelRef2TH;
                                tempRecLocal["hdnBillerGroupType"] = gblmasterBillerAndTopupBBMB[j]["BillerGroupType"];
                                tempRecLocal["hdnRef2BB"] = tmpRef2BB;
                                tempRecLocal["hdnRef2Type"] = tmpRef2Type;
                                tempRecLocal["hdnRef2Data"] = tmpRef2Data;
                                if (kony.i18n.getCurrentLocale() == "en_US") {
                                    tempRecLocal["lblBillerName"] = gblmasterBillerAndTopupBBMB[j]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBBMB[j]["BillerCompcode"] + ")";
                                    tempRecLocal["lblRef1"] = labelRef1EN;
                                } else {
                                    tempRecLocal["lblBillerName"] = gblmasterBillerAndTopupBBMB[j]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBBMB[j]["BillerCompcode"] + ")";
                                    tempRecLocal["lblRef1"] = labelRef1TH;
                                }
                                if (tmpRef2BB == "Y") {
                                    if (kony.i18n.getCurrentLocale() == "en_US") {
                                        tempRecLocal["lblRef2"] = labelRef2EN;
                                    } else {
                                        tempRecLocal["lblRef2"] = labelRef2TH;
                                    }
                                    tempRecLocal["hdnLabelRefNum2EN"] = labelRef2EN;
                                    tempRecLocal["hdnLabelRefNum2TH"] = labelRef2TH;
                                } else {
                                    tempRecLocal["lblRef2"] = "";
                                    tempRecLocal["hdnLabelRefNum2EN"] = "";
                                    tempRecLocal["hdnLabelRefNum2TH"] = "";
                                }
                                break;
                            }
                        }
                        tempRec.push(tempRecLocal);
                        showLoadingScreen();
                        //fetchMasterBillerForBB(result["custBBInqRs"][i]["bbCompCode"]);
                    }
                    frmMyTopUpList.lblSuggestedBillersText.setVisibility(true)
                    frmMyTopUpList.segBillersList.removeAll();
                    //frmIBBeepAndBillList.segBillersList.setData(tempRec);
                    gblCustomerBBMBInqRs = [];
                    gblCustomerBBMBInqRs = tempRec;
                    gblBBMoreMB = 0;
                    gblBillersForSearchMB = [];
                    gblBillersForSearchMB = tempRec;
                    if (executionFlowBBMB == false && gblToUpdateBBList == false) {
                        onMyBBSelectBillersMoreDynamicMB();
                        frmMyTopUpList.show();
                    } else if (executionFlowBBMB == true) {
                        getBBRefrenceNoMB();
                    }
                    dismissLoadingScreen();
                }
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function onMyBBSelectBillersMoreDynamicMB() {
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblCustTopUpMore to max length
    gblBBMoreMB = gblBBMoreMB + maxLength;
    var newData = [];
    var totalData = gblBillersForSearchMB; //data from static population 
    var totalLength = totalData.length;
    if (totalLength == 0) {
        frmMyTopUpList.lblMyBillers.setVisibility(false);
    } else {
        frmMyTopUpList.lblMyBillers.setVisibility(true);
    }
    if (totalLength > gblBBMoreMB) {
        frmMyTopUpList.hbxMoreBB.setVisibility(true);
        var endPoint = gblBBMoreMB;
    } else {
        frmMyTopUpList.hbxMoreBB.setVisibility(false);
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(gblBillersForSearchMB[i])
    }
    frmMyTopUpList.segBillersList.setData(newData);
}

function customerPaymentStatusInquiryForBBPushMsg(Obj, notificationHistroy) {
    executionFlowBBMB = true;
    //updateAccountListBBMB();
    //
    messageObj["pinCode"] = Obj["Pincode"];
    messageObj["dueDate"] = Obj["Expriedate"];
    messageObj["toAcctNickname"] = Obj["BBNickname"];
    messageObj["ref1"] = Obj["Reference1"];
    messageObj["finTxnAmount"] = Obj["Amount"];
    messageObj["txnFeeAmount"] = Obj["FeeAmount"];
    messageObj["BBCompcode"] = Obj["cmpCode"];
    //messageObj["finTxnRefID"] = result["finTxnRefID"];
    if (messageObj["ref2"] != undefined) {
        messageObj["ref2"] = Obj["ref2"];
    } else {
        messageObj["ref2"] = "";
    }
    frmBBExecuteConfirmAndComplete.lblAmountVal.text = messageObj["finTxnAmount"] + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text = messageObj["txnFeeAmount"] + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmBBExecuteConfirmAndComplete.lblTransIDVal.text = messageObj["pinCode"];
    //frmBBExecuteConfirmAndComplete.lblFeeVal.text = feeAmount;
    var date = messageObj["dueDate"];
    var arrDate = date.split("-")
    frmBBExecuteConfirmAndComplete.lblDueDateVal.text = arrDate[2] + '/' + arrDate[1] + '/' + arrDate[0];
    var inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["PIN"] = messageObj["pinCode"];
    inputParam["finTxnAmount"] = messageObj["finTxnAmount"];
    inputParam["ref1"] = messageObj["ref1"];
    //inputParam["BillerName"] = getBillerNameMB(messageObj["BBCompcode"]);
    showLoadingScreen();
    if (gblmasterBillerAndTopupBBMB.length == 0) {
        gblUpdateMasterData = true;
        inquireMasterBillerAndTopupMB();
    } else {
        inputParam["BillerName"] = getBillerNameMB(messageObj["BBCompcode"]);
        invokeServiceSecureAsync("CustomerBBPaymentStatusInquiry", inputParam, customerBBPayemntStatusInquiryMBCallBack);
    }
}

function getBillerNameMB(compCode) {
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        if (compCode == gblmasterBillerAndTopupBBMB[i]["BillerCompcode"]) {
            if (kony.i18n.getCurrentLocale() == "en_US") return gblmasterBillerAndTopupBBMB[i]["BillerNameEN"];
            else return gblmasterBillerAndTopupBBMB[i]["BillerNameTH"];
        }
    }
}

function populateOnExecuteBBPage() {
    changeStatusBarColor();
    //alert("inside populateOnExecuteBBPage");
    var bbListength = gblCustomerBBMBInqRs.length;
    var tempFlag = false;
    for (var i = 0; i < bbListength; i++) {
        var ref2Flag = true;
        if (messageObj["ref2"] == "") {
            ref2Flag = false;
        } else {
            ref2Flag = true;
        }
        var flag = true;
        if (ref2Flag == true) {
            flag = (gblCustomerBBMBInqRs[i]["lblRef1Value"] == messageObj["ref1"] && gblCustomerBBMBInqRs[i]["lblRef2Value"] == messageObj["ref2"])
        } else {
            flag = (gblCustomerBBMBInqRs[i]["lblRef1Value"] == messageObj["ref1"]);
        }
        if (gblCustomerBBMBInqRs[i]["hdnBillerCompcode"] == messageObj["BBCompcode"] && flag) {
            //
            //
            //alert("match found at : " + gblCustomerBBMBInqRs[i]["lblBillerNickname"] + " " +gblCustomerBBMBInqRs[i]["hdnRef2BB"]);
            frmBBExecuteConfirmAndComplete.lblRef2.text = gblCustomerBBMBInqRs[i]["lblRef2"];
            frmBBExecuteConfirmAndComplete.lblRef2Value.text = gblCustomerBBMBInqRs[i]["lblRef2Value"];
            frmBBExecuteConfirmAndComplete.lblRef1.text = gblCustomerBBMBInqRs[i]["lblRef1"];
            frmBBExecuteConfirmAndComplete.lblRef1Value.text = gblCustomerBBMBInqRs[i]["lblRef1Value"];
            gblBillerCompCodeBBMB = gblCustomerBBMBInqRs[i]["hdnBillerCompcode"];
            frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text = gblCustomerBBMBInqRs[i]["lblBillerName"];
            var accNo = gblCustomerBBMBInqRs[i]["hdnAccNo"];
            accNo = accNo.substring(accNo.length - 10, accNo.length);
            frmBBExecuteConfirmAndComplete.lblAccountNum.text = addHyphenMB(accNo);
            frmBBExecuteConfirmAndComplete.imgBillerPic.src = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblCustomerBBMBInqRs[i]["BillerCompCode"] + "&modIdentifier=MyBillers"
            frmBBExecuteConfirmAndComplete.lblBillerNickname.text = gblCustomerBBMBInqRs[i]["lblBillerNickname"];
            if (gblCustomerBBMBInqRs[i]["hdnRef2BB"] == "N") {
                frmBBExecuteConfirmAndComplete.hbxRef2.setVisibility(false);
            } else {
                frmBBExecuteConfirmAndComplete.hbxRef2.setVisibility(true);
            }
            for (var j = 0; j < gblcustomerAccountsListBBMB.length; j++) {
                var accId = removeHyphenIB(gblcustomerAccountsListBBMB[j]["accId"]);
                accId = accId.substring(accId.length - 10, accId.length);
                if (accId == removeHyphenIB(frmBBExecuteConfirmAndComplete.lblAccountNum.text)) {
                    tempFlag = true;
                    if (kony.i18n.getCurrentLocale() == "en_US") {
                        frmBBExecuteConfirmAndComplete.lblProductName.text = gblcustomerAccountsListBBMB[j]["ProductNameEng"];
                    } else {
                        frmBBExecuteConfirmAndComplete.lblProductName.text = gblcustomerAccountsListBBMB[j]["ProductNameThai"];
                    }
                    frmBBExecuteConfirmAndComplete.lblAccountName.text = gblcustomerAccountsListBBMB[j]["accountName"];
                    var accNo = gblcustomerAccountsListBBMB[j]["accId"];
                    accNo = accNo.substring(accNo.length - 10, accNo.length);
                    frmBBExecuteConfirmAndComplete.lblAccountNum.text = addHyphenMB(accNo);
                    var balance = gblcustomerAccountsListBBMB[j]["availableBal"];
                    frmBBExecuteConfirmAndComplete.lblBalBeforePayValue.text = commaFormattedOpenAct(balance) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmBBExecuteConfirmAndComplete.lblBalBeforePay.text = kony.i18n.getLocalizedString("keyBalanceBeforePayment");
                    //frmBBExecuteConfirmAndComplete.hbxAccountDetails.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.hbxAccountDetails.setVisibility(true);
                    frmBBExecuteConfirmAndComplete.lblFrom.setVisibility(true);
                    if (flowSpa) {
                        frmBBExecuteConfirmAndComplete.btnConfirmSpa.setEnabled(true);
                        frmBBExecuteConfirmAndComplete.btnConfirmSpa.skin = btnBlueSkin;
                    } else {
                        frmBBExecuteConfirmAndComplete.btnConfirm.setEnabled(true);
                        frmBBExecuteConfirmAndComplete.btnConfirm.skin = btnBlueSkin;
                    }
                    frmBBExecuteConfirmAndComplete.hbxHiddenAcc.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.imgFromAccount.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblcustomerAccountsListBBMB[0]["ICON_ID"] + "&modIdentifier=PRODICON";
                    if ((gblPmtStatusMB == "p" || gblPmtStatusMB == "P") && gblcustomerAccountsListBBMB[j]["personalizedAcctStatus"] == "02") {
                        if (flowSpa) {
                            frmBBExecuteConfirmAndComplete.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") + "<br>" + kony.i18n.getLocalizedString("S2S_DelAcctMsg2") + "</br>" + "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + "<br>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans") + "</br>";
                        } else {
                            frmBBExecuteConfirmAndComplete.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") + "" + kony.i18n.getLocalizedString("S2S_DelAcctMsg2") + " " + "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + " " + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
                        }
                        //frmBBExecuteConfirmAndComplete.hbxHiddenAcc.setVisibility(true);
                        frmBBExecuteConfirmAndComplete.hbxAccountDetails.setVisibility(false);
                        //frmBBExecuteConfirmAndComplete.lblFrom.setVisibility(false);
                        frmBBExecuteConfirmAndComplete.hbxHiddenAcc.setVisibility(true);
                        if (flowSpa) {
                            frmBBExecuteConfirmAndComplete.btnConfirmSpa.setEnabled(false);
                            frmBBExecuteConfirmAndComplete.btnConfirmSpa.skin = btnDisabledGray;
                        } else {
                            frmBBExecuteConfirmAndComplete.btnConfirm.setEnabled(false);
                            frmBBExecuteConfirmAndComplete.btnConfirm.skin = btnDisabledGray;
                        }
                    }
                    break;
                }
            }
            if (tempFlag == false) {
                if (flowSpa) {
                    frmBBExecuteConfirmAndComplete.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") + "<br>" + kony.i18n.getLocalizedString("S2S_DelAcctMsg2") + "</br>" + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + "<br>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans") + "</br>";
                } else {
                    frmBBExecuteConfirmAndComplete.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") + "" + kony.i18n.getLocalizedString("S2S_DelAcctMsg2") + " " + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + " " + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
                }
                //frmBBExecuteConfirmAndComplete.hbxHiddenAcc.setVisibility(true);
                frmBBExecuteConfirmAndComplete.hbxAccountDetails.setVisibility(false);
                //frmBBExecuteConfirmAndComplete.lblFrom.setVisibility(false);
                frmBBExecuteConfirmAndComplete.hbxHiddenAcc.setVisibility(true);
                if (flowSpa) {
                    frmBBExecuteConfirmAndComplete.btnConfirmSpa.setEnabled(false);
                    frmBBExecuteConfirmAndComplete.btnConfirmSpa.skin = btnDisabledGray;
                } else {
                    frmBBExecuteConfirmAndComplete.btnConfirm.setEnabled(false);
                    frmBBExecuteConfirmAndComplete.btnConfirm.skin = btnDisabledGray;
                }
            }
            frmBBExecuteConfirmAndComplete.show();
            if (gblPmtStatusMB == "S" || gblPmtStatusMB == "s") {
                showAlert(kony.i18n.getLocalizedString("keyBBExAlreadyPaidMsg"), kony.i18n.getLocalizedString("info"));
            }
            if (isRefreshCalenderMB == true) {
                //frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(true);
                frmBBExecuteConfirmAndComplete.hbxAdvertisement.setVisibility(false);
                if (flowSpa) {
                    frmBBExecuteConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
                    frmBBExecuteConfirmAndComplete.btnCancelSpa.setVisibility(true);
                } else {
                    frmBBExecuteConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
                    frmBBExecuteConfirmAndComplete.btnCancel.setVisibility(true);
                }
                frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
            }
            return true;
        }
    }
    return false;
}

function onclickOfSuggestedBillersAddBB() {
    //var isBarCode = frmBBList.segSuggestedBillers.selectedItems[0].BarcodeOnly; 	
    var isBarCode = "0";
    if (kony.string.equalsIgnoreCase(isBarCode, "1")) {
        //alert("Cannot Add Biller - Only for Barcode");
    } else {
        gblBillerCompCodeBBMB = "";
        gblBillerIdMB = "";
        gblBillerMethodBBMB = "";
        gblRef2FlagMB = "";
        gblRef1LenMB = "";
        gblRef2LenMB = "";
        gblIsRef2RequiredMB = "";
        gblIsRef2RequiredMB = frmBBList.segSuggestedBillers.selectedItems[0].IsRequiredRefNumber2Add.text;
        gblBillerIdMB = frmBBList.segSuggestedBillers.selectedItems[0].BillerID.text;
        gblBillerMethodBBMB = frmBBList.segSuggestedBillers.selectedItems[0].BillerMethod.text;
        gblEffDtMB = frmBBList.segSuggestedBillers.selectedItems[0].EffDt.text;
        gblBillerCompCodeBBMB = frmBBList.segSuggestedBillers.selectedItems[0].BillerCompCode.text;
        gblRef1LenMB = frmBBList.segSuggestedBillers.selectedItems[0].Ref1Len.text;
        frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblRef1LenMB);
        frmAddTopUpToMB.lblAddedRef1.text = frmBBList.segSuggestedBillers.selectedItems[0].Ref1Label.text;
        //if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "N")) 
        if (gblIsRef2RequiredMB == "N") {
            frmAddTopUpToMB.lblAddedRef2.setVisibility(false);
            frmAddTopUpToMB.txtRef2.setVisibility(false);
            gblRef2FlagMB = false;
            gblRef2LenMB = 0;
            //else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "Y"))
        } else if (gblIsRef2RequiredMB == "Y") {
            gblRef2FlagMB = true;
            frmAddTopUpToMB.lblAddedRef2.text = frmBBList.segSuggestedBillers.selectedItems[0].Ref2Label.text;
            gblRef2LenMB = frmBBList.segSuggestedBillers.selectedItems[0].Ref2Len.text;
        }
        var indexOfSelectedRow = frmBBList.segSuggestedBillers.selectedIndex[1];
        var indexOfSelectedIndex = frmBBList.segSuggestedBillers.selectedItems[0];
        frmAddTopUpToMB.imgAddedBiller.src = indexOfSelectedIndex.imgSuggestedBiller.src;
        frmAddTopUpToMB.lblAddbillerName.text = indexOfSelectedIndex.lblSuggestedBiller.text;
        frmAddTopUpToMB.show();
    }
}

function onSelectSugBillerBB() {
    if (checkMBUserStatus()) {
        //var isBarCode = frmMyTopUpSelect.segSelectList.selectedItems[0].BarcodeOnly; 	
        //
        gblBillerCompCodeBBMB = "";
        gblBillerIdMB = "";
        gblBillerMethodBBMB = "";
        gblRef2FlagMB = "";
        gblRef1LenMB = "";
        gblRef2LenMB = "";
        gblIsRef2RequiredMB = "";
        var indexOfSelectedIndex = frmMyTopUpList.segSuggestedBillers.selectedItems[0];
        gblIsRef2RequiredMB = indexOfSelectedIndex.IsRequiredRefNumber2Pay.text;
        gblBillerIdMB = indexOfSelectedIndex.BillerID.text;
        gblBillerMethodBBMB = indexOfSelectedIndex.BillerMethod.text;
        gblEffDtMB = indexOfSelectedIndex.EffDt.text;
        gblBillerCompCodeBBMB = indexOfSelectedIndex.BillerCompCode.text;
        //alert("global comp code" + gblBillerCompCodeBBMB);
        gblRef1LenMB = indexOfSelectedIndex.Ref1Len.text;
        //alert("Biller ref1 Length MB---" + gblRef1LenMB);
        frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblRef1LenMB);
        frmAddTopUpToMB.lblAddedRef1.text = indexOfSelectedIndex.Ref1Label.text;
        //alert("Value of ref1 rcvd from suggested biller" + frmAddTopUpToMB.lblAddedRef1.text);
        //alert("ref2 required- " + indexOfSelectedIndex.IsRequiredRefNumber2Add.text);
        //if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "N")) 
        if (indexOfSelectedIndex.hdnRef2BB.text == "N" || indexOfSelectedIndex.hdnRef2BB.text == "n") {
            frmAddTopUpToMB.hbxref2.setVisibility(false);
            frmAddTopUpToMB.lineRef2.isVisible = false;
            gblRef2FlagMB = false;
            gblRef2LenMB = 0;
            // else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "Y"))
        } else if (indexOfSelectedIndex.hdnRef2BB.text == "Y" || indexOfSelectedIndex.hdnRef2BB.text == "y") {
            if (indexOfSelectedIndex.hdnRef2Type.text == "Dropdown") {
                frmAddTopUpToMB.txtRef2.setVisibility(false);
                frmAddTopUpToMB.btnRef2Dropdown.setVisibility(true);
                frmAddTopUpToMB.btnRef2Dropdown.text = kony.i18n.getLocalizedString("keySelectRef2BB");
                gblRef2masterData = indexOfSelectedIndex.hdnRef2Data.text;
            } else {
                frmAddTopUpToMB.txtRef2.setVisibility(true);
                frmAddTopUpToMB.btnRef2Dropdown.setVisibility(false);
                gblRef2masterData = "";
            }
            frmAddTopUpToMB.hbxref2.setVisibility(true);
            frmAddTopUpToMB.lineRef2.isVisible = true;
            gblRef2FlagMB = true;
            frmAddTopUpToMB.lblAddedRef2.text = indexOfSelectedIndex.Ref2Label.text;
            //alert("Value of ref2 rcvd from suggested biller" + frmAddTopUpToMB.lblAddedRef2.text);
            gblRef2LenMB = indexOfSelectedIndex.Ref2Len.text;
            frmAddTopUpToMB.txtRef2.maxTextLength = parseInt(gblRef2LenMB);
            //alert("Biller ref2 Length MB---" + gblRef2LenMB);
        }
        frmAddTopUpToMB.imgAddedBiller.src = indexOfSelectedIndex.imgSuggestedBiller.src;
        frmAddTopUpToMB.lblAddbillerName.text = indexOfSelectedIndex.lblSuggestedBiller.text;
        //alert(indexOfSelectedIndex.Ref1Len.text + " " + indexOfSelectedIndex.Ref2Len.text)
        //frmAddTopUpToMB.txtRef1.maxTextLength = indexOfSelectedIndex.Ref1Len.text;
        //frmAddTopUpToMB.txtRef2.maxTextLength = indexOfSelectedIndex.Ref2Len.text;
        frmAddTopUpToMB.txbNickName.text = "";
        frmAddTopUpToMB.txtRef1.text = "";
        frmAddTopUpToMB.txtRef2.text = "";
        //frmAddTopUpToMB.lblAddedRef1 = indexOfSelectedIndex.Ref1Label;
        //frmAddTopUpToMB.lblAddedRef2 = indexOfSelectedIndex.Ref1Labe2;
        //showAccountListBBMB();
    }
}

function onSelectBillerBB() {
    var isBarCode = frmMyTopUpSelect.segSelectList.selectedItems[0].BarcodeOnly;
    isBarCode = "0";
    if (kony.string.equalsIgnoreCase(isBarCode, "1")) {
        alert("Cannot Add Biller - Only for Barcode");
    } else {
        gblBillerCompCodeBBMB = "";
        gblBillerIdMB = "";
        gblBillerMethodBBMB = "";
        gblRef2FlagMB = "";
        gblRef1LenMB = "";
        gblRef2LenMB = "";
        gblIsRef2RequiredMB = "";
        var indexOfSelectedIndex = frmMyTopUpSelect.segSelectList.selectedItems[0];
        gblIsRef2RequiredMB = indexOfSelectedIndex.IsRequiredRefNumber2Add.text;
        gblBillerIdMB = indexOfSelectedIndex.BillerID.text;
        gblBillerMethodBBMB = indexOfSelectedIndex.BillerMethod.text;
        gblEffDtMB = indexOfSelectedIndex.EffDt.text;
        gblBillerCompCodeBBMB = indexOfSelectedIndex.BillerCompCode.text;
        gblRef1LenMB = indexOfSelectedIndex.Ref1Len.text;
        frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblRef1LenMB);
        frmAddTopUpToMB.lblAddedRef1.text = appendColon(indexOfSelectedIndex.Ref1Label.text);
        gblIsRef2RequiredMB = gblIsRef2RequiredMB + "";
        if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "N")) {
            frmAddTopUpToMB.lblAddedRef2.setVisibility(false);
            frmAddTopUpToMB.txtRef2.setVisibility(false);
            gblRef2FlagMB = false;
            gblRef2LenMB = 0;
        } else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "Y")) {
            gblRef2FlagMB = true;
            frmAddTopUpToMB.lblAddedRef2.text = appendColon(indexOfSelectedIndex.Ref2Label.text);
            gblRef2LenMB = indexOfSelectedIndex.Ref2Len.text;
        }
        frmAddTopUpToMB.imgAddedBiller.src = indexOfSelectedIndex.imgSuggestedBiller.src;
        frmAddTopUpToMB.lblAddbillerName.text = indexOfSelectedIndex.lblSuggestedBiller;
        //frmAddTopUpToMB.lblAddedRef1.text = indexOfSelectedIndex.Ref1Label;
        //frmAddTopUpToMB.lblAddedRef2.text = indexOfSelectedIndex.Ref1Labe2;
        frmAddTopUpToMB.show();
    }
}

function addBBToList() {
    var editedData = [{
        imgBillerLogo: frmBBConfirmAndComplete.imgBB.src,
        lblBillerNickname: frmBBConfirmAndComplete.lblNickName.text,
        lblRef1: frmBBConfirmAndComplete.lblAddedRef1.text,
        lblRef2: frmBBConfirmAndComplete.lblAddedRef2.text,
        lblRef1Value: frmBBConfirmAndComplete.lblAddedRef1Val.text,
        lblRef2Value: frmBBConfirmAndComplete.lblAddedRef2Val.text,
        hdnimgBBAcc: frmBBConfirmAndComplete.imgBBAcc.src,
        hdnlblAccType: frmBBConfirmAndComplete.lblAccType.text,
        hdnlblAccNo: frmBBConfirmAndComplete.lblAccNo.text,
        hdnlblAccHolderName: frmBBConfirmAndComplete.lblAccHolderName.text,
        hdnlblBalVal: frmBBConfirmAndComplete.lblBalVal.text
    }];
    frmMyTopUpList.segBillersList.addAll(editedData);
}

function deleteBeepAndBillCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var statusCode = result["additionalStatus"][0]["statusCode"];
            var severity = result["additionalStatus"][0]["severity"];
            var statusDesc = result["additionalStatus"][0]["statusDesc"];
            //activityLogServiceCall("033", "", "01", "", frmBBMyBeepAndBill.lblBillerNickName.text,
            //   frmBBMyBeepAndBill.lblAccNo.text, gblPHONENUMBER, frmBBMyBeepAndBill.lblRef1Val.text, "", "");
            //call Notification Add
            if (gblBillerMethodBBMB != 1) {
                showAlert(kony.i18n.getLocalizedString("keyBBDelAddOfflineMsg"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
                frmMyTopUpList.show();
            } else {
                onSelectBBSubMenuMB();
            }
        } else {
            kony.application.dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
            //showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function commonValidationsApplyBBNextMB() {
    if (frmAddTopUpToMB.lblAddbillerName.text == null) {
        showAlert(kony.i18n.getLocalizedString("KeyPlzSelBiller"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (frmAddTopUpToMB.txbNickName.text == "" || frmAddTopUpToMB.txbNickName.text == null) {
        frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
        frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    if (frmAddTopUpToMB.txbNickName.text.length > 20) {
        frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
        frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keyMaxNickNameChars"), kony.i18n.getLocalizedString("info"))
        return false;
    }
    var isValidNickName = benNameAlpNumValidation(frmAddTopUpToMB.txbNickName.text);
    var isValidRef1 = validateRef1ValueMB(frmAddTopUpToMB.txtRef1.text);
    var isValidRef2 = "";
    if (frmAddTopUpToMB.txtRef2.isVisible == true) isValidRef2 = validateRef2ValueAll(frmAddTopUpToMB.txtRef2.text);
    else isValidRef2 = validateRef2ValueAll(frmAddTopUpToMB.btnRef2Dropdown.text);
    if (isValidNickName == false) {
        frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
        frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keyincorrectNickName"), kony.i18n.getLocalizedString("info"));
        return false;
    } else if (isValidRef1 == false || frmAddTopUpToMB.txtRef1.text == null) {
        showAlert(kony.i18n.getLocalizedString("keyWrngRef1Val") + " " + frmAddTopUpToMB.lblAddedRef1.text, kony.i18n.getLocalizedString("info"));
        return false;
    } else if (frmAddTopUpToMB.hbxref2.isVisible == true) {
        if (isValidRef2 == false) {
            showAlert(kony.i18n.getLocalizedString("keyWrngRef2Val") + " " + frmAddTopUpToMB.lblAddedRef2.text, kony.i18n.getLocalizedString("info"));
            return false;
        } else if (frmAddTopUpToMB.btnRef2Dropdown.isVisible == true && (kony.i18n.getLocalizedString("keySelectRef2BB") == frmAddTopUpToMB.btnRef2Dropdown.text)) {
            showAlert(kony.i18n.getLocalizedString("keyWrngRef2Val") + " " + frmAddTopUpToMB.lblAddedRef2.text, kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
    return true;
}

function onClickNextMBApplyBB() {
    var inputParam = [];
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, onClickApplyNextBBMBTokenSwitchCallBack);
}

function onClickApplyNextBBMBTokenSwitchCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (frmAddTopUpToMB.lblAddbillerName.text == "") {
                alert(kony.i18n.getLocalizedString("Valid_SelBiller"));
                dismissLoadingScreen();
                return;
            }
            if (commonValidationsApplyBBNextMB() == true) {
                callBillerValidationBillPayServiceBBMB();
            }
            dismissLoadingScreen();
        }
    }
}

function callBillerValidationBillPayServiceBBMB() {
    inputParam = {};
    //inputParam["BillerNickName"] = billerNickname;
    inputParam["compCode"] = gblBillerCompCodeBBMB;
    inputParam["BBNickname"] = frmAddTopUpToMB.txbNickName.text;
    inputParam["ref1"] = frmAddTopUpToMB.txtRef1.text;
    var isRef2Exist = "";
    if (frmAddTopUpToMB.hbxref2.isVisible) {
        isRef2Exist = "Y";
        if (frmAddTopUpToMB.txtRef2.isVisible) inputParam["ref2"] = frmAddTopUpToMB.txtRef2.text;
        else inputParam["ref2"] = frmAddTopUpToMB.btnRef2Dropdown.text;
    } else {
        isRef2Exist = "N";
        inputParam["ref2"] = "";
    }
    inputParam["isRef2Exist"] = isRef2Exist;
    //inputParam["Amount"] = billAmountBillPay;
    //inputParam["ScheduleDate"] = scheduleDtBillPay;
    //inputParam["ModuleName"] = "BillAdd";
    invokeServiceSecureAsync("beepAndBillerValidation", inputParam, callBillerValidationBillPayServiceCallBackBBMB);
}

function callBillerValidationBillPayServiceCallBackBBMB(status, result) {
    if (status == 400) //success responce
    {
        dismissLoadingScreen();
        var validationBillPayMBFlag = "";
        if (result["opstatus"] == 0) {
            validationBillPayMBFlag = result["validationResult"];
        } else {
            dismissLoadingScreen();
            validationBillPayMBFlag = result["validationResult"];
        }
        if (validationBillPayMBFlag == "true") {
            billerValidationsMBBB();
        } else {
            alert(kony.i18n.getLocalizedString("keyBBValidationFail"));
            //billerValidationsMBBB();
            return false;
        }
    }
}

function toSetRef1Ref2MB() {
    var tmpRef2BB = "N";
    var tmpRef2Type = "";
    var tmpRef2Data = "";
    //alert(collectiondata[i]["BillerMiscData"].length + " collection data")
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        if (gblBillerCompCodeBBMB == gblmasterBillerAndTopupBBMB[i]["BillerCompcode"]) {
            if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"].length > 0) {
                for (var j = 0; j < gblmasterBillerAndTopupBBMB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.IsRequiredRefNumber2") {
                        tmpRef2BB = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                        tmpRef2Type = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                        tmpRef2Data = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"];
                    }
                }
            }
        }
    }
    if (tmpRef2BB == "Y" || tmpRef2BB == "y") {
        frmAddTopUpToMB.hbxref2.setVisibility(true);
        if (tmpRef2Type == "Dropdown") {
            frmAddTopUpToMB.txtRef2.setVisibility(false);
            frmAddTopUpToMB.btnRef2Dropdown.setVisibility(true);
            frmAddTopUpToMB.btnRef2Dropdown.text = kony.i18n.getLocalizedString("keySelectRef2BB");
            gblRef2masterData = tmpRef2Data;
        } else {
            frmAddTopUpToMB.txtRef2.setVisibility(true);
            frmAddTopUpToMB.btnRef2Dropdown.setVisibility(false);
        }
        //frmIBBeepAndBillApplyCW.txtAddBillerRef2.text = currForm.segSuggestedBillersList.selectedItems[0].hdnRef2Data.text;
    } else {
        frmAddTopUpToMB.hbxref2.setVisibility(false);
    }
}

function onClickBillersApplyBBLink() {
    gblMyBillerTopUpBB = 2;
    frmAddTopUpToMB.imgAddedBiller.src = frmViewTopUpBiller.imgTopUPBiller.src;
    frmAddTopUpToMB.lblAddbillerName.text = frmViewTopUpBiller.lblCnfrmBillerNameComp.text;
    var str = frmViewTopUpBiller.lblCnfrmBillerNameComp.text;
    gblBillerCompCodeBBMB = str.substring(str.length - 5, str.length - 1);
    frmAddTopUpToMB.txbNickName.text = frmViewTopUpBiller.lblCnfrmNickName.text;
    frmAddTopUpToMB.lblAddedRef1.text = frmViewTopUpBiller.lblConfrmRef1.text;
    frmAddTopUpToMB.lblAddedRef2.text = frmViewTopUpBiller.lblConfrmRef2.text;
    frmAddTopUpToMB.txtRef1.text = frmViewTopUpBiller.lblcnfrmRef1Value.text;
    frmAddTopUpToMB.txtRef2.text = frmViewTopUpBiller.lblcnfrmRef2Value.text;
    if (gblmasterBillerAndTopupBBMB.length == 0) {
        updateMasterBillerAndTopupMB();
    } else {
        //toSetRef1Ref2MB();
        showAccountListBBMB();
    }
}

function updateMasterBillerAndTopupMB() {
    var inputParams = {
        IsActive: "1",
        BillerGroupType: "0"
    };
    inputParams["flagBillerList"] = "BB";
    showLoadingScreen();
    gblmasterBillerAndTopupBBMB = [];
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, updateMasterBillerForBBMB);
}

function updateMasterBillerForBBMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0 || callBackResponse["opstatus"] == "0") {
            if (callBackResponse["StatusCode"] == 0 || callBackResponse["StatusCode"] == "0") {
                var length = callBackResponse["MasterBillerInqRs"].length;
                //alert(length + "  topups length");	
                for (var i = 0; i < length; i++) {
                    var noOfChannels = callBackResponse["MasterBillerInqRs"][i]["ValidChannel"].length;
                    for (var j = 0; j < noOfChannels; j++) {
                        if (callBackResponse["MasterBillerInqRs"][i]["ValidChannel"][j]["ChannelCode"] == "51") {
                            gblmasterBillerAndTopupBBMB.push(callBackResponse["MasterBillerInqRs"][i]);
                            break;
                        }
                    }
                }
            }
            for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
                if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"] + ":";
                if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"] + ":";
                if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"] + ":";
                if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"]) == false) gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"] = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"] + ":";
                for (var j = 0; j < gblmasterBillerAndTopupBBMB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                        if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                        if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                        if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                        if (isEndsWithColon(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]) == false) gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"] + ":";
                    }
                }
            }
            populateBillerCategory();
            toSetRef1Ref2MB();
            showAccountListBBMB();
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function searchSugBillerMBBB() {
    gblTopUpMore = 0;
    gblsearchtxt = "";
    if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
        gblsearchtxt = frmMyTopUpList.txbSearch.text;
        if (gblsearchtxt.length >= 3) {
            //alert("gblsearchtxt : " + gblsearchtxt);
            isSearched = true;
            searchFlagForLoadMore = true;
            getMySuggestBillListMBBB();
            frmMyTopUpList.segSuggestedBillers.setVisibility(true);
        } else {
            searchFlagForLoadMore = false;
            frmMyTopUpList.segSuggestedBillers.setVisibility(false);
        }
    } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect') {
        gblsearchtxt = frmMyTopUpSelect.txbSearch.text;
        if (gblsearchtxt.length >= 3) {
            isSearched = true;
            getMySuggestBillListMBBB();
        }
    }
}

function getMySuggestBillListMBBB() {
    if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
        frmMyTopUpList.segSuggestedBillers.removeAll();
        //frmMyTopUpList.segBillersList.removeAll();
    } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect') {
        frmMyTopUpSelect.segSelectList.removeAll();
    }
    if (isSearched) {
        var inputParams = {
            IsActive: "1",
            billerName: gblsearchtxt
        };
    } else {
        var inputParams = {
            IsActive: "1"
        };
    }
    inputParams["flagBillerList"] = "BB";
    showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMySuggestBillListMBBBServiceAsyncCallback);
}

function startMySuggestBillListMBBBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                //	populateMyTopUpSuggestMB(callBackResponse["MasterBillerInqRs"]);
                if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
                    populateMySuggestListBBMB(callBackResponse["MasterBillerInqRs"]);
                } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect') {
                    populateMySuggestListBBMB(callBackResponse["MasterBillerInqRs"]);
                }
                if (isfirstCallToMaster)
                //	getMyTopUpListMB();
                    isfirstCallToMaster = false;
                isSearched = false;
            } else {
                if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
                    frmMyTopUpList.lblSuggestedBillersText.setVisibility(false);
                    frmMyTopUpList.segSuggestedBillers.removeAll();
                } else if (kony.application.getCurrentForm().id == 'frmMyTopUpSelect') {
                    //frmMyTopUpSelect.lb.setVisibility(false);
                    //frmSelectBiller.segSuggestedBillers.removeAll();
                }
                dismissLoadingScreen();
                if (isSearched) {
                    //alert(kony.i18n.getLocalizedString("keybillernotfound"));
                    isSearched = false;
                }
            }
        } else {
            if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
                frmMyTopUpList.segSuggestedBillers.removeAll();
            } else if (kony.application.getCurrentForm().id == 'frmSelectBiller') {
                //frmSelectBiller.lblSuggestedBillerTopUpHeader.setVisibility(false);
                //frmSelectBiller.hboxMoreSelectBiller.setVisibility(false);
                //frmSelectBiller.segSuggestedBillers.removeAll();
            }
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    } else {
        if (status == 300) {
            if (kony.application.getCurrentForm().id == 'frmMyTopUpList') {
                frmMyTopUpList.segSuggestedBillers.removeAll();
            } else if (kony.application.getCurrentForm().id == 'frmSelectBiller') {
                //frmSelectBiller.lblSuggestedBillerTopUpHeader.setVisibility(false);
                //frmSelectBiller.hboxMoreSelectBiller.setVisibility(false);
                //frmSelectBiller.segSuggestedBillers.removeAll();
            }
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function populateMySuggestListBBMB(collectiondata) {
    //showLoadingScreen();
    //var tmpRef2= null;
    var tmpRef2Len = 0;
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    var tmpRef2label = "";
    var j = 0;
    var masterData = [];
    var tmpCatFilteredData = [];
    var locale = kony.i18n.getCurrentLocale();
    var tmpIsRef2Req = "";
    var tmpRef2BB = "";
    var tmpRef2Type = "";
    var tmpRef2Data = "";
    if (kony.i18n.getCurrentLocale() == "en_US") {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.i18n.getCurrentLocale() == "th_TH") {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    //    if(!isCatSelected)
    //    {
    var segSugData = [];
    for (var i = 0; i < collectiondata.length; i++) {
        if (gblMyBillerTopUpBB == 2) {
            tmpIsRef2Req = collectiondata[i]["IsRequiredRefNumber2Add"];
            //
            tmpRef2BB = "N";
            tmpRef2Type = "";
            tmpRef2Data = "";
            //alert(collectiondata[i]["BillerMiscData"].length + " collection data")
            if (collectiondata[i]["BillerMiscData"].length > 0) {
                for (var j = 0; j < collectiondata[i]["BillerMiscData"].length; j++) {
                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.IsRequiredRefNumber2") {
                        tmpRef2BB = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                        tmpRef2Type = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (collectiondata[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                        tmpRef2Data = collectiondata[i]["BillerMiscData"][j]["MiscText"];
                    }
                }
            }
            if (tmpRef2BB == "Y" || tmpRef2BB == "y") {
                //
                tmpRef2label = collectiondata[i][ref2label];
                tmpRef2Value = collectiondata[i]["ReferenceNumber2"];
                tmpRef2Len = collectiondata[i]["Ref2Len"];
            } else {
                //
                tmpRef2label = "";
                tmpRef2Value = "";
                tmpRef2Len = 0;
            }
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectiondata[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var tempRecord = {
                "lblSuggestedBiller": {
                    "text": collectiondata[i][billername] + "(" + collectiondata[i]["BillerCompcode"] + ")"
                },
                "btnAddBiller": {
                    "text": "  ",
                    "skin": "btnAdd"
                },
                "BillerID": {
                    "text": collectiondata[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectiondata[i]["BillerCompcode"]
                },
                "BarcodeOnly": collectiondata[i]["BarcodeOnly"],
                "Ref1Label": {
                    "text": collectiondata[i][ref1label]
                },
                "Ref2Label": {
                    "text": tmpRef2label
                },
                "imgSuggestedBiller": {
                    "src": imagesUrl
                },
                "IsRequiredRefNumber2Add": {
                    "text": collectiondata[i]["IsRequiredRefNumber2Add"]
                },
                "IsRequiredRefNumber2Pay": {
                    "text": collectiondata[i]["IsRequiredRefNumber2Pay"]
                },
                "EffDt": {
                    "text": collectiondata[i]["EffDt"]
                },
                "BillerMethod": {
                    "text": collectiondata[i]["BillerMethod"]
                },
                "Ref1Len": {
                    "text": collectiondata[i]["Ref1Len"]
                },
                "Ref2Len": {
                    "text": tmpRef2Len
                },
                "BillerCategoryID": {
                    "text": collectiondata[i]["BillerCategoryID"]
                },
                "hdnRef2BB": {
                    "text": tmpRef2BB
                },
                "hdnRef2Type": {
                    "text": tmpRef2Type
                },
                "hdnRef2Data": {
                    "text": tmpRef2Data
                },
                "hdnLabelRefNum1EN": {
                    "text": collectiondata[i]["LabelReferenceNumber1EN"]
                },
                "hdnLabelRefNum1TH": {
                    "text": collectiondata[i]["LabelReferenceNumber1TH"]
                },
                "hdnLabelRefNum2EN": {
                    "text": collectiondata[i]["LabelReferenceNumber2EN"]
                },
                "hdnLabelRefNum2TH": {
                    "text": collectiondata[i]["LabelReferenceNumber2TH"]
                },
                "hdnBillerNameEN": {
                    "text": collectiondata[i]["BillerNameEN"]
                },
                "hdnBillerNameTH": {
                    "text": collectiondata[i]["BillerNameTH"]
                }
            }
            segSugData.push(tempRecord);
        }
    }
    //dismissLoadingScreen();
    //myTopupSuggestListMB = myTopUpSuggestListRs;
    //	gblTopUpSelectFormDataHolder = myTopUpSuggestListRs;
    //	//alert("calling sugBillerCatChangeMB");
    //	sugBillerCatChangeMB();
    // myTopupSuggestListMB = myTopUpSuggestListRs;
    gblSugBillersForSearchMB = [];
    gblSugBillersForSearchMB = segSugData;
    //frmMyTopUpSelect.segSelectList.setData(myTopUpSelect);
    gblSugBBMoreMB = 0;
    onMyBBSelectMoreDynamicMB();
}

function onMyBBSelectMoreDynamicMB() {
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblTopUpMore to max length
    gblSugBBMoreMB = gblSugBBMoreMB + maxLength;
    var newData = [];
    var totalData = gblSugBillersForSearchMB; //data from static population 
    var totalLength = totalData.length;
    if (totalLength > gblSugBBMoreMB) {
        frmMyTopUpList.hbxSugMoreBB.setVisibility(true);
        var endPoint = gblSugBBMoreMB
    } else {
        frmMyTopUpList.hbxSugMoreBB.setVisibility(false);
        var endPoint = totalLength;
    }
    if (totalLength > gblSugBBMoreMB) {
        var endPoint = gblSugBBMoreMB
    } else {
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(gblSugBillersForSearchMB[i])
    }
    frmMyTopUpList.segSuggestedBillers.removeAll();
    frmMyTopUpList.segSuggestedBillers.setVisibility(true)
    frmMyTopUpList.segSuggestedBillers.setData(newData);
}

function selectBillerToViewBB() {
    var indexOfSelectedRow = frmMyTopUpList.segBillersList.selectedIndex[1];
    var indexOfSelectedIndex = frmMyTopUpList.segBillersList.selectedItems[0];
    if (gblBBorBillers == true) {
        frmBBMyBeepAndBill.imgBB.src = indexOfSelectedIndex.imgBillerLogo.src;
        //alert("lblCOnfirmNickName : " + indexOfSelectedRow);
        frmBBMyBeepAndBill.lblSelectedIndex.text = indexOfSelectedRow;
        gblBillerCompCodeBBMB = indexOfSelectedIndex.BillerCompCode;
        gblBillerMethodBBMB = indexOfSelectedIndex.hdnBillerMethod;
        frmBBMyBeepAndBill.lblBillerNickName.text = indexOfSelectedIndex.lblBillerNickname;
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmBBMyBeepAndBill.lblRef1.text = indexOfSelectedIndex.hdnLabelRefNum1EN;
            frmBBMyBeepAndBill.lblBillerCompcode.text = indexOfSelectedIndex.hdnBillerNameEN + "(" + indexOfSelectedIndex.BillerCompCode + ")";
        } else {
            frmBBMyBeepAndBill.lblRef1.text = indexOfSelectedIndex.hdnLabelRefNum1TH;
            frmBBMyBeepAndBill.lblBillerCompcode.text = indexOfSelectedIndex.hdnBillerNameTH + "(" + indexOfSelectedIndex.BillerCompCode + ")";
        }
        frmBBMyBeepAndBill.lblRef1Val.text = indexOfSelectedIndex.lblRef1Value;
        if (indexOfSelectedIndex.hdnRef2BB == "Y") {
            frmBBMyBeepAndBill.hbxRef2.setVisibility(true);
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmBBMyBeepAndBill.lblRef2.text = indexOfSelectedIndex.hdnLabelRefNum2EN;
            } else {
                frmBBMyBeepAndBill.lblRef2.text = indexOfSelectedIndex.hdnLabelRefNum2TH;
            }
            frmBBMyBeepAndBill.lblRef2Val.text = indexOfSelectedIndex.lblRef2Value;
        } else {
            frmBBMyBeepAndBill.hbxRef2.setVisibility(false);
            frmBBMyBeepAndBill.lblRef2.text = "";
            frmBBMyBeepAndBill.lblRef2Val.text = "";
        }
        var status = indexOfSelectedIndex.hdnStatus;
        if (status == "S" || status == "s") {
            frmBBMyBeepAndBill.lblStatusVal.text = kony.i18n.getLocalizedString("keySuccessApplied");
        } else if (status == "W" || status == "w") {
            frmBBMyBeepAndBill.lblStatusVal.text = kony.i18n.getLocalizedString("keyWaiting");
        } else {
            frmBBMyBeepAndBill.lblStatusVal.text = kony.i18n.getLocalizedString("keyReject");
        }
        //frmBBMyBeepAndBill.lblAccType.text = indexOfSelectedIndex.hdnlblAccType;
        //frmBBMyBeepAndBill.lblAccNo.text = indexOfSelectedIndex.hdnAccNo;
        //frmBBMyBeepAndBill.lblBalVal.text = indexOfSelectedIndex.hdnlblBalVal;
        //frmBBMyBeepAndBill.lblAccHolderName.text = indexOfSelectedIndex.hdnlblAccHolderName
        for (var i = 0; i < gblcustomerAccountsListBBMB.length; i++) {
            var tempAccNo = gblcustomerAccountsListBBMB[i]["accId"];
            var accNoLen = tempAccNo.length;
            var accNo = "";
            for (var j = 0; j < accNoLen; j++) {
                if (tempAccNo[j] != '-') accNo = accNo + tempAccNo[j];
            }
            accNo = accNo.substring(accNo.length - 10, accNo.length);
            if (indexOfSelectedIndex.hdnAccNo == accNo) {
                frmBBMyBeepAndBill.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["accountName"]
                frmBBMyBeepAndBill.lblBalVal.text = commaFormatted(gblcustomerAccountsListBBMB[i]["availableBal"])
                    //frmBBMyBeepAndBill.lblAcc.text = gblcustomerAccountsListBBMB[i]["accId"]
                frmBBMyBeepAndBill.lblAccNo.text = addHyphenMB(accNo);
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    frmBBMyBeepAndBill.lblAccType.text = gblcustomerAccountsListBBMB[i]["ProductNameEng"]
                    if (gblcustomerAccountsListBBMB[i]["acctNickName"] != undefined) frmBBMyBeepAndBill.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["acctNickName"]
                    else frmBBMyBeepAndBill.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["ProductNameEng"] + accNo.substring(accNo.length - 4, accNo.length);
                } else {
                    frmBBMyBeepAndBill.lblAccType.text = gblcustomerAccountsListBBMB[i]["ProductNameThai"]
                    if (gblcustomerAccountsListBBMB[i]["acctNickName"] != undefined) frmBBMyBeepAndBill.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["acctNickName"]
                    else frmBBMyBeepAndBill.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["ProductNameThai"] + accNo.substring(accNo.length - 4, accNo.length);
                }
                frmBBMyBeepAndBill.imgAccDetails.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblcustomerAccountsListBBMB[i]["ICON_ID"] + "&modIdentifier=PRODICON";
            }
        }
        frmBBMyBeepAndBill.show();
    } else {
        //frmAddTopUpToMB.btnRef2Dropdown.setVisibility(false);
        //frmAddTopUpToMB.txtRef2.setVisibility(true);
        gblBillerCompCodeBBMB = indexOfSelectedIndex.BillerCompCode.text;
        gblBillerMethodBBMB = indexOfSelectedIndex.hdnBillerMethod.text;
        frmAddTopUpToMB.imgAddedBiller.src = indexOfSelectedIndex.imgBillerLogo.src;
        frmAddTopUpToMB.lblAddbillerName.text = indexOfSelectedIndex.lblBillerName.text;
        frmAddTopUpToMB.txbNickName.text = indexOfSelectedIndex.lblBillerNickname.text;
        frmAddTopUpToMB.lblAddedRef1.text = indexOfSelectedIndex.lblRef1.text;
        frmAddTopUpToMB.txtRef1.text = indexOfSelectedIndex.lblRef1Value.text;
        frmAddTopUpToMB.lblAddedRef2.text = indexOfSelectedIndex.lblRef2.text;
        frmAddTopUpToMB.txtRef2.text = indexOfSelectedIndex.lblRef2Value.text;
        //alert(indexOfSelectedIndex.isRef2Pay.text + " : is ref2")
        /*if (indexOfSelectedIndex.isRef2Pay.text == "Y" || indexOfSelectedIndex.isRef2Pay.text == "y") {
            frmAddTopUpToMB.hbxref2.setVisibility(true);
            frmAddTopUpToMB.lineRef2.isVisible = true;
        } else {
            frmAddTopUpToMB.hbxref2.setVisibility(false);
            frmAddTopUpToMB.lineRef2.isVisible = false;
        }*/
        //populateOnFrmIBBeepAndBillListMB();
        showAccountListBBMB();
    }
}

function customerPaymentStatusInquiryForBBMBCalendar(result) {
    //kony.application.showLoadingScreen(frmLoading, "", d.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    // get msg from the unique ID from DB
    //parsing message
    //var splittedMsg = messsage.split(',');
    TMBUtil.DestroyForm(frmBBExecuteConfirmAndComplete);
    //executionFlowBB = true;
    //updateAccountListBB();
    //
    var BBObj = result["finFlexValues"].split('|');
    messageObj["pinCode"] = BBObj[0];
    messageObj["dueDate"] = BBObj[1];
    messageObj["toAcctNickname"] = BBObj[2];
    messageObj["ref1"] = BBObj[3];
    messageObj["finTxnAmount"] = BBObj[4];
    messageObj["txnFeeAmount"] = BBObj[5];
    messageObj["BBCompcode"] = BBObj[6];
    messageObj["finTxnRefID"] = result["finTxnRefID"];
    if (BBObj.length == 8) {
        messageObj["ref2"] = BBObj[7];
    } else {
        messageObj["ref2"] = "";
    }
    frmBBExecuteConfirmAndComplete.lblAmountVal.text = messageObj["finTxnAmount"] + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text = messageObj["txnFeeAmount"] + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmBBExecuteConfirmAndComplete.lblTransIDVal.text = messageObj["pinCode"];
    //frmBBExecuteConfirmAndComplete.lblFeeVal.text = feeAmount;
    var date = messageObj["dueDate"];
    frmBBExecuteConfirmAndComplete.lblDueDateVal.text = date.substring(0, 2) + "/" + date.substring(2, 4) + "/" + date.substring(4, 8);
    var inputParam = {};
    inputParam["rmNum"] = "";
    inputParam["PIN"] = messageObj["pinCode"];
    inputParam["finTxnAmount"] = messageObj["finTxnAmount"];
    inputParam["ref1"] = messageObj["ref1"];
    //inputParam["BillerName"] = getBillerNameMB(messageObj["BBCompcode"]);
    showLoadingScreen();
    if (gblmasterBillerAndTopupBBMB.length == 0) {
        gblUpdateMasterData = true;
        inquireMasterBillerAndTopupMB();
    } else {
        inputParam["BillerName"] = getBillerNameMB(messageObj["BBCompcode"]);
        invokeServiceSecureAsync("CustomerBBPaymentStatusInquiry", inputParam, customerBBPayemntStatusInquiryMBCallBack);
    }
}

function customerBBPayemntStatusInquiryMBCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var statusCode = result["additionalStatus"][0]["statusCode"];
            var severity = result["additionalStatus"][0]["severity"];
            var statusDesc = result["additionalStatus"][0]["statusDesc"];
            if (statusCode == 0) {
                //alert("PmtStatus : " + result["custBBPmtStatInqRs"][0]["PmtStatus"]);
                gblPmtStatusMB = result["custBBPmtStatInqRs"][0]["PmtStatus"];
                //alert(gblPmtStatusMB)
                if (result["custBBPmtStatInqRs"][0]["PmtStatus"] == "S") {
                    //alert("This Bill is already paid");
                    isRefreshCalenderMB = false;
                    executionFlowBBMB = true;
                    isToUpdateAccListMB = true;
                    gblToUpdateBBList = true;
                    if (gblmasterBillerAndTopupBBMB.length == 0) {
                        inquireMasterBillerAndTopupMB();
                    } else {
                        updateAccountListBBMB();
                    }
                    frmBBExecuteConfirmAndComplete.hbxStartImage.setVisibility(true);
                    //frmBBExecuteConfirmAndComplete.lblTransIDVal.text = result["custBBPmtStatInqRs"][0]["TransID"];
                    frmBBExecuteConfirmAndComplete.hboxSaveCamEmail.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
                    frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text = result["custBBPmtStatInqRs"][0]["PmtDate"];
                    frmBBExecuteConfirmAndComplete.btnRight.setVisibility(true);
                    if (flowSpa) {
                        frmBBExecuteConfirmAndComplete.hbxbpconfcancelspa.setVisibility(false);
                        frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(true);
                    } else {
                        frmBBExecuteConfirmAndComplete.hbxbpconfcancel.setVisibility(false);
                        frmBBExecuteConfirmAndComplete.hbxReturn.setVisibility(true);
                    }
                    return;
                    //show complete screen
                } else if (result["custBBPmtStatInqRs"][0]["PmtStatus"] == "P") {
                    //show comfirmatin page
                    //frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text = result["PmtDate"]
                    isRefreshCalenderMB = true;
                    executionFlowBBMB = true;
                    isToUpdateAccListMB = true;
                    gblToUpdateBBList = true;
                    if (gblmasterBillerAndTopupBBMB.length == 0) {
                        inquireMasterBillerAndTopupMB();
                    } else {
                        updateAccountListBBMB();
                    }
                    //frmIBBeepAndBillExecuteConfComp.hbxBillersOTPContainer.setVisibility(true);
                    //frmBBExecuteConfirmAndComplete.lblTransIDVal.text = result["custBBPmtStatInqRs"][0]["TransID"];
                    frmBBExecuteConfirmAndComplete.hboxSaveCamEmail.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
                    frmBBExecuteConfirmAndComplete.btnRight.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text = result["currentTime"];
                    frmBBExecuteConfirmAndComplete.hbxStartImage.setVisibility(false);
                    if (flowSpa) {
                        frmBBExecuteConfirmAndComplete.hbxbpconfcancelspa.setVisibility(true);
                        frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(false);
                    } else {
                        frmBBExecuteConfirmAndComplete.hbxbpconfcancel.setVisibility(true);
                        frmBBExecuteConfirmAndComplete.hbxReturn.setVisibility(false);
                    }
                    //frmIBBeepAndBillExecuteConfComp.show();
                }
                // populate updated list in frmMyTopupList
            } else {
                dismissLoadingScreen();
                showAlert(result["additionalStatus"][0]["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function getBBRefrenceNoMB() {
    var inputParam = {}
    inputParam["transRefType"] = "BB";
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, callBackgetBBRefrenceNoMB)
}

function callBackgetBBRefrenceNoMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblBBRefNoMB = resulttable.transRefNum;
            frmBBExecuteConfirmAndComplete.lblTransRefNoVal.text = gblBBRefNoMB + "00";
            populateOnExecuteBBPage();
            //checking transfer type
        } else {
            //alert(" "+resulttable["errMsg"]);
        }
    }
}
/*function createFinActivityLogObjBBMB(errorCd, tellerId) {

    GBLFINANACIALACTIVITYLOG.crmId = gblcrmId;

    GBLFINANACIALACTIVITYLOG.finTxnRefId = gblBBRefNoMB + "00";
    GBLFINANACIALACTIVITYLOG.tellerId = tellerId;
    var date = frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text;
    if (date != null) date = replaceCommon(date, "/", "");
    else date = "000000";
    GBLFINANACIALACTIVITYLOG.finTxnDate = date;

    var feeAmount = frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text;
    feeAmount = feeAmount.substring(0, feeAmount.length - 1)

    var amount = frmBBExecuteConfirmAndComplete.lblAmountVal.text;
    amount = amount.substring(0, amount.length - 1);

    GBLFINANACIALACTIVITYLOG.finTxnFee = feeAmount;
    GBLFINANACIALACTIVITYLOG.billerRef1 = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
    GBLFINANACIALACTIVITYLOG.billerRef2 = frmBBExecuteConfirmAndComplete.lblRef2Value.text;
    GBLFINANACIALACTIVITYLOG.billerCommCode = gblBillerCompCodeBBMB;
    GBLFINANACIALACTIVITYLOG.beepAndBillTxnId = frmBBExecuteConfirmAndComplete.lblTransIDVal.text;

    var DueDate = frmBBExecuteConfirmAndComplete.lblDueDateVal.text;
    GBLFINANACIALACTIVITYLOG.dueDate = DueDate.split("/").join("-");

    GBLFINANACIALACTIVITYLOG.activityTypeId = "034";

    GBLFINANACIALACTIVITYLOG.channelId = "02";

    GBLFINANACIALACTIVITYLOG.fromAcctId = removeUnwatedSymbols(frmBBExecuteConfirmAndComplete.lblAccountNum.text);

    GBLFINANACIALACTIVITYLOG.fromAcctName = frmBBExecuteConfirmAndComplete.lblAccountName.text;

    GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmBBExecuteConfirmAndComplete.lblBillerNickname.text;

    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        //alert(gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + " == " + gblBillerCompCodeBBMB )
        if (gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] == gblBillerCompCodeBBMB) {
            if (gblmasterBillerAndTopupBBMB[i]["BillerMethod"] == "2" || gblmasterBillerAndTopupBBMB[i]["BillerMethod"] == "3")
                GBLFINANACIALACTIVITYLOG.toAcctId = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
            else
                GBLFINANACIALACTIVITYLOG.toAcctId = gblmasterBillerAndTopupBBMB[i]["ToAccountKey"];

            if (kony.i18n.getCurrentLocale() == "en_US")
                GBLFINANACIALACTIVITYLOG.toAcctName = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"];
            else
                GBLFINANACIALACTIVITYLOG.toAcctName = gblmasterBillerAndTopupBBMB[i]["BillerNameTH"];

            //alert(GBLFINANACIALACTIVITYLOG.toAcctId + " and " + GBLFINANACIALACTIVITYLOG.toAcctName )

            break;
        }
    }


    //GBLFINANACIALACTIVITYLOG.toAcctId = "";//removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);

    //GBLFINANACIALACTIVITYLOG.toAcctName = "";//exeToAcctType;

    GBLFINANACIALACTIVITYLOG.toAcctNickname = ""; //exeToAcctName;

    //	GBLFINANACIALACTIVITYLOG.bankCD = "11";
    GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";

    GBLFINANACIALACTIVITYLOG.finTxnAmount = removeCommas(amount);

    GBLFINANACIALACTIVITYLOG.finTxnBalance = ""; //retailLiqTrnsAdd["FromAmt"];

    if (gblPmtStatusMB == "S" || gblPmtStatusMB == "s") {
        GBLFINANACIALACTIVITYLOG.finTxnStatus = "01";
        GBLFINANACIALACTIVITYLOG.clearingStatus = "01";
    } else {
        GBLFINANACIALACTIVITYLOG.finTxnStatus = "02";
        GBLFINANACIALACTIVITYLOG.clearingStatus = "02";
    }

    //if(retailLiqTrnsAdd["statusCode"] != 0) {
    GBLFINANACIALACTIVITYLOG.errorCd = errorCd;
    //} else {
    //GBLFINANACIALACTIVITYLOG.errorCd = retailLiqTrnsAdd["addStatusCode"];
    //}

    GBLFINANACIALACTIVITYLOG.eventId = gblBBRefNoMB.replace("BB", "");

    GBLFINANACIALACTIVITYLOG.txnType = "007";
    //GBLFINANACIALACTIVITYLOG.dueDate = frmBBExecuteConfirmAndComplete.lblDueDateVal.text;
    //GBLFINANACIALACTIVITYLOG.beepAndBillTxnId = frmBBExecuteConfirmAndComplete.lblTransIDVal.text;

    
    //

    GBLFINANACIALACTIVITYLOG.finFlexValues1 = gblPHONENUMBER;
    GBLFINANACIALACTIVITYLOG.finFlexValues2 = amount + "";
    GBLFINANACIALACTIVITYLOG.finFlexValues3 = frmBBExecuteConfirmAndComplete.lblAccountNum.text;
    GBLFINANACIALACTIVITYLOG.finFlexValues4 = "";
    GBLFINANACIALACTIVITYLOG.finFlexValues5 = "";

    financialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG);
}*/
function getMyBillListMBBB() {
    frmMyTopUpList.segBillersList.removeAll();
    var inputParams = {
        crmId: gblcrmId,
        clientDate: getCurrentDate()
    };
    showLoadingScreen();
    invokeServiceSecureAsync("customerBillInquiry", inputParams, startMyBillListMBBBServiceAsyncCallback);
}

function startMyBillListMBBBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            frmMyTopUpList.segBillersList.removeAll();
            var responseData = callBackResponse["CustomerBillInqRs"];
            if (responseData.length > 0) {
                frmMyTopUpList.lblMyBillers.setVisibility(false);
                populateMyBillsBBMB(callBackResponse["CustomerBillInqRs"]);
                dismissLoadingScreen();
            } else {
                frmMyTopUpList.show();
                frmMyTopUpList.segBillersList.removeAll();
                frmMyTopUpList.lblMyBillers.setVisibility(false);
                frmMyTopUpList.hbxMoreBB.setVisibility(false);
                dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyaddbillerstolist"), kony.i18n.getLocalizedString("info"));
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } else {
        if (status == 300) {
            frmMyTopUpList.segBillersList.removeAll();
            dismissIBLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function populateMyBillsBBMB(collectionData) {
    var billername = "BillerNameEN";
    var ref1label = "";
    var ref2label = "";
    var i = 0;
    var tmpRef2Value = "";
    var tmpRef2 = "";
    var tmpRef2label = "";
    var tmpIsRef2Req = "";
    var locale = kony.i18n.getCurrentLocale();
    myBillerTopupListMB = collectionData;
    if (kony.i18n.getCurrentLocale() == "en_US") {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.i18n.getCurrentLocale() == "th_TH") {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    var myBillerTopUpListRs = [];
    gblCustomerBillListBB = [];
    for (var i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BeepNBillFlag"] == "Y" || collectionData[i]["BeepNBillFlag"] == "y") {
            tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Pay"];
            if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
                tmpRef2label = collectionData[i][ref2label];
                tmpRef2Value = collectionData[i]["ReferenceNumber2"];
            } else {
                tmpRef2label = "";
                tmpRef2Value = "";
            }
            if (isEndsWithColon(collectionData[i][ref1label]) == false) collectionData[i][ref1label] = collectionData[i][ref1label] + ":";
            if (isEndsWithColon(tmpRef2label) == false) tmpRef2label = tmpRef2label + ":";
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var tempRecord = {
                "crmId": collectionData[i]["CRMID"],
                "lblBillerNickname": {
                    "text": collectionData[i]["BillerNickName"]
                },
                "lblBillerName": {
                    "text": collectionData[i][billername] + "(" + collectionData[i]["BillerCompcode"] + ")"
                },
                "lblRef1Value": {
                    "text": collectionData[i]["ReferenceNumber1"]
                },
                "lblRef2Value": {
                    "text": tmpRef2Value
                },
                "imgBillerLogo": {
                    "src": imagesUrl
                },
                "imgBillersRtArrow": {
                    "src": "bg_arrow_right.png"
                },
                "lblRef1": {
                    "text": collectionData[i][ref1label]
                },
                "lblRef2": {
                    "text": tmpRef2label
                },
                "BillerID": {
                    "text": collectionData[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "BeepndBill": {
                    "text": collectionData[i]["BeepNBillFlag"]
                },
                "CustomerBillerID": {
                    "text": collectionData[i]["CustomerBillID"]
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                },
                "hdnBillerMethod": {
                    "text": collectionData[i]["BillerMethod"]
                },
                "isRef2Pay": {
                    "text": collectionData[i]["IsRequiredRefNumber2Pay"]
                }
            }
            gblCustomerBillListBB.push(tempRecord);
        }
    }
    frmMyTopUpList.segBillersList.removeAll();
    if (gblCustomerBillListBB.length == 0) {
        frmMyTopUpList.lblMyBillers.setVisibility(false);
        frmMyTopUpList.hbxMoreBB.setVisibility(false);
        gblBillersForSearchMB = [];
        //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
    } else {
        gblBillersForSearchMB = [];
        gblBillersForSearchMB = gblCustomerBillListBB;
        frmMyTopUpList.lblMyBillers.setVisibility(true);
        frmMyTopUpList.hbxMoreBB.setVisibility(true);
        gblBBMoreMB = 0;
        onMyBBSelectBillersMoreDynamicMB();
    }
    frmMyTopUpList.show();
    dismissLoadingScreen();
}

function setIBTnCBBMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            frmBBApplyNow.lblTandC.text = resulttable["fileContent"];
            //frmIBBeepAndBillTandC.show();
            frmBBApplyNow.btnAgree.setEnabled(true);
            frmBBApplyNow.btnAgreeSpa.setEnabled(true);
        } else {
            if (kony.i18n.getCurrentLocale() == "th_TH") frmBBApplyNow.lblTandC.text = "";
            else frmBBApplyNow.lblTandC.text = "";
            frmBBApplyNow.btnAgree.setEnabled(false);
            frmBBApplyNow.btnAgreeSpa.setEnabled(false);
        }
    }
}

function VerifyPasswordEx_BBCompositeMB(otpTokenVal) {
    var inputParam = {}
        //VerifyPassword
    inputParam["flowspa"] = false;
    //inputParam["gblEditFTSchdule"] = gblEditFTSchduleMB+"";
    //inputParam["ClientDt"] = gblOrderDateFTMB;
    inputParam["channel"] = "rc";
    //SPA Changes 
    if (flowSpa) {
        inputParam["flowspa"] = "true";
        inputParam["channel"] = "wap";
        if (gblTokenSwitchFlag) {
            inputParam["gblTokenSwitchFlag"] = "true";
            inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
            inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
            inputParam["verifyTokenEx_userId"] = gblUserName;
            inputParam["verifyTokenEx_password"] = otpTokenVal;
            inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        } else {
            inputParam["gblTokenSwitchFlag"] = "false";
            inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
            inputParam["verifyPasswordEx_password"] = otpTokenVal;
            inputParam["password"] = otpTokenVal;
            inputParam["verifyPasswordEx_userId"] = gblUserName;
        }
    } else {
        inputParam["flowspa"] = "false";
    }
    var currForm = kony.application.getCurrentForm();
    if (currForm.id == 'frmBBConfirmAndComplete') {
        setInputParamsaddBBToListMB(inputParam);
        callNotificationAddServiceMBBBApply(inputParam);
        setactivityLogServiceCallCompositeBBMB(inputParam);
        invokeServiceSecureAsync("BeepAndBillApplyJavaService", inputParam, callBackVerifyTxnPwdBBComposite);
    } else if (currForm.id == 'frmBBExecuteConfirmAndComplete') {
        inputParam["customerBBPaymentAdd_PINcode"] = messageObj["pinCode"];
        callNotificationAddServiceMBBBExecuteComposite(inputParam);
        setactivityLogServiceCallBBExecuteCompositeMB(inputParam);
        setInputParamFinancialActivityLogBBExMB(inputParam);
        invokeServiceSecureAsync("BeepAndBillExecuteJavaService", inputParam, callBackVerifyTxnPwdBBComposite);
    }
}

function onBBApplyConfirmationAgreeComposite(tranPassword) {
    if (tranPassword == '') {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    }
    var inputParam = {}
    inputParam["verifyPassword_loginModuleId"] = "MB_TxPwd";
    inputParam["verifyPassword_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["verifyPassword_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["verifyPassword_password"] = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
    inputParam["password"] = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
    popupTractPwd.lblPopupTract7.setVisibility(false);
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    showLoadingScreen();
    inputParam["flowspa"] = "false";
    inputParam["channel"] = "rc";
    inputParam["appID"] = "TMB";
    if (gblTokenSwitchFlag) {
        inputParam["gblTokenSwitchFlag"] = "true";
    } else {
        inputParam["gblTokenSwitchFlag"] = "false";
    }
    var currForm = kony.application.getCurrentForm();
    if (currForm.id == 'frmBBConfirmAndComplete') {
        setInputParamsaddBBToListMB(inputParam);
        callNotificationAddServiceMBBBApply(inputParam);
        setactivityLogServiceCallCompositeBBMB(inputParam);
        invokeServiceSecureAsync("BeepAndBillApplyJavaService", inputParam, callBackVerifyTxnPwdBBComposite);
    } else if (currForm.id == 'frmBBExecuteConfirmAndComplete') {
        inputParam["customerBBPaymentAdd_PINcode"] = messageObj["pinCode"];
        callNotificationAddServiceMBBBExecuteComposite(inputParam);
        setactivityLogServiceCallBBExecuteCompositeMB(inputParam);
        setInputParamFinancialActivityLogBBExMB(inputParam);
        invokeServiceSecureAsync("BeepAndBillExecuteJavaService", inputParam, callBackVerifyTxnPwdBBComposite);
    }
}

function callBackVerifyTxnPwdBBComposite(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            popupTractPwd.dismiss();
            gblRetryCountRequestOTP = "0";
            var currForm = kony.application.getCurrentForm();
            if (currForm.id == 'frmBBExecuteConfirmAndComplete') {
                dismissLoadingScreen();
                frmBBExecuteConfirmAndComplete.hbxAdvertisement.setVisibility(true)
                frmBBExecuteConfirmAndComplete.hbxStartImage.setVisibility(true);
                frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
                frmBBExecuteConfirmAndComplete.btnRight.setVisibility(true);
                if (flowSpa) {
                    frmBBExecuteConfirmAndComplete.hbxbpconfcancelspa.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(true);
                    popOtpSpa.dismiss();
                } else {
                    frmBBExecuteConfirmAndComplete.hbxbpconfcancel.setVisibility(false);
                    frmBBExecuteConfirmAndComplete.hbxReturn.setVisibility(true);
                }
                frmBBExecuteConfirmAndComplete.lblBalBeforePay.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
                frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text = resulttable["currentTime"];
                frmBBExecuteConfirmAndComplete.lblBalBeforePayValue.text = commaFormattedOpenAct(resulttable["updatedBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
            } else if (currForm.id == 'frmBBConfirmAndComplete') {
                //invokeApplyBeenAndBill();
                frmBBConfirmAndComplete.hbxAdv.setVisibility(true);
                frmBBConfirmAndComplete.hbxAdv.isVisible = true;
                frmBBConfirmAndComplete.hbxArrow.setVisibility(true);
                frmBBConfirmAndComplete.imgHeaderRight.src = "empty.png";
                frmBBConfirmAndComplete.imgHeaderMiddle.src = "arrowtop.png";
                frmBBConfirmAndComplete.btnRightComplete.containerWeight = 45;
                frmBBConfirmAndComplete.btnRightConfirm.containerWeight = 1;
                frmBBConfirmAndComplete.hboxSaveCamEmail.setVisibility(false);
                frmBBConfirmAndComplete.hbxIconComplete.setVisibility(true);
                frmBBConfirmAndComplete.btnRightConfirm.setVisibility(false);
                //frmBBConfirmAndComplete.btnRightConfirm.isVisible = false;
                frmBBConfirmAndComplete.btnRightComplete.setVisibility(true);
                frmBBConfirmAndComplete.btnRightComplete.containerWeight = 16;
                //frmBBConfirmAndComplete.btnRightComplete.isVisible = true;
                frmBBConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete")
                if (flowSpa) {
                    frmBBConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyReturn");
                    frmBBConfirmAndComplete.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyApplyMore");
                    popOtpSpa.dismiss();
                } else {
                    frmBBConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
                    frmBBConfirmAndComplete.btnConfirm.text = kony.i18n.getLocalizedString("keyApplyMore");
                }
                if (gblBillerMethodBBMB != 1) {
                    //showAlert(kony.i18n.getLocalizedString("keyBBDelAddOfflineMsg"), kony.i18n.getLocalizedString("info"));
                    frmBBConfirmAndComplete.hbxAddDelOfflineMsg.setVisibility(true);
                } else {
                    frmBBConfirmAndComplete.hbxAddDelOfflineMsg.setVisibility(false);
                }
                campaginService("image2447443295186", "frmBBConfirmAndComplete", "M");
            }
        } else if (flowSpa) {
            dismissLoadingScreen();
            if (resulttable["opstatus"] == "8005") {
                if (resulttable["errCode"] == "VrfyOTPErr00001") {
                    gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                    popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                    popOtpSpa.lblPopupTract4Spa.text = "";
                    popOtpSpa.txtOTP.text = "";
                    return false;
                } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                    //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                    if (flowSpa) popOtpSpa.dismiss();
                    popTransferConfirmOTPLock.show();
                    return false;
                } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
                    kony.application.dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                    kony.application.dismissLoadingScreen();
                    popupTractPwd.lblPopupTract7.text = "";
                    showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                    kony.application.dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                    kony.application.dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errCode"] == "depErr02") {
                    kony.application.dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errCode"] == "depErr01") {
                    kony.application.dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errMsg"] != undefined) {
                    showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                    return false;
                } else {
                    kony.application.dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
            } else {
                popOtpSpa.txtOTP.text = "";
                if (resulttable["errMsg"] != undefined) {
                    kony.application.dismissLoadingScreen();
                    showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                    return false;
                } else {
                    kony.application.dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                    //alert(" "+resulttable["keyErrResponseOne"]);
                }
            }
        } else {
            gblRtyCtrVrfyTxPin = resulttable["retryCounterVerifyTransPwd"];
            gblRtyCtrVrfyAxPin = resulttable["retryCounterVerifyAccessPin"];
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
                showTranPwdLockedPopup();
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00002") {
                //alert("" + kony.i18n.getLocalizedString("KeyIncorrectPWD"));
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
                popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
                popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
                popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
                popupTractPwd.lblPopupTract7.isVisible = true;
                gblVerifyOTP = gblVerifyOTP + 1;
                return false;
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00001") {
                kony.application.dismissLoadingScreen();
                popupTractPwd.lblPopupTract7.text = "";
                showAlert(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "depErr02") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "depErr01") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errMsg"] != undefined) {
                kony.application.dismissLoadingScreen();
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                kony.application.dismissLoadingScreen();
                popupTractPwd.lblPopupTract7.text = "";
                showAlert(kony.i18n.getLocalizedString("keyErrResponseOne"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
}

function setInputParamsaddBBToListMB(inputParam) {
    var accNo = frmBBConfirmAndComplete.lblAccNo.text;
    accNo = removeHyphenIB(accNo);
    /*for(var i=0;i<tempAccNo.length;i++){
		if(tempAccNo[i]!= '-') accNo = accNo+tempAccNo[i]
	}*/
    accNo = accNo.substring(accNo.length - 10, accNo.length);
    var compCode = frmBBConfirmAndComplete.lblBillerCompcode.text;
    compCode = compCode.substring(compCode.length - 5, compCode.length - 1)
        //inputParam = {};
    inputParam["customerBBAdd_rmNum"] = "";
    inputParam["customerBBAdd_rqUUId"] = "";
    inputParam["customerBBAdd_BBNickname"] = frmBBConfirmAndComplete.lblNickName.text;
    inputParam["customerBBAdd_BBCompCode"] = gblBillerCompCodeBBMB;
    inputParam["customerBBAdd_Ref1"] = frmBBConfirmAndComplete.lblAddedRef1Val.text;
    inputParam["customerBBAdd_Ref1Label"] = frmBBConfirmAndComplete.lblAddedRef1.text;
    if (frmBBConfirmAndComplete.hbxRef2.isVisible) {
        inputParam["customerBBAdd_Ref2"] = frmBBConfirmAndComplete.lblAddedRef2Val.text;
        inputParam["customerBBAdd_Ref2Label"] = frmBBConfirmAndComplete.lblAddedRef2.text;
    } else {
        inputParam["customerBBAdd_Ref2"] = "";
        inputParam["customerBBAdd_Ref2Label"] = "";
    }
    inputParam["customerBBAdd_AcctIdentValue"] = accNo;
    inputParam["customerBBAdd_mobileNumber"] = gblPHONENUMBER;
    inputParam["customerBBAdd_ActionFlag"] = "A";
    inputParam["customerBBAdd_MIBFlag"] = "N";
    inputParam["customerBBAdd_Channel"] = "MIB";
    inputParam["customerBBAdd_ApplyDate"] = getTodaysDate();
    inputParam["customerBBAdd_ApplyTime"] = "155712";
}

function callNotificationAddServiceMBBBApply(inputParam) {
    //var inputparam = [];
    //inputparam["channelName"] = "Internet Banking"; please confirm whether it should be IB or MB or InternetBanking
    inputParam["NotificationAdd_notificationType"] = "Email"; // check this : which value we have to pass
    //inputparam["phoneNumber"] = gblPHONENUMBER;
    //inputparam["emailId"] = gblEmailId;
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_fromAccount"] = frmBBConfirmAndComplete.lblAccNo.text;
    // need partial masked account number, 5-9 digits unmasked
    inputParam["NotificationAdd_fromAcctNick"] = frmBBConfirmAndComplete.lblAccHolderName.text;
    inputParam["NotificationAdd_billerName"] = frmBBConfirmAndComplete.lblNickName.text;
    inputParam["NotificationAdd_billerCompCode"] = frmBBConfirmAndComplete.lblBillerCompcode.text;
    var labelRef1 = frmBBConfirmAndComplete.lblAddedRef1.text;
    labelRef1 = labelRef1.substring(0, labelRef1.length - 1);
    inputParam["NotificationAdd_ref1Label"] = labelRef1;
    inputParam["NotificationAdd_ref1Value"] = frmBBConfirmAndComplete.lblAddedRef1Val.text;
    if (frmBBConfirmAndComplete.hbxRef2.isVisible == true) {
        var labelRef2 = frmBBConfirmAndComplete.lblAddedRef2.text;
        labelRef2 = labelRef2.substring(0, labelRef2.length - 1);
        inputParam["NotificationAdd_ref2Label"] = labelRef2;
        inputParam["NotificationAdd_ref2Value"] = frmBBConfirmAndComplete.lblAddedRef2Val.text;
    } else {
        inputParam["NotificationAdd_ref2Label"] = "";
        inputParam["NotificationAdd_ref2Value"] = "";
    }
    // check for ref2 exists
    //inputparam["todayDate"] = getTodaysDate(); 
    //inputparam["todayTime"] = ""; // why blank ?
    //inputparam["channelID"] = "01";
    inputParam["NotificationAdd_source"] = "sendEmailForBBApply";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    //invokeServiceSecureAsync("NotificationAdd", inputparam, callBackNotificationAddService)
}

function setactivityLogServiceCallCompositeBBMB(inputParam) {
    var platformChannel = gblDeviceInfo.name;
    //inputParam = {};
    //inputParam["ipAddress"] = ""; 
    inputParam["activationActivityLog_activityTypeID"] = "032";
    //For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
    inputParam["activationActivityLog_errorCd"] = "";
    //inputParam["activationActivityLog_errorCd"] = errorCode; 
    //This status willl be "01" for success and "02" for failure of the actual event/instance
    inputParam["activationActivityLog_activityStatus"] = "01";
    inputParam["activationActivityLog_deviceNickName"] = gblDeviceNickName;
    inputParam["activationActivityLog_activityFlexValues1"] = frmBBConfirmAndComplete.lblBillerCompcode.text;
    inputParam["activationActivityLog_activityFlexValues2"] = frmBBConfirmAndComplete.lblAccNo.text;
    inputParam["activationActivityLog_activityFlexValues3"] = gblPHONENUMBER;
    inputParam["activationActivityLog_activityFlexValues4"] = frmBBConfirmAndComplete.lblAddedRef1Val.text;
    inputParam["activationActivityLog_activityFlexValues5"] = frmBBConfirmAndComplete.lblAddedRef2Val.text;;
    if (platformChannel == "thinclient") inputParam["activationActivityLog_channelId"] = GLOBAL_IB_CHANNEL;
    else inputParam["activationActivityLog_channelId"] = GLOBAL_MB_CHANNEL;
    inputParam["activationActivityLog_logLinkageId"] = "";
    //invokeServiceSecureAsync("activationActivityLog", inputParam, callBackActivityLog)
}

function verifyOTPBBCompositeEx() {
    var currForm = kony.application.getCurrentForm()
    var text = currForm.txtBxOTPBB.text;
    var isNumOTP = kony.string.isNumeric(text);
    var txtLen1 = text.length;
    var inputParam = {};
    inputParam["flowspa"] = "false";
    inputParam["channel"] = "wap";
    inputParam["appID"] = "TMB";
    inputParam["customerBBPaymentAdd_PINcode"] = messageObj["pinCode"];
    callNotificationAddServiceIBBBExecuteComposite(inputParam);
    setactivityLogServiceCallBBExecuteComposite(inputParam);
    setInputParamFinancialActivityLogBBEx(inputParam);
    if (gblTokenSwitchFlagBB == false) {
        if (text == "" || text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;
        }
        inputParam["gblTokenSwitchFlagBB"] = "false";
        inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblVerifyOTP;
        inputParam["verifyPasswordEx_userId"] = gblUserName;
        inputParam["verifyPasswordEx_userStoreId"] = "DefaultStore";
        inputParam["verifyPasswordEx_loginModuleId"] = "IBSMSOTP";
        inputParam["verifyPasswordEx_password"] = text;
        inputParam["password"] = text;
        inputParam["verifyPasswordEx_segmentId"] = "MIB";
        showLoadingScreenPopup();
        frmIBBeepAndBillConfAndComp.txtBxOTPBB.text = "";
        currForm.txtBxOTPBB.text = "";
        invokeServiceSecureAsync("BeepAndBillExecuteJavaService", inputParam, verifyTokenOTPBBCallbackComposite);
    } else {
        if (currForm.tbxToken.text == "" || currForm.tbxToken.text == null) {
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return false;
        }
        inputParam["gblTokenSwitchFlagBB"] = "true";
        inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
        inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["verifyTokenEx_userId"] = gblUserName;
        inputParam["verifyTokenEx_password"] = currForm.tbxToken.text;
        inputParam["password"] = currForm.tbxToken.text;
        inputParam["verifyTokenEx_sessionVal"] = "";
        inputParam["verifyTokenEx_segmentId"] = "segmentId";
        inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
        inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        showLoadingScreenPopup();
        currForm.tbxToken.text == ""
            //invokeServiceSecureAsync("verifyTokenEx", inputParam, verifyTokenOTPBBCallback)
        invokeServiceSecureAsync("BeepAndBillExecuteJavaService", inputParam, verifyTokenOTPBBCallbackComposite);
    }
}

function callNotificationAddServiceMBBBExecuteComposite(inputParam) {
    //inputparam["channelName"] = "Internet Banking";
    inputParam["NotificationAdd_notificationType"] = "Email"; // check this : which value we have to pass
    //inputparam["phoneNumber"] = gblPHONENUMBER;
    //inputparam["emailId"] = gblEmailId;
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_fromAccount"] = frmBBExecuteConfirmAndComplete.lblAccountNum.text;
    // need partial masked account number, 5-9 digits unmasked
    inputParam["NotificationAdd_fromAcctNick"] = frmBBExecuteConfirmAndComplete.lblAccountName.text;
    inputParam["NotificationAdd_billerNick"] = frmBBExecuteConfirmAndComplete.lblBillerNickname.text;
    inputParam["NotificationAdd_billerCompCode"] = frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text;
    inputParam["NotificationAdd_ref1Label"] = frmBBExecuteConfirmAndComplete.lblRef1.text;
    inputParam["NotificationAdd_ref1Value"] = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
    if (frmBBExecuteConfirmAndComplete.hbxRef2.isVisible == true) {
        inputParam["NotificationAdd_ref2Label"] = frmBBExecuteConfirmAndComplete.lblRef2.text;
        inputParam["NotificationAdd_ref2Value"] = frmBBExecuteConfirmAndComplete.lblRef2Value.text;
    }
    inputParam["NotificationAdd_amount"] = frmBBExecuteConfirmAndComplete.lblAmountVal.text;
    inputParam["NotificationAdd_fee"] = frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text;
    inputParam["NotificationAdd_initiationDt"] = frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text;
    //inputparam["todayDate"] = getTodaysDate(); 
    //inputparam["todayTime"] = ""; // why blank ?
    inputParam["NotificationAdd_transactionRefNum"] = frmBBExecuteConfirmAndComplete.lblTransRefNoVal.text;
    //inputparam["channelID"] = "01";
    inputParam["NotificationAdd_source"] = "sendEmailForBBExecute";
    inputParam["Locale"] = kony.i18n.getCurrentLocale();
}

function setactivityLogServiceCallBBExecuteCompositeMB(inputParam) {
    inputParam["activationActivityLog_activityTypeID"] = "034";
    //For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
    inputParam["activationActivityLog_errorCd"] = "";
    //inputParam["activationActivityLog_errorCd"] = errorCode; 
    //This status willl be "01" for success and "02" for failure of the actual event/instance
    inputParam["activationActivityLog_activityStatus"] = "01";
    inputParam["activationActivityLog_deviceNickName"] = gblDeviceNickName;
    inputParam["activationActivityLog_activityFlexValues1"] = frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text;
    inputParam["activationActivityLog_activityFlexValues2"] = gblPHONENUMBER;
    inputParam["activationActivityLog_activityFlexValues3"] = frmBBExecuteConfirmAndComplete.lblAccountNum.text;
    inputParam["activationActivityLog_activityFlexValues4"] = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
    inputParam["activationActivityLog_activityFlexValues5"] = frmBBExecuteConfirmAndComplete.lblAmountVal.text + "+" + frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text;
    //if (platformChannel == "thinclient")
    //	inputParam["activationActivityLog_channelId"] = GLOBAL_IB_CHANNEL;
    //else
    inputParam["activationActivityLog_channelId"] = "02";
    inputParam["activationActivityLog_logLinkageId"] = "";
}

function setInputParamFinancialActivityLogBBExMB(inputParam) {
    inputParam["financialActivityLog_crmId"] = gblcrmId;
    inputParam["financialActivityLog_finTxnRefId"] = gblBBRefNoMB + "00";
    inputParam["financialActivityLog_tellerId"] = "";
    //var date = frmIBBeepAndBillExecuteConfComp.lblPaymentDateVal.text;
    //if(date != null) date = replaceCommon(date, "/", "");
    //else date = "000000";
    inputParam["financialActivityLog_finTxnDate"] = frmBBExecuteConfirmAndComplete.lblPaymentDateVal.text;;
    var feeAmount = frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text;
    feeAmount = feeAmount.substring(0, feeAmount.length - 1);
    feeAmount = feeAmount.replace(",", "");
    inputParam["financialActivityLog_finTxnFee"] = feeAmount;
    inputParam["financialActivityLog_billerRef1"] = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
    inputParam["financialActivityLog_billerRef2"] = frmBBExecuteConfirmAndComplete.lblRef2Value.text;
    inputParam["financialActivityLog_billerCommCode"] = gblBillerCompCodeBBMB;
    inputParam["financialActivityLog_beepAndBillTxnId"] = frmBBExecuteConfirmAndComplete.lblTransIDVal.text;
    var DueDate = frmBBExecuteConfirmAndComplete.lblDueDateVal.text;
    //inputParam["financialActivityLog_dueDate"] = DueDate.split("/").join("-");
    inputParam["financialActivityLog_dueDate"] = (DueDate != null && DueDate != "" && DueDate != undefined) ? DueDate.replace(/\//g, "-") : "";
    inputParam["financialActivityLog_activityTypeId"] = "034";
    inputParam["financialActivityLog_channelId"] = "01";
    inputParam["financialActivityLog_fromAcctId"] = removeUnwatedSymbols(frmBBExecuteConfirmAndComplete.lblAccountNum.text);
    inputParam["financialActivityLog_fromAcctName"] = frmBBExecuteConfirmAndComplete.lblAccountName.text;
    inputParam["financialActivityLog_fromAcctNickname"] = frmBBExecuteConfirmAndComplete.lblBillerNickname.text;
    var masterBillerRec = "";
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        //alert(gblmasterBillerAndTopupBB[i]["BillerCompcode"] + " == " + gblBillerCompCodeBB )
        if (gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] == gblBillerCompCodeBBMB) {
            if (gblmasterBillerAndTopupBBMB[i]["BillerMethod"] == "2" || gblmasterBillerAndTopupBBMB[i]["BillerMethod"] == "3") inputParam["financialActivityLog_toAcctId"] = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
            else inputParam["financialActivityLog_toAcctId"] = gblmasterBillerAndTopupBBMB[i]["ToAccountKey"];
            if (kony.i18n.getCurrentLocale() == "en_US") inputParam["financialActivityLog_toAcctName"] = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"];
            else inputParam["financialActivityLog_toAcctName"] = gblmasterBillerAndTopupBBMB[i]["BillerNameTH"];
            //alert(GBLFINANACIALACTIVITYLOG.toAcctId + " and " + GBLFINANACIALACTIVITYLOG.toAcctName )
            break;
        }
    }
    //GBLFINANACIALACTIVITYLOG.toAcctId = "";//removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
    //GBLFINANACIALACTIVITYLOG.toAcctName = "";//exeToAcctType;
    inputParam["financialActivityLog_toAcctNickname"] = ""; //exeToAcctName;
    //	GBLFINANACIALACTIVITYLOG.bankCD = "11";
    inputParam["financialActivityLog_toBankAcctCd"] = "11";
    var amount = frmBBExecuteConfirmAndComplete.lblAmountVal.text;
    amount = amount.substring(0, amount.length - 1);
    amount = amount.replace(",", "");
    inputParam["financialActivityLog_finTxnAmount"] = "" + amount;
    inputParam["financialActivityLog_finTxnBalance"] = ""; //retailLiqTrnsAdd["FromAmt"];
    if (gblPmtStatusMB == "S" || gblPmtStatusMB == "s") {
        inputParam["financialActivityLog_finTxnStatus"] = "01";
        inputParam["financialActivityLog_clearingStatus"] = "01";
    } else {
        inputParam["financialActivityLog_finTxnStatus"] = "02";
        inputParam["financialActivityLog_clearingStatus"] = "02";
    }
    //if(retailLiqTrnsAdd["statusCode"] != 0) {
    inputParam["financialActivityLog_errorCd"] = "";
    //} else {
    //GBLFINANACIALACTIVITYLOG.errorCd = retailLiqTrnsAdd["addStatusCode"];
    //}
    inputParam["financialActivityLog_eventId"] = gblBBRefNoMB.replace("BB", "") + "00";
    inputParam["financialActivityLog_txnType"] = "007";
    //GBLFINANACIALACTIVITYLOG.dueDate = frmIBBeepAndBillExecuteConfComp.lblDueDateVal.text + "";
    //GBLFINANACIALACTIVITYLOG.beepAndBillTxnId = frmIBBeepAndBillExecuteConfComp.lblTransactionIDVal.text + "";
    //
    inputParam["financialActivityLog_finFlexValues1"] = frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text;
    inputParam["financialActivityLog_finFlexValues2"] = "" + gblPHONENUMBER;
    inputParam["financialActivityLog_finFlexValues3"] = frmBBExecuteConfirmAndComplete.lblAccountNum.text;
    inputParam["financialActivityLog_finFlexValues4"] = frmBBExecuteConfirmAndComplete.lblRef1Value.text;
    inputParam["financialActivityLog_finFlexValues5"] = frmBBExecuteConfirmAndComplete.lblAmountVal.text + "+" + frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text;
}

function onBBListLocaleChange() {
    if (gblBBorBillers == true) {
        gblOnClickLocaleChangeBB = true;
        getSelectBillerCategoryServiceBBMB();
        frmBBListPreshow();
        populateBillerCategory();
    } else {
        frmMyTopUpList.lblSuggestedBillersText.setVisibility(true)
            //frmMyTopUpList.segBillersList.setData(gblCustomerBBMBInqRs);
        frmMyTopUpList.segBillersList.removeAll();
        frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString("keyBillPaymentSelectBill");
        showLoadingScreen();
        gblBBorBillers = false
        populateMySuggestListBBMB(gblmasterBillerAndTopupBBMB);
        getMyBillListMBBB();
    }
}

function onBBApplyLocaleChange() {
    //showAccountListBBMB();
    //frmBBPaymentApplyPreshow();
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        if (gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] == gblBillerCompCodeBBMB) {
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmAddTopUpToMB.lblAddbillerName.text = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                //alert(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"] + " " + gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"])
                frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"];
                frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"];
            } else {
                //alert(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"] + " " + gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"])
                frmAddTopUpToMB.lblAddbillerName.text = gblmasterBillerAndTopupBBMB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"];
                frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"];
            }
        }
    }
    showAccountListBBMB();
}

function onBBConfirmCompleteLocaleChange() {
    if (frmBBConfirmAndComplete.lblBalVal.isVisible) {
        frmBBConfirmAndComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
    } else {
        frmBBConfirmAndComplete.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    }
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        if (gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] == gblBillerCompCodeBBMB) {
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmBBConfirmAndComplete.lblBillerCompcode.text = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                frmBBConfirmAndComplete.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"];
                frmBBConfirmAndComplete.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"];
            } else {
                frmBBConfirmAndComplete.lblBillerCompcode.text = gblmasterBillerAndTopupBBMB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                frmBBConfirmAndComplete.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"];
                frmBBConfirmAndComplete.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"];
            }
        }
    }
    var accNo = removeUnwatedSymbols(frmBBConfirmAndComplete.lblAccNo.text);
    for (var i = 0; i < gblcustomerAccountsListBBMB.length; i++) {
        var acc = gblcustomerAccountsListBBMB[i]["accId"].substring(gblcustomerAccountsListBBMB[i]["accId"].length - 10, gblcustomerAccountsListBBMB[i]["accId"].length);
        if (acc == accNo) {
            if (kony.i18n.getCurrentLocale() == "th_TH") {
                frmBBConfirmAndComplete.lblAccType.text = gblcustomerAccountsListBBMB[i]["ProductNameThai"];
                if (gblcustomerAccountsListBBMB[i]["acctNickName"] != undefined) frmBBConfirmAndComplete.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["acctNickName"]
                else frmBBConfirmAndComplete.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["ProductNameThai"] + acc.substring(acc.length - 4, acc.length);
            } else {
                frmBBConfirmAndComplete.lblAccType.text = gblcustomerAccountsListBBMB[i]["ProductNameEng"];
                if (gblcustomerAccountsListBBMB[i]["acctNickName"] != undefined) frmBBConfirmAndComplete.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["acctNickName"]
                else frmBBConfirmAndComplete.lblAccHolderName.text = gblcustomerAccountsListBBMB[i]["ProductNameEng"] + acc.substring(acc.length - 4, acc.length);
            }
        }
    }
    frmBBConfirmAndComplete.lblSend.text = kony.i18n.getLocalizedString("keyMyBBPayment");
    frmBBConfirmAndComplete.lblSaveTo.text = kony.i18n.getLocalizedString("keyChargeTo");
    if (frmBBConfirmAndComplete.hbxIconComplete.isVisible == false) {
        frmBBConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
        if (flowSpa) {
            frmBBConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
            frmBBConfirmAndComplete.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
        } else {
            frmBBConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
            frmBBConfirmAndComplete.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        }
    } else {
        frmBBConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete")
        if (flowSpa) {
            frmBBConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyReturn");
            frmBBConfirmAndComplete.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyApplyMore");
        } else {
            frmBBConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
            frmBBConfirmAndComplete.btnConfirm.text = kony.i18n.getLocalizedString("keyApplyMore");
        }
    }
    frmBBConfirmAndCompletePreshow();
}

function onLocaleChangeExecuteBB() {
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        if (gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] == messageObj["BBCompcode"]) {
            if (kony.i18n.getCurrentLocale() == "en_US") {
                //alert("setting to eng");
                frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                frmBBExecuteConfirmAndComplete.lblRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"];
                frmBBExecuteConfirmAndComplete.lblRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"];
                break;
            } else {
                //alert("setting to Thai" + gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"]);
                frmBBExecuteConfirmAndComplete.lblBillerNameCompCode.text = gblmasterBillerAndTopupBBMB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                frmBBExecuteConfirmAndComplete.lblRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"] + "";
                frmBBExecuteConfirmAndComplete.lblRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"] + "";
                break;
            }
        }
    }
    /*var accNo = removeHyphenIB(frmBBExecuteConfirmAndComplete.lblAccountNum.text);
    for(var i=0;i < gblcustomerAccountsListBBMB.length ; i++){
    	var acc = gblcustomerAccountsListBBMB[i]["accId"].substring(gblcustomerAccountsListBBMB[i]["accId"].length - 10, gblcustomerAccountsListBBMB[i]["accId"].length);
    	
    	if( acc == accNo){
    		alert(acc + " = " + accNo);
    		if(kony.i18n.getCurrentLocale() == "en_US"){
    			frmBBExecuteConfirmAndComplete.lblProductName.text = gblcustomerAccountsListBBMB[i]["ProductNameEng"];
    			break;
    		}else{
    			frmBBExecuteConfirmAndComplete.lblProductName.text = gblcustomerAccountsListBBMB[i]["ProductNameThai"];
    			break;
    		}
    	}
    }*/
    for (var j = 0; j < gblcustomerAccountsListBBMB.length; j++) {
        var accId = removeHyphenIB(gblcustomerAccountsListBBMB[j]["accId"]);
        accId = accId.substring(accId.length - 10, accId.length);
        if (accId == removeHyphenIB(frmBBExecuteConfirmAndComplete.lblAccountNum.text)) {
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmBBExecuteConfirmAndComplete.lblProductName.text = gblcustomerAccountsListBBMB[j]["ProductNameEng"];
            } else {
                frmBBExecuteConfirmAndComplete.lblProductName.text = gblcustomerAccountsListBBMB[j]["ProductNameThai"];
            }
            frmBBExecuteConfirmAndComplete.imgFromAccount.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblcustomerAccountsListBBMB[0]["ICON_ID"] + "&modIdentifier=PRODICON";
        }
    }
    if (frmBBExecuteConfirmAndComplete.hbxHiddenAcc.isVisible) {
        if (flowSpa) {
            frmBBExecuteConfirmAndComplete.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") + "<br>" + kony.i18n.getLocalizedString("S2S_DelAcctMsg2") + "</br>" + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + "<br>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans") + "</br>";
        } else {
            frmBBExecuteConfirmAndComplete.richTxtLnkdAcctHidden.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") + "" + kony.i18n.getLocalizedString("S2S_DelAcctMsg2") + " " + "<a onclick= \"return onClickCallBackFunctionIB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + " " + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
        }
        //frmBBExecuteConfirmAndComplete.hbxHiddenAcc.setVisibility(true);
        frmBBExecuteConfirmAndComplete.hbxAccountDetails.setVisibility(false);
        //frmBBExecuteConfirmAndComplete.lblFrom.setVisibility(false);
        frmBBExecuteConfirmAndComplete.hbxHiddenAcc.setVisibility(true);
        if (flowSpa) {
            frmBBExecuteConfirmAndComplete.btnConfirmSpa.setEnabled(false);
            frmBBExecuteConfirmAndComplete.btnConfirmSpa.skin = btnDisabledGray;
        } else {
            frmBBExecuteConfirmAndComplete.btnConfirm.setEnabled(false);
            frmBBExecuteConfirmAndComplete.btnConfirm.skin = btnDisabledGray;
        }
    }
    if (gblPmtStatusMB == "P") {
        if (flowSpa) {
            frmBBExecuteConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
            frmBBExecuteConfirmAndComplete.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
            //frmBBExecuteConfirmAndComplete.hbxReturn.setVisibility(true);
            frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(false);
        } else {
            frmBBExecuteConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
            frmBBExecuteConfirmAndComplete.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
            frmBBExecuteConfirmAndComplete.hbxReturn.setVisibility(false);
            //frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(true);
        }
        frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
        //frmBBExecuteConfirmAndComplete.lblBankRefBB.text = kony.i18n.getLocalizedString("keybankrefno");
        //frmBBExecuteConfirmAndComplete.btnOTPReq.text = kony.i18n.getLocalizedString("RequestOTP");
        //frmBBExecuteConfirmAndComplete.txtBxOTPBB.placeholder = kony.i18n.getLocalizedString("enterOTP");
        //frmBBExecuteConfirmAndComplete.btnToken.text = kony.i18n.getLocalizedString("keySwitchToOTP");
        //frmBBExecuteConfirmAndComplete.tbxToken.placeholder = kony.i18n.getLocalizedString("enterToken");
        frmBBExecuteConfirmAndComplete.lblBalBeforePay.text = kony.i18n.getLocalizedString("keyBalanceBeforePayment");
    } else {
        frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
        frmBBExecuteConfirmAndComplete.lblBalBeforePay.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
        if (flowSpa) {
            frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(true);
        } else {
            frmBBExecuteConfirmAndComplete.hbxReturn.setVisibility(true);
        }
    }
    frmBBExecuteConfirmAndComplete.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
    frmBBExecuteConfirmAndComplete.lblAmount.text = kony.i18n.getLocalizedString("keyXferAmt");
    frmBBExecuteConfirmAndComplete.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
    frmBBExecuteConfirmAndComplete.lblDueDate.text = kony.i18n.getLocalizedString("MyActivitiesIB_Due");
    frmBBExecuteConfirmAndComplete.lblPaymentDate.text = kony.i18n.getLocalizedString("keyIBPaymentDate");
    frmBBExecuteConfirmAndComplete.lblTransID.text = kony.i18n.getLocalizedString("MyActivitiesIB_TransId");
    frmBBExecuteConfirmAndComplete.lblTransRefNo.text = kony.i18n.getLocalizedString("keyTransactionRefNo");
    frmBBExecuteConfirmAndComplete.lblToBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
    frmBBExecuteConfirmAndComplete.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
    //frmBBExecuteConfirmAndComplete.show()
}

function onClickLeftArrowBB() {
    gblshotcutToACC = false
    if (gbltranFromSelIndex[1] == 0) {
        frmAddTopUpToMB.segSlider.selectedIndex = [0, (gblNoOfFromAcsBB - 1)];
    } else {
        frmAddTopUpToMB.segSlider.selectedIndex = [0, (gbltranFromSelIndex[1] - 1)];
    }
    gbltranFromSelIndex = frmAddTopUpToMB.segSlider.selectedIndex;
    //if (gblRC_QA_TEST_VAL == 0) {
    //		gbltdFlag = frmAddTopUpToMB.segSlider.selectedItems[0].lblSliderAccN2;
    //	} else {
    //		gbltdFlag = frmAddTopUpToMB.segSlider.selectedItems[0].tdFlag;
    //		
    //	}
    //toggleTDMaturityCombobox(gbltdFlag);
    //ResetTransferHomePage();
    //frmTransferLanding.txtTranLandAmt.setEnabled(true);
    //frmTransferLanding.txtTranLandAmt.text = "";
    //frmTransferLanding.btnSchedTo.setEnabled(true);
}

function onClickRightArrowBB() {
    if (gbltranFromSelIndex[1] == (gblNoOfFromAcsBB - 1)) frmAddTopUpToMB.segSlider.selectedIndex = [0, (gbltranFromSelIndex[1] - (gblNoOfFromAcsBB - 1))];
    else frmAddTopUpToMB.segSlider.selectedIndex = [0, (gbltranFromSelIndex[1] + 1)];
    gbltranFromSelIndex = frmAddTopUpToMB.segSlider.selectedIndex;
    //if (gblRC_QA_TEST_VAL == 0) {
    //		gbltdFlag = frmTransferLanding.segTransFrm.selectedItems[0].lblSliderAccN2;
    //	} else {
    //		gbltdFlag = frmTransferLanding.segTransFrm.selectedItems[0].tdFlag;
    //		
    //	}
    //	toggleTDMaturityCombobox(gbltdFlag);
    //	ResetTransferHomePage();
    //	frmTransferLanding.txtTranLandAmt.setEnabled(true);
    //	frmTransferLanding.txtTranLandAmt.text = "";
    //	frmTransferLanding.btnSchedTo.setEnabled(true);
}

function populateOnFrmIBBeepAndBillListMB() {
    frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyBBApplyPayment');
    frmAddTopUpToMB.lblBiller.text = kony.i18n.getLocalizedString('billerMB');
    frmAddTopUpToMB.lblNickname.text = kony.i18n.getLocalizedString('Receipent_NickName');
    //frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString('keyRef1');
    //frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString('keyRef2');
    if (flowSpa) {
        frmAddTopUpToMB.btnSpaNext.text = kony.i18n.getLocalizedString('Next');
    } else {
        frmAddTopUpToMB.button15633509701481.text = kony.i18n.getLocalizedString('Next');
    }
    for (var i = 0; i < gblmasterBillerAndTopupBBMB.length; i++) {
        if (gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] == gblBillerCompCodeBBMB) {
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmAddTopUpToMB.lblAddbillerName.text = gblmasterBillerAndTopupBBMB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"];
                frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"];
            } else {
                frmAddTopUpToMB.lblAddbillerName.text = gblmasterBillerAndTopupBBMB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] + ")";
                frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"];
                frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"];
            }
            try {
                frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblmasterBillerAndTopupBBMB[i]["Ref1Len"])
                frmAddTopUpToMB.txtRef2.maxTextLength = parseInt(gblmasterBillerAndTopupBBMB[i]["Ref2Len"]);
            } catch (e) {}
            var tmpRef2BB = "N";
            var tmpRef2Type = "";
            var tmpRef2Data = "";
            for (var k = 0; k < gblmasterBillerAndTopupBBMB[i]["BillerMiscData"].length; k++) {
                if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][k]["MiscName"] == "BB.IsRequiredRefNumber2") {
                    tmpRef2BB = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][k]["MiscText"];
                }
                if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.TYPE") {
                    tmpRef2Type = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][k]["MiscText"];
                }
                if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][k]["MiscName"] == "BB.Ref2.INPUT.VALUE") {
                    tmpRef2Data = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][k]["MiscText"];
                }
            }
            if (tmpRef2BB == "Y") {
                frmAddTopUpToMB.hbxref2.setVisibility(true);
                if (tmpRef2Type == "text" || tmpRef2Type == "Text") {
                    frmAddTopUpToMB.txtRef2.setVisibility(true);
                    frmAddTopUpToMB.btnRef2Dropdown.setVisibility(false);
                } else if (tmpRef2Type == "Dropdown") {
                    frmAddTopUpToMB.txtRef2.setVisibility(false);
                    frmAddTopUpToMB.btnRef2Dropdown.isVisible = true;
                    frmAddTopUpToMB.btnRef2Dropdown.text = kony.i18n.getLocalizedString("keySelectRef2BB");
                    gblRef2masterData = tmpRef2Data;
                }
            } else {
                frmAddTopUpToMB.hbxref2.setVisibility(false);
            }
            //alert(tmpRef2BB + " " + tmpRef2Type + " " + tmpRef2Data );
            if (kony.i18n.getCurrentLocale() == "en_US") {
                for (var j = 0; j < gblmasterBillerAndTopupBBMB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.EN") {
                        frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.EN") {
                        frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LEN") {
                        frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]);
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LEN") {
                        frmAddTopUpToMB.txtRef2.maxTextLength = parseInt(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]);
                    }
                }
            } else {
                for (var j = 0; j < gblmasterBillerAndTopupBBMB[i]["BillerMiscData"].length; j++) {
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LABEL.TH") {
                        frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LABEL.TH") {
                        frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"];
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref1.LEN") {
                        frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]);
                    }
                    if (gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscName"] == "BB.Ref2.LEN") {
                        frmAddTopUpToMB.txtRef2.maxTextLength = parseInt(gblmasterBillerAndTopupBBMB[i]["BillerMiscData"][j]["MiscText"]);
                    }
                }
            }
        }
    }
}

function handleBeepAndBillExecutionIphone(notificationID) {
    var inputParams = {
        notificationID: notificationID
    };
    invokeServiceSecureAsync("beepAndBillData", inputParams, callBackHandleBeepAndBillExecutionIphone)
    return;
}

function callBackHandleBeepAndBillExecutionIphone(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0 && callBackResponse["errCode"] == "0") {
            var bbData = callBackResponse["bbData"];
            if (bbData == "" || bbData == null) {
                alert("Beep And  Bill Data Empty");
                return;
            }
            var bbArray = bbData.split("|")
            messageObj["pinCode"] = bbArray[0];
            messageObj["dueDate"] = bbArray[1];
            messageObj["toAcctNickname"] = bbArray[2];
            messageObj["ref1"] = bbArray[3];
            messageObj["finTxnAmount"] = bbArray[4];
            messageObj["txnFeeAmount"] = bbArray[5];
            messageObj["BBCompcode"] = bbArray[6];
            messageObj["ref2"] = bbArray[7];
            frmBBExecuteConfirmAndComplete.lblAmountVal.text = messageObj["finTxnAmount"] + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmBBExecuteConfirmAndComplete.lblPaymentFeeVal.text = messageObj["txnFeeAmount"] + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmBBExecuteConfirmAndComplete.lblTransIDVal.text = messageObj["pinCode"];
            //frmBBExecuteConfirmAndComplete.lblFeeVal.text = feeAmount;
            var date = messageObj["dueDate"];
            var day = date.substring(0, 2);
            var month = date.substring(2, 4);
            var year = date.substring(4, 8);
            var newDate = year + "-" + month + "-" + day;
            var arrDate = newDate.split("-");
            frmBBExecuteConfirmAndComplete.lblDueDateVal.text = arrDate[2] + '/' + arrDate[1] + '/' + arrDate[0];
            var inputParam = {};
            inputParam["rmNum"] = "";
            inputParam["PIN"] = messageObj["pinCode"];
            inputParam["finTxnAmount"] = messageObj["finTxnAmount"];
            inputParam["ref1"] = messageObj["ref1"];
            showLoadingScreen();
            if (gblmasterBillerAndTopupBBMB.length == 0) {
                gblUpdateMasterData = true;
                inquireMasterBillerAndTopupMB();
            } else {
                inputParam["BillerName"] = getBillerNameMB(messageObj["BBCompcode"]);
                invokeServiceSecureAsync("CustomerBBPaymentStatusInquiry", inputParam, customerBBPayemntStatusInquiryMBCallBack);
            }
        }
    }
}