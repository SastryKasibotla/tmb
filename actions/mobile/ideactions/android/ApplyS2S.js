var s2s_activityTypeID;
var s2s_errorCode;
var s2s_activityStatus;
var s2s_deviceNickName;
var s2s_activityFlexValues1;
var s2s_activityFlexValues2;
var s2s_activityFlexValues3;
var s2s_activityFlexValues4;
var s2s_activityFlexValues5;
var s2s_logLinkageId;
var linkdAcct;
var noFixedAcct;
var collectionDataEdit;
var collectionDataExecute;
var liqInqCollectionData;
var transRefNumForS2S;
var eligibleS2SAcctInfo;
var noFixedAcctInfo;
var exeFromAcctName;
var exeFromAcctType;
var exeToAcctName;
var exeToAcctType;
//var exeToAccountProdId;
/** This method is used to check the service hours **/
function checkBusinessHoursForS2S() {
    var inputParams = {
        myProfileFlag: "false"
    };
    invokeServiceSecureAsync("checkS2SBusinessHours", inputParams, startApplyS2SServiceAsyncCallback);
}

function startApplyS2SServiceAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            getIBMBStatus();
            if (collectionData["s2sBusinessHrsFlag"] == "true") {
                // call if current time falls in between provided business hours
                gblSSServieHours = "applyServiceHours";
                getIBMBStatus();
            } else {
                showAlert(kony.i18n.getLocalizedString("S2S_ApplyBusinessHours"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
            return false;
        }
    }
}
/** This method is used to get the ib, mb statuses **/
function getIBMBStatus() {
    showLoadingScreen();
    var inputParams = {
        crmId: ""
    };
    invokeServiceSecureAsync("crmProfileInq", inputParams, startcrmProfileInqServiceAsyncCallback);
}
/**************************************************************************************
		Module	: startcrmProfileInqServiceAsyncCallback
		Author  : Kony
		Date    : 
		Purpose : This method is used to get the ib and mb status values by inquiring the crmprofile
****************************************************************************************/
function startcrmProfileInqServiceAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            var statusCode = collectionData["statusCode"];
            var severity = collectionData["severity"];
            var statusDesc = collectionData["statusDesc"];
            if (statusCode != 0) {
                kony.application.dismissLoadingScreen();
                showAlert(collectionData["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
                return false;
            } else {
                var ibStat = collectionData["ibUserStatusIdTr"];
                var mbStat = collectionData["mbFlowStatusIdRs"];
                //gblEmailId=	collectionData["emailId"];   Commenting for DEF11920
                if (mbStat != null) {
                    var inputParams = {
                        mbStatus: mbStat,
                        ibStatus: ibStat
                    };
                    invokeServiceSecureAsync("checkIBMBStatus", inputParams, checkStatus);
                }
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
            gblExeS2S = "dummyJP"; // to control alter flow of S2S execution
            return false;
        }
    }
}

function checkStatus(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            // this will be made as true when we click on notification link. true = we are trying to execute s2s.
            if (gblExeS2S == "true") {
                if (flowSpa) {
                    if (collectionData["ibStatusFlag"] == "true") {
                        //alert("SPA")
                        showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        return false;
                    } else {
                        invokeRetailLiquidityTransferInq();
                    }
                } else {
                    if (collectionData["mbStatusFlag"] == "true") {
                        popTransferConfirmOTPLock.show();
                        //showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"),kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        return false;
                    } else {
                        invokeRetailLiquidityTransferInq();
                    }
                }
            } else {
                if (flowSpa) {
                    if (collectionData["ibStatusFlag"] == "true") {
                        //alert("SPA")
                        showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        return false;
                    } else {
                        getCustomerAccountInfo();
                    }
                } else {
                    if (collectionData["mbStatusFlag"] == "true") {
                        popTransferConfirmOTPLock.show();
                        //showAlert(kony.i18n.getLocalizedString("keyTranxPwdLocked"),kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        return false;
                    } else {
                        getCustomerAccountInfo();
                    }
                }
            }
        } else {
            if (gblExeS2S == "true") {
                onClickReturnS2S();
                gblExeS2S = "dummyJpB"; // to control flow
            }
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
            return false;
        }
    }
}
/** This method is used to get the account information for the respective crmid given **/
function getCustomerAccountInfo() {
    showLoadingScreen();
    if (gblExeS2S == "true") {
        s2sEditFlagVal = "false";
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sEditFlag: s2sEditFlagVal,
            s2sExeFlag: gblExeS2S,
            linkedAccount: linkdAcct,
            noFixedAccount: noFixedAcct
        };
    } else if (gbls2sEditFlag == "true") {
        gblExeS2S = "false";
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sEditFlag: gbls2sEditFlag,
            linkedAccount: linkdAcct,
            noFixedAccount: noFixedAcct,
            s2sExeFlag: gblExeS2S
        };
    } else {
        gblExeS2S = "false";
        var inputParams = {
            crmId: "",
            locale: kony.i18n.getCurrentLocale(),
            s2sEditFlag: gbls2sEditFlag,
            s2sExeFlag: gblExeS2S
        };
    }
    invokeServiceSecureAsync("s2sCustomerAccInq", inputParams, startCustAccountInqServiceAsyncCallback);
}

function startCustAccountInqServiceAsyncCallback(status, collectionData) {
    dismissLoadingScreen();
    if (status == 400) {
        collectionDataEdit = "";
        collectionDataExecute = "";
        if (collectionData["opstatus"] == 0) {
            var statusCode = collectionData["statusCode"];
            var severity = collectionData["severity"];
            var statusDesc = collectionData["statusDesc"];
            gblCurrentDateS2SMB = "";
            if (statusCode != 0) {
                showAlert(collectionData["acctStatusDesc"], kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
            } else {
                gblCurrentDateS2SMB = collectionData["currentDate"];
                if (gbls2sEditFlag == "false" && gblExeS2S == "false") {
                    eligibleS2SAcctInfo = collectionData["eligibleS2SAcctInfo"];
                    noFixedAcctInfo = collectionData["noFixedAcctInfo"];
                    if (noFixedAcctInfo.length > 0) {
                        //for(var i=0; i < noFixedAcctInfo.length; i++) {
                        if (noFixedAcctInfo[0]["isNoFixedExist"] == "true") {
                            gblNFOpnStats = noFixedAcctInfo[0]["isNoFixedExist"].trim();
                            gblNFActiStats = noFixedAcctInfo[0]["isNoFixedActive"].trim();
                            gblNFHidden = noFixedAcctInfo[0]["isNoFixedHidden"].trim();
                        } else if (noFixedAcctInfo[0]["isNoFixedExist"] == "false") {
                            gblNFOpnStats = noFixedAcctInfo[0]["isNoFixedExist"].trim();
                            gblNFActiStats = false;
                            gblNFHidden = false;
                        }
                    }
                    if (collectionData["eligibleS2SAcctInfo"].length != 0) {
                        frmSSSApply.show();
                        dismissLoadingScreen();
                    } else {
                        showAlert("you dont have any link account which is eligible", kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    }
                } else if (gbls2sEditFlag == "true") {
                    gblSSLinkedStatus = collectionData["linkedAcctStatus"];
                    gblNFHidden = collectionData["NoFixedAcctStatus"];
                    linkdAcct = linkdAcct.substring(linkdAcct.length - 10, linkdAcct.length);
                    noFixedAcct = noFixedAcct.substring(noFixedAcct.length - 10, noFixedAcct.length);
                    collectionDataEdit = collectionData;
                    //For DEF13096  //Resetting all the values as per tmb.properties file
                    gblSSTrnsLmtMin = collectionData["toMinAmount"];
                    gblSSTrnMinofMax = collectionData["toMaxAmount"];
                    gblSSTrnMaxofMin = collectionData["fromMinAmount"];
                    gblSSTrnsLmtMax = collectionData["fromMaxAmount"];
                    //--
                    var imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + collectionData["ICON_IDLINK"] + "&modIdentifier=PRODICON";
                    frmSSServiceED.imgLinkAcc.src = imageName;
                    frmSSServiceED.lblAmntLmtMax.text = commaFormatted(liqInqCollectionData["maxAmt"]);
                    frmSSServiceED.lblAmntLmtMin.text = commaFormatted(liqInqCollectionData["minAmt"]);
                    frmSSServiceED.lblFromAccNo.text = formatAccountNo(linkdAcct);
                    frmSSServiceED.lblToAccNo.text = formatAccountNo(noFixedAcct);
                    frmSSServiceED.lblFromAccBal.text = commaFormatted(collectionData["lblFromAccBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    //	frmSSServiceED.lblCurrencyLogo1.text = kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmSSServiceED.lblFromAccName.text = collectionData["lblFromAccName"];
                    frmSSServiceED.lblFromAccType.text = collectionData["lblFromAccType"];
                    frmSSServiceED.lblToAccBal.text = commaFormatted(collectionData["lblToAccBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    //	frmSSServiceED.lblCurrencyLogo2.text = kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmSSServiceED.lblToAccName.text = collectionData["lblToAccName"];
                    frmSSServiceED.lblToAccType.text = collectionData["lblToAccType"];
                    //if() both Accounts {No fixed and Linked account} are in active state and not in Hidden State
                    gbls2sEditFlag = "dummyZeet";
                    frmSSServiceED.show();
                } else if (gblExeS2S == "true") {
                    /*
                     * a/c icon for Send From Account
                     */
                    var icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + collectionData["ICON_IDLINKED"] + "&modIdentifier=PRODICON";
                    /*if(collectionData["lblExeFromAccProdID"] == "219" || collectionData["lblExeFromAccProdID"] == "220" || collectionData["lblExeFromAccProdID"] == "222"){
							icon="prod_nofee.png"
						}
						else if(collectionData["lblExeFromAccProdID"] == "221" || collectionData["lblExeFromAccProdID"] == "203" || collectionData["lblExeFromAccProdID"] == "206"){
							icon="prod_savingd.png"
						}
						else{
							icon="prod_currentac.png"
						}	
            			*/
                    /*
                     * a/c icon for Save To Account
                     */
                    var icon1 = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + collectionData["ICON_IDNOFIXED"] + "&modIdentifier=PRODICON";
                    /*if(collectionData["lblExeToAccProdID"] == "219" || collectionData["lblExeToAccProdID"] == "220" || collectionData["lblExeToAccProdID"] == "222"){
							icon1="prod_nofee.png"
						}
						else if(collectionData["lblExeToAccProdID"] == "221" || collectionData["lblExeToAccProdID"] == "203" || collectionData["lblExeToAccProdID"] == "206"){
							icon1="prod_savingd.png"
						}
						else{
							icon1="prod_currentac.png"
						}	
            	        */
                    collectionDataExecute = collectionData;
                    frmSSSExecute.imgFromAcc.src = icon;
                    frmSSSExecute.imgToAcc.src = icon1;
                    exeFromAcctName = collectionData["lblExeFromAccName"];
                    exeFromAcctType = collectionData["lblExeFromAccType"];
                    exeToAcctName = collectionData["lblExeToAccName"];
                    //exeToAcctType = collectionData["lblFromAccType"];
                    exeToAcctType = collectionData["lblFromAccType"];
                    var exeToAccountProdId = collectionData["lblExeToAccProdID"];
                    //getMinMaxAmounts();
                    finActivityLog(); // to get reference id
                }
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
            if (gblExeS2S == "true") {
                onClickReturnS2S();
                gblExeS2S = "JpBXYB"; // to control flow
            }
            return false;
        }
    }
}
// This method is to get the status of S2S. Whether the customer already applied for S2S or not.
function getApplyS2SStatus() {
    showLoadingScreen();
    gblRetryCountRequestOTP = "0";
    var inputParams = {}
    invokeServiceSecureAsync("RetailLiquidityInquiry", inputParams, startRetailLiqInqAsyncCallback);
}

function startRetailLiqInqAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            // checking already applied or not 
            //As of now date is using have to check in vit/previt env with sample req
            //
            if (collectionData["statusCode"] == "0") {
                if (collectionData["originalDtRegister"] != 0) {
                    gblSSApply = "false";
                    gblExeS2S = "false";
                    gbls2sEditFlag = "true";
                    linkdAcct = collectionData["acctIdentValue"];
                    noFixedAcct = collectionData["partyId"];
                    liqInqCollectionData = collectionData;
                    getCustomerAccountInfo(); // Edit/Delete flow: fetching linked account and nofixed account details for edit flow
                } else {
                    gbls2sEditFlag = "false";
                    gblExeS2S = "false";
                    gblSSApply = "true";
                    checkBusinessHoursForS2S(); // Send to save Apply flow
                }
            } else if (collectionData["serverStatusCode"] == 15) {
                gbls2sEditFlag = "false";
                gblExeS2S = "false";
                gblSSApply = "true";
                checkBusinessHoursForS2S(); // Send to save Apply flow
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString(collectionData["statusDesc"]), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
            return false;
        }
    }
}

function callBackVerifyTxnPwdApply(status, result) {
    if (result["opstatusVPX"] == "0") {
        //popupTractPwd.dismiss();
        //dismissLoadingScreen();
        if (flowSpa) gblVerifyOTPCounter = "0";
        gblRetryCountRequestOTP = "0";
        if (!flowSpa) popupTractPwd.dismiss();
        if (result["opstatus"] == "0") {
            frmSSConfirmation.label47422528158852.text = result["currentDate"];
            showS2SActivationCompleteFlow();
        } else {
            dismissLoadingScreen();
            alert(result["errMsg"]);
            return false;
        }
    } else {
        if (flowSpa) {
            dismissLoadingScreen();
            if (result["opstatusVPX"] == "8005") {
                if (result["errCode"] == "VrfyOTPErr00001") {
                    gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                    popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                    popOtpSpa.lblPopupTract4Spa.text = "";
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00002") {
                    gblVerifyOTPCounter = "0";
                    popOtpSpa.dismiss();
                    popTransferConfirmOTPLock.show();
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00005") {
                    showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00006") {
                    gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                    alert("" + result["errMsg"]);
                    return false;
                } else {
                    alert("" + result["errMsg"]);
                    return false;
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
            }
        } else {
            gblRtyCtrVrfyTxPin = result["retryCounterVerifyTransPwd"];
            gblRtyCtrVrfyAxPin = result["retryCounterVerifyAccessPin"];
            dismissLoadingScreen();
            if (result["errCode"] == "VrfyTxPWDErr00003") {
                showTranPwdLockedPopup();
            } else {
                //alert("" + kony.i18n.getLocalizedString("KeyIncorrectPWD"));
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
                popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
                popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
                popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd")
                gblVerifyOTP = gblVerifyOTP + 1;
                return false;
            }
        }
        return false;
    }
}

function s2sVerifyTxnPwdApply(trnsPwd) {
    showLoadingPopUpScreen();
    var inputParam = {};
    setNotificationAddMB(inputParam);
    setS2SLiquidityAddServiceMB(inputParam)
    var txtBalMaxVal = frmSSConfirmation.lblAmntMax.text
    txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmSSConfirmation.lblAmntMin.text
    txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    inputParam["flowspa"] = false;
    inputParam["channel"] = "rc";
    inputParam["activationActivityLog_channelId"] = "02";
    if (flowSpa) inputParam["activationActivityLog_channelId"] = "01";
    inputParam["activationActivityLog_activityTypeID"] = "035";
    inputParam["activationActivityLog_activityFlexValues1"] = gblPHONENUMBER;
    inputParam["activationActivityLog_activityFlexValues2"] = removeUnwatedSymbols(frmSSConfirmation.lblLinkAccNo.text);
    inputParam["activationActivityLog_activityFlexValues3"] = removeUnwatedSymbols(frmSSConfirmation.lblNFAccNo.text);
    inputParam["activationActivityLog_activityFlexValues4"] = removeCommas(txtBalMaxVal);
    inputParam["activationActivityLog_activityFlexValues4"] = removeCommas(txtBalMinVal);
    inputParam["appID"] = appConfig.appId;
    if (flowSpa) {
        inputParam["flowspa"] = "true";
        inputParam["channel"] = "wap";
        if (gblTokenSwitchFlag) {
            inputParam["gblTokenSwitchFlag"] = "true";
            inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
            inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
            inputParam["verifyTokenEx_userId"] = gblUserName;
            inputParam["verifyTokenEx_password"] = trnsPwd;
            inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        } else {
            inputParam["gblTokenSwitchFlag"] = "false";
            inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
            inputParam["verifyPasswordEx_password"] = trnsPwd;
            inputParam["verifyPasswordEx_userId"] = gblUserName;
        }
    } else {
        inputParam["flowspa"] = "false";
    }
    inputParam["verifyPassword_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["verifyPassword_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["verifyPassword_password"] = trnsPwd;
    if (trnsPwd == "") {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        dismissLoadingScreen();
    } else {
        invokeServiceSecureAsync("SendToSaveApplyConfirm", inputParam, callBackVerifyTxnPwdApply);
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
}

function setS2SLiquidityAddServiceMB(inputParam) {
    var txtBalMaxVal = frmSSConfirmation.lblAmntMax.text
    txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmSSConfirmation.lblAmntMin.text
    txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    inputParam["retailLiquidityAdd_mobileNumber"] = gblPHONENUMBER;
    inputParam["retailLiquidityAdd_acctIdentValue"] = removeUnwatedSymbols(frmSSConfirmation.lblLinkAccNo.text);
    inputParam["retailLiquidityAdd_partyId"] = noFixedAcctInfo[0]["acctIdentValue"];
    inputParam["retailLiquidityAdd_maxAmt"] = removeCommas(txtBalMaxVal);
    inputParam["retailLiquidityAdd_minAmt"] = removeCommas(txtBalMinVal);
}
// populate No Fixed Account details on frmSSService page
function s2sNoFixedAcctPopulate() {
    var locale = kony.i18n.getCurrentLocale();
    if (gblNFOpnStats == "true" && gblNFActiStats == "true" && gblNFHidden == "true") {
        if (locale == "en_US") {
            if (noFixedAcctInfo[0]["personalizedAcctNickNameEN"] == null || noFixedAcctInfo[0]["personalizedAcctNickNameEN"] == '') frmSSService.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickName"];
            else frmSSService.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickNameEN"];
            if (noFixedAcctInfo[0]["productNameEN"] == null || noFixedAcctInfo[0]["productNameEN"] == '') frmSSService.lblNFAccType.text = noFixedAcctInfo[0]["productName"]
            else frmSSService.lblNFAccType.text = noFixedAcctInfo[0]["productNameEN"]
        } else {
            if (noFixedAcctInfo[0]["personalizedAcctNickNameTH"] == null || noFixedAcctInfo[0]["personalizedAcctNickNameTH"] == '') frmSSService.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickName"];
            else frmSSService.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickNameTH"];
            if (noFixedAcctInfo[0]["productNameTH"] == null || noFixedAcctInfo[0]["productNameTH"] == '') frmSSService.lblNFAccType.text = noFixedAcctInfo[0]["productName"]
            else frmSSService.lblNFAccType.text = noFixedAcctInfo[0]["productNameTH"]
        }
        frmSSService.image2448366816169765 = "piggyico.png";
        frmSSService.lblNFAccBal.text = commaFormatted(noFixedAcctInfo[0]["availableAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); // + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmSSService.lblAccNo.text = kony.i18n.getLocalizedString("AccountNoLabel") + ": ";
        frmSSService.lblNFAccNo.text = formatAccountNo(noFixedAcctInfo[0]["acctIdentValue"]);
    }
}
// add data using liquidityadd service
function addS2SDetailsToLiquidityAdd() {
    showLoadingScreen();
    var txtBalMaxVal = frmSSConfirmation.lblAmntMax.text
    txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmSSConfirmation.lblAmntMin.text
    txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    // mobileNumber will be picked from session in preprocessor
    var inputParams = {
        acctIdentValue: removeUnwatedSymbols(frmSSConfirmation.lblLinkAccNo.text),
        partyId: noFixedAcctInfo[0]["acctIdentValue"],
        maxAmt: removeCommas(txtBalMaxVal),
        minAmt: removeCommas(txtBalMinVal)
    };
    invokeServiceSecureAsync("RetailLiquidityAdd", inputParams, startRetailLiqAddAsyncCallback);
}

function startRetailLiqAddAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            var statusCode = collectionData["statusCode"];
            var severity = collectionData["severity"];
            var statusDesc = collectionData["statusDesc"];
            if (collectionData["statusCode"] == 0 && collectionData["addStatusCode"] == 0) {
                gblConfOrComp = true; //related to UI 
                frmSSConfirmation.label47422528158852.text = collectionData["currentDate"];
                showS2SActivationCompleteFlow(); // To show complete screen
                sendNotification();
            } else {
                if (collectionData["addStatusCode"] != 0) {
                    s2s_activityStatus = "02";
                    s2s_errorCode = collectionData["addStatusCode"];
                    callApplyS2SActivityLogForS2SActiComplete(s2s_activityStatus);
                    if (collectionData["addStatusDesc"].indexOf("{") != -1 && collectionData["addStatusDesc"].indexOf("}") != -1) {
                        var alertMsg = collectionData["addStatusDesc"].split("{");
                        alertMsg = alertMsg[1].split("}")
                        showAlert(alertMsg.toString().replace(",", ""), kony.i18n.getLocalizedString("info"));
                    } else {
                        showAlert(collectionData["addStatusDesc"], kony.i18n.getLocalizedString("info"));
                    }
                    dismissLoadingScreen();
                    return false;
                }
            }
        } else {
            s2s_activityStatus = "02";
            s2s_errorCode = collectionData["opstatus"];
            callApplyS2SActivityLogForS2SActiComplete(s2s_activityStatus);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
        }
    }
}
/** This function is used to check the business hours for S2S when click on confirm **/
function checkBusinessHoursBeforeConfirm() {
    var inputParams = {
        myProfileFlag: "false"
    };
    invokeServiceSecureAsync("checkS2SBusinessHoursBeforeConfirm", inputParams, startCheckBusinessHoursAsyncCallback);
}

function startCheckBusinessHoursAsyncCallback(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            if (collectionData["s2sBusinessHrsFlag"] == "true") {
                if (checkBusinessHoursBeforeConfirm) {
                    //gblSSServieHours = "applyServiceHours";
                    if (flowSpa) {
                        if (gblIBFlowStatus == "04") {
                            //showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
                            popTransferConfirmOTPLock.show();
                            return false;
                        } else {
                            var inputParams = {}
                            spaChnage = "SendtoSave"
                            gblOTPFlag = true;
                            gblOnClickReq = false;
                            try {
                                kony.timer.cancel("otpTimer")
                            } catch (e) {}
                            //input params for generateotpwithuser
                            gblSpaChannel = "ApplyS2S"
                            onClickOTPRequestSpa();
                        }
                    } else {
                        showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", onClickSSConfrm, 3);
                    }
                }
            } else {
                s2s_activityStatus = "01";
                activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
                alert("You Can't apply for Send To Save");
            }
        } else {
            s2s_activityStatus = "02";
            activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}
// Send to save notification add call
function sendNotification() {
    showLoadingScreen();
    var deliveryMethod = "Email";
    var inputParam = {}
    var locale = kony.i18n.getCurrentLocale();
    //inputParam["channelName"] ="MB-INQ";
    inputParam["Locale"] = kony.i18n.getCurrentLocale(); //"en_US";
    //inputParam["emailId"]= gblEmailId;
    inputParam["customerName"] = gblCustomerName;
    inputParam["notificationType"] = deliveryMethod;
    if (gblExeS2S == "randonXjp") {
        var maxAmount = (frmSSSExecute.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        var minAmount = (frmSSSExecute.lblAmntLmtMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        inputParam["source"] = "sendEmailForS2SExecution";
        inputParam["lblAmntMax"] = removeCommas(maxAmount);
        inputParam["lblAmntMin"] = removeCommas(minAmount);
        inputParam["lblFromAccName"] = frmSSSExecute.lblSendFromAccName.text;
        inputParam["lblFromAccNo"] = (frmSSSExecute.lblSendFromAccNo.text).substring(6, 11);
        inputParam["lblToAccName"] = frmSSSExecute.lblSendToAccName.text;
        inputParam["lblToAccNo"] = (frmSSSExecute.lblSendToAccNo.text).substring(6, 11);
        inputParam["lblTransAmnt"] = frmSSSExecute.lblTrnsAmnt.text;
        inputParam["lblRefNo"] = frmSSSExecute.lblTrnsRefNo.text;
    } else {
        var maxAmountA = (frmSSConfirmation.lblAmntMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        var minAmountA = (frmSSConfirmation.lblAmntMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        inputParam["source"] = "sendEmailForS2S"
        inputParam["fromAcctNick"] = frmSSConfirmation.lblLinkAccName.text;
        inputParam["fromAccNo"] = (frmSSConfirmation.lblLinkAccNo.text).substring(6, 11);
        inputParam["toAccNick"] = frmSSConfirmation.lblNFAccName.text;
        inputParam["toAccNo"] = (frmSSConfirmation.lblNFAccNo.text).substring(6, 11);
        inputParam["amntTrnsFromNF"] = removeCommas(maxAmountA);
        inputParam["amntTrnsToNF"] = removeCommas(minAmountA);
        inputParam["date"] = "date";
    }
    invokeServiceSecureAsync("NotificationAdd", inputParam, callBackS2SNotificationAddService)
}

function callBackS2SNotificationAddService(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            dismissLoadingScreen();
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                s2s_activityStatus = "01";
                if (gblExeS2S == "randonXjp") {
                    callApplyS2SActivityLogForS2SExecutionComplete(s2s_activityStatus);
                    gblExeS2S = "dummyDCZ"; // to control flow
                } else {
                    callApplyS2SActivityLogForS2SActiComplete(s2s_activityStatus);
                }
            } else {
                if (result["addStatusCode"] != 0) {
                    s2s_activityStatus = "02";
                    if (gblExeS2S == "randonXjp") {
                        callApplyS2SActivityLogForS2SExecutionComplete(s2s_activityStatus);
                        gblExeS2S = "dummyDCZ"; // to control flow
                    } else {
                        callApplyS2SActivityLogForS2SActiComplete(s2s_activityStatus);
                    }
                    s2s_errorCode = result["errMsg"];
                    showAlert(result["additionalStatusDesc"], kony.i18n.getLocalizedString("info"));
                    dismissLoadingScreen();
                }
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
        }
    }
}

function onClickReturnS2S() {
    //frmAccountSummaryLanding =null;
    //    frmAccountSummaryLandingGlobals();
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    showAccuntSummaryScreen();
}

function finActivityLog() {
    var inputParam = {}
    inputParam["transRefType"] = "SS";
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, callBackgetS2STransRefNoMB)
}

function callBackgetS2STransRefNoMB(status, collectionData) {
    if (status == 400) {
        if (collectionData["opstatus"] == 0) {
            if (collectionData["errCode"] == "") {
                transRefNumForS2S = collectionData["transRefNum"];
                s2sExeFormMapping(transRefNumForS2S);
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
        }
    }
}

function createFinActivityLogObj() {
    GBLFINANACIALACTIVITYLOG.crmId = retailLiqTranInqRslt["crmId"];
    GBLFINANACIALACTIVITYLOG.finTxnRefId = transRefNumForS2S + "00";
    var date = frmSSSExecute.lblTransDate.text;
    if (date != null) date = kony.string.replace(date, "/", "");
    else date = "000000";
    GBLFINANACIALACTIVITYLOG.finTxnDate = date;
    GBLFINANACIALACTIVITYLOG.activityTypeId = "038";
    GBLFINANACIALACTIVITYLOG.channelId = "02";
    GBLFINANACIALACTIVITYLOG.fromAcctId = removeUnwatedSymbols(frmSSSExecute.lblSendFromAccNo.text);
    GBLFINANACIALACTIVITYLOG.fromAcctName = exeFromAcctType;
    GBLFINANACIALACTIVITYLOG.fromAcctNickname = exeFromAcctName;
    GBLFINANACIALACTIVITYLOG.toAcctId = removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
    GBLFINANACIALACTIVITYLOG.toAcctName = exeToAcctType;
    GBLFINANACIALACTIVITYLOG.toAcctNickname = exeToAcctName;
    //	GBLFINANACIALACTIVITYLOG.bankCD = "11";
    GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
    GBLFINANACIALACTIVITYLOG.finTxnAmount = removeCommas(frmSSSExecute.lblTrnsAmnt.text);
    GBLFINANACIALACTIVITYLOG.finTxnBalance = retailLiqTrnsAdd["FromAmt"];
    if (retailLiqTrnsAdd["TransferStatus"] == "S" || retailLiqTrnsAdd["TransferStatus"] == "s") {
        GBLFINANACIALACTIVITYLOG.finTxnStatus = "01";
        GBLFINANACIALACTIVITYLOG.clearingStatus = "01";
    } else {
        GBLFINANACIALACTIVITYLOG.finTxnStatus = "02";
        GBLFINANACIALACTIVITYLOG.clearingStatus = "02";
    }
    if (retailLiqTrnsAdd["statusCode"] != 0) {
        GBLFINANACIALACTIVITYLOG.errorCd = retailLiqTrnsAdd["addStatusCode"];
    } else {
        GBLFINANACIALACTIVITYLOG.errorCd = retailLiqTrnsAdd["addStatusCode"];
    }
    GBLFINANACIALACTIVITYLOG.eventId = transRefNumForS2S.replace("SS", "");
    GBLFINANACIALACTIVITYLOG.txnType = "008";
    if (exeToAccountProdId == "221") GBLFINANACIALACTIVITYLOG.sendToSavePoint = "1";
    else GBLFINANACIALACTIVITYLOG.sendToSavePoint = "0";
    GBLFINANACIALACTIVITYLOG.finFlexValues1 = retailLiqTranInqRslt["mobileNumber"];
    GBLFINANACIALACTIVITYLOG.finFlexValues2 = frmSSSExecute.lblTrnsAmnt.text;
    GBLFINANACIALACTIVITYLOG.finFlexValues3 = retailLiqTrnsAdd["FromAcctNo"];
    GBLFINANACIALACTIVITYLOG.finFlexValues4 = retailLiqTrnsAdd["ToAcctNo"];
    financialActivityLogForS2SMB(GBLFINANACIALACTIVITYLOG);
}

function financialActivityLogForS2SMB(CRMFinancialActivityLogAddRequest) {
    showLoadingScreen();
    inputParam = {};
    inputParam["crmId"] = CRMFinancialActivityLogAddRequest.crmId;
    inputParam["finTxnRefId"] = CRMFinancialActivityLogAddRequest.finTxnRefId;
    inputParam["finTxnDate"] = CRMFinancialActivityLogAddRequest.finTxnDate;
    inputParam["activityTypeId"] = CRMFinancialActivityLogAddRequest.activityTypeId;
    inputParam["txnCd"] = CRMFinancialActivityLogAddRequest.txnCd;
    inputParam["tellerId"] = CRMFinancialActivityLogAddRequest.tellerId;
    inputParam["txnDescription"] = CRMFinancialActivityLogAddRequest.txnDescription;
    inputParam["finLinkageId"] = CRMFinancialActivityLogAddRequest.finLinkageId;
    inputParam["channelId"] = CRMFinancialActivityLogAddRequest.channelId;
    inputParam["fromAcctId"] = CRMFinancialActivityLogAddRequest.fromAcctId;
    inputParam["toAcctId"] = CRMFinancialActivityLogAddRequest.toAcctId;
    inputParam["toBankAcctCd"] = CRMFinancialActivityLogAddRequest.toBankAcctCd;
    inputParam["finTxnAmount"] = CRMFinancialActivityLogAddRequest.finTxnAmount;
    inputParam["finTxnFee"] = CRMFinancialActivityLogAddRequest.finTxnFee;
    inputParam["finTxnBalance"] = CRMFinancialActivityLogAddRequest.finTxnBalance;
    inputParam["finTxnMemo"] = CRMFinancialActivityLogAddRequest.finTxnMemo;
    inputParam["billerRef1"] = CRMFinancialActivityLogAddRequest.billerRef1;
    inputParam["billerRef2"] = CRMFinancialActivityLogAddRequest.billerRef2;
    inputParam["noteToRecipient"] = CRMFinancialActivityLogAddRequest.noteToRecipient;
    inputParam["recipientMobile"] = CRMFinancialActivityLogAddRequest.recipientMobile;
    inputParam["recipientEmail"] = CRMFinancialActivityLogAddRequest.recipientEmail;
    inputParam["finTxnStatus"] = CRMFinancialActivityLogAddRequest.finTxnStatus;
    inputParam["smartFlag"] = CRMFinancialActivityLogAddRequest.smartFlag;
    inputParam["clearingStatus"] = CRMFinancialActivityLogAddRequest.clearingStatus;
    inputParam["finSchduleRefId"] = CRMFinancialActivityLogAddRequest.finSchduleRefId;
    inputParam["billerCommCode"] = CRMFinancialActivityLogAddRequest.billerCommCode;
    inputParam["fromAcctName"] = CRMFinancialActivityLogAddRequest.fromAcctName;
    inputParam["fromAcctNickname"] = CRMFinancialActivityLogAddRequest.fromAcctNickname;
    inputParam["toAcctName"] = CRMFinancialActivityLogAddRequest.toAcctName;
    inputParam["toAcctNickname"] = CRMFinancialActivityLogAddRequest.toAcctNickname;
    inputParam["billerCustomerName"] = CRMFinancialActivityLogAddRequest.billerCustomerName;
    inputParam["billerBalance"] = CRMFinancialActivityLogAddRequest.billerBalance;
    inputParam["activityRefId"] = CRMFinancialActivityLogAddRequest.activityRefId;
    inputParam["eventId"] = CRMFinancialActivityLogAddRequest.eventId;
    inputParam["errorCd"] = CRMFinancialActivityLogAddRequest.errorCd;
    inputParam["txnType"] = CRMFinancialActivityLogAddRequest.txnType;
    inputParam["tdInterestAmount"] = CRMFinancialActivityLogAddRequest.tdInterestAmount;
    inputParam["tdTaxAmount"] = CRMFinancialActivityLogAddRequest.tdTaxAmount;
    inputParam["tdPenaltyAmount"] = CRMFinancialActivityLogAddRequest.tdPenaltyAmount;
    inputParam["tdNetAmount"] = CRMFinancialActivityLogAddRequest.tdNetAmount;
    inputParam["tdMaturityDate"] = CRMFinancialActivityLogAddRequest.tdMaturityDate;
    inputParam["remainingFreeTxn"] = CRMFinancialActivityLogAddRequest.remainingFreeTxn;
    inputParam["toAccountBalance"] = CRMFinancialActivityLogAddRequest.toAccountBalance;
    inputParam["sendToSavePoint"] = CRMFinancialActivityLogAddRequest.sendToSavePoint;
    inputParam["openTdTerm"] = CRMFinancialActivityLogAddRequest.openTdTerm;
    inputParam["openTdInterestRate"] = CRMFinancialActivityLogAddRequest.openTdInterestRate;
    inputParam["openTdMaturityDate"] = CRMFinancialActivityLogAddRequest.openTdMaturityDate;
    inputParam["affiliatedAcctNickname"] = CRMFinancialActivityLogAddRequest.affiliatedAcctNickname;
    inputParam["affiliatedAcctId"] = CRMFinancialActivityLogAddRequest.affiliatedAcctId;
    inputParam["beneficialFirstname"] = CRMFinancialActivityLogAddRequest.beneficialFirstname;
    inputParam["beneficialLastname"] = CRMFinancialActivityLogAddRequest.beneficialLastname;
    inputParam["relationship"] = CRMFinancialActivityLogAddRequest.relationship;
    inputParam["percentage"] = CRMFinancialActivityLogAddRequest.percentage;
    inputParam["finFlexValues1"] = CRMFinancialActivityLogAddRequest.finFlexValues1;
    inputParam["finFlexValues2"] = CRMFinancialActivityLogAddRequest.finFlexValues2;
    inputParam["finFlexValues3"] = CRMFinancialActivityLogAddRequest.finFlexValues3;
    inputParam["finFlexValues4"] = CRMFinancialActivityLogAddRequest.finFlexValues4;
    inputParam["finFlexValues5"] = CRMFinancialActivityLogAddRequest.finFlexValues5;
    inputParam["dueDate"] = CRMFinancialActivityLogAddRequest.dueDate;
    inputParam["beepAndBillTxnId"] = CRMFinancialActivityLogAddRequest.beepAndBillTxnId;
    invokeServiceSecureAsync("financialActivityLog", inputParam, callBackFinActivityLogForS2SMB)
}

function callBackFinActivityLogForS2SMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            sendNotification();
            dismissLoadingScreen();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
        }
    }
}
/*
 * function to control UI and display S2S apply complete screen
 */
function showS2SActivationCompleteFlow() {
    gblConfOrComp = true; //To control UI of S2S apply confirmation/complete screen
    // To show complete page on success of verifypwd
    frmSSConfirmation.hbxCompIcon.setVisibility(true);
    frmSSConfirmation.hbxCompIcon.isVisible = true;
    frmSSConfirmation.hbxAdv.setVisibility(true);
    frmSSConfirmation.hbxAdv.isVisible = true;
    frmSSConfirmation.btnRight.setVisibility(true);
    frmSSConfirmation.hboxApplyDate.isVisible = true;
    frmSSConfirmation.btnRight.containerWeight = 16;
    if (flowSpa) {
        frmSSConfirmation.btnCancelSpa.text = kony.i18n.getLocalizedString("keybtnReturn");
        frmSSConfirmation.btnConfirmSpa.text = kony.i18n.getLocalizedString("keySendToSaveCompleteLabel");
    } else {
        frmSSConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keybtnReturn");
        frmSSConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keySendToSaveCompleteLabel");
    }
    frmSSConfirmation.show();
    campaginService("image2447443295186", "frmSSConfirmation", "M");
}

function setNotificationAddMB(inputParam) {
    var deliveryMethod = "Email";
    var deliveryMethod = "Email";
    var maxAmountA = (frmSSConfirmation.lblAmntMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var minAmountA = (frmSSConfirmation.lblAmntMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var locale = kony.i18n.getCurrentLocale();
    inputParam["Locale"] = kony.i18n.getCurrentLocale(); //"en_US";
    //inputParam["emailId"]= gblEmailId;
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_notificationType"] = deliveryMethod;
    inputParam["NotificationAdd_source"] = "sendEmailForS2S"
    inputParam["NotificationAdd_fromAcctNick"] = frmSSConfirmation.lblLinkAccName.text;
    inputParam["NotificationAdd_fromAccNo"] = "xxx-x-" + (frmSSConfirmation.lblLinkAccNo.text).substring(6, 11) + "-x"; //(frmSSConfirmation.lblLinkAccNo.text).substring(6,11);
    inputParam["NotificationAdd_toAccNick"] = frmSSConfirmation.lblNFAccName.text;
    inputParam["NotificationAdd_toAccNo"] = "xxx-x-" + (frmSSConfirmation.lblNFAccNo.text).substring(6, 11) + "-x";
    inputParam["NotificationAdd_amntTrnsFromNF"] = commaFormatted(removeCommas(maxAmountA))
    inputParam["NotificationAdd_amntTrnsToNF"] = commaFormatted(removeCommas(minAmountA))
}

function s2sVerifyTxnPwdEdit(trnsPwd) {
    showLoadingPopUpScreen();
    var inputParam = {};
    setRetailLiquidityModInputParamMB(inputParam);
    var txtBalMax = (frmSSService.txtBalMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMin = (frmSSService.txtBalMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    inputParam["flowspa"] = false;
    inputParam["channel"] = "MB";
    if (flowSpa) inputParam["activationActivityLog_channelId"] = "01";
    else inputParam["activationActivityLog_channelId"] = "02";
    inputParam["appID"] = appConfig.appId;
    //activitylogging
    inputParam["activationActivityLog_activityTypeID"] = "037"
    inputParam["activationActivityLog_activityFlexValues1"] = gblPHONENUMBER;
    inputParam["activationActivityLog_activityFlexValues2"] = removeUnwatedSymbols(frmSSService.lblFromAccNo.text);
    inputParam["activationActivityLog_activityFlexValues3"] = removeUnwatedSymbols(frmSSService.lblNFAccNo.text);
    inputParam["activationActivityLog_activityFlexValues4"] = removeCommas(txtBalMax);
    inputParam["activationActivityLog_activityFlexValues4"] = removeCommas(txtBalMin);
    if (flowSpa) {
        inputParam["flowspa"] = "true";
        inputParam["channel"] = "wap";
        if (gblTokenSwitchFlag) {
            inputParam["gblTokenSwitchFlag"] = true;
            inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
            inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
            inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
            inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
            inputParam["verifyTokenEx_userId"] = gblUserName;
            inputParam["verifyTokenEx_password"] = trnsPwd;
            inputParam["verifyTokenEx_sessionVal"] = "";
            inputParam["verifyTokenEx_segmentId"] = "segmentId";
            inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
            inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        } else {
            inputParam["gblTokenSwitchFlag"] = "false";
            inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
            inputParam["verifyPasswordEx_password"] = trnsPwd;
            inputParam["verifyPasswordEx_userId"] = gblUserName;
        }
    } else {
        inputParam["flowspa"] = "false";
    }
    inputParam["verifyPassword_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["verifyPassword_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["verifyPassword_password"] = trnsPwd;
    if (trnsPwd == "") {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        dismissLoadingScreen();
    } else {
        invokeServiceSecureAsync("SendToSaveEditConfirm", inputParam, callBackVerifyTxnPwdEdit);
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
}

function callBackVerifyTxnPwdEdit(status, result) {
    if (result["opstatusVPX"] == "0") {
        //popupTractPwd.dismiss();
        dismissLoadingScreen();
        if (flowSpa) gblVerifyOTPCounter = "0";
        gblRetryCountRequestOTP = "0";
        if (!flowSpa) popupTractPwd.dismiss();
        if (result["opstatus"] == "0") {
            frmSSServiceED.lblAmntLmtMax.text = commaFormatted(result["MaxAmount"]);
            frmSSServiceED.lblAmntLmtMin.text = commaFormatted(result["MinAmount"]);
            gblSSApply = true;
            frmSSServiceED.show();
        } else {
            dismissLoadingScreen();
            alert(result["errMsg"]);
            return false;
        }
    } else {
        if (flowSpa) {
            dismissLoadingScreen();
            if (result["opstatusVPX"] == "8005") {
                if (result["errCode"] == "VrfyOTPErr00001") {
                    gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                    popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                    popOtpSpa.lblPopupTract4Spa.text = "";
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00002") {
                    popOtpSpa.dismiss();
                    popTransferConfirmOTPLock.show();
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00005") {
                    showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00006") {
                    gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                    alert("" + result["errMsg"]);
                    return false;
                } else {
                    alert("" + result["errMsg"]);
                    return false;
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
            }
        } else {
            gblRtyCtrVrfyTxPin = result["retryCounterVerifyTransPwd"];
            gblRtyCtrVrfyAxPin = result["retryCounterVerifyAccessPin"];
            dismissLoadingScreen();
            if (result["errCode"] == "VrfyTxPWDErr00003") {
                showTranPwdLockedPopup();
            } else {
                //alert("" + kony.i18n.getLocalizedString("KeyIncorrectPWD"));
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
                popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
                popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
                popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd")
                gblVerifyOTP = gblVerifyOTP + 1;
                return false;
            }
        }
        return false;
    }
}

function setRetailLiquidityModInputParamMB(inputParam) {
    var txtBalMaxVal = frmSSService.txtBalMax.text
    txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmSSService.txtBalMin.text
    txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    inputParam["retailLiquidityMod_acctIdentValue"] = linkdAcct;
    inputParam["retailLiquidityMod_partyId"] = noFixedAcct;
    inputParam["retailLiquidityMod_maxAmt"] = removeCommas(txtBalMaxVal);
    inputParam["retailLiquidityMod_minAmt"] = removeCommas(txtBalMinVal);
}

function s2sVerifyTxnPwdExecute(trnsPwd) {
    showLoadingPopUpScreen();
    var txtBalMax = frmSSSExecute.lblAmntLmtMax.text;
    var txtBalMin = frmSSSExecute.lblAmntLmtMin.text;
    var inputParam = {};
    setFinActivityInputParamMB(inputParam);
    setNotificationAddServiceMB(inputParam);
    inputParam["flowspa"] = false;
    inputParam["channel"] = "rc";
    if (flowSpa) inputParam["activationActivityLog_channelId"] = "01";
    else inputParam["activationActivityLog_channelId"] = "02";
    inputParam["appID"] = appConfig.appId;
    //activitylogging
    inputParam["activationActivityLog_activityTypeID"] = "038"
    inputParam["activationActivityLog_activityFlexValues1"] = gblPHONENUMBER;
    inputParam["activationActivityLog_activityFlexValues2"] = removeUnwatedSymbols(frmSSSExecute.lblSendFromAccNo.text);
    inputParam["activationActivityLog_activityFlexValues3"] = removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
    inputParam["activationActivityLog_activityFlexValues4"] = removeCommas(frmSSSExecute.lblTrnsAmnt.text);
    //inputParam["activationActivityLog_activityFlexValues4"]=removeCommas(txtBalMin);	
    if (flowSpa) {
        inputParam["flowspa"] = "true";
        inputParam["channel"] = "wap";
        if (gblTokenSwitchFlag) {
            inputParam["gblTokenSwitchFlag"] = true;
            inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
            inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
            inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
            inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
            inputParam["verifyTokenEx_userId"] = gblUserName;
            inputParam["verifyTokenEx_password"] = trnsPwd;
            inputParam["verifyTokenEx_sessionVal"] = "";
            inputParam["verifyTokenEx_segmentId"] = "segmentId";
            inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
            inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        } else {
            inputParam["gblTokenSwitchFlag"] = "false";
            inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
            inputParam["verifyPasswordEx_password"] = trnsPwd;
            inputParam["verifyPasswordEx_userId"] = gblUserName;
        }
    } else {
        inputParam["flowspa"] = "false";
    }
    inputParam["verifyPassword_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["verifyPassword_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["verifyPassword_password"] = trnsPwd;
    if (trnsPwd == "") {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        dismissLoadingScreen();
    } else {
        invokeServiceSecureAsync("SendToSaveExecuteConfirm", inputParam, callBackVerifyTxnPwdExecute);
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
}

function callBackVerifyTxnPwdExecute(status, result) {
    if (result["opstatusVPX"] == "0") {
        //popupTractPwd.dismiss();
        //dismissLoadingScreen();
        if (flowSpa) gblVerifyOTPCounter = "0";
        gblRetryCountRequestOTP = "0";
        popOtpSpa.dismiss();
        if (!flowSpa) popupTractPwd.dismiss();
        if (result["opstatus"] == "0") {
            gblSSExcuteCnfrm = "transferS2Ssuccess";
            var date1 = result["TransferDate"];
            var dateSplit1 = date1.split("-");
            var formattedDate = dateSplit1[2] + "/" + dateSplit1[1] + "/" + dateSplit1[0].substring(dateSplit1[0].length - 2, dateSplit1[0].length);
            frmSSSExecute.lblFromAccBalAftr.text = commaFormatted(result["FromAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
            frmSSSExecute.lblSendToAccBal.text = commaFormatted(result["ToAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmSSSExecute.lblTransDate.text = gblCurrentDateS2SMB; //13/09/13, 2013-09-13
            frmSSSExecute.lblTrnsAmnt.text = commaFormatted(result["TranAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            // frmSSSExecute.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
            //frmSSSExecute.btnRight.setVisibility(true);
            //frmSSSExecute.hbxConfrmCancl.setVisibility(false);
            //frmSSSExecute.hbxCmptReturn.setVisibility(true);
            //frmSSSExecute.hboxSaveCamEmail.setVisibility(false);	
            //frmSSSExecute.show();
            completeExecute();
            //frmSSSExecute.lblTrnsRefNo.text = resultTbl["TrnId"];
        } else {
            dismissLoadingScreen();
            alert(result["errMsg"]);
            return false;
        }
    } else {
        if (flowSpa) {
            dismissLoadingScreen();
            if (result["opstatusVPX"] == "8005") {
                if (result["errCode"] == "VrfyOTPErr00001") {
                    if (gblTokenSwitchFlag) {
                        popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
                        popOtpSpa.txttokenspa.text = "";
                    } else {
                        popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                        popOtpSpa.lblPopupTract4Spa.text = "";
                    }
                    kony.application.dismissLoadingScreen();
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00002") {
                    popOtpSpa.dismiss();
                    kony.application.dismissLoadingScreen();
                    popTransferConfirmOTPLock.show();
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00005") {
                    popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
                    popOtpSpa.txttokenspa.text = "";
                    alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                    return false;
                } else if (result["errCode"] == "VrfyOTPErr00006") {
                    gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                    alert("" + result["errMsg"]);
                    return false;
                } else {
                    alert("" + result["errMsg"]);
                    return false;
                }
            } else {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
            }
        } else {
            gblRtyCtrVrfyTxPin = result["retryCounterVerifyTransPwd"];
            gblRtyCtrVrfyAxPin = result["retryCounterVerifyAccessPin"];
            dismissLoadingScreen();
            if (result["errCode"] == "VrfyTxPWDErr00003") {
                showTranPwdLockedPopup();
            } else {
                //alert("" + kony.i18n.getLocalizedString("KeyIncorrectPWD"));
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
                popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
                popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
                popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
                popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd")
                gblVerifyOTP = gblVerifyOTP + 1;
                return false;
            }
        }
        return false;
    }
}

function setFinActivityInputParamMB(inputParam) {
    inputParam["financialActivityLog_finTxnRefId"] = frmSSSExecute.lblTrnsRefNo.text;
    var date = frmSSSExecute.lblTransDate.text;
    date = replaceCommon(date, "/", "");
    inputParam["financialActivityLog_finTxnDate"] = date;
    inputParam["financialActivityLog_activityTypeId"] = "038";
    inputParam["financialActivityLog_channelId"] = "02";
    inputParam["financialActivityLog_fromAcctId"] = removeUnwatedSymbols(frmSSSExecute.lblSendFromAccNo.text);
    inputParam["financialActivityLog_fromAcctName"] = exeFromAcctType;
    inputParam["financialActivityLog_fromAcctNickname"] = exeFromAcctName;
    inputParam["financialActivityLog_toAcctId"] = removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
    inputParam["financialActivityLog_toAcctName"] = exeToAcctType;
    inputParam["financialActivityLog_toAcctNickname"] = exeToAcctName;
    inputParam["financialActivityLog_toBankAcctCd"] = "11";
    //inputParam["financialActivityLog_finTxnAmount"] = removeCommas(frmSSSExecute.lblTrnsAmnt.text);
    inputParam["financialActivityLog_eventId"] = (frmSSSExecute.lblTrnsRefNo.text).replace("SS", "");
    inputParam["financialActivityLog_txnType"] = "008";
    if (exeToAccountProdId == "221") inputParam["financialActivityLog_sendToSavePoint"] = "1";
    else inputParam["financialActivityLog_sendToSavePoint"] = "0";
}

function setNotificationAddServiceMB(inputParam) {
    var maxAmount = (frmSSSExecute.lblAmntLmtMax.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var minAmount = (frmSSSExecute.lblAmntLmtMin.text).replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var locale = kony.i18n.getCurrentLocale();
    inputParam["NotificationAdd_channelName"] = "MB-INQ";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale(); //"en_US";
    inputParam["NotificationAdd_notificationSubject"] = "";
    inputParam["NotificationAdd_notificationContent"] = "";
    inputParam["NotificationAdd_customerName"] = gblCustomerName;
    inputParam["NotificationAdd_emailId"] = gblEmailId;
    inputParam["NotificationAdd_notificationType"] = "Email";
    inputParam["NotificationAdd_source"] = "sendEmailForS2SExecution";
    inputParam["NotificationAdd_lblAmntMax"] = removeCommas(maxAmount);
    inputParam["NotificationAdd_lblAmntMin"] = removeCommas(minAmount);
    inputParam["NotificationAdd_lblFromAccName"] = frmSSSExecute.lblSendFromAccName.text;
    inputParam["NotificationAdd_lblFromAccNo"] = "x-xxx-" + (frmSSSExecute.lblSendFromAccNo.text).substring(6, 11) + "-x";;
    inputParam["NotificationAdd_lblToAccName"] = frmSSSExecute.lblSendToAccName.text;
    inputParam["NotificationAdd_lblToAccNo"] = "x-xxx-" + (frmSSSExecute.lblSendToAccNo.text).substring(6, 11) + "-x";;
    inputParam["NotificationAdd_lblTransAmnt"] = frmSSSExecute.lblTrnsAmnt.text;
    inputParam["NotificationAdd_lblDate"] = frmSSSExecute.lblTransDate.text;
    inputParam["NotificationAdd_lblTransFee"] = " ";
    inputParam["NotificationAdd_lblRefNo"] = frmSSSExecute.lblTrnsRefNo.text;
}

function completeExecute() {
    frmSSSExecute.scrollboxMain.containerWeight = 185;
    frmSSSExecute.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
    frmSSSExecute.hbxFromAccBalAftr.setVisibility(true);
    frmSSSExecute.hbxFromAccBalAftr.isVisible = true;
    frmSSSExecute.lblFromAccBalAftr.isVisible = true;
    frmSSSExecute.lblSendToAccBal.isVisible = true;
    frmSSSExecute.hbxFromAccCurrBal.setVisibility(false);
    frmSSSExecute.hbxFromAccCurrBal.isVisible = false;
    frmSSSExecute.vboxAftrT.setVisibility(true);
    frmSSSExecute.vboxAftrT.isVisible = true;
    frmSSSExecute.vboxTobal.isVisible = true;
    frmSSSExecute.hbxCompIcon.isVisible = true;
    frmSSSExecute.btnRight.setVisibility(true);
    frmSSSExecute.btnRight.isVisible = true;
    if (flowSpa) frmSSSExecute.hbxConfrmCanclSpa.isVisible = false
    else {
        frmSSSExecute.hbxConfrmCancl.setVisibility(true);
        frmSSSExecute.hbxConfrmCancl.isVisible = false
    }
    if (flowSpa) frmSSSExecute.hbxCmptReturnSpa.isVisible = true
    else {
        frmSSSExecute.hbxCmptReturn.setVisibility(true);
        frmSSSExecute.hbxCmptReturn.isVisible = true
    }
    frmSSSExecute.hboxSaveCamEmail.isVisible = false
    dismissLoadingScreen();
}

function s2sSegmentSliderMappingForConfirmOnLocaleChange() {
    var locale = kony.i18n.getCurrentLocale();
    if (flowSpa) {
        gblnormSelIndex = gbltranFromSelIndex[1];
        gblmbSpaselectedData = frmSSService.segSliderSpa.data[gblnormSelIndex];
        frmSSConfirmation.imgLinkAcc.src = gblmbSpaselectedData.img1;
        if (locale == "en_US") {
            if (gblmbSpaselectedData["hiddenPersonalizedAcctNickNameEN"] == null || gblmbSpaselectedData["hiddenPersonalizedAcctNickNameEN"] == '') frmSSConfirmation.lblLinkAccName.text = gblmbSpaselectedData.lblCustName;
            else frmSSConfirmation.lblLinkAccName.text = gblmbSpaselectedData.hiddenPersonalizedAcctNickNameEN
            frmSSConfirmation.lblLinkAccType.text = gblmbSpaselectedData.hiddenProductNameEN;
        } else {
            if (gblmbSpaselectedData["hiddenPersonalizedAcctNickNameTH"] == null || gblmbSpaselectedData["hiddenPersonalizedAcctNickNameTH"] == '') frmSSConfirmation.lblLinkAccName.text = gblmbSpaselectedData.lblCustName;
            else frmSSConfirmation.lblLinkAccName.text = gblmbSpaselectedData.hiddenPersonalizedAcctNickNameTH;
            frmSSConfirmation.lblLinkAccType.text = gblmbSpaselectedData.hiddenProductNameTH;
        }
        var amnt1 = gblmbSpaselectedData.lblBalance;
        frmSSConfirmation.lblLinkAccBal.text = amnt1;
        frmSSConfirmation.lblAcc.text = gblmbSpaselectedData.lblACno;
        frmSSConfirmation.lblLinkAccNo.text = gblmbSpaselectedData.lblActNoval;
    } else {
        if (locale == "en_US") {
            if (frmSSService["segSlider"]["selectedItems"][0]["hiddenPersonalizedAcctNickNameEN"] == null || frmSSService["segSlider"]["selectedItems"][0]["hiddenPersonalizedAcctNickNameEN"] == '') frmSSConfirmation.lblLinkAccName.text = frmSSService["segSlider"]["selectedItems"][0]["lblCustName"];
            else frmSSConfirmation.lblLinkAccName.text = frmSSService["segSlider"]["selectedItems"][0]["hiddenPersonalizedAcctNickNameEN"];
            frmSSConfirmation.lblLinkAccType.text = frmSSService["segSlider"]["selectedItems"][0]["hiddenProductNameEN"];
        } else {
            if (frmSSService["segSlider"]["selectedItems"][0]["hiddenPersonalizedAcctNickNameTH"] == null || frmSSService["segSlider"]["selectedItems"][0]["hiddenPersonalizedAcctNickNameTH"] == '') frmSSConfirmation.lblLinkAccName.text = frmSSService["segSlider"]["selectedItems"][0]["lblCustName"];
            else frmSSConfirmation.lblLinkAccName.text = frmSSService["segSlider"]["selectedItems"][0]["hiddenPersonalizedAcctNickNameTH"];
            frmSSConfirmation.lblLinkAccType.text = frmSSService["segSlider"]["selectedItems"][0]["hiddenProductNameTH"];
        }
    }
}

function s2sNoFixedAcctMappingForConfirmLocaleChange() {
    frmSSConfirmation.lblNFAccName.text = frmSSService.lblNFAccName.text;
    frmSSConfirmation.lblNFAccType.text = frmSSService.lblNFAccType.text;
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        if (noFixedAcctInfo[0]["personalizedAcctNickNameEN"] == null || noFixedAcctInfo[0]["personalizedAcctNickNameEN"] == '') frmSSConfirmation.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickName"];
        else frmSSConfirmation.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickNameEN"];
        if (noFixedAcctInfo[0]["productNameEN"] == null || noFixedAcctInfo[0]["productNameEN"] == '') frmSSConfirmation.lblNFAccType.text = noFixedAcctInfo[0]["productName"]
        else frmSSConfirmation.lblNFAccType.text = noFixedAcctInfo[0]["productNameEN"]
    } else {
        if (noFixedAcctInfo[0]["personalizedAcctNickNameTH"] == null || noFixedAcctInfo[0]["personalizedAcctNickNameTH"] == '') frmSSConfirmation.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickName"];
        else frmSSConfirmation.lblNFAccName.text = noFixedAcctInfo[0]["personalizedAcctNickNameTH"];
        if (noFixedAcctInfo[0]["productNameTH"] == null || noFixedAcctInfo[0]["productNameTH"] == '') frmSSConfirmation.lblNFAccType.text = noFixedAcctInfo[0]["productName"]
        else frmSSConfirmation.lblNFAccType.text = noFixedAcctInfo[0]["productNameTH"]
    }
}

function changeLocaleforApplyService() {
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        if (collectionDataEdit["lblFromAccNameEN"] == null || collectionDataEdit["lblFromAccNameEN"] == '') frmSSService.lblFromAccName.text = collectionDataEdit["lblFromAccName"];
        else frmSSService.lblFromAccName.text = collectionDataEdit["lblFromAccNameEN"];
        if (collectionDataEdit["lblFromAccTypeEN"] == null || collectionDataEdit["lblFromAccTypeEN"] == '') frmSSService.lblFromAccType.text = collectionDataEdit["lblFromAccType"];
        else frmSSService.lblFromAccType.text = collectionDataEdit["lblFromAccTypeEN"];
        if (collectionDataEdit["lblToAccNameEN"] == null || collectionDataEdit["lblToAccNameEN"] == '') frmSSService.lblNFAccName.text = collectionDataEdit["lblToAccName"];
        else frmSSService.lblNFAccName.text = collectionDataEdit["lblToAccName"]
        if (collectionDataEdit["lblToAccTypeEN"] == null || collectionDataEdit["lblToAccTypeEN"] == '') frmSSService.lblNFAccType.text = collectionDataEdit["lblToAccType"]
        else frmSSService.lblNFAccType.text = collectionDataEdit["lblToAccTypeEN"];
    } else {
        if (collectionDataEdit["lblFromAccNameTH"] == null || collectionDataEdit["lblFromAccNameTH"] == '') frmSSService.lblFromAccName.text = collectionDataEdit["lblFromAccName"];
        else frmSSService.lblFromAccName.text = collectionDataEdit["lblFromAccNameTH"];
        if (collectionDataEdit["lblFromAccTypeTH"] == null || collectionDataEdit["lblFromAccTypeTH"] == '') frmSSService.lblFromAccType.text = collectionDataEdit["lblFromAccType"];
        else frmSSService.lblFromAccType.text = collectionDataEdit["lblFromAccTypeTH"];
        if (collectionDataEdit["lblToAccNameEN"] == null || collectionDataEdit["lblToAccNameEN"] == '') frmSSService.lblNFAccName.text = collectionDataEdit["lblToAccName"];
        else frmSSService.lblNFAccName.text = collectionDataEdit["lblToAccName"]
        if (collectionDataEdit["lblToAccTypeTH"] == null || collectionDataEdit["lblToAccTypeTH"] == '') frmSSService.lblNFAccType.text = collectionDataEdit["lblToAccType"]
        else frmSSService.lblNFAccType.text = collectionDataEdit["lblToAccTypeTH"];
    }
}

function changeLocaleforEditDetail() {
    if (collectionDataEdit != null || collectionDataEdit != '') var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        if (collectionDataEdit["lblFromAccNameEN"] == null || collectionDataEdit["lblFromAccNameEN"] == '') frmSSServiceED.lblFromAccName.text = collectionDataEdit["lblFromAccName"];
        else frmSSServiceED.lblFromAccName.text = collectionDataEdit["lblFromAccNameEN"];
        if (collectionDataEdit["lblFromAccTypeEN"] == null || collectionDataEdit["lblFromAccTypeEN"] == '') frmSSServiceED.lblFromAccType.text = collectionDataEdit["lblFromAccType"];
        else frmSSServiceED.lblFromAccType.text = collectionDataEdit["lblFromAccTypeEN"];
        if (collectionDataEdit["lblToAccNameEN"] == null || collectionDataEdit["lblToAccNameEN"] == '') frmSSServiceED.lblToAccName.text = collectionDataEdit["lblToAccName"];
        else frmSSServiceED.lblToAccName.text = collectionDataEdit["lblToAccName"]
        if (collectionDataEdit["lblToAccTypeEN"] == null || collectionDataEdit["lblToAccTypeEN"] == '') frmSSServiceED.lblToAccType.text = collectionDataEdit["lblToAccType"]
        else frmSSServiceED.lblToAccType.text = collectionDataEdit["lblToAccTypeEN"];
    } else {
        if (collectionDataEdit["lblFromAccNameTH"] == null || collectionDataEdit["lblFromAccNameTH"] == '') frmSSServiceED.lblFromAccName.text = collectionDataEdit["lblFromAccName"];
        else frmSSServiceED.lblFromAccName.text = collectionDataEdit["lblFromAccNameTH"];
        if (collectionDataEdit["lblFromAccTypeTH"] == null || collectionDataEdit["lblFromAccTypeTH"] == '') frmSSServiceED.lblFromAccType.text = collectionDataEdit["lblFromAccType"];
        else frmSSServiceED.lblFromAccType.text = collectionDataEdit["lblFromAccTypeTH"];
        if (collectionDataEdit["lblToAccNameTH"] == null || collectionDataEdit["lblToAccNameTH"] == '') frmSSServiceED.lblToAccName.text = collectionDataEdit["lblToAccName"];
        else frmSSServiceED.lblToAccName.text = collectionDataEdit["lblToAccNameTH"]
        if (collectionDataEdit["lblToAccTypeTH"] == null || collectionDataEdit["lblToAccTypeTH"] == '') frmSSServiceED.lblToAccType.text = collectionDataEdit["lblToAccType"]
        else frmSSServiceED.lblToAccType.text = collectionDataEdit["lblToAccTypeTH"];
    }
}

function onClickExecutePreConfirmMB() {
    inputParam = {};
    var Amount = frmSSSExecute.lblTrnsAmnt.text
    inputParam["amount"] = Amount;
    inputParam["accountName"] = frmSSSExecute.lblSendToAccName.text;
    inputParam["accountNum"] = "xxx-x-" + frmSSSExecute.lblSendToAccNo.text.substring(6, 11) + "-x";
    inputParam["bankName"] = "TMB";
    inputParam["module"] = "execute";
    invokeServiceSecureAsync("ExecuteTransferValidationService", inputParam, sendToSaveSaveInSessionCallBackMb);
}

function sendToSaveSaveInSessionCallBackMb(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            onClickOTPRequestSpa();
        }
    }
}

function hideUnhide() {
    if (frmSSSExecute.lblFromAccBalAftr.isVisible) {
        frmSSSExecute.lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
    } else {
        frmSSSExecute.lblHide.text = kony.i18n.getLocalizedString("Hide")
    }
    if (frmSSSExecute.lblSendToAccBal.isVisible) {
        frmSSSExecute.lblHide1.text = kony.i18n.getLocalizedString("keyUnhide")
    } else {
        frmSSSExecute.lblHide1.text = kony.i18n.getLocalizedString("Hide")
    }
}