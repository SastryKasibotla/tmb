function frmAccTrcPwdInterMenuPreshow() {
    if (gblCallPrePost) {
        //ehFrmAccTrcPwdInter_frmAccTrcPwdInter_preshow.call(this, eventobject, neworientation);
        popupTractPwd.destroy();
        frmAccTrcPwdInter.scrollboxMain.scrollToEnd();
        frmAccTrcPwdInterPreShow();
        hbxFooterPwdNote.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
        isMenuShown = false;
        DisableFadingEdges(frmAccTrcPwdInter);
    }
}

function frmAccTrcPwdInterMenuPostshow() {
    if (gblCallPrePost) {
        //ehFrmAccTrcPwdInter_frmAccTrcPwdInter_postshow.call(this, eventobject, neworientation);
        frmAccTrcPwdInter.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}

function frmApplyInternetBankingConfirmationMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmApplyInternetBankingConfirmation.scrollboxMain.scrollToEnd();
        frmApplyInternetBankingConfirmationPreShow.call(this);
        DisableFadingEdges.call(this, frmApplyInternetBankingConfirmation);
    }
}

function frmApplyInternetBankingConfirmationMenuPostshow() {
    if (gblCallPrePost) {
        campaginService.call(this, "imgMIBConf", "frmApplyInternetBankingConfirmation", "M");
    }
    assignGlobalForMenuPostshow();
}

function frmApplyInternetBankingMBMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmApplyInternetBankingMB.scrollboxMain.scrollToEnd();
        frmApplyInternetBankingMB.lblMobileNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
        frmApplyInternetBankingMB.lblApplyInternetBankingNote.text = kony.i18n.getLocalizedString('NoteIncorrectMob')
        frmApplyInternetBankingMB.lnkChangeMobileNo.text = kony.i18n.getLocalizedString('changeMobNo')
        frmApplyInternetBankingMB.btnInternetBankingNext.text = kony.i18n.getLocalizedString('Next')
        gblLocale = false;
        DisableFadingEdges.call(this, frmApplyInternetBankingMB);
    }
}

function frmApplyInternetBankingMBMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmApplyMBConfirmationSPAMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmApplyMBConfirmationSPA.scrollboxMain.scrollToEnd();
        frmApplyMBConfirmationSPAPreShow.call(this);
    }
}

function frmApplyMBConfirmationSPAMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmApplyServicesMBMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmApplyServicesMB.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmApplyServicesMB);
    }
}

function frmApplyServicesMBMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmGetTMBTouchMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmGetTMBTouch.scrollboxMain.scrollToEnd();
        frmGetTMBTouchPreshow.call(this);
    }
}

function frmGetTMBTouchMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBAccLockedMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmMBAccLocked.scrollboxMain.scrollToEnd();
        frmMBAccLockedPreShow.call(this);
        DisableFadingEdges.call(this, frmMBAccLocked);
    }
}

function frmMBAccLockedMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBActiAtmIdMobileMenuPreshow() {
    if (gblCallPrePost) {
        frmMBActiAtmIdMobilePreShow.call(this);
    }
}

function frmMBActiAtmIdMobileMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBActiAtmIdMobileMenuOnHideshow() {
    if (gblCallPrePost) {
        frmMBActiAtmIdMobile.txtCaptchaText.text = "";
        frmMBActiAtmIdMobile.hboxCaptcha.setVisibility(false);
        frmMBActiAtmIdMobile.hboxCaptchaText.setVisibility(false);
    }
}

function frmMBActiCompleteMenuPreshow() {
    if (gblCallPrePost) {
        var deviceInfo = kony.os.deviceInfo();
        if (flowSpa) {} else {
            frmMBActiComplete.lblHdrTxt.margin = [43, 0, 0, 0];
        }
        if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
            frmMBActiCompletePreShow.call(this);
        } else {
            isMenuShown = false;
            gblUserLockStatusMB = "02";
            frmMBActiComplete.scrollboxMain.scrollToEnd();
            frmMBActiCompletePreShow.call(this);
        }
        DisableFadingEdges.call(this, frmMBActiComplete);
    }
}

function frmMBActiCompleteMenuPostshow() {
    if (gblCallPrePost) {
        frmMBActiComplete.scrollboxMain.scrollToEnd();
        TMBUtil.DestroyForm(frmAccTrcPwdInter); //destroying this form to prepare it for post login menu
        gblRiskServicecall = false;
        if (gblActionCode == "21" || gblActionCode == "23") {
            collectRiskData();
        }
    }
    assignGlobalForMenuPostshow();
}

function frmMBActiConfirmMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmMBActiConfirm.scrollboxMain.scrollToEnd();
        frmMBActiConfirmPreShow.call(this);
        toMBActiConfirmPS.call(this);
        DisableFadingEdges.call(this, frmMBActiConfirm);
    }
}

function frmMBActiConfirmMenuPostshow() {
    if (gblCallPrePost) {
        if (flowSpa) {
            isMenuRendered = false;
            isMenuShown = false;
            frmMBActiConfirm.scrollboxMain.scrollToEnd();
        }
    }
    assignGlobalForMenuPostshow();
}

function frmMBActiEmailDeviceNameMenuPreshow() {
    if (gblCallPrePost) {
        frmMBActiEmailDeviceNamePreShow.call(this);
    }
}

function frmMBActiEmailDeviceNameMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBActivateAnyIdMenuPreshow() {
    if (gblCallPrePost) {
        frmMBActivateAnyId.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmMBActivateAnyId);
        frmMBActivateAnyIdPreShow.call(this);
        frmMBActivateAnyIdPreShowLocale.call(this);
        if (fromBack) {
            showAgreeORNext.call(this);
        }
    }
}

function frmMBActivateAnyIdMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBActivateDebitCardCompleteMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmMBActivateDebitCardComplete.scrollboxMain.scrollToEnd();
        frmMBActivateDebitCardCompletePreShow.call(this);
        DisableFadingEdges.call(this, frmMBActivateDebitCardComplete);
    }
}

function frmMBActivateDebitCardCompleteMenuPostshow() {
    if (gblCallPrePost) {
        if (flowSpa) {
            isMenuRendered = false;
            isMenuShown = false;
            frmMBActiConfirm.scrollboxMain.scrollToEnd();
        }
    }
    assignGlobalForMenuPostshow();
}

function frmMBActivateEnterCVVMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmMBActivateEnterCVV.scrollboxMain.scrollToEnd();
        frmMBActivateEnterCVV.btnNext.setEnabled(false);
        frmMBActivateEnterCVVPreShow.call(this);
        DisableFadingEdges.call(this, frmMBActivateEnterCVV);
    }
}

function frmMBActivateEnterCVVMenuPostshow() {
    if (gblCallPrePost) {
        frmMBActivateEnterCVV.scrollboxMain.scrollToEnd();
        frmMBActivateEnterCVV.sboxRight.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}

function frmMBActivationMenuPreshow() {
    if (gblCallPrePost) {
        frmMBActivation.txtActivationCode.skin = txtNormalBG;
        frmMBActivation.txtIDPass.skin = txtNormalBG;
        isMenuShown = false;
        frmMBActivation.scrollboxMain.scrollToEnd();
        var flagKey = 0;
        frmMBActivationPreShow.call(this);
        toMBActiPS.call(this);
        if (flowSpa) {
            frmMBActivation.txtAccountNumberspa.text = "";
            if (gblSetPwdSpa) {
                frmMBActivation.lblActivationCode.setVisibility(false);
                frmMBActivation.hbxHoldActTextHelpIcon.setVisibility(false);
                frmMBActivation.line448366816408.setVisibility(false);
            } else {
                frmMBActivation.lblActivationCode.setVisibility(true);
                frmMBActivation.hbxHoldActTextHelpIcon.setVisibility(true);
                frmMBActivation.line448366816408.setVisibility(true);
            }
        } else {
            frmMBActivation.txtAccountNumber.text = "";
            frmMBActivation.txtAccountNumber.skin = txtNormalBG;
            frmMBActivation.label475124774164.padding = [0, 0, 0, 0];
        }
        frmMBActivation.txtActivationCode.text = "";
        frmMBActivation.txtIDPass.text = "";
        gtemp = "";
        DisableFadingEdges.call(this, frmMBActivation);
    }
}

function frmMBActivationMenuPostshow() {
    if (gblCallPrePost) {
        if (flowSpa) {
            isMenuRendered = false;
            isMenuShown = false;
            frmMBActivation.scrollboxMain.scrollToEnd();
        }
    }
    assignGlobalForMenuPostshow();
}

function frmMBActivationConfirmMobileMenuPreshow() {
    if (gblCallPrePost) {
        frmMBActivationConfirmMobilePreShow.call(this);
    }
}

function frmMBActivationConfirmMobileMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBActivationIBLoginMenuPreshow() {
    if (gblCallPrePost) {
        frmMBActivationIBLoginPreShow.call(this);
        frmMBActivationIBLogin.txtUserId.setFocus(true);
        frmMBActivationIBLogin.txtUserId.text = "";
        frmMBActivationIBLogin.txtPassword.text = "";
        frmMBActivationIBLogin.txtCaptchaText.text = "";
    }
}

function frmMBActivationIBLoginMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBankingMenuPreshow() {
    if (gblCallPrePost) {
        MBankingMenuDesign_preshow.call(this);
    }
}

function frmMBankingMenuPostshow() {
    if (gblCallPrePost) {
        MBankingMenuDesign_postshow.call(this);
    }
    assignGlobalForMenuPostshow();
    gblApplicationLaunch = false;
}

function frmMBAssignAtmPinMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmMBAssignAtmPin.scrollboxMain.scrollToEnd();
        frmMBAssignAtmPinpreShow.call(this);
    }
}

function frmMBAssignAtmPinMenuPostshow() {
    if (gblCallPrePost) {
        frmMBAssignAtmPin.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}

function frmMBAssignAtmPinOldMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmMBAssignAtmPin.scrollboxMain.scrollToEnd();
        frmMBAssignAtmPin.txtAtmPin.text = "";
        frmMBAssignAtmPin.txtAtmPinComfirm.text = "";
        atmPinShowAssignAtmPin("", false);
        frmMBAssignAtmPin.txtAtmPin.setFocus(true);
        frmMBAssignAtmPin.btnNext.setEnabled(false);
        DisableFadingEdges.call(this, frmMBAssignAtmPin);
    }
}

function frmMBAssignAtmPinOldMenuPostshow() {
    if (gblCallPrePost) {
        frmMBAssignAtmPin.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}

function frmMBEnterATMPinMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmMBEnterATMPin.scrollboxMain.scrollToEnd();
        frmMBEnterATMPin.btnNext.setEnabled(false);
        frmMBEnterATMPinPreShow.call(this);
        DisableFadingEdges.call(this, frmMBEnterATMPin);
    }
}

function frmMBEnterATMPinMenuPostshow() {
    if (gblCallPrePost) {
        frmMBEnterATMPin.scrollboxMain.scrollToEnd();
        frmMBEnterATMPin.sboxRight.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}

function frmMBForgotPinMenuPreshow() {
    if (gblCallPrePost) {
        kony.print("frmMBForgotPin preshow called");
        isMenuShown = false;
        frmMBForgotPin.scrollboxMain.scrollToEnd();
        popupTractPwd.dismiss();
        frmMBForgotPinPreShow();
        frmMBForgotPin.bounces = false;
        frmMBForgotPin.scrollboxMain.bounces = false;
        DisableFadingEdges.call(this, frmMBForgotPin);
    }
}

function frmMBForgotPinMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBSetAccPinTxnPwdMenuPreshow() {
    if (gblCallPrePost) {
        gblflag = 0;
        isMenuShown = false;
        frmMBSetAccPinTxnPwd.txtAccessPin.setFocus(true);
        frmMBSetAccPinTxnPwd.scrollboxMain.scrollToEnd();
        frmMBSetAccPinTxnPwd.lblPasswdStrength.setVisibility(false);
        frmMBSetAccPinPwdPreShow.call(this);
        DisableFadingEdges.call(this, frmMBSetAccPinTxnPwd);
    }
}

function frmMBSetAccPinTxnPwdMenuPostshow() {
    if (gblCallPrePost) {
        frmMBSetAccPinTxnPwd.scrollboxMain.scrollToEnd();
        frmMBSetAccPinTxnPwd.txtAccessPin.setFocus(true);
    }
    assignGlobalForMenuPostshow();
}

function frmMBsetPasswdMenuPreshow() {
    if (gblCallPrePost) {
        frmMBsetPasswd.txtTempAccess.skin = txtNormalBG;
        frmMBsetPasswd.txtAccessPwd.skin = txtNormalBG;
        frmMBsetPasswd.txtTransPass.skin = txtNormalBG
        frmMBsetPasswd.txtTemp.skin = txtNormalBG;
        gblShowPwd = gblShowPwdNo * 2;
        isMenuShown = false;
        frmMBsetPasswd.txtAccessPwd.text = "";
        frmMBsetPasswd.txtDeviceName.text = "";
        frmMBsetPasswd.txtTemp.text = "";
        frmMBsetPasswd.txtTempAccess.text = "";
        frmMBsetPasswd.txtTransPass.text = "";
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        frmMBsetPasswd.lblPasswdStrength.setVisibility(false);
        frmMBsetPasswd.txtAccessPwd.keyBoardStyle = constants.TEXTBOX_NUMERIC_PASSWORD;
        if (gblAddOrAuth == 0) {} else {
            frmMBsetPasswd.lblDeviceName.isVisible = true;
            frmMBsetPasswd.txtDeviceName.isVisible = true;
        }
        toMBsetPasswdPS.call(this);
        frmMBsetPasswdPreShow.call(this);
        DisableFadingEdges.call(this, frmMBsetPasswd);
    }
}

function frmMBsetPasswdMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmMBSetuseridSPAMenuPreshow() {
    if (gblCallPrePost) {
        frmMBSetuseridSPA.txtUserID.text = ""
        frmMBSetuseridSPA.txtTransPass.text = ""
        frmMBSetuseridSPA.txtConfirmPassword.text = "";
        if (flowSpa && gblSetPwdSpa) {
            frmMBSetuseridSPA.hbxUserId.setVisibility(false);
            frmMBSetuseridSPA.lblIBInlineUserIDGuidelines.setVisibility(false);
            frmMBSetuseridSPA.label50285458139.setVisibility(false);
            frmMBSetuseridSPA.line50368857864580.setVisibility(false)
        } else {
            frmMBSetuseridSPA.hbxUserId.setVisibility(true);
            frmMBSetuseridSPA.lblIBInlineUserIDGuidelines.setVisibility(true);
            frmMBSetuseridSPA.label50285458139.setVisibility(true);
            frmMBSetuseridSPA.line50368857864580.setVisibility(true)
        }
        frmMBSetuseridSPAPreShow.call(this);
    }
}

function frmMBSetuseridSPAMenuPostshow() {
    if (gblCallPrePost) {
        frmMBSetuseridSPA.scrollboxMain.scrollToEnd();
    }
    assignGlobalForMenuPostshow();
}

function frmPostLoginMenuNewMenuPreshow() {
    if (gblCallPrePost) {
        setDataToMenuSeg.call(this);
    }
}

function frmPostLoginMenuNewMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmTouchIdIntermediateLoginMenuPreshow() {
    kony.print("FPRINT: gblCallPrePost=" + gblCallPrePost);
    if (gblCallPrePost) {
        preShowfrmTouchIdIntermediateLogin.call(this);
    }
}

function frmTouchIdIntermediateLoginMenuPostshow() {
    assignGlobalForMenuPostshow();
}

function frmTouchIdSettingsMenuPreshow() {
    if (gblCallPrePost) {
        frmTouchIdSettings.scrollboxMain.scrollToEnd();
        preShowTouchIdSetting.call(this);
        setEnableDisableTouchLogin.call(this);
    }
}

function frmRegistrationPopupMenuPreshow() {
    if (gblCallPrePost) {
        preShowRegistrationPopup.call(this);
    }
}

function preShowRegistrationPopup() {
    kony.print("FPRINT: keyContinueButton=" + kony.i18n.getLocalizedString("keyContinueButton"));
    kony.print("FPRINT: keyCancelButton=" + kony.i18n.getLocalizedString("keyCancelButton"));
    var locale_app = kony.i18n.getCurrentLocale();
    kony.print("FPRINT: locale_app=" + locale_app);
    if (locale_app == "th_TH") {
        frmRegistrationPopup.imgWelcome.src = getServerImagePath("welcome_th");
        kony.print("FPRINT getServerImagePath=" + getServerImagePath("welcome_th"));
    } else {
        frmRegistrationPopup.imgWelcome.src = getServerImagePath("welcome_en");
        kony.print("FPRINT getServerImagePath=" + getServerImagePath("welcome_en"));
    }
    frmRegistrationPopup.btnContinue.text = kony.i18n.getLocalizedString("keyContinueButton");
    frmRegistrationPopup.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
}

function frmFPSettingMenuPreshow() {
    kony.print("FPRINT: gblCallPrePost=" + gblCallPrePost);
    if (gblCallPrePost) {
        kony.print("FPRINT: IN frmFPSettingMenuPreshow START");
        preShowFPSetting();
        //setEnableDisableTouchLogin.call(this);
        setEnableDisableTouchLogin();
        kony.print("FPRINT: IN frmFPSettingMenuPreshow STOP");
    }
}

function preShowFPSetting() {
    kony.print("MB_AndFinPrintTitle1=" + kony.i18n.getLocalizedString("MB_AndFinPrintTitle1"));
    kony.print("MB_AndFinPrintTitle2=" + kony.i18n.getLocalizedString("MB_AndFinPrintTitle2"));
    kony.print("MB_AndFinPrintDesc1=" + kony.i18n.getLocalizedString("MB_AndFinPrintDesc1"));
    kony.print("MB_AndFinPrintDesc1=" + kony.i18n.getLocalizedString("MB_AndFinPrintDesc2"));
    kony.print("MB_AndFinPrintDesc1=" + kony.i18n.getLocalizedString("MB_AndFinPrintTog"));
    kony.print("MB_AndFinPrintDesc1=" + kony.i18n.getLocalizedString("MB_AndFinPrintReturn"));
    frmFPSetting.lblTouchHdr.text = kony.i18n.getLocalizedString("MB_AndFinPrintTitle1");
    frmFPSetting.lblTouchTitle.text = kony.i18n.getLocalizedString("MB_AndFinPrintTitle2");
    frmFPSetting.lblTouchInfo1.text = kony.i18n.getLocalizedString("MB_AndFinPrintDesc1");
    frmFPSetting.lblTouchInfo2.text = kony.i18n.getLocalizedString("MB_AndFinPrintDesc2");
    frmFPSetting.lblTouchInfo3.text = kony.i18n.getLocalizedString("MB_AndFinPrintTog");
    frmFPSetting.btnReturnHome.text = kony.i18n.getLocalizedString("MB_AndFinPrintReturn");
}

function frmTouchIdSettingsMenuPostshow() {
    assignGlobalForMenuPostshow();
}