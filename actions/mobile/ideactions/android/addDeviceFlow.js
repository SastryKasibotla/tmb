/**
 * Flow to addDevice functionality starts here and before callig this method the
 * validations on accesspin, transaction pwd and device name has to be performed
 * @returns - no return values
 */
function addNewDeviceFlow(accessPIN) {
    //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
    inputParam = {};
    inputParam["loginModuleId"] = "MB_AcPwd";
    inputParam["userStoreId"] = "DefaultStore";
    inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["userId"] = "";
    inputParam["password"] = accessPIN;
    inputParam["trxnpassword"] = frmMBsetPasswd.txtTransPass.text;
    inputParam["sessionVal"] = "";
    inputParam["segmentId"] = "segmentId";
    //var entries = JSON.stringify([{"segmentId":"MIB"}]);
    inputParam["segmentIdVal"] = "MIB";
    invokeServiceSecureAsync("verifyAddDevice", inputParam, callBackPwdVerifyaddNewDeviceTwo)
}
//function callBackPwdVerifyaddNewDeviceOne(status, resulttable) {
//	if (status == 400) {
//		
//		if (resulttable["opstatus"] == 0) {
//			//kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
//			showLoadingScreen();
//			inputParam = {};
//			inputParam["loginModuleId"] = "MB_TxPwd";
//			inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
//			inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
//			inputParam["userStoreId"] = "DefaultStore";
//			inputParam["userId"] = "";
//			inputParam["password"] = frmMBsetPasswd.txtTransPass.text;
//			inputParam["sessionVal"] = "";
//			inputParam["segmentId"] = "segmentId";
//			//var entries = JSON.stringify([{"segmentId":"MIB"}]);
//			inputParam["segmentIdVal"] = "MIB";
//			invokeServiceSecureAsync("verifyPassword", inputParam, callBackPwdVerifyaddNewDeviceTwo)
//		} //end of opstatus
//		else {
//			gblRtyCtrVrfyAxPin = resulttable["retryCounterVerifyAccessPin"];
//			gblRtyCtrVrfyTxPin = resulttable["retryCounterVerifyTransPwd"];
//			if (resulttable["errCode"] == "VrfyAcPWDErr00001") {
//				kony.application.dismissLoadingScreen();
//				showAlert(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"));
//				return false;
//			} else if (resulttable["errCode"] == "VrfyAcPWDErr00002") {
//				kony.application.dismissLoadingScreen();
//				showAlert(kony.i18n.getLocalizedString("ECVrfyAccPinErr00001"), kony.i18n.getLocalizedString("info"));
//				return false;
//			} else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
//				gblRtyCtrVrfyAxPin = "0";
//				kony.application.dismissLoadingScreen();
//				showAlert(kony.i18n.getLocalizedString("ECVrfyAccPinErr00002"), kony.i18n.getLocalizedString("info"));
//				var actionType = "18"
//				//invokeCRMProfileUpdate(actionType);
//				return false;
//			} else if (resulttable["errCode"] == "JavaErr00001") {
//				kony.application.dismissLoadingScreen();
//				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
//				return false;
//			} else {
//				kony.application.dismissLoadingScreen();
//				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
//				return false;
//			}
//		} //end of opstatus else block
//	} //status 400 end
//}
/*****************************************************************
 *	Name    : callBackPwdVerifyaddNewDeviceTwo
 *	Author  : Kony Solutions
 *	Purpose : Calling add device service call after checking the access pin and tranaction pin entered are correct or not.
 ******************************************************************/
function callBackPwdVerifyaddNewDeviceTwo(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            showLoadingScreen();
            GBL_FLOW_ID_CA = 1;
            //var deviceInfo = kony.os.deviceInfo();
            //if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
            //	}else if (deviceInfo["name"] == "android"){
            getUniqueID();
        } else {
            gblRtyCtrVrfyAxPin = resulttable["retryCounterVerifyAccessPin"];
            gblRtyCtrVrfyTxPin = resulttable["retryCounterVerifyTransPwd"];
            if (resulttable["errCode"] == "VrfyTxPWDErr00001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00002") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
                gblRtyCtrVrfyTxPin = "0";
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00002"), kony.i18n.getLocalizedString("info"));
                var actionType = "19"
                    //invokeCRMProfileUpdate(actionType);
                return false;
            } else if (resulttable["errCode"] == "JavaErr00001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyAcPWDErr00002") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyAccPinErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
                gblRtyCtrVrfyAxPin = "0";
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyAccPinErr00002"), kony.i18n.getLocalizedString("info"));
                var actionType = "18"
                    //invokeCRMProfileUpdate(actionType);
                return false;
            } else if (resulttable["errCode"] == "JavaErr00001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
}
/*****************************************************************
 *	Name    : updateDeviceServiceCall
 *	Author  : Kony Solutions
 *	Purpose : Calling add device java service call
 ******************************************************************/
function updateDeviceServiceCall(uniqueId) {
    kony.print("CRMMATCH FPRINT IN updateDeviceServiceCall uniqueId=" + uniqueId);
    //kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
    if (true) { //	if(gblMBActivationVia == "2"){
        inputParam = {};
        inputParam["deviceId"] = uniqueId;
        GBL_UNIQ_ID = uniqueId;
        inputParam["deviceNickName"] = frmMBActiEmailDeviceName.tbxDeviceName.text.trim();
        inputParam["deviceStatus"] = "0";
        inputParam["deviceAuthLevel"] = "0";
        inputParam["flexNote"] = "Adding device";
        inputParam["deviceModel"] = getDeviceModel();
        inputParam["osType"] = getDeviceOS();
        if (frmMBActiEmailDeviceName.tbxEmail.isVisible) {
            inputParam["emailAddr"] = frmMBActiEmailDeviceName.tbxEmail.text;
        }
        invokeServiceSecureAsync("updateDeviceNameEmail", inputParam, callBackUpdatDeviceNameEmail)
    } else {
        inputParam = {};
        inputParam["deviceId"] = uniqueId;
        GBL_UNIQ_ID = uniqueId;
        //inputParam["associationId"] = "001100000000000000000000007820";
        if (gblActionCode == "21") {
            inputParam["deviceNickName"] = frmConnectAccMB.txtDeviceName.text;
        } else if (gblActionCode == "23") {
            inputParam["deviceNickName"] = frmMBsetPasswd.txtDeviceName.text;
        }
        inputParam["deviceStatus"] = "0";
        inputParam["deviceAuthLevel"] = "0";
        inputParam["flexNote"] = "Adding device";
        inputParam["activationCode"] = gActivationCode;
        inputParam["deviceModel"] = getDeviceModel();
        inputParam["osType"] = getDeviceOS();
        inputParam["subscriptionToken"] = gPushRegId; //subscriptionToken from kpnsMB.js
        //inputParam["actionCode"] = "21";
        //inputParam["flowStatusId"] = "91";
        //inputParam["channelId"] = "02";
        invokeServiceSecureAsync("addDevice", inputParam, callBackUpdateDeviceaddNewDeviceTwo)
    }
}
/*****************************************************************
 *	Name    : callBackUpdateDeviceaddNewDeviceTwo
 *	Author  : Kony Solutions
 *	Purpose : Calling update device after checking the status code from add device service
 ******************************************************************/
function callBackUpdateDeviceaddNewDeviceTwo(status, resulttable) {
    if (status == 400) {
        if (resulttable["errCode"] == "KonyDvMgmtErr00003") {
            showAlert(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00003"), kony.i18n.getLocalizedString("info"));
            kony.application.dismissLoadingScreen();
            return false;
        }
        //
        if (resulttable["opstatus"] == 0) {
            if (resulttable["Results"][0]["statusCode"] == "1") {
                //alert("The update is successful");
                encryptSecretKey(resulttable["Results"][0]["secretKey"], resulttable["Results"][0]["encryptKey"]);
                gblcrmId = resulttable["crmId"];
                var glbRegFlag = "true"
                    //var deviceInfo = kony.os.deviceInfo();
                GBL_FLOW_ID_CA = 2;
                //if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
                getUniqueID();
            } else {
                //showAlert(resulttable["Results"][0]["errMsg"], kony.i18n.getLocalizedString("info"));
                showActivationIncompletePage()
                kony.application.dismissLoadingScreen();
                return false;
            }
        } else if (resulttable["opstatus"] == 1000 || resulttable["opstatus"] == "1000" || resulttable["opstatus"] == "1000.00" || resulttable["opstatus"] == 1000.00) {
            showAlert(kony.i18n.getLocalizedString("MW1000"), kony.i18n.getLocalizedString("info"));
            //showActivationIncompletePage()
            kony.application.dismissLoadingScreen();
            return false;
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            //showActivationIncompletePage()
            kony.application.dismissLoadingScreen();
            return false;
        }
        kony.application.dismissLoadingScreen();
    }
}

function updateDevicePartOfActivation(uniqueId) {
    kony.print("CRMMATCH FPRINT IN updateDevicePartOfActivation uniqueId=" + uniqueId);
    inputParam = {};
    inputParam["deviceId"] = uniqueId;
    inputParam["associationId"] = "";
    if (gblActionCode == "21") {
        inputParam["deviceNickName"] = frmConnectAccMB.txtDeviceName.text;
    } else if (gblActionCode == "23") {
        inputParam["deviceNickName"] = frmMBsetPasswd.txtDeviceName.text;
    }
    inputParam["deviceStatus"] = "1";
    inputParam["deviceAuthLevel"] = "1";
    inputParam["flexNote"] = "Activated and Trusted";
    if (gblActionCode != "23") {
        inputParam["emailAddr"] = frmConnectAccMB.tbxPopupTractPwdtxt.text;
    }
    inputParam["subscriptionToken"] = gPushRegId; //subscriptionToken from kpnsMB.js
    invokeServiceSecureAsync("updateDevice", inputParam, callBackReceiveAckFromDevice)
}
/*****************************************************************
 *	Name    : callBackReceiveAckFromDevice
 *	Author  : Kony Solutions
 *	Purpose : Calling crmProfileMod service
 ******************************************************************/
function callBackReceiveAckFromDevice(status, resulttable) {
    if (status == 400) {
        //
        if (resulttable["opstatus"] == 0) {
            if (resulttable["Results"][0]["statusCode"] == "1") {
                if (gblActionCode == "23") {
                    frmMBActiComplete.image250285458166.src = "iconcomplete.png";
                    frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString("appReadytoUse");
                    frmMBActiComplete.btnStart.setVisibility(true);
                    notificationAddServiceForAddDeviceFlow();
                    frmMBActiComplete.show();
                } else if (gblActionCode == "21") {
                    inputParam = {};
                    var locale = kony.i18n.getCurrentLocale();
                    var list;
                    if (locale == "en_US") {
                        inputParam["languageCd"] = "EN";
                        list = {
                            appLocale: "en_US"
                        }
                    } else {
                        inputParam["languageCd"] = "TH";
                        list = {
                            appLocale: "th_TH"
                        }
                    }
                    kony.store.setItem("curAppLocale", list);
                    inputParam["actionType"] = "14";
                    inputParam["emailAddr"] = frmConnectAccMB.tbxPopupTractPwdtxt.text;
                    //
                    //inputParam["p2pLinkAcctID"] = frmConnectAccMB.segSlider.selectedItems[0].hiddenActVal;
                    invokeServiceSecureAsync("crmProfileModActivation", inputParam, callBackCrmProfileActUpdate);
                }
            } else {
                //showAlert(resulttable["Results"][0]["errMsg"], kony.i18n.getLocalizedString("info"));
                kony.store.removeItem("encrytedText");
                showActivationIncompletePage();
                kony.application.dismissLoadingScreen();
                return false;
            }
        } else if (resulttable["opstatus"] == 1000 || resulttable["opstatus"] == "1000" || resulttable["opstatus"] == "1000.00" || resulttable["opstatus"] == 1000.00) {
            showAlert(kony.i18n.getLocalizedString("MW1000"), kony.i18n.getLocalizedString("info"));
            kony.application.dismissLoadingScreen();
            return false;
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            kony.application.dismissLoadingScreen();
            return false;
        }
    }
}
/*****************************************************************
 *	Name    : callBackReceiveAckFromDevice
 *	Author  : Kony Solutions
 *	Purpose : Calling crmProfileMod service
 ******************************************************************/
//function callBackCrmProfileInqActivation(status, resulttable) {
//	if (status == 400) {
//		
//		if (resulttable["opstatus"] == 0) {
//			inputParam = {};
//			inputParam["rqUUId"] = "";
//			inputParam["channelName"] = "MB-INQ";
//			inputParam["ibUserId"] = resulttable["ibUserId"];
//			inputParam["ebTxnLimitAmt"] = resulttable["ebTxnLimitAmt"];
//			inputParam["ebMaxLimitAmtCurrent"] = resulttable["ebMaxLimitAmtCurrent"];
//			inputParam["ebMaxLimitAmtHist"] = resulttable["ebMaxLimitAmtHist"];
//			inputParam["ebMaxLimitAmtRequest"] = resulttable["ebMaxLimitAmtRequest"];
//			inputParam["ebAccuUsgAmtDaily"] = resulttable["ebAccuUsgAmtDaily"];
//			inputParam["firstActivationDate"] = "";
//			inputParam["firstIbLoginDate"] = resulttable["firstIbLoginDate"];
//			inputParam["firstMbLoginDate"] = "";
//			inputParam["lastIbLoginSucessDate"] = resulttable["lastIbLoginSucessDate"];
//			inputParam["lastIbLoginFailDate"] = resulttable["lastIbLoginFailDate"];
//			inputParam["lastIbLogoutDate"] = resulttable["lastIbLogoutDate"];
//			inputParam["lastMbLoginSucessDate"] = "";
//			inputParam["lastMbLoginFailDate"] = "";
//			inputParam["lastMbLogoutDate"] = resulttable["lastMbLogoutDate"];
//			inputParam["lastIbPasswordChangeDate"] = resulttable["lastIbPasswordChangeDate"];
//			inputParam["lastMbAccessPasswordChangeDate"] = resulttable["lastMbAccessPasswordChangeDate"];
//			inputParam["lastMbTxnPwdChangeDate"] = resulttable["lastMbTxnPwdChangeDate"];
//			inputParam["ibApplyChannel"] = resulttable["ibApplyChannel"];
//			var locale = kony.i18n.getCurrentLocale();
//			if (locale == "en_US") {
//				inputParam["languageCd"] = "EN";
//			} else {
//				inputParam["languageCd"] = "TH";
//			}
//			inputParam["tokenDeviceFlag"] = resulttable["tokenDeviceFlag"];
//			inputParam["facebookID"] = resulttable["facebookId"];
//			inputParam["ebCustomerStatusID"] = "02";
//			inputParam["ibUserStatusId"] = resulttable["ibUserStatusId"];
//			inputParam["mbUserStatusId"] = "02";
//			inputParam["createdDt"] = resulttable["createdDt"];
//			inputParam["actionType"] = "14";
//			inputParam["p2pLinkAcctID"] = resulttable["p2pLinkedAcct"];
//			inputParam["emailAddr"] = frmConnectAccMB.tbxPopupTractPwdtxt.text;
//			invokeServiceSecureAsync("crmProfileMod", inputParam, callBackCrmProfileActUpdate)
//			//frmMBActiComplete.show()
//		} else {
//			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
//			kony.application.dismissLoadingScreen();
//			return false;
//		}
//	}
//}
/*****************************************************************
 *	Name    : encryptSecretKey
 *	Author  : Kony Solutions
 *	Purpose : Encrypting and storing the secret key generated from device add java service on to the device
 ******************************************************************/
function callBackCrmProfileActUpdate(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == "0") {
            gblEmailId = resulttable["EmailAddr"];
            gblMBFlowStatus = resulttable["MBUserStatusID"];
            frmMBActiComplete.image250285458166.src = "iconcomplete.png";
            frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString("appReadytoUse");
            frmMBActiComplete.btnStart.setVisibility(true);
            if (gblActionCode == "23" || gblActionCode == "21") {
                var inputparam = {}
                invokeServiceSecureAsync("partyInquiry", inputparam, FstTmeActivatnPartySrviceCallback);
                //notificationAddServiceForAddDeviceFlow();
            }
            frmMBActiComplete.show()
        } else {
            kony.store.removeItem("encrytedText");
            showActivationIncompletePage();
        }
    }
}
/*****************************************************************
*	Name    : Encrypt
*	Author  : Kony Solutions
*	Purpose : Hashing the Activation code and salt and encrypting the hashed code

Commented as we are not getting salt from the add device java service.Can be used once we get the salt from that


function Encrypt()
{
	try
	{
		var algo="aes";
		if(kony.os.deviceInfo().name == "blackberry")
			var encryptDecryptKey = kony.crypto.newKey("passphrase", 128, {passphrasetext: ["inputstring1inputstring1"], subalgo: "aes", passphrasehashalgo: "md5"});
		else
		var encryptDecryptKey = kony.crypto.newKey("random", 128, {passphrasetext: null, subalgo: "aes", passphrasehashalgo: ""});
		var activationCode = gActivationCode
		var salt = "AES345DF"
		var secKey = gActivationCode+salt
		var secKeyHash = kony.crypto.createHash("sha256",secKey)
		var inputstr = secKeyHash;		
		var prptobj = {padding:"pkcs5",mode:"cbc",initializationvector:"1234567890123456"};
        myEncryptedTextRaw = kony.crypto.encrypt(algo,encryptDecryptKey,inputstr,prptobj);
        saveEncrykey = kony.crypto.saveKey("encrtykey", encryptDecryptKey)
        var list = {enckey:myEncryptedTextRaw,encKeyVal:saveEncrykey}
       	saveEncrykey = kony.crypto.saveKey("encrtykey", encryptDecryptKey)
		kony.store.setItem("encrytedText", list);
		var myEncryptedText  = kony.convertToBase64(myEncryptedTextRaw);
		
		
		
	}
	catch(err)
	{
		alert(typeof err);
		alert("Error in callbackEncryptAes : "+err );
	}
}
******************************************************************/
/*****************************************************************
 *	Name    : encryptSecretKey
 *	Author  : Kony Solutions
 *	Purpose : Encrypting and storing the secret key generated from device add java service on to the device
 ******************************************************************/
function encryptSecretKey(secretKey, initVectorKey) {
    try {
        var algo = "aes";
        if (gblDeviceInfo.name == "blackberry") var encryptDecryptKey = kony.crypto.newKey("passphrase", 128, {
            passphrasetext: ["inputstring1inputstring1"],
            subalgo: "aes",
            passphrasehashalgo: "md5"
        });
        else var encryptDecryptKey = kony.crypto.newKey("random", 128, {
            passphrasetext: null,
            subalgo: "aes",
            passphrasehashalgo: ""
        });
        var inputstr = secretKey;
        var prptobj = {
            padding: "pkcs5",
            mode: "cbc",
            initializationvector: initVectorKey
        };
        var myEncryptedTextRaw = kony.crypto.encrypt(algo, encryptDecryptKey, inputstr, prptobj);
        var saveEncrykey = kony.crypto.saveKey("encrtykey", encryptDecryptKey)
        var list = {
                enckey: myEncryptedTextRaw,
                encKeyVal: saveEncrykey
            }
            //saveEncrykey = kony.crypto.saveKey("encrtykey", encryptDecryptKey)
        kony.store.setItem("encrytedText", list);
    } catch (err) {
        //alert(typeof err);
        alert("Error in callbackEncryptAes : " + err);
    }
}
/*****************************************************************
 *	Name    : decrypt
 *	Author  : Kony Solutions
 *	Purpose : To decrypt the encrypted text and returing the decrypted text.
 ******************************************************************/
function decrypt(decInitVector) {
    try {
        var algo = "aes";
        var prptobj = {
            padding: "pkcs5",
            mode: "cbc",
            initializationvector: decInitVector
        };
        var encrKeyFromDevice = kony.store.getItem("encrytedText");
        var readEncryVal = encrKeyFromDevice["encKeyVal"]
        var decryptkey = kony.crypto.readKey(readEncryVal)
        var retencrKeyFromDevice = encrKeyFromDevice["enckey"]
        var myClearText = kony.crypto.decrypt(algo, decryptkey, retencrKeyFromDevice, prptobj);
        //frmMBanking.label47517791118.text ="Decrypted text = "+myClearText.toString();
        return myClearText
    } catch (err) {
        //alert(typeof err);
        alert("Error in callbackDecryptAes : " + err);
    }
}
/*****************************************************************
 *	Name    : generateOTP
 *	Author  : Kony Solutions
 *	Purpose : To generate OTP by applying hashing technique of decrypted text and device id and current date
 ******************************************************************/
//function generateOTP() {
//	var secKeyVal = decrypt();
//	var hardwareID = getDeviceID();
//	var currentdate = new Date();
//	var datetime = currentdate.getDay() + "/" + currentdate.getMonth() + "/" + currentdate.getFullYear() + currentdate.getHours() +
//		":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
//	var secKey = hardwareID + datetime + secKeyVal;
//	var secKeyHash = kony.crypto.createHash("sha256", secKey);
//	
//	return secKeyHash
//}
/*****************************************************************
 *	Name    : getDeviceID
 *	Author  : Kony Solutions
 *	Purpose : To get the device ID
 ******************************************************************/
function getDeviceID() {
    var hardwareID = "";
    var konyID = "";
    //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "android") {
        konyID = gblDeviceInfo["ANDROID_ID"];
        var macID = getMacAddress.getWlan();
        var newmacID = macID.replace(/:/g, "");
        hardwareID = konyID + newmacID;
    }
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
        // modifying the code for kony device id.
        hardwareID = gblDeviceInfo["identifierForVendor"];
    }
    return hardwareID
}
/*****************************************************************
 *	Name    : getDeviceOS
 *	Author  : Kony Solutions
 *	Purpose : To get the device OS
 ******************************************************************/
function getDeviceOS() {
    var deviceOS = ""
        //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "android") {
        deviceOS = "0";
    } else if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
        deviceOS = "1";
    } else {
        deviceOS = "3";
    }
    return deviceOS
}
/*****************************************************************
 *	Name    : getDeviceModel
 *	Author  : Kony Solutions
 *	Purpose : To get the device model
 ******************************************************************/
function getDeviceModel() {
    var deviceModel = ""
        //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "android") {
        deviceModel = gblDeviceInfo["model"];
    }
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
        deviceModel = gblDeviceInfo["model"];
    }
    return deviceModel
}
/**
 * description
 * returns {}
 */
//function callBackDeviceInquiry(status, resulttable) {
//	if (status == 400) {
//		
//		if (resulttable["opstatus"] == 0) {
//			if (resulttable["Results"].length > 0 && resulttable["Results"][0]["deviceId"] != null) {
//				//Add device flow
//				gblAddOrAuth = 1;
//				
//				//getHeader(mobileMethod, kony.i18n.getLocalizedString("keyConfirmPwd") , 0,0,0);
//				frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keyPwdInput");
//				frmMBsetPasswd.show();
//			} else {
//				//Activation flow
//				gblAddOrAuth = 0
//				
//				frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keySetInfoLabel");
//				frmMBsetPasswd.show();
//				//getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPassword") , 0,0,0);
//			}
//		}
//		kony.application.dismissLoadingScreen();
//	}
//}
function showAddDevOrActivation() {
    if (gblActionCode == "23") {
        //Add device flow
        gblAddOrAuth = 1;
        //getHeader(mobileMethod, kony.i18n.getLocalizedString("keyConfirmPwd") , 0,0,0);
        //Removing lblSetInfo as suggested
        //frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keyPwdInput");
        frmMBsetPasswd.show();
    } else if (gblActionCode == "21") {
        //Activation flow
        gblAddOrAuth = 0
            //Removing lblSetInfo as suggested
            //frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keySetInfoLabel");
        frmMBsetPasswd.show();
        //getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPassword") , 0,0,0);
    } else if (gblActionCode == "22") {
        //Activation flow
        gblAddOrAuth = 0
            //Removing lblSetInfo as suggested
            //frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keySetInfoLabel");
        frmMBsetPasswd.show();
        //getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPassword") , 0,0,0);
    } else {}
}

function notificationAddServiceForAddDeviceFlow() {
    var inputparam = {};
    inputparam["channelName"] = "Mobile Banking";
    inputparam["channelID"] = "02";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    if (tnckeyword == undefined || tnckeyword == "") {
        inputparam["moduleKey"] = "TermsAndConditions";
    } else {
        inputparam["moduleKey"] = tnckeyword;
    }
    invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationAddService);
}
gblNewCrmId = "NO";

function callBackUpdatDeviceNameEmail(status, resulttable) {
    if (status == 400) {
        kony.application.dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            gblShowPwdNo = kony.os.toNumber(resulttable["showPinPwdCount"]);
            gblShowPinPwdSecs = kony.os.toNumber(resulttable["showPinPwdSecs"]);
            gblNewCrmId = resulttable["newCrmId"];
            frmMBSetAccPinTxnPwd.txtAccessPin.setFocus(true);
            frmMBSetAccPinTxnPwd.show();
        } else if (resulttable["errMsg"] == "duplicateDeviceName") {
            alert(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00003"))
        } else if (resulttable["opstatus"] == "-1") {
            //showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            frmMBanking.show();
        } else {
            alert("error " + resulttable["errMsg"]);
        }
    }
}