function onClickATM() {
    gblLatitude = "";
    gblLongitude = "";
    gblCurrenLocLat = "";
    gblCurrenLocLon = "";
    provinceID = "";
    districtID = "";
    gblProvinceData = [];
    gblDistrictData = [];
    frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
    frmATMBranch.btnCombDistrict.text = kony.i18n.getLocalizedString("FindTMB_District");
    frmATMBranch.txtKeyword.text = "";
    try {
        gblFindTMBType = "ATM";
        frmATMBranch.btnAtm.skin = "btnATMButtonFocus";
        frmATMBranch.btnBrch.skin = "btnBranch";
        frmATMBranch.btnExgBooth.skin = "btnExchangeBooth";
        //getGeoLocation();
        androidMPermissionforLocation();
    } catch (e) {}
}

function onClickBranch() {
    try {
        gblFindTMBType = "BRANCH";
        frmATMBranch.btnAtm.skin = "btnATMButton";
        frmATMBranch.btnBrch.skin = "btnBranchFocus";
        frmATMBranch.btnExgBooth.skin = "btnExchangeBooth";
        //getGeoLocation();
        androidMPermissionforLocation();
    } catch (e) {}
}

function onClickExchangeBooth() {
    try {
        gblFindTMBType = "EX";
        frmATMBranch.btnAtm.skin = "btnATMButton";
        frmATMBranch.btnBrch.skin = "btnBranch";
        frmATMBranch.btnExgBooth.skin = "btnExchangeBoothFocus";
        //getGeoLocation();
        androidMPermissionforLocation();
    } catch (e) {}
}

function androidMPermissionforLocation() {
    if (isAndroidM()) {
        kony.print("Brajesh: CALLIING PERMISSIONS FFI....");
        var permissionsArr = [gblPermissionsJSONObj.LOCATION_GROUP];
        //Creates an object of class 'PermissionFFI'
        var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
        //Invokes method 'checkpermission' on the object
        RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr, null, callBackandroidMPermissionforLocation);
    } else {
        getGeoLocation();
    }
}

function callBackandroidMPermissionforLocation(result) {
    kony.print("result " + result);
    if (result == "1") {
        kony.print("Brajesh: IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
        getGeoLocation();
    } else {
        kony.print("Brajesh: IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
        //var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
        //alert(messageErr);
    }
}
var GeoLocationObject = null;

function getGeoLocation() {
    try {
        //var deviceInfo = kony.os.deviceInfo();
        //var deviceName = deviceInfo.name;
        //var deviceType = deviceInfo["type"];
        if (gblLatitude == "" || gblLongitude == "") {
            //var positionoptions = {enableHighAccuracy: true, timeout: 15000, maximumAge: 20000 };
            var locationData = null;
            //if(deviceName == "iPhone" || deviceType == "spa"){
            //				locationData = kony.location.getCurrentPosition(successcallback, errorcallback);
            //				
            //			}else if(deviceName == "android"){
            locationData = kony.location.getCurrentPosition(successcallback, errorcallback);
            //}else{
            //				locationData = kony.location.getCurrentPosition(successcallback, errorcallback);
            //				
            //			}
        } else {
            setDataMap();
        }
    } catch (e) {}
}

function successcallback(position) {
    try {
        if (position.coords.latitude != "" || position.coords.longitude != "") {
            gblLatitude = position.coords.latitude;
            gblLongitude = position.coords.longitude;
            if (flowSpa) {
                gblIsCurrentLcoation = true;
                gblCurrenLocLat = gblLatitude;
                gblCurrenLocLon = gblLongitude;
            }
        } else {
            gblLatitude = kony.i18n.getLocalizedString("TMBLatitude");
            gblLongitude = kony.i18n.getLocalizedString("TMBLongitude");
        }
        setDataMap();
    } catch (e) {}
}

function errorcallback(positionerror) {
    try {
        var errorMesg = "Error code:::::" + positionerror.code + "/@@@Message:::::" + positionerror.message;
        if (flowSpa) {
            gblIsCurrentLcoation = false;
            if (positionerror.code == 1) {
                gblLatitude = kony.i18n.getLocalizedString("TMBLatitude");
                gblLongitude = kony.i18n.getLocalizedString("TMBLongitude");
                setDataMap();
            } else if (positionerror.code == 2) {
                gblLatitude = kony.i18n.getLocalizedString("TMBLatitude");
                gblLongitude = kony.i18n.getLocalizedString("TMBLongitude");
                setDataMap();
            }
        } else {
            if (positionerror.code == 102) {
                gblLatitude = kony.i18n.getLocalizedString("TMBLatitude");
                gblLongitude = kony.i18n.getLocalizedString("TMBLongitude");
                setDataMap();
            } else if (positionerror.code == 103) {
                gblLatitude = kony.i18n.getLocalizedString("TMBLatitude");
                gblLongitude = kony.i18n.getLocalizedString("TMBLongitude");
                setDataMap();
            } else if (positionerror.code == 105) {
                gblLatitude = kony.i18n.getLocalizedString("TMBLatitude");
                gblLongitude = kony.i18n.getLocalizedString("TMBLongitude");
                setDataMap();
            }
        }
    } catch (e) {}
}

function successcallbackAndriod(lat, lon) {
    try {
        if (lat != "" || lon != "") {
            gblIsCurrentLcoation = true;
            gblLatitude = lat;
            gblLongitude = lon;
            gblCurrenLocLat = gblLatitude;
            gblCurrenLocLon = gblLongitude;
            setDataMap();
        }
    } catch (e) {}
}

function errorcallbackAndroid(errorCode) {
    try {
        gblLatitude = kony.i18n.getLocalizedString("TMBLatitude");;
        gblLongitude = kony.i18n.getLocalizedString("TMBLongitude");;
        gblIsCurrentLcoation = false;
        setDataMap();
    } catch (e) {}
}

function setDataMap() {
    try {
        var locatorInputParams = {};
        showLoadingScreen();
        locatorInputParams["applicationchannel"] = "MB";
        locatorInputParams["branchType"] = gblFindTMBType;
        locatorInputParams["latitude"] = gblLatitude;
        locatorInputParams["longitude"] = gblLongitude;
        invokeServiceSecureAsync("ATMLocatorService", locatorInputParams, setDataMapCallBack)
    } catch (e) {}
}

function setDataMapCallBack(status, resulttable) {
    try {
        //
        gbl3dTouchAction = "";
        var locatorResultSet = [];
        var tempLocaterData = [];
        gblMapData = [];
        gblMapData = resulttable;
        isListDetails = false;
        var isAndroid = true;
        var isIphone = true;
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                frmATMBranch.btnSearchIcon.skin = "btnFindSearch";
                frmATMBranch.hboxSearch.setVisibility(false);
                var currentLocale = kony.i18n.getCurrentLocale();
                //if(gblDeviceInfo["name"] == "android"){
                isAndroid = false;
                isIphone = true;
                frmATMBranch.mapATMBranch.widgetDataMapForCallout = {
                    lblName: "tmbName",
                    lblAddress: "tmbAddress",
                    btnGetDirection: "btnGetDirection",
                    btnDetls: "btnDetls",
                    btnAndroidDetails: "btnAndroidDetails"
                };
                if (gblDeviceInfo["name"] == "android" || flowSpa) {
                    if (gblIsCurrentLcoation) {
                        tempLocaterData = {
                            lat: gblCurrenLocLat,
                            lon: gblCurrenLocLon,
                            image: "currentpin.png",
                            showcallout: true,
                            calloutData: {
                                tmbName: {
                                    "isVisible": true,
                                    "text": kony.i18n.getLocalizedString("FindTMB_Youarehere")
                                },
                                tmbAddress: {
                                    "isVisible": false,
                                    "text": ""
                                },
                                btnGetDirection: {
                                    "isVisible": false,
                                    "text": ""
                                },
                                btnDetls: {
                                    "isVisible": false,
                                    "text": ""
                                },
                                btnAndroidDetails: {
                                    "isVisible": false,
                                    "text": ""
                                }
                            }
                        };
                        locatorResultSet.push(tempLocaterData);
                    }
                }
                for (var i = 0; i < resulttable["mapDataOfATMLocator"].length; i++) {
                    var address = "";
                    var name = ""
                    var workingHrs = "";
                    if (gblFindTMBType == "ATM") {
                        if (currentLocale == "en_US") {
                            name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_EN"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
                            workingHrs = " ";
                        } else if (currentLocale == "th_TH") {
                            name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_TH"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
                            workingHrs = " ";
                        }
                        tempLocaterData = {
                            lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
                            lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
                            image: "locator_pin.png",
                            showcallout: true,
                            calloutData: {
                                tmbName: name,
                                tmbAddress: address,
                                btnGetDirection: {
                                    "isVisible": isIphone,
                                    "text": kony.i18n.getLocalizedString("FindTMB_GetDirections")
                                },
                                btnDetls: {
                                    "isVisible": isIphone,
                                    "text": kony.i18n.getLocalizedString("FindTMB_Details")
                                },
                                btnAndroidDetails: {
                                    "isVisible": isAndroid,
                                    "text": kony.i18n.getLocalizedString("FindTMB_Details")
                                }
                            },
                            branchName: name,
                            addressLine: address,
                            engbranchName: resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_EN"],
                            thaibranchName: resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_TH"],
                            engAddressLine: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
                            thaiAddressLine: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
                            engOfficeHrs: workingHrs,
                            thaiOfficeHrs: workingHrs,
                            primaryPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
                            otherPhoneNo: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            faxNumber: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            officeHrs: workingHrs
                        };
                    } else if (gblFindTMBType == "BRANCH") {
                        if (currentLocale == "en_US") {
                            name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_EN"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"];
                        } else if (currentLocale == "th_TH") {
                            name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_TH"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"];
                        }
                        tempLocaterData = {
                            lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
                            lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
                            image: "locator_pin.png",
                            showcallout: true,
                            calloutData: {
                                tmbName: name,
                                tmbAddress: address,
                                btnGetDirection: {
                                    "isVisible": isIphone,
                                    "text": kony.i18n.getLocalizedString("FindTMB_GetDirections")
                                },
                                btnDetls: {
                                    "isVisible": isIphone,
                                    "text": kony.i18n.getLocalizedString("FindTMB_Details")
                                },
                                btnAndroidDetails: {
                                    "isVisible": isAndroid,
                                    "text": kony.i18n.getLocalizedString("FindTMB_Details")
                                }
                            },
                            branchName: name,
                            addressLine: address,
                            primaryPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
                            otherPhoneNo: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            engbranchName: resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_EN"],
                            thaibranchName: resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_TH"],
                            engAddressLine: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
                            thaiAddressLine: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
                            engOfficeHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"],
                            thaiOfficeHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"],
                            faxNumber: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            officeHrs: workingHrs
                        };
                    }
                    if (gblFindTMBType == "EX") {
                        if (currentLocale == "en_US") {
                            name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"];
                            if (name != null || name != undefined || name != "") {
                                name = " ";
                            } else {
                                name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"];
                            }
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_EN"];
                        } else if (currentLocale == "th_TH") {
                            name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"];
                            if (name != null || name != undefined || name != "") {
                                name = " ";
                            } else {
                                name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"];
                            }
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_TH"];
                        }
                        tempLocaterData = {
                            lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
                            lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
                            image: "locator_pin.png",
                            showcallout: true,
                            calloutData: {
                                tmbName: name,
                                tmbAddress: address,
                                btnGetDirection: {
                                    "isVisible": isIphone,
                                    "text": kony.i18n.getLocalizedString("FindTMB_GetDirections")
                                },
                                btnDetls: {
                                    "isVisible": isIphone,
                                    "text": kony.i18n.getLocalizedString("FindTMB_Details")
                                },
                                btnAndroidDetails: {
                                    "isVisible": isAndroid,
                                    "text": kony.i18n.getLocalizedString("FindTMB_Details")
                                }
                            },
                            branchName: name,
                            addressLine: address,
                            primaryPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
                            engbranchName: resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"],
                            thaibranchName: resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"],
                            engAddressLine: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
                            thaiAddressLine: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
                            engOfficeHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"],
                            thaiOfficeHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"],
                            //otherPhoneNo: "", 
                            faxNumber: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            officeHrs: workingHrs
                        };
                    }
                    locatorResultSet.push(tempLocaterData);
                    tempLocaterData = [];
                }
                frmATMBranch.mapATMBranch.calloutWidth = 55;
                frmATMBranch.mapATMBranch.locationData = locatorResultSet;
                if (locatorResultSet.length == 0) {
                    showAlertRcMB(kony.i18n.getLocalizedString("FindTMBResultErrorMsg"), kony.i18n.getLocalizedString("info"), "info");
                } else {
                    if (gblDeviceInfo["name"] == "android" || flowSpa) {
                        currentLocDetails = locatorResultSet[0];
                        if (!flowSpa) {
                            frmATMBranch.mapATMBranch.navigateToLocation(locatorResultSet[0], true, true);
                        }
                    }
                }
                //DEF668
                dismissLoadingScreen();
                frmATMBranch.show();
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } catch (e) {}
}

function onSelectMapPin(mapLocationData) {
    try {
        currentLocDetails = mapLocationData;
        /* var name = "";
        var address = "";
        name = mapLocationData.branchName;
        address = mapLocationData.addressLine;
        if(name == null || name == ""){popATMBranch.lblName.text = "";}else{popATMBranch.lblName.text = name}
        if(address == null || address == ""){popATMBranch.lblAddress.text = "";}else{popATMBranch.lblAddress.text = address}
        popATMBranch.btnGetDirection.text = kony.i18n.getLocalizedString("FindTMB_GetDirections");
        popATMBranch.btnDtls.text = kony.i18n.getLocalizedString("FindTMB_Details");
        if(frmATMBranch.hboxSearch.isVisible){
        	frmATMBranch.btnSearchIcon.skin = "btnFindSearch";
        	frmATMBranch.hboxSearch.setVisibility(false);
        }
        //var popUpContext= {"widget":frmATMBranch.mapATMBranch, "anchor":"center", "sizetoanchorwidth":false};
        //popATMBranch.setContext(popUpContext);
        popATMBranch.show();*/
    } catch (e) {}
}

function onClickDetails() {
    try {
        if (currentLocDetails != null) {
            //popATMBranch.dismiss();
            var name = "";
            var phoneNumber = "";
            var faxNumber = ""
            var officeHrs = "";
            var currentLocale = kony.i18n.getCurrentLocale();
            if (currentLocDetails.primaryPhoneNo != "") {
                if (currentLocDetails.otherPhoneNo == "" || currentLocDetails.otherPhoneNo == null) {
                    currentLocDetails.otherPhoneNo = " ";
                }
                phoneNumber = currentLocDetails.primaryPhoneNo + ", " + currentLocDetails.otherPhoneNo
            }
            if (currentLocDetails.faxNumber == "" || currentLocDetails.faxNumber == null) {
                faxNumber = "-";
            } else {
                faxNumber = currentLocDetails.faxNumber
            }
            if (gblFindTMBType == "ATM") {
                frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
            } else if (gblFindTMBType == "BRANCH") {
                frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
            } else if (gblFindTMBType == "EX") {
                frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
            }
            if (currentLocale == "en_US") {
                if (currentLocDetails.engbranchName == "" || currentLocDetails.engbranchName == null) {
                    name = " ";
                } else {
                    name = currentLocDetails.engbranchName;
                }
                frmATMBranchesDetails.lblName.text = name;
                frmATMBranchesDetails.lblAddress.text = currentLocDetails.engAddressLine;
                if (currentLocDetails.engOfficeHrs == "" || currentLocDetails.engOfficeHrs == null) {
                    officeHrs = "-";
                } else {
                    officeHrs = currentLocDetails.engOfficeHrs;
                }
                frmATMBranchesDetails.lblOfficeHrs.text = officeHrs;
            } else if (currentLocale == "th_TH") {
                if (currentLocDetails.thaibranchName == "" || currentLocDetails.thaibranchName == null) {
                    name = "-";
                } else {
                    name = currentLocDetails.thaibranchName;
                }
                frmATMBranchesDetails.lblName.text = name;
                frmATMBranchesDetails.lblAddress.text = currentLocDetails.thaiAddressLine;
                if (currentLocDetails.thaiOfficeHrs == "" || currentLocDetails.thaiOfficeHrs == null) {
                    officeHrs = "-";
                } else {
                    officeHrs = currentLocDetails.thaiOfficeHrs;
                }
                frmATMBranchesDetails.lblOfficeHrs.text = officeHrs;
            }
            frmATMBranchesDetails.lblPhoneNo.text = phoneNumber;
            frmATMBranchesDetails.lblFaxNo.text = faxNumber;
            if (gblFindTMBType == "ATM") {
                if (frmATMBranchesDetails.hboxTel.isVisible) {
                    frmATMBranchesDetails.hboxTel.setVisibility(false);
                    frmATMBranchesDetails.hboxFax.setVisibility(false);
                    frmATMBranchesDetails.hboxOfficeHrs.setVisibility(false);
                    frmATMBranchesDetails.bttnCall.setVisibility(false);
                }
            } else {
                if (!frmATMBranchesDetails.hboxTel.isVisible) {
                    frmATMBranchesDetails.hboxTel.setVisibility(true);
                    frmATMBranchesDetails.hboxFax.setVisibility(true);
                    frmATMBranchesDetails.hboxOfficeHrs.setVisibility(true);
                    frmATMBranchesDetails.bttnCall.setVisibility(true);
                }
            }
            frmATMBranchesDetails.show();
        }
    } catch (e) {}
}

function onclickBySearchIcon() {
    try {
        var btnSkin = frmATMBranch.btnSearchIcon.skin;
        /*frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
        frmATMBranch.btnCombDistrict.text = kony.i18n.getLocalizedString("FindTMB_District");
        frmATMBranch.btnCombDistrict.setEnabled(false);
        provinceID = "";
        districtID = "";
        frmATMBranch.txtKeyword.text = "";*/
        if (btnSkin == "btnFindSearch") {
            frmATMBranch.btnSearchIcon.skin = "btnFindSearchFocus";
            frmATMBranch.hboxSearch.setVisibility(true);
        } else if (btnSkin == "btnFindSearchFocus") {
            frmATMBranch.btnSearchIcon.skin = "btnFindSearch";
            frmATMBranch.hboxSearch.setVisibility(false);
        }
    } catch (e) {}
}

function callATMProvinceData() {
    try {
        showLoadingScreen();
        gblATMSearchFlag = "Province"
        var inputParams = {};
        invokeServiceSecureAsync("GetProvinceID", inputParams, getProvinceDateCallBack);
    } catch (e) {}
}

function callATMDistrictData() {
    try {
        if (provinceID != "") {
            showLoadingScreen();
            gblATMSearchFlag = "District"
            var inputParams = {};
            inputParams["provinceCD"] = provinceID;
            invokeServiceSecureAsync("GetDistrictID", inputParams, getProvinceDateCallBack);
        }
    } catch (e) {}
}

function getProvinceDateCallBack(status, resulttable) {
    try {
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                if (gblATMSearchFlag == "Province") {
                    if (resulttable["state"][0].length != 0) {
                        gblProvinceData = resulttable["state"];
                        populateProvinceDistrictData(resulttable["state"]);
                    }
                } else if (gblATMSearchFlag == "District") {
                    if (resulttable["district"][0].length != 0) {
                        gblDistrictData = resulttable["district"];
                        populateProvinceDistrictData(resulttable["district"]);
                    }
                }
            } else {
                dismissLoadingScreen();
                showAlertRcMB(kony.i18n.getLocalizedString("FindTMBResultErrorMsg"), kony.i18n.getLocalizedString("info"), "info");
            }
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } catch (e) {}
}

function populateProvinceDistrictData(provinceDistrictResultSet) {
    try {
        resultTableData = [];
        var currentLocale = kony.i18n.getCurrentLocale();
        for (var i = 0; i < provinceDistrictResultSet.length; i++) {
            var tempData = [];
            var provinceDTName = "";
            if (gblATMSearchFlag == "Province") {
                if (currentLocale == "en_US") {
                    provinceDTName = provinceDistrictResultSet[i].ProvinceNameEN;
                } else if (currentLocale == "th_TH") {
                    provinceDTName = provinceDistrictResultSet[i].ProvinceNameTH;
                }
                tempData = {
                    lblProvince: provinceDTName,
                    provinceID: provinceDistrictResultSet[i].ProvinceCD
                }
            } else if (gblATMSearchFlag == "District") {
                if (currentLocale == "en_US") {
                    provinceDTName = provinceDistrictResultSet[i].DistrictNameEN;
                } else if (currentLocale == "th_TH") {
                    provinceDTName = provinceDistrictResultSet[i].DistrictNameTH;
                }
                tempData = {
                    lblProvince: provinceDTName,
                    provinceID: provinceDistrictResultSet[i].DistrictCD
                }
            }
            resultTableData.push(tempData);
        }
        popUpProvinceData.segProvinceData.setData(resultTableData);
        dismissLoadingScreen();
        if (flowSpa) popUpProvinceData.skin = "popUpSpaWhiteBg";
        //popUpProvinceData.show();
    } catch (e) {}
}

function onClickSegProvinceDistrict() {
    try {
        var selectedItem = popUpProvinceData.segProvinceData.selectedItems[0].lblProvince;
        if (gblATMSearchFlag == "Province") {
            provinceID = popUpProvinceData.segProvinceData.selectedItems[0].provinceID;
            frmATMBranch.btnCombProvince.text = selectedItem;
        } else if (gblATMSearchFlag == "District") {
            districtID = popUpProvinceData.segProvinceData.selectedItems[0].provinceID;
            frmATMBranch.btnCombDistrict.text = selectedItem;
        }
        if (provinceID != null) {
            frmATMBranch.btnCombDistrict.setEnabled(true);
        }
        popUpProvinceData.dismiss();
    } catch (e) {}
}

function onClickSearchBtn() {
    try {
        isListDetails = true;
        var textKey = frmATMBranch.txtKeyword.text;
        if (textKey == "" && provinceID == "" && districtID == "") {
            showAlertRcMB(kony.i18n.getLocalizedString("FindTMB_SearchValidation1"), kony.i18n.getLocalizedString("info"), "info");
            //alert(kony.i18n.getLocalizedString("FindTMB_SearchValidation1"));
        } else if (textKey == "" && provinceID != "" && districtID == "") {
            showAlertRcMB(kony.i18n.getLocalizedString("FindTMB_SearchValidation2"), kony.i18n.getLocalizedString("info"), "info");
            //alert(kony.i18n.getLocalizedString("FindTMB_SearchValidation2"));
        } else {
            var inputParams = {};
            showLoadingScreen();
            inputParams["branchType"] = gblFindTMBType;
            inputParams["provinceId"] = provinceID;
            inputParams["districtId"] = districtID;
            inputParams["serachText"] = textKey;
            invokeServiceSecureAsync("ATMLocatorService", inputParams, onSuccesATMSearch);
        }
    } catch (e) {}
}

function onSuccesATMSearch(status, resulttable) {
    try {
        var listData = [];
        var currentLocale = kony.i18n.getCurrentLocale();
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                for (var i = 0; i < resulttable["mapDataOfATMLocator"].length; i++) {
                    var tempData = [];
                    var address = "";
                    var name = "";
                    var workingHrs = "";
                    if (gblFindTMBType == "ATM") {
                        if (currentLocale == "en_US") {
                            name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_EN"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
                            //workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"];
                        } else if (currentLocale == "th_TH") {
                            name = resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_TH"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
                            //workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"];
                        }
                        tempData = {
                            lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
                            lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
                            lblName: name,
                            engName: resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_EN"],
                            thaiName: resulttable["mapDataOfATMLocator"][i]["ATM_ADM_NAME_TH"],
                            lblAddress: address,
                            engAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
                            thaiAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
                            imgArrow: "bg_arrow_right.png",
                            lblPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
                            lblFaxno: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            lblWorkingHrs: workingHrs,
                            engWorkingHrs: workingHrs,
                            thaiWorkingHrs: workingHrs
                        };
                    } else if (gblFindTMBType == "BRANCH") {
                        if (currentLocale == "en_US") {
                            name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_EN"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"];
                        } else if (currentLocale == "th_TH") {
                            name = resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_TH"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"];
                        }
                        tempData = {
                            lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
                            lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
                            lblName: name,
                            engName: resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_EN"],
                            thaiName: resulttable["mapDataOfATMLocator"][i]["BRANCH_NAME_TH"],
                            lblAddress: address,
                            engAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
                            thaiAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
                            imgArrow: "bg_arrow_right.png",
                            lblPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
                            lblFaxno: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            lblWorkingHrs: workingHrs,
                            engWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_EN"],
                            thaiWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BRANCH_WORKING_TIME_TH"]
                        };
                    } else if (gblFindTMBType == "EX") {
                        if (currentLocale == "en_US") {
                            name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_EN"];
                        } else if (currentLocale == "th_TH") {
                            name = resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"];
                            address = resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"];
                            workingHrs = resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_TH"];
                        }
                        tempData = {
                            lat: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LADTITUDE_INFO"],
                            lon: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_LONGTITUDE_INFO"],
                            lblName: name,
                            engName: resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_EN"],
                            thaiName: resulttable["mapDataOfATMLocator"][i]["EXCHANGE_BOOTH_NAME_TH"],
                            lblAddress: address,
                            engAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_EN"],
                            thaiAddress: resulttable["mapDataOfATMLocator"][i]["BR_ATM_EX_ADDRESS_TH"],
                            imgArrow: "bg_arrow_right.png",
                            lblPhoneNo: resulttable["mapDataOfATMLocator"][i]["PRIMARY_PHONE_NUMBER"],
                            lblFaxno: resulttable["mapDataOfATMLocator"][i]["OTHER_PHONE_NUMBER"],
                            lblWorkingHrs: workingHrs,
                            engWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_EN"],
                            thaiWorkingHrs: resulttable["mapDataOfATMLocator"][i]["BOOTH_WORKING_TIME_TH"]
                        };
                    }
                    listData.push(tempData);
                }
                frmATMBranchList.segATMListDetails.setData(listData);
                dismissLoadingScreen();
                if (resulttable["mapDataOfATMLocator"].length == 0) {
                    showAlertRcMB(kony.i18n.getLocalizedString("FindTMBResultErrorMsg"), kony.i18n.getLocalizedString("info"), "info");
                } else {
                    frmATMBranchList.show();
                }
            } else {
                dismissLoadingScreen();
                showAlertRcMB(kony.i18n.getLocalizedString("FindTMBResultErrorMsg"), kony.i18n.getLocalizedString("info"), "info");
            }
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } catch (e) {}
}

function onClickBackList() {
    try {
        frmATMBranchList.segATMListDetails.removeAll();
        if (frmATMBranch.hboxSearch.isVisible) {
            frmATMBranch.btnSearchIcon.skin = "btnFindSearch";
            frmATMBranch.hboxSearch.setVisibility(false);
        }
        frmATMBranch.show();
    } catch (e) {}
}

function onClickListDetails() {
    try {
        var slectedName = "";
        var workingHrs = "";
        var address = "";
        var phoneNumber = "";
        var faxNumber = "";
        var workingHrs = "";
        var currentLocale = kony.i18n.getCurrentLocale();
        if (gblFindTMBType == "ATM") {
            frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
        } else if (gblFindTMBType == "BRANCH") {
            frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
        } else if (gblFindTMBType == "EX") {
            frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
        }
        if (currentLocale == "en_US") {
            slectedName = frmATMBranchList.segATMListDetails.selectedItems[0].engName;
            address = frmATMBranchList.segATMListDetails.selectedItems[0].engAddress;
            workingHrs = frmATMBranchList.segATMListDetails.selectedItems[0].engWorkingHrs;
        } else if (currentLocale == "th_TH") {
            slectedName = frmATMBranchList.segATMListDetails.selectedItems[0].thaiName;
            address = frmATMBranchList.segATMListDetails.selectedItems[0].thaiAddress;
            workingHrs = frmATMBranchList.segATMListDetails.selectedItems[0].thaiWorkingHrs;
        }
        //slectedName = frmATMBranchList.segATMListDetails.selectedItems[0].lblName;
        //address = frmATMBranchList.segATMListDetails.selectedItems[0].lblAddress;
        //workingHrs = frmATMBranchList.segATMListDetails.selectedItems[0].lblWorkingHrs;
        phoneNumber = frmATMBranchList.segATMListDetails.selectedItems[0].lblPhoneNo;
        faxNumber = frmATMBranchList.segATMListDetails.selectedItems[0].lblFaxno;
        if (slectedName == null || slectedName == "") {
            frmATMBranchesDetails.lblName.text = "";
        } else {
            frmATMBranchesDetails.lblName.text = slectedName;
        }
        if (address == null || address == "") {
            frmATMBranchesDetails.lblAddress.text = "";
        } else {
            frmATMBranchesDetails.lblAddress.text = address;
        }
        if (phoneNumber == null || phoneNumber == "") {
            frmATMBranchesDetails.lblPhoneNo.text = "";
        } else {
            frmATMBranchesDetails.lblPhoneNo.text = phoneNumber;
        }
        if (faxNumber == null || faxNumber == "") {
            frmATMBranchesDetails.lblFaxNo.text = "-";
        } else {
            frmATMBranchesDetails.lblFaxNo.text = faxNumber;
        }
        if (workingHrs == null || workingHrs == "") {
            frmATMBranchesDetails.lblOfficeHrs.text = "";
        } else {
            frmATMBranchesDetails.lblOfficeHrs.text = workingHrs;
        }
        if (gblFindTMBType == "ATM") {
            if (frmATMBranchesDetails.hboxTel.isVisible) {
                frmATMBranchesDetails.hboxTel.setVisibility(false);
                frmATMBranchesDetails.hboxFax.setVisibility(false);
                frmATMBranchesDetails.hboxOfficeHrs.setVisibility(false);
                frmATMBranchesDetails.bttnCall.setVisibility(false);
            }
        } else {
            if (!frmATMBranchesDetails.hboxTel.isVisible) {
                frmATMBranchesDetails.hboxTel.setVisibility(true);
                frmATMBranchesDetails.hboxFax.setVisibility(true);
                frmATMBranchesDetails.hboxOfficeHrs.setVisibility(true);
                frmATMBranchesDetails.bttnCall.setVisibility(true);
            }
        }
        frmATMBranchesDetails.show();
    } catch (e) {}
}

function onClickCallBtn() {
    try {
        var phoneNumber = frmATMBranchesDetails.lblPhoneNo.text;
        var splitPhNo = "";
        splitPhNo = kony.string.split(phoneNumber, ",");
        phoneNumber = splitPhNo[1];
        kony.phone.dial(phoneNumber);
    } catch (e) {}
}

function onClickGetDirectionMap() {
    try {
        var url = "";
        url = "https://maps.google.com/maps?saddr=" + gblLatitude + "," + gblLongitude + "&daddr=" + currentLocDetails.lat + "," + currentLocDetails.lon;
        //
        kony.application.openURL(url);
    } catch (e) {}
}

function onClickGetDirectionList() {
    try {
        var url = "";
        var destLat = "";
        var destLon = "";
        if (kony.application.getPreviousForm().id == "frmATMBranchList") {
            destLat = frmATMBranchList.segATMListDetails.selectedItems[0].lat;
            destLon = frmATMBranchList.segATMListDetails.selectedItems[0].lon;
        } else if (kony.application.getPreviousForm().id == "frmATMBranch") {
            destLat = currentLocDetails.lat;
            destLon = currentLocDetails.lon;
        }
        url = "https://maps.google.com/maps?saddr=" + gblLatitude + "," + gblLongitude + "&daddr=" + destLat + "," + destLon;
        kony.application.openURL(url);
    } catch (e) {}
}

function onClickBackDetails() {
    try {
        var previousForm = kony.application.getPreviousForm().id;
        if (previousForm == "frmATMBranchList") {
            frmATMBranchList.segATMListDetails.removeAll();
            onClickSearchBtn();
            //frmATMBranchList.show();
        } else if (previousForm == "frmATMBranch") {
            setDataMapCallBack(400, gblMapData)
        }
    } catch (e) {}
}

function frmATMBranchPreShow() {
    changeStatusBarColor();
    try {
        if (!(LocaleController.isFormUpdatedWithLocale(frmATMBranch.id))) {
            frmATMBranch.lblHeaderText.text = kony.i18n.getLocalizedString("MenuFindTMB");
            frmATMBranch.btnAtm.text = kony.i18n.getLocalizedString("FindTMB_Atm");
            frmATMBranch.btnBrch.text = kony.i18n.getLocalizedString("FindTMB_Branch");
            frmATMBranch.btnExgBooth.text = kony.i18n.getLocalizedString("FindTMB_ExchangeBooth");
            frmATMBranch.btnSearchFind.text = kony.i18n.getLocalizedString("FindTMB_Search");
            //frmATMBranch.txtKeyword.placeholder = kony.i18n.getLocalizedString("FindTMB_Keyword");
            LocaleController.updatedForm(frmATMBranch.id);
        }
    } catch (e) {}
}

function frmATMBranchListPreShow() {
    changeStatusBarColor();
    try {
        if (!(LocaleController.isFormUpdatedWithLocale(frmATMBranchList.id))) {
            frmATMBranchList.lblHeaderText.text = kony.i18n.getLocalizedString("FindTBM_FindTMB");
            frmATMBranchList.bttnBack.text = kony.i18n.getLocalizedString("Back");
            LocaleController.updatedForm(frmATMBranchList.id);
        }
    } catch (e) {}
}

function frmATMBranchesDetailsPreShow() {
    changeStatusBarColor();
    try {
        if (!(LocaleController.isFormUpdatedWithLocale(frmATMBranchesDetails.id))) {
            if (gblFindTMBType == "ATM") {
                frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
            } else if (gblFindTMBType == "BRANCH") {
                frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
            } else if (gblFindTMBType == "EX") {
                frmATMBranchesDetails.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
            }
            frmATMBranchesDetails.lblTel.text = kony.i18n.getLocalizedString("FindTMB_Tel");
            frmATMBranchesDetails.lblFax.text = kony.i18n.getLocalizedString("FindTMB_Fax");
            frmATMBranchesDetails.lblOfficeHrsText.text = kony.i18n.getLocalizedString("FindTMB_OfficeHours");
            frmATMBranchesDetails.btnGetDir.text = kony.i18n.getLocalizedString("FindTMB_GetDirections");
            frmATMBranchesDetails.bttnCall.text = kony.i18n.getLocalizedString("FindTMB_Call");
            if (flowSpa) {
                frmATMBranchesDetails.bttnBack.text = kony.i18n.getLocalizedString("keyBackSpa");
            } else {
                frmATMBranchesDetails.bttnBack.text = kony.i18n.getLocalizedString("Back");
            }
            LocaleController.updatedForm(frmATMBranchesDetails.id);
        }
    } catch (e) {}
}

function synfrmATMBranch() {
    try {
        setDataMap();
        if (provinceID != "") {
            gblATMSearchFlag = "Province";
            populateProvinceDistrictData(gblProvinceData);
        }
        if (districtID != "") {
            gblATMSearchFlag = "District";
            populateProvinceDistrictData(gblDistrictData);
        }
        frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
        frmATMBranch.btnCombDistrict.text = kony.i18n.getLocalizedString("FindTMB_District");
        provinceID = "";
        districtID = "";
    } catch (e) {}
}

function synfrmATMBranchesDetails() {
    try {
        if (isListDetails) {
            onClickListDetails();
        } else {
            onClickDetails();
        }
    } catch (e) {}
}