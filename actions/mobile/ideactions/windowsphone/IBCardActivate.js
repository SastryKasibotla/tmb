var glbPin = "";
var glbPinArr = [];
var glbRandomRang = [];
var glbDigitWeight = [];
var glbIndex = 0;
var maxLenPin = 4;
var iFlag = true;
var localeIndex = 0;
var isConfirmPin = false;
var emptyPinSkin = "btnIBEmpyATMPin";
var fillPinSkin = "btnIBFillATMPin";
var disablePinSkin = "btnIBEmptyATMPINDisable";
var maskCardNo;
/*
************************************************************************
      Function Name : deleteATMPIN
            Purpose : Deleting ATM PIN when customer press delete button.
************************************************************************
*/
function deleteATMPIN() {
    if (isConfirmPin) {
        if (isNotBlank(glbPin)) {
            decreaseATMPINVal();
        } else {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
            toggleConfirmPIN(false);
            toggleKeyPad(false);
            decreaseATMPINVal();
        }
    } else {
        if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenPin) {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
        }
        if (isNotBlank(glbPin)) {
            decreaseATMPINVal();
            toggleConfirmPIN(false);
            toggleKeyPad(false);
        }
    }
}
/*
************************************************************************
      Function Name : decreaseATMPINVal
            Purpose : Decrease ATM PIN Value
************************************************************************
*/
function decreaseATMPINVal() {
    --glbIndex;
    glbPin = glbPin.substring(0, glbPin.length - 1);
    assignATMPINRender("");
    toggleButton(false);
}
/*
************************************************************************
      Function Name : clearATMPIN
            Purpose : Clear all ATM PIN enterd.
************************************************************************
*/
function clearATMPIN() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    toggleButton(false);
    toggleConfirmPIN(false);
    toggleKeyPad(false);
    assignATMPINRender("");
}
/*
************************************************************************
      Function Name : assignATMPINRender
            Purpose : Assign ATM PIN
************************************************************************
*/
function assignATMPINRender(pin) {
    if (isNotBlank(pin)) {
        if (isConfirmPin) {
            if (glbPin.length < maxLenPin) {
                addConfirmPin(pin, ++glbIndex);
            }
        } else {
            addPin(pin, ++glbIndex);
        }
    } else {
        renderPIN();
    }
    kony.print("glbPin :" + glbPin);
}
/*
************************************************************************
      Function Name : renderPIN
            Purpose : Render PIN images
************************************************************************
*/
function renderPIN() {
    var btnObject = [];
    if (isConfirmPin) {
        btnObject = [frmIBDebitCardActivateAssignPIN.btnConfirmFirstPin, frmIBDebitCardActivateAssignPIN.btnConfirmSecondPin, frmIBDebitCardActivateAssignPIN.btnConfirmThirdPin, frmIBDebitCardActivateAssignPIN.btnConfirmFourthPin];
    } else {
        btnObject = [frmIBDebitCardActivateAssignPIN.btnFirstPin, frmIBDebitCardActivateAssignPIN.btnSecondPin, frmIBDebitCardActivateAssignPIN.btnThirdPin, frmIBDebitCardActivateAssignPIN.btnFourthPin];
    }
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].skin = fillPinSkin;
        btnObject[i].hoverSkin = fillPinSkin;
        btnObject[i].focusSkin = fillPinSkin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].skin = emptyPinSkin;
        btnObject[i - 1].hoverSkin = emptyPinSkin;
        btnObject[i - 1].focusSkin = emptyPinSkin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].skin = disablePinSkin;
            btnObject[i - 1].hoverSkin = disablePinSkin;
            btnObject[i - 1].focusSkin = disablePinSkin;
        }
    }
}
/*
************************************************************************
      Function Name : toggleConfirmPIN
            Purpose : enable/disable confirm PIN
************************************************************************
*/
function toggleConfirmPIN(command) {
    frmIBDebitCardActivateAssignPIN.hbxPinConfirmRender.setVisibility(command);
    frmIBDebitCardActivateAssignPIN.hbxConfirmPIN.setVisibility(command);
    /*	var btnObject = [frmIBCardActivation.btnConfirmFirstPin, frmIBCardActivation.btnConfirmSecondPin, frmIBCardActivation.btnConfirmThirdPin, frmIBCardActivation.btnConfirmFourthPin];
    	for(var i=0; i < btnObject.length; i++){
    			btnObject[i].skin = command ? emptyPinSkin : disablePinSkin;
    			btnObject[i].hoverSkin = command ? emptyPinSkin : disablePinSkin;
    			btnObject[i].focusSkin = command ? emptyPinSkin : disablePinSkin;
    			btnObject[i].setEnabled(command);
    	}
    */
}
/*
************************************************************************
      Function Name : addPin
            Purpose : Add new PIN
************************************************************************
*/
function addPin(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderPIN();
    if (glbPin.length == maxLenPin) {
        toggleKeyPad(true);
        toggleConfirmPIN(true);
        glbPinArr[0] = glbPin;
        glbPin = "";
        glbIndex = 0;
        renderPIN();
    }
}
/*
************************************************************************
      Function Name : addPin
            Purpose : Add new PIN
************************************************************************
*/
function addConfirmPin(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderPIN();
    if (glbPin.length == maxLenPin) {
        toggleButton(true);
    }
}
/*
************************************************************************
      Function Name : toggleKeyPad
            Purpose : set keypad state generate PIN (PIN/Confirm PIN)
************************************************************************
*/
function toggleKeyPad(command) {
    isConfirmPin = command;
    var isConfirmBox = frmIBDebitCardActivateAssignPIN.hbxPinConfirmRender.isVisible;
    if (!isConfirmBox) {
        if (command) {
            frmIBDebitCardActivateAssignPIN.hbxKeyPad.margin = [0, 5, 0, 0];
        } else {
            frmIBDebitCardActivateAssignPIN.hbxKeyPad.margin = [0, 28.7, 0, 0];
        }
    }
}
/*
************************************************************************
      Function Name : nextToConfirmPin
            Purpose : hadle next button
************************************************************************
*/
function nextToConfirmPin() {
    glbPinArr[1] = glbPin;
    if (glbPinArr[0] == glbPinArr[1]) {
        /*alert("PIN Match. Go to complete page");
        call function here.*/
        verifyDebitCardPin();
    } else {
        alert(kony.i18n.getLocalizedString("keyAssignPinNotMatch"));
        clearATMPIN();
    }
}
/*
************************************************************************
      Function Name : verifyDebitCardPin
            Purpose : verify data return from service
            		  (implement on integration back-end service phrase)
************************************************************************
*/
function verifyDebitCardPin() {
    encryptPinUsingE2EE(decryptPinDigit(glbPin));
    //if(gblAssignPinSuccess){
    //		frmIBDebitCardActivateComplete.hbxSuccess.setVisibility(true);
    //		frmIBDebitCardActivateComplete.lblTrxStatusSuccess.setVisibility(true);
    //		frmIBDebitCardActivateComplete.lblTrxStatusSuccess.text = kony.i18n.getLocalizedString("keyAssignPinCompleted").replace("[cardno]",maskCardNo);
    //	}else{
    //		frmIBDebitCardActivateComplete.hbxFail.setVisibility(true);
    //		frmIBDebitCardActivateComplete.lblTrxStatusFail.setVisibility(true);
    //	}
    //	frmIBCardActivation.hbxEnterYourPIN.setVisibility(false);
    //	frmIBCardActivation.hbxPinRender.setVisibility(false);
    //	frmIBCardActivation.hbxConfirmPIN.setVisibility(false);
    //	frmIBCardActivation.hbxDebitCardInfo.setVisibility(false);
    //	frmIBCardActivation.hbxPinConfirmRender.setVisibility(false);
    //	frmIBCardActivation.hbxKeyPad.setVisibility(false);
    //	frmIBCardActivation.hbxBtnNext.setVisibility(false);
    //	frmIBCardActivation.hbxReturn.setVisibility(true);
    //	frmIBCardActivation.hbxReturn.margin = [0,57,0,1];
    //	frmIBCardActivation.hbxCardActivateNote.setVisibility(false);
    //	
    //	if(iFlag){
    //		localeIndex = 4;
    //		frmIBCardActivation.hbxSuccess.setVisibility(true);
    //		frmIBCardActivation.hbxTrxStatus.setVisibility(true);
    //		frmIBCardActivation.lblTrxStatus.text = kony.i18n.getLocalizedString("keyAssignPinCompleted").replace("[cardno]",maskCardNo);
    //		frmIBCardActivation.lblATMPinHeader.text = kony.i18n.getLocalizedString("Completed");
    //		
    //	}else{
    //		localeIndex = 5;
    //		frmIBCardActivation.hbxFail.setVisibility(true);
    //		frmIBCardActivation.hbxTrxStatus.setVisibility(true);
    //		frmIBCardActivation.lblTrxStatus.text = kony.i18n.getLocalizedString("keyAssignPinFailed");
    //		frmIBCardActivation.lblATMPinHeader.text = kony.i18n.getLocalizedString("keyCalendarFailed");
    //	}
}
/*
************************************************************************
      Function Name : initialParam
            Purpose : Generate digit Weight, random Rang
************************************************************************
*/
function initialParam() {
    var randomRangMin = 13;
    var randomRangMax = 24;
    var digitWeightMin = 1;
    var digitWeightMax = 9;
    for (var i = 0; i < 4; i++) {
        glbRandomRang[i] = randomDigit(randomRangMin, randomRangMax);
        glbDigitWeight[i] = randomDigit(digitWeightMin, digitWeightMax);
    }
}
/*
************************************************************************
      Function Name : randomDigit
            Purpose : randomDigit
************************************************************************
*/
function randomDigit(max, min) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
/*
************************************************************************
      Function Name : DecryptPinDigit
            Purpose : Decrypt Pin Digit
************************************************************************
*/
function decryptPinDigit(glbPin) {
    var result = "";
    for (var i = 0; i < maxLenPin; i++) {
        var pinVal = glbPin.substring(i, i + 1);
        pinVal = parseInt(pinVal.charCodeAt(0));
        pinVal -= parseInt(glbRandomRang[i]);
        pinVal -= parseInt(glbDigitWeight[i]);
        result += String.fromCharCode(pinVal);
    }
    return result;
}
/*
************************************************************************
      Function Name : preOTPScreenForDebitCardActivate
            Purpose : hadle event button
************************************************************************
*/
function preOTPScreenForDebitCardActivate() {
    preOTPScreeneIB("debitCardActivate");
}
/*
************************************************************************
      Function Name : confirmOTPForDebitCardActivate
            Purpose : 
************************************************************************
*/
function confirmOTPForDebitCardActivate() {
    if (VerifyOTPAndTokenInputFormatForIB()) {
        var inputParam = arrangeInputParamOTPAndTokenForIB();
        callVerifyOTPAndTokenServiceForAssignPIN(inputParam);
    }
}

function callVerifyOTPAndTokenServiceForAssignPIN(inputParam) {
    showLoadingScreenPopup();
    invokeServiceSecureAsync("atmPINverifyOTPComposite", inputParam, callBackVerifyOTPAndTokenServiceForAssignPIN);
}

function callBackVerifyOTPAndTokenServiceForAssignPIN(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreenPopup();
        if (resulttable["opstatus"] == 0) {
            initialParam();
            renderPIN();
            toggleConfirmPIN(false);
            frmIBDebitCardActivateAssignPIN.show();
        } else {
            handleExceptionOTPAndTokenForIB(resulttable);
        }
    }
}

function frmIBDebitCardActivateLandingPreshow() {
    initOnlineATMPIN();
    resetIBDebitCardActivate();
}
/*
************************************************************************
      Function Name : resetFrmIBCardActivate
            Purpose : reset form to first stage
************************************************************************
*/
function resetIBDebitCardActivate() {
    glbRandomRang = [];
    glbDigitWeight = [];
    maxLenPin = 4;
    localeIndex = 0;
    clearATMPIN();
}
/*
************************************************************************
      Function Name : toggleButton
            Purpose : enable/disable button next
************************************************************************
*/
function toggleButton(command) {
    if (command) {
        frmIBDebitCardActivateAssignPIN.btnNextPin.skin = "btnIB158";
        frmIBDebitCardActivateAssignPIN.btnNextPin.hoverSkin = "btnIB158active";
        frmIBDebitCardActivateAssignPIN.btnNextPin.focusSkin = "btnIB158active";
    } else {
        frmIBDebitCardActivateAssignPIN.btnNextPin.skin = "btnIB158disabled";
        frmIBDebitCardActivateAssignPIN.btnNextPin.hoverSkin = "btnIB158disabled";
        frmIBDebitCardActivateAssignPIN.btnNextPin.focusSkin = "btnIB158disabled";
    }
    frmIBDebitCardActivateAssignPIN.btnNextPin.setEnabled(command);
}
/*
************************************************************************
      Function Name : syncIBCardActivate
            Purpose : set locale switching language
************************************************************************
*/
function syncIBCardActivate() {
    var currForm = kony.application.getCurrentForm();
    if ("frmIBDebitCardActivateLanding" == currForm.id) {
        setAccountNickName();
    }
    if ("frmIBDebitCardActivateComplete" == currForm.id) {
        if (frmIBDebitCardActivateComplete.hbxSuccess.isVisible) {
            currForm.lblTrxStatusSuccess.text = kony.i18n.getLocalizedString("keyAssignPinCompleted").replace("[cardno]", maskCardNo);
            frmIBDebitCardActivateComplete.lblATMPinHeader.text = kony.i18n.getLocalizedString("Complete");
        } else {
            frmIBDebitCardActivateComplete.lblATMPinHeader.text = kony.i18n.getLocalizedString("keyCalendarFailed");
        }
    }
    if ((undefined !== currForm.btnMenuMyAccountSummary) && (currForm.id == "frmIBDebitCardActivateLanding" || currForm.id == "frmIBDebitCardActivateAssignPIN" || currForm.id == "frmIBDebitCardActivateComplete")) {
        if (kony.i18n.getCurrentLocale() == "th_TH") currForm.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
        else currForm.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
    }
}
/*
************************************************************************
      Function Name : IBCardActivateDepositAccountInquiryService
            Purpose : call service depositAccountInquiry
************************************************************************
*/
function IBCardActivateDepositAccountInquiryService() {
    showLoadingScreenPopup();
    //resetFrmIBCardActivate();
    var inputparam = {};
    inputparam["activateCard"] = "yes";
    inputparam["selectedCard"] = frmIBAccntSummary.segDebitCard.selectedIndex[1];
    inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, IBCardActivateDepositAccountInquiryCallBack);
}

function IBCardActivateDepositAccountInquiryCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var availableBal;
            var ledgerBal;
            if (resulttable["AcctBal"].length > 0) {
                for (var i = 0; i < resulttable["AcctBal"].length; i++) {
                    if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
                        availableBal = resulttable["AcctBal"][i]["Amt"];
                    }
                    if (resulttable["AcctBal"][i]["BalType"] == "Ledger") {
                        ledgerBal = resulttable["AcctBal"][i]["Amt"];
                    }
                }
            }
            if (null != availableBal) {
                frmIBDebitCardActivateLanding.lblBalanceVal.text = commaFormatted(availableBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            }
            if (resulttable["accountTitle"] != null && resulttable["accountTitle"] != "") {
                frmIBDebitCardActivateLanding.lblAccountName.text = resulttable["accountTitle"];
            }
            frmIBDebitCardActivateLanding.lblAccountNumber.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
            var lastFourDigitAccntNo = gblAccountTable["custAcctRec"][gblIndex]["accId"];
            var length = lastFourDigitAccntNo.length;
            lastFourDigitAccntNo = lastFourDigitAccntNo.substring(length - 4, length);
            accNickNameDefualtEng = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"] + " " + lastFourDigitAccntNo;
            accNickNameDefualtThai = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"] + " " + lastFourDigitAccntNo;
            if (kony.i18n.getCurrentLocale() == "en_US") {
                accNickName = accNickNameDefualtEng;
            } else {
                accNickName = accNickNameDefualtThai;
            }
            accountNickName = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
            if (null != accountNickName) {
                frmIBDebitCardActivateLanding.lblAccountNicknameVal.text = accountNickName;
            } else {
                frmIBDebitCardActivateLanding.lblAccountNicknameVal.text = accNickName;
            }
            var selectedIndex = frmIBAccntSummary.segDebitCard.selectedIndex[1];
            var debitCardNo = GLOBAL_DEBITCARD_TABLE[selectedIndex]["cardNo"];
            maskCardNo = frmIBAccntSummary.segDebitCard.selectedItems[0].lblCreditCardNo;
            frmIBDebitCardActivateLanding.lblMaskCardNo.text = maskCardNo;
            frmIBDebitCardActivateAssignPIN.lblMaskCardNo.text = maskCardNo;
            frmIBDebitCardActivateLanding.lblExpiryDate.text = frmIBAccntSummary.segDebitCard.selectedItems[0].lblExpiryDateV;
            var imgAccnt = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"] + "&modIdentifier=PRODICON";
            var imgCard = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=undefined" + "&modIdentifier=AccountDetails";
            frmIBDebitCardActivateLanding.imgAccountDetailsPic.src = imgAccnt;
            frmIBDebitCardActivateLanding.imgDebitCard.src = imgCard;
            frmIBDebitCardActivateAssignPIN.imgDebitCard.src = imgCard;
            frmIBDebitCardActivateLanding.show();
        } else {
            alert(" " + resulttable["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}

function setAccountNickName() {
    if (kony.i18n.getCurrentLocale() == "en_US") {
        accNickName = accNickNameDefualtEng;
    } else {
        accNickName = accNickNameDefualtThai;
    }
    if (null != accountNickName) {
        frmIBDebitCardActivateLanding.lblAccountNicknameVal.text = accountNickName;
    } else {
        frmIBDebitCardActivateLanding.lblAccountNicknameVal.text = accNickName;
    }
}

function onClickCancelDebitcardActivateIB() {
    frmIBAccntSummary.show();
    frmIBAccntSummary.segAccountDetails.selectedIndex = [0, gblIndex];
    showIBAccountDetails();
    OTPcheckOnAccountSummary();
}

function checkCustStatuseDebitCardActivate() {
    if (getCRMLockStatus()) {
        curr_form = kony.application.getCurrentForm().id;
        dismissLoadingScreenPopup();
        popIBBPOTPLocked.show();
    } else {
        IBCardActivateDepositAccountInquiryService();
    }
}