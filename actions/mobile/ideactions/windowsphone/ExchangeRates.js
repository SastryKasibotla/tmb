function onClickExchangeRates() {
    if (kony.i18n.getCurrentLocale() == "en_US") {
        kony.application.openURL(GLOBAL_Exchange_Rates_Link_EN)
    } else {
        kony.application.openURL(GLOBAL_Exchange_Rates_Link_TH)
    }
    //Commented for ENH_209
    /*try{
    	
    	showLoadingScreen();
    	inputParams = {};
    	invokeServiceSecureAsync("GetExchangeRateData", inputParams, callBackExchangeRates)
    }catch(e){	
    	
    }*/
}

function setExchangeRateFlag(flagCode) {
    try {
        //
        flagCode = kony.string.trim(flagCode);
        var flagUrl = "";
        flagUrl = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/ImageRender?crmId=&personalizedId=&billerId=" + flagCode;
        //flagUrl = "http://10.11.12.212:9080/tmb/ImageRender?crmId=&personalizedId=&billerId="+flagCode
        return flagUrl;
    } catch (e) {}
}

function frmExchangeRatePreShow() {
    changeStatusBarColor();
    try {
        if (!(LocaleController.isFormUpdatedWithLocale(frmExchangeRate.id))) {
            frmExchangeRate.lblExgHeader.text = kony.i18n.getLocalizedString("ExgRates");
            frmExchangeRate.lblCurrencies.text = kony.i18n.getLocalizedString("ExgRates_Currencies");
            frmExchangeRate.lblTMBSells.text = kony.i18n.getLocalizedString("ExgRates_Sells");
            frmExchangeRate.lblTMBBuys.text = kony.i18n.getLocalizedString("ExgRates_Buy");
            frmExchangeRate.lblUpdated.text = kony.i18n.getLocalizedString("ExgRates_Update");
            LocaleController.updatedForm(frmExchangeRate.id);
        }
    } catch (e) {}
}