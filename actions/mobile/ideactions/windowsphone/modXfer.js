function setDateOnly() {
    var todaysDate = new Date(GLOBAL_TODAY_DATE);
    var year = todaysDate.getFullYear();
    var date = todaysDate.getDate();
    if ((date.toString().length) == 1) {
        date = "0" + date;
    }
    var month = todaysDate.getMonth() + 1;
    if ((month.toString().length) == 1) {
        month = "0" + month;
    }
    var hourTime = todaysDate.getHours();
    var minuteTime = todaysDate.getMinutes();
    var timeMode = "";
    if ((minuteTime.toString().length) == 1) {
        minuteTime = "0" + minuteTime;
    }
    if (hourTime >= "12") {
        hourTime -= "12";
        if ((hourTime.toString().length) == 1) {
            hourTime = "0" + hourTime;
        }
        timeMode = "PM";
    } else {
        timeMode = "AM";
    }
    var TransferDate = date + "/" + month + "/" + year;
    var requiredDate = date + "/" + month + "/" + year + " " + "[" + hourTime + ":" + minuteTime + timeMode + "]";
    // frmIBTransferCustomWidgetLP.lblXferTransferRange.text = kony.i18n.getLocalizedString("keyToday");
    // frmIBTranferLP.lblXferTransferRange.text = kony.i18n.getLocalizedString("keyToday");
    frmIBTransferCustomWidgetLP.lblXferTransferRange.text = TransferDate;
    if (gblPaynow) {
        frmIBTranferLP.lblXferTransferRange.text = TransferDate
    }
    frmIBTransferNowConfirmation.lblSplitXferDtVal.text = requiredDate;
    frmIBTransferNowConfirmation.lblTransferVal.text = requiredDate;
    frmIBTransferNowCompletion.lblTransferVal.text = requiredDate;
    frmIBTransferNowCompletion.lblSplitXferDtVal.text = requiredDate;
    var currentDate = TransferDate;
    currentDate = currentDate.toString();
    var dateTD = currentDate.substr(0, 2);
    var yearTD = currentDate.substr(6, 4);
    var monthTD = currentDate.substr(3, 2);
    gblTransferDate = yearTD + "-" + monthTD + "-" + dateTD;
}

function futureCalDateXfersIB() {
    //var currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var currentDate = new Date(new Date().getTime()); //MIB-377 - Allow today's date also as valid start date
    var day = currentDate.getDate()
    var month = currentDate.getMonth() + 1
    var year = currentDate.getFullYear()
        //frmIBTranferLP.CalendarXferDate.enableRangeOfDates(startDate, "", "", enable)
    frmIBTranferLP.CalendarXferDate.validStartDate = [day, month, year];
    frmIBTranferLP.calendarXferUntil.validStartDate = [day, month, year];
}

function setDate() {
    var todaysDate = new Date();
    var year = todaysDate.getFullYear();
    var lasttwodigits = year % 100;
    if (lasttwodigits == "00") {
        lasttwodigits = year;
    }
    var hourTime = todaysDate.getHours();
    var timeMode = "";
    if (hourTime >= "12") {
        timeMode = "PM";
    } else {
        timeMode = "AM";
    }
    var requiredDate = todaysDate.getDate() + "/" + (todaysDate.getMonth() + 1) + "/" + lasttwodigits + " " + "[" + todaysDate.getHours() + ":" + todaysDate.getMinutes() + timeMode + "]";
    //frmIBTranferLP.lblXferTransferRange.text = requiredDate; 
}

function toggleArrowonClickFalse() {
    //frmIBTranferLP.arrowXferToField.setVisibility(false);
    //frmIBTranferLP.arrowXferScheduleField.setVisibility(false);
}

function showToField() {
    //frmIBTranferLP.lblXferTransferRange.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    frmIBTranferLP.hbxwaterWheel.setVisibility(false);
    frmIBTranferLP.hbxXferTo.setVisibility(true);
    frmIBTranferLP.hbxXferSegment.setVisibility(true);
    frmIBTranferLP.hbxScheduledTransfer.setVisibility(false);
    frmIBTranferLP.scrollbox21149857403622.setVisibility(true);
}
//function showFromField() {
//	frmIBTranferLP.hbxwaterWheel.setVisibility(true);
//	frmIBTranferLP.hbxXferTo.setVisibility(false);
//	frmIBTranferLP.hbxXferSegment.setVisibility(false);
//	frmIBTranferLP.hbxScheduledTransfer.setVisibility(false);
//}
function showScheduleTransferField() {
    frmIBTranferLP.hbxwaterWheel.setVisibility(false);
    frmIBTranferLP.hbxXferTo.setVisibility(false);
    frmIBTranferLP.hbxXferSegment.setVisibility(false);
    frmIBTranferLP.hbxScheduledTransfer.setVisibility(true);
}

function onSaveClick() {
    if (DailyClicked) {
        if (frmIBTranferLP.btnDaily.skin == "btnIBTab4LeftNrml") {
            DailyClicked = false;
        }
    }
    if (weekClicked) {
        if (frmIBTranferLP.btnWeekly.skin == "btnIBTab4MidNrml") {
            weekClicked = false;
        }
    }
    if (monthClicked) {
        if (frmIBTranferLP.btnMonthly.skin == "btnIBTab4MidNrml") {
            monthClicked = false;
        }
    }
    if (YearlyClicked) {
        if (frmIBTranferLP.btnYearly.skin == "btnIbTab4RightNrml") {
            YearlyClicked = false;
        }
    }
    //frmIBTranferLP.lblXferTransferRange.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    var timesOrDate = "0";
    var firstDate = frmIBTranferLP.CalendarXferDate.formattedDate;
    var today = new Date();
    var times;
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    if (firstDate == "" || firstDate == null) {
        alert(kony.i18n.getLocalizedString("KeySelStartDate"));
        return;
    } else if ((parseDate(firstDate) - parseDate(today)) == 0) {
        gblPaynow = true;
        closeRightPanelTransfer();
        if (gblisTMB != gblTMBBankCD) {
            getTransferFeeIB();
        }
        frmIBTranferLP.lblXferTransferRange.text = firstDate;
        frmIBTranferLP.lblXferInterval.text = "";
        return;
    } else if ((parseDate(firstDate) - parseDate(today)) < 0) {
        alert(kony.i18n.getLocalizedString("KeySelVldStartDate"));
        return;
    } else {
        var secondDate;
        if (frmIBTranferLP.hbxEndCalendar.isVisible) {
            timesOrDate = "1";
            secondDate = frmIBTranferLP.calendarXferUntil.formattedDate;
            if (secondDate == "" || secondDate == null) {
                alert(kony.i18n.getLocalizedString("Valid_SelectEndDate"));
                return;
            }
            if (parseDate(firstDate) - parseDate(secondDate) >= 0) {
                alert(kony.i18n.getLocalizedString("keySelectValidEndDateFT"));
                return;
            }
            times = numberOfDaysTransferIB(firstDate, secondDate);
            if (times > 99) {
                alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
                return;
            }
        } else if (frmIBTranferLP.hbxTimes.isVisible) {
            timesOrDate = "0";
            times = frmIBTranferLP.txtTimes.text;
            if (isNaN(times) || times == null || times == "" || times <= 0) {
                alert(kony.i18n.getLocalizedString("Valid_EntervalidRecValue"));
                return false;
            }
            if (times > 99) {
                alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
                return;
            }
            frmIBTranferLP.lblXferTransferRange.text = firstDate + " " + kony.i18n.getLocalizedString("keyTo") + " " + firstDate;
            var startDate = parseDate(firstDate)
            secondDate = endDateCalculatorTransferIB(startDate, times)
        } else {
            gblPaynow = false;
            frmIBTranferLP.lblXferTransferRange.text = firstDate + " " + kony.i18n.getLocalizedString("keyTo") + " " + firstDate;
            frmIBTranferLP.lblXferInterval.text = "";
            //frmIBTranferLP.lblXferInterval.setVisibility(false)
            if (monthClicked == true) {
                frmIBTranferLP.lblXferInterval.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly");
            } else if (weekClicked == true) {
                frmIBTranferLP.lblXferInterval.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly");
            } else if (DailyClicked == true) {
                frmIBTranferLP.lblXferInterval.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily");
            } else if (YearlyClicked == true) {
                frmIBTranferLP.lblXferInterval.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly");
            } else {
                frmIBTranferLP.lblXferInterval.text = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
            }
            frmIBTranferLP.lblXferInterval.setVisibility(true);
            gblPaynow = false;
            if (gblisTMB != gblTMBBankCD) {
                GBL_SMART_DATE_FT = frmIBTranferLP.CalendarXferDate.formattedDate;
                getTransferFeeIB();
            }
            closeRightPanelTransfer();
            return;
        }
        var startDate = parseDate(firstDate);
        var endDate = parseDate(secondDate);
        var dateDifference = endDate - startDate;
        dateDifference = dateDifference / 86400000;
        dateDifference = dateDifference + 1;
        var week = parseInt(dateDifference / 7);
        var month = parseInt(dateDifference / 30);
        var year = parseInt(dateDifference / 365);
        if (timesOrDate == "1") {
            week = parseInt(dateDifference / 7) + 1;
            month = parseInt(dateDifference / 30) + 1;
            year = 1 + AvgYears(startDate, endDate, dateDifference);
        } else {
            week = times;
            month = times;
            year = times;
        }
        if (dateDifference < 0) {
            alert(kony.i18n.getLocalizedString("keySelectValidEndDateFT"));
            return;
        }
        if (dateDifference == 0 && times == '') {
            labelTransfertop = firstDate;
        } else {
            labelTransfertop = firstDate + " " + kony.i18n.getLocalizedString("keyTo") + " " + secondDate;
        }
        var labelTransferBottomWeek = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        var labelTransferBottomMonth = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        var labelTransferBottomYear = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        var labelTransferBottomDaily = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily") + " " + times + " " + kony.i18n.getLocalizedString("keyTimesMB");
        frmIBTranferLP.lblXferTransferRange.text = labelTransfertop;
        gblPaynow = false;
        if (monthClicked == true) {
            frmIBTranferLP.lblXferInterval.text = labelTransferBottomMonth;
        } else if (weekClicked == true) {
            frmIBTranferLP.lblXferInterval.text = labelTransferBottomWeek;
        } else if (DailyClicked == true) {
            frmIBTranferLP.lblXferInterval.text = labelTransferBottomDaily;
        } else if (YearlyClicked == true) {
            frmIBTranferLP.lblXferInterval.text = labelTransferBottomYear;
        }
        frmIBTranferLP.lblXferInterval.setVisibility(true);
    }
    closeRightPanelTransfer();
    if (gblisTMB != gblTMBBankCD) {
        GBL_SMART_DATE_FT = frmIBTranferLP.CalendarXferDate.formattedDate;
        getTransferFeeIB();
    }
}

function numberOfDaysTransferIB(date1, date2) {
    var startDate = parseDate(date1);
    var endDate = parseDate(date2);
    if (DailyClicked) {
        gblNumberOfDays = daydiff(startDate, endDate);
    } else if (weekClicked) {
        gblNumberOfDays = weekdiff(startDate, endDate);
    } else if (monthClicked) {
        gblNumberOfDays = monthdiff(startDate, endDate);
    } else if (YearlyClicked) {
        gblNumberOfDays = yeardiff(startDate, endDate);
    }
    return gblNumberOfDays;
}