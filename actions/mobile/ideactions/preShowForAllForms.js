/*
function changeLocale(eventObj) {
	var locale = kony.i18n.getCurrentLocale();
	var curformID = kony.application.getCurrentForm();
	
	if (kony.string.startsWith(locale, "en", true) == true) {
		if (eventObj["id"] == "btnThai") {
			//this snippet is added to change the button skin to highlighted skin
			curformID.btnEng.skin = btnOnNormal
			curformID.btnThai.skin = btnOffNorm
			//end of snippet	 
			
			kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
		}
	}
	else {
		if (eventObj["id"] == "btnEng") {
			//this snippet is added to change the button skin to highlighted skin
			curformID.btnEng.skin = btnOnFocus
			curformID.btnThai.skin = btnOffFocus
			//end of snippet	 
			
			kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChange, onFailureLocaleChange, "");
		}
	}
}

function onSuccessLocaleChange() {
	gblLocale = true;
	var currentFormId = kony.application.getCurrentForm();
	menuLocaleChange(currentFormId);
	kony.application.getCurrentForm()
		.preShow();;
}

function onFailureLocaleChange() {
	alert("Unable to change onFailureLocaleChange");
}
*/
function preLoginMenuLocaleChange()
{
	changeStatusBarColor();
	if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
				segMenuTable = [{
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions")
						},{
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices")
						}
				]
			}else{
				segMenuTable = [{
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB")
						}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates")
					}, {
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions")
					},{
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices")
					},{
						lblMenuItem: ""
					}
				]
			}
		frmPreMenu.lblmenuLogin.text = kony.i18n.getLocalizedString("login");
		frmPreMenu.btnThai.text=kony.i18n.getLocalizedString("languageThai");
		frmPreMenu.btnEng.text=kony.i18n.getLocalizedString("languageEng");
		var locale = kony.i18n.getCurrentLocale();
		if (locale == "en_US")
	    {
			frmPreMenu.btnEng.skin = btnEngMenu;
			frmPreMenu.btnEng.zIndex = 2;
			frmPreMenu.btnThai.skin = btnThaiMenu;
			frmPreMenu.btnThai.zIndex = 1;
		}
		else
		{
			frmPreMenu.btnEng.skin = btnThaiMenu;
			frmPreMenu.btnEng.zIndex=1;
			frmPreMenu.btnThai.skin = btnEngMenu;
			frmPreMenu.btnThai.zIndex=2;
		}
		frmPreMenu.btnEng.padding=[0,0,0,0];
		frmPreMenu.btnThai.padding=[0,0,0,0];	
		frmPreMenu.lblversion.text=kony.i18n.getLocalizedString("keyVersion")+" "+ appConfig.appVersion;
		frmPreMenu.lblCopyright.text=getCopyRightText();
		frmPreMenu.segPreLogin.removeAll();
		frmPreMenu.segPreLogin.setData(segMenuTable);
}
function reMenuLocaleChange() {
	changeStatusBarColor();
	isSignedUser = true;
	frmMenu.btnMenuTransfer.text = kony.i18n.getLocalizedString("keymenuTransfer");
    frmMenu.btnMenuBillPay.text = kony.i18n.getLocalizedString("MenuBillPayment");
    frmMenu.btnMenuTopUp.text = kony.i18n.getLocalizedString("TopUp");
    frmMenu.btnMenuAccntSumry.text = kony.i18n.getLocalizedString("accountSummary");
    frmMenu.btnMenuActivities.text = kony.i18n.getLocalizedString("MyActivities");
    frmMenu.btnMenuOffers.text = kony.i18n.getLocalizedString("MenuHotPromotions");
    frmMenu.lblinboxmenu.text = kony.i18n.getLocalizedString("MenuMyInbox");
    frmMenu.btnMenuSettings.text = kony.i18n.getLocalizedString("MenuAboutMe");
    frmMenu.btnMenuMore.text = kony.i18n.getLocalizedString("More");//"More";
    frmMenu.btnThai.text = kony.i18n.getLocalizedString("languageThai");
    frmMenu.btnEng.text = kony.i18n.getLocalizedString("languageEng");
    frmMenu.lbllogout.text = kony.i18n.getLocalizedString("keySPAmenuLogout");
    frmMenu.lbllogoutsmall.text = kony.i18n.getLocalizedString("keySPAmenuLogout");
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US")
	{
		if(frmMenu.flexNormal.isVisible)
		{
			frmMenu.lbllastlogintimestamp.text= kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin")+" "+gbllastMbLoginSucessDateEng;
		}
		else
		{
			frmMenu.lbllastlogintimestampsmall.text=kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin")
			frmMenu.lbllastlogintimestampval.text=gbllastMbLoginSucessDateEng
		}
		frmMenu.lbllastlogintimestamp.text= kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin")+" "+ gbllastMbLoginSucessDateEng;	
		if(isNotBlank(gblCustomerName))
		{
			frmMenu.lblfirstName.text = kony.i18n.getLocalizedString("khun");
            frmMenu.lblLastname.text = gblCustomerName.split(" ")[0];
        }
	}
	else
	{
		if(frmMenu.flexNormal.isVisible)
		{
			frmMenu.lbllastlogintimestamp.text= kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin")+" "+gbllastMbLoginSucessDateThai;
		}
		else
		{
			frmMenu.lbllastlogintimestampsmall.text=kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin");
			frmMenu.lbllastlogintimestampval.text=gbllastMbLoginSucessDateThai
		}
		frmMenu.lbllastlogintimestamp.text= kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin")+" "+ gbllastMbLoginSucessDateThai;
		if(isNotBlank(gblCustomerNameTh))
		{
			frmMenu.lblfirstName.text = kony.i18n.getLocalizedString("khun");
            frmMenu.lblLastname.text = gblCustomerNameTh.split(" ")[0];
        }
	}
	frmMenu.btnEng.padding=[0,0,0,0];
	frmMenu.btnThai.padding=[0,0,0,0];	
	frmMenu.lblversion.text=kony.i18n.getLocalizedString("keyVersion")+" "+ appConfig.appVersion;
	frmMenu.lblcopyright.text=getCopyRightText();
    //setting the badge value to the button **** Total Count of Unread and Messages count from unreadInboxMessagesTrackerLocalDBSyncCB
    frmMenu.btnbadge.skin = btnBadgeSmall;
    frmMenu.btnbadge.setVisibility(false);
    if (gblMyInboxTotalCountMB > 0) {
        frmMenu.btnbadge.text = gblMyInboxTotalCountMB.toString();
        frmMenu.btnbadge.setVisibility(true);
    } else {
        frmMenu.btnbadge.setVisibility(false);
    }

    //Adding the segment Data
    // Onclick of Settings
    if (gblTouchDevice == true) {
    
    	//#ifdef iphone
				touchIdMenu= kony.i18n.getLocalizedString("keyTouchIDMenu");
		//#else
				//#ifdef android
					kony.print("touchIdMenu="+kony.i18n.getLocalizedString("MB_AndFinPrintMenu"));
					touchIdMenu= kony.i18n.getLocalizedString("MB_AndFinPrintMenu"); //MB_AndFinPrintMenu
				//#endif
		//#endif 

        var segSettingsTable = [{
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyProfile")

        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMyAccount")

        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("menuCardManagement")

        },
        {
        	lblSettingsItem: kony.i18n.getLocalizedString("MIB_AnyIDMenu")
    	
    	},
    	{
        	lblSettingsItem: kony.i18n.getLocalizedString("keyEStmt")
    	
    	},
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyRecipients")

        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMybills")

        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("myTopUpsMB")

        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("SQB_Title")

        },
        {
            lblSettingsItem: touchIdMenu

        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice")

        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("UnlockIBanking")

        }]
    } else {
        var segSettingsTable = [{
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyProfile")
        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMyAccount")
        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("menuCardManagement")
		},
        {
        	lblSettingsItem: kony.i18n.getLocalizedString("MIB_AnyIDMenu")
    	},
    	{
        	lblSettingsItem: kony.i18n.getLocalizedString("keyEStmt")
    	},
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyRecipients")
        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMybills")
        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("myTopUpsMB")
        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("SQB_Title")
        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice")
        },
        {
            lblSettingsItem: kony.i18n.getLocalizedString("UnlockIBanking")
        }]
    }

    //Adding code to More Segment
    var segMoreTable = [{
        lblmoreitems: kony.i18n.getLocalizedString("kelblOpenNewAccount")
    },
    {
        lblmoreitems: kony.i18n.getLocalizedString("InternetBanking")
    },
    {
        lblmoreitems: kony.i18n.getLocalizedString("MenuFindTMB")
    },
    {
        lblmoreitems: kony.i18n.getLocalizedString("MenuExRates")
    },
    {
        lblmoreitems: kony.i18n.getLocalizedString("MenuTour")
    },
    {
        lblmoreitems: kony.i18n.getLocalizedString("keyContactUs")
    },
    {
        lblmoreitems: kony.i18n.getLocalizedString("keyTermsNConditions")
    },
    {
        lblmoreitems: kony.i18n.getLocalizedString("keySecurityAdvices")
    }]


    //Add Items for Notifications Segment
    //gblUnreadCount=0;
    //gblMessageCount=0;
    gblNotificationData = [];
    gblMessageData = [];
    //changes done for MyInbox badge requirement
    if (parseInt(gblUnreadCount) > 0) {
        gblNotificationData = {
            "text": gblUnreadCount,
            "isVisible": true,
            "skin": "btnBadgeSmall"
        };
    } else {
        gblNotificationData = {
            "text": gblUnreadCount,
            "isVisible": false,
            "skin": "btnBadgeSmall"
        };
    }
    if (parseInt(gblMessageCount) > 0) {
        gblMessageData = {
            "text": gblMessageCount,
            "isVisible": true,
            "skin": "btnBadgeSmall"
        };
    } else {
        gblMessageData = {
            "text": gblMessageCount,
            "isVisible": false,
            "skin": "btnBadgeSmall"
        };
    }

    var SegMyinboxTable = [{
        lblNotifications: kony.i18n.getLocalizedString("keylblNotification"),
        btnBadge: gblNotificationData
    },
    {
        lblNotifications: kony.i18n.getLocalizedString("keylblmessage"),
        btnBadge: gblMessageData
    }]
	chngLocalDynamicContainers();
	frmMenu.segInbox.removeAll();
	frmMenu.segMore.removeAll();
	frmMenu.segSettings.removeAll();
    frmMenu.segInbox.setData(SegMyinboxTable);
    frmMenu.segMore.setData(segMoreTable);
    
    if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06') {
     	frmMenu.segMore.removeAt(1);
    }
    
    /*
    if (!flowSpa) {
        if (gblShowAnyIDRegistration == "false") {
            frmMenu.segMore.removeAt(0);
        }
        if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06') {
            if (gblShowAnyIDRegistration == "true") {
                frmMenu.segMore.removeAt(2);
            } else {
                frmMenu.segMore.removeAt(1);
            }
        }
        if (!isShowEStatementMenu()) {
            if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06') {
                if (gblShowAnyIDRegistration == "true") {
                    frmMenu.segMore.removeAt(2);
                } else {
                    frmMenu.segMore.removeAt(1);
                }
            } else {
                if (gblShowAnyIDRegistration == "true") {
                    frmMenu.segMore.removeAt(3);
                } else {
                    frmMenu.segMore.removeAt(2);
                }
            }
        }
    } else {

        //Removed as part of ENH_207_7
    }
    */
    frmMenu.segSettings.setData(segSettingsTable);
    //filter out
	// Filter out Prompt Pay
		//Prompt Pay False removing
	if (gblShowAnyIDRegistration == "false") {
            frmMenu.segMore.removeAt(3);
    }
	// Estatement False removing
    if (!isShowEStatementMenu()) {
	  //Prompt Pay true Remove At 3 else location 2
      if (gblShowAnyIDRegistration == "true") {
                frmMenu.segSettings.removeAt(4);
      } else {
                frmMenu.segSettings.removeAt(3);
      }
    }
    // if Quick Balance is set to False
	if(gblQuickBalanceEnable != "ON")
	{
		//Promptpay is true and Estmnt is true
		if (gblShowAnyIDRegistration == "true" && isShowEStatementMenu())
		{
			frmMenu.segSettings.removeAt(8);
		}
		//Promptpay is false and Estmnt is false
		else if(gblShowAnyIDRegistration == "false" && !isShowEStatementMenu())
		{
			frmMenu.segSettings.removeAt(6);
		}
		//Promptpay or Estmnt if any one of them is false/true
		else
		{
			frmMenu.segSettings.removeAt(7);
		}
		
	}
	// Touch Id is set to false
	if (gblIBFlowStatus == '00' || gblIBFlowStatus == '01' || gblIBFlowStatus == '08' || gblIBFlowStatus == '07') {
		if (gblTouchDevice == true) 
		{
			//Promptpay is true and Estmnt is true and QuickBalance is true
			if (gblShowAnyIDRegistration == "true" && isShowEStatementMenu() && gblQuickBalanceEnable == "ON")
			{
				frmMenu.segSettings.removeAt(9);
			}
			else if(gblShowAnyIDRegistration == "true" && !isShowEStatementMenu() && gblQuickBalanceEnable != "ON")
			{
				frmMenu.segSettings.removeAt(7);
			}
			else if(gblShowAnyIDRegistration == "false" && !isShowEStatementMenu() && gblQuickBalanceEnable == "ON")
			{
				frmMenu.segSettings.removeAt(7);
			}
			else if(gblShowAnyIDRegistration == "false" && isShowEStatementMenu() && gblQuickBalanceEnable != "ON")
			{
				frmMenu.segSettings.removeAt(7);
			}
			//
			else 
			{
				frmMenu.segSettings.removeAt(6);
			}
		}
		else
		{	
			if (gblShowAnyIDRegistration == "true" && isShowEStatementMenu() && gblQuickBalanceEnable == "ON")
			{
				frmMenu.segSettings.removeAt(8);
			}
			else if(gblShowAnyIDRegistration == "true" && !isShowEStatementMenu() && gblQuickBalanceEnable != "ON")
			{
				frmMenu.segSettings.removeAt(6);
			}
			else if(gblShowAnyIDRegistration == "false" && !isShowEStatementMenu() && gblQuickBalanceEnable == "ON")
			{
				frmMenu.segSettings.removeAt(6);
			}
			else if(gblShowAnyIDRegistration == "false" && isShowEStatementMenu() && gblQuickBalanceEnable != "ON")
			{
				frmMenu.segSettings.removeAt(6);
			}
			//
			else 
			{
				frmMenu.segSettings.removeAt(5);
			}
		}
	}
}

function chngLocalDynamicContainers()
{
	frmMenu.flexdynamicContainer.removeAll();
	dynamicPromotion();
}

function menuLocaleChange(currentFormId) {

	if (isSignedUser == false) {
		var segMenuTable;
		if(flowSpa){
			segMenuTable = [{
					imgMenuIcon: "findatm.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "exhrates.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "apptour.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
					imgMenuChevron: "arrow_menu_white.png"
				},
				{
					imgMenuIcon: "icon_phone.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "termsconditions.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "lock.png",
					lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "",
					lblMenuItem: "",
					imgMenuChevron: "empty.png"
				}		
			]
		}else{
			if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
				segMenuTable = [{
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},
					{
						imgMenuIcon: "icon_phone.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					}
				]
			}else{
				segMenuTable = [{
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},
					{
						imgMenuIcon: "icon_phone.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "",
						lblMenuItem: "",
						imgMenuChevron: "empty.png"
					}	
				]
			}
		}
		currentFormId.SegMainMenu.setData(segMenuTable);
		if(currentFormId.btnLoginText != null && currentFormId.btnLoginText != undefined)
			currentFormId.btnLoginText.text = kony.i18n.getLocalizedString("login");
		
		if(currentFormId.lblAppVersion != null && currentFormId.lblAppVersion != undefined)
			currentFormId.lblAppVersion.text = kony.i18n.getLocalizedString("keyVersion")+" "+ appConfig.appVersion;
		
		if(currentFormId.lblCopyright != null && currentFormId.lblCopyright != undefined)
			currentFormId.lblCopyright.text = getCopyRightText();
	} else {
		var segMenuTable;
		var lblItemMenuHotProText  = kony.i18n.getLocalizedString("MenuHotPromotions");
		var lblItemMenuHotPro_other = "";
		var lblItemMenuHotPro_ios = "";
		
		if(gblIsNewOffersExists){	
			lblItemMenuHotPro_ios = "<table style='width:100%'><tr><td>"+lblItemMenuHotProText +" </td>"
				+"<td style='float:right;padding-top:0px;padding-bottom:50px;padding-left:9px;vertical-align:super;'>"
				+"&nbsp;<img src='hot_new_notify.png'  style ='vertical-align:super;'/></td></tr></table>" ;	
			
			lblItemMenuHotPro_other = lblItemMenuHotProText 
				+"&nbsp;<img src='hot_new_notify.png'  style ='vertical-align:super;'/>" ;		
						
		}else {
			lblItemMenuHotPro_ios   = lblItemMenuHotProText;
			lblItemMenuHotPro_other = lblItemMenuHotProText;
		}
		if(flowSpa){
			 segMenuTable = [{
					imgMenuIcon: "hotpromotions.png",
					//lblMenuItem: kony.i18n.getLocalizedString("MenuHotPromotions"),
					lblMenuItem: lblItemMenuHotPro_other,	
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "findatm.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "exhrates.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "apptour.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "icon_phone.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "termsconditions.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "lock.png",
					lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "",
					lblMenuItem: "",
					imgMenuChevron: "empty.png"
				}	
			]
		}else{
			if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {				
				segMenuTable = [{
						imgMenuIcon: "hotpromotions.png",
						//lblMenuItem: kony.i18n.getLocalizedString("MenuHotPromotions"),
						lblMenuItem: lblItemMenuHotPro_ios,
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "icon_phone.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					}
				] 
			}else{
				segMenuTable = [{
						imgMenuIcon: "hotpromotions.png",
						//lblMenuItem: kony.i18n.getLocalizedString("MenuHotPromotions"),
						lblMenuItem: lblItemMenuHotPro_other,
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "icon_phone.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "",
						lblMenuItem: "",
						imgMenuChevron: "empty.png"
					}	
				]
			}
		}
		
		if(flowSpa){
		  	var segAboutMenuTable = [{
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyProfile"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMyAccount"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyRecipients"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMybills"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("myTopUpsMB"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
				            lblAboutMenuItem: kony.i18n.getLocalizedString("ActivateTokenIB"),
				            imgAboutMenuChevron: "arrow_menu_white.png"
				}]
				
		} else if(gblTouchDevice==true){
			var segAboutMenuTable = [{
				lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyProfile"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMyAccount"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyRecipients"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMybills"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("myTopUpsMB"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("SQB_Title"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("keyTouchIDMenu"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("UnlockIBanking"),
				imgAboutMenuChevron: "arrow_menu_white.png"	
			}]
		} else {
			var segAboutMenuTable = [{
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyProfile"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMyAccount"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyRecipients"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMybills"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("myTopUpsMB"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("SQB_Title"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("UnlockIBanking"),
					imgAboutMenuChevron: "arrow_menu_white.png"	
				}]
			}
			var SegMyinboxTable = [{
					lblNotification: kony.i18n.getLocalizedString("keylblNotification"),
					btnBadge: gblNotificationData,
					imgArrow: "arrow_menu_white.png"
				}, {
					lblNotification: kony.i18n.getLocalizedString("keylblmessage"),
					btnBadge: gblMessageData, 
					imgArrow: "arrow_menu_white.png"
				}]
		if(flowSpa){
			var segConvSerTable = [
			{
		        lblAboutMenuItem: kony.i18n.getLocalizedString("kelblOpenNewAccount"),
		        imgAboutMenuChevron: "arrow_menu_white.png"
		    }, 
		    /*{
		        lblAboutMenuItem: kony.i18n.getLocalizedString("keylblbeep&Bill"),
		        imgAboutMenuChevron: "arrow_menu_white.png"
		    }, {
		        lblAboutMenuItem: kony.i18n.getLocalizedString("keylblsendToSave"),
		        imgAboutMenuChevron: "arrow_menu_white.png"
		    },*/ 
		    {
		        lblAboutMenuItem: kony.i18n.getLocalizedString("keyMBankinglblMobileBanking"),
		        imgAboutMenuChevron: "arrow_menu_white.png"
		  
		    }]
		} else {
	     	var segConvSerTable = [{
					lblAboutMenuItem: kony.i18n.getLocalizedString("MIB_AnyIDMenu"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				},{
				lblAboutMenuItem: kony.i18n.getLocalizedString("kelblOpenNewAccount"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, /*{
				lblAboutMenuItem: kony.i18n.getLocalizedString("keylblbeep&Bill"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			}, {
				lblAboutMenuItem: kony.i18n.getLocalizedString("keylblsendToSave"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			},*/ {
				lblAboutMenuItem: kony.i18n.getLocalizedString("InternetBanking"),
				imgAboutMenuChevron: "arrow_menu_white.png"
			
			},{
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyEStmt"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}]
		}
		currentFormId.SegMainMenu.setData(segMenuTable);
		if (gblFlagMenu == "MyInbox") {
			currentFormId.SegMyInbox.setData(SegMyinboxTable);
		} else if (gblFlagMenu == "AboutMenu") {
			currentFormId.SegAboutMenu.setData(segAboutMenuTable);
			if(!flowSpa) {
				if (gblIBFlowStatus == '00' || gblIBFlowStatus == '01' || gblIBFlowStatus == '08' || gblIBFlowStatus == '07'){
					if(gblTouchDevice==true)
						currentFormId.SegAboutMenu.removeAt(8);
					else
						currentFormId.SegAboutMenu.removeAt(7);
				}
				if(gblQuickBalanceEnable != "ON"){
					currentFormId.SegAboutMenu.removeAt(5);
				}
			}else{
							
				if(!gblTokenActivationFlag){
					currentFormId.SegAboutMenu.removeAt(5);
				}
				//Removed as part of ENH_207_7	 
			}
		} else if (gblFlagMenu == "Services") {
			currentFormId.SegAboutMenu.setData(segConvSerTable);
			if(!flowSpa) {
				if(gblShowAnyIDRegistration=="false"){
					currentFormId.SegAboutMenu.removeAt(0);
				}
			 	if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06'){
				   	//currentFormId.SegAboutMenu.removeAt(1);
	
					if(gblShowAnyIDRegistration=="true")
			    	{
			    		currentFormId.SegAboutMenu.removeAt(2);
			    	}else{
			    		currentFormId.SegAboutMenu.removeAt(1);
			    	}	
				}
				if(!isShowEStatementMenu()){
				   	if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06'){
				   		if(gblShowAnyIDRegistration=="true"){
				   			currentFormId.SegAboutMenu.removeAt(2);
				   		}
				   		else{
				   			currentFormId.SegAboutMenu.removeAt(1);
				   		}	
				   	}else{
			    		if(gblShowAnyIDRegistration=="true"){
			    			currentFormId.SegAboutMenu.removeAt(3);
			    		}	
			    		else{
			    			currentFormId.SegAboutMenu.removeAt(2);
			    		}
			    	}		
				}
			}
			else{
					//Removed as part of ENH_207_7
			}
		
		} else{
			  currentFormId.SegAboutMenu.removeAll();
			  currentFormId.SegMyInbox.removeAll();
			  currentFormId.vbxMyinbox.skin = "vbxMenuInvox";
			   currentFormId.vboxAbtMe.skin = "vbxMenuAbtMe";
			   currentFormId.vboxConvService.skin = "vbxConvService";
		}

	    currentFormId.btnLogOutText.text = kony.i18n.getLocalizedString("keySPAmenuLogout");
		currentFormId.lblTransfer.text = kony.i18n.getLocalizedString("Transfer");
		currentFormId.lblMyActivities.text = kony.i18n.getLocalizedString("MyActivities");
		currentFormId.lblMyAccSumm.text = kony.i18n.getLocalizedString("accountSummary");
		currentFormId.lblBillPayment.text = kony.i18n.getLocalizedString("MenuBillPayment");
		currentFormId.lblCardLessWithDraw.text = kony.i18n.getLocalizedString("MenuCardlessWithdrawal");
		currentFormId.lblTopUp.text = kony.i18n.getLocalizedString("TopUp");
		currentFormId.lblAbtMe.text = kony.i18n.getLocalizedString("MenuAboutMe");
		currentFormId.lblMyInbox.text = kony.i18n.getLocalizedString("MenuMyInbox");
		currentFormId.lblConviServices.text = kony.i18n.getLocalizedString("MenuConvenientServices");
}
}


function frmMBankingPreShow() {
	changeStatusBarColor();
	spaCheck()
	if (flowSpa) {
		frmMBankingSpa.lblMobileBanking.text = kony.i18n.getLocalizedString("keySPAlblInternetBanking");
		//frmMBankingSpa.image244538774217455.src = "internetbanking.png"
		//frmMBankingSpa.lblInternetBanking.text = kony.i18n.getLocalizedString("keyMBankinglbleWallet");
	} else {
		//ENH_207_1  --- language toggle code will be placed later
		frmMBanking.lblActivation1.text=kony.i18n.getLocalizedString("keyActivationATMCard");
		frmMBanking.lblActivation2.text=kony.i18n.getLocalizedString("keyActivationUserID");
		frmMBanking.lblActivation3.text=kony.i18n.getLocalizedString("keyActivationCode");
		if(gblSetPwd){
			frmMBanking.lblEasy.text=kony.i18n.getLocalizedString("keyEasyStepsReactivate");
			//frmMBanking.lblActivation3.text=kony.i18n.getLocalizedString("keyReactivationCode");  same text for both flows in 3 options
		}else{
			frmMBanking.lblEasy.text=kony.i18n.getLocalizedString("keyEasyStepsActivate");
			//frmMBanking.lblActivation3.text=kony.i18n.getLocalizedString("keyActivationCode");
		}
		var deviceInfo=kony.os.deviceInfo();
		var locale_app=kony.i18n.getCurrentLocale(); 
		
		if(locale_app == "en_US"){
			frmMBanking.hbxActivationCode.padding=[2,4,0,4];
			frmMBanking.hbxActivationIB.padding=[2,4,0,4];
			frmMBanking.hbxActivationATM.padding=[2,4,0,4];
		}else{
			if (!gblShowUserIdLink) {
				frmMBanking.hbxActivationCode.padding=[2,7,0,7];
				frmMBanking.hbxActivationIB.padding=[2,7,0,7];
				frmMBanking.hbxActivationATM.padding=[2,7,0,7];
			}else{
				frmMBanking.hbxActivationCode.padding=[2,4,0,4];
				frmMBanking.hbxActivationIB.padding=[2,4,0,4];
				frmMBanking.hbxActivationATM.padding=[2,4,0,4];
			}
		}
		
		//#ifdef iphone
			if(deviceInfo["model"].indexOf("6") > -1){
				frmMBanking.imgTMB.src="tmblogowhite.png";
			}
			if(deviceInfo["model"].indexOf("4") > -1){
				if(locale_app == "en_US"){
					frmMBanking.hbxActivationCode.padding=[2,1,0,0];
					frmMBanking.hbxActivationIB.padding=[2,0,0,0];
					frmMBanking.hbxActivationATM.padding=[2,0,0,0];
				}else{
					frmMBanking.hbxActivationCode.padding=[2,3,0,3];
					frmMBanking.hbxActivationIB.padding=[2,3,0,3];
					frmMBanking.hbxActivationATM.padding=[2,3,0,3];
				
				}
			}
		//#endif
	}
	//gblLocale = false;
	if(!gblShowUserIdLink){
		frmMBanking.hbxActivationIB.setVisibility(false);
		if(gblSetPwd){
			frmMBanking.lblEasy.text=kony.i18n.getLocalizedString("keyTwoEasyStepsDeactivate");		
		}else{
			frmMBanking.lblEasy.text=kony.i18n.getLocalizedString("keyTwoEasyStepsActivate");
		}
	}
}

function PreShowChngeMobNuTransLimit() {
	changeStatusBarColor();
	if (gblMobNoTransLimitFlag == true) {
		frmChangeMobNoTransLimitMB.lblHead.text = kony.i18n.getLocalizedString("changeMobNo");
   		frmChangeMobNoTransLimitMB.hbxChangeTransactionLimit.setVisibility(false);
		frmChangeMobNoTransLimitMB.hbxChangeMobileNumber.setVisibility(true);
		frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtFocusBG;
   } else {
		frmChangeMobNoTransLimitMB.lblHead.text = kony.i18n.getLocalizedString("DailyTransLimit");
   		frmChangeMobNoTransLimitMB.hbxChangeMobileNumber.setVisibility(false);
		frmChangeMobNoTransLimitMB.hbxChangeTransactionLimit.setVisibility(true);
		var commaamt1 = ProfileCommFormat(gblEBMaxLimitAmtCurrent);	
		frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtFocusBG;
		frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.placeholder = commaamt1;
		frmChangeMobNoTransLimitMB.lblMaxLimitDisplay.text = getDailyChangeLimtNote(ProfileCommFormat(GLOBAL_MAX_LIMIT_HIST), ProfileCommFormat(GLOBAL_CEILING_LIMIT));
   }
  if(!(LocaleController.isFormUpdatedWithLocale(frmChangeMobNoTransLimitMB.id))){
	//frmChangeMobNoTransLimitMB.destroy();                -----crashing the App-----
	frmChangeMobNoTransLimitMB.button474230331217991.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmChangeMobNoTransLimitMB.button474230331217989.text= kony.i18n.getLocalizedString("keysave");
	if (gblMobNoTransLimitFlag == true) {		
		frmChangeMobNoTransLimitMB.lblHead.text = kony.i18n.getLocalizedString("changeMobNo");
		frmChangeMobNoTransLimitMB.txtChangeMobileNumber.placeholder = kony.i18n.getLocalizedString("keyenternewnumberPH");
		frmChangeMobNoTransLimitMB.lblChangeMobileNumber.text =kony.i18n.getLocalizedString("keyInputNewNumber");
		frmChangeMobNoTransLimitMB.lblMobileStaticTxt.text =kony.i18n.getLocalizedString("MIB_MobChangeWarning");
		frmChangeMobNoTransLimitMB.lblNote.text =kony.i18n.getLocalizedString("MIB_MobChangeWarning_Note");
	} else {
		frmChangeMobNoTransLimitMB.lblHead.text = kony.i18n.getLocalizedString("DailyTransLimit");
		frmChangeMobNoTransLimitMB.lblMaxLimitDisplay.text = getDailyChangeLimtNote(ProfileCommFormat(GLOBAL_MAX_LIMIT_HIST), ProfileCommFormat(GLOBAL_CEILING_LIMIT));
		frmChangeMobNoTransLimitMB.lblChangeTransactionLimit.text=kony.i18n.getLocalizedString("changeLimitInputlbl"); 
	}
    LocaleController.updatedForm(frmChangeMobNoTransLimitMB.id);
   }   
}




function frmeditMyProfilePreShow() 
{changeStatusBarColor();
	if (kony.i18n.getCurrentLocale() == "th_TH")
		{frmeditMyProfile.lblcustname.text = gblCustomerNameTh;}
	else
		{frmeditMyProfile.lblcustname.text = gblCustomerName;}
	frmeditMyProfile.txtemailvalue.placeholder=gblEmailAddr;
   if(!(LocaleController.isFormUpdatedWithLocale(frmeditMyProfile.id))){	
	frmeditMyProfile.lbldevicename.text = kony.i18n.getLocalizedString("keyDeviceNameLabel") + ":";
	frmeditMyProfile.lblemail.text =  	kony.i18n.getLocalizedString("keyEmail");
	if (flowSpa){
	frmeditMyProfile.button104235284554498.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmeditMyProfile.button104235284554500.text = kony.i18n.getLocalizedString("keysave");
	}
		else{
	frmeditMyProfile.button474230331217991.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmeditMyProfile.button474230331217989.text = kony.i18n.getLocalizedString("keysave");
		}
	frmeditMyProfile.button209682697497232.text = kony.i18n.getLocalizedString("keychoosePicture");
	frmeditMyProfile.label475124774164.text = kony.i18n.getLocalizedString("keyIBEditMyProfile");
	frmeditMyProfile.lblfbid.text = kony.i18n.getLocalizedString("keyFacebookID");
	frmeditMyProfile.lblcontadd.text = kony.i18n.getLocalizedString("keyContactAddress");
	LocaleController.updatedForm(frmeditMyProfile.id);
   }
	//frmeditMyProfile.lblcustname.text = frmMyProfile.lblCusName.text;	
	frmeditMyProfile.hbox4758937266356.setEnabled(false);
	frmeditMyProfile.hbox4758937266374.setEnabled(false);
	frmeditMyProfile.hbox4758937266440.setEnabled(false);
	var ps = kony.i18n.getLocalizedString('keyIBPleaseSelect');
	var currentLocales = kony.i18n.getCurrentLocale();
	if(gblnotcountry)
	{
		frmeditMyProfile.lblsubdistrict.text = ps;
        frmeditMyProfile.lbldistrict.text = ps;
        frmeditMyProfile.lblProvince.text = ps;
        frmeditMyProfile.lblzipcode.text = ps;
	}
	else
	{
		if(mptemp != "locale" && confirmEdit != true)
		{
			if (gblStateValue == null || gblStateValue == "undefined" || gblStateValue == "" || gblStateEng == null || gblStateEng == "undefined" || gblStateEng == "") {
				
				frmeditMyProfile.lblProvince.text = ps;
				frmeditMyProfile.lbldistrict.text = ps;
				frmeditMyProfile.lblsubdistrict.text = ps;
				frmeditMyProfile.lblzipcode.text = ps;
			} 
			else
			{
			        if(currentLocales == "th_TH")
							frmeditMyProfile.lblProvince.text = gblStateValue;
						else 	frmeditMyProfile.lblProvince.text = gblStateEng;		
					if (gbldistrictValue == null || gbldistrictValue == "undefined" || gbldistrictValue == "" || gblDistEng == null || gblDistEng == "undefined" || gblDistEng == "") {
						frmeditMyProfile.lbldistrict.text = ps;
						frmeditMyProfile.lblsubdistrict.text = ps;
						frmeditMyProfile.lblzipcode.text = ps;
					} else {
					    if(currentLocales == "th_TH")
							frmeditMyProfile.lbldistrict.text = gbldistrictValue;
						else 	frmeditMyProfile.lbldistrict.text = gblDistEng;
						if (gblsubdistrictValue == null || gblsubdistrictValue == "undefined" || gblsubdistrictValue == "" || gblSubDistEng == null || gblSubDistEng == "undefined" || gblSubDistEng == "") {
								frmeditMyProfile.lblsubdistrict.text = ps;
								frmeditMyProfile.lblzipcode.text = ps;
							} 
						else {
						    if(currentLocales == "th_TH")
								frmeditMyProfile.lblsubdistrict.text = gblsubdistrictValue;
							else 	frmeditMyProfile.lblsubdistrict.text = gblSubDistEng;
							if (gblzipcodeValue == null || gblzipcodeValue == "undefined" || gblzipcodeValue == "" || gblZipEng == null || gblZipEng == "undefined" || gblZipEng == "") {
								frmeditMyProfile.lblzipcode.text = ps;
							} else {
								frmeditMyProfile.lblzipcode.text = gblzipcodeValue;
							}
						}
						
					}
					
			}
		}
		else
		{
			mptemp = "preshow";
			populateAddressLocaleFbConfirmEdit();
		}
	}
	frmeditMyProfile.imgprofpic.src = frmMyProfile.image247502979411375.src;
	frmeditMyProfile.btnfb.setEnabled(true);
	frmeditMyProfile.txtAddress1.text = gblAddress1Value;
	frmeditMyProfile.txtAddress2.text = gblAddress2Value;
	frmeditMyProfile.txtemailvalue.text = "";
	frmeditMyProfile.txtemailvalue.placeholder = frmMyProfile.lblEmailVal.text;
	if(flowSpa)
	 {
	 frmeditMyProfile.hbox475868836197589.setVisibility(false);
	 }
	 else
	 {
	frmeditMyProfile.lbldevicename.text = kony.i18n.getLocalizedString("keyDeviceNameLabel") + ":";
	frmeditMyProfile.txtdevicenamevalue.text = "";
	frmeditMyProfile.txtdevicenamevalue.placeholder = gblDeviceNickName;
	}
	if(((frmMyProfile.lblfbidstudio1.text) != null) && ((frmMyProfile.lblfbidstudio1.text) != ""))//Modified by Studio Viz
	{
		frmeditMyProfile.lblfbidvalue.text = frmMyProfile.lblfbidstudio1.text;//Modified by Studio Viz
		//frmeditMyProfile.btnfb.setEnabled(false);
	}
	else{
		frmeditMyProfile.lblfbidvalue.text = "";
	}
}


function frmMBTnCPreShow() {
	changeStatusBarColor();
	var currentLocale = kony.i18n.getCurrentLocale();
	frmMBTnC.lblHdrTxt.text = kony.i18n.getLocalizedString('keyTermsNConditions')
	frmMBTnC.btnEngR.text = kony.i18n.getLocalizedString('languageEng')
	frmMBTnC.btnThaiR.text = kony.i18n.getLocalizedString('languageThai')
	//frmMBTnC.lblTandC.text = kony.i18n.getLocalizedString('TCMessage')
	 
	if (flowSpa) {
		frmMBTnC.btnCancelSpa.text = kony.i18n.getLocalizedString('keyCancelButton')
		frmMBTnC.btnAgreeSpa.text = kony.i18n.getLocalizedString('keyAgreeButton')
	} else {
		frmMBTnC.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton')
		frmMBTnC.btnAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
	}
	if (currentLocale == "en_US") {
		frmMBTnC.btnEngR.skin = "btnOnFocus";
		frmMBTnC.btnThaiR.skin = "btnOffFocus";
	} else {
		frmMBTnC.btnEngR.skin = "btnOnNormal";
		frmMBTnC.btnThaiR.skin = "btnOffNorm";
	}
	//gblLocale = false;
	var input_param = {};
	 input_param["localeCd"]=currentLocale;
	 input_param["activationVia"]=gblMBActivationVia;
	 input_param["moduleKey"]='TermsAndConditions';
	 showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, setMBTnC);	
}


function setMBTnC(status, result) {
    var currentLocale = kony.i18n.getCurrentLocale();
    if (status == 400) {
        if (result["opstatus"] == 0) {

            if (flowSpa) {
                frmMBTnC.lblTandCSpa.text = "";
                frmMBTnC.lblTandCSpa.text = result["fileContent"];
				frmMBTnC.btnAgreeSpa.setEnabled(true);
            } else {
                gblAESKey = result["ak"];
                GLOBAL_PUBLIC_KEY = result["publicKey"];
                gblDPPk=result["pk"];
                gblDPRandNumber=result["randomNumber"];
                frmMBTnC.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton')
                frmMBTnC.btnAgree.text = kony.i18n.getLocalizedString('keyAgreeButton')
                frmMBTnC.lblHdrTxt.text = kony.i18n.getLocalizedString('keyTermsNConditions')
                if (currentLocale == "en_US") {
                    frmMBTnC.lblTandCEng.text = "";
                    frmMBTnC.hbxTandCEng.setVisibility(true);
                    frmMBTnC.hbxTandCTh.setVisibility(false);
                    frmMBTnC.lblTandCEng.text = result["fileContent"];
                } else {
                    frmMBTnC.lblTandCTh.text = "";
                    frmMBTnC.hbxTandCEng.setVisibility(false);
                    frmMBTnC.hbxTandCTh.setVisibility(true);
                    frmMBTnC.lblTandCTh.text = result["fileContent"];
                }
				frmMBTnC.btnAgree.setEnabled(true);
            }

        } else {
            if (flowSpa) {
                if (kony.i18n.getCurrentLocale() == "th_TH") {
                    frmMBTnC.lblTandCSpa.text = "";
                } else {
                    frmMBTnC.lblTandCSpa.text = "";
                }
               frmMBTnC.btnAgreeSpa.setEnabled(false);
            } else {
                frmMBTnC.lblTandC.text = "";
                frmMBTnC.btnAgree.setEnabled(false);
            }
        }
        frmMBTnC.show();
    }
    dismissLoadingScreen();
}

function frmMBAccLockedPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmMBAccLocked.id))){
	frmMBAccLocked.lblHdrTxt.text = kony.i18n.getLocalizedString('keyAccountLocked')
	frmMBAccLocked.richtext4483370253249.text = kony.i18n.getLocalizedString('languageThai')
	frmMBAccLocked.richtext4483370253244.text = kony.i18n.getLocalizedString('contactTMB')
	frmMBAccLocked.button4483370253250.text = kony.i18n.getLocalizedString('resetPass')
    LocaleController.updatedForm(frmMBAccLocked.id);
  }
}

function frmMBActiCompletePreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMBActiComplete.id))){
	frmMBActiComplete.lblHdrTxt.text = kony.i18n.getLocalizedString('Complete')
	//frmMBActiComplete.lblMsg.text = kony.i18n.getLocalizedString('ATMmsg')
	//frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString('appReadytoUse')
	frmMBActiComplete.btnStart.text = kony.i18n.getLocalizedString('keyStart')
	if(flowSpa){
		frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString("keyIBFirstActiveSucces");
	}
    LocaleController.updatedForm(frmMBActiComplete.id);
   }
	gblLocale = false;
	if(!flowSpa)frmMBActiComplete.lblHdrTxt.containerWeight=100;
	//changeCampaignMBPreLocale(frmMBActiComplete.image2447443295186,frmMBActiComplete.hbxAdv);
	ChangeCampaignLocale();
	
	//code for personalized banner display
	try{
	   	frmMBActiComplete.hbxAdv.setVisibility(false);
	   	frmMBActiComplete.gblBrwCmpObject.handleRequest="";
	   	frmMBActiComplete.gblBrwCmpObject.htmlString="";
	   	frmMBActiComplete.image2447443295186.src="";
	   	frmMBActiComplete.gblVbxCmp.remove(gblBrwCmpObject);
       	frmMBActiComplete.hbxAdv.remove(gblVbxCmp);
	}
	catch(e)
	{
	}
	
}


function frmMBActiConfirmPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmMBActiConfirm.id))){
	if (flowSpa) {
		frmMBActiConfirm.label475124774164.text = kony.i18n.getLocalizedString('keyActivateIBAppSpa')
		frmMBActiConfirm.btnOTP.text = kony.i18n.getLocalizedString('Next')
	} else {
		frmMBActiConfirm.label475124774164.text = kony.i18n.getLocalizedString('keyActivateMobileApp');
		frmMBActiConfirm.btnConfirm.text = kony.i18n.getLocalizedString("Next");
		frmMBActiConfirm.btnCancelActivation.text = kony.i18n.getLocalizedString("keyCancelButton");
	    frmMBActiConfirm.label475124774164.padding = [0, 0, 0, 0];
	}
	frmMBActiConfirm.lblInfo.text = kony.i18n.getLocalizedString('nextFollowInfoCrt')
	frmMBActiConfirm.lblAccNo.text = kony.i18n.getLocalizedString('AccNo')
	frmMBActiConfirm.lblIDNo.text = kony.i18n.getLocalizedString('IDNo')
	frmMBActiConfirm.lblPhNo.text = kony.i18n.getLocalizedString('PhNo')
	frmMBActiConfirm.label4742849861214.text = kony.i18n.getLocalizedString('keyStep1Label')
    LocaleController.updatedForm(frmMBActiConfirm.id);
  }
	gblLocale = false;
}

function frmMBActivationPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmMBActivation.id))){
	if (flowSpa) {
		frmMBActivation.label475124774164.text = kony.i18n.getLocalizedString('keyActivateIBAppSpa')
	} else {
				frmMBActivation.label475124774164.text = kony.i18n.getLocalizedString('keyActivateMobileApp');//DEF1413
	
		/*if(gblSetPwd)
			frmMBActivation.label475124774164.text = kony.i18n.getLocalizedString('ResetMBPwd')
		else
			frmMBActivation.label475124774164.text = kony.i18n.getLocalizedString('keyActivateMobileApp')*/
	}
	frmMBActivation.label4742849861214.text = kony.i18n.getLocalizedString('keyStep1Label')
	frmMBActivation.lblInfo.text = kony.i18n.getLocalizedString('keyInputInfolbl')
	frmMBActivation.lblActivationCode.text = kony.i18n.getLocalizedString('ActCode')
	frmMBActivation.lblAccountNumber.text = kony.i18n.getLocalizedString('AccNo')
	frmMBActivation.lblIDPass.text = kony.i18n.getLocalizedString('IDNo')
	if(flowSpa)
	{
	frmMBActivation.btnConfirmSpa.text = kony.i18n.getLocalizedString('Next')
	}
	else
	{
	frmMBActivation.btnConfirm.text = kony.i18n.getLocalizedString('Next');
	frmMBActivation.btnCancelActivation.text=kony.i18n.getLocalizedString("keyCancelButton");
	}
    LocaleController.updatedForm(frmMBActivation.id);
  }
	gblLocale = false;
}

function frmMBSetAccPinPwdPreShow() {
	changeStatusBarColor();
	if(!isNotBlank(gblShowPwdNo)) {
		gblShowPwdNo = 3;
	}
	frmMBSetAccPinTxnPwd.lblShowPinPwd.text = kony.i18n.getLocalizedString('showPnP')
	frmMBSetAccPinTxnPwd.lblHdrTxt.text = kony.i18n.getLocalizedString('keySetPINandPassword')
	frmMBSetAccPinTxnPwd.lblAccPin.text = kony.i18n.getLocalizedString('AccesPIN')
	frmMBSetAccPinTxnPwd.lblTxnPwd.text = kony.i18n.getLocalizedString('TransactionPass')
	frmMBSetAccPinTxnPwd.lblStep3.text = kony.i18n.getLocalizedString('step3')

	frmMBSetAccPinTxnPwd.lblAccPinRule.text=kony.i18n.getLocalizedString("keyAccPinRule");
	frmMBSetAccPinTxnPwd.lblTransPwdRule.text=kony.i18n.getLocalizedString("keyTxnPwdRule");
	frmMBSetAccPinTxnPwd.btnNext.text = kony.i18n.getLocalizedString('Next');
	frmMBSetAccPinTxnPwd.btnCancel1.text = kony.i18n.getLocalizedString('keyCancelButton');
	frmMBSetAccPinTxnPwd.txtTransPass.placeholder = kony.i18n.getLocalizedString('keyPleaseSetTxnPwd');
	frmMBSetAccPinTxnPwd.txtTransPassShow.placeholder = kony.i18n.getLocalizedString('keyPleaseSetTxnPwd');
	frmMBSetAccPinTxnPwd.lblInformation.text = kony.i18n.getLocalizedString('keyPleaseSetInfo');
	frmMBSetAccPinTxnPwd.lblAccPinTxnPwdNote.text = kony.i18n.getLocalizedString('keyAccPinTxnPwdNote');
	gblLocale = false;
	
}

function frmMBEnterATMPinPreShow() {
	changeStatusBarColor();
	frmMBEnterATMPin.lblHdrTxt.text = kony.i18n.getLocalizedString("keyActivateMobileApp");
	frmMBEnterATMPin.lblEnterAtmPin.text = kony.i18n.getLocalizedString("keyAtmCardPin");
	frmMBEnterATMPin.btnNext.text = kony.i18n.getLocalizedString("Next");
	frmMBEnterATMPin.btnCancel1.text = kony.i18n.getLocalizedString("keyCancelButton");
	gblLocale = false;
	
}
function frmMBsetPasswdPreShow() {
	changeStatusBarColor();
	frmMBsetPasswd.label50285458145.text = kony.i18n.getLocalizedString('showPnP')
	frmMBsetPasswd.btnPwdOn.text = kony.i18n.getLocalizedString('keyONButton')
	frmMBsetPasswd.btnPwdOff.text = kony.i18n.getLocalizedString('keyOFFButton')
	frmMBsetPasswd.label50285458139.text = kony.i18n.getLocalizedString('AccesPIN')
	frmMBsetPasswd.label50285458141.text = kony.i18n.getLocalizedString('TransactionPass')
	frmMBsetPasswd.lblDeviceName.text = kony.i18n.getLocalizedString('keyDeviceNameLabel')
	frmMBsetPasswd.label50285458134.text = kony.i18n.getLocalizedString('keyStep2Label')
	if (gblActionCode == "23") {
		//frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keyPwdInput");
	} else if (gblActionCode == "21") {
		//frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keySetInfoLabel");
		frmMBsetPasswd.lblAccessPinRule.text=kony.i18n.getLocalizedString("AccessPinInLineRule");
		frmMBsetPasswd.lblTransPwdRule.text=kony.i18n.getLocalizedString("TraxnPwdInlineRule");
	} else if (gblActionCode == "22") {
		//frmMBsetPasswd.lblSetInfo.text = kony.i18n.getLocalizedString("keySetInfoLabel");
	}
	frmMBsetPasswd.button44726454989124.text = kony.i18n.getLocalizedString('Next')
	popAccessPinBubble.btnAccessPinbubble.text = kony.i18n.getLocalizedString('keyPwdHelpIcon')
	gblLocale = false;
	// below block is inserted to overcome DEF 412
	iphone6check();
	if(isIphone6MB){
		frmMBsetPasswd.button474999996108.margin = [0, 0, 3, 0];
		frmMBsetPasswd.button474999996107.margin = [0, 0, 2, 0];
		frmMBsetPasswd.vbox44473234061.containerWeight = 91;
		frmMBsetPasswd.button474999996107.containerWeight = 9;
		//frmMBsetPasswd.button474999996107.widgetAlignment = constants.WIDGET_ALIGN_TOP_RIGHT;
	}	
}
/*function frmeditMyProfilePre()
{
	frmeditMyProfile.lbldevicename.text = kony.i18n.getLocalizedString("keyDeviceNameLabel") + ":";
	frmeditMyProfile.lblemail.text =  	kony.i18n.getLocalizedString("keyEmail");
	if (kony.i18n.getCurrentLocale() == "th_TH")
		{frmeditMyProfile.lblcustname.text = gblCustomerNameTh;}
	else
		{frmeditMyProfile.lblcustname.text = gblCustomerName;}
	frmeditMyProfile.txtemailvalue.placeholder=gblEmailAddr;
	frmeditMyProfile.button209682697497232.text = kony.i18n.getLocalizedString("keychoosePicture");
	frmeditMyProfile.label475124774164.text = kony.i18n.getLocalizedString("keyIBEditMyProfile");
	frmeditMyProfile.lblfbid.text = kony.i18n.getLocalizedString("keyFacebookID");
	frmeditMyProfile.lblcontadd.text = kony.i18n.getLocalizedString("keyContactAddress");
	if (flowSpa){
	frmeditMyProfile.button104235284554498.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmeditMyProfile.button104235284554500.text = kony.i18n.getLocalizedString("keysave");}
	
	else{
	frmeditMyProfile.button474230331217991.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmeditMyProfile.button474230331217989.text = kony.i18n.getLocalizedString("keysave");
	
	}
	var ps = kony.i18n.getLocalizedString('keyIBPleaseSelect');
	if(gblnotcountry)
	{
		frmeditMyProfile.lblsubdistrict.text = ps;
        frmeditMyProfile.lbldistrict.text = ps;
        frmeditMyProfile.lblProvince.text = ps;
        frmeditMyProfile.lblzipcode.text = ps;
	}
	else
	{
		if (gblStateValue == null || gblStateValue == "undefined" || gblStateValue == "" || gblStateEng == null || gblStateEng == "undefined" || gblStateEng == "") {
			
			frmeditMyProfile.lblProvince.text = ps;
			frmeditMyProfile.lbldistrict.text = ps;
			frmeditMyProfile.lblsubdistrict.text = ps;
			frmeditMyProfile.lblzipcode.text = ps;
		} 
		else
		{
		        if(currentLocales == "th_TH")
						frmeditMyProfile.lblProvince.text = gblStateValue;
					else 	frmeditMyProfile.lblProvince.text = gblStateEng;		
				if (gbldistrictValue == null || gbldistrictValue == "undefined" || gbldistrictValue == "" || gblDistEng == null || gblDistEng == "undefined" || gblDistEng == "") {
					frmeditMyProfile.lbldistrict.text = ps;
					frmeditMyProfile.lblsubdistrict.text = ps;
					frmeditMyProfile.lblzipcode.text = ps;
				} else {
				    if(currentLocales == "th_TH")
						frmeditMyProfile.lbldistrict.text = gbldistrictValue;
					else 	frmeditMyProfile.lbldistrict.text = gblDistEng;
					if (gblsubdistrictValue == null || gblsubdistrictValue == "undefined" || gblsubdistrictValue == "" || gblSubDistEng == null || gblSubDistEng == "undefined" || gblSubDistEng == "") {
							frmeditMyProfile.lblsubdistrict.text = ps;
							frmeditMyProfile.lblzipcode.text = ps;
						} 
					else {
					    if(currentLocales == "th_TH")
							frmeditMyProfile.lblsubdistrict.text = gblsubdistrictValue;
						else 	frmeditMyProfile.lblsubdistrict.text = gblSubDistEng;
						if (gblzipcodeValue == null || gblzipcodeValue == "undefined" || gblzipcodeValue == "" || gblZipEng == null || gblZipEng == "undefined" || gblZipEng == "") {
							frmeditMyProfile.lblzipcode.text = ps;
						} else {
							frmeditMyProfile.lblzipcode.text = gblzipcodeValue;
						}
					}
					
				}
				
		}
	}
}*/
function frmMyProfilePreShow() {
	changeStatusBarColor();
	if (kony.i18n.getCurrentLocale() == "th_TH"){ 
			frmMyProfile.lblCusName.text = gblCustomerNameTh;}
			else
			{
			frmMyProfile.lblCusName.text = gblCustomerName;
			}
	frmMyProfile.lblEmailVal.text=gblEmailAddr;
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyProfile.id))){
	frmMyProfile.label475124774164.text = kony.i18n.getLocalizedString('keyMyProfile')
	frmMyProfile.lblEmail.text = kony.i18n.getLocalizedString('keyEmail')
	frmMyProfile.lnkExpand.text = kony.i18n.getLocalizedString('More')
	frmMyProfile.lblupdatedprofile.text = kony.i18n.getLocalizedString('keyupdatedProfile')
	//frmMyProfile.lblCusName.text = 
	frmMyProfile.label47502979411388.text = kony.i18n.getLocalizedString('keyMobileNumber')
	if (flowSpa) {
		frmMyProfile.label104026732091043.text = kony.i18n.getLocalizedString('keySPAChangeUserID')
		frmMyProfile.label104026732090593.text = kony.i18n.getLocalizedString('keyChangePassword')
	} else {
		frmMyProfile.label47502979413802.text = kony.i18n.getLocalizedString('keyChangeAccessPass')
		frmMyProfile.label47502979413832.text = kony.i18n.getLocalizedString('keyChangeTransPass')
	}
	frmMyProfile.lblDailytransLimit.text = kony.i18n.getLocalizedString('keyDailyTransLimit')
	frmMyProfile.lblContactAdd1.text = kony.i18n.getLocalizedString('keyContactAddress')
	frmMyProfile.lblRegAddress1.text = kony.i18n.getLocalizedString('keyRegAddress')
	frmMyProfile.lblDevicename.text = kony.i18n.getLocalizedString("keyDeviceNameLabel") + ":";
	frmMyProfile.link508395714292481.text=kony.i18n.getLocalizedString('keyRequestHistory');
	frmMyProfile.lblSetID.text = kony.i18n.getLocalizedString('MIB_AnyIDMenu');
	LocaleController.updatedForm(frmMyProfile.id);
   }
	//gblLocale = false;
}
function frmMyProfileReqHistoryPreShow(){
			changeStatusBarColor();
			 var temp=[];
		    temp.push(["1",kony.i18n.getLocalizedString("keySortAsc")]);
			temp.push(["2",kony.i18n.getLocalizedString("keySortDesc")]);
			frmMyProfileReqHistory.combobox508395714279435.masterData=temp;
			frmMyProfileReqHistory.combobox508395714279435.selectedKey = 2;
			//frmMyProfileReqHistory.combobox508395714279435.onSelection = sortedRequestDataMB;
			frmMyProfileReqHistory.label475064479211622.text = kony.i18n.getLocalizedString("Transfer_Date");
			frmMyProfileReqHistory.label475124774164.text = kony.i18n.getLocalizedString("keyRequestHistory");
			frmMyProfileReqHistory.button508395714293791.text = kony.i18n.getLocalizedString("Back");
			viewRequestHistoryMB(2);
			
}

function frmCMChgTransPwdPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmCMChgTransPwd.id))){
	frmCMChgTransPwd.lblHdrTxt.text = kony.i18n.getLocalizedString('changeTransPass')
	frmCMChgTransPwd.label4769997143084.text = kony.i18n.getLocalizedString('currentPass')
	frmCMChgTransPwd.label4769997143085.text = kony.i18n.getLocalizedString('newPass')
	frmCMChgTransPwd.label50285458145.text = kony.i18n.getLocalizedString('showPass')
	frmCMChgTransPwd.btnPwdOn.text = kony.i18n.getLocalizedString('keyONButton')
	frmCMChgTransPwd.btnPwdOff.text = kony.i18n.getLocalizedString('keyOFFButton')
	frmCMChgTransPwd.label4769997143099.text = kony.i18n.getLocalizedString('TransactionPass')
	frmCMChgTransPwd.button44843014510078.text = kony.i18n.getLocalizedString('keyCancelButton')
	frmCMChgTransPwd.button4769997143093.text = kony.i18n.getLocalizedString('keysave')
	LocaleController.updatedForm(frmCMChgTransPwd.id);
  }
	gblLocale = false;
}

function frmCMChgAccessPinPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmCMChgAccessPin.id))){
	if (flowSpa) {
		frmCMChgAccessPin.lblHdrTxt.text = kony.i18n.getLocalizedString('keySPAChangeUserID')
		frmCMChgAccessPin.label104026732091607.text = kony.i18n.getLocalizedString('keySPACurrentUserID')
		frmCMChgAccessPin.label104026732091614.text = kony.i18n.getLocalizedString('keySPANewUserID')
		frmCMChgAccessPin.lblinlineuseridgudlines.text=kony.i18n.getLocalizedString('keyIBInlineUserIDGuidelines')
		frmCMChgAccessPin.button473361467785887.text = kony.i18n.getLocalizedString('keyCancelButton')
		frmCMChgAccessPin.button473361467785889.text = kony.i18n.getLocalizedString('keysave')
	} else {
		frmCMChgAccessPin.lblHdrTxt.text = kony.i18n.getLocalizedString('changeAccessPIN')
		frmCMChgAccessPin.label4769997143084.text = kony.i18n.getLocalizedString('currentAccessPIN')
		frmCMChgAccessPin.label4769997143085.text = kony.i18n.getLocalizedString('newAccessPIN')
		frmCMChgAccessPin.label50285458145.text = kony.i18n.getLocalizedString('showPass')
		frmCMChgAccessPin.btnPwdOn.text = kony.i18n.getLocalizedString('keyONButton')
		frmCMChgAccessPin.btnPwdOff.text = kony.i18n.getLocalizedString('keyOFFButton')
		frmCMChgAccessPin.button4769997143092.text = kony.i18n.getLocalizedString('keyCancelButton')
		frmCMChgAccessPin.button4769997143093.text = kony.i18n.getLocalizedString('keysave')
	}
   LocaleController.updatedForm(frmCMChgAccessPin.id);
  }
   if (flowSpa) {
   		frmCMChgAccessPin.txtCurUserID.text = ""
		frmCMChgAccessPin.txtNewUserID.text = ""
		//frmCMChgAccessPin.txtCurUserID.setFocus(true);
		frmCMChgAccessPin.hbox50285458144.setVisibility(false);
		frmCMChgAccessPin.lblHdrTxt.text = kony.i18n.getLocalizedString('keySPAChangeUserID')
   } else {
   		frmCMChgAccessPin.lblHdrTxt.text = kony.i18n.getLocalizedString('changeAccessPIN')
   		frmCMChgAccessPin.tbxCurAccPin.setVisibility(true);
		frmCMChgAccessPin.txtAccessPwd.setVisibility(true);
		frmCMChgAccessPin.btnPwdOn.skin = "btnOnNormal";
		frmCMChgAccessPin.btnPwdOff.skin = "btnOffNorm";
		frmCMChgAccessPin.tbxCurAccPinUnMask.setVisibility(false);
		frmCMChgAccessPin.txtAccessPwdUnMask.setVisibility(false);
   }
	gblLocale = false;
}

function frmApplyInternetBankingMBPreShow() {
	changeStatusBarColor();
//changed the i18key from Complete to InternetBanking
   if(!(LocaleController.isFormUpdatedWithLocale(frmApplyInternetBankingMB.id))){
	if (gblApplyServices == 2)
	{
		frmApplyInternetBankingMB.lblHead.text = kony.i18n.getLocalizedString('InternetBanking')
		frmApplyInternetBankingMB.lblNumberInformation.text = kony.i18n.getLocalizedString('InternetBankingDesc')
		frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString("activationCodeSentInternet");
	}
	else if(gblApplyServices == 4)
	{
		var lblText = kony.i18n.getLocalizedString("keyForgotPasswordHeader");
		frmApplyInternetBankingMB.lblHead.text = lblText;
		frmApplyInternetBankingMB.lblNumberInformation.text = kony.i18n.getLocalizedString("unlockIbankingDesc");
		frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString("activationCodeSentUnlockIBanking");
	}
	else if(gblApplyServices == 3)
	{
		var lblText = kony.i18n.getLocalizedString("AddNewDeviceIB");
		frmApplyInternetBankingMB.lblHead.text = lblText;
		frmApplyInternetBankingMB.lblNumberInformation.text = kony.i18n.getLocalizedString("addDeviceDesc");
		frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString("activationCodeSentNewDevice");
	}
	//frmApplyInternetBankingMB.lblMobileNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
	frmApplyInternetBankingMB.lblApplyInternetBankingNote.text = kony.i18n.getLocalizedString('NoteIncorrectMob')
	frmApplyInternetBankingMB.lnkChangeMobileNo.text = kony.i18n.getLocalizedString('changeMobNo')
	frmApplyInternetBankingMB.btnInternetBankingNext.text = kony.i18n.getLocalizedString('Next')
    LocaleController.updatedForm(frmApplyInternetBankingMB.id);
  }
    frmApplyInternetBankingMB.lblMobileNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
	gblLocale = false;
}
//function frmApplyMBSPAPreShow() {
//	frmApplyMBSPA.lblHead.text = kony.i18n.getLocalizedString('Complete')
//	frmApplyMBSPA.lblNumberInformation.text = kony.i18n.getLocalizedString('activationCodeSentNewDevice')
//	frmApplyMBSPA.lblMobileNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
//	frmApplyMBSPA.lblApplyInternetBankingNote.text = kony.i18n.getLocalizedString('NoteIncorrectMob')
//	frmApplyMBSPA.lnkChangeMobileNo.text = kony.i18n.getLocalizedString('changeMobNo')
//	frmApplyMBSPA.btnInternetBankingNext.text = kony.i18n.getLocalizedString('Next')
//	gblLocale = false;
//}

function frmApplyInternetBankingConfirmationPreShow() {
changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmApplyInternetBankingConfirmation.id))){
	frmApplyInternetBankingConfirmation.label475124774164.text = kony.i18n.getLocalizedString('Complete')
	if (gblApplyServices == 2) 
	{
		frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString('activationCodeSentInternet')
  	}
	else if (gblApplyServices == 3) 
	{
		frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString('activationCodeSentNewDevice')
	}
	else if (gblApplyServices == 4)
	{
		frmApplyInternetBankingConfirmation.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString('activationCodeSentUnlockIBanking')	
	}
	frmApplyInternetBankingConfirmation.btnBackHome.text = kony.i18n.getLocalizedString('retToHome')
    LocaleController.updatedForm(frmApplyInternetBankingConfirmation.id);
  }
	gblLocale = false;
	
	//code for personalized banner display
	try{
	   	frmApplyInternetBankingConfirmation.hbxAdv.setVisibility(false);
	   	frmApplyInternetBankingConfirmation.gblBrwCmpObject.handleRequest="";
	   	frmApplyInternetBankingConfirmation.gblBrwCmpObject.htmlString="";
	   	frmApplyInternetBankingConfirmation.imgMIBConf.src="";
	   	frmApplyInternetBankingConfirmation.gblVbxCmp.remove(gblBrwCmpObject);
       	frmApplyInternetBankingConfirmation.hbxAdv.remove(gblVbxCmp);
	}
	catch(e)
	{
	}
	
}
function frmApplyMBConfirmationSPAPreShow() {
	var applyMbFromSpa = false;
	if (gblApplyServices == 2){
		applyMbFromSpa = true;
	}
   if(!(LocaleController.isFormUpdatedWithLocale(frmApplyMBConfirmationSPA.id))){
	frmApplyMBConfirmationSPA.label475124774164.text = kony.i18n.getLocalizedString('Complete')
	if (gblApplyServices == 2) 
	{
		//applyMbFromSpa = true;
		frmApplyMBConfirmationSPA.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString('keySPAapplyServicesMB')
  	}
	else if (gblApplyServices == 3) 
	{
		frmApplyMBConfirmationSPA.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString('keySPAapplyServicesAddDevice')
	}
	else if (gblApplyServices == 4)
	{
		frmApplyMBConfirmationSPA.lblDescriptionActivationCodeSent.text = kony.i18n.getLocalizedString('keySPAapplyServicesResetMBPwd')	
	}
	frmApplyMBConfirmationSPA.btnBackHome.text = kony.i18n.getLocalizedString('retToHome')
    LocaleController.updatedForm(frmApplyMBConfirmationSPA.id);
   }
   
	gblLocale = false;
	if(applyMbFromSpa == false)
	{
		frmApplyMBConfirmationSPA.btnBackHome.setVisibility(true);
		frmApplyMBConfirmationSPA.hboxApplyMBPlaystoreLinks.setVisibility(false);
	}
	else
	{
		//frmApplyMBConfirmationSPA.btnBackHome.setVisibility(false);
		frmApplyMBConfirmationSPA.hboxApplyMBPlaystoreLinks.setVisibility(true);
	}
	
}

function clearFPflex(){
	frmMBPreLoginAccessesPin.lblLoginDesc1.text ="";
	frmMBPreLoginAccessesPin.imgFprint.src="";
}

gblFromLogout=false;
atAppLaunch=true;
function showTouchIdPopup(){
	showFPFlex(false);
	if (gblDeviceInfo["name"] == "iPhone") {
        //alert("onClickCheckTouchIdAvailable--->"+onClickCheckTouchIdAvailable());
		var currentFormId = kony.application.getCurrentForm().id;
         onClickCheckTouchIdAvailable();
         var returnedValue = TouchId.isTouchIDAvailable("iPhone");
         //alert("returnedValue: "+returnedValue);
        if (returnedValue == "YES" && isTouchIDEnabled()) {
            frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
            frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
            if (gblDeviceInfo["name"] == "iPhone" && gblPinCount == 0 && isMenuShown == false && currentFormId != "frmPreMenu") {
                //Creates an object of class 'TouchIdLogin
                var TouchIdLoginObject = new TouchId.TouchIdLogin();
                //show loading indicator
                showLoadingScreen();
                
            var localStr = ""; 
            if(kony.i18n.getCurrentLocale() == "en_US"){
                localStr = kony.i18n.getLocalizedString("keyTouchCancel");//"Or press cancel to login with Access PIN";
             } else { 
               //localStr = "ใส่รหัสผ่าน iPhone หรือ กด ยกเลิก เพื่อใส่รหัสผ่าน TMB Touch";
                localStr = kony.i18n.getLocalizedString("keyTouchCancel");//"ใส่รหัสผ่านเครื่อง/กดยกเลิกใส่รหัสTMB Touch";
             } 
  	             if(gblTouchShow){
                    //Invokes method 'authenicateButtonTapped' on the object
                    TouchIdLoginObject.authenicateButtonTapped(onTouchIdScan,localStr);
                 }else{
                 	gblTouchShow = true;
                 	kony.application.dismissLoadingScreen();
                 }
            }

            //onClickTounchandBack()        
        }else {
            onClickChangeTouchIcon();
        }
    }else if (gblDeviceInfo["name"] == "android"){
        kony.print("FPRINT: IN showTouchIdPopup ANDRIOD gblFromLogout="+gblFromLogout );
		onClickCheckTouchIdAvailable();
		if(isAuthUsingTouchSupported() && hasRegisteredFinger() && isTouchIDEnabled())
		{
			if(languageFlip){
				kony.print("FPRINT DO NOTHING LANGUAGE FLIP");
				languageFlip=false;
			}else{
				if(gblFromLogout){
					frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
					frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
					gblFromLogout=false;
					showFPFlex(false);
					gotUserStatus=true;
					isUserStatusActive=true;
				}else{	
				kony.print("FPRINT isMFsdkinit="+isMFsdkinit);
				kony.print("FPRINT isUserStatusActive="+isUserStatusActive);
						if(isUserStatusActive){ //After chkUserStatus(getDeviceID())
							kony.print("FPRINT gotUserStatus true and isUserStatusActive true");
							frmMBPreLoginAccessesPin.btnTouchnBack.skin = "btntouchiconstudio5";
							frmMBPreLoginAccessesPin.btnTouchnBack.focusSkin = "btntouchiconfoc";
		                	showFPFlex(true);
							fpdefault();
							closeQuickBalance();
						}else{
							kony.print("FPRINT gotUserStatus true and isUserStatusActive false");
							kony.print("FPRINT USER STATUS IS INACTIVE");
		                	showFPFlex(false);
							onClickChangeTouchIcon();
						}
				}
			
			}
		}else {
			showFPFlex(false);
			onClickChangeTouchIcon();
		}
	}
    dismissLoadingScreen();
}

function frmMBPreLoginAccessesPinPreShow() {
	changeStatusBarColor();
kony.print("FPRINT: IN frmMBPreLoginAccessesPinPreShow");

    if (gblDeviceInfo.length == 0) {
        gblDeviceInfo = kony.os.deviceInfo();
    }

    //alert("device name ---->"+gblDeviceInfo["name"]);
    //alert("device model---->"+gblDeviceInfo["model"]);
    var model = gblDeviceInfo["model"];
   /* if (model == "4GS iPhone") {
        //L T R B ..

        // alert("device model---->"+gblDeviceInfo["model"]);
        frmMBPreLoginAccessesPin.hbxHeadar.padding = [4, 3, 0, 3];
        frmMBPreLoginAccessesPin.imgTMBTouch.margin = [23, 0, 0, 0];

        frmMBForgotPin.hbxHeadar.padding = [4, 3, 0, 3];
        frmMBForgotPin.imgTMBTouch.margin = [23, 0, 0, 0];

        frmMBPreLoginAccessesPin.lblEnterPin.margin = [0, 0, 0, 0];
        frmMBPreLoginAccessesPin.hbxSmallCircles.margin = [8, 0, 0, 0];
        frmMBPreLoginAccessesPin.hbxtwo.margin = [0, 1, 0, 0];
        frmMBPreLoginAccessesPin.hbxthree.margin = [0, 1, 0, 0];
        frmMBPreLoginAccessesPin.hbxfour.margin = [0, 1, 0, 0];
        frmMBPreLoginAccessesPin.btnTwo.margin = [1, 1, 1, 1];
        frmMBPreLoginAccessesPin.btnFive.margin = [1, 1, 1, 1];
        frmMBPreLoginAccessesPin.btnEight.margin = [1, 1, 1, 1];
        frmMBPreLoginAccessesPin.btnZero.margin = [1, 1, 1, 1];

        //frmMBPreLoginAccessesPin.

    } */
    if(model == "iPhone 6 Plus"){
    
       //L T R B ..
      //frmMBPreLoginAccessesPin.btnForgotPin.padding = [4, 0, 4, 0];
    }
    
	//Commentin the code for DEF631
    /* if (!(LocaleController.isFormUpdatedWithLocale(frmMBPreLoginAccessesPin.id))) {
        frmMBPreLoginAccessesPin.btnForgotPin.text = kony.i18n.getLocalizedString('btnforgotpin');
        frmMBPreLoginAccessesPin.lblEnterPin.text = kony.i18n.getLocalizedString('enterpin');
        LocaleController.updatedForm(frmMBPreLoginAccessesPin.id);
    } */

    gblPinCount = 0;
    gblNum = "";

    frmMBPreLoginAccessesPin.lblEnterPin.text = kony.i18n.getLocalizedString("enterpin");
    frmMBPreLoginAccessesPin.btnForgotPin.text = kony.i18n.getLocalizedString('btnforgotpin');
    frmMBPreLoginAccessesPin.lblEnterPin.skin = "lblBlackMed150New";
    
    // Locale change as per new screen with QB
	frmMBPreLoginAccessesPin.lblHeader1.text = kony.i18n.getLocalizedString("VQB_MSG01");
	frmMBPreLoginAccessesPin.lblHeader2.text = kony.i18n.getLocalizedString("VQB_MSG02");
	frmMBPreLoginAccessesPin.btnQuickBalance.text = kony.i18n.getLocalizedString("VQB_MSG01");
	frmMBPreLoginAccessesPin.btnLoginToQB.text = kony.i18n.getLocalizedString("VQB_MSG03");
	frmMBPreLoginAccessesPin.lblQuickBalaceHeader.text = kony.i18n.getLocalizedString("VQB_MSG01");
	frmMBPreLoginAccessesPin.btnQuickBalanceCancel.text = kony.i18n.getLocalizedString("LOG_LogIn");
	
	if(kony.i18n.getCurrentLocale() == "th_TH"){       
		frmMBPreLoginAccessesPin.imgQBStatic.src = "quick_balance_th.png"
    }else{
        frmMBPreLoginAccessesPin.imgQBStatic.src = "quick_balance.png"
    }
	
	/* if(gblQuickBalanceResponse != ""){
		setDatatoQuickBalanceSegment();
	} */
    frmMBPreLoginAccessesPin.imgone.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgTwo.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgThree.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgFour.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgFive.src = "key_default.png";
    frmMBPreLoginAccessesPin.imgSix.src = "key_default.png";
   // var currentFormId = kony.application.getCurrentForm().id;
    //alert("onClickCheckTouchIdAvailable--->"+onClickCheckTouchIdAvailable());
    //alert("Device--->"+gblDeviceInfo["name"]);
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android") {
    kony.print("FPRINT: IN gblApplicationLaunch="+gblApplicationLaunch);
    	if(!gblApplicationLaunch){
    		showTouchIdPopup();
    	}
    }else {
        onClickChangeTouchIcon();
    }
    //frmAfterLogoutMB.tbxAccessPIN.text = "";
    //MBcopyright_text_display();
    //changeCampaignMBPreLocale(frmAfterLogoutMB.imgMBPreLogin,frmAfterLogoutMB.hbxAdv); need to confirm 
    if(frmMBPreLoginAccessesPin.flexAccounts.isVisible) {
      	changeLocalQuickBalanceService();
    }

    gblLocale = false;
    gblApplicationLaunch = false;
}

function frmMBForgotPinPreShow(){
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmMBForgotPin.id))){
      
      frmMBForgotPin.lblForgotPin.text = kony.i18n.getLocalizedString('forgotpin');
      frmMBForgotPin.lblMsgOne.text = kony.i18n.getLocalizedString('lblmsgone');
      frmMBForgotPin.lbldotmsgone.text = kony.i18n.getLocalizedString('dotmsgone');
      frmMBForgotPin.lbldotmsgtwo.text = kony.i18n.getLocalizedString('dotmsgtwo');
      frmMBForgotPin.lbldotmsgthree.text = kony.i18n.getLocalizedString('dotmsgthree');
      frmMBForgotPin.lblMsgTwo.text = kony.i18n.getLocalizedString('lblmsgtwo');
      frmMBForgotPin.btnReactivate.text = kony.i18n.getLocalizedString('reactivate');
      frmMBForgotPin.botnBack.text = kony.i18n.getLocalizedString('Back');
      
      
      LocaleController.updatedForm(frmMBForgotPin.id);
  }
  
}

function frmAccTrcPwdInterPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmAccTrcPwdInter.id))){
	frmAccTrcPwdInter.lblHdrTxt.text = kony.i18n.getLocalizedString('keyNotePwd')
	frmAccTrcPwdInter.lblAccTrcPwdInter.text = kony.i18n.getLocalizedString('keyNote')
	frmAccTrcPwdInter.label5027391408669.text = kony.i18n.getLocalizedString('keyChgPswd')
	if (flowSpa) {
		frmAccTrcPwdInter.btnPopUpTerminationSpa.text = kony.i18n.getLocalizedString('Next')
	} else {
		frmAccTrcPwdInter.btnPopUpTermination.text = kony.i18n.getLocalizedString('Next')
	}
   LocaleController.updatedForm(frmAccTrcPwdInter.id);
  }
   
	gblLocale = false;
	//popupTractPwd.dismiss();
}

function frmAccountSummaryLandingPreShow() {
	changeStatusBarColor();
	if(!(LocaleController.isFormUpdatedWithLocale(frmAccountSummaryLanding.id))){
		frmAccountSummaryLanding.label475124774164.text = kony.i18n.getLocalizedString('accountSummary')
		frmAccountSummaryLanding.lblTotal.text = kony.i18n.getLocalizedString('keyTotalDepoBal')
		frmAccountSummaryLanding.label4751247744353.text = kony.i18n.getLocalizedString('forUse')
		frmAccountSummaryLanding.label4751247744355.text = kony.i18n.getLocalizedString('forSave')
		frmAccountSummaryLanding.label4751247744357.text = kony.i18n.getLocalizedString('KeyForInvest')
		LocaleController.updatedForm(frmAccountSummaryLanding.id);
	}
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US"){
		frmAccountSummaryLanding.lblAccntHolderName.text = gblCustomerName;
		frmAccountSummaryLanding.label475124774164.margin = [0, 0, 0, 0];
	}
	else{
		frmAccountSummaryLanding.lblAccntHolderName.text = gblCustomerNameTh;
		frmAccountSummaryLanding.label475124774164.margin = [8, 0, 0, 0];
	}
	gblLocale = false;
}

function frmAccountDetailsMBPreShow() {
	try{
	  changeStatusBarColor();
	  if (gblAccountTable!= null && gblAccountTable != undefined && gblIndex!= null && gblIndex!= undefined && gblAccountTable["custAcctRec"] != null && gblAccountTable["custAcctRec"][gblIndex] != undefined && gblAccountTable["custAcctRec"][gblIndex] != null){
	   var sbStr = gblAccountTable["custAcctRec"][gblIndex]["accId"];
	   var length = sbStr.length;
	   if (gblAccountTable["custAcctRec"][gblIndex]["accType"] == kony.i18n.getLocalizedString("Loan"))
					sbStr = sbStr.substring(7, 11);
				else
					sbStr = sbStr.substring(length - 4, length);
	   	if(glb_viewName=="PRODUCT_CODE_CREDITCARD_TABLE" || glb_viewName=="PRODUCT_CODE_READYCASH_TABLE")
	   	{
	   		frmAccountDetailsMB.flexNormalImage.setVisibility(false);
	   		frmAccountDetailsMB.flxCardImage.setVisibility(true);
	   		frmAccountDetailsMB.flxCardImage.top = "-85dp";
	   		if(isNotBlank(gblAccountTable["custAcctRec"][gblIndex]["ProductImg"]))
	   		{
	   			var cardImage=gblAccountTable["custAcctRec"][gblIndex]["ProductImg"]
	   		}
	   		else
	   		{
	   			var cardImage="";
	   		}
	   		var randomnum = Math.floor((Math.random() * 10000) + 1);
			var imageName = getImageSRC(cardImage);
			frmAccountDetailsMB.imgcard.src=imageName;
			
        	var imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"] + "&modIdentifier=PRODICON&dummy=" + randomnum;
        	frmAccountDetailsMB.imgAccountDetailsPic.src = imageName;
			
	   	}
	   	else
	   	{
	   		frmAccountDetailsMB.flexNormalImage.setVisibility(true);
			frmAccountDetailsMB.flxCardImage.setVisibility(false);
		var randomnum = Math.floor((Math.random() * 10000) + 1);
			var imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"]+"_ACCDET"+"&modIdentifier=PRODICON&dummy=" + randomnum;
			frmAccountDetailsMB.imgAccountDetailsPic.src=imageName;
		}
		frmAccountDetailsMB.lblHead.text = kony.i18n.getLocalizedString('AccountDetails');
		frmAccountDetailsMB.btnMyActivities.text = kony.i18n.getLocalizedString('MyActivities');
		frmAccountDetailsMB.btnFullStatement.text = kony.i18n.getLocalizedString('FullStat');
		frmAccountDetailsMB.btnDreamSavings.text = kony.i18n.getLocalizedString("keyviewDream");
		frmAccountDetailsMB.btnEditBeneficiary.text=kony.i18n.getLocalizedString("editBenefit");
		//frmAccountDetailsMB.btnlinkDebitCard.text = "Debit Cards";
		//Yet to add the Credit Crad Code
		frmAccountDetailsMB.btnApplySoGood.text = kony.i18n.getLocalizedString("keyApplySoGooODLink");
		frmAccountDetailsMB.btnPointRedeem.text = kony.i18n.getLocalizedString("keyPointRedemption");
		frmAccountDetailsMB.btnManageCard.text = kony.i18n.getLocalizedString("keyManageCard");
		frmAccountDetailsMB.btnDreamSavings.text = kony.i18n.getLocalizedString("keyviewDream");
		//frmAccountDetailsMB.linkMore.text = kony.i18n.getLocalizedString('More')
		//frmAccountDetailsMB.lblDateHeader.text = kony.i18n.getLocalizedString('MatDate')
		//frmAccountDetailsMB.lblHeaderTransaction.text = kony.i18n.getLocalizedString('InterestRate')
		frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString('keyAvailableBalanceMB')
		frmAccountDetailsMB.lblDepositDetails.text = kony.i18n.getLocalizedString('DepDetails')
		frmAccountDetailsMB.btnBack1.text = kony.i18n.getLocalizedString('Back');
		
		//frmAccountDetailsMB.image24750595786995.setVisibility(true);
		//frmAccountStatementMB.hbxMyactivities.setVisibility(true);
		//frmAccountDetailsMB.lnkMyActivities.setVisibility(true);
		//frmAccountDetailsMB.lblLinkedDebit.text=kony.i18n.getLocalizedString('LinkedTo');
		
		// Sanjay New UI
		checkShowDebitCardMenu();
		var payBill=gblAccountTable["custAcctRec"][gblIndex]["Paybill"];
		var topup=gblAccountTable["custAcctRec"][gblIndex]["Topup"];
		var payMyCredit=gblAccountTable["custAcctRec"][gblIndex]["PayMyCreditCard"];
		var transfer=gblAccountTable["custAcctRec"][gblIndex]["Transfer"];
		var payMyLoan=gblAccountTable["custAcctRec"][gblIndex]["PayMyLoan"];
		var allowRedeemPoints = gblAccountTable["custAcctRec"][gblIndex]["allowRedeemPoints"];
		allowApplySoGoood = gblAccountTable["custAcctRec"][gblIndex]["allowApplySoGoood"];
		/*
		if(payBill=="Y")
		frmAccountDetailsMB.btnPayBill.isVisible = true;
		 if(topup=="Y")
		frmAccountDetailsMB.BtnTopUp.isVisible = true;
		 if(transfer=="Y")
		frmAccountDetailsMB.btnTransfer.isVisible = true;
		 if(payMyCredit=="Y")
		frmAccountDetailsMB.payMyCredit.isVisible = true;
		 if(payMyLoan=="Y")
		frmAccountDetailsMB.payMyLoan.isVisible = true;
		*/
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
		   if(payMyCredit=="Y" && payMyLoan=="N" && transfer=="N" && topup=="N"&& payBill=="N"){
			  frmAccountDetailsMB.flexbuttonone.isVisible = true;
			  frmAccountDetailsMB.flexbuttontwo.isVisible = false;
			  frmAccountDetailsMB.flexbuttonthree.isVisible = false;
			  frmAccountDetailsMB.button1.text=kony.i18n.getLocalizedString("PayBill");
			  frmAccountDetailsMB.button1.onClick=onclickCreditfromDetails;
			  //write on click and assign i18n key
			  }
			  else if(payMyCredit=="N" && payMyLoan=="Y" && transfer=="N" && topup=="N"&& payBill=="N"){
			 
			  frmAccountDetailsMB.flexbuttonone.isVisible = true;
			  frmAccountDetailsMB.flexbuttontwo.isVisible = false;
			  frmAccountDetailsMB.flexbuttonthree.isVisible = false;
			  frmAccountDetailsMB.button1.text=kony.i18n.getLocalizedString("PayBill");
			  frmAccountDetailsMB.button1.onClick=onclickLoanfromDetails;
			  //write on click and assign i18n key
			  }
			  else if(payMyCredit=="N" && payMyLoan=="N" && transfer=="Y" && topup=="N" && payBill=="N"){
			 
			  frmAccountDetailsMB.flexbuttonone.isVisible = true;
			  frmAccountDetailsMB.flexbuttontwo.isVisible = false;
			  frmAccountDetailsMB.flexbuttonthree.isVisible = false;
			  frmAccountDetailsMB.button1.text=kony.i18n.getLocalizedString("Transfer");
			  frmAccountDetailsMB.button1.onClick=onclickgetTransferFromAccountsFromAccDetails;
			  //write on click and assign i18n key
			  }
			  else if(payMyCredit=="N" && payMyLoan=="N" && transfer=="N" && topup=="N" && payBill=="Y")
			  {
			 
			  frmAccountDetailsMB.flexbuttonone.isVisible = true;
			  frmAccountDetailsMB.flexbuttontwo.isVisible = false;
			  frmAccountDetailsMB.flexbuttonthree.isVisible = false;
			  frmAccountDetailsMB.button1.text=kony.i18n.getLocalizedString("PayBill");
			  frmAccountDetailsMB.button1.onClick=onclickgetBillPaymentFromAccountsFromAccDetails;
			  //write on click and assign i18n key
			  }
			  else if(payMyCredit=="N" && payMyLoan=="Y" && transfer=="Y" && topup=="N" && payBill=="N")
			  {
			  frmAccountDetailsMB.flexbuttonone.isVisible = false;
			  frmAccountDetailsMB.flexbuttontwo.isVisible = true;
			  frmAccountDetailsMB.flexbuttonthree.isVisible = false;
			  frmAccountDetailsMB.button21.text=kony.i18n.getLocalizedString("Transfer");
			  frmAccountDetailsMB.button22.text=kony.i18n.getLocalizedString("PayBill");
			  frmAccountDetailsMB.button21.onClick=onclickgetTransferFromAccountsFromAccDetails;
			  frmAccountDetailsMB.button22.onClick=onclickgetBillPaymentFromAccountsFromAccDetails;
			  //write on click and assign i18n key
			 }
		   else if(payMyCredit=="N" && payMyLoan=="N" && transfer=="Y" && topup=="Y" && payBill=="Y"){
			 frmAccountDetailsMB.flexbuttonone.isVisible = false;
			 frmAccountDetailsMB.flexbuttontwo.isVisible = false;
			 frmAccountDetailsMB.flexbuttonthree.isVisible = true;
			 frmAccountDetailsMB.button11.text=kony.i18n.getLocalizedString("Transfer");
			 frmAccountDetailsMB.button12.text=kony.i18n.getLocalizedString("PayBill");
			 frmAccountDetailsMB.button13.text=kony.i18n.getLocalizedString("TopUp");
			 frmAccountDetailsMB.button11.onClick=onclickgetTransferFromAccountsFromAccDetails;
			 frmAccountDetailsMB.button12.onClick=onclickgetBillPaymentFromAccountsFromAccDetails;
			 frmAccountDetailsMB.button13.onClick=onclickgetTopUpFromAccountsFromAccDetails;
			  //write on click and assign i18n key
			}
		 else
		  {
			kony.print("In else button display condition");
			/*frmAccountDetailsMB.btnTransfer.containerWeight =23;
			frmAccountDetailsMB.payMyLoan.containerWeight=13;
			frmAccountDetailsMB.payMyCredit.containerWeight=14;
			frmAccountDetailsMB.btnPayBill.containerWeight=27;
			frmAccountDetailsMB.BtnTopUp.containerWeight=23;
			*/
		  }
		//code change for new UI
	
		if (glb_viewName == "PRODUCT_CODE_DREAM_SAVING") {
		   frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
		   frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
			var locale = kony.i18n.getCurrentLocale();
			var branchName;
			var branchName;
			var accNickName;
			var accType;
			if (locale == "en_US") {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
				
			} else {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			frmAccountDetailsMB.lblUsefulValue1.text = productName;
			frmAccountDetailsMB.lblUsefulValue10.text = branchName;
			frmAccountDetailsMB.lblUsefulValue6.text = accType;
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("DreamDes");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("DreamTarAmt");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("AccountType");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("AccountNo");
			frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("AccountName");
			frmAccountDetailsMB.lblUseful10.text = kony.i18n.getLocalizedString("BranchName");
			//frmAccountDetailsMB.lblUseful11.text = kony.i18n.getLocalizedString("LedgerBalance");
			frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("Status");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("DreamMonTranAmt");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("DreamMonTranDate");
			frmAccountDetailsMB.lblUseful13.text = kony.i18n.getLocalizedString("LinkedAcc");
			if((frmAccountDetailsMB.lblUsefulValue13.text).length!=13){
			  frmAccountDetailsMB.lblUsefulValue13.text="";
			 }
			if((frmAccountDetailsMB.lblUsefulValue13.text).length==0){
			 frmAccountDetailsMB.lblUsefulValue13.text=kony.i18n.getLocalizedString("lblNotFound");
			}
			if(gblAccountStatus!=null){
				var status=gblAccountStatus.split("|")
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue9.text =status[0];
				else
				frmAccountDetailsMB.lblUsefulValue9.text =status[1];
			}
		} else if (glb_viewName == "PRODUCT_CODE_NOFIXED") {
			frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
		   	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
			
			var accNickName;
			var productName;
			var branchName;
			var accType;
			var locale = kony.i18n.getCurrentLocale();
			if (locale == "en_US") {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
			} else {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			//frmAccountDetailsMB.link449290336691040.text = kony.i18n.getLocalizedString("applySSSrvce")
			frmAccountDetailsMB.lblUsefulValue1.text = productName;
			frmAccountDetailsMB.lblUsefulValue8.text = branchName;
			frmAccountDetailsMB.lblUsefulValue4.text = accType;
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountType");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("AccountNo");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("AccountName");
			frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("BranchName");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("Status");
			frmAccountDetailsMB.lblUseful10.text = kony.i18n.getLocalizedString("IntrestAmount");
			//frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("LedgerBalance");
			frmAccountDetailsMB.lblUseful11.text = kony.i18n.getLocalizedString("LinkedAcc")
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("SendToMaxPoint");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("SendToMinPoint");
			if(gblAccountStatus!=null){
				var status=gblAccountStatus.split("|")
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue7.text =status[0];
				else
				frmAccountDetailsMB.lblUsefulValue7.text =status[1];
			}
		} else if (glb_viewName == "PRODUCT_CODE_NOFEESAVING_TABLE") {
			frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
		   	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
			var locale = kony.i18n.getCurrentLocale();
			var branchName;
			var accNickName;
			var productName;
			var accType;
			if (locale == "en_US") {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
			} else {
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			frmAccountDetailsMB.lblUsefulValue7.text = branchName;
			frmAccountDetailsMB.lblUsefulValue3.text = accType;
			//frmAccountDetailsMB.lblUsefulValue3.textCopyable=true;
			frmAccountDetailsMB.lblUsefulValue1.text = productName;
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("remFreeTran");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("AccountType");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountNo");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("AccountName");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("BranchName");
			//frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("LedgerBalance");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("Status");
			if(gblAccountStatus!=null){
				var status=gblAccountStatus.split("|")
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue6.text =status[0];
				else
				frmAccountDetailsMB.lblUsefulValue6.text =status[1];
			}
		} else if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName ==
			"PRODUCT_CODE_SAVINGCARE" || glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
			var locale = kony.i18n.getCurrentLocale();
			frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
		   	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
			var branchName;
			var accNickName;
			var productName;
			var accType;
			if (locale == "en_US") {
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
			} else {
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			kony.print("GOWRI123 lblAccountNameHeader="+gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]);
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			if (gblAccountTable["custAcctRec"][gblIndex]["accType"] == kony.i18n.getLocalizedString("Current"))
				frmAccountDetailsMB.lblUsefulValue2.text = accType;
			else
			frmAccountDetailsMB.lblUsefulValue2.text = accType;
			frmAccountDetailsMB.lblUsefulValue2.textCopyable=true;
			frmAccountDetailsMB.lblUsefulValue1.text = productName;
			frmAccountDetailsMB.lblUsefulValue6.text = branchName;
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("Status");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("AccountType");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("AccountNo");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("BranchName");
			//frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("LedgerBalance");
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			if(gblAccountStatus!=null){
				var status=gblAccountStatus.split("|")
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue5.text =status[0];
				else
				frmAccountDetailsMB.lblUsefulValue5.text =status[1];
			}
			if(gblSavingsCareFlow=="AccountDetailsFlow"){
				kony.print("Saving care flow");
				frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(true);
				frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(true);
				frmAccountDetailsMB.btnEditBeneficiary.text=kony.i18n.getLocalizedString("editBenefit");
				handleSavingCareMBDetails(frmAccountDetailsMB);			
			}
		} else if (glb_viewName == "PRODUCT_CODE_CREDITCARD_TABLE") {
			frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString('keyMBCreditlimit');
			frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
			//frmAccountDetailsMB.image24750595786995.setVisibility(false);
			//frmAccountStatementMB.hbxMyactivities.setVisibility(false);
			//frmAccountDetailsMB.lnkMyActivities.setVisibility(false);
			var locale = kony.i18n.getCurrentLocale();
			var accNickName;
			if (locale == "en_US") {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				
			} else {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
			frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
			frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
			
			//var allowRedeemPoints = gblAccountTable["custAcctRec"][gblIndex]["allowRedeemPoints"];
				var allowCardTypeForRedeem = gblAccountTable["custAcctRec"][gblIndex]["allowCardTypeForRedeem"];		
			if (allowCardTypeForRedeem == "1") {
				frmAccountDetailsMB.hbxUseful14.setVisibility(true);
				frmAccountDetailsMB.hbxUseful15.setVisibility(true);		
				frmAccountDetailsMB.lblUseful14.text = kony.i18n.getLocalizedString("keyMBAvailable");			
				frmAccountDetailsMB.lblUsefulValue14.text = frmAccountDetailsMB.lblUsefulValue14.text.split(" ")[0];
				frmAccountDetailsMB.lblUseful15.text = kony.i18n.getLocalizedString("keyPointExpiringOn") + " " + reformatDate(gblAccountTable["custAcctRec"][gblIndex]["bonusPointExpiryDate"]) + ": ";
				frmAccountDetailsMB.lblUsefulValue15.text = frmAccountDetailsMB.lblUsefulValue15.text.split(" ")[0];
				//if(!flowSpa)frmAccountDetailsMB.lnkRedeemPoint.text = kony.i18n.getLocalizedString("keyPointRedemption");
			}			
					
			
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");		
			if(!flowSpa){
				//frmAccountDetailsMB.lnkApplySoGooOD.text = kony.i18n.getLocalizedString("keyApplySoGooODLink");
				//frmAccountDetailsMB.lnkRedeemPoint.text = kony.i18n.getLocalizedString("keyPointRedemption");
			}	
		} else if (glb_viewName == "PRODUCT_CODE_OLDREADYCASH_TABLE") {
			frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
		   	frmAccountDetailsMB.image24750595786995.setVisibility(false);
			frmAccountStatementMB.hbxMyactivities.setVisibility(false);
			frmAccountDetailsMB.lnkMyActivities.setVisibility(false);
			var locale = kony.i18n.getCurrentLocale();
			var accNickName;
			if (locale == "en_US") {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				
			} else {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
			frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
			frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
		} else if (glb_viewName == "PRODUCT_CODE_TD_TABLE") {
			frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
		  	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
			var locale = kony.i18n.getCurrentLocale();
			var branchName;
			var productName;
			var accNickName;
			var accType;
			if (locale == "en_US") {
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameEh"];
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
			} else {
				branchName = gblAccountTable["custAcctRec"][gblIndex]["BranchNameTh"];
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
				accType=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			frmAccountDetailsMB.lblUsefulValue1.text = productName;
			frmAccountDetailsMB.lblUsefulValue3.text = accType;
			frmAccountDetailsMB.lblUsefulValue3.textCopyable=true;
			frmAccountDetailsMB.lblUsefulValue7.text = branchName;
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("AccountType");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountNo");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("AccountName");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("BranchName");
			//frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("LedgerBalance");
			frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("LinkedAcc");
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("tenor");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("Status");
			frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
			frmAccountDetailsMB.flexBenefiList.setVisibility(false);
			frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(true);
			frmAccountDetailsMB.flexMaturityDisplay.setVisibility(true)
			frmAccountDetailsMB.lblDateH.text = kony.i18n.getLocalizedString("MatDate");
			frmAccountDetailsMB.lblBalanceH.text = kony.i18n.getLocalizedString("Balance");
			frmAccountDetailsMB.lblTransactionH.text = kony.i18n.getLocalizedString("InterestRate");
			frmAccountDetailsMB.lblDepositDetails.text = kony.i18n.getLocalizedString("DepDetails");
			if(gblAccountStatus!=null){
				var status=gblAccountStatus.split("|")
				if (locale == "en_US")
				frmAccountDetailsMB.lblUsefulValue6.text =status[0];
				else
				frmAccountDetailsMB.lblUsefulValue6.text =status[1];
			}
		} else if (glb_viewName == "PRODUCT_CODE_LOAN_CASH2GO") {
			frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
			//frmAccountDetailsMB.image24750595786995.setVisibility(false);
			//frmAccountStatementMB.hbxMyactivities.setVisibility(false);
			//frmAccountDetailsMB.lnkMyActivities.setVisibility(false);
			var locale = kony.i18n.getCurrentLocale();
			var productName;
			var accNickName;
			if (locale == "en_US"){
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				}
			else{
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
				}
			frmAccountDetailsMB.lblUsefulValue1.text = productName;
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("AccountNo");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("SuffixNo");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("PayDueDate");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("MonthlyInstallement");
			frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
		} else if (glb_viewName == "PRODUCT_CODE_LOAN_HOMELOAN") {
			frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
			//frmAccountDetailsMB.image24750595786995.setVisibility(false);
			//frmAccountDetailsMB.lnkMyActivities.setVisibility(false);
			//frmAccountStatementMB.hbxMyactivities.setVisibility(false);
			var locale = kony.i18n.getCurrentLocale();
			var productName;
			var accNickName;
			if (locale == "en_US"){
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				}
			else{
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
				}
			frmAccountDetailsMB.lblUsefulValue1.text = productName;
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("AccountNo");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("SuffixNo");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("PayDueDate");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("MonthlyInstallement");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("DirectCredit");
			frmAccountDetailsMB.lblDateH.text = kony.i18n.getLocalizedString("keyLoanLimit");
			frmAccountDetailsMB.lblTransactionH.text = kony.i18n.getLocalizedString('InterestRate');
			frmAccountDetailsMB.lblBalanceH.text = kony.i18n.getLocalizedString("keyHomeLoanExpiryDate");
			frmAccountDetailsMB.lblDepositDetails.text = kony.i18n.getLocalizedString("interestRateDetail");
		} else if (glb_viewName == "PRODUCT_CODE_READYCASH_TABLE") {
			frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString('keyMBCreditlimit');
			frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
			//frmAccountDetailsMB.image24750595786995.setVisibility(false);
			//frmAccountDetailsMB.lnkMyActivities.setVisibility(false);
			//frmAccountStatementMB.hbxMyactivities.setVisibility(false);
			var locale = kony.i18n.getCurrentLocale();
			var accNickName;
			if (locale == "en_US") {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"];
				accNickName= gblAccountTable["custAcctRec"][gblIndex]["ProductNameEng"]+" "+sbStr;
				
			} else {
				productName = gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"];
				accNickName=gblAccountTable["custAcctRec"][gblIndex]["ProductNameThai"]+" "+sbStr;
			}
			if(null !=gblAccountTable["custAcctRec"][gblIndex]["acctNickName"]){
			frmAccountDetailsMB.lblAccountNameHeader.text =gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
			}
			else{
			 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
			}
			frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
			frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
			frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
			frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
			frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
			frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
			frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
			frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
			frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
			//frmAccountDetailsMB.BtnTopUp.text = kony.i18n.getLocalizedString("TopUp");
			//frmAccountDetailsMB.btnTransfer.text = kony.i18n.getLocalizedString("Transfer");
			//frmAccountDetailsMB.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyCredit.text=kony.i18n.getLocalizedString("PayBill");
			//frmAccountDetailsMB.payMyLoan.text=kony.i18n.getLocalizedString("PayBill");
			
		}
		gblLocale = false;
		}
	}catch (err) {
		//alert(typeof err);
	}
}

function frmMyRecipientAddAccPreShow() {
changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientAddAcc.id))){
	//frmMyRecipientAddAcc.label474136033132882.text = kony.i18n.getLocalizedString('keyBank')
	frmMyRecipientAddAcc.lblHdrTxt.text = kony.i18n.getLocalizedString('keylblAddAccount')
	frmMyRecipientAddAcc.label4733076528859.text = kony.i18n.getLocalizedString('AccountNo')
	frmMyRecipientAddAcc.label4733076528885.text = kony.i18n.getLocalizedString('Nickname')
	frmMyRecipientAddAcc.button4733076528913.text = kony.i18n.getLocalizedString('keyCancelButton')
	frmMyRecipientAddAcc.button4733076528925.text = kony.i18n.getLocalizedString('Next')
	if(frmMyRecipientAddAcc.btnBanklist.text == kony.i18n.getLocalizedString("keyBank")){
		frmMyRecipientAddAcc.btnBanklist.text=kony.i18n.getLocalizedString("keyBank");
	}
	LocaleController.updatedForm(frmMyRecipientAddAcc.id);
   }
	frmMyRecipientAddAcc.btnBanklist.setVisibility(true);
	gblLocale = false;
}

function frmMyRecipientAddAccConfPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientAddAccConf.id))){
	frmMyRecipientAddAccConf.lblHdrTxt.text = kony.i18n.getLocalizedString('keyConfirmation')
	frmMyRecipientAddAccConf.lblMobileNo.text = kony.i18n.getLocalizedString('keyMobileNumber')
	frmMyRecipientAddAccConf.lblEmailId.text = kony.i18n.getLocalizedString('keyEmail')
	frmMyRecipientAddAccConf.lblFbIdstudio6.text = kony.i18n.getLocalizedString('keyFacebookID')//Modified by Studio Viz
	//frmMyRecipientAddAccConf.imgprofilepic.src = frmMyRecipientAddProfile.imgProfilePic.src 
	//frmMyRecipientAddAccConf.lblAccount.text = kony.i18n.getLocalizedString('keyFacebookID')
	for(var i=0;i<AddAccountList.length;i++)
		  {  
			  AddAccountList[i]["lblNickName"]= kony.i18n.getLocalizedString('Nickname');
			  AddAccountList[i]["lblNumber"]= kony.i18n.getLocalizedString('keyNumber');
			  AddAccountList[i]["lblBankName"]=getBankNameOfcurrentLocale(AddAccountList[i]["bankCode"]);
		 }
   //myRecAccAddConfLoad();
	frmMyRecipientAddAccConf.lblAccount.text=kony.i18n.getLocalizedString("Receipent_Account"); 
    frmMyRecipientAddAccConf.segMyRecipientDetail.setData(AddAccountList);
	if(flowSpa){
	frmMyRecipientAddAccConf.btnCancelSpa.text = kony.i18n.getLocalizedString('keyCancelButton')
	frmMyRecipientAddAccConf.btnAgreeSpa.text = kony.i18n.getLocalizedString('keyConfirm')
	}
	else{
	frmMyRecipientAddAccConf.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton')
	frmMyRecipientAddAccConf.btnAgree.text = kony.i18n.getLocalizedString('keyConfirm')
	}
	LocaleController.updatedForm(frmMyRecipientAddAccConf.id);
   }
	gblLocale = false;
}

function frmMyRecipientAddAccCompletePreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientAddAccComplete.id))){
		frmMyRecipientAddAccComplete.lblHdrTxt.text = kony.i18n.getLocalizedString('keylblComplete')
		frmMyRecipientAddAccComplete.label47502979411852.text = kony.i18n.getLocalizedString('keyMobileNumber')
		frmMyRecipientAddAccComplete.label47502979412689.text = kony.i18n.getLocalizedString('keyEmail')
		frmMyRecipientAddAccComplete.label47502979413736.text = kony.i18n.getLocalizedString('keyFacebookID')
		for(var i=0;i<AddAccountList.length;i++)
			  {  
				  AddAccountList[0]["lblNickName"]= kony.i18n.getLocalizedString('Nickname');
				  AddAccountList[0]["lblNumber"]= kony.i18n.getLocalizedString('keyNumber');
				  AddAccountList[0]["lblBankName"]=getBankNameOfcurrentLocale(AddAccountList[0]["bankCode"]);
			 }
	   //myRecAccAddConfLoad();
	    frmMyRecipientAddAccComplete.segMyRecipientDetail.setData(AddAccountList);
		frmMyRecipientAddAccComplete.lblAccounts.text=kony.i18n.getLocalizedString("Receipent_Account"); 
		if(flowSpa){
			frmMyRecipientAddAccComplete.btnCancelSpa.text = kony.i18n.getLocalizedString('keyReturn')
			frmMyRecipientAddAccComplete.btnAgreeSpa.text = kony.i18n.getLocalizedString('keyAddMoreRecipient')
		}else{
			frmMyRecipientAddAccComplete.btnCancel.text = kony.i18n.getLocalizedString('keyReturn')
			frmMyRecipientAddAccComplete.btnAgree.text = kony.i18n.getLocalizedString('keyAddMoreRecipient')
		}
		LocaleController.updatedForm(frmMyRecipientAddAccComplete.id);
    }
	gblLocale = false;
	ChangeCampaignLocale();
	frmMyRecipientAddAccComplete.btnAgree.text = kony.i18n.getLocalizedString('keyAddMoreRecipient');
	//code for personalized banner
	try{
		frmMyRecipientAddAccComplete.hbxAdv.setVisibility(false);
		frmMyRecipientAddAccComplete.image2447443295186.src="";
    	frmMyRecipientAddAccComplete.gblBrwCmpObject.handleRequest="";
    	frmMyRecipientAddAccComplete.gblBrwCmpObject.htmlString="";
    	frmMyRecipientAddAccComplete.gblVbxCmp.remove(gblBrwCmpObject);
       	frmMyRecipientAddAccComplete.hbxAdv.remove(gblVbxCmp);
    }
    catch(e)
    {
    }
}

function frmMyRecipientAddProfilePreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientAddProfile.id))){
	frmMyRecipientAddProfile.label4733076529431.text = kony.i18n.getLocalizedString('keyMobileNumber')
	frmMyRecipientAddProfile.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyRecipient')
	frmMyRecipientAddProfile.label4733076529432.text = kony.i18n.getLocalizedString('Receipent_Email')
	frmMyRecipientAddProfile.tbxRecipientName.placeholder = kony.i18n.getLocalizedString('Receipent_ReceipentName');
	if(flowSpa){
	frmMyRecipientAddProfile.lnkAddBankAcc.text = kony.i18n.getLocalizedString('keyAddBankAccount')
	}
	else{
	frmMyRecipientAddProfile.link59324032018947.text = kony.i18n.getLocalizedString('Recpent_AddBankAccount')
	}
	frmMyRecipientAddProfile.btnNext.text = kony.i18n.getLocalizedString('Next')
	LocaleController.updatedForm(frmMyRecipientAddProfile.id);
   }
	gblLocale = false;
}

function frmMyRecipientAddProfileCompPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientAddProfileComp.id))){
	frmMyRecipientAddProfileComp.lblHdrTxt.text = kony.i18n.getLocalizedString('Complete')
	frmMyRecipientAddProfileComp.lblRecipientName.text = kony.i18n.getLocalizedString('keyRecipientName')
	frmMyRecipientAddProfileComp.lblNumber.text = kony.i18n.getLocalizedString('keyNumber')
	frmMyRecipientAddProfileComp.lblEmail.text = kony.i18n.getLocalizedString('keyEmail')
	frmMyRecipientAddProfileComp.lblfbIDstudio7.text = kony.i18n.getLocalizedString('keyFacebookID')//Modified by Studio Viz
	frmMyRecipientAddProfileComp.button101086657957077.text = kony.i18n.getLocalizedString('Back')
	LocaleController.updatedForm(frmMyRecipientAddProfileComp.id);
   }
	gblLocale = false;
}

function frmMyRecipientEditAccountPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientEditAccount.id))){
	frmMyRecipientEditAccount.lblHdrTxt.text = kony.i18n.getLocalizedString('keyRecipientEditAccount');
	frmMyRecipientEditAccount.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
	frmMyRecipientEditAccount.btnAgree.text = kony.i18n.getLocalizedString('keysave');
	frmMyRecipientEditAccount.lblAccNN.text= kony.i18n.getLocalizedString('keyAccountNickNameMB');
	frmMyRecipientEditAccount.lblAccountName.text=kony.i18n.getLocalizedString('AccountName');	
	LocaleController.updatedForm(frmMyRecipientEditAccount.id);
   }
	gblLocale = false;
}

function frmMyRecipientEditProfilePreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientEditProfile.id))){
	frmMyRecipientEditProfile.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyRecipient')
	frmMyRecipientEditProfile.label474136033103563.text = kony.i18n.getLocalizedString('keyFacebookID')
	//frmMyRecipientEditProfile.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton')
	frmMyRecipientEditProfile.button447478018365122.text = kony.i18n.getLocalizedString('keysave')
	frmMyRecipientEditProfile.btnCancel.text=kony.i18n.getLocalizedString('keyCancelButton')
	frmMyRecipientEditProfile.label4733076529431.text=kony.i18n.getLocalizedString('keyMobileNumber')
	frmMyRecipientEditProfile.label4733076529432.text=kony.i18n.getLocalizedString('keyEmail')
	frmMyRecipientEditProfile.label474136033103563.text=kony.i18n.getLocalizedString('keyFacebookID')
	LocaleController.updatedForm(frmMyRecipientEditProfile.id);
   }
	gblLocale = false;
}

function frmMyRecipientEditProfileCompPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientEditProfileComp.id))){
	frmMyRecipientEditProfileComp.lblHdrTxt.text = kony.i18n.getLocalizedString('Complete')
	frmMyRecipientEditProfileComp.lblRecipientName.text = kony.i18n.getLocalizedString('keyRecipientName')
	frmMyRecipientEditProfileComp.lblNumber.text = kony.i18n.getLocalizedString('keyNumber')
	frmMyRecipientEditProfileComp.lblEmail.text = kony.i18n.getLocalizedString('keyEmail')
	frmMyRecipientEditProfileComp.lblfbID.text = kony.i18n.getLocalizedString('keyFacebookID')
	frmMyRecipientEditProfileComp.button101086657957077.text = kony.i18n.getLocalizedString('Back')
    LocaleController.updatedForm(frmMyRecipientEditProfileComp.id);
   }
	gblLocale = false;
}

function frmMyRecipientsPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipients.id))){
	frmMyRecipients.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyRecipients')	
	frmMyRecipients.textbox247327209467957.placeholder=kony.i18n.getLocalizedString('keySearch');
	LocaleController.updatedForm(frmMyRecipients.id);
   }
	frmMyRecipients.textbox247327209467957.text = "";
	gblLocale = false;
}

function frmMyRecipientDetailPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientDetail.id))){
	frmMyRecipientDetail.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyRecipients')
	frmMyRecipientDetail.label47502979411852.text = kony.i18n.getLocalizedString('keyMobileNumber')
	frmMyRecipientDetail.label47502979412689.text = kony.i18n.getLocalizedString('keyEmail')
	frmMyRecipientDetail.lblFBstudio4.text = kony.i18n.getLocalizedString('keyFacebookID')//Modified by Studio Viz
	if(flowSpa){
	frmMyRecipientDetail.button474076451737588.text = kony.i18n.getLocalizedString('Back')
	}else{
	frmMyRecipientDetail.button101086657957077.text = kony.i18n.getLocalizedString('Back')
	}
	LocaleController.updatedForm(frmMyRecipientDetail.id);
   }
	gblLocale = false;
}

function frmMyRecipientSelectContactsPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientSelectContacts.id))){
	frmMyRecipientSelectContacts.lblHdrTxt.text = kony.i18n.getLocalizedString('keySelectRecipients')
	frmMyRecipientSelectContacts.btnNext.text = kony.i18n.getLocalizedString('Next');
	frmMyRecipientSelectContacts.btnReturn.text = kony.i18n.getLocalizedString('keyReturn');
	frmMyRecipientSelectContacts.textbox286685406430472.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmMyRecipientSelectContacts.label475095112172022.text=kony.i18n.getLocalizedString("NoContacts");
    LocaleController.updatedForm(frmMyRecipientSelectContacts.id);
   }
} 

function frmMyRecipientSelectContactsConfPreShow() {   
	changeStatusBarColor();
	frmMyRecipientSelectContacts.lblHdrTxt.text = kony.i18n.getLocalizedString('keyConfirmation')
    frmMyRecipientSelectContactsConf.button4733076529054.text = kony.i18n.getLocalizedString('keyCancelButton')
	frmMyRecipientSelectContactsConf.button4733076529055.text = kony.i18n.getLocalizedString('keyConfirm')
}

function frmMyRecipientSelectMobilePreShow() {
	changeStatusBarColor();
	//frmMyRecipientSelectContacts.lblHdrTxt.text = kony.i18n.getLocalizedString('keyConfirmation')
	frmMyRecipientSelectContacts.btnNext.text = kony.i18n.getLocalizedString('Next')
	frmMyRecipientSelectContacts.btnReturn.text = kony.i18n.getLocalizedString('keyReturn')
}

function frmMyRecipientEditAccCompletePreShow() {
	changeStatusBarColor();
	//frmMyRecipientEditAccComplete.lblBank.text=kony.i18n.getLocalizedString('keyBank')
	frmMyRecipientEditAccComplete.lblNickName.text = kony.i18n.getLocalizedString('keyNickname');
	frmMyRecipientEditAccComplete.lblHdrTxt.text=kony.i18n.getLocalizedString('keyMyRecipientAccount');
	frmMyRecipientEditAccComplete.lblNumber.text = kony.i18n.getLocalizedString('Receipent_Number');
	frmMyRecipientEditAccComplete.lblAccountName.text = kony.i18n.getLocalizedString('AccountName');
	frmMyRecipientEditAccComplete.button101086657957077.text = kony.i18n.getLocalizedString('Back');
	var bankName=getBankNameOfcurrentLocale(frmMyRecipientViewAccount.lblBankCD.text);
	//if(frmMyRecipientViewAccount.lblBankCD.text==gblTMBBankCD)
	frmMyRecipientEditAccComplete.hbox47327209476200.setVisibility(true);
	//else
	//frmMyRecipientEditAccComplete.hbox47327209476200.setVisibility(false);
	frmMyRecipientEditAccComplete.lblBankName.text = bankName;
}

function frmMyRecipientSelectContactsCompPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientSelectContactsComp.id))){
	frmMyRecipientSelectContactsComp.lblHdrTxt.text = kony.i18n.getLocalizedString('Complete')
	//frmMyRecipientSelectContactsComp.lblMsg.text = kony.i18n.getLocalizedString('ATMmsg')
	frmMyRecipientSelectContactsComp.button4733076529054.text = kony.i18n.getLocalizedString('keyReturn')
	frmMyRecipientSelectContactsComp.button4733076529055.text = kony.i18n.getLocalizedString('keyAddMore')
	LocaleController.updatedForm(frmMyRecipientSelectContactsComp.id);
   }
	gblLocale = false;
	ChangeCampaignLocale();
	
	//code for personalized banner display
	try{
	   	frmMyRecipientSelectContactsComp.hbxAdv.setVisibility(false);
	   	frmMyRecipientSelectContactsComp.image2447443295186.src="";
	   	frmMyRecipientSelectContactsComp.gblBrwCmpObject.handleRequest="";
	   	frmMyRecipientSelectContactsComp.gblBrwCmpObject.htmlString="";
	   	frmMyRecipientSelectContactsComp.gblVbxCmp.remove(gblBrwCmpObject);
       	frmMyRecipientSelectContactsComp.hbxAdv.remove(gblVbxCmp);
	}
	catch(e)
	{
	}
	
}

function frmMyRecipientViewAccountPreShow() {
changeStatusBarColor();
		frmMyRecipientViewAccount.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyRecipientAccount');
		frmMyRecipientViewAccount.lblNumber.text = kony.i18n.getLocalizedString('Receipent_Number');
		frmMyRecipientViewAccount.lblNickName.text = kony.i18n.getLocalizedString('Nickname');
		frmMyRecipientViewAccount.lblAccountName.text = kony.i18n.getLocalizedString('AccountName');
		frmMyRecipientViewAccount.button101086657957077.text = kony.i18n.getLocalizedString('Back');
		viewAccountShow();
	}

function frmMyTopUpListPreShow() {
changeStatusBarColor();
	frmMyTopUpList.btnSelectCat.text = kony.i18n.getLocalizedString('keyBillPaymentSelectCategory');
	frmMyTopUpList.txbSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmMyTopUpList.lblSuggestedBillersText.text = kony.i18n.getLocalizedString('keySuggestedBillers');
	frmMyTopUpList.lblErrorMsg.text=kony.i18n.getLocalizedString('keybillernotfound');
	frmMyTopUpList.label44934994593275.text = kony.i18n.getLocalizedString("More");
	frmMyTopUpList.label44934994593185.text = kony.i18n.getLocalizedString("More");
	frmMyTopUpList.lblHide.text = kony.i18n.getLocalizedString("More");
	if (gblMyBillerTopUpBB == 0) {
		frmMyTopUpList.lblMyBillers.text = kony.i18n.getLocalizedString('KeyMyBillsMB');
		frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyMyBillsMB');
		frmMyTopUpList.lblNoBill.text = kony.i18n.getLocalizedString('keyaddbillerstolist');
	} else if (gblMyBillerTopUpBB == 1) {
		frmMyTopUpList.lblMyBillers.text = kony.i18n.getLocalizedString('myTopUpsMB');
		frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('myTopUpsMB');
		frmMyTopUpList.lblNoBill.text = kony.i18n.getLocalizedString('keyNoTopUps');		
	} else if (gblMyBillerTopUpBB == 2) {
		frmMyTopUpList.lblMyBillers.text = kony.i18n.getLocalizedString('keyMyBills');
		frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBB');
	}
	
	gblLocale = false;
}

function frmBBListPreshow() {
	changeStatusBarColor();
	frmMyTopUpList.btnSelectCat.text = kony.i18n.getLocalizedString('keyBillPaymentSelectCategory');
	frmMyTopUpList.lblSuggestedBillersText.text = kony.i18n.getLocalizedString('keySuggestedBillers');
	frmMyTopUpList.lblMyBillers.text = kony.i18n.getLocalizedString('keyMyBills');
	frmMyTopUpList.txbSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	frmMyTopUpList.lblNoBill.text = kony.i18n.getLocalizedString("keyaddbillerstolist");
	frmMyTopUpList.label44934994593275.text = kony.i18n.getLocalizedString("More");
	frmMyTopUpList.label44934994593185.text = kony.i18n.getLocalizedString("More");
		
	frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBB');
	gblLocale = false;
}

function frmBBApplyNowPreshow() {
changeStatusBarColor();
	//frmBBApplyNow.lblTandC.text = kony.i18n.getLocalizedString("keyIBActivationTnC");
	var currentLocale = kony.i18n.getCurrentLocale();
	if(!(LocaleController.isFormUpdatedWithLocale(frmBBApplyNow.id))){
	frmBBApplyNow.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyApplyBeep')
	frmBBApplyNow.btnEngR.text = kony.i18n.getLocalizedString('languageEng')
	frmBBApplyNow.btnThaiR.text = kony.i18n.getLocalizedString('languageThai')
	if (flowSpa) {
		frmBBApplyNow.btnCancelSpa.text = kony.i18n.getLocalizedString('keyCancelButton')
		frmBBApplyNow.btnAgreeSpa.text = kony.i18n.getLocalizedString('keyApplyNow')
	} else {
		frmBBApplyNow.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton')
		frmBBApplyNow.btnAgree.text = kony.i18n.getLocalizedString('keyApplyNow')
	}
	LocaleController.updatedForm(frmBBApplyNow.id);
    }
	if (currentLocale == "en_US") {
		frmBBApplyNow.btnEngR.skin = "btnOnFocus";
		frmBBApplyNow.btnThaiR.skin = "btnOffFocus";
	} else {
		frmBBApplyNow.btnEngR.skin = "btnOnNormal";
		frmBBApplyNow.btnThaiR.skin = "btnOffNorm";
	}
	//gblLocale = false;
	gblLocale = false;
}

function frmBBConfirmAndCompletePreshow() {
changeStatusBarColor();
    if(!(LocaleController.isFormUpdatedWithLocale(frmBBConfirmAndComplete.id))){
	frmBBConfirmAndComplete.lblBalance.text = kony.i18n.getLocalizedString("Balance");
	//frmBBConfirmAndComplete.lblMsg.text = kony.i18n.getLocalizedString("ATMmsg");
	/*if(flowSpa){
	 frmBBConfirmAndComplete.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmBBConfirmAndComplete.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
	}else{
    frmBBConfirmAndComplete.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmBBConfirmAndComplete.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	}
	frmBBConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");*/
	//frmBBConfirmAndComplete.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
	//frmBBConfirmAndComplete.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
    LocaleController.updatedForm(frmBBConfirmAndComplete.id);
    }
	gblLocale = false;
	ChangeCampaignLocale();
}



function frmBBMyBeepAndBillPreshow() {
	changeStatusBarColor();
	frmBBMyBeepAndBill.lblHdrTxt.text = kony.i18n.getLocalizedString("keyMyBB");
	frmBBMyBeepAndBill.lblSaveTo.text = kony.i18n.getLocalizedString("keyBB");
	if(flowSpa)
	{
		frmBBMyBeepAndBill.btnCancelBBBackSpa.text = kony.i18n.getLocalizedString("Back");
	}
	else
	{
		frmBBMyBeepAndBill.btnCancelBBBack.text = kony.i18n.getLocalizedString("Back");
	}
	frmBBMyBeepAndBill.lblAcc.text = kony.i18n.getLocalizedString("AccountNo");
	frmBBMyBeepAndBill.lblStatus.text = kony.i18n.getLocalizedString("Status");
	//frmBBMyBeepAndBill.lblRef1.text = kony.i18n.getLocalizedString("keyRef1");
	//frmBBMyBeepAndBill.lblRef2.text = kony.i18n.getLocalizedString("keyRef2");
	gblLocale = false;
}

function frmViewTopUpBillerPreShow() {
	changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	
	if (locale == "en_US") {
		frmViewTopUpBiller.lblConfrmRef1.text = gblCurRef1LblEN;
		frmViewTopUpBiller.lblCnfrmBillerNameComp.text = gblBillerCompCodeEN;
	}else{
		frmViewTopUpBiller.lblConfrmRef1.text = gblCurRef1LblTH;
		frmViewTopUpBiller.lblCnfrmBillerNameComp.text = gblBillerCompCodeTH;
	}
		
	if (gblMyBillerTopUpBB == 0) {
		frmViewTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyMyBillMB');
		if (locale == "en_US") {
			frmViewTopUpBiller.lblConfrmRef2.text = gblCurRef2LblEN;
		}else{
			frmViewTopUpBiller.lblConfrmRef2.text = gblCurRef2LblTH;
		}
	} else {
		frmViewTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('myTopUpMB');
		
	}
	//frmViewTopUpBiller.lblConfrmRef1.text = kony.i18n.getLocalizedString('keyRef1');
	//frmViewTopUpBiller.lblConfrmRef2.text = kony.i18n.getLocalizedString('keyRef2')
	frmViewTopUpBiller.btnBack.text = kony.i18n.getLocalizedString('Back');
	gblLocale = false;
}

function frmMyTopUpEditScreensPreshow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyTopUpEditScreens.id))){
	if (gblMyBillerTopUpBB == 0) {
		frmMyTopUpEditScreens.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyMyBillMB');
	} else {
		frmMyTopUpEditScreens.lblHdrTxt.text = kony.i18n.getLocalizedString('myTopUpMB');
	}
	frmMyTopUpEditScreens.label101003159684553.text = kony.i18n.getLocalizedString('billerNicknameMB');
	frmMyTopUpEditScreens.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
	frmMyTopUpEditScreens.btnAgree.text = kony.i18n.getLocalizedString('keysave');
    LocaleController.updatedForm(frmMyTopUpEditScreens.id);
   } 
	frmMyTopUpEditScreens.txtEditName.text = frmViewTopUpBiller.lblCnfrmNickName.text;
	gblLocale = false;
}

function frmAddTopUpBillerPreShow() {
   //if(!(LocaleController.isFormUpdatedWithLocale(frmAddTopUpBiller.id))){
	if (gblMyBillerTopUpBB == 0) {
		frmAddTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('keyAddBillMB');
	} else if (gblMyBillerTopUpBB == 1) {
		frmAddTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('addtopUpMB');
	} else if (gblMyBillerTopUpBB == 2) {
		frmAddTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBBPayment');
	}
	frmAddTopUpBiller.lblBiller.text = kony.i18n.getLocalizedString('billerMB');
	frmAddTopUpBiller.lblNickName.text = kony.i18n.getLocalizedString('keyNickNameIB');
	frmAddTopUpBiller.lblrefNo.text = kony.i18n.getLocalizedString('keyRef1');
	frmAddTopUpBiller.lblrefNo2.text = kony.i18n.getLocalizedString('keyRef2');
	frmAddTopUpBiller.btnNext.text = kony.i18n.getLocalizedString('Next');
    //LocaleController.updatedForm(frmAddTopUpBiller.id);
//  } 
    if (gblMyBillerTopUpBB == 0) {
    	frmAddTopUpBiller.hbxSegMain.isVisible = false;
    }else if (gblMyBillerTopUpBB == 1) {
    	frmAddTopUpBiller.hbxSegMain.isVisible = false;
    }else if (gblMyBillerTopUpBB == 2) {
    	frmAddTopUpBiller.hbxSegMain.isVisible = true;
    }
	gblLocale = false;
}

function frmMyTopUpSelectPreshow() {
changeStatusBarColor();
	if (gblMyBillerTopUpBB == 0) {
		frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('keyBillPaymentSelectBill');
	} else if (gblMyBillerTopUpBB == 1) {
		frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('SelectTopUp');
	} else if (gblMyBillerTopUpBB == 2) {
		frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBB');
	}
	frmMyTopUpSelect.btnSelectCat.text = kony.i18n.getLocalizedString('keyBillPaymentSelectCategory');
	frmMyTopUpSelect.lnkMore.text = kony.i18n.getLocalizedString('More');
	frmMyTopUpSelect.txbSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	gblLocale = false;
}

function frmAddTopUpToMBPreShow() {
changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	
	
	if (gblMyBillerTopUpBB == 0) {
		frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyAddBillMB');
		if (locale == "en_US") {
			frmAddTopUpToMB.lblAddedRef2.text = gblCurRef2LblEN;
		}else{
			frmAddTopUpToMB.lblAddedRef2.text = gblCurRef2LblTH;
		}
	} else if (gblMyBillerTopUpBB == 1) {
		frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('addtopUpMB');
	} else if (gblMyBillerTopUpBB == 2) {
		frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyBBApplyPayment');
	}
	frmAddTopUpToMB.lblBiller.text = kony.i18n.getLocalizedString('billerMB');
	frmAddTopUpToMB.lblNickname.text = kony.i18n.getLocalizedString('keyNickNameIB');
	
	if (locale == "en_US") {
	    if(gblCurRef1LblEN == "Mobile Number 10 Digit"){
	     //alert("in ENG ifff")
	     frmAddTopUpToMB.lblAddedRef1.text = gblCurRef1LblEN+":";
	    }else{
	     frmAddTopUpToMB.lblAddedRef1.text = gblCurRef1LblEN;
        }
		frmAddTopUpToMB.lblAddbillerName.text = gblBillerCompCodeEN;
	}else{
	    if(gblCurRef1LblTH == "หมายเลขโทรศัพท์มือถือ 10 หลัก"){
	      //alert("in Thai ifff")
	      frmAddTopUpToMB.lblAddedRef1.text = gblCurRef1LblTH+":";
	    }else{
	      frmAddTopUpToMB.lblAddedRef1.text = gblCurRef1LblTH;
	    }
		
		frmAddTopUpToMB.lblAddbillerName.text = gblBillerCompCodeTH;
	}
	if((gblCurRef1LblEN.trim()=="")&&gblCurRef1LblTH.trim()=="")
	{
	frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString('keyRef1');
	}
	//if((gblCurRef2LblEN.trim()=="")&&gblCurRef2LblTH.trim()==""){
	  frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString('keyRef2');
	
	//}
	
	
	if(flowSpa){
		frmAddTopUpToMB.btnSpaNext.text = kony.i18n.getLocalizedString('Next');
	} else {
		frmAddTopUpToMB.button15633509701481.text = kony.i18n.getLocalizedString('Next');
	}
	gblLocale = false;
}

function frmAddTopUpBillerconfrmtnPreshow() {
changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmAddTopUpBillerconfrmtn.id))){
	frmAddTopUpBillerconfrmtn.lblHdrTxt.text = kony.i18n.getLocalizedString('Confirmation');
	
	if(flowSpa){
		frmAddTopUpBillerconfrmtn.btnAgreeSpa.text = kony.i18n.getLocalizedString('keyConfirm');
		frmAddTopUpBillerconfrmtn.btnCancelSpa.text = kony.i18n.getLocalizedString('keyCancelButton');
	} else {
	frmAddTopUpBillerconfrmtn.btnAgree.text = kony.i18n.getLocalizedString('keyConfirm');
	frmAddTopUpBillerconfrmtn.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
	}
   LocaleController.updatedForm(frmAddTopUpBillerconfrmtn.id);
  }
	gblLocale = false;
}


function ehFrmMyTopUpComplete_frmMyTopUpComplete_preshow() {
changeStatusBarColor();
      if(!(LocaleController.isFormUpdatedWithLocale(frmMyTopUpComplete.id))){
      frmMyTopUpComplete.lblHdrTxt.text = kony.i18n.getLocalizedString('Complete');
      if(flowSpa){
      frmMyTopUpComplete.btnCancelSpa.text = kony.i18n.getLocalizedString('keyReturn');
      frmMyTopUpComplete.btnAgreeSpa.text = kony.i18n.getLocalizedString('keybtnAddMore');
      }else{
      frmMyTopUpComplete.btnCancel.text = kony.i18n.getLocalizedString('keyReturn');
      frmMyTopUpComplete.btnAgree.text = kony.i18n.getLocalizedString('keybtnAddMore');
      }
      LocaleController.updatedForm(frmMyTopUpComplete.id);
   }
      gblLocale = false;
      //ChangeCampaignLocale();
      
      //code for personalized banner display
      try{
            frmMyTopUpComplete.hbxAdv.setVisibility(false);
            frmMyTopUpComplete.hbxCommon.setVisibility(false);
            frmMyTopUpComplete.image2447443295186.src="";
            frmMyTopUpComplete.imgTwo.src="";
            frmMyTopUpComplete.gblBrwCmpObject.handleRequest="";
            frmMyTopUpComplete.gblBrwCmpObject.htmlString="";
            frmMyTopUpComplete.gblVbxCmp.remove(gblBrwCmpObject);
            frmMyTopUpComplete.hbxAdv.remove(gblVbxCmp);
            frmMyTopUpComplete.hbxCommon.remove(gblVbxCmp);
      }
      catch(e)
      {
      }

}


function frmMyTopUpCompletePreshow() {
changeStatusBarColor();
	frmMyTopUpComplete.scrollboxMain.scrollToEnd();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyTopUpComplete.id))){
	frmMyTopUpComplete.lblHdrTxt.text = kony.i18n.getLocalizedString('Complete');
	if(flowSpa){
	frmMyTopUpComplete.btnCancelSpa.text = kony.i18n.getLocalizedString('keyReturn');
	frmMyTopUpComplete.btnAgreeSpa.text = kony.i18n.getLocalizedString('keybtnAddMore');
	}else{
	frmMyTopUpComplete.btnCancel.text = kony.i18n.getLocalizedString('keyReturn');
	frmMyTopUpComplete.btnAgree.text = kony.i18n.getLocalizedString('keybtnAddMore');
	}
	LocaleController.updatedForm(frmMyTopUpComplete.id);
   }
	gblLocale = false;
	ChangeCampaignLocale();
	
	//code for personalized banner display
	try{
	  /* 	frmMyTopUpComplete.hbxAdv.setVisibility(false);
	   	frmMyTopUpComplete.hbxCommon.setVisibility(false);
	   	frmMyTopUpComplete.image2447443295186.src="";
	   	frmMyTopUpComplete.imgTwo.src="";
	   	frmMyTopUpComplete.gblBrwCmpObject.handleRequest="";
	   	frmMyTopUpComplete.gblBrwCmpObject.htmlString="";
	   	frmMyTopUpComplete.gblVbxCmp.remove(gblBrwCmpObject);
       	frmMyTopUpComplete.hbxAdv.remove(gblVbxCmp);
       	frmMyTopUpComplete.hbxCommon.remove(gblVbxCmp);*/
	}
	catch(e)
	{
	}
	
}




function frmBillPaymentPreShow() {
	if(!(LocaleController.isFormUpdatedWithLocale(frmBillPayment.id))){
	frmBillPayment.lblHead.text = kony.i18n.getLocalizedString("keyBillPayment");
	//frmBillPayment.label448366816305.text = kony.i18n.getLocalizedString("keyFrom");
	frmBillPayment.label1010778103136563.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPayment.lblAmountForPenaltyOption.text = kony.i18n.getLocalizedString("keyPenalty");
	
    frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount"); 
	frmBillPayment.button475004897849.text = kony.i18n.getLocalizedString("keyBillPaymentFull");
	frmBillPayment.button475004897851.text = kony.i18n.getLocalizedString("keyBillPaymentMinimum");
	frmBillPayment.button475004897853.text = kony.i18n.getLocalizedString("keyBillPaymentSpecified");
	frmBillPayment.btnFull2.text = kony.i18n.getLocalizedString("keyBillPaymentFull");
	frmBillPayment.btnSpecified2.text = kony.i18n.getLocalizedString("keyBillPaymentSpecified");
	frmBillPayment.tbxMyNoteValue.placeholder = kony.i18n.getLocalizedString("TREnter_PL_My_Note");
	frmBillPayment.lblPayBillOn.text = kony.i18n.getLocalizedString("keyBillPayPayBillOn");
	frmBillPayment.btnback.text = kony.i18n.getLocalizedString("Back");
	frmBillPayment.btnNext.text = kony.i18n.getLocalizedString("Next");
	LocaleController.updatedForm(frmBillPayment.id);
    }
    
    frmBillPayment.tbxBillerNickName.placeholder = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillerNicknamePlaceholder"));
    frmBillPayment.lblBillerNickName.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillerNicknamePlaceholder"));
    frmBillPayment.lblBillerNickNameInfo.text = kony.i18n.getLocalizedString("MIB_BPNicknameSch");
    showBillerNicknameForScheduleBillerNotInMyBills();
    
	var currForm = kony.application.getCurrentForm();
	
	if(kony.i18n.getCurrentLocale() == "th_TH") {
		frmBillPayment.lblRef1.text = gblRef1LblTH;
		if(gblRef2LblTH != null)
			frmBillPayment.lblRef2.text = gblRef2LblTH;
		
		if(!gblBillerPresentInMyBills)
			frmBillPayment.lblCompCode.text = shortenBillerName(gblBillerCompCodeTH, 18);
			
	    if(gblCompCode!=undefined && gblCompCode!="" && gblCompCode=="2533"){
	    	frmBillPayment.lblMEACustName.text = gblSegBillerDataMB["billerCustNameTh"];
	    	frmBillPayment.lblMeterNo.text = gblSegBillerDataMB["billerMeterNoTh"];
			frmBillPayment.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtTh"];
			frmBillPayment.lblAmtLabel.text=gblSegBillerDataMB["billerAmountTh"];
			frmBillPayment.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntTh"];
			frmBillPayment.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtTh"];
		}
		if (currForm["id"] != "frmSchedule") {
			if(!gblPaynow) {
				frmBillPayment.lblPayBillOnValue.text = displayRecurringDates(frmBillPayment.lblPayBillOnValue.text);
			}	
		}	
	}else{
		frmBillPayment.lblRef1.text = gblRef1LblEN;	
		if(gblRef2LblEN != null)
			frmBillPayment.lblRef2.text = gblRef2LblEN;
		
		if(!gblBillerPresentInMyBills)
			frmBillPayment.lblCompCode.text = shortenBillerName(gblBillerCompCodeEN, 18);
			
		if (currForm["id"] != "frmSchedule") {	
			if(!gblPaynow) {
				frmBillPayment.lblPayBillOnValue.text = displayRecurringDates(frmBillPayment.lblPayBillOnValue.text);
			}
		}
		if(gblCompCode!=undefined && gblCompCode!="" && gblCompCode=="2533"){
			frmBillPayment.lblMEACustName.text = gblSegBillerDataMB["billerCustNameEn"];
			frmBillPayment.lblMeterNo.text = gblSegBillerDataMB["billerMeterNoEn"];
		 	frmBillPayment.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtEn"];
		 	frmBillPayment.lblAmtLabel.text=gblSegBillerDataMB["billerAmountEn"];
			frmBillPayment.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntEn"];
			frmBillPayment.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtEn"];
		}
	
	}
	if(gblCompCode!=undefined && gblCompCode!="" && gblCompCode=="2533"){
		frmBillPayment.hbxPayBillOn.setEnabled(false);
	}else{
		frmBillPayment.hbxPayBillOn.setEnabled(true);
		frmBillPayment.hbxAmountDetailsMEA.setVisibility(false);
	}
	gblLocale = false;
}

function frmTopUpPreShow() {
   if(!(LocaleController.isFormUpdatedWithLocale(frmTopUp.id))){
	frmTopUp.lblHead.text = kony.i18n.getLocalizedString("TopUp");
	frmTopUp.btnNext.text = kony.i18n.getLocalizedString("Next");
	frmTopUp.btnback.text = kony.i18n.getLocalizedString("Back");
	frmTopUp.lblPayBillOn.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillPaymentPayBillOn"));
	frmTopUp.tbxMyNoteValue.placeholder = removeColonFromEnd(kony.i18n.getLocalizedString("TREnter_PL_My_Note"));
	//frmTopUp.lblTopUpTo.text = kony.i18n.getLocalizedString("keyBillPaymentBiller");
	frmTopUp.lblTopUpAmount.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyAmount"));
	frmTopUp.lblComboAmount.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyAmount"));
	frmTopUp.lblNickName.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillerNicknamePlaceholder"));
	frmTopUp.lblBillerNickNameInfo.text = removeColonFromEnd(kony.i18n.getLocalizedString("MIB_BPNicknameSch"));
	frmTopUp.txtNickName.placeholder = removeColonFromEnd(kony.i18n.getLocalizedString("keyBillerNicknamePlaceholder"));
	frmTopUp.btnTopUpAmountComboBox.text = kony.i18n.getLocalizedString('MIB_TUAmount');
	//frmTopUp.label448366816305.text = kony.i18n.getLocalizedString("keyFrom");
	LocaleController.updatedForm(frmTopUp.id);
   }
	var currForm = kony.application.getCurrentForm();
	showNicknameForScheduleTopUpNotInMyBills();
	if(kony.i18n.getCurrentLocale() == "th_TH") {
	frmTopUp.lblRef1.text = gblRef1LblTH;
		if (currForm["id"] != "frmSchedule") {
			if(!gblPaynow) {
				frmTopUp.lblPayBillOnValue.text = displayRecurringDates(frmTopUp.lblPayBillOnValue.text);
			}	
		}
	}else{
	frmTopUp.lblRef1.text = gblRef1LblEN;
		if (currForm["id"] != "frmSchedule") {
			if(!gblPaynow) {
				frmTopUp.lblPayBillOnValue.text = displayRecurringDates(frmTopUp.lblPayBillOnValue.text);
			}
		}	
	}
	gblLocale = false;
}

function displayRecurringDates(dateString) {
	
	var repeatDate = dateString;
	if(repeatDate.trim().indexOf(" ") != -1) {	
	if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Once") || kony.string.equalsIgnoreCase(gblScheduleEndBP, "none")) {
		var dateArray = dateString.split(" ");
		var date1 = dateArray[0];
		var date2 = dateArray[2];
		repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
	}else if(kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")) {
		var dateArray = dateString.split(" ");
		var date1 = dateArray[0];
		if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " "+ kony.i18n.getLocalizedString("keyDaily"); 
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " "+ kony.i18n.getLocalizedString("keyWeekly");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " "+ kony.i18n.getLocalizedString("keyMonthly");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " "+ kony.i18n.getLocalizedString("keyYearly");
		}
	}if(kony.string.equalsIgnoreCase(gblScheduleEndBP, "After") || kony.string.equalsIgnoreCase(gblScheduleEndBP, "OnDate")) {
		var dateArray = dateString.split(" ");
		var date1 = dateArray[0];
		var date2 = dateArray[2];
		var numOfTimes = dateArray[5];
		if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
			repeatDate = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
		}
	}
	}
	
	return repeatDate;
}

function frmBillPaymentConfirmationFuturePreShow() {
	changeStatusBarColor();
	//Adding line to add braces as per new UI
	//gblCompCode="2530";
	var actualFee=frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
	frmBillPaymentConfirmationFuture.lblPaymentFeeValueH.text= actualFee;
	//
	if(!(LocaleController.isFormUpdatedWithLocale(frmBillPaymentConfirmationFuture.id))){
	frmBillPaymentConfirmationFuture.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
	frmBillPaymentConfirmationFuture.lblPayBy.text = kony.i18n.getLocalizedString("MIB_BPkeyPayFrom");
	frmBillPaymentConfirmationFuture.lblPayToNickname.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
	if(GblBillTopFlag){
		frmBillPaymentConfirmationFuture.lblPayToBillerName.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
	}else{
		frmBillPaymentConfirmationFuture.lblPayToBillerName.text = kony.i18n.getLocalizedString("keyTopUpCompltPayTo");
	}
	frmBillPaymentConfirmationFuture.lblBalBeforePay.text = kony.i18n.getLocalizedString("keyBalanceBeforePayment");
	frmBillPaymentConfirmationFuture.lblAccNumber.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountNumber");
	frmBillPaymentConfirmationFuture.lblAccName.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountName");
	if(gblPaynow){
		frmBillPaymentConfirmationFuture.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	}else{
		frmBillPaymentConfirmationFuture.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	}	
	
	frmBillPaymentConfirmationFuture.label1010748312179650.text = kony.i18n.getLocalizedString("keyEasyPassCustomerName");
	frmBillPaymentConfirmationFuture.lblAmount.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
	frmBillPaymentConfirmationFuture.lblfee.text=kony.i18n.getLocalizedString("keylblfee");
	//frmBillPaymentConfirmationFuture.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmBillPaymentConfirmationFuture.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyPaymentDateTime");
	//frmBillPaymentConfirmationFuture.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmBillPaymentConfirmationFuture.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmBillPaymentConfirmationFuture.lblRepeatAs.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmBillPaymentConfirmationFuture.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmBillPaymentConfirmationFuture.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	//frmBillPaymentConfirmationFuture.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmBillPaymentConfirmationFuture.lblTxnNum.text = kony.i18n.getLocalizedString("keyTransactionRefNumber");
	frmBillPaymentConfirmationFuture.lblTopUpRefNum.text = kony.i18n.getLocalizedString("keyTopUpRefId");
	frmBillPaymentConfirmationFuture.lblFreeTran.text = kony.i18n.getLocalizedString("remFreeTran");
	//frmBillPaymentConfirmationFuture.lblExecuteValue.text = kony.i18n.getLocalizedString("keyTimesMB");
	
	var timesVariable = frmBillPaymentConfirmationFuture.lblExecuteValue.text;
	if(isNotBlank(timesVariable))
	{
		timesVariable =timesVariable.substr(0,timesVariable.indexOf(" "))
	}
	frmBillPaymentConfirmationFuture.lblExecuteValue.text = timesVariable  + " "+  kony.i18n.getLocalizedString("keyTimesMB");
	//frmBillPaymentConfirmationFuture.lblExecuteValue.text = kony.i18n.getLocalizedString("keyTimesMB");
	/*
	if(flowSpa){
		frmBillPaymentConfirmationFuture.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmBillPaymentConfirmationFuture.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
	}	else{
	*/
		frmBillPaymentConfirmationFuture.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmBillPaymentConfirmationFuture.btnTransCnfrmConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	//}
	if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
		frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyDaily");
	}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
		frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyWeekly");
	}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
		frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyMonthly");
	}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
		frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyYearly");
	}else{
		frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyOnce");
	}
	LocaleController.updatedForm(frmBillPaymentConfirmationFuture.id);
    }
    //frmBillPaymentConfirmationFuture.hbxAmountDetailsMEA.setVisibility(false);
    if(gblCompCode=="2533"){
    	//frmBillPaymentConfirmationFuture.hbxExpand.setVisibility(true);
    	//frmBillPaymentConfirmationFuture.lnkExpand.text=kony.i18n.getLocalizedString("show");
    	frmBillPaymentConfirmationFuture.hbxAmountDetailsMEA.setVisibility(true);
    	frmBillPaymentConfirmationFuture.hbxCustAddress.setVisibility(true);
    	frmBillPaymentConfirmationFuture.btnHdrMenu.setEnabled(false);
		frmBillPaymentConfirmationFuture.hbxMEACustDetails.setVisibility(true);
    	frmBillPaymentConfirmationFuture.lblAmtLabel.text=frmBillPayment.lblAmtLabel.text;
    	frmBillPaymentConfirmationFuture.lblAmountValue1.text=frmBillPayment.lblAmountValue.text;
    	frmBillPaymentConfirmationFuture.lblAmtInterest.text=frmBillPayment.lblAmtInterest.text;
    	frmBillPaymentConfirmationFuture.lblAmtInterestValue.text=frmBillPayment.lblAmtInterestValue.text;
    	frmBillPaymentConfirmationFuture.lblAmtDisconnected.text=frmBillPayment.lblAmtDisconnected.text;
    	frmBillPaymentConfirmationFuture.lblAmtDisconnectedValue.text=frmBillPayment.lblAmtDisconnectedValue.text;
    	showMEACustDetailsMB(frmBillPaymentConfirmationFuture);
    	frmBillPaymentComplete.hbox101271281131304.setVisibility(true); //Remove Show HIDE button for MEA
    }else if(gblCompCode=="2530"){ // added this condition for 	MWA, need to change gblCompCode value 
    	frmBillPaymentConfirmationFuture.hbxAmountDetailsMEA.setVisibility(true);
    	frmBillPaymentConfirmationFuture.hbxCustAddress.setVisibility(true);
    	frmBillPaymentConfirmationFuture.btnHdrMenu.setEnabled(true);
		frmBillPaymentConfirmationFuture.hbxMEACustDetails.setVisibility(true);
    	frmBillPaymentConfirmationFuture.lblAmtLabel.text="Total Amount";
    	frmBillPaymentConfirmationFuture.lblAmountValue1.text=frmBillPayment.lblAmountValue.text;
    	frmBillPaymentConfirmationFuture.lblAmtInterest.text="Total Vat ";
    	frmBillPaymentConfirmationFuture.lblAmtInterestValue.text=frmBillPayment.lblAmtInterestValue.text;
    	frmBillPaymentConfirmationFuture.hbxAmtDisconnected.setVisibility(false);
    	frmBillPaymentConfirmationFuture.lblAmount.text="Total Payment Amount";
    	frmBillPaymentConfirmationFuture.lblAmountValue.text="25,473";
    	showMEACustDetailsMB(frmBillPaymentConfirmationFuture);
    	
    
    }else{
    	frmBillPaymentConfirmationFuture.hbxAmountDetailsMEA.setVisibility(false);
    	frmBillPaymentConfirmationFuture.btnHdrMenu.setEnabled(true);
    	frmBillPaymentConfirmationFuture.hbxMEACustDetails.setVisibility(false);
    	frmBillPaymentConfirmationFuture.hbxCustAddress.setVisibility(false);
    	frmBillPaymentComplete.hbox101271281131304.setVisibility(true); 
    }
	if(kony.i18n.getCurrentLocale() == "th_TH") {
		frmBillPaymentConfirmationFuture.lblRef1.text = gblRef1LblTH;//appendColon(gblRef1LblTH);
		if(gblRef2LblTH != null)
		frmBillPaymentConfirmationFuture.lblRef2.text = gblRef2LblTH;//appendColon(gblRef2LblTH);
		if(!gblBillerPresentInMyBills && gblPaynow){
			frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text = gblBillerCompCodeTH;
			frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = shortenBillerName(gblBillerCompCodeTH, 18);
		}
		frmBillPaymentConfirmationFuture.lblAccountName.text = gblProductNameTH;
		//frmBillPaymentConfirmationFuture.lblAccUserName.text = gblCustomerNameTh;
		if(gblCompCode=="2533"){
			frmBillPaymentConfirmationFuture.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtTh"] ;
			frmBillPaymentConfirmationFuture.lblAmtLabel.text=gblSegBillerDataMB["billerAmountTh"];
			frmBillPaymentConfirmationFuture.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntTh"];
			frmBillPaymentConfirmationFuture.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtTh"];
		}else if(gblCompCode=="2530"){ // added this condition for 	MWA, need to change gblCompCode value 
			//frmBillPaymentConfirmationFuture.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtTh"] ;
			//frmBillPaymentConfirmationFuture.lblAmtLabel.text=gblSegBillerDataMB["billerAmountTh"];
			//frmBillPaymentConfirmationFuture.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntTh"];
			//frmBillPaymentConfirmationFuture.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtTh"];
		}else{
			frmBillPaymentConfirmationFuture.lblAmount.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
		}
	}else{
		frmBillPaymentConfirmationFuture.lblRef1.text = gblRef1LblEN;//appendColon(gblRef1LblEN);
		if(gblRef2LblEN != null)
		frmBillPaymentConfirmationFuture.lblRef2.text = gblRef2LblEN;//appendColon(gblRef2LblEN);
		if(!gblBillerPresentInMyBills && gblPaynow){
			frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text = gblBillerCompCodeEN;
			frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = shortenBillerName(gblBillerCompCodeEN, 18);
		}
		frmBillPaymentConfirmationFuture.lblAccountName.text = gblProductNameEN;
		//frmBillPaymentConfirmationFuture.lblAccUserName.text = gblCustomerName;	
		if(gblCompCode=="2533"){
			frmBillPaymentConfirmationFuture.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtEn"];
			frmBillPaymentConfirmationFuture.lblAmtLabel.text=gblSegBillerDataMB["billerAmountEn"];
			frmBillPaymentConfirmationFuture.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntEn"];
			frmBillPaymentConfirmationFuture.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtEn"];
		}else if(gblCompCode=="2530"){ // added this condition for 	MWA, need to change gblCompCode value 
			//frmBillPaymentConfirmationFuture.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtTh"] ;
			//frmBillPaymentConfirmationFuture.lblAmtLabel.text=gblSegBillerDataMB["billerAmountTh"];
			//frmBillPaymentConfirmationFuture.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntTh"];
			//frmBillPaymentConfirmationFuture.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtTh"];
			frmBillPaymentConfirmationFuture.lblRef1.text = "Customer Code:";
			//frmBillPaymentConfirmationFuture.lblRef1Value.text="";
			frmBillPaymentConfirmationFuture.hbxMeterNum.setVisibility(false);
		}else{
			frmBillPaymentConfirmationFuture.lblAmount.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
		}
	}
	gblLocale = false;
}

function frmBillPaymentCompletePreShow() {
changeStatusBarColor();
	gblFromAccountSummary = false;
	gblFromAccountSummaryBillerAdded = false;
	if(!(LocaleController.isFormUpdatedWithLocale(frmBillPaymentComplete.id))){
	//share functionality
	frmBillPaymentComplete.lblSaveImage.text = kony.i18n.getLocalizedString("TRShare_Image");
	frmBillPaymentComplete.lblOthers.text = kony.i18n.getLocalizedString("More");
	//
	frmBillPaymentComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("TRComplete_Title");
	frmBillPaymentComplete.lblPayBy.text = kony.i18n.getLocalizedString("MIB_BPkeyPayFrom");
	frmBillPaymentComplete.lblPayToNickname.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
	if(GblBillTopFlag){
		frmBillPaymentComplete.lblPayToBillerName.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
	}else{
		frmBillPaymentComplete.lblPayToBillerName.text = kony.i18n.getLocalizedString("keyTopUpCompltPayTo");
	}
	frmBillPaymentComplete.lblAccNumber.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountNumber");
	frmBillPaymentComplete.lblAccName.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountName");
	if(gblPaynow){
		frmBillPaymentComplete.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	}else{
		frmBillPaymentComplete.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	}
	frmBillPaymentComplete.lblBalBeforePay.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
	//frmBillPaymentComplete.lblName.text = kony.i18n.getLocalizedString("keyEasyPassCustomerName");
	frmBillPaymentComplete.lblAmount.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
	frmBillPaymentComplete.lblfee.text=kony.i18n.getLocalizedString("keylblfee");
	//frmBillPaymentComplete.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
    /*
	if(flowSpa){
		frmBillPaymentComplete.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyPaymentDateTime");
	}	else{
	*/
		frmBillPaymentComplete.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyPaymentDateTime");
	//}
	frmBillPaymentComplete.lblSucessScreenshot.text=kony.i18n.getLocalizedString("Complete");
	frmBillPaymentComplete.lblFreeTran.text = kony.i18n.getLocalizedString("remFreeTran");
	//frmBillPaymentComplete.lblBalanceAfterPayment.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
	//frmBillPaymentComplete.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmBillPaymentComplete.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmBillPaymentComplete.lblRepeatAs.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmBillPaymentComplete.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmBillPaymentComplete.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	//frmBillPaymentComplete.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmBillPaymentComplete.lblTxnNum.text = kony.i18n.getLocalizedString("keyTransactionRefNumber");
	frmBillPaymentComplete.lblTopUpRefNum.text = kony.i18n.getLocalizedString("keyTopUpRefId");
	frmBillPaymentComplete.lblCardBal.text=kony.i18n.getLocalizedString("keyIBCardBalance");
	LocaleController.updatedForm(frmBillPaymentComplete.id);
    }
    frmBillPaymentComplete.lblPaymentFeeValueH.text=frmBillPaymentConfirmationFuture.lblPaymentFeeValueH.text;
	var timesVariable = frmBillPaymentComplete.lblExecuteValue.text;
	if(isNotBlank(timesVariable))
	{
		timesVariable =timesVariable.substr(0,timesVariable.indexOf(" "))
	}
	frmBillPaymentComplete.lblExecuteValue.text = timesVariable  + " "+  kony.i18n.getLocalizedString("keyTimesMB");
	if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
		frmBillPaymentComplete.lblEveryMonth.text = kony.i18n.getLocalizedString("keyDaily");
	}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
		frmBillPaymentComplete.lblEveryMonth.text = kony.i18n.getLocalizedString("keyWeekly");
	}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
		frmBillPaymentComplete.lblEveryMonth.text = kony.i18n.getLocalizedString("keyMonthly");
	}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
		frmBillPaymentComplete.lblEveryMonth.text = kony.i18n.getLocalizedString("keyYearly");
	}else{
		frmBillPaymentComplete.lblEveryMonth.text = kony.i18n.getLocalizedString("keyOnce");
	}
	
	
	frmBillPaymentComplete.btnBillCancel.text = kony.i18n.getLocalizedString("TRComplete_Btn_Return");
	if(GblBillTopFlag){
		frmBillPaymentComplete.btnBillConfirm.text = kony.i18n.getLocalizedString("keyMakeAnotherBP");
	}else{
		frmBillPaymentComplete.btnBillConfirm.text = kony.i18n.getLocalizedString("keyIBMakeTopUp");
	}
	
	if(gblDisplayBalanceBillPayment){
		frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");		
	}else{
		frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("show");	
	}
	
	frmBillPaymentComplete.hbxAmountDetailsMEA.setVisibility(false);
	frmBillPaymentComplete.hbxCustAddress.setVisibility(false);
	if(gblCompCode=="2533"){
		//frmBillPaymentComplete.hbxExpand.setVisibility(true);
		//frmBillPaymentComplete.lnkExpand.text=kony.i18n.getLocalizedString("show");
		frmBillPaymentComplete.hbxMEACustDetails.setVisibility(true);
		frmBillPaymentComplete.hbxCustAddress.setVisibility(true);
    	showMEACustDetailsMB(frmBillPaymentComplete);
    	frmBillPaymentComplete.hbox101271281131304.setVisibility(true); //Remove Show HIDE button for MEA
	}else{
		//frmBillPaymentComplete.hbxExpand.setVisibility(false);
		frmBillPaymentComplete.hbxMEACustDetails.setVisibility(false);
		frmBillPaymentComplete.hbxCustAddress.setVisibility(false);
		frmBillPaymentComplete.hbox101271281131304.setVisibility(true); //Remove Show HIDE button for MEA
	}
	
	if(kony.i18n.getCurrentLocale() == "th_TH") {		
		frmBillPaymentComplete.lblRef1.text = gblRef1LblTH;//appendColon(gblRef1LblTH);
		if(gblRef2LblTH != null)
		frmBillPaymentComplete.lblRef2.text = gblRef2LblTH;//appendColon(gblRef2LblTH);
		if(!gblBillerPresentInMyBills && gblPaynow){
			frmBillPaymentComplete.lblBillerNameCompCode.text = shortenBillerName(gblBillerCompCodeTH, 18);
			frmBillPaymentComplete.lblBillerNickname.text = gblBillerCompCodeTH;
		}
		frmBillPaymentComplete.lblAccountName.text = gblProductNameTH;
		//frmBillPaymentComplete.lblAccUserName.text = gblCustomerNameTh;
		if(gblCompCode=="2533"){
			frmBillPaymentComplete.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtTh"];
			frmBillPaymentComplete.lblAmtLabel.text=gblSegBillerDataMB["billerAmountTh"];
			frmBillPaymentComplete.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntTh"];
			frmBillPaymentComplete.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtTh"];
		}else{
			frmBillPaymentComplete.lblAmount.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
		}
		
	}else{
		frmBillPaymentComplete.lblRef1.text = gblRef1LblEN;//appendColon(gblRef1LblEN);
		if(gblRef2LblEN != null)
		frmBillPaymentComplete.lblRef2.text = gblRef2LblEN;//appendColon(gblRef2LblEN);
		if(!gblBillerPresentInMyBills && gblPaynow){
			frmBillPaymentComplete.lblBillerNameCompCode.text = shortenBillerName(gblBillerCompCodeEN, 18);
			frmBillPaymentComplete.lblBillerNickname.text = gblBillerCompCodeEN;
		}
		frmBillPaymentComplete.lblAccountName.text = gblProductNameEN;
		//frmBillPaymentComplete.lblAccUserName.text = gblCustomerName;
		if(gblCompCode=="2533"){
			frmBillPaymentComplete.lblAmount.text=gblSegBillerDataMB["billerTotalPayAmtEn"];
			frmBillPaymentComplete.lblAmtLabel.text=gblSegBillerDataMB["billerAmountEn"];
			frmBillPaymentComplete.lblAmtInterest.text=gblSegBillerDataMB["billerTotalIntEn"];
			frmBillPaymentComplete.lblAmtDisconnected.text=gblSegBillerDataMB["billerDisconnectAmtEn"];
		}else{
			frmBillPaymentComplete.lblAmount.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
		}
	}
	// handle btn add to my bills
	if(kony.i18n.getCurrentLocale() == "th_TH" && frmBillPaymentComplete.hbxAddToMyBills.isVisible) {
		if(GblBillTopFlag) {
			frmBillPaymentComplete.imgAddToMyBills.src = "th_button_add_to_my_bills.png";
		}else{
			frmBillPaymentComplete.imgAddToMyBills.src = "th_button_add_to_my_topup.png";
		}
	}else{
		if(GblBillTopFlag) {
			frmBillPaymentComplete.imgAddToMyBills.src = "en_button_add_to_my_bills.png";
		}else{
			frmBillPaymentComplete.imgAddToMyBills.src = "en_button_add_to_my_topup.png";
		}
	}
	ChangeCampaignLocale();
	gblLocale = false;
	
}

function frmSchedulePreShow() {
   if(!(LocaleController.isFormUpdatedWithLocale(frmSchedule.id))){
		frmSchedule.lblSchedule.text = kony.i18n.getLocalizedString("keyBillPaymentSchedule");
		frmSchedule.lblRepeat.text = kony.i18n.getLocalizedString("keyRepeat");
		frmSchedule.btnDaily.text = kony.i18n.getLocalizedString("keyDaily");
		frmSchedule.btnWeekly.text = kony.i18n.getLocalizedString("keyWeekly");
		frmSchedule.btnMonthly.text = kony.i18n.getLocalizedString("keyMonthly");
		frmSchedule.btnYearly.text = kony.i18n.getLocalizedString("keyYearly");
		frmSchedule.lblEnd.text = kony.i18n.getLocalizedString("Transfer_Ending");
		frmSchedule.btnNever.text = kony.i18n.getLocalizedString("keyNever");
		frmSchedule.btnAfter.text = kony.i18n.getLocalizedString("keyAfter");
		frmSchedule.btnOnDate.text = kony.i18n.getLocalizedString("keyOnDate");
		frmSchedule.label1010033328246485.text = kony.i18n.getLocalizedString("keyTimesMB");
		frmSchedule.lblNumberOfTimes.text = kony.i18n.getLocalizedString("keyIncludeThisTime");
		frmSchedule.lblScheduleUntil.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyUntil"));
		frmSchedule.btnAgree.text = kony.i18n.getLocalizedString("keysave");
		frmSchedule.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		LocaleController.updatedForm(frmSchedule.id);
   	}
	gblLocale = false;
}

function onclickHelpButton() {
	helpMessage.label444732340761.text = kony.i18n.getLocalizedString('keyPwdHelpIcon');
	hbox447443295153818.label474999996104.text = kony.i18n.getLocalizedString('keyPasswordRulesLabel');
	hbox447443295153926.button474999996106.text = kony.i18n.getLocalizedString('keyClose');
	//	gblLocale = false
}
function onclickuseridButton() {
	helpMessage.label444732340761.text = kony.i18n.getLocalizedString('keyIBUserIDGuidelines');
    hbox447443295153818.label474999996104.text = kony.i18n.getLocalizedString('keySPAUserID');
    if(flowSpa)
    	popupActivationHelp.button474999996106.text = kony.i18n.getLocalizedString('keyClose');
    else	
        hbox447443295153926.button474999996106.text = kony.i18n.getLocalizedString('keyClose');	
    //	gblLocale = false
}
function onclickActivationHelp(){
	//popupActivationHelp.containerHeight = 80;
	var currentLocale = kony.i18n.getCurrentLocale();
	popupActivationHelp.lblHead.text=kony.i18n.getLocalizedString('keyActHelpHead');
	/*
	if(gblActionCode == "23"){
		if (currentLocale == "en_US") {
			popupActivationHelp.richTxtActivationHelp.text=GLOBAL_ACTIVATION_HELP_TEXT_EN;
		}else{
			popupActivationHelp.richTxtActivationHelp.text=GLOBAL_ACTIVATION_HELP_TEXT_TH;
		}
		//popupActivationHelp.richTxtActivationHelp.text = kony.i18n.getLocalizedString("keyActivationHelpTextMBActAdd");
	}else{
		if (currentLocale == "en_US") {
			popupActivationHelp.richTxtActivationHelp.text=GLOBAL_REACTIVATION_HELP_TEXT_EN;
		}else{
			popupActivationHelp.richTxtActivationHelp.text=GLOBAL_REACTIVATION_HELP_TEXT_TH;
		}
		//popupActivationHelp.richTxtActivationHelp.text = kony.i18n.getLocalizedString("keyActivationHelpTextRct");
	}*/
	if(flowSpa){
		//#ifdef spaip
			popupActivationHelp.containerHeight="36";
		//#else
			if(kony.os.deviceInfo().userAgent.indexOf("Chrome") > 0)
				popupActivationHelp.containerHeight="50";
			else
				popupActivationHelp.containerHeight="40";
		//#endif
	}
	//#ifdef android
		popupActivationHelp.containerHeight="50";
	//#else
		
	//#endif
	
	var getEncKeyFromDevice = kony.store.getItem("encrytedText");
            if (getEncKeyFromDevice != null) {
                  if (currentLocale == "en_US") {
                        popupActivationHelp.richTxtActivationHelp.text=GLOBAL_REACTIVATION_HELP_TEXT_EN;
                  }else{
                        popupActivationHelp.richTxtActivationHelp.text=GLOBAL_REACTIVATION_HELP_TEXT_TH;
                  }
            }else{
                  if (currentLocale == "en_US") {
                        popupActivationHelp.richTxtActivationHelp.text=GLOBAL_ACTIVATION_HELP_TEXT_EN;
                  }else{
                        popupActivationHelp.richTxtActivationHelp.text=GLOBAL_ACTIVATION_HELP_TEXT_TH;
                  }
                   }
    if(!flowSpa) {                
     onclickHelpButtonTrans();
     }
}

function onclickHelpButtonTrans() {
	popHelpMessageTrans.label444732340761.text = kony.i18n.getLocalizedString('keyTxnPwdHelpIcon');
	hbox447443295153818.label474999996104.text = kony.i18n.getLocalizedString('keyPasswordRulesLabel');
	hbox447443295153926.button474999996106.text = kony.i18n.getLocalizedString('keyClose');
	//	gblLocale = false
}

function frmConnectAccMBPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmConnectAccMB.id))){
	if(flowSpa)
	{
		frmConnectAccMB.label475124774164.text = kony.i18n.getLocalizedString("keyConnectAccountSPA");
	}
	else
	{
		frmConnectAccMB.label475124774164.text = kony.i18n.getLocalizedString("keyConnectAccount");
	}
	frmConnectAccMB.label5027391406035.text = kony.i18n.getLocalizedString("email");
	if (flowSpa) {} else {
		frmConnectAccMB.lblConnectAccMbEmail.text = kony.i18n.getLocalizedString("keyDeviceNameLabel");
	}
	frmConnectAccMB.label45104812431174.text = kony.i18n.getLocalizedString("step3");
    LocaleController.updatedForm(frmConnectAccMB.id);
  }
	//gblLocale = false;
}

function frmMyAccountListPreShow() {
changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmMyAccountList.id))){
	frmMyAccountList.lblHdrTxtList.text = kony.i18n.getLocalizedString("keylblMyAccount");
	frmMyAccountList.lblTMBBank.text = kony.i18n.getLocalizedString("keylblTMBBank");
	frmMyAccountList.lblOtherBankACCnt.text = kony.i18n.getLocalizedString("keylblOtherBankAccount");
	LocaleController.updatedForm(frmMyAccountList.id);
  }
	//srvGetAcctType();
	myAccountListService();
}

function frmMyAccountViewPreShow() {
	changeStatusBarColor();
	// Dynamic locale
   if (gblMyAccountFlowFlag == 01) {
   		frmMyAccountView.lblAccntSuffix.text = "";
   }else if (gblMyAccountFlowFlag == 02) { 
   }else {
   		frmMyAccountView.lblAccntSuffix.text = "";
   }
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyAccountView.id))){	
	if (gblMyAccountFlowFlag == 01) {
		frmMyAccountView.lblAccountNo.text = kony.i18n.getLocalizedString("AccountNoLabel");
		frmMyAccountView.lblAccountName.text = kony.i18n.getLocalizedString("AccountName");
		//frmMyAccountView.lblAccntSuffix.text = "";
	} else if (gblMyAccountFlowFlag == 02) {
		frmMyAccountView.lblAccountNo.text = kony.i18n.getLocalizedString("AccountNoLabel");
		frmMyAccountView.lblAccountName.text = kony.i18n.getLocalizedString("keylblAccountSuffix") + ":";
		frmMyAccountView.lblAccntSuffix.text = kony.i18n.getLocalizedString("AccountName");
	} else {
		frmMyAccountView.lblAccountNo.text = kony.i18n.getLocalizedString("keyCreditCardNo") + ":";
		frmMyAccountView.lblAccountName.text = kony.i18n.getLocalizedString("keyCardHolderName") + ":";
		//frmMyAccountView.lblAccntSuffix.text = "";
	}
	// Fix locale
	frmMyAccountView.lblHdrTxtview.text = kony.i18n.getLocalizedString("keylblMyAccount");
	if (flowSpa) {
		frmMyAccountView.btnbackSpa.text = kony.i18n.getLocalizedString("Back")
	} else {
		frmMyAccountView.btnBack.text = kony.i18n.getLocalizedString("Back")
	}
	LocaleController.updatedForm(frmMyAccountView.id);
  }
}

function frmMyAccountEditPreShow() {
	changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmMyAccountEdit.id))){
	frmMyAccountEdit.lblHdrTxtEdit.text = kony.i18n.getLocalizedString("keylblEditMyAccnt"),
	frmMyAccountEdit.lblEditAccntNickName.text = kony.i18n.getLocalizedString("keylblNewAccntrNickName"),
	frmMyAccountEdit.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton"),
	frmMyAccountEdit.btnAgree.text = kony.i18n.getLocalizedString("keysave")
	LocaleController.updatedForm(frmMyAccountEdit.id);
  }
}

function frmMyAccntAddAccountPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyAccntAddAccount.id))){
	frmMyAccntAddAccount.lblHdrTxtAdd.text = kony.i18n.getLocalizedString("keylblAddAccount");
	frmMyAccntAddAccount.btnBanklist.text = kony.i18n.getLocalizedString("keylblBankName");
	frmMyAccntAddAccount.btnacntlist.text = kony.i18n.getLocalizedString("keybtnPleaseSelect");
	frmMyAccntAddAccount.lblAccntNo.text = kony.i18n.getLocalizedString("AccountNumber");
	frmMyAccntAddAccount.lblAccountSuffix.text = kony.i18n.getLocalizedString("keylblAccountSuffix");
	frmMyAccntAddAccount.label475028955317973.text = kony.i18n.getLocalizedString("Nickname");
	frmMyAccntAddAccount.lblAccountName.text =  kony.i18n.getLocalizedString("AccountName");
	if(flowSpa){
		frmMyAccntAddAccount.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmMyAccntAddAccount.btnAgreeSpa.text = kony.i18n.getLocalizedString("Next");
	}
	else
	{
		frmMyAccntAddAccount.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmMyAccntAddAccount.btnAgree.text = kony.i18n.getLocalizedString("Next")
	}
	LocaleController.updatedForm(frmMyAccntAddAccount.id);
   }
	srvGetBankList();
	srvGetAcctType();
}

function frmMyAccntConfirmationAddAccountPreShow() {
	changeStatusBarColor();
   //if(!(LocaleController.isFormUpdatedWithLocale(frmMyAccntConfirmationAddAccount.id))){
   	var isDisplay = frmMyAccntConfirmationAddAccount.imgcomplete.isVisible;
	//alert(" dissss--->"+isDisplay);
	if(isDisplay == true)
	{
	  frmMyAccntConfirmationAddAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
	}else{
	  frmMyAccntConfirmationAddAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblConfirmation");
	}


	//frmMyAccntConfirmationAddAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblConfirmation");
	frmMyAccntConfirmationAddAccount.lblbankAccount.text = kony.i18n.getLocalizedString("keylblBnkAccnt");
	//	frmMyAccntConfirmationAddAccount.lblbankname.text = kony.i18n.getLocalizedString("keylblBankName");
	//	frmMyAccntConfirmationAddAccount.lblAccntNo.text = kony.i18n.getLocalizedString("AccountNo"),
	//	frmMyAccntConfirmationAddAccount.lblAccountNickname.text =kony.i18n.getLocalizedString("keylblNickname") + ":",
	//	frmMyAccntConfirmationAddAccount.lblAccntSuffix.text =kony.i18n.getLocalizedString("keylblAccountSuffix") + ":",
	if(flowSpa){
		frmMyAccntConfirmationAddAccount.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmMyAccntConfirmationAddAccount.btnAgreeSpa.text = kony.i18n.getLocalizedString("keyConfirm");
		frmMyAccntConfirmationAddAccount.button473361467792207.text = kony.i18n.getLocalizedString("keyReturn");
		//frmMyAccntConfirmationAddAccount.button473361467792209.text = kony.i18n.getLocalizedString("keybtnAddMoreAccount");
		frmMyAccntConfirmationAddAccount.button473361467792209.text = kony.i18n.getLocalizedString("keyAddMoreIB");
	}	else {
		frmMyAccntConfirmationAddAccount.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmMyAccntConfirmationAddAccount.btnAgree.text = kony.i18n.getLocalizedString("keyConfirm");
		frmMyAccntConfirmationAddAccount.button450155142484043.text = kony.i18n.getLocalizedString("keyReturn");
		//frmMyAccntConfirmationAddAccount.button450155142484045.text = kony.i18n.getLocalizedString("keybtnAddMoreAccount");
		frmMyAccntConfirmationAddAccount.button450155142484045.text = kony.i18n.getLocalizedString("keyAddMoreIB");
		}
	
	// Update segement value
	for (var i = 0; i < gblMyAccntAddTmpData.length; i++) {
		gblMyAccntAddTmpData[i].lblAccountNickname = kony.i18n.getLocalizedString("keylblNewAccntrNickName") + ":";
		var lblAccntNo = kony.i18n.getLocalizedString("AccountNo") + "";
		
		if (gblMyAccntAddTmpData[i].lblAccntNoValue.length == 19)
			lblAccntNo = kony.i18n.getLocalizedString("keyCreditCardNo") + ":";
		gblMyAccntAddTmpData[i].lblAccntNo = lblAccntNo;
		if (gblMyAccntAddTmpData[i].lblAccntNoValue.length == 19)
			gblMyAccntAddTmpData[i].lblbankname = kony.i18n.getLocalizedString("keyCardHolderName");
		else if (gblMyAccntAddTmpData[i].lblbankname != null && gblMyAccntAddTmpData[i].lblbankname != undefined &&
			gblMyAccntAddTmpData[i].lblbankname != "")
			gblMyAccntAddTmpData[i].lblbankname = kony.i18n.getLocalizedString("AccountName");
		
		if (gblMyAccntAddTmpData[i].lblAccntSuffix != null && gblMyAccntAddTmpData[i].lblAccntSuffix != undefined &&
			gblMyAccntAddTmpData[i].lblAccntSuffix != "")
			gblMyAccntAddTmpData[i].lblAccntSuffix = kony.i18n.getLocalizedString("keylblAccountSuffix") + ":";
	   if(gblDelteIcn)
	   {
	        gblMyAccntAddTmpData[i].btnDelete = { isVisible:false };
	        gblMyAccntAddTmpData[i].template = hbxBankAccntComp;
	   }
   }
	 
	
	frmMyAccntConfirmationAddAccount.segBankAccnt.setData(gblMyAccntAddTmpData);
    //LocaleController.updatedForm(frmMyAccntConfirmationAddAccount.id);
//   }
    ChangeCampaignLocale();
    
    //code for personalized banner display
	try{
	   	frmMyAccntConfirmationAddAccount.hbxAdv.setVisibility(false);
	   	frmMyAccntConfirmationAddAccount.image2448367682213126.src="";
	   	frmMyAccntConfirmationAddAccount.gblBrwCmpObject.handleRequest="";
	   	frmMyAccntConfirmationAddAccount.gblBrwCmpObject.htmlString="";
	   	frmMyAccntConfirmationAddAccount.gblVbxCmp.remove(gblBrwCmpObject);
       	frmMyAccntConfirmationAddAccount.hbxAdv.remove(gblVbxCmp);
	}
	catch(e)
	{
	}
    
}

function frmSSSApplyPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmSSSApply.id))){
	frmSSSApply.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblsendToSave");
	frmSSSApply.lblTnCmessage1.text=kony.i18n.getLocalizedString("S2S_ApplyContent");
	//frmSSSApply.lblNote.text = kony.i18n.getLocalizedString("keyNote");
	//frmSSSApply.lblTnCmessage.text = kony.i18n.getLocalizedString("TCMessage");
	if(flowSpa)
	{
		frmSSSApply.btnOpnNFASpa.text = kony.i18n.getLocalizedString("S2S_lblOpenNoFixed");
		frmSSSApply.btnApplySpa.text=kony.i18n.getLocalizedString("keyApplyNow");
	}
	else
	{
		frmSSSApply.btnOpnNFA.text = kony.i18n.getLocalizedString("S2S_lblOpenNoFixed");
		frmSSSApply.btnApply.text=kony.i18n.getLocalizedString("keyApplyNow");
	}
	LocaleController.updatedForm(frmSSSApply.id);
   }
}

function frmSSTnCPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmSSTnC.id))){
	frmSSTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("applySSSrvce");
	//frmSSTnC.lblNote.text = kony.i18n.getLocalizedString("keyNote");
	//frmSSTnC.lblTnCMsg.text = kony.i18n.getLocalizedString("TCMessage");
	if(flowSpa){
	frmSSTnC.btnAgreeSpa.text = kony.i18n.getLocalizedString("keyAgreeButton");
	frmSSTnC.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
	}else{
	frmSSTnC.btnAgree.text = kony.i18n.getLocalizedString("keyAgreeButton");
	frmSSTnC.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	}
	LocaleController.updatedForm(frmSSTnC.id);
   }
}

function frmSSConfirmationPreShow() {
	changeStatusBarColor();
	 s2sSegmentSliderMappingForConfirmOnLocaleChange();
     s2sNoFixedAcctMappingForConfirmLocaleChange();
   //if(!(LocaleController.isFormUpdatedWithLocale(frmSSConfirmation.id))){
	frmSSConfirmation.lbltoNoFix.text = kony.i18n.getLocalizedString("S2S_FromExeedsMsg") 
	frmSSConfirmation.lblTrnsFrom.text = kony.i18n.getLocalizedString("S2S_ToBelowMsg") 
	if(flowSpa)
 {
    frmSSConfirmation.lbltoNoFix.text = kony.i18n.getLocalizedString("S2S_FromExeedsMsgSpa");
    frmSSConfirmation.lblTrnsFrom.text = kony.i18n.getLocalizedString("S2S_ToBelowMsgSpa");
 }
	if (!gblConfOrComp) {
		frmSSConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
		if(flowSpa){
		frmSSConfirmation.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
		frmSSConfirmation.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
		}else{
		frmSSConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
		frmSSConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		}
		frmSSConfirmation.lblSend.text = kony.i18n.getLocalizedString("sendFrom");
		frmSSConfirmation.lblSaveTo.text = kony.i18n.getLocalizedString("saveTo");
		frmSSConfirmation.lblAcc.text = kony.i18n.getLocalizedString("AccountNo");
		frmSSConfirmation.lblAccNo.text = kony.i18n.getLocalizedString("AccountNo");
	} else {
		frmSSConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
		if(flowSpa){
		frmSSConfirmation.btnCompReturnSpa.text = kony.i18n.getLocalizedString("keyReturn");
		}else{
		frmSSConfirmation.btnCompReturn.text = kony.i18n.getLocalizedString("keyReturn");
		}
		//frmSSConfirmation.lblATMMsg.text = kony.i18n.getLocalizedString("ATMmsg");
		frmSSConfirmation.lblSend.text = kony.i18n.getLocalizedString("sendFrom");
		frmSSConfirmation.lblSaveTo.text = kony.i18n.getLocalizedString("saveTo");
		frmSSConfirmation.lblAccNo.text = kony.i18n.getLocalizedString("AccountNo");
		frmSSConfirmation.lblAcc.text = kony.i18n.getLocalizedString("AccountNo");
		frmSSConfirmation.label47422528158851.text=kony.i18n.getLocalizedString("lblAplyDate");
	}
	//LocaleController.updatedForm(frmSSConfirmation.id);
//    }
	ChangeCampaignLocale();
}

function frmSSServicePreShow() {
	changeStatusBarColor();
	if(gbls2sEditFlag == "false"){
    setRowTemplateForS2S();
    s2sNoFixedAcctPopulate();
    }
    else if(gbls2sEditFlag=="dummyZeet")
    changeLocaleforApplyService();
    if (gblSSServieHours == "S2SAuthenticated") {
		frmSSService.lblHeaderTitle.text = kony.i18n.getLocalizedString("keylblsendToSave");
	}
    if(!(LocaleController.isFormUpdatedWithLocale(frmSSService.id))){
	frmSSService.lblHeaderTitle.text = kony.i18n.getLocalizedString("applySSSrvce");
	frmSSService.lblHeaderTitle.text = kony.i18n.getLocalizedString("applySSSrvce");
	frmSSService.lblSelectSlider.text = kony.i18n.getLocalizedString("slctSndngAcc");
	frmSSService.lblAcc.text = kony.i18n.getLocalizedString("AccountNo");
	frmSSService.lblSend.text = kony.i18n.getLocalizedString("sendFrom");
	frmSSService.lblTransFromMsg1.text = kony.i18n.getLocalizedString("trnfrToNoFix");
	frmSSService.lblTransFromMsg2.text = kony.i18n.getLocalizedString("exceed");
	frmSSService.lblTransToMsg1.text = kony.i18n.getLocalizedString("trnsFronNoFix");
	frmSSService.lblTransToMsg2.text = kony.i18n.getLocalizedString("isBelow");
	if(flowSpa){
	frmSSService.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmSSService.btnSaveSpa.text = kony.i18n.getLocalizedString("keysave");
	frmSSService.btnNextSpa.text = kony.i18n.getLocalizedString("Next");
	}else{
	frmSSService.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmSSService.btnSave.text = kony.i18n.getLocalizedString("keysave");
	frmSSService.btnNext.text = kony.i18n.getLocalizedString("Next");
	}
	frmSSService.lblAccNo.text = kony.i18n.getLocalizedString("AccountNo");
	frmSSService.lblSendTo.text = kony.i18n.getLocalizedString("saveTo");
	LocaleController.updatedForm(frmSSService.id);
    }
}

function frmSSSExecutePreShow() {
	changeStatusBarColor();
   changeLocaleforExecuteDetail();
   //if(!(LocaleController.isFormUpdatedWithLocale(frmSSSExecute.id))){
   if (frmSSSExecute.lblFromAccBalAftr.isVisible)
{
frmSSSExecute.lblHide.text=kony.i18n.getLocalizedString("keyUnhide")
}
else
{
frmSSSExecute.lblHide.text=kony.i18n.getLocalizedString("Hide")
}
if (frmSSSExecute.lblSendToAccBal.isVisible)
{
frmSSSExecute.lblHide1.text=kony.i18n.getLocalizedString("keyUnhide")
}
else
{
frmSSSExecute.lblHide1.text=kony.i18n.getLocalizedString("Hide")
}

	
	if(gblSSExcuteCnfrm == "transferS2Ssuccess"){
		
		frmSSSExecute.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
	}
	else{
		
		frmSSSExecute.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblsendToSave");
	}
	frmSSSExecute.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblsendToSave");
	frmSSSExecute.lblSendFrmMsg.text = kony.i18n.getLocalizedString("sendFrom");
	frmSSSExecute.lblAcc.text = kony.i18n.getLocalizedString("AccountNo");
	if(flowSpa)
	{
	frmSSSExecute.rchTxtSendFrmhidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br>"+"<a>" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >"+"<br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";
	frmSSSExecute.rchTxtSendToHidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br>"+"<a>" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >"+"<br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";
	}
	else
	{
	frmSSSExecute.rchTxtSendFrmhidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") +""+ kony.i18n.getLocalizedString("S2S_DelAcctMsg2") +" "+ "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + " " + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
	frmSSSExecute.rchTxtSendToHidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") +""+ kony.i18n.getLocalizedString("S2S_DelAcctMsg2") +" "+ "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + " " + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
	}
	frmSSSExecute.lbltoNoFixMsg.text = kony.i18n.getLocalizedString("trnfrToNoFix") + " " + kony.i18n.getLocalizedString(
		"exceed");
	frmSSSExecute.lblFromNoFixMsg.text = kony.i18n.getLocalizedString("trnsFronNoFix") + " " + kony.i18n.getLocalizedString(
		"isBelow");
	frmSSSExecute.lblCurrBalMsg.text = kony.i18n.getLocalizedString("keyBalanceBeforTrans");
	frmSSSExecute.lblFromAccBalAftrMsg.text = kony.i18n.getLocalizedString("keyBalanceAfterTrans");
	frmSSSExecute.rchTxtSendToHidn.text = kony.i18n.getLocalizedString("accDeleted");
	frmSSSExecute.rchTxtSendFrmhidn.text = kony.i18n.getLocalizedString("accDeleted");
	frmSSSExecute.lblAccName.text = kony.i18n.getLocalizedString("AccountNo");
	frmSSSExecute.lblS2Spoint.text = kony.i18n.getLocalizedString("send2savePoint");
	frmSSSExecute.lblTrnsAmtMsg.text = kony.i18n.getLocalizedString("Transfer") + " " + kony.i18n.getLocalizedString(
		"Transfer_amount");
	frmSSSExecute.lblTrnDtMsg.text = kony.i18n.getLocalizedString("keyMBTransactionDate");
	frmSSSExecute.lblTrnsRefMsg.text = kony.i18n.getLocalizedString("keyTransactionRefNo");
	frmSSSExecute.lblSaveTo.text = kony.i18n.getLocalizedString("saveTo");
	if(flowSpa)
	{
		frmSSSExecute.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmSSSExecute.btnAgreeSpa.text = kony.i18n.getLocalizedString("keyConfirm");
		frmSSSExecute.btnS2SReturnSpa.text = kony.i18n.getLocalizedString("keyReturn");
		
	}
	else
	{
		frmSSSExecute.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmSSSExecute.btnAgree.text = kony.i18n.getLocalizedString("keyConfirm");
		frmSSSExecute.btnS2SReturn.text = kony.i18n.getLocalizedString("keyReturn");
	}
	//LocaleController.updatedForm(frmSSSExecute.id);
//	}
}

function frmSSSExecuteCalendarPreShow() {
	changeStatusBarColor();
	frmSSSExecuteCalendar.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblsendToSave");
	frmSSSExecuteCalendar.lblSendFrmMsg.text = kony.i18n.getLocalizedString("sendFrom");
	frmSSSExecuteCalendar.lblAcc.text = kony.i18n.getLocalizedString("AccountNo");
	/*if(flowSpa)
	{
	frmSSSExecuteCalendar.rchTxtSendFrmhidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br>"+"<a>" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >"+"<br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";
	frmSSSExecuteCalendar.rchTxtSendToHidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br>"+"<a>" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >"+"<br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";
	}
	else
	{*/
	frmSSSExecuteCalendar.rchTxtSendFrmhidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") +""+ kony.i18n.getLocalizedString("S2S_DelAcctMsg2") +" "+ "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + " " + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
	frmSSSExecuteCalendar.rchTxtSendToHidn.text = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") +""+ kony.i18n.getLocalizedString("S2S_DelAcctMsg2") +" "+ "<a onclick= \"return onClickCallBackFunctionMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" + " " + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
	//}
	frmSSSExecuteCalendar.lbltoNoFixMsg.text = kony.i18n.getLocalizedString("trnfrToNoFix") + " " + kony.i18n.getLocalizedString("exceed");
	frmSSSExecuteCalendar.lblFromNoFixMsg.text = kony.i18n.getLocalizedString("trnsFronNoFix") + " " + kony.i18n.getLocalizedString("isBelow");
	//frmSSSExecuteCalendar.lblCurrBalMsg.text = kony.i18n.getLocalizedString("keyBalanceBeforTrans");
	frmSSSExecuteCalendar.lblFromAccBalAftrMsg.text = kony.i18n.getLocalizedString("keyBalanceAfterTrans");
	frmSSSExecuteCalendar.rchTxtSendToHidn.text = kony.i18n.getLocalizedString("accDeleted");
	frmSSSExecuteCalendar.rchTxtSendFrmhidn.text = kony.i18n.getLocalizedString("accDeleted");
	frmSSSExecuteCalendar.lblAccName.text = kony.i18n.getLocalizedString("AccountNo");
	frmSSSExecuteCalendar.lblS2Spoint.text = kony.i18n.getLocalizedString("send2savePoint");
	frmSSSExecuteCalendar.lblTrnsAmtMsg.text = kony.i18n.getLocalizedString("Transfer") + " " + kony.i18n.getLocalizedString("Transfer_amount");
	frmSSSExecuteCalendar.lblTrnDtMsg.text = kony.i18n.getLocalizedString("keyMBTransactionDate");
	frmSSSExecuteCalendar.lblTrnsRefMsg.text = kony.i18n.getLocalizedString("keyTransactionRefNo");
	frmSSSExecuteCalendar.lblSaveTo.text = kony.i18n.getLocalizedString("saveTo");
	/*if(flowSpa)
	{
		frmSSSExecuteCalendar.btnS2SReturnSpa.text = kony.i18n.getLocalizedString("keyReturn");
		
	}
	else
	{*/
		frmSSSExecuteCalendar.btnS2SReturn.text = kony.i18n.getLocalizedString("keyReturn");
	//}
}

function frmBillPaymentCompleteCalendarPreShow() {
changeStatusBarColor();
    frmBillPaymentCompleteCalendar.lblHdrTxt.text = kony.i18n.getLocalizedString("TRComplete_Title");
    frmBillPaymentCompleteCalendar.lblPayBy.text = kony.i18n.getLocalizedString("MIB_BPkeyPayFrom");
    frmBillPaymentCompleteCalendar.lblPayToNickname.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
    if (gblActivityIds == "030" || gblActivityIds == "031") {
		frmBillPaymentCompleteCalendar.lblPayToBillerName.text = kony.i18n.getLocalizedString("keyTopUpCompltPayTo");
	} else{
		frmBillPaymentCompleteCalendar.lblPayToBillerName.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
	} 
    frmBillPaymentCompleteCalendar.lblAccNumber.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountNumber");
    frmBillPaymentCompleteCalendar.lblAccName.text = kony.i18n.getLocalizedString("MIB_BPkeyAccountName");
	frmBillPaymentCompleteCalendar.lblSaveImage.text = kony.i18n.getLocalizedString("TRShare_Image");
	frmBillPaymentCompleteCalendar.lblOthers.text = kony.i18n.getLocalizedString("More");
	if(gblBpBalHideUnhide=="true")
	{
		frmBillPaymentCompleteCalendar.lblHide.text=kony.i18n.getLocalizedString("show");
	}
	else
	{
		frmBillPaymentCompleteCalendar.lblHide.text=kony.i18n.getLocalizedString("Hide");
	}
	if (gblPaynow) {
        frmBillPaymentCompleteCalendar.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
    } else {
        frmBillPaymentCompleteCalendar.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
    }
    frmBillPaymentCompleteCalendar.lblBalanceAfterPayment.text = kony.i18n.getLocalizedString("keyBalanceAfterPayment");
    frmBillPaymentCompleteCalendar.lblAmount.text = kony.i18n.getLocalizedString("keyBillPayTotalAmount");
    frmBillPaymentCompleteCalendar.lblfee.text=kony.i18n.getLocalizedString("keylblfee");
    /*
    if (flowSpa) {
        frmBillPaymentCompleteCalendar.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyPaymentDateTime");
    } else {
    */
        frmBillPaymentCompleteCalendar.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyPaymentDateTime");
    //}
    frmBillPaymentCompleteCalendar.lblSucessScreenshot.text=kony.i18n.getLocalizedString("Complete");
    frmBillPaymentCompleteCalendar.lblFreeTran.text = kony.i18n.getLocalizedString("remFreeTran");
    frmBillPaymentCompleteCalendar.lblRepeatAs.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
    frmBillPaymentCompleteCalendar.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
    frmBillPaymentCompleteCalendar.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
    frmBillPaymentCompleteCalendar.label1010748312179650.text=kony.i18n.getLocalizedString("keyEasyPassCustomerName");
	frmBillPaymentCompleteCalendar.lblCardBal.text=kony.i18n.getLocalizedString("keyIBCardBalance");
    frmBillPaymentCompleteCalendar.lblTxnNum.text = kony.i18n.getLocalizedString("keyTransactionRefNumber");
    frmBillPaymentCompleteCalendar.lblTopUpRefNum.text = kony.i18n.getLocalizedString("keyTopUpRefId");
    var timesVariable = frmBillPaymentConfirmationFuture.lblExecuteValue.text;
	if(isNotBlank(timesVariable))
	{
		timesVariable =timesVariable.substr(0,timesVariable.indexOf(" "))
	}
	frmBillPaymentConfirmationFuture.lblExecuteValue.text = timesVariable  + " "+  kony.i18n.getLocalizedString("keyTimesMB");
    if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
        frmBillPaymentCompleteCalendar.lblEveryMonth.text = kony.i18n.getLocalizedString("keyDaily");
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
        frmBillPaymentCompleteCalendar.lblEveryMonth.text = kony.i18n.getLocalizedString("keyWeekly");
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
        frmBillPaymentCompleteCalendar.lblEveryMonth.text = kony.i18n.getLocalizedString("keyMonthly");
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
        frmBillPaymentCompleteCalendar.lblEveryMonth.text = kony.i18n.getLocalizedString("keyYearly");
    } else {
        frmBillPaymentCompleteCalendar.lblEveryMonth.text = kony.i18n.getLocalizedString("keyOnce");
    }
	if(kony.i18n.getCurrentLocale() == "th_TH") {		
		frmBillPaymentCompleteCalendar.lblRef1.text = appendColon(gblExecRef1THVal);
		if(gblExecRef2THVal != null)
		frmBillPaymentCompleteCalendar.lblRef2.text = appendColon(gblExecRef2THVal);
	}
	else
	{
		frmBillPaymentCompleteCalendar.lblRef1.text = appendColon(gblExecRef1ENVal);
		if(gblExecRef2ENVal != null)
		frmBillPaymentCompleteCalendar.lblRef2.text = appendColon(gblExecRef2ENVal);
	}	
	frmBillPaymentCompleteCalendar.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
}

function frmOpenAccountNSConfirmationCalendarPreShow() {
	
}

function frmOpenActDSAckCalendarPreShow() {
	
}

function frmOpenActSavingCareCnfNAckCalendarPreShow() {

	
}

function frmOpenActTDAckCalendarPreShow() {
	
}

function frmTransfersAckCalendarPreShow() {
changeStatusBarColor();
	frmTransfersAckCalendar.scrollboxMain.scrollToEnd();
    frmTransfersAckCalendar.lblHdrTxt.text = kony.i18n.getLocalizedString("TRComplete_Title");
	if (gblTDTransfer){
		frmTransfersAckCalendar.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
	}else{
		frmTransfersAckCalendar.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_AmountFee");
	}
    frmTransfersAckCalendar.lblTransNPbAckBalAfter.text = kony.i18n.getLocalizedString("TRComplete_BalAfter");
    frmTransfersAckCalendar.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
   
   	frmTransfersAckCalendar.lblTransCnfmTotAmt.text = kony.i18n.getLocalizedString("TRConfirm_AmountFee");
	frmTransfersAckCalendar.lblCnfmToRefTitle1.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
    frmTransfersAckCalendar.lblTransNPbAckDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
	frmTransfersAckCalendar.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
	if(gblSmartFlag){
		frmTransfersAckCalendar.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive02");
	}
	// Split Amount:
	if(frmTransfersAckCalendar.segTransAckSplit.isVisible){
		frmTransfersAckCalendar.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
	}else{
		frmTransfersAckCalendar.lblHideMore.text = kony.i18n.getLocalizedString("show");
	}
	
	frmTransfersAckCalendar.btnTransNPbAckReturn.text = kony.i18n.getLocalizedString("Back");
	frmTransfersAckCalendar.btnTransNPbAckMakeAnthr.text = kony.i18n.getLocalizedString("TRComplete_BtnAgain");

	// for sharing
	frmTransfersAckCalendar.lblSaveImage.text = kony.i18n.getLocalizedString("TRShare_Image");
	frmTransfersAckCalendar.lblOthers.text = kony.i18n.getLocalizedString("More");

	//For ITMX Transfer detail
	if(gblToMobile){
		frmTransfersAckCalendar.lblTransNPbAckToName.text = getPhraseToMobileNumber();
	}
}

function frmSSServiceEDPreShow() {
	changeStatusBarColor();
	
	changeLocaleforEditDetail();
	if(!(LocaleController.isFormUpdatedWithLocale(frmSSServiceED.id))){
	frmSSServiceED.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblsendToSave");
	frmSSServiceED.lblSend.text = kony.i18n.getLocalizedString("sendFrom");
	frmSSServiceED.lblAccNo1.text = kony.i18n.getLocalizedString("AccountNo");
	frmSSServiceED.lblAccNo2.text = kony.i18n.getLocalizedString("AccountNo");
	if(flowSpa)
	{
	frmSSServiceED.rchTxtSendFromMsg.text=kony.i18n.getLocalizedString("S2S_DelAcctMsg") + "<a onclick= \"return onClickCallBackFunction();\" href = \"#\"  >" +" "+ kony.i18n.getLocalizedString("S2S_MyAcctList") +" "+ "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
	frmSSServiceED.rchTxtSendToMsg.text=kony.i18n.getLocalizedString("S2S_DelAcctMsg") + "<a onclick= \"return onClickCallBackFunction();\" href = \"#\"  >" +" "+ kony.i18n.getLocalizedString("S2S_MyAcctList") +" "+ "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
	}
	else
	{
	frmSSServiceED.rchTxtSendFromMsg.text = kony.i18n.getLocalizedString("accDeleted")
	frmSSServiceED.rchTxtSendToMsg.text = kony.i18n.getLocalizedString("accDeleted")
	}
	
	frmSSServiceED.lblToNoFixMsg.text = kony.i18n.getLocalizedString("S2S_FromExeedsMsg"); 
    frmSSServiceED.lblFromNoFixMsg.text = kony.i18n.getLocalizedString("S2S_ToBelowMsg");
	frmSSServiceED.lblSaveTo.text = kony.i18n.getLocalizedString("saveTo");
	if(flowSpa){
	frmSSServiceED.btnBack1Spa.text = kony.i18n.getLocalizedString("Back");
	frmSSServiceED.lblToNoFixMsg.text = kony.i18n.getLocalizedString("S2S_FromExeedsMsgSpa");
    frmSSServiceED.lblFromNoFixMsg.text = kony.i18n.getLocalizedString("S2S_ToBelowMsgSpa");
	}else{
	frmSSServiceED.btnBack1.text = kony.i18n.getLocalizedString("Back");
	}
	LocaleController.updatedForm(frmSSServiceED.id);
    }
}

function frmBBPaymentApplyPreshow() {
	changeStatusBarColor();
	frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyBBApplyPayment');
	frmAddTopUpToMB.lblBiller.text = kony.i18n.getLocalizedString('billerMB');
	frmAddTopUpToMB.lblNickname.text = kony.i18n.getLocalizedString('Receipent_NickName');
	//frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString('keyRef1');
	//frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString('keyRef2');
	if(flowSpa){
	frmAddTopUpToMB.btnSpaNext.text = kony.i18n.getLocalizedString('Next');
	}else{
	frmAddTopUpToMB.button15633509701481.text = kony.i18n.getLocalizedString('Next');
	}
	
	for(var i=0;i<gblmasterBillerAndTopupBBMB.length;i++){
		if(gblmasterBillerAndTopupBBMB[i]["BillerCompcode"] == gblBillerCompCodeBBMB){
			if(kony.i18n.getCurrentLocale() == "en_US"){
				frmAddTopUpToMB.lblAddbillerName.text =  gblmasterBillerAndTopupBBMB[i]["BillerNameEN"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"]+")";
				//alert(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"] + " " + gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"])
				frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1EN"];
				frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2EN"];
			}else{
				//alert(gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"] + " " + gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"])
				frmAddTopUpToMB.lblAddbillerName.text =  gblmasterBillerAndTopupBBMB[i]["BillerNameTH"] + "(" + gblmasterBillerAndTopupBBMB[i]["BillerCompcode"]+")";
				frmAddTopUpToMB.lblAddedRef1.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber1TH"];
				frmAddTopUpToMB.lblAddedRef2.text = gblmasterBillerAndTopupBBMB[i]["LabelReferenceNumber2TH"];
			}
		}	
	}
	
	
	gblLocale = false;
}

function frmBBSelectBillPreshow() {
	changeStatusBarColor();
	frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBB');
	frmMyTopUpSelect.btnSelectCat.text = kony.i18n.getLocalizedString('keyBillPaymentSelectCategory');
	frmMyTopUpSelect.lnkMore.text = kony.i18n.getLocalizedString('More');
	gblLocale = false;
}
function frmDreamSavingMaintainPreShow(){
changeStatusBarColor();
	 var locale = kony.i18n.getCurrentLocale();
	
         	var accTypeDream= "";
   if(!(LocaleController.isFormUpdatedWithLocale(frmDreamSavingMB.id))){			
	frmDreamSavingMB.lblTranfrEveryMnth.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth");
	frmDreamSavingMB.lblInterestRate.text = kony.i18n.getLocalizedString("InterestRate");
	frmDreamSavingMB.lblOpeningDate.text = kony.i18n.getLocalizedString("keyMBOpeningDate");
	frmDreamSavingMB.lblBranch.text = kony.i18n.getLocalizedString("Branch");
	if(flowSpa)
	  frmDreamSavingMB.btnBackSpa.text = kony.i18n.getLocalizedString("Back");
	else
	  frmDreamSavingMB.btnBack.text = kony.i18n.getLocalizedString("Back");
	frmDreamSavingMB.lblHdrTxtview.text = kony.i18n.getLocalizedString("keylblMyAccount");
	frmDreamSavingMB.label866795318717733.text = kony.i18n.getLocalizedString("keyMBMydream");
	frmDreamSavingMB.label866795318717734.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");
	frmDreamSavingMB.label866795318717737.text = kony.i18n.getLocalizedString("keylblAccountDetails");
	frmDreamSavingMB.lblMnthlySavingAmnt.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
	frmDreamSavingMB.label866795318717753.text = kony.i18n.getLocalizedString("KeyMBMonthlySavingfrm");
	LocaleController.updatedForm(frmDreamSavingMB.id);
   }
	if(DreamCustomerNickname!=null)
                	{
                		frmDreamSavingMB.lblfromNickname.text = DreamCustomerNickname;
						
                	}
                	else
                	{
						if (kony.i18n.getCurrentLocale() == "en_US") {

                                frmDreamSavingMB.lblfromNickname.text = DreamProductnameENGIB;

                            } else {

                                frmDreamSavingMB.lblfromNickname.text = DreamProductnameTHIB;

                            }
                	}
	
	   	if (kony.i18n.getCurrentLocale() == "en_US") {
	  						 frmDreamSavingMB.lblInterestRate.text= DreamInterestRateEN+":" ;
							    
						} else {
							    frmDreamSavingMB.lblInterestRate.text  =DreamInterestRateTH+":" ;
						}
					
                	if(linkedAccType=="SDA")
                	{
                	  accTypeDream=kony.i18n.getLocalizedString("keySavingMB");
                	}else if (linkedAccType == "CDA"){
                		accTypeDream=kony.i18n.getLocalizedString("keyTermDepositMB");
                	}else if (linkedAccType == "DDA"){
                		accTypeDream=kony.i18n.getLocalizedString("keyCurrentMB");
                	}else if (linkedAccType == "LOC"){
                		accTypeDream=kony.i18n.getLocalizedString("keylblLoan");
                	}
					
				
				
				frmDreamSavingMB.lblAccType.text  = accTypeDream;
}
function frmDreamSavingEditPreshow(){
changeStatusBarColor();
	
	frmDreamSavingEdit.lblNickname.text = kony.i18n.getLocalizedString("keyNickname");
	frmDreamSavingEdit.lblTargetAmnt.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");
	frmDreamSavingEdit.lblHdrTxtview.text = kony.i18n.getLocalizedString("keylblEditMyAccnt");
	frmDreamSavingEdit.lblSelectSlider.text = kony.i18n.getLocalizedString("keyMBMydream");
	frmDreamSavingEdit.lblDreamDesc.text = kony.i18n.getLocalizedString("DreamDes");
	frmDreamSavingEdit.lblSetSaving.text = kony.i18n.getLocalizedString("kayMBSetNewSaving");
	MnthlySAvingAmnt = frmDreamSavingEdit.lblSetSavingVal.text;
    var MnthlySAvingAmnt1 = MnthlySAvingAmnt.replace(/,/g, "").replace("/", "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var MnthlySAvingAmnt2;
    	if(kony.i18n.getCurrentLocale() == "en_US"){
   			 MnthlySAvingAmnt2=MnthlySAvingAmnt1.replace("เดือน", "");
   			 }else{
   			 	 MnthlySAvingAmnt2=MnthlySAvingAmnt1.replace("Month", "");
   			 }
	if((frmDreamSavingEdit.lblSetSavingVal.text!=null) && (frmDreamSavingEdit.lblSetSavingVal.text!= "")){
//	if(true){
	frmDreamSavingEdit.lblSetSavingVal.text	= MnthlySAvingAmnt2 +kony.i18n.getLocalizedString("currencyThaiBaht")+"/"+ kony.i18n.getLocalizedString("keyCalendarMonth");

	}else{

	}
	
	if(flowSpa)
	{
	  frmDreamSavingEdit.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
	  frmDreamSavingEdit.btnAgreeSpa.text = kony.i18n.getLocalizedString("keyConfirm");
	
		
	}
	else
	{
	
	  frmDreamSavingEdit.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	  frmDreamSavingEdit.btnAgree.text = kony.i18n.getLocalizedString("keyConfirm");
	}
	//frmDreamSavingEdit.btnDreamSavecombo.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth");
	DreamonClickAgreeOpenDSActsFori18();
}
function frmDreamCalculatorPreShow(){
changeStatusBarColor();
	frmDreamCalculator.lblHdrTxtview.text = kony.i18n.getLocalizedString("keylblCalculateMonthlySAving");
	frmDreamCalculator.lbltargetamnt.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");
	frmDreamCalculator.lblMnthlySaving.text = kony.i18n.getLocalizedString("keyMBMonthToMyDream");
	frmDreamCalculator.lblor.text = kony.i18n.getLocalizedString("keyor");
	frmDreamCalculator.lblMnthTODReam.text = kony.i18n.getLocalizedString("keyMBMonthToMyDream");
	frmDreamCalculator.btnCancel.text = kony.i18n.getLocalizedString("Back");
	frmDreamCalculator.btnAgree.text = kony.i18n.getLocalizedString("Next");
	

}

function frmTransferConfirmPreShow() {
changeStatusBarColor();
changeStatusBarColor();
	if(kony.application.getCurrentForm().id =="frmTransferConfirm"){
		popOtpSpa.button474046533325957.text = kony.i18n.getLocalizedString("keyCancelButton");
	}
	frmTransferConfirm.scrollboxMain.scrollToEnd();
	// Header From section:
	frmTransferConfirm.lblHdrConfirm.text = kony.i18n.getLocalizedString("Confirmation");
  	frmTransferConfirm.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_AmountFee");
	frmTransferConfirm.lblTransCnfmBalBef.text = kony.i18n.getLocalizedString("TRConfirm_BalBefore");
	frmTransferConfirm.lblFreeTran.text = kony.i18n.getLocalizedString("TRConfirm_Remain");
	
	// Transfer details: 
	frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
	frmTransferConfirm.lblTransCnfmTotAmt.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
	frmTransferConfirm.lblCnfmToRefTitle1.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
	frmTransferConfirm.lblCnfmToRefTitle2.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
  	frmTransferConfirm.lblTransCnfmDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
	frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
  	frmTransferConfirm.lblScheduleNote.text = kony.i18n.getLocalizedString("TRConfirm_FeeNotice");
  	var orftFlag = getORFTFlag(gblisTMB);
	var smartFlag = getSMARTFlag(gblisTMB);
	if(gblSelTransferMode == 1 && 
		(isNotBlank(orftFlag) && isNotBlank(smartFlag) && orftFlag == "N" && smartFlag == "Y")
		|| gblTransSMART == 1){
		frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive02");
	}
  	// Schedule Transfer: 
	if(!gblPaynow){
		frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_SchDetails");
		frmTransferConfirm.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		frmTransferConfirm.lblTransCnfmRefNum.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
		frmTransferConfirm.lblStartOn.text = kony.i18n.getLocalizedString("TRConfirm_SchDebit");
		frmTransferConfirm.lblRepeatAs.text = kony.i18n.getLocalizedString("TRConfirm_SchRepeat");
		frmTransferConfirm.lblEndOn.text = kony.i18n.getLocalizedString("TRConfirm_SchEnd");
		frmTransferConfirm.lblExecute.text = kony.i18n.getLocalizedString("TRConfirm_SchExe");
		var times = frmTransferConfirm.lblExecuteValue.text.split(" ")[0];
		frmTransferConfirm.lblExecuteValue.text = times + " "+ kony.i18n.getLocalizedString("keyTimesMB");
		if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")){
			frmTransferConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")){
			frmTransferConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")){
			frmTransferConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
		}else if(kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")){
			frmTransferConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
		}else{
			frmTransferConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
		}
	}
	// Split Amount:
	if(gblsplitAmt.length > 0){
		frmTransferConfirm.lblTransCnfmTotAmt2.text = kony.i18n.getLocalizedString("TRConfirm_Amount");
		if(frmTransferConfirm.segTransCnfmSplit.isVisible){
			frmTransferConfirm.lblHideMore.text = kony.i18n.getLocalizedString("Hide");
		}else{
			frmTransferConfirm.lblHideMore.text = kony.i18n.getLocalizedString("show");
		}
	}
	// TD Transfer:
	if(frmTransferLanding.hboxTD.isVisible){
  		frmTransferConfirm.lblTransCnfmTransDet.text = kony.i18n.getLocalizedString("TRConfirm_TranDetail");
		frmTransferConfirm.lblTransAmtFeeTitle.text = kony.i18n.getLocalizedString("TRConfirm_NetAmount");
		frmTransferConfirm.lblTransCnfmPenaltyAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDPenalty");
		frmTransferConfirm.lblPrincipalAmnt.text = kony.i18n.getLocalizedString("TRConfirm_Principal");
		frmTransferConfirm.lblTransCnfmIntAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDInt");
		frmTransferConfirm.lblTransCnfmTaxAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDTax");
		frmTransferConfirm.lblTransCnfmNetAmt.text = kony.i18n.getLocalizedString("TRConfirm_TDNet");
		frmTransferConfirm.lblTransCnfmRefNum.text = kony.i18n.getLocalizedString("TRConfirm_RefNo");
		frmTransferConfirm.lblTransCnfmDate.text = kony.i18n.getLocalizedString("TRConfirm_Debit");
		frmTransferConfirm.lblSmartDate.text = kony.i18n.getLocalizedString("TRConfirm_Receive");
  	}
	// Footer section:
	frmTransferConfirm.btnTransCnfrmConfirm.text = kony.i18n.getLocalizedString("TRConfirm_BtnConfirm");
	frmTransferConfirm.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	
	if(!isNotBlank(gblSelectedRecipentName) && (gblSelTransferMode == 2 || gblSelTransferMode == 3)){
		frmTransferConfirm.lblTransCnfmToBankName.text = getPhraseToMobileNumber();
	}
}

function frmMyRecipientSelectFBConfPreShow() {}

function frmMyRecipientSelectFbIDPreShow() {}

function frmMyRecipientSelectFBCompPreShow() {
	changeStatusBarColor();
  ChangeCampaignLocale();
  
  //code for personalized banner display
	try{
	   	frmMyRecipientSelectFBComp.hbxAdv.setVisibility(false);
	   	frmMyRecipientSelectFBComp.image2447443295186.src="";
	   	frmMyRecipientSelectFBComp.gblBrwCmpObject.handleRequest="";
	   	frmMyRecipientSelectFBComp.gblBrwCmpObject.htmlString="";
	   	frmMyRecipientSelectFBComp.gblVbxCmp.remove(gblBrwCmpObject);
       	frmMyRecipientSelectFBComp.hbxAdv.remove(gblVbxCmp);
	   	
	}
	catch(e)
	{
	} 
  
}

function frmMyRecipientSelectFacebookPreShow() {
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmMyRecipientSelectFacebook.id))){
	frmMyRecipientSelectFacebook.lblHdrTxt.text=kony.i18n.getLocalizedString('keySelectRecipients')
	if(flowSpa){
		frmMyRecipientSelectFacebook.btnReturnSpa.text=kony.i18n.getLocalizedString('keyReturn');
		frmMyRecipientSelectFacebook.btnNextSpa.text=kony.i18n.getLocalizedString('Next');
	}else{
		frmMyRecipientSelectFacebook.btnReturn.text=kony.i18n.getLocalizedString('keyReturn');
		frmMyRecipientSelectFacebook.btnNext.text=kony.i18n.getLocalizedString('Next');
	}
	frmMyRecipientSelectFacebook.textbox247327209467957.placeholder=kony.i18n.getLocalizedString("keySearch");
	
	frmMyRecipientSelectFacebook.label47402263614029.text=kony.i18n.getLocalizedString("Receipent_alert_NoFacebookFriends");
	LocaleController.updatedForm(frmMyRecipientSelectFacebook.id);
   }
}
function frmOpnActSelActPreShow() {
	changeStatusBarColor();
	frmOpnActSelAct.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
	frmOpnActSelAct.lblSelectSlider.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
	frmOpnActSelAct.lblOpenActSelAmt.text = kony.i18n.getLocalizedString("keyXferAmt");
	
	frmOpnActSelAct.label45731775454460.text = kony.i18n.getLocalizedString("Nickname");
	frmOpnActSelAct.btnDreamSavecombo.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth"); 
	var locale = kony.i18n.getCurrentLocale();
	if(!flowSpa){
			frmOpnActSelAct.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
			frmOpnActSelAct.btnOATDConfCancel.text = kony.i18n.getLocalizedString("Back");
	}
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpnActSelAct.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
					
						} else {
							frmOpnActSelAct.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
						}
						
						if(gblSelOpenActProdCode==206){
							var ifAccounts = true;
								if(flowSpa)
								{
								frmOpnActSelAct.btnPopUpTerminationSpa.text = kony.i18n.getLocalizedString("Next");
									gblspaSelIndex=0;
									ifAccounts = setDataforOpenFromActs(frmOpnActSelAct.segNSSliderSpa);
									if (ifAccounts == false){
									    return false;
									}
									gblmbSpatransflow=false;
								}
								else
								{
								frmOpnActSelAct.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
							    ifAccounts = setDataforOpenFromActs(frmOpnActSelAct.segNSSlider, frmOpnActSelAct.btnLtArrow, frmOpnActSelAct.btnRtArrow);
							    if (ifAccounts == false){
							    return false;
							    }
							    frmOpnActSelAct.segNSSlider.selectedIndex = dsSelActSelIndex;
							    dsSelActLength = frmOpnActSelAct.segNSSlider.data.length - 1;
							    }
						}else{
						
								dsSelActSelIndex = [0, 0];
			 					dsSelActLength = 0;
							 var ifAccounts = true;
							if(flowSpa)
							{
								gblspaSelIndex = 0
								gblmbSpatransflow=false;
						    	gblnormSelIndex = gbltranFromSelIndex[1];
								ifAccounts = setDataforOpenFromActs(frmOpnActSelAct.segNSSliderSpa);
								if (ifAccounts == false){
							    return false;
							    }
							}
							else
							{
								
							    ifAccounts = setDataforOpenFromActs(frmOpnActSelAct.segNSSlider,frmOpnActSelAct.btnLtArrow,frmOpnActSelAct.btnRtArrow);
							    if (ifAccounts == false){
							    return false;
							    }
								
								frmOpnActSelAct.segNSSlider.selectedIndex = dsSelActSelIndex;
								dsSelActLength = frmOpnActSelAct.segNSSlider.data.length - 1;
							}
						
						
						}
						
						if(flowSpa)
						frmOpnActSelAct.show();
}

function frmOpenProdDetnTnCPreShow() {
	changeStatusBarColor();
frmOpenProdDetnTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
	if(flowSpa){
		if (frmOpenProdDetnTnC.btnOpenProdAgreeSpa.isVisible == true){
			frmOpenProdDetnTnC.btnOpenProdAgreeSpa.text = kony.i18n.getLocalizedString('keyAgreeButton');
		   frmOpenProdDetnTnC.btnOpenProdCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
		   frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
		}else{
				var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenProdDetnTnC.lblOpenAccIntType.text = gblFinActivityLogOpenAct["intersetRateLabelEN"];
						} else {
							frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenProdDetnTnC.lblOpenAccIntType.text = gblFinActivityLogOpenAct["intersetRateLabelTH"];
						}
						
						if(gblSelOpenActProdCode == "220"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNoFeeProdDetSpa');
				} else if(gblSelOpenActProdCode == "200"){ 
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNormalSavProdDetSpa');
				} else if(gblSelOpenActProdCode == "221"){ 
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNoFixedProdDetSpa');
				} else if(gblSelOpenActProdCode == "222" ){
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyFreeFlowProdDetSpa');
				}else if(gblSelOpenActProdCode == "206"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyDreamProdDetSpa');
				}else if(gblSelOpenActProdCode == "203"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keySavingCareProdDetSpa');
				}else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermProdDetSpa');
				} else if(gblSelOpenActProdCode == "659"){
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermUpProdDetSpa');
				} else if(gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermQuickProdDetSpa');
				}
				frmOpenProdDetnTnC.btnOpenProdContinueSpa.text = kony.i18n.getLocalizedString('Next')
			}
	}else{
		if (frmOpenProdDetnTnC.btnOpenProdAgree.isVisible == true){		
		  frmOpenProdDetnTnC.btnOpenProdAgree.text = kony.i18n.getLocalizedString('keyAgreeButton');
		  frmOpenProdDetnTnC.btnOpenProdCancel.text = kony.i18n.getLocalizedString("keyCancelButton"); 
		  frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
		}else{
				var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenProdDetnTnC.lblOpenAccIntType.text = gblFinActivityLogOpenAct["intersetRateLabelEN"];
						} else {
							frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenProdDetnTnC.lblOpenAccIntType.text = gblFinActivityLogOpenAct["intersetRateLabelTH"];
						}
						
						if(gblSelOpenActProdCode == "220"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNoFeeProdDet');
				} else if(gblSelOpenActProdCode == "200"){ 
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNormalSavProdDet');
				} else if(gblSelOpenActProdCode == "221"){ 
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNoFixedProdDet');
				} else if(gblSelOpenActProdCode == "222" ){
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyFreeFlowProdDet');
				}else if(gblSelOpenActProdCode == "206"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyDreamProdDet');
				}else if(gblSelOpenActProdCode == "203"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keySavingCareProdDet');
				}else if(gblSelOpenActProdCode == "225"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyAllFreeSuperior');
				}else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602"){
				 frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermProdDet');
				} else if(gblSelOpenActProdCode == "659"){
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermUpProdDet');
				} else if(gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
				frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermQuickProdDet');
				}
				frmOpenProdDetnTnC.btnOpenProdContinue.text = kony.i18n.getLocalizedString('Next');
				frmOpenProdDetnTnC.btnTermsBack.text = kony.i18n.getLocalizedString('Back');
			}
	}
}


function frmOpenActTDConfirmPreShow() {
changeStatusBarColor();
//popupTractPwd.lblPopupTranscPwd.text=  kony.i18n.getLocalizedString("transPasswordSub ")
frmOpenActTDConfirm.label475124774164.text = kony.i18n.getLocalizedString('Confirmation');
frmOpenActTDConfirm.lblOATDNickName.text = kony.i18n.getLocalizedString('keyNickNameIB');
frmOpenActTDConfirm.lblOATDActName.text = kony.i18n.getLocalizedString('AccountName');
frmOpenActTDConfirm.lblOATDBranch.text = kony.i18n.getLocalizedString('Branch');
frmOpenActTDConfirm.lblOATDAmt.text = kony.i18n.getLocalizedString('keyLblOpnAmount');
//frmOpenActTDConfirm.lblOATDIntRate.text = kony.i18n.getLocalizedString('keyNickNameIB');
frmOpenActTDConfirm.lblTDOpenDate.text = kony.i18n.getLocalizedString('keyLblOpeningDate');
frmOpenActTDConfirm.lblTdTransRef.text = kony.i18n.getLocalizedString('keyTransactionRefNo');
frmOpenActTDConfirm.lblPayTDTo.text = kony.i18n.getLocalizedString('keyPayInterest');
frmOpenActTDConfirm.lblTDOpenAFrm.text = kony.i18n.getLocalizedString('keyOpenAmountFrom');
frmOpenActTDConfirm.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString('keyBalanceBeforTrans');
	var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenActTDConfirm.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenActTDConfirm.lblOATDIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] +":";
							frmOpenActTDConfirm.lblOATDBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
							frmOpenActTDConfirm.lblTDTransFrmNickName.text = gblFinActivityLogOpenAct["nickEN"];	
								if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
										frmOpenActTDConfirm.lblTDTransFrmActType.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
									 	frmOpenActTDConfirm.lblTDTransFrmActType.text = kony.i18n.getLocalizedString("CurrentMB");
									}		
						} else {
							frmOpenActTDConfirm.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenActTDConfirm.lblOATDIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
							frmOpenActTDConfirm.lblOATDBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
							frmOpenActTDConfirm.lblTDTransFrmNickName.text = gblFinActivityLogOpenAct["nickTH"];
								 if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
										frmOpenActTDConfirm.lblTDTransFrmActType.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
									 	frmOpenActTDConfirm.lblTDTransFrmActType.text = kony.i18n.getLocalizedString("CurrentMB");
									}			
						}
			if(flowSpa)
			{
			  frmOpenActTDConfirm.btnOATDConfCancelSpa.text = kony.i18n.getLocalizedString('keyCancelButton');
			  frmOpenActTDConfirm.btnOATDConfConfirmSpa.text = kony.i18n.getLocalizedString('keyConfirm');
			}
			else 
			{
			  frmOpenActTDConfirm.btnOATDConfCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
			  frmOpenActTDConfirm.btnOATDConfConfirm.text = kony.i18n.getLocalizedString('keyConfirm');
			}
					if(gblisDisToActTD == "Y"){
								if (kony.i18n.getCurrentLocale() == "en_US") {
								frmOpenActTDConfirm.lblTDTransToNickName.text = gblFinActivityLogOpenAct["nickENTO"];
									if(gblFinActivityLogOpenAct["actTypeToo"] == "SDA"){
										frmOpenActTDConfirm.lblTDTransToActType.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeToo"] == "DDA"){
									 	frmOpenActTDConfirm.lblTDTransToActType.text = kony.i18n.getLocalizedString("CurrentMB");
									}
								}else {
								frmOpenActTDConfirm.lblTDTransToNickName.text = gblFinActivityLogOpenAct["nickTHTO"];
									if(gblFinActivityLogOpenAct["actTypeToo"] == "SDA"){
										frmOpenActTDConfirm.lblTDTransToActType.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeToo"] == "DDA"){
									 	frmOpenActTDConfirm.lblTDTransToActType.text = kony.i18n.getLocalizedString("CurrentMB");
									}
								}
					}

}


function frmOpenActTDAckPreShow() {
changeStatusBarColor();
frmOpenActTDAck.lblHide.text = kony.i18n.getLocalizedString('Hide');
frmOpenActTDAck.label475124774164.text = kony.i18n.getLocalizedString('Complete');
frmOpenActTDAck.lblOATDTermAck.text = kony.i18n.getLocalizedString('keyTerm');
frmOpenActTDAck.lblOATDNickNameAck.text = kony.i18n.getLocalizedString('keyNickNameIB');
frmOpenActTDAck.lblOATDAckActNo.text = kony.i18n.getLocalizedString('AccountNo');
frmOpenActTDAck.lblOATDActNameAck.text = kony.i18n.getLocalizedString('AccountName');
frmOpenActTDAck.lblOATDBranchAck.text = kony.i18n.getLocalizedString('Branch');
frmOpenActTDAck.lblOATDAmtAck.text = kony.i18n.getLocalizedString('keyLblOpnAmount');
//frmOpenActTDAck.lblOATDIntRateAck.text = kony.i18n.getLocalizedString('Complete');
frmOpenActTDAck.lblTDOpenDateAck.text = kony.i18n.getLocalizedString('keyLblOpeningDate');
frmOpenActTDAck.lblTDMatDateAck.text = kony.i18n.getLocalizedString('keyMaturityDate');
frmOpenActTDAck.lblOATDTransRefAck.text = kony.i18n.getLocalizedString('keyTransactionRefNo');
frmOpenActTDAck.lblOAPayIntAck.text = kony.i18n.getLocalizedString('keyPayInterest');
frmOpenActTDAck.lblOATDActFrmAck.text = kony.i18n.getLocalizedString('KeyMBMonthlySavingfrm');
frmOpenActTDAck.label47505874739957.text = kony.i18n.getLocalizedString('keyBalanceAfterTrans');

if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602"){
				 frmOpenActTDAck.lblGreeting.text = kony.i18n.getLocalizedString('keyTermGreet');
				} else if(gblSelOpenActProdCode == "659"){
				frmOpenActTDAck.lblGreeting.text = kony.i18n.getLocalizedString('keyTermUpGreet');
				} else if(gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
				frmOpenActTDAck.lblGreeting.text = kony.i18n.getLocalizedString('keyTermQuickGreet');
				}

frmOpenActTDAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne")
frmOpenActTDAck.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi");
	var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenActTDAck.lblOATDAckTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenActTDAck.lblOATDIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
							frmOpenActTDAck.lblOATDBranchValAck.text = gblFinActivityLogOpenAct["branchEN"];
							frmOpenActTDAck.lblOAFrmNameAck.text = gblFinActivityLogOpenAct["nickEN"];	
									if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
										frmOpenActTDAck.lblOAFrmTypeAck.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
									 	frmOpenActTDAck.lblOAFrmTypeAck.text = kony.i18n.getLocalizedString("CurrentMB");
									}	
					
						} else {
							frmOpenActTDAck.lblOATDAckTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenActTDAck.lblOATDIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelTH"]+ ":";
							frmOpenActTDAck.lblOATDBranchValAck.text = gblFinActivityLogOpenAct["branchTH"];
							frmOpenActTDAck.lblOAFrmNameAck.text = gblFinActivityLogOpenAct["nickTH"];	
									if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
										frmOpenActTDAck.lblOAFrmTypeAck.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
									 	frmOpenActTDAck.lblOAFrmTypeAck.text = kony.i18n.getLocalizedString("CurrentMB");
									}	
						}
if(flowSpa)
{

  frmOpenActTDAck.btnOATDAckReturnSpa.text = kony.i18n.getLocalizedString('keybtnReturn');
  frmOpenActTDAck.btnOATDAckOpenMoreSpa.text = kony.i18n.getLocalizedString('btnOpenMore');
}
else
{
  frmOpenActTDAck.btnOATDAckReturn.text = kony.i18n.getLocalizedString('keybtnReturn');
  frmOpenActTDAck.btnOATDAckOpenMore.text = kony.i18n.getLocalizedString('btnOpenMore');
}


		if(gblisDisToActTD == "Y"){
								if (kony.i18n.getCurrentLocale() == "en_US") {
								frmOpenActTDAck.lblOATDToNicNam.text = gblFinActivityLogOpenAct["nickENTO"];
									if(gblFinActivityLogOpenAct["actTypeToo"] == "SDA"){
										frmOpenActTDAck.lblOATDToActType.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeToo"] == "DDA"){
									 	frmOpenActTDAck.lblOATDToActType.text = kony.i18n.getLocalizedString("CurrentMB");
									}
								}else {
								frmOpenActTDAck.lblOATDToNicNam.text = gblFinActivityLogOpenAct["nickTHTO"];
									if(gblFinActivityLogOpenAct["actTypeToo"] == "SDA"){
										frmOpenActTDAck.lblOATDToActType.text = kony.i18n.getLocalizedString("SavingMB");
									}else if(gblFinActivityLogOpenAct["actTypeToo"] == "DDA"){
									 	frmOpenActTDAck.lblOATDToActType.text = kony.i18n.getLocalizedString("CurrentMB");
									}
								}
					}
ChangeCampaignLocale();
}

function frmOpenActSelProdPreShow() {
	
	frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
	frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
	
	
	if(flowSpa)
		{
		  frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
		  frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
		  frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
		  frmOpenActSelProd.lblUnderForUseTab.text= kony.i18n.getLocalizedString("keyOpenAccountForUseComingSoon");
		}else{
		frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
		frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
		frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
		frmOpenActSelProd.lblUnderForUseTab.text= kony.i18n.getLocalizedString("keyOpenAccountForUseComingSoon");
		}
	if(gblSelProduct == "ForUse"){
		setDataForUse();
	}else if(gblSelProduct == "ForSave" || gblSelProduct == "TMBSavingcare"){
		setDataForSave();
	}else if(gblSelProduct == "ForTerm"){
		setDataForTD();
	}
	
}

function frmOpenActSavingCareCnfNAckPreShow() {
changeStatusBarColor();
	preShowiSavingCareConfirm();
	frmOpenActSavingCareCnfNAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne")
	var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenActSavingCareCnfNAck.lblOASCTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenActSavingCareCnfNAck.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"]+ ":" ;
							frmOpenActSavingCareCnfNAck.lblOASCFrmNameAck.text = gblFinActivityLogOpenAct["nickEN"];
    						frmOpenActSavingCareCnfNAck.lblOASCFrmTypeAck.text = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].lblSliderAccN2;
							frmOpenActSavingCareCnfNAck.lblOASCBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
								if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenActSavingCareCnfNAck.lblOASCFrmTypeAck.text = kony.i18n.getLocalizedString("SavingMB");
								}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenActSavingCareCnfNAck.lblOASCFrmTypeAck.text = kony.i18n.getLocalizedString("CurrentMB");
								}
						} else {
							frmOpenActSavingCareCnfNAck.lblOASCTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenActSavingCareCnfNAck.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"]+ ":";
							frmOpenActSavingCareCnfNAck.lblOASCFrmNameAck.text = gblFinActivityLogOpenAct["nickEN"];
							frmOpenActSavingCareCnfNAck.lblOASCBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
    							if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenActSavingCareCnfNAck.lblOASCFrmTypeAck.text = kony.i18n.getLocalizedString("SavingMB");
								}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenActSavingCareCnfNAck.lblOASCFrmTypeAck.text = kony.i18n.getLocalizedString("CurrentMB");
								}
    						
						}
			if (arrbenf1.length > 0) {
	            frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal1.text = RelationI18n(arrbenf1);
	        }
	        if (arrbenf2.length > 0) {
	            frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal2.text = RelationI18n(arrbenf2);
	        }
	        if (arrbenf3.length > 0) {
	            frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal3.text = RelationI18n(arrbenf3);
	        }
	        if (arrbenf4.length > 0) {
	            frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal4.text = RelationI18n(arrbenf4);
	        }
	        if (arrbenf5.length > 0) {
	            frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal5.text = RelationI18n(arrbenf5);
   			}
	
	
  ChangeCampaignLocale();
}

function frmOpenActDSConfirmPreShow() {
changeStatusBarColor();
frmOpenActDSConfirm.lblAccountDetails.text = kony.i18n.getLocalizedString("AccountDetails");
frmOpenActDSConfirm.label475124774164.text = kony.i18n.getLocalizedString("Confirmation");
frmOpenActDSConfirm.lblopenAcntMyDream.text = kony.i18n.getLocalizedString("keyMBMydream");
frmOpenActDSConfirm.lblMyDreamDes.text = kony.i18n.getLocalizedString("DreamDes");
frmOpenActDSConfirm.lblOADreamDetailTarAmt.text = kony.i18n.getLocalizedString("keyTargetAmnt");
frmOpenActDSConfirm.lblOADSNickName.text = kony.i18n.getLocalizedString("keyNickNameIB");
frmOpenActDSConfirm.lblOADSActName.text = kony.i18n.getLocalizedString("AccountName");
frmOpenActDSConfirm.lblOADSBranch.text = kony.i18n.getLocalizedString("Branch");
frmOpenActDSConfirm.lblOADSAmt.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
frmOpenActDSConfirm.lblOADMnth.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth");
//frmOpenActDSConfirm.lblOADIntRate.text = kony.i18n.getLocalizedString("keyOpenAcc");
frmOpenActDSConfirm.lblDSOpenDate.text = kony.i18n.getLocalizedString("keyLblOpeningDate");
frmOpenActDSConfirm.lblMnthlySavngFrm.text = kony.i18n.getLocalizedString("KeyMBMonthlySavingfrm");
var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenActDSConfirm.lblDSCnfmTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenActDSConfirm.lblOADIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] ;
							frmOpenActDSConfirm.lblOADSBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
							frmOpenActDSConfirm.lblOAMnthlySavName.text = gblFinActivityLogOpenAct["nickEN"];
							if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenActDSConfirm.lblOAMnthlySavType.text = kony.i18n.getLocalizedString("SavingMB");
								}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenActDSConfirm.lblOAMnthlySavType.text = kony.i18n.getLocalizedString("CurrentMB");
								}
					
						} else {
							frmOpenActDSConfirm.lblDSCnfmTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenActDSConfirm.lblOADIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"];
							frmOpenActDSConfirm.lblOADSBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
							frmOpenActDSConfirm.lblOAMnthlySavName.text = gblFinActivityLogOpenAct["nickTH"];
							if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenActDSConfirm.lblOAMnthlySavType.text = kony.i18n.getLocalizedString("SavingMB");
							}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenActDSConfirm.lblOAMnthlySavType.text = kony.i18n.getLocalizedString("CurrentMB");
							}
						}
if(flowSpa)
{
  frmOpenActDSConfirm.btnODConfCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmOpenActDSConfirm.btnODConfConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
}
else
{
  frmOpenActDSConfirm.btnODConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmOpenActDSConfirm.btnODConfConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
}
}

function frmOpenActDSAckPreShow() {
changeStatusBarColor();
frmOpenActDSAck.label475124774164.text = kony.i18n.getLocalizedString("Complete");
frmOpenActDSAck.lblopenAcntMyDreamAck.text = kony.i18n.getLocalizedString("keyMBMydream");
frmOpenActDSAck.lblMyDreamDesAck.text = kony.i18n.getLocalizedString("DreamDes");
frmOpenActDSAck.lblOADSNickNameAck.text = kony.i18n.getLocalizedString("keyNickNameIB");
frmOpenActDSAck.lblOADSActNameAck.text = kony.i18n.getLocalizedString("AccountName");
frmOpenActDSAck.lblDSAckActNo.text = kony.i18n.getLocalizedString("AccountNo");
frmOpenActDSAck.lblOADSBranchAck.text = kony.i18n.getLocalizedString("Branch");
frmOpenActDSAck.lblOADSAmtAck.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
frmOpenActDSAck.lblOADMnthAck.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth");
//frmOpenActDSAck.lblOADIntRateAck.text = kony.i18n.getLocalizedString("Complete");
frmOpenActDSAck.lblDSOpenDateAck.text = kony.i18n.getLocalizedString("keyLblOpeningDate");
frmOpenActDSAck.lblMnthlySavngFrmAck.text = kony.i18n.getLocalizedString("KeyMBMonthlySavingfrm");

frmOpenActDSAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
frmOpenActDSAck.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi");
frmOpenActDSAck.lblGreetingDS.text = kony.i18n.getLocalizedString("keyDreamGreet");

var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenActDSAck.lblDSAckTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenActDSAck.lblOADIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] +":";
							frmOpenActDSAck.lblOADSBranchValAck.text = gblFinActivityLogOpenAct["branchEN"];
							frmOpenActDSAck.lblOAMnthlySavNameAck.text = gblFinActivityLogOpenAct["nickEN"];
							if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenActDSAck.lblOAMnthlySavTypeAck.text = kony.i18n.getLocalizedString("SavingMB");
								}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenActDSAck.lblOAMnthlySavTypeAck.text = kony.i18n.getLocalizedString("CurrentMB");
								}
					
						} else {
							frmOpenActDSAck.lblDSAckTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenActDSAck.lblOADIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelTH"]+":";
							frmOpenActDSAck.lblOADSBranchValAck.text = gblFinActivityLogOpenAct["branchTH"];
							frmOpenActDSAck.lblOAMnthlySavNameAck.text = gblFinActivityLogOpenAct["nickTH"];
							if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenActDSAck.lblOAMnthlySavTypeAck.text = kony.i18n.getLocalizedString("SavingMB");
								}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenActDSAck.lblOAMnthlySavTypeAck.text = kony.i18n.getLocalizedString("CurrentMB");
								}
						}
if(flowSpa)
{
  frmOpenActDSAck.btnOADSAckReturnSpa.text = kony.i18n.getLocalizedString("returnMB");
  frmOpenActDSAck.btnOADSAckOpenMoreSpa.text = kony.i18n.getLocalizedString("btnOpenMore");
}
else
{
  frmOpenActDSAck.btnOADSAckReturn.text = kony.i18n.getLocalizedString("returnMB");
  frmOpenActDSAck.btnOADSAckOpenMore.text = kony.i18n.getLocalizedString("btnOpenMore");
}
ChangeCampaignLocale();
}

function frmOpenAcDreamSavingPreShow() {
changeStatusBarColor();
frmOpenAcDreamSaving.label475124774164.text = kony.i18n.getLocalizedString("keyMBMydream");
frmOpenAcDreamSaving.lblODPicIt.text = kony.i18n.getLocalizedString("keyPictureIt");
frmOpenAcDreamSaving.lblODDreamDesc.text = kony.i18n.getLocalizedString("DreamDes");
frmOpenAcDreamSaving.lblODTargetAmt.text = kony.i18n.getLocalizedString("keyTargetAmnt");
frmOpenAcDreamSaving.lblBuildUpDream.text = kony.i18n.getLocalizedString("keyBuildMyDream");
frmOpenAcDreamSaving.lblODMnthSavAmt.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
frmOpenAcDreamSaving.lblODMyDream.text = kony.i18n.getLocalizedString("keyMBMonthToMyDream");
frmOpenAcDreamSaving.txtODTargetAmt.placeholder = kony.i18n.getLocalizedString("keyTargetAmt");
if(frmOpenAcDreamSaving.txtODMyDream.text !="" && frmOpenAcDreamSaving.txtODMyDream.text !=null){
	
		frmOpenAcDreamSaving.txtODMyDream.text = gblDreamMnths + kony.i18n.getLocalizedString("keymonths");

}else{
	
	frmOpenAcDreamSaving.txtODMyDream.text ="";
}
frmOpenAcDreamSaving.label47505874738694.text = kony.i18n.getLocalizedString("keyor");
if (kony.i18n.getCurrentLocale() == "en_US") {
	frmOpenAcDreamSaving.lblAccountNicknameValue.text = gblFinActivityLogOpenAct["prodNameEN"];
} else {
	frmOpenAcDreamSaving.lblAccountNicknameValue.text = gblFinActivityLogOpenAct["prodNameTH"];
}
if(flowSpa)
{
  frmOpenAcDreamSaving.btnDSNextSpa.text = kony.i18n.getLocalizedString("Next");
}
else
  frmOpenAcDreamSaving.btnDSNext.text = kony.i18n.getLocalizedString("Next");
  frmOpenAcDreamSaving.btnOATDConfCancel.text = kony.i18n.getLocalizedString("Back");
  if (kony.i18n.getCurrentLocale() == "en_US") {
	frmOpenAcDreamSaving.lblAccountNicknameValue.text = gblFinActivityLogOpenAct["prodNameEN"];
	}else{
	frmOpenAcDreamSaving.lblAccountNicknameValue.text = gblFinActivityLogOpenAct["prodNameTH"];
	}
onClickAgreeOpenDSActs();
}

function frmOpenAccTermDepositPreShow() {
changeStatusBarColor();
if(flowSpa){
frmOpenAccTermDeposit.lblTranLandTo.text = kony.i18n.getLocalizedString("To");
}
frmOpenAccTermDeposit.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
frmOpenAccTermDeposit.lblTDOpenAcctFrm.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
frmOpenAccTermDeposit.lblTDamount.text = kony.i18n.getLocalizedString("amount");
frmOpenAccTermDeposit.lblDSNickName.text = kony.i18n.getLocalizedString("keyNickname");
frmOpenAccTermDeposit.lblTDToAct.text = kony.i18n.getLocalizedString("keyPayInterest");
	if(flowSpa)
	{
	frmOpenAccTermDeposit.btnTDSelAccNxtSpa.text = kony.i18n.getLocalizedString("Next");
	}
	else frmOpenAccTermDeposit.btnTDSelAccNxt.text = kony.i18n.getLocalizedString("Next");

	if (kony.i18n.getCurrentLocale() == "en_US") {
	frmOpenAccTermDeposit.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
	}else{
	frmOpenAccTermDeposit.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
	}
frmOpenAccTermDeposit.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
	if(flowSpa)
		frmOpenAccTermDeposit.btnTDSelAccNxtSpa.text =  kony.i18n.getLocalizedString("Next");
	else
		frmOpenAccTermDeposit.btnTDSelAccNxt.text =  kony.i18n.getLocalizedString("Next");
		frmOpenAccTermDeposit.btnOATDConfCancel.text =  kony.i18n.getLocalizedString("Back");
	if (gblisDisToActTD == "Y")
	{	
		if(flowSpa)
		{
		frmOpenAccTermDeposit.hbxTranLandToSel.setVisibility(true);
		}
		frmOpenAccTermDeposit.hbxSliderToTD.setVisibility(true);
		frmOpenAccTermDeposit.segToTDAct.setVisibility(true);
		
		showTDToActs();
	}else{
		
			frmOpenAccTermDeposit.segToTDAct.widgetDataMap = {
			lblACno: "lblACno",
			lblAcntType: "lblAcntType",
			img1: "img1",
			lblCustName: "lblCustName",
			lblBalance: "lblBalance",
			lblActNoval: "lblActNoval",
			lblDummy: "lblDummy",
			lblSliderAccN1: "lblSliderAccN1",
			lblSliderAccN2: "lblSliderAccN2",
			lblSliderBranch: "lblSliderBranch",
			lblSliderBranchVal: "lblSliderBranchVal",
			imgHidden: "imgHidden"
			}
		
			frmOpenAccTermDeposit.hbxSliderToTD.setVisibility(false);
			frmOpenAccTermDeposit.segToTDAct.setVisibility(false);
			if(flowSpa)
			{
				frmOpenAccTermDeposit.hbxTranLandToSel.setVisibility(false);
			}
	}

	frmOpenAccTermDeposit.imgOATDProd.src = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
	if (kony.i18n.getCurrentLocale() == "en_US") {
	frmOpenAccTermDeposit.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
	}else{
	frmOpenAccTermDeposit.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
	}
	frmOpenAccTermDeposit.txtTDamount.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
	frmOpenAccTermDeposit.txtTDamount.skin = txtNormalBG;
	frmOpenAccTermDeposit.txtDSNickName.skin = txtNormalBG;
	
		if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != ""){
		frmOpenAccTermDeposit.txtDSNickName.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
		}else{
		frmOpenAccTermDeposit.txtDSNickName.maxTextLength = 20;
		}	
		
	
	showTDFromActs();
	
}

function frmOpenAccountSavingCareMBPreShow() {
changeStatusBarColor();
preShowISavingCare();
var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenAccountSavingCareMB.lblSavinCareSelProdTxt.text = gblFinActivityLogOpenAct["prodNameEN"];
							
						} else {
							frmOpenAccountSavingCareMB.lblSavinCareSelProdTxt.text = gblFinActivityLogOpenAct["prodNameTH"];
						}
								if (arrbenf1.length > 0) {
						            frmOpenAccountSavingCareMB.lblBenRefVal1.text = RelationI18n(arrbenf1);
						        }
						        if (arrbenf2.length > 0) {
						            frmOpenAccountSavingCareMB.lblBenRefVal2.text = RelationI18n(arrbenf2);
						        }
						        if (arrbenf3.length > 0) {
						            frmOpenAccountSavingCareMB.lblBenRefVal3.text = RelationI18n(arrbenf3);
						        }
						        if (arrbenf4.length > 0) {
						            frmOpenAccountSavingCareMB.lblBenRefVal4.text = RelationI18n(arrbenf4);
						        }
						        if (arrbenf5.length > 0) {
						            frmOpenAccountSavingCareMB.lblBenRefVal5.text = RelationI18n(arrbenf5);
					   			}
			  
			  var ifAccounts = true;
				dsSelActSelIndex = [0, 0];
				dsSelActLength = 0;
	if(flowSpa)
	{
		ifAccounts = setDataforOpenFromActs(frmOpenAccountSavingCareMB.segSliderSpa);
		if (ifAccounts == false){
	    return false;
	    }
	}
	else
	{
		
	    ifAccounts = setDataforOpenFromActs(frmOpenAccountSavingCareMB.segSlider,frmOpenAccountSavingCareMB.btnLtArrow,frmOpenAccountSavingCareMB.btnRtArrow);
	    if (ifAccounts == false){
	    return false;
	    }
	
	frmOpenAccountSavingCareMB.segSlider.selectedIndex = dsSelActSelIndex;
	dsSelActLength = frmOpenAccountSavingCareMB.segSlider.data.length - 1;
	}
  
  
  
}


function frmOpenAccountNSConfirmationPreShow() {
changeStatusBarColor();
	frmOpenAccountNSConfirmation.lblNickName.text = kony.i18n.getLocalizedString("Nickname");
	frmOpenAccountNSConfirmation.lblProdType.text = kony.i18n.getLocalizedString("ProductName");
	frmOpenAccountNSConfirmation.lblAccuntNo.text = kony.i18n.getLocalizedString("AccountNo");
	frmOpenAccountNSConfirmation.lblAccounName.text = kony.i18n.getLocalizedString("AccountName");
	frmOpenAccountNSConfirmation.lblBranch.text = kony.i18n.getLocalizedString("Branch");
	frmOpenAccountNSConfirmation.lblOpeningAmt.text = kony.i18n.getLocalizedString("keyLblOpnAmount");
	frmOpenAccountNSConfirmation.lblOpeningDate.text = kony.i18n.getLocalizedString("keyLblOpeningDate");
	frmOpenAccountNSConfirmation.lblTransactionref.text = kony.i18n.getLocalizedString("keyTransactionRefNo");
	frmOpenAccountNSConfirmation.lblOpenAccountFrom.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
	frmOpenAccountNSConfirmation.lblHide.text = kony.i18n.getLocalizedString("Hide");
	frmOpenAccountNSConfirmation.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
	frmOpenAccountNSConfirmation.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi");
	frmOpenAccountNSConfirmation.lblAddressHeader.text=appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
	frmOpenAccountNSConfirmation.lblCardFee.text=kony.i18n.getLocalizedString("keyEntranceFee");
	frmOpenAccountNSConfirmation.label474195740167888.text=kony.i18n.getLocalizedString("CardType");
	var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenAccountNSConfirmation.lblProdNSConfName.text = gblFinActivityLogOpenAct["prodNameEN"];
							frmOpenAccountNSConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] +":";
							frmOpenAccountNSConfirmation.lblBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
					        frmOpenAccountNSConfirmation.lblNFAccName.text = gblFinActivityLogOpenAct["nickEN"];
					        frmOpenAccountNSConfirmation.lblProdTypeVal.text = gblFinActivityLogOpenAct["prodNameEN"];
					        
					        	if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenAccountNSConfirmation.lblNFAccBal.text = kony.i18n.getLocalizedString("SavingMB");
								}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenAccountNSConfirmation.lblNFAccBal.text = kony.i18n.getLocalizedString("CurrentMB");
								}
					
						} else {
							frmOpenAccountNSConfirmation.lblProdNSConfName.text = gblFinActivityLogOpenAct["prodNameTH"];
							frmOpenAccountNSConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"]+":";
							frmOpenAccountNSConfirmation.lblBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
					        frmOpenAccountNSConfirmation.lblNFAccName.text = gblFinActivityLogOpenAct["nickTH"];
					        frmOpenAccountNSConfirmation.lblProdTypeVal.text =gblFinActivityLogOpenAct["prodNameTH"];
					        if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
									frmOpenActSavingCareCnfNAck.lblOASCFrmTypeAck.text = kony.i18n.getLocalizedString("SavingMB");
								}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenActSavingCareCnfNAck.lblOASCFrmTypeAck.text = kony.i18n.getLocalizedString("CurrentMB");
								}
					       if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "SDA"){
					       		frmOpenAccountNSConfirmation.lblNFAccBal.text = kony.i18n.getLocalizedString("SavingMB");
							}else if(gblFinActivityLogOpenAct["actTypeFromOpen"] == "DDA"){
								 	frmOpenAccountNSConfirmation.lblNFAccBal.text = kony.i18n.getLocalizedString("CurrentMB");
							}
						}
	
	if (frmOpenAccountNSConfirmation.hbxOpenActNotify.isVisible == true){
	
	frmOpenAccountNSConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
	frmOpenAccountNSConfirmation.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("S2S_BalAfterTrans");
	
	
						if(gblSelOpenActProdCode == "220"){
						 frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyNoFeeGreet');
						} else if(gblSelOpenActProdCode == "200"){ 
						frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyNormalSavGreet');
						} else if(gblSelOpenActProdCode == "221"){ 
						frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyNoFixedGreet');
						} else if(gblSelOpenActProdCode == "222" ){
						frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyFreeFlowGreet');
						} else if(gblSelOpenActProdCode == "225"){
							frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyAllFreeGreet');
						}
	if(flowSpa)
	{
		  popOtpSpa.btnPopUpTractCancel.text=  kony.i18n.getLocalizedString("keyRequest");
	  popOtpSpa.lblOTP.text=  kony.i18n.getLocalizedString("keyOTP");
	  popOtpSpa.txttokenspa.placeholder=  kony.i18n.getLocalizedString("enterToken");
	  popOtpSpa.label474165697415022.text=  kony.i18n.getLocalizedString("keyOTP");
	  popOtpSpa.button474165697415035.text=  kony.i18n.getLocalizedString("keySwitchToSMS");	
	  frmOpenAccountNSConfirmation.btnOpenMoreSpa.text = kony.i18n.getLocalizedString("btnOpenMore");
	  frmOpenAccountNSConfirmation.buttonBackSpa.text = kony.i18n.getLocalizedString("keybtnReturn");
	}
	else
	{
	  frmOpenAccountNSConfirmation.btnOpenMore.text = kony.i18n.getLocalizedString("btnOpenMore");
	  frmOpenAccountNSConfirmation.buttonBack.text = kony.i18n.getLocalizedString("keybtnReturn");
	}
	}else{
	frmOpenAccountNSConfirmation.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
	frmOpenAccountNSConfirmation.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("keyBalanceBeforTrans");
	if(flowSpa)
	{
	  frmOpenAccountNSConfirmation.btnConfirmSpa.text = kony.i18n.getLocalizedString("keyConfirm");
	  frmOpenAccountNSConfirmation.btnCancelSpa.text = kony.i18n.getLocalizedString("keyCancelButton");
	}
	else
	{
	  frmOpenAccountNSConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	  frmOpenAccountNSConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	}
	}
    ChangeCampaignLocale();
}

function frmMBFTViewPreShow(){
changeStatusBarColor();
	frmMBFTView.lblHdrTxt.text = kony.i18n.getLocalizedString("keyViewTransferMB");
	frmMBFTView.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmMBFTView.lblTrnsfr.text = kony.i18n.getLocalizedString("keyTransferTo");
	frmMBFTView.lblTransferDetails.text =  kony.i18n.getLocalizedString("keyTransferDetails");
	frmMBFTView.lblBankName.text = kony.i18n.getLocalizedString("keyBank");
	frmMBFTView.lblFTViewAmount.text = kony.i18n.getLocalizedString("keyAmount");
	frmMBFTView.lblFee.text = kony.i18n.getLocalizedString("keyFee");
	frmMBFTView.lblOrderDate.text = kony.i18n.getLocalizedString("keyOrderDAteFTMB");
	frmMBFTView.lblTransferScheduleMB.text = kony.i18n.getLocalizedString("keyTransferScheduleMB");
	frmMBFTView.lblStartOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmMBFTView.lblRepeatAs.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmMBFTView.lblEndOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmMBFTView.lblExcute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmMBFTView.lblRemaining.text = kony.i18n.getLocalizedString("keyRemainingMB");
	var times = frmMBFTView.lblRemainingVal.text.split(" ")[0];
	frmMBFTView.lblRemainingVal.text = times + " "+ kony.i18n.getLocalizedString("keyTimesMB") +")";
	
	frmMBFTView.lblTimes.text = kony.i18n.getLocalizedString("keyTimesMB");
	frmMBFTView.lblNotifyRecipient.text = kony.i18n.getLocalizedString("Transfer_notifyRecipent");
	frmMBFTView.lblNoteToRecipient.text = kony.i18n.getLocalizedString("Transfer_noteToRecipent");
	frmMBFTView.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmMBFTView.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
        frmMBFTView.lblFrmAccntNickName.text =  gblFrmAccntNickEN;             
	}else{
		if(gblFrmAccntNickTH != ""){
			frmMBFTView.lblFrmAccntNickName.text = gblFrmAccntNickTH;
		}else
			frmMBFTView.lblFrmAccntNickName.text = gblFrmAccntNickEN;
	}
	
	if(gblFTViewRepeatAsValMB == "Daily"){
	   frmMBFTView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyDaily");	   
	}else if(gblFTViewRepeatAsValMB == "Weekly"){
	    frmMBFTView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
	}else if(gblFTViewRepeatAsValMB == "Monthly"){
	    frmMBFTView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
	}else if(gblFTViewRepeatAsValMB == "Yearly"){
	    frmMBFTView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
	} else
		frmMBFTView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce");
	
	
	  if(flowSpa){
	     frmMBFTView.btnConfirmSPA.text =  kony.i18n.getLocalizedString("returnMB");
	  }else{
	     frmMBFTView.btnConfirm.text = kony.i18n.getLocalizedString("returnMB");
	  }
	gblLocale = false;
}

function frmMBFTEditPreShow(){
changeStatusBarColor();
	frmMBFTEdit.lblHdrTxt.text = kony.i18n.getLocalizedString("keyEditFTMB");
	frmMBFTEdit.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmMBFTEdit.lblFTEditAftrAvailBal.text = kony.i18n.getLocalizedString("keyAvailableBalanceMB");
	frmMBFTEdit.lblTrnsfr.text = kony.i18n.getLocalizedString("keyTransferTo");
	frmMBFTEdit.lblTransferDetails.text =  kony.i18n.getLocalizedString("keyTransferDetails");
	frmMBFTEdit.lblBankName.text = kony.i18n.getLocalizedString("keyBank");
	frmMBFTEdit.lblFTViewAmount.text = kony.i18n.getLocalizedString("keyAmount");
	frmMBFTEdit.lblFee.text = kony.i18n.getLocalizedString("keyFee");
	frmMBFTEdit.lblOrderDate.text = kony.i18n.getLocalizedString("keyOrderDAteFTMB");
	frmMBFTEdit.lblTransferScheduleMB.text = kony.i18n.getLocalizedString("keyTransferScheduleMB");
	frmMBFTEdit.lblStartOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmMBFTEdit.lblRepeatAs.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmMBFTEdit.lblEndOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmMBFTEdit.lblExcute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmMBFTEdit.lblRemaining.text = kony.i18n.getLocalizedString("keyRemainingMB");
	
	var times = frmMBFTEdit.lblRemainingVal.text.split(" ")[0];
	frmMBFTEdit.lblRemainingVal.text = times + " "+ kony.i18n.getLocalizedString("keyTimesMB") +")";
	
	frmMBFTEdit.lblTimesEdit.text = kony.i18n.getLocalizedString("keyTimesMB");
	frmMBFTEdit.lblNotifyRecipient.text = kony.i18n.getLocalizedString("Transfer_notifyRecipent");
	frmMBFTEdit.lblNoteToRecipient.text = kony.i18n.getLocalizedString("Transfer_noteToRecipent");
	frmMBFTEdit.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmMBFTEdit.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
        frmMBFTEdit.lblFrmAccntNickName.text =  gblFrmAccntNickEN;             
	}else{
		if(gblFrmAccntNickTH != ""){
			frmMBFTEdit.lblFrmAccntNickName.text = gblFrmAccntNickTH;
		}else
			frmMBFTEdit.lblFrmAccntNickName.text = gblFrmAccntNickEN;
	}

	if(gblRepeatAsCopy == "Daily"){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyDaily");	   
	}else if(gblRepeatAsCopy == "Weekly"){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
	}else if(gblRepeatAsCopy == "Monthly"){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
	}else if(gblRepeatAsCopy == "Yearly"){
	    frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
	} else 
		frmMBFTEdit.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce");
	  if(flowSpa){
	     frmMBFTEdit.btnCancelSPA.text = kony.i18n.getLocalizedString("keyCancelButton");
	     frmMBFTEdit.btnConfirmSPA.text = kony.i18n.getLocalizedString("Next");
	  }else{
	     frmMBFTEdit.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	     frmMBFTEdit.btnConfirm.text = kony.i18n.getLocalizedString("Next");
	  }
	gblLocale = false;
}

function frmMBFTEditCnfrmtnPreShow(){
changeStatusBarColor();
    frmMBFTEditCnfrmtn.lblHdrTxt.text = kony.i18n.getLocalizedString("keyFTConfrmatnMB");
	frmMBFTEditCnfrmtn.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmMBFTEditCnfrmtn.lblTrnsfr.text = kony.i18n.getLocalizedString("keyTransferTo");
	frmMBFTEditCnfrmtn.lblTransferDetails.text =  kony.i18n.getLocalizedString("keyTransferDetails");
	frmMBFTEditCnfrmtn.lblBankName.text = kony.i18n.getLocalizedString("keyBank");
	frmMBFTEditCnfrmtn.lblFTViewAmount.text = kony.i18n.getLocalizedString("keyAmount");
	frmMBFTEditCnfrmtn.lblFee.text = kony.i18n.getLocalizedString("keyFee");
	frmMBFTEditCnfrmtn.lblOrderDate.text = kony.i18n.getLocalizedString("keyOrderDAteFTMB");
	frmMBFTEditCnfrmtn.lblTransferScheduleMB.text = kony.i18n.getLocalizedString("keyTransferScheduleMB");
	frmMBFTEditCnfrmtn.lblStartOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmMBFTEditCnfrmtn.lblRepeatAs.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmMBFTEditCnfrmtn.lblEndOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmMBFTEditCnfrmtn.lblExcute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmMBFTEditCnfrmtn.lblRemaining.text = kony.i18n.getLocalizedString("keyRemainingMB");
	
	var times = frmMBFTEditCnfrmtn.lblRemainingVal.text.split(" ")[0];
	frmMBFTEditCnfrmtn.lblRemainingVal.text = times + " "+ kony.i18n.getLocalizedString("keyTimesMB") +")";
	
	frmMBFTEditCnfrmtn.lblTimesConf.text = kony.i18n.getLocalizedString("keyTimesMB");
	
	frmMBFTEditCnfrmtn.lblNotifyRecipient.text = kony.i18n.getLocalizedString("Transfer_notifyRecipent");
	frmMBFTEditCnfrmtn.lblNoteToRecipient.text = kony.i18n.getLocalizedString("Transfer_noteToRecipent");
	frmMBFTEditCnfrmtn.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmMBFTEditCnfrmtn.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
        frmMBFTEditCnfrmtn.lblFrmAccntNickName.text =  gblFrmAccntNickEN;             
	}else{
		if(gblFrmAccntNickTH != ""){
			frmMBFTEditCnfrmtn.lblFrmAccntNickName.text = gblFrmAccntNickTH;
		}else
			frmMBFTEditCnfrmtn.lblFrmAccntNickName.text = gblFrmAccntNickEN;
	}
	
	
	if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyDaily")){
	   frmMBFTEditCnfrmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyDaily");	   
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyWeekly")){
	    frmMBFTEditCnfrmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyMonthly")){
	    frmMBFTEditCnfrmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyYearly")){
	    frmMBFTEditCnfrmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
	} else
		frmMBFTEditCnfrmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce");
	
	
	  if(flowSpa){
	     frmMBFTEditCnfrmtn.btnCancelSPA.text = kony.i18n.getLocalizedString("keyCancelButton");
	     frmMBFTEditCnfrmtn.btnConfirmSPA.text = kony.i18n.getLocalizedString("keyConfirm");
	  }else{
	     frmMBFTEditCnfrmtn.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	     frmMBFTEditCnfrmtn.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	  }
	gblLocale = false;
}

function frmMBFTEditCmpletePreShow(){
changeStatusBarColor();
    frmMBFTEditCmplete.lblHdrTxt.text = kony.i18n.getLocalizedString("keyFTUpdatedMB");
	frmMBFTEditCmplete.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmMBFTEditCmplete.lblTrnsfr.text = kony.i18n.getLocalizedString("keyTransferTo");
	frmMBFTEditCmplete.lblTransferDetails.text =  kony.i18n.getLocalizedString("keyTransferDetails");
	frmMBFTEditCmplete.lblBankName.text = kony.i18n.getLocalizedString("keyBank");
	frmMBFTEditCmplete.lblFTViewAmount.text = kony.i18n.getLocalizedString("keyAmount");
	frmMBFTEditCmplete.lblFee.text = kony.i18n.getLocalizedString("keyFee");
	frmMBFTEditCmplete.lblOrderDate.text = kony.i18n.getLocalizedString("keyOrderDAteFTMB");
	frmMBFTEditCmplete.lblTransferScheduleMB.text = kony.i18n.getLocalizedString("keyTransferScheduleMB");
	frmMBFTEditCmplete.lblStartOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmMBFTEditCmplete.lblRepeatAs.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmMBFTEditCmplete.lblEndOnDate.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmMBFTEditCmplete.lblExcute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmMBFTEditCmplete.lblRemaining.text = kony.i18n.getLocalizedString("keyRemainingMB");
	
	var times = frmMBFTEditCmplete.lblRemainingVal.text.split(" ")[0];
	frmMBFTEditCmplete.lblRemainingVal.text = times + " "+ kony.i18n.getLocalizedString("keyTimesMB") +")";
	
	frmMBFTEditCmplete.lblTimesComp.text = kony.i18n.getLocalizedString("keyTimesMB");
	
	frmMBFTEditCmplete.lblNotifyRecipient.text = kony.i18n.getLocalizedString("Transfer_notifyRecipent");
	frmMBFTEditCmplete.lblNoteToRecipient.text = kony.i18n.getLocalizedString("Transfer_noteToRecipent");
	frmMBFTEditCmplete.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmMBFTEditCmplete.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
        frmMBFTEditCmplete.lblFrmAccntNickName.text =  gblFrmAccntNickEN;             
	}else{
		if(gblFrmAccntNickTH != ""){
			frmMBFTEditCmplete.lblFrmAccntNickName.text = gblFrmAccntNickTH;
		}else
			frmMBFTEditCmplete.lblFrmAccntNickName.text = gblFrmAccntNickEN;
	}
	
	if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyDaily")){
	   frmMBFTEditCmplete.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyDaily");	   
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyWeekly")){
	    frmMBFTEditCmplete.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyMonthly")){
	    frmMBFTEditCmplete.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
	}else if(gblScheduleRepeatFTMB == kony.i18n.getLocalizedString("keyYearly")){
	    frmMBFTEditCmplete.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
	} else
		frmMBFTEditCmplete.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce");
	
	
	  if(flowSpa){
	     frmMBFTEditCmplete.btnReturnSPA.text = kony.i18n.getLocalizedString("returnMB");
	  }else{
	     frmMBFTEditCmplete.btnReturn1.text = kony.i18n.getLocalizedString("returnMB");
	  }
	  ChangeCampaignLocale();
	gblLocale = false;
}

function frmMBFtSchedulePreShow(){
changeStatusBarColor();
	frmMBFtSchedule.lblSchedule.text=kony.i18n.getLocalizedString("keyScheduledTranferMB");
	frmMBFtSchedule.lblDate.text = kony.i18n.getLocalizedString("Transfer_Date");
	frmMBFtSchedule.lblRepeat.text = kony.i18n.getLocalizedString("keyRepeat");
	frmMBFtSchedule.btnDaily.text = kony.i18n.getLocalizedString("keyDaily");
	frmMBFtSchedule.btnWeekly.text = kony.i18n.getLocalizedString("keyWeekly");
	frmMBFtSchedule.btnMonthly.text = kony.i18n.getLocalizedString("keyMonthly");
	frmMBFtSchedule.btnYearly.text = kony.i18n.getLocalizedString("keyYearly");
	frmMBFtSchedule.lblEnd.text = kony.i18n.getLocalizedString("Transfer_Ending");
	frmMBFtSchedule.btnNever.text = kony.i18n.getLocalizedString("keyNever");
	frmMBFtSchedule.btnAfter.text = kony.i18n.getLocalizedString("keyAfter");
	frmMBFtSchedule.btnOnDate.text = kony.i18n.getLocalizedString("keyOnDate");
	frmMBFtSchedule.lblEndAfter.text = kony.i18n.getLocalizedString("keyEndAfter");
	frmMBFtSchedule.lblTimesMB.text = kony.i18n.getLocalizedString("keyTimesMB");
	frmMBFtSchedule.lblNumberOfTimes.text = kony.i18n.getLocalizedString("keyIncludeThisTime");
	frmMBFtSchedule.lblScheduleUntil.text = kony.i18n.getLocalizedString("Transfer_Until");
	frmMBFtSchedule.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmMBFtSchedule.btnSave.text = kony.i18n.getLocalizedString("keysave");
    gblLocale = false;
}


function frmBillPaymentViewPreShow(){
changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
		
		frmBillPaymentView.lblref1.text = findColonLastString(gblRef1LblEN) ? gblRef1LblEN + ":" : gblRef1LblEN;
		frmBillPaymentView.lblref2.text = findColonLastString(gblRef2LblEN) ? gblRef2LblEN + ":" : gblRef2LblEN;
		frmBillPaymentView.lblFromAccountNickname.text = gblCustNickBP_EN;
		frmBillPaymentView.lblBillerName.text = gblbillerNameMB_EN + " (" + gblBillerCompcodeEditMB + ") ";
	}else{
		
		frmBillPaymentView.lblref1.text = findColonLastString(gblRef1LblTH) ? gblRef1LblTH + ":" : gblRef1LblTH;
		frmBillPaymentView.lblref2.text = findColonLastString(gblRef2LblTH) ? gblRef2LblTH + ":" : gblRef2LblTH;
		frmBillPaymentView.lblFromAccountNickname.text = gblCustNickBP_TH;
		frmBillPaymentView.lblBillerName.text = gblbillerNameMB_TH + " (" + gblBillerCompcodeEditMB + ") ";
	}
	//if(!(LocaleController.isFormUpdatedWithLocale(frmBillPaymentView.id))){
	if(gblBPflag){
		frmBillPaymentView.lblHead.text = kony.i18n.getLocalizedString("keyBillPaymentViewMB");
	}else{
		frmBillPaymentView.lblHead.text = kony.i18n.getLocalizedString("keyMBViewTopUP");
	}
	frmBillPaymentView.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmBillPaymentView.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	//frmBillPaymentView.lblref1.text = kony.i18n.getLocalizedString("Ref1");
	//frmBillPaymentView.lblref2.text = kony.i18n.getLocalizedString("Ref2");
		
	if(gblPaynow){
		frmBillPaymentView.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	}else{
		frmBillPaymentView.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	}	
	frmBillPaymentView.lblAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentView.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmBillPaymentView.lblPaymentDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmBillPaymentView.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmBillPaymentView.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmBillPaymentView.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmBillPaymentView.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmBillPaymentView.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmBillPaymentView.lblMyNotes.text = kony.i18n.getLocalizedString("keyMyNote");
	frmBillPaymentView.lblTimesBP.text = kony.i18n.getLocalizedString("keyTimesMB");
	frmBillPaymentView.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	//if(flowSpa){
	//  frmBillPaymentView.btnreturnSPA.text = kony.i18n.getLocalizedString("keyXferBtnRetn");
	//}else
	  frmBillPaymentView.btnreturn.text = kony.i18n.getLocalizedString("keyXferBtnRetn");
	  
	   if (gblOnLoadRepeatAsMB == "Daily") {
            frmBillPaymentView.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyDaily");
       } else if (gblOnLoadRepeatAsMB == "Weekly") {
            frmBillPaymentView.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyWeekly");
       } else if (gblOnLoadRepeatAsMB == "Monthly") {
            frmBillPaymentView.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyMonthly");
       } else if (gblOnLoadRepeatAsMB == kony.i18n.getLocalizedString("keyYearly")) {
            frmBillPaymentView.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyYearly");
       } else {
            frmBillPaymentView.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyOnce");
       }
	//LocaleController.updatedForm(frmBillPaymentView.id);
//	}
	gblLocale = false;
}


function frmBillPaymentEditPreShow(){
changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
		
		frmBillPaymentEdit.lblref1.text = findColonLastString(gblRef1LblEN) ? gblRef1LblEN + ":" : gblRef1LblEN;
		frmBillPaymentEdit.lblref2.text = findColonLastString(gblRef2LblEN) ? gblRef2LblEN + ":" : gblRef2LblEN;
		frmBillPaymentEdit.lblFromAccountNickname.text = gblCustNickBP_EN;
		//frmBillPaymentEdit.lblBillerName.text = frmBillPaymentView.lblBillerName.text 
		//frmBillPaymentView.lblBillerName.text = gblbillerNameMB_EN + " (" + gblBillerCompcodeEditMB + ") ";
	}else{
		
		frmBillPaymentEdit.lblref1.text = findColonLastString(gblRef1LblTH) ? gblRef1LblTH + ":" : gblRef1LblTH;
		frmBillPaymentEdit.lblref2.text = findColonLastString(gblRef2LblTH) ? gblRef2LblTH + ":" : gblRef2LblTH;
		frmBillPaymentEdit.lblFromAccountNickname.text = gblCustNickBP_TH;
		//frmBillPaymentView.lblBillerName.text = gblbillerNameMB_TH + " (" + gblBillerCompcodeEditMB + ") ";
	}
	if(gblBPflag){
		frmBillPaymentEdit.lblHead.text = kony.i18n.getLocalizedString("keyEditBillPaymentMB");
	}else{
		frmBillPaymentEdit.lblHead.text = kony.i18n.getLocalizedString("KeyMBEditTopUP");//"Edit Top UP";
	}
	if(!(LocaleController.isFormUpdatedWithLocale(frmBillPaymentEdit.id))){
    frmBillPaymentEdit.lblHead.text = kony.i18n.getLocalizedString("keyEditBillPaymentMB");
	frmBillPaymentEdit.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmBillPaymentEdit.lblAvailableBalance.text = kony.i18n.getLocalizedString("keyAvailableBalanceMB");
	frmBillPaymentEdit.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	//frmBillPaymentEdit.lblref1.text = kony.i18n.getLocalizedString("Ref1");
	//frmBillPaymentEdit.lblref2.text = kony.i18n.getLocalizedString("Ref2");
	
	if(gblPaynow){
		frmBillPaymentEdit.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	}else{
		frmBillPaymentEdit.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	}
	frmBillPaymentEdit.labelOnlineAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentEdit.labelPenaltyAmt.text = kony.i18n.getLocalizedString("keyPenalty");
	frmBillPaymentEdit.buttonOnlineFull.text = kony.i18n.getLocalizedString("keyBillPaymentFull");
	frmBillPaymentEdit.buttonOnlineSpecified.text = kony.i18n.getLocalizedString("keyBillPaymentSpecified");
	frmBillPaymentEdit.btnfullpay.text = kony.i18n.getLocalizedString("keyBillPaymentFull");
	frmBillPaymentEdit.btnminpay.text = kony.i18n.getLocalizedString("keyBillPaymentMinimum");
	frmBillPaymentEdit.btnspecpay.text = kony.i18n.getLocalizedString("keyBillPaymentSpecified");
	frmBillPaymentEdit.lblTPamount.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentEdit.lblAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentEdit.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmBillPaymentEdit.lblPaymentDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmBillPaymentEdit.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmBillPaymentEdit.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmBillPaymentEdit.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmBillPaymentEdit.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmBillPaymentEdit.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmBillPaymentEdit.lblMyNotes.text = kony.i18n.getLocalizedString("keyMyNote");
	frmBillPaymentEdit.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	/*
	if(flowSpa){
	  frmBillPaymentEdit.btnCancelSPA.text = kony.i18n.getLocalizedString("keyCancelButton");
	  frmBillPaymentEdit.btnConfirmSPA.text = kony.i18n.getLocalizedString("Next");
	}else{
	*/
	  frmBillPaymentEdit.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	  frmBillPaymentEdit.btnConfirm.text = kony.i18n.getLocalizedString("Next");
	//}
	frmBillPaymentEdit.lblTimesEditBP.text = kony.i18n.getLocalizedString("keyTimesMB");
	LocaleController.updatedForm(frmBillPaymentEdit.id);
    }
	var prevFrm = kony.application.getPreviousForm().id;
	//"frmScheduleBillPayEditFuture"
	/*if(prevFrm == "frmBillPaymentView"){
		 if (gblOnLoadRepeatAsMB == "Daily") {
            frmBillPaymentEdit.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyDaily");
         } else if (gblOnLoadRepeatAsMB == "Weekly") {
            frmBillPaymentEdit.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyWeekly");
         } else if (gblOnLoadRepeatAsMB == "Monthly") {
            frmBillPaymentEdit.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyMonthly");
         } else if (gblOnLoadRepeatAsMB == kony.i18n.getLocalizedString("keyYearly")) {
            frmBillPaymentEdit.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyYearly");
         } else {
            frmBillPaymentEdit.labelRepeatAsValue.text = kony.i18n.getLocalizedString("keyOnce");
         }
	}*/
	
	gblLocale = false;
}

function frmEditFutureBillPaymentConfirmPreShow(){
changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
		frmEditFutureBillPaymentConfirm.lblref1.text = findColonLastString(gblRef1LblEN) ? gblRef1LblEN + ":" : gblRef1LblEN;
		frmEditFutureBillPaymentConfirm.lblRef2.text = findColonLastString(gblRef2LblEN) ? gblRef2LblEN + ":" : gblRef2LblEN;
	}else{
		frmEditFutureBillPaymentConfirm.lblref1.text = findColonLastString(gblRef1LblTH) ? gblRef1LblTH + ":" : gblRef1LblTH;
		frmEditFutureBillPaymentConfirm.lblRef2.text = findColonLastString(gblRef2LblTH) ? gblRef2LblTH + ":" : gblRef2LblTH;
	}
	if(!(LocaleController.isFormUpdatedWithLocale(frmEditFutureBillPaymentConfirm.id))){
    frmEditFutureBillPaymentConfirm.lblHead.text = kony.i18n.getLocalizedString("Confirmation");
	frmEditFutureBillPaymentConfirm.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmEditFutureBillPaymentConfirm.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	//frmEditFutureBillPaymentConfirm.lblref1.text = kony.i18n.getLocalizedString("Ref1");
	//frmEditFutureBillPaymentConfirm.lblRef2.text = kony.i18n.getLocalizedString("Ref2");

	if(gblPaynow){
		frmEditFutureBillPaymentConfirm.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	}else{
		frmEditFutureBillPaymentConfirm.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	}
	frmEditFutureBillPaymentConfirm.lblAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmEditFutureBillPaymentConfirm.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmEditFutureBillPaymentConfirm.lblPaymentDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmEditFutureBillPaymentConfirm.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmEditFutureBillPaymentConfirm.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmEditFutureBillPaymentConfirm.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmEditFutureBillPaymentConfirm.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmEditFutureBillPaymentConfirm.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmEditFutureBillPaymentConfirm.lblMyNotes.text = kony.i18n.getLocalizedString("keyMyNote");
	frmEditFutureBillPaymentConfirm.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	frmEditFutureBillPaymentConfirm.lblTimesBP.text = kony.i18n.getLocalizedString("keyTimesMB");
	if(flowSpa){
	   frmEditFutureBillPaymentConfirm.btnCancelSPA.text = kony.i18n.getLocalizedString("keyCancelButton");
	   frmEditFutureBillPaymentConfirm.btnConfirmSPA.text = kony.i18n.getLocalizedString("keyConfirm");
	}else{
	   frmEditFutureBillPaymentConfirm.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	   frmEditFutureBillPaymentConfirm.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	}
	
	/*
	  if (gblOnLoadRepeatAsMB == "Daily") {
            frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyDaily");
                  } else if (gblOnLoadRepeatAsMB == "Weekly") {
            frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyWeekly");
                  } else if (gblOnLoadRepeatAsMB == "Monthly") {
            frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyMonthly");
                  } else if (gblOnLoadRepeatAsMB == "Annually") {
            frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyYearly");
                  } else {
                  frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyOnce");
                  }
	
	*/
	LocaleController.updatedForm(frmEditFutureBillPaymentConfirm.id);
    }
	gblLocale = false;
}

function frmEditFutureBillPaymentCompletePreShow(){
changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	
	if(locale == "en_US"){
	  frmEditFutureBillPaymentComplete.lblRef2.text = findColonLastString(gblRef1LblEN) ? gblRef1LblEN + ":" : gblRef1LblEN;
	  frmEditFutureBillPaymentComplete.lblRef1.text = findColonLastString(gblRef2LblEN) ? gblRef2LblEN + ":" : gblRef2LblEN;
	}else{
	   frmEditFutureBillPaymentComplete.lblRef2.text = findColonLastString(gblRef1LblTH) ? gblRef1LblTH + ":" : gblRef1LblTH;
	   frmEditFutureBillPaymentComplete.lblRef1.text = findColonLastString(gblRef1LblTH) ? gblRef1LblTH + ":" : gblRef1LblTH;
	}
	if(!(LocaleController.isFormUpdatedWithLocale(frmEditFutureBillPaymentComplete.id))){
    frmEditFutureBillPaymentComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
	frmEditFutureBillPaymentComplete.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmEditFutureBillPaymentComplete.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	//frmEditFutureBillPaymentComplete.lblRef1.text = kony.i18n.getLocalizedString("Ref1");
	//frmEditFutureBillPaymentComplete.lblRef2.text = kony.i18n.getLocalizedString("Ref2");

	if(gblPaynow){
		frmEditFutureBillPaymentComplete.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	}else{
		frmEditFutureBillPaymentComplete.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	}
	frmEditFutureBillPaymentComplete.lblAmount.text = kony.i18n.getLocalizedString("keyAmount");
	frmEditFutureBillPaymentComplete.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmEditFutureBillPaymentComplete.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmEditFutureBillPaymentComplete.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmEditFutureBillPaymentComplete.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmEditFutureBillPaymentComplete.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmEditFutureBillPaymentComplete.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmEditFutureBillPaymentComplete.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmEditFutureBillPaymentComplete.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmEditFutureBillPaymentComplete.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	//frmEditFutureBillPaymentComplete.lblMsg.text = kony.i18n.getLocalizedString("ATMmsg");
	frmEditFutureBillPaymentComplete.lblTimesBP.text = kony.i18n.getLocalizedString("keyTimesMB");
	if(flowSpa){
	  frmEditFutureBillPaymentComplete.btnCancelSPA.text = kony.i18n.getLocalizedString("keyReturn");
	}else
	  frmEditFutureBillPaymentComplete.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
	 
	 /*
	  if (gblOnLoadRepeatAsMB == "Daily") {
            frmEditFutureBillPaymentComplete.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyDaily");
                  } else if (gblOnLoadRepeatAsMB == "Weekly") {
            frmEditFutureBillPaymentComplete.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyWeekly");
                  } else if (gblOnLoadRepeatAsMB == "Monthly") {
            frmEditFutureBillPaymentComplete.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyMonthly");
                  } else if (gblOnLoadRepeatAsMB == "Annually") {
            frmEditFutureBillPaymentComplete.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyYearly");
                  } else {
                  frmEditFutureBillPaymentComplete.lblrepeatasvalue.text = kony.i18n.getLocalizedString("keyOnce");
                  }
      */            
	LocaleController.updatedForm(frmEditFutureBillPaymentComplete.id);
    }  
	ChangeCampaignLocale();
	gblLocale = false;
	
	try{
    	frmEditFutureBillPaymentComplete.hbxAdv.setVisibility(false);
    	frmEditFutureBillPaymentComplete.hbxCommon.setVisibility(false);
    	frmEditFutureBillPaymentComplete.imgAd.src="";
    	frmEditFutureBillPaymentComplete.imgTwo.src="";
    	frmEditFutureBillPaymentComplete.gblBrwCmpObject.handleRequest="";
    	frmEditFutureBillPaymentComplete.gblBrwCmpObject.htmlString="";
    	frmEditFutureBillPaymentComplete.gblVbxCmp.remove(gblBrwCmpObject);
       	frmEditFutureBillPaymentComplete.hbxAdv.remove(gblVbxCmp);
       	frmEditFutureBillPaymentComplete.hbxCommon.remove(gblVbxCmp);
    }
    catch(e)
    {
    }
	
}

function frmScheduleBillPayEditFuturePreShow(){
changeStatusBarColor();
if(!(LocaleController.isFormUpdatedWithLocale(frmScheduleBillPayEditFuture.id))){
 frmScheduleBillPayEditFuture.lblSchedule.text = kony.i18n.getLocalizedString("keyBillPaymentSchedule");
 frmScheduleBillPayEditFuture.lblDate.text = kony.i18n.getLocalizedString("Transfer_Date");
 frmScheduleBillPayEditFuture.lblRepeat.text = kony.i18n.getLocalizedString("keyRepeat");
 frmScheduleBillPayEditFuture.btnDaily.text = kony.i18n.getLocalizedString("keyDaily");
 frmScheduleBillPayEditFuture.btnWeekly.text = kony.i18n.getLocalizedString("keyWeekly");
 frmScheduleBillPayEditFuture.btnMonthly.text = kony.i18n.getLocalizedString("keyMonthly");
 frmScheduleBillPayEditFuture.btnYearly.text = kony.i18n.getLocalizedString("keyYearly");
 frmScheduleBillPayEditFuture.lblEnd.text = kony.i18n.getLocalizedString("Transfer_Ending");
 frmScheduleBillPayEditFuture.btnNever.text = kony.i18n.getLocalizedString("keyNever");
 frmScheduleBillPayEditFuture.btnAfter.text = kony.i18n.getLocalizedString("keyAfter");
 frmScheduleBillPayEditFuture.btnOnDate.text = kony.i18n.getLocalizedString("keyOnDate");
 frmScheduleBillPayEditFuture.lblTimesMB.text = kony.i18n.getLocalizedString("keyTimesMB");
 frmScheduleBillPayEditFuture.lblNumberOfTimes.text = kony.i18n.getLocalizedString("keyIncludeThisTime");
 frmScheduleBillPayEditFuture.lblScheduleUntil.text = kony.i18n.getLocalizedString("keyUntil");
 frmScheduleBillPayEditFuture.btnSaveSchedule.text = kony.i18n.getLocalizedString("keysave");
 LocaleController.updatedForm(frmScheduleBillPayEditFuture.id);
}
}

function frmSPALoginPreShow(){
changeStatusBarColor();
	frmSPALogin.txtUserId.placeholder = kony.i18n.getLocalizedString("keySPAUserID");
	frmSPALogin.txtPassword.placeholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
	frmSPALogin.btnLogIn.text = kony.i18n.getLocalizedString("keySPAPreloginLoginButton");
	//frmSPALogin.link502735421204405.text = kony.i18n.getLocalizedString("keyForgotPassword");
	frmSPALogin.link502735421204405.text = kony.i18n.getLocalizedString("keyForgotPassword");
	frmSPALogin.link502735421204465.text = kony.i18n.getLocalizedString("keySPAPreLoginInternetBankingReactivation");
	frmSPALogin.label449200994471754.text = kony.i18n.getLocalizedString("keyEnterCaptcha");
	//frmSPALogin.lblFooter.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf"); - to fix defect 15612
	//frmSPALogin.label474016869588269.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf"); - deleted this label from form
	SPAcopyright_text_display();
	var currentLocale = kony.i18n.getCurrentLocale();
	if (currentLocale == "en_US") {
	frmSPALogin.image2475178741954981.src = "aw_banner_spa.png"
	}else{
	frmSPALogin.image2475178741954981.src = "aw_banner_spat.png"
	}
	 frmSPALogin.txtCaptchaText.text="";
//	frmSPALogin.label503629151205727.text = kony.i18n.getLocalizedString("keySPAPreLoginFinancialFreedom");
//	frmSPALogin.label503629151205728.text = kony.i18n.getLocalizedString("keySPAPreLoginBeyondBoundaries");
//	frmSPALogin.label503629151205729.text = kony.i18n.getLocalizedString("keySPAPreLoginActivationMessage");
//	frmSPALogin.button503629151205731.text = kony.i18n.getLocalizedString("keySPAPreLoginbtnActivateNow");
//frmSPALogin.label4464602187016.text = kony.i18n.getLocalizedString("ATMmsg");
	changeCampaignMBPreLocale(frmSPALogin.imgMBPreLogin,frmSPALogin.hbxAdv);
}
function frmMBActivationIBLoginPreShow(){
	frmMBActivationIBLogin.txtUserId.placeholder = kony.i18n.getLocalizedString("keySPAUserID");
	frmMBActivationIBLogin.txtPassword.placeholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
	frmMBActivationIBLogin.btnLogIn.text = kony.i18n.getLocalizedString("keySPAPreloginLoginButton");
	frmMBActivationIBLogin.label449200994471754.text = kony.i18n.getLocalizedString("keyEnterCaptcha");
    frmMBActivationIBLogin.txtCaptchaText.text="";
	frmMBActivationIBLogin.button4733076528913.text=kony.i18n.getLocalizedString("keyCancelButton");
    frmMBActivationIBLogin.btnLogIn.text=kony.i18n.getLocalizedString("login");
    frmMBActivationIBLogin.lblHdrTxt.text=kony.i18n.getLocalizedString("keyActivateMobileApp");
    frmMBActivationIBLogin.label4742849861214.text=kony.i18n.getLocalizedString("keyStep1Label");
    frmMBActivationIBLogin.lblInfo.text=kony.i18n.getLocalizedString("loginMsgActivation");
}


function frmMBActiEmailDeviceNamePreShow(){
	frmMBActiEmailDeviceName.tbxEmail.placeholder=kony.i18n.getLocalizedString("keyPlzSetEmail");
	frmMBActiEmailDeviceName.tbxDeviceName.placeholder=kony.i18n.getLocalizedString("keyPlzSetDeviceName");
	frmMBActiEmailDeviceName.lblEmailHead.text=kony.i18n.getLocalizedString("email");
	frmMBActiEmailDeviceName.lblDeviceName.text=kony.i18n.getLocalizedString("keyDeviceNameLabel");
	frmMBActiEmailDeviceName.lblEmailMsg.text=kony.i18n.getLocalizedString("keyEmailMsg");
	frmMBActiEmailDeviceName.lblDeviceNameMsg.text=kony.i18n.getLocalizedString("keyDeviceNameHint");
	frmMBActiEmailDeviceName.button4733076528913.text=kony.i18n.getLocalizedString("keyCancelButton");
	frmMBActiEmailDeviceName.btnNext.text=kony.i18n.getLocalizedString("Next");
	frmMBActiEmailDeviceName.lblStep2.text=kony.i18n.getLocalizedString("keyStep2Label");
	if(frmMBActiEmailDeviceName.tbxEmail.isVisible){
		frmMBActiEmailDeviceName.tbxEmail.setFocus(true);
	}else{
		frmMBActiEmailDeviceName.tbxDeviceName.setFocus(true);
	}
}
function frmMBActiAtmIdMobilePreShow(){
	frmMBActiAtmIdMobile.lblHdrTxt.text=kony.i18n.getLocalizedString("keyActivateMobileApp");
	frmMBActiAtmIdMobile.lblIdPassport.text=kony.i18n.getLocalizedString("IDNo");
	frmMBActiAtmIdMobile.lblMobileNo.text=kony.i18n.getLocalizedString("PhNo");
	frmMBActiAtmIdMobile.label4742849861214.text=kony.i18n.getLocalizedString("keyStep1Label");
	frmMBActiAtmIdMobile.tbxAtmNumber.placeholder=kony.i18n.getLocalizedString("Act_ATMPL");
	frmMBActiAtmIdMobile.tbxIdPassport.placeholder=kony.i18n.getLocalizedString("keyMBEnterIDorPassportNo");
	frmMBActiAtmIdMobile.tbxMobileNo.placeholder=kony.i18n.getLocalizedString("keyEnterRegisteredMobileNum");
	frmMBActiAtmIdMobile.lblAtmNumber.text=kony.i18n.getLocalizedString("Act_ATMLabel");
	frmMBActiAtmIdMobile.btnNext.text=kony.i18n.getLocalizedString("Next");
	frmMBActiAtmIdMobile.button4733076528913.text=kony.i18n.getLocalizedString("keyCancelButton");
	frmMBActiAtmIdMobile.label449200994471754.text = kony.i18n.getLocalizedString("keyEnterCaptcha");
    frmMBActiAtmIdMobile.txtCaptchaText.text="";
    frmMBActiAtmIdMobile.lblOption.text = kony.i18n.getLocalizedString("Act_Options");
    frmMBActiAtmIdMobile.lblCreditCardNum.text = kony.i18n.getLocalizedString("Act_CreditNoLabel");
    frmMBActiAtmIdMobile.txtCreditCardNum.placeholder = kony.i18n.getLocalizedString("Act_CreditNoPL");
    if(frmMBActiAtmIdMobile.hbxATM.skin == hbxATMcardfocus){
    	frmMBActiAtmIdMobile.lblCardDesc.text = kony.i18n.getLocalizedString("Act_ATM_Option");
    }	
    else{
    	frmMBActiAtmIdMobile.lblCardDesc.text = kony.i18n.getLocalizedString("Act_CreditCard_Option");
    }	
}

function frmMBSetuseridSPAPreShow()
{
	changeStatusBarColor();	
	if(gblSetPwdSpa)
	{
		frmMBSetuseridSPA.lblHdrTxt.text=kony.i18n.getLocalizedString("userIdPwd");
	}
	else
	{
		frmMBSetuseridSPA.lblHdrTxt.text=kony.i18n.getLocalizedString("userIdPwd");
	}
	frmMBSetuseridSPA.label50285458134.text=kony.i18n.getLocalizedString("keyStep2Label");
	frmMBSetuseridSPA.lblSetInfo.text=kony.i18n.getLocalizedString("keySetInfoLabel");
	frmMBSetuseridSPA.label50285458139.text=kony.i18n.getLocalizedString("keyUserIDPreLogin");
	frmMBSetuseridSPA.lblIBInlineUserIDGuidelines.text=kony.i18n.getLocalizedString("keyIBInlineUserIDGuidelines");
	frmMBSetuseridSPA.label50285458141.text=kony.i18n.getLocalizedString("keySPApassword");
	frmMBSetuseridSPA.lblInlinePasswordGuidelines.text=kony.i18n.getLocalizedString("keyIBInlinePasswordGuidelines");
	frmMBSetuseridSPA.lblConfirmPassword.text=kony.i18n.getLocalizedString("confirmPassword");
	frmMBSetuseridSPA.button50368857830932.text=kony.i18n.getLocalizedString("Next");
}



function frmNotificationHomePreShow(){
changeStatusBarColor();
    if(!(LocaleController.isFormUpdatedWithLocale(frmNotificationHome.id))){
		frmNotificationHome.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblNotification");
		frmNotificationHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
		LocaleController.updatedForm(frmNotificationHome.id);
    }
	frmNotificationHome.btnRight.skin = "btnDeleteDB";
	frmNotificationHome.btnRight.focusSkin = "btnDeleteDB";
	frmNotificationHome.btnRight.onClick=populateNotificationsDeleteState;
	//frmNotificationHome.btnRight.setEnabled(false);
	/*if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo.name == "iPhone Simulator"){
		frmNotificationHome.scrollbox474135225108084.containerHeight = 92;
	}*/
		if(kony.i18n.getCurrentLocale() == "en_US"){
			locale = "en_US";
			populateNotifications();
		}
		else{
			locale = "th_TH";
			populateNotifications();
		}
	
}

function frmNotificationDetailsPreShow(){
changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmNotificationDetails.id))){
	frmNotificationDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblNotificationDetails");
	frmNotificationDetails.lblDate.text = kony.i18n.getLocalizedString("Transfer_Date");
	if(flowSpa){
		frmNotificationDetails.btnbackSpa.text = kony.i18n.getLocalizedString("Back");
		frmNotificationDetails.btnCancelSpa.text = kony.i18n.getLocalizedString("Back");
	}
	else{
		frmNotificationDetails.button4733076529054.text = kony.i18n.getLocalizedString("Back");
		frmNotificationDetails.btnBackRC.text = kony.i18n.getLocalizedString("Back");
	}
	LocaleController.updatedForm(frmNotificationDetails.id);
   }
		if(kony.i18n.getCurrentLocale() == "en_US"){
			locale = "en_US";
			populateNotiDetails();
		}
		else{
			locale = "th_TH";
			populateNotiDetails();
		}
}

function frmInboxHomePreShow(){
changeStatusBarColor();
  	frmInboxHome.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblmessage");
	frmInboxHome.label477746511278619.text = kony.i18n.getLocalizedString("keyCalendarSortBy");
	//frmInboxHome.btnAscenDescend.text = kony.i18n.getLocalizedString("keyAscending");
	//frmInboxHome.btnDateTime.text = kony.i18n.getLocalizedString("MyActivitiesIB_Date");
	frmInboxHome.cbxDateTime.selectedKey = "000";//kony.i18n.getLocalizedString("MyActivitiesIB_Date");
	frmInboxHome.cbxAscenDescend.selectedKey = "000";//kony.i18n.getLocalizedString("keyAscending");	
	SortByOrder = "000";//kony.i18n.getLocalizedString("keyAscending");
	SortByType = "000";//kony.i18n.getLocalizedString("MyActivitiesIB_Date");
			var masDorder= [["000", kony.i18n.getLocalizedString("keyAscending")],
            ["001", kony.i18n.getLocalizedString("keyDescending")]];
        frmInboxHome.cbxAscenDescend.masterData= masDorder;
        
        var masDtype= [["000", kony.i18n.getLocalizedString("MyActivitiesIB_Date")],
            ["001", kony.i18n.getLocalizedString("keySender")]];
            frmInboxHome.cbxDateTime.masterData = masDtype;
	
    LocaleController.updatedForm(frmInboxHome.id);
	frmInboxHome.btnRight.onClick=populateInboxDeleteState;
	frmInboxHome.hbox477746511278660.isVisible = false;
	/*if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo.name == "iPhone Simulator"){
		frmInboxHome.scrollbox474135225108084.containerHeight = 79;
	}*/
		if(kony.i18n.getCurrentLocale() == "en_US"){
			locale = "en_US";
			populateInbox();
		}
		else{
			locale = "th_TH";
			populateInbox();
		}
}

function frmInboxDetailsPreShow(){
changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmInboxDetails.id))){
	frmInboxDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblmessage");
	if(flowSpa){
		frmInboxDetails.lblDate.text = kony.i18n.getLocalizedString("Transfer_Date");
		frmInboxDetails.button58778872837658.text = kony.i18n.getLocalizedString("Back");
	} else{
		frmInboxDetails.lblDate.text = kony.i18n.getLocalizedString("Transfer_Date");
		frmInboxDetails.lblFrom.text = kony.i18n.getLocalizedString("Transfer_from");
		frmInboxDetails.button4733076529054.text = kony.i18n.getLocalizedString("Back");
	}
	frmInboxDetails.button1399939188243213.text = kony.i18n.getLocalizedString("Back");
	frmInboxDetails.btnMBpromo.text = kony.i18n.getLocalizedString("keyClick");
    LocaleController.updatedForm(frmInboxDetails.id);
  }
   
	if(kony.i18n.getCurrentLocale() == "en_US"){
			locale = "en_US";
			populateInboxDetails();
	}
	else{
			locale = "th_TH";
			populateInboxDetails();
	}
}

function frmMBMyActivitiesPreShow(){
	changeStatusBarColor();
	frmMBMyActivities.lblDate.text = getMonthEng(gnCurrCalMonth) + " " + gnCurrCalYear;
	frmMBMyActivities.lblHead.text = kony.i18n.getLocalizedString("MyActivities");
	frmMBMyActivities.btnMonth.text = kony.i18n.getLocalizedString("keyCalendarMonth");
	frmMBMyActivities.btnMonthSpa.text = kony.i18n.getLocalizedString("keyCalendarMonth");
	frmMBMyActivities.btnDay.text = kony.i18n.getLocalizedString("keyCalendarDay");
	frmMBMyActivities.BtnList.text = kony.i18n.getLocalizedString("keyCalendarList");
	frmMBMyActivities.spabtnList.text = kony.i18n.getLocalizedString("keyCalendarList");
	frmMBMyActivities.lblNoActivities.text = kony.i18n.getLocalizedString("keyCalendarNoActivitiesForTxnType");
	frmMBMyActivities.label5894627291160355.text = kony.i18n.getLocalizedString("keyCalendarSun");
	frmMBMyActivities.label5894627291160605.text = kony.i18n.getLocalizedString("keyCalendarMon");
	frmMBMyActivities.label5894627291160617.text = kony.i18n.getLocalizedString("keyCalendarTue");
	frmMBMyActivities.label5894627291160629.text = kony.i18n.getLocalizedString("keyCalendarWed");
	frmMBMyActivities.label5894627291160641.text = kony.i18n.getLocalizedString("keyCalendarThu");
	frmMBMyActivities.label5894627291160653.text = kony.i18n.getLocalizedString("keyCalendarFri");
	frmMBMyActivities.label5894627291160665.text = kony.i18n.getLocalizedString("keyCalendarSat");
	frmMBMyActivities.lblSchedule.text = kony.i18n.getLocalizedString("keyCalendarScheduled");
	frmMBMyActivities.lblDue.text = kony.i18n.getLocalizedString("keyCalendarDue");
	frmMBMyActivities.lblCompleted.text = kony.i18n.getLocalizedString("keyCalendarCompleted");
	frmMBMyActivities.lblFailed.text = kony.i18n.getLocalizedString("keyCalendarFailed");
	frmMBMyActivities.lblPending.text = kony.i18n.getLocalizedString("keyCalendarPending");
	frmMBMyActivities.label473277017409291.text = kony.i18n.getLocalizedString("keyCalendarSortBy");
	gblLocale = false;
}

function frmAppTourPreShow() {
changeStatusBarColor();
	if (!(LocaleController.isFormUpdatedWithLocale(frmAppTour.id))) {
		frmAppTour.lblHdrTxt.text = kony.i18n.getLocalizedString("MenuTour");
		frmAppTour.btnAgree.text = kony.i18n.getLocalizedString("keyContactUs");
		frmAppTour.btnCancel.text = kony.i18n.getLocalizedString("Back");
		var locale = "TH";
		if (kony.i18n.getCurrentLocale() == "en_US") {
			locale = "EN";
		}
		LocaleController.updatedForm(frmAppTour.id);
	}
	if (flowSpa) {
			if (gblTourFlag) {
				//frmAppTour.browserAppTourEN.url = gblCampaignDataEN;
				var urlEN = {
					URL : gblCampaignURLEN,
					requestMethod : constants.BROWSER_REQUEST_METHOD_GET
				};
				var urlThai = {
					URL : gblCampaignURLTH,
					requestMethod : constants.BROWSER_REQUEST_METHOD_GET
				};
				if (kony.i18n.getCurrentLocale() == "en_US") {
					var locale = "EN";
					frmAppTour.browserAppTourEN.setVisibility(true);
					//	frmAppTour.browserAppTourTH.setVisibility(false);
					frmAppTour.browserAppTourEN.requestURLConfig = urlEN;
				} else {
					frmAppTour.browserAppTourEN.setVisibility(true);
					//	frmAppTour.browserAppTourTH.setVisibility(false);
					frmAppTour.browserAppTourEN.requestURLConfig = urlThai;
				}
			} else {
				frmAppTour.browserAppTourEN.url = kony.i18n.getLocalizedString("keyAppTour" + locale);
			}

		} else {
			if (gblTourFlag) {
				var urlEN = {
					URL : gblCampaignURLEN,
					requestMethod : constants.BROWSER_REQUEST_METHOD_GET
				};
				var urlThai = {
					URL : gblCampaignURLTH,
					requestMethod : constants.BROWSER_REQUEST_METHOD_GET
				};
			} else {
				var urlEN = {
					URL : kony.i18n.getLocalizedString("keyAppTourEN"),
					requestMethod : constants.BROWSER_REQUEST_METHOD_GET
				};
				var urlThai = {
					URL : kony.i18n.getLocalizedString("keyAppTourTH"),
					requestMethod : constants.BROWSER_REQUEST_METHOD_GET
				};
			}
			if (kony.i18n.getCurrentLocale() == "en_US") {
				var locale = "EN";
				frmAppTour.browserAppTourEN.setVisibility(true);
				//	frmAppTour.browserAppTourTH.setVisibility(false);
				frmAppTour.browserAppTourEN.requestURLConfig = urlEN;
			} else {
				frmAppTour.browserAppTourEN.setVisibility(true);
				//	frmAppTour.browserAppTourTH.setVisibility(false);
				frmAppTour.browserAppTourEN.requestURLConfig = urlThai;
			}
		}
	
	//if(!flowSpa){
	//   		if(kony.i18n.getCurrentLocale() == "en_US"){
	//			var locale = "EN";
	//			frmAppTour.browserAppTourEN.setVisibility(true);
	//		//	frmAppTour.browserAppTourTH.setVisibility(false);
	//			frmAppTour.browserAppTourEN.requestURLConfig = urlEN;
	//		}
	//		else{
	//			frmAppTour.browserAppTourEN.setVisibility(true);
	//		//	frmAppTour.browserAppTourTH.setVisibility(false);
	//			frmAppTour.browserAppTourEN.requestURLConfig = urlThai;
	//		}
	//   }
	gblTourFlag = false;
}

function frmContactUsMBPreShow(){
changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmContactUsMB.id))){
	frmContactUsMB.button477746511286959.text = kony.i18n.getLocalizedString("keyFeedback");
	frmContactUsMB.button506459299445012.text = kony.i18n.getLocalizedString("keyContactUs");
	frmContactUsMB.button477746511286957.text = kony.i18n.getLocalizedString("keyContactUs");
	frmContactUsMB.button506459299445013.text = kony.i18n.getLocalizedString("keyFeedback");
	frmContactUsMB.lblheader.text =kony.i18n.getLocalizedString("keyContactUs");
	frmContactUsMB.link506459299445023.text   = kony.i18n.getLocalizedString("SeeFAQ");
	frmContactUsMB.label506459299445025.text  = kony.i18n.getLocalizedString("keyWriteUs");
	frmContactUsMB.label506459299445035.text  = kony.i18n.getLocalizedString("keyContactMe");
	frmContactUsMB.btnContactSend.text = kony.i18n.getLocalizedString("keySend");
	frmContactUsMB.label506459299445030.text = kony.i18n.getLocalizedString("cnt_message");
	if (flowSpa){ 
	frmContactUsMB.tbxMessageSPA.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
	frmContactUsMB.tbxCNTBoxSPA.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
	}
    else {
	frmContactUsMB.tbxMessage.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
	frmContactUsMB.tbxCNTBox.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
	}
	//frmContactUsMB.tbxCNTBox.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
	frmContactUsMB.button506459299404188.text = kony.i18n.getLocalizedString("keyTMBCenter");
	frmContactUsMB.button506459299404191.text = kony.i18n.getLocalizedString("SeeFAQ");
	var temp=[];
	temp.push(["1",kony.i18n.getLocalizedString("cnt_topic")]);
	temp.push(["2",kony.i18n.getLocalizedString("Cnt_IB")]);
	temp.push(["3",kony.i18n.getLocalizedString("Cnt_DA")]);
	temp.push(["4",kony.i18n.getLocalizedString("Cnt_LP")]);
	temp.push(["5",kony.i18n.getLocalizedString("Cnt_Compliants")]);
	frmContactUsMB.cmbOption.masterData=temp;
	 
	 var data=[];
     data.push(["1",kony.i18n.getLocalizedString("mobilePhone")]);
	 data.push(["2",kony.i18n.getLocalizedString("email")]);
	 frmContactUsMB.radioGroup.masterData = data;
	 frmContactUsMB.btn1.text = kony.i18n.getLocalizedString("keyFBone");
	 frmContactUsMB.btn2.text = kony.i18n.getLocalizedString("keyFBtwo");
	 frmContactUsMB.btn3.text = kony.i18n.getLocalizedString("keyFBthree");
	 frmContactUsMB.label477746511291445.text = kony.i18n.getLocalizedString("keyAdditionalFB");
	 frmContactUsMB.btnSend.text = kony.i18n.getLocalizedString("keySend");
	 LocaleController.updatedForm(frmContactUsMB.id);
  }
	 if(flowSpa){
	 	frmContactUsMB.tbxCNTBoxSPA.text="";
	 } else {
	 	frmContactUsMB.tbxCNTBox.text="";
	 }
	 frmContactUsMB.hbxContactMe.setVisibility(false);
	 frmContactUsMB.hbxRadioButton.setVisibility(false);
	
	
	if(flowSpa)
	{frmContactUsMB.tbxMessageSPA.text="";}
	else
	{frmContactUsMB.tbxMessage.text = "";
	frmContactUsMB.hbxFB1.skin = "hboxFBBlue";
	frmContactUsMB.hbxFB2.skin = "hboxFBGrey";
	frmContactUsMB.hbxFB3.skin = "hboxFBGrey";
	frmContactUsMB.btn1.skin = "btnDBOzoneXMedWhite142";
	frmContactUsMB.btn2.skin = "btnDBOzoneXMedBlack142";
	frmContactUsMB.btn3.skin = "btnDBOzoneXMedBlack142";}
}


function frmOpenAccTermDepositToSpaPreShow(){

}
function frmCMChgPwdSPAPreShow(){
changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmCMChgPwdSPA.id))){
	frmCMChgPwdSPA.label47428873338427.text = kony.i18n.getLocalizedString("CurrentPwdSPA");
	frmCMChgPwdSPA.label47428873338438.text = kony.i18n.getLocalizedString("NewPasswordIB");
	frmCMChgPwdSPA.label47428873338465.text = kony.i18n.getLocalizedString("confirmPassword");
	frmCMChgPwdSPA.lblinlinechngpwd.text=kony.i18n.getLocalizedString("keyIBInlinePasswordGuidelines");
	frmCMChgPwdSPA.lblHdrTxt.text=kony.i18n.getLocalizedString("keyChangePassword");
	frmCMChgPwdSPA.button4769997143092.text=kony.i18n.getLocalizedString("keyCancelButton");
	frmCMChgPwdSPA.button4769997143093.text=kony.i18n.getLocalizedString("keysave");
    LocaleController.updatedForm(frmCMChgPwdSPA.id);
  }
}
function frmApplyMBSPAPreShow(){
	changeStatusBarColor();
   if(!(LocaleController.isFormUpdatedWithLocale(frmApplyMBSPA.id))){
	if(gblApplyServices == 2){
	var lblText =  kony.i18n.getLocalizedString("MobileAppSpa");
	}
	if(gblApplyServices == 3){
	 var lblText = kony.i18n.getLocalizedString("AddNewDeviceIB");
	}
	if(gblApplyServices == 4){
	var lblText = kony.i18n.getLocalizedString("ResetMBPwd");
	}
	frmApplyMBSPA.lblHead.text=lblText;
    frmApplyMBSPA.lblNumberInformation.text = kony.i18n.getLocalizedString("wewillsendinfo");
    frmApplyMBSPA.lblApplyInternetBankingNote.text = kony.i18n.getLocalizedString("NoteIncorrectMob");
    frmApplyMBSPA.lblApplyInternetBankingNote.text = kony.i18n.getLocalizedString("NoteIncorrectMob");
    frmApplyMBSPA.lnkChangeMobileNo.text = kony.i18n.getLocalizedString('changeMobNo')
    frmApplyMBSPA.btnInternetBankingNext.text = kony.i18n.getLocalizedString('Next')
    LocaleController.updatedForm(frmApplyMBSPA.id);
  }
}

function frmGetTMBTouchPreshow(){
changeStatusBarColor();
	frmGetTMBTouch.lblHead.text=kony.i18n.getLocalizedString("keyMBankinglblMobileBanking");
	frmGetTMBTouch.lblActivateYourDevice.text=kony.i18n.getLocalizedString("keyActivateYourDevice");
	frmGetTMBTouch.lblDetail1.text=kony.i18n.getLocalizedString("keyGetTMBTouchStep1");
	frmGetTMBTouch.lblDetail2.text=kony.i18n.getLocalizedString("keyGetTMBTouchStep2");
	frmGetTMBTouch.lblForgotAccessPin.text=kony.i18n.getLocalizedString("keyForgotAccessPIN");
	frmGetTMBTouch.lblForgotDetail1.text=kony.i18n.getLocalizedString("keyForgotAccessPINStep1");
	frmGetTMBTouch.lblForgotDetail2.text=kony.i18n.getLocalizedString("keyForgotAccessPINStep2");
	frmGetTMBTouch.lblForgotStep1.text=kony.i18n.getLocalizedString("keyGetTMBTouchLabel1");
	frmGetTMBTouch.lblForgotStep2.text=kony.i18n.getLocalizedString("keyGetTMBTouchLabel2");
	frmGetTMBTouch.lblStep1.text=kony.i18n.getLocalizedString("keyGetTMBTouchLabel1");
	frmGetTMBTouch.lblStep2.text=kony.i18n.getLocalizedString("keyGetTMBTouchLabel2");
}

function frmContactusFAQMBPreshow()
{
changeStatusBarColor();
  if(!(LocaleController.isFormUpdatedWithLocale(frmContactusFAQMB.id))){	
	frmContactusFAQMB.label475124774164.text = kony.i18n.getLocalizedString("keyContactUs");
	if(flowSpa){
	frmContactusFAQMB.button506459299404751.text = kony.i18n.getLocalizedString("keyBackSpa");
	}else{
	frmContactusFAQMB.button506459299404751.text = kony.i18n.getLocalizedString("Back");
	}
  	LocaleController.updatedForm(frmContactusFAQMB.id);
  }
}
function frmContactUsCompleteScreenMBPreShow(){
	changeStatusBarColor();
    if (frmContactUsCompleteScreenMB.lblmessage.text != "") {
        var content = frmContactUsCompleteScreenMB.lblmessage.text  
        frmContactUsCompleteScreenMB.lblmessage.text = content;
        
		if(gblContactComplete != undefined || gblContactComplete != ""){
			if(cmbValue[1] != undefined || cmbValue[1] == kony.i18n.getLocalizedString("Cnt_Compliants")){
				frmContactUsCompleteScreenMB.lblmessage.text = kony.i18n.getLocalizedString("keyTMBCompleteData") + " " + gblContactComplete + " " + kony.i18n.getLocalizedString("keyTMBComplete");
			} else {
				if(kony.i18n.getCurrentLocale()=='th_TH'){
					frmContactUsCompleteScreenMB.lblmessage.text = kony.i18n.getLocalizedString("keyTMBCompleteData") + " " + gblContactComplete + " " ;
				} else {
					frmContactUsCompleteScreenMB.lblmessage.text = kony.i18n.getLocalizedString("keyTMBCompleteData") + " " + gblContactComplete + " " + kony.i18n.getLocalizedString("keyShortly");
				}
				
			}
		
		}
		frmContactUsCompleteScreenMB.label475124774164.text = kony.i18n.getLocalizedString("keylblComplete");
		frmContactUsCompleteScreenMB.button506459299454835.text = kony.i18n.getLocalizedString("Back");
        
        frmContactUsCompleteScreenMB.label506459299451093.text = kony.i18n.getLocalizedString("keyWritingus");
    }
}

function frmSpaTokenactivationstartupPreShow(){
	changeStatusBarColor();
    if(!(LocaleController.isFormUpdatedWithLocale(frmSpaTokenactivationstartup.id))){
	frmSpaTokenactivationstartup.lblHdrTxt.text=kony.i18n.getLocalizedString("tokenActivation");
	frmSpaTokenactivationstartup.lblTokenSerialNo.text=kony.i18n.getLocalizedString("tokenSerialNumber");
	frmSpaTokenactivationstartup.btnTokenSNoNext.text=kony.i18n.getLocalizedString("Next");
	//frmSpaTokenactivationstartup.tbxTokenSerialNo.placeholder=kony.i18n.getLocalizedString("tokenSerialNumber");
    LocaleController.updatedForm(frmSpaTokenactivationstartup.id);
    }
	if(frmSpaTokenactivationstartup.tbxTokenSerialNo.isVisible)
	{
		frmSpaTokenactivationstartup.tbxTokenSerialNo.text="";
	}
	if(frmSpaTokenactivationstartup.tbxTokenOTP.isVisible)
	{
		frmSpaTokenactivationstartup.tbxTokenOTP.text="";
	}
}

function frmSpaTokenConfirmationPreshow(){
   if(!(LocaleController.isFormUpdatedWithLocale(frmSpaTokenConfirmation.id))){
	frmSpaTokenConfirmation.lblHdrTxt.text=kony.i18n.getLocalizedString("Complete");
	frmSpaTokenConfirmation.label50550597934910.text=kony.i18n.getLocalizedString("tokenReadyUse");
	LocaleController.updatedForm(frmSpaTokenConfirmation.id);
   }
}


function frmFeedbackCompletePreShow(){
	changeStatusBarColor();
	frmFeedbackComplete.lblHdrTxt.text= kony.i18n.getLocalizedString("Complete");
	frmFeedbackComplete.label476108006293792.text = kony.i18n.getLocalizedString("keyFBMessage");
	frmFeedbackComplete.button101086657957077.text = kony.i18n.getLocalizedString("Back");
	if(flowSpa){
		frmFeedbackComplete.button10403270597268.text = kony.i18n.getLocalizedString("Back");
	}
}

function frmFATCAQuestionnaire1PreShow() {
	changeStatusBarColor();
	if(gblFATCAPage == 1)
	{
		frmFATCAQuestionnaire1.lblHdrTxt.text = "FATCA";
		frmFATCAQuestionnaire1.lblQuestionHrd1.text = kony.i18n.getLocalizedString("keyFATCAPart1Info");
		frmFATCAQuestionnaire1.lblQuestionHrd2.text = kony.i18n.getLocalizedString("keyFATCAPart1");
		frmFATCAQuestionnaire1.lblStep1.text = kony.i18n.getLocalizedString("keyStep1Label");
		frmFATCAQuestionnaire1.lblStep2.text = kony.i18n.getLocalizedString("keyStep2Label");
		frmFATCAQuestionnaire1.lblStep1.skin = "lblBlackSmall";
		frmFATCAQuestionnaire1.lblStep2.skin = "lblWhiteSmall";
		frmFATCAQuestionnaire1.lblStep2.setVisibility(true);
		frmFATCAQuestionnaire1.lblStep1.setVisibility(true);
		frmFATCAQuestionnaire1.imgStep1.src = "step2_1_sel.png";
		frmFATCAQuestionnaire1.imgStep2.src = "step2_2.png";
		if(flowSpa) {
			frmFATCAQuestionnaire1.btnFATCAQues1NextSpa.text = kony.i18n.getLocalizedString("Next");
		}
		else {
			frmFATCAQuestionnaire1.btnFATCAQues1Next.text = kony.i18n.getLocalizedString("Next");
		}
		
		if (kony.i18n.getCurrentLocale() == "en_US") {
			frmFATCAQuestionnaire1.btnEngR.skin = "btnOnFocus";
			frmFATCAQuestionnaire1.btnThaiR.skin = "btnOffFocus";
		}
		if (kony.i18n.getCurrentLocale() == "th_TH") {
			frmFATCAQuestionnaire1.btnEngR.skin = "btnOnNormal";
			frmFATCAQuestionnaire1.btnThaiR.skin = "btnOffNorm";
		}
	}
}

function frmFATCAQuestionnaire2PreShow() {
	kony.print("IN preshow 2: >");
	if(gblFATCAPage == 2)
	{
		frmFATCAQuestionnaire1.lblHdrTxt.text = "FATCA";
		frmFATCAQuestionnaire1.lblQuestionHrd1.text = kony.i18n.getLocalizedString("keyFATCAPart2Info");
		frmFATCAQuestionnaire1.lblQuestionHrd2.text = kony.i18n.getLocalizedString("keyFATCAPart2");
		frmFATCAQuestionnaire1.lblStep2.text = kony.i18n.getLocalizedString("keyStep2Label");
		frmFATCAQuestionnaire1.lblStep1.text = kony.i18n.getLocalizedString("keyStep1Label");
		frmFATCAQuestionnaire1.lblStep2.skin = "lblBlackSmall";
		frmFATCAQuestionnaire1.lblStep1.skin = "lblWhiteSmall";
		frmFATCAQuestionnaire1.lblStep1.setVisibility(true);
		frmFATCAQuestionnaire1.lblStep2.setVisibility(true);
		frmFATCAQuestionnaire1.imgStep1.src = "step2_1.png";
		frmFATCAQuestionnaire1.imgStep2.src = "step2_2_sel.png";
		if(flowSpa) {
			frmFATCAQuestionnaire1.btnFATCAQues1NextSpa.text = kony.i18n.getLocalizedString("Next");
		}
		else {
			frmFATCAQuestionnaire1.btnFATCAQues1Next.text = kony.i18n.getLocalizedString("Next");
		}
	}
	
	if (kony.i18n.getCurrentLocale() == "en_US") {
		frmFATCAQuestionnaire1.btnEngR.skin = "btnOnFocus";
		frmFATCAQuestionnaire1.btnThaiR.skin = "btnOffFocus";
	}
	if (kony.i18n.getCurrentLocale() == "th_TH") {
		frmFATCAQuestionnaire1.btnEngR.skin = "btnOnNormal";
		frmFATCAQuestionnaire1.btnThaiR.skin = "btnOffNorm";
	}
}

function frmFATCATnCPreShow()
{
	changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US")
	{
		frmFATCATnC.btnEngR.skin = "btnOnFocus";
		frmFATCATnC.btnThaiR.skin = "btnOffFocus";
		
	}
	else
	{
		frmFATCATnC.btnEngR.skin = "btnOnNormal";
		frmFATCATnC.btnThaiR.skin = "btnOffNorm";
	}
	if(gblInfoFlow == false)
	{
		if(kony.i18n.getCurrentLocale() == "en_US")
			frmFATCATnC.rTextTandCFATCA.text = GLOBAL_FATCA_TNC_en_US;
	    else
	        frmFATCATnC.rTextTandCFATCA.text = GLOBAL_FATCA_TNC_th_TH;
	 }
	 else
	 	frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCATnCInfo");      
        
	if(gblFATCAUpdateFlag == "0")
	{
		if(gblInfoFlow == false) {
			frmFATCATnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyTermsNConditions");
			frmFATCATnC.hbxTnC.setVisibility(true);
		}
		else {
			frmFATCATnC.lblHdrTxt.text = "FATCA";
			frmFATCATnC.hbxTnC.setVisibility(false);
		}
	}
	else if(gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9" || gblFATCAUpdateFlag == "R" || gblFATCAUpdateFlag == "Z")
	{
		frmFATCATnC.lblHdrTxt.text = "FATCA";
		if(GBL_Fatca_Flow == "login" || GBL_Fatca_Flow == "pushnote")
			frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_001");
		else
		{
			if(gblFATCAUpdateFlag == "Z")	
				frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_003");
			else	frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_005");	
		}
		frmFATCATnC.hbxTnC.setVisibility(false);
	}
	else 
	{
		frmFATCATnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
		if(GBL_Fatca_Flow == "login" || GBL_Fatca_Flow == "pushnote")
			frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_002");
		else		frmFATCATnC.lblGoToBranch.text = kony.i18n.getLocalizedString("keyFATCAError_006");
		frmFATCATnC.hbxTnC.setVisibility(false);
	}	
				
	frmFATCATnC.lblAgreeText.text = kony.i18n.getLocalizedString("keyFATCAAgree");
	frmFATCATnC.btnClose.text = kony.i18n.getLocalizedString("keyFATCAClose");
	
	frmFATCATnC.lblError.text = "Error";
	if(flowSpa) {
		frmFATCATnC.btnNextSpa.text = kony.i18n.getLocalizedString("Next");
		frmFATCATnC.btnAgreeSpa.text = kony.i18n.getLocalizedString("Next");
	}
	else {
		frmFATCATnC.btnNext.text = kony.i18n.getLocalizedString("Next");
		frmFATCATnC.btnAgree.text = kony.i18n.getLocalizedString("Next");
	}
	
	if (kony.i18n.getCurrentLocale() == "en_US") {
		frmFATCATnC.btnEngR.skin = "btnOnFocus";
		frmFATCATnC.btnThaiR.skin = "btnOffFocus";
	}
	if (kony.i18n.getCurrentLocale() == "th_TH") {
		frmFATCATnC.btnEngR.skin = "btnOnNormal";
		frmFATCATnC.btnThaiR.skin = "btnOffNorm";
	}	
}

//new function to fix DEF460
function frmOpenActSelProdPreShowMin()
{

	frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
	frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
	
	if(flowSpa)
	{
		  frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
		  frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
		  frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
		  frmOpenActSelProd.lblUnderForUseTab.text= kony.i18n.getLocalizedString("keyOpenAccountForUseComingSoon");
	}else
	{
		frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
		frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
		frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
		frmOpenActSelProd.lblUnderForUseTab.text= kony.i18n.getLocalizedString("keyOpenAccountForUseComingSoon");
	}

}

function frmMBActivationConfirmMobilePreShow(){
	frmMBActivationConfirmMobile.lblHdrTxt.text=kony.i18n.getLocalizedString("keyActivateMobileApp");
	frmMBActivationConfirmMobile.label4742849861214.text=kony.i18n.getLocalizedString("keyStep1Label");
	frmMBActivationConfirmMobile.lblNumberInformation.text=kony.i18n.getLocalizedString("mobileConfirmMsg");
	frmMBActivationConfirmMobile.lblMobileNo.text=glbMobileNo;
	frmMBActivationConfirmMobile.lblApplyInternetBankingNote.text=kony.i18n.getLocalizedString("mobileNumbercorrectMsg");
	frmMBActivationConfirmMobile.btnNext.text=kony.i18n.getLocalizedString("Next");
	frmMBActivationConfirmMobile.button4733076528913.text=kony.i18n.getLocalizedString("keyCancelButton");
	
}