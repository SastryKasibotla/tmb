function ehFrmIBTranferLP_frmIBTranferLP_preshow(eventobject, neworientation) {
    frmIBTranferLP.tbxXferNTR.maxTextLength = 20;
    //monthClicked = false;
    //weekClicked = false;
    //DailyClicked = false;
    //YearlyClicked = false;
    //frmIBTranferLP.CalendarXferDate.dateEditable=false;//28094
    //frmIBTranferLP.calendarXferUntil.dateEditable=false;//to disable textbox
    /* 
handleLocaleChangeIBTranferLP.call(this);

 */
    setDateOnly();
    //setSelTabBankAccountOrMobile();
};

function ehFrmIBTranferLP_frmIBTranferLP_postshow(eventobject, neworientation) {
    addIBMenu();
    nameofform = kony.application.getCurrentForm();
    nameofform.segMenuOptions.removeAll();
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocusThai";
    } else {
        nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocus";
    }
    frmIBTranferLP.label458266510409652.text = " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    if (!kony.appinit.isIE8) {
        frmIBTranferLP.txbXferAmountRcvd.placeholder = "0.00";
    }
    document.getElementById("CalendarXferDate").setAttribute('readonly', "readonly");
    document.getElementById("calendarXferUntil").setAttribute('readonly', "readonly");
    addNumberCheckListner("txbXferAmountRcvd");
};

function ehFrmIBTranferLP_button1011732624601_onClick(eventobject) {
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
    clearDataOnRcNewRecipientAdditionForm();
    recipientAddFromTransfer = true;
    gblRefreshRcCache = true;
    frmIBMyReceipentsAddContactManually.show();
    startDisplaySelectbankCacheService();
};

function ehFrmIBTranferLP_hbox101262635416685_onClick(eventobject) {
    //frmIBTranferLP.hbox101262635416685.skin = "hbxlightblue";
};

function ehFrmIBTranferLP_CalendarXferDate_onSelection(eventobject, isValidDateSelected) {
    date1 = frmIBTranferLP.CalendarXferDate.formattedDate;
    var today = currentSystemDate();
    if ((parseDate(date1) - parseDate(today)) == 0) {
        clearTransferRepeatOptions();
        clearTransferEndingOptions();
        return;
    }
}

function ehFrmIBTranferLP_btnDaily_onClick(eventobject) {
    if (shouldNotAllowRecurringForTodayDate()) {
        return;
    }
    monthClicked = false;
    weekClicked = false;
    YearlyClicked = false;
    times = 0;
    frmIBTranferLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnYearly.skin = "btnIbTab4RightNrml";
    if (frmIBTranferLP.btnDaily.skin == "btnIBTab4LeftNrml") {
        DailyClicked = true;
        frmIBTranferLP.btnDaily.skin = "btnIBTab4LeftFocus";
        frmIBTranferLP.lblXferEnding.setVisibility(true);
        frmIBTranferLP.hbxRec.setVisibility(true);
    } else {
        DailyClicked = false;
        frmIBTranferLP.btnDaily.skin = "btnIBTab4LeftNrml";
        clearTransferEndingOptions();
    }
}

function ehFrmIBTranferLP_btnWeekly_onClick(eventobject) {
    if (shouldNotAllowRecurringForTodayDate()) {
        return;
    }
    monthClicked = false;
    DailyClicked = false;
    YearlyClicked = false;
    times = 0;
    frmIBTranferLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTranferLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnYearly.skin = "btnIbTab4RightNrml";
    if (frmIBTranferLP.btnWeekly.skin == "btnIBTab4MidNrml") {
        weekClicked = true;
        frmIBTranferLP.btnWeekly.skin = "btnIBTab4MidFocus";
        frmIBTranferLP.lblXferEnding.setVisibility(true);
        frmIBTranferLP.hbxRec.setVisibility(true);
    } else {
        weekClicked = false;
        frmIBTranferLP.btnWeekly.skin = "btnIBTab4MidNrml";
        clearTransferEndingOptions();
    }
}

function ehFrmIBTranferLP_btnMonthly_onClick(eventobject) {
    if (shouldNotAllowRecurringForTodayDate()) {
        return;
    }
    weekClicked = false;
    DailyClicked = false;
    YearlyClicked = false;
    times = 0;
    frmIBTranferLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTranferLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnYearly.skin = "btnIbTab4RightNrml";
    if (frmIBTranferLP.btnMonthly.skin == "btnIBTab4MidNrml") {
        monthClicked = true;
        frmIBTranferLP.btnMonthly.skin = "btnIBTab4MidFocus";
        frmIBTranferLP.lblXferEnding.setVisibility(true);
        frmIBTranferLP.hbxRec.setVisibility(true);
    } else {
        monthClicked = false;
        frmIBTranferLP.btnMonthly.skin = "btnIBTab4MidNrml";
        clearTransferEndingOptions();
    }
}

function ehFrmIBTranferLP_btnYearly_onClick(eventobject) {
    if (shouldNotAllowRecurringForTodayDate()) {
        return;
    }
    monthClicked = false;
    weekClicked = false;
    DailyClicked = false;
    times = 0;
    frmIBTranferLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTranferLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnMonthly.skin = "btnIBTab4MidNrml";
    if (frmIBTranferLP.btnYearly.skin == "btnIbTab4RightNrml") {
        YearlyClicked = true;
        frmIBTranferLP.btnYearly.skin = "btnIBTab4RightFocus";
        frmIBTranferLP.lblXferEnding.setVisibility(true);
        frmIBTranferLP.hbxRec.setVisibility(true);
    } else {
        YearlyClicked = false;
        frmIBTranferLP.btnYearly.skin = "btnIbTab4RightNrml";
        clearTransferEndingOptions();
    }
}

function ehFrmIBTranferLP_btnNever_onClick(eventobject) {
    frmIBTranferLP.btnNever.skin = "btnIBTab3LeftFocus";
    frmIBTranferLP.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBTranferLP.btnAfter.skin = "btnIBTab3MidNrml";
    times = "0";
    frmIBTranferLP.hbxEndCalendar.setVisibility(false);
    frmIBTranferLP.hbxTimes.setVisibility(false);
    frmIBTranferLP.lblENDtext.setVisibility(false);
    frmIBTranferLP.lineAfter.setVisibility(false);
    frmIBTranferLP.txtTimes.text = "";
    frmIBTranferLP.calendarXferUntil.clear();
}

function ehFrmIBTranferLP_btnAfter_onClick(eventobject) {
    frmIBTranferLP.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBTranferLP.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBTranferLP.btnAfter.skin = "btnIBTab3MidFocus";
    frmIBTranferLP.hbxEndCalendar.setVisibility(false);
    frmIBTranferLP.hbxTimes.setVisibility(true);
    frmIBTranferLP.lblENDtext.setVisibility(false);
    //frmIBTranferLP.lblENDtext.text="End After";
    frmIBTranferLP.lineAfter.setVisibility(true);
    times = "1";
    frmIBTranferLP.calendarXferUntil.clear();
}

function ehFrmIBTranferLP_btnOnDate_onClick(eventobject) {
    frmIBTranferLP.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBTranferLP.btnOnDate.skin = "btnIBTab3RightFocus";
    frmIBTranferLP.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBTranferLP.hbxEndCalendar.setVisibility(true);
    frmIBTranferLP.hbxTimes.setVisibility(false);
    frmIBTranferLP.lblENDtext.setVisibility(false);
    frmIBTranferLP.txtTimes.text = "";
    //frmIBTranferLP.lblENDtext.text="End Date";
    //frmIBTranferLP.lineAfter.setVisibility(true);
    times = "2";
}

function clearTransferEndingOptions() {
    frmIBTranferLP.lblXferEnding.setVisibility(false);
    frmIBTranferLP.hbxRec.setVisibility(false);
    frmIBTranferLP.btnNever.skin = "btnIBTab3LeftFocus";
    frmIBTranferLP.btnAfter.skin = "btnIBTab3MidNrml";
    frmIBTranferLP.hbxTimes.setVisibility(false);
    frmIBTranferLP.btnOnDate.skin = "btnIBTab3RightNrml";
    frmIBTranferLP.hbxEndCalendar.setVisibility(false);
    frmIBTranferLP.lineAfter.setVisibility(false);
    frmIBTranferLP.txtTimes.text = "";
    frmIBTranferLP.calendarXferUntil.clear();
}

function clearTransferRepeatOptions() {
    monthClicked = false;
    weekClicked = false;
    DailyClicked = false;
    YearlyClicked = false;
    times = 0;
    frmIBTranferLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTranferLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnYearly.skin = "btnIbTab4RightNrml";
}

function shouldNotAllowRecurringForTodayDate() {
    var selectedDate = frmIBTranferLP.CalendarXferDate.formattedDate;
    var today = currentSystemDate();
    if (!isNotBlank(selectedDate)) {
        alert(kony.i18n.getLocalizedString("KeySelStartDate"));
        return true;
    } else if ((parseDate(selectedDate) - parseDate(today)) == 0) {
        alert(kony.i18n.getLocalizedString("Schedule_TodayErr"));
        return true;
    } else {
        return false;
    }
}

function ehFrmIBTranferLP_calendarXferUntil_onSelection(eventobject, isValidDateSelected) {
    date2 = frmIBTranferLP.calendarXferUntil.formattedDate;
    kony.print("date 2 is" + date2);
};

function ehFrmIBTranferLP_btnCancel_onClick(eventobject) {
    closeRightPanelTransfer();
};

function ehFrmIBTranferLP_btnXferShowContact_onClick(eventobject) {
    showToField();
    toggleArrowonClickFalse();
    frmIBTranferLP.arrowXferToField.setVisibility(true)
    var accno = removeHyphenIB(gblcwselectedData.accountWOF)
        //  frmIBTranferLP.vbox47679117772050.skin = "vboxLightBlue"
    getToRecipientsNewIB(gblcwselectedData.prodCode, accno);
    frmIBTranferLP.button1011732624601.setVisibility(true);
    // frmIBTranferLP.vbox47679117772050.skin = "vboxLightBlue"
    frmIBTranferLP.hbox47679117770859.skin = "hbxProperFocus40px";
};

function ehFrmIBTranferLP_txbXferAmountRcvd_onBeginEditing(eventobject, changedtext) {
    transAmountOnClickIB();
    frmIBTranferLP.hbxAmount.skin = "hbxProperFocus40px"
    frmIBTranferLP.hbox47679117770859.skin = "hbxProper40px";
};

function ehFrmIBTranferLP_txbXferAmountRcvd_onEndEditing(eventobject, changedtext) {
    frmIBTranferLP.txbXferAmountRcvd.text = onDoneEditingAmountValue(frmIBTranferLP.txbXferAmountRcvd.text);
    transAmountOnDoneIB();
    frmIBTranferLP.hbxAmount.skin = "hbxProper40px";
};

function ehFrmIBTranferLP_hbox1011732624591_onClick(eventobject) {
    toggleArrowonClickFalse();
    frmIBTranferLP.txtArMn.setFocus(true)
};

function ehFrmIBTranferLP_txtArMn_onEndEditing() {
    frmIBTranferLP.hbox1011732624591.skin = "hbxProper40px"
    frmIBTranferLP.txtArMn.skin = "txtAreaIBNoScroll"
};

function ehFrmIBTranferLP_txtArMn_onBeginEditing() {
    frmIBTranferLP.hbox1011732624591.skin = "hbxProperFocus40px"
    frmIBTranferLP.txtArMn.skin = "txtAreaIBNoScrollFocus"
};

function ehFrmIBTranferLP_btnScheduledTransfer_onClick(eventobject) {
    showScheduleTransferField();
    toggleArrowonClickFalse();
    frmIBTranferLP.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBTranferLP.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBTranferLP.btnYearly.skin = "btnIbTab4RightNrml";
    frmIBTranferLP.txtTimes.text = "";
    futureCalDateXfersIB();
    frmIBTranferLP.vbox47679117772050.skin = "NoSkin";
    populateSelectedScheduleTransferDetails();
}

function populateSelectedScheduleTransferDetails() {
    var scheduleOrNowDate = frmIBTranferLP.lblXferTransferRange.text.trim();
    if (gblPaynow) {
        scheduleOrNowDate = scheduleOrNowDate.split("/", 3);
        var day = scheduleOrNowDate[0];
        var month = scheduleOrNowDate[1];
        var year = scheduleOrNowDate[2];
        frmIBTranferLP.CalendarXferDate.dateComponents = [day, month, year];
        clearTransferEndingOptions();
    } else {
        var scheduleRepeatOption = frmIBTranferLP.lblXferInterval.text.trim();
        scheduleOrNowDate = scheduleOrNowDate.split(" ");
        scheduleRepeatOption = scheduleRepeatOption.split(" ");
        if (DailyClicked) {
            ehFrmIBTranferLP_btnDaily_onClick();
            populateEndingTransferOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else if (weekClicked) {
            ehFrmIBTranferLP_btnWeekly_onClick();
            populateEndingTransferOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else if (monthClicked) {
            ehFrmIBTranferLP_btnMonthly_onClick();
            populateEndingTransferOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else if (YearlyClicked) {
            ehFrmIBTranferLP_btnYearly_onClick();
            populateEndingTransferOptions(scheduleOrNowDate, scheduleRepeatOption);
        } else {
            //Once
            scheduleOrNowDate = scheduleOrNowDate[0].split("/", 3);
            var day = scheduleOrNowDate[0];
            var month = scheduleOrNowDate[1];
            var year = scheduleOrNowDate[2];
            frmIBTranferLP.CalendarXferDate.dateComponents = [day, month, year];
            clearTransferRepeatOptions();
            clearTransferEndingOptions();
        }
    }
}

function populateEndingTransferOptions(scheduleOrNowDate, scheduleRepeatOption) {
    if (frmIBTranferLP.hbxTimes.isVisible) {
        ehFrmIBTranferLP_btnAfter_onClick();
        frmIBTranferLP.txtTimes.text = scheduleRepeatOption[2];
    } else if (frmIBTranferLP.hbxEndCalendar.isVisible) {
        ehFrmIBTranferLP_btnOnDate_onClick();
        scheduleOrNowDate = scheduleOrNowDate[2].split("/", 3);
        var day = scheduleOrNowDate[0];
        var month = scheduleOrNowDate[1];
        var year = scheduleOrNowDate[2];
        frmIBTranferLP.calendarXferUntil.dateComponents = [day, month, year];
    } else {
        ehFrmIBTranferLP_btnNever_onClick();
    }
}

function ehFrmIBTransferCustomWidgetLP_hbxNoteToRecipent_onClick(eventobject) {
    toggleArrowonClick();
};

function ehFrmIBTranferLP_tbxXferNTR_onBeginEditing(eventobject, changedtext) {
    frmIBTranferLP.hbxNoteToRecipent.skin = "hbxProperFocus40px"
};

function ehFrmIBTranferLP_tbxXferNTR_onEndEditing(eventobject, changedtext) {
    frmIBTranferLP.hbxNoteToRecipent.skin = "hbxProper40px"
};

function ehFrmIBTranferLP_btnXferNext_onClick(eventobject) {
    if (gblSelTransferMode == 1) {
        gblp2pAccountNumber = removeHyphenIB(frmIBTranferLP.lblXferToContactRcvd.text);
        onTransferLndngNextIB();
    } else {
        onTransferLndngNextP2PIB();
    }
    if (!gblPaynow) {
        frmIBTransferNowConfirmation.hbxFreeTransactions.setVisibility(false);
        frmIBTransferNowCompletion.hbxFreeTransactions.setVisibility(false);
    }
};

function ehFrmIBTranferLP_hbxFrom_onClick(eventobject) {
    getFromXferAccountsIB();
    frmIBTranferLP.vbox47679117772050.skin = "vboxWhiteBg"
};

function ehFrmIBFTrnsrView_frmIBFTrnsrView_postshow(eventobject, neworientation) {
    addIBMenu();
    //frmIBFTrnsrView.hbxMenuMyActivities.skin = "hbxIBMyActivitiesMenuFocus"
    pagespecificSubMenu();
    frmIBFTrnsrView.imgTMB.setVisibility(true);
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);
    //document.getElementById("frmIBFTrnsrView_txtNumOfTimes").type = "number";
    addNumberCheckListner("txtNumOfTimes");
    addNumberCheckListner("txtFTEditAmountval");
};

function ehFrmIBFTrnsrView_txtFTEditAmountval_onBeginEditing(eventobject, changedtext) {
    frmIBFTrnsrView.hbxFTEditAmnt.skin = "hbox320pxpaddingfocus";
};

function ehFrmIBFTrnsrView_txtFTEditAmountval_onEndEditing(eventobject, changedtext) {
    if (frmIBFTrnsrView.txtFTEditAmountval.text == "") {
        frmIBFTrnsrView.txtFTEditAmountval.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmIBFTrnsrView.txtFTEditAmountval.text = commaFormatted(parseFloat(removeCommos(frmIBFTrnsrView.txtFTEditAmountval.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    frmIBFTrnsrView.hbxFTEditAmnt.skin = "hbox320pxpadding"
};

function ehFrmIBFTrnsrView_btnFTEdit_onClick(eventobject) {
    frmIBFTrnsrView.calStartDate.dateEditable = false;
    frmIBFTrnsrView.calEndDate.dateEditable = false;
    frmIBFTrnsrView.hbxEditFT.setVisibility(true);
    frmIBFTrnsrView.imgTMB.setVisibility(false);
    var dateSet = frmIBFTrnsrView.lblViewStartOnDateVal.text;
    dateSet = dateSet.split("/", 3);
    var d0 = dateSet[0];
    var d1 = dateSet[1];
    var d2 = dateSet[2];
    frmIBFTrnsrView.calStartDate.dateComponents = [d0, d1, d2];
    if (gblScheduleRepeatFT == "Once") {
        frmIBFTrnsrView.hbxEndingBtns.setVisibility(false);
        frmIBFTrnsrView.lblEnding.setVisibility(false);
        frmIBFTrnsrView.hbxRepeatAs.setVisibility(true);
        frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
        frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false)
        frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
        frmIBFTrnsrView.btnMonthly.skin = btnIBTab4MidNrml
        frmIBFTrnsrView.btnWeekly.skin = btnIBTab4MidNrml
        frmIBFTrnsrView.btnDaily.skin = btnIBTab4LeftNrml
        frmIBFTrnsrView.btnYearly.skin = btnIbTab4RightNrml
        frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
        frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
        frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml
        frmIBFTrnsrView.txtNumOfTimes.text = "";
        gblEndValTemp = "none";
    }
    gblTempStartOnDate = frmIBFTrnsrView.lblViewStartOnDateVal.text;
    gblTempEndOnDate = frmIBFTrnsrView.lblEndOnDateVal.text;
    gblTempExcuteVal = frmIBFTrnsrView.lblExcuteVal.text;
    showEditSchedule();
};

function ehFrmIBFTrnsrView_btnCancel_onClick(eventobject) {
    frmIBFTrnsrView.lblViewStartOnDateVal.text = formatDateFT(gblFTViewStartOnDate);
    frmIBFTrnsrView.lblRepeatAsVal.text = gblFTViewRepeatAsVal;
    var tmpEndDate = gblFTViewEndDate;
    if (gblFTViewEndDate == "-") {
        frmIBFTrnsrView.lblEndOnDateVal.text = "-";
    } else {
        if (tmpEndDate.indexOf("-", 0) != -1) {
            tmpEndDate = formatDateFT(gblFTViewEndDate);
        } else frmIBFTrnsrView.lblEndOnDateVal.text = tmpEndDate;
    }
    frmIBFTrnsrView.lblExcuteVal.text = gblFTViewExcuteVal
    frmIBFTrnsrView.lblRemainingVal.text = gblFTViewExcuteremaing + " )";
    gblIsEditAftr = gblIsEditAftrFrmService
    gblIsEditOnDate = gblIsEditOnDateFrmService
    gblScheduleRepeatFT = gblFTViewRepeatAsVal;
    gblScheduleEndFT = gblFTViewEndVal;
    gblEditFTSchdule = false;
    frmIBFTrnsrView.lblFeeVal.text = gblFTFeeView;
    frmIBFTrnsrView.imgTMB.setVisibility(true);
    if (gblViewRepeatAsLS == "Daily") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyDaily");
    } else if (gblViewRepeatAsLS == "Weekly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyWeekly");
    } else if (gblViewRepeatAsLS == "Monthly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyMonthly");
    } else if (gblViewRepeatAsLS == "Yearly") {
        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyYearly");
    } else frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyOnce");
    gblRepeatAsforLS = gblViewRepeatAsLS;
    //frmIBFTrnsrView.lblToAccntNickName.text = gblToAccntNick;
    frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(false);
    frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(true);
    frmIBFTrnsrView.lblFTEditHdr.setVisibility(false);
    frmIBFTrnsrView.btnFTEdit.setVisibility(false);
    frmIBFTrnsrView.hbxFTViewHdr.setVisibility(true);
    //frmIBFTrnsrView.hbxbtnReturn.setVisibility(true);
    frmIBFTrnsrView.btnReturnView.setVisibility(true)
    frmIBFTrnsrView.hbxEditBtns.setVisibility(false);
    frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(false);
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);
    //New
    frmIBFTrnsrView.hbxTrnsfrDetails2.setVisibility(false);
    frmIBFTrnsrView.hbxTrnsfrDetails2ForView.setVisibility(true);
    frmIBFTrnsrView.btnFTEditFlow.setVisibility(true);
    frmIBFTrnsrView.btnFTDel.setVisibility(true);
    gblIsEditAftr = gblIsEditAftrFrmService;
    gblIsEditOnDate = gblIsEditOnDateFrmService;
    /* 
frmIBFTrnsrView.lblViewStartOnDateVal.text = formatDateFT(gblFTViewStartOnDate);
frmIBFTrnsrView.lblRepeatAsVal.text = gblFTViewRepeatAsVal;

var tmpEndDate = gblFTViewEndDate;
if(gblFTViewEndDate == "-"){
frmIBFTrnsrView.lblEndOnDateVal.text = "-";
}else{
 if(tmpEndDate.indexOf("-", 0) != -1){
 tmpEndDate = formatDateFT(gblFTViewEndDate);
 }else
 frmIBFTrnsrView.lblEndOnDateVal.text = tmpEndDate;
}
 
 
frmIBFTrnsrView.lblExcuteVal.text = gblFTViewExcuteVal
frmIBFTrnsrView.lblRemainingVal.text = gblFTViewExcuteremaing + " )";
 
gblIsEditAftr = gblIsEditAftrFrmService
gblIsEditOnDate = gblIsEditOnDateFrmService
gblScheduleRepeatFT = gblFTViewRepeatAsVal;
gblScheduleEndFT = gblFTViewEndVal;
gblEditFTSchdule = false;

frmIBFTrnsrView.lblFeeVal.text = gblFTFeeView;

frmIBFTrnsrView.imgTMB.setVisibility(true);


 */
};

function ehFrmIBFTrnsrView_btnNxt_onClick(eventobject) {
    isEditSchedule();
    frmIBFTrnsrEditCnfmtn.hbxFTCmpletngHdr.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.imgComplete.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.btnReturnUpdated.setVisibility(false)
    frmIBFTrnsrEditCnfmtn.hbxFTCmpleAvilBal.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(true);
    frmIBFTrnsrEditCnfmtn.hbxFTViewHdr.setVisibility(true);
    frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(true);
    frmIBFTrnsrEditCnfmtn.txtBxOTPFT.setFocus(true);
    frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
};

function ehFrmIBFTrnsrView_btnFTEditFlow_onClick(eventobject) {
    gblCRMProfileInqCal = true;
    frmIBFTrnsrView.hbxTrnsfrDetails2.setVisibility(true);
    frmIBFTrnsrView.hbxTrnsfrDetails2ForView.setVisibility(false);
    frmIBFTrnsrView.lblViewStartOnDateVal.text = frmIBFTrnsrView.lblViewStartOnDateValView.text;
    frmIBFTrnsrView.lblRepeatAsVal.text = frmIBFTrnsrView.lblRepeatAsValView.text;
    frmIBFTrnsrView.lblEndOnDateVal.text = frmIBFTrnsrView.lblEndOnDateValView.text;
    frmIBFTrnsrView.lblExcuteVal.text = frmIBFTrnsrView.lblExcuteValView.text;
    frmIBFTrnsrView.lblRemainingVal.text = frmIBFTrnsrView.lblRemainingValView.text;
    getFeeCrmProfileInq();
    /* 
frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(true);
frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(false);
frmIBFTrnsrView.lblFTEditHdr.setVisibility(true);

frmIBFTrnsrView.btnFTEdit.setVisibility(true);
frmIBFTrnsrView.hbxFTViewHdr.setVisibility(false);
frmIBFTrnsrView.btnReturnView.setVisibility(false)
frmIBFTrnsrView.hbxEditBtns.setVisibility(true);

var tmpAmt = frmIBFTrnsrView.lblFTViewAmountVal.text;
tmpAmt = tmpAmt.substring(0, tmpAmt.length-1)

frmIBFTrnsrView.txtFTEditAmountval.text = tmpAmt;
frmIBFTrnsrView.txtFTEditAmountval.setFocus(true);
frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(true);

 */
    frmIBFTrnsrView.btnFTEdit.padding = [0, 0, 20, 0]
    frmIBFTrnsrView.txtFTEditAmountval.setFocus(true)
};

function ehfrmIBFTrnsrView_btnFTDel_onClick(eventobject) {
    srvCrmProfileInqforFTCancel();
    /* 
popupIBFTDelete.lblConfoMsg.text = "Are you sure to delete this Future Transaction?";


 */
    /* 
activityLogCancelFT.call(this);

 */
    /* 
popupIBFTDelete.show();
	
 */
};

function ehFrmIBFTrnsrView_btnWeekly_onClick(eventobject) {
    frmIBFTrnsrView.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBFTrnsrView.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBFTrnsrView.btnYearly.skin = "btnIbTab4RightNrml";
    if (gblScheduleRepeatFT != kony.i18n.getLocalizedString("Transfer_Weekly")) {
        frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Weekly");
        frmIBFTrnsrView.btnWeekly.skin = "btnIBTab4MidFocus";
        if (gblScheduleEndFT == "none") {
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.lblEnding.setVisibility(true);
        }
    } else {
        gblScheduleRepeatFT = "Once";
        frmIBFTrnsrView.btnWeekly.skin = "btnIBTab4MidNrml";
        clearEndingOptionsEditScheduleTransfer();
    }
};

function ehFrmIBFTrnsrView_btnMonthly_onClick(eventobject) {
    frmIBFTrnsrView.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBFTrnsrView.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBFTrnsrView.btnYearly.skin = "btnIbTab4RightNrml";
    if (gblScheduleRepeatFT != kony.i18n.getLocalizedString("Transfer_Monthly")) {
        frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Monthly");
        frmIBFTrnsrView.btnMonthly.skin = "btnIBTab4MidFocus";
        if (gblScheduleEndFT == "none") {
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.lblEnding.setVisibility(true);
        }
    } else {
        gblScheduleRepeatFT = "Once";
        frmIBFTrnsrView.btnMonthly.skin = "btnIBTab4MidNrml";
        clearEndingOptionsEditScheduleTransfer();
    }
};

function ehFrmIBFTrnsrView_btnYearly_onClick(eventobject) {
    frmIBFTrnsrView.btnDaily.skin = "btnIBTab4LeftNrml";
    frmIBFTrnsrView.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBFTrnsrView.btnMonthly.skin = "btnIBTab4MidNrml";
    if (gblScheduleRepeatFT != kony.i18n.getLocalizedString("Transfer_Yearly")) {
        frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Yearly");
        frmIBFTrnsrView.btnYearly.skin = "btnIBTab4RightFocus";
        if (gblScheduleEndFT == "none") {
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.lblEnding.setVisibility(true);
        }
    } else {
        gblScheduleRepeatFT = "Once";
        frmIBFTrnsrView.btnYearly.skin = "btnIbTab4RightNrml";
        clearEndingOptionsEditScheduleTransfer();
    }
};

function ehFrmIBFTrnsrView_btnDaily_onClick(eventobject) {
    frmIBFTrnsrView.btnWeekly.skin = "btnIBTab4MidNrml";
    frmIBFTrnsrView.btnMonthly.skin = "btnIBTab4MidNrml";
    frmIBFTrnsrView.btnYearly.skin = "btnIbTab4RightNrml";
    if (gblScheduleRepeatFT != kony.i18n.getLocalizedString("Transfer_Daily")) {
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Daily");
        frmIBFTrnsrView.btnDaily.skin = "btnIBTab4LeftFocus";
        if (gblScheduleEndFT == "none") {
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.lblEnding.setVisibility(true);
        }
    } else {
        gblScheduleRepeatFT = "Once";
        frmIBFTrnsrView.btnDaily.skin = "btnIBTab4LeftNrml";
        clearEndingOptionsEditScheduleTransfer();
    }
};

function clearEndingOptionsEditScheduleTransfer() {
    frmIBFTrnsrView.hbxEndingBtns.setVisibility(false);
    frmIBFTrnsrView.lblEnding.setVisibility(false);
    frmIBFTrnsrView.hbxRepeatAs.setVisibility(true);
    frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
    frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false)
    frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
    frmIBFTrnsrView.btnMonthly.skin = btnIBTab4MidNrml;
    frmIBFTrnsrView.btnWeekly.skin = btnIBTab4MidNrml;
    frmIBFTrnsrView.btnDaily.skin = btnIBTab4LeftNrml;
    frmIBFTrnsrView.btnYearly.skin = btnIbTab4RightNrml;
    frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml;
    frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml;
    frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml;
    frmIBFTrnsrView.txtNumOfTimes.text = "";
    gblEndValTemp = "none";
    gblScheduleEndFT = "none";
}

function ehFrmIBFTrnsrView_btnNever_onClick(eventobject) {
    frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
    frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
    frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
    gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_Never");
    frmIBFTrnsrView.btnNever.skin = "btnIBTab3LeftFocus"
    frmIBFTrnsrView.btnAftr.skin = "btnIBTab3MidNrml"
    frmIBFTrnsrView.btnOnDate.skin = "btnIBTab3RightNrml"
};

function ehFrmIBFTrnsrView_btnAftr_onClick(eventobject) {
    frmIBFTrnsrView.hbxNumOfTimes.setVisibility(true);
    frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
    frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(true);
    gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_After");
    /*   if (frmIBFTrnsrView.lblExcuteVal.text == "-") {
           frmIBFTrnsrView.txtNumOfTimes.text = "";
       }
    */
    frmIBFTrnsrView.txtNumOfTimes.text = "";
    frmIBFTrnsrView.txtNumOfTimes.setFocus(true);
    frmIBFTrnsrView.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBFTrnsrView.btnAftr.skin = "btnIBTab3MidFocus";
    frmIBFTrnsrView.btnOnDate.skin = "btnIBTab3RightNrml";
};

function ehFrmIBFTrnsrView_btnOnDate_onClick(eventobject) {
    frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(true);
    frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
    frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
    gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_OnDate");
    frmIBFTrnsrView.btnNever.skin = "btnIBTab3LeftNrml";
    frmIBFTrnsrView.btnAftr.skin = "btnIBTab3MidNrml";
    frmIBFTrnsrView.btnOnDate.skin = "btnIBTab3RightFocus";
    /* if (frmIBFTrnsrView.lblEndOnDateVal.text == "-") {
         var tmpDate = frmIBFTrnsrView.calStartDate.dateComponents;
         tmpDate = new Date(tmpDate[2], tmpDate[1] - 1, tmpDate[0]);
         var d1 = new Date(tmpDate.setDate(tmpDate.getDate() + 1));
         frmIBFTrnsrView.calEndDate.dateComponents = [d1.getDate(), d1.getMonth() + 1, d1.getFullYear()];
     } else {
         var tempdate = frmIBFTrnsrView.lblEndOnDateVal.text;
         var d = tempdate.split("/");
         frmIBFTrnsrView.calEndDate.dateComponents = [d[0], d[1], d[2]]
     }*/
    frmIBFTrnsrView.calEndDate.clear();
};

function ehfrmIBFTrnsrView_btnCancel1_onClick(eventobject) {
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);
    frmIBFTrnsrView.imgTMB.setVisibility(false);
    gblScheduleRepeatFT = gblFTViewRepeatAsVal;
    gblScheduleEndFT = gblEndValTemp;
    if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Daily")) gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Daily");
    else if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Weekly")) gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Weekly");
    else if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Monthly")) gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Monthly");
    else if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Yearly")) gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Yearly");
    else if (gblScheduleRepeatFT == "Once") frmIBFTrnsrView.lblRepeatAsVal.text = "Once";
    frmIBFTrnsrView.lblViewStartOnDateVal.text = gblTempStartOnDate;
    frmIBFTrnsrView.lblEndOnDateVal.text = gblTempEndOnDate;
    frmIBFTrnsrView.lblExcuteVal.text = gblTempExcuteVal;
    frmIBFTrnsrView.imgTMB.setVisibility(true);
    /* 
frmIBFTrnsrView.hbxEditFT.setVisibility(false);
frmIBFTrnsrView.imgTMB.setVisibility(false);
gblScheduleRepeatFT = gblFTViewRepeatAsVal;
 
gblScheduleEndFT = gblEndValTemp;

if(frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Daily"))
 gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Daily");
else if(frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Weekly"))
 gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Weekly");
else if(frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Monthly"))
 gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Monthly");
else if(frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Yearly"))
 gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Yearly");
else if(gblScheduleRepeatFT == "Once")
 frmIBFTrnsrView.lblRepeatAsVal.text = "Once";

frmIBFTrnsrView.lblViewStartOnDateVal.text = gblTempStartOnDate;
frmIBFTrnsrView.lblEndOnDateVal.text = gblTempEndOnDate;
frmIBFTrnsrView.lblExcuteVal.text = gblTempExcuteVal;
frmIBFTrnsrView.imgTMB.setVisibility(true);
 

 */
};

function ehfrmIBFTrnsrView_btnNext1_onClick(eventobject) {
    /* 
if(gblFTViewRepeatAsVal != gblScheduleRepeatFT)
 gblEditFTSchdule = true;
 
var d1 = frmIBFTrnsrView.lblViewStartOnDateVal.text;
var d2 = frmIBFTrnsrView.calStartDate.formattedDate;
if(parseDateIB(d1) != parseDateIB(d1)){
 gblEditFTSchdule = true;
}

if(gblScheduleEndFT != gblEndValTemp){
 
 if(gblScheduleEndFT == "none" && gblEndValTemp == kony.i18n.getLocalizedString("keyAfter")){
 if(frmIBFTrnsrView.lblExcuteVal.text != frmIBFTrnsrView.txtNumOfTimes.text)
 gblEditFTSchdule = true;
 
 }
 if(gblScheduleEndFT == "none" && gblEndValTemp == kony.i18n.getLocalizedString("keyOnDate")){
 var d1 = frmIBFTrnsrView.lblEndOnDateVal.text;
 var d2 = frmIBFTrnsrView.calEndDate.formattedDate;
 if(parseDateIB(d1) != parseDateIB(d2))
 gblEditFTSchdule = true;
 
 }
 

}else{
 if(gblScheduleEndFT == kony.i18n.getLocalizedString("keyAfter")){
 if(frmIBFTrnsrView.lblExcuteVal.text != frmIBFTrnsrView.txtNumOfTimes.text)
 gblEditFTSchdule = true;
 
 }
 if(gblScheduleEndFT == kony.i18n.getLocalizedString("keyOnDate")){
 var d1 = frmIBFTrnsrView.lblEndOnDateVal.text;
 var d2 = frmIBFTrnsrView.calEndDate.formattedDate;
 if(parseDateIB(d1) != parseDateIB(d2))
 gblEditFTSchdule = true;
 
 }
 

}

 */
    validateSchdule();
    //frmIBFTrnsrView.vbox447417227834.skin="vbxLogo"
    //frmIBFTrnsrView.vbox447417227834.margin=[0,77,0,0];
    //frmIBFTrnsrView.imgTMB.setVisibility(true);
};

function ehFrmIBTransferCustomWidgetLP_frmIBTransferCustomWidgetLP_preshow(eventobject, neworientation) {
    setDateOnly();
    /* 
loadData.call(this);

 */
    frmIBTransferCustomWidgetLP.lblXferFromNameRcvd.text = ""
}

function ehFrmIBTransferCustomWidgetLP_frmIBTransferCustomWidgetLP_postshow(eventobject, neworientation) {
    addIBMenu();
    nameofform = kony.application.getCurrentForm();
    nameofform.segMenuOptions.removeAll();
};

function ehFrmIBTransferCustomWidgetLP_btnXferNext_onClick(eventobject) {
    /* 
splitXferNowFRConfirmation.call(this);

 */
    /* 
XferBeforeBal.call(this);

 */
    /* 
onTransferLndngNextIB.call(this);

 */
    if (frmIBTransferCustomWidgetLP.lblXferFromNameRcvd.text == "") {
        alert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"));
    }
};

function ehFrmIBTransferCustomWidgetLP_btnXferShowContact_onClick(eventobject) {
    /* 
showToField.call(this);

 */
    /* 
toggleArrowonClickFalse.call(this);

 */
    /* 
frmIBTranferLP.arrowXferToField.setVisibility(true)

 */
    /* 
getToRecipientsIB.call(this);

 */
    alert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"));
};

function ehFrmIBTransferCustomWidgetLP_btnScheduledTransfer_onClick(eventobject) {
    showScheduleTransferField();
    toggleArrowonClickFalse();
    frmIBTranferLP.arrowXferScheduleField.setVisibility(true)
};

function ehFrmIBTransferNowCompletion_frmIBTransferNowCompletion_preshow(eventobject, neworientation) {
    /* 
enableSegmentTransferCompl.call(this);

 */
    /* 
campaginService.call(this,'image2101640690016078', 'frmIBTransferNowCompletion', 'I');

 */
    if (!gblPaynow) {
        frmIBTransferNowCompletion.hbox101461944018201.setVisibility(false);
        frmIBTransferNowCompletion.lblBal.setVisibility(false);
    }
};

function ehFrmIBTransferNowCompletion_frmIBTransferNowCompletion_postshow(eventobject, neworientation) {
    addIBMenu();
    campaginService('image2101640690016078', 'frmIBTransferNowCompletion', 'I');
    //if(gblPaynow && gblTransSMART==1){
    //frmIBTransferNowCompletion.lblTransferVal.text = GBL_SMART_DATE_FT
    //}
};

function ehFrmIBTransferNowCompletion_frmIBTransferNowCompletion_onDeviceBack(eventobject, neworientation) {
    disableBackButton();
};

function ehFrmIBTransferNowConfirmation_frmIBTransferNowConfirmation_postshow(eventobject, neworientation) {
    addIBMenu();
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBTransferNowConfirmation.btnMenuTransfer.skin = 'btnIBMenuTransferFocusThai';
    } else {
        frmIBTransferNowConfirmation.btnMenuTransfer.skin = 'btnIBMenuTransferFocus';
    }
    if ((gblisTMB == gblTMBBankCD) && (gblTransEmail == 1) && (gblTrasSMS == 1)) {
        frmIBTransferNowConfirmation.hbxButn.setVisibility(true);
        frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(false);
    } else {
        frmIBTransferNowConfirmation.hbxButn.setVisibility(false);
        frmIBTransferNowConfirmation.hboxPreConfirm.setVisibility(true);
    }
};

function ehFrmIBTransferNowConfirmation_button101450027636_onClick(eventobject) {
    editConfirmOTPXfer();
    setSelTabBankAccountOrMobile();
    frmIBTranferLP.hbxScheduledTransfer.setVisibility(false);
    var imageTransfer = frmIBTransferNowConfirmation.image247327596554550.src;
    frmIBTranferLP.imgXferToImage.src = imageTransfer;
    frmIBTranferLP.show();
    frmIBTranferLP.hbxwaterWheel.setVisibility(true);
    frmIBTranferLP.hbxXferTo.setVisibility(false);
    frmIBTranferLP.hbxXferSegment.setVisibility(false);
    if (gblSelTransferMode == 2 || gblSelTransferMode == 3) {
        frmIBTranferLP.txtXferMobileNumber.placeholder = kony.i18n.getLocalizedString("IB_P2PNoToValue");
        if (isNotBlank(gblisTMB)) {
            frmIBTranferLP.lblBankName.text = getBankNameIB(gblisTMB);
        }
        checkDisplayNotifyP2PMOorCI();
        calcuateITMXFeeIB();
    }
};

function ehFrmIBTransferNowConfirmation_txtBxOTP_onBeginEditing(eventobject, changedtext) {
    frmIBTransferNowConfirmation.hbxOTPEntry.skin = "hbxOtpTextField"
};

function ehFrmIBTransferNowConfirmation_txtBxOTP_onEndEditing(eventobject, changedtext) {
    frmIBTransferNowConfirmation.hbxOTPEntry.skin = "hbxOtpTextFieldNormal"
};

function ehFrmIBTransferNowConfirmation_btnOTPReq_onClick(eventobject) {
    requestButtonForOTPIB();
};

function ehfrmIBTransferNowConfirmation_tbxToken_onBeginEditing(eventobject, changedtext) {
    frmIBTransferNowConfirmation.hbxToken.skin = "hbxOtpTextField"
};

function ehFrmIBTransferNowConfirmation_tbxToken_onEndEditing(eventobject, changedtext) {
    frmIBTransferNowConfirmation.hbxToken.skin = "hbxOtpTextFieldNormal"
};

function ehFrmIBTransferNowConfirmation_tbxToken_onKeyUp(eventobject, changedtext) {
    dontAllowNonNeumaric(frmIBTransferNowConfirmation.tbxToken);
};

function ehFrmIBTransferNowConfirmation_button506369893806238_onClick(eventobject) {
    frmIBTransferNowConfirmation.hbxToken.setVisibility(false);
    frmIBTransferNowConfirmation.hbxOTPEntry.setVisibility(true);
    frmIBTransferNowConfirmation.txtBxOTP.setFocus(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    requestButtonForOTPIB();
};

function ehFrmIBTransferNowConfirmation_btnCancel_onClick(eventobject) {
    //TMBUtil.DestroyForm(frmIBTranferLP);
    //TMBUtil.DestroyForm(frmIBTransferNowConfirmation);
    //TMBUtil.DestroyForm(frmIBTransferNowCompletion);
    frmIBTransferNowConfirmation.info = {};
    frmIBTransferNowCompletion.info = {};
    transferMenuClick();
};

function ehfrmIBTransferNowConfirmation_brnConfirm_onClick(eventobject) {
    onClickConfirmTransferIB();
};

function ehFrmIBTransferTemplate_btnMenuMyActivities_onClick(eventobject) {
    onMenuMyActivitiesClick();
};

function ehFrmIBTransferTemplate_btnMenuMyAccountSummary_onClick(eventobject) {
    accountsummaryLangToggleIB();
    frmIBAccntSummary.segAccountDetails.selectedIndex = [0, 0];
};

function ehFrmIBTransferTemplate_btnMenuMyInbox_onClick(eventobject) {
    segMyInboxLoad();
    nameofform = kony.application.getCurrentForm();
    nameofform.btnMenuMyInbox.skin = "btnIBMenuMyInboxFocus"
    nameofform.btnMenuMyInbox.hoverSkin = "btnIBMenuMyInboxFocus"
    gblMenuSelection = 3;
};

function ehFrmIBTransferTemplate_btnMenuConvenientServices_onClick(eventobject) {
    segConvenientServicesLoad();
    nameofform = kony.application.getCurrentForm();
    nameofform.btnMenuConvenientServices.skin = "btnIBMenuConvenientServices"
    nameofform.btnMenuConvenientServices.hoverSkin = "btnIBMenuConvenientServices"
    gblMenuSelection = 2;
};

function ehPopSplitTransfers_popSplitTransfers_init(eventobject, neworientation) {
    //popSplitTransfers.enabledForIdleTimeout = true
};

function ehPopSplitTransfers_btnCancel_onClick(eventobject) {
    dismissLoadingScreenPopup();
    popSplitTransfers.dismiss();
};
/*======================  Mobile snippet=============================== */
function ehFrmAccTrcPwdInter_btnPopUpTermination_onClick(eventobject) {
    if (isMenuShown == false) {
        onClickNextPwdRules();
    } else {
        frmAccTrcPwdInter.scrollboxMain.scrollToEnd();
    }
};

function ehFrmAccTrcPwdInter_btnPopUpTerminationSpa_onClick(eventobject) {
    if (isMenuShown == false) {
        onClickNextPwdRules.call(this);
    } else {
        frmAccTrcPwdInter.scrollboxMain.scrollToEnd();
    }
};

function ehFrmCMChgTransPwd_frmCMChgTransPwd_preshow(eventobject, neworientation) {
    frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtNormalBG;
    frmCMChgTransPwd.txtTransPass.skin = txtNormalBG;
    frmCMChgTransPwd.txtTemp.skin = txtNormalBG;
    frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtNormalBG;
    frmCMChgTransPwd.txtTransPass.skin = txtNormalBG;
    frmCMChgTransPwd.txtTemp.skin = txtNormalBG;
    frmCMChgTransPwd.hbxPasswdStrength.text = ""
        //frmCMChgTransPwd.hbxPasswdStrength.skin="No skin"
        //frmCMChgTransPwd.txtTransPass.setVisibility(true);
        //frmCMChgTransPwd.button50285458147.setFocus(true);
    frmCMChgTransPwd.txtTemp.setVisibility(false);
    frmCMChgTransPwd.tbxTranscCrntPwdTemp.setVisibility(false);
    frmCMChgTransPwd.lblPasswdStrength.setVisibility(false);
    transCount = 0
    frmCMChgTransPwd.txtTransPass.setVisibility(true);
    frmCMChgTransPwd.tbxTranscCrntPwd.setVisibility(true);
    frmCMChgTransPwd.btnPwdOn.skin = "btnOnNormal";
    frmCMChgTransPwd.btnPwdOff.skin = "btnOffNorm";
    frmCMChgTransPwd.txtTransPass.text = "";
    frmCMChgTransPwd.tbxTranscCrntPwd.text = "";
    //frmCMChgTransPwd.hbxPasswdStrength.skin = "";
    gblShowPwd = gblShowPwdNo * 2;
    gblflag = 0;
    frmCMChgTransPwd.scrollboxMain.scrollToEnd();
    isMenuShown = false;
    frmCMChgTransPwdPreShow();
    DisableFadingEdges(frmCMChgTransPwd);
};

function ehFrmCMChgTransPwd_frmCMChgTransPwd_postshow(eventobject, neworientation) {};

function ehFrmCMChgTransPwd_button44843014510078_onClick(eventobject) {
    if (frmCMChgTransPwd.txtTemp.isVisible) {
        cancelTimer();
    }
    frmMyProfile.show();
};

function ehFrmCMChgTransPwd_button4769997143093_onClick(eventobject) {
    validateChangeTxnPwd();
};

function ehFrmCMChgTransPwd_txtTransPass_onDone(eventobject, changedtext) {
    popBubble.destroy();
};

function ehFrmCMChgTransPwd_txtTransPass_onTextChange(eventobject, changedtext) {
    var str = frmCMChgTransPwd.txtTransPass.text;
    if (str != null && str.length >= 1) {
        popBubble.destroy();
    } else {
        popBubble.show();
    }
    flag = 0
    passwordChanged();
};

function ehFrmCMChgTransPwd_txtTemp_onDone(eventobject, changedtext) {
    popBubble.destroy();
};

function ehFrmCMChgTransPwd_txtTemp_onTextChange(eventobject, changedtext) {
    var str = frmCMChgTransPwd.txtTemp.text;
    if (str != null && str.length >= 1) {
        popBubble.destroy();
    } else {
        popBubble.show();
    }
    flag = 1
    passwordChanged();
};

function ehFrmCMChgTransPwd_btnPwdOn_onClick(eventobject) {
    gblTxtFocusFlag = 5;
    onClickOnBtn();
};

function ehFrmCMChgTransPwd_btnPwdOff_onClick(eventobject) {
    gblTxtFocusFlag = 5;
    onClickOffBtn();
};

function ehFrmMBFTEdit_frmMBFTEdit_preshow(eventobject, neworientation) {
    isMenuShown = false;
    isSignedUser = true;
    frmMBFTEdit.scrollboxMain.scrollToEnd();
    frmMBFTEdit.btnFTEdit.setVisibility(true);
    frmMBFTEdit.txtEditAmnt.skin = txtLightBlueBG128;
    frmMBFTEdit.txtEditAmnt.focusSkin = txtLightBlueBG128;
    if (frmMBFTView.hbxNoteToReceipent.isVisible) {
        frmMBFTEdit.hbxNoteToReceipent.setVisibility(true);
        frmMBFTEdit.hbxNotifyReceipent.setVisibility(true);
    } else {
        frmMBFTEdit.hbxNoteToReceipent.setVisibility(false);
        frmMBFTEdit.hbxNotifyReceipent.setVisibility(false);
    }
    frmMBFTEditPreShow();
    frmMBFTEdit.lblTimesEdit.setVisibility(true);
    DisableFadingEdges(frmMBFTEdit);
};

function ehFrmMBFTEdit_frmMBFTEdit_postshow(eventobject, neworientation) {};

function ehFrmMBFTEdit_btnCancel_onClick(eventobject) {
    gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
    gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;
    gblRepeatAsCopy = gblFTViewRepeatAsValMB
    gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
    gblScheduleEndFTMB = gblFTViewEndValMB;
    gblEditFTSchduleMB = false;
    frmMBFTView.show();
}

function ehFfrmMBFTEdit_btnCancelSPA_onClick(eventobject) {
    gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
    gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;
    gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
    gblRepeatAsCopy = gblFTViewRepeatAsValMB
    gblScheduleEndFTMB = gblFTViewEndValMB;
    gblEditFTSchduleMB = false;
    kony.print("gblIsEditAftrMB: " + gblIsEditAftrMB + " gblIsEditOnDateMB: " + gblIsEditOnDateMB);
    frmMBFTView.show();
};

function ehFrmMBFTEdit_btnConfirmSPA_onClick(eventobject) {
    isEditScheduleMB();
};

function ehFrmMBFTEdit_txtEditAmnt_onEndEditing(eventobject, changedtext) {
    if (frmMBFTEdit.txtEditAmnt.text == "") {
        frmMBFTEdit.txtEditAmnt.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmMBFTEdit.txtEditAmnt.text = commaFormatted(parseFloat(removeCommos(frmMBFTEdit.txtEditAmnt.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
};

function ehFrmMBFTEdit_btnFTEdit_onClick(eventobject) {
    frmMBFtSchedule.calScheduleStartDate.dateComponents = dateFormatForDateComp(frmMBFTEdit.lblViewStartOnDateVal.text);
    if (gblScheduleRepeatFTMB == "Once" || frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyOnce")) {
        setRepeatClickedToFalse();
        frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(false);
        frmMBFtSchedule.lblEnd.setVisibility(false);
        disableRepeatBtnEditFT();
        disableEndBtnEditFT();
        frmMBFtSchedule.lblEnd.setVisibility(false);
        frmMBFtSchedule.lblEndAfter.setVisibility(false);
        frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
        frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
        frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
        frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
        frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
        frmMBFtSchedule.hbxEndAfter.setVisibility(false);
        frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
        frmMBFtSchedule.lblRepeat.setVisibility(true);
        frmMBFtSchedule.tbxAfterTimes.text = "";
        gblEndValTempMB = "none";
    }
    gblTempStartOnDateMB = frmMBFTEdit.lblViewStartOnDateVal.text;
    gblTempEndOnDateMB = frmMBFTEdit.lblEndOnDateVal.text;
    gblTempExcuteValMB = frmMBFTEdit.lblExcuteVal.text
    showEditScheduleMB();
};

function ehFrmMBFTEditCmplete_frmMBFTEditCmplete_preshow() {
    varAvailBal = false
    isMenuShown = false;
    isSignedUser = true;
    frmMBFTEditCmplete.scrollboxMain.scrollToEnd();
    if (frmMBFTView.hbxNoteToReceipent.isVisible) {
        frmMBFTEditCmplete.hbxNoteToReceipent.setVisibility(true);
        frmMBFTEditCmplete.hbxNotifyReceipent.setVisibility(true);
    } else {
        frmMBFTEditCmplete.hbxNoteToReceipent.setVisibility(false);
        frmMBFTEditCmplete.hbxNotifyReceipent.setVisibility(false);
    }
    frmMBFTEditCmpletePreShow();
    if (frmMBFTEditCmplete.lblExcuteVal.text == "" || frmMBFTEditCmplete.lblExcuteVal.text == "-") {
        frmMBFTEditCmplete.lblTimesComp.setVisibility(false);
    } else {
        frmMBFTEditCmplete.lblTimesComp.setVisibility(true);
    }
    DisableFadingEdges(frmMBFTEditCmplete);
    //code for personalized campaign
    try {
        frmMBFTEditCmplete.hbxAdv.setVisibility(false);
        frmMBFTEditCmplete.imgAd.src = "";
        frmMBFTEditCmplete.gblBrwCmpObject.handleRequest = "";
        frmMBFTEditCmplete.gblBrwCmpObject.htmlString = "";
        frmMBFTEditCmplete.gblVbxCmp.remove(gblBrwCmpObject);
        frmMBFTEditCmplete.hbxAdv.remove(gblVbxCmp);
    } catch (e) {}
};

function ehFrmMBFTEditCmplete_frmMBFTEditCmplete_postshow() {
    campaginService("imgAd", "frmMBFTEditCmplete", "M");
};

function ehFrmMBFTEditCmplete_btnRight_onClick() {
    //frmMBFTEditCmplete.hboxSharelist.setVisibility(true)
    if (frmMBFTEditCmplete.hboxSharelist.isVisible) {
        frmMBFTEditCmplete.hboxSharelist.setVisibility(false)
    } else frmMBFTEditCmplete.hboxSharelist.setVisibility(true)
};

function ehFrmMBFTEditCmplete_btnEmailto_onClick() {
    gblPOWcustNME = frmMBFTEditCmplete.lblFrmAccntNickName.text
    gblPOWtransXN = "EditFutureTransfer"
    gblPOWchannel = "MB"
    postOnWall()
    requestFromForm = "frmMBFTEditCmplete"
};

function ehFrmMBFTEditCmplete_btnReturnSPA_onClick() {
    MBMyActivitiesReloadAndShowCalendar();
};

function ehFrmMBFTEditCmplete_btnB4T_onClick() {
    if (varAvailBal == false) {
        frmMBFTEditCmplete.lblFTEditAftrAvailBal.isVisible = true;
        frmMBFTEditCmplete.lblFTEditAftrAvailBalVal.isVisible = true;
        varAvailBal = true;
        frmMBFTEditCmplete.lblHide.text = "Hide";
    } else if (varAvailBal == true) {
        frmMBFTEditCmplete.lblFTEditAftrAvailBal.isVisible = false;
        frmMBFTEditCmplete.lblFTEditAftrAvailBalVal.isVisible = false;
        varAvailBal = false;
        frmMBFTEditCmplete.lblHide.text = "Show";
    }
};

function ehFrmMBFTEditCmpleteCalendar_btnB4T_onClick() {
    if (varAvailBal == false) {
        frmMBFTEditCmplete.lblFTEditAftrAvailBal.isVisible = true;
        frmMBFTEditCmplete.lblFTEditAftrAvailBalVal.isVisible = true;
        varAvailBal = true;
        frmMBFTEditCmplete.lblHide.text = "Hide";
    } else if (varAvailBal == true) {
        frmMBFTEditCmplete.lblFTEditAftrAvailBal.isVisible = false;
        frmMBFTEditCmplete.lblFTEditAftrAvailBalVal.isVisible = false;
        varAvailBal = false;
        frmMBFTEditCmplete.lblHide.text = "Show";
    }
};

function ehFrmMBFTEditCnfrmtn_frmMBFTEditCnfrmtn_preshow() {
    isMenuShown = false;
    isSignedUser = true;
    frmMBFTEditCnfrmtn.scrollboxMain.scrollToEnd();
    if (frmMBFTView.hbxNoteToReceipent.isVisible) {
        frmMBFTEditCnfrmtn.hbxNoteToReceipent.setVisibility(true);
        frmMBFTEditCnfrmtn.hbxNotifyReceipent.setVisibility(true);
    } else {
        frmMBFTEditCnfrmtn.hbxNoteToReceipent.setVisibility(false);
        frmMBFTEditCnfrmtn.hbxNotifyReceipent.setVisibility(false);
    }
    frmMBFTEditCnfrmtnPreShow();
    if (frmMBFTEditCnfrmtn.lblExcuteVal.text == "" || frmMBFTEditCnfrmtn.lblExcuteVal.text == "-") {
        frmMBFTEditCnfrmtn.lblTimesConf.setVisibility(false);
    } else {
        frmMBFTEditCnfrmtn.lblTimesConf.setVisibility(true);
    }
    DisableFadingEdges(frmMBFTEditCnfrmtn);
};

function ehFrmMBFTEditCnfrmtn_frmMBFTEditCnfrmtn_postshow() {};

function ehFrmMBFTEditCnfrmtn_btnHdrMenu_onClick() {
    handleMenuBtn();
};

function ehFrmMBFTEditCnfrmtn_button445536670970293_onClick() {
    //frmMBFTEdit.lblToAccntName.text = gblAccntName_ORFT_MB
    frmMBFTEdit.lblFeeVal.text = frmMBFTEditCnfrmtn.lblFeeVal.text;
    frmMBFTEdit.show();
};

function ehFrmMBFTEditCnfrmtn_btnCancel_onClick() {
    gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
    gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;
    gblRepeatAsCopy = gblFTViewRepeatAsValMB
    gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
    gblScheduleEndFTMB = gblFTViewEndValMB;
    gblEditFTSchduleMB = false;
    frmMBFTView.show();
};

function ehFrmMBFTEditCnfrmtn_btnCancelSPA_onClick() {
    gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
    gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;
    gblRepeatAsCopy = gblFTViewRepeatAsValMB
    gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
    gblScheduleEndFTMB = gblFTViewEndValMB;
    gblEditFTSchduleMB = false;
    frmMBFTView.show();
};

function ehFrmMBFTEditCnfrmtn_btnConfirmSPA_onClick() {
    onEditFtConfirm();
    frmMBFTEditCmplete.hboxSharelist.setVisibility(false)
};

function ehFrmMBFTSchduleOldFt_frmMBFTSchduleOldFt_postshow() {};

function ehFrmMBFTSchduleOldFt_btnDaily_onClick() {
    frmMBFTSchdule.lblEnding.setVisibility(true);
    frmMBFTSchdule.hbxEndingBtns.setVisibility(true);
    frmMBFTSchdule.hbxNumOfTimes.setVisibility(false)
    frmMBFTSchdule.hbxFtEndOnDate.setVisibility(false)
    frmMBFTSchdule.lblInsturction.setVisibility(false)
};

function ehFrmMBFTSchduleOldFt_btnYearly_onClick() {
    frmMBFTSchdule.hbxEndingBtns.setVisibility(true)
    frmMBFTSchdule.lblEnding.setVisibility(true)
    frmMBFTSchdule.hbxNumOfTimes.setVisibility(false)
    frmMBFTSchdule.hbxFtEndOnDate.setVisibility(false)
    frmMBFTSchdule.lblInsturction.setVisibility(false)
};

function ehFrmMBFTSchduleOldFt_button4484263971057831_onClick() {
    frmMBFTSchdule.hbxNumOfTimes.setVisibility(false);
    frmMBFTSchdule.lblInsturction.setVisibility(false)
    frmMBFTSchdule.hbxFtEndOnDate.setVisibility(false);
};

function ehFrmMBFTSchduleOldFt_button4484263971057833_onClick() {
    frmMBFTSchdule.hbxNumOfTimes.setVisibility(true);
    frmMBFTSchdule.hbxFtEndOnDate.setVisibility(false);
    frmMBFTSchdule.lblInsturction.setVisibility(true)
};

function ehFrmMBFTSchduleOldFt_btnOnDate_onClick() {
    frmMBFTSchdule.hbxFtEndOnDate.setVisibility(true);
    frmMBFTSchdule.hbxNumOfTimes.setVisibility(false);
    frmMBFTSchdule.lblInsturction.setVisibility(false)
};

function ehFrmMBFtSchedule_frmMBFtSchedule_preshow() {
    frmMBFtSchedule.tbxAfterTimes.skin = txtLightBlueBG128;
    frmMBFtSchedule.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    frmMBFtSchedulePreShow();
};

function ehFrmMBFtSchedule_frmMBFtSchedule_postshow() {
    frmMBFtSchedule.lblEndAfter.setVisibility(false);
};

function ehFrmMBFtSchedule_btnDaily_onClick() {
    if (kony.string.equalsIgnoreCase(frmMBFtSchedule.btnDaily.skin, "btnScheduleLeftFocus")) {
        disableRepeatBtnEditFT();
        disableEndAfterButtonHolderEditFT();
        setRepeatClickedToFalse();
    } else {
        if (isScheduleFirstShow()) {
            ehFrmMBFtSchedule_btnNever_onClick();
        }
        setFocusOnDailyEditFT();
        DailyClicked = true;
    }
}

function ehFrmMBFtSchedule_btnWeekly_onClick() {
    if (kony.string.equalsIgnoreCase(frmMBFtSchedule.btnWeekly.skin, "btnScheduleMidFocus")) {
        disableRepeatBtnEditFT();
        disableEndAfterButtonHolderEditFT();
        setRepeatClickedToFalse();
    } else {
        if (isScheduleFirstShow()) {
            ehFrmMBFtSchedule_btnNever_onClick();
        }
        setFocusOnWeeklyEditFT();
        weekClicked = true;
    }
}

function ehFrmMBFtSchedule_btnMonthly_onClick() {
    if (kony.string.equalsIgnoreCase(frmMBFtSchedule.btnMonthly.skin, "btnScheduleMidFocus")) {
        disableRepeatBtnEditFT();
        disableEndAfterButtonHolderEditFT();
        setRepeatClickedToFalse();
    } else {
        if (isScheduleFirstShow()) {
            ehFrmMBFtSchedule_btnNever_onClick();
        }
        setFocusOnMonthlyEditFT();
        monthClicked = true;
    }
}

function ehFrmMBFtSchedule_btnYearly_onClick() {
    if (kony.string.equalsIgnoreCase(frmMBFtSchedule.btnYearly.skin, "btnScheduleRightFocus")) {
        disableRepeatBtnEditFT();
        disableEndAfterButtonHolderEditFT();
        setRepeatClickedToFalse();
    } else {
        if (isScheduleFirstShow()) {
            ehFrmMBFtSchedule_btnNever_onClick();
        }
        setFocusOnYearlyEditFT();
        YearlyClicked = true;
    }
}

function ehFrmMBFtSchedule_btnNever_onClick() {
    frmMBFtSchedule.lblEndAfter.setVisibility(false);
    frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
    frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
    frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
    frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
    frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
    gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyNever");
    frmMBFtSchedule.tbxAfterTimes.text = "";
    setFocusOnNeverBtnEditFT();
};

function ehFrmMBFtSchedule_btnAfter_onClick() {
    frmMBFtSchedule.lblEndAfter.setVisibility(false);
    frmMBFtSchedule.hbxNumOfTimes.setVisibility(true);
    frmMBFtSchedule.lineAfterTxtbx.setVisibility(true);
    frmMBFtSchedule.lblNumberOfTimes.setVisibility(true)
    frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
    frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
    frmMBFtSchedule.hbxEndAfter.setVisibility(true);
    setFocusOnAfterBtnEditFT();
    gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyAfter");
    frmMBFtSchedule.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
};

function ehFrmMBFtSchedule_btnOnDate_onClick() {
    frmMBFtSchedule.lblEndAfter.setVisibility(false);
    frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
    frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
    frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
    frmMBFtSchedule.hbxEndDateSelectn.setVisibility(true);
    frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(true);
    frmMBFtSchedule.hbxEndOnDate.setVisibility(true);
    frmMBFtSchedule.tbxAfterTimes.text = "";
    gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyOnDate");
    setFocusOnOnDateBtnEditFT();
    frmMBFtSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmMBFtSchedule.calScheduleStartDate.dateComponents);
};

function ehFrmMBFtSchedule_btnCancel_onClick() {
    gblScheduleEndFTMB = gblEndValTempMB;
    gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
    if (frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyDaily")) gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyDaily");
    else if (frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyWeekly")) gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyWeekly");
    else if (frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyMonthly")) gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyMonthly");
    else if (frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyYearly")) gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyYearly");
    else if (gblScheduleRepeatFTMB == "Once") frmMBFTEdit.lblRepeatAsVal.text = "Once";
    frmMBFTEdit.lblViewStartOnDateVal.text = gblTempStartOnDateMB
    frmMBFTEdit.lblEndOnDateVal.text = gblTempEndOnDateMB
    frmMBFTEdit.lblExcuteVal.text = gblTempExcuteValMB
    frmMBFTEdit.show();
};

function ehFrmMBFtSchedule_btnSave_onClick() {
    validateSchduleMB();
};

function ehFrmMBFTView_frmMBFTView_preshow() {
    frmMBFTViewPreShow();
    isMenuShown = false;
    isSignedUser = true;
    frmMBFTView.scrollboxMain.scrollToEnd();
    frmMBFTView.lblTimes.setVisibility(true);
    DisableFadingEdges(frmMBFTView);
};

function ehFrmMBFTView_frmMBFTView_postshow() {};

function ehFrmMBFTView_btnFTEditFlow_onClick() {
    frmMBFTEdit.txtEditAmnt.setFocus(true);
    gblCRMProfileInqCalMB = true;
    getFeeCrmProfileInqMB();
};

function ehFrmMBFTViewCalendar_btnConfirm_onClick() {
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate, frmMBMyActivities);
};

function ehFrmTMBTransAck_hboxaddfblist_onClick() {
    frm2.hbox50285458127.setVisibility(true)
};

function ehFrmScheduleTransfer_btnBack_onClick() {
    frmTransferLanding.show();
};

function ehFrmTMBTransAck_btnRight_onClick() {
    /* 
onClickContactList.call(this);

 */
    var btnskin = "btnShare";
    var btnFocusSkin = "btnShareFoc";
    if (frmTMBTransAck.hboxaddfblist.isVisible) {
        frmTMBTransAck.hboxaddfblist.isVisible = false;
        frmTMBTransAck.btnRight.skin = btnskin;
        frmTMBTransAck.imgHeaderMiddle.src = "empty.png";
        frmTMBTransAck.imgHeaderRight.src = "empty.png";
    } else {
        frmTMBTransAck.hboxaddfblist.isVisible = true;
        frmTMBTransAck.btnRight.skin = btnFocusSkin;
        frmTMBTransAck.imgHeaderMiddle.src = "empty.png";
        frmTMBTransAck.imgHeaderRight.src = "empty.png";
    }
};

function ehFrmTMBTransAck_hboxaddfblist_onClick() {
    frm2.hbox50285458127.setVisibility(true)
};

function ehFrmTMBTransAck_hbox101271281131304_onClick() {
    if (gblAckFlage == "true") {
        frmTMBTransAck.lblTransNPbAckBalAfter.setVisibility(false);
        frmTMBTransAck.lblTransNPbAckFrmBal.setVisibility(false)
        frmTMBTransAck.lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
        gblAckFlage = "false"
    } else {
        frmTMBTransAck.lblTransNPbAckBalAfter.setVisibility(true);
        frmTMBTransAck.lblTransNPbAckFrmBal.setVisibility(true)
        frmTMBTransAck.lblHide.text = kony.i18n.getLocalizedString("Hide")
        gblAckFlage = "true"
    }
};

function ehFrmTMBTransAck_vbox156335099531837_onClick() {
    if (frmTMBTransAck.lblTransNPbAckFrmBal.isVisible) {
        frmTMBTransAck.lblTransNPbAckFrmBal.setVisibility(false);
        frmTMBTransAck.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmTMBTransAck.lblTransNPbAckFrmBal.setVisibility(true);
        frmTMBTransAck.lblHide.text = kony.i18n.getLocalizedString("keyHideIB");
    }
};

function ehFrmTranfersToRecipents_frmTranfersToRecipents_preshow() {
    preShowfrmTranfersToRecipentsMB();
    /* 
populateTransferToRecipientsData.call(this);

 */
    frmTranfersToRecipents.txbXferSearch.text = "";
};

function ehFrmTranfersToRecipents_button156335099532181_onClick() {
    isFromEdit = true;
    frmTransferLanding.show();
};

function ehFrmTransferConfirm_frmTransferConfirm_preshow() {
    frmTransferConfirm.scrollboxMain.scrollToEnd();
    gblIndex = -1;
    isMenuShown = false;
    isSignedUser = true;
    preShowfrmTransferConfirmMB();
    DisableFadingEdges(frmTransferConfirm);
};

function ehFrmTransferConfirm_btnTransCnfrmCancelSpa_onClick() {
    ResetTransferHomePage();
    frmTransferLanding.hbxTransLndFee.setVisibility(false);
    frmTransferLanding.hbxFeeTransfer.setVisibility(true);
    //frmTransferLanding.lineFeeButton.setVisibility(false);
    //frmTransferLanding.hboxFeeType.setVisibility(false)
    frmTransferLanding.show();
};

function ehFrmTransferConfirm_btnTransCnfrmConfirmSpa_onClick() {
    showTransPwdPopupForTransfers();
};

function ehFrmTransferLanding_frmTransferLanding_init() {
    ResetTransferHomePage();
};

function ehFrmTransferLanding_frmTransferLanding_preshow() {
    frmTransferLanding.txtTranLandMyNote.numberOfVisibleLines = 1;
    frmTransferLanding.scrollboxMain.scrollToEnd();
    gblIndex = -1;
    isMenuShown = false;
    preShowfrmTransferLandingMB();
    DisableFadingEdges(frmTransferLanding);
};

function ehFrmTransferLanding_frmTransferLanding_postshow() {
    gblSelTransferFromAcctNo = "";
}

function ehFrmTransferLanding_btnLtArrow_onClick() {
    if (isMenuShown == false) {
        onClickLeftArrowTransfer()
    } else {
        frmTransferLanding.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function ehFrmTransferLanding_segTransFrm_onRowClick() {
    onClickCoverFlowTransfersMB()
};

function ehFrmTransferLanding_btnTD_onClick() {
    selectTDDetails();
};

function ehFrmTransferLanding_btnTranLandToSel_onClick() {
    popUpBankList.dismiss();
    var pr = "";
    var acc = ""
    pr = frmTransferLanding.segTransFrm.selectedItems[0].prodCode;
    acc = frmTransferLanding.segTransFrm.selectedItems[0].lblActNoval;
    acc = kony.string.replace(acc, "-", "");
    getToRecipientsNewMB(pr, acc);
};

function ehFrmTransferLanding_hbxTranLandMyNote_onClick() {
    frmTransferLanding.txtTranLandMyNote.setFocus(true)
};

function ehFrmTransferLanding_btnSchedTo_onClick() {
    frmSchedule.lblSetSchePrev.text = frmTransferLanding.lblSchedSel.text;
    gblScheduleEndBPTmp = gblScheduleEndBP;
    gblScheduleRepeatBPTmp = gblScheduleRepeatBP;
    gblScheduleTransfers = true;
    frmSchedule.show();
};

function ehFrmTransferLanding_btnTransLndORFT_onClick() {
    if (gblTrasORFT == 0) {
        frmTransferLanding.btnTransLndORFT.skin = "btnFeeTopFoc"
        frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom"
        gblTrasORFT = gblTrasORFT + 1;
        gblTransSMART = 0;
        makeWidgetsBelowTransferFeeButtonVisible(true);
        holdPreviouslySelectedNotifyDetails();
        frmTransferLanding.lblRecievedBy.text = getDateFormatForTransfers(GLOBAL_TODAY_DATE);
        frmTransferLanding.lblRecievedByValue.text = kony.i18n.getLocalizedString("keyNOW");
        frmTransferLanding.lineRecipientNote.setVisibility(true);
        frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_01");
    } else {
        frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop"
        gblTrasORFT = 0;
        gblTransSMART = 0;
        makeWidgetsBelowTransferFeeButtonVisible(false);
        frmTransferLanding.lineMyNote.setVisibility(false);
        frmTransferLanding.hbxTranLandNotifyRec.setVisibility(false);
        frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
        frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_01");
        frmTransferLanding.txtTransLndSms.setVisibility(false);
        frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
        frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
        frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
        frmTransferLanding.lineRecipientNote.setVisibility(false);
    }
    //frmTransferLanding.enabledForIdleTimeout = true
};

function ehFrmTransferLanding_btnTransLndSmart_onClick() {
    if (gblTransSMART == 0 || gblTrasORFT == 1) {
        frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottomFoc";
        frmTransferLanding.btnTransLndORFT.skin = "btnFeeTop";
        gblTransSMART = gblTransSMART + 1
        gblTrasORFT = 0
        frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_02");
        makeWidgetsBelowTransferFeeButtonVisible(true);
        holdPreviouslySelectedNotifyDetails();
        var smartDateTime = smartDate.split(" ");
        if (smartDateTime.length > 1) {
            frmTransferLanding.lblRecievedBy.text = smartDateTime[0];
            frmTransferLanding.lblRecievedByValue.text = smartDateTime[1];
        }
        frmTransferLanding.lineRecipientNote.setVisibility(true);
    } else {
        frmTransferLanding.btnTransLndSmart.skin = "btnFeeBottom";
        gblTransSMART = 0;
        makeWidgetsBelowTransferFeeButtonVisible(false);
        frmTransferLanding.lineMyNote.setVisibility(false);
        frmTransferLanding.hbxTranLandNotifyRec.setVisibility(false);
        frmTransferLanding.lineNotifyRecipientButton.setVisibility(false);
        frmTransferLanding.lblRcvTime.text = kony.i18n.getLocalizedString("TREnter_Time_01");
        frmTransferLanding.txtTransLndSms.setVisibility(false);
        frmTransferLanding.txtTransLndSmsNEmail.setVisibility(false);
        frmTransferLanding.hbxTranLandRecNote.setVisibility(false);
        frmTransferLanding.hbxRecNoteEmail.setVisibility(false);
        frmTransferLanding.lineRecipientNote.setVisibility(false);
    }
};

function ehFrmTransferLanding_btnTranLandNext_onClick() {
    if (isMenuShown == false) {
        if (gblSelTransferMode == 1) {
            popUpBankList.dismiss();
            setEnabledTransferLandingPage(false);
            checkEnteredAccountPresentInRecipientList(true);
        } else {
            //onTransferLndngNext();
            mobileNumberTransferNext();
        }
    } else {
        frmTransferLanding.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function ehFrmTransfersAck_frmTransfersAck_preshow() {
    frmTransfersAck.scrollboxMain.scrollToEnd();
    gblIndex = -1;
    isMenuShown = false;
    isSignedUser = true;
    frmTransfersAck.btnRight.skin = btnShare;
    preShowfrmTransferAckMB();
    enableSegmentTransferMB(gblSplitTransfer);
    if (gblPaynow) {
        frmTransfersAck.hbxBalAfterTransfer.setVisibility(true);
        frmTransfersAck.lblTransNPbAckFrmBal.setVisibility(true);
    } else {
        frmTransfersAck.hbxBalAfterTransfer.setVisibility(false);
        frmTransfersAck.lblTransNPbAckFrmBal.setVisibility(false);
    }
    //ChangeCampaignLocale();
    DisableFadingEdges(frmTransfersAck);
    //code for personalized banner display
    try {
        frmTransfersAck.hbxAdv.setVisibility(false);
        frmTransfersAck.imgTransAdv.src = "";
        frmTransfersAck.gblBrwCmpObject.handleRequest = "";
        frmTransfersAck.gblBrwCmpObject.htmlString = "";
        frmTransfersAck.gblVbxCmp.remove(gblBrwCmpObject);
        frmTransfersAck.hbxAdv.remove(gblVbxCmp);
    } catch (e) {}
};

function ehFrmTransfersAck_frmTransfersAck_postshow() {
    campaginService("imgTransAdv", "frmTransfersAck", "M");
};

function ehFrmTransfersAck_frmTransfersAck_onhide() {
    removeMenu();
};

function ehFrmTransfersAck_btnTransNPbAckReturn_onClick() {
    //TMBUtil.DestroyForm(frmTransferLanding);
    //TMBUtil.DestroyForm(frmAccountSummaryLanding);
    TMBUtil.DestroyForm(frmTransferLanding);
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    cleanUpGlobalVariableTransfersMB();
    showAccuntSummaryScreen();
}

function ehFrmTransfersAckCalendar_frmTransfersAckCalendar_preshow() {
    isMenuShown = false;
    isSignedUser = true;
    frmTransfersAckCalendar.scrollboxMain.scrollToEnd();
    frmTransfersAckCalendarPreShow();
    DisableFadingEdges(frmTransfersAckCalendar);
};

function ehFAccntTransPwd_btnPopUpTractCancel_onClick() {
    onClickOTPRequest();
};

function ehAccntTransPwd_btnPopupTractConf_onClick() {
    OTPValdatn(popupTractPwd.tbxPopupTractPwdtxt.text);
    popUpLogout.dismiss();
};

function ehPopBPTransactionPwd_btnPopTransactionCancel_onClick() {
    popTransactionPwd.dismiss();
};

function ehPopTransactionPwd_popTransactionPwd_init() {
    popTransactionPwd.tbxPopTransactionPwd.skin = tbxPopupBlue;
    popTransactionPwd.tbxPopTransactionPwd.focusSkin = tbxPopupBlue;
    popTransactionPwd.lblPopTranscationMsg.skin = lblPopupLabelTxt;
};

function ehPopTransactionPwd_btnPopTransactionCancel_onClick() {
    popTransactionPwd.dismiss();
};

function ehPopTransactionPwd_btnPopTransactionConf_onClick() {
    popTransactionPwd.tbxPopTransactionPwd.skin = tbxPopupBlue;
    popTransactionPwd.tbxPopTransactionPwd.focusSkin = tbxPopupBlue;
    popTransactionPwd.lblPopTranscationMsg.skin = lblPopupLabelTxt;
    if (gblS2SHiddenStatus == "true") {
        onClickContinuePopForS2S();
        gblS2SHiddenStatus = false;
    } else {
        onClickConfirmPop();
    }
};

function ehPopTransactionPwd_tbxPopTransactionPwd_onBigningEdit() {
    popTransactionPwd.hbxPopTranscationPwd.skin = "hbxPopupTrnsPwdBlue";
}

function ehPopTransactionPwd_tbxPopTransactionPwd_onEndingEdit() {
    popTransactionPwd.hbxPopTranscationPwd.skin = "hbxPopupTrnsPwdBlue";
}

function ehPopTransferConfirmOTPLock_popTransferConfirmOTPLock_init() {
    // popTransferConfirmOTPLock.label58862488528485.text = kony.i18n.getLocalizedString("keyYourTransactionPasswordhasbeen");
    // popTransferConfirmOTPLock.label58862488528486.text = kony.i18n.getLocalizedString("keylockedPleasecallTMBContactCenter");
    popTransferConfirmOTPLock.containerHeight = 38;
};

function ehPopTransferConfirmOTPLock_button58862488528490_onClick() {
    popTransferConfirmOTPLock.dismiss();
    popTransferConfirmOTPLock.dismiss();
    popTransferConfirmOTPLock.destroy();
    if (kony.application.getCurrentForm().id == "frmAccountSummaryLanding") {
        frmAccountSummaryLanding.scrollboxMain.scrollToEnd()
    } else {
        TMBUtil.DestroyForm(frmAccountSummaryLanding);
        showAccuntSummaryScreen();
    }
};

function ehPopupTractPwd_btnPopUpTractCancel_onClick() {
    onClickOTPRequest();
};

function ehPopupTractPwd_btnPopupTractConf_onClick() {
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    popupTractPwd.dismiss()
    OTPValdatn(popupTractPwd.tbxPopupTractPwdtxt.text);
};

function showTransferLandingHome() {
    /*frmTransferLanding.lblTranLandToName.text = "";
    frmTransferLanding.lblTranLandToNum.text = "";
    frmTransferLanding.txtTranLandAmt.text="";
    frmTransferLanding.txtTranLandMyNote.text="";
    frmTransferLanding.lblSchedSel.text="";
    frmTransferLanding.imgTranLandTo.src = "avatar.png";
    frmTransferLanding.show();*/
    transferFromMenu();
}

function disableEndAfterButtonHolderEditFT() {
    frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(false);
    frmMBFtSchedule.lblEnd.setVisibility(false);
    frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
    frmMBFtSchedule.hbxEndAfter.setVisibility(false);
    frmMBFtSchedule.tbxAfterTimes.text = "";
    disableEndBtnEditFT();
}

function disableRepeatBtnEditFT() {
    gblScheduleRepeatFTMB = "Once";
    gblScheduleEndFTMB = "none";
    frmMBFtSchedule.btnDaily.skin = btnScheduleLeft;
    frmMBFtSchedule.btnWeekly.skin = btnScheduleMid;
    frmMBFtSchedule.btnMonthly.skin = btnScheduleMid;
    frmMBFtSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnDailyEditFT() {
    gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyDaily");
    frmMBFtSchedule.btnDaily.skin = btnScheduleLeftFocus;
    frmMBFtSchedule.btnWeekly.skin = btnScheduleMid;
    frmMBFtSchedule.btnMonthly.skin = btnScheduleMid;
    frmMBFtSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnWeeklyEditFT() {
    gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyWeekly");
    frmMBFtSchedule.btnDaily.skin = btnScheduleLeft;
    frmMBFtSchedule.btnWeekly.skin = btnScheduleMidFocus;
    frmMBFtSchedule.btnMonthly.skin = btnScheduleMid;
    frmMBFtSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnMonthlyEditFT() {
    gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyMonthly");
    frmMBFtSchedule.btnDaily.skin = btnScheduleLeft;
    frmMBFtSchedule.btnWeekly.skin = btnScheduleMid;
    frmMBFtSchedule.btnMonthly.skin = btnScheduleMidFocus;
    frmMBFtSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnYearlyEditFT() {
    gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyYearly");
    frmMBFtSchedule.btnDaily.skin = btnScheduleLeft;
    frmMBFtSchedule.btnWeekly.skin = btnScheduleMid;
    frmMBFtSchedule.btnMonthly.skin = btnScheduleMid;
    frmMBFtSchedule.btnYearly.skin = btnScheduleRightFocus;
}

function disableEndBtnEditFT() {
    frmMBFtSchedule.btnNever.skin = btnScheduleEndLeft;
    frmMBFtSchedule.btnAfter.skin = btnScheduleEndMid;
    frmMBFtSchedule.btnOnDate.skin = btnScheduleEndRight;
    frmMBFtSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmMBFtSchedule.calScheduleStartDate.dateComponents);
}

function setFocusOnNeverBtnEditFT() {
    frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
    frmMBFtSchedule.lblEnd.setVisibility(true);
    frmMBFtSchedule.btnNever.skin = btnScheduleEndLeftFocus;
    frmMBFtSchedule.btnAfter.skin = btnScheduleEndMid;
    frmMBFtSchedule.btnOnDate.skin = btnScheduleEndRight;
    frmMBFtSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmMBFtSchedule.calScheduleStartDate.dateComponents);
}

function setFocusOnAfterBtnEditFT() {
    frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
    frmMBFtSchedule.lblEnd.setVisibility(true);
    frmMBFtSchedule.btnNever.skin = btnScheduleEndLeft;
    frmMBFtSchedule.btnAfter.skin = btnScheduleEndMidFocus;
    frmMBFtSchedule.btnOnDate.skin = btnScheduleEndRight;
    frmMBFtSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmMBFtSchedule.calScheduleStartDate.dateComponents);
}

function setFocusOnOnDateBtnEditFT() {
    frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
    frmMBFtSchedule.lblEnd.setVisibility(true);
    frmMBFtSchedule.btnNever.skin = btnScheduleEndLeft
    frmMBFtSchedule.btnAfter.skin = btnScheduleEndMid
    frmMBFtSchedule.btnOnDate.skin = btnScheduleRightFocus;
}

function isScheduleFirstShow() {
    if (!DailyClicked && !weekClicked && !monthClicked && !YearlyClicked) {
        return true;
    }
    return false;
}

function getDefaultEndDateEditFT(data) {
    if (isNotBlank(data)) {
        return [(data[0] * 1) + 1, data[1], data[2], 0, 0, 0];
    } else {
        var day = new Date(GLOBAL_TODAY_DATE);
        var dd = day.getDate() * 1;
        var mm = day.getMonth() + 1;
        var yyyy = day.getFullYear();
        //var deviceInfo = kony.os.deviceInfo();
        if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
            var returnedValue = iPhoneCalendar.getDeviceDateLocale();
            var deviceDate = returnedValue.split("|")[0];
            yyyy = deviceDate.substr(0, 4)
        }
        return [dd, mm, yyyy, 0, 0, 0];
    }
}