function onclickExchangeRatesIB() {
    if (kony.i18n.getCurrentLocale() == "en_US") kony.application.openURL(GLOBAL_Exchange_Rates_Link_EN);
    else kony.application.openURL(GLOBAL_Exchange_Rates_Link_TH);
    //Commented for ENH_209
    /*try{
    	
    	inputParams = {};
    	showLoadingScreenPopup();
    	invokeServiceSecureAsync("GetExchangeRateData", inputParams, callBackExchangeRatesIB)
    }catch(e){
    	
    }*/
}

function callBackExchangeRatesIB(status, resulttable) {
    try {
        //
        var exgRateResultTableIB = [];
        var updateTime = ""
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                for (var i = 0; i < resulttable["exchangeRates"].length; i++) {
                    var tempDataIB = [];
                    var desp = resulttable["exchangeRates"][i]["description"];
                    var countryCode = resulttable["exchangeRates"][i]["countryCode"];
                    var currencyDesp = "";
                    if (resulttable["exchangeRates"][i]["currencyDesc"] == null || resulttable["exchangeRates"][i]["currencyDesc"] == "") {
                        currencyDesp = "";
                    } else {
                        currencyDesp = resulttable["exchangeRates"][i]["currencyDesc"];
                    }
                    var flagImage = setExchangeRateFlag(countryCode);
                    //flagImage = "afghanistan.png"; //hardcoded for testing purpose.
                    tempDataIB = {
                        "column1": {
                            "imgFlag": {
                                "src": flagImage.trim()
                            },
                            "lblCurrency": {
                                "text": desp,
                                "skin": "lblIB18pxBlack"
                            },
                            "lblCurrencyDesc": {
                                "text": currencyDesp,
                                "skin": "lblIB16pxBlack"
                            }
                        },
                        "column2": {
                            "lblAmt": {
                                "text": resulttable["exchangeRates"][i]["buyingDdTc"],
                                "skin": "lblIB18pxBlack"
                            }
                        },
                        "column3": {
                            "lblAmt": {
                                "text": resulttable["exchangeRates"][i]["buyingTt"],
                                "skin": "lblIB18pxBlack"
                            }
                        },
                        "column4": {
                            "lblAmt": {
                                "text": resulttable["exchangeRates"][i]["buyingNotes"],
                                "skin": "lblIB18pxBlue"
                            }
                        },
                        "column5": {
                            "lblAmt": {
                                "text": resulttable["exchangeRates"][i]["sellingBills"],
                                "skin": "lblIB18pxBlack"
                            }
                        },
                        "column6": {
                            "lblAmt": {
                                "text": resulttable["exchangeRates"][i]["sellingNotes"],
                                "skin": "lblIB18pxBlue"
                            }
                        }
                    }
                    exgRateResultTableIB.push(tempDataIB);
                    updateTime = resulttable["exchangeRates"][i]["updateDateTime"];
                }
                exchangeRatesDataGridHeaders()
                var myTime = updateTime;
                var dateForm = myTime.substring(0, 8);
                var timeForm = myTime.substring(9, 17);
                var timeStr = dateForm + ", " + timeForm;
                frmIBExchangeRates.lblUpdatedTime.text = timeStr;
                frmIBExchangeRates.dataGridExgRates.data = exgRateResultTableIB;
                dismissLoadingScreenPopup();
                frmIBExchangeRates.show();
            } else {
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } catch (e) {}
}

function exchangeRatesDataGridHeaders() {
    var headertext = frmIBExchangeRates.dataGridExgRates.columnHeadersConfig;
    headertext[0].columnheadertemplate.data.lblHdrName.text = "  ";
    for (var i = 1; i < 6; i++) {
        headertext[i].columnheadertemplate.data.lblHdrName.skin = "lblIB14pxBoldBlack";
    }
    headertext[1].columnheadertemplate.data.lblHdrName.text = kony.i18n.getLocalizedString("ExgRates_DD_TC");
    headertext[2].columnheadertemplate.data.lblHdrName.text = kony.i18n.getLocalizedString("ExgRates_TT");
    headertext[3].columnheadertemplate.data.lblHdrName.text = kony.i18n.getLocalizedString("ExgRates_Notes");
    headertext[4].columnheadertemplate.data.lblHdrName.text = kony.i18n.getLocalizedString("ExgRates_Bills");
    headertext[5].columnheadertemplate.data.lblHdrName.text = kony.i18n.getLocalizedString("ExgRates_Notes");
}

function exchangeRatefooterLinks() {
    try {
        var locale = kony.i18n.getCurrentLocale();
        if (gblIsNewOffersExists) {
            hbxFooterPrelogin.linkHotPromoImg.isVisible = true;
        } else {
            hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
        }
        frmIBExchangeRates.hbxFooterPrelogin.linkFindTmb.skin = "footerLinkFind";
        frmIBExchangeRates.hbxFooterPrelogin.linkExchangeRate.skin = "footerLinkERFoc";
        frmIBExchangeRates.hbxFooterPrelogin.linkSiteTour.skin = "footerLinkST";
        frmIBExchangeRates.hbxFooterPrelogin.linkTnC.skin = "footerLinkTC";
        frmIBExchangeRates.hbxFooterPrelogin.linkSecurity.skin = "footerLinkSecurity";
    } catch (e) {}
}