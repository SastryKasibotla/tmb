/*
************************************************************************
            Name    : checkDuplicateNicknameOnAddTopUp()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To check for duplicate Names when adding Topups
        Input params: None
       Output params: boolean 
        ServiceCalls: nil
        Called From : clicking of Next button on frmAddTopUpToMB form
************************************************************************
 */
//noOfRowsOfSegMyTopUpList = 0;
function checkDuplicateNicknameOnAddTopUp() {
    //var TopUplist = frmMyTopUpList.segBillersList.data;
    var TopUplist = myTopupListMB;
    var ConfirmationList = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
    var nickname = frmAddTopUpToMB.txbNickName.text;
    //alert("Nickname " + nickname);
    var check = "";
    var val = "";
    for (var i = 0;
        (((TopUplist) != null) && i < TopUplist.length); i++) {
        val = TopUplist[i].lblBillerNickname.text;
        if (kony.string.equals(nickname, val)) {
            return false;
        }
    }
    for (var j = 0;
        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
        val = ConfirmationList[j].lblCnfrmNickName;
        if (kony.string.equals(nickname, val)) {
            return false;
        }
    }
    return true;
}

function isBillerSelected() {
    if (!isNotBlank(frmAddTopUpToMB.lblAddbillerName.text)) {
        return false;
    } else {
        return true;
    }
}

function nicknameRef1Ref2ValidationCheck() {
    var flagNick = validationNickName(frmAddTopUpToMB.txbNickName.text);
    var flagRef1 = validateRef1ValueMB(frmAddTopUpToMB.txtRef1.text);
    if (gblIsRef2RequiredMB) {
        var flagRef2 = validateRef2ValueMB(frmAddTopUpToMB.txtRef2.text);
        if (!flagNick || !flagRef1 || !flagRef2) {
            if (!flagNick) {
                alert("Wrong Nickname");
            }
            if (!flagRef1) {
                alert("Wrong Ref1 Value");
            }
            if (!flagRef2) {
                alert("Wrong Ref2 Value");
            }
        } else {
            if (gblMyBillerTopUpBB == 0) {
                addBillerToMBNext();
            } else if (gblMyBillerTopUpBB == 1) {
                addTopUpToMBNext();
            }
        }
    } else {
        if (!flagNick || !flagRef1) {
            if (!flagNick) {
                alert("Wrong Nickname");
            }
            if (!flagRef1) {
                alert("Wrong Ref1 Value");
            }
        } else {
            if (gblMyBillerTopUpBB == 0) {
                addBillerToMBNext();
            } else if (gblMyBillerTopUpBB == 1) {
                addTopUpToMBNext();
            }
        }
    }
}
/*
************************************************************************
            Name    : addTopUpToMBNext()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To populate the added topup to confirmation screen
        Input params: None
       Output params: none 
        ServiceCalls: nil
        Called From : Next of addTopUpToMB
************************************************************************
 */
function addTopUpToMBNext() {
    var billerMethodFlagMB = null;
    //var billerTopupType = 4 ; // 3 for MB biller and 4 for MB topup
    //verifyBillerMethod(billerTopupType);
    var nickname = frmAddTopUpToMB.txbNickName.text;
    var ref1Val = frmAddTopUpToMB.txtRef1.text;
    var ref2Val = "";
    var billerName = frmAddTopUpToMB.lblAddbillerName.text;
    callBillerValidationService(gblCompCode, nickname, ref1Val, ref2Val, billerName, channelFlagTopupMB);
}
var gblFlagConfirmDataAddedMB = 0;

function loadTopupConfirmSegmentDataMB() {
    //if(billerMethodFlagMB==true){
    //	alert("GLOBAL biller ID filler confirm seg"+ gblBillerIdMB);
    //below line is added for CR - PCI-DSS masked Credit card no
    var maskedRef1 = maskCreditCard(frmAddTopUpToMB.txtRef1.text);
    var dataSeg = [{
        lblCnfrmNickName: frmAddTopUpToMB.txbNickName.text,
        imgTopUPBiller: frmAddTopUpToMB.imgAddedBiller.src,
        lblNameComp: frmAddTopUpToMB.lblAddbillerName.text,
        lblConfrmRef1: frmAddTopUpToMB.lblAddedRef1.text,
        lblcnfrmRef1Value: frmAddTopUpToMB.txtRef1.text,
        lblcnfrmRef1ValueMasked: maskedRef1,
        btnDlt: {
            skin: "btnDelete"
        },
        BillerCompCode: gblCompCode,
        BillerId: gblBillerIdMB,
        BillerNameEN: gblBillerCompCodeEN,
        BillerNameTH: gblBillerCompCodeTH,
        Ref1LblEN: gblCurRef1LblEN,
        Ref1LblTH: gblCurRef1LblTH,
        Ref2LblEN: gblCurRef2LblEN,
        Ref2LblTH: gblCurRef2LblTH
    }];
    frmAddTopUpBillerconfrmtn.segConfirmationList.addAll(dataSeg);
    gblFlagConfirmDataAddedMB = 1;
    var billerCount = "";
    billerCount = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
    var maxBulkAddCount = kony.os.toNumber(GLOBAL_MAX_BILL_ADD);
    if (billerCount.length >= maxBulkAddCount) {
        frmMyTopUpList.button1010778103103263.setEnabled(false);
        disableAddButtonTopUpBillerConfirm();
        frmMyTopUpList.segSuggestedBillers.setEnabled(false);
        gblBlockBillerTopUpAdd = true;
    }
    frmAddTopUpBillerconfrmtn.show();
    //TMBUtil.DestroyForm(frmAddTopUpToMB);
    //added to avoid crash appearing sometimes in iPHONE
    frmAddTopUpToMB.txbNickName = "";
    frmAddTopUpToMB.txtRef1 = "";
}

function disableAddButtonTopUpBillerConfirm() {
    frmAddTopUpBillerconfrmtn.btnRight.setEnabled(false);
    frmAddTopUpBillerconfrmtn.btnRight.skin = btnAddDisableRect;
    frmAddTopUpBillerconfrmtn.btnRight.focusSkin = btnAddDisableRect;
}

function enableAddButtonTopUpBillerConfirm() {
    frmAddTopUpBillerconfrmtn.btnRight.setEnabled(true);
    frmAddTopUpBillerconfrmtn.btnRight.skin = btnAddRecipient;
    frmAddTopUpBillerconfrmtn.btnRight.focusSkin = btnAddRecipient;
}

function addMoreBills() {
    gblAddBillerFromPay = false;
    TMBUtil.DestroyForm(frmAddTopUpToMB);
    frmAddTopUpToMB.show();
}
/*
************************************************************************
            Name    : selctedTopUpBiller()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To populate the selected topup to add top  up screen screen
        Input params: None
       Output params: none 
        ServiceCalls: nil
        Called From : row click of segment in frmMyTopUpSelect
************************************************************************
 */
function selctedTopUpBiller() {
    if (checkMBUserStatus()) {
        var isBarCode = frmMyTopUpSelect.segSelectList.selectedItems[0].BarcodeOnly;
        isBarCode = "0";
        if (kony.string.equalsIgnoreCase(isBarCode, "1")) {
            alert("Cannot Add Biller - Only for Barcode");
        } else {
            gblCompCode = "";
            gblBillerIdMB = "";
            gblBillerMethod = "";
            gblRef2FlagMB = "";
            gblRef1LenMB = "";
            gblRef2LenMB = "";
            gblIsRef2RequiredMB = "";
            var indexOfSelectedIndex = frmMyTopUpSelect.segSelectList.selectedItems[0];
            gblIsRef2RequiredMB = indexOfSelectedIndex.IsRequiredRefNumber2Add.text;
            gblreccuringDisablePay = indexOfSelectedIndex.IsRequiredRefNumber2Pay.text;
            gblreccuringDisableAdd = indexOfSelectedIndex.IsRequiredRefNumber2Add.text;
            gblToAccountKey = indexOfSelectedIndex.ToAccountKey.text;
            gblbillerGroupType = indexOfSelectedIndex.BillerGroupType;
            gblBillerIdMB = indexOfSelectedIndex.BillerID.text;
            gblPayFull = indexOfSelectedIndex.IsFullPayment;
            gblBillerBancassurance = indexOfSelectedIndex.billerBancassurance;
            gblAllowRef1AlphaNum = indexOfSelectedIndex.allowRef1AlphaNum;
            gblBillerMethod = indexOfSelectedIndex.BillerMethod.text;
            gblEffDtMB = indexOfSelectedIndex.EffDt.text;
            gblCompCode = indexOfSelectedIndex.BillerCompCode.text;
            //ENh 113
            if (gblCompCode == "2533") {
                gblSegBillerDataMB = frmMyTopUpSelect.segSelectList.selectedItems[0]
            }
            gBillerStartTime = indexOfSelectedIndex.billerStartTime;
            gBillerEndTime = indexOfSelectedIndex.billerEndTime;
            gblRef1LenMB = indexOfSelectedIndex.Ref1Len.text;
            gblBillerCategoryID = frmMyTopUpSelect.segSelectList.selectedItems[0].BillerCategoryID.text;
            if (gblBillerBancassurance == "Y" || gblAllowRef1AlphaNum == "Y") {
                frmAddTopUpToMB.txtRef1.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
                frmAddTopUpToMB.txtRef1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT;
            } else {
                frmAddTopUpToMB.txtRef1.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
                frmAddTopUpToMB.txtRef1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            }
            frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblRef1LenMB);
            frmAddTopUpToMB.lblAddedRef1.text = appendColon(indexOfSelectedIndex.Ref1Label.text);
            gblRef2LenMB = indexOfSelectedIndex.Ref2Len.text;
            gblIsRef2RequiredMB = gblIsRef2RequiredMB + "";
            if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "N")) {
                frmAddTopUpToMB.lblAddedRef2.setVisibility(false);
                frmAddTopUpToMB.txtRef2.setVisibility(false);
                frmAddTopUpToMB.lineRef2.setVisibility(false);
                frmAddTopUpToMB.hbxref2.setVisibility(false);
                gblRef2FlagMB = false;
                //gblRef2LenMB = 0;
            } else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "Y")) {
                gblRef2FlagMB = true;
                frmAddTopUpToMB.lblAddedRef2.setVisibility(true);
                frmAddTopUpToMB.txtRef2.setVisibility(true);
                frmAddTopUpToMB.lblAddedRef2.text = appendColon(indexOfSelectedIndex.Ref2Label.text);
                //gblRef2LenMB = indexOfSelectedIndex.Ref2Len.text;
                frmAddTopUpToMB.txtRef2.maxTextLength = parseInt(gblRef2LenMB);
                frmAddTopUpToMB.lineRef2.setVisibility(true);
                frmAddTopUpToMB.hbxref2.setVisibility(true);
            }
            gblCurRef1LblEN = indexOfSelectedIndex.hdnLabelRefNum1EN.text;
            gblCurRef1LblTH = indexOfSelectedIndex.hdnLabelRefNum1TH.text;
            gblCurRef2LblEN = indexOfSelectedIndex.hdnLabelRefNum2EN.text;
            gblCurRef2LblTH = indexOfSelectedIndex.hdnLabelRefNum2TH.text;
            gblBillerCompCodeEN = indexOfSelectedIndex.lblBillerNameEN;
            gblBillerCompCodeTH = indexOfSelectedIndex.lblBillerNameTH;
            gblRef1LblEN = gblCurRef1LblEN;
            gblRef1LblTH = gblCurRef1LblTH;
            gblRef2LblEN = gblCurRef2LblEN;
            gblRef2LblTH = gblCurRef2LblTH;
            frmAddTopUpToMB.imgAddedBiller.src = indexOfSelectedIndex.imgSuggestedBiller.src;
            frmAddTopUpToMB.lblAddbillerName.text = indexOfSelectedIndex.lblSuggestedBiller.text;
            frmAddTopUpToMB.show();
        }
    }
}
/*
************************************************************************
            Name    : onMyTopUpSelectMore()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To Display 15 more data on click of more Link
        Input params: None
       Output params: none 
        ServiceCalls: nil
        Called From : click of more Link on MyTopUpSelect form
************************************************************************
 */
//function onMyTopUpSelectMore() {
//	var maxLength = 15; //configurable parameter also initilaize gblTopUpMore to max length
//	gblTopUpMore = gblTopUpMore + maxLength;
//	var newData = [];
//	var totalData = segSelectData; //data from static population 
//	var totalLength = totalData.length;
//	if (totalLength > gblTopUpMore) {
//		frmMyTopUpSelect.hbxLink.isVisible = true;
//		var endPoint = gblTopUpMore
//	} else {
//		frmMyTopUpSelect.hbxLink.isVisible = false;
//		var endPoint = totalLength;
//	}
//	for (i = 0; i < endPoint; i++) {
//		newData.push(segSelectData[i])
//	}
//	frmMyTopUpSelect.segSelectList.data = newData;
//}
/*
************************************************************************
            Name    : onMytopUpConfirm()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To populate static data on topup select screen
        Input params: None
       Output params: none 
        ServiceCalls: nil
        Called From : frmAddTopUpBillerConfrmtn confirm button 
************************************************************************
 */
function onMytopUpConfirm() {
    if (flowSpa) {
        if (gblIBFlowStatus == "04") {
            alertUserStatusLocked();
        } else {
            var inputParams = {}
            spaChnage = "addbillertopup"
            gblOTPFlag = true;
            try {
                kony.timer.cancel("otpTimer")
            } catch (e) {}
            //input params for SPA OTP
            var locale = kony.i18n.getCurrentLocale();
            gblSpaChannel = "MyBiller";
            /*
            if (locale == "en_US") {
            		gblSpaChannel = "IB_BILLER_EN";
            		SpaEventNotificationPolicy = "MIB_MyBiller_EN";
            		SpaSMSSubject = "MIB_MyBiller_EN";
            	} else {
            		gblSpaChannel = "IB_BILLER_TH";
            		SpaEventNotificationPolicy = "MIB_MyBiller_TH";
            		SpaSMSSubject = "MIB_MyBiller_TH";
            }*/
            if (frmAddTopUpBillerconfrmtn.segConfirmationList.data != null) gblAccountDetails = frmAddTopUpBillerconfrmtn.segConfirmationList.data.length;
            onClickOTPRequestSpa();
        }
    } else {
        var lblText = kony.i18n.getLocalizedString("transPasswordSub");
        var refNo = "";
        var mobNO = "";
        showOTPPopup(lblText, refNo, mobNO, topupConfirmationAgree, 3);
    }
}
/*
************************************************************************
            Name    : topupConfirmationAgree()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To validate transaction password and populate the complete screen
        Input params: None
       Output params: none 
        ServiceCalls: nil
        Called From : call back of onMytopUpConfirm
************************************************************************
 */
function topupConfirmationAgree(tranPassword) {
    /*var transFlag = trassactionPwdValidatn(tranPassword);
    if (transFlag != false) {*/
    if (tranPassword == null || tranPassword == '') {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    } else {
        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
        popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
        //verifyPasswordBillerTopup(tranPassword);
        var module = ""
        if (gblMyBillerTopUpBB == 0) module = "AddBillersMB"
        else module = "AddTopupsMB"
        validateOTPtextBillTopUpJavaService(tranPassword, module, false)
    }
}
/*
************************************************************************
            Name    : onclickOfSuggestedBillersAdd()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To add a top up from suggested billers
        Input params: none
       Output params: none
        ServiceCalls: nil
        Called From :on click of suggested billers row in frmMyToplist
************************************************************************
 */
function onclickOfSuggestedBillersAdd() {
    var isBarCode = frmMyTopUpList.segSuggestedBillers.selectedItems[0].BarcodeOnly;
    if (kony.string.equalsIgnoreCase(isBarCode, "1")) {
        alert("Cannot Add Biller - Only for Barcode");
    } else {
        gblCompCode = "";
        gblBillerIdMB = "";
        gblBillerMethod = "";
        gblRef2FlagMB = "";
        gblRef1LenMB = "";
        gblRef2LenMB = "";
        gblIsRef2RequiredMB = "";
        gblIsRef2RequiredMB = frmMyTopUpList.segSuggestedBillers.selectedItems[0].IsRequiredRefNumber2Add.text;
        gblBillerIdMB = frmMyTopUpList.segSuggestedBillers.selectedItems[0].BillerID.text;
        gblBillerMethod = frmMyTopUpList.segSuggestedBillers.selectedItems[0].BillerMethod.text;
        gblEffDtMB = frmMyTopUpList.segSuggestedBillers.selectedItems[0].EffDt.text;
        gblPayFull = frmMyTopUpList.segSuggestedBillers.selectedItems[0].IsFullPayment;
        gblBillerBancassurance = frmMyTopUpList.segSuggestedBillers.selectedItems[0].billerBancassurance;
        gblAllowRef1AlphaNum = frmMyTopUpList.segSuggestedBillers.selectedItems[0].allowRef1AlphaNum;
        gblCompCode = frmMyTopUpList.segSuggestedBillers.selectedItems[0].BillerCompCode.text;
        gblRef1LenMB = frmMyTopUpList.segSuggestedBillers.selectedItems[0].Ref1Len.text;
        if (gblBillerBancassurance == "Y" || gblAllowRef1AlphaNum == "Y") {
            frmAddTopUpToMB.txtRef1.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
            frmAddTopUpToMB.txtRef1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT;
        } else {
            frmAddTopUpToMB.txtRef1.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
            frmAddTopUpToMB.txtRef1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
        }
        frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblRef1LenMB);
        frmAddTopUpToMB.lblAddedRef1.text = frmMyTopUpList.segSuggestedBillers.selectedItems[0].Ref1Label.text + ":";
        gblBillerCategoryID = frmMyTopUpList.segSuggestedBillers.selectedItems[0].BillerCategoryID.text;
        gblCurRef1LblEN = frmMyTopUpList.segSuggestedBillers.selectedItems[0].hdnLabelRefNum1EN.text;
        gblCurRef1LblTH = frmMyTopUpList.segSuggestedBillers.selectedItems[0].hdnLabelRefNum1TH.text;
        gblCurRef2LblEN = frmMyTopUpList.segSuggestedBillers.selectedItems[0].hdnLabelRefNum2EN.text;
        gblCurRef2LblTH = frmMyTopUpList.segSuggestedBillers.selectedItems[0].hdnLabelRefNum2TH.text;
        gblBillerCompCodeEN = frmMyTopUpList.segSuggestedBillers.selectedItems[0].lblBillerNameEN;
        gblBillerCompCodeTH = frmMyTopUpList.segSuggestedBillers.selectedItems[0].lblBillerNameTH;
        //ENh 113
        gBillerStartTime = frmMyTopUpList.segSuggestedBillers.selectedItems[0].billerStartTime;
        gBillerEndTime = frmMyTopUpList.segSuggestedBillers.selectedItems[0].billerEndTime;
        if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "N")) {
            frmAddTopUpToMB.lblAddedRef2.setVisibility(false);
            frmAddTopUpToMB.txtRef2.setVisibility(false);
            frmAddTopUpToMB.lineRef2.setVisibility(false);
            frmAddTopUpToMB.hbxref2.setVisibility(false);
            gblRef2FlagMB = false;
            gblRef2LenMB = 0;
        } else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "Y")) {
            gblRef2FlagMB = true;
            frmAddTopUpToMB.lblAddedRef2.setVisibility(true);
            frmAddTopUpToMB.txtRef2.setVisibility(true);
            frmAddTopUpToMB.lblAddedRef2.text = frmMyTopUpList.segSuggestedBillers.selectedItems[0].Ref2Label.text + ":";
            gblRef2LenMB = frmMyTopUpList.segSuggestedBillers.selectedItems[0].Ref2Len.text;
            frmAddTopUpToMB.txtRef2.maxTextLength = parseInt(gblRef2LenMB);
            frmAddTopUpToMB.lineRef2.setVisibility(true);
            frmAddTopUpToMB.hbxref2.setVisibility(true);
        }
        var indexOfSelectedRow = frmMyTopUpList.segSuggestedBillers.selectedIndex[1];
        var indexOfSelectedIndex = frmMyTopUpList.segSuggestedBillers.selectedItems[0];
        frmAddTopUpToMB.imgAddedBiller.src = indexOfSelectedIndex.imgSuggestedBiller.src;
        frmAddTopUpToMB.lblAddbillerName.text = indexOfSelectedIndex.lblSuggestedBiller.text;
        frmAddTopUpToMB.show();
    }
}
/*
************************************************************************
            Name    : viewTopupBack()
            Author  : Kumar Abhisek
            Date    : 07/06/2013
            Purpose : To view mytop up list after editing the nickname
        Input params: none
       Output params: none
        ServiceCalls: nil
        Called From :on back button click in myTopUpView
************************************************************************
 */
function viewTopupBack() {
    var editedData = {
        imgTopUPBiller: frmViewTopUpBiller.imgTopUPBiller.src,
        lblCnfrmNickName: frmViewTopUpBiller.lblCnfrmNickName.text,
        lblConfrmRef1: frmViewTopUpBiller.lblConfrmRef1.text,
        lblcnfrmRef1Value: frmViewTopUpBiller.lblcnfrmRef1Value.text,
        lblNameComp: frmViewTopUpBiller.lblCnfrmBillerNameComp.text
    };
    var segmentData = frmMyTopUpList.segBillersList.data;
    segmentData[gblTopupDelete].lblCnfrmNickName = frmViewTopUpBiller.lblCnfrmNickName.text;
    //alert("segment Data"+segmentData[gblTopupDelete].lblCnfrmNickName)
    //alert("nickname"+frmViewTopUpBiller.lblCnfrmNickName.text)
    frmMyTopUpList.segBillersList.data = segmentData;
    frmMyTopUpList.show();
    frmViewTopUpBiller.imgComplete.setVisibility(false);
}
//function categoryPopUpRowClick(currForm) {
//	//popUpMyBillers.show();
//	var selected = popUpMyBillers.segPop.selectedIndex[1];
//	var data = popUpMyBillers.segPop.data;
//	var selCategory = data[selected].lblCategory;
//	currForm.btnSelectCat.text = selCategory;
//	popUpMyBillers.dismiss();
//}
function checkDuplicateBillerTopupCompCodeRef1() {
    var BillerList = gblCustomerBillList;
    var ConfirmationList = {};
    //	ConfirmationList.length = 0 ;
    if (gblFlagConfirmDataAddedMB == 1) {
        ConfirmationList = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
        //  
    } else {
        frmAddTopUpBillerconfrmtn.segConfirmationList.data = {};
        ConfirmationList.length = 0;
        //  
    }
    var ref1 = frmAddTopUpToMB.txtRef1.text;
    var ref2 = frmAddTopUpToMB.txtRef2.text;
    var compCode = gblCompCode;
    var valCompCode = "";
    var valRef1 = "";
    var valRef2 = "";
    var valRef = "";
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {
        valCompCode = BillerList[i].BillerCompCode.text;
        valRef1 = BillerList[i].lblRef1Value.text;
        valRef2 = BillerList[i].lblRef2Value.text;
        if (compCode == valCompCode && ref1 == valRef1) {
            if (gblIsRef2RequiredMB == "Y") {
                if (ref2 == valRef2) {
                    return false; //Duplicate Biller found
                }
            } else {
                return false; //Duplicate Biller found
            }
        }   
    }
    for (var j = 0;
        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
        valCompCode = ConfirmationList[j].BillerCompCode;
        valRef1 = ConfirmationList[j].lblcnfrmRef1Value;
        valRef2 = ConfirmationList[j].lblcnfrmRef2Value;
        if (compCode == valCompCode && ref1 == valRef1) {
            if (gblIsRef2RequiredMB == "Y") {
                if (ref2 == valRef2) {
                    return false; //Duplicate Biller found
                }
            } else {
                return false; //Duplicate Biller found
            }
        }   
    }
    return true; //No Duplicate Biller
}

function tokenExchangeBeforeSaveParamsInSessSPA() {
    var inputParam = [];
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, tokenExchangeBeforeSaveParamsInSessSPACallBack);
}

function tokenExchangeBeforeSaveParamsInSessSPACallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            addBillerTopupValidationsMB();
        }
    }
}

function addBillerTopupValidationsMB() {
    var billerTopUpNickName = frmAddTopUpToMB.txbNickName.text;
    if (!NickNameValid(billerTopUpNickName)) {
        frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
        frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
        alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
        return;
    }
    var ref1value = frmAddTopUpToMB.txtRef1.text;
    var duplicateRef1CompCodeFlag = checkDuplicateBillerTopupCompCodeRef1()
    if (gblMyBillerTopUpBB == 0) {
        var ref2result = false;
        var ref2value = "";
        //Empty check 
        if (frmAddTopUpToMB.txtRef1.text.length == 0) {
            alert(kony.i18n.getLocalizedString("keyPleaseEnter") + " " + frmAddTopUpToMB.lblAddedRef1.text);
            return;
        }
        if (gblIsRef2RequiredMB == "Y") {
            var ref2value = frmAddTopUpToMB.txtRef2.text;
            if (frmAddTopUpToMB.txtRef2.text.length == 0) {
                alert(kony.i18n.getLocalizedString("keyPleaseEnter") + " " + frmAddTopUpToMB.lblAddedRef2.text);
                return;
            }
            ref2result = validateRef2ValueMB(ref2value);
        } else {
            ref2value = "";
            ref2result = true;
        }
        if (duplicateRef1CompCodeFlag && ref2result) {
            addBillerToMBNext();
        } else {
            if (!duplicateRef1CompCodeFlag) {
                alert(kony.i18n.getLocalizedString("keyduplicatebiller"));
            } else if (!ref2result) {
                //alert(kony.i18n.getLocalizedString("keyWrngRef2Val"));
                alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
                return;
            }
        }
    } else if (gblMyBillerTopUpBB == 1) {
        if (duplicateRef1CompCodeFlag) {
            addTopUpToMBNext();
        } else {
            if (!duplicateRef1CompCodeFlag) {
                alert(kony.i18n.getLocalizedString("keyduplicatebiller"));
                return;
            } else if (!ref2result) {
                alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
                return;
            }
        }
    }
}

function frmIBTopUpComplete_frmIBTopUpComplete_postshow() {
    addIBMenu();
    campaginService("img1", "frmIBTopUpComplete", "I");
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBTopUpComplete.btnMenuTopUp.skin = "btnIBMenuTopUpFocus";
    else frmIBTopUpComplete.btnMenuTopUp.skin = "btnIBMenuTopUpFocusThai";
};

function frmIBEditFutureTopUpPrecnf_frmIBEditFutureTopUpPrecnf_preshow() {
    frmIBEditFutureTopUpPrecnf.hbxAdv.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(false);
    frmIBEditFutureTopUpPrecnf.btnNext.setVisibility(true);
    var curFrm = kony.application.getCurrentForm().id;
    if (curFrm == "frmIBFBLogin") {
        frmIBEditFutureTopUpPrecnf.hbxTPCmpletngHdr.setVisibility(true);
        frmIBEditFutureTopUpPrecnf.imgComplete.setVisibility(true);
        frmIBEditFutureTopUpPrecnf.hbxbtnUpdatedReturn.setVisibility(true);
        frmIBEditFutureTopUpPrecnf.hbxTPSave.setVisibility(true);
        frmIBEditFutureTopUpPrecnf.hbxTPViewHdr.setVisibility(false);
        frmIBEditFutureTopUpPrecnf.hbxOTP.setVisibility(false);
        frmIBEditFutureTopUpPrecnf.hbxEditBtns.setVisibility(false);
        frmIBEditFutureTopUpPrecnf.btnNext.setVisibility(false);
        frmIBEditFutureTopUpPrecnf.hbxAdv.setVisibility(true);
    }
};

function frmIBEditFutureTopUpPrecnf_frmIBEditFutureTopUpPrecnf_postshow() {
    addIBMenu();
    //checking below condition to show banner in complete screen, frmIBEditFutureTopUpPrecnf form is used in total flow
    if (frmIBEditFutureTopUpPrecnf.imgComplete.isVisible == true || frmIBEditFutureTopUpPrecnf.imgComplete.isVisible == "true") {
        campaginService("imgAd1", "frmIBEditFutureTopUpPrecnf", "I");
    }
    pagespecificSubMenu();
};