function setSortBtnSkinMBMF() {
	gblStmntSessionFlag= "2";
	noOfStmtServiceCalls=0;
	bindata="";
	binlength="";
	frmMFFullStatementMB.segcredit.removeAll();
	frmMFFullStatementMB.segOrdToPrced.removeAll();
}
//on click of segment, get the details
function getdescriptionMFMB(){
	frmMFFullStatementMB.segcredit.widgetDataMap={
			lblDate: "lblDate",
			lblAmount: "lblAmount",
			lblDescription: "lblDescription",
			ishidden: "yes",
			effectiveDatelbl:"effectiveDatelbl",
			effectiveDate: "effectiveDate",
			unitlbl:"unitlbl",
			unit: "unit",
			investmentlbl:"investmentlbl",
			investmentVal:"investmentVal",
			unitBalLbl:"unitBalLbl",
			unitBal: "unitBal",
			pricelbl:"pricelbl",
			price:"price",
			channel:"channel",
			channelexpval:"channelexpval"
	};
	var descstmt;
	var channelstmt; 
	var tab1=[];
	var indexOfSelectedIndex = frmMFFullStatementMB.segcredit.selectedItems[0];
	var indexOfSelectedRow = frmMFFullStatementMB.segcredit.selectedIndex[1];
	var indexOfSelectedRowexp = gblsegmentdataMB[indexOfSelectedRow];
	if(indexOfSelectedIndex.ishidden == "yes"){
		var selectedData = {
			lblDate: indexOfSelectedIndex.lblDate,
			lblAmount: indexOfSelectedIndex.lblAmount,
			lblDescription: indexOfSelectedIndex.lblDescription,
			ishidden: "no",
			effectiveDatelbl:kony.i18n.getLocalizedString("MF_thr_TransactionDate")+":",
			effectiveDate: indexOfSelectedRowexp.effectiveDate,
			unitlbl:kony.i18n.getLocalizedString("MF_lbl_Unit"),
			unit: indexOfSelectedRowexp.unit,
			investmentlbl:kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2"),
			investmentVal:indexOfSelectedRowexp.investmentVal,
			unitBalLbl:kony.i18n.getLocalizedString("MF_thr_Unit_balance")+":",
			unitBal: indexOfSelectedRowexp.unitBal,
			pricelbl:kony.i18n.getLocalizedString("MF_thr_Price")+":",
			price:indexOfSelectedRowexp.price,
			channel:kony.i18n.getLocalizedString("MF_lbl_Channel"),
			channelexpval:indexOfSelectedRowexp.channelexpval,
			template: hbcActStmtMF
				};
			}
	if(indexOfSelectedIndex.ishidden == "no")
	{
		var selectedData = {
			lblDate: indexOfSelectedIndex.lblDate,
			lblAmount: indexOfSelectedIndex.lblAmount,
			lblDescription: indexOfSelectedIndex.lblDescription,
			ishidden: "yes",
			template: hbxaccntstmt
			};
	}
	
	kony.table.insert(tab1,selectedData);
	var tab2 = []
			for (var i = 0; i < indexOfSelectedRow; i++) {
			
				kony.table.insert(tab2, gblsegmentdataMB[i])
			}
			kony.table.append(tab2, tab1);
			for (var j = indexOfSelectedRow + 1; j < gblsegmentdataMB.length; j++) {
				kony.table.insert(tab2, gblsegmentdataMB[j]);
			}
			frmMFFullStatementMB.segcredit.removeAll();
			frmMFFullStatementMB.segcredit.setData(tab2);
}



function MBcallMutualFundsSummary(){

 	 //#ifdef iphone
		TMBUtil.DestroyForm(frmMutualFundsSummaryLanding);		
	 //#endif
	 
	 showLoadingScreen();
	 var inputParam = {};
     invokeServiceSecureAsync("MFUHAccountSummaryInq", inputParam, MBcallMutualFundsSummaryCallBack);
}
function MBcallMutualFundsSummaryCallBack(status,resulttable){
	if (status == 400) {
		dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
        	gblMFSummaryData=resulttable;
        	
        	if (kony.i18n.getCurrentLocale() == "en_US"){
				frmMutualFundsSummaryLanding.lblAccntHolderName.text = gblCustomerName;
			}else{
				frmMutualFundsSummaryLanding.lblAccntHolderName.text = gblCustomerNameTh;
			}
			frmMutualFundsSummaryLanding.label475124774164.text=kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
			frmMutualFundsSummaryLanding.lblBalanceValue.text=kony.i18n.getLocalizedString("MF_lbl_Investment_value_h1");
			frmMutualFundsSummaryLanding.btnBack1.text = kony.i18n.getLocalizedString("Back");
	        frmMutualFundsSummaryLanding.imgProfile.src = frmAccountSummaryLanding.imgProfile.src;
	 		frmMutualFundsSummaryLanding.lblInvestmentValue.text = gblAccountTable["mfTotalAmount"]+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	 		frmMutualFundsSummaryLanding.show();
	 		frmMutualFundsSummaryLanding.scrollboxMain.scrollToEnd();
	     }
	}
}

function  MBpopulateDataMutualFundsSummary(summaryDataTotal){
	var summaryData=summaryDataTotal["FundClassDS"];
	var segmentDataTemp = [];
	var segmentHeaderTemp = [];
	var segmentData=[];
	var logo;
	var fundName;
	var unitHolderNumber;
	var unRealizedPL = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
	var unRealizedPLValue;
	var investment = kony.i18n.getLocalizedString("MF_lbl_Market_value");
	var investmentValue;
	var segmentTemplate;
	var locale = kony.i18n.getCurrentLocale();
	var header ;
	for(var i=0;i<summaryData.length;i++){
	
		segmentDataTemp = [];
		segmentHeaderTemp = [];
		
		var fundhouseDS = summaryData[i].FundHouseDS
		var fundHouseLength = fundhouseDS.length;
		
			
			var noOfAccounts = 0;
		
			for(var j=0;j<fundHouseLength;j++){
			
					var FundCodeDS = fundhouseDS[j].FundCodeDS;
					var FundCodeDSLength = FundCodeDS.length;
					
					var fundHosueCode = fundhouseDS[j]["FundHouseCode"]
					for(var k=0; k < FundCodeDSLength;k++){
						
						noOfAccounts++;
						logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
						 "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHosueCode+"&modIdentifier=MFLOGOS";
						unRealizedPLValue = FundCodeDS[k]["UnrealizedProfit"];
						var unRealizedPLValueSkin;
						
						if(unRealizedPLValue == "0"){
							unRealizedPLValueSkin = lblIB20pxBlack;
							unRealizedPLValue = commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						}else if(unRealizedPLValue.indexOf("-") < 0){
							unRealizedPLValueSkin = lblIB24pxRegGreen;
							unRealizedPLValue = "+" + commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						}else{
							unRealizedPLValueSkin = lblIB24pxRegRed;
							unRealizedPLValue = commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						}
						fundName = FundCodeDS[k]["FundNickNameEN"];
						if(locale != "en_US"){
							fundName = FundCodeDS[k]["FundNickNameTH"];
						}
						var dataObject = {
							imgLogo:logo,
							lblfundName:fundName,
							lblfundNickNameEN:FundCodeDS[k]["FundNickNameEN"],
							lblfundNickNameTH:FundCodeDS[k]["FundNickNameTH"],
							lblfundNameEN:FundCodeDS[k]["FundNameEN"],
							lblfundNameTH:FundCodeDS[k]["FundNameTH"],
							lblunitHolderNumber: formatUnitHolderNumer(FundCodeDS[k]["UnitHolderNo"]),
							lblunRealizedPL:unRealizedPL,
							lblunRealizedPLValue:{"text":unRealizedPLValue,"skin":unRealizedPLValueSkin},
							lblinvestment:investment,
							lblinvestmentValue:commaFormatted(FundCodeDS[k]["MarketValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
							template:MBMFContent,
							fundCode:FundCodeDS[k]["FundCode"],
							imageRightArrow:{"src":"navarrow.png"}
						};
					
						segmentDataTemp.push(dataObject);
						
					}//for loop end k
			}//for loop end j


			var moreThanOneAccnt = noOfAccounts > 1 ? "s" : "";
			if(locale == "en_US"){
				header = summaryData[i]["FundClassNameEN"] + " (" + noOfAccounts + " " + kony.i18n.getLocalizedString("MF_lbl_Account")+moreThanOneAccnt+")";
			}else{
				header = summaryData[i]["FundClassNameTH"] + " (" + noOfAccounts + " " +kony.i18n.getLocalizedString("MF_lbl_Account") +")";
			}
			var dataObjectHeader = {
				lblHead: header,
				lblHeadEN: summaryData[i]["FundClassNameEN"] + " (" + noOfAccounts + " ",
				lblHeadTH: summaryData[i]["FundClassNameTH"] + " (" + noOfAccounts + " ",
				template:MBMFHeader
			};
		segmentHeaderTemp.push(dataObjectHeader);

		segmentData = segmentData.concat(segmentHeaderTemp.concat(segmentDataTemp));
		
	}//for loop end i
	return segmentData;
}

function frmMutualFundsSummaryLandingPreShow(){
	changeStatusBarColor();
	if (kony.i18n.getCurrentLocale() == "en_US"){
		frmMutualFundsSummaryLanding.lblAccntHolderName.text = gblCustomerName;
	}else{
		frmMutualFundsSummaryLanding.lblAccntHolderName.text = gblCustomerNameTh;
	}
	frmMutualFundsSummaryLanding.label475124774164.text=kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
	frmMutualFundsSummaryLanding.lblBalanceValue.text=kony.i18n.getLocalizedString("MF_lbl_Investment_value_h1");
	////MIB-1418  > MIB-1669
	frmMutualFundsSummaryLanding.btnBack1.text = kony.i18n.getLocalizedString("Back");
	var segmentData= MBpopulateDataMutualFundsSummary(gblMFSummaryData);
    frmMutualFundsSummaryLanding.segAccountDetails.widgetDataMap ={
		lblHead: "lblHead",
       	imgLogo:"imgLogo",
		lblfundName:"lblfundName",
		lblfundNickNameTH:"lblfundNickNameTH",
		lblfundNickNameEN:"lblfundNickNameEN",
		lblunitHolderNumber: "lblunitHolderNumber",
		lblunRealizedPL:"lblunRealizedPL",
		lblunRealizedPLValue:"lblunRealizedPLValue",
		lblinvestment:"lblinvestment",
		lblinvestmentValue:"lblinvestmentValue",
		fundCode:"fundCode",
		imageRightArrow:"imageRightArrow"
	};
    frmMutualFundsSummaryLanding.segAccountDetails.removeAll();
   	frmMutualFundsSummaryLanding.segAccountDetails.setData(segmentData);
}

//Mutual fund details
function callMBMutualFundsDetails(unitHolderNuber,fundcode){
	if(unitHolderNuber == null || unitHolderNuber == undefined) {
		return;
	}
	
	gblUnitHolderNumber = unitHolderNuber;
	gblFundCode = fundcode;
	var inputParam = {};
	inputParam["unitHolderNo"] = unitHolderNuber;
	inputParam["funCode"] = fundcode;
	inputParam["serviceType"] = "1";
	showLoadingScreen();
    invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callMutualFundsDetailsCallBackMB);
}

function callMutualFundsDetailsCallBackMB(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	 dismissLoadingScreen();
        	gblMBMFDetailsResulttable = resulttable;
        	var locale = kony.i18n.getCurrentLocale();
        	gblSelFundNickNameEN = frmMutualFundsSummaryLanding.segAccountDetails.selectedItems[0].lblfundNickNameEN;
        	gblSelFundNickNameTH = frmMutualFundsSummaryLanding.segAccountDetails.selectedItems[0].lblfundNickNameTH;
        	var fundNickName = gblSelFundNickNameEN;
			if(locale != "en_US"){
				fundNickName = gblSelFundNickNameTH;
			}
        	frmMBMutualFundDetails.lblAccountNameHeader.text = fundNickName;
		    frmMBMutualFundDetails.lblAccountBalanceHeader.text = frmMutualFundsSummaryLanding.segAccountDetails.selectedItems[0].lblinvestmentValue;
		    frmMBMutualFundDetails.imgAccountDetailsPic.src = frmMutualFundsSummaryLanding.segAccountDetails.selectedItems[0].imgLogo;
        	if(resulttable["TaxDoc"] == "N"){
	        	frmMBMutualFundDetails.hboxTaxDoc.setVisibility(false);
        	}else{
	        	frmMBMutualFundDetails.hboxTaxDoc.setVisibility(true);
        	}
			var fundName="";
			if (locale == "th_TH"){ 
				fundName = resulttable["FundNameTH"];
			}else{
				fundName = resulttable["FundNameEN"];
			}
			gblFundName = fundName;
	        frmMBMutualFundDetails.lblUseful1.text = kony.i18n.getLocalizedString("MF_lbl_Fund_name");
	        frmMBMutualFundDetails.lblUsefulValue1.text = fundName;
	        frmMBMutualFundDetails.lblUseful2.text = kony.i18n.getLocalizedString("MF_lbl_Unit_holder_no");
	        frmMBMutualFundDetails.lblUsefulValue2.text = resulttable["UnitHolderNo"];
	        frmMBMutualFundDetails.lblUseful3.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
	        frmMBMutualFundDetails.lblUsefulValue3.text = resulttable["DateAsOf"];
	        frmMBMutualFundDetails.lblUseful4.text = kony.i18n.getLocalizedString("MF_lbl_Units");
	        frmMBMutualFundDetails.lblUsefulValue4.text = verifyDisplayUnit(resulttable["Unit"]);
	        frmMBMutualFundDetails.lblUseful5.text = kony.i18n.getLocalizedString("MF_lbl_NAV_unit");
	        frmMBMutualFundDetails.lblUsefulValue5.text = verifyDisplayUnit(resulttable["Nav"]);
	        frmMBMutualFundDetails.lblUseful6.text = kony.i18n.getLocalizedString("MF_lbl_Cost");
	        frmMBMutualFundDetails.lblUsefulValue6.text = commaFormatted(resulttable["Cost"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	        frmMBMutualFundDetails.lblUseful7.text = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
	        frmMBMutualFundDetails.lblUsefulValue7.text = commaFormatted(resulttable["InvestmentValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	        frmMBMutualFundDetails.lblUseful8.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
	        frmMBMutualFundDetails.lblUsefulValue8.text = getValueForUnrealPL(resulttable["UnrealizedProfit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	        frmMBMutualFundDetails.lblUsefulValue8.skin = getSkinValueForUnrealPLMB(resulttable["UnrealizedProfit"]);
	        frmMBMutualFundDetails.lblUseful9.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_percent");
	        frmMBMutualFundDetails.lblUsefulValue9.text = getValueForUnrealPL(resulttable["UnrealizedProfitPerc"]) + " %";
	       	frmMBMutualFundDetails.lblUsefulValue9.skin = getSkinValueForUnrealPLMB(resulttable["UnrealizedProfitPerc"]);
	       displayUnitLTF5YMB(resulttable["UnitLTF5Y"]);
			frmMBMutualFundDetails.hbxAccountDetails.setVisibility(true);
			frmMBMutualFundDetails.show();
        } else {
        	dismissLoadingScreen();
	        alert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
        }
     }else{
        dismissLoadingScreen();
     }
} 

function getSkinValueForUnrealPLMB(data){
	if(parseFloat(data) > 0){
		return lblIB18pxGreenNoMed;
	}else if(parseFloat(data) < 0){
		return lblIB18pxRedNoMed;
	}else{
		return lblGray;
	}
}
function displayUnitLTF5YMB(data){
	if(isNotBlank(data)){
		frmMBMutualFundDetails.hbxUseful10.setVisibility(true);
		frmMBMutualFundDetails.lblUseful10.text = kony.i18n.getLocalizedString("MF_lbl_LTF_Units");
		frmMBMutualFundDetails.lblUsefulValue10.text = verifyDisplayUnit(data);
	}else{
		frmMBMutualFundDetails.hbxUseful10.setVisibility(false);
		frmMBMutualFundDetails.lblUseful10.text = "";
	    frmMBMutualFundDetails.lblUsefulValue10.text = "";
	}
} 

function frmMBMutualFundsSummaryPreShow(){
	changeStatusBarColor();
	var locale = kony.i18n.getCurrentLocale();
	
	// MF Details
	frmMBMutualFundDetails.lblHead.text = kony.i18n.getLocalizedString("MF_Acc_Detail_Title");
	if(isNotBlank(gblFundName)) {
		frmMBMutualFundDetails.lblUsefulValue1.text = gblFundName;
	}
	if (kony.i18n.getCurrentLocale() == "en_US"){
		frmMBMutualFundDetails.lblAccountNameHeader.text = gblSelFundNickNameEN;
	}else{
		frmMBMutualFundDetails.lblAccountNameHeader.text = gblSelFundNickNameTH;
	}
	frmMBMutualFundDetails.lblOrdToBProcess.text = kony.i18n.getLocalizedString("MF_lbl_Order_tobe_process");
	frmMBMutualFundDetails.lnkFullStatement.text = kony.i18n.getLocalizedString("MF_lbl_Full_statement");
	frmMBMutualFundDetails.lnkTaxDoc.text = kony.i18n.getLocalizedString("MF_lbl_Tax_doc");
	frmMBMutualFundDetails.btnBack1.text = kony.i18n.getLocalizedString("Back");	
	frmMBMutualFundDetails.lblUseful1.text = kony.i18n.getLocalizedString("MF_lbl_Fund_name");
    frmMBMutualFundDetails.lblUseful2.text = kony.i18n.getLocalizedString("MF_lbl_Unit_holder_no");
    frmMBMutualFundDetails.lblUseful3.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
    frmMBMutualFundDetails.lblUseful4.text = kony.i18n.getLocalizedString("MF_lbl_Units");
    frmMBMutualFundDetails.lblUseful5.text = kony.i18n.getLocalizedString("MF_lbl_NAV_unit");
    frmMBMutualFundDetails.lblUseful6.text = kony.i18n.getLocalizedString("MF_lbl_Cost");
    frmMBMutualFundDetails.lblUseful7.text = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
    frmMBMutualFundDetails.lblUseful8.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
    frmMBMutualFundDetails.lblUseful9.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_percent");
	frmMBMutualFundDetails.lblUseful10.text = kony.i18n.getLocalizedString("MF_lbl_LTF_Units");
	frmMBMutualFundDetails.lblUsefulValue1.text = locale == "th_TH" ? gblMBMFDetailsResulttable["FundNameTH"] : gblMBMFDetailsResulttable["FundNameEN"];

}

function viewFullStatementMF() {
	showLoadingScreen();
	noOfStmtServiceCalls=0;
	var input_params={};
	invokeServiceSecureAsync("GetServerDateTime", input_params, clBackFullStmtMFShow)
	//frmMFFullStatementMB.show(); 
} 

function clBackFullStmtMFShow(status,result){
	if(status==400){
		if(result["opstatus"]==0){
			gblDate=result["date"];
			getMonthCycleTestMF(result["date"],frmMFFullStatementMB);
			//setSortBtnSkinMBMF();
			dismissLoadingScreen();
			viewMFFullStatementMB(gblViewType);
			//frmMFFullStatementMB.show(); 
		}
		else{
			dismissLoadingScreen();
		}
	}
}

function viewMFFullStatementMB(viewType) {
	//frmMFFullStatementMB.destroy();
 	showLoadingScreen();
 	totalGridData = [];
 	//disableMFPageNumbers();
 	endDate = getTodaysDateStmtMF();
 	startDate = dateFormatChangeMF(new Date(new Date().getFullYear(), new Date().getMonth(), 1));
 	gblViewType = viewType;
 	frmMFFullStatementMB.lblname.text = frmMBMutualFundDetails.lblAccountNameHeader.text;
	frmMFFullStatementMB.lblamounttot.text = frmMBMutualFundDetails.lblAccountBalanceHeader.text;
    frmMFFullStatementMB.imgacnt.src = frmMBMutualFundDetails.imgAccountDetailsPic.src;
    startDateStmt="";
	endDateStmt="";
	selectedIndex=0;
 	frmMFFullStatementMB.show();
} 

function frmMBMFAcctFullStatementPreShow(){
	changeStatusBarColor();
	frmMFFullStatementMB.lblSelectMonth.text = kony.i18n.getLocalizedString("keyMBSelectMonth")+":";
	frmMFFullStatementMB.btnunbilled.text=kony.i18n.getLocalizedString("MF_lbl_Full_statement");
	frmMFFullStatementMB.btnbilled.text=kony.i18n.getLocalizedString("MF_lbl_Order_tobe_process");
	frmMFFullStatementMB.lblHead.text=kony.i18n.getLocalizedString("MF_lbl_Full_statement");
	frmMFFullStatementMB.btnBack.text=kony.i18n.getLocalizedString("Back");
	effectiveDatei18n = kony.i18n.getLocalizedString("MF_thr_TransactionDate");
	unitlbli18n=kony.i18n.getLocalizedString("MF_lbl_Unit");
	investmentlbli18n=kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
	unitBalLbli18n=kony.i18n.getLocalizedString("MF_thr_Unit_balance");
	pricelbli18n=kony.i18n.getLocalizedString("MF_thr_Price");
	channeli18n=kony.i18n.getLocalizedString("MF_lbl_Channel");
	if (kony.i18n.getCurrentLocale() == "en_US"){
		frmMFFullStatementMB.lblname.text = gblSelFundNickNameEN;
	}else{
		frmMFFullStatementMB.lblname.text = gblSelFundNickNameTH;
	}
	//getMonthCycleTestMF()
	if(gblViewType == "F"){
		getMonthCycleTestMF(gblDate,frmMFFullStatementMB);
		currentpageStmt = 1;
		viewMFFullStmt(startDate, endDate, "1");
	}else{
		callOrderToBeProcessServiceMB();
	}
}

function viewMFFullStmt(startDate, endDate,pageNumber) {
	showLoadingScreen();
	var inputParam = {};
	if(startDateStmt!=null && startDateStmt!="")
		inputParam["dateFrom"] = startDateStmt;
	else
		inputParam["dateFrom"] = startDate;
		
	if(endDateStmt!=null && endDateStmt!="")
		inputParam["dateTo"] = endDateStmt;
	else
		inputParam["dateTo"] = endDate;
		
 	
 	inputParam["pageNumber"] = pageNumber;
 	inputParam["fundCode"] = gblFundCode;//"TB1";
 	inputParam["unitHolderNumber"] = removeHyphenIB(gblUnitHolderNumber);//"3060008079";
    //invokeServiceSecureAsync("MFUHFullStmntInquiry", inputParam, startFullStmtMFServiceMBCallBack);
     invokeServiceSecureAsync("getMFFullStamentAllRecords", inputParam, startFullStmtMFServiceMBCallBack);
}

function startFullStmtMFServiceMBCallBack(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			gblStmntSessionFlag=callBackResponse["sesFlag"];
			kony.application.dismissLoadingScreen();
			bindata = callBackResponse["BinData"];
			binlength = callBackResponse["BinLength"];
			var temp = [];
			if(callBackResponse["StatusCode"] == 0){
			if(callBackResponse["listOrderDS"].length == 0){
				frmMFFullStatementMB.cmbMFDates.selectedKey=selectedIndex;
				frmMFFullStatementMB.lblfulpay.setVisibility(false);
				frmMFFullStatementMB.lblNoRecs.setVisibility(true);
				frmMFFullStatementMB.lblNoRecs.text=kony.i18n.getLocalizedString("MF_MSG_No_Record");
				//alert(kony.i18n.getLocalizedString("NoRecsFound"));
				frmMFFullStatementMB.hbxSegHeader.setVisibility(false);	
				frmMFFullStatementMB.hbxaccnt.setVisibility(true);
				frmMFFullStatementMB.segOrdToPrced.removeAll();
				frmMFFullStatementMB.segOrdToPrced.setVisibility(false);
				frmMFFullStatementMB.segcredit.removeAll();
				frmMFFullStatementMB.segcredit.setVisibility(true);
				frmMFFullStatementMB.hbox15049479341746216.setVisibility(true);
				frmMFFullStatementMB.hbxOnHandClick.setVisibility(false);
				frmMFFullStatementMB.lblMonths.text=getLatestMonthMF();
				frmMFFullStatementMB.hbxbilled.setVisibility(false);
				return;
			}
				
			var locale = kony.i18n.getCurrentLocale();
	        	var tranTypeHub = "TranTypeHubEN";
	        	var channelHub = "ChannelHubEN";
				if (locale == "th_TH"){		      
			      	tranTypeHub = "TranTypeHubTH";
	        		channelHub = "ChannelHubTH";
			    }	
			for (var i = 0; i < callBackResponse["listOrderDS"].length; i++) {
				var segData = {
					seqid:i,
					lblDate: callBackResponse["listOrderDS"][i]["OrderDate"],
					lblDescription: callBackResponse["listOrderDS"][i][tranTypeHub],
					lblAmount: commaFormatted(callBackResponse["listOrderDS"][i]["Amount"]),
					ishidden:"yes",
					effectiveDate:callBackResponse["listOrderDS"][i]["EffDate"],
					unit:verifyDisplayUnit(callBackResponse["listOrderDS"][i]["Unit"]),
					investmentVal:callBackResponse["listOrderDS"][i]["InvestValue"],
					unitBal:verifyDisplayUnit(callBackResponse["listOrderDS"][i]["UnitBalance"]),
					price:verifyDisplayUnit(callBackResponse["listOrderDS"][i]["Price"]),
					channelexpval:callBackResponse["listOrderDS"][i][channelHub]
				}
				temp.push(segData);
				}
				
				totalPage = callBackResponse["listOrderDS"]["TotalPage"];
				totalRecord = callBackResponse["listOrderDS"]["TotalRecord"];
				gblMaxRecordPerPage = callBackResponse["transPerPageMB"];
				if(!isNotBlank(gblMaxRecordPerPage)) gblMaxRecordPerPage = 15;
				//loadMBMFFullStmtData(temp);
				frmMFFullStatementMB.segcredit.removeAll();
				frmMFFullStatementMB.segOrdToPrced.removeAll();
				frmMFFullStatementMB.segOrdToPrced.setVisibility(false);
				frmMFFullStatementMB.cmbMFDates.selectedKey=selectedIndex;
				frmMFFullStatementMB.btnunbilled.skin= "btnunbilledfoc";
				frmMFFullStatementMB.btnunbilled.focusSkin = "btnunbilledfoc";
				frmMFFullStatementMB.btnbilled.skin= "btnunbilled";	
				frmMFFullStatementMB.btnbilled.focusSkin= "btnunbilled";
				frmMFFullStatementMB.segcredit.addAll(temp);
				gblsegmentdataMB = frmMFFullStatementMB.segcredit.data;
				frmMFFullStatementMB.hbxSegHeader.setVisibility(true);
				frmMFFullStatementMB.segcredit.setVisibility(true);
				frmMFFullStatementMB.hbxaccnt.setVisibility(true);
				frmMFFullStatementMB.hbxbilled.setVisibility(false);
				frmMFFullStatementMB.lblDateHeader.text= kony.i18n.getLocalizedString("MF_thr_Orderdate");//"Date";
				frmMFFullStatementMB.lblHeaderTransaction.text=kony.i18n.getLocalizedString("MF_thr_Trans");//"Amount";
				frmMFFullStatementMB.lblHeaderBalance.text=kony.i18n.getLocalizedString("MF_thr_Amount") + " (" + kony.i18n.getLocalizedString("currencyThaiBaht")+ ")";
				frmMFFullStatementMB.hbxOnHandClick.setVisibility(true);
				frmMFFullStatementMB.lblfulpay.setVisibility(false);
        		frmMFFullStatementMB.lblNoRecs.setVisibility(false);
				frmMFFullStatementMB.lblMonths.text=getLatestMonthMF();
				

		}else{
		
			alert(callBackResponse["errMsg"]);
			return;
		}
		}
		else {
			kony.application.dismissLoadingScreen();
			if(callBackResponse["errMsg"] != undefined){
			
				alert(" " + callBackResponse["errMsg"]);
			} else {
				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			}
		}
	}
	else {
		if (status == 300) {
			kony.application.dismissLoadingScreen();
			alert("" + kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}




//Order to be process
function callOrderToBeProcessServiceMB(){
	var inputParam = {};
	inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber);//"110053000033";//
	inputParam["funCode"] = gblFundCode;//"70/30-D LTF";
	inputParam["serviceType"] = "2";
	showLoadingScreen();
	//frmIBMFAcctFullStatement.dgOrder.removeAll();
	//frmIBMFAcctFullStatement.hbxNoRcdFound.setVisibility(false);
    invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callOrderToBeProcessServiceCallBackMB);
    //frmIBMFAcctFullStatement.hbxbottomfuture.setVisibility(false);
}

function callOrderToBeProcessServiceCallBackMB(status, resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	if(resulttable["StatusCode"] == 0){
        		if (resulttable["OrderToBeProcessDS"].length == 0) {
        		frmMFFullStatementMB.hbxbilled.setVisibility(true);
        		frmMFFullStatementMB.lblfulpay.setVisibility(true);
        		frmMFFullStatementMB.lblNoRecs.setVisibility(false);
        		frmMFFullStatementMB.lblfulpay.text=kony.i18n.getLocalizedString("MF_MSG_No_Record");
	 			frmMFFullStatementMB.segOrdToPrced.removeAll();
				frmMFFullStatementMB.segOrdToPrced.setVisibility(true);
				frmMFFullStatementMB.segcredit.removeAll();
				frmMFFullStatementMB.segcredit.setVisibility(false);
	 			dismissLoadingScreen();
	 			return;
	 			}
	        	var tempData = [];
	        	var locale = kony.i18n.getCurrentLocale();
	        	var tranTypeHub = "TranTypeHubEN";
	        	var statusHub = "StatusHubEN";
	        	var channelHub = "ChannelHubEN";
				if (locale == "th_TH"){		      
			      	tranTypeHub = "TranTypeHubTH";
	        		statusHub = "StatusHubTH";
	        		channelHub = "ChannelHubTH";
			    }
				for (var i = 0; i < resulttable["OrderToBeProcessDS"].length; i++) {
					var segData = {
					seqid:i,
					lblOrderNo: kony.i18n.getLocalizedString("MF_thr_Order_no")+":",
					lblOrderVal: resulttable["OrderToBeProcessDS"][i]["ItemNo"],
					lblorderDate: kony.i18n.getLocalizedString("MF_thr_Orderdate")+":",
					lblorderDateVal: resulttable["OrderToBeProcessDS"][i]["OrderDate"],
					label15049479341740049:kony.i18n.getLocalizedString("MF_thr_Amount")+":",
					lblAmountVal: commaFormatted(resulttable["OrderToBeProcessDS"][i]["Amount"]),
					//ishidden:"yes",
					lbleffectiveDate:kony.i18n.getLocalizedString("MF_thr_Effectivedate")+":",
					lbleffectiveDateVal:resulttable["OrderToBeProcessDS"][i]["EfftDate"],
					lblUnit:kony.i18n.getLocalizedString("MF_thr_Unit")+":",
					lblunitVal:verifyDisplayUnit(resulttable["OrderToBeProcessDS"][i]["Unit"]),
					lblTransType:kony.i18n.getLocalizedString("MF_thr_Trans")+":",
					lblTransTypeVal:resulttable["OrderToBeProcessDS"][i][tranTypeHub],
					lblStatus:kony.i18n.getLocalizedString("MF_thr_Status")+":",
					lblStatusVal:resulttable["OrderToBeProcessDS"][i][statusHub],
					lblChannel:kony.i18n.getLocalizedString("MF_thr_Channel")+":",
					channelexpval:resulttable["OrderToBeProcessDS"][i][channelHub]
				}
					tempData.push(segData);
				}
				frmMFFullStatementMB.segOrdToPrced.removeAll();
				frmMFFullStatementMB.segOrdToPrced.setVisibility(true);
				frmMFFullStatementMB.segcredit.removeAll();
				frmMFFullStatementMB.segcredit.setVisibility(true);
				frmMFFullStatementMB.segOrdToPrced.addAll(tempData);
				gblsegmentdataMB = frmMFFullStatementMB.segcredit.data;
				frmMFFullStatementMB.btnunbilled.text=kony.i18n.getLocalizedString("MF_lbl_Full_statement");
				frmMFFullStatementMB.btnbilled.text=kony.i18n.getLocalizedString("MF_lbl_Order_tobe_process");
				frmMFFullStatementMB.btnBack.text=kony.i18n.getLocalizedString("Back");
				frmMFFullStatementMB.btnunbilled.skin= "btnunbilled";
				frmMFFullStatementMB.btnunbilled.focusSkin = "btnunbilled";
				frmMFFullStatementMB.btnbilled.skin= "btnunbilledfoc";	
				frmMFFullStatementMB.btnbilled.focusSkin= "btnunbilledfoc";
				frmMFFullStatementMB.hbxSegHeader.setVisibility(false);
				frmMFFullStatementMB.hbxaccnt.setVisibility(false);
				frmMFFullStatementMB.hbxOnHandClick.setVisibility(false);
				frmMFFullStatementMB.hbxbilled.setVisibility(false);
				frmMFFullStatementMB.lblfulpay.setVisibility(false);
        		frmMFFullStatementMB.lblNoRecs.setVisibility(false);
				dismissLoadingScreen();
        	}else{
        		dismissLoadingScreen();
	        	showAlert(kony.i18n.getLocalizedString("keyOrderToBProError"), kony.i18n.getLocalizedString("info"));
				return false;
        	}
        } else {
        	dismissLoadingScreen();
	        showAlert(kony.i18n.getLocalizedString("keyOrderToBProError"), kony.i18n.getLocalizedString("info"));
			return false;
        }
     }else{
        dismissLoadingScreen();
     }
}

function clBackChangeMonth(status, result){
	if(status==400){
		if(result["opstatus"]==0){
			getMonthCycleTestMF(result["date"],frmMFFullStatementMB);
		}
	}
}


/** get date in the required format from button text dynamically
**/

function getDateFormatStmtMF(btntext,btnno){
	 
	 var monthMB;
	
	 var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
 	kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
 	 kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
 	  kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
	
	 var month = btntext.split(" ");
	 for(i=0;i< montharray.length; i++){
	 if(month[0] == montharray[i])
	 	monthMB = i;
	 }
	 var yr = parseInt(month[1]);//+1;
	 
	 var date = new Date();
	 date.setFullYear(yeararray[btnno],monthMB,1);
	 var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	 var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	 startDateStmt = dateFormatChangeMF(firstDay);
	 endDateStmt = dateFormatChangeMF(lastDay);
	  
	
	 
	 if(btnno == 0)
	 {
	 	endDateStmt = dateFormatChangeMF(todaysDate);//getTodaysDateStmt();
	 }
	 
	 nextpageStmt = 1;
	 viewMFFullStmt(startDateStmt,endDateStmt,nextpageStmt);
}

 /**
  *
  * format date in the required format which is returned fron slider
  */

 function dateFormatChangeMF(dateIB) {
 	var d = new Date(dateIB);
 	var curr_date = d.getDate();
 	var curr_month = d.getMonth() + 1; //Months are zero based
 	var curr_year = d.getFullYear();
 	if (curr_month < 10)
 		curr_month = "0" + curr_month;
 	if (curr_date < 10)
 		curr_date = "0" + curr_date;
 	var finalDate =curr_date + "/"+ curr_month + "/" +  curr_year;
 	
 	return finalDate;
 }
 
 
 function getMonthCycleTestMF(date,formname){
	var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
 	kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
 	 kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
 	  kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
	var tempRec = [];
	var resulttableRS = [];
	//todaysDate=date.replace("/", "-");
	todaysDate=date.split("/")[0]+"-"+date.split("/")[1]+"-"+date.split("/")[2];
	
	var mm=date.split("/")[1];
	var fullYear=date.split("/")[0];
	//yy = fullYear.toString();
    yy = fullYear.substring(2, 4);
    
    yeararray[0] =  fullYear;
    
    tempRec = ["0", montharray[mm-1]+" "+yy];
    resulttableRS.push(tempRec);
    
    
    mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4);  		
  	}
  	yeararray[1] =  fullYear;
  	tempRec = ["1", montharray[mm-1]+" "+yy];
	resulttableRS.push(tempRec);
	
   
    mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	 yeararray[2] =  fullYear;
  	tempRec = ["2", montharray[mm-1]+" "+yy];
	resulttableRS.push(tempRec);
	
    
    mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	yeararray[3] =  fullYear;
  	tempRec = ["3", montharray[mm-1]+" "+yy];
	resulttableRS.push(tempRec);
		 
   
    mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
	 yeararray[4] =  fullYear;
  	tempRec = ["4", montharray[mm-1]+" "+yy];
  	 resulttableRS.push(tempRec);

   
    mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	 yeararray[5] =  fullYear;
  	tempRec = ["5", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);
  	
  
	mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	 yeararray[6] =  fullYear;
  	tempRec = ["6", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);
  	
  	
  	mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	yeararray[7] =  fullYear;
  	/*tempRec = ["7", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);
  	
  	 yeararray[8] =  fullYear;
  	mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	tempRec = ["8", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);
  	
  	 yeararray[9] =  fullYear;
  	mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	tempRec = ["9", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);
  	
  	 yeararray[10] =  fullYear;
  	mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	tempRec = ["10", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);
  	
  	 yeararray[11] =  fullYear;
  	mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	tempRec = ["11", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);
  	
  	yeararray[12] =  fullYear;
  	mm = mm - 1;
    if(mm == 0){
  		mm = 12;
  		fullYear = fullYear - 1;
    	yy = fullYear.toString();
    	yy = yy.substring(2, 4); 
  	}
  	tempRec = ["12", montharray[mm-1]+" "+yy];
  	resulttableRS.push(tempRec);*/
    //popMFCmboBox.segordToBePrcd.removeAll();
   // popMFCmboBox.segordToBePrcd.setData(resulttableRS);
   // popMFCmboBox.show();
frmMFFullStatementMB.cmbMFDates.masterData=resulttableRS;
frmMFFullStatementMB.cmbMFDates.selectedKey=0;
}


function onRowSelectMFMonth() {
    //selectedMonth = popMFCmboBox.segordToBePrcd.selectedItems[0].lblMonth;
    //var selectedIndex = popMFCmboBox.segordToBePrcd.selectedIndex[1];
	selectedMonth=frmMFFullStatementMB["cmbMFDates"]["selectedKeyValue"][1];
	selectedIndex=frmMFFullStatementMB["cmbMFDates"]["selectedKeyValue"][0];
   	getDateFormatStmtMF(selectedMonth,selectedIndex);
   //	gblDate=startDate;
   //	frmMFFullStatementMB.lblMonths.text=getLatestMonthMF();
	onRowSelect="Y";
    //popMFCmboBox.dismiss();
}

function getLatestMonthMF()
{
	var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
 	kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
      kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
 	  kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
 	  var selectedDate=gblDate;
 	  
 	  if(onRowSelect=="Y")
 	  {
 	  	selectedDate=startDateStmt;
 	  	onRowSelec="";
 	  }
 	 // gblDate=startDateStmt;
	var mm=selectedDate.split("/")[1];
	//alert("mm:"+mm);
	var fullYear=selectedDate.split("/")[2];
    yy = fullYear.substring(2, 4);
    gblCurrentMonth = montharray[mm-1]+" "+yy;
    return gblCurrentMonth;
}

function generateTaxDocPdfMB(){
		
	var inputParam = {};
	inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber);
	inputParam["funCode"] = gblFundCode;
	showLoadingScreenPopup();
    invokeServiceSecureAsync("MFTaxDocumentView", inputParam, mbRenderFileCallbackfunction);
}

function savePDFMutualFundFullStatementServiceMB(fileType, templateName) {
    var inputParam = {};     
    var totalData = {
             "localeId": kony.i18n.getCurrentLocale()         
     };

    if (gblDeviceInfo.name == "thinclient"){
            inputParam["channelId"] = GLOBAL_IB_CHANNEL;
    } else{
        inputParam["channelId"] = GLOBAL_MB_CHANNEL;
            }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = templateName;
    inputParam["unitHolderNo"] = gblUnitHolderNumber;
    inputParam["fundName"] = frmMFFullStatementMB.lblname.text;
    inputParam["investmentValue"] = frmMFFullStatementMB.lblamounttot.text;
    inputParam["datajson"] = JSON.stringify(totalData, "");
       
    var url = "";
    invokeServiceSecureAsync("generatePdfImageForMFFullStmts", inputParam, mbRenderFileCallbackfunction);
}

/**
 * on CCMonth1 Click
 */
function onCCMonth1ClickMF() {
	setSortBtnSkinMBMF();
	/*clearCCValuesMF();
	frmMFFullStatementMB.btn1.skin = "btnTabLeftBlue";
	frmMFFullStatementMB.btn2.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn3.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn4.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn5.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn6.skin = "btnTab3RightNrml";*/
	callOrderToBeProcessServiceMB();
}





