confirmRequestPinFlag = 1;

function onClickOfReqNewPinConfirmManageCard() {
    frmMBManageCard.show();
}

function onClickOfRequestNewPinMenu() {
    var inputParam = {};
    kony.print("inside onClickOfRequestNewPinMenu gblCACardRefNumber---->" + gblCACardRefNumber)
    inputParam["cardRefId"] = gblCACardRefNumber; // TO-DO, Check once this param.
    showLoadingScreen();
    invokeServiceSecureAsync("creditCardStatusInq", inputParam, callbackCreditCardAddr);
}

function callbackCreditCardAddr(status, resulttable) {
    kony.print("inside callbackCreditCardAddr------>" + JSON.stringify(resulttable));
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            if (resulttable["MailAddr"] != null && resulttable["MailAddr"] != "" && resulttable["MailAddr"] != undefined) {
                var cardAdd = resulttable["MailAddr"][0];
                frmMBRequestNewPin.rchAddress.text = cardAdd["Addr1"] + " " + cardAdd["Addr2"] + " " + cardAdd["City"] + " " + cardAdd["PostalCode"];
                frmMBRequestNewPin.lblCardAccountName.text = resulttable["CustNameEN"];
                frmMBRequestNewPin.lblCardNumber.skin = getCardNoSkin();
                frmMBRequestNewPin.lblCardNumber.text = resulttable["CardID"];
                frmMBRequestNewPin.show();
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function onClickBackOfRequestNewPinConfirmScreen() {
    frmMBManageCard.show();
    frmMBRequestNewPin.destroy();
}

function onClickConfirmOfRequestNewPinConfirmScreen() {
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    var refNo = "";
    var mobNO = "";
    showLoadingScreen();
    confirmRequestPinFlag = 1;
    gblReqPinFail = 0;
    showOTPPopup(lblText, refNo, mobNO, onClickRequestPINConfirmTxnPopup, 3);
}

function onClickBackOfManageCardScreen() {
    preShowMBCardList();
}

function onClickChangeAddrOfRequestNewPinConfirmScreen() {
    showLoadingScreen();
    frmMBBlockCCChangeAddress.show();
    dismissLoadingScreen();
}

function onClickChangeAddrOfFailure() {
    frmMBRequestPinFailure.show();
}

function onClickCloseOfRequestPinContactScreen() {
    frmMBRequestNewPin.flxBody.scrollToWidget(frmMBRequestNewPin.lblRequestPinTitle);
    frmMBRequestNewPin.show();
}

function onClickManageOfRequestPinSuccessScreen() {
    gblisCardActivationCompleteFlow = true;
    onClickBackManageListCard();
}

function onClickRequestPINConfirmTxnPopup(tranPassword) {
    if (popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text == "") {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    } else {
        showLoadingScreen();
        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
        popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
        onClickConfirmVerifyRequestPINMB(tranPassword);
    }
}

function onClickConfirmVerifyRequestPINMB(tranPassword) {
    var inputParam = {};
    inputParam["password"] = tranPassword;
    inputParam["cardRefId"] = gblCACardRefNumber;
    inputParam["confirmRequestPinFlag"] = confirmRequestPinFlag;
    inputParam["tryAgainFlag"] = "false";
    inputParam["tryAgainNumber"] = gblReqPinFail;
    invokeServiceSecureAsync("requestNewPinCompositeService", inputParam, callBackConfirmRequestPINService);
}

function callBackConfirmRequestPINService(status, resulttable) {
    if (status == 400) {
        confirmRequestPinFlag = 0;
        if (resulttable["opstatus"] == 0) {
            popupTractPwd.dismiss();
            dismissLoadingScreen();
            gotoRequestNewPinSuccess();
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
        } else {
            dismissLoadingScreen();
            popupTractPwd.dismiss();
            //var errorMsgTop = resulttable["errMsg"];
            //if(!isNotBlank(resulttable["errMsg"])){
            //	errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
            //	}
            // showAlertWithCallBack(errorMsgTop, kony.i18n.getLocalizedString("info"), gotoRequestPinFailScreen);
            gotoRequestPinFailScreen();
        }
    }
}

function gotoRequestNewPinSuccess() {
    frmMBRequestPinSuccess.rchAddress.text = frmMBRequestNewPin.rchAddress.text;
    frmMBRequestPinSuccess.lblProductName.text = frmMBRequestNewPin.lblCardAccountName.text;
    frmMBRequestPinSuccess.show();
}

function gotoRequestPinFailScreen() {
    frmMBRequestPinFailure.show();
    if (!frmMBRequestPinFailure.flxtryagain.isVisible) {
        frmMBRequestPinFailure.flxCallcenternum.top = "6%";
        frmMBRequestPinFailure.flxtryagain.setVisibility(true);
    }
}

function onClickTryAgainRequestNewPin() {
    showLoadingScreen();
    gblReqPinFail += 1;
    var inputParam = {};
    inputParam["tryAgainFlag"] = "true";
    inputParam["cardRefId"] = gblCACardRefNumber;
    inputParam["tryAgainNumber"] = gblReqPinFail;
    invokeServiceSecureAsync("requestNewPinCompositeService", inputParam, callBackTryAgainRequestNewPinService);
}

function callBackTryAgainRequestNewPinService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            gotoRequestNewPinSuccess();
        } else {
            if (resulttable["requestPinTryAgain"] != null && resulttable["requestPinTryAgain"] != "" && resulttable["requestPinTryAgain"] != undefined && resulttable["requestPinTryAgain"] == "true") {
                if (!frmMBRequestPinFailure.flxtryagain.isVisible) {
                    frmMBRequestPinFailure.flxCallcenternum.top = "6%";
                    frmMBRequestPinFailure.flxtryagain.setVisibility(true);
                }
            } else {
                if (frmMBRequestPinFailure.flxtryagain.isVisible) {
                    frmMBRequestPinFailure.flxCallcenternum.top = "20%";
                    frmMBRequestPinFailure.flxtryagain.setVisibility(false);
                }
            }
            dismissLoadingScreen();
        }
    }
}