gblCASelectedToAcctNum = "";
gblCASelectedToAcctNameEN = "";
gblCASelectedToAcctNameTH = "";
gblCACardRefNumber = "";
gblMinCashAdvanceAmount = "";
gblHideDisplayAmtLabelValue = true;
gblAvailableBalance = "";
gblCASelectedToAcctStatus = "";

function onClickOfCashAdvanceMenu() {
    cashAdvanceServiceHoursCheck();
    //frmMBCashAdvanceCardInfo.show();
}

function onClickNextOfCardInfoScreen() {
    showLoadingScreen();
    var RemDailyLimit = removeCommos(frmMBCashAdvanceCardInfo.lblRemDailyLimitVal.text);
    var AvailToSpend = removeCommos(frmMBCashAdvanceCardInfo.lblAvailSpendVal.text);
    var AvailCashAdv = parseFloat(RemDailyLimit) < parseFloat(AvailToSpend) ? RemDailyLimit : AvailToSpend;
    var errorMsg = "";
    if (parseFloat(AvailCashAdv) < parseFloat(gblMinCashAdvanceAmount)) {
        dismissLoadingScreen();
        errorMsg = kony.i18n.getLocalizedString("CashAdv_Err_Amount");
        errorMsg = errorMsg.replace("{min_amount}", commaFormatted(gblMinCashAdvanceAmount));
        showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
        return false;
    } else {
        dismissLoadingScreen();
        frmMBCashAdvanceTnC.show();
    }
}

function onClickBackOfCardInfoScreen() {
    frmMBManageCard.show();
}

function onClickCancelOfAccountSelectScreen() {
    frmCashAdvancePlanDetails.show();
    frmMBCashAdvAcctSelect.destroy();
}

function onRowClickSegmentOfAccountSelectScreen() {
    var accountName = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountName;
    gblCASelectedToAcctNum = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountNum;
    gblCASelectedToAcctNameEN = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountNameEN;
    gblCASelectedToAcctNameTH = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountNameTH;
    gblCASelectedToAcctStatus = frmMBCashAdvAcctSelect.segAccounts.selectedItems[0].lblAccountStatus;
    frmCashAdvancePlanDetails.lblSelectedAccount.text = accountName;
    frmCashAdvancePlanDetails.show();
}

function onSelectAccountCashAdvDetailsScreen() {
    customerAccountInquiryForSelectAccounts();
}

function onClickNextOfCashAdvTncScreen() {
    gblCASelectedToAcctNum = "";
    gblCASelectedToAcctNameEN = "";
    gblCASelectedToAcctNameTH = "";
    frmCashAdvancePlanDetails.flxEnterFullAmountDtls.setVisibility(false);
    frmCashAdvancePlanDetails.lblReceivedDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
    frmCashAdvancePlanDetails.lblDebitCardDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
    frmCashAdvancePlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
    var availableBal = frmMBCashAdvanceCardInfo.lblAvailCashAdvVal.text;
    gblAvailableBalance = availableBal;
    frmCashAdvancePlanDetails.lblAvailableCashAmt.text = availableBal;
    frmCashAdvancePlanDetails.lblAvailableCashDesc.text = kony.i18n.getLocalizedString("CAV02_keyAvailCA") + " " + availableBal;
    frmCashAdvancePlanDetails.lblEnterAmount.text = commaFormatted(gblMinCashAdvanceAmount);
    frmCashAdvancePlanDetails.txtEnterAmount.text = commaFormatted(gblMinCashAdvanceAmount);
    enableOrDisableAddAmount(parseFloat(gblMinCashAdvanceAmount), parseFloat(removeCommos(availableBal)));
    enableOrDisableMinusAmount(parseFloat(gblMinCashAdvanceAmount)); //12345
    frmCashAdvancePlanDetails.flxAmount.setVisibility(true);
    frmCashAdvancePlanDetails.flxEnterFullAmountDtls.setVisibility(false);
    cashAdvanceCreditCardDetailsInqGetInterest();
    //frmCashAdvancePlanDetails.show();
}

function onClickBackOfCashAdvTncScreen() {
    frmMBCashAdvanceCardInfo.show();
    frmMBCashAdvanceTnC.destroy();
}

function onClickNextOfCashAdvPlanDetailScreen() {
    if (!onClickNextValidatePlanAmount()) {
        return false;
    } else {
        callCashAdvanceSaveToSessionService();
    }
}

function onClickBackOfCashAdvPlanDetailScreen() {
    gblCASelectedToAcctNum = "";
    frmMBCashAdvanceTnC.show();
    frmCashAdvancePlanDetails.destroy();
}

function onClickConfirmOfCashAdvConfirmScreen() {
    showCashAdvancePwdPopupForTransfers();
}

function onClickCancelOfCashAdvConfirmScreen() {
    onClickNextOfCashAdvTncScreen();
    frmCashAdvancePlanDetails.destroy();
}

function onClickBackOfCashAdvConfirmScreen() {
    frmCashAdvancePlanDetails.flexBodyScroll.scrollToWidget(frmCashAdvancePlanDetails.flxAmount);
    frmCashAdvancePlanDetails.show();
}

function showCashAdvancePwdPopupForTransfers() {
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    var refNo = "";
    var mobNO = "";
    showLoadingScreen();
    showOTPPopup(lblText, refNo, mobNO, onClickCashAdvaceConfirmPop, 3);
}

function onClickCashAdvaceConfirmPop(tranPassword) {
    if (isNotBlank(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text)) {
        showLoadingScreen();
        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
        popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
        callCashAdvanceCompositeService(tranPassword);
    } else {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    }
}

function gotoCAPaymentPlanCompleteMB(status) {
    if (status == "0") { //success 
        frmCAPaymentPlanComplete.imgIconComplete.src = "completeico.png";
        frmCAPaymentPlanComplete.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle") + " " + frmCAPaymentPlanComplete.lblAmountVal.text;
        frmCAPaymentPlanComplete.lblAmountVal.text = frmCAPaymentPlanConfirmation.lblAmountVal.text;
        frmCAPaymentPlanComplete.lblFeeVal.text = frmCAPaymentPlanConfirmation.lblFeeVal.text;
    } else { //failed
        frmCAPaymentPlanComplete.imgIconComplete.src = "icon_notcomplete.png";
        frmCAPaymentPlanComplete.lblAmountVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmCAPaymentPlanComplete.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle") + " 0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmCAPaymentPlanComplete.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    frmCAPaymentPlanComplete.lblFromAcctName.text = frmCAPaymentPlanConfirmation.lblFromAcctName.text;
    frmCAPaymentPlanComplete.lblFromAcctNo.text = frmCAPaymentPlanConfirmation.lblFromAcctNo.text;
    frmCAPaymentPlanComplete.lblToAcctName.text = frmCAPaymentPlanConfirmation.lblToAcctName.text;
    frmCAPaymentPlanComplete.lblToAcctNo.text = frmCAPaymentPlanConfirmation.lblToAcctNo.text;
    frmCAPaymentPlanComplete.lblReferenceVal.text = frmCAPaymentPlanConfirmation.lblReferenceVal.text;
    frmCAPaymentPlanComplete.lblAnnualRateVal.text = frmCAPaymentPlanConfirmation.lblAnnualRateVal.text;
    frmCAPaymentPlanComplete.lblDebitCardOnVal.text = frmCAPaymentPlanConfirmation.lblDebitCardOnVal.text;
    frmCAPaymentPlanComplete.lblRecieveDateVal.text = frmCAPaymentPlanConfirmation.lblRecieveDateVal.text;
    frmCAPaymentPlanComplete.lblManageOtherCards.text = kony.i18n.getLocalizedString("CAV08_btnManageOther");
    frmCAPaymentPlanComplete.show();
}
// Added by vijay
function onClickAmount() {
    frmCashAdvancePlanDetails.flxAmount.setVisibility(false);
    frmCashAdvancePlanDetails.txtEnterAmount.setFocus(true);
    frmCashAdvancePlanDetails.flxEnterFullAmountDtls.setVisibility(true);
    frmCashAdvancePlanDetails.txtEnterAmount.text = frmCashAdvancePlanDetails.lblEnterAmount.text;
    frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtBlueNormal200";
    frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtBlueNormal200";
    //frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = lblGrey36px;
    veriflyEnterAmountValueCA();
}

function veriflyEnterAmountValueCA() {
    var amountVal = frmCashAdvancePlanDetails.txtEnterAmount.text;
    var availableBal = frmCashAdvancePlanDetails.lblAvailableCashAmt.text;
    var minAmountErrKey = kony.i18n.getLocalizedString("CAV04_msgMinErr");
    minAmountErrKey = minAmountErrKey.replace("{Minimum_Amount}", commaFormatted(gblMinCashAdvanceAmount));
    if (parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(availableBal))) {
        frmCashAdvancePlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_msgMaxErr");
        frmCashAdvancePlanDetails.flxEnterAmt.skin = "flexWhiteBGRedBorder";
        frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtRed48px";
        frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
        frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = "lblRedInlineErr";
    } else if (parseFloat(removeCommos(amountVal)) < parseFloat(removeCommos(gblMinCashAdvanceAmount))) {
        frmCashAdvancePlanDetails.lblEnterAmountDesc.text = minAmountErrKey;
        frmCashAdvancePlanDetails.flxEnterAmt.skin = "flexWhiteBGRedBorder";
        frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtRed48px";
        frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
        frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = "lblRedInlineErr";
    } else if (!isNotBlank(amountVal)) {
        frmCashAdvancePlanDetails.lblEnterAmountDesc.text = minAmountErrKey;
        frmCashAdvancePlanDetails.flxEnterAmt.skin = "flexWhiteBGRedBorder";
        frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtRed48px";
        frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
        frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = "lblRedInlineErr";
    } else {
        amountVal = kony.string.replace(amountVal, ",", "");
        frmCashAdvancePlanDetails.txtEnterAmount.text = commaFormattedTransfer(amountVal);
        frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtBlueNormal200";
        frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtBlueNormal200";
        frmCashAdvancePlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");
        frmCashAdvancePlanDetails.flxEnterAmt.skin = "flexWhiteBGBlueBorder";
        frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = "lblGrey36px";
    }
}

function onDoneEnterAmount() {
    onValidateChangePlanAmount();
    if (!gblHideDisplayAmtLabelValue) {
        gblHideDisplayAmtLabelValue = true;
        frmCashAdvancePlanDetails.flxEnterFullAmountDtls.setVisibility(true);
        frmCashAdvancePlanDetails.flxAmount.setVisibility(false);
    } else {
        frmCashAdvancePlanDetails.lblEnterAmount.text = commaFormatted(parseFloat(removeCommos(frmCashAdvancePlanDetails.txtEnterAmount.text)).toFixed(2));
        frmCashAdvancePlanDetails.flxEnterFullAmountDtls.setVisibility(false);
        frmCashAdvancePlanDetails.flxAmount.setVisibility(true);
        if (!isNotBlank(frmCashAdvancePlanDetails.lblEnterAmount.text)) {
            frmCashAdvancePlanDetails.btnMinusAmount.setEnabled(false);
        } else {
            enableOrDisableAddAmount(parseFloat(removeCommos(frmCashAdvancePlanDetails.lblEnterAmount.text)), parseFloat(removeCommos(frmCashAdvancePlanDetails.lblAvailableCashAmt.text)));
            enableOrDisableMinusAmount(parseFloat(removeCommos(frmCashAdvancePlanDetails.lblEnterAmount.text)))
        }
        cashAdvanceCreditCardDetailsInqGetInterest();
    }
}
// Added by Vijay for Plan details Add/Minus Amount
function addPlanAmount() {
    var amount = parseFloat(removeCommos(frmCashAdvancePlanDetails.lblEnterAmount.text));
    amount = amount + parseFloat(gblMinCashAdvanceAmount);
    var availableBal = parseFloat(removeCommos(gblAvailableBalance));
    enableOrDisableAddAmount(amount, availableBal);
    frmCashAdvancePlanDetails.lblEnterAmount.text = commaFormatted(parseFloat(removeCommos(amount)).toFixed(2));
    enableOrDisableMinusAmount(amount);
    cashAdvanceCreditCardDetailsInqGetInterest();
}

function minusPlanAmount() {
    var amount = parseFloat(removeCommos(frmCashAdvancePlanDetails.lblEnterAmount.text));
    var minusAmt = parseFloat(gblMinCashAdvanceAmount);
    amount = amount - minusAmt;
    enableOrDisableMinusAmount(amount);
    frmCashAdvancePlanDetails.lblEnterAmount.text = commaFormatted(parseFloat(removeCommos(amount)).toFixed(2));
    var availableBal = parseFloat(removeCommos(gblAvailableBalance));
    enableOrDisableAddAmount(amount, availableBal);
    cashAdvanceCreditCardDetailsInqGetInterest();
}

function onValidateChangePlanAmount() {
    var minAmountErrKey = kony.i18n.getLocalizedString("CAV04_msgMinErr");
    minAmountErrKey = minAmountErrKey.replace("{Minimum_Amount}", commaFormatted(gblMinCashAdvanceAmount));
    var amountVal = frmCashAdvancePlanDetails.txtEnterAmount.text;
    if (!isNotBlank(amountVal)) {
        frmCashAdvancePlanDetails.txtEnterAmount.text = amountVal;
        frmCashAdvancePlanDetails.lblEnterAmountDesc.text = minAmountErrKey;
        frmCashAdvancePlanDetails.flxEnterAmt.skin = flexWhiteBGRedBorder;
        frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtRed48px";
        frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
        frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = lblRedInlineErr;
        gblHideDisplayAmtLabelValue = false;
    } else {
        amountVal = commaFormatted(fixedToTwoDecimal(amountVal));
        var availableBal = frmCashAdvancePlanDetails.lblAvailableCashAmt.text;
        //frmCashAdvancePlanDetails.txtEnterAmount.text = commaFormattedTransfer(amountVal);
        if (parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(availableBal))) {
            frmCashAdvancePlanDetails.txtEnterAmount.text = amountVal;
            frmCashAdvancePlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_msgMaxErr");
            frmCashAdvancePlanDetails.flxEnterAmt.skin = flexWhiteBGRedBorder;
            frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtRed48px";
            frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
            frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = lblRedInlineErr;
            gblHideDisplayAmtLabelValue = false;
        } else if (parseFloat(removeCommos(amountVal)) < parseFloat(removeCommos(gblMinCashAdvanceAmount))) {
            frmCashAdvancePlanDetails.txtEnterAmount.text = amountVal;
            frmCashAdvancePlanDetails.lblEnterAmountDesc.text = minAmountErrKey;
            frmCashAdvancePlanDetails.flxEnterAmt.skin = flexWhiteBGRedBorder;
            frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtRed48px";
            frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
            frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = lblRedInlineErr;
            gblHideDisplayAmtLabelValue = false;
        } else if (!isNotBlank(amountVal)) {
            frmCashAdvancePlanDetails.txtEnterAmount.text = amountVal;
            frmCashAdvancePlanDetails.lblEnterAmountDesc.text = minAmountErrKey;
            frmCashAdvancePlanDetails.flxEnterAmt.skin = flexWhiteBGRedBorder;
            frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtRed48px";
            frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtRed48px";
            frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = lblRedInlineErr;
            gblHideDisplayAmtLabelValue = false;
        } else {
            frmCashAdvancePlanDetails.txtEnterAmount.text = amountVal;
            frmCashAdvancePlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");
            frmCashAdvancePlanDetails.flxEnterAmt.skin = flexWhiteBGBlueBorder;
            frmCashAdvancePlanDetails.txtEnterAmount.skin = "txtBlueNormal200";
            frmCashAdvancePlanDetails.txtEnterAmount.focusSkin = "txtBlueNormal200";
            frmCashAdvancePlanDetails.lblEnterAmountDesc.skin = lblGrey36px;
            gblHideDisplayAmtLabelValue = true;
        }
    }
    frmCashAdvancePlanDetails.txtEnterAmount.text = isNotBlank(amountVal) ? amountVal : "";
    frmCashAdvancePlanDetails.lblEnterAmount.text = isNotBlank(amountVal) ? amountVal : "";
}

function onTextChangePlanAmount() {
    var amountVal = frmCashAdvancePlanDetails.txtEnterAmount.text;
    if (isNotBlank(amountVal)) {
        amountVal = kony.string.replace(amountVal, ",", "");
        if (isNotBlank(amountVal) && amountVal.length > 0 && parseFloat(amountVal, 10) == 0) {} else {
            frmCashAdvancePlanDetails.txtEnterAmount.text = commaFormattedTransfer(amountVal);
        }
    }
}

function onClickNextValidatePlanAmount() {
    var minAmountErrKey = kony.i18n.getLocalizedString("CAV04_msgMinErr");
    minAmountErrKey = minAmountErrKey.replace("{Minimum_Amount}", commaFormatted(gblMinCashAdvanceAmount));
    var amountVal = frmCashAdvancePlanDetails.txtEnterAmount.text;
    var availableBal = frmCashAdvancePlanDetails.lblAvailableCashAmt.text;
    var selAccounttxt = frmCashAdvancePlanDetails.lblSelectedAccount.text;
    var activeAcct = "Active"; //Check Active Account
    var inActiveAcct = "Inactive"; //Check Active Account
    if (parseFloat(removeCommos(amountVal)) > parseFloat(removeCommos(availableBal))) {
        showAlert(kony.i18n.getLocalizedString("CAV04_msgMaxErr"), kony.i18n.getLocalizedString("info"));
        return false;
    } else if (parseFloat(removeCommos(amountVal)) < parseFloat(removeCommos(gblMinCashAdvanceAmount))) {
        showAlert(minAmountErrKey, kony.i18n.getLocalizedString("info"));
        return false;
    } else if (!isNotBlank(amountVal)) {
        showAlert(minAmountErrKey, kony.i18n.getLocalizedString("info"));
        return false;
    } else if (kony.string.equalsIgnoreCase(selAccounttxt, kony.i18n.getLocalizedString("CAV04_plToAct"))) {
        showAlert(kony.i18n.getLocalizedString("CAV04_msgToAccount"), kony.i18n.getLocalizedString("info"));
        return false;
    } else if (!kony.string.startsWith(gblCASelectedToAcctStatus, activeAcct, true) && !kony.string.startsWith(gblCASelectedToAcctStatus, inActiveAcct, true)) {
        showAlert(kony.i18n.getLocalizedString("CAV04_msgInvalidStatus"), kony.i18n.getLocalizedString("info"));
        return false;
    } else {
        return true;
    }
}

function calculateCashAdvanceFee(amount, rate) {
    var fee = "0.00";
    if (isNotBlank(amount)) {
        amount = removeCommos(amount);
        rate = parseFloat(rate);
        fee = (amount / 100) * rate;
    }
    fee = parseFloat(fee).toFixed(2);
    return fee;
}

function loadTermsNConditionCashAdvanceMB() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    //module key again is from tmb_tnc file for Cash Advance
    input_param["moduleKey"] = "TMBCashAdvance";
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, loadTNCCahAdvanceCallBackMB);
}

function loadTNCCahAdvanceCallBackMB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmMBCashAdvanceTnC.lblCashAdvanceDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
            frmMBCashAdvanceTnC.lblCashAdvanceHdrTxt.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
            frmMBCashAdvanceTnC.btnCashAdvanceBack.text = kony.i18n.getLocalizedString("CAV02_btnBack");
            frmMBCashAdvanceTnC.btnCashAdvanceAgree.text = kony.i18n.getLocalizedString("CAV03_btnAgree");
            frmMBCashAdvanceTnC.richtextTnC.setVisibility(true);
            frmMBCashAdvanceTnC.richtextTnC.text = result["fileContent"];
            frmMBCashAdvanceTnC.lblCashAdvanceDescSubTitle.setFocus(true);
            frmMBCashAdvanceTnC.flexBodyScroll.scrollToWidget(frmMBCashAdvanceTnC.flexContainer761510865415876);
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function onClickEmailTnCMBCashAdvance() {
    showLoadingScreen()
    var inputparam = {};
    inputparam["channelName"] = "Mobile Banking";
    inputparam["channelID"] = "02";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "TMBCashAdvance";
    invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCCashAdvanceMB);
}

function callBackEmailTnCCashAdvanceMB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
            } else {
                dismissLoadingScreen()
                return false;
            }
        } else {
            dismissLoadingScreen()
        }
    }
}

function enableOrDisableAddAmount(enteredAmt, availableBal) {
    var checkAmount = enteredAmt + parseFloat(gblMinCashAdvanceAmount); // Should not allow next time	
    //kony.print("checkAmount--->" + checkAmount + "availableBal---->" + availableBal);
    if (checkAmount > availableBal) {
        //kony.print("Enable false in add amount");
        frmCashAdvancePlanDetails.btnAddAmount.setEnabled(false);
        frmCashAdvancePlanDetails.btnAddAmount.skin = "btnDisabledAddAmount";
        frmCashAdvancePlanDetails.btnAddAmount.focusSkin = "btnDisabledAddAmount";
    } else {
        //kony.print("Enable true in add amount");
        frmCashAdvancePlanDetails.btnAddAmount.setEnabled(true);
        frmCashAdvancePlanDetails.btnAddAmount.skin = "btnTransWBGBlueFont";
        frmCashAdvancePlanDetails.btnAddAmount.focusSkin = "btnTransGreyBGBlueFont";
    }
}

function enableOrDisableMinusAmount(enteredAmt) {
    var minusAmt = parseFloat(gblMinCashAdvanceAmount);
    //kony.print("minusAmt--->" + minusAmt + "enteredAmt---->" + enteredAmt);
    var checkAmount = enteredAmt - minusAmt; // Should not allow next time	
    if (checkAmount < minusAmt) {
        //kony.print("Enable false in add minus amount");
        frmCashAdvancePlanDetails.btnMinusAmount.setEnabled(false);
        frmCashAdvancePlanDetails.btnMinusAmount.skin = "btnDisabledAddAmount";
        frmCashAdvancePlanDetails.btnMinusAmount.focusSkin = "btnDisabledAddAmount";
    } else {
        //kony.print("Enable true in add minus amount");
        frmCashAdvancePlanDetails.btnMinusAmount.setEnabled(true);
        frmCashAdvancePlanDetails.btnMinusAmount.skin = "btnTransWBGBlueFont";
        frmCashAdvancePlanDetails.btnMinusAmount.focusSkin = "btnTransGreyBGBlueFont";
    }
}

function onClickManageOtherCardCompleteCA() {
    gblisCardActivationCompleteFlow = true;
    gblManageCardFlow = "frmMBCardList";
    onClickBackManageListCard();
}