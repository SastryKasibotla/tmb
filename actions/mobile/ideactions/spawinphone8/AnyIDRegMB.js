gblMBOtherBankLinked = false;
gblCIOtherBankLinked = false;
gblMBTMBLinked = false;
gblCITMBLinked = false;
userCIChanged = false;
userMBChanged = false;
agreeButton = false;
gblServiceMBAct = "";
gblServiceCIAct = "";
fromTnC = false;
noEligibleActs = false;
singleAct = false;
rawDataTMB = [];
anyIDProdFeature_image_EN = "";
anyIDProdFeature_image_TH = "";
gblCIanyIDActName = "";
gblCIanyIDActNameTH = "";
gblanyIDActName = "";
gblanyIDActNameTH = "";
gblAnyIDimageName = "";
gblanyIDActID = "";
gblCIAndroidSrvVal = false;
gblCIiPhoneSrvVal = false;
gblMBAndroidSrvVal = "";
gblMBiPhoneSrvVal = "";
gblselectedChckBox = "";
isMBRegister = false;
isCIRegister = false;
isMBDeregister = false;
isCIDeregister = false;
gblAccountsAnyID = [];
gblMBActsForLocaleChange = [];
gblCIActsForLocaleChange = [];
gblCIAccountsEN = "";
gblCIAccountsTH = "";
gblLinkOtherBankLangStr = [];
gblMBLinkOtherBankLangStr = [];
fromBack = false;
gblLocalfromDeregister = false;
gblAnyIDAccountTable = [];

function frmMBActivateAnyIdPreShow() {
    //frmMBActivateAnyId.hboxPhoneNums.setVisibility(false);
    //frmMBActivateAnyId.hboxSocialNums.setVisibility(false);
    frmMBActivateAnyId.lblPhoneNum.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
    if (gblCustomerIDType == "CI") {
        frmMBActivateAnyId.lblCI.text = maskCitizenID(gblCustomerIDValue);
        unCIHboxDetails();
    } else hideCIHboxDetails();
}

function hideCIHboxDetails() {
    frmMBActivateAnyId.hbox508341360349300.setVisibility(false);
    frmMBActivateAnyId.hbox508341360540829.setVisibility(false);
    frmMBActivateAnyId.hboxTnC.setVisibility(false);
    frmMBActivateAnyId.line508341360728137.setVisibility(false);
}

function unCIHboxDetails() {
    frmMBActivateAnyId.hbox508341360349300.setVisibility(true);
    frmMBActivateAnyId.hbox508341360540829.setVisibility(true);
    frmMBActivateAnyId.hboxTnC.setVisibility(true);
    frmMBActivateAnyId.line508341360728137.setVisibility(true);
}

function frmMBActivateAnyIdPreShowLocale() {
    changeStatusBarColor();
    //setAgreeBtnText();
    frmMBActivateAnyId.btnback.text = kony.i18n.getLocalizedString("Back");
    if (agreeButton) frmMBActivateAnyId.btnNext.text = kony.i18n.getLocalizedString("keyAgreeButton");
    else frmMBActivateAnyId.btnNext.text = kony.i18n.getLocalizedString("Next");
    frmMBActivateAnyId.lblHdrTxt.text = kony.i18n.getLocalizedString("MIB_P2PPrdBri");
    frmMBActivateAnyId.label508341360345707.text = kony.i18n.getLocalizedString("MIB_P2PDesc");
    frmMBActivateAnyId.label508341360459536.text = kony.i18n.getLocalizedString("MIB_P2PRegisMob");
    frmMBActivateAnyId.label508341360460106.text = kony.i18n.getLocalizedString("MIB_P2PRegisCI");
    if (gblMBOtherBankLinked) {
        var mbDeRegTxt = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankMobileNo");
        var res = mbDeRegTxt.replace("<Bank short name", gblMBLinkOtherBankLangStr.bankShortName);
        if (kony.i18n.getCurrentLocale() == "th_TH") var res = res.replace("Bank name>", gblMBLinkOtherBankLangStr.bankNameTH);
        else var res = res.replace("Bank name>", gblMBLinkOtherBankLangStr.bankNameEN);
        frmMBActivateAnyId.lblTxtPhone.text = res;
        frmMBActivateAnyId.lblTxtPhone.skin = lblRedNormal;
        frmMBActivateAnyId.lblDeRegister.text = kony.i18n.getLocalizedString("MIB_P2PHowtoMobileNo");
    } else if (gblMBTMBLinked || !gblMBOtherBankLinked) {
        frmMBActivateAnyId.lblTxtPhone.text = kony.i18n.getLocalizedString("MIB_P2PRegisMobInfo");
        //frmMBActivateAnyId.lblTxtPhone.skin=lblBlue80;
        frmMBActivateAnyId.lblTxtPhone.skin = lblBlueDBOzone128;
        frmMBActivateAnyId.lblDeRegister.text = "";
        if (gblMBActsForLocaleChange != null) {
            if (kony.i18n.getCurrentLocale() == "th_TH") frmMBActivateAnyId.lblname.text = gblMBActsForLocaleChange["lblAccountNameTH"];
            else frmMBActivateAnyId.lblname.text = gblMBActsForLocaleChange["lblAccountNameEN"];
        }
    }
    //kony.i18n.getLocalizedString("MIB_P2PRegisCIInfo");MIB_P2PLinkOtherBankCitizenID
    if (gblCIOtherBankLinked) {
        frmMBActivateAnyId.label1504947934171198.text = kony.i18n.getLocalizedString("MIB_P2PHowtoCitizenID");
        var CIDeRegTxt = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankCitizenID");
        var res = CIDeRegTxt.replace("<Bank short name", gblLinkOtherBankLangStr.bankShortName);
        if (kony.i18n.getCurrentLocale() == "th_TH") var res = res.replace("Bank name>", gblLinkOtherBankLangStr.bankNameTH);
        else var res = res.replace("Bank name>", gblLinkOtherBankLangStr.bankNameEN);
        frmMBActivateAnyId.lblTxtCI.text = res;
        frmMBActivateAnyId.lblTxtCI.skin = lblRedNormal;
    } else if (gblCITMBLinked || (!gblCITMBLinked && !gblCIOtherBankLinked)) {
        frmMBActivateAnyId.lblTxtCI.text = kony.i18n.getLocalizedString("MIB_P2PRegisCIInfo");
        frmMBActivateAnyId.lblTxtCI.skin = lblBlueDBOzone128;
        frmMBActivateAnyId.label1504947934171198.text = "";
        if (gblCIActsForLocaleChange != null) {
            if (kony.i18n.getCurrentLocale() == "th_TH") frmMBActivateAnyId.lblCIActName.text = gblCIActsForLocaleChange["lblAccountNameTH"];
            else frmMBActivateAnyId.lblCIActName.text = gblCIActsForLocaleChange["lblAccountNameEN"];
        }
    }
    frmMBActivateAnyId.richtextTnc.text = kony.i18n.getLocalizedString("MIB_P2PAcceptTC");
    if (gblCIActsForLocaleChange != null && gblCIActsForLocaleChange["lblAccountNameTH"] != undefined && gblCIActsForLocaleChange["lblAccountNameEN"] != undefined) {
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmMBActivateAnyId.lblCIActName.text = gblCIActsForLocaleChange["lblAccountNameTH"];
            //frmMBActivateAnyId.lblCIActName.text=gblCIAccountsTH;
        } else {
            frmMBActivateAnyId.lblCIActName.text = gblCIActsForLocaleChange["lblAccountNameEN"];
            //frmMBActivateAnyId.lblCIActName.text=gblCIAccountsEN;
        }
    }
    if (gblMBActsForLocaleChange != null && gblMBActsForLocaleChange["lblAccountNameTH"] != undefined && gblMBActsForLocaleChange["lblAccountNameEN"] != undefined) {
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmMBActivateAnyId.lblname.text = gblMBActsForLocaleChange["lblAccountNameTH"];
            //frmMBActivateAnyId.lblname.text=gblCIAccountsTH;
        } else {
            //alert(gblMBActsForLocaleChange["lblAccountNameEN"]);
            frmMBActivateAnyId.lblname.text = gblMBActsForLocaleChange["lblAccountNameEN"];
            //frmMBActivateAnyId.lblname.text=gblCIAccountsEN;
        }
    }
    //frmMBActivateAnyId.label508341360459536.text=kony.i18n.getLocalizedString("MIB_P2PRegisMob");	
    /*if(gblMBTMBLinked || gblMBOtherBankLinked){
    	frmMBActivateAnyId.lblDeRegister.text = kony.i18n.getLocalizedString("MIB_P2PHowtoMobileNo");
    }
    if(gblMBTMBLinked || !gblMBOtherBankLinked){
    	frmMBActivateAnyId.lblDeRegister.text="";
    }
    if(gblCITMBLinked || !gblCIOtherBankLinked){
    	frmMBActivateAnyId.label1504947934171198.text="";
    }*/
}

function frmMBAnyIdCompletePreShowLocale() {
    changeStatusBarColor();
    frmMBAnyIdRegCompleted.lblAnyIdSuccessEdit.text = kony.i18n.getLocalizedString("MIB_P2PCompleteTextSuccessOnToOff");
    frmMBAnyIdRegCompleted.lblAnyIdFailure.text = kony.i18n.getLocalizedString("MIB_P2PCompleteTextFailure");
    frmMBAnyIdRegCompleted.lblAnyIdSuccess.text = kony.i18n.getLocalizedString("MIB_P2PCompleteTextSuccessOffToOn");
    frmMBAnyIdRegCompleted.lblAnyIdSuccess1stLaunch.text = kony.i18n.getLocalizedString("MIB_P2PCompleteTextSuccessOffToOnFirstLaunch");
    frmMBAnyIdRegCompleted.lblAnyIdSuccess2ndLaunch.text = kony.i18n.getLocalizedString("MIB_P2PCompleteTextSuccessOffToOnSecondLaunch");
    frmMBAnyIdRegCompleted.btnSettings.text = kony.i18n.getLocalizedString("MIB_P2PViewID");
    frmMBAnyIdRegCompleted.btnHome.text = kony.i18n.getLocalizedString("TRComplete_Btn_Return");
    frmMBAnyIdRegCompleted.lblHdrTxt.text = kony.i18n.getLocalizedString("MIB_P2PCompleteTitle");
    adjustBannerMB("frmMBAnyIdRegCompleted");
}

function onClickAnyIDRegTnCAgree() {
    kony.print("Insdie onClickAnyIDRegTnCAgree");
    fromTnC = true;
    singleAct = false;
    //showTMBAccounts();
    callCustActInqForAnyID();
    //showAgreeORNext();
    /*if(gblMBOtherBankLinked)
    {
    	frmMBActivateAnyId.lblDeRegister.text=kony.i18n.getLocalizedString("MIB_P2PHowtoMobileNo");
    	frmMBActivateAnyId.hbox508341360349300.margin=[0,12,0,0];
    	frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
    	var res = phoneDeRegTxt.replace("<Bank short name",resulttable.MobileDS[0].bankShortName);
    	if (kony.i18n.getCurrentLocale() == "th_TH") 
    		var res = res.replace("Bank name>",resulttable.MobileDS[0].bankNameTH);
    	else 
    		var res = res.replace("Bank name>",resulttable.MobileDS[0].bankNameEN);
    	frmMBActivateAnyId.lblTxtPhone.text=res;
    	frmMBActivateAnyId.lblTxtPhone.skin=lblRedNormal;
    	if(gblDeviceInfo["name"] == "android")
    		frmMBActivateAnyId.btnPhCheckBox.skin=btnCheck;
    	if(gblDeviceInfo["name"] == "iPhone")
    		frmMBActivateAnyId.switchMobile.selectedIndex = 1;
    }
    if(gblMBTMBLinked || !gblMBOtherBankLinked)
    {
    	fromTnC=false;
    	frmMBActivateAnyId.lblDeRegister.text="";
    	frmMBActivateAnyId.hbxPhoneAccount.setVisibility(true);
    	frmMBActivateAnyId.hbxPhoneAccount.onClick=showTMBAccounts;
    	frmMBActivateAnyId.lblTxtPhone.text=kony.i18n.getLocalizedString("MIB_P2PRegisMobInfo");
    	frmMBActivateAnyId.lblTxtPhone.skin=lblBlue80;
    	if(gblMBTMBLinked){
    		if(gblDeviceInfo["name"] == "android")
    			frmMBActivateAnyId.btnPhCheckBox.skin=btnCheckFoc;
    		if(gblDeviceInfo["name"] == "iPhone")
    			frmMBActivateAnyId.switchMobile.selectedIndex = 0;
    	}else{
    		if(gblDeviceInfo["name"] == "android")
    			frmMBActivateAnyId.btnPhCheckBox.skin=btnCheck;
    		if(gblDeviceInfo["name"] == "iPhone")
    			frmMBActivateAnyId.switchMobile.selectedIndex = 1;
    	}
    	displayTMBMBAct(gblServiceMBAct);
    }
    if(gblCIOtherBankLinked)
    {
    	frmMBActivateAnyId.label1504947934171198.text=kony.i18n.getLocalizedString("MIB_P2PHowtoCitizenID");
    	var CIDeRegTxt = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankCitizenID");
    	
    	var res = CIDeRegTxt.replace("<Bank short name",resulttable.CIDS[0].bankShortName);
    	if (kony.i18n.getCurrentLocale() == "th_TH") 
    		var res = res.replace("Bank name>",resulttable.CIDS[0].bankNameTH);
    	else 
    		var res = res.replace("Bank name>",resulttable.CIDS[0].bankNameEN);
    	frmMBActivateAnyId.lblTxtCI.text=res;
    	frmMBActivateAnyId.lblTxtCI.skin=lblRedNormal;
    	frmMBActivateAnyId.hbox508341360540829.margin=[3,2,0,0];
    	frmMBActivateAnyId.lblTxtCI.margin=[0,0,0,0];
    	frmMBActivateAnyId.hbox508341360540829.padding=[0,0,1,0];
    	frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
    	if(gblDeviceInfo["name"] == "android")
    		frmMBActivateAnyId.btnCIChckBox.skin=btnCheck;
    	if(gblDeviceInfo["name"] == "iPhone")
    		frmMBActivateAnyId.switchSocialID.selectedIndex = 1;
    	frmMBActivateAnyId.hboxTnC.padding=[0,0,10,0];
    }
    if(gblCITMBLinked || !gblCIOtherBankLinked)
    {
    	fromTnC=false;
    	frmMBActivateAnyId.label1504947934171198.text="";
    	frmMBActivateAnyId.hboxSocialAccount.setVisibility(true);
    	frmMBActivateAnyId.hboxSocialAccount.onClick=showCITMBAccounts;
    	frmMBActivateAnyId.lblTxtCI.text=kony.i18n.getLocalizedString("MIB_P2PRegisCIInfo");
    	frmMBActivateAnyId.lblTxtCI.skin=lblBlue80;
    	if(gblCITMBLinked){
    		if(gblDeviceInfo["name"] == "android")
    			frmMBActivateAnyId.btnCIChckBox.skin=btnCheckFoc;
    		if(gblDeviceInfo["name"] == "iPhone")
    			frmMBActivateAnyId.switchSocialID.selectedIndex = 0;
    	}
    	else{
    		if(gblDeviceInfo["name"] == "android")
    			frmMBActivateAnyId.btnCIChckBox.skin=btnCheck;
    		if(gblDeviceInfo["name"] == "iPhone")
    			frmMBActivateAnyId.switchSocialID.selectedIndex = 1;
    	}
    		
    	displayTMBCIAct(gblServiceCIAct);
    }
    if(!gblCITMBLinked)
    {
    	frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
    }
    if(!gblMBTMBLinked)
    {
    	frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
    }
    frmMBActivateAnyId.btnNext.skin=btnDisabledGray;
    frmMBActivateAnyId.btnNext.focusSkin=btnDisabledGray;
    frmMBActivateAnyId.btnNext.onClick=disableBackButton();
    frmMBActivateAnyId.show();*/
}

function onSelectActBack() {
    if (gblMBOtherBankLinked) {
        frmMBActivateAnyId.lblDeRegister.text = kony.i18n.getLocalizedString("MIB_P2PHowtoMobileNo");
        frmMBActivateAnyId.hbox508341360349300.margin = [0, 12, 0, 0];
        frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
        if (undefined != gblMBLinkOtherBankLangStr) {
            var mbDeRegTxt = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankMobileNo");
            var res = mbDeRegTxt.replace("<Bank short name", gblMBLinkOtherBankLangStr.bankShortName);
            if (kony.i18n.getCurrentLocale() == "th_TH") var res = res.replace("Bank name>", gblMBLinkOtherBankLangStr.bankNameTH);
            else var res = res.replace("Bank name>", gblMBLinkOtherBankLangStr.bankNameEN);
            frmMBActivateAnyId.lblAccountNum.text = gblMBActsForLocaleChange.lblActNoVal;
            frmMBActivateAnyId.lblTxtPhone.text = res;
        }
        frmMBActivateAnyId.lblTxtPhone.skin = lblRedNormal;
    }
    if (gblMBTMBLinked || !gblMBOtherBankLinked) {
        fromTnC = false;
        frmMBActivateAnyId.lblDeRegister.text = "";
        frmMBActivateAnyId.lblTxtPhone.text = kony.i18n.getLocalizedString("MIB_P2PRegisMobInfo");
        frmMBActivateAnyId.lblTxtPhone.skin = lblBlueDBOzone128;
        var actName = "";
        if (null != gblMBActsForLocaleChange.lblActNoVal) {
            var actName = "";
            if (kony.i18n.getCurrentLocale() == "th_TH") actName = gblMBActsForLocaleChange.lblAccountNameTH;
            else actName = gblMBActsForLocaleChange.lblAccountNameEN;
            frmMBActivateAnyId.lblname.text = actName;
            frmMBActivateAnyId.imgacnt.text = gblMBActsForLocaleChange.imgAccountPicture;
            frmMBActivateAnyId.lblAccountNum.text = gblMBActsForLocaleChange.lblActNoVal;
            frmMBActivateAnyId.hbxPhoneAccount.setVisibility(true);
            frmMBActivateAnyId.hbxPhoneAccount.onClick = showTMBAccounts;
        } else {
            gblMBActsForLocaleChange = [];
            frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
            //frmMBActivateAnyId.hbxPhoneAccount.onClick=showTMBAccounts;
        }
        /*if(gblMBTMBLinked){
        	
        	if(gblDeviceInfo["name"] == "android")
        		frmMBActivateAnyId.btnPhCheckBox.skin=btnCheckFoc;
        	if(gblDeviceInfo["name"] == "iPhone")
        		frmMBActivateAnyId.switchMobile.selectedIndex = 0;
        }else{
        	if(gblDeviceInfo["name"] == "android")
        		frmMBActivateAnyId.btnPhCheckBox.skin=btnCheck;
        	if(gblDeviceInfo["name"] == "iPhone")
        		frmMBActivateAnyId.switchMobile.selectedIndex = 1;
        }
        displayTMBMBAct(gblServiceMBAct);*/
    }
    if (gblCIOtherBankLinked) {
        frmMBActivateAnyId.label1504947934171198.text = kony.i18n.getLocalizedString("MIB_P2PHowtoCitizenID");
        if (undefined != gblLinkOtherBankLangStr) {
            var CIDeRegTxt = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankCitizenID");
            var res = CIDeRegTxt.replace("<Bank short name", gblLinkOtherBankLangStr.bankShortName);
            if (kony.i18n.getCurrentLocale() == "th_TH") var res = res.replace("Bank name>", gblLinkOtherBankLangStr.bankNameTH);
            else var res = res.replace("Bank name>", gblLinkOtherBankLangStr.bankNameEN);
            frmMBActivateAnyId.lblTxtCI.text = res;
            frmMBActivateAnyId.lblCIActNum.text = gblCIActsForLocaleChange.lblActNoVal;
        }
        frmMBActivateAnyId.lblTxtCI.skin = lblRedNormal;
        frmMBActivateAnyId.hbox508341360540829.margin = [3, 2, 0, 0];
        frmMBActivateAnyId.lblTxtCI.margin = [0, 0, 0, 0];
        frmMBActivateAnyId.hbox508341360540829.padding = [0, 0, 1, 0];
        frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
        frmMBActivateAnyId.hboxTnC.padding = [0, 0, 10, 0];
    }
    if (gblCITMBLinked || !gblCIOtherBankLinked) {
        fromTnC = false;
        frmMBActivateAnyId.label1504947934171198.text = "";
        frmMBActivateAnyId.lblTxtCI.text = kony.i18n.getLocalizedString("MIB_P2PRegisCIInfo");
        frmMBActivateAnyId.lblTxtCI.skin = lblBlueDBOzone128;
        var actName = "";
        if (null != gblCIActsForLocaleChange.lblActNoVal) {
            if (kony.i18n.getCurrentLocale() == "th_TH") actName = gblCIActsForLocaleChange.lblAccountNameTH;
            else actName = gblCIActsForLocaleChange.lblAccountNameEN;
            frmMBActivateAnyId.lblCIActName.text = actName;
            frmMBActivateAnyId.imgCI.text = gblCIActsForLocaleChange.imgAccountPicture;
            frmMBActivateAnyId.lblCIActNum.text = gblCIActsForLocaleChange.lblActNoVal;
            frmMBActivateAnyId.hboxSocialAccount.setVisibility(true);
            frmMBActivateAnyId.hboxSocialAccount.onClick = showCITMBAccounts;
        } else {
            gblCIActsForLocaleChange = [];
            frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
            //frmMBActivateAnyId.hboxSocialAccount.onClick=showCITMBAccounts;
        }
        /*if(gblCITMBLinked){
        	if(gblDeviceInfo["name"] == "android")
        		frmMBActivateAnyId.btnCIChckBox.skin=btnCheckFoc;
        	if(gblDeviceInfo["name"] == "iPhone")
        		frmMBActivateAnyId.switchSocialID.selectedIndex = 0;
        }
        else{
        	if(gblDeviceInfo["name"] == "android")
        		frmMBActivateAnyId.btnCIChckBox.skin=btnCheck;
        	if(gblDeviceInfo["name"] == "iPhone")
        		frmMBActivateAnyId.switchSocialID.selectedIndex = 1;
        }*/
        //displayTMBMBAct(gblServiceCIAct);	
    }
    /*if(!gblCITMBLinked)
    {
    	frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
    }
    if(!gblMBTMBLinked)
    {
    	frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
    }*/
    frmMBActivateAnyId.btnNext.text = kony.i18n.getLocalizedString("Next");
    frmMBActivateAnyId.btnNext.skin = btnDisabledGray;
    frmMBActivateAnyId.btnNext.focusSkin = btnDisabledGray;
    frmMBActivateAnyId.btnNext.onClick = disableBackButton();
    frmMBActivateAnyId.show();
}

function displayTMBMBAct(serviceactid) {
    if (gblAnyIDAccountTable != null) {
        for (var i = 0; gblAnyIDAccountTable.custAcctRec != null && i < gblAnyIDAccountTable.custAcctRec.length; i++) {
            if ((gblAnyIDAccountTable.custAcctRec[i].accId.indexOf(serviceactid)) > -1) {
                if ((gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == null || (gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == '') {
                    var sbStr = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
                    var length = sbStr.length;
                    sbStr = sbStr.substring(length - 4, length);
                    if (kony.i18n.getCurrentLocale() == "th_TH") accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"] + " " + sbStr;
                    else accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"] + " " + sbStr;
                    accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"] + " " + sbStr;
                    accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"] + " " + sbStr;
                } else {
                    accountName = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
                    accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
                    accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
                }
                if (gblAnyIDAccountTable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard")) {
                    accountNo = gblAnyIDAccountTable["custAcctRec"][i]["accountNoFomatted"];
                } else {
                    accountNoUnformatted = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
                    accountNoTenDigit = accountNoUnformatted.substring(accountNoUnformatted.length - 10, accountNoUnformatted.length);
                    accountNo = formatAccountNo(accountNoTenDigit);
                }
                imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblAnyIDAccountTable["custAcctRec"][i]["ICON_ID"] + "&modIdentifier=PRODICON";
                var tempAnyIDCustActInq = {
                    "imgAccountPicture": imageName,
                    "lblTMBAccountDetails": accountName,
                    "lblActNoVal": accountNo,
                    "lblAccountNumberText": kony.i18n.getLocalizedString("keyAccountNoIB"),
                    "lblAccountNameEN": accountNameEN,
                    "lblAccountNameTH": accountNameTH,
                    "hiddenAccountId": gblAnyIDAccountTable["custAcctRec"][i]["accId"],
                    "hiddenAcctType": gblAnyIDAccountTable["custAcctRec"][i]["accType"]
                }
            }
            //alert(tempAnyIDCustActInq.lblAccountNameTH);
        }
        return tempAnyIDCustActInq;
        //frmMBActivateAnyId.hbxPhoneAccount.setVisibility(true);
    }
}

function showOTPPopupForAnyIDOTPValidation(lblText, refNo, mobNO, callBackConfirm) {
    popupTractPwd.containerWeight = 94;
    popupTractPwd.btnPopupTractConf.text = kony.i18n.getLocalizedString("keyConfirm");
    popupTractPwd.btnPopupTractConf.onClick = onClickAnyIDOTPConfirm;
    popupTractPwd.txtOTP.text = "";
    popupTractPwd.lblOTP.text = lblText;
    popupTractPwd.hbxIncorrectOTP.isVisible = false;
    popupTractPwd.lblPopupTract1.isVisible = true;
    if (flowSpa) {
        popupTractPwd.hbxPopupTractlblHoldSpa.isVisible = true;
    } else {
        popupTractPwd.hbxPopupTractlblHold.isVisible = true;
    }
    popupTractPwd.lblPopupTract5.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    popupTractPwd.lblPopupTract5.isVisible = true;
    if (flowSpa) {
        popupTractPwd.lblPopupTract4Spa.text = mobNO;
    } else {
        popupTractPwd.lblPopupTract4.text = mobNO;
    }
    popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
    popupTractPwd.btnPopUpTractCancel.focusSkin = btnLightBlue;
    popupTractPwd.btnPopUpTractCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupTractPwd.btnPopUpTractCancel.onClick = onClickAnyIDOTPCancel;
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        popupTractPwd.lblOTP.containerWeight = 12;
    } else {
        popupTractPwd.lblOTP.containerWeight = 12;
    }
    popupTractPwd.hbxOTP.isVisible = true;
    popupTractPwd.hbxPoupAccesspin.isVisible = false;
    popupTractPwd.hbxPopupTranscPwd.isVisible = false;
    popupTractPwd.lblOTP.text = lblText;
    popupTractPwd.txtOTP.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
    popupTractPwd.txtOTP.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    dismissLoadingScreen();
    popupTractPwd.show();
}

function onClickAnyIDOTPCancel() {
    gblLocalfromDeregister = false;
    popupTractPwd.dismiss();
}

function generateOTPAnyIDRegService() {
    gblPhoneNumberReq = frmMBActivateAnyId.lblPhoneNum.text;
    var inputParams = {
        Channel: "anyIDRegistration",
        locale: "en_US",
        Recipient_Name: gblCustomerName,
        retryCounterRequestOTP: gblRetryCountRequestOTP
    };
    invokeServiceSecureAsync("generateOTP", inputParams, generateOTPAnyIDRegCallBack);
}

function generateOTPAnyIDRegCallBack(status, resulttable) {
    dismissLoadingScreen();
    if (status == 400) //success response
    {
        if (resulttable["opstatus"] == 0) {
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            reqOtpTimer = kony.os.toNumber(reqOtpTimer);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            try {
                kony.timer.cancel("otpTimerAnyID")
            } catch (e) {}
            kony.timer.schedule("otpTimerAnyID", otpTimerAnyIDRegCallBack, reqOtpTimer, false);
            var otptext = popupTractPwd.txtOTP.text;
            showOTPPopupForAnyIDOTPValidation(kony.i18n.getLocalizedString("keyOTP"), "ABCD", gblPHONENUMBER, callBackOTPRequest);
            var refVal = "";
            for (var d = 0; d < resulttable["Collection1"].length; d++) {
                if (resulttable["Collection1"][d]["keyName"] == "pac") {
                    refVal = resulttable["Collection1"][d]["ValueString"];
                    break;
                }
            }
            popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + " " + refVal;
            if (!flowSpa) {
                popupTractPwd.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
                popupTractPwd.lblPopupTract4.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            }
            popupTractPwd.lblPopupTract7.text = "";
            otpConfirmEnable();
            gblOTPFlag = false;
        } else {
            //popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + resulttable["pac"];
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            if (resulttable["requestOTPEnableTime"] == "undefined" || resulttable["requestOTPEnableTime"] == null || resulttable["requestOTPEnableTime"] == "") var reqOtpTimer = kony.os.toNumber(60);
            else var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            //kony.timer.schedule("otpTimer", otpTimerCallBack, reqOtpTimer, false);
            if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                gblOTPFlag = false;
                return false;
            } else if (resulttable["errCode"] == "GenOTPRtyErr00002") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                alert(" " + resulttable["errMsg"]);
                kony.application.dismissLoadingScreen();
                gblOTPFlag = false;
                return false;
            }
        }
    }
}

function onClickAnyIDRegRequestOtp() {
    //showLoadingScreen();
    popupDeRegister.lblText.text = "";
    //userMBChanged = false;
    if (frmMBActivateAnyId.lblAccountNum.text == "" && gblServiceMBAct.trim() != "") isMBDeregister = true;
    else isMBDeregister = false;
    if (frmMBActivateAnyId.lblCIActNum.text == "" && gblServiceCIAct.trim() != "") isCIDeregister = true;
    else isCIDeregister = false;
    if (isMBDeregister) popupDeRegister.lblText.text = kony.i18n.getLocalizedString("MIB_P2PDelPop");
    if (isCIDeregister) popupDeRegister.lblText.text = kony.i18n.getLocalizedString("MIB_P2PDelPop");
    if (isCIDeregister && isMBDeregister) popupDeRegister.lblText.text = kony.i18n.getLocalizedString("MIB_P2PDelPop");
    if (popupDeRegister.lblText.text != "" && (!gblLocalfromDeregister)) {
        popupDeRegister.lblDeRegHead.text = kony.i18n.getLocalizedString("MIB_P2PDelPopTitle");
        popupDeRegister.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        popupDeRegister.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        popupDeRegister.hBOXDeReg.setVisibility(true);
        popupDeRegister.hbxPopupTracbtn.setVisibility(false);
        popupDeRegister.show();
    } else callGenerateOTPService();
}

function callGenerateOTPService() {
    //popupDeRegister.dismiss();
    showLoadingScreen();
    gblOTPFlag = true;
    gblOnClickReq = true; //
    popupTractPwd.btnOtpRequest.skin = btnDisabledGray;
    popupTractPwd.btnOtpRequest.focusSkin = btnDisabledGray;
    popupTractPwd.btnOtpRequest.setEnabled(false);
    //checkBusinessHoursForAnyID();
    saveAnyIDSessionMB();
    //generateOTPAnyIDRegService();
}

function saveAnyIDSessionMB() {
    //dismissLoadingScreen();
    var inputParam = {};
    //params to be saved in session
    var anyIDMBActNum = frmMBActivateAnyId.lblAccountNum.text;
    if (anyIDMBActNum != null) anyIDMBActNum = anyIDMBActNum.replace(/-/g, '');
    else anyIDMBActNum = "";
    var anyIDCIActNum = frmMBActivateAnyId.lblCIActNum.text;
    if (anyIDCIActNum != null) anyIDCIActNum = anyIDCIActNum.replace(/-/g, '');
    else anyIDCIActNum = "";
    //For Mobile Registration
    inputParam["mobileActNum"] = anyIDMBActNum;
    //for CI Registration
    inputParam["CIActNum"] = anyIDCIActNum;
    invokeServiceSecureAsync("saveAnyIDRegistrationSession", inputParam, saveAnyIDSessionMBCallbackfunction);
}

function saveAnyIDSessionMBCallbackfunction(status, resulttable) {
    if (status == 400) {
        //dismissLoadingScreen();
        if (resulttable["opstatus"] == "0") {
            //Show the Confirm Form
            //checkTokenAndGenOTPAnyIDIB();
            var mobileAnyIDAccountInqStatusCode = resulttable["mobileAnyIDAccountInqStatusCode"];
            var CIAnyIDAccountInqStatusCode = resulttable["CIAnyIDAccountInqStatusCode"];
            if ((mobileAnyIDAccountInqStatusCode == "" || mobileAnyIDAccountInqStatusCode == "0") && (CIAnyIDAccountInqStatusCode == "" || CIAnyIDAccountInqStatusCode == "0")) {
                gblRetryCountRequestOTP = "0";
                generateOTPAnyIDRegService();
            } else {
                dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function otpTimerAnyIDRegCallBack() {
    popupTractPwd.btnOtpRequest.skin = btnLightBlue;
    popupTractPwd.btnOtpRequest.setEnabled(true);
    popupTractPwd.btnOtpRequest.onClick = onClickAnyIDRegRequestOtp;
    gblOTPFlag = true;
    try {
        kony.timer.cancel("otpTimerAnyID")
    } catch (e) {}
}

function callBackOTPRequest() {
    kony.print("OTP Entered navigate to complete screen");
}

function showAnyIDRegComplete() {
    frmMBAnyIdRegCompleted.show();
}
//to display list of TMB accounts 
function showTMBAccounts() {
    gblselectedChckBox = "";
    gblselectedChckBox = "PhoneCheckBox";
    if (!singleAct) showCommonTMBAct();
}

function showCommonTMBAct() {
    var tempAnyIDCustActInq = [];
    if (singleAct) singleActMB();
    else if (gblAccountsAnyID != null) {
        showLoadingScreen();
        if (gblAccountsAnyID.length > 0) {
            for (i = 0; i < gblAccountsAnyID.length; i++) {
                if (kony.i18n.getCurrentLocale() == "th_TH") accountName = gblAccountsAnyID[i]["lblAccountNameTH"];
                else accountName = gblAccountsAnyID[i]["lblAccountNameEN"];
                tempAnyIDCustActInq.push({
                    "lblTMBAccountDetails": accountName,
                    "lblActNoVal": gblAccountsAnyID[i]["lblActNoVal"],
                    "lblActNo": kony.i18n.getLocalizedString("AccNo"),
                    "SortId": gblAccountsAnyID[i]["SortId"],
                    "lblAccountNameEN": gblAccountsAnyID[i]["lblAccountNameEN"],
                    "lblAccountNameTH": gblAccountsAnyID[i]["lblAccountNameTH"],
                    "imgAccountPicture": gblAccountsAnyID[i]["imgAccountPicture"]
                });
            }
        }
        if (tempAnyIDCustActInq.length > 0) tempAnyIDCustActInq = sortByKey(tempAnyIDCustActInq, 'SortId');
        TMBUtil.DestroyForm(frmMBAnyIDSelectActs);
        frmMBAnyIDSelectActs.segTMBAccntDetails.removeAll();
        frmMBAnyIDSelectActs.segTMBAccntDetails.addAll(tempAnyIDCustActInq);
        frmMBAnyIDSelectActs.show();
        dismissLoadingScreen();
    }
}
//to display list of TMB accounts 
function showCITMBAccounts() {
    gblselectedChckBox = "";
    gblselectedChckBox = "CICheckBox";
    if (!singleAct) showCommonTMBAct();
}

function singleActMB() {
    gblMBActsForLocaleChange = gblAccountsAnyID[0];
    frmMBActivateAnyId.lblAccountNum.text = gblAccountsAnyID[0]["lblActNoVal"];
    if (kony.i18n.getCurrentLocale() == "th_TH") frmMBActivateAnyId.lblname.text = gblAccountsAnyID[0]["lblAccountNameTH"];
    else frmMBActivateAnyId.lblname.text = gblAccountsAnyID[0]["lblAccountNameEN"];
    frmMBActivateAnyId.imgacnt.src = gblAccountsAnyID[0]["imgAccountPicture"];
    frmMBActivateAnyId.imgNav.setVisibility(false);
    frmMBActivateAnyId.hbxPhoneAccount.setVisibility(true);
    if (frmMBActivateAnyId.lblAccountNum.text != "" && gblServiceMBAct.trim() != "" && frmMBActivateAnyId.lblAccountNum.text.replace(/-/g, '') == gblServiceMBAct.trim()) isMBRegister = false;
    else isMBRegister = true;
    showAgreeORNext();
}

function singleActCI() {
    gblCIActsForLocaleChange = gblAccountsAnyID[0];
    frmMBActivateAnyId.lblCIActNum.text = gblAccountsAnyID[0]["lblActNoVal"];
    if (kony.i18n.getCurrentLocale() == "th_TH") frmMBActivateAnyId.lblCIActName.text = gblAccountsAnyID[0]["lblAccountNameTH"];
    else frmMBActivateAnyId.lblCIActName.text = gblAccountsAnyID[0]["lblAccountNameEN"];
    frmMBActivateAnyId.imgCI.src = gblAccountsAnyID[0]["imgAccountPicture"];
    frmMBActivateAnyId.image2508341360639218.setVisibility(false);
    frmMBActivateAnyId.hboxSocialAccount.setVisibility(true);
    if (frmMBActivateAnyId.lblCIActNum.text != "" && gblServiceCIAct.trim() != "" && frmMBActivateAnyId.lblCIActNum.text.replace(/-/g, '') == gblServiceCIAct.trim()) isCIRegister = false;
    else isCIRegister = true;
    showAgreeORNext();
}

function clearSegDataAnyIDAccntList() {
    // Clearing old data
    //frmMBAnyIDSelectActs.segTMBAccntDetails.removeAll();
    frmMBAnyIDSelectActs.lblTMBBank.isVisible = false;
}

function frmAnyIDAccountListPreShow() {
    if (!(LocaleController.isFormUpdatedWithLocale(frmMBAnyIDSelectActs.id))) {
        frmMBAnyIDSelectActs.lblHdrTxtList.text = kony.i18n.getLocalizedString("MIB_P2PAcct");
        frmMBAnyIDSelectActs.lblTMBBank.text = kony.i18n.getLocalizedString("keylblTMBBank");
        frmMBAnyIDSelectActs.btnAnyID.text = kony.i18n.getLocalizedString("Back");
        fromTnC = false;
        //LocaleController.updatedForm(frmMBAnyIDSelectActs.id);
        //myAccountListService();
    }
}

function callCustActInqForAnyID() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["accountsFlag"] = "true";
    invokeServiceSecureAsync("customerAccountInquiry", inputparam, callBackCustActInqForAnyid);
}

function callBackCustActInqForAnyid(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblAnyIDAccountTable = resulttable;
            nonCASAAct = 0;
            gblAccountsAnyID = [];
            if (gblAnyIDAccountTable != null) {
                rawDataTMB = [];
                for (var i = 0; gblAnyIDAccountTable.custAcctRec != null && i < gblAnyIDAccountTable.custAcctRec.length; i++) {
                    var accountName;
                    var accountNo;
                    var accountNameEN;
                    var accountNameTH;
                    var accountNoTenDigit;
                    var accountNoUnformatted;
                    var allowRegID = gblAnyIDAccountTable["custAcctRec"][i]["allowRegisterAnyId"];
                    if (allowRegID == 1) {
                        if ((gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == null || (gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == '') {
                            var sbStr = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
                            var length = sbStr.length;
                            sbStr = sbStr.substring(length - 4, length);
                            if (kony.i18n.getCurrentLocale() == "th_TH") accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"] + " " + sbStr;
                            else accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"] + " " + sbStr;
                            accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"] + " " + sbStr;
                            accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"] + " " + sbStr;
                        } else {
                            accountName = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
                            accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
                            accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
                        }
                        if (gblAnyIDAccountTable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard")) {
                            accountNo = gblAnyIDAccountTable["custAcctRec"][i]["accountNoFomatted"];
                        } else {
                            accountNoUnformatted = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
                            accountNoTenDigit = accountNoUnformatted.substring(accountNoUnformatted.length - 10, accountNoUnformatted.length);
                            accountNo = formatAccountNo(accountNoTenDigit);
                        }
                        imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblAnyIDAccountTable["custAcctRec"][i]["ICON_ID"] + "&modIdentifier=PRODICON";
                        var tempAnyIDCustActInq = {
                            "imgAccountPicture": imageName,
                            "lblTMBAccountDetails": accountName,
                            "lblActNo": kony.i18n.getLocalizedString("AccNo"),
                            "lblActNoVal": accountNo,
                            "lblAccountNumberText": kony.i18n.getLocalizedString("keyAccountNoIB"),
                            "lblAccountNameEN": accountNameEN,
                            "lblAccountNameTH": accountNameTH,
                            "SortId": gblAnyIDAccountTable["custAcctRec"][i]["SortId"],
                            "hiddenAccountId": gblAnyIDAccountTable["custAcctRec"][i]["accId"],
                            "hiddenAcctType": gblAnyIDAccountTable["custAcctRec"][i]["accType"]
                        }
                        gblAccountsAnyID.push(tempAnyIDCustActInq);
                    }
                }
                if (gblAccountsAnyID.length == 1) singleAct = true;
                if (gblAccountsAnyID.length == 0) {
                    noEligibleActs = true;
                    showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_P2PErr_NoEliAcc"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
                    return false;
                } else callAnyIDInq();
            }
        }
    }
}
//display the selected acct details
function displayselectedAccs() {
    fromBack = true;
    if (gblselectedChckBox == "PhoneCheckBox") {
        var mobileactId = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0].lblActNoVal;
        gblMBActsForLocaleChange = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0];
        frmMBActivateAnyId.lblAccountNum.text = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0].lblActNoVal;
        //frmMBActivateAnyId.lblname.text=accountName;
        frmMBActivateAnyId.imgacnt.src = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0].imgAccountPicture;
        frmMBActivateAnyId.hbxPhoneAccount.setVisibility(true);
        frmMBActivateAnyId.imgNav.setVisibility(true);
        frmMBActivateAnyId.hboxTnC.setVisibility(true);
        if (gblServiceMBAct == "" && gblServiceMBAct != frmMBActivateAnyId.lblAccountNum.text) isMBRegister = true;
        else isMBRegister = false;
        if (gblMBTMBLinked && gblServiceMBAct.trim() != "" && gblServiceMBAct != mobileactId.replace(/-/g, '')) userMBChanged = true;
        else userMBChanged = false;
    }
    if (gblselectedChckBox == "CICheckBox") {
        gblCIActsForLocaleChange = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0];
        var ciActid = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0].lblActNoVal;
        frmMBActivateAnyId.lblCIActNum.text = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0].lblActNoVal;
        //frmMBActivateAnyId.lblCIActName.text=CIaccountName;
        frmMBActivateAnyId.imgCI.src = frmMBAnyIDSelectActs.segTMBAccntDetails.selectedItems[0].imgAccountPicture;
        frmMBActivateAnyId.line508341360728137.setVisibility(true);
        frmMBActivateAnyId.hboxSocialAccount.setVisibility(true);
        frmMBActivateAnyId.image2508341360639218.setVisibility(true);
        frmMBActivateAnyId.hboxTnC.setVisibility(true);
        if (gblServiceCIAct == "" && gblServiceCIAct != frmMBActivateAnyId.lblCIActNum.text) isCIRegister = true;
        else isCIRegister = false;
        if (gblCITMBLinked && gblServiceCIAct.trim() != "" && gblServiceCIAct != ciActid.replace(/-/g, '')) userCIChanged = true;
        else userCIChanged = false;
    }
    //showAgreeORNext();
    frmMBActivateAnyId.show();
}

function deRegisterPopUP() {
    popupDeRegister.lblDeRegHead.text = kony.i18n.getLocalizedString("MIB_P2PHowtoTitle");
    popupDeRegister.lblText.text = kony.i18n.getLocalizedString("MIB_P2PErr_LinkOtherMobileNo");
    popupDeRegister.btnPopupTractConf.text = kony.i18n.getLocalizedString("keyClose");
    popupDeRegister.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    popupDeRegister.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupDeRegister.hBOXDeReg.setVisibility(false);
    popupDeRegister.hbxPopupTracbtn.setVisibility(true);
    //frmMBActivateAnyId.btnPhCheckBox.skin=btnCheck;
    popupDeRegister.show();
}

function cideRegisterPopUP() {
    popupDeRegister.lblDeRegHead.text = kony.i18n.getLocalizedString("MIB_P2PHowtoTitle");
    popupDeRegister.lblText.text = kony.i18n.getLocalizedString("MIB_P2PErr_LinkOtherCitizenID");
    popupDeRegister.btnPopupTractConf.text = kony.i18n.getLocalizedString("keyClose");
    popupDeRegister.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
    popupDeRegister.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupDeRegister.hBOXDeReg.setVisibility(false);
    popupDeRegister.hbxPopupTracbtn.setVisibility(true);
    popupDeRegister.show();
}
/*
function btnChkTC()
{
	if(frmMBActivateAnyId.btnChkTC.skin==btnCheck)
	{
		frmMBActivateAnyId.btnChkTC.skin=btnCheckFoc;
		frmMBActivateAnyId.btnNext.skin=btnBlueSkin;
		frmMBActivateAnyId.btnNext.focusSkin=btnBlueSkin;
		frmMBActivateAnyId.btnNext.setEnabled(true);
	}
	else
	{
		frmMBActivateAnyId.btnNext.text=kony.i18n.getLocalizedString("Next");
		frmMBActivateAnyId.btnChkTC.skin=btnCheck;
		frmMBActivateAnyId.btnNext.skin=btnDisabledGray;
		frmMBActivateAnyId.btnNext.focusSkin=btnDisabledGray;
		frmMBActivateAnyId.btnNext.setEnabled(false);
	}
}
**/
function onClickEmailTnCMBAnyID() {
    //call notification add service for email with the tnc keyword
    //alert("tnckeyword::"+tnckeyword);
    showLoadingScreen()
    var inputparam = {};
    inputparam["channelName"] = "Mobile Banking";
    inputparam["channelID"] = "02";
    inputparam["notificationType"] = "Email"; // always email
    inputparam["phoneNumber"] = gblPHONENUMBER;
    inputparam["mail"] = gblEmailId;
    inputparam["customerName"] = gblCustomerName;
    inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "AnyIDRegistration";
    invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCAnyIDMB);
}

function callBackEmailTnCAnyIDMB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
            } else {
                dismissLoadingScreen()
                return false;
            }
        } else {
            dismissLoadingScreen()
        }
    }
}
// for Terms & COnditions
function loadAnyIDRegTermsNConditions() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    //module key again is from tmb_tnc file
    var prodDescTnC = "AnyIDRegistration";
    //prodDescTnC = prodDescTnC.replace(/\s+/g, '');
    input_param["moduleKey"] = prodDescTnC;
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, loadTNCAnyIDRegBackMB);
}
//for de-registration and others 
function combinationTypes() {
    gblLocalfromDeregister = true;
    popupDeRegister.dismiss();
    var mbactid = frmMBActivateAnyId.lblAccountNum.text;
    var ciactid = frmMBActivateAnyId.lblCIActNum.text;
    if (mbactid != null && mbactid.trim() != "" && gblMBTMBLinked && gblServiceCIAct != null && gblServiceMBAct.trim() != "" && gblServiceMBAct != mbactid.replace(/-/g, '')) userMBChanged = true;
    else userMBChanged = false;
    if (ciactid != null && ciactid.trim() != "" && gblCITMBLinked && gblServiceCIAct != null && gblServiceCIAct.trim() != "" && gblServiceCIAct != ciactid.replace(/-/g, '')) userCIChanged = true;
    else userCIChanged = false;
    if (isMBRegister || isCIRegister || userMBChanged || userCIChanged) callGenerateOTPService();
    else onClickAnyIDOTPConfirm();
}

function loadTNCAnyIDRegBackMB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmMBAnyIDRegAcceptTnC.btnRight.setVisibility(true);
            frmMBAnyIDRegAcceptTnC.hbox44726454989122.setVisibility(true);
            frmMBAnyIDRegAcceptTnC.btnAnyID.text = kony.i18n.getLocalizedString('Back');
            frmMBAnyIDRegAcceptTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
            frmMBAnyIDRegAcceptTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyTermsNConditions");
            frmMBAnyIDRegAcceptTnC.richTextProdDetails.setVisibility(false);
            frmMBAnyIDRegAcceptTnC.hboxSaveCamEmail.setVisibility(false);
            frmMBAnyIDRegAcceptTnC.richtext47425439023817.setVisibility(true);
            frmMBAnyIDRegAcceptTnC.richtext47425439023817.text = result["fileContent"];
            frmMBAnyIDRegAcceptTnC.lblOpenActDescSubTitle.setFocus(true);
            dismissLoadingScreen();
            frmMBAnyIDRegAcceptTnC.show();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}
var screendpiservice = "";

function preShowfrmRegAnyIdAnnouncement() {
    frmRegAnyIdAnnouncement.btnRegNow.text = kony.i18n.getLocalizedString("MIB_AnyIDNow");
    frmRegAnyIdAnnouncement.btnLater.text = kony.i18n.getLocalizedString("MIB_AnyIDLater");
    var locale = kony.i18n.getCurrentLocale();
    getDeviceDpi();
    var anyIDAnnouncement_image_name_EN = "web-banner-Any-ID_N_Eng_";
    var anyIDAnnouncement_image_name_TH = "web-banner-Any-ID_N_Thai_";
    var randomnum = Math.floor((Math.random() * 10000) + 1);
    if (locale == "en_US") {
        //frmRegAnyIdAnnouncement.imgAnydAnnouPage.src = "aw_main_screen_eng.png";
        var anyIDAnnouncement_image_EN = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + anyIDAnnouncement_image_name_EN + screendpiservice + "&modIdentifier=PRODUCTPACKAGEIMG&dummy=" + randomnum;
        frmRegAnyIdAnnouncement.imgAnydAnnouPage.src = anyIDAnnouncement_image_EN;
    } else {
        //frmRegAnyIdAnnouncement.imgAnydAnnouPage.src = "aw_main_screen_th.png";
        var anyIDAnnouncement_image_TH = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + anyIDAnnouncement_image_name_TH + screendpiservice + "&modIdentifier=PRODUCTPACKAGEIMG&dummy=" + randomnum;
        frmRegAnyIdAnnouncement.imgAnydAnnouPage.src = anyIDAnnouncement_image_TH;
    }
}

function getDeviceDpi() {
    var gblDeviceInfo = kony.os.deviceInfo();
    var screendpi = "";
    var deviceName = gblDeviceInfo["name"];
    var deviceModel = gblDeviceInfo["model"];
    var screenwidth = gblDeviceInfo["deviceWidth"];
    if (deviceName == "iPhone" || deviceName == "iPad") {
        //var str = deviceName;
        //var check = str.indexOf("4");
        if (screenwidth == 320) {
            screendpi = "normal";
        } else if (screenwidth == 640) {
            if (deviceModel.indexOf("4") != -1) {
                screendpi = "normal";
            } else {
                screendpi = "retina";
            }
        } else if (screenwidth == 750) {
            screendpi = "retina";
        } else if (screenwidth == 1242) {
            screendpi = "iphone3x";
        }
    } else if (deviceName == "android") {
        if (screenwidth >= 720 && screenwidth <= 1080) {
            screendpi = "xhdpi";
        } else if (screenwidth > 1080) {
            screendpi = "xhdpi";
        } else if (screenwidth >= 480 && screenwidth < 720) {
            screendpi = "hdpi";
        } else if (screenwidth >= 320 && screenwidth < 480) {
            screendpi = "normal";
        }
    }
    screendpiservice = screendpi;
}

function onClickOfLaterFrmRegAnyIdAnnouncement() {
    displayAnnoucementtoUser = false;
    frmAccountSummaryLanding.show();
    // dismiss the frmRegAnyIdAnnouncement and continue to show account summary page.
}

function onClickOfRegNowFrmRegAnyIdAnnouncement() {
    displayAnnoucementtoUser = false;
    onClickSetUserID();
    // dismiss the frmRegAnyIdAnnouncement and Any Id registration page.
}

function onClickSetUrIdMyProfile() {
    if (checkMBUserStatus()) {
        frmMBAnyIdRegTnC.show();
    }
}

function onClickAnyIDOTPConfirm() {
    showLoadingScreen();
    var inputParam = {};
    var otp = popupTractPwd.txtOTP.text;
    var anyIDMBActNum = frmMBActivateAnyId.lblAccountNum.text;
    if (anyIDMBActNum != null) anyIDMBActNum = anyIDMBActNum.replace(/-/g, '');
    else anyIDMBActNum = "";
    var anyIDCIActNum = frmMBActivateAnyId.lblCIActNum.text;
    if (anyIDCIActNum != null) anyIDCIActNum = anyIDCIActNum.replace(/-/g, '');
    else anyIDCIActNum = "";
    inputParam["password"] = otp;
    inputParam["notificationAdd_appID"] = appConfig.appId;
    inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
    inputParam["verifyToken_retryCounterVerifyAccessPin"] = "0";
    inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
    inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
    //For Mobile Registration
    inputParam["mobileActNum"] = anyIDMBActNum;
    //for CI Registration
    inputParam["CIActNum"] = anyIDCIActNum;
    /*
    if((gblDeviceInfo["name"] == "iPhone" && frmMBActivateAnyId.switchSocialID.selectedIndex==1)||
    	(gblDeviceInfo["name"] == "android" && frmMBActivateAnyId.btnCIChckBox.skin==btnCheck))
    	 inputParam["CIActNum"] = "";
    	 
     if((gblDeviceInfo["name"] == "android" && frmMBActivateAnyId.btnPhCheckBox.skin==btnCheck) ||
     	(gblDeviceInfo["name"] == "iPhone" && frmMBActivateAnyId.switchMobile.selectedIndex==1))
    	 inputParam["mobileActNum"] = "";
    */
    invokeServiceSecureAsync("anyIDRegisterComposite", inputParam, callBackAnyIDCompJavaServiceMB);
}

function callBackAnyIDCompJavaServiceMB(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            popupTractPwd.dismiss();
            //businesshours check
            if (resulttable["registerAnyIdBusinessHrsFlag"] == "false") {
                var startTime = resulttable["regAnyIdStartTime"];
                var endTime = resulttable["regAnyIdEndTime"];
                var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
                messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
                showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
                return false;
            }
            //for registration
            var CIRegisterResult = "",
                MobileRegisterResult = "",
                isSuccess = false;
            CIRegisterResult = resulttable["opstatusCIRegistration"];
            MobileRegisterResult = resulttable["opstatusMobileRegistration"];
            if (undefined != CIRegisterResult && undefined != MobileRegisterResult) {
                if (CIRegisterResult == "0" && MobileRegisterResult == "0") {
                    isSuccess = true;
                }
            } else if (undefined != CIRegisterResult) {
                if (CIRegisterResult == "0") {
                    isSuccess = true;
                }
            } else if (undefined != MobileRegisterResult) {
                if (MobileRegisterResult == "0") {
                    isSuccess = true;
                }
            }
            //for de-registration
            var CIDeRegisterResult = "",
                MobileDeRegisterResult = "",
                isModify = false;
            CIDeRegisterResult = resulttable["opstatusCIDeRegistration"];
            MobileDeRegisterResult = resulttable["opstatusMobileDeRegistration"];
            if (undefined != CIDeRegisterResult && undefined != MobileDeRegisterResult) {
                if (CIDeRegisterResult == "0" && MobileDeRegisterResult == "0") {
                    isSuccess = true;
                }
            } else if (undefined != CIDeRegisterResult) {
                if (CIDeRegisterResult == "0") {
                    isSuccess = true;
                }
            } else if (undefined != MobileDeRegisterResult) {
                if (MobileDeRegisterResult == "0") {
                    isSuccess = true;
                }
            }
            //for Modify account
            var CIDeModifyResult = "",
                MobileModifyResult = "";
            CIDeModifyResult = resulttable["opstatusCIActModify"];
            MobileModifyResult = resulttable["opstatusMobileActModify"];
            if (undefined != CIDeModifyResult && undefined != MobileModifyResult) {
                if (CIDeModifyResult == "0" && MobileModifyResult == "0") {
                    isSuccess = true;
                }
            } else if (undefined != CIDeModifyResult) {
                if (CIDeModifyResult == "0") {
                    isSuccess = true;
                }
            } else if (undefined != MobileModifyResult) {
                if (MobileModifyResult == "0") {
                    isSuccess = true;
                }
            }
            if (isSuccess) {
                frmMBAnyIdRegCompleted.lblAnyIdFailure.setVisibility(false);
                frmMBAnyIdRegCompleted.hbxSuccess.setVisibility(true);
                frmMBAnyIdRegCompleted.hbxFail.setVisibility(false);
                if (frmMBAnyIdRegCompleted.lblAnyIdSuccess.isVisible) {
                    //Based on response value of "regAnyIdWithITMX" we have to hide show the below 3 success messages.
                    frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(false); // after Launch with ITMX
                    frmMBAnyIdRegCompleted.lblAnyIdSuccess1stLaunch.setVisibility(false); // something Temp
                    frmMBAnyIdRegCompleted.lblAnyIdSuccess2ndLaunch.setVisibility(false); // for Main Launch before ITMX
                    var regAnyIdWithITMX = resulttable["regAnyIdWithITMX"] != undefined ? resulttable["regAnyIdWithITMX"] : "";
                    regAnyIdWithITMX = regAnyIdWithITMX.toUpperCase();
                    if ("OFF" == regAnyIdWithITMX) {
                        frmMBAnyIdRegCompleted.lblAnyIdSuccess2ndLaunch.setVisibility(true);
                    } else if ("ON" == regAnyIdWithITMX) {
                        frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(true);
                    } else {
                        frmMBAnyIdRegCompleted.lblAnyIdSuccess1stLaunch.setVisibility(true);
                    }
                } else {
                    frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(false);
                    frmMBAnyIdRegCompleted.lblAnyIdSuccess1stLaunch.setVisibility(false);
                    frmMBAnyIdRegCompleted.lblAnyIdSuccess2ndLaunch.setVisibility(false);
                }
                gblRetryCountRequestOTP = "0";
                gblLocalfromDeregister = false;
                frmMBAnyIdRegCompleted.show();
            }
            /*else if(isModify){
            				frmMBAnyIdRegCompleted.hbxSuccess.setVisibility(false);
            				frmMBAnyIdRegCompleted.hbxFail.setVisibility(true);
            				frmMBAnyIdRegCompleted.lblAnyIdFailure.setVisibility(true);
            				frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(false);
            				frmMBAnyIdRegCompleted.lblAnyIdSuccess1stLaunch.setVisibility(false);
            				frmMBAnyIdRegCompleted.lblAnyIdSuccess2ndLaunch.setVisibility(false);
            				frmMBAnyIdRegCompleted.lblAnyIdSuccessEdit.setVisibility(false);
            				frmMBAnyIdRegCompleted.show();
            		}*/
            else {
                frmMBAnyIdRegCompleted.hbxSuccess.setVisibility(false);
                frmMBAnyIdRegCompleted.hbxFail.setVisibility(true);
                frmMBAnyIdRegCompleted.lblAnyIdFailure.setVisibility(true);
                frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(false);
                frmMBAnyIdRegCompleted.lblAnyIdSuccess1stLaunch.setVisibility(false);
                frmMBAnyIdRegCompleted.lblAnyIdSuccess2ndLaunch.setVisibility(false);
                frmMBAnyIdRegCompleted.lblAnyIdSuccessEdit.setVisibility(false);
                //var messageUnavailable = kony.i18n.getLocalizedString("keyOpenActGenErr");
                //showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),function(){frmMBAnyIdRegCompleted.show();});
                var messageUnavailable = kony.i18n.getLocalizedString("keyOpenActGenErr");
                var registerCIITMXCode = (undefined != resulttable["registerCIITMXCode"]) ? resulttable["registerCIITMXCode"] : "";
                var registerMobileITMXCode = (undefined != resulttable["registerMobileITMXCode"]) ? resulttable["registerMobileITMXCode"] : "";
                if (registerMobileITMXCode == "800" && registerCIITMXCode == "800") messageUnavailable = kony.i18n.getLocalizedString("MIB_P2PErr_RejBoth");
                else if (registerMobileITMXCode == "800") messageUnavailable = kony.i18n.getLocalizedString("MIB_P2PErr_RejMobile");
                else if (registerCIITMXCode == "800") messageUnavailable = kony.i18n.getLocalizedString("MIB_P2PErr_RejCI");
                gblLocalfromDeregister = false;
                showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"), function() {
                    frmMBAnyIdRegCompleted.show();
                });
            }
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("wrongOTP");
                popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
                popupTractPwd.lblPopupTract7.setVisibility(true);
                popupTractPwd.lblPopupTract2.text = "";
                popupTractPwd.lblPopupTract4.text = "";
                popupTractPwd.txtOTP.text = "";
                kony.application.dismissLoadingScreen();
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                showTranPwdLockedPopup();
                //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
                showTranPwdLockedPopup();
                popupTractPwd.dismiss();
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            alert("" + resulttable["errMsg"]);
            popupTractPwd.dismiss();
            gblLocalfromDeregister = false;
            frmMBAnyIdRegFail.show();
        }
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
            gblLocalfromDeregister = false;
            frmMBAnyIdRegFail.show();
        }
    }
}

function setAgreeBtnText() {
    if (agreeButton) {
        frmMBActivateAnyId.btnNext.text = kony.i18n.getLocalizedString("keyAgreeButton");
        frmMBActivateAnyId.btnNext.skin = btnBlueSkin;
        frmMBActivateAnyId.btnNext.focusSkin = btnBlueSkin;
        frmMBActivateAnyId.btnNext.setEnabled(true);
        frmMBActivateAnyId.btnNext.onClick = onClickAnyIDRegRequestOtp;
        frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(true);
        //frmMBAnyIdRegCompleted.lblAnyIdSuccessAdditional.setVisibility(true);
        frmMBAnyIdRegCompleted.lblAnyIdSuccessEdit.setVisibility(false);
        //frmMBActivateAnyId.hbox508341360713257.padding=[0,0,5,12];
        frmMBActivateAnyId.hbox508341360713257.setVisibility(true);
    } else {
        frmMBActivateAnyId.btnNext.text = kony.i18n.getLocalizedString("Next");
        frmMBActivateAnyId.btnNext.skin = btnDisabledGray;
        frmMBActivateAnyId.btnNext.focusSkin = btnDisabledGray;
        frmMBActivateAnyId.btnNext.setEnabled(false);
        frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
        frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(false);
        //frmMBAnyIdRegCompleted.lblAnyIdSuccessAdditional.setVisibility(false);
        frmMBAnyIdRegCompleted.lblAnyIdSuccessEdit.setVisibility(true);
    }
    /*if(singleAct && !agreeButton)
    {
    	frmMBActivateAnyId.btnNext.text=kony.i18n.getLocalizedString("Next");
    	frmMBActivateAnyId.btnNext.skin=btnBlueSkin;
    	frmMBActivateAnyId.btnNext.focusSkin=btnBlueSkin;
    	frmMBActivateAnyId.btnNext.setEnabled(true);
    	frmMBActivateAnyId.btnNext.onClick=onClickAnyIDRegRequestOtp;
    	frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
    	frmMBAnyIdRegCompleted.lblAnyIdSuccess.setVisibility(false);
    	frmMBAnyIdRegCompleted.lblAnyIdSuccessAdditional.setVisibility(false);
    	frmMBAnyIdRegCompleted.lblAnyIdSuccessEdit.setVisibility(true);
    }*/
    if (isMBDeregister || isCIDeregister || userMBChanged || userCIChanged) {
        frmMBActivateAnyId.btnNext.skin = btnBlueSkin;
        frmMBActivateAnyId.btnNext.focusSkin = btnBlueSkin;
        frmMBActivateAnyId.btnNext.setEnabled(true);
        frmMBActivateAnyId.btnNext.onClick = onClickAnyIDRegRequestOtp;
        //frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
    }
}

function frmMBAnyIdRegTnCPreShow() {
    changeStatusBarColor();
    frmMBAnyIdRegTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("MIB_P2PPrdBri");
    frmMBAnyIdRegTnC.btnCancelCon.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmMBAnyIdRegTnC.btnNext.text = kony.i18n.getLocalizedString("Next");
    getAnyIDBriefImage();
}

function frmMBAnyIDSelectActsPreShow() {
    changeStatusBarColor();
    changeStatusBarColor();
    frmMBAnyIDSelectActs.btnAnyID.text = kony.i18n.getLocalizedString("Back");
    frmMBAnyIDSelectActs.lblHdrTxtList.text = kony.i18n.getLocalizedString("MIB_P2PAcct");
    showCommonTMBAct();
}

function showAgreeORNext() {
    if (isMBRegister || isCIRegister) agreeButton = true;
    else agreeButton = false;
    setAgreeBtnText();
}

function selectMobileCheckBOx() {
    fromTnC = false;
    if (frmMBActivateAnyId.btnPhCheckBox.skin != btnCheckFoc) {
        if (gblMBOtherBankLinked) {
            deRegisterPopUP();
        } else {
            if (singleAct) singleActMB();
            else showTMBAccounts();
        }
    } else {
        //userMBChanged=true;
        gblMBActsForLocaleChange = [];
        frmMBActivateAnyId.lblAccountNum.text = "";
        frmMBActivateAnyId.btnPhCheckBox.skin = btnCheck;
        frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
        frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
        if (gblServiceMBAct == "" && gblServiceMBAct == frmMBActivateAnyId.lblAccountNum.text) isMBRegister = false;
    }
    if (frmMBActivateAnyId.lblAccountNum.text == "" && gblServiceMBAct.trim() != "") isMBDeregister = true;
    else isMBDeregister = false;
    showAgreeORNext();
}

function selectCICheckBOx() {
    fromTnC = false;
    if (frmMBActivateAnyId.btnCIChckBox.skin != btnCheckFoc) {
        if (gblCIOtherBankLinked) {
            frmMBActivateAnyId.btnCIChckBox.skin = btnCheck;
            cideRegisterPopUP(); //showAlert(kony.i18n.getLocalizedString("MIB_P2PErr_LinkOtherCitizenID"), kony.i18n.getLocalizedString("info"));
        } else {
            if (singleAct) singleActCI();
            else showCITMBAccounts();
        }
    } else {
        //userCIChanged=true;
        gblCIActsForLocaleChange = [];
        frmMBActivateAnyId.lblCIActNum.text = "";
        frmMBActivateAnyId.btnCIChckBox.skin = btnCheck;
        frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
        frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
        if (gblServiceCIAct == "" && gblServiceCIAct == frmMBActivateAnyId.lblCIActNum.text) isCIRegister = false;
    }
    if (frmMBActivateAnyId.lblCIActNum.text == "" && gblServiceCIAct.trim() != "") isCIDeregister = true;
    else isCIDeregister = false;
    showAgreeORNext();
}

function onSlideMobileNum() {
    fromTnC = false;
    var selectedKey = frmMBActivateAnyId.switchMobile.selectedIndex;
    if (selectedKey == 0) { //ON
        if (gblMBOtherBankLinked) {
            frmMBActivateAnyId.switchMobile.selectedIndex = 1;
            deRegisterPopUP();
        } else {
            if (singleAct) singleActMB();
            else showTMBAccounts();
        }
    } else {
        //userMBChanged=true;
        gblMBActsForLocaleChange = [];
        frmMBActivateAnyId.lblAccountNum.text = "";
        frmMBActivateAnyId.switchMobile.selectedIndex = 1;
        frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
        frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
        if (gblServiceMBAct == "" && gblServiceMBAct == frmMBActivateAnyId.lblAccountNum.text) isMBRegister = false;
    }
    if (frmMBActivateAnyId.lblAccountNum.text == "" && gblServiceMBAct.trim() != "") isMBDeregister = true;
    else isMBDeregister = false;
    showAgreeORNext();
}

function onSlideCitizenID() {
    fromTnC = false;
    var selectedKey = frmMBActivateAnyId.switchSocialID.selectedIndex;
    if (selectedKey == 0) { //ON
        if (gblCIOtherBankLinked) {
            frmMBActivateAnyId.switchSocialID.selectedIndex = 1;
            cideRegisterPopUP();
        } else {
            if (singleAct) singleActCI();
            else showCITMBAccounts();
        }
    } else {
        //userCIChanged=true;
        frmMBActivateAnyId.lblCIActNum.text = "";
        gblCIActsForLocaleChange = [];
        frmMBActivateAnyId.switchSocialID.selectedIndex = 1;
        frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
        frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
        if (gblServiceCIAct == "" && gblServiceCIAct == frmMBActivateAnyId.lblCIActNum.text) isCIRegister = false;
    }
    if (frmMBActivateAnyId.lblCIActNum.text == "" && gblServiceCIAct.trim() != "") isCIDeregister = true;
    else isCIDeregister = false;
    showAgreeORNext();
}

function getAnyIDBriefImage() {
    var anyIDProdFeature_image_name_EN = "AnyIdIntroEN";
    var anyIDProdFeature_image_name_TH = "AnyIdIntroTH";
    if (anyIDProdFeature_image_EN == "") anyIDProdFeature_image_EN = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + anyIDProdFeature_image_name_EN + "&modIdentifier=PRODUCTPACKAGEIMG";
    if (anyIDProdFeature_image_TH == "") anyIDProdFeature_image_TH = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + anyIDProdFeature_image_name_TH + "&modIdentifier=PRODUCTPACKAGEIMG";
    if (kony.i18n.getCurrentLocale() == "th_TH") frmMBAnyIdRegTnC.imgTransNBpAckComplete.src = anyIDProdFeature_image_TH;
    else frmMBAnyIdRegTnC.imgTransNBpAckComplete.src = anyIDProdFeature_image_EN;
}

function callAnyIDInq() {
    showLoadingScreen();
    var inputParams = {};
    invokeServiceSecureAsync("AnyIDInqComposite", inputParams, anyIDInqCallBack);
}

function anyIDInqCallBack(status, resulttable) {
    //	if (status == 400) 
    //{
    clearGblVariable();
    if (resulttable != null) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            if (resulttable["registerAnyIdBusinessHrsFlag"] != null) {
                if (resulttable["registerAnyIdBusinessHrsFlag"] == "false") {
                    var startTime = resulttable["regAnyIdStartTime"];
                    var endTime = resulttable["regAnyIdEndTime"];
                    var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
                    messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
                    messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
                    showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
                    return false;
                }
            }
            /*if(resulttable["responseCode"]!=null)
            {
            	responseCode=resulttable["responseCode"];
            	if(responseCode!="000")
            	{
            		frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
            		 frmMBActivateAnyId.line508341360728137.setVisibility(false);
            		 gblMBTMBLinked=false;
            		 gblServiceMBAct="";
            		 gblServiceCIAct="";
            		 frmMBActivateAnyId.lblCIActNum.text="";
            		 frmMBActivateAnyId.lblAccountNum.text="";
            		 if(gblDeviceInfo["name"] == "android"){
            			frmMBActivateAnyId.btnPhCheckBox.skin=btnCheck;
            			gblMBAndroidSrvVal="btnCheck";
            			}
            		 if(gblDeviceInfo["name"] == "iPhone"){
            			frmMBActivateAnyId.switchMobile.selectedIndex=1;
            			gblMBiPhoneSrvVal=1;
            			}
            	}
            }*/
            if (resulttable.MobileDS != null && resulttable.MobileDS.length > 0) {
                if (undefined != resulttable.MobileDS[0].acctIdentValue) {
                    var mbactID = resulttable.MobileDS[0].acctIdentValue;
                    gblMBActsForLocaleChange = displayTMBMBAct(mbactID);
                }
                var mbiTMXFlag = resulttable.MobileDS[0].iTMXFlag;
                var mbonUSFlag = resulttable.MobileDS[0].onUSFlag;
                gblMBLinkOtherBankLangStr = resulttable.MobileDS[0];
                if (mbiTMXFlag == "Y" && mbonUSFlag == "Y") {
                    gblMBOtherBankLinked = false;
                    fromTnC = false;
                    gblMBTMBLinked = true;
                    gblServiceMBAct = mbactID;
                    //gblselectedChckBox="PhoneCheckBox"
                    frmMBActivateAnyId.lblDeRegister.text = "";
                    frmMBActivateAnyId.lblAccountNum.text = formatAccountNo(mbactID);
                    frmMBActivateAnyId.imgNav.setVisibility(true);
                    if (singleAct) frmMBActivateAnyId.imgNav.setVisibility(false);
                    if (kony.i18n.getCurrentLocale() == "th_TH") {
                        frmMBActivateAnyId.lblname.text = gblMBActsForLocaleChange["lblAccountNameTH"];
                        //gblCIAccountsTH = gblMBActsForLocaleChange["lblAccountNameTH"];
                    } else {
                        frmMBActivateAnyId.lblname.text = gblMBActsForLocaleChange["lblAccountNameEN"];
                        //gblCIAccountsEN = gblCIActsForLocaleChange["lblAccountNameEN"];
                    }
                    frmMBActivateAnyId.imgacnt.src = gblMBActsForLocaleChange["imgAccountPicture"];
                    frmMBActivateAnyId.hbxPhoneAccount.onClick = showTMBAccounts;
                    frmMBActivateAnyId.lblTxtPhone.text = kony.i18n.getLocalizedString("MIB_P2PRegisMobInfo");
                    frmMBActivateAnyId.lblTxtPhone.skin = lblBlueDBOzone128;
                    frmMBActivateAnyId.hbxPhoneAccount.setVisibility(true);
                    if (frmMBActivateAnyId.lblAccountNum.text == "") {
                        frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
                        frmMBActivateAnyId.line508341360728137.setVisibility(false);
                    }
                    /*
                    if(gblDeviceInfo["name"] == "android" && frmMBActivateAnyId.lblAccountNum.text!=""){
                    	frmMBActivateAnyId.btnPhCheckBox.skin=btnCheckFoc;
                    	gblMBAndroidSrvVal=btnCheckFoc;
                    	}
                    else if(gblDeviceInfo["name"] == "android" && frmMBActivateAnyId.lblAccountNum.text==""){
                    	frmMBActivateAnyId.btnPhCheckBox.skin=btnCheck;
                    	gblMBAndroidSrvVal=btnCheck;
                    	}
                    	
                    if(gblDeviceInfo["name"] == "iPhone" && frmMBActivateAnyId.lblAccountNum.text!=""){
                    	frmMBActivateAnyId.switchMobile.selectedIndex=0;
                    	gblMBiPhoneSrvVal=0;
                    	}
                    else if(gblDeviceInfo["name"] == "iPhone" && frmMBActivateAnyId.lblAccountNum.text==""){
                    	frmMBActivateAnyId.switchMobile.selectedIndex=1;
                    	gblMBiPhoneSrvVal=1;
                    	}
                    */
                }
                if (mbiTMXFlag == "Y" && mbonUSFlag == "N") {
                    gblMBOtherBankLinked = true;
                    gblMBTMBLinked = false;
                    gblMBAndroidSrvVal = btnCheck;
                    gblMBActsForLocaleChange = [];
                    gblServiceMBAct = "";
                    frmMBActivateAnyId.lblDeRegister.text = kony.i18n.getLocalizedString("MIB_P2PHowtoMobileNo");
                    frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
                    var phoneDeRegTxt = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankMobileNo");
                    var res = phoneDeRegTxt.replace("<Bank short name", resulttable.MobileDS[0].bankShortName);
                    if (kony.i18n.getCurrentLocale() == "th_TH") var res = res.replace("Bank name>", resulttable.MobileDS[0].bankNameTH);
                    else var res = res.replace("Bank name>", resulttable.MobileDS[0].bankNameEN);
                    frmMBActivateAnyId.lblTxtPhone.text = res;
                    frmMBActivateAnyId.lblTxtPhone.skin = lblRedNormal;
                }
                if (mbiTMXFlag == "N") {
                    gblMBOtherBankLinked = false;
                    gblMBTMBLinked = false;
                    gblMBActsForLocaleChange = [];
                    gblServiceMBAct = "";
                    gblMBiPhoneSrvVal = 1;
                    gblMBAndroidSrvVal = btnCheck;
                    frmMBActivateAnyId.lblTxtPhone.text = kony.i18n.getLocalizedString("MIB_P2PRegisMobInfo");
                    frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
                    frmMBActivateAnyId.lblDeRegister.text = "";
                    frmMBActivateAnyId.lblAccountNum.text = "";
                    //frmMBActivateAnyId.lblTxtPhone.skin=lblBlue80;
                    frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
                }
            } else {
                gblMBOtherBankLinked = false;
                gblMBTMBLinked = false;
                gblServiceMBAct = "";
                frmMBActivateAnyId.hbxPhoneAccount.setVisibility(false);
            }
            //For Thai Citizen--start
            if (resulttable.CIDS != null && resulttable.CIDS.length > 0) {
                var ciactID = resulttable.CIDS[0].acctIdentValue;
                var ciiTMXFlag = resulttable.CIDS[0].iTMXFlag;
                var cionUSFlag = resulttable.CIDS[0].onUSFlag;
                gblCIActsForLocaleChange = displayTMBMBAct(ciactID)
                gblLinkOtherBankLangStr = resulttable.CIDS[0];;
                if (ciiTMXFlag == "Y" && cionUSFlag == "Y") {
                    fromTnC = false;
                    gblCIOtherBankLinked = false;
                    gblCITMBLinked = true;
                    frmMBActivateAnyId.hboxSocialAccount.setVisibility(true);
                    frmMBActivateAnyId.image2508341360639218.setVisibility(true);
                    frmMBActivateAnyId.hbox508341360349300.setVisibility(true);
                    frmMBActivateAnyId.label1504947934171198.text = "";
                    gblServiceCIAct = ciactID;
                    if (singleAct) frmMBActivateAnyId.image2508341360639218.setVisibility(false);
                    frmMBActivateAnyId.lblCIActNum.text = formatAccountNo(ciactID);
                    if (kony.i18n.getCurrentLocale() == "th_TH") {
                        frmMBActivateAnyId.lblCIActName.text = gblCIActsForLocaleChange["lblAccountNameTH"];
                    } else {
                        frmMBActivateAnyId.lblCIActName.text = gblCIActsForLocaleChange["lblAccountNameEN"];
                    }
                    frmMBActivateAnyId.imgCI.src = gblCIActsForLocaleChange["imgAccountPicture"];
                    frmMBActivateAnyId.hboxSocialAccount.onClick = showCITMBAccounts;
                    frmMBActivateAnyId.hbox508341360540829.setVisibility(true);
                    frmMBActivateAnyId.lblTxtCI.text = kony.i18n.getLocalizedString("MIB_P2PRegisCIInfo");
                    frmMBActivateAnyId.lblTxtCI.skin = lblBlueDBOzone128;
                    if (frmMBActivateAnyId.lblCIActNum.text == "") {
                        frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
                        frmMBActivateAnyId.line508341360728137.setVisibility(false);
                    }
                    /*
                    if(gblDeviceInfo["name"] == "android" && frmMBActivateAnyId.lblCIActNum.text!=""){
                    	frmMBActivateAnyId.btnCIChckBox.skin=btnCheckFoc;
                    	gblCIAndroidSrvVal=btnCheckFoc;
                    	}
                    else if(gblDeviceInfo["name"] == "android" && frmMBActivateAnyId.lblCIActNum.text==""){
                    	frmMBActivateAnyId.btnCIChckBox.skin=btnCheck;
                    	gblCIAndroidSrvVal=btnCheck;
                    	}
                    	
                    if(gblDeviceInfo["name"] == "iPhone" && frmMBActivateAnyId.lblCIActNum.text!=""){
                    	frmMBActivateAnyId.switchSocialID.selectedIndex=0;
                    	gblCIiPhoneSrvVal=0;
                    	}
                    else if(gblDeviceInfo["name"] == "iPhone" && frmMBActivateAnyId.lblCIActNum.text==""){
                    	frmMBActivateAnyId.switchSocialID.selectedIndex=1;
                    	gblCIiPhoneSrvVal=1;
                    	}
                    */
                }
                if (ciiTMXFlag == "Y" && cionUSFlag == "N") {
                    gblCIOtherBankLinked = true;
                    gblCIAndroidSrvVal = btnCheck;
                    gblServiceCIAct = "";
                    gblCITMBLinked = false;
                    gblCIActsForLocaleChange = [];
                    frmMBActivateAnyId.label1504947934171198.text = kony.i18n.getLocalizedString("MIB_P2PHowtoCitizenID");
                    var CIDeRegTxt = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankCitizenID");
                    var res = CIDeRegTxt.replace("<Bank short name", resulttable.CIDS[0].bankShortName);
                    if (kony.i18n.getCurrentLocale() == "th_TH") var res = res.replace("Bank name>", resulttable.CIDS[0].bankNameTH);
                    else var res = res.replace("Bank name>", resulttable.CIDS[0].bankNameEN);
                    frmMBActivateAnyId.lblTxtCI.text = res;
                    frmMBActivateAnyId.lblTxtCI.skin = lblRedNormal;
                    frmMBActivateAnyId.hbox508341360540829.margin = [3, 2, 0, 0];
                    frmMBActivateAnyId.lblCIActNum.text = ""
                    frmMBActivateAnyId.lblTxtCI.margin = [0, 0, 0, 0];
                    frmMBActivateAnyId.hbox508341360540829.padding = [0, 0, 1, 0];
                    frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
                    frmMBActivateAnyId.hboxTnC.padding = [0, 0, 10, 0];
                }
                if (ciiTMXFlag == "N") {
                    gblCIOtherBankLinked = false;
                    gblCITMBLinked = false;
                    gblCIActsForLocaleChange = [];
                    gblServiceCIAct = "";
                    gblCIiPhoneSrvVal = 1;
                    gblCIAndroidSrvVal = btnCheck;
                    frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
                    frmMBActivateAnyId.label1504947934171198.text = "";
                    frmMBActivateAnyId.lblCIActNum.text = "";
                    //frmMBActivateAnyId.btnCIChckBox.skin=btnCheck;
                }
            } else {
                frmMBActivateAnyId.lblTxtCI.skin = lblBlueDBOzone128;
                gblCIOtherBankLinked = false;
                gblCITMBLinked = false;
                gblServiceCIAct = "";
                frmMBActivateAnyId.hboxSocialAccount.setVisibility(false);
            }
            //For Thai Citizen---end
            fromTnC = false;
            frmMBActivateAnyId.btnNext.text = kony.i18n.getLocalizedString("Next");
            frmMBActivateAnyId.btnNext.skin = btnDisabledGray;
            frmMBActivateAnyId.btnNext.focusSkin = btnDisabledGray;
            frmMBActivateAnyId.btnNext.setEnabled(false);
            frmMBActivateAnyId.btnNext.onClick = disableBackButton;
            frmMBActivateAnyId.hbox508341360713257.setVisibility(false);
            //if((resulttable.MobileDS!=null && resulttable.MobileDS.length>0) || (resulttable.CIDS!=null && resulttable.CIDS.length>0)) 
            frmMBActivateAnyId.show();
        } else {
            dismissLoadingScreen();
            if (resulttable["opstatus"] == 1) showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
    }
    //}
}

function checkBusinessHours() {
    var inputParam = {};
    invokeServiceSecureAsync("anyIDBusinessHrsCheck", inputParam, anyIDEditProfileBusinessHrsChkCallback);
}

function anyIDEditProfileBusinessHrsChkCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            //for edit profile
            kony.print("inside anyIDBusinessHrsCheckJavaServiceCallback------->" + JSON.stringify(callBackResponse));
            if (callBackResponse["s2sBusinessHrsFlag"] != null) {
                s2sBusinessHrsFlag = callBackResponse["s2sBusinessHrsFlag"];
            }
            if (callBackResponse["s2sStartTime"] != null) {
                s2sStartTime = callBackResponse["s2sStartTime"];
            }
            if (callBackResponse["s2sEndTime"] != null) {
                s2sEndTime = callBackResponse["s2sEndTime"];
            }
            if (s2sBusinessHrsFlag == "false") {
                showAlert(getErrorMsgForBizHrs(), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
}

function checkAnyIDEditProfile() {
    checkBusinessHours();
    if (checkMBUserStatus()) {
        if (s2sBusinessHrsFlag == "true") {
            gblMobNoTransLimitFlag = true;
            frmChangeMobNoTransLimitMB.show();
        }
    }
}

function frmMBAnyIDRegAcceptTnCLocale() {
    changeStatusBarColor();
    frmMBAnyIDRegAcceptTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyTermsNConditions");
}

function clearGblVariable() {
    isMBRegister = false;
    isCIRegister = false;
    userMBChanged = false;
    userCIChanged = false;
    isMBDeregister = false;
    isCIDeregister = false;
    gblMBTMBLinked = false;
    gblCITMBLinked = false;
    gblMBOtherBankLinked = false;
    gblCIOtherBankLinked = false;
    gblServiceMBAct = "";
    gblServiceCIAct = "";
}