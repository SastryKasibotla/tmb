function encryptForDPAndroid(cleartext) {
    var e2eeDPResult = E2EEEncryptDP.encryptDP("", cleartext, gblDPPk, gblDPRandNumber);
    kony.print("E2EEDP Return Code = " + e2eeDPResult[1]);
    kony.print("E2EEDP RPIN = " + e2eeDPResult[0]);
    return e2eeDPResult[0];
}

function encryptForDPForIphone(cleartext) {
    var e2eeDPResult = E2EEEncryptDP.encryptDPFuncForiPhone(cleartext, gblDPPk, gblDPRandNumber);
    return e2eeDPResult;
}

function encryptForDPForDesktopWeb(cleartext) {}

function encryptDPUsingE2EE(cleartext) {
    var e2eeDPResult = "";
    if (gblDPPk != "" && gblDPRandNumber != "") {
        if (undefined != e2eeDPResult) {
            return e2eeDPResult;
        } else {
            //Error handling when encryption fail
            return "";
        }
    } else {
        //Error handling when init service failed
        return e2eeDPResult;
    }
}