loginFlow = false;
/*function callbackSPAMockTesting(status, result) {
	if (status == 400) {
		
		//frmTransferLanding.show();
		getTransferFromAccounts();
	}
}*/
function spaCheck() {
    //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
        flowSpa = true;
    } else {
        flowSpa = false;
    }
}

function captchaValidationSpa(text) {
    var pat1 = /^[a-z0-9]+$/;
    var isMatching = pat1.test(text);
    if ((!isMatching) || (text.length != 6)) {
        alert(kony.i18n.getLocalizedString("msgIBCaptchaWrong"));
        return false;
    } else {
        MBSpaLoginService();
        return true;
    }
}

function MBSpaLoginService(uniqueId) {
    if (flowSpa) {
        //showLoadingScreen(); //fix for DEF1075 in this DEF92 has been fixed.
        //UAT DEF92 hot fix issue where the loading screen was not dismissing
        showLoadingScreenWithNoIndicator();
        MBSpauserID = frmSPALogin.txtUserId.text
        var inputParam = {};
        inputParam["loginId"] = kony.string.trim(frmSPALogin.txtUserId.text);
        inputParam["password"] = kony.string.trim(frmSPALogin.txtPassword.text);
        inputParam["userid"] = kony.string.trim(frmSPALogin.txtUserId.text);
        gblUserName = frmSPALogin.txtUserId.text;
        if (frmSPALogin.hboxCaptchaText.isVisible) inputParam["capchaID"] = frmSPALogin.txtCaptchaText.text;
        gblLoginType = "Login";
        invokeServiceSecureAsync("IBVerifyLoginEligibility", inputParam, callBackMBSpaLoginService);
    } else {
        var inputParam = {};
        inputParam["deviceId"] = uniqueId;
        inputParam["loginId"] = kony.string.trim(frmMBActivationIBLogin.txtUserId.text);
        inputParam["password"] = kony.string.trim(frmMBActivationIBLogin.txtPassword.text);
        inputParam["userid"] = kony.string.trim(frmMBActivationIBLogin.txtUserId.text);
        if (frmMBActivationIBLogin.hboxCaptchaText.isVisible) inputParam["capchaID"] = frmMBActivationIBLogin.txtCaptchaText.text;
        showLoadingScreen();
        invokeServiceSecureAsync("IBVerifyLoginEligibility", inputParam, callBackMBActivationViaIB);
    }
}

function callBackMBActivationViaIB(status, resulttable) {
    if (status == 400) {
        frmMBActivationIBLogin.txtPassword.text = "";
        if (resulttable["reactivationFlow"] == "true") { //
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("resetPwdCorrectCodeInActivationMB"), kony.i18n.getLocalizedString("info"));
            return;
        } else if (resulttable["reactivationFlow"] == "false") {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyTknErr00003" || resulttable["errCode"] == "VrfyOTPErr00002") {
                showAlertWithCallBack(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"), calBackLockAlert);
            } else {
                alert(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
            }
            if (isNotBlank(resulttable["captchaCode"])) {
                if (resulttable["captchaCode"] == "1") {
                    frmMBActivationIBLogin.hboxCaptcha.setVisibility(true);
                    frmMBActivationIBLogin.hboxCaptchaText.setVisibility(true);
                    frmMBActivationIBLogin.imgcaptcha.base64 = resulttable["captchaImage"];
                }
            } else {
                frmMBActivationIBLogin.hboxCaptcha.setVisibility(false);
                frmMBActivationIBLogin.hboxCaptchaText.setVisibility(false);
                frmMBActivationIBLogin.txtCaptchaText.text = "";
            }
            return;
        }
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == "statusDesc") {
                var errMessage = kony.i18n.getLocalizedString("crmerrMsg");
                alert(errMessage)
            } else if (resulttable["statusCode"] == "errMsg") {
                var errMessage = kony.i18n.getLocalizedString("errMsg");
                alert(errMessage)
            } else {
                if (resulttable.PhnNum != "") glbMobileNo = resulttable.PhnNum;
                gblCustomerName = resulttable.fullName;
                frmMBActivationIBLogin.hboxCaptcha.setVisibility(false);
                frmMBActivationIBLogin.hboxCaptchaText.setVisibility(false);
                dismissLoadingScreen();
                frmMBActivationConfirmMobile.lblMobileNo.text = glbMobileNo;
                frmMBActivationConfirmMobile.show();
                //LoginflowPartyInqServiceSPA(); //invokes party service for Customer Name
            }
        } else {
            // Start when opstatus not 0
            frmMBActivationIBLogin.txtPassword.text = "";
            if (resulttable["opstatus"] == 1) {
                alert(kony.i18n.getLocalizedString("keyNetworkError"), kony.i18n.getLocalizedString("info"));
                //loginFailUpdate();
                dismissLoadingScreen();
                return false;
            } else if (resulttable["opstatus"] == 8005) {
                if (resulttable["captchaCode"] == "0") {
                    frmMBActivationIBLogin.hboxCaptcha.setVisibility(false);
                    frmMBActivationIBLogin.hboxCaptchaText.setVisibility(false);
                }
                if (resulttable["faultdetail"]) {
                    if (resulttable["faultdetail"].match(/K000000300005/gi) != null) {
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError002"), kony.i18n.getLocalizedString("info"));
                        //alert("User is already logged in or Browser was force closed, Please try after 10 mins ");
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300001/gi) != null) {
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("User profile is not setup"); //any generall error
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/E10403/gi) != null) {
                        showAlertWithCallBack(kony.i18n.getLocalizedString("keyIBLoginError003"), kony.i18n.getLocalizedString("info"), calBackLockAlert);
                        //alert("Account blocked due to max failed attempts");//ECAS LOCKED
                        gblUserName = MBSpauserID;
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/E10020/gi) != null || resulttable["faultdetail"].match(/E10001/gi) != null) {
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("Either User ID or password is incorrect"); // wrong password scenario
                        //alert("Account does not exist"); to fix DEF3565
                        frmMBActivationIBLogin.txtCaptchaText.text = "";
                        if (resulttable["captchaCode"] == "1") {
                            frmMBActivationIBLogin.hboxCaptcha.setVisibility(true);
                            frmMBActivationIBLogin.hboxCaptchaText.setVisibility(true);
                            frmMBActivationIBLogin.imgcaptcha.base64 = resulttable["captchaImage"];
                        } else {
                            frmMBActivationIBLogin.hboxCaptcha.setVisibility(false);
                            frmMBActivationIBLogin.hboxCaptchaText.setVisibility(false);
                        }
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300009/gi) != null) {
                        //migrated user errors
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError009"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300010/gi) != null) {
                        // no crm id for userid
                        faultDetail = "K000000300010";
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError010"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300011/gi) != null) {
                        //cancelled by bank
                        faultDetail = "K000000300011";
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError011"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300012/gi) != null) {
                        //Temporary Suspended By Bank
                        faultDetail = "K000000300012";
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError012"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300013/gi) != null) {
                        // customer OTP locked
                        faultDetail = "K000000300013";
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError013"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/E10105/gi) != null) {
                        // Another kind of locked by ecas
                        showAlert(kony.i18n.getLocalizedString("keyIBLoginError004"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300007/gi) != null) {
                        // db error while saving login status table
                        showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else {
                        showAlert("System error :" + resulttable["faultdetail"]);
                        dismissLoadingScreen();
                        return false;
                    }
                } else {
                    // else if opstatus not 8005 also
                    dismissLoadingScreen();
                    if (resulttable["captchaCode"] == "1") {
                        showAlert(kony.i18n.getLocalizedString("msgIBCaptchaWrong"));
                        frmMBActivationIBLogin.hboxCaptcha.setVisibility(true);
                        frmMBActivationIBLogin.hboxCaptchaText.setVisibility(true);
                        frmMBActivationIBLogin.imgcaptcha.base64 = resulttable["captchaImage"];
                        frmMBActivationIBLogin.txtCaptchaText.text = "";
                    } else {
                        frmMBActivationIBLogin.hboxCaptcha.setVisibility(false);
                        frmMBActivationIBLogin.hboxCaptchaText.setVisibility(false);
                        frmMBActivationIBLogin.txtCaptchaText.text = "";
                    }
                    if (resulttable["errCode"] == "VrfyOTPCtrErr00001") {
                        //You have entered incorrect ActivationCode
                        showAlert(kony.i18n.getLocalizedString("ECVrfyACTErr00001"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        return false;
                    }
                }
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                dismissLoadingScreen();
                return false;
            }
        }
    }
}

function callBackMBSpaLoginService(status, resulttable) {
    if (status == 400) {
        gblSPAUsername = "";
        gblSPAPassword = "";
        if (resulttable["opstatus"] == 0) {
            // Reactivation Flow check good
            if (resulttable["reactivationFlow"] == "true") {
                dismissLoadingScreen();
                gblSetPwdSpa = true;
                gblSPAUsername = frmSPALogin.txtUserId.text;
                navigateToTandC();
                return;
            } else if (resulttable["reactivationFlow"] == "false") {
                // Reactivation Flow has errors
                dismissLoadingScreen();
                frmSPALogin.txtPassword.text = "";
                frmSPALogin.hboxCaptcha.setVisibility(false);
                frmSPALogin.hboxCaptchaText.setVisibility(false);
                frmSPALogin.txtCaptchaText.text = "";
                if (resulttable["errCode"] == "VrfyOTPErr00002") {
                    //You have reached max number of invalid attempts. Your account is locked. Please re-apply.
                    popupSpaForgotPassword.lblText.text = kony.i18n.getLocalizedString("ECVrfyActLockErr00002");
                    popupSpaForgotPassword.show();
                    return false;
                } else if (resulttable["errCode"] == "JavaErr00001") {
                    //Error occured. Please contact TMB helpdesk.
                    showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
                } else if (resulttable["errCode"] == "VrfyTknErr00003") {
                    //Activation Code expired for Reset Password / Unlock Otp
                    //showAlert(kony.i18n.getLocalizedString("ECVrfyActExpErr00003"), kony.i18n.getLocalizedString("info"));
                    popupSpaForgotPassword.lblText.text = kony.i18n.getLocalizedString("ECVrfyActExpErr00003");
                    popupSpaForgotPassword.show();
                    return false;
                } else {
                    //System Error
                    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
                return;
            } // Reactivation Flow verification Ends
            // If reach here means normal login flow
            if (resulttable["statusCode"] == "statusDesc") {
                var errMessage = kony.i18n.getLocalizedString("crmerrMsg");
                alert(errMessage)
            } else if (resulttable["statusCode"] == "errMsg") {
                var errMessage = kony.i18n.getLocalizedString("errMsg");
                alert(errMessage)
            } else {
                if (resulttable.mobNo != "") var glbMobileNo = resulttable.mobNo; //"865832721"; //hardcoading this has to replace with service output param from mibInquiry
                else glbMobileNo = "865832721";
                gblCustomerName = resulttable.fullName;
                frmSPALogin.hboxCaptcha.setVisibility(false);
                frmSPALogin.hboxCaptchaText.setVisibility(false);
                showLoadingScreen();
                LoginflowPartyInqServiceSPA(); //invokes party service for Customer Name
            }
            // End below of opstatus 0 case
        } else {
            // Start when opstatus not 0
            frmSPALogin.txtPassword.text = "";
            if (resulttable["opstatus"] == 1) {
                alert(kony.i18n.getLocalizedString("keyNetworkError"), kony.i18n.getLocalizedString("info"));
                //loginFailUpdate();
                dismissLoadingScreen();
                dismissLoadingScreen()
                return false;
            } else if (resulttable["opstatus"] == 8005) {
                if (resulttable["captchaCode"] == "0") {
                    frmSPALogin.hboxCaptcha.setVisibility(false);
                    frmSPALogin.hboxCaptchaText.setVisibility(false);
                }
                if (resulttable["faultdetail"]) {
                    if (resulttable["faultdetail"].match(/K000000300005/gi) != null) {
                        alert(kony.i18n.getLocalizedString("keyIBLoginError002"), kony.i18n.getLocalizedString("info"));
                        //alert("User is already logged in or Browser was force closed, Please try after sometime ");
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300001/gi) != null) {
                        alert(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("User profile is not setup");
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/E10001/gi) != null) {
                        alert(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("Account does not exist"); to fix DEF3565 
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/E10403/gi) != null) {
                        alert(kony.i18n.getLocalizedString("keyIBLoginError003"), kony.i18n.getLocalizedString("info"));
                        //alert("Account blocked due to max failed attempts");
                        gblUserName = MBSpauserID;
                        //BlockNotificationCrmProfileInq();
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/E10020/gi) != null) {
                        alert(kony.i18n.getLocalizedString("keyIBLoginError001"), kony.i18n.getLocalizedString("info"));
                        //alert("Either User ID or password is incorrect"); // wrong password scenario
                        if (resulttable["captchaCode"] == "1") {
                            frmSPALogin.hboxCaptcha.setVisibility(true);
                            frmSPALogin.hboxCaptchaText.setVisibility(true);
                            frmSPALogin.imgcaptcha.base64 = resulttable["captchaImage"];
                        } else {
                            frmSPALogin.hboxCaptcha.setVisibility(false);
                            frmSPALogin.hboxCaptchaText.setVisibility(false);
                        }
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300009/gi) != null) {
                        alert(kony.i18n.getLocalizedString("keyIBLoginError009"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        //alert("Logging not allowed")
                    } else if (resulttable["faultdetail"].match(/K000000300010/gi) != null) {
                        faultDetail = "K000000300010";
                        alert(kony.i18n.getLocalizedString("keyIBLoginError010"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        //alert("Logging not allowed")
                    } else if (resulttable["faultdetail"].match(/K000000300011/gi) != null) {
                        faultDetail = "K000000300011";
                        alert(kony.i18n.getLocalizedString("keyIBLoginError011"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        //alert("Logging not allowed")
                    } else if (resulttable["faultdetail"].match(/K000000300012/gi) != null) {
                        faultDetail = "K000000300012";
                        showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError012"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/E10105/gi) != null) {
                        alert(kony.i18n.getLocalizedString("keyIBLoginError004"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    } else if (resulttable["faultdetail"].match(/K000000300007/gi) != null) {
                        alert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                        //alert("Logging not allowed")
                    } else {
                        alert("System error :" + resulttable["faultdetail"]);
                        dismissLoadingScreen();
                        return false;
                    }
                } else {
                    // else if opstatus not 8005 also
                    dismissLoadingScreen();
                    if (resulttable["errCode"] == "VrfyOTPCtrErr00001" && resulttable["captchaCode"] == "1") {
                        //You have entered incorrect ActivationCode
                        showAlertIB(kony.i18n.getLocalizedString("ECVrfyACTErr00001"), kony.i18n.getLocalizedString("info"));
                        frmSPALogin.hboxCaptcha.setVisibility(true);
                        frmSPALogin.hboxCaptchaText.setVisibility(true);
                        frmSPALogin.imgcaptcha.base64 = resulttable["captchaImage"];
                        frmSPALogin.txtCaptchaText.text = "";
                        dismissLoadingScreen();
                        return false;
                    } else if (resulttable["errCode"] == "VrfyOTPCtrErr00001") {
                        showAlertIB(kony.i18n.getLocalizedString("ECVrfyACTErr00001"), kony.i18n.getLocalizedString("info"));
                        dismissLoadingScreen();
                    }
                    if (resulttable["captchaCode"] == "1") {
                        alert(kony.i18n.getLocalizedString("msgIBCaptchaWrong"));
                        frmSPALogin.hboxCaptcha.setVisibility(true);
                        frmSPALogin.hboxCaptchaText.setVisibility(true);
                        frmSPALogin.imgcaptcha.base64 = resulttable["captchaImage"];
                        frmSPALogin.txtCaptchaText.text = "";
                    } else {
                        frmSPALogin.hboxCaptcha.setVisibility(false);
                        frmSPALogin.hboxCaptchaText.setVisibility(false);
                        frmSPALogin.txtCaptchaText.text = "";
                    }
                }
            } else if (resulttable["opstatus"] == 99999) {
                if (resulttable["errmsg"] != undefined) {
                    alert(resulttable["errmsg"]);
                    window.location.reload(true);
                } else {
                    alert(kony.i18n.getLocalizedString("keyUndefinedError"));
                }
                dismissLoadingScreen();
            }
        }
    }
    frmSPALogin.txtUserId.text = "";
    frmSPALogin.txtPassword.text = "";
}

function LoginflowPartyInqServiceSPA() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["fIIdent"] = "";
    inputparam["rqUUId"] = "";
    inputparam["activationCompleteFlag"] = "Login";
    inputparam["partyIdentValue"] = "";
    if (isFromLogin) inputparam["TriggerEmail"] = "yes";
    isFromLogin = false;
    invokeServiceSecureAsync("LoginProcessServiceExecute", inputparam, LoginFlowPartyInqCallBackSPA); // Optimizing the login flow for SPA Channel.
}

function LoginFlowPartyInqCallBackSPA(status, resulttable) {
    if (status == 400) {
        var isError = showCommonAlertIB(resulttable)
        if (isError) return false;
        if (resulttable["opstatusPartyInq"] == 0 && resulttable["opstatuscrm"] == 0) {
            gblCustomerName = resulttable["customerName"];
            gblEmailId = resulttable["emailId"];
            //hbxIBPostLogin.lblName.text = resulttable["customerName"];
            //for (var i = 0; i < resulttable["ContactNums"].length; i++) {
            //				if (resulttable["ContactNums"][i]["PhnType"] == 'Mobile')
            //					gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
            //			}
            gblPHONENUMBER = resulttable["PhnNum"];
            //frmIBPostLoginDashboard.show();
            //onclickActivationCompleteStart();
            //adding these two line from MB and the rest of the flow is MB with loginnotification for SPA
            registerForTimeOut();
            frmAccountSummaryLanding.lblAccntHolderName.text = resulttable["customerName"];
            //frmAccountSummaryLanding = null;
            //			frmAccountSummaryLandingGlobals();
            TMBUtil.DestroyForm(frmAccountSummaryLanding);
            //frmAccountDetailsMB = null;
            //			frmAccountDetailsMBGlobals();
            gblAccountTable = "";
            GLOBAL_DEBITCARD_TABLE = "";
            gblSpaTokenServFalg = true;
            frmAccountDetailsMB.segDebitCard.removeAll();
            TMBUtil.DestroyForm(frmAccountDetailsMB);
            loginFlow = true;
            TMBUtil.DestroyForm(frmFATCAQuestionnaire1);
            TMBUtil.DestroyForm(frmFATCATnC);
            gblFATCAAnswers = {};
            FatcaQues_EN = {};
            FatcaQues_TH = {};
            frmSPALogin.hboxCaptcha.setVisibility(false);
            frmSPALogin.hboxCaptchaText.setVisibility(false);
            SPAcrmProfileInqCallback(status, resulttable);
            //callPartyInquiryService();
            //callCustomerAccountService();
        } else {
            invokeSPACRMProfileUpdateLogout();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function SPAcrmProfileInq() {
    showLoadingScreen();
    inputParam = {};
    inputParam["rqUUId"] = "";
    inputParam["channelName"] = "IB-INQ";
    //showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileInq", inputParam, SPAcrmProfileInqCallback)
}

function SPAcrmProfileInqCallback(status, resulttable) {
    if (status == 400) {
        gblFBCode = resulttable["facebookId"];
        var SpaLastLogin = "";
        if (resulttable["lastIbLoginSucessDate"] != undefined) SpaLastLogin = resulttable["lastIbLoginSucessDate"];
        if (SpaLastLogin != null && SpaLastLogin != "") {
            SpaLastLogin.replace(/-/g, "/");
            var SpaLoginDate = SpaLastLogin.match(/[-0-9]*T/g).toString();
            SpaLoginDate = SpaLoginDate.replace("T", "");
            var SpaLoginTime = SpaLastLogin.match(/T[0-9:]*/g).toString();
            SpaLoginTime = SpaLoginTime.replace("T", "");
        }
        gblCustomerStatus = resulttable["ebCustomerStatusId"]
        gblIBFlowStatus = resulttable["ibUserStatusIdTr"]
        gblMBFlowStatus = resulttable["mbFlowStatusIdRs"]
        gblEmailId = resulttable["emailAddr"]
        gblcrmId = resulttable["crmProfileId"];
        //LoginNotificationAlertServiceSpa();//No need to call here, it is called from loginvalidationpostprocessor
        //SpacrmProfileMod();
        //code for language flip for SPA
        if (flowSpa) {
            var langPref;
            if (resulttable["languageCd"] != null) {
                if (resulttable["languageCd"] == "TH") {
                    /*	list = {
                    	    appLocale: "th_TH"
                    	} */
                    langPref = "th_TH";
                    setLocaleTH();
                } else {
                    /*	list = {
                        appLocale: "en_US"
                    	} */
                    langPref = "en_US";
                    setLocaleEng();
                }
                kony.i18n.setCurrentLocaleAsync(langPref, SpaLangChangePartyInInboxCallBack(status, resulttable), SpaLangChangePartyInInboxCallBack(status, resulttable), "");
            }
            //kony.store.removeItem("curAppLocale");
            //kony.store.setItem("curAppLocale", list);
            //var getcurAppLocale = kony.store.getItem("curAppLocale");
            /*if (getcurAppLocale["appLocale"] != null) 
            {
               	if (getcurAppLocale["appLocale"] == "en_US") 
               	{
               	    setLocaleEng();
               	}
               	else if (getcurAppLocale["appLocale"] == "th_TH") 
               	{
                   	setLocaleTH();
               	}
            } */
        }
    }
}

function SpaLangChangePartyInInboxCallBack(status, resulttable) {
    showLoadingScreen();
    assignPreLoginFormSetUndefined();
    partyInquiryCallBack(status, resulttable);
    unreadInboxMessagesMBLoginCallBack(status, resulttable);
}

function invokeSPACRMProfileUpdateLogout() {
    showIBLoadingScreen();
    inputParam = {};
    inputParam["actionType"] = "23";
    invokeServiceSecureAsync("crmProfileMod", inputParam, callBackinvokeSPACRMProfileUpdateLogout)
}
/**
 * description
 * @returns {}
 */
function callBackinvokeSPACRMProfileUpdateLogout(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //IBLogoutService();
            invokeLogoutService();
            resetAllRcCacheData();
        } else {}
        kony.application.dismissLoadingScreen();
    }
}
/*
*************************************************************************************
		Module	: onClickSetPasswordNext
		Author  : Kony
		Purpose : Invoking create user ECAS service
****************************************************************************************
*/
function SpaUseridPasswordValidation() {
    showLoadingScreen();
    if (!gblSetPwdSpa) {
        if (frmMBSetuseridSPA.txtUserID.text == null || frmMBSetuseridSPA.txtUserID.text == "") {
            dismissLoadingScreen();
            showAlertRcMB(kony.i18n.getLocalizedString("keyEnterUserId"), kony.i18n.getLocalizedString("info"));
            frmMBSetuseridSPA.txtUserID.setFocus(true);
            //frmMBSetuseridSPA.btnConfirmSpa.setEnabled(false);
            return false
        }
    }
    if (frmMBSetuseridSPA.txtTransPass.text == null || frmMBSetuseridSPA.txtTransPass.text == "") {
        dismissLoadingScreen();
        showAlertRcMB(kony.i18n.getLocalizedString("keyPasswordRequired"), kony.i18n.getLocalizedString("info"));
        frmMBSetuseridSPA.txtTransPass.setFocus(true);
        //frmMBSetuseridSPA.btnConfirmSpa.setEnabled(false);
        return false
    }
    if (frmMBSetuseridSPA.txtConfirmPassword.text == null || frmMBSetuseridSPA.txtConfirmPassword.text == "") {
        dismissLoadingScreen();
        showAlertRcMB(kony.i18n.getLocalizedString("keyEnterConfirmPassword"), kony.i18n.getLocalizedString("info"));
        frmMBSetuseridSPA.txtConfirmPassword.setFocus(true);
        //frmMBSetuseridSPA.btnConfirmSpa.setEnabled(false);
        return false
    }
    if (!gblSetPwdSpa) {
        if (frmMBSetuseridSPA.txtUserID.text.length < 8 || frmMBSetuseridSPA.txtUserID.text.length > 20) {
            dismissLoadingScreen();
            showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
            frmMBSetuseridSPA.txtUserID.setFocus(true);
            frmMBSetuseridSPA.txtUserID.text = "";
            return;
        }
        if (!(frmMBSetuseridSPA.txtUserID.text.match(/[a-z]/ig))) {
            dismissLoadingScreen();
            showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
            frmMBSetuseridSPA.txtUserID.setFocus(true);
            frmMBSetuseridSPA.txtUserID.text = "";
            return;
        }
        if (kony.string.containsChars(frmMBSetuseridSPA.txtUserID.text, ["<"]) && kony.string.containsChars(frmMBSetuseridSPA.txtUserID.text, [">"])) {
            dismissLoadingScreen();
            showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
            frmMBSetuseridSPA.txtUserID.setFocus(true);
            frmMBSetuseridSPA.txtUserID.text = "";
            return;
        }
    }
    if ((frmMBSetuseridSPA.txtTransPass.text.length < 8 || frmMBSetuseridSPA.txtTransPass.text.length > 20)) {
        dismissLoadingScreen();
        showAlertRcMB(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmMBSetuseridSPA.txtTransPass.setFocus(true);
        return;
    }
    if (!(frmMBSetuseridSPA.txtTransPass.text.match(/[a-z]/gi) != null && frmMBSetuseridSPA.txtTransPass.text.match(/[0-9]/g) != null)) {
        dismissLoadingScreen();
        showAlertRcMB(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmMBSetuseridSPA.txtTransPass.setFocus(true);
        return;
    }
    if (kony.string.containsChars(frmMBSetuseridSPA.txtTransPass.text, ["<"]) && kony.string.containsChars(frmMBSetuseridSPA.txtTransPass.text, [">"])) {
        dismissLoadingScreen();
        showAlertRcMB(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
        frmMBSetuseridSPA.txtTransPass.text = "";
        frmMBSetuseridSPA.txtTransPass.setFocus(true);
        return;
    }
    if (frmMBSetuseridSPA.txtTransPass.text != frmMBSetuseridSPA.txtConfirmPassword.text) {
        dismissLoadingScreen();
        showAlertRcMB(kony.i18n.getLocalizedString("PassNoMatch"), kony.i18n.getLocalizedString("info"));
        frmMBSetuseridSPA.txtTransPass.text = "";
        frmMBSetuseridSPA.txtConfirmPassword.text = "";
        frmMBSetuseridSPA.txtTransPass.setFocus(true);
        return;
    }
    if (gblSetPwdSpa == true) {
        gblSPAPassword = frmMBSetuseridSPA.txtTransPass.text;
        resetSpaPassword(); //SpacrmProfileInqResetPwd();
    } else {
        SPAcreateUser();
    }
}

function SPAcreateUser() {
    inputParam = {};
    inputParam["userStoreId"] = "";
    inputParam["loginId"] = frmMBSetuseridSPA.txtUserID.text;
    inputParam["segmentId"] = "MIB";
    inputParam["ibPwd"] = frmMBSetuseridSPA.txtTransPass.text;
    inputParam["session"] = "";
    inputParam["username"] = "";
    invokeServiceSecureAsync("createUserIB", inputParam, callBackSPAcreateUser)
}

function callBackSPAcreateUser(status, resulttable) {
    if (status == 400) {
        if (resulttable["errCode"] == "UseridErr") {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("invalidUserId"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (resulttable["errCode"] == "IBPwdError") {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (resulttable["opstatus"] == 0) {
            // going to the normal MB flow
            kony.application.dismissLoadingScreen();
            frmConnectAccMB.tbxPopupTractPwdtxt.text = gblEmailId
                //assigning the values to global variables to fix spa activation issues
            gblSPAUsername = frmMBSetuseridSPA.txtUserID.text;
            gblSPAPassword = frmMBSetuseridSPA.txtTransPass.text;
            frmConnectAccMB.show();
        } else if (resulttable["opstatus"] == 8009 && resulttable["errCode"] == "validationErr") {
            if (resulttable["errorKey"] == "keyPassSettingGuidelinesIB") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyPassSettingGuidelinesIB"), kony.i18n.getLocalizedString("info"));
                frmMBSetuseridSPA.txtTransPass.text = "";
                frmMBSetuseridSPA.txtConfirmPassword.text = "";
                //frmMBSetuseridSPA.txtTransPass.setFocus(true);
                return false;
            }
            if (resulttable["errorKey"] == "keyWrongPwd") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
                frmMBSetuseridSPA.txtTransPass.text = "";
                frmMBSetuseridSPA.txtConfirmPassword.text = "";
                //frmMBSetuseridSPA.txtTransPass.setFocus(true);
                return false;
            }
        } else {
            if (resulttable["code"] == "1000") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECCreateUserExits"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "40001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECCreateUserError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errorKey"] == "keyRepeatCharErr") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyRepeatCharErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                kony.application.dismissLoadingScreen();
                if (resulttable["errMsg"] != null || resulttable["errMsg"] != "") showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                else showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
}

function resetSpaPassword() {
    //showLoadingScreenPopup();
    showLoadingScreen();
    var password_inputparam = {};
    //take userID from session
    //password_inputparam["userID"] = frmMBSetuseridSPA.txtUserID.text;
    gblUserName = frmMBSetuseridSPA.txtUserID.text;
    password_inputparam["loginId"] = "IB_Pwd";
    password_inputparam["newPassword"] = frmMBSetuseridSPA.txtTransPass.text; //map to transation password of form
    gblPassword = frmMBSetuseridSPA.txtTransPass.text;
    password_inputparam["segIdVal"] = "MIB"
        //password_inputparam["userStoreId"] = "DefaultStore"
        //password_inputparam["key"] = ""
        //password_inputparam["value"] = ""
    invokeServiceSecureAsync("resetPasswordIB", password_inputparam, resetSpaPasswordCallBack);
}

function resetSpaPasswordCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["resetPasswordResponse"] != null) {
                modifyUserForUnlockOTPSpa();
            }
        } else if (resulttable["opstatus"] == 8009 && resulttable["errCode"] == "validationErr") {
            if (resulttable["errorKey"] == "keyPassSettingGuidelinesIB") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyPassSettingGuidelinesIB"), kony.i18n.getLocalizedString("info"));
                frmMBSetuseridSPA.txtTransPass.text = "";
                frmMBSetuseridSPA.txtConfirmPassword.text = "";
                //frmMBSetuseridSPA.txtTransPass.setFocus(true);
                return false;
            }
            if (resulttable["errorKey"] == "keyWrongPwd") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"), kony.i18n.getLocalizedString("info"));
                frmMBSetuseridSPA.txtTransPass.text = "";
                frmMBSetuseridSPA.txtConfirmPassword.text = "";
                //	frmMBSetuseridSPA.txtTransPass.setFocus(true);
                return false;
            }
        } else {
            if (resulttable["opstatus"] == 8005) alert(resulttable["errMsg"]);
            dismissLoadingScreen();
        }
    }
}
/*
*************************************************************************************
		Module	: SPAcrmProfileUpdate
		Author  : Kony
		Purpose : Invoking create user ECAS service
****************************************************************************************
*/
function SPAcrmProfileUpdate() {
    //showLoadingScreenPopup();
    inputParam = {};
    inputParam["rqUUId"] = "";
    inputParam["channelName"] = "IB-INQ";
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackSPAcrmProfileUpdate);
}

function callBackSPAcrmProfileUpdate(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //crmUpdateActivation("01");
            gblFBCode = resulttable["facebookId"];
            inputParam = {};
            inputParam["rqUUId"] = "";
            inputParam["channelName"] = "IB-INQ";
            inputParam["ibUserId"] = frmMBSetuseridSPA.txtUserID.text;
            inputParam["actionCode"] = gblActionCode;
            //inputParam["ebTxnLimitAmt"] = resulttable["ebTxnLimitAmt"];
            //inputParam["ebMaxLimitAmtCurrent"] = resulttable["ebMaxLimitAmtCurrent"];
            //inputParam["ebMaxLimitAmtHist"] = resulttable["ebMaxLimitAmtHist"];
            //inputParam["ebMaxLimitAmtRequest"] = resulttable["ebMaxLimitAmtRequest"];
            //inputParam["ebAccuUsgAmtDaily"] = resulttable["ebAccuUsgAmtDaily"];
            //inputParam["firstActivationDate"] = "";
            //inputParam["firstIbLoginDate"] = ""
            //inputParam["firstMbLoginDate"] = resulttable["firstMbLoginDate"];;
            //inputParam["lastIbLoginSucessDate"] = ""
            //inputParam["lastIbLoginFailDate"] = ""
            //inputParam["lastIbLogoutDate"] = resulttable["lastIbLogoutDate"];
            //inputParam["lastMbLoginSucessDate"] = resulttable["lastMbLoginSucessDate"];
            //inputParam["lastMbLoginFailDate"] = resulttable["lastMbLoginFailDate"];
            //inputParam["lastMbLogoutDate"] = resulttable["lastMbLogoutDate"];
            //inputParam["lastIbPasswordChangeDate"] = resulttable["lastIbPasswordChangeDate"];
            //inputParam["lastMbAccessPasswordChangeDate"] = resulttable["lastMbAccessPasswordChangeDate"];
            //inputParam["lastMbTxnPwdChangeDate"] = resulttable["lastMbTxnPwdChangeDate"];
            //inputParam["ibApplyChannel"] = resulttable["ibApplyChannel"];
            var locale = kony.i18n.getCurrentLocale();
            if (locale == "en_US") {
                inputParam["languageCd"] = "EN";
            } else {
                inputParam["languageCd"] = "TH";
            }
            //inputParam["tokenDeviceFlag"] = resulttable["tokenDeviceFlag"];
            //inputParam["facebookID"] = resulttable["facebookId"];
            inputParam["ebCustomerStatusID"] = "02";
            inputParam["ibUserStatusId"] = "02";
            //inputParam["mbUserStatusId"] = resulttable["mbUserStatusId"];
            //inputParam["createdDt"] = resulttable["createdDt"];
            if (gblActionCode == "11") inputParam["actionType"] = "24";
            else if (gblActionCode == "12" || gblActionCode == "13") inputParam["actionType"] = "29";
            //inputParam["p2pLinkAcctID"] = resulttable["p2pLinkedAcct"];
            //inputParam["emailAddr"] = kony.string.trim(frmConnectAccMB.tbxPopupTractPwdtxt.text);
            if (frmConnectAccMB.tbxPopupTractPwdtxt.text != "") {
                inputParam["emailAddr"] = kony.string.trim(frmConnectAccMB.tbxPopupTractPwdtxt.text);
            }
            invokeServiceSecureAsync("crmProfileModActivation", inputParam, callBackSPAcrmProfileUpdateStep2)
                //frmMBActiComplete.show()
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
            return false;
        }
    }
}

function modifyUserForUnlockOTPSpa() {
    var inputParams = {}
    inputParams["userId"] = frmMBSetuseridSPA.txtUserID.text;
    inputParams["status"] = "Activated";
    invokeServiceSecureAsync("modifyUserToUnlockOTP", inputParams, callBackmodifyUserForUnlockOTPSpa);
}

function callBackmodifyUserForUnlockOTPSpa(status, resultTable) {
    if (status == 400) {
        if (resultTable["opstatus"] == 0) {
            SPAcrmProfileUpdate();
        } else {
            alert(resulttable["errMsg"]);
            dismissLoadingScreenPopup();
        }
    }
}

function callBackSPAcrmProfileUpdateStep2(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["errCode"] == "JavaErr00001") {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                gblUserLockStatusIB = resulttable["IBUserStatusID"];
                //activityLogServiceCall("005","0000000000000","01");	
                if (gblActionCode == "12" || gblActionCode == "13") {
                    //alert("gblActionCode" + gblActionCode);
                    var inputparam = {}
                        // invokeServiceSecureAsync("partyInquiry", inputparam, rectivationCallBack);
                        //alert("gblActionCode complete");
                }
                IBsendTnCMailPartyInq();
                frmMBActiComplete.image250285458166.src = "iconcomplete.png";
                frmMBActiComplete.show();
                campaginService("image2447443295186", "frmMBActiComplete", "M");
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            dismissLoadingScreen();
            return false;
        }
        dismissLoadingScreen();
    }
}

function SpaConnectAccValidatn() {
    var emailFlag = emailValidatn(frmConnectAccMB.tbxPopupTractPwdtxt.text);
    //var devNameFlag = MblNickName(frmConnectAccMB.txtDeviceName.text);
    var invalidEmail1 = kony.i18n.getLocalizedString("invalidEmail");
    //var invalidDivName = kony.i18n.getLocalizedString("invalidDeviceName");
    var info1 = kony.i18n.getLocalizedString("info");
    var okk = kony.i18n.getLocalizedString("keyOK");
    //gblActionCode = "21";
    //gActivationCode = "erT12345";
    if (emailFlag == false) {
        showAlert(invalidEmail1, info1);
        //	alert("INVALID EMAIL ID");
    } else {
        SPAcrmProfileUpdate();
        //frmMBActiComplete.show();
    }
}

function partyInqMobForApplyMBServiceSpa() {
    var inputParam = {};
    showLoadingScreen();
    invokeServiceSecureAsync("partyInquiry", inputParam, partyInqMobForApplyMBServiceCallBackSpa);
}

function partyInqMobForApplyMBServiceCallBackSpa(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //alert("inside partyInquiryViewProfileCallBack");
            for (var i = 0; i < resulttable["ContactNums"].length; i++) {
                var PhnType = resulttable["ContactNums"][i]["PhnType"];
                if (PhnType != null && PhnType != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined) {
                    if (PhnType == "Mobile") {
                        if ((resulttable["ContactNums"][i]["PhnNum"] != null) && (resulttable["ContactNums"][i]["PhnNum"] != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined)) {
                            gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
                            gblPHONENUMBEROld = resulttable["ContactNums"][i]["PhnNum"];
                            frmApplyMBSPA.lblMobileNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                            // alert("mobile no");
                        }
                    }
                }
            }
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
        }
    } else {
        dismissLoadingScreen();
    }
}

function invokeSPACRMProfileUpdateLogout() {
    showIBLoadingScreen();
    inputParam = {};
    inputParam["actionType"] = "23";
    invokeServiceSecureAsync("crmProfileMod", inputParam, callBackinvokeSPACRMProfileUpdateLogout)
}
/**
 * description
 * @returns {}
 */
function callBackinvokeSPACRMProfileUpdateLogout(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblLogoutTime = resulttable["LastIBLogoutDate"];
            invokeLogoutService();
            //ResetTransferHomePageIB(); //Clearing cache for Transfers Module
            resetAllRcCacheData();
            //resetAllBillTopupCacheDataIB(); //Clear Caching for Billers And Topups.
        } else {}
        dismissLoadingScreen();
    }
}

function removeHyphenSPA(accno) {
    var AcctID;
    for (var i = 0; i < accno.length; i++) {
        if (accno[i] != "-") {
            if (AcctID == null) {
                AcctID = accno[i];
            } else {
                AcctID = AcctID + accno[i];
            }
        }
    }
    return AcctID;
}

function SpaLoginServiceonComplete() {
    showLoadingScreen();
    MBSpauserID = gblSPAUsername;
    var inputParam = {};
    inputParam["loginId"] = kony.string.trim(gblSPAUsername);
    inputParam["password"] = kony.string.trim(gblSPAPassword);
    inputParam["userid"] = kony.string.trim(gblSPAUsername);
    gblUserName = gblSPAUsername;
    //inputParam["bankCD"] = "11";
    //inputParam["loginChannel"] = "01";
    //inputParam["sessionId"] = "qwerty1234556";
    //inputParam["remoteIP"] = "10.20.3.30";
    //inputParam["capchaID"] = "123";
    inputParam["capchaID"] = "";
    invokeServiceSecureAsync("IBVerifyLoginEligibility", inputParam, callBackMBSpaLoginService)
}
//Do not Comment these function as it is for Smart Banner
function smartBannerIphone() {
    var code = "<script>$(document).ready(function(){$.smartbanner({title: 'TMB Touch',author: 'TMB Bank Public Company Limited',icon: 'https://lh4.ggpht.com/3vEyuc1-y_d1UNnHwusYIxunuvnee9h0mZVc7ihuaHBl44M59cN_R_lpNdsN1fPeldUJ=w300',url: 'https://itunes.apple.com/app/id884079963'});});</scr" + "ipt>";
    $('body').append($(code)[0]);
};

function smartBannerAndroid() {
    var code = "<script>$(document).ready(function(){$.smartbanner({title: 'TMB Touch',author: 'TMB Bank Public Company Limited',icon: 'https://lh4.ggpht.com/3vEyuc1-y_d1UNnHwusYIxunuvnee9h0mZVc7ihuaHBl44M59cN_R_lpNdsN1fPeldUJ=w300',url: 'https://play.google.com/store/apps/details?id=com.TMBTOUCH.PRODUCTION&hl=en'});});</scr" + "ipt>";
    $('body').append($(code)[0]);
};
/**
 * Function to assign pre login forms having menu created as true to false. 
 */
function assignPreLoginFormSetUndefined() {
    if (frmATMBranch.info != undefined) {
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.hboxMenuHeader);
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.SegMainMenu);
        frmATMBranch.info = undefined;
    }
    if (frmATMBranchesDetails.info != undefined) {
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.hboxMenuHeader);
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.SegMainMenu);
        frmATMBranchesDetails.info = undefined;
    }
    if (frmATMBranchList.info != undefined) {
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.hboxMenuHeader);
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.SegMainMenu);
        frmATMBranchList.info = undefined;
    }
    if (frmExchangeRate.info != undefined) {
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.hboxMenuHeader);
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.SegMainMenu);
        frmExchangeRate.info = undefined;
    }
    if (frmContactUsMB.info != undefined) {
        frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.hboxMenuHeader);
        frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.SegMainMenu);
        frmContactUsMB.info = undefined;
    }
    if (frmContactusFAQMB.info != undefined) {
        frmContactusFAQMB.scrollboxLeft.remove(frmContactusFAQMB.hboxMenuHeader);
        frmContactusFAQMB.scrollboxLeft.remove(frmContactusFAQMB.SegMainMenu);
        frmContactusFAQMB.info = undefined;
    }
}
/**
 * Function to assign post login forms having menu created as true to false.
 */
function assignPostLoginFormSetUndefined() {
    if (frmATMBranch.info != undefined) {
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.hboxMenuHeader);
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.hbxDown);
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.hbxup);
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.SegAboutMenu);
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.SegMyInbox);
        frmATMBranch.scrollboxLeft.remove(frmATMBranch.SegMainMenu);
        frmATMBranch.info = undefined;
    }
    if (frmATMBranchesDetails.info != undefined) {
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.hboxMenuHeader);
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.hbxDown);
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.hbxup);
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.SegAboutMenu);
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.SegMyInbox);
        frmATMBranchesDetails.scrollboxLeft.remove(frmATMBranchesDetails.SegMainMenu);
        frmATMBranchesDetails.info = undefined;
    }
    if (frmATMBranchList.info != undefined) {
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.hboxMenuHeader);
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.hbxDown);
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.hbxup);
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.SegAboutMenu);
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.SegMyInbox);
        frmATMBranchList.scrollboxLeft.remove(frmATMBranchList.SegMainMenu);
        frmATMBranchList.info = undefined;
    }
    if (frmExchangeRate.info != undefined) {
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.hboxMenuHeader);
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.hbxDown);
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.hbxup);
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.SegAboutMenu);
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.SegMyInbox);
        frmExchangeRate.scrollboxLeft.remove(frmExchangeRate.SegMainMenu);
        frmExchangeRate.info = undefined;
    }
    /*if(frmContactUsMB.info != undefined) {
    	frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.hboxMenuHeader);
    	frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.hbxDown);
    	frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.hbxup);
    	frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.SegAboutMenu);
    	frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.SegMyInbox);
    	frmContactUsMB.scrollboxLeft.remove(frmContactUsMB.SegMainMenu);
    	frmContactUsMB.info = undefined;
    }
    if(frmContactusFAQMB.info != undefined) {
        frmCofrmContactusFAQMBntactUsMB.scrollboxLeft.remove(frmCofrmContactusFAQMBntactUsMB.hboxMenuHeader);
    	frmCofrmContactusFAQMBntactUsMB.scrollboxLeft.remove(frmCofrmContactusFAQMBntactUsMB.hbxDown);
    	frmCofrmContactusFAQMBntactUsMB.scrollboxLeft.remove(frmCofrmContactusFAQMBntactUsMB.hbxup);
    	frmCofrmContactusFAQMBntactUsMB.scrollboxLeft.remove(frmCofrmContactusFAQMBntactUsMB.SegAboutMenu);
    	frmCofrmContactusFAQMBntactUsMB.scrollboxLeft.remove(frmCofrmContactusFAQMBntactUsMB.SegMyInbox);
        frmCofrmContactusFAQMBntactUsMB.scrollboxLeft.remove(frmCofrmContactusFAQMBntactUsMB.SegMainMenu);
    	frmContactusFAQMB.info = undefined;
    }*/
}

function refreshCaptchaImageSPA() {
    showLoadingScreen();
    var inputParam = {};
    invokeServiceSecureAsync("getCaptchaOnRefresh", inputParam, getCaptchaOnRefreshCallbackSPA)
}

function getCaptchaOnRefreshCallbackSPA(status, resulttable) {
    dismissLoadingScreen();
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (flowSpa) {
                frmSPALogin.imgcaptcha.base64 = resulttable["captchaImage"];
                frmSPALogin.txtCaptchaText.text = "";
            } else {
                //Added for ENH_207_1 and Modified for ENH_207_6
                if (kony.application.getCurrentForm().id == "frmMBActivationIBLogin") {
                    frmMBActivationIBLogin.imgcaptcha.base64 = resulttable["captchaImage"];
                    frmMBActivationIBLogin.txtCaptchaText.text = "";
                } else {
                    frmMBActiAtmIdMobile.imgcaptcha.base64 = resulttable["captchaImage"];
                    frmMBActiAtmIdMobile.txtCaptchaText.text = "";
                }
            }
        }
    }
}

function mbActivationIBLogin() {
    //Added for ENH_207 to send uniqueid in ib login for MB Activation - function MBSpaLoginService()
    GBL_FLOW_ID_CA = 8;
    TrusteerDeviceId();
}