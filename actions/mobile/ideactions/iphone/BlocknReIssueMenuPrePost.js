//var creditCardImgSrc = "https://dev.tau2904.com/tmb/ImageRender?crmId=&&personalizedId=&billerId=cardoneiphone6plus&modIdentifier=PRODUCTPACKAGEIMG&dummy=";
function frmMBBlockCCReasonMenuPreshow() {
    changeStatusBarColor();
    if (gblCallPrePost) {
        frmMBBlockCCReason.flxSubheader.top = "20dp";
        frmMBBlockCCReason.flxSubheader.height = "50dp";
        frmMBBlockCCReason.flexLineUp.top = "0dp";
        frmMBBlockCCReason.lblHdrTxt.text = kony.i18n.getLocalizedString("keyReason");
        frmMBBlockCCReason.lblsubheader.text = kony.i18n.getLocalizedString("msgSelectBlockReason");
        frmMBBlockCCReason.btnBack.text = kony.i18n.getLocalizedString("Back");
        frmMBBlockCCReason.btnBack.focusSkin = "btnBlue200GreyBG";
        frmMBBlockCCReason.flexLineUp.height = 0.5;
        var dataSegment = [];
        var currentLang = kony.i18n.getCurrentLocale();
        for (i = 0; i < cardBlockReasons.length; i++) {
            var currentRecord = cardBlockReasons[i];
            if (currentLang == currentRecord.blockReasonLang) {
                var tempSegmentRecord;
                tempSegmentRecord = {
                    "hdnlblReasonCode": currentRecord.blockReasonCode,
                    "lblReasonValue": currentRecord.blockReasonDesc
                };
                dataSegment.push(tempSegmentRecord);
            }
        }
        frmMBBlockCCReason.segReason.setData(dataSegment);
    }
}

function frmMBBlockCCReasonMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

function frmMBReIssueDBProductMenuPreshow() {
    changeStatusBarColor();
    if (gblCallPrePost) {
        frmMBReIssueDBProduct.flxSubheader.width = "95%";
        frmMBReIssueDBProduct.lblHdrTxt.text = kony.i18n.getLocalizedString("titleIssueDebitCard");
        frmMBReIssueDBProduct.lblsubheader.text = kony.i18n.getLocalizedString("DBI03_Desc");
        frmMBReIssueDBProduct.lblAnnualFee.text = kony.i18n.getLocalizedString("DBI03_AnnualFee");
        frmMBReIssueDBProduct.lblCardNum.text = kony.i18n.getLocalizedString("DBI03_CardName");
        frmMBReIssueDBProduct.lblEntranceFee.text = kony.i18n.getLocalizedString("DBI03_EntranceFee");
        frmMBReIssueDBProduct.btnback.text = kony.i18n.getLocalizedString("Back");
        frmMBReIssueDBProduct.btnnext.text = kony.i18n.getLocalizedString("DBI03_BtnIssue");
        //frmMBReIssueDBProduct.flxCardDetails.centerX = "50%";
        frmMBReIssueDBProduct.flxCardDetails.width = "80%";
        frmMBReIssueDBProduct.lblCardNum.width = "38%";
        frmMBReIssueDBProduct.lblCardNum.right = "0dp";
        frmMBReIssueDBProduct.lblCardNumVal.left = "38%";
        frmMBReIssueDBProduct.lblCardNumVal.right = "0dp";
        frmMBReIssueDBProduct.lblCardNumVal.width = "62%";
        frmMBReIssueDBProduct.lblCardNumVal.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        frmMBReIssueDBProduct.flxEntranceFee.top = "5dp";
        frmMBReIssueDBProduct.lblAnnualFee.width = "75%";
        if (getDeviceType() == "iPhone6") {
            frmMBReIssueDBProduct.flxCardImage.width = "90%";
            frmMBReIssueDBProduct.flxCardImage.height = "220dp";
        }
        frmMBReIssueDBProduct.flxcardMsg.width = "80%";
        if (gblofferCardDetails["EntranceFee"] == "0" || gblofferCardDetails["EntranceFee"] == 0) {
            frmMBReIssueDBProduct.lblEntranceFeeVal.text = kony.i18n.getLocalizedString("DBI03_FeeValue");
        } else {
            frmMBReIssueDBProduct.lblEntranceFeeVal.text = gblofferCardDetails["EntranceFee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        if (gblofferCardDetails["annualFee"] == "0" || gblofferCardDetails["annualFee"] == 0) {
            frmMBReIssueDBProduct.lblAnnualFeeVal.text = kony.i18n.getLocalizedString("DBI03_AnnualFeeValue");
        } else {
            frmMBReIssueDBProduct.lblAnnualFeeVal.text = gblofferCardDetails["annualFee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        if (gblofferCardDetails["productID"] != gblofferCardDetails["newProductID"]) {
            //frmMBReIssueDBProduct.flxcardMsg.setVisibility(true);
            frmMBReIssueDBProduct.richtextCardMsg.text = kony.i18n.getLocalizedString("DBI03_SwitchDesc");
        } else {
            //frmMBReIssueDBProduct.flxcardMsg.setVisibility(false);
            frmMBReIssueDBProduct.richtextCardMsg.text = kony.i18n.getLocalizedString("DBI03_NotSwitchDesc");
        }
    }
}

function frmMBReIssueDBProductMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}
//frmMBBlockCardRecommendation
function frmMBBlockCardRecommendationMenuPreshow() {
    changeStatusBarColor();
    if (gblCallPrePost) {
        //ui part
        //frmMBBlockCardRecommendation.richtextCardMsg.skin = "lblGrey48px";
        frmMBBlockCardRecommendation.richtextCardMsg.top = "5dp";
        frmMBBlockCardRecommendation.btnchangeAdd.top = "15dp";
        frmMBBlockCardRecommendation.btnchangeAdd.focusSkin = "btnBlue200GreyBG";
        frmMBBlockCardRecommendation.btnback.focusSkin = "btnBlue200GreyBG";
        if (getDeviceType() == "iPhone6") {
            frmMBBlockCardRecommendation.flxCardImage.height = "220dp"
        }
        frmMBBlockCardRecommendation.flxCardImage.centerX = "50%";
        frmMBBlockCardRecommendation.flxCardImage.top = "0dp";
        frmMBBlockCardRecommendation.imgcard.src = getImageSRC("Cutting" + gblSelectedCard["IMG_CARD_ID"]);
        frmMBBlockCardRecommendation.lblHdrTxt.text = kony.i18n.getLocalizedString("titleBlockCreditCard");
        frmMBBlockCardRecommendation.lblsubheader.text = kony.i18n.getLocalizedString("msgRecommendation");
        //frmMBBlockCardRecommendation.lblsubheader.text = kony.i18n.getLocalizedString("DBI03_Desc");
        //frmMBBlockCardRecommendation.lblCardAccountName.text=frmMBBlockCardCCDBConfirm.lblCardAccountName.text;
        //frmMBBlockCardRecommendation.lblCardNumber.text=frmMBBlockCardCCDBConfirm.lblCardNumber.text;
        frmMBBlockCardRecommendation.lblCardAccountName.text = "";
        frmMBBlockCardRecommendation.lblCardNumber.text = "";
        frmMBBlockCardRecommendation.richtextCardMsg.text = kony.i18n.getLocalizedString("msgRecommendationDetails");
        frmMBBlockCardRecommendation.btnchangeAdd.text = kony.i18n.getLocalizedString("btnBlockCardNow");
        frmMBBlockCardRecommendation.btnback.text = kony.i18n.getLocalizedString("DBI03_BtnCancel");
        frmMBBlockCardRecommendation.btnback.onClick = frmMBBlockCardRecommendationBtnbackOnClick;
        frmMBBlockCardRecommendation.btnchangeAdd.onClick = showBlockCardPwdPopup;
    }
}

function frmMBBlockCardRecommendationMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}
//frmMBBlockDebitCardConfirm
function frmMBBlockDebitCardConfirmMenuPreshow() {
    changeStatusBarColor();
    if (gblCallPrePost) {
        //ui part
        frmMBBlockDebitCardConfirm.lblCardNumber.skin = getCardNoSkin();
        frmMBBlockDebitCardConfirm.flxCardImage.top = "6%";
        frmMBBlockDebitCardConfirm.richtextCardMsg.top = "1dp";
        frmMBBlockDebitCardConfirm.btnchangeAdd.top = "1dp";
        frmMBBlockDebitCardConfirm.btnchangeAdd.focusSkin = "btnBlue200GreyBG";
        frmMBBlockDebitCardConfirm.btnback.focusSkin = "btnBlue200GreyBG";
        frmMBBlockDebitCardConfirm.flxCardImage.width = "90%";
        frmMBBlockDebitCardConfirm.flxCardImage.height = "41%";
        frmMBBlockDebitCardConfirm.imgcard.width = "95%";
        frmMBBlockDebitCardConfirm.imgcard.height = "100%";
        frmMBBlockDebitCardConfirm.lblCardNumber.top = "53%";
        frmMBBlockDebitCardConfirm.lblCardNumber.left = "10%";
        frmMBBlockDebitCardConfirm.lblCardNumber.width = "80%";
        frmMBBlockDebitCardConfirm.lblCardNumber.height = "15%";
        frmMBBlockDebitCardConfirm.lblCardAccountName.top = "67%";
        frmMBBlockDebitCardConfirm.lblCardAccountName.left = "10%";
        frmMBBlockDebitCardConfirm.lblCardAccountName.width = "80%";
        frmMBBlockDebitCardConfirm.lblCardAccountName.height = "15%";
        frmMBBlockDebitCardConfirm.lblHdrTxt.text = kony.i18n.getLocalizedString("DBB02_Topic");
        frmMBBlockDebitCardConfirm.imgcard.src = frmMBManageCard.imgCard.src;
        frmMBBlockDebitCardConfirm.lblCardAccountName.text = frmMBManageCard.lblCardAccountName.text;
        frmMBBlockDebitCardConfirm.lblCardNumber.text = frmMBManageCard.lblCardNumber.text;
        frmMBBlockDebitCardConfirm.lblRecommendation.text = kony.i18n.getLocalizedString("DBB02_Recommend");
        frmMBBlockDebitCardConfirm.richtextCardMsg.text = kony.i18n.getLocalizedString("DBB02_RecommendDesc");
        frmMBBlockDebitCardConfirm.btnback.text = kony.i18n.getLocalizedString("Back");
        frmMBBlockDebitCardConfirm.btnback.onClick = onclickBackfrmMBBlockCardCCDBConfirm;
        frmMBBlockDebitCardConfirm.btnchangeAdd.text = kony.i18n.getLocalizedString("DBB02_Block");
        frmMBBlockDebitCardConfirm.btnchangeAdd.onClick = onClickBlockDebitCard;
        frmMBBlockDebitCardConfirm.flxCardImage.height = "37.75%";
        frmMBBlockDebitCardConfirm.flxCardImage.width = "83%";
        //frmMBBlockDebitCardConfirm.lblRibbon.top="16%";
        frmMBBlockDebitCardConfirm.lblCardAccountName.top = "73%";
        frmMBBlockDebitCardConfirm.lblCardNumber.top = "59%";
        frmMBBlockDebitCardConfirm.lblCardAccountName.left = "12%";
        frmMBBlockDebitCardConfirm.lblCardNumber.left = "12%";
    }
}

function frmMBBlockDebitCardConfirmMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}
//frmMBBlockCardCCDBConfirm
function frmMBBlockCardCCDBConfirmMenuPreshow() {
    changeStatusBarColor();
    if (gblCallPrePost) {
        kony.print("gblCCDBCardFlow  " + gblCCDBCardFlow);
        frmMBBlockCardCCDBConfirm.flxCardImage.width = "90%";
        frmMBBlockCardCCDBConfirm.flxCardImage.height = "41%";
        frmMBBlockCardCCDBConfirm.imgcard.width = "95%";
        frmMBBlockCardCCDBConfirm.imgcard.height = "100%";
        frmMBBlockCardCCDBConfirm.lblCardNumber.skin = getCardNoSkin();
        frmMBBlockCardCCDBConfirm.lblCardNumber.top = "53%";
        frmMBBlockCardCCDBConfirm.lblCardNumber.left = "10%";
        frmMBBlockCardCCDBConfirm.lblCardNumber.width = "80%";
        frmMBBlockCardCCDBConfirm.lblCardNumber.height = "15%";
        frmMBBlockCardCCDBConfirm.lblCardAccountName.top = "67%";
        frmMBBlockCardCCDBConfirm.lblCardAccountName.left = "10%";
        frmMBBlockCardCCDBConfirm.lblCardAccountName.width = "80%";
        frmMBBlockCardCCDBConfirm.lblCardAccountName.height = "15%";
        //ui part
        //frmMBBlockCardCCDBConfirm.richtextCardMsg.skin = "richTxtBlack200";
        //frmMBBlockCardCCDBConfirm.lblSendAddress.skin = "lblGrey48px";
        frmMBBlockCardCCDBConfirm.flxSendAddressVal.top = "1dp";
        frmMBBlockCardCCDBConfirm.btnchangeAdd.top = "1dp";
        //frmMBBlockCardCCDBConfirm.flxCardImage.height = "37.75%";
        //frmMBBlockCardCCDBConfirm.flxCardImage.width = "83%";
        //frmMBBlockCardCCDBConfirm.lblRibbon.top="16%";
        frmMBBlockCardCCDBConfirm.lblCardAccountName.top = "73%";
        frmMBBlockCardCCDBConfirm.lblCardNumber.top = "59%";
        frmMBBlockCardCCDBConfirm.lblCardAccountName.left = "12%";
        frmMBBlockCardCCDBConfirm.lblCardNumber.left = "12%";
        frmMBBlockCardCCDBConfirm.btnback.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmMBBlockCardCCDBConfirm.btnnext.text = kony.i18n.getLocalizedString("keyConfirm");
        if (gblCCDBCardFlow == "CREDIT_CARD_BLOCK") {
            frmMBBlockCardCCDBConfirm.lblHdrTxt.text = kony.i18n.getLocalizedString("titleBlockCreditCard");
            //frmMBBlockCardCCDBConfirm.richtextSubheader.text=kony.i18n.getLocalizedString("msgBlockCreditCard");
            frmMBBlockCardCCDBConfirm.flxSubheader.setVisibility(false);
            if (getDeviceType() == "iPhone6") {
                frmMBBlockCardCCDBConfirm.flxCardImage.top = "-100dp";
            } else {
                frmMBBlockCardCCDBConfirm.flxCardImage.top = "-85dp";
            }
            frmMBBlockCardCCDBConfirm.imgcard.src = frmMBManageCard.imgCard.src;
            frmMBBlockCardCCDBConfirm.lblCardAccountName.text = frmMBManageCard.lblCardAccountName.text;
            frmMBBlockCardCCDBConfirm.flxCardDetails.setVisibility(false);
            frmMBBlockCardCCDBConfirm.flxcardMsg.setVisibility(true);
            frmMBBlockCardCCDBConfirm.flxSendAddress.setVisibility(true);
            frmMBBlockCardCCDBConfirm.flxSendAddressVal.setVisibility(true);
            frmMBBlockCardCCDBConfirm.btnchangeAdd.setVisibility(true);
            if (frmMBBlockCCReason.segReason.selectedItems[0]["hdnlblReasonCode"] == "I") { //Damaged
                frmMBBlockCardCCDBConfirm.btnnext.text = kony.i18n.getLocalizedString("Next");
                frmMBBlockCardCCDBConfirm.btnnext.onClick = onclickNextfrmMBBlockCardCCDNext;
            } else {
                frmMBBlockCardCCDBConfirm.btnnext.text = kony.i18n.getLocalizedString("btnBlockCard");
                frmMBBlockCardCCDBConfirm.btnnext.onClick = onclickNextfrmMBBlockCardCCDBConfirm;
            }
            frmMBBlockCardCCDBConfirm.richtextCardMsg.text = kony.i18n.getLocalizedString("msgBlockCardRemark");
            frmMBBlockCardCCDBConfirm.lblSendAddress.text = kony.i18n.getLocalizedString("msgConfirmAddr");
            //frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text=kony.i18n.getLocalizedString("");
            frmMBBlockCardCCDBConfirm.btnchangeAdd.text = kony.i18n.getLocalizedString("msgChangeAddrQuestion");
            frmMBBlockCardCCDBConfirm.btnback.text = kony.i18n.getLocalizedString("Back");
            frmMBBlockCardCCDBConfirm.btnback.onClick = onclickBackfrmMBBlockCardCCDBConfirm;
            //frmMBBlockCardCCDBConfirm.btnnext.onClick=onclickNextfrmMBBlockCardCCDBConfirm;
        } else if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
            frmMBBlockCardCCDBConfirm.lblHdrTxt.text = kony.i18n.getLocalizedString("keyConfirmation");
            frmMBBlockCardCCDBConfirm.flxSubheader.setVisibility(true);
            frmMBBlockCardCCDBConfirm.flxCardImage.top = "0%";
            frmMBBlockCardCCDBConfirm.richtextSubheader.text = frmMBReIssueDBProduct.lblCardNumVal.text; //gblofferCardDetails["cardType"];// "Debit All Free"; //kony.i18n.getLocalizedString("");
            frmMBBlockCardCCDBConfirm.imgcard.src = frmMBReIssueDBProduct.imgCard.src; //debitCardImgSrc;
            frmMBBlockCardCCDBConfirm.flxCardDetails.top = "-102dp";
            frmMBBlockCardCCDBConfirm.flxCardDetails.setVisibility(true);
            frmMBBlockCardCCDBConfirm.flxcardMsg.setVisibility(false);
            frmMBBlockCardCCDBConfirm.flxSendAddress.setVisibility(true);
            frmMBBlockCardCCDBConfirm.flxSendAddressVal.setVisibility(true);
            frmMBBlockCardCCDBConfirm.btnchangeAdd.setVisibility(true);
            frmMBBlockCardCCDBConfirm.lblLinkAcct.text = kony.i18n.getLocalizedString("DBI05_LinkedAcc");
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmMBBlockCardCCDBConfirm.lblAcctName.text = gblofferCardDetails["acctNickNameEN"];
            } else {
                frmMBBlockCardCCDBConfirm.lblAcctName.text = gblofferCardDetails["acctNickNameTH"];
            }
            frmMBBlockCardCCDBConfirm.lblAcctNickName.text = gblofferCardDetails["accountNoFomatted"];
            frmMBBlockCardCCDBConfirm.lblCardNum.text = "";
            frmMBBlockCardCCDBConfirm.flxSendAddress.top = "-50dp";
            frmMBBlockCardCCDBConfirm.lblSendAddress.text = kony.i18n.getLocalizedString("DBI04_CardToAddr");
            //frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text=kony.i18n.getLocalizedString("");
            frmMBBlockCardCCDBConfirm.btnchangeAdd.text = kony.i18n.getLocalizedString("msgChangeAddrQuestion");
            frmMBBlockCardCCDBConfirm.btnback.onClick = onclickBackfrmMBBlockCardCCDBConfirm;
            frmMBBlockCardCCDBConfirm.btnnext.onClick = onclickNextfrmMBBlockCardCCDBConfirm;
        }
    }
}

function frmMBBlockCardCCDBConfirmMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}
//frmMBBlockCardSuccess
function frmMBBlockCardSuccessMenuPreshow() {
    changeStatusBarColor();
    kony.print("frmMBBlockCardSuccessMenuPreshow, blockNReissueSuccess = " + blockNReissueSuccess + ", blockNReissueTryagain = " + blockNReissueTryagain);
    if (gblCallPrePost) {
        kony.print("gblCCDBCardFlow  " + gblCCDBCardFlow);
        var cardheight = "40%";
        var gblDeviceInfo = kony.os.deviceInfo();
        var deviceName = gblDeviceInfo["name"];
        var screenwidth = gblDeviceInfo["deviceWidth"];
        var screenheight = gblDeviceInfo["deviceHeight"];
        //showAlert("screenheight " + screenheight+ "screenwidth "+screenwidth, kony.i18n.getLocalizedString("info"));
        if (deviceName == "android") {
            if ((screenwidth == 1080 && screenheight < 1920) || screenwidth == 480) {
                cardheight = "43%";
            } else if (screenwidth == 768 && screenheight < 1280) {
                cardheight = "46%";
            } else {
                cardheight = "40%";
            }
        } else {
            cardheight = "45%"; //37.75%
        }
        frmMBBlockCardSuccess.flxCardImage.width = "83%";
        frmMBBlockCardSuccess.flxCardImage.height = cardheight;
        frmMBBlockCardSuccess.flxCardImage.top = "2%";
        frmMBBlockCardSuccess.flxCardImage.left = "10.89%";
        frmMBBlockCardSuccess.flxCardImage.centerX = "50%";
        frmMBBlockCardSuccess.imgcard.top = "-8%";
        frmMBBlockCardSuccess.imgcard.width = "100%";
        frmMBBlockCardSuccess.imgcard.height = "100%";
        frmMBBlockCardSuccess.flexContent.top = "0";
        frmMBBlockCardSuccess.flexContent.left = "0";
        frmMBBlockCardSuccess.flexContent.width = "100%";
        frmMBBlockCardSuccess.flexContent.height = "40%";
        frmMBBlockCardSuccess.flexContent.centerX = "50%";
        frmMBBlockCardSuccess.flexContent.centerY = "65%";
        frmMBBlockCardSuccess.lblCardNumber.top = "5%";
        frmMBBlockCardSuccess.lblCardNumber.left = "10%";
        frmMBBlockCardSuccess.lblCardNumber.width = "90%";
        frmMBBlockCardSuccess.lblCardAccountName.top = "35%";
        frmMBBlockCardSuccess.lblCardAccountName.left = "10%";
        frmMBBlockCardSuccess.lblCardAccountName.width = "90%";
        frmMBBlockCardSuccess.lblRibbon.top = "10%";
        frmMBBlockCardSuccess.lblRibbon.right = "-12%";
        frmMBBlockCardSuccess.lblHdrTxt.text = kony.i18n.getLocalizedString("TRComplete_Title");
        frmMBBlockCardSuccess.btnback.text = kony.i18n.getLocalizedString("DBI05_BtnManageCard");
        frmMBBlockCardSuccess.btnback.onClick = onclickManageCard;
        frmMBBlockCardSuccess.btnback.focusSkin = "btnBlue200GreyBG";
        frmMBBlockCardSuccess.flxCardImage.skin = "flexWhiteBG";
        frmMBBlockCardSuccess.lblCardNumber.skin = getCardNoSkin();
        if (getDeviceOS() == "0") {
            frmMBBlockCardSuccess.imgcard.top = "-3%";
            frmMBBlockCardSuccess.lblCardNumber.top = "15%";
            frmMBBlockCardSuccess.lblCardAccountName.top = "52%";
            frmMBBlockCardSuccess.lblRibbon.right = "-18%";
            frmMBBlockCardSuccess.lblRibbon.top = "12%";
            frmMBBlockCardSuccess.lblRibbon.height = "16%";
        }
        //frmMBBlockCardSuccess.richtextCardMsg.skin = "richTxtBlack200";
        //frmMBBlockCardSuccess.flxCardImage.width = "90%";
        //frmMBBlockCardSuccess.flxCardImage.height = "41%";
        // frmMBBlockCardSuccess.imgcard.width = "95%";
        //        frmMBBlockCardSuccess.imgcard.height = "100%";
        //
        //        frmMBBlockCardSuccess.lblCardNumber.top = "53%";
        //        frmMBBlockCardSuccess.lblCardNumber.left = "10%";
        //        frmMBBlockCardSuccess.lblCardNumber.width = "80%";
        //        frmMBBlockCardSuccess.lblCardNumber.height = "15%";
        //
        //        frmMBBlockCardSuccess.lblCardAccountName.top = "67%";
        //        frmMBBlockCardSuccess.lblCardAccountName.left = "10%";
        //        frmMBBlockCardSuccess.lblCardAccountName.width = "80%";
        //        frmMBBlockCardSuccess.lblCardAccountName.height = "15%";
        //frmMBBlockCardSuccess.flxCardImage.height = "37.75%";
        //frmMBBlockCardSuccess.flxCardImage.width = "83%";
        //frmMBBlockCardSuccess.lblRibbon.top = "16%";
        //frmMBBlockCardSuccess.lblCardAccountName.top = "73%";
        //frmMBBlockCardSuccess.lblCardNumber.top = "59%";
        //frmMBBlockCardSuccess.lblCardAccountName.left = "12%";
        //frmMBBlockCardSuccess.lblCardNumber.left = "12%";
        if (gblCCDBCardFlow == "CREDIT_CARD_BLOCK") {
            if (blockNReissueSuccess) {
                //frmMBBlockCardSuccess.flxSubheader.height = "140dp";
                frmMBBlockCardSuccess.richtextSubheader.text = kony.i18n.getLocalizedString("msgBlockCardSuccess");
                frmMBBlockCardSuccess.imgComplete.src = "iconcomplete.png";
                frmMBBlockCardSuccess.flxCardImage.setVisibility(true);
                frmMBBlockCardSuccess.imgcard.src = frmMBManageCard.imgCard.src;
                frmMBBlockCardSuccess.lblCardNumber.text = frmMBBlockCardCCDBConfirm.lblCardNumber.text;
                frmMBBlockCardSuccess.lblCardAccountName.text = frmMBBlockCardCCDBConfirm.lblCardAccountName.text;
                frmMBBlockCardSuccess.flxCardDetails.setVisibility(false);
                frmMBBlockCardSuccess.flxSendAddress.top = "3%";
                frmMBBlockCardSuccess.flxSendAddress.setVisibility(true);
                frmMBBlockCardSuccess.flxSendAddressVal.setVisibility(true);
                frmMBBlockCardSuccess.lblSendAddress.text = kony.i18n.getLocalizedString("msgConfirmAddr");
                frmMBBlockCardSuccess.richtextSendAddressVal.text = frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text;
                frmMBBlockCardSuccess.flxTryAgain.setVisibility(false);
                frmMBBlockCardSuccess.flxCall.setVisibility(false);
                frmMBBlockCardSuccess.flxcardMsg.setVisibility(true);
                frmMBBlockCardSuccess.richtextCardMsg.text = kony.i18n.getLocalizedString("msgTransferToNewCard");
            } else {
                //frmMBBlockCardSuccess.flxSubheader.height = "160dp";
                frmMBBlockCardSuccess.richtextSubheader.text = kony.i18n.getLocalizedString("msgBlockCardFailed");
                frmMBBlockCardSuccess.imgComplete.src = "iconnotcomplete.png";
                frmMBBlockCardSuccess.flxCardImage.setVisibility(false);
                frmMBBlockCardSuccess.flxCardDetails.setVisibility(false);
                frmMBBlockCardSuccess.flxSendAddress.setVisibility(false);
                frmMBBlockCardSuccess.flxSendAddressVal.setVisibility(false);
                frmMBBlockCardSuccess.flxTryAgain.setVisibility(true);
                frmMBBlockCardSuccess.flxTryAgain.top = "20%";
                if (blockNReissueTryagain) {
                    frmMBBlockCardSuccess.btntryAgain.text = kony.i18n.getLocalizedString("DBI03_Tryagain");
                    frmMBBlockCardSuccess.btntryAgain.setVisibility(true);
                    frmMBBlockCardSuccess.btntryAgain.onClick = onclickTryAgainBlockCard;
                } else {
                    frmMBBlockCardSuccess.btntryAgain.setVisibility(false);
                }
                frmMBBlockCardSuccess.flxCall.setVisibility(true);
                frmMBBlockCardSuccess.btncall.text = kony.i18n.getLocalizedString("DBI03_Call1558");
                frmMBBlockCardSuccess.btncall.onClick = frmMBBlockCCChangeAddressOnClickBtnCallCenter;
                frmMBBlockCardSuccess.flxcardMsg.setVisibility(false);
            }
        } else if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
            if (blockNReissueSuccess) {
                //frmMBBlockCardSuccess.flxSubheader.height = "140dp";
                var newCardNo = gblofferCardDetails["newCardBinNo"].substring(0, 4) + " " + gblofferCardDetails["newCardBinNo"].substring(4, 6) + "XX XXXX XXXX";
                frmMBBlockCardSuccess.richtextSubheader.text = kony.i18n.getLocalizedString("IssueDB_Success");
                frmMBBlockCardSuccess.imgComplete.src = "iconcomplete.png";
                frmMBBlockCardSuccess.flxCardDetails.setVisibility(true);
                frmMBBlockCardSuccess.lblLinkAcct.setVisibility(true);
                frmMBBlockCardSuccess.lblAcctName.setVisibility(true);
                frmMBBlockCardSuccess.flxCardImage.setVisibility(false);
                frmMBBlockCardSuccess.lblLinkAcct.text = kony.i18n.getLocalizedString("DBI05_LinkedAcc");
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    frmMBBlockCardSuccess.lblAcctName.text = gblofferCardDetails["acctNickNameEN"];
                } else {
                    frmMBBlockCardSuccess.lblAcctName.text = gblofferCardDetails["acctNickNameTH"];
                }
                frmMBBlockCardSuccess.lblAcctNickName.text = gblofferCardDetails["cardType"];
                frmMBBlockCardSuccess.lblCardNum.text = newCardNo;
                frmMBBlockCardSuccess.flxTryAgain.setVisibility(false);
                frmMBBlockCardSuccess.flxCall.setVisibility(false);
                //frmMBBlockCardSuccess.btncall.text=kony.i18n.getLocalizedString("DBI03_Call1558");
                //frmMBBlockCardSuccess.btntryAgain.text=kony.i18n.getLocalizedString("DBI03_Tryagain");
                frmMBBlockCardSuccess.flxSendAddress.top = "-10dp";
                frmMBBlockCardSuccess.flxSendAddress.setVisibility(true);
                frmMBBlockCardSuccess.flxSendAddressVal.setVisibility(true);
                frmMBBlockCardSuccess.lblSendAddress.text = kony.i18n.getLocalizedString("DBI05_CardToAddr");
                frmMBBlockCardSuccess.richtextSendAddressVal.text = frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text;
                frmMBBlockCardSuccess.flxcardMsg.setVisibility(false);
            } else {
                //frmMBBlockCardSuccess.flxSubheader.height = "160dp";
                frmMBBlockCardSuccess.richtextSubheader.text = kony.i18n.getLocalizedString("IssueDB_Failed");
                frmMBBlockCardSuccess.flxCardImage.setVisibility(false);
                frmMBBlockCardSuccess.imgComplete.src = "iconnotcomplete.png";
                frmMBBlockCardSuccess.flxCardDetails.setVisibility(false);
                frmMBBlockCardSuccess.flxTryAgain.setVisibility(true);
                frmMBBlockCardSuccess.flxTryAgain.top = "20%";
                if (blockNReissueTryagain) {
                    frmMBBlockCardSuccess.btntryAgain.text = kony.i18n.getLocalizedString("DBI03_Tryagain");
                    frmMBBlockCardSuccess.btntryAgain.setVisibility(true);
                    frmMBBlockCardSuccess.btntryAgain.onClick = onclickTryAgainBlockCard;
                } else {
                    frmMBBlockCardSuccess.btntryAgain.setVisibility(false);
                }
                frmMBBlockCardSuccess.flxCall.setVisibility(true);
                frmMBBlockCardSuccess.btncall.text = kony.i18n.getLocalizedString("DBI03_Call1558");
                frmMBBlockCardSuccess.btncall.onClick = frmMBBlockCCChangeAddressOnClickBtnCallCenter;
                frmMBBlockCardSuccess.flxSendAddress.setVisibility(false);
                frmMBBlockCardSuccess.flxSendAddressVal.setVisibility(false);
                frmMBBlockCardSuccess.flxcardMsg.setVisibility(false);
            }
        } else if (gblCCDBCardFlow == "DEBIT_CARD_BLOCK") {
            frmMBBlockCardSuccess.flxSendAddress.setVisibility(false);
            frmMBBlockCardSuccess.flxSendAddressVal.setVisibility(false);
            if (blockNReissueSuccess) {
                //frmMBBlockCardSuccess.flxSubheader.height = "140dp";
                frmMBBlockCardSuccess.richtextSubheader.text = kony.i18n.getLocalizedString("msgBlockCardSuccess");
                frmMBBlockCardSuccess.imgComplete.src = "iconcomplete.png";
                frmMBBlockCardSuccess.flxCardDetails.setVisibility(false);
                frmMBBlockCardSuccess.lblCardNumber.text = frmMBBlockDebitCardConfirm.lblCardNumber.text;
                frmMBBlockCardSuccess.lblCardAccountName.text = frmMBBlockDebitCardConfirm.lblCardAccountName.text;
                //frmMBBlockCardSuccess.lblLinkAcct.setVisibility(false);
                //frmMBBlockCardSuccess.lblAcctName.setVisibility(false);
                //frmMBBlockCardSuccess.lblAcctNickName.text=kony.i18n.getLocalizedString("");
                //frmMBBlockCardSuccess.lblCardNum.text=kony.i18n.getLocalizedString("");
                frmMBBlockCardSuccess.flxCardImage.setVisibility(true);
                frmMBBlockCardSuccess.imgcard.src = getImageSRC(gblSelectedCard["IMG_CARD_ID"]);
                frmMBBlockCardSuccess.flxTryAgain.setVisibility(true);
                frmMBBlockCardSuccess.btntryAgain.setVisibility(true);
                frmMBBlockCardSuccess.flxTryAgain.top = "5%";
                frmMBBlockCardSuccess.btntryAgain.text = kony.i18n.getLocalizedString("DBI03_IssueNewCard");
                frmMBBlockCardSuccess.btntryAgain.onClick = debitCardInqServiceBlockCard;
                frmMBBlockCardSuccess.flxCall.setVisibility(false);
                frmMBBlockCardSuccess.flxSendAddress.setVisibility(false);
                frmMBBlockCardSuccess.flxSendAddressVal.setVisibility(false);
                frmMBBlockCardSuccess.flxcardMsg.setVisibility(false);
            } else {
                //frmMBBlockCardSuccess.flxSubheader.height = "160dp";
                frmMBBlockCardSuccess.richtextSubheader.text = kony.i18n.getLocalizedString("msgBlockCardFailed");
                frmMBBlockCardSuccess.flxCardImage.setVisibility(false);
                frmMBBlockCardSuccess.imgComplete.src = "iconnotcomplete.png";
                frmMBBlockCardSuccess.flxCardDetails.setVisibility(false);
                frmMBBlockCardSuccess.flxTryAgain.setVisibility(true);
                frmMBBlockCardSuccess.flxTryAgain.top = "20%";
                if (blockNReissueTryagain) {
                    frmMBBlockCardSuccess.btntryAgain.text = kony.i18n.getLocalizedString("DBI03_Tryagain");
                    frmMBBlockCardSuccess.btntryAgain.setVisibility(true);
                    frmMBBlockCardSuccess.btntryAgain.onClick = onclickTryAgainBlockCard;
                } else {
                    frmMBBlockCardSuccess.btntryAgain.setVisibility(false);
                }
                frmMBBlockCardSuccess.flxCall.setVisibility(true);
                frmMBBlockCardSuccess.btncall.text = kony.i18n.getLocalizedString("DBI03_Call1558");
                frmMBBlockCardSuccess.btncall.onClick = frmMBBlockCCChangeAddressOnClickBtnCallCenter;
                frmMBBlockCardSuccess.flxSendAddress.setVisibility(false);
                frmMBBlockCardSuccess.flxSendAddressVal.setVisibility(false);
                frmMBBlockCardSuccess.flxcardMsg.setVisibility(false);
            }
        }
    }
}

function frmMBBlockCardSuccessMenuPostshow() {
    if (gblCallPrePost) {
        animateribbon();
        //..
    }
    assignGlobalForMenuPostshow();
}
//frmMBBlockCCChangeAddress
function frmMBBlockCCChangeAddressMenuPreshow() {
    changeStatusBarColor();
    frmMBBlockCCChangeAddress.lblHeaderContent.text = kony.i18n.getLocalizedString("msgHowtoChangeAddrQuestion");
    frmMBBlockCCChangeAddress.rchTxtContent.text = kony.i18n.getLocalizedString("msgHowtoChangeAddr");
    frmMBBlockCCChangeAddress.btnCallCenter.text = kony.i18n.getLocalizedString("DBI03_Call1558");
}

function frmMBBlockCCChangeAddressMenuPostshow() {
    try {
        kony.application.getCurrentForm().statusBarColor = "64FFFFFF"; //transparency
        kony.application.getCurrentForm().scrollboxMain.scrollToEnd();
    } catch (error) {}
}