var glbPin = "";
var glbPinArr = [];
var glbRandomRang = [];
var glbDigitWeight = [];
var glbIndex = 0;
var maxLenPin = 4;
var iFlag = true;
var localeIndex = 0;
var isConfirmPin = false;
var emptyPinSkin = "btnIBEmpyATMPin";
var fillPinSkin = "btnIBFillATMPin";
var disablePinSkin = "btnIBEmptyATMPINDisable";
var maskCardNo;
var maxLenCVV = 3;

function frmMBAssignAtmPinpreShow() {
    changeStatusBarColor();
    frmMBAssignAtmPin.lblHdrTxt.text = kony.i18n.getLocalizedString("keyEnterPin");
    frmMBAssignAtmPin.btnCancel1.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmMBAssignAtmPin.btnNext.text = kony.i18n.getLocalizedString("Next");
    frmMBAssignAtmPin.lblEnterAtmPin.text = kony.i18n.getLocalizedString("keySet4DigitPin");
    frmMBAssignAtmPin.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("keyConfirm4DigitPin");
    frmMBAssignAtmPin.lblCardActivateNote.text = kony.i18n.getLocalizedString("keyActivateCardNote");
}

function initializeAndNavigatetoPinAssign() {
    initialParam();
    clearATMPINMB();
    frmMBAssignAtmPin.show();
}

function clearATMPINMB() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    toggleButtonMB(false);
    toggleConfirmPINMB(false);
    //toggleKeyPad(false);
    isConfirmPin = false;
    assignATMPINRenderMB("");
}

function toggleConfirmPINMB(command) {
    frmMBAssignAtmPin.hbxAtmPinConfirm.setVisibility(command);
    frmMBAssignAtmPin.lblConfirmAtmPin.setVisibility(command);
    if (!command) {
        frmMBAssignAtmPin.hbxKeyPad.margin = [18, 30, 18, 0];
    } else {
        frmMBAssignAtmPin.hbxKeyPad.margin = [18, 8, 18, 0];
    }
}

function decreaseATMPINValMB() {
    --glbIndex;
    glbPin = glbPin.substring(0, glbPin.length - 1);
    assignATMPINRenderMB("");
    toggleButtonMB(false);
}

function toggleButtonMB(command) {
    if (command) {
        frmMBAssignAtmPin.btnNext.skin = "btnBlueSkin";
        frmMBAssignAtmPin.btnNext.hoverSkin = "btnBlueSkin";
        frmMBAssignAtmPin.btnNext.focusSkin = "btnBlueSkin";
    } else {
        frmMBAssignAtmPin.btnNext.skin = "btnDisabledGray";
        frmMBAssignAtmPin.btnNext.hoverSkin = "btnDisabledGray";
        frmMBAssignAtmPin.btnNext.focusSkin = "btnDisabledGray";
    }
    frmMBAssignAtmPin.btnNext.setEnabled(command);
}

function toggleKeyPadMB(command) {
    isConfirmPin = command;
    var isConfirmBox = frmMBAssignAtmPin.hbxAtmPinConfirm.isVisible;
    /*if(!isConfirmBox){
    	if(command){
    		frmIBCardActivation.hbxKeyPad.margin = [0,5,0,0];
    	}else{
    		frmIBCardActivation.hbxKeyPad.margin = [0,28.7,0,0];
    	}
    }*/
}

function deleteATMPINMB() {
    if (isConfirmPin) {
        if (isNotBlank(glbPin)) {
            decreaseATMPINValMB();
        } else {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
            toggleConfirmPINMB(false);
            isConfirmPin = false; //toggleKeyPad(false);
            decreaseATMPINValMB();
        }
    } else {
        if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenPin) {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
        }
        if (isNotBlank(glbPin)) {
            decreaseATMPINValMB();
            toggleConfirmPINMB(false);
            isConfirmPin = false; //toggleKeyPad(false);
        }
    }
}

function assignATMPINRenderMB(pin) {
    if (isNotBlank(pin)) {
        if (isConfirmPin) {
            if (glbPin.length < maxLenPin) {
                addConfirmPinMB(pin, ++glbIndex);
            }
        } else {
            addPinMB(pin, ++glbIndex);
        }
    } else {
        renderPINMB();
    }
}

function renderPINMB() {
    var btnObject = [];
    if (isConfirmPin) {
        btnObject = [frmMBAssignAtmPin.imgPin1Confirm, frmMBAssignAtmPin.imgPin2Confirm, frmMBAssignAtmPin.imgPin3Confirm, frmMBAssignAtmPin.imgPin4Confirm];
    } else {
        btnObject = [frmMBAssignAtmPin.imgPin1, frmMBAssignAtmPin.imgPin2, frmMBAssignAtmPin.imgPin3, frmMBAssignAtmPin.imgPin4];
    }
    var emptyPinDis = "square_empty_disable.png";
    var emptyPin = "square_empty.png";
    var fillPin = "square_filled.png";
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].src = fillPin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].src = emptyPin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].src = emptyPinDis;
        }
    }
}

function addConfirmPinMB(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderPINMB();
    if (glbPin.length == maxLenPin) {
        toggleButtonMB(true);
    }
}

function addPinMB(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderPINMB();
    if (glbPin.length == maxLenPin) {
        //toggleKeyPad(true);
        toggleConfirmPINMB(true);
        isConfirmPin = true;
        glbPinArr[0] = glbPin;
        glbPin = "";
        glbIndex = 0;
        renderPINMB();
    }
}

function initializeAndNavigatetoVerifyCVV() {
    initialParam();
    clearCVVMB();
    frmMBActivateEnterCVV.show();
}

function frmMBActivateEnterCVVPreShow() {
    frmMBActivateEnterCVV.lblHdrTxt.text = kony.i18n.getLocalizedString("keyActivateMobileApp");
    frmMBActivateEnterCVV.lblEnterCreditCardCVV.text = kony.i18n.getLocalizedString("Act_EnterCVV_CVV");
    frmMBActivateEnterCVV.lblCVVHelpNote.text = kony.i18n.getLocalizedString("Act_EnterCVV_Details");
    frmMBActivateEnterCVV.btnCancel1.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmMBActivateEnterCVV.btnNext.text = kony.i18n.getLocalizedString("Next");
    gblLocale = false;
}

function verifyCVVRenderMB(pin) {
    if (isNotBlank(pin)) {
        if (glbPin.length < maxLenCVV) {
            addCVVMB(pin, ++glbIndex);
        }
    } else {
        renderCVVMB();
    }
}

function addCVVMB(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderCVVMB();
    if (glbPin.length == maxLenCVV) {
        toggleButtonMBCVV(true);
    }
}

function toggleButtonMBCVV(command) {
    if (command) {
        frmMBActivateEnterCVV.btnNext.skin = "btnBlueSkin";
        frmMBActivateEnterCVV.btnNext.hoverSkin = "btnBlueSkin";
        frmMBActivateEnterCVV.btnNext.focusSkin = "btnBlueSkin";
    } else {
        frmMBActivateEnterCVV.btnNext.skin = "btnDisabledGray";
        frmMBActivateEnterCVV.btnNext.hoverSkin = "btnDisabledGray";
        frmMBActivateEnterCVV.btnNext.focusSkin = "btnDisabledGray";
    }
    frmMBActivateEnterCVV.btnNext.setEnabled(command);
}

function renderCVVMB() {
    var btnObject = [];
    btnObject = [frmMBActivateEnterCVV.imgPin1, frmMBActivateEnterCVV.imgPin2, frmMBActivateEnterCVV.imgPin3];
    var emptyPinDis = "square_empty_disable.png";
    var emptyPin = "square_empty.png";
    var fillPin = "square_filled.png";
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].src = fillPin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].src = emptyPin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].src = emptyPinDis;
        }
    }
}

function clearCVVMB() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    toggleButtonMBCVV(false);
    verifyCVVRenderMB("");
}

function deleteCVVMB() {
    if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenCVV) {
        glbPin = glbPinArr[0];
        glbIndex = glbPinArr[0].length;
        glbPinArr = [];
    }
    if (isNotBlank(glbPin)) {
        decreaseCVVValMB();
        toggleButtonMBCVV(false);
    }
}

function decreaseCVVValMB() {
    --glbIndex;
    glbPin = glbPin.substring(0, glbPin.length - 1);
    verifyCVVRenderMB("");
}

function initializeAndNavigatetoVerifyPin() {
    initialParam();
    clearVerifyPinMB();
    frmMBEnterATMPin.show();
}

function verifyPinRenderMB(pin) {
    if (isNotBlank(pin)) {
        if (glbPin.length < maxLenPin) {
            addVerifyPinMB(pin, ++glbIndex);
        }
    } else {
        renderVerifyPinMB();
    }
}

function addVerifyPinMB(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderVerifyPinMB();
    if (glbPin.length == maxLenPin) {
        toggleButtonMBVerifyPin(true);
    }
}

function toggleButtonMBVerifyPin(command) {
    if (command) {
        frmMBEnterATMPin.btnNext.skin = "btnBlueSkin";
        frmMBEnterATMPin.btnNext.hoverSkin = "btnBlueSkin";
        frmMBEnterATMPin.btnNext.focusSkin = "btnBlueSkin";
    } else {
        frmMBEnterATMPin.btnNext.skin = "btnDisabledGray";
        frmMBEnterATMPin.btnNext.hoverSkin = "btnDisabledGray";
        frmMBEnterATMPin.btnNext.focusSkin = "btnDisabledGray";
    }
    frmMBEnterATMPin.btnNext.setEnabled(command);
}

function renderVerifyPinMB() {
    var btnObject = [];
    btnObject = [frmMBEnterATMPin.imgPin1, frmMBEnterATMPin.imgPin2, frmMBEnterATMPin.imgPin3, frmMBEnterATMPin.imgPin4];
    var emptyPinDis = "square_empty_disable.png";
    var emptyPin = "square_empty.png";
    var fillPin = "square_filled.png";
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].src = fillPin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].src = emptyPin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].src = emptyPinDis;
        }
    }
}

function clearVerifyPinMB() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    toggleButtonMBVerifyPin(false);
    verifyPinRenderMB("");
}

function deleteVerifyPinMB() {
    if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenPin) {
        glbPin = glbPinArr[0];
        glbIndex = glbPinArr[0].length;
        glbPinArr = [];
    }
    if (isNotBlank(glbPin)) {
        decreaseVerifyPinValMB();
        toggleButtonMBVerifyPin(false);
    }
}

function decreaseVerifyPinValMB() {
    --glbIndex;
    glbPin = glbPin.substring(0, glbPin.length - 1);
    verifyPinRenderMB("");
}