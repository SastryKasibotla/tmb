function otpValidationspaActivation(text) {
    //kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
    otpCodePattStr = "^[0-9]{" + gblOTPLENGTH + "}$";
    var otpCodePatt = new RegExp(otpCodePattStr, "g");
    resultOtpCodePatt = otpCodePatt.test(text);
    if (resultOtpCodePatt) {
        //	kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        var inputParam = {};
        inputParam["tokenUUID"] = gblTokenNum;
        inputParam["otp"] = text;
        inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
        inputParam["session"] = "session";
        invokeServiceSecureAsync("verifyOTP", inputParam, callBackOTPVerifyspaChngecreds)
    } else {
        var invlidOTP = kony.i18n.getLocalizedString("invalidOTP");
        var info1 = kony.i18n.getLocalizedString("info");
        var okk = kony.i18n.getLocalizedString("keyOK");
        //Defining basicConf parameter for alert
        var basicConf = {
            message: invlidOTP,
            alertType: constants.ALERT_TYPE_INFO,
            alertTitle: info1,
            yesLabel: okk,
            noLabel: "",
            alertHandler: handle2
        };
        kony.application.dismissLoadingScreen();
        //Defining pspConf parameter for alert
        var pspConf = {};
        //Alert definition
        var infoAlert = kony.ui.Alert(basicConf, pspConf);

        function handle2(response) {}
        //	alert("INVALID ACTIVATION CODE");
        return false;
    }
}

function onClickOTPSpaAactivate() {
    if (gblOTPFlag) {
        popupTractPwd.btnPopUpTractCancel.skin = btnDisabledGray;
        popupTractPwd.btnPopUpTractCancel.setEnabled(false);
        var inputParam = {};
        //inputParam["tokenUUID"] = gblTokenNum
        //inputParam["sessionId"] = "session";
        /*	inputParam["policyId"] = "";
		
		var locale=kony.i18n.getCurrentLocale();
		// For Activation Flow
		if(gblActionCode == "21"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPActivate_EN";
				inputParam["SMS_Subject"] = "MIB_OTPActivateMB_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPActivate_TH";
				inputParam["SMS_Subject"] = "MIB_OTPActivateMB_TH";
			} 
		}// For Add device Flow
		else if(gblActionCode == "23"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPAddDevice_EN";
				inputParam["SMS_Subject"] = "MIB_OTPAddDevice_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPAddDevice_TH";
				inputParam["SMS_Subject"] = "MIB_OTPAddDevice_TH";
			} 
		}// For Reset Password Flow
		else if(gblActionCode == "22"){
			if(locale=="en_US"){
				
				inputParam["eventNotificationPolicyId"] = "MIB_OTPResetPWD_EN";
				inputParam["SMS_Subject"] = "MIB_OTPResetPWDMB_EN";
			}
			else{
				inputParam["eventNotificationPolicyId"] = "MIB_OTPResetPWD_TH";
				inputParam["SMS_Subject"] = "MIBOTPResetPWDMB_TH";
			} 
		}
		
		
		
		inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;  
	    inputParam["storeId"] = "";
	    inputParam["Batch_No"] = "";
	    inputParam["Bank_Ref"] = "";
	    inputParam["Product_Code"] = "";
	    inputParam["Recipient_Name"] = "";
	    inputParam["MobileNumber"] = gblPHONENUMBER;
	    inputParam["Channel"] = "";
	    inputParam["inputSeed"]="";
		invokeServiceSecureAsync("requestOTP",inputParam,CallbackonClickOTPRequest);*/
        var locale = kony.i18n.getCurrentLocale();
        var spaChannel
            // For Activation Flow
        if (flowSpa) //if else condition for SPA
        {
            if (gblActionCode == "11") {
                if (locale == "en_US") {
                    inputParam["eventNotificationId"] = "MIB_OTPActivate_EN";
                    inputParam["smsSubject"] = "MIB_OTPActivateIB_EN";
                    spaChannel = "IB-ACTIVATION";
                } else {
                    inputParam["eventNotificationId"] = "MIB_OTPActivate_TH";
                    inputParam["smsSubject"] = "MIB_OTPActivateIB_TH";
                    spaChannel = "IB-ACTIVATION";
                }
            } else if (gblActionCode == "12" || gblActionCode == "13") {
                if (locale == "en_US") {
                    inputParam["eventNotificationId"] = "MIB_OTPResetPWD_EN";
                    inputParam["smsSubject"] = "MIB_OTPResetPWDIB_EN";
                    spaChannel = "IB-ACTIVATION";
                } else {
                    inputParam["eventNotificationId"] = "MIB_OTPResetPWD_TH";
                    inputParam["smsSubject"] = "MIB_OTPResetPWDIB_TH";
                    spaChannel = "IB-ACTIVATION";
                }
            } else if (spaChnage == "chguseridib") {
                inputParam["eventNotificationId"] = "MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
                inputParam["smsSubject"] = "MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
                spaChannel = "IB-CHANGEUSERID";
            } else if (spaChnage == "chgpwdidib") {
                inputParam["eventNotificationId"] = "MIB_ChangeIBPWD_" + kony.i18n.getCurrentLocale();
                inputParam["smsSubject"] = "MIB_ChangeIBPWD_" + kony.i18n.getCurrentLocale();
                spaChannel = "IB-CHANGEPWD";
            }
        } else {
            if (gblActionCode == "21") {
                if (locale == "en_US") {
                    inputParam["eventNotificationId"] = "MIB_OTPActivate_EN";
                    inputParam["smsSubject"] = "MIB_OTPActivateMB_EN";
                } else {
                    inputParam["eventNotificationId"] = "MIB_OTPActivate_TH";
                    inputParam["smsSubject"] = "MIB_OTPActivateMB_TH";
                }
            } // For Add device Flow
            else if (gblActionCode == "23") {
                if (locale == "en_US") {
                    inputParam["eventNotificationId"] = "MIB_OTPAddDevice_EN";
                    inputParam["smsSubject"] = "MIB_OTPAddDevice_EN";
                } else {
                    inputParam["eventNotificationId"] = "MIB_OTPAddDevice_TH";
                    inputParam["smsSubject"] = "MIB_OTPAddDevice_TH";
                }
            } // For Reset Password Flow
            else if (gblActionCode == "22") {
                // this condition should not be reached for SPA
                if (locale == "en_US") {
                    inputParam["eventNotificationId"] = "MIB_OTPResetPWDMB_EN";
                    inputParam["smsSubject"] = "MIB_OTPResetPWDMB_EN";
                } else {
                    inputParam["eventNotificationId"] = "MIB_OTPResetPWDMB_TH";
                    inputParam["smsSubject"] = "MIBOTPResetPWDMB_TH";
                }
            }
        }
        inputParam["bankRef"] = "";
        inputParam["batchNo"] = "";
        if (flowSpa) //for SPA
        {
            inputParam["Channel"] = spaChannel;
        } else {
            inputParam["channel"] = "";
        }
        // inputParam["eventNotificationId"] = "";
        inputParam["inputSeedString"] = "";
        //inputParam["mobileNo"] = gblPHONENUMBER; //"0865832721" Mobile num VIT offshore; mib xpress inq already has it
        inputParam["param"] = "";
        inputParam["entries"] = "";
        inputParam["key"] = "";
        inputParam["value"] = "";
        inputParam["policyId"] = "";
        inputParam["productCode"] = "";
        inputParam["receipientName"] = "";
        inputParam["session"] = "";
        //inputParam["smsSubject"] = ""; 
        inputParam["storeId"] = "";
        inputParam["tokenUUID"] = "";
        inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
        invokeServiceSecureAsync("RequestOTPKony", inputParam, CallbackonClickOTPRequestSpaActivate);
    }
}

function CallbackonClickOTPRequestSpaActivate(status, resulttable) {
    if (resulttable["errCode"] == "GenOTPRtyErr00002") {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (resulttable["errCode"] == "GenOTPRtyErr00001") {
        //alert("Test1");
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["errCode"] == "GenOTPRtyErr00002") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            kony.timer.schedule("otpTimer", otpTimerCallBackSpa, reqOtpTimer, false);
            if (gblOnClickReq == false) {
                showOTPPopupSpa(kony.i18n.getLocalizedString("keyOTP"), "ABCD", "xxx-xxx-" + gblPHONENUMBER.substring(6, 10), otpValidationspaActivation, 1)
                    //(kony.i18n.getLocalizedString("keyOTP"), "ABCD","xxx-xxx-" + gblPHONENUMBER.substring(6, 10),otpValidationspa, 1)
            }
            //var refVal="";
            popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + resulttable["pac"];
            //popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + refVal;
            if (flowSpa) {
                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("keyotpmsg");
                popOtpSpa.lblPopupTract7.text = "";
                popOtpSpa.lblPopupTract4Spa.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            } else {
                popOtpSpa.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
                popOtpSpa.lblPopupTract7.text = "";
                popOtpSpa.lblPopupTract4.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            }
            gblOTPFlag = false;
        } else {
            popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + resulttable["pac"];
            gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
            var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
            gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
            kony.timer.schedule("otpTimer", otpTimerCallBack, reqOtpTimer, false);
            if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                gblOTPFlag = false;
                return false;
            } else {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                gblOTPFlag = false;
                return false;
            }
        }
        kony.application.dismissLoadingScreen();
    }
}