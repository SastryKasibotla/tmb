function displayPostLoginMenu() {
    var gblDeviceInfomenu = kony.os.deviceInfo();
    frmMenu.btnMenuTransfer.text = kony.i18n.getLocalizedString("keymenuTransfer");
    frmMenu.btnMenuBillPay.text = kony.i18n.getLocalizedString("MenuBillPayment");
    frmMenu.btnMenuTopUp.text = kony.i18n.getLocalizedString("TopUp");
    frmMenu.btnMenuAccntSumry.text = kony.i18n.getLocalizedString("accountSummary");
    frmMenu.btnMenuActivities.text = kony.i18n.getLocalizedString("MyActivities");
    frmMenu.btnMenuOffers.text = kony.i18n.getLocalizedString("MenuHotPromotions");
    frmMenu.lblinboxmenu.text = kony.i18n.getLocalizedString("MenuMyInbox");
    frmMenu.btnMenuSettings.text = kony.i18n.getLocalizedString("MenuAboutMe");
    frmMenu.btnMenuMore.text = kony.i18n.getLocalizedString("More");
    frmMenu.btnThai.text = kony.i18n.getLocalizedString("languageThai");
    frmMenu.btnEng.text = kony.i18n.getLocalizedString("languageEng");
    frmMenu.lbllogout.text = kony.i18n.getLocalizedString("keySPAmenuLogout");
    frmMenu.lbllogoutsmall.text = kony.i18n.getLocalizedString("keySPAmenuLogout");
    var screenwidth = gblDeviceInfomenu["deviceWidth"];
    if (screenwidth < 750) {
        frmMenu.flexNormal.setVisibility(false);
        frmMenu.flexSmall.setVisibility(true);
    } else {
        frmMenu.flexNormal.setVisibility(true);
        frmMenu.flexSmall.setVisibility(false);
    }
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        if (frmMenu.flexNormal.isVisible) {
            frmMenu.lbllastlogintimestamp.text = kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin") + " " + gbllastMbLoginSucessDateEng;
        } else {
            frmMenu.lbllastlogintimestampsmall.text = kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin")
            frmMenu.lbllastlogintimestampval.text = gbllastMbLoginSucessDateEng
        }
        frmMenu.btnEng.skin = btnEngMenu;
        frmMenu.btnEng.zIndex = 2;
        frmMenu.btnThai.skin = btnThaiMenuLight;
        frmMenu.btnThai.zIndex = 1;
        if (isNotBlank(gblCustomerName)) {
            frmMenu.lblfirstName.text = kony.i18n.getLocalizedString("khun");
            frmMenu.lblLastname.text = gblCustomerName.split(" ")[0];
        }
    } else {
        if (frmMenu.flexNormal.isVisible) {
            frmMenu.lbllastlogintimestamp.text = kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin") + " " + gbllastMbLoginSucessDateThai;
        } else {
            frmMenu.lbllastlogintimestampsmall.text = kony.i18n.getLocalizedString("keyMBHeaderlblLastLogin");
            frmMenu.lbllastlogintimestampval.text = gbllastMbLoginSucessDateThai
        }
        frmMenu.btnEng.skin = btnThaiMenuLight;
        frmMenu.btnEng.zIndex = 1;
        frmMenu.btnThai.skin = btnEngMenu;
        frmMenu.btnThai.zIndex = 2;
        if (isNotBlank(gblCustomerNameTh)) {
            frmMenu.lblfirstName.text = kony.i18n.getLocalizedString("khun");
            frmMenu.lblLastname.text = gblCustomerNameTh.split(" ")[0];
        }
    }
    frmMenu.lblversion.text = kony.i18n.getLocalizedString("keyVersion") + " " + appConfig.appVersion;
    frmMenu.lblcopyright.text = getCopyRightText();
    //setting the badge value to the button **** Total Count of Unread and Messages count from unreadInboxMessagesTrackerLocalDBSyncCB
    frmMenu.btnbadge.skin = btnBadgeSmall;
    frmMenu.btnbadge.setVisibility(false);
    if (gblMyInboxTotalCountMB > 0) {
        frmMenu.btnbadge.text = gblMyInboxTotalCountMB.toString();
        frmMenu.btnbadge.setVisibility(true);
    } else {
        frmMenu.btnbadge.setVisibility(false);
    }
    //Adding the segment Data
    // Onclick of Settings
    var touchIdMenu = "";
    if (gblTouchDevice == true) {
        touchIdMenu = kony.i18n.getLocalizedString("keyTouchIDMenu");
        var segSettingsTable = [{
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyProfile")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMyAccount")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("menuCardManagement")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("MIB_AnyIDMenu")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keyEStmt")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyRecipients")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMybills")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("myTopUpsMB")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("SQB_Title")
        }, {
            lblSettingsItem: touchIdMenu
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("UnlockIBanking")
        }]
    } else {
        var segSettingsTable = [{
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyProfile")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMyAccount")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("menuCardManagement")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("MIB_AnyIDMenu")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keyEStmt")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keyMyRecipients")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblMybills")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("myTopUpsMB")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("SQB_Title")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice")
        }, {
            lblSettingsItem: kony.i18n.getLocalizedString("UnlockIBanking")
        }]
    }
    //Adding code to More Segment
    var segMoreTable = [{
            lblmoreitems: kony.i18n.getLocalizedString("kelblOpenNewAccount")
        }, {
            lblmoreitems: kony.i18n.getLocalizedString("InternetBanking")
        }, {
            lblmoreitems: kony.i18n.getLocalizedString("MenuFindTMB")
        }, {
            lblmoreitems: kony.i18n.getLocalizedString("MenuExRates")
        }, {
            lblmoreitems: kony.i18n.getLocalizedString("MenuTour")
        }, {
            lblmoreitems: kony.i18n.getLocalizedString("keyContactUs")
        }, {
            lblmoreitems: kony.i18n.getLocalizedString("keyTermsNConditions")
        }, {
            lblmoreitems: kony.i18n.getLocalizedString("keySecurityAdvices")
        }]
        //Add Items for Notifications Segment
        //gblUnreadCount=0;
        //gblMessageCount=0;
    gblNotificationData = [];
    gblMessageData = [];
    //changes done for MyInbox badge requirement
    if (parseInt(gblUnreadCount) > 0) {
        gblNotificationData = {
            "text": gblUnreadCount,
            "isVisible": true,
            "skin": "btnBadgeSmall"
        };
    } else {
        gblNotificationData = {
            "text": gblUnreadCount,
            "isVisible": false,
            "skin": "btnBadgeSmall"
        };
    }
    if (parseInt(gblMessageCount) > 0) {
        gblMessageData = {
            "text": gblMessageCount,
            "isVisible": true,
            "skin": "btnBadgeSmall"
        };
    } else {
        gblMessageData = {
            "text": gblMessageCount,
            "isVisible": false,
            "skin": "btnBadgeSmall"
        };
    }
    var SegMyinboxTable = [{
        lblNotifications: kony.i18n.getLocalizedString("keylblNotification"),
        btnBadge: gblNotificationData
    }, {
        lblNotifications: kony.i18n.getLocalizedString("keylblmessage"),
        btnBadge: gblMessageData
    }]
    frmMenu.segInbox.setData(SegMyinboxTable);
    frmMenu.segMore.setData(segMoreTable);
    if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06') {
        frmMenu.segMore.removeAt(1);
    }
    //filter out Prompt Pay and Estatement Move to settings
    /* Commnenting as per new Req
    if (!flowSpa) {
        if (gblShowAnyIDRegistration == "false") {
            frmMenu.segMore.removeAt(0);
        }
        if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06') {
            if (gblShowAnyIDRegistration == "true") {
                frmMenu.segMore.removeAt(2);
            } else {
                frmMenu.segMore.removeAt(1);
            }
        }
        if (!isShowEStatementMenu()) {
            if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06') {
                if (gblShowAnyIDRegistration == "true") {
                    frmMenu.segMore.removeAt(2);
                } else {
                    frmMenu.segMore.removeAt(1);
                }
            } else {
                if (gblShowAnyIDRegistration == "true") {
                    frmMenu.segMore.removeAt(3);
                } else {
                    frmMenu.segMore.removeAt(2);
                }
            }
        }
    } else {

        //Removed as part of ENH_207_7
    } */
    frmMenu.segSettings.setData(segSettingsTable);
    //filter out
    // Filter out Prompt Pay
    //Prompt Pay False removing
    if (gblShowAnyIDRegistration == "false") {
        frmMenu.segSettings.removeAt(3);
    }
    // Estatement False removing
    if (!isShowEStatementMenu()) {
        //Prompt Pay true Remove At 3 else location 2
        if (gblShowAnyIDRegistration == "true") {
            frmMenu.segSettings.removeAt(4);
        } else {
            frmMenu.segSettings.removeAt(3);
        }
    }
    // if Quick Balance is set to False
    if (gblQuickBalanceEnable != "ON") {
        //Promptpay is true and Estmnt is true
        if (gblShowAnyIDRegistration == "true" && isShowEStatementMenu()) {
            frmMenu.segSettings.removeAt(8);
        }
        //Promptpay is false and Estmnt is false
        else if (gblShowAnyIDRegistration == "false" && !isShowEStatementMenu()) {
            frmMenu.segSettings.removeAt(6);
        }
        //Promptpay or Estmnt if any one of them is false/true
        else {
            frmMenu.segSettings.removeAt(7);
        }
    }
    // Touch Id is set to false
    if (gblIBFlowStatus == '00' || gblIBFlowStatus == '01' || gblIBFlowStatus == '08' || gblIBFlowStatus == '07') {
        if (gblTouchDevice == true) {
            //Promptpay is true and Estmnt is true and QuickBalance is true
            if (gblShowAnyIDRegistration == "true" && isShowEStatementMenu() && gblQuickBalanceEnable == "ON") {
                frmMenu.segSettings.removeAt(9);
            } else if (gblShowAnyIDRegistration == "true" && !isShowEStatementMenu() && gblQuickBalanceEnable != "ON") {
                frmMenu.segSettings.removeAt(7);
            } else if (gblShowAnyIDRegistration == "false" && !isShowEStatementMenu() && gblQuickBalanceEnable == "ON") {
                frmMenu.segSettings.removeAt(7);
            } else if (gblShowAnyIDRegistration == "false" && isShowEStatementMenu() && gblQuickBalanceEnable != "ON") {
                frmMenu.segSettings.removeAt(7);
            }
            //
            else {
                frmMenu.segSettings.removeAt(6);
            }
        } else {
            if (gblShowAnyIDRegistration == "true" && isShowEStatementMenu() && gblQuickBalanceEnable == "ON") {
                frmMenu.segSettings.removeAt(8);
            } else if (gblShowAnyIDRegistration == "true" && !isShowEStatementMenu() && gblQuickBalanceEnable != "ON") {
                frmMenu.segSettings.removeAt(6);
            } else if (gblShowAnyIDRegistration == "false" && !isShowEStatementMenu() && gblQuickBalanceEnable == "ON") {
                frmMenu.segSettings.removeAt(6);
            } else if (gblShowAnyIDRegistration == "false" && isShowEStatementMenu() && gblQuickBalanceEnable != "ON") {
                frmMenu.segSettings.removeAt(6);
            }
            //
            else {
                frmMenu.segSettings.removeAt(5);
            }
        }
    }
    if (!dynamicMenuLoaded) {
        dynamicPromotion();
    }
    //frmMenu.FlexScrollContainer0e763baec53954e.scrollsToTop=true;
    frmMenu.show();
    resetMenuEvents();
}

function setSwipeLanguageToggle() {
    var setSwipeButton = {
        fingers: 1,
        swipedistance: 90,
        swipevelocity: 75,
        swipeDirection: 1
    };
    frmMenu.FlexContainer0959d0209173044.addGestureRecognizer(2, setSwipeButton, myButtonSwipeCallback);
    frmMenu.statusBarColor = "007ABC00";
}

function myButtonSwipeCallback() {
    var locale = kony.i18n.getCurrentLocale();
    var eventObj = [];
    if (locale == "en_US") {
        eventObj["id"] = "btnThai";
        changeLocale(eventObj);
    } else {
        eventObj["id"] = "btnEng";
        changeLocale(eventObj);
    }
}

function dynamicPromotion() {
    var btnid = "";
    var btnname = "";
    var flexid = "";
    var onClickDynamickEvent = "";
    if (gblMenuConfig.length >= 1) {
        dynamicMenuLoaded = true;
        frmMenu.flexdynamicContainer.setVisibility(true);
        for (var i = 0; i < gblMenuConfig.length; i++) {
            btnid = "btnDynamicConfig" + i;
            btnname = eval(gblMenuConfig[i]["DISPLAYKEY"]);
            onClickDynamickEvent = eval(gblMenuConfig[i]["EVENT"]);
            flexid = "flexDynamicContainer" + i;
            btnid = new kony.ui.Button({
                "id": btnid,
                "top": "0dp",
                "left": "0dp",
                "width": "100%",
                "height": "44dp",
                "zIndex": 1,
                "isVisible": true,
                "text": btnname,
                "skin": "btn20pxMenuWhite",
                "focusSkin": "btn20pxMenuWhite",
                "onClick": onClickDynamickEvent
            }, {
                "padding": [20, 0, 0, 0],
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "marginInPixel": false,
                "paddingInPixel": false,
                "containerWeight": 0
            }, {
                "glowEffect": false,
                "showProgressIndicator": true
            });
            if (i == 0) {
                flexid = new kony.ui.FlexContainer({
                    "id": flexid,
                    "top": "0dp",
                    "left": "0dp",
                    "width": "100%",
                    "height": "47dp",
                    "zIndex": 1,
                    "isVisible": true,
                    "clipBounds": true,
                    "Location": "[0,2682]",
                    "skin": "flexHeaderSkin",
                    "focusSkin": "flexHeaderSkin",
                    "layoutType": kony.flex.FREE_FORM
                }, {
                    "padding": [0, 0, 0, 0]
                }, {});;
            } else {
                flexid = new kony.ui.FlexContainer({
                    "id": flexid,
                    "top": "1dp",
                    "left": "0dp",
                    "width": "100%",
                    "height": "47dp",
                    "zIndex": 1,
                    "isVisible": true,
                    "clipBounds": true,
                    "Location": "[0,2682]",
                    "skin": "flexHeaderSkin",
                    "focusSkin": "flexHeaderSkin",
                    "layoutType": kony.flex.FREE_FORM
                }, {
                    "padding": [0, 0, 0, 0]
                }, {});;
            }
            flexid.setDefaultUnit(kony.flex.DP)
            flexid.add(btnid);
            frmMenu.flexdynamicContainer.add(flexid)
        }
    }
    //alert("The Result is " + gblMenuConfig[0]["DISPLAYKEY"]);
}

function onClickDynamiContainer(eventobject) {
    /*   var btnname = eventobject.text;
       //settting & more
       if (btnname == kony.i18n.getLocalizedString("keyMyProfile")) {
           onClickMyProfileFlow();
       } else if (btnname == kony.i18n.getLocalizedString("keylblMyAccount")) {
           onClickMyAccountsFlow();
       } else if (btnname == kony.i18n.getLocalizedString("keyMyRecipients")) {
           onClickMyRecipientsFlow();
       } else if (btnname == kony.i18n.getLocalizedString("keylblMybills")) {
           onClickMyBillerFlow();
       } else if (btnname == kony.i18n.getLocalizedString("myTopUpsMB")) {
           onClickMyTopUpFlow();
       } else if (btnname == kony.i18n.getLocalizedString("SQB_Title")) {
           onClickQuickBalanceMenu();
       } else if (gblTouchDevice == true && btnname == kony.i18n.getLocalizedString("keyTouchIDMenu")) {
           if (gblUserLockStatusMB == "03") {
               //alert("Transation Password Locked")
               showTranPwdLockedPopup();
           } else {
               onClickTouchIdSettings();
           }
       } else if (btnname == kony.i18n.getLocalizedString("keylblUseFormultipleDevice")) {
           //ActionCode is 23 for add device
           gblApplyServices = 3;
           if (checkMBUserStatus()) {
               applyServicesCommon("23", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
           }
       } else if (btnname == kony.i18n.getLocalizedString("UnlockIBanking")) {
           //ActionCode is 12 for reset
           gblApplyServices = 4;
           if (checkMBUserStatus()) {
               applyServicesCommon("12", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
           }
       } else if (btnname == kony.i18n.getLocalizedString("MIB_AnyIDMenu")) {
           getAnyIDBriefImage();
           onClickSetUserID();
       } else if (btnname == kony.i18n.getLocalizedString("kelblOpenNewAccount")) {
           if (checkMBUserStatus()) {
               //setting isCmpFlow to false as it is not internal link of campaign to product brief screnn MIB-971
               isCmpFlow = false;
               //  alert("This functionality will be implemented in codedrop 5");
               OnClickOpenNewAccount();
           }
       } else if (btnname == kony.i18n.getLocalizedString("InternetBanking")) {
           gblApplyServices = 2;
           //ActionCode is 11 for ApplyIB
           if (checkMBUserStatus()) {
               applyServicesCommon("11", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
           }
       } else if (btnname == kony.i18n.getLocalizedString("keyEStmt")) {
           if (checkMBUserStatus()) {
               checkpointMBestmtBousinessHrs();
           }
       } else if (btnname == kony.i18n.getLocalizedString("MenuFindTMB")) {
           onClickFindTMB();
       } else if (btnname == kony.i18n.getLocalizedString("MenuExRates")) {
           //Exchange Rates.
           onClickExchangeRates();
           //callOnClickAboutMenu(); // call function due to DEF1405
       } else if (btnname == kony.i18n.getLocalizedString("MenuTour")) {
           //App Tour.
           showAppTourForm();
           //callOnClickAboutMenu(); // call function due to DEF1405
       } else if (btnname == kony.i18n.getLocalizedString("keyContactUs")) {
           conatctUsForm();
       } else if (btnname == kony.i18n.getLocalizedString("keyTermsNConditions")) {
           onClickDownloadTnC('pdf', 'TermsAndCondtionsFooter');
       } else if (btnname == kony.i18n.getLocalizedString("keySecurityAdvices")) {
           onClickSecurityAdvices();
           //callOnClickAboutMenu(); // call function due to DEF1405
       } else {
           //..
       }*/
}

function onClickSettingsSegMenu() {
    var selectedsettingsIndex = frmMenu.segSettings.selectedIndex[1];
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("keyMyProfile")) {
        // onClickMyProfileFlow()
        menuMyProfileonClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("keylblMyAccount")) {
        // onClickMyAccountsFlow()
        menuMyAccountsonClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("MIB_AnyIDMenu")) {
        menuSetUserIDonClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("menuCardManagement")) {
        menuCarcdManagementClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("keyEStmt")) {
        menuMBestmtBousinessHrsonClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("keyMyRecipients")) {
        menuMyRecipientsonClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("keylblMybills")) {
        menuMyBilleronClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("myTopUpsMB")) {
        menuMyTopUponClick();
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("SQB_Title")) {
        menuQuickBalanceonClick();
    }
    if (gblTouchDevice == true && frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("keyTouchIDMenu")) {
        menuTouchIdonClick();
        /*if (gblUserLockStatusMB == "03") {
            //alert("Transation Password Locked")
            showTranPwdLockedPopup();
        } else {
            onClickTouchIdSettings();
        }*/
    }
    if (gblTouchDevice == true && frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("MB_AndFinPrintMenu")) {
        menuTouchIdonClick();
        /*if (gblUserLockStatusMB == "03") {
            //alert("Transation Password Locked")
            showTranPwdLockedPopup();
        } else {
            onClickTouchIdSettings();
        }*/
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("keylblUseFormultipleDevice")) {
        //ActionCode is 23 for add device
        menuMBUserStatusSer23onClick();
        /*gblApplyServices = 3;
        if (checkMBUserStatus()) {
            applyServicesCommon("23", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
        }*/
    }
    if (frmMenu.segSettings.data[frmMenu.segSettings.selectedIndex[1]].lblSettingsItem == kony.i18n.getLocalizedString("UnlockIBanking")) {
        //ActionCode is 12 for reset
        menuMBUserStatusSer12onClick();
        /*gblApplyServices = 4;
        if (checkMBUserStatus()) {

            applyServicesCommon("12", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
        }*/
    }
}

function onClickMoresegMenu() {
    //frmMenu.segMore
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("MIB_AnyIDMenu")) {
        /*getAnyIDBriefImage();
        onClickSetUserID();*/
        menuSetUserIDonClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("kelblOpenNewAccount")) {
        /* if (checkMBUserStatus()) {
             //setting isCmpFlow to false as it is not internal link of campaign to product brief screnn MIB-971
             isCmpFlow = false;
             //  alert("This functionality will be implemented in codedrop 5");
             OnClickOpenNewAccount();
         }*/
        menuOpenNewAccountonClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("InternetBanking")) {
        /* gblApplyServices = 2;
         //ActionCode is 11 for ApplyIB
         if (checkMBUserStatus()) {
             applyServicesCommon("11", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
         }*/
        menuMBUserStatusSer11onClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("keyEStmt")) {
        /*if (checkMBUserStatus()) {
            checkpointMBestmtBousinessHrs();
        }*/
        menuMBestmtBousinessHrsonClick();
    }
    //Adding more data here as per new menu design
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("MenuFindTMB")) {
        //Find TMB.
        /*  gblLatitude = "";
          gblLongitude = "";
          gblCurrenLocLat = "";
          gblCurrenLocLon = "";
          provinceID = "";
          districtID = "";
          gblProvinceData = [];
          gblDistrictData = [];
          frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
          frmATMBranch.btnCombDistrict.text = kony.i18n.getLocalizedString("FindTMB_District");
          frmATMBranch.txtKeyword.text = "";
          onClickATM();*/
        menuATMonClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("MenuExRates")) {
        //Exchange Rates.
        // onClickExchangeRates();
        //callOnClickAboutMenu(); // call function due to DEF1405 - Not Using
        menuExchangeRatesonClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("MenuTour")) {
        //App Tour.
        //showAppTourForm();
        //callOnClickAboutMenu(); // call function due to DEF1405 - Not Using
        menuAppTouronClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("keyContactUs")) {
        // conatctUsForm();
        menuContactUsMBonClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("keyTermsNConditions")) {
        // onClickDownloadTnC('pdf', 'TermsAndCondtionsFooter');
        menuTermsAndConditionsonClick();
    }
    if (frmMenu.segMore.data[frmMenu.segMore.selectedIndex[1]].lblmoreitems == kony.i18n.getLocalizedString("keySecurityAdvices")) {
        //onClickSecurityAdvices();
        //callOnClickAboutMenu(); // call function due to DEF1405 - Not using
        menuSecurityAdvicesonClick();
    }
}

function onClickSegMenuInbox() {
    var selectedInboxIndex = frmMenu.segInbox.selectedIndex[1];
    if (selectedInboxIndex == 0) {
        onClickNotifications();
    }
    if (selectedInboxIndex == 1) {
        onClickMessages();
    }
}

function onClickFindTMB() {
    //Find TMB.
    gblLatitude = "";
    gblLongitude = "";
    gblCurrenLocLat = "";
    gblCurrenLocLon = "";
    provinceID = "";
    districtID = "";
    gblProvinceData = [];
    gblDistrictData = [];
    frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
    frmATMBranch.btnCombDistrict.text = kony.i18n.getLocalizedString("FindTMB_District");
    frmATMBranch.txtKeyword.text = "";
    onClickATM();
}

function menuMyProfileonClick() {
    assignGlobalForMenuClick("frmMyProfile");
    onClickMyProfileFlow();
}

function menuMyAccountsonClick() {
    assignGlobalForMenuClick("frmMyAccountList");
    onClickMyAccountsFlow();
}

function menuMyRecipientsonClick() {
    assignGlobalForMenuClick("frmMyRecipients");
    onClickMyRecipientsFlow();
}

function menuMyBilleronClick() {
    assignGlobalForMenuClick("");
    onClickMyBillerFlow();
}

function menuMyTopUponClick() {
    assignGlobalForMenuClick("");
    onClickMyTopUpFlow();
}

function menuQuickBalanceonClick() {
    assignGlobalForMenuClick("frmMBQuickBalanceSetting");
    onClickQuickBalanceMenu();
}

function menuTouchIdonClick() {
    kony.print("FPRINT: in menuTouchIdonClick");
    if (gblDeviceInfo["name"] == "iPhone") {
        assignGlobalForMenuClick("frmTouchIdSettings");
        if (gblUserLockStatusMB == "03") {
            showTranPwdLockedPopup();
        } else {
            onClickTouchIdSettings();
        }
    } else if (gblDeviceInfo["name"] == "android") {
        kony.print("FPRINT: in menuTouchIdonClick android");
        assignGlobalForMenuClick("frmFPSetting");
        if (gblUserLockStatusMB == "03") {
            showTranPwdLockedPopup();
        } else {
            kony.print("FPRINT: in menuTouchIdonClick android calling onClickTouchIdAndroidSettings");
            onClickTouchIdAndroidSettings();
        }
    }
}

function menuMBUserStatusSer23onClick() {
    assignGlobalForMenuClick("frmApplyInternetBankingMB");
    gblApplyServices = 3;
    if (checkMBUserStatus()) {
        applyServicesCommon("23", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
    }
}

function menuMBUserStatusSer12onClick() {
    assignGlobalForMenuClick("frmApplyInternetBankingMB");
    gblApplyServices = 4;
    if (checkMBUserStatus()) {
        applyServicesCommon("12", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
    }
}

function menuSetUserIDonClick() {
    assignGlobalForMenuClick("frmMBAnyIdRegTnC");
    getAnyIDBriefImage();
    onClickSetUserID();
}

function menuOpenNewAccountonClick() {
    assignGlobalForMenuClick("frmMBAnyIdRegTnC");
    if (checkMBUserStatus()) {
        //setting isCmpFlow to false as it is not internal link of campaign to product brief screnn MIB-971
        isCmpFlow = false;
        OnClickOpenNewAccount();
    }
}

function menuMBUserStatusSer11onClick() {
    assignGlobalForMenuClick("frmApplyInternetBankingMB");
    gblApplyServices = 2;
    //ActionCode is 11 for ApplyIB
    if (checkMBUserStatus()) {
        applyServicesCommon("11", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
    }
}

function menuMBestmtBousinessHrsonClick() {
    assignGlobalForMenuClick("frmMBEStatementProdFeature");
    if (checkMBUserStatus()) {
        checkpointMBestmtBousinessHrs();
    }
}

function menuATMonClick() {
    assignGlobalForMenuClick("frmATMBranch");
    gblLatitude = "";
    gblLongitude = "";
    gblCurrenLocLat = "";
    gblCurrenLocLon = "";
    provinceID = "";
    districtID = "";
    gblProvinceData = [];
    gblDistrictData = [];
    frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
    frmATMBranch.btnCombDistrict.text = kony.i18n.getLocalizedString("FindTMB_District");
    frmATMBranch.txtKeyword.text = "";
    onClickATM();
}

function menuExchangeRatesonClick() {
    gblCallPrePost = false;
    onClickExchangeRates();
}

function menuAppTouronClick() {
    assignGlobalForMenuClick("frmAppTour");
    showAppTourForm();
}

function menuContactUsMBonClick() {
    assignGlobalForMenuClick("frmContactUsMB");
    conatctUsForm();
}

function menuTermsAndConditionsonClick() {
    gblCallPrePost = false;
    onClickDownloadTnC('pdf', 'TermsAndCondtionsFooter');
}

function menuSecurityAdvicesonClick() {
    gblCallPrePost = false;
    onClickSecurityAdvices();
}

function menuCarcdManagementClick() {
    //gblCallPrePost = false;
    if (gblUserLockStatusMB == "03") {
        showTranPwdLockedPopup();
    } else {
        preShowMBCardList();
    }
}