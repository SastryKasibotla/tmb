BillerList = null;

function moveToBillPaymentFromAccountSummaryForCreditCard() {
    // code to move to bill pay modules as per situation
    showLoadingScreenPopup();
    var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
    gblAccountType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
    gblProductCode = gblAccountTable["custAcctRec"][gblIndex]["productID"];
    accId = removeHyphenIB(accId);
    gblReference1 = accId.substring(8, accId.length);
    gblMyBillerTopUpBB = 0;
    gblBillerPresentInMyBills = false;
    gblFromAccountSummary = true;
    gblBillPaymentEdit = false;
    clearIBBillPaymentLP();
    getCompcode(gblProductCode, gblAccountType) //"0699";
}

function masterBillerForCreditCardLoan() {
    var inputParams = {
        IsActive: "1",
        BillerGroupType: gblGroupTypeBiller,
        flagBillerList: "IB"
    };
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, masterBillerForAccountSummaryCallBack);
}

function masterBillerForAccountSummaryCallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                for (var i = 0; i < responseData.length; i++) {
                    if (responseData[i]["BillerCompcode"] == gblCompCode) {
                        //Setting required global variables to complete the bill payment
                        gblCompCode = responseData[i]["BillerCompcode"];
                        gblBillerId = responseData[i]["BillerID"];
                        gblbillerGroupType = responseData[i]["BillerGroupType"];
                        gblBillerBancassurance = responseData[i]["IsBillerBancassurance"];
                        gblAllowRef1AlphaNum = responseData[i]["AllowRef1AlphaNum"];
                        gblRef1LblEN = responseData[i]["LabelReferenceNumber1EN"];
                        gblRef2LblEN = responseData[i]["LabelReferenceNumber2EN"];
                        gblRef1LblTH = responseData[i]["LabelReferenceNumber1TH"];
                        gblRef2LblTH = responseData[i]["LabelReferenceNumber2TH"];
                        gblRef1LenIB = responseData[i]["Ref1Len"];
                        gblRef2LenIB = responseData[i]["Ref2Len"];
                        gblreccuringDisablePay = responseData[i]["IsRequiredRefNumber2Pay"];
                        gblreccuringDisableAdd = responseData[i]["IsRequiredRefNumber2Add"];
                        gblBillerMethod = responseData[i]["BillerMethod"];
                        gblPayFull = responseData[i]["IsFullPayment"];
                        gblBillerCompCodeEN = responseData[i]["BillerNameEN"] + " (" + gblCompCode + ")";
                        gblBillerCompCodeTH = responseData[i]["BillerNameTH"] + " (" + gblCompCode + ")";
                        gblToAccountKey = responseData[i]["ToAccountKey"];
                        break;
                    }
                }
                callCustomerBillerInqForCreditCardIB();
            } else {
                dismissLoadingScreenPopup();
                alert("No Suggested Biller  found");
            }
        } else {
            dismissLoadingScreenPopup();
            alert("No Suggested Biller found");
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("No Suggested Biller found");
        }
    }
}

function callCustomerBillerInqForCreditCardIB() {
    var inputParams = {
        crmId: gblcrmId,
        IsActive: "1"
    };
    invokeServiceSecureAsync("customerBillInquiry", inputParams, callCustomerBillerInqForCreditCardIBCallBack);
}

function callCustomerBillerInqForCreditCardIBCallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var flag = false;
            var responseData = callBackResponse["CustomerBillInqRs"];
            gblMyBillList = [];
            if (responseData.length > 0) {
                gblMyBillList = responseData;
                var nickName = "";
                for (var i = 0; i < responseData.length; i++) {
                    if (gblCompCode == responseData[i]["BillerCompcode"] && gblReference1 == responseData[i]["ReferenceNumber1"]) {
                        if (responseData[i]["IsRequiredRefNumber2Add"] == "Y") {
                            if (responseData[i]["ReferenceNumber2"] == gblReference2) {
                                gblBillerPresentInMyBills = true;
                                nickName = responseData[i]["BillerNickName"];
                                gblBillerID = responseData[i]["CustomerBillID"];
                                break;
                            }
                        } else {
                            gblBillerPresentInMyBills = true;
                            nickName = responseData[i]["BillerNickName"];
                            gblBillerID = responseData[i]["CustomerBillID"];
                            break;
                        }
                    }
                }
            }
            //var billerGroupType = 0;
            if (!frmIBBillPaymentCW.hbxRef1.isVisible) {
                frmIBBillPaymentCW.hbxRef1.setVisibility(true);
            }
            if (gblreccuringDisablePay == "Y") {
                frmIBBillPaymentCW.hbxRef2.setVisibility(true);
                var ref2Value = gblReference2;
            } else {
                frmIBBillPaymentCW.hbxRef2.setVisibility(false);
                var ref2Value = "";
            }
            var locale = kony.i18n.getCurrentLocale()
            frmIBBillPaymentCW.hbxTo.setVisibility(true);
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblCompCode;
            if (locale == "en_US") {
                frmIBBillPaymentCW.lblToCompCode.text = gblBillerCompCodeEN;
                frmIBBillPaymentCW.lblBPRef1.text = appendColon(gblRef1LblEN);
                frmIBBillPaymentCW.lblBPRef2.text = appendColon(gblRef2LblEN);
            } else {
                frmIBBillPaymentCW.lblToCompCode.text = gblBillerCompCodeTH;
                frmIBBillPaymentCW.lblBPRef1.text = appendColon(gblRef1LblTH);
                frmIBBillPaymentCW.lblBPRef2.text = appendColon(gblRef2LblTH);
            }
            if (isNotBlank(nickName)) {
                frmIBBillPaymentCW.lblToCompCode.text = nickName;
            }
            frmIBBillPaymentCW.lblBPBillerName.text = frmIBBillPaymentCW.lblToCompCode.text;
            frmIBBillPaymentCW.imgBPBillerImage.src = imagesUrl;
            frmIBBillPaymentCW.lblBPRef1Val.text = gblReference1;
            frmIBBillPaymentCW.lblBPRef1Val.maxTextLength = parseInt(gblRef1LenIB);
            //below line is added for CR - PCI-DSS masked Credit card no
            frmIBBillPaymentCW.lblBPRef1ValMasked.text = maskCreditCard(gblReference1);
            frmIBBillPaymentCW.txtBPRef2Val.text = ref2Value;
            frmIBBillPaymentCW.txtBPRef2Val.maxTextLength = parseInt(gblRef2LenIB);
            frmIBBillPaymentLP.lblBPAmount.containerWeight = 20;
            if (gblBillerBancassurance == "Y") {
                gblBillPaymentEdit = true;
                gblPenalty = false;
                gblFullPayment = false;
                frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
                //Hide All
                frmIBBillPaymentCW.btnAmtFull.setVisibility(false);
                frmIBBillPaymentCW.btnAmtFull.setEnabled(false);
                frmIBBillPaymentCW.btnAmtMinimum.setVisibility(false);
                frmIBBillPaymentCW.btnAmtMinimum.setEnabled(false);
                frmIBBillPaymentCW.btnAmtSpecified.setEnabled(false);
                frmIBBillPaymentCW.btnAmtSpecified.setVisibility(false);
                frmIBBillPaymentCW.hbxMinMax.setVisibility(false);
                frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(gblBAPolicyAmount));
                frmIBBillPaymentCW.txtBillAmt.setEnabled(false);
                frmIBBillPaymentCW.txtBillAmt.setVisibility(true)
                frmIBBillPaymentCW.hbxAmtVal.setVisibility(true);
                frmIBBillPaymentCW.lblFullPayment.setVisibility(false);
                //Ref2 should not editable
                frmIBBillPaymentCW.txtBPRef2Val.text = gblReference2;
                frmIBBillPaymentCW.txtBPRef2Val.setEnabled(false);
                frmIBBillPaymentCW.txtBPRef2Val.skin = "txtBoxTransParentGrey";
                callBillPaymentCustomerAccountServiceIB();
            } else {
                frmIBBillPaymentCW.txtBPRef2Val.setEnabled(true);
                frmIBBillPaymentCW.txtBPRef2Val.skin = "txtIB20pxGrey";
                frmIBBillPaymentCW.txtBillAmt.setEnabled(true);
                if (gblAccountType == "LOC") {
                    populateLoanonBillPayCw(gblReference1, gblReference2)
                } else if (gblAccountType == "CCA") {
                    populateCreditonBillPayCw(gblReference1);
                }
            }
        }
    }
}

function populateCreditonBillPayCw(cards) {
    //for CreditCard and Ready cash
    var inputParam = {};
    var cardId = cards
    inputParam["cardId"] = cardId;
    //inputParam["waiverCode"] = "I";
    inputParam["tranCode"] = TRANSCODEMIN;
    invokeServiceSecureAsync("creditcardDetailsInq", inputParam, populateCreditCardOnBillPayCw);
}

function populateCreditCardOnBillPayCw(status, result) {
    fullAmt = "0.00";
    minAmt = "0.00";
    if (status == 400) //success responce
    {
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] == 0) {
                fullAmt = result["fullPmtAmt"];
                if (parseInt(fullAmt) < 0) gblPaymentOverpaidFlag = true;
                minAmt = result["minPmtAmt"]
                if (fullAmt == "") fullAmt = "0.00";
                if (minAmt == "") minAmt = "0.00";
                gblPenalty = false;
                frmIBBillPaymentCW.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(fullAmt));
                frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
                frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
                //Penalty Hide
                //frmIBBillPaymentCW.hbxPenalty.setVisibility(false);
                frmIBBillPaymentCW.btnAmtFull.setEnabled(true);
                frmIBBillPaymentCW.btnAmtFull.setVisibility(true);
                frmIBBillPaymentCW.btnAmtMinimum.setEnabled(true);
                frmIBBillPaymentCW.btnAmtMinimum.setVisibility(true);
                frmIBBillPaymentCW.btnAmtSpecified.setEnabled(true);
                frmIBBillPaymentCW.btnAmtSpecified.setVisibility(true);
                frmIBBillPaymentCW.hbxMinMax.setVisibility(true);
                frmIBBillPaymentCW.hbxAmtVal.isVisible = true;
                frmIBBillPaymentCW.txtBillAmt.setVisibility(false);
                eh_frmIBBillPaymentCW_btnAmtFull_onClick();
                //frmIBBillPaymentCW.show();
                gblBillPaymentEdit = true;
                callBillPaymentCustomerAccountServiceIB();
            } else {
                dismissLoadingScreenPopup();
                alert(" " + result["errMsg"]);
            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + result["errMsg"]);
        }
    }
}

function getCompcode(productCode, AccountType) {
    var inputParam = {};
    var noCASAAct = noActiveActs();
    if (noCASAAct) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
        return false;
    }
    inputParam["productCode"] = productCode;
    inputParam["accountType"] = AccountType;
    invokeServiceSecureAsync("getBillerCompCode", inputParam, getCompCodeCallBack);
}

function getCompCodeCallBack(status, result) {
    if (status == "400") {
        if (result["opstatus"] == "0") {
            if (isNotBlank(result["compCode"])) {
                gblCompCode = result["compCode"];
                masterBillerForCreditCardLoan();
            } else {
                alert("CompCode Not Found");
                dismissLoadingScreen();
                return;
            }
        }
    }
}