allowApplySoGoood = "";

function onClickOfAccountDetailsMore() {
    if (frmAccountDetailsMB.linkMore.text == kony.i18n.getLocalizedString("More")) {
        frmAccountDetailsMB.linkMore.text = kony.i18n.getLocalizedString("Hide");
        frmAccountDetailsMB.hbxUseful7.isVisible = true;
        frmAccountDetailsMB.hbxUseful8.isVisible = true;
        if (!(frmAccountDetailsMB.lblUsefulValue9.text == undefined || frmAccountDetailsMB.lblUsefulValue9.text == "")) {
            frmAccountDetailsMB.hbxUseful9.isVisible = true;
        }
        frmAccountDetailsMB.hbxUseful10.isVisible = true;
        if (glb_viewName != "PRODUCT_CODE_NOFIXED") {
            if (!(frmAccountDetailsMB.lblUsefulValue11.text == undefined || frmAccountDetailsMB.lblUsefulValue11.text == "")) {
                frmAccountDetailsMB.hbxUseful11.isVisible = true;
            }
        }
        if (glb_viewName == "PRODUCT_CODE_NOFIXED" && (frmAccountDetailsMB.lblUsefulValue2.text).length == 0) {
            //frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
        }
        frmAccountDetailsMB.hbxUseful12.isVisible = true;
        frmAccountDetailsMB.hbxUseful13.isVisible = true;
        if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName == "PRODUCT_CODE_NOFEESAVING_TABLE" || glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
            frmAccountDetailsMB.segDebitCard.setVisibility(true);
            if (GLOBAL_DEBITCARD_TABLE != null && GLOBAL_DEBITCARD_TABLE != "") {
                var temp = [];
                var imageForDisplay = "";
                for (var i = 0; i < GLOBAL_DEBITCARD_TABLE.length; i++) {
                    imageForDisplay = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + GLOBAL_DEBITCARD_TABLE[i]["cardLogo"] + "&modIdentifier=AccountDetails";
                    //imageForDisplay="debitcard.png"
                    var segTable = {
                        imgCreditCard: imageForDisplay,
                        lblCreditCardNo: GLOBAL_DEBITCARD_TABLE[i]["maskedCardNo"],
                        lblExpiryDate: kony.i18n.getLocalizedString("ExpiryDate") + "  " + GLOBAL_DEBITCARD_TABLE[i]["expiryDate"]
                    }
                    temp.push(segTable);
                    //frmAccountDetailsMB.segDebitCard.setData(temp);
                    //					frmAccountDetailsMB.segDebitCard.setVisibility(true);
                    //					frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
                }
                frmAccountDetailsMB.segDebitCard.setData(temp);
                frmAccountDetailsMB.segDebitCard.setVisibility(true);
                frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
            }
        }
    } else {
        frmAccountDetailsMB.linkMore.text = kony.i18n.getLocalizedString("More");
        frmAccountDetailsMB.hbxUseful7.isVisible = false;
        frmAccountDetailsMB.hbxUseful8.isVisible = false;
        frmAccountDetailsMB.hbxUseful9.isVisible = false;
        frmAccountDetailsMB.hbxUseful10.isVisible = false;
        frmAccountDetailsMB.hbxUseful11.isVisible = false;
        frmAccountDetailsMB.hbxUseful12.isVisible = false;
        frmAccountDetailsMB.hbxUseful13.isVisible = false;
        frmAccountDetailsMB.segDebitCard.setVisibility(false);
        frmAccountDetailsMB.hbxLinkedTo.isVisible = false;
        frmAccountDetailsMB.hboxApplySendToSave.isVisible = false;
        frmAccountDetailsMB.hbxUseful11.isVisible = false;
        if (glb_viewName == "PRODUCT_CODE_NOFIXED" && (frmAccountDetailsMB.lblUsefulValue2.text).length == 0) {
            //frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
        }
    }
}

function onClickOfAccountDetailsBack() {
    var curr_form_name = kony.application.getCurrentForm().id; //Added to fix blank screen issue on clicking of alert popups in Menu
    if (curr_form_name != "frmAccountSummaryLanding") TMBUtil.DestroyForm(frmAccountSummaryLanding);
    gaccType = "";
    glb_viewName = "";
    gblAccountTable = "";
    GLOBAL_DEBITCARD_TABLE = ""
    if (flowSpa) {
        isMenuShown = false
            //frmAccountSummaryLanding = null;
            //     frmAccountSummaryLandingGlobals();    
        TMBUtil.DestroyForm(frmAccountSummaryLanding);
        gblAccountTable = "";
    }
    callCustomerAccountService()
    frmAccountSummaryLanding.lblAccntHolderName.text = gblCustomerName;
}

function simpleAccountFunction() {
    frmAccountDetailsMB.lblUseful8.text = "";
    frmAccountDetailsMB.lblUseful9.text = "";
    frmAccountDetailsMB.lblUseful10.text = "";
    frmAccountDetailsMB.lblUseful11.text = "";
    frmAccountDetailsMB.lblUseful12.text = "";
    frmAccountDetailsMB.lblUseful13.text = "";
    frmAccountDetailsMB.lblUseful14.text = "";
    frmAccountDetailsMB.lblUseful15.text = "";
    //Setting the links and lines
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
    frmAccountDetailsMB.flexLineStatement.setVisibility(true);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
    //frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    //frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
    frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
    //
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    //
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
    //
    frmAccountDetailsMB.lblUsefulValue3.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
    frmAccountDetailsMB.lblUsefulValue3.textCopyable = true;
    frmAccountDetailsMB.lblUsefulValue8.text = "";
    frmAccountDetailsMB.lblUsefulValue9.text = "";
    frmAccountDetailsMB.lblUsefulValue10.text = "";
    frmAccountDetailsMB.lblUsefulValue11.text = "";
    frmAccountDetailsMB.lblUsefulValue12.text = "";
    frmAccountDetailsMB.lblUsefulValue13.text = "";
    frmAccountDetailsMB.lblUsefulValue14.text = "";
    frmAccountDetailsMB.lblUsefulValue15.text = "";
    //frmAccountDetailsMB.lblLinkedDebit.text = kony.i18n.getLocalizedString("LinkedTo");
    moreAccountDetails();
}

function TDAccountFunction() {
    frmAccountDetailsMB.lblUseful10.text = "";
    frmAccountDetailsMB.lblUseful11.text = "";
    frmAccountDetailsMB.lblUseful12.text = "";
    frmAccountDetailsMB.lblUseful13.text = "";
    frmAccountDetailsMB.lblUseful14.text = "";
    frmAccountDetailsMB.lblUseful15.text = "";
    //frmAccountDetailsMB.lblLinkedDebit.text = "";
    //frmAccountDetailsMB.line4740217943851.setVisibility(true);
    //frmAccountDetailsMB.hbxSegHeader.isVisible = true;
    //frmAccountDetailsMB.lblDepositDetails.setVisibility(true);
    //Setting the links and lines
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
    frmAccountDetailsMB.flexLineStatement.setVisibility(true);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
    frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
    //
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(true);
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(true);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(true);
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(true);
    //
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    //
    //this is savings care container
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    //
    //frmAccountDetailsMB.segDebitCard.setVisibility(false);
    //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_termdeposits.png";
    //var imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblAccountTable["custAcctRec"][gblIndex]["ICON_ID"]+"&modIdentifier=PRODICON";
    //frmAccountDetailsMB.imgAccountDetailsPic.src=imageName;
    frmAccountDetailsMB.lblUsefulValue4.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
    frmAccountDetailsMB.lblUsefulValue4.textCopyable = true;
    frmAccountDetailsMB.lblUsefulValue10.text = "";
    frmAccountDetailsMB.lblUsefulValue11.text = "";
    frmAccountDetailsMB.lblUsefulValue12.text = "";
    frmAccountDetailsMB.lblUsefulValue13.text = "";
    frmAccountDetailsMB.lblUsefulValue14.text = "";
    frmAccountDetailsMB.lblUsefulValue15.text = "";
    moreAccountDetails();
}

function noFeeSavingAccount() {
    //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_nofee.png";
    frmAccountDetailsMB.lblUseful9.text = "";
    frmAccountDetailsMB.lblUseful10.text = "";
    frmAccountDetailsMB.lblUseful11.text = "";
    frmAccountDetailsMB.lblUseful12.text = "";
    frmAccountDetailsMB.lblUseful13.text = "";
    frmAccountDetailsMB.lblUseful14.text = "";
    frmAccountDetailsMB.lblUseful15.text = "";
    frmAccountDetailsMB.lblUsefulValue14.text = "";
    frmAccountDetailsMB.lblUsefulValue15.text = "";
    //Setting the links and lines
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
    frmAccountDetailsMB.flexLineStatement.setVisibility(true);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
    //frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    //frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
    frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
    //
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    //
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    //
    frmAccountDetailsMB.lblUsefulValue2.text = gblAccountTable["custAcctRec"][gblIndex]["remainingFee"];
    frmAccountDetailsMB.lblUsefulValue4.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
    frmAccountDetailsMB.lblUsefulValue4.textCopyable = true;
    moreAccountDetails();
}

function dreamSavingAccount() {
    //frmAccountDetailsMB.linkDreamSaving.isVisible = true;
    //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_savingd.png";
    //frmAccountDetailsMB.lblLinkedDebit.text = "";
    //frmAccountDetailsMB.lblLinkedDebit.text = "";
    //Setting the links and lines
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
    frmAccountDetailsMB.flexLineStatement.setVisibility(true);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(true);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(true);
    frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
    frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
    //
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    //
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    //
    frmAccountDetailsMB.lblUsefulValue7.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
    frmAccountDetailsMB.lblUsefulValue7.textCopyable = true;
    moreAccountDetails();
}

function noFixedAccount() {
    //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_savingd.png";
    frmAccountDetailsMB.lblUseful12.text = "";
    frmAccountDetailsMB.lblUseful13.text = "";
    frmAccountDetailsMB.lblUseful14.text = "";
    frmAccountDetailsMB.lblUseful15.text = "";
    frmAccountDetailsMB.lblUsefulValue14.text = "";
    frmAccountDetailsMB.lblUsefulValue15.text = "";
    //frmAccountDetailsMB.lblLinkedDebit.text = "";
    //frmAccountDetailsMB.lblLinkedDebit.text = "";
    //Setting the links and lines
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(true);
    frmAccountDetailsMB.flexLineStatement.setVisibility(true);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
    frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
    //
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    //
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    //
    frmAccountDetailsMB.lblUsefulValue5.text = gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
    frmAccountDetailsMB.lblUsefulValue5.textCopyable = true;
    moreAccountDetails();
}

function creditCardAccount() {
    //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_creditcard.png";
    //frmAccountDetailsMB.lblAccountNameHeader.text = gblAccountTable["custAcctRec"][gblIndex]["acctNickName"];
    frmAccountDetailsMB.lblUseful10.text = "";
    frmAccountDetailsMB.lblUseful11.text = "";
    frmAccountDetailsMB.lblUseful12.text = "";
    frmAccountDetailsMB.lblUseful13.text = "";
    //hiding the links and lines
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(false);
    frmAccountDetailsMB.flexLineStatement.setVisibility(false);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    //frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    //frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    //frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    //frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    //frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(true);
    //frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(true);
    //
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    //
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    //
    frmAccountDetailsMB.lblUsefulValue10.text = "";
    frmAccountDetailsMB.lblUsefulValue11.text = "";
    frmAccountDetailsMB.lblUsefulValue12.text = "";
    frmAccountDetailsMB.lblUsefulValue13.text = "";
    moreAccountDetails();
}

function cash2GoAccount() {
    //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_cash2go.png";
    frmAccountDetailsMB.lblUseful10.text = "";
    frmAccountDetailsMB.lblUseful11.text = "";
    frmAccountDetailsMB.lblUseful12.text = "";
    frmAccountDetailsMB.lblUseful13.text = "";
    //frmAccountDetailsMB.lblLinkedDebit.text = "";
    //rmAccountDetailsMB.lblLinkedDebit.text = "";
    //frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
    //frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    //hiding the links and lines
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(false);
    frmAccountDetailsMB.flexLineStatement.setVisibility(false);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
    frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
    //
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(false);
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(false);
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    //
    if (gblLoanAccountTable["loanAccounNo"] != "" && gblLoanAccountTable["loanAccounNo"] != null) {
        frmAccountDetailsMB.lblUsefulValue2.text = formatAccountNo(gblLoanAccountTable["loanAccounNo"].substring(17, 27));
        frmAccountDetailsMB.lblUsefulValue3.text = gblLoanAccountTable["loanAccounNo"].substring(27);
    }
    frmAccountDetailsMB.lblUsefulValue4.text = gblLoanAccountTable["accountName"]
    frmAccountDetailsMB.lblUsefulValue5.text = reformatDate(gblLoanAccountTable["nextPmtDt"]);
    if (gblLoanAccountTable["regPmtCurAmt"] != "" && gblLoanAccountTable["regPmtCurAmt"] != null) {
        frmAccountDetailsMB.lblUsefulValue6.text = commaFormatted(gblLoanAccountTable["regPmtCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    if (gblLoanAccountTable["debitAccount"].length != 0) {
        frmAccountDetailsMB.lblUsefulValue9.text = formatAccountNo(gblLoanAccountTable["debitAccount"].substring(4));
    } else {
        frmAccountDetailsMB.lblUseful9.isVisible = false;
    }
    frmAccountDetailsMB.lblUsefulValue7.text = ""
    frmAccountDetailsMB.lblUsefulValue8.text = ""
    frmAccountDetailsMB.lblUsefulValue10.text = "";
    frmAccountDetailsMB.lblUsefulValue11.text = "";
    frmAccountDetailsMB.lblUsefulValue12.text = "";
    frmAccountDetailsMB.lblUsefulValue13.text = "";
    moreAccountDetails();
}

function homeLoanAccount() {
    //frmAccountDetailsMB.imgAccountDetailsPic.src = "prod_homeloan.png";
    frmAccountDetailsMB.lblUseful8.text = "";
    frmAccountDetailsMB.lblUseful9.text = "";
    frmAccountDetailsMB.lblUseful10.text = "";
    frmAccountDetailsMB.lblUseful11.text = "";
    frmAccountDetailsMB.lblUseful12.text = "";
    frmAccountDetailsMB.lblUseful13.text = "";
    frmAccountDetailsMB.lblUseful14.text = "";
    frmAccountDetailsMB.lblUseful15.text = "";
    frmAccountDetailsMB.lblUsefulValue8.text = "";
    frmAccountDetailsMB.lblUsefulValue9.text = "";
    frmAccountDetailsMB.lblUsefulValue10.text = "";
    frmAccountDetailsMB.lblUsefulValue11.text = "";
    frmAccountDetailsMB.lblUsefulValue12.text = "";
    frmAccountDetailsMB.lblUsefulValue13.text = "";
    frmAccountDetailsMB.lblUsefulValue14.text = "";
    frmAccountDetailsMB.lblUsefulValue15.text = "";
    //hiding the links
    frmAccountDetailsMB.flexLinkMyAcctivities.setVisibility(false);
    frmAccountDetailsMB.flexLineStatement.setVisibility(false);
    frmAccountDetailsMB.flexLineSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLinkSavingsCare.setVisibility(false);
    frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
    frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(false);
    frmAccountDetailsMB.flexLineApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinkApplysoGood.setVisibility(false);
    frmAccountDetailsMB.flexLinePointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLinkPointsRdeem.setVisibility(false);
    frmAccountDetailsMB.flexLineManageCreditCard.setVisibility(false);
    frmAccountDetailsMB.flexLinkManageCreditCard.setVisibility(false);
    //
    frmAccountDetailsMB.Copyflexlinebottom0ece6d5355f9943.setVisibility(true);
    frmAccountDetailsMB.flexMaturityHeaderone.setVisibility(true);
    frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(true);
    frmAccountDetailsMB.flexMaturityDisplay.setVisibility(true);
    frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
    frmAccountDetailsMB.flexBenefiList.setVisibility(false);
    frmAccountDetailsMB.hboxBeneficiary.setVisibility(false);
    //
    frmAccountDetailsMB.hbox566614587293709.setVisibility(false);
    //
    if (gblLoanAccountTable["loanAccounNo"] != "" && gblLoanAccountTable["loanAccounNo"] != null) {
        frmAccountDetailsMB.lblUsefulValue2.text = formatAccountNo(gblLoanAccountTable["loanAccounNo"].substring(17, 27));
        frmAccountDetailsMB.lblUsefulValue3.text = gblLoanAccountTable["loanAccounNo"].substring(27);
    }
    frmAccountDetailsMB.lblUsefulValue4.text = gblLoanAccountTable["accountName"];
    frmAccountDetailsMB.lblUsefulValue5.text = reformatDate(gblLoanAccountTable["nextPmtDt"]);
    frmAccountDetailsMB.lblUsefulValue6.text = commaFormatted(gblLoanAccountTable["regPmtCurAmt"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    if (gblLoanAccountTable["debitAccount"].length != 0) {
        frmAccountDetailsMB.hbxUseful7.isVisible = false;
        frmAccountDetailsMB.lblUsefulValue7.text = formatAccountNo(gblLoanAccountTable["debitAccount"].substring(4));
    } else {
        frmAccountDetailsMB.lblUseful7.isVisible = false;
    }
    moreAccountDetails();
}

function showAccountDetails(eventObject) {
    gblCallPrePost = true;
    var hboxID = eventObject.id;
    var indexString = hboxID.split("hboxSwipe");
    gblIndex = indexString[1];
    if (gblAccountTable != null && gblAccountTable != undefined && gblIndex != null && gblIndex != undefined) {
        //added code for 16067
        var productId = gblAccountTable["custAcctRec"][gblIndex]["productID"];
        gaccType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
        glb_viewName = gblAccountTable["custAcctRec"][gblIndex]["VIEW_NAME"];
        if (glb_viewName == "PRODUCT_CODE_NOFIXED") {
            frmAccountDetailsMB.hbxUseful2.isVisible = false;
            frmAccountDetailsMB.hbxUseful3.isVisible = false;
        }
        // Moved to Preshow of the form
        //code change for new UI
        //alert("glb_viewName is"+glb_viewName)
        var productId = gblAccountTable["custAcctRec"][gblIndex]["productID"];
        gaccType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
        glb_viewName = gblAccountTable["custAcctRec"][gblIndex]["VIEW_NAME"];
        var allowRedeemPoints = gblAccountTable["custAcctRec"][gblIndex]["allowRedeemPoints"];
        if (glb_viewName == "PRODUCT_CODE_DREAM_SAVING") {
            dreamSavingCallService();
        } else if (glb_viewName == "PRODUCT_CODE_NOFIXED") {
            depositAccInqCallServiceForNoFixed();
        } else if (glb_viewName == "PRODUCT_CODE_NOFEESAVING_TABLE") {
            callDepositAccountInquiryServiceNofee()
        } else if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName == "PRODUCT_CODE_SAVINGCARE" || glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
            callDepositAccountInquiryService();
        } else if (glb_viewName == "PRODUCT_CODE_CREDITCARD_TABLE") {
            creditCardCallService();
        } else if (glb_viewName == "PRODUCT_CODE_OLDREADYCASH_TABLE") {
            creditCardCallService();
        } else if (glb_viewName == "PRODUCT_CODE_TD_TABLE") {
            callTimeDepositDetailService();
        } else if (glb_viewName == "PRODUCT_CODE_LOAN_CASH2GO") {
            loanAccountCallService(cash2GoAccountCallBck);
        } else if (glb_viewName == "PRODUCT_CODE_LOAN_HOMELOAN") {
            loanAccountCallService(homeLoanAccountCallBck);
        } else if (glb_viewName == "PRODUCT_CODE_READYCASH_TABLE") {
            creditCardCallService();
        }
        if (flowSpa) {
            kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
        }
    }
}

function onclickgetTransferFromAccountsFromAccDetails() {
    if (checkMBUserStatus()) {
        if (gblAccountTable != null && gblAccountTable != undefined && gblIndex != null && gblIndex != undefined && gblAccountTable["custAcctRec"] != null && gblAccountTable["custAcctRec"][gblIndex] != undefined && gblAccountTable["custAcctRec"][gblIndex] != null) {
            var accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
            var accountStatus = gblAccountTable["custAcctRec"][gblIndex].acctStatusCode;
            var jointActXfer = gblAccountTable["custAcctRec"][gblIndex].partyAcctRelDesc;
            if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
                return;
            }
            if (accountStatus.indexOf("Active") == -1) {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
                return;
            }
            var noCASAAct = noActiveActs();
            if (noCASAAct) {
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
                return false;
            }
            getTransferFromAccountsFromAccSmry(accId);
        }
    }
}

function onclickgetBillPaymentFromAccountsFromAccDetails() {
    if (checkMBUserStatus()) {
        if (gblAccountTable != null && gblAccountTable != undefined && gblIndex != null && gblIndex != undefined && gblAccountTable["custAcctRec"] != null && gblAccountTable["custAcctRec"][gblIndex] != undefined && gblAccountTable["custAcctRec"][gblIndex] != null) {
            glb_accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
            var jointActXfer = gblAccountTable["custAcctRec"][gblIndex].partyAcctRelDesc;
            if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
                alert("" + kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
                return;
            }
            var accountStatus = gblAccountTable["custAcctRec"][gblIndex].acctStatusCode;
            if (accountStatus.indexOf("Active") == -1) {
                alert("" + kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
                return;
            }
            var noCASAAct = noActiveActs();
            if (noCASAAct) {
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
                return false;
            }
            gblMyBillerTopUpBB = 0;
            GblBillTopFlag = true;
            gblBillPayShortCut = true;
            callMasterBillerService();
        }
    }
}

function onclickgetTopUpFromAccountsFromAccDetails() {
    if (checkMBUserStatus()) {
        if (gblAccountTable != null && gblAccountTable != undefined && gblIndex != null && gblIndex != undefined && gblAccountTable["custAcctRec"] != null && gblAccountTable["custAcctRec"][gblIndex] != undefined && gblAccountTable["custAcctRec"][gblIndex] != null) {
            glb_accId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
            var jointActXfer = gblAccountTable["custAcctRec"][gblIndex].partyAcctRelDesc;
            if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
                alert("" + kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
                return;
            }
            var accountStatus = gblAccountTable["custAcctRec"][gblIndex].acctStatusCode;
            if (accountStatus.indexOf("Active") == -1) {
                alert("" + kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
                return;
            }
            var noCASAAct = noActiveActs();
            if (noCASAAct) {
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
                return false;
            }
            gblMyBillerTopUpBB = 1;
            GblBillTopFlag = false;
            callMasterBillerService(); // For Top Up Redesign PBI
        }
    }
}

function showAccountSummaryFromMenu() {
    isMenuShown = false;
    gblFlagMenu = "";
    gblfromCalender = false;
    if (kony.application.getCurrentForm().id == "frmAccountSummaryLanding") frmAccountSummaryLanding.scrollboxMain.scrollToEnd()
    else {
        gblRetryCountRequestOTP = "0";
        //frmAccountSummaryLanding = null;
        //		frmAccountSummaryLandingGlobals();		
        TMBUtil.DestroyForm(frmAccountSummaryLanding);
        if (!flowSpa) {
            //frmAccountDetailsMB = null;
            //			frmAccountDetailsMBGlobals();			
            TMBUtil.DestroyForm(frmAccountDetailsMB);
        }
        gblAccountTable = "";
        gblAccountStatus = "";
        GLOBAL_DEBITCARD_TABLE = "";
        destroyMenuSpa();
        //frmAccountDetailsMB.segDebitCard.removeAll();
        showAccuntSummaryScreen();
        glb_accountId = "";
        glb_accId = "";
    }
}

function accountDetailsClear() {
    frmAccountDetailsMB.lblUseful1.text = ""
    frmAccountDetailsMB.lblUseful2.text = ""
    frmAccountDetailsMB.lblUseful3.text = ""
    frmAccountDetailsMB.lblUseful4.text = ""
    frmAccountDetailsMB.lblUseful5.text = ""
    frmAccountDetailsMB.lblUseful6.text = ""
    frmAccountDetailsMB.lblUseful7.text = ""
    frmAccountDetailsMB.lblUseful8.text = ""
    frmAccountDetailsMB.lblUseful9.text = ""
    frmAccountDetailsMB.lblUseful10.text = ""
    frmAccountDetailsMB.lblUseful11.text = ""
    frmAccountDetailsMB.lblUseful12.text = ""
    frmAccountDetailsMB.lblUseful13.text = ""
    frmAccountDetailsMB.lblUseful14.text = ""
    frmAccountDetailsMB.lblUseful15.text = ""
    frmAccountDetailsMB.segMaturityDisplay.removeAll();
    frmAccountDetailsMB.lblUsefulValue1.text = ""
    frmAccountDetailsMB.lblUsefulValue2.text = ""
    frmAccountDetailsMB.lblUsefulValue3.text = ""
    frmAccountDetailsMB.lblUsefulValue4.text = ""
    frmAccountDetailsMB.lblUsefulValue5.text = ""
    frmAccountDetailsMB.lblUsefulValue6.text = ""
    frmAccountDetailsMB.lblUsefulValue7.text = ""
    frmAccountDetailsMB.lblUsefulValue8.text = ""
    frmAccountDetailsMB.lblUsefulValue9.text = ""
    frmAccountDetailsMB.lblUsefulValue10.text = ""
    frmAccountDetailsMB.lblUsefulValue11.text = ""
    frmAccountDetailsMB.lblUsefulValue12.text = ""
    frmAccountDetailsMB.lblUsefulValue13.text = ""
    frmAccountDetailsMB.lblUsefulValue14.text = ""
    frmAccountDetailsMB.lblUsefulValue15.text = ""
    frmAccountDetailsMB.hbxUseful7.isVisible = false;
    frmAccountDetailsMB.hbxUseful8.isVisible = false;
    frmAccountDetailsMB.hbxUseful9.isVisible = false;
    frmAccountDetailsMB.hbxUseful10.isVisible = false;
    frmAccountDetailsMB.hbxUseful11.isVisible = false;
    frmAccountDetailsMB.hbxUseful12.isVisible = false;
    frmAccountDetailsMB.hbxUseful13.isVisible = false;
    frmAccountDetailsMB.hbxUseful14.isVisible = false;
    frmAccountDetailsMB.hbxUseful15.isVisible = false;
    frmAccountDetailsMB.hbxUseful2.isVisible = true;
    frmAccountDetailsMB.hbxUseful3.isVisible = true;
    if (!(frmAccountDetailsMB.lblUsefulValue11.text == undefined || frmAccountDetailsMB.lblUsefulValue11.text == "")) {
        frmAccountDetailsMB.hbxUseful11.isVisible = true;
    }
    frmAccountDetailsMB.hbxUseful4.isVisible = true;
    frmAccountDetailsMB.hbxUseful5.isVisible = true;
    frmAccountDetailsMB.lblUseful9.isVisible = true;
    gblAccountStatus = "";
}
/*function crmCallBackFunctionacc(status, result) {
	
	if (status == 400) {
		
		
		showAccuntSummaryScreen();
	} else if (status == 300) {
		alert("status is not 300");
	}
}*/
function moreAccountDetails() {
    frmAccountDetailsMB.hbxUseful7.isVisible = false;
    frmAccountDetailsMB.hbxUseful8.isVisible = false;
    frmAccountDetailsMB.hbxUseful9.isVisible = false;
    frmAccountDetailsMB.hbxUseful10.isVisible = false;
    frmAccountDetailsMB.hbxUseful11.isVisible = false;
    frmAccountDetailsMB.hbxUseful12.isVisible = false;
    frmAccountDetailsMB.hbxUseful13.isVisible = false;
    frmAccountDetailsMB.hbxUseful14.isVisible = false;
    frmAccountDetailsMB.hbxUseful15.isVisible = false;
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue1.text)) {
        frmAccountDetailsMB.hbxUseful1.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue2.text)) {
        frmAccountDetailsMB.hbxUseful2.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue3.text)) {
        frmAccountDetailsMB.hbxUseful3.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue4.text)) {
        frmAccountDetailsMB.hbxUseful4.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue5.text)) {
        frmAccountDetailsMB.hbxUseful5.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue6.text)) {
        frmAccountDetailsMB.hbxUseful6.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue7.text)) {
        frmAccountDetailsMB.hbxUseful7.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue8.text)) {
        frmAccountDetailsMB.hbxUseful8.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue9.text)) {
        frmAccountDetailsMB.hbxUseful9.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue10.text)) {
        frmAccountDetailsMB.hbxUseful10.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue11.text)) {
        frmAccountDetailsMB.hbxUseful11.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue12.text)) {
        frmAccountDetailsMB.hbxUseful12.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue13.text)) {
        frmAccountDetailsMB.hbxUseful13.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue14.text)) {
        frmAccountDetailsMB.hbxUseful14.isVisible = true;
    }
    if (isNotBlank(frmAccountDetailsMB.lblUsefulValue15.text)) {
        frmAccountDetailsMB.hbxUseful15.isVisible = true;
    }
    /*
		if (glb_viewName != "PRODUCT_CODE_NOFIXED") {
			if(isNotBlank(frmAccountDetailsMB.lblUsefulValue11.text)){
				frmAccountDetailsMB.hbxUseful11.isVisible = true;
			}
		}
		if (glb_viewName == "PRODUCT_CODE_NOFIXED" && (frmAccountDetailsMB.lblUsefulValue2.text).length == 0) {
			
			//frmAccountDetailsMB.hboxApplySendToSave.isVisible = true;
		}
		
		if (glb_viewName == "PRODUCT_CODE_SAVING_TABLE" || glb_viewName == "PRODUCT_CODE_CURRENT_TABLE" || glb_viewName ==
			"PRODUCT_CODE_NOFEESAVING_TABLE"||glb_viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
			
			//frmAccountDetailsMB.segDebitCard.setVisibility(true);
			if (GLOBAL_DEBITCARD_TABLE != null && GLOBAL_DEBITCARD_TABLE != "") {
				var temp = [];
				var imageForDisplay="";
				var imageStatusDisplay = "";
				for (var i = 0; i < GLOBAL_DEBITCARD_TABLE.length; i++) {
				imageForDisplay="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+GLOBAL_DEBITCARD_TABLE[i]["cardLogo"]+"&modIdentifier=AccountDetails";
                imageStatusDisplay = imageForDisplay;
				  //imageForDisplay="debitcard.png"
				
				//ENH_092 - Debit Card Activation
				var cardStatus = GLOBAL_DEBITCARD_TABLE[i]["cardStatusCode"];   
				    var linkActivate = "";
				    var imgActivateCard = "";
				    
				    if (cardStatus == "" || cardStatus == "Active") {
				    	// Active - Activated
						cardStatus = kony.i18n.getLocalizedString("keyCardActivated");
						imageStatusDisplay = "";
				    } else if (cardStatus == "Pin Assign(P)") {
				    	// Pin Assign(P) - Waiting for Activate
						cardStatus = kony.i18n.getLocalizedString("keyCardWaitForActivated");
						linkActivate = kony.i18n.getLocalizedString('keyActivateCard');
				    } else {
						cardStatus = "";						
				    }
	       				       
					var segTable = {
						imgCreditCard: imageForDisplay,
						lblCreditCardNo: GLOBAL_DEBITCARD_TABLE[i]["maskedCardNo"],
						lblExpiryDate: kony.i18n.getLocalizedString("ExpiryDate") + "  " +GLOBAL_DEBITCARD_TABLE[i]["expiryDate"],
						lblCardStatus: cardStatus,	
						linkActivateCard: linkActivate,
						imgActivateLinkImg: imageStatusDisplay
					}
					temp.push(segTable);
					//frmAccountDetailsMB.segDebitCard.setData(temp);
					//frmAccountDetailsMB.segDebitCard.setVisibility(true);
					//frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
				}
				frmAccountDetailsMB.segDebitCard.setData(temp);
				frmAccountDetailsMB.segDebitCard.setVisibility(true);
				frmAccountDetailsMB.hbxLinkedTo.isVisible = true;
			}
		} */
    if (gblAccountTable != null && gblAccountTable != undefined && gblIndex != null && gblIndex != undefined && gblAccountTable["custAcctRec"] != null) {
        //moreAccountDetails();
        frmAccountDetailsMB.show();
    }
}

function shortCutActDetailsPage() {
    if (!flowSpa) {
        /*
        currForm = frmAccountDetailsMB;
        var deviceHght = kony.os.deviceInfo().deviceHeight;
        var osType = kony.os.deviceInfo().name;
        var btnskin = "btnToDoIcon";
        var btnFocusSkin = "btnToDoIconFoc";
		
        frmAccountDetailsMB.hbxOnHandClick.isVisible = true;
        frmAccountDetailsMB.btnOptions.skin = btnFocusSkin;
        frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
        frmAccountDetailsMB.imgHeaderRight.src = "empty.png";		
		
        if (frmAccountDetailsMB.hbxOnHandClick.isVisible) {
        	frmAccountDetailsMB.hbxOnHandClick.isVisible = false;
        	frmAccountDetailsMB.btnOptions.skin = btnskin;
        	frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
        	frmAccountDetailsMB.imgHeaderRight.src = "empty.png";
        } else {
        	frmAccountDetailsMB.hbxOnHandClick.isVisible = true;
        	frmAccountDetailsMB.btnOptions.skin = btnFocusSkin;
        	frmAccountDetailsMB.imgHeaderMiddle.src = "arrowtop.png";
        	frmAccountDetailsMB.imgHeaderRight.src = "empty.png";
        }
        */
    } else {
        toDo();
    }
}

function postShowOfAccountSummary() {
    deviceInfo = kony.os.deviceInfo();
    if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
        isMenuRendered = false;
        isMenuShown = false;
        frmAccountSummaryLanding.scrollboxMain.scrollToEnd();
    }
}

function checkShowDebitCardMenu() {
    if (gblAccountTable["custAcctRec"][gblIndex]["productID"] != undefined && gblAccountTable["custAcctRec"][gblIndex]["productID"] != "" && gblDebitCardResult != null && gblDebitCardResult != "") {
        var allowList = gblDebitCardResult["enableDebitCardProductList"];
        var productCode = gblAccountTable["custAcctRec"][gblIndex]["productID"];
        var allowAccountStatusList = gblDebitCardResult["enableDebitCardAccountStatus"];
        var acctStatus = gblAccountTable["custAcctRec"][gblIndex]["acctStatus"];
        var idx = acctStatus.indexOf("|");
        if (idx > -1) {
            acctStatus = acctStatus.substring(0, idx);
            acctStatus = acctStatus.trim();
        }
        if (allowList.indexOf(productCode) > -1 && allowAccountStatusList.indexOf(acctStatus) > -1) {
            var cardActivated = 0;
            var cardNew = 0;
            if (GLOBAL_DEBITCARD_TABLE != null && GLOBAL_DEBITCARD_TABLE != "") {
                for (var i = 0; i < GLOBAL_DEBITCARD_TABLE.length; i++) {
                    var cardStatus = GLOBAL_DEBITCARD_TABLE[i]["cardStatus"];
                    if (cardStatus == "Active") {
                        cardActivated++;
                    } else if (cardStatus == "Not Activated") {
                        cardNew++;
                    }
                }
            }
            if (cardActivated > 0 && cardNew > 0) {
                frmAccountDetailsMB.btnlinkDebitCard.text = kony.i18n.getLocalizedString('keyDebitCard') + "(" + cardActivated + kony.i18n.getLocalizedString('keyForAnd') + cardNew + kony.i18n.getLocalizedString('keyForNew') + ")";
            } else if (cardActivated == 0 && cardNew > 0) {
                frmAccountDetailsMB.btnlinkDebitCard.text = kony.i18n.getLocalizedString('keyDebitCard') + "(" + cardNew + kony.i18n.getLocalizedString('keyForNew') + ")";
            } else if (cardNew == 0 && cardActivated > 0) {
                frmAccountDetailsMB.btnlinkDebitCard.text = kony.i18n.getLocalizedString('DBA01_Title') + "(" + cardActivated + ")";
            } else {
                frmAccountDetailsMB.btnlinkDebitCard.text = kony.i18n.getLocalizedString('DBA01_Title');
            }
            frmAccountDetailsMB.flexLineDebitCards.setVisibility(true);
            frmAccountDetailsMB.flexLinkDebitCards.setVisibility(true);
        } else {
            frmAccountDetailsMB.flexLineDebitCards.setVisibility(false);
            frmAccountDetailsMB.flexLinkDebitCards.setVisibility(false);
        }
    }
}