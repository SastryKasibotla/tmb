
function invokeMigrationUserCompositeSpa(currPwd,newPwd,userId){

 var inputParam = {};
 inputParam["currPwd"] = currPwd;
 inputParam["newPwd"] = newPwd;
 inputParam["newuserId"] = userId;
 inputParam["modifyUserErrorAttempt"] = gblModifyUserErr;
 showLoadingScreen();
 invokeServiceSecureAsync("ForceChangePasswordExecute", inputParam, callBackCompCMigUserSpa);

}



function callBackCompCMigUserSpa(status,resulttable){

	if(status == 400){
		if(resulttable["opstatus"] == 0){
				frmSpaMigrationComplete.image250285458166.src="iconcomplete.png";
				frmSpaMigrationComplete.label50285458167.text = kony.i18n.getLocalizedString("appReadytoUse");
				//gblIsFromMigration = true;
				gblActionCode = "04";
				//IBsendTnCMailPartyInq();
				dismissLoadingScreen();
				frmSpaMigrationComplete.show();
		}else if (resulttable["errCode"] == "validationErr") {
		  			
                    if(resulttable["errorKey"] == "keyUserIdUnderRestrictedList"){
                    	dismissLoadingScreen();
                    	showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
                    	frmSpaMigrationSetuserid.txtUserID.text = "";
                    	frmSpaMigrationSetuserid.txtUserID.setFocus(true);
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyRepetativeChars"){
                    	dismissLoadingScreen();
                    	showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
                    	frmSpaMigrationSetuserid.txtUserID.setFocus(true);
                    	frmSpaMigrationSetuserid.txtUserID.text = "";
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyWrongNewUserId"){
                    	dismissLoadingScreen();
                    	showAlertRcMB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
                    	frmSpaMigrationSetuserid.txtUserID.setFocus(true);
                    	frmSpaMigrationSetuserid.txtUserID.text = "";
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyPassSettingGuidelinesIB"){
                    	dismissLoadingScreen();
                    	showAlertRcMB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
                    	frmSpaMigrationSetuserid.txtTransPass.text = "";
                    	frmSpaMigrationSetuserid.txtConfirmPassword.text = "";
                    	frmSpaMigrationSetuserid.txtTransPass.setFocus(true);
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyWrongPwd"){
                    	dismissLoadingScreen();
                    	showAlertRcMB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
                    	frmSpaMigrationSetuserid.txtTransPass.setFocus(true);
                    	frmSpaMigrationSetuserid.txtTransPass.text = "";
                    	frmSpaMigrationSetuserid.txtConfirmPassword.text = "";
                    	return false;
                    }
                    
		}else if(resulttable["errCode"] == "VrfyAcPWDErr00001"){
			dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"));
			frmSpaMigrationSetuserid.txtCnfCrntPwd.setFocus(true);
			return false;
		}else if(resulttable["errCode"] == "VrfyAcPWDErr00002"){
			dismissLoadingScreen();
			showAlertRcMB(kony.i18n.getLocalizedString("keyInCrtCurPwd"), kony.i18n.getLocalizedString("info"));
			frmSpaMigrationSetuserid.txtCnfCrntPwd.text = "";
			frmSpaMigrationSetuserid.txtCnfCrntPwd.setFocus(true);
			return false;
		}else if(resulttable["errCode"] == "VrfyTxPWDErr00003"){
			 dismissLoadingScreen();
			 showAlertRcMB(kony.i18n.getLocalizedString("keyInCrtCurPwdLockd"), kony.i18n.getLocalizedString("info"));
			 invokeLogoutService();
			 return false;
		}else if(resulttable["errCode"] == "verifyPwdOtherErr"){
			 dismissLoadingScreen();
			 showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			 frmSpaMigrationSetuserid.txtCnfCrntPwd.setFocus(true);
			 return false;
		}else if(resulttable["errCode"] == "modifyUserErr"){
			 dismissLoadingScreen();
			 gblModifyUserErr = resulttable["retryModifyUser"]
				if(resulttable["errMsg"] != null && resulttable["errMsg"] != ""){
				showAlertRcMB(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
				}else{
				showAlertRcMB(kony.i18n.getLocalizedString("keyModifyUserMigErr"), kony.i18n.getLocalizedString("info"));
				}
			 
			 frmSpaMigrationSetuserid.txtUserID.setFocus(true);
			 return false;
		}else if(resulttable["errCode"] == "modifyUserErrLimitReached"){
			 dismissLoadingScreen();
			 showAlertRcMB(kony.i18n.getLocalizedString("keyIBLoginError003"), kony.i18n.getLocalizedString("info"));
			 invokeLogoutService();
			 return false;
		}else if(resulttable["errCode"] == "resetPwdErr"){
			 dismissLoadingScreen();
			 if(resulttable["errMsg"] != null && resulttable["errMsg"] != ""){
				showAlertRcMB(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
				}else{
				showAlertRcMB(kony.i18n.getLocalizedString("keyResetPwdMigUser"), kony.i18n.getLocalizedString("info"));
				}
			 frmSpaMigrationSetuserid.txtTransPass.setFocus(true);
			 return false;
		}else{
		 	dismissLoadingScreen();
			 showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			 return false;
		}
		
	
	}

}
