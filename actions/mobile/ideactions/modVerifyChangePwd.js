








/*function invokePwdChange() {
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
	showLoadingPopUpScreen();
	inputParam = {};
	if (gblChangePWDFlag == 1) {
		inputParam["loginModuleId"] = "MB_TxPwd";
	} else if (gblChangePWDFlag == 0) {
		inputParam["loginModuleId"] = "MB_AcPwd";
	}
	if (gblChangePWDFlag == 0) {
		if(frmCMChgAccessPin.tbxCurAccPin.isVisible == true){
		inputParam["oldPassword"] = frmCMChgAccessPin.tbxCurAccPin.text;
		inputParam["newPassword"] = frmCMChgAccessPin.txtAccessPwd.text;
		}else{
		inputParam["oldPassword"] = frmCMChgAccessPin.tbxCurAccPinUnMask.text;
		inputParam["newPassword"] = frmCMChgAccessPin.txtAccessPwdUnMask.text;
		}
	} else if (gblChangePWDFlag == 1) {
		if(frmCMChgTransPwd.tbxTranscCrntPwd.isVisible == true){
		 inputParam["oldPassword"] = frmCMChgTransPwd.tbxTranscCrntPwd.text
	     inputParam["newPassword"] = frmCMChgTransPwd.txtTransPass.text	     
		}else{
		inputParam["oldPassword"] = frmCMChgTransPwd.tbxTranscCrntPwdTemp.text;
		inputParam["newPassword"] = frmCMChgTransPwd.txtTemp.text;
		}
		
	}
	inputParam["retryCounterVerifyAccessPin"] = "0";
	inputParam["retryCounterVerifyTransPwd"] = "0";
	invokeServiceSecureAsync("changePassword", inputParam, callBackChangePwd)
}*/

/*function callBackChangePwd(status, resulttable) {
	
	if (status == 400) {
		//For activity logging
		var successFlag = false;
		var actionType = "";
		//var activityId = "";
		if (resulttable["opstatus"] == 0) {
			successFlag = true;
			popupTractPwd.dismiss();
			if (gblChangePWDFlag == 0) {
				actionType = "11";
				cancelTimerChngAc();
			} else if (gblChangePWDFlag == 1) {
				actionType = "12";
			}
			invokeCRMProfileUpdate(actionType);
			if (gblChangePWDFlag == 0)
			  NotificationChangeAccessPinService();
			else  
			  NotificationChangeTransactionPasswdService();
			frmMyProfile.show();
			TMBUtil.DestroyForm(frmCMChgAccessPin);
			TMBUtil.DestroyForm(frmCMChgTransPwd);
		} else if (resulttable["code"] != null && resulttable["code"] == "10403") {
//			showAlertPwdLocked(kony.i18n.getLocalizedString("keyTranPwdLockedErrMsg"));
			if (gblChangePWDFlag == 0) {
				showAlertPwdLocked(kony.i18n.getLocalizedString("keyTranPwdLockedErrMsg"));
				cancelTimerChngAc();
				actionType = "18";
				invokeCRMProfileUpdate(actionType);
				invokeLogoutService();
			} else if (gblChangePWDFlag == 1) {
				actionType = "19";
				showTranPwdLockedPopup();
			}
			//activityLogServiceCall(activityId, "0000000000000", "02");
		} else if (resulttable["code"] != null && resulttable["code"] == "56789") {
			showAlertPwdLocked(kony.i18n.getLocalizedString("invalidCurrPIN"));
			if (gblChangePWDFlag == 0) cancelTimerChngAc();
		} else {
			alert(" " + resulttable["errMsg"]);
			if (gblChangePWDFlag == 0) cancelTimerChngAc();
			//activityLogServiceCall(activityId, "0000000000000", "02");
		}
		//Activity logging
		if (successFlag) {
			if (gblChangePWDFlag == 1) {
				activityLogServiceCall("020", "", "01", "", "", "", "", "", "", "")
				//transaction pwd change 
			} else if (gblChangePWDFlag == 0) {
				//access pwd change
				activityLogServiceCall("019", "", "01", "", "", "", "", "", "", "")
			}
		} else {
			if (gblChangePWDFlag == 1) {
				activityLogServiceCall("020", "", "02", "", "", "", "", "", "", "")
				//transaction pwd change 
			} else if (gblChangePWDFlag == 0) {
				//access ped change
				activityLogServiceCall("019", "", "02", "", "", "", "", "", "", "")
			}
		}
		kony.application.dismissLoadingScreen();
	}
}*/
/**
 * description
 * @returns {}
 */

function onClickConfirmAccessPIN(accessPIN) {
	//var info1 = kony.i18n.getLocalizedString("info");
	//	var okk = kony.i18n.getLocalizedString("keyOK");
	//PWd validation should be done here
	/*if (gblShowPwd == 3) {
		//Defining basicConf parameter for alert
		var basicConf = {
			message: "Entered wrong Access PIN 3 times",
			alertType: constants.ALERT_TYPE_INFO,
			alertTitle: info1,
			yesLabel: okk,
			noLabel: "",
			alertHandler: handle2
		};
		//Defining pspConf parameter for alert
		var pspConf = {};
		//Alert definition
		var infoAlert = kony.ui.Alert(basicConf, pspConf);

		function handle2(response) {}
		//	alert("Entered wrong Access PIN 3 times")
		isSignedUser = false;
		invokeLogoutService();
		return false;
	}
	*/
	var curPin = accessPIN;
	var isNum = kony.string.isNumeric(curPin);
	if (curPin != null && curPin.length < 6) {
		gblShowPwd++;
		setTransPwdFailedError(kony.i18n.getLocalizedString("keyIncorrectPIN"));
		//Defining basicConf parameter for alert
		//var basicConf = {
		//			message: "Please enter 6 Characters for Access PIN",
		//			alertType: constants.ALERT_TYPE_INFO,
		//			alertTitle: info1,
		//			yesLabel: okk,
		//			noLabel: "",
		//			alertHandler: handle2
		//		};
		//Defining pspConf parameter for alert
		var pspConf = {};
		//Alert definition
		//var infoAlert = kony.ui.Alert(basicConf, pspConf);

		function handle2(response) {}
		//	alert("Please enter 6 Characters for Access PIN")
		return false;
	} else if (!isNum) {
		gblShowPwd++;
		popupTractPwd.lblPopupTract7.setVisibility(true);
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyIncorrectPIN");
		popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
		//Defining basicConf parameter for alert
		//var basicConf = {
		//			message: "Please enter only numeric characters",
		//			alertType: constants.ALERT_TYPE_INFO,
		//			alertTitle: info1,
		//			yesLabel: okk,
		//			noLabel: "",
		//			alertHandler: handle2
		//		};
		//Defining pspConf parameter for alert
		var pspConf = {};
		//Alert definition
		//var infoAlert = kony.ui.Alert(basicConf, pspConf);

		function handle2(response) {}
		//	alert("Please enter only numeric characters")
		return false;
	} else {
		//if(kony.application.getCurrentForm().id = "frmCMChgTransPwd") {
			changeTxnPasswordcompositeService(accessPIN);
	
	}
}
/**
 * description
 * @returns {}
 */

function onClickConfirmTranscPwd(transcPwd) {
	//PWd validation should be done here
	//if(gblShowPwd ==3)
	//	{
	//	alert("Entered wrong Transaction Password 3 times")
	//	isSignedUser = false;
	//	invokeLogoutService();
	//frmAfterLogoutMB.show();
	//	return false;
	//}
	var curPin = transcPwd;
	//var pat1 = /[A-Za-z]/g
	//var pat2 = /[0-9]/g
	//var isAlpha = pat1.test(curPin)
	//var isNum = pat2.test(curPin)
	if (curPin != null && curPin.length < 0) {
		gblShowPwd++;
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("KeyIncorrectPWD");
		popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
		//alert("Please enter atleast 8 Characters for Transaction Password")
		return false;
	} /*else if (isAlpha == false || isNum == false) {
		gblShowPwd++;
		popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("KeyIncorrectPWD");
		popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
		//alert("Please Enter atleast 1 alphabet and 1 numeric character for Transaction Password");
		return false;
	} */
	//if(kony.application.getCurrentForm().id = "frmCMChgTransPwd") {
		changeTxnPasswordcompositeService(transcPwd);

	
}
/**
 * description
 * @returns {}
 */

//function onClickAboutMe() {
//	frmMyProfile.show();
//}
/**
 * description
 * @returns {}
 */

//function invokeCRMProfileUpdate(actionType) {
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
	/*showLoadingScreen();
	inputParam = {};
	if (actionType == "10") {
		inputParam["mbUserStatusId"] = "05";
		inputParam["actionType"] = "17";
	}
	inputParam["actionType"] = actionType;
	invokeServiceSecureAsync("crmProfileMod", inputParam, callBackCRMPUpdate)*/
//}

/**
 * description
 * @returns {}
 */

function showAlertPwdLocked(errMsg) {
	var alert_seq0_act1 = kony.ui.Alert({
		"message": errMsg,
		"alertType": constants.ALERT_TYPE_ERROR,
		"alertTitle": "Error",
		"yesLabel": "Ok",
		"noLabel": "",
		"alertIcon": "",
		"alertHandler": null
	}, {});
}

function showAlertPwdLockedwithHandler(errMsg) {
	var alert_seq0_act1 = kony.ui.Alert({
		"message": errMsg,
		"alertType": constants.ALERT_TYPE_ERROR,
		"alertTitle": "Error",
		"yesLabel": "Ok",
		"noLabel": "",
		"alertIcon": "",
		"alertHandler": invokeLogoutService
	}, {});
}

////changed...
//function NotificationChangeAccessPinService() {
//	gblProfileNotifyFlag = false;
//	var newemail;
//	tempemail = frmeditMyProfile.txtemailvalue.text;
//	if(tempemail == null || tempemail == "")
//	{  newemail = gblEmailAddr; }
//	else 
//	{
//	newemail = tempemail;
//	}
//	
//	var inputparam = [];
//	inputparam["custName"] = customerName;
//	inputparam["notificationType"] = "Email"; // check this : which value we have to pass
//	inputparam["notificationSubject"] ="";
//	inputparam["notificationContent"] ="";
//	inputparam["NoSendInd"] = "0";
//	inputparam["Locale"] = kony.i18n.getCurrentLocale();
//	inputparam["source"] = "changeAccessPin";
//   
//	invokeServiceSecureAsync("NotificationAdd", inputparam, NotificationChangeAccessPinServicecallBack)
//}
//
//function NotificationChangeAccessPinServicecallBack(status, result) {
//	if (status == 400) {
//		if (result["opstatus"] == 0) {
//			var StatusCode = result["StatusCode"];
//			var Severity = result["Severity"];
//			var StatusDesc = result["StatusDesc"];
//			if (StatusCode == 0) {
//				//frmMyProfile.show();
//				//toCallViewProfileonSave();
//			} else {
//				
//				return false;
//			}
//		} else {
//			
//		}
//	}
//} //toCallViewProfileonSave();

//function NotificationChangeTransactionPasswdService() {
//	gblProfileNotifyFlag = false;
//	var newemail;
//	tempemail = frmeditMyProfile.txtemailvalue.text;
//	if(tempemail == null || tempemail == "")
//	{  newemail = gblEmailAddr; }
//	else 
//	{
//	newemail = tempemail;
//	}
//	
//	var inputparam = [];
//	inputparam["custName"] = customerName;
//	inputparam["notificationType"] = "Email"; // check this : which value we have to pass
//	inputparam["notificationSubject"] ="";
//	inputparam["notificationContent"] ="";
//	inputparam["NoSendInd"] = "0";
//	inputparam["Locale"] = kony.i18n.getCurrentLocale();
//	inputparam["source"] = "changeTransactionPassword";
//	invokeServiceSecureAsync("NotificationAdd", inputparam, NotificationChangeTransactionPasswdServicecallBack)
//}
//
//function NotificationChangeTransactionPasswdServicecallBack(status, result) {
//	if (status == 400) {
//		if (result["opstatus"] == 0) {
//			var StatusCode = result["StatusCode"];
//			var Severity = result["Severity"];
//			var StatusDesc = result["StatusDesc"];
//			if (StatusCode == 0) {
//				//frmMyProfile.show();
//				//toCallViewProfileonSave();
//				//show success popup 
//			} else {
//				
//				return false;
//			}
//		} else {
//			
//		}
//	}
//} //toCallViewProfileonSave();

function changeTxnPasswordcompositeService(password) {

	if (password == "")
	{
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
		return;
	}
	var composite_inputparam = {};
 	
 	composite_inputparam["password"] = password;
 	composite_inputparam["retryCounterVerifyAccessPin"] = "0";
 	composite_inputparam["retryCounterVerifyTransPwd"] = "0";
 	var actionType = "";
 	if (gblChangePWDFlag == 1) {
  		composite_inputparam["loginModuleId"] = "MB_TxPwd";
  		if(frmCMChgTransPwd.tbxTranscCrntPwd.isVisible == true){
   			composite_inputparam["oldPassword"] = frmCMChgTransPwd.tbxTranscCrntPwd.text
      		composite_inputparam["newPassword"] = frmCMChgTransPwd.txtTransPass.text      
  		}else{
  			composite_inputparam["oldPassword"] = frmCMChgTransPwd.tbxTranscCrntPwdTemp.text;
  			composite_inputparam["newPassword"] = frmCMChgTransPwd.txtTemp.text;
  		}
  		composite_inputparam["source"] = "changeTransactionPassword";
 	} else if (gblChangePWDFlag == 0) {
  		composite_inputparam["loginModuleId"] = "MB_AcPwd";
  		if(frmCMChgAccessPin.tbxCurAccPin.isVisible == true){
  			composite_inputparam["oldPassword"] = frmCMChgAccessPin.tbxCurAccPin.text;
  			composite_inputparam["newPassword"] = frmCMChgAccessPin.txtAccessPwd.text;
  		}else{
  			composite_inputparam["oldPassword"] = frmCMChgAccessPin.tbxCurAccPinUnMask.text;
  			composite_inputparam["newPassword"] = frmCMChgAccessPin.txtAccessPwdUnMask.text;
  		}
  		composite_inputparam["source"] = "changeAccessPin";
 	}
	// inputParam["actionType"] = actionType;
 	//Notification related params
 	composite_inputparam["custName"] = customerName;
 	composite_inputparam["notificationType"] = "Email"; // check this : which value we have to pass
 	composite_inputparam["notificationSubject"] ="";
 	composite_inputparam["notificationContent"] ="";
 	composite_inputparam["NoSendInd"] = "0";
 	composite_inputparam["Locale"] = kony.i18n.getCurrentLocale();
 
 	//Activity
 	composite_inputparam["activityTypeID"] = "020";
 	composite_inputparam["errorCd"] = ""; 
 	composite_inputparam["activityStatus"] = "01";
 	composite_inputparam["deviceNickName"] = "";
 	composite_inputparam["activityFlexValues1"] = "";
 	composite_inputparam["activityFlexValues2"] = "";
 	composite_inputparam["activityFlexValues3"] = "";
 	composite_inputparam["activityFlexValues4"] = "";
 	composite_inputparam["activityFlexValues5"] = "";
 
 	if (gblDeviceInfo.name == "thinclient")
  		composite_inputparam["channelId"] = GLOBAL_IB_CHANNEL;
 	else
  		composite_inputparam["channelId"] = GLOBAL_MB_CHANNEL;
 	composite_inputparam["logLinkageId"] = "";
 	showLoadingScreen();
 	invokeServiceSecureAsync("chgAccPinTxnPwdCompositeService", composite_inputparam, changeTxnPwdCompositeCallBack)
}

function changeTxnPwdCompositeCallBack(status, resulttable)
{
	if(status == 400) {
	  	dismissLoadingScreen();
  		if(resulttable["opstatus"] == 0) {
   			popupTractPwd.dismiss();
   			if (gblChangePWDFlag == 0) {
    			cancelTimerChngAc();
   			}
   			completeicon = true;
   			frmMyProfile.show();
   			TMBUtil.DestroyForm(frmCMChgAccessPin);
   			TMBUtil.DestroyForm(frmCMChgTransPwd);
  		} else if (resulttable["opstatus_verifyPwd"] != 0) {
   			if (resulttable["code"] != null && resulttable["code"] == "10403") {
	     		var actionType = "";
	    		if (gblChangePWDFlag == 1) {
	     			showAlertPwdLockedwithHandler(kony.i18n.getLocalizedString("keyAccessPwdLocked"));
	     			//invokeLogoutService();
	    		} else if (gblChangePWDFlag == 0) {
	     			cancelTimerChngAc();
	     			showTranPwdLockedPopup();
	    		}
	   		} else if (gblChangePWDFlag == 0 && resulttable["code"] != null && resulttable["code"] == "10020") {
	    		setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
	   		} else if (gblChangePWDFlag == 1&& resulttable["code"] != null && resulttable["code"] == "10020") {
	    		popupTractPwd.tbxPopupTractPwdtxtAccPin.text = "";
	    		setTransPwdFailedError(kony.i18n.getLocalizedString("keyIncorrectPIN"));
	   		} else {
	    		alert(" " + resulttable["errMsg"]);
	   		}	
  		}
  		else if(resulttable["opstatus_changePwd"] != 0)
	  	{
  			popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
	   		if (resulttable["code"] != null && resulttable["code"] == "10403") {
	    		if (gblChangePWDFlag == 0) {
	     			showAlertPwdLockedwithHandler(kony.i18n.getLocalizedString("keyAccessPwdLocked"));
	     			cancelTimerChngAc();
	     			//invokeLogoutService();
	    		} else if (gblChangePWDFlag == 1) {
	     			showTranPwdLockedPopup();
	    		}
	   		} else if (resulttable["code"] != null && resulttable["code"] == "56789") {
	    		showAlertPwdLocked(kony.i18n.getLocalizedString("invalidCurrPIN"));
	    		if (gblChangePWDFlag == 0) cancelTimerChngAc();
	   		}
	   		else if (gblChangePWDFlag == 0 && resulttable["code"] != null && resulttable["code"] == "10200") {
	    		showAlertRcMB(kony.i18n.getLocalizedString("invalidNewPin"), kony.i18n.getLocalizedString("error"), "error");
	    		frmCMChgAccessPin.tbxCurAccPin.text = "";
        		frmCMChgAccessPin.tbxCurAccPinUnMask.text = "";
      			frmCMChgAccessPin.txtAccessPwdUnMask.text = "";
      			frmCMChgAccessPin.txtAccessPwd.text = "";
       			popupTractPwd.dismiss();
	    		if (gblChangePWDFlag == 0) cancelTimerChngAc();
	   		}
	   		else if (gblChangePWDFlag == 0 && resulttable["code"] != null && resulttable["code"] == "10020") {
	    		showAlertRcMB(kony.i18n.getLocalizedString("invalidCurrPIN"), kony.i18n.getLocalizedString("error"), "error");
	    		frmCMChgAccessPin.tbxCurAccPin.text = "";
        		frmCMChgAccessPin.tbxCurAccPinUnMask.text = "";
      			frmCMChgAccessPin.txtAccessPwdUnMask.text = "";
      			frmCMChgAccessPin.txtAccessPwd.text = "";
       			popupTractPwd.dismiss();
	    		if (gblChangePWDFlag == 0) cancelTimerChngAc();
	   		}
	   		else if (gblChangePWDFlag == 1 && resulttable["code"] != null && resulttable["code"] == "10020") {
       			frmCMChgTransPwd.tbxTranscCrntPwd.text = "";
        		frmCMChgTransPwd.txtTransPass.text = "";
      			frmCMChgTransPwd.tbxTranscCrntPwdTemp.text = "";
      			frmCMChgTransPwd.txtTemp.text = "";
       			popupTractPwd.dismiss();
       			showAlertPwdLocked(kony.i18n.getLocalizedString("invalidTxnPwd"));
   			} else {
	    		alert(" " + resulttable["errMsg"]);
	    		if (gblChangePWDFlag == 0) cancelTimerChngAc();
	   		}
	  	}
 	} else if(status == 300){
  		dismissLoadingScreen();
 	}
}
