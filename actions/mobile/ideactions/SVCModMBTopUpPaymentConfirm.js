







/*
 * Method CallBillPaymentAddService returns the status of service
 *
 */

function callTopUpPaymentAddService() {
	var toAccType;
	var toAcctNo = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
	var billerMethod = frmSelectBiller.segMyBills.selectedItems[0]["billerMethod"];
	GBLFINANACIALACTIVITYLOG.toAcctId = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
	var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0]["billerGroupType"];
	
	
	var transferFee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
	var transferAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)); //+parseFloat(transferFee) 
	if (toAcctNo.length == 14) {
		toAccType = "SDA";
	} else {
		fourthDigit = toAcctNo.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			toAccType = "SDA";
			toAcctNo = "0000" + toAcctNo;
		} else {
			toAccType = "DDA";
		}
	}
	var tranCode;
	var fromAccType = frmBillPaymentConfirmationFuture.lblTopUpProductName.text;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	var invoice;
	// if (gblreccuringDisableAdd == "Y") {
	invoice = "XXX"; //topup
	// } else {
	// invoice = "XXX";//workaround
	//   }
	var accTypeCode;
	if (toAccType == "DDA") {
		accTypeCode = "0000";
	} else {
		accTypeCode = "0200";
	}
	var fromAcctID;
	var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
	inputParam = {};
	var text = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text;
	var compcode = text.substring(text.lastIndexOf("(") + 1, text.lastIndexOf(")"));
	if (flowSpa) 
	{
    	var selectedItem = gblnormSelIndex;
    	var selectedData = frmTopUp.segSliderSpa.data[gblnormSelIndex];
    }
    else
    {
    	var selectedItem = frmTopUp.segSlider.selectedIndex[1];
    	var selectedData = frmTopUp.segSlider.data[selectedItem];
    }
	var frmFiident = selectedData.fromFIIdent
	inputParam["fromAcctNo"] = fromAcctID; //frmBillPaymentConfirmationFuture.lblAccountNum.text;
	inputParam["fromAcctTypeValue"] = fromAcctType;
	inputParam["toAcctNo"] = toAcctNo;
	inputParam["toAcctTypeValue"] = toAccType;
	inputParam["toFIIdent"] = fiident;
	inputParam["frmFiident"] = frmFiident;
	inputParam["transferAmt"] = transferAmount.toFixed(2); //need to know this value;//"amt+payment fee"
	inputParam["tranCode"] = tranCode;
	inputParam["channelName"] = "MB";
	inputParam["billPmtFee"] = parseFloat(transferFee);
	inputParam["pmtRefIdent"] = frmTopUp.lblRef1Value.text;
	inputParam["invoiceNum"] = invoice; //frmBillPayment.lblRef2Value.text;
	inputParam["PostedDt"] = getTodaysDate();
	inputParam["EPAYCode"] = "EPYS";
	inputParam["CompCode"] = compcode;
	inputParam["billPay"] = "billPay";
	
	invokeServiceSecureAsync("billpaymentAdd", inputParam, TopUpBillPaymentAddServiceCallBack);
}

function callTopUpOnlinePaymentAddService() {
	var toAccType;
	var toAcctNo = frmSelectBiller["segMyBills"]["selectedItems"][0]["ToAccountKey"];
	GBLFINANACIALACTIVITYLOG.toAcctId = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
	var billerMethod = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerMethod"];
	var billerGroupType = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerGroupType"];
	
	
	
	var transferFee = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text)).toFixed(2);
	var transferAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)).toFixed(2); //+ parseFloat(transferFee.substring(0, transferFee.indexOf(" "))) ;
	
	if (toAcctNo.length == 14) {
		toAccType = "SDA";
	} else {
		fourthDigit = toAcctNo.charAt(3);
		
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			toAccType = "SDA";
			toAcctNo = "0000" + toAcctNo;
		} else {
			toAccType = "DDA";
		}
		
	}
	var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text; //fromAcctID;  
	var fromAcctID = "";
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	var tranCode;
	if (fromAcctType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	var invoice;
	invoice = "XXX"; //workaround
	var accTypeCode;
	if (toAccType == "DDA") {
		accTypeCode = "0000";
	} else {
		accTypeCode = "0200";
	}
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
	
	var ref1 = "";
	var ref2 = "";
	var ref3 = "";
	var ref4 = "";
	var bankrefID = "";
	var gblCompCode = frmSelectBiller["segMyBills"]["selectedItems"][0]["BillerCompCode"].text;
	
		if(gblCompCode == "2151" || gblCompCode == "2347" || gblCompCode == "2348" || gblCompCode == "0333" || gblCompCode == "0594" || gblCompCode == "0803")
	{
		bankrefID = BankRefId;
		
	} else if(gblCompCode == "0014" || gblCompCode == "0249" || gblCompCode == "2270" ) {
	
		bankrefID = TranId;
	} else {
		bankrefID = "";
	}
	if(gblCompCode == "2151" || gblCompCode == "2347" || gblCompCode == "2348" || gblCompCode == "0333" || gblCompCode == "0254" || gblCompCode == "0185")
	{
		ref1 = gblRef1;
		ref2 = gblRef2;
		ref3 = gblRef3;
		ref4 = gblRef4; 
	} else {
		ref1 = "";
		ref2 = "";
		ref3 = "";
		ref4 = ""; 
	} 
	var TranID = TranId; //frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
	inputParam["TrnId"] = TranID;
	inputParam["BankId"] = "011";
	inputParam["BranchId"] = "0001";
	inputParam["BankRefId"] = bankrefID;
	inputParam["Amt"] = transferAmount;
	inputParam["Ref1"] = ref1; //frmIBBillPaymentLP.lblBPRef1Val.text;
	inputParam["Ref2"] = ref2; //frmIBBillPaymentLP.lblRefVal2.text;
	inputParam["Ref3"] = ref3;
	inputParam["Ref4"] = ref4;
	inputParam["MobileNumber"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	inputParam["fromAcctNo"] = fromAcctID; // removeHyphenIB(frmIBBillPaymentConfirm.lblFromAccountNo.text); //
	inputParam["fromAcctTypeValue"] = fromAcctType;
	inputParam["toAcctNo"] = toAcctNo;
	inputParam["toAcctType"] = toAccType;
	inputParam["toFIIdent"] = fiident;
	if(flowSpa)
	{
	inputParam["frmFiident"] = gblmbSpaselectedData.fromFIIdent;
	}
	else
	{
	inputParam["frmFiident"] = frmTopUp.segSlider.selectedItems[0].fromFIIdent;
	}
	inputParam["transferAmt"] = transferAmount;
	inputParam["TranCode"] = tranCode;
	inputParam["channelName"] = "MB";
	inputParam["billPmtFee"] = transferFee;
	inputParam["pmtRefIdent"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	inputParam["invoiceNum"] = invoice;
	inputParam["PostedDt"] = getTodaysDate();
	inputParam["EPAYCode"] = "EPYS";
	inputParam["compCode"] = gblCompCode;
	inputParam["InterRegionFee"] = "0.00";
	inputParam["billPay"] = "billPay";
	
	invokeServiceSecureAsync("onlinePaymentAdd", inputParam, TopUpPaymentOnlinePaymentAddServiceCallBack);
}
/*
 * Method CallBillPaymentTransferService
 *
 */

function callTopUpPaymentTransferService() {
	var billerMethod = frmSelectBiller.segMyBills.selectedItems[0].billerMethod;
	GBLFINANACIALACTIVITYLOG.toAcctId = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
	var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0].billerGroupType;
	inputParam = {};
	var i = gbltranFromSelIndex[1];
	var toAcctTypeValue;
	var tValue;
	var fValue;
	if(flowSpa)
	{
	 var fromData = frmTopUp.segSliderSpa.data;
	}
	else
	{
    var fromData = frmTopUp.segSlider.data;
    }
	var frmID = fromData[i].lblActNoval;
	//var fromFIIdent=fromData[i].fromFIIdent;
	var fromAcctID;
	for (var i = 0; i < frmID.length; i++) {
		if (frmID[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmID[i];
			} else {
				fromAcctID = fromAcctID + frmID[i];
			}
		}
	}
		if (fromAcctID.length == 14) {
			fromAcctTypeValue = "SDA";
			} else {
				fourthDigit = fromAcctID.charAt(3);
			if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
				fromAcctTypeValue = "SDA";
				fromAcctID = "0000" + fromAcctID;
			} else {
			fromAcctTypeValue = "DDA";
		}
	}
	//alert(billerMethod);
	var toAcctNo = frmTopUp.lblRef1Value.text
	toAcctNo = kony.string.replace(toAcctNo, "-", "");
	if (billerMethod == 2) {
		toAcctTypeValue = "CCA";//kony.i18n.getLocalizedString("CCA");
		tValue = 9;
		if(toAcctNo.length == 10){
       		 toAcctNo="00000000000000"+toAcctNo;
       	}
       	if(toAcctNo.length == 16){
       		 toAcctNo="00000000"+toAcctNo;
       	}     
	} else if (billerMethod == 3) {
		toAcctTypeValue = "LOC";//kony.i18n.getLocalizedString("Loan");
		tValue = 5;
		if(toAcctNo.length == 10){
      		 toAcctNo=toAcctNo+"000";
       	}
        if(toAcctNo.length == 13){
        	toAcctNo=toAcctNo;
       	} 
	} else {
		toAcctTypeValue = "CDA";//kony.i18n.getLocalizedString("CDA");
		tValue = 3;
	}
	if (fromAcctTypeValue == kony.i18n.getLocalizedString("SDA")) {
		fValue = 2;
	} else {
		fValue = 1;
	}
	var amount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
	if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
		inputParam["fromAcctNo"] = fromAcctID;
		inputParam["fromAcctTypeValue"] = fromAcctTypeValue;
		inputParam["toAcctNo"] = toAcctNo;//frmTopUp.lblRef1Value.text;
		inputParam["toAcctTypeValue"] = toAcctTypeValue;
		inputParam["transferAmt"] = amount.toFixed(2);
		inputParam["tranCode"] = "88" + fValue + tValue;
		inputParam["transferDate"] = getTodaysDate();
		
		invokeServiceSecureAsync("TransferAdd", inputParam, TopUpPaymentTransferAddServiceCallBack);
	}
}

function callTopUpPaymentServiceOnConfirmClick() {
	var billerMethod = frmSelectBiller.segMyBills.selectedItems[0].billerMethod;
	//	var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0].billerGroupType;
	if (gblPaynow == true) {
		if (billerMethod == 0) {
			callTopUpPaymentAddService();
		}
		if (billerMethod == 1) {
			callTopUpOnlinePaymentAddService();
		}
		if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
			callTopUpPaymentTransferService();
		}
	}
	// TODO::user clicks on the check box
	else {
		callPaymentAddTopUpServiceMB(billerMethod);
	}
	//gotoBillPaymentComplete();
}


function callPaymentAddTopUpServiceMB(billerMethod) {
	var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	GBLFINANACIALACTIVITYLOG.toAcctId = frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
	var toAcctID = frmSelectBiller["segMyBills"]["selectedItems"][0]["ToAccountKey"];
	var billerID = frmSelectBiller["segMyBills"]["selectedItems"][0]["CustomerBillID"];
	var toAcct;
	for (var i = 0; i < toAcctID.length; i++) {
		if (frmAcct[i] != "-") {
			if (toAcct == null) {
				toAcct = toAcctID[i];
			} else {
				toAcct = toAcct + toAcctID[i];
			}
		}
	}
	var toAccType;
	var dueDate = frmBillPaymentConfirmationFuture.lblStartOnValue.text.trim();
	var acctIdlen = toAcct.length;
	var fourthDigit;
	if (acctIdlen == 14) {
		fourthDigit = toAcct.charAt(7);
	} else {
		fourthDigit = toAcct.charAt(3);
	}
	if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9")
		toAccType = "SDA";
	else if (fourthDigit == "1")
		toAccType = "DDA";
	else if (fourthDigit == "3")
		toAccType = "CDA";
	else if (fourthDigit == "5" || fourthDigit == "6")
		toAccType = "LOC";
	else
		toAccType = "CCA";
	var fromAcctID;
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	var dateOrTimes = frmBillPaymentConfirmationFuture.lblHiddenVariableForScheduleEnd.text;
	if (dateOrTimes == "2") {
		inputParam["NumInsts"] = frmBillPaymentConfirmationFuture.lblExecuteValue.text.substring(0, 1);
	} else if (dateOrTimes == "3") {
		inputParam["FinalDueDt"] = changeDateFormatForService(frmBillPaymentConfirmationFuture.lblEndOnValue.text);
	}// else if (dateOrTimes == "1") {
//		//inputParam["NumInsts"] = "99";
//	}
	var tranCode;
	var f;
	var t;
	if (fromAcctType == "DDA") {
		f = "1";
	} else if (fromAcctType == "SDA") {
		f = "2";
	} else {
		f = "3";
	}
	if (toAccType == "DDA") {
		t = "1";
	} else if (toAccType == "SDA") {
		t = "2";
	} else if (toAccType == "CDA") {
		t = "3";
	} else if (toAccType == "LOC") {
		t = "5";
	} else {
		t = "9";
	}
	var pmtMethod = "BILL_PAYMENT"
    if(billerMethod == 1){
    	pmtMethod = "ONLINE_PAYMENT";
    	inputParam["PmtMiscType"] = "MOBILE" ;
    	inputParam["MiscText"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
    }
	else if(billerMethod == 2 || billerMethod == 3 || billerMethod == 4){
		pmtMethod = "INTERNAL_PAYMENT"
	}
	var amount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
	//tranCode = "88" + f + t;
	if(billerMethod == 0 || billerMethod == 1){
		tranCode = "88"+ f + "0"
	}
	else{
		tranCode = "88" + f + t;
	}
	inputParam["channelId"]="MB";
	inputParam["rqUID"] = "";
	inputParam["toBankId"] = "011";
	inputParam["crmId"] = gblcrmId;
	inputParam["amt"] = amount.toFixed(2);//getAmount();
	inputParam["fromAcct"] = fromAcctID;
	inputParam["fromAcctType"] = fromAcctType;
	inputParam["toAcct"] = toAcct;
	inputParam["toAccTType"] = toAccType;
	inputParam["dueDate"] = changeDateFormatForService(frmBillPaymentConfirmationFuture.lblStartOnValue.text);
	inputParam["pmtRefNo1"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	inputParam["custPayeeId"] = billerID;
	inputParam["transCode"] = tranCode;
	inputParam["memo"] = frmTopUp.tbxMyNoteValue.text;
	inputParam["pmtMethod"] = pmtMethod;
	var tranId = frmBillPaymentConfirmationFuture.lblTxnNumValue.text
	inputParam["extTrnRefId"] = "SU" + kony.string.sub(tranId, 2, tranId.length);
	inputParam["curCode"] = "THB";
	var frequency = frmBillPaymentConfirmationFuture.lblEveryMonth.text;
	inputParam["freq"] = frequency;
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
		inputParam["freq"] = "Daily";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
		inputParam["freq"] = "Weekly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
		inputParam["freq"] = "Monthly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
		inputParam["freq"] = "Annually";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce"))) {
        inputParam["freq"] = "once";
    }
	inputParam["desc"] = "";
	invokeServiceSecureAsync("doPmtAdd", inputParam, callPaymentAddTopUpServiceMBCallBack);
}

function callPaymentAddTopUpServiceMBCallBack(status, resulttable) {
	
	
	if (status == 400) //success responce
	{
		
		
		
		if (resulttable["opstatus"] == 0) {
			
			// callTopUpPaymentNotificationAddService();
			callTopUpPaymentCrmProfileUpdateService();
			 gblCurrentBillPayTime = resulttable["currentTime"];
		}
	}
}


/////////////////Composite service calls code starts from here//////////////////////

function svcModMBTopUpPaymentConfirm_verifyBillPaymentTopUp(loginModuleId,retryCounterVerifyAccessPin,retryCounterVerifyTransPwd,pwd){

	var inputParam = {};
	var billerMethod = gblBillerMethod;
		
	if(gblPaynow){
		inputParam["gblPaynow"] = "true";
	}else{
		inputParam["gblPaynow"] = "false";
		//Adding this param for schedule top up without adding.
		var scheduleNikcname=frmTopUp.txtNickName.text;
		inputParam["addBill_BillerNickName"] = undefined != scheduleNikcname?scheduleNikcname:"";
	}
	inputParam["channel"] = "rc";
	inputParam["billerMethod"] = billerMethod;
	inputParam["topUpCompCode"] = gblCompCode;
	
	if(flowSpa){
	 	
	 	inputParam["channel"] = "wap";
	 	inputParam["flowspa"] = true;
	 	if(gblTokenSwitchFlag == true ) {
	 		  inputParam["gblTokenSwitchFlag"] = "true";;	
			  inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
		      inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
		      inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
		      inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
		      inputParam["verifyTokenEx_password"] = popOtpSpa.txttokenspa.text;
		      //
		      inputParam["verifyTokenEx_segmentId"] = "segmentId";
		      inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
		      //inputParam["TokenSwitchFlag"] = true;
		      
		      //invokeServiceSecureAsync("verifyTokenEx", inputParam, callBackOTPVerifyspaChngecreds)
		} else {
			//kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
			//var inputParam = {};
			inputParam["gblTokenSwitchFlag"] = false;
			 inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
       		 inputParam["verifyPasswordEx_password"] = pwd;
	 	}
	 } else {
	 	inputParam["verifyPassword_loginModuleId"] = "MB_TxPwd";
		inputParam["verifyPassword_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
		inputParam["verifyPassword_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
		inputParam["verifyPassword_password"] = pwd;
	 
	 }
	 
	
	//inputParam["verifyPassword_loginModuleId"] = "MB_TxPwd";
	//inputParam["verifyPassword_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
	//inputParam["verifyPassword_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
	//inputParam["verifyPassword_password"] = pwd;

	setMBTopUpServiceInputParams(inputParam);
	setMBTopUpPaymentCrmProfileUpdateService(inputParam);
	setMBBillPaymentNotificationAddService(inputParam);
	setMBTopUpPaymentActivityLogging(inputParam);
	setFinancialActivityLogForTopupInputParamMB(frmBillPaymentConfirmationFuture.lblTxnNumValue.text,
			frmBillPaymentConfirmationFuture.lblAmountValue.text, frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text,
			"01",inputParam);
	
	invokeServiceSecureAsync("TopupBillPaymentCompositeService", inputParam,callBackVerifyPasswordTopUpBillPayment);
	
	//gotoBillPaymentCompleteMB();
}

function callBackVerifyPasswordTopUpBillPayment(status, resulttable) {
	
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			popupTractPwd.dismiss();
			//dismissLoadingScreen();
			
			
			frmBillPaymentComplete.lblBalBeforePayValue.text = commaFormatted(resulttable["availBal"])+ kony.i18n.getLocalizedString("currencyThaiBaht");
			if (!gblPaynow)
				gblCurrentBillPayTime = resulttable["currentTime"];
				
			gotoBillPaymentCompleteMB(resulttable);
			
		} else {			
			gblRtyCtrVrfyTxPin = resulttable["retryCounterVerifyTransPwd"];
			gblRtyCtrVrfyAxPin = resulttable["retryCounterVerifyAccessPin"];
			// popupTractPwd.dismiss();
			//  
			dismissLoadingScreen();
			//showAlert("" + resulttable["errMsg"]);
			// return false;					
			if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
				showTranPwdLockedPopup();
			}else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
						setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
						return false;
			}else if (resulttable["opstatus"] == 8005) {
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                gblIBFlowStatus="04";
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            	 dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            }else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
             	dismissLoadingScreen();
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            
            if(resulttable["opstatus"] == 1) {
            	if (resulttable["errCode"] == "ERRDUPTR0001") {
						if(flowSpa) {
							popOtpSpa.dismiss()
						}else{
							popupTractPwd.dismiss();
						}
            			showAlertWithHandler(kony.i18n.getLocalizedString("keyErrDuplicatemt"), kony.i18n.getLocalizedString("info"), topupExecutionErrCallBackMB);
            			return;
           		} if (resulttable["errCode"] == "MAXBILLERS") {
            		alert(kony.i18n.getLocalizedString("MIB_BPErrMaxBiller"));
            	} else {
					alert("" + resulttable["errMsg"]);
				}
				alert("" + resulttable["errMsg"]);
			} else {
				alert("" + kony.i18n.getLocalizedString("keyErrResponseOne"));
			}
            	if((resulttable["opstatus"] == 9000 || resulttable["opstatus"] == 1) && resulttable["isServiceFailed"] == "true") {
					if(flowSpa) {
						popOtpSpa.dismiss()
					}else{
						popupTractPwd.dismiss();
					}
					gblMyBillerTopUpBB = 1;
					callBillPaymentCustomerAccountService();
				}
        }
		
		}
	}else {
        if (status == 300) {
            dismissLoadingScreen();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}
function topupExecutionErrCallBackMB(){
	gblMyBillerTopUpBB = 1;
	callBillPaymentCustomerAccountService();
}
function setMBTopUpServiceInputParams(inputParam){
	
	var billerMethod = gblBillerMethod;
		
	//	var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0].billerGroupType;
	if (gblPaynow == true) {
		if (billerMethod == 0) {
			setMBTopUpPaymentAddService(inputParam);
		}
		if (billerMethod == 1) {
			setMBTopUpOnlinePaymentAddService(inputParam);
		}
		if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
			setMBTopUpPaymentTransferService(inputParam);
		}
	}
	// TODO::user clicks on the check box
	else {
		setPaymentAddTopUpServiceMB(billerMethod,inputParam);
		//callPaymentAddTopUpServiceMB(billerMethod);
	}
}

function setMBTopUpPaymentAddService(inputParam) {
	var toAccType;
	var toAcctNo = "";
	var billerMethod = "";
	toAcctNo = gblToAccountKey;
	billerMethod = gblBillerMethod;
	
	GBLFINANACIALACTIVITYLOG.toAcctId = toAcctNo;
	//var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0]["billerGroupType"];
	
	//
	var transferFee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
	var transferAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)); //+parseFloat(transferFee) 
	if (toAcctNo.length == 14) {
		toAccType = "SDA";
	} else {
		fourthDigit = toAcctNo.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			toAccType = "SDA";
			//toAcctNo = "0000" + toAcctNo;
		} else {
			toAccType = "DDA";
		}
	}
	var tranCode;
	var fromAccType = frmBillPaymentConfirmationFuture.lblTopUpProductName.text;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	var invoice;
	// if (gblreccuringDisableAdd == "Y") {
	invoice = ""; //topup
	// } else {
	// invoice = "XXX";//workaround
	//   }
	var accTypeCode;
	if (toAccType == "DDA") {
		accTypeCode = "0000";
	} else {
		accTypeCode = "0200";
	}
	var fromAcctID;
	var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
	
	if(flowSpa)
	{
	var frmFiident   = gblmbSpaselectedData.fromFIIdent;
	}
	else
	{
	var frmFiident   = frmTopUp.segSlider.selectedItems[0].fromFIIdent;
	}
	//var selectedItem = frmTopUp.segSlider.selectedIndex[1];
	//var selectedData = frmTopUp.segSlider.data[selectedItem];
	//var frmFiident = selectedData.fromFIIdent
	inputParam["billpaymentAdd_fromAcctNo"] = fromAcctID; //frmBillPaymentConfirmationFuture.lblAccountNum.text;
	inputParam["billpaymentAdd_fromAcctTypeValue"] = fromAcctType;
	inputParam["billpaymentAdd_toAcctNo"] = toAcctNo;
	inputParam["billpaymentAdd_toAcctTypeValue"] = toAccType;
	inputParam["billpaymentAdd_toFIIdent"] = fiident;
	inputParam["billpaymentAdd_frmFiident"] = frmFiident;
	inputParam["billpaymentAdd_transferAmt"] = transferAmount.toFixed(2); //need to know this value;//"amt+payment fee"
	inputParam["billpaymentAdd_tranCode"] = tranCode;
	inputParam["billpaymentAdd_channelName"] = "MB";
	inputParam["billpaymentAdd_billPmtFee"] = parseFloat(transferFee);
	inputParam["billpaymentAdd_pmtRefIdent"] = frmTopUp.lblRef1Value.text;
	inputParam["billpaymentAdd_invoiceNum"] = invoice; //frmBillPayment.lblRef2Value.text;
	inputParam["billpaymentAdd_PostedDt"] = getTodaysDate();
	inputParam["billpaymentAdd_EPAYCode"] = "";
	inputParam["billpaymentAdd_CompCode"] = gblCompCode;
	inputParam["billpaymentAdd_billPay"] = "billPay";
	
	//invokeServiceSecureAsync("billpaymentAdd", inputParam, TopUpBillPaymentAddServiceCallBack);
}


function setMBTopUpOnlinePaymentAddService(inputParam) {
	var toAccType;
	
	var toAcctNo = "";
	var billerMethod = "";
	toAcctNo = gblToAccountKey;
	billerMethod = gblBillerMethod;
	
	GBLFINANACIALACTIVITYLOG.toAcctId = toAcctNo;
//	var billerGroupType = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerGroupType"];
	
	
	//
	var transferFee = parseFloat(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text);
	var transferAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)); //+ parseFloat(transferFee.substring(0, transferFee.indexOf(" "))) ;
	
	if (toAcctNo.length == 14) {
		toAccType = "SDA";
	} else {
		fourthDigit = toAcctNo.charAt(3);
		
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			toAccType = "SDA";
			//toAcctNo = "0000" + toAcctNo;
		} else {
			toAccType = "DDA";
		}
		
	}
	var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text; //fromAcctID;  
	var fromAcctID = "";
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	var tranCode;
	if (fromAcctType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	var invoice;
	if(gblCompCode == "0835" )
	{
		invoice = "01"+TranId;
	} else if(gblCompCode == "2136" || gblCompCode == "0472" ){
		invoice = TranId;
	} else if(gblCompCode == "0185" || gblCompCode == "0254"|| gblCompCode == "0328"|| gblCompCode == "2135"|| gblCompCode == "0131" ){
		invoice = gblRef2
	}else if (gblCompCode == "2151"){
		invoice = BankRefId;
	}else {
		invoice = "";
	}
	var accTypeCode;
	if (toAccType == "DDA") {
		accTypeCode = "0000";
	} else {
		accTypeCode = "0200";
	}
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
	
	var ref1 = "";
	var ref2 = "";
	var ref3 = "";
	var ref4 = "";
	var bankrefID = "";
	//var gblCompCode = frmSelectBiller["segMyBills"]["selectedItems"][0]["BillerCompCode"].text;
	if(gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185")
	{
		bankrefID = BankRefId;
		
	} else if(gblCompCode == "0014" || gblCompCode == "0249" || gblCompCode == "2270" ) {
	
		bankrefID = TranId;
	} else {
		bankrefID = "";
	}
	if(gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185")
	{
		ref1 = gblRef1;
		ref2 = gblRef2;
		ref3 = gblRef3;
		
		if(gblCompCode == "2151")
			ref4 = ""; 
		else
			ref4 = gblRef4; 
			
	} else {
		ref1 = "";
		ref2 = "";
		ref3 = "";
		ref4 = ""; 
	} 
	var TranID = TranId; //frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
	inputParam["onlinePaymentAdd_TrnId"] = TranID;
	inputParam["onlinePaymentAdd_BankId"] = "011";
	inputParam["onlinePaymentAdd_BranchId"] = "0001";
	inputParam["onlinePaymentAdd_BankRefId"] = bankrefID;
	inputParam["onlinePaymentAdd_Amt"] = transferAmount.toFixed(2);
	inputParam["onlinePaymentAdd_Ref1"] = ref1; //frmIBBillPaymentLP.lblBPRef1Val.text;
	inputParam["onlinePaymentAdd_Ref2"] = ref2; //frmIBBillPaymentLP.lblRefVal2.text;
	inputParam["onlinePaymentAdd_Ref3"] = ref3;
	inputParam["onlinePaymentAdd_Ref4"] = ref4;
	inputParam["onlinePaymentAdd_MobileNumber"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	inputParam["onlinePaymentAdd_fromAcctNo"] = fromAcctID; // removeHyphenIB(frmIBBillPaymentConfirm.lblFromAccountNo.text); //
	inputParam["onlinePaymentAdd_fromAcctTypeValue"] = fromAcctType;
	inputParam["onlinePaymentAdd_toAcctNo"] = toAcctNo;
	inputParam["onlinePaymentAdd_toAcctType"] = toAccType;
	inputParam["onlinePaymentAdd_toFIIdent"] = fiident;
	if(flowSpa)
	{
	inputParam["onlinePaymentAdd_frmFiident"] = gblmbSpaselectedData.fromFIIdent;
	}
	else
	{
	inputParam["onlinePaymentAdd_frmFiident"] = frmTopUp.segSlider.selectedItems[0].fromFIIdent;
	}
	inputParam["onlinePaymentAdd_transferAmt"] = transferAmount;
	inputParam["onlinePaymentAdd_TranCode"] = tranCode;
	inputParam["onlinePaymentAdd_channelName"] = "IB";
	inputParam["onlinePaymentAdd_billPmtFee"] = transferFee;
	inputParam["onlinePaymentAdd_pmtRefIdent"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	inputParam["onlinePaymentAdd_invoiceNum"] = invoice;
	inputParam["onlinePaymentAdd_PostedDt"] = getTodaysDate();
	inputParam["onlinePaymentAdd_EPAYCode"] = "";//"EPYS";
	inputParam["onlinePaymentAdd_compCode"] = gblCompCode;
	inputParam["onlinePaymentAdd_InterRegionFee"] = "0.00";
	inputParam["onlinePaymentAdd_billPay"] = "billPay";
	
	//invokeServiceSecureAsync("onlinePaymentAdd", inputParam, TopUpPaymentOnlinePaymentAddServiceCallBack);
}

function setMBTopUpPaymentTransferService(inputParam) {

	var toAcctNo = "";
	var billerMethod = "";
	toAcctNo = gblToAccountKey;
	billerMethod = gblBillerMethod;
	GBLFINANACIALACTIVITYLOG.toAcctId = toAcctNo;
	//var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0].billerGroupType;
	var i = gbltranFromSelIndex[1];
	var toAcctTypeValue;
	var tValue;
	var fValue;
	/*if(flowSpa)
	{
	 var fromData = frmTopUp.segSliderSpa.data;
	}
	else
	{
    var fromData = frmTopUp.segSlider.data;
    }*/
	var frmID = frmBillPaymentConfirmationFuture.lblAccountNum.text; //fromData[i].lblActNoval;
	//var fromFIIdent=fromData[i].fromFIIdent;
	var fromAcctID;
	for (var i = 0; i < frmID.length; i++) {
		if (frmID[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmID[i];
			} else {
				fromAcctID = fromAcctID + frmID[i];
			}
		}
	}
		if (fromAcctID.length == 14) {
			fromAcctTypeValue = "SDA";
			} else {
				fourthDigit = fromAcctID.charAt(3);
			if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
				fromAcctTypeValue = "SDA";
				fromAcctID = "0000" + fromAcctID;
			} else {
			fromAcctTypeValue = "DDA";
		}
	}
	//alert(billerMethod);
	var toAcctNo = frmTopUp.lblRef1Value.text;
	toAcctNo = kony.string.replace(toAcctNo, "-", "");
	if (billerMethod == 2) {
		toAcctTypeValue = "CCA";//kony.i18n.getLocalizedString("CCA");
		tValue = 9;
		/*if(toAcctNo.length == 10){
       		 toAcctNo="00000000000000"+toAcctNo;
       	}
       	if(toAcctNo.length == 16){
       		 toAcctNo="00000000"+toAcctNo;
       	}  */   
	} else if (billerMethod == 3) {
		toAcctTypeValue = "LOC";//kony.i18n.getLocalizedString("Loan");
		tValue = 5;
		if(toAcctNo.length == 10){
      		 //toAcctNo=toAcctNo+"000";
       	}
        if(toAcctNo.length == 13){
        	toAcctNo=toAcctNo;
       	} 
	} else {
		toAcctTypeValue = "CDA";//kony.i18n.getLocalizedString("CDA");
		tValue = 3;
	}
	if (fromAcctTypeValue == kony.i18n.getLocalizedString("SDA")) {
		fValue = 2;
	} else {
		fValue = 1;
	}
	var amount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
	if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
		inputParam["TransferAdd_fromAcctNo"] = fromAcctID;
		inputParam["TransferAdd_fromAcctTypeValue"] = fromAcctTypeValue;
		inputParam["TransferAdd_toAcctNo"] = toAcctNo;//frmTopUp.lblRef1Value.text;
		inputParam["TransferAdd_toAcctTypeValue"] = toAcctTypeValue;
		inputParam["TransferAdd_transferAmt"] = amount.toFixed(2);
		inputParam["TransferAdd_tranCode"] = "88" + fValue + tValue;
		inputParam["TransferAdd_transferDate"] = getTodaysDate();
		
		//invokeServiceSecureAsync("TransferAdd", inputParam, TopUpPaymentTransferAddServiceCallBack);
	}
}

function setPaymentAddTopUpServiceMB(billerMethod,inputParam) {
	var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	
	var toAcctNo = "";
	var billerMethod = "";
	var billerID = "";
	toAcctNo = gblToAccountKey;
	billerMethod = gblBillerMethod;
	billerID = gblBillerID;
	
	GBLFINANACIALACTIVITYLOG.toAcctId = toAcctNo;
	var toAcctID = toAcctNo;
	
	var toAcct = toAcctID;
	

	
	var toAccType;
	var dueDate = frmBillPaymentConfirmationFuture.lblStartOnValue.text.trim();
	var acctIdlen = toAcct.length;
	var fourthDigit;
	if (acctIdlen == 14) {
		fourthDigit = toAcct.charAt(7);
	} else {
		fourthDigit = toAcct.charAt(3);
	}
	if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9")
		toAccType = "SDA";
	else if (fourthDigit == "1")
		toAccType = "DDA";
	else if (fourthDigit == "3")
		toAccType = "CDA";
	else if (fourthDigit == "5" || fourthDigit == "6")
		toAccType = "LOC";
	else
		toAccType = "CCA";
		
		if(billerMethod == "2")
			toAccType = "CCA";
		else if(billerMethod == "3")
			toAccType = "LOC";
		else if(billerMethod == "4")
			toAccType = "CDA";
			
	var fromAcctID;
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	var dateOrTimes = frmBillPaymentConfirmationFuture.lblHiddenVariableForScheduleEnd.text;
	if (dateOrTimes == "2") {
		inputParam["doPmtAdd_NumInsts"] = frmBillPaymentConfirmationFuture.lblExecuteValue.text.split(" ")[0].trim();
	} else if (dateOrTimes == "3") {
		inputParam["doPmtAdd_FinalDueDt"] = changeDateFormatForService(frmBillPaymentConfirmationFuture.lblEndOnValue.text);
	}// else if (dateOrTimes == "1") {
//		//inputParam["NumInsts"] = "99";
//	}
	var tranCode;
	var f;
	var t;
	if (fromAcctType == "DDA") {
		f = "1";
	} else if (fromAcctType == "SDA") {
		f = "2";
	} else {
		f = "3";
	}
	if (toAccType == "DDA") {
		t = "1";
	} else if (toAccType == "SDA") {
		t = "2";
	} else if (toAccType == "CDA") {
		t = "3";
	} else if (toAccType == "LOC") {
		t = "5";
	} else  if (toAccType == "CCA"){
		t = "9";
	}
	var pmtMethod = "BILL_PAYMENT"
    if(billerMethod == 1){
    	pmtMethod = "ONLINE_PAYMENT";
    	inputParam["doPmtAdd_PmtMiscType"] = "MOBILE" ;
    	inputParam["doPmtAdd_MiscText"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
    }
	else if(billerMethod == 2 || billerMethod == 3 || billerMethod == 4){
		pmtMethod = "INTERNAL_PAYMENT"
	}
	var amount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
	//tranCode = "88" + f + t;
	if(billerMethod == 0 || billerMethod == 1){
		tranCode = "88"+ f + "0"
	}
	else{
		tranCode = "88" + f + t;
	}
	inputParam["doPmtAdd_channelId"]="MB";
	inputParam["doPmtAdd_rqUID"] = "";
	inputParam["doPmtAdd_toBankId"] = "011";
	inputParam["doPmtAdd_crmId"] = gblcrmId;
	inputParam["doPmtAdd_amt"] = amount.toFixed(2);//getAmount();
	inputParam["doPmtAdd_fromAcct"] = fromAcctID;
	inputParam["doPmtAdd_fromAcctType"] = fromAcctType;
	inputParam["doPmtAdd_toAcct"] = toAcctID;
	inputParam["doPmtAdd_toAccTType"] = toAccType;
	inputParam["doPmtAdd_dueDate"] = changeDateFormatForService(frmBillPaymentConfirmationFuture.lblStartOnValue.text);
	inputParam["doPmtAdd_pmtRefNo1"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	inputParam["doPmtAdd_custPayeeId"] = billerID;
	inputParam["doPmtAdd_transCode"] = tranCode;
	inputParam["doPmtAdd_memo"] = frmTopUp.tbxMyNoteValue.text;
	inputParam["doPmtAdd_pmtMethod"] = pmtMethod;
	var tranId = frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
	inputParam["doPmtAdd_extTrnRefId"] = "SU" + kony.string.sub(tranId, 2, tranId.length);
	inputParam["doPmtAdd_curCode"] = "THB";
	inputParam["doPmtAdd_fromAcctNickName"] = frmBillPaymentConfirmationFuture.lblAccountName.text;
	inputParam["doPmtAdd_toNickName"] = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
	var frequency = frmBillPaymentConfirmationFuture.lblEveryMonth.text;
	inputParam["doPmtAdd_freq"] = frequency;
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
		inputParam["doPmtAdd_freq"] = "Daily";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
		inputParam["doPmtAdd_freq"] = "Weekly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
		inputParam["doPmtAdd_freq"] = "Monthly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly")) || kony.string.equalsIgnoreCase(frequency, "Yearly")) {
		inputParam["doPmtAdd_freq"] = "Annually";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce")) || kony.string.equalsIgnoreCase(frequency, "Once")) {
        inputParam["doPmtAdd_freq"] = "once";
    }
	inputParam["doPmtAdd_desc"] = "";
	
	//invokeServiceSecureAsync("doPmtAdd", inputParam, callPaymentAddTopUpServiceMBCallBack);
}


function setMBTopUpPaymentCrmProfileUpdateService(inputParam) {
	inputParam["crmProfileMod_actionType"]="0";
	if(gblPaynow){
		inputParam["crmProfileMod_ebAccuUsgAmtDaily"] = (UsageLimit + totalBillPayAmt).toFixed(2);
	}else {
		inputParam["crmProfileMod_ebAccuUsgAmtDaily"] = UsageLimit;
		
	}
	
	//invokeServiceSecureAsync("crmProfileMod", inputParam, callTopUpPaymentCrmProfileUpdateServiceCallBack);
}

function setMBBillPaymentNotificationAddService(inputParams) {
    
    platformChannel =  gblDeviceInfo.name;
    if (platformChannel == "thinclient")
        inputParams["NotificationAdd_channelId"] = "Internet Banking";
    else
        inputParams["NotificationAdd_channelId"] ="Mobile Banking";

	if(gblPaynow){
	inputParams["NotificationAdd_source"]="billpaymentnow";
	}
	else{
	inputParams["NotificationAdd_source"]="FutureBillPaymentAndTopUp";
	inputParams["NotificationAdd_startDt"] = frmBillPaymentConfirmationFuture.lblStartOnValue.text; 
	inputParams["NotificationAdd_initiationDt"] = frmBillPaymentConfirmationFuture.lblPaymentDateValue.text; 
	inputParams["NotificationAdd_todayTime"] = "23:59:59"; 
	
	//Fixed defect MIB-1396 
	var frequency = frmBillPaymentConfirmationFuture.lblEveryMonth.text;
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDaily"))) {
		inputParams["NotificationAdd_recurring"] = "keyDaily";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeekly"))) {
		inputParams["NotificationAdd_recurring"] = "keyWeekly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthly"))) {
		inputParams["NotificationAdd_recurring"] = "keyMonthly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearly"))) {
		inputParams["NotificationAdd_recurring"] = "keyYearly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce"))) {
        inputParams["NotificationAdd_recurring"] = "keyOnce";
    }
	
	//inputParams["NotificationAdd_recurring"] = frmBillPaymentConfirmationFuture.lblEveryMonth.text 
	inputParams["NotificationAdd_endDt"] = frmBillPaymentConfirmationFuture.lblEndOnValue.text 
	//inputParams["mynote"] = frmIBBillPaymentConfirm.lblMyNoteVal.text

			if(frmBillPaymentConfirmationFuture.lblEndOnValue.text == "-") {
				inputParams["NotificationAdd_PaymentSchedule"] = frmBillPaymentConfirmationFuture.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentConfirmationFuture.lblEveryMonth.text;
			}else{
				inputParams["NotificationAdd_PaymentSchedule"] = frmBillPaymentConfirmationFuture.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " +  frmBillPaymentConfirmationFuture.lblEndOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentConfirmationFuture.lblEveryMonth.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmBillPaymentConfirmationFuture.lblExecuteValue.text;
				inputParams["NotificationAdd_endDt"] = inputParams["NotificationAdd_endDt"] + " - " + frmBillPaymentConfirmationFuture.lblExecuteValue.text;
			}
	}
	
    inputParams["NotificationAdd_customerName"] = gblCustomerName;
    inputParams["NotificationAdd_fromAccount"] = frmBillPaymentConfirmationFuture.lblAccountNum.text;
    inputParams["NotificationAdd_fromAcctNick"] = frmBillPaymentConfirmationFuture.lblAccountName.text;
    inputParams["NotificationAdd_fromAcctName"] = frmBillPaymentConfirmationFuture.lblAccUserName.text;
    inputParams["NotificationAdd_billerNick"] = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
    //Fixed defect MIB-1396
    //inputParams["NotificationAdd_billerName"] = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text;
	inputParams["NotificationAdd_billerName"] = gblBillerCompCodeEN;
    inputParams["NotificationAdd_billerNameTH"] = gblBillerCompCodeTH;
    inputParams["NotificationAdd_ref1EN"] = gblRef1LblEN+" : " + frmBillPaymentConfirmationFuture.lblRef1Value.text;
    inputParams["NotificationAdd_ref1TH"] = gblRef1LblTH+" : " + frmBillPaymentConfirmationFuture.lblRef1Value.text;
    
    if(frmBillPaymentConfirmationFuture.hbxRef2.isVisible){
    	 ref2 = frmBillPaymentConfirmationFuture.lblRef2Value.text;
    	 inputParams["NotificationAdd_ref2EN"] = "<br/>"+gblRef2LblEN+" : " + ref2;
         inputParams["NotificationAdd_ref2TH"] = "<br/>"+gblRef2LblTH+" : " + ref2;
     }
     else{
         inputParams["NotificationAdd_ref2EN"] = "";
         inputParams["NotificationAdd_ref2TH"] = "";
     }
 
    //inputParams["ref2"] = frmBillPaymentConfirmationFuture.lblRef2Value.text;
    inputParams["NotificationAdd_amount"] = frmBillPaymentConfirmationFuture.lblAmountValue.text;
    inputParams["NotificationAdd_fee"] = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
    inputParams["NotificationAdd_date"] = frmBillPaymentConfirmationFuture.lblPaymentDateValue.text;
    inputParams["NotificationAdd_refID"] = frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
    inputParams["NotificationAdd_memo"] = frmTopUp.tbxMyNoteValue.text;
	
    inputParams["NotificationAdd_deliveryMethod"] = "Email";
    inputParams["NotificationAdd_noSendInd"] = "0";
   // inputParams["emailId"] = gblEmailIdBillerNotification;//
    inputParams["NotificationAdd_notificationType"] = "Email";
    inputParams["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    inputParams["NotificationAdd_notificationSubject"] = "billpay";
    inputParams["NotificationAdd_notificationContent"] = "billPaymentDone";
    //alert("calling NotificationAdd ");
    //showLoadingScreen();
    
    //invokeServiceSecureAsync("NotificationAdd", inputParams, callBillPaymentNotificationAddServiceCallBack);
}


function setMBTopUpPaymentActivityLogging(inputParam) {
	
	var activityTypeID = "";
	var errorCode = "";
	var activityStatus = "";
	var deviceNickName = "";
	var activityFlexValues5 = "";
	var activityFlexValues1 = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.substring(0, frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.lastIndexOf("("));
	var activityFlexValues2 = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	var activityFlexValues3 = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	var amt = parseFloat(frmBillPaymentConfirmationFuture.lblAmountValue.text);
	var fee = parseFloat(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text);
	var activityFlexValues4 = amt.toString() + "+" + fee.toString();
	var logLinkageId = "";
	if (gblPaynow) {
		activityTypeID = "030";
	} else {
		activityTypeID = "031";
	}
	
	activityStatus = "01";
	
	inputParam["activationActivityLog_gblPaynow"] = gblPaynow;
	
	inputParam["activationActivityLog_activityTypeID"] = activityTypeID;
	//For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
	inputParam["activationActivityLog_errorCd"] = errorCode; 
	//This status willl be "01" for success and "02" for failure of the actual event/instance
	inputParam["activationActivityLog_activityStatus"] = activityStatus;
	inputParam["activationActivityLog_deviceNickName"] = deviceNickName;
	inputParam["activationActivityLog_activityFlexValues1"] = activityFlexValues1;
	inputParam["activationActivityLog_activityFlexValues2"] = activityFlexValues2;
	inputParam["activationActivityLog_activityFlexValues3"] = activityFlexValues3;
	inputParam["activationActivityLog_activityFlexValues4"] = activityFlexValues4;
	inputParam["activationActivityLog_activityFlexValues5"] = activityFlexValues5;
	if (platformChannel == "thinclient"){
		inputParam["activationActivityLog_channelId"] = GLOBAL_IB_CHANNEL;
	}else{
		inputParam["activationActivityLog_channelId"] = GLOBAL_MB_CHANNEL;
	}
	inputParam["activationActivityLog_logLinkageId"] = logLinkageId;
	
	/*activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
		activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
	if(gblPaynow){
		setFinancialActivityLogForTopupMB(frmBillPaymentConfirmationFuture.lblTxnNumValue.text,
		frmBillPaymentConfirmationFuture.lblAmountValue.text, frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text,
		activityStatus);
	}*/
	
}

function setFinancialActivityLogForTopupInputParamMB(finTxnRefId, finTxnAmount, finTxnFee, finTxnStatus,inputParam) {
	GBLFINANACIALACTIVITYLOG.finTxnRefId = finTxnRefId;
	var activityType = "030"
	
	var tranCode;
	var fromAccType = frmBillPaymentConfirmationFuture.lblAccountName.text;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	GBLFINANACIALACTIVITYLOG.activityTypeId = activityType
	//02 is for mobile banking
	GBLFINANACIALACTIVITYLOG.channelId = "02"
	var fromAccount = frmBillPaymentConfirmationFuture.lblAccountNum.text;
	GBLFINANACIALACTIVITYLOG.fromAcctId = fromAccount.replace(/-/g, "");
	//GBLFINANACIALACTIVITYLOG.toAcctId = "";
	GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
	GBLFINANACIALACTIVITYLOG.finTxnAmount = parseFloat(removeCommos(finTxnAmount)).toFixed(2);
	GBLFINANACIALACTIVITYLOG.txnCd = tranCode;
	GBLFINANACIALACTIVITYLOG.finTxnFee = parseFloat(removeCommos(finTxnFee)).toFixed(2);
	var availableBal = removeCommos(frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text);
	availableBal = parseFloat(availableBal)
	
	if (finTxnStatus == "01")
		availableBal = availableBal - parseFloat(removeCommos(finTxnAmount)) + parseFloat(removeCommos(finTxnFee));
	GBLFINANACIALACTIVITYLOG.finTxnBalance = availableBal.toFixed(2);
	// 01 for success 02 for fail
	GBLFINANACIALACTIVITYLOG.finTxnStatus = finTxnStatus;
	GBLFINANACIALACTIVITYLOG.smartFlag = "N";
	GBLFINANACIALACTIVITYLOG.fromAcctName = frmBillPaymentConfirmationFuture.lblAccUserName.text;
	GBLFINANACIALACTIVITYLOG.toAcctName = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.split("(")[0];
	GBLFINANACIALACTIVITYLOG.toAcctNickname = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
	GBLFINANACIALACTIVITYLOG.errorCd = "0000";
	var yearPadd = new Date()
		.getFullYear()
		.toString()
		.substring(2, 2);
	var refNumber = kony.string.sub(finTxnRefId, 2, finTxnRefId.length);
	GBLFINANACIALACTIVITYLOG.eventId = refNumber;
	GBLFINANACIALACTIVITYLOG.billerBalance = "0";
	GBLFINANACIALACTIVITYLOG.txnType = "005";
	GBLFINANACIALACTIVITYLOG.billerRef1 = frmBillPaymentConfirmationFuture.lblRef1Value.text;
	GBLFINANACIALACTIVITYLOG.billerRef2 = "";
	GBLFINANACIALACTIVITYLOG.billerCustomerName = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
	GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmBillPaymentConfirmationFuture.lblAccountName.text;//"Testnick";
	GBLFINANACIALACTIVITYLOG.finTxnMemo = frmTopUp.tbxMyNoteValue.text;
	GBLFINANACIALACTIVITYLOG.tellerId = "4";
	//GBLFINANACIALACTIVITYLOG.txnDescription = "done";
	GBLFINANACIALACTIVITYLOG.finLinkageId = "1";
	
	GBLFINANACIALACTIVITYLOG.billerCommCode = gblCompCode;
	
	GBLFINANACIALACTIVITYLOG.finSchduleRefId = "SU" + kony.string.sub(finTxnRefId, 2, finTxnRefId.length);
	
	setFinancialActivityLogServiceCallTopupMB(GBLFINANACIALACTIVITYLOG,inputParam);
}

function setFinancialActivityLogServiceCallTopupMB(CRMFinancialActivityLogAddRequest, inputParam) {
	//inputParams = {};
	//inputParam["ipAddress"] = ""; 
	inputParam["financialActivityLog_crmId"] = CRMFinancialActivityLogAddRequest.crmId;
	inputParam["financialActivityLog_finTxnRefId"] = CRMFinancialActivityLogAddRequest.finTxnRefId;
	inputParam["financialActivityLog_finTxnDate"] = CRMFinancialActivityLogAddRequest.finTxnDate;
	inputParam["financialActivityLog_activityTypeId"] = CRMFinancialActivityLogAddRequest.activityTypeId;
	inputParam["financialActivityLog_txnCd"] = CRMFinancialActivityLogAddRequest.txnCd;
	inputParam["financialActivityLog_tellerId"] = CRMFinancialActivityLogAddRequest.tellerId;
	inputParam["financialActivityLog_txnDescription"] = CRMFinancialActivityLogAddRequest.txnDescription;
	inputParam["financialActivityLog_finLinkageId"] = CRMFinancialActivityLogAddRequest.finLinkageId;
	inputParam["financialActivityLog_channelId"] = CRMFinancialActivityLogAddRequest.channelId;
	inputParam["financialActivityLog_fromAcctId"] = CRMFinancialActivityLogAddRequest.fromAcctId;
	inputParam["financialActivityLog_toAcctId"] = CRMFinancialActivityLogAddRequest.toAcctId;
	inputParam["financialActivityLog_toBankAcctCd"] = CRMFinancialActivityLogAddRequest.toBankAcctCd;
	inputParam["financialActivityLog_finTxnAmount"] = CRMFinancialActivityLogAddRequest.finTxnAmount;
	inputParam["financialActivityLog_finTxnFee"] = CRMFinancialActivityLogAddRequest.finTxnFee;
	inputParam["financialActivityLog_finTxnBalance"] = CRMFinancialActivityLogAddRequest.finTxnBalance;
	inputParam["financialActivityLog_finTxnMemo"] = CRMFinancialActivityLogAddRequest.finTxnMemo;
	inputParam["financialActivityLog_billerRef1"] = CRMFinancialActivityLogAddRequest.billerRef1;
	inputParam["financialActivityLog_billerRef2"] = CRMFinancialActivityLogAddRequest.billerRef2;
	inputParam["financialActivityLog_noteToRecipient"] = CRMFinancialActivityLogAddRequest.noteToRecipient;
	inputParam["financialActivityLog_recipientMobile"] = CRMFinancialActivityLogAddRequest.recipientMobile;
	inputParam["financialActivityLog_recipientEmail"] = CRMFinancialActivityLogAddRequest.recipientEmail;
	inputParam["financialActivityLog_finTxnStatus"] = CRMFinancialActivityLogAddRequest.finTxnStatus;
	inputParam["financialActivityLog_smartFlag"] = CRMFinancialActivityLogAddRequest.smartFlag;
	inputParam["financialActivityLog_clearingStatus"] = CRMFinancialActivityLogAddRequest.clearingStatus;
	inputParam["financialActivityLog_finSchduleRefId"] = CRMFinancialActivityLogAddRequest.finSchduleRefId;
	inputParam["financialActivityLog_billerCommCode"] = CRMFinancialActivityLogAddRequest.billerCommCode;
	inputParam["financialActivityLog_fromAcctName"] = CRMFinancialActivityLogAddRequest.fromAcctName;
	inputParam["financialActivityLog_fromAcctNickname"] = CRMFinancialActivityLogAddRequest.fromAcctNickname;
	inputParam["financialActivityLog_toAcctName"] = CRMFinancialActivityLogAddRequest.toAcctName;
	inputParam["financialActivityLog_toAcctNickname"] = CRMFinancialActivityLogAddRequest.toAcctNickname;
	inputParam["financialActivityLog_billerCustomerName"] = CRMFinancialActivityLogAddRequest.billerCustomerName;
	inputParam["financialActivityLog_billerBalance"] = CRMFinancialActivityLogAddRequest.billerBalance;
	inputParam["financialActivityLog_activityRefId"] = CRMFinancialActivityLogAddRequest.activityRefId;
	inputParam["financialActivityLog_eventId"] = CRMFinancialActivityLogAddRequest.eventId;
	inputParam["financialActivityLog_errorCd"] = CRMFinancialActivityLogAddRequest.errorCd;
	inputParam["financialActivityLog_txnType"] = CRMFinancialActivityLogAddRequest.txnType;
	inputParam["financialActivityLog_tdInterestAmount"] = CRMFinancialActivityLogAddRequest.tdInterestAmount;
	inputParam["financialActivityLog_tdTaxAmount"] = CRMFinancialActivityLogAddRequest.tdTaxAmount;
	inputParam["financialActivityLog_tdPenaltyAmount"] = CRMFinancialActivityLogAddRequest.tdPenaltyAmount;
	inputParam["financialActivityLog_tdNetAmount"] = CRMFinancialActivityLogAddRequest.tdNetAmount;
	inputParam["financialActivityLog_tdMaturityDate"] = CRMFinancialActivityLogAddRequest.tdMaturityDate;
	inputParam["financialActivityLog_remainingFreeTxn"] = CRMFinancialActivityLogAddRequest.remainingFreeTxn;
	inputParam["financialActivityLog_toAccountBalance"] = CRMFinancialActivityLogAddRequest.toAccountBalance;
	inputParam["financialActivityLog_sendToSavePoint"] = CRMFinancialActivityLogAddRequest.sendToSavePoint;
	inputParam["financialActivityLog_openTdTerm"] = CRMFinancialActivityLogAddRequest.openTdTerm;
	inputParam["financialActivityLog_openTdInterestRate"] = CRMFinancialActivityLogAddRequest.openTdInterestRate;
	inputParam["financialActivityLog_openTdMaturityDate"] = CRMFinancialActivityLogAddRequest.openTdMaturityDate;
	inputParam["financialActivityLog_affiliatedAcctNickname"] = CRMFinancialActivityLogAddRequest.affiliatedAcctNickname;
	inputParam["financialActivityLog_affiliatedAcctId"] = CRMFinancialActivityLogAddRequest.affiliatedAcctId;
	inputParam["financialActivityLog_beneficialFirstname"] = CRMFinancialActivityLogAddRequest.beneficialFirstname;
	inputParam["financialActivityLog_beneficialLastname"] = CRMFinancialActivityLogAddRequest.beneficialLastname;
	inputParam["financialActivityLog_relationship"] = CRMFinancialActivityLogAddRequest.relationship;
	inputParam["financialActivityLog_percentage"] = CRMFinancialActivityLogAddRequest.percentage;
	inputParam["financialActivityLog_finFlexValues1"] = CRMFinancialActivityLogAddRequest.finFlexValues1;
	inputParam["financialActivityLog_finFlexValues2"] = CRMFinancialActivityLogAddRequest.finFlexValues2;
	inputParam["financialActivityLog_finFlexValues3"] = CRMFinancialActivityLogAddRequest.finFlexValues3;
	inputParam["financialActivityLog_finFlexValues4"] = CRMFinancialActivityLogAddRequest.finFlexValues4;
	inputParam["financialActivityLog_finFlexValues5"] = CRMFinancialActivityLogAddRequest.finFlexValues5;
	inputParam["financialActivityLog_dueDate"] = CRMFinancialActivityLogAddRequest.dueDate;
	inputParam["financialActivityLog_beepAndBillTxnId"] = CRMFinancialActivityLogAddRequest.beepAndBillTxnId;
	//invokeServiceSecureAsync("financialActivityLog", inputParam, callBackFinancialActivityLog)
	
	
	
	
}



/////////////////Composite service calls code ends  here//////////////////////
