







var totalRequestData=[];
var sortType="";
function viewRequestHistory(type){
			var formname=kony.application.getCurrentForm();
			formname.arrowrequest.setVisibility(true);
			//formname.imgleft.setVisibility(true);
			var inputParam = [];
			inputParam["crmId"] = gblcrmId;
			var d1= dateFormatChange(new Date(new Date().getFullYear(), new Date().getMonth(),  new Date().getDate()+1));
			var d2= dateFormatChange(new Date(new Date().getFullYear()-1, new Date().getMonth(),  new Date().getDate()));
			//var d = new Date();
   			//var dformat = [d.getDate(),d.getMonth()+1,d.getFullYear()].join('-')+' ' +[d.getHours(),d.getMinutes(),d.getSeconds()].join(':');
			
			inputParam["todate"]=d1
			inputParam["fromdate"]=d2;
			if(type==1){
				inputParam["sortType"]="ASC";
				sortType = "ASC";
			} else {
				inputParam["sortType"]="DESC";
				sortType = "DESC";
			}
			
			 showLoadingScreenPopup();
			 //if(kony.application.getCurrentForm().id!="frmIBCMMyProfile"){
//			 	formname.hbxRequest.setVisibility(true);
//			 }
			if(kony.application.getCurrentForm().id=="frmIBCMMyProfile"){
				invokeServiceSecureAsync("requestHistory", inputParam, ibRequestHistoryCallbackfunction);
			} else if(kony.application.getCurrentForm().id=="frmIBCMEditMyProfile"){
				invokeServiceSecureAsync("requestHistory", inputParam, ibRHEditMyProfileCallbackfunction);
			} else if (kony.application.getCurrentForm().id=="frmIBCMConfirmationPwd"){
				invokeServiceSecureAsync("requestHistory", inputParam, ibRHConfPwdCallbackfunction);
			} else if(kony.application.getCurrentForm().id=="frmIBCMConfirmation"){
				invokeServiceSecureAsync("requestHistory", inputParam, ibRHConfCallbackfunction);
			} else if(kony.application.getCurrentForm().id=="frmIBCMChngUserID"){
				invokeServiceSecureAsync("requestHistory", inputParam, ibRHChngUseIdCallbackfunction);
			} else if(kony.application.getCurrentForm().id=="frmIBCMChgMobNoTxnLimit"){
				invokeServiceSecureAsync("requestHistory", inputParam, ibRHChngMobNoTxnCallbackfunction);
			} else if(kony.application.getCurrentForm().id=="frmIBCMChgPwd"){
				invokeServiceSecureAsync("requestHistory", inputParam, ibRHChngPwdCallbackfunction);
			}
			
			
			
	}
	function ibRHChngPwdCallbackfunction(status,callBackResponse){
			
			if(status==400){
				if(callBackResponse["opstatus"]== 0 ) {
						dismissLoadingScreenPopup();
			 			if (callBackResponse["requestHistory"].length == 0) {
			 				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 				frmIBCMChgPwd.hbxRequest.setVisibility(false);
			 				return false;
			 			}
			 			frmIBCMChgPwd.hbxRequest.setVisibility(true);
			 			//frmIBCMChgPwd.hbxReqLink.skin ="skin Defaults";
			 			frmIBCMChgPwd.link508395714289050.skin="lnkMyReqHisFoc"
			 			frmIBCMChgPwd.lnkHeader.setVisibility(true);
			 			frmIBCMChgPwd.lblConfirmation.setVisibility(false);
			 			frmIBCMChgPwd.hbox47428873359193.setVisibility(false);
			 			frmIBCMChgPwd.hbxCancelSaveOld.setVisibility(false);
			 			frmIBCMChgPwd.segRequestHistory.removeAll();
			 			frmIBCMChgPwd.ArrowEditProf.setVisibility(false);
			 			if(sortType=="DESC"){
			 				frmIBCMChgPwd.cmbSort.selectedKey=2;
			 			}
			 			var tempData =[];
			 			for (var i=0;i<callBackResponse["requestHistory"].length;i++){
			 					var reqHistoryData={
			 								lblServiceRequestId:kony.i18n.getLocalizedString("keyReqNumber")+": "+callBackResponse["requestHistory"][i]["SR_LOG_ID"],
			 								lblRequestDate :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
			 								lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
			 								lblRequestDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
			 								lblRequestStatus:kony.i18n.getLocalizedString("keyStatus")+": "+callBackResponse["requestHistory"][i]["STATUS_DESC"] 
			 								
			 				};
			 				tempData.push(reqHistoryData);
			 			}
			 			totalRequestData = tempData;
			 			frmIBCMChgPwd.segRequestHistory.addAll(tempData);
			 			if ( tempData.length == 1 ){
			 				frmIBCMChgPwd.cmbSort.setEnabled(false);
			 			} 
			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
				}else {
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 		return false;
				}
			}
	}
	function ibRHChngMobNoTxnCallbackfunction(status,callBackResponse){
			
			if(status==400){
				if(callBackResponse["opstatus"]== 0 ) {
						dismissLoadingScreenPopup();
			 			if (callBackResponse["requestHistory"].length == 0) {
			 				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 				frmIBCMChgMobNoTxnLimit.hbxRequest.setVisibility(false);
			 				return false;
			 			}
			 			frmIBCMChgMobNoTxnLimit.hbxRequest.setVisibility(true);
			 			//frmIBCMChgMobNoTxnLimit.hbxReqLink.skin ="skin Defaults";
			 			frmIBCMChgMobNoTxnLimit.link508395714289050.skin="lnkMyReqHisFoc"
			 			frmIBCMChgMobNoTxnLimit.lnkHeader.setVisibility(true);
			 			frmIBCMChgMobNoTxnLimit.lblHead.setVisibility(false);
			 			frmIBCMChgMobNoTxnLimit.hbxChangeMobileNumber.setVisibility(false);
			 			frmIBCMChgMobNoTxnLimit.hbxChangeTransactionLimit.setVisibility(false);
			 			frmIBCMChgMobNoTxnLimit.hbxOtpBox.setVisibility(false);
			 			frmIBCMChgMobNoTxnLimit.hbox47505957819121.setVisibility(false);
			 			frmIBCMChgMobNoTxnLimit.segRequestHistory.removeAll();
			 			frmIBCMChgMobNoTxnLimit.ArrowMobNu.setVisibility(false);
			 			frmIBCMChgMobNoTxnLimit.arrowlimit.setVisibility(false);
			 			if(sortType=="DESC"){
			 				frmIBCMChgMobNoTxnLimit.cmbSort.selectedKey=2;
			 			}
			 			
			 			var tempData =[];
			 			for (var i=0;i<callBackResponse["requestHistory"].length;i++){
			 				var reqHistoryData={
			 								lblServiceRequestId:kony.i18n.getLocalizedString("keyReqNumber")+": "+callBackResponse["requestHistory"][i]["SR_LOG_ID"],
			 								lblRequestDate :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
			 								lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
			 								lblRequestDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
			 								lblRequestStatus:kony.i18n.getLocalizedString("keyStatus")+": "+callBackResponse["requestHistory"][i]["STATUS_DESC"] 
			 								
			 				};
			 				tempData.push(reqHistoryData);
			 			}
			 			totalRequestData = tempData;
			 			frmIBCMChgMobNoTxnLimit.segRequestHistory.addAll(tempData);
			 			if ( tempData.length == 1 ){
			 				frmIBCMChgMobNoTxnLimit.cmbSort.setEnabled(false);
			 			} 
			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
				}else {
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 		return false;
				}
			}
	}
	function ibRHChngUseIdCallbackfunction(status,callBackResponse){
			
			if(status==400){
				if(callBackResponse["opstatus"]== 0 ) {
						dismissLoadingScreenPopup();
			 			if (callBackResponse["requestHistory"].length == 0) {
			 				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 				frmIBCMChngUserID.hbxRequest.setVisibility(false);
			 				return false;
			 			}
			 			frmIBCMChngUserID.hbxRequest.setVisibility(true);
			 			//frmIBCMChngUserID.hbxReqLink.skin ="skin Defaults";
			 			frmIBCMChngUserID.link508395714289050.skin="lnkMyReqHisFoc"
			 			frmIBCMChngUserID.lnkHeader.setVisibility(true);
			 			frmIBCMChngUserID.lblChngUID.setVisibility(false);
			 			frmIBCMChngUserID.hbox47428873349614.setVisibility(false);
			 			frmIBCMChngUserID.hbxCancelSave.setVisibility(false);
			 			frmIBCMChngUserID.hboxCaptcha.setVisibility(false);
			 			frmIBCMChngUserID.hboxCaptchaText.setVisibility(false);
			 			frmIBCMChngUserID.segRequestHistory.removeAll();
			 			frmIBCMChngUserID.arrowUserId.setVisibility(false);
			 			if(sortType=="DESC"){
			 				frmIBCMChngUserID.cmbSort.selectedKey=2;
			 			}
			 			//frmIBCMChngUserID.cmbSort.selectedKey=2;
			 			var tempData =[];
			 			for (var i=0;i<callBackResponse["requestHistory"].length;i++){
			 				var reqHistoryData={
			 								lblServiceRequestId:kony.i18n.getLocalizedString("keyReqNumber")+": "+callBackResponse["requestHistory"][i]["SR_LOG_ID"],
			 								lblRequestDate :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
			 								lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
			 								lblRequestDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
			 								lblRequestStatus:kony.i18n.getLocalizedString("keyStatus")+": "+callBackResponse["requestHistory"][i]["STATUS_DESC"] 
			 								
			 				};
			 				tempData.push(reqHistoryData);
			 			}
			 			totalRequestData = tempData;
			 			frmIBCMChngUserID.segRequestHistory.addAll(tempData);
			 			if ( tempData.length == 1 ){
			 				frmIBCMChngUserID.cmbSort.setEnabled(false);
			 			} 
			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
				}else {
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 		return false;
				}
			}
	}
	function ibRHConfCallbackfunction(status,callBackResponse){
			
			if(status==400){
				if(callBackResponse["opstatus"]== 0 ) {
						dismissLoadingScreenPopup();
			 			if (callBackResponse["requestHistory"].length == 0) {
			 				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 				frmIBCMConfirmation.hbxRequest.setVisibility(false);
			 				return false;
			 			}
			 			frmIBCMConfirmation.hbxRequest.setVisibility(true);
			 			//frmIBCMConfirmation.hbxReqLink.skin ="skin Defaults";
			 			frmIBCMConfirmation.link508395714289050.skin="lnkMyReqHisFoc"
			 			frmIBCMConfirmation.lnkHeader.setVisibility(true);
			 			frmIBCMConfirmation.lblConfirmation.setVisibility(false);
			 			frmIBCMConfirmation.hbox47428873334338.setVisibility(false);
			 			frmIBCMConfirmation.hbox476047582115271.setVisibility(false);
			 			frmIBCMConfirmation.hbxCancelSaveOld.setVisibility(false);
			 			//frmIBCMConfirmation.lblSbHeader.setVisibility(false);
			 			frmIBCMConfirmation.segRequestHistory.removeAll();
			 			frmIBCMConfirmation.arrowUserId.setVisibility(false);
			 			if(sortType=="DESC"){
			 				frmIBCMConfirmation.cmbSort.selectedKey=2;
			 			}
			 			//
			 			var tempData =[];
			 			for (var i=0;i<callBackResponse["requestHistory"].length;i++){
			 				var reqHistoryData={
			 								lblServiceRequestId:kony.i18n.getLocalizedString("keyReqNumber")+": "+callBackResponse["requestHistory"][i]["SR_LOG_ID"],
			 								lblRequestDate :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
			 								lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
			 								lblRequestDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
			 								lblRequestStatus:kony.i18n.getLocalizedString("keyStatus")+": "+callBackResponse["requestHistory"][i]["STATUS_DESC"] 
			 								
			 				};
			 				tempData.push(reqHistoryData);
			 			}
			 			totalRequestData = tempData;
			 			frmIBCMConfirmation.segRequestHistory.addAll(tempData);
			 			if ( tempData.length == 1 ){
			 				frmIBCMConfirmation.cmbSort.setEnabled(false);
			 			} 
			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
				}else {
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 		return false;
				}
			}
	}
	function ibRHConfPwdCallbackfunction(status,callBackResponse){
			
			if(status==400){
				if(callBackResponse["opstatus"]== 0 ) {
						dismissLoadingScreenPopup();
			 			if (callBackResponse["requestHistory"].length == 0) {
			 				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 				frmIBCMConfirmationPwd.hbxRequest.setVisibility(false);
			 				return false;
			 			}
			 			frmIBCMConfirmationPwd.hbxRequest.setVisibility(true);
			 			//frmIBCMConfirmationPwd.hbxReqLink.skin ="skin Defaults";
			 			frmIBCMConfirmationPwd.link508395714289050.skin="lnkMyReqHisFoc"
			 			frmIBCMConfirmationPwd.lnkHeader.setVisibility(true);
			 			frmIBCMConfirmationPwd.lblConfirmation.setVisibility(false);
			 			frmIBCMConfirmationPwd.hbox476047582115271.setVisibility(false);
			 			frmIBCMConfirmationPwd.hbxCancelSave.setVisibility(false);
			 			frmIBCMConfirmationPwd.hbxCancelSaveOld.setVisibility(false);
			 			frmIBCMConfirmationPwd.lblSbHeader.setVisibility(false);
			 			frmIBCMConfirmationPwd.segRequestHistory.removeAll();
			 			frmIBCMConfirmationPwd.ArrowEditProf.setVisibility(false);
			 			if(sortType=="DESC"){
			 				frmIBCMConfirmationPwd.cmbSort.selectedKey=2;
			 			}
			 			//frmIBCMConfirmationPwd.cmbSort.selectedKey=2;
			 			var tempData =[];
			 			for (var i=0;i<callBackResponse["requestHistory"].length;i++){
			 				var reqHistoryData={
			 								lblServiceRequestId:kony.i18n.getLocalizedString("keyReqNumber")+": "+callBackResponse["requestHistory"][i]["SR_LOG_ID"],
			 								lblRequestDate :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
			 								lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
			 								lblRequestDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
			 								lblRequestStatus:kony.i18n.getLocalizedString("keyStatus")+": "+callBackResponse["requestHistory"][i]["STATUS_DESC"] 
			 								
			 				};
			 				tempData.push(reqHistoryData);
			 			}
			 			totalRequestData = tempData;
			 			frmIBCMConfirmationPwd.segRequestHistory.addAll(tempData);
			 			if ( tempData.length == 1 ){
			 				frmIBCMConfirmationPwd.cmbSort.setEnabled(false);
			 			} 
			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
				}else {
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 		return false;
				}
			}
	}
	
	function ibRHEditMyProfileCallbackfunction(status,callBackResponse){
			
			if(status==400){
				if(callBackResponse["opstatus"]== 0 ) {
						dismissLoadingScreenPopup();
			 			if (callBackResponse["requestHistory"].length == 0) {
			 				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 				frmIBCMEditMyProfile.hbxRequest.setVisibility(false);
			 				return false;
			 			}
			 			frmIBCMEditMyProfile.hbxRequest.setVisibility(true);
			 			//frmIBCMEditMyProfile.hbxReqLink.skin ="skin Defaults";
			 			frmIBCMEditMyProfile.link508395714289050.skin="lnkMyReqHisFoc"
			 			frmIBCMEditMyProfile.lnkHeader.setVisibility(true);
			 			frmIBCMEditMyProfile.label475124774164.setVisibility(false);
			 			frmIBCMEditMyProfile.hboxEdit.setVisibility(false);
			 			frmIBCMEditMyProfile.hbxCancelSave.setVisibility(false);
			 			frmIBCMEditMyProfile.hbxcnf1.setVisibility(false);
			 			frmIBCMEditMyProfile.hbxcnf2.setVisibility(false);
			 			frmIBCMEditMyProfile.hbxOtpBox.setVisibility(false);
			 			frmIBCMEditMyProfile.segRequestHistory.removeAll();
			 			frmIBCMEditMyProfile.ArrowEditProf.setVisibility(false);
			 			if(sortType=="DESC"){
			 				frmIBCMEditMyProfile.cmbSort.selectedKey=2;
			 			}
			 			//frmIBCMEditMyProfile.cmbSort.selectedKey=2;
			 			var tempData =[];
			 			for (var i=0;i<callBackResponse["requestHistory"].length;i++){
			 				var reqHistoryData={
			 								lblServiceRequestId:kony.i18n.getLocalizedString("keyReqNumber")+": "+callBackResponse["requestHistory"][i]["SR_LOG_ID"],
			 								lblRequestDate :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
			 								lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
			 								lblRequestDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
			 								lblRequestStatus:kony.i18n.getLocalizedString("keyStatus")+": "+callBackResponse["requestHistory"][i]["STATUS_DESC"] 
			 								
			 				};
			 				tempData.push(reqHistoryData);
			 			}
			 			totalRequestData = tempData;
			 			frmIBCMEditMyProfile.segRequestHistory.addAll(tempData);
			 			if ( tempData.length == 1 ){
			 				frmIBCMEditMyProfile.cmbSort.setEnabled(false);
			 			} 
			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
				}else {
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 		return false;
				}
			}
	}
	function ibRequestHistoryCallbackfunction(status,callBackResponse){
			//
			if(status==400){
				if(callBackResponse["opstatus"]== 0 ) {
						dismissLoadingScreenPopup();
			 			if (callBackResponse["requestHistory"].length == 0) {
			 				//alert(" " + keyalertIBNoRecord);
			 				//frmIBAccntFullStatement.dgdeposit.setVisibility(false);
			 				//disablePagenumbers();
			 				alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 				frmIBCMMyProfile.hbximage.setVisibility(true);
			 				return false;
			 			}
			 				
			 			var formname=kony.application.getCurrentForm();
			 				
			 			frmIBCMMyProfile.lblRequestHeader.setVisibility(true);
			 			frmIBCMMyProfile.hbxListBox.setVisibility(true);
			 			frmIBCMMyProfile.hbxData.setVisibility(true);
			 			//frmIBCMMyProfile.hbxReqLink.skin ="skin Defaults";
			 			frmIBCMMyProfile.link508395714289050.skin="lnkMyReqHisFoc"
			 			frmIBCMMyProfile.hbximage.setVisibility(false);
			 			frmIBCMMyProfile.lnkHeader.setVisibility(true);
			 			frmIBCMMyProfile.segRequestHistory.removeAll();
			 			if(sortType=="DESC"){
			 				frmIBCMMyProfile.cmbSort.selectedKey=2;
			 			}
			 			//frmIBCMMyProfile.cmbSort.selectedKey=2;
			 			var tempData =[];
			 			for (var i=0;i<callBackResponse["requestHistory"].length;i++){
			 				var reqHistoryData={
			 								lblServiceRequestId:kony.i18n.getLocalizedString("keyReqNumber")+": "+callBackResponse["requestHistory"][i]["SR_LOG_ID"],
			 								lblRequestDate :callBackResponse["requestHistory"][i]["SERVICE_REQUEST_DATE"],
			 								lblTime:"["+callBackResponse["requestHistory"][i]["SERVICE_REQUEST_TIME"]+"]",
			 								lblRequestDesc :callBackResponse["requestHistory"][i]["ACTIVITY_FLEX_VALUES"],
			 								lblRequestStatus:kony.i18n.getLocalizedString("keyStatus")+": "+callBackResponse["requestHistory"][i]["STATUS_DESC"] 
			 								
			 				};
			 				tempData.push(reqHistoryData);
			 			}
			 			totalRequestData = tempData;
			 			frmIBCMMyProfile.segRequestHistory.addAll(tempData);
			 			if ( tempData.length == 1 ){
			 				frmIBCMMyProfile.cmbSort.setEnabled(false);
			 			} 
			 			//var sortedTotalData = loadSortedData(frmAccountStatementMB.segcredit.data);
						//frmIBCMMyProfile.segRequestHistory.setData(sortedTotalData);
				}else {
					dismissLoadingScreenPopup();
					alert(kony.i18n.getLocalizedString("NoRecsFound"));
			 		return false;
				}
			}
	}
	
	
 
