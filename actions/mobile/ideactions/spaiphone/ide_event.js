function p2kwiet2012247626342_frmAccountDetailsMB_preshow_seq0(eventobject) {
    frmAccountDetailsMBMenuPreshow.call(this);
};

function p2kwiet2012247626342_frmAccountDetailsMB_postshow_seq0(eventobject) {
    frmAccountDetailsMBMenuPostshow.call(this);
};

function p2kwiet2012247626342_frmAccountDetailsMB_onhide_seq0(eventobject) {
    gblCallPrePost = true;
};

function p2kwiet2012247626342_button968116430409562_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626342_btnMyActivities_onClick_seq0(eventobject) {
    onLinkMyActivitiesClickMB.call(this);
};

function p2kwiet2012247626342_btnFullStatement_onClick_seq0(eventobject) {
    viewFullStatement.call(this);
};

function p2kwiet2012247626342_btnDreamSavings_onClick_seq0(eventobject) {
    callDepositAccntsummmry.call(this);
};

function p2kwiet2012247626342_btnlinkDebitCard_onClick_seq0(eventobject) {
    preShowMBDebitCardListfromAccntSummary.call(this);
};

function p2kwiet2012247626342_btnEditBeneficiary_onClick_seq0(eventobject) {
    flow = "myAccount";
    accDetail = "true";
    /* 
loadSavingsCareTermsNConditions.call(this);

 */
    onMyAccountDetailsEdit.call(this);
};

function p2kwiet2012247626342_btnApplySoGood_onClick_seq0(eventobject) {
    onClickApplySoGooODLink.call(this);
};

function p2kwiet2012247626342_btnPointRedeem_onClick_seq0(eventobject) {
    checkMBCustStatus.call(this);
};

function p2kwiet2012247626342_btnManageCard_onClick_seq0(eventobject) {
    callCreditReadyCardInqService.call(this);
};

function p2kwiet2012247626342_btnBack1_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247626480_frmAccountDetailsMBold_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626480_frmAccountDetailsMBold_preshow_seq0(eventobject, neworientation) {
    frmAccountDetailsMBMenuPreshow.call(this);
};

function p2kwiet2012247626480_frmAccountDetailsMBold_postshow_seq0(eventobject, neworientation) {
    frmAccountDetailsMBMenuPostshow.call(this);
};

function p2kwiet2012247626480_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626480_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626480_btnOptions_onClick_seq0(eventobject) {
    toDo.call(this);
};

function p2kwiet2012247626480_btnTransfer_onClick_seq0(eventobject) {
    onclickgetTransferFromAccountsFromAccDetails.call(this);
};

function p2kwiet2012247626480_btnPayBill_onClick_seq0(eventobject) {
    onclickgetBillPaymentFromAccountsFromAccDetails.call(this);
};

function p2kwiet2012247626480_BtnTopUp_onClick_seq0(eventobject) {
    onclickgetTopUpFromAccountsFromAccDetails.call(this);
};

function p2kwiet2012247626480_payMyCredit_onClick_seq0(eventobject) {
    onclickCreditfromDetails.call(this);
};

function p2kwiet2012247626480_payMyLoan_onClick_seq0(eventobject) {
    onclickLoanfromDetails.call(this);
};

function p2kwiet2012247626480_btnTrnsferCenter_onClick_seq0(eventobject) {
    onclickgetTransferFromAccountsFromAccDetails.call(this);
};

function p2kwiet2012247626480_lnkMyActivities_onClick_seq0(eventobject) {
    onLinkMyActivitiesClickMB.call(this);
};

function p2kwiet2012247626480_lnkFullStatement_onClick_seq0(eventobject) {
    viewFullStatement.call(this);
};

function p2kwiet2012247626480_linkActivateCard_onClick_seq0(eventobject, context) {
    debitcardAcivationConfirm.call(this);
};

function p2kwiet2012247626480_linkMore_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickOfAccountDetailsMore.call(this);
    } else {
        frmAccountDetailsMB.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247626480_link449290336691040_onClick_seq0(eventobject) {
    getApplyS2SStatus()
};

function p2kwiet2012247626480_lnkApplySoGooOD_onClick_seq0(eventobject) {
    onClickApplySoGooODLink.call(this);
};

function p2kwiet2012247626480_lnkRedeemPoint_onClick_seq0(eventobject) {
    checkMBCustStatus.call(this);
};

function p2kwiet2012247626480_linkDreamSaving_onClick_seq0(eventobject) {
    callDepositAccntsummmry.call(this);
};

function p2kwiet2012247626480_linkEditBenefi_onClick_seq0(eventobject) {
    flow = "myAccount";
    accDetail = "true";
    /* 
loadSavingsCareTermsNConditions.call(this);

 */
    onMyAccountDetailsEdit.call(this);
};

function p2kwiet2012247626480_btnBackSpa_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickOfAccountDetailsBack.call(this);
    } else {
        //kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        showLoadingScreen();
        frmAccountDetailsMB.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        kony.application.dismissLoadingScreen();
    }
};

function p2kwiet2012247626480_btnBack1_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickOfAccountDetailsBack.call(this);
    } else {
        //kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        showLoadingScreen();
        frmAccountDetailsMB.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        kony.application.dismissLoadingScreen();
    }
};

function p2kwiet2012247626596_frmAccountStatementMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626596_frmAccountStatementMB_preshow_seq0(eventobject, neworientation) {
    frmAccountStatementMBMenuPreshow.call(this);
};

function p2kwiet2012247626596_frmAccountStatementMB_postshow_seq0(eventobject, neworientation) {
    frmAccountStatementMBMenuPostshow.call(this);
};

function p2kwiet2012247626596_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626596_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626596_btnOptions_onClick_seq0(eventobject) {
    var skin = frmAccountStatementMB.btnOptions.skin
    if (frmAccountStatementMB.hbxOnHandClick.isVisible == false) {
        frmAccountStatementMB.hbxOnHandClick.isVisible = true;
        frmAccountStatementMB.imgHeaderRight.src = "arrowtop.png"
        frmAccountStatementMB.imgHeaderMiddle.src = "empty.png"
        frmAccountStatementMB.btnOptions.skin = "btnShareFoc";
    } else {
        frmAccountStatementMB.hbxOnHandClick.isVisible = false;
        frmAccountStatementMB.imgHeaderMiddle.src = "arrowtop.png"
        frmAccountStatementMB.imgHeaderRight.src = "empty.png"
        frmAccountStatementMB.btnOptions.skin = "btnShare";
    }
};

function p2kwiet2012247626596_btnpdf_onClick_seq0(eventobject) {
    savePDFAccountHistoryMB.call(this, "pdf");
};

function p2kwiet2012247626596_linkactivity_onClick_seq0(eventobject) {
    onLinkMyActivitiesClickMB.call(this);
};

function p2kwiet2012247626596_btnunbilled_onClick_seq0(eventobject) {
    frmAccountStatementMB.segcredit.removeAll();
    frmAccountStatementMB.btnunbilled.skin = "btnunbilledfoc";
    frmAccountStatementMB.btnunbilled.focusSkin = "btnunbilledfoc";
    frmAccountStatementMB.btnbilled.skin = "btnunbilled";
    frmAccountStatementMB.hbxunbilled.setVisibility(true);
    frmAccountStatementMB.hbxbilled.setVisibility(false);
    setSortBtnSkinMB();
    startCCstatementservice("unbilled1", "0");
};

function p2kwiet2012247626596_btnbilled_onClick_seq0(eventobject) {
    frmAccountStatementMB.segcredit.removeAll();
    frmAccountStatementMB.btnunbilled.skin = "btnunbilled";
    frmAccountStatementMB.btnbilled.skin = "btnunbilledfoc";
    frmAccountStatementMB.hbxunbilled.setVisibility(false);
    frmAccountStatementMB.hbxbilled.setVisibility(true);
    onCCMonth1Click();
    //startCCstatementservice("billed", "1");
};

function p2kwiet2012247626596_btn1_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btn1.skin = "btnTabLeftBlue";
frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();
startCCstatementservice("billed", "1");


 */
    onCCMonth1Click.call(this);
};

function p2kwiet2012247626596_btn2_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btn2.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();
//startCCstatementservice("billed", "2");


 */
    onCCMonth2Click.call(this);
};

function p2kwiet2012247626596_btn3_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn3.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();
startCCstatementservice("billed", "3");


 */
    onCCMonth3Click.call(this);
};

function p2kwiet2012247626596_btn4_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn4.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();
startCCstatementservice("billed", "4");


 */
    onCCMonth4Click.call(this);
};

function p2kwiet2012247626596_btn5_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn5.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btn6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();
startCCstatementservice("billed", "5");



 */
    onCCMonth5Click.call(this);
};

function p2kwiet2012247626596_btn6_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btn1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btn2.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btn6.skin = "btnTabRightBlue";
frmAccountStatementMB.segcredit.removeAll();
startCCstatementservice("billed", "6");




 */
    onCCMonth6Click.call(this);
};

function p2kwiet2012247626596_btnm1_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btnm1.skin = "btnTabLeftBlue";
frmAccountStatementMB.btnm2.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();


 */
    onMonth1Click.call(this);
    /* 
selectedMonthData.call(this);

 */
    getDateFormatStmt.call(this, frmAccountStatementMB.btnm1.text, "0");
};

function p2kwiet2012247626596_btnm2_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btnm2.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();


 */
    onMonth2Click.call(this);
    /* 
selectedMonthData.call(this);

 */
    getDateFormatStmt.call(this, frmAccountStatementMB.btnm2.text, "1");
};

function p2kwiet2012247626596_btnm3_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btnm2.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();


 */
    onMonth3Click.call(this);
    /* 
selectedMonthData.call(this);

 */
    getDateFormatStmt.call(this, frmAccountStatementMB.btnm3.text, "2");
};

function p2kwiet2012247626596_btnm4_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btnm2.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();


 */
    onMonth4Click.call(this);
    /* 
selectedMonthData.call(this);

 */
    getDateFormatStmt.call(this, frmAccountStatementMB.btnm4.text, "3");
};

function p2kwiet2012247626596_btnm5_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btnm2.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();


 */
    onMonth5Click.call(this);
    /* 
selectedMonthData.call(this);

 */
    getDateFormatStmt.call(this, frmAccountStatementMB.btnm5.text, "4");
};

function p2kwiet2012247626596_btnm6_onClick_seq0(eventobject) {
    /* 
frmAccountStatementMB.btnm1.skin = "btnTab3LeftNrml";
frmAccountStatementMB.btnm2.skin = "btnTabMiddleBlue";
frmAccountStatementMB.btnm3.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm4.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm5.skin = "btnTab3MidNrml";
frmAccountStatementMB.btnm6.skin = "btnTab3RightNrml";
frmAccountStatementMB.segcredit.removeAll();


 */
    onMonth6Click.call(this);
    /* 
selectedMonthData.call(this);

 */
    getDateFormatStmt.call(this, frmAccountStatementMB.btnm6.text, "5");
};

function p2kwiet2012247626596_segcredit_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    getdescriptionMB.call(this);
};

function p2kwiet2012247626596_linkMore_onClick_seq0(eventobject) {
    loadMoreData.call(this);
    if (isMenuShown == false) {
        /* 
onClickOfAccountDetailsMore.call(this);

 */
    } else {
        /* 
frmAccountDetailsMB.scrollboxMain.scrollToEnd();
isMenuShown=false;

 */
    }
};

function p2kwiet2012247626596_btnBackSpa_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        /* 
onClickOfAccountDetailsBack.call(this);

 */
    } else {
        /* 
//kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
showLoadingScreen();
frmAccountDetailsMB.scrollboxMain.scrollToEnd();
isMenuShown=false;
kony.application.dismissLoadingScreen();

 */
    }
    frmAccountStatementMB.segcredit.removeAll();
    gblStmntSessionFlag = "2";
    //TMBUtil.DestroyForm(frmAccountStatementMB);
    shortCutActDetailsPage();
    frmAccountDetailsMB.show();
    //frmAccountStatementMBDummy.show();
};

function p2kwiet2012247626596_btnBack_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        /* 
onClickOfAccountDetailsBack.call(this);

 */
    } else {
        /* 
//kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
showLoadingScreen();
frmAccountDetailsMB.scrollboxMain.scrollToEnd();
isMenuShown=false;
kony.application.dismissLoadingScreen();

 */
    }
    frmAccountStatementMB.segcredit.removeAll();
    gblStmntSessionFlag = "2";
    //TMBUtil.DestroyForm(frmAccountStatementMB);
    shortCutActDetailsPage();
    if (gblStatementFlow == "frmMBManageCard") {
        frmMBManageCard.show();
    } else if (gblStatementFlow == "frmAccountDetailsMB") {
        frmAccountDetailsMB.show();
    } else {
        frmAccountDetailsMB.show();
    }
    //frmAccountStatementMBDummy.show();
};

function p2kwiet2012247626636_frmAccountSummaryLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    try {
        if (isMenuShown == false) {
            if (!gblTimerFlg) {
                showToastMsg();
                kony.timer.schedule("btnTimer", callback, 4, false);
                popGoback.show();
                gblTimerFlg = true;
            }
        }

        function callback() {
            popGoback.destroy();
            popGoback.dismiss();
            kony.timer.cancel("btnTimer");
            gblTimerFlg = false;
        }
    } catch (Error) {
        alert("back btn");
    }
};

function p2kwiet2012247626636_frmAccountSummaryLanding_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626636_frmAccountSummaryLanding_preshow_seq0(eventobject, neworientation) {
    frmAccountSummaryLandingMenuPreshow.call(this);
};

function p2kwiet2012247626636_frmAccountSummaryLanding_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    /* 
deviceInfo = kony.os.deviceInfo();
if (deviceInfo["name"] == "thinclient" & deviceInfo["type"]=="spa")
{
 isMenuRendered = false;
 isMenuShown = false;
 frmAccountSummaryLanding.scrollboxMain.scrollToEnd();
}



 */
    postShowOfAccountSummary.call(this);
};

function p2kwiet2012247626636_hboxRight_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626636_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626636_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626636_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626636_hbox475124774240_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626636_hboxTotal_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626636_hbxBarGraph_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626636_hboxPercentage_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626677_frmAccountSummaryLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626677_frmAccountSummaryLanding_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626677_frmAccountSummaryLanding_preshow_seq0(eventobject, neworientation) {
    registerForTimeOut.call(this);
    frmAccountSummaryLanding.scrollboxMain.scrollToEnd();
    gblIndex = -1;
    isMenuShown = false;
    frmAccountSummaryLandingPreShow.call(this);
    /* 
          

    */
};

function p2kwiet2012247626677_frmAccountSummaryLanding_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    deviceInfo = kony.os.deviceInfo();
    if (deviceInfo["name"] == "thinclient" & deviceInfo["type"] == "spa") {
        isMenuRendered = false;
        isMenuShown = false;
        frmAccountSummaryLanding.scrollboxMain.scrollToEnd();
    }
};

function p2kwiet2012247626677_hboxRight_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626677_vbox1040416525400053_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626677_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626677_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626677_hbox475124774240_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626677_hboxTotal_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626677_hbxBarGraph_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626677_hboxPercentage_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626702_frmAccTrcPwdInter_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626702_frmAccTrcPwdInter_preshow_seq0(eventobject, neworientation) {
    frmAccTrcPwdInterMenuPreshow.call(this);
};

function p2kwiet2012247626702_frmAccTrcPwdInter_postshow_seq0(eventobject, neworientation) {
    frmAccTrcPwdInterMenuPostshow.call(this);
};

function p2kwiet2012247626702_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626702_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626702_btnPopUpTerminationSpa_onClick_seq0(eventobject) {
    if (flowSpa) {
        if (chngUseridspa) {
            /* 
frmCMChgAccessPin.show();
	
 */
        } else if (chngPwdspa) {
            /* 
frmCMChgPwdSPA.show();
	
 */
        } else {
            /* 
kony.print("Inside the spa logic for pwd screen")

 */
            /* 
frmMBSetuseridSPA.show();
	
 */
        }
    } else {
        if (isMenuShown == false) {
            /* 
onClickNextPwdRules.call(this);

 */
        } else {
            /* 
frmAccTrcPwdInter.scrollboxMain.scrollToEnd();

 */
        }
    }
    ehFrmAccTrcPwdInter_btnPopUpTerminationSpa_onClick.call(this, eventobject);
};

function p2kwiet2012247626702_btnPopUpTermination_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        /* 
onClickNextPwdRules.call(this);

 */
    } else {
        /* 
frmAccTrcPwdInter.scrollboxMain.scrollToEnd();

 */
    }
    ehFrmAccTrcPwdInter_btnPopUpTermination_onClick.call(this, eventobject);
};

function p2kwiet2012247626745_frmAddTopUpBiller_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626745_frmAddTopUpBiller_preshow_seq0(eventobject, neworientation) {
    frmAddTopUpBillerMenuPreshow.call(this);
};

function p2kwiet2012247626745_frmAddTopUpBiller_postshow_seq0(eventobject, neworientation) {
    frmAddTopUpBillerMenuPostshow.call(this);
};

function p2kwiet2012247626745_frmAddTopUpBiller_onhide_seq0(eventobject, neworientation) {
    removeMenu.call(this);
};

function p2kwiet2012247626745_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626745_btnHdrMenu_onClick_seq0(eventobject) {
    onClickMenuBtnInHdr.call(this);
};

function p2kwiet2012247626745_btnShow_onClick_seq0(eventobject) {
    frmMyTopUpSelect.show();
    TMBUtil.DestroyForm(frmAddTopUpBiller);
};

function p2kwiet2012247626745_textbox21011640310171357_onDone_seq0(eventobject, changedtext) {
    var alert_seq0_act0 = kony.ui.Alert({
        "message": "Please select a Topup",
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "",
        "alertIcon": "",
        "alertHandler": null
    }, {});
};

function p2kwiet2012247626745_textbox21011640310171358_onDone_seq0(eventobject, changedtext) {
    var alert_seq0_act0 = kony.ui.Alert({
        "message": "Please select a Topup",
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "",
        "alertIcon": "",
        "alertHandler": null
    }, {});
};

function p2kwiet2012247626745_textbox244741353089006_onDone_seq0(eventobject, changedtext) {
    var alert_seq0_act0 = kony.ui.Alert({
        "message": "Please select a Topup",
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "",
        "alertIcon": "",
        "alertHandler": null
    }, {});
};

function p2kwiet2012247626745_btnLtArrow_onClick_seq0(eventobject) {
    onClickLeftArrow1.call(this, null);
};

function p2kwiet2012247626745_btnRtArrow_onClick_seq0(eventobject) {
    onClickRightArrow1.call(this, null);
};

function p2kwiet2012247626745_btnNext_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        var alert_seq5_act0 = kony.ui.Alert({
            "message": "Please select a Topup",
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "",
            "yesLabel": "Yes",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
    } else {
        frmAddTopUpBiller.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247626788_frmAddTopUpBillerconfrmtn_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626788_frmAddTopUpBillerconfrmtn_preshow_seq0(eventobject, neworientation) {
    frmAddTopUpBillerconfrmtnMenuPreshow.call(this);
};

function p2kwiet2012247626788_frmAddTopUpBillerconfrmtn_postshow_seq0(eventobject, neworientation) {
    frmAddTopUpBillerconfrmtnMenuPostshow.call(this);
};

function p2kwiet2012247626788_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626788_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626788_btnRight_onClick_seq0(eventobject) {
    addMoreBills.call(this);
};

function p2kwiet2012247626788_btnDlt_onClick_seq0(eventobject, context) {
    gblTopupDelete = frmAddTopUpBillerconfrmtn.segConfirmationList.selectedIndex[1];
    frmAddTopUpBillerconfrmtn.segConfirmationList.removeAt(gblTopupDelete);
    var tmpLength = 0;
    if (frmAddTopUpBillerconfrmtn.segConfirmationList.data != null) tmpLength = frmAddTopUpBillerconfrmtn.segConfirmationList.data.length;
    else tmpLength = 0;
    if (tmpLength < GLOBAL_MAX_BILL_ADD) {
        frmMyTopUpList.button1010778103103263.setEnabled(true);
        enableAddButtonTopUpBillerConfirm();
        frmMyTopUpList.segSuggestedBillers.setEnabled(true);
    }
    if (tmpLength == 0) {
        TMBUtil.DestroyForm(frmAddTopUpToMB);
        frmMyTopUpList.show();
        gblFlagConfirmDataAddedMB = 0;
    }
};

function p2kwiet2012247626788_btnCancelSpa_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        gblAddBillerFromPay = false;
        frmMyTopUpList.show();
        frmAddTopUpBillerconfrmtn.segConfirmationList.data = [];
        gblFlagConfirmDataAddedMB = 0;
        frmMyTopUpList.button1010778103103263.setEnabled(true);
        enableAddButtonTopUpBillerConfirm();
        frmMyTopUpList.segSuggestedBillers.setEnabled(true);
    } else {
        frmAddTopUpBillerconfrmtn.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        gblAddBillerFromPay = false;
    }
};

function p2kwiet2012247626788_btnAgreeSpa_onClick_seq0(eventobject) {
    if (checkMaxBillerCountOnConfirmMB()) {
        onMytopUpConfirm.call(this);
    } else {
        var segData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
        var listData = myTopupListMB;
        var maxBillAllowed = parseInt(GLOBAL_MAX_BILL_COUNT);
        var TotalBillerCanBeAdded = maxBillAllowed - listData.length;
        alert(kony.i18n.getLocalizedString("Valid_CantAddMore") + TotalBillerCanBeAdded + kony.i18n.getLocalizedString("keyBillersIB"));
    }
};

function p2kwiet2012247626788_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        frmAddTopUpBillerconfrmtn.segConfirmationList.data = [];
        gblFlagConfirmDataAddedMB = 0;
        frmMyTopUpList.button1010778103103263.setEnabled(true);
        enableAddButtonTopUpBillerConfirm();
        frmMyTopUpList.segSuggestedBillers.setEnabled(true);
        frmMyTopUpList.show();
    } else {
        frmAddTopUpBillerconfrmtn.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247626788_btnAgree_onClick_seq0(eventobject) {
    if (checkMaxBillerCountOnConfirmMB()) {
        onMytopUpConfirm.call(this);
    } else {
        var segData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
        var listData = myTopupListMB;
        var maxBillAllowed = parseInt(GLOBAL_MAX_BILL_COUNT);
        var TotalBillerCanBeAdded = maxBillAllowed - listData.length;
        alert(kony.i18n.getLocalizedString("Valid_CantAddMore") + TotalBillerCanBeAdded + kony.i18n.getLocalizedString("keyBillersIB"));
    }
};

function p2kwiet2012247626847_frmAddTopUpToMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626847_frmAddTopUpToMB_preshow_seq0(eventobject, neworientation) {
    frmAddTopUpToMBMenuPreshow.call(this);
};

function p2kwiet2012247626847_frmAddTopUpToMB_postshow_seq0(eventobject, neworientation) {
    frmAddTopUpToMBMenuPostshow.call(this);
};

function p2kwiet2012247626847_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626847_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626847_btnShow_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == 2) {
        frmMyTopUpList.lblSuggestedBillersText.setVisibility(true)
            //frmMyTopUpList.segBillersList.setData(gblCustomerBBMBInqRs);
        frmMyTopUpList.segBillersList.removeAll();
        frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString("keyBillPaymentSelectBill");
        showLoadingScreen();
        gblBBorBillers = false
        populateMySuggestListBBMB(gblmasterBillerAndTopupBBMB);
        getMyBillListMBBB.call(this);
    } else {
        if (flowSpa) {
            TMBUtil.DestroyForm(frmMyTopUpSelect);
            frmMyTopUpSelect.hboxMenuHeader = null;
            isMenuShown = false;
        }
        frmMyTopUpSelect.show();
    }
};

function p2kwiet2012247626847_btnRef2Dropdown_onClick_seq0(eventobject) {
    popUpMyBillers.lblFlag.text = "frmAddTopUpToMB";
    onSelectRef2BB.call(this);
};

function p2kwiet2012247626847_btnLtArrow_onClick_seq0(eventobject) {
    /* 
onClickLeftArrow1.call(this,null);

 */
    if (isMenuShown == false) {
        onClickLeftArrowBB.call(this);
    } else {
        frmAddTopUpToMB.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247626847_segSlider_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectSeg.call(this);
};

function p2kwiet2012247626847_segSlider_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247626847_btnRtArrow_onClick_seq0(eventobject) {
    /* 
onClickRightArrow1.call(this,null);

 */
    onClickRightArrowBB.call(this);
};

function p2kwiet2012247626847_btnSpaNext_onClick_seq0(eventobject) {
    frmAddTopUpToMB.txbNickName.skin = txtNormalBG;
    frmAddTopUpToMB.txbNickName.focusSkin = txtFocusBG;
    frmAddTopUpToMB.txtRef1.skin = txtNormalBG;
    frmAddTopUpToMB.txtRef1.focusSkin = txtFocusBG;
    if (gblMyBillerTopUpBB == 2) {
        if (isMenuShown == false) {
            if (gblMyBillerTopUpBB == 2) {
                onClickNextMBApplyBB.call(this);
            } else {
                if (checkDuplicateNicknameOnAddTopUp()) {
                    nicknameRef1Ref2ValidationCheck.call(this);
                } else {
                    var alert_seq25_act0 = kony.ui.Alert({
                        "message": "Duplicate Nickname !",
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                }
            }
        } else {
            frmBBPaymentApply.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    } else {
        if (isMenuShown == false) {
            if (frmAddTopUpToMB.txbNickName.text.length > 2 && isBillerSelected()) {
                if (checkDuplicateNicknameOnAddTopUp()) {
                    tokenExchangeBeforeSaveParamsInSessSPA.call(this);
                } else {
                    var alert_seq32_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("Valid_DuplicateNickname"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Ok",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                }
            } else {
                if (!isBillerSelected()) {
                    alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
                } else {
                    frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
                    frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
                    alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
                }
            }
        } else {
            frmAddTopUpToMB.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    }
};

function p2kwiet2012247626847_button15633509701481_onClick_seq0(eventobject) {
    frmAddTopUpToMB.txbNickName.skin = txtNormalBG;
    frmAddTopUpToMB.txbNickName.focusSkin = txtFocusBG;
    frmAddTopUpToMB.txtRef1.skin = txtNormalBG;
    frmAddTopUpToMB.txtRef1.focusSkin = txtFocusBG;
    if (gblMyBillerTopUpBB == 2) {
        if (isMenuShown == false) {
            if (gblMyBillerTopUpBB == 2) {
                onClickNextMBApplyBB.call(this);
            } else {
                if (checkDuplicateNicknameOnAddTopUp()) {
                    nicknameRef1Ref2ValidationCheck.call(this);
                } else {
                    var alert_seq31_act0 = kony.ui.Alert({
                        "message": "Duplicate Nickname !",
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                }
            }
        } else {
            frmBBPaymentApply.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    } else {
        if (isMenuShown == false) {
            if (frmAddTopUpToMB.txbNickName.text.length > 0 && isBillerSelected()) {
                if (checkDuplicateNicknameOnAddTopUp()) {
                    addBillerTopupValidationsMB.call(this);
                } else {
                    var alert_seq38_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("Valid_DuplicateNickname"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Ok",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                }
            } else {
                if (!isBillerSelected()) {
                    alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
                } else {
                    frmAddTopUpToMB.txbNickName.skin = txtErrorBG;
                    frmAddTopUpToMB.txbNickName.focusSkin = txtErrorBG;
                    alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
                }
            }
        } else {
            frmAddTopUpToMB.scrollboxMain.scrollToEnd();
            isMenuShown = false;
        }
    }
};

function p2kwiet2012247626873_frmApplyInternetBankingConfirmation_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626873_frmApplyInternetBankingConfirmation_preshow_seq0(eventobject, neworientation) {
    frmApplyInternetBankingConfirmationMenuPreshow.call(this);
};

function p2kwiet2012247626873_frmApplyInternetBankingConfirmation_postshow_seq0(eventobject, neworientation) {
    frmApplyInternetBankingConfirmationMenuPostshow.call(this);
};

function p2kwiet2012247626873_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626873_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626873_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247626873_btnBackHome_onClick_seq0(eventobject) {
    //frmAccountSummaryLanding =null;
    //frmAccountSummaryLandingGlobals();
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    showAccuntSummaryScreen();
};

function p2kwiet2012247626899_frmApplyInternetBankingMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626899_frmApplyInternetBankingMB_preshow_seq0(eventobject, neworientation) {
    frmApplyInternetBankingMBMenuPreshow.call(this);
};

function p2kwiet2012247626899_frmApplyInternetBankingMB_postshow_seq0(eventobject, neworientation) {
    frmApplyInternetBankingMBMenuPostshow.call(this);
};

function p2kwiet2012247626899_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626899_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626899_lnkChangeMobileNo_onClick_seq0(eventobject) {
    showLoadingScreen.call(this);
    viewprofileServiceCall.call(this);
};

function p2kwiet2012247626899_btnInternetBankingNext_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickNext.call(this);
    } else {
        frmApplyInternetBankingMB.scrollboxMain.scrollToEnd();
    }
};

function p2kwiet2012247626930_frmApplyMBConfirmationSPA_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626930_frmApplyMBConfirmationSPA_preshow_seq0(eventobject, neworientation) {
    frmApplyMBConfirmationSPAMenuPreshow.call(this);
};

function p2kwiet2012247626930_frmApplyMBConfirmationSPA_postshow_seq0(eventobject, neworientation) {
    frmApplyMBConfirmationSPAMenuPostshow.call(this);
};

function p2kwiet2012247626930_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626930_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626930_vboxAppleStore_onClick_seq0(eventobject) {
    navigateToAppleStoreIphone.call(this);
};

function p2kwiet2012247626930_vboxGooglePlaystore_onClick_seq0(eventobject) {
    navigateToGooglePlaystore.call(this);
};

function p2kwiet2012247626930_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247626930_btnBackHome_onClick_seq0(eventobject) {
    //frmAccountSummaryLanding =null;
    //frmAccountSummaryLandingGlobals();
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    showAccuntSummaryScreen();
};

function p2kwiet2012247626951_frmApplyServicesMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247626951_frmApplyServicesMB_preshow_seq0(eventobject, neworientation) {
    frmApplyServicesMBMenuPreshow.call(this);
};

function p2kwiet2012247626951_frmApplyServicesMB_postshow_seq0(eventobject, neworientation) {
    frmApplyServicesMBMenuPostshow.call(this);
};

function p2kwiet2012247626951_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247626951_button4490818261082_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247626951_segServiceList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClicSegkApplyServices.call(this);
};

function p2kwiet2012247627017_frmApplySoGooODComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627017_frmApplySoGooODComplete_preshow_seq0(eventobject, neworientation) {
    frmApplySoGooODCompleteMenuPreshow.call(this);
};

function p2kwiet2012247627017_frmApplySoGooODComplete_postshow_seq0(eventobject, neworientation) {
    frmApplySoGooODCompleteMenuPostshow.call(this);
};

function p2kwiet2012247627017_frmApplySoGooODComplete_onhide_seq0(eventobject, neworientation) {};

function p2kwiet2012247627017_btnLeft_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627017_btnRight_onClick_seq0(eventobject) {
    var skin = frmApplySoGooODComplete.btnRight.skin
    if (frmMBSoGooodTnC.hboxaddfblist.isVisible == false) {
        frmMBSoGooodTnC.hboxaddfblist.isVisible = true;
        frmMBSoGooodTnC.imgHeaderRight.src = "arrowtop.png"
        frmMBSoGooodTnC.imgHeaderMiddle.src = "empty.png"
        frmMBSoGooodTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmMBSoGooodTnC.hboxaddfblist.isVisible = false;
        frmMBSoGooodTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmMBSoGooodTnC.imgHeaderRight.src = "empty.png"
        frmMBSoGooodTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247627017_btnPDF_onClick_seq0(eventobject) {
    saveSoGooODCompleteAsPDFImage.call(this, "pdf");
};

function p2kwiet2012247627017_btnEmailto_onClick_seq0(eventobject) {
    postOnFBSoGooODComplete.call(this);
};

function p2kwiet2012247627017_hbox47792425956433_onClick_seq0(eventobject) {
    eh_frmBillPaymentComplete_hbox47792425956433_onClick.call(this);
};

function p2kwiet2012247627017_linkMoreHide_onClick_seq0(eventobject, context) {
    if (isMenuShown == false) {
        onClickOfExcutedSoGooODTnxDetailsMore.call(this);
    } else {
        frmAccountDetailsMB.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247627017_btnStatement_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247627017_btnApplyMore_onClick_seq0(eventobject) {
    onClickApplySoGooODMore.call(this);
};

function p2kwiet2012247627045_frmApplySoGooodLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627045_frmApplySoGooodLanding_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627045_frmApplySoGooodLanding_preshow_seq0(eventobject, neworientation) {
    frmApplySoGooodLandingMenuPreshow.call(this);
};

function p2kwiet2012247627045_frmApplySoGooodLanding_postshow_seq0(eventobject, neworientation) {
    frmApplySoGooodLandingMenuPostshow.call(this);
};

function p2kwiet2012247627045_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627045_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627045_btnRight_onClick_seq0(eventobject) {
    getSoGooODTransactions.call(this);
};

function p2kwiet2012247627045_btnCancel1_onClick_seq0(eventobject) {
    shortCutActDetailsPage.call(this);
    frmAccountDetailsMB.show();
};

function p2kwiet2012247627068_frmAppTour_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247627068_frmAppTour_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627068_frmAppTour_preshow_seq0(eventobject, neworientation) {
    frmAppTourMenuPreshow.call(this);
};

function p2kwiet2012247627068_frmAppTour_postshow_seq0(eventobject, neworientation) {
    frmAppTourMenuPostshow.call(this);
};

function p2kwiet2012247627068_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627068_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627068_btnCancel_onClick_seq0(eventobject) {
    /* 
AppTourOnClickBack.call(this);

 */
};

function p2kwiet2012247627068_btnAgree_onClick_seq0(eventobject) {
    if (isSignedUser) {
        //POST LOGIN CONTACT US
        frmContactUsMB.show();
        //frmContactUsMB.hbxPostLogin.setVisibility(true);
        //frmContactUsMB.hbxContact.setVisibility(false);
        //frmContactUsMB.hbxFAQ.setVisibility(false);
        //frmContactUsMB.hbxFB.setVisibility(false);
        //frmContactUsMB.hbxFindTMB.setVisibility(false);
        //frmContactUsMB.hbxFeedback.setVisibility(false);
        //frmContactUsMB.hboxContactSend.setVisibility(true);
        //frmContactUsMB.hboxFeedBackSend.setVisibility(false);
        //frmContactUsMB.line47592361418663.setVisibility(false);
        //frmContactUsMB.line47592361418559.setVisibility(false);
    } else {
        //PRE LOGIN CONTACT US
        //frmContactUsMB.hbxPostLogin.setVisibility(false);
        //frmContactUsMB.hbxContact.setVisibility(true);
        //frmContactUsMB.hbxFAQ.setVisibility(true);
        frmContactUsMB.hbxFB.setVisibility(true);
        frmContactUsMB.hbxFindTMB.setVisibility(true);
        //frmContactUsMB.hbxFeedback.setVisibility(false);
        //frmContactUsMB.hboxContactSend.setVisibility(false);
        //frmContactUsMB.hboxFeedBackSend.setVisibility(false);
        frmContactUsMB.line47592361418663.setVisibility(false);
        frmContactUsMB.line47592361418559.setVisibility(false);
        frmContactUsMB.show();
    }
    TMBUtil.DestroyForm(frmAppTour);
};

function p2kwiet2012247627099_frmATMBranch_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627099_frmATMBranch_preshow_seq0(eventobject, neworientation) {
    ATMBranchPreshowMenuDesign.call(this);
};

function p2kwiet2012247627099_frmATMBranch_postshow_seq0(eventobject, neworientation) {
    frmATMBranchMenuPostshow.call(this);
};

function p2kwiet2012247627099_hboxRight_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627099_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627099_button104047601447750_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627099_btnAtm_onClick_seq0(eventobject) {
    onClickATM.call(this);
};

function p2kwiet2012247627099_btnBrch_onClick_seq0(eventobject) {
    onClickBranch.call(this);
};

function p2kwiet2012247627099_btnExgBooth_onClick_seq0(eventobject) {
    onClickExchangeBooth.call(this);
};

function p2kwiet2012247627099_btnSearchIcon_onClick_seq0(eventobject) {
    onclickBySearchIcon.call(this);
};

function p2kwiet2012247627099_btnCombProvince_onClick_seq0(eventobject) {
    callATMProvinceData.call(this);
};

function p2kwiet2012247627099_btnCombDistrict_onClick_seq0(eventobject) {
    callATMDistrictData.call(this);
    if (provinceID != "") popUpProvinceData.show();
};

function p2kwiet2012247627099_btnSearchFind_onClick_seq0(eventobject) {
    onClickSearchBtn.call(this);
};

function p2kwiet2012247627099_mapATMBranch_onSelection_seq0(eventobject, location) {};

function p2kwiet2012247627099_mapATMBranch_onPinClick_seq0(eventobject, location) {
    onSelectMapPin.call(this, location);
};

function p2kwiet2012247627135_frmATMBranchesDetails_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627135_frmATMBranchesDetails_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627135_frmATMBranchesDetails_preshow_seq0(eventobject, neworientation) {
    frmATMBranchesDetailsReDesign.call(this);
};

function p2kwiet2012247627135_frmATMBranchesDetails_postshow_seq0(eventobject, neworientation) {
    frmATMBranchesDetailsMenuPostshow.call(this);
};

function p2kwiet2012247627135_hboxRight_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627135_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627135_button865870932217514_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627135_btnGetDir_onClick_seq0(eventobject) {
    onClickGetDirectionList.call(this);
};

function p2kwiet2012247627135_bttnCall_onClick_seq0(eventobject) {
    onClickCallBtn.call(this);
};

function p2kwiet2012247627135_bttnBack_onClick_seq0(eventobject) {
    onClickBackDetails.call(this);
};

function p2kwiet2012247627164_frmATMBranchList_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627164_frmATMBranchList_preshow_seq0(eventobject, neworientation) {
    frmATMBranchListReDesign.call(this);
};

function p2kwiet2012247627164_frmATMBranchList_postshow_seq0(eventobject, neworientation) {
    frmATMBranchListMenuPostshow.call(this);
};

function p2kwiet2012247627164_vbox45116704471826_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627164_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627164_button104047601447750_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627164_segATMListDetails_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickListDetails.call(this);
};

function p2kwiet2012247627164_button104047601447636_onClick_seq0(eventobject) {
    onClickBackList.call(this);
};

function p2kwiet2012247627164_bttnBack_onClick_seq0(eventobject) {
    onClickBackList.call(this);
};

function p2kwiet2012247627312_frmBillPayment_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627312_frmBillPayment_preshow_seq0(eventobject, neworientation) {
    /* 
          

    */
    /* 
frmBillPaymentPreShow.call(this);

 */
    /* 
isMenuShown = false;
isSignedUser = true;
gblMyBillerTopUpBB = 0;
GblBillTopFlag =true;
frmBillPayment.tbxMyNoteValue.numberOfVisibleLines = 1;


 */
    if (gblPaynow) {
        /* 
frmBillPayment.lblPayBillOnValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());

 */
    }
    /* 
DisableFadingEdges.call(this,frmBillPayment);

 */
    frmBillPaymentMenuPreshow.call(this);
};

function p2kwiet2012247627312_frmBillPayment_postshow_seq0(eventobject, neworientation) {
    /* 
       

 /* 
commonMBPostShow.call(this);

 */
    * / 
    /* 
       

 /* 
frmBillPayment.scrollboxMain.scrollToEnd();
kony.print("SPA====>>>>");
frmBillPayment.tbxAmount.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
frmBillPayment.tbxAmount.maxTextLength = 16;

 */
    * / 
    /* 
frmBillPaymentPostShow.call(this);

 */
    frmBillPaymentMenuPostshow.call(this);
};

function p2kwiet2012247627312_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627312_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627312_segSlider_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    billpaymentcvindex = frmBillPayment.segSlider.selectedIndex;
};

function p2kwiet2012247627312_segSlider_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247627312_lblRef1Value_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onFocusLostOfRef1TextBox.call(this);
};

function p2kwiet2012247627312_lblRef1Value_Android_onEndEditing_seq0(eventobject, changedtext) {
    onFocusLostOfRef1TextBox.call(this);
};

function p2kwiet2012247627312_lblRef1Value_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeOfRef1AndRef2.call(this);
};

function p2kwiet2012247627312_tbxRef2Value_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onFocusLostOfRef2TextBox.call(this);
};

function p2kwiet2012247627312_tbxRef2Value_Android_onEndEditing_seq0(eventobject, changedtext) {
    onFocusLostOfRef2TextBox.call(this);
};

function p2kwiet2012247627312_tbxRef2Value_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeOfRef1AndRef2.call(this);
};

function p2kwiet2012247627312_vbox38848739581608_onClick_seq0(eventobject) {
    showMEADetailsOnPopUp.call(this);
};

function p2kwiet2012247627312_btnFull2_onClick_seq0(eventobject) {
    fullPress.call(this);
    fullSpecButtonColorSet.call(this, eventobject);
};

function p2kwiet2012247627312_btnSpecified2_onClick_seq0(eventobject) {
    fullSpecButtonColorSet.call(this, eventobject);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    frmBillPayment.tbxAmount.placeholder = "0.00";
    frmBillPayment.tbxAmount.setFocus(true);
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.text = commaFormatted(fullAmt);
    gblFullPayment = false;
};

function p2kwiet2012247627312_button475004897849_onClick_seq0(eventobject) {
    fullMinSpecButtonColorSet.call(this, eventobject);
    fullPress.call(this);
};

function p2kwiet2012247627312_button475004897851_onClick_seq0(eventobject) {
    fullMinSpecButtonColorSet.call(this, eventobject);
    minPress.call(this);
};

function p2kwiet2012247627312_button475004897853_onClick_seq0(eventobject) {
    fullMinSpecButtonColorSet.call(this, eventobject);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    frmBillPayment.tbxAmount.setFocus(true);
    frmBillPayment.tbxAmount.text = fullAmt;
    frmBillPayment.tbxAmount.placeholder = "0.00";
    frmBillPayment.lblForFullPayment.setVisibility(false);
    gblFullPayment = false;
};

function p2kwiet2012247627312_tbxAmount_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneEditingAmountBillPayment.call(this);
};

function p2kwiet2012247627312_tbxAmount_onDone_seq0(eventobject, changedtext) {
    onDoneEditingAmountBillPayment.call(this);
};

function p2kwiet2012247627312_tbxAmount_onTextChange_seq0(eventobject, changedtext) {
    formatBillPayAmountOnTextChange.call(this);
};

function p2kwiet2012247627312_hbxPayBillOn_onClick_seq0(eventobject) {
    eh_frmBillPayment_btnSchedulePay_onClick.call(this);
};

function p2kwiet2012247627312_hbxMyNote_onClick_seq0(eventobject) {
    ehFrmTransferLanding_hbxTranLandMyNote_onClick.call(this);
};

function p2kwiet2012247627312_tbxMyNoteValue_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247627312_tbxMyNoteValue_Android_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247627312_tbxMyNoteValue_onTextChange_seq0(eventobject, changedtext) {
    notAllowEmojiChars.call(this, eventobject);
};

function p2kwiet2012247627312_btnback_onClick_seq0(eventobject) {
    callMasterBillerService.call(this);
};

function p2kwiet2012247627312_btnNext_onClick_seq0(eventobject) {
    validateChannelMBBillPay.call(this);
    getAccountNameOfUser.call(this, frmBillPayment["segSlider"]["selectedItems"][0].lblActNoval);
};

function p2kwiet2012247627325_frmBillPaymentBillerCategories_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627325_frmBillPaymentBillerCategories_preshow_seq0(eventobject, neworientation) {
    frmBillPaymentBillerCategoriesMenuPreshow.call(this);
};

function p2kwiet2012247627325_btnClose_onClick_seq0(eventobject) {
    onClickBackButtonbillerCategory.call(this);
};

function p2kwiet2012247627325_segBillerCategories_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onRowClickCategories.call(this);
};

function p2kwiet2012247627519_frmBillPaymentComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627519_frmBillPaymentComplete_SPA_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627519_frmBillPaymentComplete_preshow_seq0(eventobject, neworientation) {
    frmBillPaymentCompleteMenuPreshow.call(this);
};

function p2kwiet2012247627519_frmBillPaymentComplete_postshow_seq0(eventobject, neworientation) {
    frmBillPaymentCompleteMenuPostshow.call(this);
};

function p2kwiet2012247627519_frmBillPaymentComplete_onhide_seq0(eventobject, neworientation) {
    /* 
removeMenu.call(this);

 */
};

function p2kwiet2012247627519_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627519_btnRight_onClick_seq0(eventobject) {
    if (frmBillPaymentComplete.hbxShareOption.isVisible) {
        frmBillPaymentComplete.hbxShareOption.isVisible = false;
        frmBillPaymentComplete.imgHeaderMiddle.src = "arrowtop.png";
    } else {
        frmBillPaymentComplete.hbxShareOption.isVisible = true;
        frmBillPaymentComplete.imgHeaderMiddle.src = "empty.png";
    }
};

function p2kwiet2012247627519_vbxImage_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == "0") {} else {}
};

function p2kwiet2012247627519_vbxMessenger_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == "0") {
        shareIntentCall.call(this, "facebook", "BillPayment");
    } else {
        shareIntentCall.call(this, "facebook", "TOPUpPayment");
    }
};

function p2kwiet2012247627519_vbxLine_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == "0") {
        shareIntentCall.call(this, "line", "BillPayment");
    } else {
        shareIntentCall.call(this, "line", "TOPUpPayment");
    }
};

function p2kwiet2012247627519_vbxOthers_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == "0") {} else {}
};

function p2kwiet2012247627519_hbox101271281131304_onClick_seq0(eventobject) {
    hideUnhideBalanceBillPayment.call(this);
};

function p2kwiet2012247627519_hbox47792425956439_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627519_hbxAddToMyBills_onClick_seq0(eventobject) {
    onClickAddToMyBills.call(this);
};

function p2kwiet2012247627519_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247627519_btnBillCancel_onClick_seq0(eventobject) {
    onClickCancelBillPayConfirm.call(this);
};

function p2kwiet2012247627519_btnBillConfirm_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == "0") {
        callBillPaymentFromMenu.call(this);
    } else {
        callTopUpFromMainMenu.call(this);
    }
};

function p2kwiet2012247627714_frmBillPaymentCompleteCalendar_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627714_frmBillPaymentCompleteCalendar_SPA_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627714_frmBillPaymentCompleteCalendar_preshow_seq0(eventobject, neworientation) {
    frmBillPaymentCompleteCalendarMenuPreshow.call(this);
};

function p2kwiet2012247627714_frmBillPaymentCompleteCalendar_postshow_seq0(eventobject, neworientation) {
    frmBillPaymentCompleteCalendarMenuPostshow.call(this);
};

function p2kwiet2012247627714_frmBillPaymentCompleteCalendar_onhide_seq0(eventobject, neworientation) {
    /* 
removeMenu.call(this);

 */
};

function p2kwiet2012247627714_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627714_btnRight_onClick_seq0(eventobject) {
    onClickShareBillpayCalendar.call(this);
};

function p2kwiet2012247627714_vbxImage_onClick_seq0(eventobject) {
    if (gblActivityIds == "27" || gblActivityIds == "28") {} else {}
};

function p2kwiet2012247627714_vbxMessenger_onClick_seq0(eventobject) {
    if (gblActivityIds == "27" || gblActivityIds == "28") {
        shareIntentCall.call(this, "facebook", "BillPayment");
    } else {
        shareIntentCall.call(this, "facebook", "TOPUpPayment");
    }
};

function p2kwiet2012247627714_vbxLine_onClick_seq0(eventobject) {
    if (gblActivityIds == "27" || gblActivityIds == "28") {
        shareIntentCall.call(this, "line", "BillPayment");
    } else {
        shareIntentCall.call(this, "line", "TOPUpPayment");
    }
};

function p2kwiet2012247627714_vbxOthers_onClick_seq0(eventobject) {
    if (gblActivityIds == "27" || gblActivityIds == "28" || gblActivityIds == "027" || gblActivityIds == "028") {} else {}
};

function p2kwiet2012247627714_hbox101271281131304_onClick_seq0(eventobject) {
    if (gblBpBalHideUnhide == "true") {
        frmBillPaymentCompleteCalendar.lblBalanceAfterPayment.setVisibility(false);
        frmBillPaymentCompleteCalendar.lblBalAfterPayValue.setVisibility(false);
        frmBillPaymentCompleteCalendar.lblHide.text = kony.i18n.getLocalizedString("show");
        gblBpBalHideUnhide = "false";
    } else {
        frmBillPaymentCompleteCalendar.lblBalanceAfterPayment.setVisibility(true);
        frmBillPaymentCompleteCalendar.lblBalAfterPayValue.setVisibility(true);
        frmBillPaymentCompleteCalendar.lblHide.text = kony.i18n.getLocalizedString("Hide");
        gblBpBalHideUnhide = "true";
    }
};

function p2kwiet2012247627714_hbox47792425956439_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627714_hbox50285458168_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247627714_btnCancel_onClick_seq0(eventobject) {
    //frmMBMyActivities.show();
    //showCalendar(gsSelectedDate,frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247627874_frmBillPaymentConfirmationFuture_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627874_frmBillPaymentConfirmationFuture_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627874_frmBillPaymentConfirmationFuture_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627874_frmBillPaymentConfirmationFuture_preshow_seq0(eventobject, neworientation) {
    frmBillPaymentConfirmationFutureMenuPreshow.call(this);
};

function p2kwiet2012247627874_frmBillPaymentConfirmationFuture_postshow_seq0(eventobject, neworientation) {
    eh_frmBillPaymentConfirmationFuture_postshow.call(this);
};

function p2kwiet2012247627874_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627874_btnRight_onClick_seq0(eventobject) {
    eh_frmBillPaymentConfirmationFuture_btnRight_onClick.call(this);
};

function p2kwiet2012247627874_hbox47792425956439_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627874_btnTransCnfrmCancel_onClick_seq0(eventobject) {
    onClickCancelBillPayConfirm.call(this);
};

function p2kwiet2012247627874_btnTransCnfrmConfirm_onClick_seq0(eventobject) {
    eh_frmBillPaymentConfirmationFuture_btnConfirm_onClick.call(this);
};

function p2kwiet2012247627976_frmBillPaymentEdit_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627976_frmBillPaymentEdit_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247627976_frmBillPaymentEdit_preshow_seq0(eventobject, neworientation) {
    frmBillPaymentEditMenuPreshow.call(this);
};

function p2kwiet2012247627976_frmBillPaymentEdit_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    frmBillPaymentEdit.scrollboxMain.scrollToEnd();
    frmBillPaymentEdit.txtAmtInput.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY
};

function p2kwiet2012247627976_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247627976_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247627976_buttonOnlineFull_onClick_seq0(eventobject) {
    frmBillPaymentEdit.txtamountvalue.setVisibility(false);
    frmBillPaymentEdit.buttonOnlineFull.skin = "btnScheduleEndLeftFocus";
    frmBillPaymentEdit.buttonOnlineSpecified.skin = "btnScheduleRight";
    if (gblEditBillMethodMB == 3 && gblEditBillerGroupTypeMB == 0) {
        frmBillPaymentEdit.txtAmtInput.text = fullAmtLoanMB;
    } else if (gblEditBillMethodMB == 1 && gblEditBillerGroupTypeMB == 0) {
        frmBillPaymentEdit.txtAmtInput.text = commaFormatted(gblFullAmtVal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); //gblFullAmtVal;    //fullAmtValueOnline;
    }
    //New
    frmBillPaymentEdit.txtAmtInput.setVisibility(true);
    frmBillPaymentEdit.txtAmtInput.setEnabled(false)
        //frmBillPaymentEdit.txtAmtInput.text = gblFullAmtVal +" "+  kony.i18n.getLocalizedString("currencyThaiBaht");; //frmBillPaymentView.lblamtvalue.text;
};

function p2kwiet2012247627976_buttonOnlineSpecified_onClick_seq0(eventobject) {
    //frmBillPaymentEdit.txtamountvalue.text = frmBillPaymentView.lblamtvalue.text; //NEw
    frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
    frmBillPaymentEdit.txtAmtInput.setVisibility(true);
    frmBillPaymentEdit.txtAmtInput.setEnabled(true);
    frmBillPaymentEdit.txtamountvalue.setVisibility(false);
    frmBillPaymentEdit.buttonOnlineFull.skin = "btnScheduleEndLeft";
    frmBillPaymentEdit.buttonOnlineSpecified.skin = "btnScheduleRightFocus";
};

function p2kwiet2012247627976_btnfullpay_onClick_seq0(eventobject) {
    frmBillPaymentEdit.btnspecpay.skin = btnScheduleRight;
    frmBillPaymentEdit.btnminpay.skin = btnScheduleMid;
    frmBillPaymentEdit.btnfullpay.skin = btnScheduleLeftFocus;
    frmBillPaymentEdit.txtamountvalue.setEnabled(false);
    frmBillPaymentEdit.txtamountvalue.text = fullAmountMB;
};

function p2kwiet2012247627976_btnminpay_onClick_seq0(eventobject) {
    frmBillPaymentEdit.btnspecpay.skin = btnScheduleRight;
    frmBillPaymentEdit.btnminpay.skin = btnScheduleMidFocus;
    frmBillPaymentEdit.btnfullpay.skin = btnScheduleLeft;
    frmBillPaymentEdit.txtamountvalue.setEnabled(false);
    frmBillPaymentEdit.txtamountvalue.text = minAmountMB;
};

function p2kwiet2012247627976_btnspecpay_onClick_seq0(eventobject) {
    frmBillPaymentEdit.btnspecpay.skin = btnScheduleRightFocus;
    frmBillPaymentEdit.btnminpay.skin = btnScheduleMid;
    frmBillPaymentEdit.btnfullpay.skin = btnScheduleLeft;
    //enable specified button and amt text
    frmBillPaymentEdit.txtamountvalue.setEnabled(true);
    frmBillPaymentEdit.txtamountvalue.text = frmBillPaymentView.lblamtvalue.text;
};

function p2kwiet2012247627976_btnCalen_onClick_seq0(eventobject) {
    gblTempTPStartOnDateMB = frmBillPaymentEdit.lblStartDate.text
    gblTempTPEndOnDateMB = frmBillPaymentEdit.lblEndOnDate.text
    gblTempTPRepeatAsMB = frmBillPaymentEdit.lblExecutetimes.text
    onClickScheduleButtonMB.call(this);
};

function p2kwiet2012247627976_btnCancel_onClick_seq0(eventobject) {
    onClickCancelEditPage.call(this);
};

function p2kwiet2012247627976_btnConfirm_onClick_seq0(eventobject) {
    isBPScheduleEdited.call(this);
    /* 
onClickNextEditPage.call(this);

 */
};

function p2kwiet2012247628058_frmBillPaymentView_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628058_frmBillPaymentView_preshow_seq0(eventobject, neworientation) {
    frmBillPaymentViewMenuPreshow.call(this);
};

function p2kwiet2012247628058_frmBillPaymentView_postshow_seq0(eventobject, neworientation) {
    frmBillPaymentViewMenuPostshow.call(this);
};

function p2kwiet2012247628058_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628058_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628058_btnedit_onClick_seq0(eventobject) {
    checkMBStatusForEditBP.call(this);
};

function p2kwiet2012247628058_btndelete_onClick_seq0(eventobject) {
    popupEditBPDele.buttonCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupEditBPDele.buttonConfirm.text = kony.i18n.getLocalizedString("keyYes");
    checkMBStatusForDeleteMB.call(this);
};

function p2kwiet2012247628058_btnreturn_onClick_seq0(eventobject) {
    /*
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate,frmMBMyActivities);
    */
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247628105_frmCAPaymentPlanComplete_preshow_seq0(eventobject) {
    frmCAPaymentPlanCompletePreshow.call(this);
};

function p2kwiet2012247628105_frmCAPaymentPlanComplete_postshow_seq0(eventobject) {
    frmCAPaymentPlanCompletePostshow.call(this);
};

function p2kwiet2012247628105_btnMenuMain_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628105_lblManageOtherCards_onClick_seq0(eventobject) {
    onClickManageOtherCardCompleteCA.call(this);
};

function p2kwiet2012247628153_frmCAPaymentPlanConfirmation_preshow_seq0(eventobject) {
    frmCAPaymentPlanConfirmationPreshow.call(this);
};

function p2kwiet2012247628153_frmCAPaymentPlanConfirmation_postshow_seq0(eventobject) {
    frmCAPaymentPlanConfirmationPostshow.call(this);
};

function p2kwiet2012247628153_btnMenuMain_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628153_btnHdrEdit_onClick_seq0(eventobject) {
    onClickBackOfCashAdvConfirmScreen.call(this);
};

function p2kwiet2012247628153_btnBack_onClick_seq0(eventobject) {
    onClickCancelOfCashAdvConfirmScreen.call(this);
};

function p2kwiet2012247628153_btnNext_onClick_seq0(eventobject) {
    onClickConfirmOfCashAdvConfirmScreen.call(this);
};

function p2kwiet2012247628173_frmCardActivationComplete_preshow_seq0(eventobject) {
    frmCardActivationCompletePreshow.call(this);
};

function p2kwiet2012247628173_frmCardActivationComplete_postshow_seq0(eventobject) {
    frmCardActivationCompletePostShow.call(this);
};

function p2kwiet2012247628173_frmCardActivationComplete_init_seq0(eventobject) {
    frmCardActivationCompleteInit.call(this);
};

function p2kwiet2012247628173_btnManageCards_onClick_seq0(eventobject) {
    onClickManageOfRequestPinSuccessScreen.call(this);
};

function p2kwiet2012247628258_frmCardActivationDetails_preshow_seq0(eventobject) {
    frmCardActivationDetailsPreShow.call(this);
};

function p2kwiet2012247628258_frmCardActivationDetails_postshow_seq0(eventobject) {
    frmCardActivationDetailsPostShow.call(this);
};

function p2kwiet2012247628258_frmCardActivationDetails_init_seq0(eventobject) {
    frmCardActivationDetailsInit.call(this);
};

function p2kwiet2012247628258_btn1_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn2_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn3_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn4_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn5_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn6_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn7_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn8_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn9_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn10_onClick_seq0(eventobject) {
    clearATMPINMBNew.call(this);
};

function p2kwiet2012247628258_btn11_onClick_seq0(eventobject) {
    assignATMPINRenderMBNew.call(this, eventobject["text"]);
};

function p2kwiet2012247628258_btn12_onClick_seq0(eventobject) {
    deleteATMPINMBNew.call(this);
};

function p2kwiet2012247628327_frmCashAdvancePlanDetails_preshow_seq0(eventobject) {
    frmCashAdvancePlanDetailsMenuPreshow.call(this);
};

function p2kwiet2012247628327_frmCashAdvancePlanDetails_postshow_seq0(eventobject) {
    frmCashAdvancePlanDetailsMenuPostshow.call(this);
};

function p2kwiet2012247628327_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628327_txtEnterAmount_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneEnterAmount.call(this);
};

function p2kwiet2012247628327_txtEnterAmount_onDone_seq0(eventobject, changedtext) {
    onDoneEnterAmount.call(this);
};

function p2kwiet2012247628327_txtEnterAmount_onTextChange_seq0(eventobject, changedtext) {
    onTextChangePlanAmount.call(this);
};

function p2kwiet2012247628327_flxAmountDtls_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onClickAmount.call(this);
};

function p2kwiet2012247628327_btnAddAmount_onClick_seq0(eventobject) {
    addPlanAmount.call(this);
};

function p2kwiet2012247628327_btnMinusAmount_onClick_seq0(eventobject) {
    minusPlanAmount.call(this);
};

function p2kwiet2012247628327_flxCardType_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onSelectAccountCashAdvDetailsScreen.call(this);
};

function p2kwiet2012247628327_btnBack_onClick_seq0(eventobject) {
    onClickBackOfCashAdvPlanDetailScreen.call(this);
};

function p2kwiet2012247628327_btnNext_onClick_seq0(eventobject) {
    onClickNextOfCashAdvPlanDetailScreen.call(this);
};

function p2kwiet2012247628362_button474230331217991_onClick_seq0_seq4() {
    /* 
frmMyProfile.show();
	
 */
    /* 
TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);

 */
};

function p2kwiet2012247628362_button474230331217991_onClick_seq0_seq5() {
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
};

function act0_p2kwiet2012247628362_button474230331217991_onClick_seq0_seq0(response) {
    if (response == true) {
        p2kwiet2012247628362_button474230331217991_onClick_seq0_seq4()
    } else {
        p2kwiet2012247628362_button474230331217991_onClick_seq0_seq5()
    }
};

function p2kwiet2012247628362_frmChangeMobNoTransLimitMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628362_frmChangeMobNoTransLimitMB_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    dummy.call(this);
};

function p2kwiet2012247628362_frmChangeMobNoTransLimitMB_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    dummy.call(this);
};

function p2kwiet2012247628362_frmChangeMobNoTransLimitMB_preshow_seq0(eventobject, neworientation) {
    frmChangeMobNoTransLimitMBMenuPreshow.call(this);
};

function p2kwiet2012247628362_frmChangeMobNoTransLimitMB_postshow_seq0(eventobject, neworientation) {
    frmChangeMobNoTransLimitMBMenuPostshow.call(this);
};

function p2kwiet2012247628362_vbox444677659278414_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628362_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628362_txtChangeMobileNumber_onTextChange_seq0(eventobject, changedtext) {
    onEditMobileNumber(frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text);
};

function p2kwiet2012247628362_txtChangeTransactionLimit_onDone_seq0(eventobject, changedtext) {
    if (frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text != "" && frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text != undefined && frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text != null && frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text.length > 0) frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = commaFormattedOpenAct(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
};

function p2kwiet2012247628362_button474230331217991_onClick_seq0(eventobject) {
    /* 
       var alert_seq0_act0 = kony.ui.Alert({"message" : "Are you sure you want to cancel?", "alertType" : constants.ALERT_TYPE_CONFIRMATION,"alertTitle": "", "yesLabel" : "Yes", "noLabel" : "No", "alertIcon" : "", "alertHandler" : act0_p2kwiet2012247628362_button474230331217991_onClick_seq0_seq0}, {});

    */
    /* 
frmMyProfile.show();
	
 */
    /* 
TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);

 */
    changeMobileOnclickBack.call(this);
};

function p2kwiet2012247628362_button474230331217989_onClick_seq0(eventobject) {
    if (frmChangeMobNoTransLimitMB.hbxChangeMobileNumber.isVisible) {
        validateMobNuProf.call(this);
    } else {
        changeLimitNext.call(this);
    }
};

function p2kwiet2012247628422_frmCheckContactInfo_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628422_frmCheckContactInfo_SPA_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628422_frmCheckContactInfo_preshow_seq0(eventobject, neworientation) {
    frmCheckContactInfoMenuPreshow.call(this);
};

function p2kwiet2012247628422_frmCheckContactInfo_postshow_seq0(eventobject, neworientation) {
    frmCheckContactInfoMenuPostshow.call(this);
};

function p2kwiet2012247628422_frmCheckContactInfo_onhide_seq0(eventobject, neworientation) {
    /* 
removeMenu.call(this);

 */
    ehFrmTransfersAck_frmTransfersAck_onhide.call(this);
};

function p2kwiet2012247628422_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628422_button67633371958776_onClick_seq0(eventobject) {
    /* 
TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
gblMobNoTransLimitFlag=true;



 */
    /* 
editbuttonflag="number";


 */
    /* 
getIBMBEditProfileStatus.call(this);

 */
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
    editbuttonflag = "profile";
    gblOpenProdAddress = false;
    /* 
getIBMBEditProfileStatus.call(this);

 */
    getMBEditContactStatus.call(this);
    /* 
frmeditMyProfile.show();
	
 */
    /* 
frmeditMyProfile.show();
	
 */
};

function p2kwiet2012247628422_button67633371994156_onClick_seq0(eventobject) {
    /* 
TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
gblMobNoTransLimitFlag=true;



 */
    /* 
editbuttonflag="number";


 */
    /* 
getIBMBEditProfileStatus.call(this);

 */
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
    editbuttonflag = "profile";
    gblOpenProdAddress = false;
    /* 
getIBMBEditProfileStatus.call(this);

 */
    getMBEditContactStatus.call(this);
    /* 
frmeditMyProfile.show();
	
 */
    /* 
frmeditMyProfile.show();
	
 */
};

function p2kwiet2012247628422_button67633371988604_onClick_seq0(eventobject) {
    /* 
TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
gblMobNoTransLimitFlag=true;



 */
    /* 
editbuttonflag="number";


 */
    /* 
getIBMBEditProfileStatus.call(this);

 */
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
    gblMobNoTransLimitFlag = true;
    gblOpenAccountFlow = true;
    editbuttonflag = "number";
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text = "";
    checkStatusEditProfile.call(this);
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
};

function p2kwiet2012247628422_btnback_onClick_seq0(eventobject) {
    /* 
frmTransferLanding.destroy();
frmAccountSummaryLanding.destroy();
gblPaynow = true;

 */
    /* 
cleanUpGlobalVariableTransfersMB.call(this);

 */
    /* 
showAccuntSummaryScreen.call(this);

 */
    /* 
ehFrmTransfersAck_btnTransNPbAckReturn_onClick.call(this);

 */
    loadTermsNConditionOpenAccountMB.call(this);
};

function p2kwiet2012247628422_btnnext_onClick_seq0(eventobject) {
    /* 
makeAnotherTransfer.call(this);

 */
    OnclickShowCheckContactInfo.call(this);
};

function p2kwiet2012247628486_frmCMChgAccessPin_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628486_frmCMChgAccessPin_preshow_seq0(eventobject, neworientation) {
    frmCMChgAccessPinMenuPreshow.call(this);
};

function p2kwiet2012247628486_frmCMChgAccessPin_postshow_seq0(eventobject, neworientation) {
    frmCMChgAccessPinMenuPostshow.call(this);
};

function p2kwiet2012247628486_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628486_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628486_txtAccessPwd_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupAccessPIN.call(this);
};

function p2kwiet2012247628486_txtAccessPwd_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    popAccessPinBubble.destroy();
};

function p2kwiet2012247628486_txtAccessPwd_Android_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupAccessPIN.call(this);
};

function p2kwiet2012247628486_txtAccessPwd_Android_onEndEditing_seq0(eventobject, changedtext) {
    popAccessPinBubble.destroy();
};

function p2kwiet2012247628486_txtAccessPwd_onDone_seq0(eventobject, changedtext) {
    popAccessPinBubble.destroy();
};

function p2kwiet2012247628486_txtAccessPwd_onTextChange_seq0(eventobject, changedtext) {
    var str = frmCMChgAccessPin.txtAccessPwd.text;
    if (str != null && str.length >= 1) {
        popAccessPinBubble.destroy();
    } else {
        var context = {
            "widget": frmCMChgAccessPin.hbox4769997143083,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
        popAccessPinBubble.setContext(context);
        popAccessPinBubble.show();
    }
};

function p2kwiet2012247628486_txtAccessPwdUnMask_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupAccessPIN.call(this);
};

function p2kwiet2012247628486_txtAccessPwdUnMask_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    popAccessPinBubble.dismiss()
};

function p2kwiet2012247628486_txtAccessPwdUnMask_Android_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupAccessPINunmask.call(this);
};

function p2kwiet2012247628486_txtAccessPwdUnMask_Android_onEndEditing_seq0(eventobject, changedtext) {
    popAccessPinBubble.destroy()
};

function p2kwiet2012247628486_txtAccessPwdUnMask_onDone_seq0(eventobject, changedtext) {
    popAccessPinBubble.dismiss()
};

function p2kwiet2012247628486_txtAccessPwdUnMask_onTextChange_seq0(eventobject, changedtext) {
    var str = frmCMChgAccessPin.txtAccessPwdUnMask.text;
    if (str != null && str.length >= 1) {
        popAccessPinBubble.destroy();
    } else {
        var context = {
            "widget": frmCMChgAccessPin.hbox4769997143083,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
        popAccessPinBubble.setContext(context);
        popAccessPinBubble.show();
    }
};

function p2kwiet2012247628486_button474999996108_onClick_seq0(eventobject) {
    onclickHelpButton.call(this);
};

function p2kwiet2012247628486_txtCurUserID_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmCMChgAccessPin.txtCurUserID.text;
if(temp.length != null && temp.length < 1){ 
    var context = {
            "widget": frmCMChgAccessPin.hbox104026732091605,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
 popupBubbleUserId.setContext(context); 
 popupBubbleUserId.show();
 //alert("control here")
}
else { 
 popupBubbleUserId.dismiss();
}

 */
};

function p2kwiet2012247628486_txtNewUserID_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupAccessPIN.call(this);
};

function p2kwiet2012247628486_txtNewUserID_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    popAccessPinBubble.dismiss()
};

function p2kwiet2012247628486_txtNewUserID_Android_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupAccessPIN.call(this);
};

function p2kwiet2012247628486_txtNewUserID_Android_onEndEditing_seq0(eventobject, changedtext) {
    popAccessPinBubble.dismiss()
};

function p2kwiet2012247628486_txtNewUserID_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmCMChgAccessPin.txtNewUserID.text;
if(temp.length != null && temp.length < 1){ 
    var context = {
            "widget": frmCMChgAccessPin.hbox104026732091612,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
 popupBubbleUserId.setContext(context); 
 popupBubbleUserId.show();
 //alert("control here")
}
else { 
 popupBubbleUserId.dismiss();
}

 */
};

function p2kwiet2012247628486_button104026732091617_onClick_seq0(eventobject) {
    onclickuseridButton.call(this);
};

function p2kwiet2012247628486_txtCaptchaText_onDone_seq0(eventobject, changedtext) {
    if (frmIBPreLogin.txtUserId.text != "") {
        if (frmIBPreLogin.txtPassword.text != "") {
            if (frmIBPreLogin.hboxCaptchaText.isVisible) {
                if (frmIBPreLogin.txtCaptchaText.text != "") {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    var alert_seq20_act0 = kony.ui.Alert({
                        "message": "Field is mandatory",
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            var alert_seq16_act0 = kony.ui.Alert({
                "message": "Field is mandatory",
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "",
                "alertIcon": "",
                "alertHandler": null
            }, {});
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        var alert_seq14_act0 = kony.ui.Alert({
            "message": "Field is mandatory",
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
        frmIBPreLogin.txtUserId.setFocus(true);
    }
};

function p2kwiet2012247628486_btnPwdOn_onClick_seq0(eventobject) {
    gblTxtFocusFlag = 5;
    onClickOnChnAxBtn.call(this);
};

function p2kwiet2012247628486_btnPwdOff_onClick_seq0(eventobject) {
    gblTxtFocusFlag = 5;
    onClickChnAxOffBtn.call(this);
};

function p2kwiet2012247628486_button473361467785887_onClick_seq0(eventobject) {
    if (flowSpa) {
        frmMyProfile.show();
    } else {
        if (frmCMChgAccessPin.txtAccessPwdUnMask.isVisible) {
            cancelTimerChngAc();
        }
        frmMyProfile.show();
    }
};

function p2kwiet2012247628486_button473361467785889_onClick_seq0(eventobject) {
    if (flowSpa) {
        validateSpaChangeUserId.call(this);
    } else {
        //if(gblShowPwd ==3){
        //alert("Entered wrong Current PIN 3 times")
        //isSignedUser = false;
        //invokeLogoutService();
        //return false;
        //}
        chngAccepin.call(this);
    }
};

function p2kwiet2012247628486_button4769997143092_onClick_seq0(eventobject) {
    if (flowSpa) {
        frmMyProfile.show();
    } else {
        if (frmCMChgAccessPin.txtAccessPwdUnMask.isVisible) {
            cancelTimerChngAc();
        }
        frmMyProfile.show();
    }
};

function p2kwiet2012247628486_button4769997143093_onClick_seq0(eventobject) {
    frmCMChgAccessPin.tbxCurAccPin.skin = txtNormalBG;
    frmCMChgAccessPin.tbxCurAccPinUnMask.skin = txtNormalBG;
    frmCMChgAccessPin.txtAccessPwd.skin = txtNormalBG;
    frmCMChgAccessPin.txtAccessPwdUnMask.skin = txtNormalBG;
    if (flowSpa) {
        validateSpaChangeUserId.call(this);
    } else {
        //if(gblShowPwd ==3){
        //alert("Entered wrong Current PIN 3 times")
        //isSignedUser = false;
        //invokeLogoutService();
        //return false;
        //}
        chngAccepin.call(this);
    }
};

function p2kwiet2012247628524_frmCMChgPwdSPA_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628524_frmCMChgPwdSPA_preshow_seq0(eventobject, neworientation) {
    frmCMChgPwdSPAMenuPreshow.call(this);
};

function p2kwiet2012247628524_frmCMChgPwdSPA_postshow_seq0(eventobject, neworientation) {
    frmCMChgPwdSPAMenuPostshow.call(this);
};

function p2kwiet2012247628524_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628524_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628524_tbxTranscCrntPwd_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmCMChgPwdSPA.tbxTranscCrntPwd.text;
if(temp.length != null && temp.length < 2){ 
     var context = {
            "widget": frmCMChgPwdSPA.hbox50449293523069,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
 popupBubblePasswordRules.setContext(context); 
 popupBubblePasswordRules.show();
 //alert("control here")
}
else {
 popupBubblePasswordRules.dismiss();
}

 */
};

function p2kwiet2012247628524_txtTransPass_onDone_seq0(eventobject, changedtext) {
    frmCMChgPwdSPA.hbxPassword.skin = "NoSkin"
};

function p2kwiet2012247628524_txtTransPass_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmCMChgPwdSPA.txtTransPass.text;
if(temp.length != null && temp.length < 2){ 
    var context = {
            "widget": frmCMChgPwdSPA.hbxPassword,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
 popupBubblePasswordRules.setContext(context); 
 popupBubblePasswordRules.show();
 //alert("control here")
}
else {
 popupBubblePasswordRules.dismiss();
}

 */
    passwordChanged.call(this);
};

function p2kwiet2012247628524_button47428873338447_onClick_seq0(eventobject) {
    hbox447443295153818.label474999996104.text = kony.i18n.getLocalizedString("keySPApassword");
    helpMessage.label444732340761.text = kony.i18n.getLocalizedString("keyIBPasswordGuidelines")
};

function p2kwiet2012247628524_txtConfirmPassword_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmCMChgPwdSPA.txtConfirmPassword.text;
if(temp.length != null && temp.length < 2){ 
   var context = {
            "widget": frmCMChgPwdSPA.hbox50449293522992,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
 popupBubblePasswordRules.setContext(context); 
 popupBubblePasswordRules.show();
 //alert("control here")
}
else {
 popupBubblePasswordRules.dismiss();
}

 */
};

function p2kwiet2012247628524_button4769997143092_onClick_seq0(eventobject) {
    frmMyProfile.show();
};

function p2kwiet2012247628524_button4769997143093_onClick_seq0(eventobject) {
    validatePasswordProfileSpa.call(this);
};

function p2kwiet2012247628567_frmCMChgTransPwd_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628567_frmCMChgTransPwd_preshow_seq0(eventobject, neworientation) {
    frmCMChgTransPwdMenuPreshow.call(this);
};

function p2kwiet2012247628567_frmCMChgTransPwd_postshow_seq0(eventobject, neworientation) {
    frmCMChgTransPwdMenuPostshow.call(this);
};

function p2kwiet2012247628567_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628567_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628567_txtTransPass_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupTransPass.call(this);
};

function p2kwiet2012247628567_txtTransPass_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    popBubble.destroy();
};

function p2kwiet2012247628567_txtTransPass_Android_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupTransPass.call(this);
};

function p2kwiet2012247628567_txtTransPass_Android_onEndEditing_seq0(eventobject, changedtext) {
    popBubble.destroy();
};

function p2kwiet2012247628567_txtTransPass_onDone_seq0(eventobject, changedtext) {
    /* 
popBubble.destroy();

 */
    ehFrmCMChgTransPwd_txtTransPass_onDone.call(this, eventobject, changedtext);
};

function p2kwiet2012247628567_txtTransPass_onTextChange_seq0(eventobject, changedtext) {
    /* 
var str = frmCMChgTransPwd.txtTransPass.text;

if (str != null && str.length >=1 ){
 popBubble.destroy();
}
else {
 popBubble.show();
}

 */
    /* 
flag=0

 */
    /* 
passwordChanged.call(this);

 */
    ehFrmCMChgTransPwd_txtTransPass_onTextChange.call(this, eventobject, changedtext);
};

function p2kwiet2012247628567_txtTemp_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupTransPass.call(this);
};

function p2kwiet2012247628567_txtTemp_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    popBubble.destroy();
};

function p2kwiet2012247628567_txtTemp_Android_onBeginEditing_seq0(eventobject, changedtext) {
    anchorupTransPassunmask.call(this);
};

function p2kwiet2012247628567_txtTemp_Android_onEndEditing_seq0(eventobject, changedtext) {
    popBubble.destroy();
};

function p2kwiet2012247628567_txtTemp_onDone_seq0(eventobject, changedtext) {
    /* 
popBubble.destroy();

 */
    ehFrmCMChgTransPwd_txtTemp_onDone.call(this, eventobject, changedtext);
};

function p2kwiet2012247628567_txtTemp_onTextChange_seq0(eventobject, changedtext) {
    /* 
var str = frmCMChgTransPwd.txtTemp.text;

if (str != null && str.length >=1 ){
 popBubble.destroy();
}
else {
 popBubble.show();
}

 */
    /* 
flag=1

 */
    /* 
passwordChanged.call(this);

 */
    ehFrmCMChgTransPwd_txtTemp_onTextChange.call(this, eventobject, changedtext);
};

function p2kwiet2012247628567_button474999996108_onClick_seq0(eventobject) {
    onclickHelpButtonTrans.call(this);
};

function p2kwiet2012247628567_btnPwdOn_onClick_seq0(eventobject) {
    /* 
gblTxtFocusFlag = 5;

 */
    /* 
onClickOnBtn.call(this);

 */
    ehFrmCMChgTransPwd_btnPwdOn_onClick.call(this, eventobject);
};

function p2kwiet2012247628567_btnPwdOff_onClick_seq0(eventobject) {
    /* 
gblTxtFocusFlag = 5;

 */
    /* 
onClickOffBtn.call(this);

 */
    ehFrmCMChgTransPwd_btnPwdOff_onClick.call(this, eventobject);
};

function p2kwiet2012247628567_button44843014510078_onClick_seq0(eventobject) {
    /* 
if(frmCMChgTransPwd.txtTemp.isVisible)
{
 cancelTimer();
}

 */
    /* 
frmMyProfile.show();
	
 */
    ehFrmCMChgTransPwd_button44843014510078_onClick.call(this, eventobject);
};

function p2kwiet2012247628567_button4769997143093_onClick_seq0(eventobject) {
    /* 
frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtNormalBG;
frmCMChgTransPwd.tbxTranscCrntPwdTemp.skin = txtNormalBG;
frmCMChgTransPwd.txtTransPass.skin = txtNormalBG;
frmCMChgTransPwd.txtTemp.skin = txtNormalBG;

 */
    /* 
var info1 = kony.i18n.getLocalizedString("info");
 //if(gblShowPwd == 3){
 // alert("Entered wrong Current Password 3 times")
 // var invalidCurPWD = kony.i18n.getLocalizedString("invalidCurPwd");
 // showAlert(invalidCurPWD, info1);
 // isSignedUser=false;
 // invokeLogoutService();
  //frmAfterLogoutMB.show();
 // return false;
 //}

 var curPin=frmCMChgTransPwd.tbxTranscCrntPwd.text
 var pat1 = /[A-Za-z]/g
 var pat2 = /[0-9]/g
 var isAlpha = pat1.test(curPin)
 var isNum = pat2.test(curPin)
 kony.print("isAlpha"+isAlpha)
 if(isAlpha == false || isNum==false)
 {
  gblShowPwd++;
  var transPwdMSG  = kony.i18n.getLocalizedString("invalidtxnCurrentPwd");
  frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtErrorBG;
  frmCMChgTransPwd.tbxTranscCrntPwdTemp.skin = txtErrorBG;
  showAlert(transPwdMSG, info1);
  // alert("Please Enter atleast 1 alphabet and 1 numeric character for Current Password");
  return false;
 }

 if (curPin== null || curPin.length <8)
 {
  gblShowPwd++;
  var minCurrPWD  = kony.i18n.getLocalizedString("minCurrPwd");
  frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtErrorBG;
  frmCMChgTransPwd.tbxTranscCrntPwdTemp.skin = txtErrorBG;
  showAlert(minCurrPWD, info1);
 // alert("Please enter atleast 8 Characters for Current Password")
  return false;
 }
    
 var newPinVisibility=frmCMChgTransPwd.txtTransPass.isVisible
 if (newPinVisibility)
 {
  var newPin=frmCMChgTransPwd.txtTransPass.text
  if(newPin==curPin){
      gblShowPwd++;
      var keySameTransPwdMsg  = kony.i18n.getLocalizedString("keySameTransPwd");
   frmCMChgTransPwd.txtTransPass.skin = txtErrorBG;
   frmCMChgTransPwd.txtTemp.skin = txtErrorBG;
   showAlert(keySameTransPwdMsg, info1);
   return false;
  }
  var pat1 = /[A-Za-z]/g
  var pat2 = /[0-9]/g
  var isAlpha = pat1.test(newPin)
  var isNum = pat2.test(newPin)
  
  if(isAlpha == false || isNum==false)
  {
   gblShowPwd++;
   var transPwdMSG  = kony.i18n.getLocalizedString("transPwdMsgNew");
   frmCMChgTransPwd.txtTransPass.skin = txtErrorBG;
   frmCMChgTransPwd.txtTemp.skin = txtErrorBG;
   showAlert(transPwdMSG, info1);
  // alert("Please Enter atleast 1 alphabet and 1 numeric character for New Password");
   return false;
  }
  if (newPin== null || newPin.length <8 ){
   var minNewPWD  = kony.i18n.getLocalizedString("minNewPwd");
   frmCMChgTransPwd.txtTransPass.skin = txtErrorBG;
   frmCMChgTransPwd.txtTemp.skin = txtErrorBG;
   showAlert(minNewPWD  , info1);
   //alert("Please enter atleast 8 Characters for New Password")
   return false;
  }
 }
 else{
  var newPin=frmCMChgTransPwd.txtTemp.text
  var pat1 = /[A-Za-z]/g
  var pat2 = /[0-9]/g
  var isAlpha = pat1.test(newPin)
  var isNum = pat2.test(newPin)
  if (newPin== null || newPin.length <8){
   //alert("Please enter atleast 8 Characters for New Password")
   var minNewPWD  = kony.i18n.getLocalizedString("minNewPwd");
   frmCMChgTransPwd.txtTemp.skin = txtErrorBG;
   frmCMChgTransPwd.txtTransPass.skin = txtErrorBG;
   showAlert(minNewPWD  , info1);
   return false;
  }
 }

temp = kony.i18n.getLocalizedString("AccesPIN");


 */
    /* 
var invalidTranPwd = kony.i18n.getLocalizedString("invalidTransPwd");
var info1= kony.i18n.getLocalizedString("info");
 
if(gblflag){
kony.print("validation value" + trassactionPwdValidatn(frmCMChgTransPwd.txtTemp.text))
 if(trassactionPwdValidatn(frmCMChgTransPwd.txtTemp.text)){
  showOTPPopup(kony.i18n.getLocalizedString("AccessPin"),"","",onClickConfirmAccessPIN,2)   
 }else{
  showAlert(invalidTranPwd, info1);  
 }
}else{
kony.print("validation value in else" + trassactionPwdValidatn(frmCMChgTransPwd.txtTransPass.text))
 if(trassactionPwdValidatn(frmCMChgTransPwd.txtTransPass.text)){
  showOTPPopup(kony.i18n.getLocalizedString("AccessPin"),"","",onClickConfirmAccessPIN,2)
 }else{
  showAlert(invalidTranPwd, info1);  
 }
}

 */
    /* 
showOTPPopup.call(this,kony.i18n.getLocalizedString("AccessPin"),    "",    "",    onClickConfirmAccessPIN,    2);

 */
    /* 
if(frmCMChgTransPwd.txtTemp.isVisible)
{
 cancelTimer();
}

 */
    /* 
validateChangeTxnPwd.call(this);

 */
    ehFrmCMChgTransPwd_button4769997143093_onClick.call(this, eventobject);
};

function p2kwiet2012247628580_frmConfirmationarchived_preshow_seq0(eventobject, neworientation) {
    frmConfirmationarchivedMenuPreshow.call(this);
};

function p2kwiet2012247628580_frmConfirmationarchived_postshow_seq0(eventobject, neworientation) {
    frmConfirmationarchivedMenuPostshow.call(this);
};

function p2kwiet2012247628580_btnBackHome_onClick_seq0(eventobject) {};

function p2kwiet2012247628605_frmContactUsCompleteScreenMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628605_frmContactUsCompleteScreenMB_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628605_frmContactUsCompleteScreenMB_preshow_seq0(eventobject, neworientation) {
    frmContactUsCompleteScreenMBMenuPreshow.call(this);
};

function p2kwiet2012247628605_frmContactUsCompleteScreenMB_postshow_seq0(eventobject, neworientation) {
    frmContactUsCompleteScreenMBMenuPostshow.call(this);
};

function p2kwiet2012247628605_hboxRight_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628605_btnHdrMenu_onClick_seq0(eventobject) {
    isSignedUser = true;
    TMBUtil.DestroyForm(frmContactUsMB);
    handleMenuBtn.call(this);
};

function p2kwiet2012247628605_button506459299454835_onClick_seq0(eventobject) {
    if (isSignedUser == true) {
        showAccountSummaryFromMenu.call(this);
    } else {}
};

function p2kwiet2012247628618_frmContactusFAQMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628618_frmContactusFAQMB_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628618_frmContactusFAQMB_preshow_seq0(eventobject, neworientation) {
    frmContactusFAQMBMenuPreshow.call(this);
};

function p2kwiet2012247628618_frmContactusFAQMB_postshow_seq0(eventobject, neworientation) {
    frmContactusFAQMBMenuPostshow.call(this);
};

function p2kwiet2012247628618_btnHdrMenu_onClick_seq0(eventobject) {
    kony.print("user--" + isSignedUser);
    kony.print("user--" + isSignedUser);
    if (flowSpa) {
        TMBUtil.DestroyForm(frmContactusFAQMB);
    }
    if (isSignedUser == true) {
        contactUsDisplayForm.call(this);
    } else {
        frmContactUsMB.show();
        frmContactUsMB.hbxPostLogin.setVisibility(false);
        frmContactUsMB.hbxFeedback.setVisibility(false);
        frmContactUsMB.hbxContact.setVisibility(true)
        frmContactUsMB.hbxFAQ.setVisibility(true);
        frmContactUsMB.hbxFB.setVisibility(false);
        frmContactUsMB.hbxFindTMB.setVisibility(false);
        frmContactUsMB.lblheader.text = kony.i18n.getLocalizedString("keyContactUs");
    }
};

function p2kwiet2012247628618_btnBackFacebook_onClick_seq0(eventobject) {
    var form = kony.application.getPreviousForm();
    var condition = null;
    if (form.id.search("MyRecipient") > 0) {
        condition = "1";
    } else if (form.id == "frmeditMyProfile") {
        condition = "2";
    } else {
        condition = "3";
    }
    gblPOWstateTrack = true;
    if (condition == "1") {
        getFriendListFromFacebook.call(this, eventobject);
    } else {
        if (condition == "2") {
            /* 
frmeditMyProfile.show();
	
 */
            gblMyFBdelinkTrack = true;
            fbprofileviewServiceCall.call(this);
        } else {
            postOnWallAfter.call(this);
        }
    }
};

function p2kwiet2012247628618_button506459299404751_onClick_seq0(eventobject) {
    kony.print("user--" + isSignedUser);
    if (flowSpa) {
        TMBUtil.DestroyForm(frmContactusFAQMB);
    }
    if (isSignedUser == true) {
        contactUsDisplayForm.call(this);
    } else {
        frmContactUsMB.show();
        frmContactUsMB.hbxPostLogin.setVisibility(false);
        frmContactUsMB.hbxFeedback.setVisibility(false);
        frmContactUsMB.hbxContact.setVisibility(true)
        frmContactUsMB.line47592361418663.setVisibility(true);
        frmContactUsMB.line47592361418559.setVisibility(true);
        frmContactUsMB.hbxFAQ.setVisibility(true);
        frmContactUsMB.hbxFB.setVisibility(false);
        frmContactUsMB.hbxFindTMB.setVisibility(false);
        frmContactUsMB.lblheader.text = kony.i18n.getLocalizedString("keyContactUs");
    }
};

function p2kwiet2012247628706_frmContactUsMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628706_frmContactUsMB_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628706_frmContactUsMB_preshow_seq0(eventobject, neworientation) {
    frmContactUsMBMenuPreshow.call(this);
};

function p2kwiet2012247628706_frmContactUsMB_postshow_seq0(eventobject, neworientation) {
    frmContactUsMBMenuPostshow.call(this);
};

function p2kwiet2012247628706_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628706_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628706_hbxContact_onClick_seq0(eventobject) {
    TMBCallPopup.button506459299657128.text = kony.i18n.getLocalizedString("keyCancelButton");
    TMBCallPopup.button506459299657130.text = kony.i18n.getLocalizedString("keyCall");
};

function p2kwiet2012247628706_button506459299404188_onClick_seq0(eventobject) {
    TMBCallPopup.button506459299657128.text = kony.i18n.getLocalizedString("keyCancelButton");
    TMBCallPopup.button506459299657130.text = kony.i18n.getLocalizedString("keyCall");
};

function p2kwiet2012247628706_button506459299404190_onClick_seq0(eventobject) {
    gblLatitude = "";
    gblLongitude = "";
    onClickATM.call(this);
};

function p2kwiet2012247628706_hbxFAQ_onClick_seq0(eventobject) {
    isSignedUser = false;
    //frmContactusFAQMB.show();
    kony.print("isSignedUser---" + isSignedUser);
    contactFAQUrl.call(this);
};

function p2kwiet2012247628706_button506459299404191_onClick_seq0(eventobject) {
    isSignedUser = false;
    //frmContactusFAQMB.show();
    kony.print("isSignedUser---" + isSignedUser);
    contactFAQUrl.call(this);
};

function p2kwiet2012247628706_button506459299445013_onClick_seq0(eventobject) {
    showFeedbackPage.call(this, eventobject);
};

function p2kwiet2012247628706_lnkCall_onClick_seq0(eventobject) {
    TMBCallPopup.button506459299657128.text = kony.i18n.getLocalizedString("keyCancelButton");
    TMBCallPopup.button506459299657130.text = kony.i18n.getLocalizedString("keyCall");
};

function p2kwiet2012247628706_link506459299445023_onClick_seq0(eventobject) {
    isSignedUser = true;
    //frmContactusFAQMB.show();
    contactFAQUrl.call(this);
};

function p2kwiet2012247628706_cmbOption_onSelection_seq0(eventobject) {
    contactUsOptions.call(this);
};

function p2kwiet2012247628706_button477746511286957_onClick_seq0(eventobject) {
    contactUsDisplayForm.call(this);
};

function p2kwiet2012247628706_hbxFB1_onClick_seq0(eventobject) {
    onFeedbackSelection.call(this, eventobject);
};

function p2kwiet2012247628706_btn1_onClick_seq0(eventobject) {
    onFeedbackSelection.call(this, eventobject);
};

function p2kwiet2012247628706_hbxFB2_onClick_seq0(eventobject) {
    onFeedbackSelection.call(this, eventobject);
};

function p2kwiet2012247628706_btn2_onClick_seq0(eventobject) {
    onFeedbackSelection.call(this, eventobject);
};

function p2kwiet2012247628706_hbxFB3_onClick_seq0(eventobject) {
    onFeedbackSelection.call(this, eventobject);
};

function p2kwiet2012247628706_btn3_onClick_seq0(eventobject) {
    onFeedbackSelection.call(this, eventobject);
};

function p2kwiet2012247628706_btnContactSend_onClick_seq0(eventobject) {
    contactUsEmainNotification.call(this);
};

function p2kwiet2012247628706_btnSend_onClick_seq0(eventobject) {
    updateFeedbackInDBMB.call(this);
};

function p2kwiet2012247628742_frmDreamCalculator_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628742_frmDreamCalculator_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    dummy.call(this);
};

function p2kwiet2012247628742_frmDreamCalculator_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    dummy.call(this);
};

function p2kwiet2012247628742_frmDreamCalculator_preshow_seq0(eventobject, neworientation) {
    frmDreamCalculatorMenuPreshow.call(this);
};

function p2kwiet2012247628742_frmDreamCalculator_postshow_seq0(eventobject, neworientation) {
    frmDreamCalculatorMenuPostshow.call(this);
};

function p2kwiet2012247628742_lblMnthlySavingVal_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthlySavingVal.skin = "txtFocusBG"
    var textmonthAmount = frmDreamCalculator.lblMnthlySavingVal.text
    frmDreamCalculator.lblMnthlySavingVal.text = textmonthAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
};

function p2kwiet2012247628742_lblMnthlySavingVal_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    calculateMnthCalculator.call(this);
    frmDreamCalculator.lblMnthlySavingVal.skin = "txtNormalBG"
};

function p2kwiet2012247628742_lblMnthlySavingVal_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthlySavingVal.skin = "txtFocusBG"
    var textmonthAmount = frmDreamCalculator.lblMnthlySavingVal.text
    frmDreamCalculator.lblMnthlySavingVal.text = textmonthAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
};

function p2kwiet2012247628742_lblMnthlySavingVal_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthlySavingVal.skin = "txtNormalBG"
    calculateMnthCalculator.call(this);
};

function p2kwiet2012247628742_lblMnthlySavingVal_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthlySavingVal.skin = "txtFocusBG"
    var textmonthAmount = frmDreamCalculator.lblMnthlySavingVal.text
    frmDreamCalculator.lblMnthlySavingVal.text = textmonthAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
};

function p2kwiet2012247628742_lblMnthlySavingVal_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthlySavingVal.skin = "txtNormalBG"
    calculateMnthCalculator.call(this);
};

function p2kwiet2012247628742_lblMnthlySavingVal_onDone_seq0(eventobject, changedtext) {
    /* 
calculateMnthCalculator.call(this);

 */
    frmDreamCalculator.lblMnthlySavingVal.skin = "txtNormalBG"
};

function p2kwiet2012247628742_lblMnthTODReamVal_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthTODReamVal.skin = "txtFocusBG"
    var textmonth = frmDreamCalculator.lblMnthTODReamVal.text
    frmDreamCalculator.lblMnthTODReamVal.text = textmonth.replace(kony.i18n.getLocalizedString("keymonths"), "");
};

function p2kwiet2012247628742_lblMnthTODReamVal_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    calMonthlySavingCalculator.call(this);
    frmDreamCalculator.lblMnthTODReamVal.skin = "txtNormalBG";
};

function p2kwiet2012247628742_lblMnthTODReamVal_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthTODReamVal.skin = "txtFocusBG";
    var textmonth = frmDreamCalculator.lblMnthTODReamVal.text
    frmDreamCalculator.lblMnthTODReamVal.text = textmonth.replace(kony.i18n.getLocalizedString("keymonths"), "");
};

function p2kwiet2012247628742_lblMnthTODReamVal_SPA_onEndEditing_seq0(eventobject, changedtext) {
    calMonthlySavingCalculator.call(this);
    frmDreamCalculator.lblMnthTODReamVal.skin = "txtNormalBG"
};

function p2kwiet2012247628742_lblMnthTODReamVal_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamCalculator.lblMnthTODReamVal.skin = "txtFocusBG";
    var textmonth = frmDreamCalculator.lblMnthTODReamVal.text
    frmDreamCalculator.lblMnthTODReamVal.text = textmonth.replace(kony.i18n.getLocalizedString("keymonths"), "");
};

function p2kwiet2012247628742_lblMnthTODReamVal_Android_onEndEditing_seq0(eventobject, changedtext) {
    calMonthlySavingCalculator.call(this);
    frmDreamCalculator.lblMnthTODReamVal.skin = "txtNormalBG"
};

function p2kwiet2012247628742_lblMnthTODReamVal_onDone_seq0(eventobject, changedtext) {
    /* 
calMonthlySavingCalculator.call(this);

 */
    frmDreamCalculator.lblMnthTODReamVal.skin = "txtNormalBG"
};

function p2kwiet2012247628742_btnCancel_onClick_seq0(eventobject) {
    if (flowSpa) {
        gblcarouselwidgetflow = "Savings"
    }
    frmDreamSavingEdit.show();
};

function p2kwiet2012247628742_btnAgree_onClick_seq0(eventobject) {
    calculateConfirm.call(this);
    /* 
frmDreamSavingEdit.show();
	
 */
};

function p2kwiet2012247628805_frmDreamSavingEdit_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628805_frmDreamSavingEdit_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628805_frmDreamSavingEdit_preshow_seq0(eventobject, neworientation) {
    frmDreamSavingEditMenuPreshow.call(this);
};

function p2kwiet2012247628805_frmDreamSavingEdit_postshow_seq0(eventobject, neworientation) {
    frmDreamSavingEditMenuPostshow.call(this);
};

function p2kwiet2012247628805_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628805_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628805_btnLtArrow_onClick_seq0(eventobject) {
    onClickLeftDS.call(this, frmDreamSavingEdit.segSlider);
    /* 
var currForm = kony.application.getCurrentForm();
 s1 = currForm.segSlider.selectedIndex;
 if(n1<1){
   return false;
   } 
 if (s1[1] == 0) {
  currForm.segSlider.selectedIndex = [0, n1];
 } else {
  currForm.segSlider.selectedIndex = [0, (s1[1] - 1)];
 }
 s1 = currForm.segSlider.selectedIndex;

 */
    dreamSavingPopulateDate.call(this);
};

function p2kwiet2012247628805_segSlider_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectDMSSeg.call(this);
    dreamSavingPopulateDate.call(this);
};

function p2kwiet2012247628805_segSlider_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {
    dreamSavingPopulateDate.call(this);
};

function p2kwiet2012247628805_btnRtArrow_onClick_seq0(eventobject) {
    onClickRightDS.call(this, frmDreamSavingEdit.segSlider);
    dreamSavingPopulateDate.call(this);
    /* 

    var currForm = kony.application.getCurrentForm()
    s1 = currForm.segSlider.selectedIndex;
     if(n1<1){
      return false;
      }
     
    if (s1[1] == n1) {
     currForm.segSlider.selectedIndex = [0, 0];
    } else {
     currForm.segSlider.selectedIndex = [0, (s1[1] + 1)];
    }
    s1 = currForm.segSlider.selectedIndex;


    */
};

function p2kwiet2012247628805_lblNicknameVal_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblNicknameVal.skin = "txtFocusBG"
};

function p2kwiet2012247628805_lblNicknameVal_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblNicknameVal.skin = "txtNormalBG"
};

function p2kwiet2012247628805_lblNicknameVal_onDone_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblNicknameVal.skin = "txtNormalBG";
};

function p2kwiet2012247628805_lblNicknameVal_onTextChange_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblNicknameVal.skin = "txtNormalBG";
};

function p2kwiet2012247628805_lblDreamDescVal_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblDreamDescVal.skin = "txtFocusBG"
};

function p2kwiet2012247628805_lblDreamDescVal_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblDreamDescVal.skin = "txtNormalBG"
};

function p2kwiet2012247628805_lblDreamDescVal_onDone_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblNicknameVal.skin = "txtNormalBG";
};

function p2kwiet2012247628805_lblDreamDescVal_onTextChange_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblNicknameVal.skin = "txtNormalBG";
};

function p2kwiet2012247628805_lblTargetAmntVal_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtFocusBG"
    var setvalue = frmDreamSavingEdit.lblTargetAmntVal.text;
    frmDreamSavingEdit.lblTargetAmntVal.text = setvalue.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
};

function p2kwiet2012247628805_lblTargetAmntVal_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG"
    if (frmDreamSavingEdit.lblTargetAmntVal.text == "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(false);
        frmDreamSavingEdit.button86682510925150.skin = "buttonCalDis";
    }
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG";
    if (frmDreamSavingEdit.lblTargetAmntVal.text != "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(true);
        frmDreamSavingEdit.button86682510925150.skin = "btnCalculate";
    }
    checkSymbol.call(this);
};

function p2kwiet2012247628805_lblTargetAmntVal_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtFocusBG";
    var setvalue = frmDreamSavingEdit.lblTargetAmntVal.text;
    frmDreamSavingEdit.lblTargetAmntVal.text = setvalue.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
};

function p2kwiet2012247628805_lblTargetAmntVal_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG"
    if (frmDreamSavingEdit.lblTargetAmntVal.text == "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(false);
        frmDreamSavingEdit.button86682510925150.skin = "buttonCalDis";
    }
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG";
    if (frmDreamSavingEdit.lblTargetAmntVal.text != "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(true);
        frmDreamSavingEdit.button86682510925150.skin = "btnCalculate";
    }
    checkSymbol.call(this);
};

function p2kwiet2012247628805_lblTargetAmntVal_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtFocusBG";
    var setvalue = frmDreamSavingEdit.lblTargetAmntVal.text;
    frmDreamSavingEdit.lblTargetAmntVal.text = setvalue.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
};

function p2kwiet2012247628805_lblTargetAmntVal_Android_onEndEditing_seq0(eventobject, changedtext) {
    if (frmDreamSavingEdit.lblTargetAmntVal.text == "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(false);
        frmDreamSavingEdit.button86682510925150.skin = "buttonCalDis";
    }
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG";
    if (frmDreamSavingEdit.lblTargetAmntVal.text != "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(true);
        frmDreamSavingEdit.button86682510925150.skin = "btnCalculate";
    }
    checkSymbol.call(this);
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG"
};

function p2kwiet2012247628805_lblTargetAmntVal_onDone_seq0(eventobject, changedtext) {
    if (frmDreamSavingEdit.lblTargetAmntVal.text == "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(false);
        frmDreamSavingEdit.button86682510925150.skin = "buttonCalDis";
    }
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG";
    if (frmDreamSavingEdit.lblTargetAmntVal.text != "0") {
        frmDreamSavingEdit.button86682510925150.setEnabled(true);
        frmDreamSavingEdit.button86682510925150.skin = "btnCalculate";
    }
    /* 
checkSymbol.call(this);

 */
};

function p2kwiet2012247628805_lblTargetAmntVal_onTextChange_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG";
};

function p2kwiet2012247628805_lblSetSavingVal_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblSetSavingVal.skin = "txtFocusBG";
    var setvalue = frmDreamSavingEdit.lblSetSavingVal.text;
    frmDreamSavingEdit.lblSetSavingVal.text = setvalue.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "")
};

function p2kwiet2012247628805_lblSetSavingVal_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblSetSavingVal.skin = "txtNormalBG"
    checkSymbolMonthly.call(this);
};

function p2kwiet2012247628805_lblSetSavingVal_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblSetSavingVal.skin = "txtFocusBG";
    var setvalue = frmDreamSavingEdit.lblSetSavingVal.text;
    frmDreamSavingEdit.lblSetSavingVal.text = setvalue.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "")
};

function p2kwiet2012247628805_lblSetSavingVal_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblSetSavingVal.skin = "txtNormalBG"
    checkSymbolMonthly.call(this);
};

function p2kwiet2012247628805_lblSetSavingVal_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblSetSavingVal.skin = "txtFocusBG";
    var setvalue = frmDreamSavingEdit.lblSetSavingVal.text;
    frmDreamSavingEdit.lblSetSavingVal.text = setvalue.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "")
};

function p2kwiet2012247628805_lblSetSavingVal_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblSetSavingVal.skin = "txtNormalBG"
    checkSymbolMonthly.call(this);
};

function p2kwiet2012247628805_lblSetSavingVal_onDone_seq0(eventobject, changedtext) {
    /* 
frmDreamSavingEdit.lblSetSavingVal.skin="txtNormalBG"



 */
    /* 
checkSymbolMonthly.call(this);

 */
};

function p2kwiet2012247628805_lblSetSavingVal_onTextChange_seq0(eventobject, changedtext) {
    frmDreamSavingEdit.lblSetSavingVal.skin = "txtNormalBG";
};

function p2kwiet2012247628805_button86682510925150_onClick_seq0(eventobject) {
    Calculatebtn.call(this);
};

function p2kwiet2012247628805_btnDreamSavecombo_onClick_seq0(eventobject) {
    DreamshowDateDreamSav.call(this);
};

function p2kwiet2012247628805_btnCancelSpa_onClick_seq0(eventobject) {
    frmDreamSavingMaintainPreShow.call(this);
    frmDreamSavingMB.show();
};

function p2kwiet2012247628805_btnAgreeSpa_onClick_seq0(eventobject) {
    dreamSavingPopulateDateSpa.call(this);
    validationForDSM.call(this);
    openFrmBranchMB.call(this);
};

function p2kwiet2012247628805_btnCancel_onClick_seq0(eventobject) {
    frmDreamSavingMaintainPreShow.call(this);
    frmDreamSavingMB.show();
};

function p2kwiet2012247628805_btnAgree_onClick_seq0(eventobject) {
    validationForDSM.call(this);
    openFrmBranchMB.call(this);
};

function p2kwiet2012247628874_frmDreamSavingMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628874_frmDreamSavingMB_preshow_seq0(eventobject, neworientation) {
    frmDreamSavingMBMenuPreshow.call(this);
};

function p2kwiet2012247628874_frmDreamSavingMB_postshow_seq0(eventobject, neworientation) {
    frmDreamSavingMBMenuPostshow.call(this);
};

function p2kwiet2012247628874_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628874_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628874_btndelelteDepositAccnt_onClick_seq0(eventobject) {
    gblAcctNicName = frmMyAccountView.lblNickName.text;
    kony.print("gblAcctNicName is" + gblAcctNicName);
    gblAccNo = frmMyAccountView.lblAccntNoValue.text;
    kony.print("gblAccNo is" + gblAccNo);
    checkMBUserStatus();
    popupdeleteDreamSave("delico.png", "", ConfirmBtnDreamSaving, CancelBtnDream)
};

function p2kwiet2012247628874_btnEditDepositAccnt_onClick_seq0(eventobject) {
    if (frmDreamSavingMB.lblInterestRateVal.text == "0.0%") {
        showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
    } else {
        checkUserStatusDSM.call(this);
        /* 
DreamonClickAgreeOpenDSActs.call(this);

 */
        clearTargetAmnt.call(this);
        /* 
openFrmBranchMB.call(this);

 */
        oldTargetAmnt = frmDreamSavingMB.lblTargetAmount.text;
        oldTargetAmnt = oldTargetAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
        oldMnthlyAmnt = frmDreamSavingMB.lblMnthlySavingAmntVal.text;
        oldMnthlyAmnt = oldMnthlyAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
        oldRecurringDate = mnthlyTransferDate;
    }
};

function p2kwiet2012247628874_linkaccount_onClick_seq0(eventobject, linktext, attributes) {
    frmMyAccountList.show();
};

function p2kwiet2012247628874_btnBackSpa_onClick_seq0(eventobject) {
    frmMyAccountList.show();
};

function p2kwiet2012247628874_btnBack_onClick_seq0(eventobject) {
    dreamSavingPerformBackButton.call(this);
};

function p2kwiet2012247628921_frmeditContactInfo_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628921_frmeditContactInfo_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628921_frmeditContactInfo_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247628921_frmeditContactInfo_preshow_seq0(eventobject, neworientation) {
    frmeditContactInfoMenuPreshow.call(this);
};

function p2kwiet2012247628921_frmeditContactInfo_postshow_seq0(eventobject, neworientation) {
    frmeditContactInfoMenuPostshow.call(this);
};

function p2kwiet2012247628921_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247628921_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247628921_hbox4758937265922_onClick_seq0(eventobject) {
    gblAddressFlag = 1;
    gblMyProfileAddressFlag = "state";
    editMyContactsProvince.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247628921_hbox4758937266356_onClick_seq0(eventobject) {
    gblAddressFlag = 2;
    gblMyProfileAddressFlag = "district";
    editMyContactsDistrict.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247628921_hbox4758937266374_onClick_seq0(eventobject) {
    gblAddressFlag = 3;
    gblMyProfileAddressFlag = "subdistrict";
    editMyContactsSubDistrict.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247628921_hbox4758937266440_onClick_seq0(eventobject) {
    gblAddressFlag = 4;
    gblMyProfileAddressFlag = "zipcode";
    editMyContactsZipcode.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247628921_button104235284554498_onClick_seq0(eventobject) {
    onClickCheckContactInfo.call(this);
};

function p2kwiet2012247628921_button104235284554500_onClick_seq0(eventobject) {
    EditContactsValidatn.call(this);
};

function p2kwiet2012247628921_button474230331217991_onClick_seq0(eventobject) {
    onClickCheckContactInfo.call(this);
};

function p2kwiet2012247628921_button474230331217989_onClick_seq0(eventobject) {
    EditContactsValidatn.call(this);
};

function p2kwiet2012247629011_frmEditFutureBillPaymentComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629011_frmEditFutureBillPaymentComplete_preshow_seq0(eventobject, neworientation) {
    frmEditFutureBillPaymentCompleteMenuPreshow.call(this);
};

function p2kwiet2012247629011_frmEditFutureBillPaymentComplete_postshow_seq0(eventobject, neworientation) {
    frmEditFutureBillPaymentCompleteMenuPostshow.call(this);
};

function p2kwiet2012247629011_btnLeft_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629011_btnRight_onClick_seq0(eventobject) {
    if (!frmEditFutureBillPaymentComplete.hboxSharelist.isVisible) {
        frmEditFutureBillPaymentComplete.hboximg.setVisibility(true);
        frmEditFutureBillPaymentComplete.hboxSharelist.setVisibility(true);
    } else {
        frmEditFutureBillPaymentComplete.hboximg.setVisibility(false);
        frmEditFutureBillPaymentComplete.hboxSharelist.setVisibility(false);
    }
};

function p2kwiet2012247629011_button47428498625_onClick_seq0(eventobject) {
    onClickGeneratePDFImageForEditMB.call(this, "pdf");
};

function p2kwiet2012247629011_btnPhoto_onClick_seq0(eventobject) {
    onClickGeneratePDFImageForEditMB.call(this, "png");
};

function p2kwiet2012247629011_btnEmailto_onClick_seq0(eventobject) {
    gblPOWcustNME = frmEditFutureBillPaymentComplete.lblFromAccountNickname.text
    gblPOWtransXN = "EditFuturePayment"
    gblPOWchannel = "MB"
    postOnWall()
    requestFromForm = "frmEditFutureBillPaymentComplete"
        /* 
shareFBMBBPTP.call(this);

 */
};

function p2kwiet2012247629011_hbox47792425956435_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        handleMenuBtn.call(this);
    } else {
        onClickForInnerBoxes.call(this);
    }
};

function p2kwiet2012247629011_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247629011_btnCancelSPA_onClick_seq0(eventobject) {
    /*
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate,frmMBMyActivities,1);
    */
    MBMyActivitiesReloadAndShowCalendar();
};

function p2kwiet2012247629011_btnCancel_onClick_seq0(eventobject) {
    /*
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate,frmMBMyActivities,1);
    */
    MBMyActivitiesReloadAndShowCalendar();
};

function p2kwiet2012247629092_frmEditFutureBillPaymentConfirm_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629092_frmEditFutureBillPaymentConfirm_preshow_seq0(eventobject, neworientation) {
    frmEditFutureBillPaymentConfirmMenuPreshow.call(this);
};

function p2kwiet2012247629092_frmEditFutureBillPaymentConfirm_postshow_seq0(eventobject, neworientation) {
    frmEditFutureBillPaymentConfirmMenuPostshow.call(this);
};

function p2kwiet2012247629092_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629092_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629092_btnedit_onClick_seq0(eventobject) {
    frmBillPaymentEdit.show()
};

function p2kwiet2012247629092_btnCancelSPA_onClick_seq0(eventobject) {
    repeatAsMB = "";
    endFreqSaveMB = "";
    gblAmountSelectedMB = false;
    gblScheduleFreqChangedMB = false;
    frmBillPaymentView.show();
};

function p2kwiet2012247629092_btnConfirmSPA_onClick_seq0(eventobject) {
    onClickNextEditBPConfirm.call(this);
};

function p2kwiet2012247629092_btnCancel_onClick_seq0(eventobject) {
    repeatAsMB = "";
    endFreqSaveMB = "";
    gblAmountSelectedMB = false;
    gblScheduleFreqChangedMB = false;
    frmBillPaymentView.show();
};

function p2kwiet2012247629092_btnConfirm_onClick_seq0(eventobject) {
    onClickNextEditBPConfirm.call(this);
};

function p2kwiet2012247629155_frmeditMyProfile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629155_frmeditMyProfile_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629155_frmeditMyProfile_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629155_frmeditMyProfile_preshow_seq0(eventobject, neworientation) {
    frmeditMyProfileMenuPreshow.call(this);
};

function p2kwiet2012247629155_frmeditMyProfile_postshow_seq0(eventobject, neworientation) {
    frmeditMyProfileMenuPostshow.call(this);
};

function p2kwiet2012247629155_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629155_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629155_hbxchangepic_onClick_seq0(eventobject) {
    /* 
          

    */
    /* 
          

    */
};

function p2kwiet2012247629155_button474076475460875_onClick_seq0(eventobject) {};

function p2kwiet2012247629155_button209682697497232_onClick_seq0(eventobject) {};

function p2kwiet2012247629155_btnfb_onClick_seq0(eventobject) {
    handleFacebookIDEdit.call(this);
};

function p2kwiet2012247629155_hbox4758937265922_onClick_seq0(eventobject) {
    gblAddressFlag = 1;
    gblMyProfileAddressFlag = "state";
    editMyProvince.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247629155_hbox4758937266356_onClick_seq0(eventobject) {
    gblAddressFlag = 2;
    gblMyProfileAddressFlag = "district";
    editMyDistrict.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247629155_hbox4758937266374_onClick_seq0(eventobject) {
    gblAddressFlag = 3;
    gblMyProfileAddressFlag = "subdistrict";
    editMySubDistrict.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247629155_hbox4758937266440_onClick_seq0(eventobject) {
    gblAddressFlag = 4;
    gblMyProfileAddressFlag = "zipcode";
    editMyZipcode.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247629155_button104235284554498_onClick_seq0(eventobject) {
    mptemp = "preshow";
    confirmEdit = false;
    selectState = "";
    selectDist = "";
    selectSubDist = "";
    selectZip = "";
    resulttableState = [];
    resulttableDist = [];
    resulttableSubDist = [];
    resulttableStateZip = [];
    changeState = false;
    changedist = false;
    changeSubDist = false;
    changeZip = false;
    frmMyProfile.show();
};

function p2kwiet2012247629155_button104235284554500_onClick_seq0(eventobject) {
    EditProfileValidatn.call(this);
};

function p2kwiet2012247629155_button474230331217991_onClick_seq0(eventobject) {
    mptemp = "preshow";
    confirmEdit = false;
    selectState = "";
    selectDist = "";
    selectSubDist = "";
    selectZip = "";
    resulttableState = [];
    resulttableDist = [];
    resulttableSubDist = [];
    resulttableStateZip = [];
    changeState = false;
    changedist = false;
    changeSubDist = false;
    changeZip = false;
    frmMyProfile.show();
};

function p2kwiet2012247629155_button474230331217989_onClick_seq0(eventobject) {
    EditProfileValidatn.call(this);
};

function p2kwiet2012247629204_frmFATCAQuestionnaire1_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629204_frmFATCAQuestionnaire1_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629204_frmFATCAQuestionnaire1_preshow_seq0(eventobject, neworientation) {
    frmFATCAQuestionnaire1MenuPreshow.call(this);
};

function p2kwiet2012247629204_frmFATCAQuestionnaire1_postshow_seq0(eventobject, neworientation) {
    frmFATCAQuestionnaire1MenuPostshow.call(this);
};

function p2kwiet2012247629204_frmFATCAQuestionnaire1_init_seq0(eventobject, neworientation) {
    /* 
ResetTransferHomePage.call(this);

 */
    if (gblshotcutToACC) {} else {
        ResetTransferHomePage.call(this);
    }
};

function p2kwiet2012247629204_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629204_btnEngR_onClick_seq0(eventobject) {
    if (kony.i18n.getCurrentLocale() != "en_US") {
        gblLang_flag = "en_US";
        //gblLang_flag = "en_US";
        //kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChange, onFailureLocaleChange, "");
        //frmMBTnC.lblTandCSpa.text="";
        setLocaleEng();
        frmFATCAQuestionnaire1.btnEngR.skin = btnOnFocus;
        frmFATCAQuestionnaire1.btnThaiR.skin = btnOffFocus;
        if (gblFATCAPage == 1) frmFATCAQuestionnaire1PreShow();
        else frmFATCAQuestionnaire2PreShow();
    }
};

function p2kwiet2012247629204_btnThaiR_onClick_seq0(eventobject) {
    if (kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH") {
        gblLang_flag = "th_TH";
        //frmMBTnC.lblTandCSpa.text="";
        setLocaleTH();
        //gblLang_flag = "th_TH";
        //kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmFATCAQuestionnaire1.btnEngR.skin = btnOnNormal;
        frmFATCAQuestionnaire1.btnThaiR.skin = btnOffNorm;
        if (gblFATCAPage == 1) frmFATCAQuestionnaire1PreShow();
        else frmFATCAQuestionnaire2PreShow();
    }
};

function p2kwiet2012247629204_btnQHelp_onClick_seq0(eventobject, context) {
    showFATCAQuesInfo.call(this);
};

function p2kwiet2012247629204_btnRadioYes_onClick_seq0(eventobject, context) {
    onClickFATCARadioBtn.call(this, eventobject);
};

function p2kwiet2012247629204_btnRadioNo_onClick_seq0(eventobject, context) {
    onClickFATCARadioBtn.call(this, eventobject);
};

function p2kwiet2012247629204_btnFATCAQues1Next_onClick_seq0(eventobject) {
    populateFATCAPageTwo.call(this);
};

function p2kwiet2012247629204_btnFATCAQues1NextSpa_onClick_seq0(eventobject) {
    populateFATCAPageTwo.call(this);
};

function p2kwiet2012247629253_frmFATCATnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629253_frmFATCATnC_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629253_frmFATCATnC_preshow_seq0(eventobject, neworientation) {
    frmFATCATnCMenuPreshow.call(this);
};

function p2kwiet2012247629253_frmFATCATnC_postshow_seq0(eventobject, neworientation) {
    frmFATCATnCMenuPostshow.call(this);
};

function p2kwiet2012247629253_frmFATCATnC_init_seq0(eventobject, neworientation) {
    frmFATCATnC.btnAgreeCheck.skin = "btnCheck"
    frmFATCATnC.btnAgreeCheck.focusSkin = "btnCheck"
};

function p2kwiet2012247629253_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629253_btnEngR_onClick_seq0(eventobject) {
    if (kony.i18n.getCurrentLocale() != "en_US") {
        gblLang_flag = "en_US";
        //gblLang_flag = "en_US";
        //kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChange, onFailureLocaleChange, "");
        setLocaleEng();
        frmFATCATnC.btnEngR.skin = btnOnFocus;
        frmFATCATnC.btnThaiR.skin = btnOffFocus;
        frmFATCATnCPreShow();
    }
};

function p2kwiet2012247629253_btnThaiR_onClick_seq0(eventobject) {
    if (kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH") {
        gblLang_flag = "th_TH";
        setLocaleTH();
        //gblLang_flag = "th_TH";
        //kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmFATCATnC.btnEngR.skin = btnOnNormal;
        frmFATCATnC.btnThaiR.skin = btnOffNorm;
        frmFATCATnCPreShow();
    }
};

function p2kwiet2012247629253_btnClose_onClick_seq0(eventobject) {
    if (flowSpa == true) {
        if (GBL_Fatca_Flow == "OAccounts") {
            if (gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9" || gblFATCAUpdateFlag == "Z") {
                TMBUtil.DestroyForm(frmAccountSummaryLanding);
                isMenuShown = false
                    //frmAccountSummaryLanding = null;
                    //frmAccountSummaryLandingGlobals();
                gblAccountTable = "";
                showLoadingScreen();
                callCustomerAccountService();
            } else {
                ivokeCustActInqForOpenAct();
                frmOpenActSelProd.scrollboxMain.scrollToEnd();
            }
        } else {
            showLoadingScreen();
            callCustomerAccountService();
        }
    } else {
        if (GBL_Fatca_Flow == "OAccounts") {
            if (gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9" || gblFATCAUpdateFlag == "Z") {
                TMBUtil.DestroyForm(frmAccountSummaryLanding); // added for DEF1410
                showAccuntSummaryScreen();
            } else {
                ivokeCustActInqForOpenAct();
            }
        } else {
            showLoadingScreen();
            LoginProcessServExecMB("");
        }
    }
};

function p2kwiet2012247629253_btnAgreeSpa_onClick_seq0(eventobject) {
    getMBFATCATermsAndCondition();
};

function p2kwiet2012247629253_btnAgree_onClick_seq0(eventobject) {
    getMBFATCATermsAndCondition();
};

function p2kwiet2012247629253_btnAgreeCheck_onClick_seq0(eventobject) {
    onClickFATCATnCAgreeBtn.call(this, eventobject);
};

function p2kwiet2012247629253_btnNext_onClick_seq0(eventobject) {
    onClickFATCATnCNext.call(this);
};

function p2kwiet2012247629253_btnNextSpa_onClick_seq0(eventobject) {
    onClickFATCATnCNext.call(this);
};

function p2kwiet2012247629260_frmFBLogin_preshow_seq0(eventobject, neworientation) {
    frmFBLoginMenuPreshow.call(this);
};

function p2kwiet2012247629260_frmFBLogin_postshow_seq0(eventobject, neworientation) {
    frmFBLoginMenuPostshow.call(this);
};

function p2kwiet2012247629260_btnBackFacebook_onClick_seq0(eventobject) {
    var form = kony.application.getPreviousForm();
    var condition = null;
    if (form.id.search("MyRecipient") > 0) {
        condition = "1";
    } else if (form.id == "frmeditMyProfile") {
        condition = "2";
    } else {
        condition = "3";
    }
    gblPOWstateTrack = true;
    if (condition == "1") {
        getFriendListFromFacebook.call(this, eventobject);
    } else {
        if (condition == "2") {
            /* 
frmeditMyProfile.show();
	
 */
            gblMyFBdelinkTrack = true;
            fbprofileviewServiceCall.call(this);
        } else {
            postOnWallAfter.call(this);
        }
    }
};

function p2kwiet2012247629278_btnCancel_onClick_seq0_seq6() {
    if (GblBillTopFlag) {
        gblFirstTimeBillPayment = true;
        gblPaynow = true;
        frmBillPayment.show();
    } else {
        gblFirstTimeTopUp = true;
        frmTopUp.show();
    }
};

function act0_p2kwiet2012247629278_btnCancel_onClick_seq0_seq0(response) {
    if (response == true) {
        p2kwiet2012247629278_btnCancel_onClick_seq0_seq6()
    } else {}
};

function p2kwiet2012247629278_frmFBProfileLogin_preshow_seq0(eventobject, neworientation) {
    frmFBProfileLoginMenuPreshow.call(this);
};

function p2kwiet2012247629278_frmFBProfileLogin_postshow_seq0(eventobject, neworientation) {
    frmFBProfileLoginMenuPostshow.call(this);
};

function p2kwiet2012247629278_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629278_button475868836189416_onClick_seq0(eventobject) {
    onClickMenuBtnInHdr.call(this);
};

function p2kwiet2012247629278_btnCancel_onClick_seq0(eventobject) {
    var alert_seq0_act0 = kony.ui.Alert({
        "message": "Do you really wish to cancel?",
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Cancel Transaction",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "",
        "alertHandler": act0_p2kwiet2012247629278_btnCancel_onClick_seq0_seq0
    }, {});
};

function p2kwiet2012247629278_btnConfirm_onClick_seq0(eventobject) {
    gotoBillPaymentComplete.call(this);
};

function p2kwiet2012247629304_frmFeedbackComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247629304_frmFeedbackComplete_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629304_frmFeedbackComplete_preshow_seq0(eventobject, neworientation) {
    frmFeedbackCompleteMenuPreshow.call(this);
};

function p2kwiet2012247629304_frmFeedbackComplete_postshow_seq0(eventobject, neworientation) {
    frmFeedbackCompleteMenuPostshow.call(this);
};

function p2kwiet2012247629304_vbox47682299024475_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629304_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629304_button10403270597268_onClick_seq0(eventobject) {
    showAccountSummaryFromMenu.call(this);
};

function p2kwiet2012247629304_button101086657957077_onClick_seq0(eventobject) {
    showAccountSummaryFromMenu.call(this);
};

function p2kwiet2012247629324_frmFPSetting_preshow_seq0(eventobject) {
    frmFPSettingMenuPreshow.call(this);
};

function p2kwiet2012247629324_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629324_flxTouchED_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onImgEDTouch.call(this);
};

function p2kwiet2012247629324_btnReturnHome_onClick_seq0(eventobject) {
    showAcntSummary.call(this);
};

function p2kwiet2012247629362_frmGetTMBTouch_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629362_frmGetTMBTouch_preshow_seq0(eventobject, neworientation) {
    frmGetTMBTouchMenuPreshow.call(this);
};

function p2kwiet2012247629362_frmGetTMBTouch_postshow_seq0(eventobject, neworientation) {
    frmGetTMBTouchMenuPostshow.call(this);
};

function p2kwiet2012247629362_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629362_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629362_hbxAppleStore_onClick_seq0(eventobject) {
    navigateToAppleStoreIphone.call(this);
};

function p2kwiet2012247629362_hbxPlayStore_onClick_seq0(eventobject) {
    navigateToGooglePlaystore.call(this);
};

function p2kwiet2012247629397_frmInboxDetails_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247629397_frmInboxDetails_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629397_frmInboxDetails_preshow_seq0(eventobject, neworientation) {
    frmInboxDetailsMenuPreshow.call(this);
};

function p2kwiet2012247629397_frmInboxDetails_postshow_seq0(eventobject, neworientation) {
    frmInboxDetailsMenuPostshow.call(this);
};

function p2kwiet2012247629397_vbox47682299021986_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629397_btnHdrMenu_onClick_seq0(eventobject) {
    unreadInboxMessagesTrackerLocalDBSync.call(this);
};

function p2kwiet2012247629397_button58778872837658_onClick_seq0(eventobject) {
    gblInboxHistoryModified = true;
    frmInboxHome.show();
};

function p2kwiet2012247629397_button4733076529054_onClick_seq0(eventobject) {
    gblInboxHistoryModified = true;
    frmInboxHome.show();
};

function p2kwiet2012247629397_button1399939188243213_onClick_seq0(eventobject) {
    gblInboxHistoryModified = true;
    frmInboxHome.show();
};

function p2kwiet2012247629397_btnMBpromo_onClick_seq0(eventobject) {
    getHotPromotionsResult.call(this);
};

function p2kwiet2012247629441_frmInboxHome_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247629441_frmInboxHome_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629441_frmInboxHome_preshow_seq0(eventobject, neworientation) {
    frmInboxHomeMenuPreshow.call(this);
};

function p2kwiet2012247629441_frmInboxHome_postshow_seq0(eventobject, neworientation) {
    frmInboxHomeMenuPostshow.call(this);
};

function p2kwiet2012247629441_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629441_btnHdrMenu_onClick_seq0(eventobject) {
    frmInboxHome_btnHdrMenu_onClick.call(this);
};

function p2kwiet2012247629441_btnCancel_onClick_seq0(eventobject) {
    InboxCancelOnClick.call(this, eventobject);
};

function p2kwiet2012247629441_btnRight_onClick_seq0(eventobject) {
    InboxDeleteOnClick.call(this, eventobject);
};

function p2kwiet2012247629441_hbox477746511278608_onClick_seq0(eventobject) {};

function p2kwiet2012247629441_cbxDateTime_onSelection_seq0(eventobject) {
    popupSortTypeFunc.call(this);
};

function p2kwiet2012247629441_cbxAscenDescend_onSelection_seq0(eventobject) {
    popupSortOrderFunc.call(this);
};

function p2kwiet2012247629441_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629441_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629460_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629485_frmMBAccLocked_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629485_frmMBAccLocked_preshow_seq0(eventobject, neworientation) {
    frmMBAccLockedMenuPreshow.call(this);
};

function p2kwiet2012247629485_frmMBAccLocked_postshow_seq0(eventobject, neworientation) {
    frmMBAccLockedMenuPostshow.call(this);
};

function p2kwiet2012247629485_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629485_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629485_richtext4483370253244_onClick_seq0(eventobject, linktext, attributes) {
    dummy.call(this);
};

function p2kwiet2012247629485_button4483370253250_onClick_seq0(eventobject) {
    gblSetPwd = true;
    if (isMenuShown) {
        frmMBAccLocked.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    } else {
        frmMBTnCPreShow();
        frmMBTnC.show();
    }
};

function p2kwiet2012247629538_frmMBActiAtmIdMobile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629538_frmMBActiAtmIdMobile_preshow_seq0(eventobject, neworientation) {
    frmMBActiAtmIdMobileMenuPreshow.call(this);
};

function p2kwiet2012247629538_frmMBActiAtmIdMobile_postshow_seq0(eventobject, neworientation) {
    frmMBActiAtmIdMobileMenuPostshow.call(this);
};

function p2kwiet2012247629538_frmMBActiAtmIdMobile_onhide_seq0(eventobject, neworientation) {
    frmMBActiAtmIdMobileMenuOnHideshow.call(this);
};

function p2kwiet2012247629538_tbxIdPassport_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditOfCIorPPInActivationUsingCard.call(this);
};

function p2kwiet2012247629538_tbxIdPassport_Android_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditOfCIorPPInActivationUsingCard.call(this);
};

function p2kwiet2012247629538_tbxIdPassport_Android_onEndEditing_seq0(eventobject, changedtext) {
    /* 
onEditCitiZenID.call(this,frmMBActiAtmIdMobile.tbxIdPassport.text);

 */
};

function p2kwiet2012247629538_tbxIdPassport_onDone_seq0(eventobject, changedtext) {
    onDoneValidateCIorPPInActivationUsingCard.call(this);
};

function p2kwiet2012247629538_tbxIdPassport_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeOfCIOrPPInActivationUsingCard.call(this);
};

function p2kwiet2012247629538_tbxMobileNo_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629538_tbxMobileNo_Android_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629538_tbxMobileNo_onTextChange_seq0(eventobject, changedtext) {
    onEditMobileNumber(frmMBActiAtmIdMobile.tbxMobileNo.text);
};

function p2kwiet2012247629538_hbxATM_onClick_seq0(eventobject) {
    onClickATMCardImg.call(this);
};

function p2kwiet2012247629538_hbxCC_onClick_seq0(eventobject) {
    onClickCreditCardImg.call(this);
};

function p2kwiet2012247629538_tbxAtmNumber_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629538_tbxAtmNumber_Android_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629538_tbxAtmNumber_onTextChange_seq0(eventobject, changedtext) {
    formatCardNumber.call(this, frmMBActiAtmIdMobile.tbxAtmNumber.text);
};

function p2kwiet2012247629538_txtCreditCardNum_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629538_txtCreditCardNum_Android_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629538_txtCreditCardNum_onTextChange_seq0(eventobject, changedtext) {
    formatCardNumber.call(this, frmMBActiAtmIdMobile.txtCreditCardNum.text);
};

function p2kwiet2012247629538_vbxReloadCaptcha_onClick_seq0(eventobject) {
    refreshCaptchaImageSPA.call(this);
};

function p2kwiet2012247629538_button4733076528913_onClick_seq0(eventobject) {
    ActivationMBViaIBLogoutService.call(this);
};

function p2kwiet2012247629538_btnNext_onClick_seq0(eventobject) {
    verifyATMCardForActivation.call(this);
};

function p2kwiet2012247629565_frmMBActiComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629565_frmMBActiComplete_preshow_seq0(eventobject, neworientation) {
    frmMBActiCompleteMenuPreshow.call(this);
};

function p2kwiet2012247629565_frmMBActiComplete_postshow_seq0(eventobject, neworientation) {
    frmMBActiCompleteMenuPostshow.call(this);
};

function p2kwiet2012247629565_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629565_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629565_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247629565_btnStart_onClick_seq0(eventobject) {
    onClickOfStartBtnForMBSnippet.call(this);
};

function p2kwiet2012247629606_frmMBActiConfirm_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629606_frmMBActiConfirm_preshow_seq0(eventobject, neworientation) {
    frmMBActiConfirmMenuPreshow.call(this);
};

function p2kwiet2012247629606_frmMBActiConfirm_postshow_seq0(eventobject, neworientation) {
    frmMBActiConfirmMenuPostshow.call(this);
};

function p2kwiet2012247629606_frmMBActiConfirm_onhide_seq0(eventobject, neworientation) {
    if (gblLang_flag) {
        TMBUtil.DestroyForm(frmMBActiConfirm)
    } else {
        gblLang_flag = false
    }
};

function p2kwiet2012247629606_frmMBActiConfirm_init_seq0(eventobject, neworientation) {
    if (flowSpa) {
        frmMBActiConfirm.lblAccNoVal.text = frmMBActivation.txtAccountNumberspa.text;
    } else {
        frmMBActiConfirm.lblAccNoVal.text = frmMBActivation.txtAccountNumber.text;
    }
    frmMBActiConfirm.lblIDNoVal.text = frmMBActivation.txtIDPass.text;
    frmMBActiConfirm.lblPhNoVal.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
};

function p2kwiet2012247629606_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629606_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629606_button50288422184100_onClick_seq0(eventobject) {
    gblLastClicked = null;
    if (isMenuShown == false) {
        onClickActiConfirm.call(this);
    } else {
        frmMBActiConfirm.scrollboxMain.scrollToEnd();
        dismissLoadingScreen();
    }
};

function p2kwiet2012247629606_btnCancelActivation_onClick_seq0(eventobject) {
    ActivationMBViaIBLogoutService.call(this);
};

function p2kwiet2012247629606_btnConfirm_onClick_seq0(eventobject) {
    showLoadingScreen();
    gblLastClicked = null;
    if (isMenuShown == false) {
        onClickActiConfirm.call(this);
    } else {
        frmMBActiConfirm.scrollboxMain.scrollToEnd();
        dismissLoadingScreen();
    }
};

function p2kwiet2012247629606_btnOTP_onClick_seq0(eventobject) {
    showLoadingScreen();
    gblLastClicked = null;
    if (isMenuShown == false) {
        onClickActiConfirm.call(this);
    } else {
        frmMBActiConfirm.scrollboxMain.scrollToEnd();
        dismissLoadingScreen();
    }
};

function p2kwiet2012247629633_frmMBActiEmailDeviceName_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629633_frmMBActiEmailDeviceName_preshow_seq0(eventobject, neworientation) {
    frmMBActiEmailDeviceNameMenuPreshow.call(this);
};

function p2kwiet2012247629633_frmMBActiEmailDeviceName_postshow_seq0(eventobject, neworientation) {
    frmMBActiEmailDeviceNameMenuPostshow.call(this);
};

function p2kwiet2012247629633_tbxEmail_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629633_tbxEmail_Android_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629633_tbxDeviceName_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629633_tbxDeviceName_Android_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247629633_button4733076528913_onClick_seq0(eventobject) {
    ActivationMBViaIBLogoutService.call(this);
};

function p2kwiet2012247629633_btnNext_onClick_seq0(eventobject) {
    mbActivationEmailDeviceNameValidatn.call(this);
};

function p2kwiet2012247629710_frmMBActivateAnyId_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629710_frmMBActivateAnyId_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629710_frmMBActivateAnyId_preshow_seq0(eventobject, neworientation) {
    frmMBActivateAnyIdMenuPreshow.call(this);
};

function p2kwiet2012247629710_frmMBActivateAnyId_postshow_seq0(eventobject, neworientation) {
    frmMBActivateAnyIdMenuPostshow.call(this);
};

function p2kwiet2012247629710_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629710_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629710_btnRight_onClick_seq0(eventobject) {
    var skin = frmOpenProdDetnTnC.btnRight.skin
    if (frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible == false) {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = true;
        frmOpenProdDetnTnC.imgHeaderRight.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = false;
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247629710_button508341360534625_onClick_seq0(eventobject) {
    /* 
TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
gblMobNoTransLimitFlag=true;



 */
    /* 
editbuttonflag="number";


 */
    /* 
getIBMBEditProfileStatus.call(this);

 */
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
    gblMobNoTransLimitFlag = true;
    //s2sBusinessHrsFlag = "true";
    gblRegisterWithAnyId = "true";
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text = "";
    /* 
checkStatusEditProfile.call(this);

 */
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
    checkAnyIDEditProfile.call(this);
};

function p2kwiet2012247629710_btnPhCheckBox_onClick_seq0(eventobject) {
    gblselectedChckBox = "";
    gblselectedChckBox = "PhoneCheckBox";
    selectMobileCheckBOx.call(this);
};

function p2kwiet2012247629710_switchMobile_onslide_seq0(eventobject) {
    gblselectedChckBox = "";
    gblselectedChckBox = "PhoneCheckBox";
    onSlideMobileNum.call(this);
};

function p2kwiet2012247629710_hbxPhoneAccount_onClick_seq0(eventobject) {
    showTMBAccounts.call(this);
};

function p2kwiet2012247629710_btnCIChckBox_onClick_seq0(eventobject) {
    gblselectedChckBox = "";
    gblselectedChckBox = "CICheckBox";
    selectCICheckBOx.call(this);
};

function p2kwiet2012247629710_switchSocialID_onslide_seq0(eventobject) {
    gblselectedChckBox = "";
    gblselectedChckBox = "CICheckBox";
    onSlideCitizenID.call(this);
};

function p2kwiet2012247629710_hboxSocialAccount_onClick_seq0(eventobject) {
    showCITMBAccounts.call(this);
};

function p2kwiet2012247629710_hbox508341360713257_onClick_seq0(eventobject) {
    loadAnyIDRegTermsNConditions.call(this);
};

function p2kwiet2012247629710_btnback_onClick_seq0(eventobject) {
    agreeButton = false;
    frmMBAnyIdRegTnC.show();
};

function p2kwiet2012247629710_btnNext_onClick_seq0(eventobject) {
    /* 
generateOTPAnyIDRegService.call(this);

 */
    /* 
showAnyIDRegComplete.call(this);

 */
    onClickAnyIDRegRequestOtp.call(this);
};

function p2kwiet2012247629734_frmMBActivateDebitCardComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629734_frmMBActivateDebitCardComplete_preshow_seq0(eventobject, neworientation) {
    frmMBActivateDebitCardCompleteMenuPreshow.call(this);
};

function p2kwiet2012247629734_frmMBActivateDebitCardComplete_postshow_seq0(eventobject, neworientation) {
    frmMBActivateDebitCardCompleteMenuPostshow.call(this);
};

function p2kwiet2012247629734_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629734_btnLeft_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629734_btnNext_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickCancelPointRedemption.call(this);
    } else {
        frmMBActivateDebitCardComplete.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247629787_frmMBActivateEnterCVV_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629787_frmMBActivateEnterCVV_preshow_seq0(eventobject, neworientation) {
    frmMBActivateEnterCVVMenuPreshow.call(this);
};

function p2kwiet2012247629787_frmMBActivateEnterCVV_postshow_seq0(eventobject, neworientation) {
    frmMBActivateEnterCVVMenuPostshow.call(this);
};

function p2kwiet2012247629787_frmMBActivateEnterCVV_onhide_seq0(eventobject, neworientation) {
    if (gblLang_flag) {
        TMBUtil.DestroyForm(frmMBActivateEnterCVV)
    } else {
        gblLang_flag = false
    }
};

function p2kwiet2012247629787_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629787_button1495287783233617_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button1495287783233618_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button1495287783233619_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button363582120224309_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button363582120224307_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button363582120224311_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button363582120225205_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button363582120225203_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button363582120225207_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button1495287783233622_onClick_seq0(eventobject) {
    clearCVVMB.call(this);
};

function p2kwiet2012247629787_button1495287783233621_onClick_seq0(eventobject) {
    verifyCVVRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247629787_button1495287783233620_onClick_seq0(eventobject) {
    deleteCVVMB.call(this);
};

function p2kwiet2012247629787_btnCancel1_onClick_seq0(eventobject) {
    ActivationMBViaIBLogoutService.call(this);
};

function p2kwiet2012247629787_btnNext_onClick_seq0(eventobject) {
    mbValidateCVV.call(this);
};

function p2kwiet2012247629833_frmMBActivation_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629833_frmMBActivation_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629833_frmMBActivation_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629833_frmMBActivation_preshow_seq0(eventobject, neworientation) {
    frmMBActivationMenuPreshow.call(this);
};

function p2kwiet2012247629833_frmMBActivation_postshow_seq0(eventobject, neworientation) {
    frmMBActivationMenuPostshow.call(this);
};

function p2kwiet2012247629833_frmMBActivation_init_seq0(eventobject, neworientation) {
    if (flowSpa) {
        frmMBActivation.txtAccountNumberspa.text = "";
    } else {
        frmMBActivation.txtAccountNumber.text = "";
    }
    frmMBActivation.txtActivationCode.text = "";
    frmMBActivation.txtIDPass.text = "";
};

function p2kwiet2012247629833_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629833_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
    frmMBActivation.btnHdrMenu.setFocus(true);
};

function p2kwiet2012247629833_txtActivationCode_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    if (isMenuShown) {
        frmMBActivation.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    frmMBActivation.hbxHoldActTextHelpIcon.skin = "hboxBG";
};

function p2kwiet2012247629833_txtActivationCode_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmMBActivation.hbxHoldActTextHelpIcon.skin = "NoSkin";
};

function p2kwiet2012247629833_txtActivationCode_Android_onBeginEditing_seq0(eventobject, changedtext) {
    if (isMenuShown) {
        frmMBActivation.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    frmMBActivation.hbxHoldActTextHelpIcon.skin = "hboxBG";
};

function p2kwiet2012247629833_txtActivationCode_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmMBActivation.hbxHoldActTextHelpIcon.skin = "NoSkin";
};

function p2kwiet2012247629833_txtActivationCode_onDone_seq0(eventobject, changedtext) {
    ActvatnCodeValidatn.call(this, frmMBActivation.txtActivationCode.text);
};

function p2kwiet2012247629833_btnActivationHelp_onClick_seq0(eventobject) {
    onclickActivationHelp.call(this);
};

function p2kwiet2012247629833_txtAccountNumber_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    if (isMenuShown) {
        frmMBActivation.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247629833_txtAccountNumber_Android_onBeginEditing_seq0(eventobject, changedtext) {
    if (isMenuShown) {
        frmMBActivation.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247629833_txtAccountNumber_onDone_seq0(eventobject, changedtext) {
    gblPrevLen = 0;
    var temp = frmMBActivation.txtAccountNumber.text;
    var numChars = temp.length;
    for (var i = 0; i < numChars; i++) {
        if (temp[i] == '-') {
            temp = temp.replace("-", "");
        }
    }
    accDoubleAddValidtn.call(this, temp);
};

function p2kwiet2012247629833_txtAccountNumber_onTextChange_seq0(eventobject, changedtext) {
    onEditAccNum.call(this, frmMBActivation.txtAccountNumber.text);
    frmMBActivation.txtActivationCode.skin = "txtNormalBG";
};

function p2kwiet2012247629833_txtAccountNumberspa_onDone_seq0(eventobject, changedtext) {
    gblPrevLen = 0;
    if (flowSpa) {
        var temp = frmMBActivation.txtAccountNumberspa.text;
    } else {
        var temp = frmMBActivation.txtAccountNumber.text;
    }
    var numChars = temp.length;
    for (var i = 0; i < numChars; i++) {
        if (temp[i] == '-') {
            temp = temp.replace("-", "");
        }
    }
    accDoubleAddValidtn.call(this, temp);
};

function p2kwiet2012247629833_txtAccountNumberspa_onTextChange_seq0(eventobject, changedtext) {
    onEditAccNum.call(this, frmMBActivation.txtAccountNumberspa.text);
    frmMBActivation.txtActivationCode.skin = "txtNormalBG";
};

function p2kwiet2012247629833_txtIDPass_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditOfCIorPPInActivationUsingActiCode.call(this);
};

function p2kwiet2012247629833_txtIDPass_Android_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditOfCIorPPInActivationUsingActiCode.call(this);
};

function p2kwiet2012247629833_txtIDPass_Android_onEndEditing_seq0(eventobject, changedtext) {
    /* 
onEditCitiZenID.call(this,frmMBActivation.txtIDPass.text);

 */
};

function p2kwiet2012247629833_txtIDPass_onDone_seq0(eventobject, changedtext) {
    onDoneValidateCIorPPInActivationUsingActiCode.call(this);
};

function p2kwiet2012247629833_txtIDPass_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeOfCIOrPPInActivationUsingActiCode.call(this);
};

function p2kwiet2012247629833_btnConfirmSpa_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        mbActivationValidatn.call(this);
    } else {
        frmMBActivation.scrollboxMain.scrollToEnd();
    }
};

function p2kwiet2012247629833_btnCancelActivation_onClick_seq0(eventobject) {
    frmMBanking.show();
};

function p2kwiet2012247629833_btnConfirm_onClick_seq0(eventobject) {
    frmMBActivation.txtActivationCode.skin = txtNormalBG;
    frmMBActivation.txtAccountNumber.skin = txtNormalBG;
    frmMBActivation.txtIDPass.skin = txtNormalBG;
    if (isMenuShown == false) {
        mbActivationValidatn.call(this);
    } else {
        frmMBActivation.scrollboxMain.scrollToEnd();
    }
};

function p2kwiet2012247629854_frmMBActivationConfirmMobile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629854_frmMBActivationConfirmMobile_preshow_seq0(eventobject, neworientation) {
    frmMBActivationConfirmMobileMenuPreshow.call(this);
};

function p2kwiet2012247629854_frmMBActivationConfirmMobile_postshow_seq0(eventobject, neworientation) {
    frmMBActivationConfirmMobileMenuPostshow.call(this);
};

function p2kwiet2012247629854_button4733076528913_onClick_seq0(eventobject) {
    ActivationMBViaIBLogoutService.call(this);
};

function p2kwiet2012247629854_btnNext_onClick_seq0(eventobject) {
    activationViaIBUserOTP.call(this);
};

function p2kwiet2012247629892_frmMBActivationIBLogin_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629892_frmMBActivationIBLogin_preshow_seq0(eventobject, neworientation) {
    frmMBActivationIBLoginMenuPreshow.call(this);
};

function p2kwiet2012247629892_frmMBActivationIBLogin_postshow_seq0(eventobject, neworientation) {
    frmMBActivationIBLoginMenuPostshow.call(this);
};

function p2kwiet2012247629892_frmMBActivationIBLogin_onhide_seq0(eventobject, neworientation) {
    frmMBActivationIBLogin.txtUserId.text = "";
    frmMBActivationIBLogin.txtPassword.text = "";
    frmMBActivationIBLogin.txtCaptchaText.text = "";
};

function p2kwiet2012247629892_txtUserId_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204231.skin = "hboxLightBlue"
};

function p2kwiet2012247629892_txtUserId_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204231.skin = "hboxWhiteback"
};

function p2kwiet2012247629892_txtUserId_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    /* 
var temp = frmSPALogin.txtUserId.text;
if(temp.length != null && temp.length < 1){ 
 var context = {
            "widget": frmSPALogin.hbox502735421204231,
            "anchor": "left",
            "sizetoanchorwidth": false
        };
        popupBubbleUserId.setContext(context);
        popupBubbleUserId.show();
        //alert("control here")
    } else {
        popupBubbleUserId.dismiss();
    }

 */
    frmMBActivationIBLogin.hbox502735421204231.skin = "hboxLightBlue"
};

function p2kwiet2012247629892_txtUserId_SPA_onEndEditing_seq0(eventobject, changedtext) {
    /* 
popupBubbleUserId.dismiss();

 */
    frmMBActivationIBLogin.hbox502735421204231.skin = "hboxWhiteback"
};

function p2kwiet2012247629892_txtUserId_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204231.skin = "hboxLightBlue"
};

function p2kwiet2012247629892_txtUserId_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204231.skin = "hboxWhiteback"
};

function p2kwiet2012247629892_txtUserId_onDone_seq0(eventobject, changedtext) {
    /* 
    popupBubbleUserId.dismiss();

    */
};

function p2kwiet2012247629892_txtUserId_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmSPALogin.txtUserId.text;
if(temp.length != null && temp.length < 1){ 
 var context = {
            "widget": frmSPALogin.hbox502735421204231,
            "anchor": "left",
            "sizetoanchorwidth": false
        };
        popupBubbleUserId.setContext(context);
        popupBubbleUserId.show();
        //alert("control here")
    } else {
        popupBubbleUserId.dismiss();
    }

 */
};

function p2kwiet2012247629892_txtPassword_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204261.skin = "hboxLightBlue"
};

function p2kwiet2012247629892_txtPassword_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204261.skin = "hboxWhiteback"
};

function p2kwiet2012247629892_txtPassword_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    /* 
var temp = frmSPALogin.txtPassword.text;
if(temp.length != null && temp.length < 1){ 
 var context = {
            "widget": frmSPALogin.hbox502735421204231,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
        popupBubblePasswordRules.setContext(context);
        popupBubblePasswordRules.show();
        //alert("control here")
    } else {
        popupBubblePasswordRules.dismiss();
    }

 */
    frmMBActivationIBLogin.hbox502735421204261.skin = "hboxLightBlue"
};

function p2kwiet2012247629892_txtPassword_SPA_onEndEditing_seq0(eventobject, changedtext) {
    /* 
popupBubblePasswordRules.dismiss();

 */
    frmMBActivationIBLogin.hbox502735421204261.skin = "hboxWhiteback"
};

function p2kwiet2012247629892_txtPassword_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204261.skin = "hboxLightBlue"
};

function p2kwiet2012247629892_txtPassword_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmMBActivationIBLogin.hbox502735421204261.skin = "hboxWhiteback"
};

function p2kwiet2012247629892_txtPassword_onDone_seq0(eventobject, changedtext) {
    /* 
popupBubblePasswordRules.dismiss();

 */
};

function p2kwiet2012247629892_txtPassword_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmSPALogin.txtPassword.text;
if(temp.length != null && temp.length < 1){ 
 var context = {
            "widget": frmSPALogin.hbox502735421204231,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
        popupBubblePasswordRules.setContext(context);
        popupBubblePasswordRules.show();
        //alert("control here")
    } else {
        popupBubblePasswordRules.dismiss();
    }

 */
};

function p2kwiet2012247629892_vbxReloadCaptcha_onClick_seq0(eventobject) {
    refreshCaptchaImageSPA.call(this);
};

function p2kwiet2012247629892_button4733076528913_onClick_seq0(eventobject) {
    frmMBanking.show();
};

function p2kwiet2012247629892_btnLogIn_onClick_seq0(eventobject) {
    if (!/^\s*$/.test(frmMBActivationIBLogin.txtUserId.text)) {
        if (frmMBActivationIBLogin.txtPassword.text != "") {
            if (frmMBActivationIBLogin.hboxCaptchaText.isVisible) {
                if (frmMBActivationIBLogin.txtCaptchaText.text != "") {
                    captchaValidationSpa.call(this, frmMBActivationIBLogin.txtCaptchaText.text);
                } else {
                    var alert_seq18_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("keyCaptchaRequired"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                    frmMBActivationIBLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                mbActivationIBLogin.call(this);
            }
        } else {
            alert(kony.i18n.getLocalizedString("keyPasswordRequired"));
            frmMBActivationIBLogin.txtPassword.setFocus(true);
        }
    } else {
        alert(kony.i18n.getLocalizedString("keyEnterUserId"));
        frmMBActivationIBLogin.txtUserId.setFocus(true);
    }
};

function p2kwiet2012247629927_frmMBanking_Android_onDeviceBack_seq0(eventobject, neworientation) {
    closeApplicationBackBtn.call(this);
};

function p2kwiet2012247629927_frmMBanking_preshow_seq0(eventobject, neworientation) {
    frmMBankingMenuPreshow.call(this);
};

function p2kwiet2012247629927_frmMBanking_postshow_seq0(eventobject, neworientation) {
    frmMBankingMenuPostshow.call(this);
};

function p2kwiet2012247629927_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629927_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629927_hbxActivationATM_onClick_seq0(eventobject) {
    /* 

    */
    frmMBTnC.show();
    gblMBActivationVia = "3"
    navigateToTandC.call(this);
};

function p2kwiet2012247629927_hbxActivationIB_onClick_seq0(eventobject) {
    gblMBActivationVia = "2"
    navigateToTandC.call(this);
};

function p2kwiet2012247629927_hbxActivationCode_onClick_seq0(eventobject) {
    gblMBActivationVia = "1"
    navigateToTandC.call(this);
};

function p2kwiet2012247629960_frmMBAnyIDRegAcceptTnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629960_frmMBAnyIDRegAcceptTnC_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629960_frmMBAnyIDRegAcceptTnC_preshow_seq0(eventobject, neworientation) {
    frmChangeMobNoTransLimitMBMenuPreshow.call(this);
};

function p2kwiet2012247629960_frmMBAnyIDRegAcceptTnC_postshow_seq0(eventobject, neworientation) {
    frmChangeMobNoTransLimitMBMenuPostshow.call(this);
};

function p2kwiet2012247629960_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629960_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629960_btnRight_onClick_seq0(eventobject) {
    var skin = frmMBAnyIDRegAcceptTnC.btnRight.skin
    if (frmMBAnyIDRegAcceptTnC.hboxSaveCamEmail.isVisible == false) {
        frmMBAnyIDRegAcceptTnC.hboxSaveCamEmail.isVisible = true;
        frmMBAnyIDRegAcceptTnC.imgHeaderRight.src = "arrowtop.png"
        frmMBAnyIDRegAcceptTnC.imgHeaderMiddle.src = "empty.png"
        frmMBAnyIDRegAcceptTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmMBAnyIDRegAcceptTnC.hboxSaveCamEmail.isVisible = false;
        frmMBAnyIDRegAcceptTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmMBAnyIDRegAcceptTnC.imgHeaderRight.src = "empty.png"
        frmMBAnyIDRegAcceptTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247629960_button47505874736294_onClick_seq0(eventobject) {
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "AnyIDRegistration");
};

function p2kwiet2012247629960_button47505874736298_onClick_seq0(eventobject) {
    onClickEmailTnCMBAnyID.call(this);
};

function p2kwiet2012247629960_btnAnyID_onClick_seq0(eventobject) {
    frmMBActivateAnyId.show();
};

function p2kwiet2012247629995_frmMBAnyIdRegCompleted_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629995_frmMBAnyIdRegCompleted_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247629995_frmMBAnyIdRegCompleted_preshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegCompletedMenuPreshow.call(this);
};

function p2kwiet2012247629995_frmMBAnyIdRegCompleted_postshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegCompletedMenuPostshow.call(this);
};

function p2kwiet2012247629995_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247629995_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247629995_btnRight_onClick_seq0(eventobject) {
    var skin = frmOpenProdDetnTnC.btnRight.skin
    if (frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible == false) {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = true;
        frmOpenProdDetnTnC.imgHeaderRight.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = false;
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247629995_btnHome_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247629995_btnSettings_onClick_seq0(eventobject) {
    isMBRegister = false;
    isCIRegister = false;
    userMBChanged = false;
    userCIChanged = false;
    isMBDeregister = false;
    isCIDeregister = false;
    callAnyIDInq.call(this);
};

function p2kwiet2012247629995_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247630022_frmMBAnyIdRegFail_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630022_frmMBAnyIdRegFail_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630022_frmMBAnyIdRegFail_preshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegFailMenuPreshow.call(this);
};

function p2kwiet2012247630022_frmMBAnyIdRegFail_postshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegFailMenuPostshow.call(this);
};

function p2kwiet2012247630022_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630022_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630022_btnRight_onClick_seq0(eventobject) {
    var skin = frmOpenProdDetnTnC.btnRight.skin
    if (frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible == false) {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = true;
        frmOpenProdDetnTnC.imgHeaderRight.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = false;
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247630022_button508341360444969_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247630022_button508341360444971_onClick_seq0(eventobject) {
    onClickAnyIDRegTnCAgree.call(this);
};

function p2kwiet2012247630047_frmMBAnyIdRegSave_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630047_frmMBAnyIdRegSave_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630047_frmMBAnyIdRegSave_preshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegSaveMenuPreshow.call(this);
};

function p2kwiet2012247630047_frmMBAnyIdRegSave_postshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegSaveMenuPreshow.call(this);
};

function p2kwiet2012247630047_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630047_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630047_btnRight_onClick_seq0(eventobject) {
    var skin = frmOpenProdDetnTnC.btnRight.skin
    if (frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible == false) {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = true;
        frmOpenProdDetnTnC.imgHeaderRight.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = false;
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247630047_button508341360444969_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247630047_button508341360444971_onClick_seq0(eventobject) {
    onClickAnyIDRegTnCAgree.call(this);
};

function p2kwiet2012247630074_frmMBAnyIdRegTnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630074_frmMBAnyIdRegTnC_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630074_frmMBAnyIdRegTnC_preshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegTnCMenuPreshow.call(this);
};

function p2kwiet2012247630074_frmMBAnyIdRegTnC_postshow_seq0(eventobject, neworientation) {
    frmMBAnyIdRegTnCMenuPostshow.call(this);
};

function p2kwiet2012247630074_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630074_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630074_btnRight_onClick_seq0(eventobject) {
    var skin = frmOpenProdDetnTnC.btnRight.skin
    if (frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible == false) {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = true;
        frmOpenProdDetnTnC.imgHeaderRight.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = false;
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247630074_btnCancelCon_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247630074_btnNext_onClick_seq0(eventobject) {
    isMBRegister = false;
    isCIRegister = false;
    userMBChanged = false;
    userCIChanged = false;
    isMBDeregister = false;
    isCIDeregister = false;
    fromBack = false;
    onClickAnyIDRegTnCAgree.call(this);
};

function p2kwiet2012247630104_frmMBAnyIDSelectActs_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630104_frmMBAnyIDSelectActs_preshow_seq0(eventobject, neworientation) {
    frmMBAnyIDSelectActsMenuPreshow.call(this);
};

function p2kwiet2012247630104_frmMBAnyIDSelectActs_postshow_seq0(eventobject, neworientation) {
    frmMBAnyIDSelectActsMenuPostshow.call(this);
};

function p2kwiet2012247630104_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630104_segTMBAccntDetails_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    displayselectedAccs.call(this);
};

function p2kwiet2012247630104_btnAnyID_onClick_seq0(eventobject) {
    onSelectActBack.call(this);
};

function p2kwiet2012247630143_frmMBApplySoGooodComplete_preshow_seq0(eventobject) {
    preshowfrmMBApplySoGooodComplete.call(this);
};

function p2kwiet2012247630143_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630143_btnRight_onClick_seq0(eventobject) {};

function p2kwiet2012247630143_flexPdf_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.buttonPdf.skin = "btnPdfSCFoc";
    frmMBCashAdvanceTnC.flexPdf.skin = "flexLightBlueLine";
};

function p2kwiet2012247630143_flexPdf_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.flexPdf.skin = "flexWhiteBG";
    frmMBCashAdvanceTnC.buttonPdf.skin = "btnPdfSC";
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "TMBCashAdvance");
};

function p2kwiet2012247630143_btnPDF_onClick_seq0(eventobject) {
    saveSoGooODCompleteAsPDFImage.call(this, "pdf");
};

function p2kwiet2012247630143_flexEmail_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.buttonEmail.skin = "btnEmailSCFoc"
    frmMBCashAdvanceTnC.flexEmail.skin = "flexLightBlueLine";
};

function p2kwiet2012247630143_flexEmail_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.flexEmail.skin = "flexWhiteBG";
    frmMBCashAdvanceTnC.buttonEmail.skin = "btnEmailSC"
    onClickEmailTnCMBCashAdvance.call(this);
};

function p2kwiet2012247630143_btnEmailto_onClick_seq0(eventobject) {
    postOnFBSoGooODComplete.call(this);
};

function p2kwiet2012247630143_btnBackHome_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247630143_btnApplyMore_onClick_seq0(eventobject) {
    onClickApplySoGooODMore.call(this);
};

function p2kwiet2012247630143_btnBackHome2_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247630143_btnTryAgain_onClick_seq0(eventobject) {
    performTryAgain.call(this);
};

function p2kwiet2012247630209_frmMBApplysogooodConfirm_preshow_seq0(eventobject) {
    preshowfrmMBApplysogooodConfirm.call(this);
};

function p2kwiet2012247630209_btnMenu1_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630209_btnBack_onClick_seq0(eventobject) {
    /* 
performCancelConfirm.call(this);

 */
    frmAccountDetailsMB.show();
};

function p2kwiet2012247630209_btnConfirm_onClick_seq0(eventobject) {
    MBcallApplySogoodConfirmationService.call(this);
};

function p2kwiet2012247630266_frmMBAssignAtmPin_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630266_frmMBAssignAtmPin_preshow_seq0(eventobject, neworientation) {
    frmMBAssignAtmPinMenuPreshow.call(this);
};

function p2kwiet2012247630266_frmMBAssignAtmPin_postshow_seq0(eventobject, neworientation) {
    frmMBAssignAtmPinMenuPostshow.call(this);
};

function p2kwiet2012247630266_frmMBAssignAtmPin_onhide_seq0(eventobject, neworientation) {
    if (gblLang_flag) {
        TMBUtil.DestroyForm(frmMBEnterATMPin)
    } else {
        gblLang_flag = false
    }
};

function p2kwiet2012247630266_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630266_btnLeft_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630266_button1495287783233617_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button1495287783233618_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button1495287783233619_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button363582120224309_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button363582120224307_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button363582120224311_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button363582120225205_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button363582120225203_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button363582120225207_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button1495287783233622_onClick_seq0(eventobject) {
    clearATMPINMB.call(this);
};

function p2kwiet2012247630266_button1495287783233621_onClick_seq0(eventobject) {
    assignATMPINRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630266_button1495287783233620_onClick_seq0(eventobject) {
    deleteATMPINMB.call(this);
};

function p2kwiet2012247630266_btnCancel1_onClick_seq0(eventobject) {
    callingFrmDetailPage.call(this);
};

function p2kwiet2012247630266_btnNext_onClick_seq0(eventobject) {
    verifyPinService.call(this);
};

function p2kwiet2012247630325_frmMBAssignAtmPinNew_preshow_seq0(eventobject) {
    frmMBAssignAtmPinNewVRpreShow.call(this);
};

function p2kwiet2012247630325_frmMBAssignAtmPinNew_init_seq0(eventobject) {
    init_frmMBAssignAtmPinNew_Function.call(this);
};

function p2kwiet2012247630369_frmMBAssignAtmPinOld_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630369_frmMBAssignAtmPinOld_preshow_seq0(eventobject, neworientation) {
    frmMBAssignAtmPinOldMenuPreshow.call(this);
};

function p2kwiet2012247630369_frmMBAssignAtmPinOld_postshow_seq0(eventobject, neworientation) {
    frmMBAssignAtmPinOldMenuPostshow.call(this);
};

function p2kwiet2012247630369_frmMBAssignAtmPinOld_onhide_seq0(eventobject, neworientation) {
    if (gblLang_flag) {
        TMBUtil.DestroyForm(frmMBEnterATMPin)
    } else {
        gblLang_flag = false
    }
};

function p2kwiet2012247630369_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630369_hbxAtmPin_onClick_seq0(eventobject) {
    frmMBAssignAtmPin.txtAtmPin.setFocus(true);
};

function p2kwiet2012247630369_txtAtmPin_onTextChange_seq0(eventobject, changedtext) {
    atmPinRenderAssignAtmPin.call(this);
};

function p2kwiet2012247630369_hbxAtmPinConfirm_onClick_seq0(eventobject) {
    frmMBAssignAtmPin.txtAtmPinComfirm.setFocus(true);
};

function p2kwiet2012247630369_txtAtmPinComfirm_onTextChange_seq0(eventobject, changedtext) {
    atmPinRenderAssignAtmPin.call(this);
};

function p2kwiet2012247630369_btnCancel1_onClick_seq0(eventobject) {
    callingFrmDetailPage.call(this);
};

function p2kwiet2012247630369_btnNext_onClick_seq0(eventobject) {
    verifyPinService.call(this);
};

function p2kwiet2012247630408_frmMBBankAssuranceDetails_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630408_frmMBBankAssuranceDetails_preshow_seq0(eventobject, neworientation) {
    frmMBBankAssuranceDetailsMenuPreshow.call(this);
};

function p2kwiet2012247630408_frmMBBankAssuranceDetails_postshow_seq0(eventobject, neworientation) {
    frmMBBankAssuranceDetailsMenuPostshow.call(this);
};

function p2kwiet2012247630408_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630408_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630408_btnPayBill_onClick_seq0(eventobject) {
    moveToBillPaymentFromBAPolicyDetailsMB.call(this);
};

function p2kwiet2012247630408_btnTaxDoc_onClick_seq0(eventobject) {
    showBATaxDocEmailConfirmPopup.call(this);
};

function p2kwiet2012247630408_btnBack1_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        MBcallBAPolicyListService.call(this);
    } else {
        //kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        showLoadingScreen();
        frmMBBankAssuranceDetails.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        kony.application.dismissLoadingScreen();
    }
};

function p2kwiet2012247630439_frmMBBankAssuranceSummary_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630439_frmMBBankAssuranceSummary_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630439_frmMBBankAssuranceSummary_preshow_seq0(eventobject, neworientation) {
    frmMBBankAssuranceSummaryMenuPreshow.call(this);
};

function p2kwiet2012247630439_frmMBBankAssuranceSummary_postshow_seq0(eventobject, neworientation) {
    frmMBBankAssuranceSummaryMenuPostshow.call(this);
};

function p2kwiet2012247630439_hboxRight_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630439_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630439_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630439_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630439_hbox475124774240_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630439_segAccountDetails_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    callBAPolicyDetailsServiceMB.call(this);
};

function p2kwiet2012247630439_btnBack1_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    showAccuntSummaryScreen();
};

function p2kwiet2012247630469_frmMBBlockCardCCDBConfirm_preshow_seq0(eventobject) {
    frmMBBlockCardCCDBConfirmMenuPreshow.call(this);
};

function p2kwiet2012247630469_frmMBBlockCardCCDBConfirm_postshow_seq0(eventobject) {
    frmMBBlockCardCCDBConfirmMenuPostshow.call(this);
};

function p2kwiet2012247630469_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630469_btnchangeAdd_onClick_seq0(eventobject) {
    frmMBBlockCardCCDBConfirmOnClickBtnChangeAdd.call(this);
};

function p2kwiet2012247630469_btnnext_onClick_seq0(eventobject) {
    frmMBSuccessCardNum.show();
};

function p2kwiet2012247630488_frmMBBlockCardRecommendation_preshow_seq0(eventobject) {
    frmMBBlockCardRecommendationMenuPreshow.call(this);
};

function p2kwiet2012247630488_frmMBBlockCardRecommendation_postshow_seq0(eventobject) {
    frmMBBlockCardRecommendationMenuPostshow.call(this);
};

function p2kwiet2012247630488_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630488_btnchangeAdd_onClick_seq0(eventobject) {
    frmMBBlockCardSuccess.show();
};

function p2kwiet2012247630488_btnback_onClick_seq0(eventobject) {
    frmMBBlockCardCCDBConfirm.show();
};

function p2kwiet2012247630522_frmMBBlockCardSuccess_preshow_seq0(eventobject) {
    frmMBBlockCardSuccessMenuPreshow.call(this);
};

function p2kwiet2012247630522_frmMBBlockCardSuccess_postshow_seq0(eventobject) {
    frmMBBlockCardSuccessMenuPostshow.call(this);
};

function p2kwiet2012247630522_frmMBBlockCardSuccess_init_seq0(eventobject) {
    addblockribbon.call(this);
};

function p2kwiet2012247630522_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630531_frmMBBlockCCChangeAddress_preshow_seq0(eventobject) {
    frmMBBlockCCChangeAddressMenuPreshow.call(this);
};

function p2kwiet2012247630531_frmMBBlockCCChangeAddress_postshow_seq0(eventobject) {
    frmMBBlockCCChangeAddressMenuPostshow.call(this);
};

function p2kwiet2012247630531_flxHeader_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBBlockCCChangeAddressOnClickBtnClose.call(this);
};

function p2kwiet2012247630531_btnclose_onClick_seq0(eventobject) {
    frmMBBlockCCChangeAddressOnClickBtnClose.call(this);
};

function p2kwiet2012247630531_btnCallCenter_onClick_seq0(eventobject) {
    frmMBBlockCCChangeAddressOnClickBtnCallCenter.call(this);
};

function p2kwiet2012247630547_frmMBBlockCCReason_preshow_seq0(eventobject) {
    frmMBBlockCCReasonMenuPreshow.call(this);
};

function p2kwiet2012247630547_frmMBBlockCCReason_postshow_seq0(eventobject) {
    frmMBBlockCCReasonMenuPostshow.call(this);
};

function p2kwiet2012247630547_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630547_segReason_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onSelectReason.call(this);
};

function p2kwiet2012247630547_btnBack_onClick_seq0(eventobject) {
    frmMBBlockCCReasonOnClickBtnBack.call(this);
};

function p2kwiet2012247630571_frmMBBlockDebitCardConfirm_preshow_seq0(eventobject) {
    frmMBBlockDebitCardConfirmMenuPreshow.call(this);
};

function p2kwiet2012247630571_frmMBBlockDebitCardConfirm_postshow_seq0(eventobject) {
    frmMBBlockDebitCardConfirmMenuPostshow.call(this);
};

function p2kwiet2012247630571_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630571_btnchangeAdd_onClick_seq0(eventobject) {
    onClickBlockDebitCard.call(this);
};

function p2kwiet2012247630571_btnback_onClick_seq0(eventobject) {
    frmStart.show();
};

function p2kwiet2012247630586_frmMBCardList_preshow_seq0(eventobject) {
    frmCardListLocalChange.call(this);
};

function p2kwiet2012247630586_frmMBCardList_postshow_seq0(eventobject) {
    postShowRibbonAll.call(this);
    postShowCardList.call(this);
};

function p2kwiet2012247630586_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630597_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630597_buttonBack_onClick_seq0(eventobject) {
    onClickBackManageListCard.call(this);
};

function p2kwiet2012247630627_frmMBCardProductLink_preshow_seq0(eventobject) {
    preshowMBCardProductLink.call(this);
};

function p2kwiet2012247630627_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630627_btnCCReadMore_onClick_seq0(eventobject) {
    openURLfrmMBCardProductURL.call(this, "Credit Card");
};

function p2kwiet2012247630627_btnDCOpenAC_onClick_seq0(eventobject) {
    openURLfrmMBCardProductURL.call(this, "Open Account");
};

function p2kwiet2012247630627_btnDCReadMore_onClick_seq0(eventobject) {
    openURLfrmMBCardProductURL.call(this, "Debit Card");
};

function p2kwiet2012247630627_btnRCReadMore_onClick_seq0(eventobject) {
    openURLfrmMBCardProductURL.call(this, "Ready Cash");
};

function p2kwiet2012247630638_frmMBCardProductURL_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630638_frmMBCardProductURL_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630638_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630638_buttonBck_onClick_seq0(eventobject) {
    frmMBCardProductURL.browser506459299404741.clearHistory();
    frmMBCardProductLink.show();
};

function p2kwiet2012247630652_frmMBCashAdvAcctSelect_preshow_seq0(eventobject) {
    frmMBCashAdvAcctSelectMenuPreshow.call(this);
};

function p2kwiet2012247630652_frmMBCashAdvAcctSelect_postshow_seq0(eventobject) {
    frmMBCashAdvAcctSelectMenuPostshow.call(this);
};

function p2kwiet2012247630652_btnHeaderBack_onClick_seq0(eventobject) {
    onClickCancelOfAccountSelectScreen.call(this);
};

function p2kwiet2012247630652_segAccounts_onRowClick_seq0(eventobject, sectionNumber, rowNumber, selectedState) {
    onRowClickSegmentOfAccountSelectScreen.call(this);
};

function p2kwiet2012247630652_lblCancel_onClick_seq0(eventobject) {
    onClickCancelOfAccountSelectScreen.call(this);
};

function p2kwiet2012247630679_frmMBCashAdvanceCardInfo_preshow_seq0(eventobject) {
    frmMBCashAdvanceCardInfoMenuPreshow.call(this);
};

function p2kwiet2012247630679_frmMBCashAdvanceCardInfo_postshow_seq0(eventobject) {
    frmMBCashAdvanceCardInfoMenuPostshow.call(this);
};

function p2kwiet2012247630679_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630679_btnBack_onClick_seq0(eventobject) {
    onClickBackOfCardInfoScreen.call(this);
};

function p2kwiet2012247630679_btnNext_onClick_seq0(eventobject) {
    onClickNextOfCardInfoScreen.call(this);
};

function p2kwiet2012247630700_frmMBCashAdvanceTnC_preshow_seq0(eventobject) {
    frmMBCashAdvanceTnCMenuPreshow.call(this);
};

function p2kwiet2012247630700_frmMBCashAdvanceTnC_postshow_seq0(eventobject) {
    frmMBCashAdvanceTnCMenuPostshow.call(this);
};

function p2kwiet2012247630700_flexPdf_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.buttonPdf.skin = "btnPdfSCFoc";
    frmMBCashAdvanceTnC.flexPdf.skin = "flexLightBlueLine";
};

function p2kwiet2012247630700_flexPdf_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.flexPdf.skin = "flexWhiteBG";
    frmMBCashAdvanceTnC.buttonPdf.skin = "btnPdfSC";
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "TMBCashAdvance");
};

function p2kwiet2012247630700_buttonPdf_onClick_seq0(eventobject) {
    /* 
            var pdfFlowType = "";
       
    */
    /* 
onClickDownloadTnC.call(this,"pdf",    "TMBCashAdvance");

 */
};

function p2kwiet2012247630700_flexEmail_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.buttonEmail.skin = "btnEmailSCFoc"
    frmMBCashAdvanceTnC.flexEmail.skin = "flexLightBlueLine";
};

function p2kwiet2012247630700_flexEmail_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.flexEmail.skin = "flexWhiteBG";
    frmMBCashAdvanceTnC.buttonEmail.skin = "btnEmailSC"
    onClickEmailTnCMBCashAdvance.call(this);
};

function p2kwiet2012247630700_buttonEmail_onClick_seq0(eventobject) {
    /* 
onClickEmailTnCMBCashAdvance.call(this);

 */
};

function p2kwiet2012247630700_btnCashAdvanceBack_onClick_seq0(eventobject) {
    onClickBackOfCashAdvTncScreen.call(this);
};

function p2kwiet2012247630700_btnCashAdvanceAgree_onClick_seq0(eventobject) {
    onClickNextOfCashAdvTncScreen.call(this);
};

function p2kwiet2012247630700_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630700_btnRight_onClick_seq0(eventobject) {};

function p2kwiet2012247630763_frmMBChangePINConfirm_preshow_seq0(eventobject) {
    frmMBChangePINConfirmMenuPreshow.call(this);
};

function p2kwiet2012247630763_frmMBChangePINConfirm_postshow_seq0(eventobject) {
    frmMBChangePINConfirmMenuPostshow.call(this);
};

function p2kwiet2012247630763_frmMBChangePINConfirm_init_seq0(eventobject) {
    init_frmMBChangePINConfirm_Function.call(this);
};

function p2kwiet2012247630763_imgPin4_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    gotoChangePinSuccess.call(this);
};

function p2kwiet2012247630775_frmMBChangePinContactCenter_preshow_seq0(eventobject) {
    frmMBChangePinContactCenterPreshow.call(this);
};

function p2kwiet2012247630775_frmMBChangePinContactCenter_postshow_seq0(eventobject) {
    frmMBChangePinContactCenterPostshow.call(this);
};

function p2kwiet2012247630775_flxClose_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onClickCloseChangePINLocked.call(this);
};

function p2kwiet2012247630775_btnCallCenterNum_onClick_seq0(eventobject) {
    callNative.call(this, gblTMBContactNumber);
};

function p2kwiet2012247630833_frmMBChangePINEnterExistsPin_preshow_seq0(eventobject) {
    frmMBChangePINEnterExistsPinMenuPreshow.call(this);
};

function p2kwiet2012247630833_frmMBChangePINEnterExistsPin_postshow_seq0(eventobject) {
    frmMBChangePINEnterExistsPinPostshow.call(this);
};

function p2kwiet2012247630833_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630833_imgPin1_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onTouchPIN.call(this);
};

function p2kwiet2012247630833_imgPin2_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onTouchPIN.call(this);
};

function p2kwiet2012247630833_imgPin3_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onTouchPIN.call(this);
};

function p2kwiet2012247630833_flxpin4_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onTouchPIN.call(this);
};

function p2kwiet2012247630833_btn1_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn2_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn3_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn4_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn5_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn6_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn7_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn8_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btn9_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btnClr_onClick_seq0(eventobject) {
    clearExistChangePIN.call(this);
};

function p2kwiet2012247630833_btn0_onClick_seq0(eventobject) {
    changeBtnPinClick.call(this, eventobject);
};

function p2kwiet2012247630833_btnDel_onClick_seq0(eventobject) {
    deleteExistChangePIN.call(this);
};

function p2kwiet2012247630833_lblrequestnewpin_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onClickForgotPINToRequestNewPIN.call(this);
};

function p2kwiet2012247630833_btnCancel_onClick_seq0(eventobject) {
    btnCancelEventExistChangePin.call(this);
};

function p2kwiet2012247630847_frmMBChangePinFailure_preshow_seq0(eventobject) {
    frmMBChangePinFailurePreshow.call(this);
};

function p2kwiet2012247630847_frmMBChangePinFailure_postshow_seq0(eventobject) {
    frmMBChangePinFailurePostshow.call(this);
};

function p2kwiet2012247630847_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630847_lblCallcenternum_onClick_seq0(eventobject) {
    callNative.call(this, gblTMBContactNumber);
};

function p2kwiet2012247630847_lblManageCards_onClick_seq0(eventobject) {
    onClickManageOfChangePINSuccess.call(this);
};

function p2kwiet2012247630865_frmMBChangePinSuccess_preshow_seq0(eventobject) {
    frmMBChangePinSuccessMenuPreshow.call(this);
};

function p2kwiet2012247630865_frmMBChangePinSuccess_postshow_seq0(eventobject) {
    frmMBChangePinSuccessPostshow.call(this);
};

function p2kwiet2012247630865_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630865_flxCardDetails_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onClickManageThisCardOfChangePINSuccess.call(this);
};

function p2kwiet2012247630865_flxManageThisCard_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onClickManageThisCardOfChangePINSuccess.call(this);
};

function p2kwiet2012247630865_lblManageOtherCards_onClick_seq0(eventobject) {
    onClickManageOfChangePINSuccess.call(this);
};

function p2kwiet2012247630915_frmMBEnterATMPin_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630915_frmMBEnterATMPin_preshow_seq0(eventobject, neworientation) {
    frmMBEnterATMPinMenuPreshow.call(this);
};

function p2kwiet2012247630915_frmMBEnterATMPin_postshow_seq0(eventobject, neworientation) {
    frmMBEnterATMPinMenuPostshow.call(this);
};

function p2kwiet2012247630915_frmMBEnterATMPin_onhide_seq0(eventobject, neworientation) {
    if (gblLang_flag) {
        TMBUtil.DestroyForm(frmMBEnterATMPin)
    } else {
        gblLang_flag = false
    }
};

function p2kwiet2012247630915_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630915_button1495287783233617_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button1495287783233618_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button1495287783233619_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button363582120224309_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button363582120224307_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button363582120224311_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button363582120225205_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button363582120225203_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button363582120225207_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button1495287783233622_onClick_seq0(eventobject) {
    clearVerifyPinMB.call(this);
};

function p2kwiet2012247630915_button1495287783233621_onClick_seq0(eventobject) {
    verifyPinRenderMB.call(this, eventobject["text"]);
};

function p2kwiet2012247630915_button1495287783233620_onClick_seq0(eventobject) {
    deleteVerifyPinMB.call(this);
};

function p2kwiet2012247630915_btnCancel1_onClick_seq0(eventobject) {
    ActivationMBViaIBLogoutService.call(this);
};

function p2kwiet2012247630915_btnNext_onClick_seq0(eventobject) {
    mbValidateAtmPin.call(this);
};

function p2kwiet2012247630942_frmMBEStatementComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630942_frmMBEStatementComplete_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630942_frmMBEStatementComplete_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630942_frmMBEStatementComplete_preshow_seq0(eventobject, neworientation) {
    frmMBEStatementCompleteMenuPreshow.call(this);
};

function p2kwiet2012247630942_frmMBEStatementComplete_postshow_seq0(eventobject, neworientation) {
    frmMBEStatementCompleteMenuPostshow.call(this);
};

function p2kwiet2012247630942_frmMBEStatementComplete_onhide_seq0(eventobject, neworientation) {
    frmMBEStatementComplete.imgHeaderMiddle1.src = "arrowtop.png";
    frmMBEStatementComplete.imgHeaderRight1.src = "empty.png";
};

function p2kwiet2012247630942_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630942_button474136157108682_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630942_btnEStatementReturn_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247630942_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247630976_frmMBEStatementConfirmation_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630976_frmMBEStatementConfirmation_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630976_frmMBEStatementConfirmation_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247630976_frmMBEStatementConfirmation_preshow_seq0(eventobject, neworientation) {
    frmMBEStatementConfirmationMenuPreshow.call(this);
};

function p2kwiet2012247630976_frmMBEStatementConfirmation_postshow_seq0(eventobject, neworientation) {
    frmMBEStatementConfirmationMenuPostshow.call(this);
};

function p2kwiet2012247630976_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247630976_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247630976_btnRight_onClick_seq0(eventobject) {
    gblFromEStatementEdit = true;
    frmMBEStatementLanding.show();
};

function p2kwiet2012247630976_btnCancel_onClick_seq0(eventobject) {
    onClickBackEStatement.call(this);
};

function p2kwiet2012247630976_btnNext_onClick_seq0(eventobject) {
    onClickEStatementConfirmationNext.call(this);
};

function p2kwiet2012247631019_frmMBEStatementLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631019_frmMBEStatementLanding_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631019_frmMBEStatementLanding_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631019_frmMBEStatementLanding_preshow_seq0(eventobject, neworientation) {
    frmMBEStatementLandingMenuPreshow.call(this);
};

function p2kwiet2012247631019_frmMBEStatementLanding_postshow_seq0(eventobject, neworientation) {
    frmMBEStatementLandingMenuPostshow.call(this);
};

function p2kwiet2012247631019_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631019_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631019_txtEStatementEmail_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    checkEmailFormatEstatement.call(this);
};

function p2kwiet2012247631019_txtEStatementEmail_SPA_onEndEditing_seq0(eventobject, changedtext) {
    checkEmailFormatEstatement.call(this);
};

function p2kwiet2012247631019_txtEStatementEmail_Android_onEndEditing_seq0(eventobject, changedtext) {
    checkEmailFormatEstatement.call(this);
};

function p2kwiet2012247631019_txtEStatementEmail_onDone_seq0(eventobject, changedtext) {};

function p2kwiet2012247631019_txtEStatementEmail_onTextChange_seq0(eventobject, changedtext) {
    checkNextButtonEnableEStatementLanding.call(this, "txtEmail");
};

function p2kwiet2012247631019_btnPaperOption_onClick_seq0(eventobject, context) {
    onClickEStmtEmailPaperOption.call(this, "btnPaperOption");
};

function p2kwiet2012247631019_btnEmailOption_onClick_seq0(eventobject, context) {
    onClickEStmtEmailPaperOption.call(this, "btnEmailOption");
};

function p2kwiet2012247631019_btnRegisterAddr_onClick_seq0(eventobject, context) {
    onClickEStmtChooseAddressButton.call(this, "btnRegisterAddr");
};

function p2kwiet2012247631019_btnOfficeAddr_onClick_seq0(eventobject, context) {
    onClickEStmtChooseAddressButton.call(this, "btnOfficeAddr");
};

function p2kwiet2012247631019_btnContactAddr_onClick_seq0(eventobject, context) {
    onClickEStmtChooseAddressButton.call(this, "btnContactAddr");
};

function p2kwiet2012247631019_btnCancel_onClick_seq0(eventobject) {
    onClickBackEStatement.call(this);
};

function p2kwiet2012247631019_btnNext_onClick_seq0(eventobject) {
    onClickEStatementLandingNext.call(this);
};

function p2kwiet2012247631053_frmMBEStatementProdFeature_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631053_frmMBEStatementProdFeature_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631053_frmMBEStatementProdFeature_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631053_frmMBEStatementProdFeature_preshow_seq0(eventobject, neworientation) {
    frmMBEStatementProdFeatureMenuPreshow.call(this);
};

function p2kwiet2012247631053_frmMBEStatementProdFeature_postshow_seq0(eventobject, neworientation) {
    frmMBEStatementProdFeatureMenuPostshow.call(this);
};

function p2kwiet2012247631053_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631053_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631053_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickBackEStatement.call(this);
    } else {
        frmMBEStatementProdFeature.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247631053_btnNext_onClick_seq0(eventobject) {
    /* 
frmMBEStatementTnC.show();
	
 */
    onClickEstatementProducts.call(this);
};

function p2kwiet2012247631081_frmMBEStatementTnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631081_frmMBEStatementTnC_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631081_frmMBEStatementTnC_preshow_seq0(eventobject, neworientation) {
    frmMBEStatementTnCMenuPreshow.call(this);
};

function p2kwiet2012247631081_frmMBEStatementTnC_postshow_seq0(eventobject, neworientation) {
    frmMBEStatementTnCMenuPostshow.call(this);
};

function p2kwiet2012247631081_frmMBEStatementTnC_onhide_seq0(eventobject, neworientation) {
    frmMBEStatementTnC.hboxSaveCamEmail.isVisible = false;
    frmMBEStatementTnC.imgHeaderMiddle.src = "arrowtop.png"
    frmMBEStatementTnC.imgHeaderRight.src = "empty.png"
    frmMBEStatementTnC.btnRight.skin = "btnShare";
};

function p2kwiet2012247631081_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631081_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631081_btnRight_onClick_seq0(eventobject) {
    var skin = frmMBEStatementTnC.btnRight.skin
    if (frmMBEStatementTnC.hboxSaveCamEmail.isVisible == false) {
        frmMBEStatementTnC.hboxSaveCamEmail.isVisible = true;
        frmMBEStatementTnC.imgHeaderRight.src = "arrowtop.png"
        frmMBEStatementTnC.imgHeaderMiddle.src = "empty.png"
        frmMBEStatementTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmMBEStatementTnC.hboxSaveCamEmail.isVisible = false;
        frmMBEStatementTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmMBEStatementTnC.imgHeaderRight.src = "empty.png"
        frmMBEStatementTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247631081_button47505874736294_onClick_seq0(eventobject) {
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "TMBEStatement");
};

function p2kwiet2012247631081_button47505874736298_onClick_seq0(eventobject) {
    onClickEmailTnCMBEStatement.call(this);
};

function p2kwiet2012247631081_btnPointCancel_onClick_seq0(eventobject) {
    frmMBEStatementProdFeature.show();
};

function p2kwiet2012247631081_btnAgree_onClick_seq0(eventobject) {
    onClickNextTnCEStatement.call(this);
};

function p2kwiet2012247631115_frmMBForgotPin_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631115_frmMBForgotPin_preshow_seq0(eventobject, neworientation) {
    frmMBForgotPinMenuPreshow.call(this);
};

function p2kwiet2012247631115_frmMBForgotPin_postshow_seq0(eventobject, neworientation) {
    frmMBForgotPinMenuPostshow.call(this);
};

function p2kwiet2012247631115_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631115_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631115_btnMsgCall_onClick_seq0(eventobject) {
    CallTheNumber.call(this);
};

function p2kwiet2012247631115_botnBack_onClick_seq0(eventobject) {
    /* 
onClickOfBack.call(this);

 */
    frmMBPreLoginAccessesPin.show();
};

function p2kwiet2012247631115_btnReactivate_onClick_seq0(eventobject) {
    frmMBanking.show();
};

function p2kwiet2012247631214_frmMBFTEdit_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631214_frmMBFTEdit_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631214_frmMBFTEdit_preshow_seq0(eventobject, neworientation) {
    frmMBFTEditMenuPreshow.call(this);
};

function p2kwiet2012247631214_frmMBFTEdit_postshow_seq0(eventobject, neworientation) {
    frmMBFTEditMenuPostshow.call(this);
};

function p2kwiet2012247631214_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631214_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631214_txtEditAmnt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    if (frmMBFTEdit.txtEditAmnt.text == "") {
        frmMBFTEdit.txtEditAmnt.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmMBFTEdit.txtEditAmnt.text = commaFormatted(parseFloat(removeCommos(frmMBFTEdit.txtEditAmnt.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
};

function p2kwiet2012247631214_txtEditAmnt_SPA_onEndEditing_seq0(eventobject, changedtext) {
    /* 
if(frmMBFTEdit.txtEditAmnt.text == ""){
frmMBFTEdit.txtEditAmnt.text = "0.00"+ kony.i18n.getLocalizedString("currencyThaiBaht");
}else {
frmMBFTEdit.txtEditAmnt.text = commaFormatted(parseFloat(removeCommos(frmMBFTEdit.txtEditAmnt.text)).toFixed(2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
}

 */
};

function p2kwiet2012247631214_txtEditAmnt_Android_onEndEditing_seq0(eventobject, changedtext) {
    /* 
if(frmMBFTEdit.txtEditAmnt.text == ""){
frmMBFTEdit.txtEditAmnt.text = "0.00"+ kony.i18n.getLocalizedString("currencyThaiBaht");
}else {
frmMBFTEdit.txtEditAmnt.text = commaFormatted(parseFloat(removeCommos(frmMBFTEdit.txtEditAmnt.text)).toFixed(2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
}

 */
    ehFrmMBFTEdit_txtEditAmnt_onEndEditing.call(this, eventobject, changedtext);
};

function p2kwiet2012247631214_txtEditAmnt_onTextChange_seq0(eventobject, changedtext) {
    formatAmountOnTextChangeFTEdit.call(this);
};

function p2kwiet2012247631214_btnFTEdit_onClick_seq0(eventobject) {
    /* 

frmMBFtSchedule.calScheduleStartDate.dateComponents = dateFormatForDateComp(frmMBFTEdit.lblViewStartOnDateVal.text);
 

if(gblScheduleRepeatFTMB == "Once"){
 frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(false);
 frmMBFtSchedule.lblEnd.setVisibility(false);
 
 frmMBFtSchedule.btnDaily.skin = btnScheduleEndLeft
 frmMBFtSchedule.btnWeekly.skin = btnScheduleEndMid
 frmMBFtSchedule.btnMonthly.skin = btnScheduleEndMid
 frmMBFtSchedule.btnYearly.skin = btnScheduleEndRight

 frmMBFtSchedule.btnNever.skin = btnScheduleEndLeft
 frmMBFtSchedule.btnAfter.skin = btnScheduleEndMid
 frmMBFtSchedule.btnOnDate.skin = btnScheduleEndRight
 
 frmMBFtSchedule.lblEnd.setVisibility(false);
 frmMBFtSchedule.lblEndAfter.setVisibility(false);
 frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
 frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
 frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
 frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
 frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
 frmMBFtSchedule.hbxEndAfter.setVisibility(false);
 frmMBFtSchedule.hbxEndOnDate.setVisibility(false);
 frmMBFtSchedule.lblRepeat.setVisibility(true);
 
 frmMBFtSchedule.tbxAfterTimes.text = ""
  gblEndValTempMB = "none";
 
}

gblTempStartOnDateMB = frmMBFTEdit.lblViewStartOnDateVal.text;
gblTempEndOnDateMB = frmMBFTEdit.lblEndOnDateVal.text;
gblTempExcuteValMB = frmMBFTEdit.lblExcuteVal.text







 */
    /* 
showEditScheduleMB.call(this);

 */
    ehFrmMBFTEdit_btnFTEdit_onClick.call(this, eventobject);
};

function p2kwiet2012247631214_btnCancelSPA_onClick_seq0(eventobject) {
    /* 
frmMBFTView.lblViewStartOnDateVal.text = formatDateFT(gblFTViewStartOnDateMB);
frmMBFTView.lblRepeatAsVal.text = gblFTViewRepeatAsValMB;

var tmpEndDate = gblFTViewEndDateMB;

if(gblFTViewEndDateMB == "-"){
 frmMBFTView.lblEndOnDateVal.text = "-";

}else{
 if(tmpEndDate.indexOf("-", 0) != -1){
     tmpEndDate = formatDateFT(gblFTViewEndDateMB);
 }else
    frmMBFTView.lblEndOnDateVal.text = tmpEndDate;
}
  
frmMBFTView.lblExcuteVal.text = gblFTViewExcuteValMB
frmMBFTView.lblRemainingVal.text = gblFTViewExcuteremaingMB

gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;

gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
gblScheduleEndFTMB = gblFTViewEndValMB;
gblEditFTSchduleMB = false;


kony.print("gblIsEditAftrMB: "+gblIsEditAftrMB+"  gblIsEditOnDateMB: "+gblIsEditOnDateMB);








 */
    /* 
gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;

gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
gblRepeatAsCopy=gblFTViewRepeatAsValMB
gblScheduleEndFTMB = gblFTViewEndValMB;
gblEditFTSchduleMB = false;


kony.print("gblIsEditAftrMB: "+gblIsEditAftrMB+"  gblIsEditOnDateMB: "+gblIsEditOnDateMB);








 */
    /* 
frmMBFTView.show();
	
 */
    ehFfrmMBFTEdit_btnCancelSPA_onClick.call(this, eventobject);
};

function p2kwiet2012247631214_btnConfirmSPA_onClick_seq0(eventobject) {
    isEditScheduleMB.call(this);
};

function p2kwiet2012247631214_btnCancel_onClick_seq0(eventobject) {
    /* 
frmMBFTView.lblViewStartOnDateVal.text = formatDateFTMB(gblFTViewStartOnDateMB);
frmMBFTView.lblRepeatAsVal.text = gblFTViewRepeatAsValMB;

var tmpEndDate = gblFTViewEndDateMB;

if(gblFTViewEndDateMB == "-"){
 frmMBFTView.lblEndOnDateVal.text = "-";

}else{
 if(tmpEndDate.indexOf("-", 0) != -1){
     tmpEndDate = formatDateFTMB(gblFTViewEndDateMB);
     frmMBFTView.lblEndOnDateVal.text = tmpEndDate;
 }else
    frmMBFTView.lblEndOnDateVal.text = tmpEndDate;
}
  
frmMBFTView.lblExcuteVal.text = gblFTViewExcuteValMB
frmMBFTView.lblRemainingVal.text = gblFTViewExcuteremaingMB

gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;

gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
gblScheduleEndFTMB = gblFTViewEndValMB;
gblEditFTSchduleMB = false;


kony.print("gblIsEditAftrMB: "+gblIsEditAftrMB+"  gblIsEditOnDateMB: "+gblIsEditOnDateMB);








 */
    /* 
gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;
gblRepeatAsCopy=gblFTViewRepeatAsValMB
gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
gblScheduleEndFTMB = gblFTViewEndValMB;
gblEditFTSchduleMB = false;


kony.print("gblIsEditAftrMB: "+gblIsEditAftrMB+"  gblIsEditOnDateMB: "+gblIsEditOnDateMB);








 */
    /* 
frmMBFTView.show();
	
 */
    ehFrmMBFTEdit_btnCancel_onClick.call(this, eventobject);
};

function p2kwiet2012247631214_btnConfirm_onClick_seq0(eventobject) {
    isEditScheduleMB.call(this);
};

function p2kwiet2012247631318_frmMBFTEditCmplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631318_frmMBFTEditCmplete_preshow_seq0(eventobject, neworientation) {
    frmMBFTEditCmpleteMenuPreshow.call(this);
};

function p2kwiet2012247631318_frmMBFTEditCmplete_postshow_seq0(eventobject, neworientation) {
    frmMBFTEditCmpleteMenuPostshow.call(this);
};

function p2kwiet2012247631318_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631318_btnRight_onClick_seq0(eventobject) {
    /* 
//frmMBFTEditCmplete.hboxSharelist.setVisibility(true)

if(frmMBFTEditCmplete.hboxSharelist.isVisible){
  frmMBFTEditCmplete.hboxSharelist.setVisibility(false)
}else
   frmMBFTEditCmplete.hboxSharelist.setVisibility(true) 



 */
    ehFrmMBFTEditCmplete_btnRight_onClick.call(this);
};

function p2kwiet2012247631318_button47428498625_onClick_seq0(eventobject) {
    saveAsPDFFutureTransferMB.call(this, "pdf");
};

function p2kwiet2012247631318_btnPhoto_onClick_seq0(eventobject) {
    saveAsPDFFutureTransferMB.call(this, "png");
};

function p2kwiet2012247631318_btnEmailto_onClick_seq0(eventobject) {
    /* 
gblPOWcustNME = frmMBFTEditCmplete.lblFrmAccntNickName.text
gblPOWtransXN = "EditFutureTransfer"
gblPOWchannel = "MB"
postOnWall()
requestFromForm= "frmMBFTEditCmplete"

 */
    ehFrmMBFTEditCmplete_btnEmailto_onClick.call(this);
};

function p2kwiet2012247631318_hbox47792425956437_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        handleMenuBtn.call(this);
    } else {
        onClickForInnerBoxes.call(this);
    }
};

function p2kwiet2012247631318_btnB4T_onClick_seq0(eventobject) {
    /* 
if(varAvailBal == false){
frmMBFTEditCmplete.lblFTEditAftrAvailBal.isVisible = true;
frmMBFTEditCmplete.lblFTEditAftrAvailBalVal.isVisible = true;
varAvailBal = true;
frmMBFTEditCmplete.lblHide.text = "Hide";

}else if(varAvailBal == true){
frmMBFTEditCmplete.lblFTEditAftrAvailBal.isVisible = false;
frmMBFTEditCmplete.lblFTEditAftrAvailBalVal.isVisible = false;
varAvailBal = false;
frmMBFTEditCmplete.lblHide.text = "Show";
}





 */
    ehFrmMBFTEditCmplete_btnB4T_onClick.call(this);
};

function p2kwiet2012247631318_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247631318_btnReturnSPA_onClick_seq0(eventobject) {
    /* 
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate,frmMBMyActivities,1);
    */
    MBMyActivitiesReloadAndShowCalendar();
    /* 
ehFrmMBFTEditCmplete_btnReturnSPA_onClick.call(this);

 */
};

function p2kwiet2012247631318_btnReturn1_onClick_seq0(eventobject) {
    MBMyActivitiesReloadAndShowCalendar.call(this);
};

function p2kwiet2012247631416_frmMBFTEditCnfrmtn_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631416_frmMBFTEditCnfrmtn_preshow_seq0(eventobject, neworientation) {
    frmMBFTEditCnfrmtnMenuPreshow.call(this);
};

function p2kwiet2012247631416_frmMBFTEditCnfrmtn_postshow_seq0(eventobject, neworientation) {
    frmMBFTEditCnfrmtnMenuPostshow.call(this);
};

function p2kwiet2012247631416_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631416_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631416_button445536670970293_onClick_seq0(eventobject) {
    /* 
//frmMBFTEdit.lblToAccntName.text = gblAccntName_ORFT_MB
frmMBFTEdit.lblFeeVal.text = frmMBFTEditCnfrmtn.lblFeeVal.text;

 */
    /* 
frmMBFTEdit.show();
	
 */
    ehFrmMBFTEditCnfrmtn_button445536670970293_onClick.call(this);
};

function p2kwiet2012247631416_btnCancelSPA_onClick_seq0(eventobject) {
    /* 
gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;
gblRepeatAsCopy=gblFTViewRepeatAsValMB

gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
gblScheduleEndFTMB = gblFTViewEndValMB;
gblEditFTSchduleMB = false;



 */
    /* 
frmMBFTView.show();
	
 */
    ehFrmMBFTEditCnfrmtn_btnCancelSPA_onClick.call(this);
};

function p2kwiet2012247631416_btnConfirmSPA_onClick_seq0(eventobject) {
    /* 
onEditFtConfirm.call(this);

 */
    /* 
frmMBFTEditCmplete.hboxSharelist.setVisibility(false)

 */
    /* 
completeProcessMB.call(this);

 */
    ehFrmMBFTEditCnfrmtn_btnConfirmSPA_onClick.call(this);
};

function p2kwiet2012247631416_btnCancel_onClick_seq0(eventobject) {
    /* 
frmMBFTView.lblViewStartOnDateVal.text = formatDateFT(gblFTViewStartOnDateMB);
frmMBFTView.lblRepeatAsVal.text = gblFTViewRepeatAsValMB;

var tmpEndDate = gblFTViewEndDateMB;

if(gblFTViewEndDateMB == "-"){
 frmMBFTView.lblEndOnDateVal.text = "-";
}else{
 if(tmpEndDate.indexOf("-", 0) != -1){
  tmpEndDate = formatDateFT(gblFTViewEndDateMB);
 }else
 frmMBFTView.lblEndOnDateVal.text = tmpEndDate;
}
 
frmMBFTView.lblExcuteVal.text = gblFTViewExcuteValMB;
frmMBFTView.lblRemainingVal.text = gblFTViewExcuteremaingMB;

gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;

gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
gblScheduleEndFTMB = gblFTViewEndValMB;
gblEditFTSchduleMB = false;







 */
    /* 
gblIsEditAftrMB = gblIsEditAftrFrmServiceMB;
gblIsEditOnDateMB = gblIsEditOnDateFrmServiceMB;
gblRepeatAsCopy=gblFTViewRepeatAsValMB
gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;
gblScheduleEndFTMB = gblFTViewEndValMB;
gblEditFTSchduleMB = false;



 */
    /* 
frmMBFTView.show();
	
 */
    ehFrmMBFTEditCnfrmtn_btnCancel_onClick.call(this);
};

function p2kwiet2012247631416_btnConfirm_onClick_seq0(eventobject) {
    onEditFtConfirm.call(this);
    /* 
if (gblEditFTSchduleMB == true) {
     srvFutureTransferDeleteMB();
} else {
     srvFutureTransferUpdateMB();
}


frmMBFTEditCmplete.hboxSharelist.setVisibility(false)

 */
    /* 
completeProcessMB.call(this);

 */
};

function p2kwiet2012247631467_frmMBFtSchedule_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631467_frmMBFtSchedule_preshow_seq0(eventobject, neworientation) {
    frmMBFtScheduleMenuPreshow.call(this);
};

function p2kwiet2012247631467_frmMBFtSchedule_postshow_seq0(eventobject, neworientation) {
    /* 
       

 /* 
commonMBPostShow.call(this);

 */
    * / 
    /* 
frmMBFtSchedule.lblEndAfter.setVisibility(false);

 */
    ehFrmMBFtSchedule_frmMBFtSchedule_postshow.call(this);
};

function p2kwiet2012247631467_btnDaily_onClick_seq0(eventobject) {
    /* 
repeatScheduleButtonSet.call(this,eventobject);

 */
    /* 
frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);

frmMBFtSchedule.btnDaily.skin = btnScheduleEndLeftFocus

frmMBFtSchedule.btnWeekly.skin = btnScheduleEndMid

frmMBFtSchedule.btnMonthly.skin = btnScheduleEndMid

frmMBFtSchedule.btnYearly.skin = btnScheduleEndRight

gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyDaily");

if(gblScheduleEndFTMB == "none"){
  frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
  frmMBFtSchedule.lblEnd.setVisibility(true);
}

 */
    ehFrmMBFtSchedule_btnDaily_onClick.call(this);
};

function p2kwiet2012247631467_btnWeekly_onClick_seq0(eventobject) {
    /* 
repeatScheduleButtonSet.call(this,eventobject);

 */
    /* 
frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);

frmMBFtSchedule.btnDaily.skin = btnScheduleEndLeft

frmMBFtSchedule.btnWeekly.skin = btnScheduleEndMidFocus

frmMBFtSchedule.btnMonthly.skin = btnScheduleEndMid

frmMBFtSchedule.btnYearly.skin = btnScheduleEndRight

gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyWeekly");

if(gblScheduleEndFTMB == "none"){
  frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
  frmMBFtSchedule.lblEnd.setVisibility(true);
}

 */
    ehFrmMBFtSchedule_btnWeekly_onClick.call(this);
};

function p2kwiet2012247631467_btnMonthly_onClick_seq0(eventobject) {
    /* 
frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);

frmMBFtSchedule.btnDaily.skin = btnScheduleEndLeft

frmMBFtSchedule.btnWeekly.skin = btnScheduleEndMid

frmMBFtSchedule.btnMonthly.skin = btnScheduleEndMidFocus

frmMBFtSchedule.btnYearly.skin = btnScheduleEndRight

gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyMonthly");

if(gblScheduleEndFTMB == "none"){
  frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
  frmMBFtSchedule.lblEnd.setVisibility(true);
}

 */
    ehFrmMBFtSchedule_btnMonthly_onClick.call(this);
};

function p2kwiet2012247631467_btnYearly_onClick_seq0(eventobject) {
    /* 
frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);

frmMBFtSchedule.btnDaily.skin = btnScheduleEndLeft

frmMBFtSchedule.btnWeekly.skin = btnScheduleEndMid

frmMBFtSchedule.btnMonthly.skin = btnScheduleEndMid

frmMBFtSchedule.btnYearly.skin = btnScheduleRightFocus

gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyYearly");

if(gblScheduleEndFTMB == "none"){
  frmMBFtSchedule.hbxEndAfterButtonHolder.setVisibility(true);
  frmMBFtSchedule.lblEnd.setVisibility(true);
}

 */
    ehFrmMBFtSchedule_btnYearly_onClick.call(this);
};

function p2kwiet2012247631467_btnNever_onClick_seq0(eventobject) {
    /* 
endScheduleButtonSet.call(this,eventobject);

 */
    /* 
frmMBFtSchedule.lblEndAfter.setVisibility(false);
frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);

frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);

gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyNever");



frmMBFtSchedule.btnNever.skin = btnScheduleEndLeftFocus
frmMBFtSchedule.btnAfter.skin = btnScheduleEndMid
frmMBFtSchedule.btnOnDate.skin = btnScheduleEndRight

 */
    ehFrmMBFtSchedule_btnNever_onClick.call(this);
};

function p2kwiet2012247631467_btnAfter_onClick_seq0(eventobject) {
    /* 
endScheduleButtonSet.call(this,eventobject);

 */
    /* 
frmMBFtSchedule.lblEndAfter.setVisibility(false);
frmMBFtSchedule.hbxNumOfTimes.setVisibility(true);
frmMBFtSchedule.lineAfterTxtbx.setVisibility(true);
frmMBFtSchedule.lblNumberOfTimes.setVisibility(true)
frmMBFtSchedule.hbxEndDateSelectn.setVisibility(false);
frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(false);
frmMBFtSchedule.hbxEndAfter.setVisibility(true);

frmMBFtSchedule.btnNever.skin = btnScheduleEndLeft
frmMBFtSchedule.btnAfter.skin = btnScheduleEndMidFocus
frmMBFtSchedule.btnOnDate.skin = btnScheduleEndRight

gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyAfter");
//btnScheduleRightFocus

if(frmMBFTEdit.lblExcuteVal.text = "-"){
 frmMBFtSchedule.tbxAfterTimes.text = "";

}

frmMBFtSchedule.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;



 */
    ehFrmMBFtSchedule_btnAfter_onClick.call(this);
};

function p2kwiet2012247631467_btnOnDate_onClick_seq0(eventobject) {
    /* 
endScheduleButtonSet.call(this,eventobject);

 */
    /* 
frmMBFtSchedule.lblEndAfter.setVisibility(false);
frmMBFtSchedule.hbxNumOfTimes.setVisibility(false);
frmMBFtSchedule.lineAfterTxtbx.setVisibility(false);
frmMBFtSchedule.lblNumberOfTimes.setVisibility(false)
frmMBFtSchedule.hbxEndDateSelectn.setVisibility(true);
frmMBFtSchedule.lineAfterEndDateSeletn.setVisibility(true);
 frmMBFtSchedule.hbxEndOnDate.setVisibility(true);

gblScheduleEndFTMB = kony.i18n.getLocalizedString("keyOnDate");

frmMBFtSchedule.btnNever.skin = btnScheduleEndLeft
frmMBFtSchedule.btnAfter.skin = btnScheduleEndMid
frmMBFtSchedule.btnOnDate.skin = btnScheduleRightFocus;

if(frmMBFTEdit.lblEndOnDateVal.text == "-"){
  var i =  frmMBFtSchedule.calScheduleStartDate.dateComponents[0] *1
  i = i+1;
  frmMBFtSchedule.calScheduleEndDate.dateComponents = [i,frmMBFtSchedule.calScheduleStartDate.dateComponents[1],frmMBFtSchedule.calScheduleStartDate.dateComponents[2]]

}else{
 var tempdate = frmMBFTEdit.lblEndOnDateVal.text;
 var d = tempdate.split("/");

 frmMBFtSchedule.calScheduleEndDate.dateComponents = [d[0],d[1],d[2]]

}


 */
    ehFrmMBFtSchedule_btnOnDate_onClick.call(this);
};

function p2kwiet2012247631467_btnCancel_onClick_seq0(eventobject) {
    /* 
gblScheduleEndFTMB = gblEndValTempMB;

gblScheduleRepeatFTMB = gblFTViewRepeatAsValMB;

if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyDaily"))
 gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyDaily");
else if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyWeekly"))
 gblScheduleRepeatFTMB = kony.i18n.getLocalizedString("keyWeekly");
else if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyMonthly"))
 gblScheduleRepeatFTMB  = kony.i18n.getLocalizedString("keyMonthly");
else if(frmMBFTEdit.lblRepeatAsVal.text == kony.i18n.getLocalizedString("keyYearly"))
  gblScheduleRepeatFTMB  = kony.i18n.getLocalizedString("keyYearly");
else if(gblScheduleRepeatFTMB == "Once")
    frmMBFTEdit.lblRepeatAsVal.text = "Once";
 
frmMBFTEdit.lblViewStartOnDateVal.text = gblTempStartOnDateMB
frmMBFTEdit.lblEndOnDateVal.text = gblTempEndOnDateMB
frmMBFTEdit.lblExcuteVal.text = gblTempExcuteValMB

 */
    /* 
frmMBFTEdit.show();
	
 */
    ehFrmMBFtSchedule_btnCancel_onClick.call(this);
};

function p2kwiet2012247631467_btnSave_onClick_seq0(eventobject) {
    /* 
if(gblFTViewRepeatAsValMB == gblScheduleRepeatFTMB)
    gblEditFTSchduleMB = true;
    
var d1 = frmMBFTEdit.lblViewStartOnDateVal.text;
var d2 = frmMBFtSchedule.calScheduleStartDate.formattedDate

if(parseDateIB(d1) != parseDateIB(d1)){
 gblEditFTSchduleMB = true;
}

if(gblScheduleEndFTMB != gblEndValTempMB){
 
   if(gblScheduleEndFTMB == "none" && gblEndValTempMB == kony.i18n.getLocalizedString("keyAfter")){
     if(frmMBFTEdit.lblExcuteVal.text != frmMBFtSchedule.tbxAfterTimes.text)
           gblEditFTSchduleMB = true;
     
   }
   if(gblScheduleEndFTMB == "none"  && gblEndValTempMB == kony.i18n.getLocalizedString("keyOnDate")){
    var d1 = frmMBFTEdit.lblEndOnDateVal.text;
    var d2 = frmMBFtSchedule.calScheduleEndDate.formattedDate
    if(parseDateIB(d1) != parseDateIB(d2))
         gblEditFTSchduleMB = true;
  }

}else{
  if(gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyAfter")){
     if(frmMBFTEdit.lblExcuteVal.text != frmMBFtSchedule.tbxAfterTimes.text)
           gblEditFTSchduleMB = true;
   
  }
  if(gblScheduleEndFTMB == kony.i18n.getLocalizedString("keyOnDate")){
    var d1 = frmMBFTEdit.lblEndOnDateVal.text;
    var d2 = frmMBFtSchedule.calScheduleEndDate.formattedDate
    if(parseDateIB(d1) != parseDateIB(d2))
         gblEditFTSchduleMB = true;
      
  }
  

}

kony.print("----gblEditFTSchduleMB---: "+gblEditFTSchduleMB);
  






 */
    /* 
validateSchduleMB.call(this);

 */
    ehFrmMBFtSchedule_btnSave_onClick.call(this);
};

function p2kwiet2012247631565_frmMBFTView_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631565_frmMBFTView_preshow_seq0(eventobject, neworientation) {
    frmMBFTViewMenuPreshow.call(this);
};

function p2kwiet2012247631565_frmMBFTView_postshow_seq0(eventobject, neworientation) {
    frmMBFTViewMenuPostshow.call(this);
};

function p2kwiet2012247631565_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631565_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631565_btnFTEditFlow_onClick_seq0(eventobject) {
    /* 
frmMBFTEdit.txtEditAmnt.setFocus(true);
gblCRMProfileInqCalMB = true;

 */
    /* 
getFeeCrmProfileInqMB.call(this);

 */
    ehFrmMBFTView_btnFTEditFlow_onClick.call(this);
};

function p2kwiet2012247631565_btnFTDelFlow_onClick_seq0(eventobject) {
    srvCrmProfileInqforFTCancelMB.call(this);
    /* 
activityLogCancelFTMB.call(this);

 */
};

function p2kwiet2012247631565_btnConfirmSPA_onClick_seq0(eventobject) {
    /*
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate,frmMBMyActivities);
    */
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247631565_btnConfirm_onClick_seq0(eventobject) {
    /*
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate,frmMBMyActivities);
    */
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247631583_frmMBListCreditCard_preshow_seq0(eventobject) {
    frmCreditCardListLocalChange.call(this);
};

function p2kwiet2012247631583_frmMBListCreditCard_postshow_seq0(eventobject) {
    postShowCreditCardRibbon.call(this);
};

function p2kwiet2012247631583_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631583_buttonBack_onClick_seq0(eventobject) {
    onClickOfAccountDetailsBack.call(this);
};

function p2kwiet2012247631604_frmMBListDebitCard_preshow_seq0(eventobject) {
    frmDebitCardListLocalChange.call(this);
};

function p2kwiet2012247631604_frmMBListDebitCard_postshow_seq0(eventobject) {
    postShowDebitCardRibbon.call(this);
    postShowCardList.call(this);
};

function p2kwiet2012247631604_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631604_btnIssueCard_onClick_seq0(eventobject) {
    frmMBNewTncNewDCShow.call(this);
};

function p2kwiet2012247631604_buttonBack_onClick_seq0(eventobject) {
    onClickBackOfDebitCardScreen.call(this);
};

function p2kwiet2012247631642_frmMBManageCard_preshow_seq0(eventobject) {
    frmMBManageCardMenuPreshow.call(this);
};

function p2kwiet2012247631642_frmMBManageCard_postshow_seq0(eventobject) {
    frmMBManageCardMenuPostshow.call(this);
};

function p2kwiet2012247631642_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631642_btnBlockCard_onClick_seq0(eventobject) {
    frmMBManageCardFlxBlockCardOnClick.call(this);
};

function p2kwiet2012247631642_btnChangePin_onClick_seq0(eventobject) {
    onClickChangePin.call(this);
};

function p2kwiet2012247631642_btnRequestPin_onClick_seq0(eventobject) {
    onClickOfRequestNewPinMenu.call(this);
};

function p2kwiet2012247631642_btnCashAdvance_onClick_seq0(eventobject) {
    onClickOfCashAdvanceMenu.call(this);
};

function p2kwiet2012247631642_btnFullStm_onClick_seq0(eventobject) {
    viewFullStatementFromCardMgt.call(this);
};

function p2kwiet2012247631642_buttonBack_onClick_seq0(eventobject) {
    onClickBackManageListCard.call(this);
};

function p2kwiet2012247631720_frmMBMutualFundDetails_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631720_frmMBMutualFundDetails_preshow_seq0(eventobject, neworientation) {
    frmMBMutualFundDetailsMenuPreshow.call(this);
};

function p2kwiet2012247631720_frmMBMutualFundDetails_postshow_seq0(eventobject, neworientation) {
    frmMBMutualFundDetailsMenuPostshow.call(this);
};

function p2kwiet2012247631720_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631720_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631720_btnOptions_onClick_seq0(eventobject) {
    toDo.call(this);
};

function p2kwiet2012247631720_lblOrdToBProcess_onClick_seq0(eventobject) {
    frmMFFullStatementMB.segcredit.removeAll();
    frmMFFullStatementMB.btnunbilled.skin = "btnunbilled";
    frmMFFullStatementMB.btnbilled.skin = "btnunbilledfoc";
    frmMFFullStatementMB.hbxaccnt.setVisibility(false);
    frmMFFullStatementMB.hbxSegHeader.setVisibility(false);
    gblViewType = "O";
    viewFullStatementMF();
    //startCCstatementservice("billed", "1");
};

function p2kwiet2012247631720_lnkFullStatement_onClick_seq0(eventobject) {
    /* 
viewMFFullStatementMB.call(this,viewType="F");

 */
    gblViewType = "F";
    frmMFFullStatementMB.btnunbilled.skin = "btnunbilledfoc";
    frmMFFullStatementMB.btnunbilled.focusSkin = "btnunbilledfoc";
    frmMFFullStatementMB.btnbilled.skin = "btnunbilled";
    viewFullStatementMF.call(this);
};

function p2kwiet2012247631720_hboxTaxDoc_onClick_seq0(eventobject) {
    generateTaxDocPdf.call(this);
};

function p2kwiet2012247631720_lnkTaxDoc_onClick_seq0(eventobject) {
    generateTaxDocPdfMB.call(this);
};

function p2kwiet2012247631720_btnBack1_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        MBcallMutualFundsSummary.call(this);
    } else {
        //kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
        showLoadingScreen();
        frmMBMutualFundDetails.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        kony.application.dismissLoadingScreen();
    }
};

function p2kwiet2012247631967_frmMBMyActivities_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247631967_frmMBMyActivities_preshow_seq0(eventobject, neworientation) {
    frmMBMyActivitiesMenuPreshow.call(this);
};

function p2kwiet2012247631967_frmMBMyActivities_postshow_seq0(eventobject, neworientation) {
    frmMBMyActivitiesMenuPostshow.call(this);
};

function p2kwiet2012247631967_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247631967_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631967_btnMonth_onClick_seq0(eventobject) {
    /* 
gListTab = 0;
frmMBMyActivities.hbxCalContainerMonthView.setVisibility(true);
frmMBMyActivities.hbxDayView.setVisibility(false);
//frmMBMyActivities.hboxTxnsDateListView.setVisibility(false);
frmMBMyActivities.btnMonth.skin=sknBtnMBCalendarMenuLeftFocus;
frmMBMyActivities.btnDay.skin=sknBtnMBCalendarMenuMid;
frmMBMyActivities.BtnList.skin=sknBtnMBCalendarMenuRight;
IBMyActivitiesCreateSegRowsForTxns();

 */
    calendarShowMonthViewMB.call(this);
};

function p2kwiet2012247631967_btnDay_onClick_seq0(eventobject) {
    /* 
gListTab = 0;
frmMBMyActivities.hbxCalContainerMonthView.setVisibility(false);
frmMBMyActivities.hbxDayView.setVisibility(true);
//frmMBMyActivities.hboxTxnsDateListView.setVisibility(false);
frmMBMyActivities.btnMonth.skin=sknBtnMBCalendarMenuLeft;
frmMBMyActivities.btnDay.skin=sknBtnMBCalendarMenuMidFocus;
frmMBMyActivities.BtnList.skin=sknBtnMBCalendarMenuRight;
frmMBMyActivities_onClick_segDayView();
IBMyActivitiesCreateSegRowsForTxns();

 */
    calendarShowDayViewMB.call(this);
};

function p2kwiet2012247631967_BtnList_onClick_seq0(eventobject) {
    /* 
gListTab = 1;
frmMBMyActivities.hbxCalContainerMonthView.setVisibility(false);
frmMBMyActivities.hbxDayView.setVisibility(false);
//frmMBMyActivities.hboxTxnsDateListView.setVisibility(true);
frmMBMyActivities.btnMonth.skin=sknBtnMBCalendarMenuLeft;
frmMBMyActivities.btnDay.skin=sknBtnMBCalendarMenuMid;
frmMBMyActivities.BtnList.skin=sknBtnMBCalendarMenuRightFocus;
IBMyActivitiesCreateSegRowsForTxns();

 */
    calendarShowListViewMB.call(this);
};

function p2kwiet2012247631967_btnMonthSpa_onClick_seq0(eventobject) {
    /* 
gListTab = 0;
frmMBMyActivities.hbxCalContainerMonthView.setVisibility(true);
frmMBMyActivities.hbxDayView.setVisibility(false);
//frmMBMyActivities.hboxTxnsDateListView.setVisibility(false);
frmMBMyActivities.btnMonth.skin=sknBtnMBCalendarMenuLeftFocus;
frmMBMyActivities.btnDay.skin=sknBtnMBCalendarMenuMid;
frmMBMyActivities.BtnList.skin=sknBtnMBCalendarMenuRight;
IBMyActivitiesCreateSegRowsForTxns();

 */
    calendarShowMonthViewMB.call(this);
};

function p2kwiet2012247631967_spabtnList_onClick_seq0(eventobject) {
    /* 
gListTab = 1;
frmMBMyActivities.hbxCalContainerMonthView.setVisibility(false);
frmMBMyActivities.hbxDayView.setVisibility(false);
//frmMBMyActivities.hboxTxnsDateListView.setVisibility(true);
frmMBMyActivities.btnMonth.skin=sknBtnMBCalendarMenuLeft;
frmMBMyActivities.btnDay.skin=sknBtnMBCalendarMenuMid;
frmMBMyActivities.BtnList.skin=sknBtnMBCalendarMenuRightFocus;
IBMyActivitiesCreateSegRowsForTxns();

 */
    calendarShowListViewMB.call(this);
};

function p2kwiet2012247631967_leftCal_onClick_seq0(eventobject) {
    frmMBMyActivities_onClick_leftCal.call(this);
};

function p2kwiet2012247631967_segDayView_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247631967_rightCal_onClick_seq0(eventobject) {
    frmMBMyActivities_onClick_rightCal.call(this);
};

function p2kwiet2012247631967_btnPrev_onClick_seq0(eventobject) {
    /* 
FirstDayOfMonthPrev.call(this);

 */
    navigateToPreviousMonth.call(this);
};

function p2kwiet2012247631967_btnNext_onClick_seq0(eventobject) {
    /* 
FirstDayOfMonthNext.call(this);

 */
    navigateToNextMonth.call(this);
};

function p2kwiet2012247631967_vbox0_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox1_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox2_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox3_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox4_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox5_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox6_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox7_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox8_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox9_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox10_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox11_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox12_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox13_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox14_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox15_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox16_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox17_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox18_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox19_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox20_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox21_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox22_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox23_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox24_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox25_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox26_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox27_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox28_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox29_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox30_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox31_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox32_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox33_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox34_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox35_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox36_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox37_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox38_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox39_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox40_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_vbox41_onClick_seq0(eventobject) {
    calendarDayOnClick.call(this, eventobject, frmMBMyActivities);
};

function p2kwiet2012247631967_cmbTxnType_onSelection_seq0(eventobject) {
    IBMyActivitiesCreateSegRowsForTxns.call(this, null);
};

function p2kwiet2012247631967_combobox589611684120191_onSelection_seq0(eventobject) {
    IBMyActivitiesCreateSegRowsForTxns.call(this, null);
};

function p2kwiet2012247631967_segCalendar_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    IBMyActivitiesSegmentOnClick.call(this, eventobject);
};

function p2kwiet2012247631989_frmMBNewTnC_preshow_seq0(eventobject) {
    frmMBNewTnCMenuPreshow.call(this);
};

function p2kwiet2012247631989_frmMBNewTnC_postshow_seq0(eventobject) {
    frmMBNewTnCMenuPostshow.call(this);
};

function p2kwiet2012247631989_flexPdf_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBNewTnC.buttonPdf.skin = "btnPdfSCFoc";
    frmMBNewTnC.flexPdf.skin = "flexLightBlueLine";
};

function p2kwiet2012247631989_flexPdf_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBNewTnC.flexPdf.skin = "flexWhiteBG";
    frmMBNewTnC.buttonPdf.skin = "btnPdfSC";
    var pdfFlowType = "";
    onClickBtnPdfFrmMBNewTnC.call(this);
};

function p2kwiet2012247631989_buttonPdf_onClick_seq0(eventobject) {
    /* 
            var pdfFlowType = "";
       
    */
    /* 
onClickDownloadTnC.call(this,"pdf",    "TMBSavingCare");

 */
};

function p2kwiet2012247631989_flexEmail_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBNewTnC.buttonEmail.skin = "btnEmailSCFoc"
    frmMBNewTnC.flexEmail.skin = "flexLightBlueLine";
};

function p2kwiet2012247631989_flexEmail_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBNewTnC.flexEmail.skin = "flexWhiteBG";
    frmMBNewTnC.buttonEmail.skin = "btnEmailSC"
    onClickBtnEmailFrmMBNewTnC.call(this);
};

function p2kwiet2012247631989_buttonEmail_onClick_seq0(eventobject) {
    /* 
onClickEmailTnCMBSavingsCare.call(this);

 */
};

function p2kwiet2012247631989_btnback_onClick_seq0(eventobject) {
    onClickBtnBackFrmMBNewTnC.call(this);
};

function p2kwiet2012247631989_btnnext_onClick_seq0(eventobject) {
    onClickBtnNextFrmMBNewTnC.call(this);
};

function p2kwiet2012247631989_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247631989_btnRight_onClick_seq0(eventobject) {};

function p2kwiet2012247632069_frmMBPointRedemptionComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632069_frmMBPointRedemptionComplete_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632069_frmMBPointRedemptionComplete_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632069_frmMBPointRedemptionComplete_preshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionCompleteMenuPreshow.call(this);
};

function p2kwiet2012247632069_frmMBPointRedemptionComplete_postshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionCompleteMenuPostshow.call(this);
};

function p2kwiet2012247632069_frmMBPointRedemptionComplete_onhide_seq0(eventobject, neworientation) {
    frmMBPointRedemptionComplete.hboxaddfblist.isVisible = false;
    frmMBPointRedemptionComplete.btnRight.skin = "btnShare";
    frmMBPointRedemptionComplete.imgHeaderMiddle1.src = "arrowtop.png";
    frmMBPointRedemptionComplete.imgHeaderRight1.src = "empty.png";
};

function p2kwiet2012247632069_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632069_button474136157108682_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632069_btnRight_onClick_seq0(eventobject) {
    shareButtonHandlerPointRedemption.call(this);
};

function p2kwiet2012247632069_button47428498625_onClick_seq0(eventobject) {
    saveRedeemCompleteAsPDFImage.call(this, "pdf");
};

function p2kwiet2012247632069_btnPhoto_onClick_seq0(eventobject) {
    saveRedeemCompleteAsPDFImage.call(this, "png");
};

function p2kwiet2012247632069_btnEmailto_onClick_seq0(eventobject) {
    postOnFBPointredemptionComplete.call(this);
};

function p2kwiet2012247632069_hbxToAccount_onClick_seq0(eventobject) {};

function p2kwiet2012247632069_hbxMemberNumber_onClick_seq0(eventobject) {};

function p2kwiet2012247632069_hbxLastName_onClick_seq0(eventobject) {};

function p2kwiet2012247632069_hbxPointsBeingRedeemed_onClick_seq0(eventobject) {};

function p2kwiet2012247632069_hbxRemainingPoints_onClick_seq0(eventobject) {};

function p2kwiet2012247632069_hbxInternalAmntCashbackRopMile_onClick_seq0(eventobject) {};

function p2kwiet2012247632069_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickOfAccountDetailsBack.call(this);
    } else {
        frmMBPointRedemptionComplete.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247632069_btnNext_onClick_seq0(eventobject) {
    onClickRedeemMoreComplete.call(this);
};

function p2kwiet2012247632142_frmMBPointRedemptionConfirmation_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632142_frmMBPointRedemptionConfirmation_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632142_frmMBPointRedemptionConfirmation_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632142_frmMBPointRedemptionConfirmation_preshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionConfirmationMenuPreshow.call(this);
};

function p2kwiet2012247632142_frmMBPointRedemptionConfirmation_postshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionConfirmationMenuPostshow.call(this);
};

function p2kwiet2012247632142_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632142_button474136157108682_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632142_btnRight_onClick_seq0(eventobject) {
    frmMBPointRedemptionLanding.show();
};

function p2kwiet2012247632142_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickCancelPointRedemption.call(this);
    } else {
        frmMBPointRedemptionConfirmation.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247632142_btnNext_onClick_seq0(eventobject) {
    onClickNextRewardsConfirmation.call(this);
};

function p2kwiet2012247632223_frmMBPointRedemptionLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632223_frmMBPointRedemptionLanding_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632223_frmMBPointRedemptionLanding_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632223_frmMBPointRedemptionLanding_preshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionLandingMenuPreshow.call(this);
};

function p2kwiet2012247632223_frmMBPointRedemptionLanding_postshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionLandingMenuPostshow.call(this);
};

function p2kwiet2012247632223_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632223_button474136157108682_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632223_hbxRewardsButton_onClick_seq0(eventobject) {
    loadPointRedemptionOptions.call(this);
};

function p2kwiet2012247632223_btnRewards1_onClick_seq0(eventobject) {
    loadPointRedemptionOptions.call(this);
};

function p2kwiet2012247632223_hbxRewardsName_onClick_seq0(eventobject) {
    loadPointRedemptionOptions.call(this);
};

function p2kwiet2012247632223_vbxCashback_onClick_seq0(eventobject) {
    onClickToAccountForCashback.call(this);
};

function p2kwiet2012247632223_hbxToAccount_onClick_seq0(eventobject) {
    onClickToAccountForCashback.call(this);
};

function p2kwiet2012247632223_btnCashbackAccounts_onClick_seq0(eventobject) {
    onClickToAccountForCashback.call(this);
};

function p2kwiet2012247632223_txtMemberNumber_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    checkMinMemberNumberLength.call(this);
};

function p2kwiet2012247632223_txtMemberNumber_SPA_onEndEditing_seq0(eventobject, changedtext) {
    checkMinMemberNumberLength.call(this);
};

function p2kwiet2012247632223_txtMemberNumber_Android_onEndEditing_seq0(eventobject, changedtext) {
    checkMinMemberNumberLength.call(this);
};

function p2kwiet2012247632223_txtMemberNumber_onTextChange_seq0(eventobject, changedtext) {
    checkNextEnableFromRewardsLanding.call(this);
};

function p2kwiet2012247632223_txtLastName_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    checkMinLastNameLength.call(this);
};

function p2kwiet2012247632223_txtLastName_Android_onEndEditing_seq0(eventobject, changedtext) {
    checkMinLastNameLength.call(this);
};

function p2kwiet2012247632223_txtLastName_onTextChange_seq0(eventobject, changedtext) {
    checkNextEnableFromRewardsLanding.call(this);
};

function p2kwiet2012247632223_txtPointsBeingRedeemed_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = removeCommas(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text);
};

function p2kwiet2012247632223_txtPointsBeingRedeemed_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    checkRedeemPointsMoreAvailablePoints.call(this);
    var redeemPoints = frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text;
    if (redeemPoints != "") frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = commaFormattedPoints(Number(redeemPoints));
    else frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = "";
};

function p2kwiet2012247632223_txtPointsBeingRedeemed_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = removeCommas(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text);
};

function p2kwiet2012247632223_txtPointsBeingRedeemed_SPA_onEndEditing_seq0(eventobject, changedtext) {
    checkRedeemPointsMoreAvailablePoints.call(this);
    var redeemPoints = frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text;
    if (redeemPoints != "") frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = commaFormattedPoints(Number(redeemPoints));
    else frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = "";
};

function p2kwiet2012247632223_txtPointsBeingRedeemed_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = replaceCommon(frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text, ",", "");
};

function p2kwiet2012247632223_txtPointsBeingRedeemed_Android_onEndEditing_seq0(eventobject, changedtext) {
    checkRedeemPointsMoreAvailablePoints.call(this);
    var redeemPoints = frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text;
    if (redeemPoints != "") frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = commaFormattedPoints(Number(redeemPoints));
    else frmMBPointRedemptionLanding.txtPointsBeingRedeemed.text = "";
};

function p2kwiet2012247632223_txtPointsBeingRedeemed_onDone_seq0(eventobject, changedtext) {};

function p2kwiet2012247632223_txtPointsBeingRedeemed_onTextChange_seq0(eventobject, changedtext) {
    calculateRemainingPoints.call(this);
    checkNextEnableFromRewardsLanding.call(this);
};

function p2kwiet2012247632223_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickCancelPointRedemption.call(this);
    } else {
        frmMBPointRedemptionLanding.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247632223_btnNext_onClick_seq0(eventobject) {
    saveRedeemRewardsInSession.call(this);
};

function p2kwiet2012247632251_frmMBPointRedemptionProdFeature_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632251_frmMBPointRedemptionProdFeature_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632251_frmMBPointRedemptionProdFeature_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632251_frmMBPointRedemptionProdFeature_preshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionProdFeatureMenuPreshow.call(this);
};

function p2kwiet2012247632251_frmMBPointRedemptionProdFeature_postshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionProdFeatureMenuPostshow.call(this);
};

function p2kwiet2012247632251_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632251_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632251_btnCancelSpa_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickCancelPointRedemption.call(this);
    } else {
        frmMBPointRedemptionProdFeature.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247632251_btnNextSpa_onClick_seq0(eventobject) {
    frmMBPointRedemptionTnC.show();
};

function p2kwiet2012247632251_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickCancelPointRedemption.call(this);
    } else {
        frmMBPointRedemptionProdFeature.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247632251_btnNext_onClick_seq0(eventobject) {
    frmMBPointRedemptionTnC.show();
};

function p2kwiet2012247632272_frmMBPointRedemptionRewardsList_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632272_frmMBPointRedemptionRewardsList_preshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionRewardsListMenuPreshow.call(this);
};

function p2kwiet2012247632272_frmMBPointRedemptionRewardsList_postshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionRewardsListMenuPostshow.call(this);
};

function p2kwiet2012247632272_btnArrowBack_onClick_seq0(eventobject) {
    frmMBPointRedemptionLanding.show();
};

function p2kwiet2012247632272_segRewardsList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onSelectingReward.call(this);
};

function p2kwiet2012247632303_frmMBPointRedemptionTnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632303_frmMBPointRedemptionTnC_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632303_frmMBPointRedemptionTnC_preshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionTnCMenuPreshow.call(this);
};

function p2kwiet2012247632303_frmMBPointRedemptionTnC_postshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionTnCMenuPostshow.call(this);
};

function p2kwiet2012247632303_frmMBPointRedemptionTnC_onhide_seq0(eventobject, neworientation) {
    frmMBPointRedemptionTnC.hboxSaveCamEmail.isVisible = false;
    frmMBPointRedemptionTnC.imgHeaderMiddle.src = "arrowtop.png"
    frmMBPointRedemptionTnC.imgHeaderRight.src = "empty.png"
    frmMBPointRedemptionTnC.btnRight.skin = "btnShare";
};

function p2kwiet2012247632303_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632303_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632303_btnRight_onClick_seq0(eventobject) {
    var skin = frmMBPointRedemptionTnC.btnRight.skin
    if (frmMBPointRedemptionTnC.hboxSaveCamEmail.isVisible == false) {
        frmMBPointRedemptionTnC.hboxSaveCamEmail.isVisible = true;
        frmMBPointRedemptionTnC.imgHeaderRight.src = "arrowtop.png"
        frmMBPointRedemptionTnC.imgHeaderMiddle.src = "empty.png"
        frmMBPointRedemptionTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmMBPointRedemptionTnC.hboxSaveCamEmail.isVisible = false;
        frmMBPointRedemptionTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmMBPointRedemptionTnC.imgHeaderRight.src = "empty.png"
        frmMBPointRedemptionTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247632303_button47505874736294_onClick_seq0(eventobject) {
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "TMBPointRedemption");
};

function p2kwiet2012247632303_button47505874736298_onClick_seq0(eventobject) {
    onClickEmailTnCMBPointRedemption.call(this);
};

function p2kwiet2012247632303_btnCancelSpa_onClick_seq0(eventobject) {
    onClickCancelPointRedemption.call(this);
};

function p2kwiet2012247632303_btnAgreeSpa_onClick_seq0(eventobject) {
    onCLickNextPointsTnC.call(this);
};

function p2kwiet2012247632303_btnPointCancel_onClick_seq0(eventobject) {
    frmMBPointRedemptionProdFeature.show();
};

function p2kwiet2012247632303_btnAgree_onClick_seq0(eventobject) {
    onCLickNextPointsTnC.call(this);
};

function p2kwiet2012247632326_frmMBPointRedemptionToAccounts_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632326_frmMBPointRedemptionToAccounts_preshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionToAccountsMenuPreshow.call(this);
};

function p2kwiet2012247632326_frmMBPointRedemptionToAccounts_postshow_seq0(eventobject, neworientation) {
    frmMBPointRedemptionToAccountsMenuPostshow.call(this);
};

function p2kwiet2012247632326_button156335099532181_onClick_seq0(eventobject) {
    frmMBPointRedemptionLanding.show();
};

function p2kwiet2012247632326_btnSearchCashBackAccounts_onClick_seq0(eventobject) {
    searchToAccountsRewardsMB.call(this, "btnSearchCashBackAccounts");
};

function p2kwiet2012247632326_txbXferSearch_onTextChange_seq0(eventobject, changedtext) {
    searchToAccountsRewardsMB.call(this, "txbXferSearch");
};

function p2kwiet2012247632326_segAccountList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onSelectToAccountForCashback.call(this);
};

function p2kwiet2012247632394_frmMBPreLoginAccessesPin_preshow_seq0(eventobject) {
    preshowPreAccessScreen.call(this);
};

function p2kwiet2012247632394_frmMBPreLoginAccessesPin_postshow_seq0(eventobject) {
    postshowPreAccessScreen.call(this);
};

function p2kwiet2012247632394_btnMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632394_flexQuickBalButton_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    showQuickBalance.call(this);
};

function p2kwiet2012247632394_btnLoginToQB_onClick_seq0(eventobject) {
    onClickLoginToQB.call(this);
};

function p2kwiet2012247632394_btnOne_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnTwo_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnThree_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnFour_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnFive_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnSix_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnSeven_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnEight_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnNine_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnForgotPin_onClick_seq0(eventobject) {
    onClickForgotPin.call(this);
};

function p2kwiet2012247632394_btnZero_onClick_seq0(eventobject) {
    onClickKeyBoardButton.call(this, eventobject);
};

function p2kwiet2012247632394_btnTouchnBack_onClick_seq0(eventobject) {
    onClickTounchandBack.call(this);
};

function p2kwiet2012247632394_segAccounts_onPull_seq0(eventobject) {
    onPullQuickBalanceSegment.call(this);
};

function p2kwiet2012247632394_flexQuickBalanceCancel_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    closeQuickBalance.call(this);
};

function p2kwiet2012247632394_btnAccessPin_onClick_seq0(eventobject) {
    onClickEnterAccessPin.call(this);
};

function p2kwiet2012247632423_frmMBQuickBalanceBrief_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632423_frmMBQuickBalanceBrief_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632423_frmMBQuickBalanceBrief_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632423_frmMBQuickBalanceBrief_preshow_seq0(eventobject, neworientation) {
    frmMBQuickBalanceBriefMenuPreshow.call(this);
};

function p2kwiet2012247632423_frmMBQuickBalanceBrief_postshow_seq0(eventobject, neworientation) {
    frmMBQuickBalanceBriefMenuPostshow.call(this);
};

function p2kwiet2012247632423_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632423_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632423_btnCancel_onClick_seq0(eventobject) {
    onClickBackEStatement.call(this);
};

function p2kwiet2012247632423_btnNext_onClick_seq0(eventobject) {
    onClickQuickBalanceBriefNext.call(this);
};

function p2kwiet2012247632459_frmMBQuickBalanceSetting_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632459_frmMBQuickBalanceSetting_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632459_frmMBQuickBalanceSetting_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632459_frmMBQuickBalanceSetting_preshow_seq0(eventobject, neworientation) {
    frmMBQuickBalanceSettingMenuPreshow.call(this);
};

function p2kwiet2012247632459_frmMBQuickBalanceSetting_postshow_seq0(eventobject, neworientation) {
    frmMBQuickBalanceSettingMenuPostshow.call(this);
};

function p2kwiet2012247632459_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632459_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632459_segQuickBalanceSetting_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickSwitchQuickBalanceSetting.call(this);
};

function p2kwiet2012247632459_btnPhCheckBox_onClick_seq0(eventobject, context) {
    onClickSwitchQuickBalanceSetting.call(this);
};

function p2kwiet2012247632459_switchMobile_onslide_seq0(eventobject, context) {
    onClickSwitchQuickBalanceSetting.call(this);
};

function p2kwiet2012247632459_btnReturnToHome_onClick_seq0(eventobject) {
    onClickBackEStatement.call(this);
};

function p2kwiet2012247632491_frmMBReIssueDBProduct_preshow_seq0(eventobject) {
    frmMBReIssueDBProductMenuPreshow.call(this);
};

function p2kwiet2012247632491_frmMBReIssueDBProduct_postshow_seq0(eventobject) {
    frmMBReIssueDBProductMenuPostshow.call(this);
};

function p2kwiet2012247632491_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632491_btnback_onClick_seq0(eventobject) {
    onclickBackfrmMBReIssueDBProduct.call(this);
};

function p2kwiet2012247632491_btnnext_onClick_seq0(eventobject) {
    onclickNextfrmMBReIssueDBProduct.call(this);
};

function p2kwiet2012247632512_frmMBRequestNewPin_preshow_seq0(eventobject) {
    frmMBRequestNewPinMenuPreshow.call(this);
};

function p2kwiet2012247632512_frmMBRequestNewPin_postshow_seq0(eventobject) {
    frmMBRequestNewPinMenuPostshow.call(this);
};

function p2kwiet2012247632512_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632512_lblChangeAddrLink_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onClickChangeAddrOfRequestNewPinConfirmScreen.call(this);
};

function p2kwiet2012247632512_btnBack_onClick_seq0(eventobject) {
    onClickBackOfRequestNewPinConfirmScreen.call(this);
};

function p2kwiet2012247632512_btnConfirm_onClick_seq0(eventobject) {
    onClickConfirmOfRequestNewPinConfirmScreen.call(this);
};

function p2kwiet2012247632528_frmMBRequestPinFailure_preshow_seq0(eventobject) {
    frmMBRequestPinFailurePreshow.call(this);
};

function p2kwiet2012247632528_frmMBRequestPinFailure_postshow_seq0(eventobject) {
    frmMBRequestPinFailurePostshow.call(this);
};

function p2kwiet2012247632528_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632528_lblTryAgain_onClick_seq0(eventobject) {
    onClickTryAgainRequestNewPin.call(this);
};

function p2kwiet2012247632528_lblCallcenternum_onClick_seq0(eventobject) {
    callNative.call(this, gblTMBContactNumber);
};

function p2kwiet2012247632528_lblManageCards_onClick_seq0(eventobject) {
    onClickManageOfRequestPinSuccessScreen.call(this);
};

function p2kwiet2012247632544_frmMBRequestPinSuccess_preshow_seq0(eventobject) {
    frmMBRequestPinSuccessPreshow.call(this);
};

function p2kwiet2012247632544_frmMBRequestPinSuccess_postshow_seq0(eventobject) {
    frmMBRequestPinSuccessPostshow.call(this);
};

function p2kwiet2012247632544_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632544_lblManageCards_onClick_seq0(eventobject) {
    onClickManageOfRequestPinSuccessScreen.call(this);
};

function p2kwiet2012247632564_frmMBSavingCareTnC_preshow_seq0(eventobject) {
    frmMBSavingCareTnCMenuPreshow.call(this);
};

function p2kwiet2012247632564_frmMBSavingCareTnC_postshow_seq0(eventobject) {
    frmMBSavingCareTnCMenuPostshow.call(this);
};

function p2kwiet2012247632564_flexPdf_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBSavingCareTnC.buttonPdf.skin = "btnPdfSCFoc";
    frmMBSavingCareTnC.flexPdf.skin = "flexLightBlueLine";
};

function p2kwiet2012247632564_flexPdf_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBSavingCareTnC.flexPdf.skin = "flexWhiteBG";
    frmMBSavingCareTnC.buttonPdf.skin = "btnPdfSC";
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "TMBSavingCare");
};

function p2kwiet2012247632564_buttonPdf_onClick_seq0(eventobject) {
    /* 
            var pdfFlowType = "";
       
    */
    /* 
onClickDownloadTnC.call(this,"pdf",    "TMBSavingCare");

 */
};

function p2kwiet2012247632564_flexEmail_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBSavingCareTnC.buttonEmail.skin = "btnEmailSCFoc"
    frmMBSavingCareTnC.flexEmail.skin = "flexLightBlueLine";
};

function p2kwiet2012247632564_flexEmail_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBSavingCareTnC.flexEmail.skin = "flexWhiteBG";
    frmMBSavingCareTnC.buttonEmail.skin = "btnEmailSC"
    onClickEmailTnCMBSavingsCare.call(this);
};

function p2kwiet2012247632564_buttonEmail_onClick_seq0(eventobject) {
    /* 
onClickEmailTnCMBSavingsCare.call(this);

 */
};

function p2kwiet2012247632564_btnback_onClick_seq0(eventobject) {
    backTnCForm.call(this);
};

function p2kwiet2012247632564_btnnext_onClick_seq0(eventobject) {
    onClickNextSavingsCareTnC.call(this);
};

function p2kwiet2012247632564_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632564_btnRight_onClick_seq0(eventobject) {};

function p2kwiet2012247632599_frmMBSavingsCareAddBal_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632599_frmMBSavingsCareAddBal_preshow_seq0(eventobject, neworientation) {
    frmMBSavingsCareAddBal.scrollboxMain.scrollToEnd();
    frmMBSavingsCareAddBalPreShow.call(this);
    DisableFadingEdges.call(this, frmMBSavingsCareAddBal);
};

function p2kwiet2012247632599_frmMBSavingsCareAddBal_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
};

function p2kwiet2012247632599_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632599_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632599_segNSSlider_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectDSSeg.call(this, frmOpnActSelAct.segNSSlider);
};

function p2kwiet2012247632599_segNSSlider_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {
    var modScroll = new getOPenActMBIndex();
    modScroll.paginationSwitch(sectionIndex, rowIndex, frmMBSavingsCareAddBal.segNSSlider);
};

function p2kwiet2012247632599_txtAmountVal_onDone_seq0(eventobject, changedtext) {
    onDoneAmount.call(this);
};

function p2kwiet2012247632599_txtAmountVal_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeAmount.call(this);
};

function p2kwiet2012247632599_btnOATDConfCancel_onClick_seq0(eventobject) {
    /* 
frmMBSavingsCareOccupationInfo.show();
	
 */
    OnClickOpenMore.call(this);
};

function p2kwiet2012247632599_btnPopUpTermination_onClick_seq0(eventobject) {
    showBeneficiariesForm.call(this);
};

function p2kwiet2012247632619_frmMBSavingsCareAddBeneficiary_preshow_seq0(eventobject) {
    /* 
frmMBSavingsCareAddBeneficiary.txtfirstName.placeholder=removeColonFromEnd(kony.i18n.getLocalizedString("keyFirstName"));
frmMBSavingsCareAddBeneficiary.txtlastName.placeholder=removeColonFromEnd(kony.i18n.getLocalizedString("keyLastName"));
frmMBSavingsCareAddBeneficiary.lblrelation.text=kony.i18n.getLocalizedString("keyOpenRelation");

 */
};

function p2kwiet2012247632619_flexContainer47826097855787_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onClickEditBenifBack.call(this);
};

function p2kwiet2012247632619_btnclose_onClick_seq0(eventobject) {
    onClickEditBenifBack.call(this);
};

function p2kwiet2012247632619_txtfirstName_onDone_seq0(eventobject, changedtext) {
    onclickfirstName.call(this);
};

function p2kwiet2012247632619_txtlastName_onDone_seq0(eventobject, changedtext) {
    onclicklastName.call(this);
};

function p2kwiet2012247632619_CopyFlexContainer071fe29bd21224b_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onclickfrmMBSavingsCareAddBeneficiaryRelationLabel.call(this);
};

function p2kwiet2012247632619_button761510865391611_onClick_seq0(eventobject) {
    /* 
onBackCallsShowAddBenef.call(this);

 */
    frmMBSavingsCareAddNickName.show();
};

function p2kwiet2012247632619_button761510865391613_onClick_seq0(eventobject) {
    validateBeneficiary.call(this);
};

function p2kwiet2012247632653_frmMBSavingsCareAddNickName_preshow_seq0(eventobject) {
    frmMBSavingsCareAddNickNameMenuPreshow.call(this);
};

function p2kwiet2012247632653_frmMBSavingsCareAddNickName_postshow_seq0(eventobject) {
    frmMBSavingsCareAddNickNameMenuPostshow.call(this);
};

function p2kwiet2012247632653_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632653_btnBack_onClick_seq0(eventobject) {
    isNickNameForm = false;
    //frmMBSavingsCareAddBal.segNSSlider.selectedIndex = gblSelectedIndexSc;
    displaySelectedFromAct.call(this);
    /* 
OnClickOpenNewAccount.call(this);

 */
    if (gblSavingsCareFlow == "AccountDetailsFlow") {
        frmAccountDetailsMB.show();
    } else {
        if (gblSavingsCareFlow == "MyAccountFlow") {
            frmMyAccountView.show();
        } else {
            frmMBSavingsCareAddBal.show();
        }
    }
    /* 
frmMBSavingsCareAddBal.show();
	
 */
};

function p2kwiet2012247632653_btnNext_onClick_seq0(eventobject) {
    /* 
callSaveOpenAccounts.call(this);

 */
    validateBeneficiariesDetails.call(this);
};

function p2kwiet2012247632707_frmMBSavingsCareComplete_preshow_seq0(eventobject) {
    frmMBSavingsCareCompleteMenuPreshow.call(this);
};

function p2kwiet2012247632707_frmMBSavingsCareComplete_postshow_seq0(eventobject) {
    frmMBSavingsCareCompleteMenuPostshow.call(this);
};

function p2kwiet2012247632707_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632707_btnBack_onClick_seq0(eventobject) {
    /* 
onBackCallsShowAddBenef.call(this);

 */
};

function p2kwiet2012247632707_btnNext_onClick_seq0(eventobject) {
    if (flow == "myAccount") {} else {
        /* 
frmMBSavingsCareComplete.show();
	
 */
    }
};

function p2kwiet2012247632758_frmMBSavingsCareConfirmation_preshow_seq0(eventobject) {
    frmMBSavingsCareConfirmationMenuPreshow.call(this);
};

function p2kwiet2012247632758_frmMBSavingsCareConfirmation_postshow_seq0(eventobject) {
    frmMBSavingsCareConfirmationMenuPostshow.call(this);
};

function p2kwiet2012247632758_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632758_btnBack_onClick_seq0(eventobject) {
    /* 
onBackCallsShowAddBenef.call(this);

 */
    frmMBSavingsCareAddNickName.show();
};

function p2kwiet2012247632758_btnNext_onClick_seq0(eventobject) {
    if (flow == "myAccount") {} else {
        /* 
frmMBSavingsCareComplete.show();
	
 */
    }
    /* 
frmMBSavingsCareComplete.show();
	
 */
    if (flow == "myAccount") {} else {
        /* 
frmMBSavingsCareComplete.show();
	
 */
    }
    showTransPwdforOpenSavingsCare.call(this);
};

function p2kwiet2012247632804_frmMBSavingsCareContactInfo_preshow_seq0(eventobject) {
    frmMBSavingsCareContactInfoMenuPreshow.call(this);
};

function p2kwiet2012247632804_frmMBSavingsCareContactInfo_postshow_seq0(eventobject) {
    frmMBSavingsCareContactInfoMenuPostshow.call(this);
};

function p2kwiet2012247632804_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632804_button763357907360409_onClick_seq0(eventobject) {
    editbuttonflag = "profile";
    gblOpenProdAddress = false;
    gblOpenActSavingCareEditCont = true;
    getMBEditContactStatus.call(this);
};

function p2kwiet2012247632804_button76335790793830_onClick_seq0(eventobject) {
    editbuttonflag = "profile";
    gblOpenProdAddress = false;
    gblOpenActSavingCareEditCont = true;
    getMBEditContactStatus.call(this);
};

function p2kwiet2012247632804_lblMobile_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    /* 
gblMobNoTransLimitFlag=true;
gblOpenAccountFlow = false;



 */
    /* 
editbuttonflag="number";
frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text = "";
gblOpenActSavingCareEditCont=true;

 */
    /* 
checkStatusEditProfile.call(this);

 */
};

function p2kwiet2012247632804_button76335790793842_onClick_seq0(eventobject) {
    gblMobNoTransLimitFlag = true;
    gblOpenAccountFlow = false;
    editbuttonflag = "number";
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text = "";
    gblOpenActSavingCareEditCont = true;
    checkStatusEditProfile.call(this);
};

function p2kwiet2012247632804_btnback_onClick_seq0(eventobject) {
    kony.print("gblCallPrePost " + gblCallPrePost);
    frmMBSavingCareTnC.show();
};

function p2kwiet2012247632804_btnnext_onClick_seq0(eventobject) {
    /* 
makeAnotherTransfer.call(this);

 */
    OnclickShowSavingsCareOccupationInfo.call(this);
};

function p2kwiet2012247632826_btnclose_onClick_seq0(eventobject) {
    onClickEditBenifBack.call(this);
};

function p2kwiet2012247632826_flexContainer47826097857407_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    onClickEditBenifBack.call(this);
};

function p2kwiet2012247632826_txtfirstName_onDone_seq0(eventobject, changedtext) {
    onclickfirstNameEdit.call(this);
};

function p2kwiet2012247632826_txtlastName_onDone_seq0(eventobject, changedtext) {
    onclicklastNameEdit.call(this);
};

function p2kwiet2012247632826_flxRelations_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onclickfrmMBSavingsCareAddBeneficiaryRelationLabel.call(this);
};

function p2kwiet2012247632826_button761510865391611_onClick_seq0(eventobject) {
    /* 
onBackCallsShowAddBenef.call(this);

 */
    frmMBSavingsCareAddNickName.show();
};

function p2kwiet2012247632826_button761510865391613_onClick_seq0(eventobject) {
    validateBeneficiaryEdit.call(this);
};

function p2kwiet2012247632826_flxDeleteBenif_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    showDeletepopSC.call(this);
};

function p2kwiet2012247632862_frmMBSavingsCareOccupationInfo_preshow_seq0(eventobject) {
    frmMBSavingsCareOccupationInfoMenuPreshow.call(this);
};

function p2kwiet2012247632862_frmMBSavingsCareOccupationInfo_postshow_seq0(eventobject) {
    frmMBSavingsCareOccupationInfoMenuPostshow.call(this);
};

function p2kwiet2012247632862_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632862_btnback_onClick_seq0(eventobject) {
    frmMBSavingsCareContactInfo.show();
};

function p2kwiet2012247632862_btnconfirm_onClick_seq0(eventobject) {
    showSavingsCareAddAccount.call(this);
};

function p2kwiet2012247632872_frmMBSavingsCareProdBrief_preshow_seq0(eventobject) {
    frmMBSavingsCareProdBriefMenuPreshow.call(this);
};

function p2kwiet2012247632872_frmMBSavingsCareProdBrief_postshow_seq0(eventobject) {
    frmMBSavingsCareProdBriefMenuPostshow.call(this);
};

function p2kwiet2012247632872_btnback_onClick_seq0(eventobject) {
    frmOpenActSelProdPreShow();
    frmOpenActSelProd.show();
};

function p2kwiet2012247632872_btnnext_onClick_seq0(eventobject) {
    loadSavingsCareTermsNConditions.call(this);
    accDetail = "openaccount";
    flow = "openaccount";
    gblSavingsCareFlow = "";
};

function p2kwiet2012247632872_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632878_frmMBSavingsCareRelationshipBeneficiary_preshow_seq0(eventobject) {
    frmMBSavingsCareRelationshipBeneficiaryMenuPreshow.call(this);
};

function p2kwiet2012247632878_btnback_onClick_seq0(eventobject) {
    if (accEdit == "true") {
        frmMBSavingsCareEditBeneficiary.show();
    } else {
        backRelationship.call(this);
    }
};

function p2kwiet2012247632878_segRelationList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onclicksegRelationRow.call(this);
};

function p2kwiet2012247632908_frmMBSavingsCareTnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632908_frmMBSavingsCareTnC_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632908_frmMBSavingsCareTnC_preshow_seq0(eventobject, neworientation) {};

function p2kwiet2012247632908_frmMBSavingsCareTnC_postshow_seq0(eventobject, neworientation) {
    frmMBSavingsCareTnCMenuPostshow.call(this);
};

function p2kwiet2012247632908_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632908_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247632908_btnRight_onClick_seq0(eventobject) {};

function p2kwiet2012247632908_button47505874736294_onClick_seq0(eventobject) {
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "AnyIDRegistration");
};

function p2kwiet2012247632908_button47505874736298_onClick_seq0(eventobject) {
    onClickEmailTnCMBAnyID.call(this);
};

function p2kwiet2012247632908_btnCancelCon_onClick_seq0(eventobject) {
    /* 
frmOpenActSelProdPreShow();
frmOpenActSelProd.show();

 */
    /* 
frmMBSavingsCareProdBrief.show();
	
 */
    if (accDetail == "true") {
        frmAccountDetailsMB.show();
    } else {
        if (flow == "myAccount") {
            frmMyAccountView.show();
        } else {
            frmMBSavingsCareProdBrief.show();
        }
    }
};

function p2kwiet2012247632908_btnNext_onClick_seq0(eventobject) {
    onClickNextSavingsCareTnC.call(this);
};

function p2kwiet2012247632957_frmMBSetAccPinTxnPwd_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247632957_frmMBSetAccPinTxnPwd_preshow_seq0(eventobject, neworientation) {
    frmMBSetAccPinTxnPwdMenuPreshow.call(this);
};

function p2kwiet2012247632957_frmMBSetAccPinTxnPwd_postshow_seq0(eventobject, neworientation) {
    frmMBSetAccPinTxnPwdMenuPostshow.call(this);
};

function p2kwiet2012247632957_frmMBSetAccPinTxnPwd_onhide_seq0(eventobject, neworientation) {
    if (gblLang_flag) {
        TMBUtil.DestroyForm(frmMBSetAccPinTxnPwd)
    } else {
        gblLang_flag = false
    }
};

function p2kwiet2012247632957_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247632957_hbxAccessPin_onClick_seq0(eventobject) {
    focusTexbox.call(this);
};

function p2kwiet2012247632957_txtAccessPin_onTextChange_seq0(eventobject, changedtext) {
    accessPinRender.call(this, frmMBSetAccPinTxnPwd.txtAccessPin.text);
};

function p2kwiet2012247632957_txtTransPass_onTextChange_seq0(eventobject, changedtext) {
    frmMBSetAccPinTxnPwd.txtTransPassShow.text = frmMBSetAccPinTxnPwd.txtTransPass.text;
    passwordStrength.call(this);
};

function p2kwiet2012247632957_txtTransPassShow_onTextChange_seq0(eventobject, changedtext) {
    frmMBSetAccPinTxnPwd.txtTransPass.text = frmMBSetAccPinTxnPwd.txtTransPassShow.text;
    passwordStrength.call(this);
};

function p2kwiet2012247632957_hbxCheckbox_onClick_seq0(eventobject) {
    showHidePin.call(this);
};

function p2kwiet2012247632957_btnCheckBox_onClick_seq0(eventobject) {
    showHidePin.call(this);
};

function p2kwiet2012247632957_btnCancel1_onClick_seq0(eventobject) {
    ActivationMBViaIBLogoutService.call(this);
    cancelScheduledTimer.call(this, "accPinPwdShowTimer");
};

function p2kwiet2012247632957_btnNext_onClick_seq0(eventobject) {
    mbSetAccPinPwdValidatn.call(this);
};

function p2kwiet2012247633010_frmMBsetPasswd_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633010_frmMBsetPasswd_preshow_seq0(eventobject, neworientation) {
    frmMBsetPasswdMenuPreshow.call(this);
};

function p2kwiet2012247633010_frmMBsetPasswd_postshow_seq0(eventobject, neworientation) {
    frmMBsetPasswdMenuPostshow.call(this);
};

function p2kwiet2012247633010_frmMBsetPasswd_onhide_seq0(eventobject, neworientation) {
    if (gblLang_flag) {
        TMBUtil.DestroyForm(frmMBsetPasswd)
    } else {
        gblLang_flag = false
    }
};

function p2kwiet2012247633010_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633010_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633010_hbox50285458140_onClick_seq0(eventobject) {
    frmMBsetPasswd.hbox50285458140.skin = "hboxBG"
};

function p2kwiet2012247633010_txtAccessPwd_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
anchorupAccessPIN.call(this);

 */
    }
    frmMBsetPasswd.hbox50285458140.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 1;
};

function p2kwiet2012247633010_txtAccessPwd_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    //popAccessPinBubble.dismiss()
    frmMBsetPasswd.hbox50285458140.skin = "NoSkin"
};

function p2kwiet2012247633010_txtAccessPwd_Android_onBeginEditing_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
anchorupAccessPIN.call(this);

 */
    }
    frmMBsetPasswd.hbox50285458140.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 1;
};

function p2kwiet2012247633010_txtAccessPwd_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmMBsetPasswd.hbox50285458140.skin = "NoSkin"
        //popAccessPinBubble.dismiss()
};

function p2kwiet2012247633010_txtAccessPwd_onDone_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
popAccessPinBubble.dismiss()

 */
    }
    frmMBsetPasswd.hbox50285458140.skin = "NoSkin"
};

function p2kwiet2012247633010_txtAccessPwd_onTextChange_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
var str = frmMBsetPasswd.txtAccessPwd.text;

if (str != null && str.length >=1 ){
 popAccessPinBubble.dismiss();
}
else {
 popAccessPinBubble.show();
}

 */
    }
    frmMBsetPasswd.hbox50285458140.skin = "hboxBG"
};

function p2kwiet2012247633010_txtTempAccess_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    /* 
anchorupAccessPIN.call(this);

 */
    frmMBsetPasswd.hbox50285458140.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 2;
};

function p2kwiet2012247633010_txtTempAccess_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    //popAccessPinBubble.dismiss()
    frmMBsetPasswd.hbox50285458140.skin = "NoSkin"
};

function p2kwiet2012247633010_txtTempAccess_Android_onBeginEditing_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
anchorupAccessPIN.call(this);

 */
    }
    frmMBsetPasswd.hbox50285458140.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 2;
};

function p2kwiet2012247633010_txtTempAccess_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmMBsetPasswd.hbox50285458140.skin = "NoSkin"
        //popAccessPinBubble.dismiss()
};

function p2kwiet2012247633010_txtTempAccess_onDone_seq0(eventobject, changedtext) {
    //popAccessPinBubble.dismiss()
    frmMBsetPasswd.hbox50285458140.skin = "NoSkin"
};

function p2kwiet2012247633010_txtTempAccess_onTextChange_seq0(eventobject, changedtext) {
    var str = frmMBsetPasswd.txtTempAccess.text;
    //if (str != null && str.length >=1 ){
    // popAccessPinBubble.dismiss();
    //}
    //else {
    //popAccessPinBubble.show();
    //}
    frmMBsetPasswd.hbox50285458140.skin = "hboxBG"
};

function p2kwiet2012247633010_button474999996107_onClick_seq0(eventobject) {
    var deviceInfo = kony.os.deviceInfo();
    if (deviceInfo["name"] == "android") {
        helpMessage.containerHeight = 50;
    }
    helpMessage.label444732340761.text = kony.i18n.getLocalizedString("keyPwdHelpIcon")
};

function p2kwiet2012247633010_txtTransPass_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBsetPasswd.hbox50285458142.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 3;
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
anchorup.call(this);

 */
    }
};

function p2kwiet2012247633010_txtTransPass_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    popBubble.dismiss()
    frmMBsetPasswd.hbox50285458142.skin = "NoSkin"
};

function p2kwiet2012247633010_txtTransPass_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBsetPasswd.hbox50285458142.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 3;
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
popBubble.show()

 */
    }
};

function p2kwiet2012247633010_txtTransPass_Android_onEndEditing_seq0(eventobject, changedtext) {
    popBubble.dismiss()
    frmMBsetPasswd.hbox50285458142.skin = "NoSkin"
};

function p2kwiet2012247633010_txtTransPass_onDone_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
popBubble.dismiss();

 */
    }
    frmMBsetPasswd.hbox50285458142.skin = "NoSkin"
};

function p2kwiet2012247633010_txtTransPass_onTextChange_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        passwordChanged.call(this);
        /* 
var str = frmMBsetPasswd.txtTransPass.text;
if (str != null && str.length >=1 ){
 popBubble.dismiss();
}
else {
 popBubble.show();
}

 */
    }
    frmMBsetPasswd.hbox50285458142.skin = "hboxBG"
};

function p2kwiet2012247633010_txtTemp_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBsetPasswd.hbox50285458142.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 4;
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
anchorup.call(this);

 */
    }
};

function p2kwiet2012247633010_txtTemp_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    /* 
popBubble.dismiss()

 */
};

function p2kwiet2012247633010_txtTemp_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMBsetPasswd.hbox50285458142.skin = "hboxBG"
    if (isMenuShown) {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
    gblTxtFocusFlag = 4;
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
popBubble.show()

 */
    }
};

function p2kwiet2012247633010_txtTemp_Android_onEndEditing_seq0(eventobject, changedtext) {
    popBubble.dismiss()
    frmMBsetPasswd.hbox50285458142.skin = "NoSkin"
};

function p2kwiet2012247633010_txtTemp_onDone_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        trassactionPwdValidatn.call(this, frmMBsetPasswd.txtTemp.text);
        popBubble.dismiss()
    }
};

function p2kwiet2012247633010_txtTemp_onTextChange_seq0(eventobject, changedtext) {
    if (frmMBsetPasswd.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        passwordChanged.call(this);
        /* 
var str = frmMBsetPasswd.txtTemp.text;

if (str != null && str.length >=1 ){
 popBubble.dismiss();
}
else {
 popBubble.show();
}

 */
    }
    frmMBsetPasswd.hbox50285458142.skin = "hboxBG"
};

function p2kwiet2012247633010_button474999996108_onClick_seq0(eventobject) {
    var deviceInfo = kony.os.deviceInfo();
    if (deviceInfo["name"] == "android") {
        helpMessage.containerHeight = 50;
    }
    helpMessage.label444732340761.text = kony.i18n.getLocalizedString("keyTxnPwdHelpIcon")
};

function p2kwiet2012247633010_btnPwdOn_onClick_seq0(eventobject) {
    /* 
if(gblflag==0 && gblShowPwd>0){
 gblShowPwd=gblShowPwd-1;
 frmMBsetPasswd.txtTemp.text=frmMBsetPasswd.txtTransPass.text;
 frmMBsetPasswd.txtTempAccess.text=frmMBsetPasswd.txtAccessPwd.text;

 frmMBsetPasswd.txtTransPass.setVisibility(false);
 frmMBsetPasswd.txtAccessPwd.setVisibility(false);
 
 frmMBsetPasswd.txtTemp.setVisibility(true);
 frmMBsetPasswd.txtTempAccess.setVisibility(true);
 
 gblflag=1;
 
 frmMBsetPasswd.btnPwdOn.skin = btnOnNormal;
 frmMBsetPasswd.btnPwdOff.skin= btnOffNorm;
}

kony.timer.schedule("mytimer",timerFunc, 5, false);

 */
    gblTxtFocusFlag = 5;
    onClickOnBtn.call(this);
};

function p2kwiet2012247633010_btnPwdOff_onClick_seq0(eventobject) {
    /* 
if(gblflag==1 && gblShowPwd>0){
    gblShowPwd=gblShowPwd-1;
 frmMBsetPasswd.txtTransPass.text = frmMBsetPasswd.txtTemp.text;
 frmMBsetPasswd.txtAccessPwd.text = frmMBsetPasswd.txtTempAccess.text;
 
 frmMBsetPasswd.txtTemp.setVisibility(false);
 frmMBsetPasswd.txtTempAccess.setVisibility(false);
 
 frmMBsetPasswd.txtTransPass.setVisibility(true);
 frmMBsetPasswd.txtAccessPwd.setVisibility(true);
 
 gblflag=0;
 
 frmMBsetPasswd.btnPwdOn.skin = btnOnFocus;
 frmMBsetPasswd.btnPwdOff.skin= btnOffFocus;
}
 


 */
    gblTxtFocusFlag = 5;
    onClickOffBtn.call(this);
};

function p2kwiet2012247633010_txtDeviceName_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247633010_txtDeviceName_Android_onBeginEditing_seq0(eventobject, changedtext) {
    gblTxtFocusFlag = 0;
};

function p2kwiet2012247633010_button44726454989124_onClick_seq0(eventobject) {
    frmMBsetPasswd.txtTempAccess.skin = txtNormalBG;
    frmMBsetPasswd.txtAccessPwd.skin = txtNormalBG;
    frmMBsetPasswd.txtTransPass.skin = txtNormalBG
    frmMBsetPasswd.txtTemp.skin = txtNormalBG;
    if (isMenuShown == false) {
        mbSetPwdValidatn.call(this);
    } else {
        frmMBsetPasswd.scrollboxMain.scrollToEnd();
    }
    onClickOffBtn.call(this);
};

function p2kwiet2012247633066_frmMBSetuseridSPA_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633066_frmMBSetuseridSPA_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633066_frmMBSetuseridSPA_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633066_frmMBSetuseridSPA_preshow_seq0(eventobject, neworientation) {
    frmMBSetuseridSPAMenuPreshow.call(this);
};

function p2kwiet2012247633066_frmMBSetuseridSPA_postshow_seq0(eventobject, neworientation) {
    frmMBSetuseridSPAMenuPostshow.call(this);
};

function p2kwiet2012247633066_frmMBSetuseridSPA_onhide_seq0(eventobject, neworientation) {
    removeMenu.call(this);
};

function p2kwiet2012247633066_vbox50285458138_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633066_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633066_txtUserID_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    /* 
popupBubbleUserId.dismiss();
popupBubblePasswordRules.dismiss();

 */
};

function p2kwiet2012247633066_txtUserID_onTextChange_seq0(eventobject, changedtext) {
    /* 
var temp = frmMBSetuseridSPA.txtUserID.text;
if(temp.length != null && temp.length < 1){ 
 popupBubbleUserId.show();
 //alert("control here")
}
else {
 popupBubbleUserId.dismiss();
}

 */
};

function p2kwiet2012247633066_button507353026230982_onClick_seq0(eventobject) {
    onclickuseridButton.call(this);
};

function p2kwiet2012247633066_txtTransPass_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    /* 
kony.print("Inside the decision")
popupBubbleUserId.dismiss();
gblflag = 0
var temp = frmMBSetuseridSPA.txtTransPass.text;
if(temp.length != null && temp.length < 2){ 
 popupBubblePasswordRules.show();
 //alert("control here")
}
else {
 popupBubblePasswordRules.dismiss();
}

 */
};

function p2kwiet2012247633066_txtTransPass_Android_onBeginEditing_seq0(eventobject, changedtext) {};

function p2kwiet2012247633066_txtTransPass_onDone_seq0(eventobject, changedtext) {
    if (frmMBSetuseridSPA.lblHdrTxt.text != kony.i18n.getLocalizedString("keyConfirmPwd")) {
        /* 
popBubble.dismiss();

 */
    }
    frmMBSetuseridSPA.hbxPassword.skin = "NoSkin"
};

function p2kwiet2012247633066_txtTransPass_onTextChange_seq0(eventobject, changedtext) {
    /* 
kony.print("Inside the decision")
gblflag = 0
var temp = frmMBSetuseridSPA.txtTransPass.text;
if(temp.length != null && temp.length < 2){ 
 popupBubblePasswordRules.show();
 //alert("control here")
}
else {
 popupBubblePasswordRules.dismiss();
}

 */
    passwordChanged.call(this);
    frmMBSetuseridSPA.hbxPassword.skin = "hboxBG"
};

function p2kwiet2012247633066_button474999996108_onClick_seq0(eventobject) {
    hbox447443295153818.label474999996104.text = kony.i18n.getLocalizedString("keySPApassword");
    helpMessage.label444732340761.text = kony.i18n.getLocalizedString("keyIBPasswordGuidelines")
};

function p2kwiet2012247633066_txtConfirmPassword_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    popupBubbleUserId.dismiss();
    popupBubblePasswordRules.dismiss();
};

function p2kwiet2012247633066_txtConfirmPassword_onTextChange_seq0(eventobject, changedtext) {
    popupBubblePasswordRules.dismiss();
};

function p2kwiet2012247633066_button50368857830932_onClick_seq0(eventobject) {
    SpaUseridPasswordValidation.call(this);
};

function p2kwiet2012247633121_frmMBSoGooodConf_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633121_frmMBSoGooodConf_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633121_frmMBSoGooodConf_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633121_frmMBSoGooodConf_preshow_seq0(eventobject, neworientation) {
    frmMBSoGooodConfMenuPreshow.call(this);
};

function p2kwiet2012247633121_frmMBSoGooodConf_postshow_seq0(eventobject, neworientation) {
    frmMBSoGooodConfMenuPostshow.call(this);
};

function p2kwiet2012247633121_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633121_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633121_btnEditMenu_onClick_seq0(eventobject) {
    frmMBSoGooodPlanList.show();
};

function p2kwiet2012247633121_linkMoreHide_onClick_seq0(eventobject, context) {
    onClickMoreHideOnTnxConf.call(this);
};

function p2kwiet2012247633121_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickOfCancelSogoood.call(this);
    } else {
        frmMBSoGooodConf.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247633121_btnNext_onClick_seq0(eventobject) {
    confirmApplySoGoodTxns.call(this);
};

function p2kwiet2012247633176_frmMBSoGooodPlanList_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633176_frmMBSoGooodPlanList_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633176_frmMBSoGooodPlanList_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633176_frmMBSoGooodPlanList_preshow_seq0(eventobject, neworientation) {
    frmMBSoGooodPlanListMenuPreshow.call(this);
};

function p2kwiet2012247633176_frmMBSoGooodPlanList_postshow_seq0(eventobject, neworientation) {
    frmMBSoGooodPlanListMenuPostshow.call(this);
};

function p2kwiet2012247633176_vboxRight1_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633176_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633176_btnRight_onClick_seq0(eventobject) {
    loadRemainingSoGoooDTransactionsMB.call(this);
};

function p2kwiet2012247633176_hbxTransaction_onClick_seq0(eventobject, context) {
    /* 
deleteSelectedSoGoooDTransaction.call(this);

 */
};

function p2kwiet2012247633176_btnDel_onClick_seq0(eventobject, context) {
    deleteSelectedSoGoooDTransaction.call(this);
};

function p2kwiet2012247633176_linkMoreHide_onClick_seq0(eventobject, context) {
    showHideMoreDetailsOfTransaction.call(this);
};

function p2kwiet2012247633176_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickOfCancelSogoood.call(this);
    } else {
        frmMBSoGooodPlanList.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247633176_btnNext_onClick_seq0(eventobject) {
    onClickOfApplySogooodPlan.call(this);
};

function p2kwiet2012247633239_frmMBSoGooodPlanSelect_preshow_seq0(eventobject) {
    frmMBSoGooodPlanSelectMenuPreshow.call(this);
};

function p2kwiet2012247633239_btnMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633239_btnSelect1_onClick_seq0(eventobject) {
    OnNewPlanSelectService.call(this, eventobject);
};

function p2kwiet2012247633239_btnSelect2_onClick_seq0(eventobject) {
    OnNewPlanSelectService.call(this, eventobject);
};

function p2kwiet2012247633239_btnSelect3_onClick_seq0(eventobject) {
    OnNewPlanSelectService.call(this, eventobject);
};

function p2kwiet2012247633239_btnSoGooODBack_onClick_seq0(eventobject) {
    loadRemainingSoGoooDTransactions.call(this);
};

function p2kwiet2012247633260_frmMBSoGooodProdBrief_preshow_seq0(eventobject) {
    frmMBSoGooodProdBriefMenuPreshow.call(this);
};

function p2kwiet2012247633260_frmMBSoGooodProdBrief_postshow_seq0(eventobject) {
    assignGlobalForMenuPostshow.call(this);
};

function p2kwiet2012247633260_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633260_btnChk_onClick_seq0(eventobject) {
    onBtnCheck.call(this);
};

function p2kwiet2012247633260_lblagree2_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBSoGooodTnC.show();
};

function p2kwiet2012247633260_btnCancel_onClick_seq0(eventobject) {
    onClickOfBackSogoood.call(this);
};

function p2kwiet2012247633260_btnNext_onClick_seq0(eventobject) {
    showNextTnc.call(this);
};

function p2kwiet2012247633279_frmMBSoGooodTnC_preshow_seq0(eventobject) {
    frmMBSoGooodTnCMenuPreshow.call(this);
};

function p2kwiet2012247633279_frmMBSoGooodTnC_postshow_seq0(eventobject) {
    frmMBCashAdvanceTnCMenuPostshow.call(this);
};

function p2kwiet2012247633279_flexPdf_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.buttonPdf.skin = "btnPdfSCFoc";
    frmMBCashAdvanceTnC.flexPdf.skin = "flexLightBlueLine";
};

function p2kwiet2012247633279_flexPdf_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.flexPdf.skin = "flexWhiteBG";
    frmMBCashAdvanceTnC.buttonPdf.skin = "btnPdfSC";
    var pdfFlowType = "";
    onClickDownloadTnC.call(this, "pdf", "TMBCashAdvance");
};

function p2kwiet2012247633279_btnPDF_onClick_seq0(eventobject) {
    /* 
            var pdfFlowType = "";
       
    */
    /* 
onClickDownloadTnC.call(this,"pdf",    "TMBCashAdvance");

 */
    onClickDownloadTnC('pdf', 'TMBSoGooOD');
};

function p2kwiet2012247633279_flexEmail_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.buttonEmail.skin = "btnEmailSCFoc"
    frmMBCashAdvanceTnC.flexEmail.skin = "flexLightBlueLine";
};

function p2kwiet2012247633279_flexEmail_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    frmMBCashAdvanceTnC.flexEmail.skin = "flexWhiteBG";
    frmMBCashAdvanceTnC.buttonEmail.skin = "btnEmailSC"
    onClickEmailTnCMBCashAdvance.call(this);
};

function p2kwiet2012247633279_btnEmailto_onClick_seq0(eventobject) {
    /* 
onClickEmailTnCMBCashAdvance.call(this);

 */
    onClickEmailTnCApplySoGooOD.call(this);
};

function p2kwiet2012247633279_btnBack1_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        onClickOfCancelSogoood.call(this);
    } else {
        isMenuShown = false;
    }
};

function p2kwiet2012247633279_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633279_btnRight_onClick_seq0(eventobject) {};

function p2kwiet2012247633297_frmMBSoGooODTranasactions_preshow_seq0(eventobject) {
    frmMBSoGooODTranasactionsMenuPreshow.call(this);
};

function p2kwiet2012247633297_btnMenu2_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633297_lblSel_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    selectAllMB.call(this);
};

function p2kwiet2012247633297_lblCardName_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    selectAllMB.call(this);
};

function p2kwiet2012247633297_txtSearch_onTextChange_seq0(eventobject, changedtext) {
    searchSoGooODTransactions.call(this, isClicked = "false");
};

function p2kwiet2012247633297_segSoGooODTxns_onRowClick_seq0(eventobject, sectionNumber, rowNumber, selectedState) {
    selectSoGooODTransaction.call(this);
};

function p2kwiet2012247633297_btnSoGooODBack_onClick_seq0(eventobject) {
    performBackFromTransactionList.call(this);
};

function p2kwiet2012247633297_btnNext_onClick_seq0(eventobject) {
    gotoSoGooODTxnPlanDetails.call(this);
};

function p2kwiet2012247633315_frmMBSoGooODTranasactionsB_preshow_seq0(eventobject) {
    frmMBSoGooODTranasactionsMenuPreshow.call(this);
};

function p2kwiet2012247633315_btnMenu2_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633315_lblSel_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    selectAllMB.call(this);
};

function p2kwiet2012247633315_lblCardName_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    selectAllMB.call(this);
};

function p2kwiet2012247633315_txtSearch_onTextChange_seq0(eventobject, changedtext) {
    searchSoGooODTransactions.call(this, isClicked = "false");
};

function p2kwiet2012247633315_segSoGooODTxns_onRowClick_seq0(eventobject, sectionNumber, rowNumber, selectedState) {
    selectSoGooODTransaction.call(this);
};

function p2kwiet2012247633315_btnSoGooODBack_onClick_seq0(eventobject) {
    /* 
onClickBackOfDebitCardScreen.call(this);

 */
    frmMBSoGooodProdBrief.show();
};

function p2kwiet2012247633315_btnNext_onClick_seq0(eventobject) {
    gotoSoGooODTxnPlanDetails.call(this);
};

function p2kwiet2012247633342_frmMBSuccessCardNum_preshow_seq0(eventobject) {
    preShowfrmMBSuccessCardNum.call(this);
};

function p2kwiet2012247633342_btnMainMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633381_frmMBTnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633381_frmMBTnC_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633381_frmMBTnC_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633381_frmMBTnC_preshow_seq0(eventobject, neworientation) {
    frmMBTnCMenuPreshow.call(this);
};

function p2kwiet2012247633381_frmMBTnC_postshow_seq0(eventobject, neworientation) {
    frmMBTnCMenuPostshow.call(this);
};

function p2kwiet2012247633381_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633381_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633381_btnRight_onClick_seq0(eventobject) {
    var skin = frmMBTnC.btnRight.skin
    if (frmMBTnC.hboxSaveCamEmail.isVisible == false) {
        frmMBTnC.hboxSaveCamEmail.isVisible = true;
        frmMBTnC.imgHeaderRight.src = "arrowtop.png"
        frmMBTnC.imgHeaderMiddle.src = "empty.png"
        frmMBTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmMBTnC.hboxSaveCamEmail.isVisible = false;
        frmMBTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmMBTnC.imgHeaderRight.src = "empty.png"
        frmMBTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247633381_button47428498625_onClick_seq0(eventobject) {
    onClickDownloadPDF.call(this);
};

function p2kwiet2012247633381_btnEmailto_onClick_seq0(eventobject) {
    if (gblEmailTCSelect == false) {
        gblEmailTCSelect = true;
    } else {
        gblEmailTCSelect = false;
    }
    if (gblEmailTCSelect) {
        alert(kony.i18n.getLocalizedString("keyTermsAndConditionsPopUp"));
    }
    /* 
emailProcessForTC.call(this);

 */
};

function p2kwiet2012247633381_btnEngR_onClick_seq0(eventobject) {
    if (kony.i18n.getCurrentLocale() != "en_US") {
        gblLang_flag = "en_US";
        //gblLang_flag = "en_US";
        //kony.i18n.setCurrentLocaleAsync("en_US", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmMBTnC.lblTandCSpa.text = "";
        setLocaleEng();
        frmMBTnC.btnEngR.skin = btnOnFocus;
        frmMBTnC.btnThaiR.skin = btnOffFocus;
        frmMBTnCPreShow();
    }
};

function p2kwiet2012247633381_btnThaiR_onClick_seq0(eventobject) {
    if (kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH") {
        gblLang_flag = "th_TH";
        frmMBTnC.lblTandCSpa.text = "";
        setLocaleTH();
        //gblLang_flag = "th_TH";
        //kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmMBTnC.btnEngR.skin = btnOnNormal;
        frmMBTnC.btnThaiR.skin = btnOffNorm;
        frmMBTnCPreShow();
    }
};

function p2kwiet2012247633381_btnCancelSpa_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        frmSPALogin.show();
    } else {
        frmMBTnC.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247633381_btnAgreeSpa_onClick_seq0(eventobject) {
    frmMBActivation.show();
};

function p2kwiet2012247633381_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        var previousForm = kony.application.getPreviousForm();
        previousForm.show();
        //frmMBanking.hbox44538774217453.skin="hboxCmnDrkBlue";
    } else {
        frmMBTnC.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247633381_btnAgree_onClick_seq0(eventobject) {
    onClickTnCConfirm.call(this);
};

function p2kwiet2012247633448_frmMenu_postshow_seq0(eventobject) {
    setSwipeLanguageToggle.call(this);
};

function p2kwiet2012247633448_FlexScrollContainer0e763baec53954e_onScrollStart_seq0(eventobject) {
    disableBackButton.call(this);
};

function p2kwiet2012247633448_FlexScrollContainer0e763baec53954e_onScrolling_seq0(eventobject) {
    disableBackButton.call(this);
};

function p2kwiet2012247633448_FlexScrollContainer0e763baec53954e_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    disableBackButton.call(this);
};

function p2kwiet2012247633448_FlexScrollContainer0e763baec53954e_onTouchEnd_seq0(eventobject, x, y, contextInfo) {
    disableBackButton.call(this);
};

function p2kwiet2012247633448_FlexScrollContainer0e763baec53954e_onTouchMove_seq0(eventobject, x, y, contextInfo) {
    disableBackButton.call(this);
};

function p2kwiet2012247633448_flexContainer679966522394309_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onLogoutPopUpConfrm.call(this);
};

function p2kwiet2012247633448_CopyflexContainer0026e095806a74a_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onLogoutPopUpConfrm.call(this);
};

function p2kwiet2012247633448_btnMenuTransfer_onClick_seq0(eventobject) {
    transferFromMenu.call(this);
};

function p2kwiet2012247633448_btnMenuBillPay_onClick_seq0(eventobject) {
    callBillPaymentFromMenu.call(this);
};

function p2kwiet2012247633448_btnMenuTopUp_onClick_seq0(eventobject) {
    callTopUpFromMainMenu.call(this);
};

function p2kwiet2012247633448_btnMenuAccntSumry_onClick_seq0(eventobject) {
    showAccountSummaryFromMenu.call(this);
};

function p2kwiet2012247633448_btnMenuActivities_onClick_seq0(eventobject) {
    showFrmMBMyActivities.call(this);
};

function p2kwiet2012247633448_btnMenuOffers_onClick_seq0(eventobject) {
    gblMyOffersDetails = false;
    onClickPromotions();
};

function p2kwiet2012247633448_btnInbox_onClick_seq0(eventobject) {
    onClickInboxnew.call(this, null, null, null);
};

function p2kwiet2012247633448_segInbox_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickSegMenuInbox.call(this);
};

function p2kwiet2012247633448_btnMenuSettings_onClick_seq0(eventobject) {
    onClickSettingsnew.call(this, null, null, null);
};

function p2kwiet2012247633448_segSettings_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickSettingsSegMenu.call(this);
};

function p2kwiet2012247633448_btnMenuMore_onClick_seq0(eventobject) {
    onClickMorenew.call(this, null, null, null);
};

function p2kwiet2012247633448_segMore_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickMoresegMenu.call(this);
};

function p2kwiet2012247633448_btnEng_onClick_seq0(eventobject) {
    changeLocale.call(this, eventobject);
};

function p2kwiet2012247633448_btnThai_onClick_seq0(eventobject) {
    changeLocale.call(this, eventobject);
};

function p2kwiet2012247633448_FlexContainer014b6bd1f18284e_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onClickClosebuttonMenu.call(this);
};

function p2kwiet2012247633448_flexContainer1983951380373788_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onClickClosebuttonMenu.call(this);
};

function p2kwiet2012247633536_frmMFFullStatementMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633536_frmMFFullStatementMB_preshow_seq0(eventobject, neworientation) {
    frmMFFullStatementMBMenuPreshow.call(this);
};

function p2kwiet2012247633536_frmMFFullStatementMB_postshow_seq0(eventobject, neworientation) {
    frmMFFullStatementMBMenuPostshow.call(this);
};

function p2kwiet2012247633536_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633536_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633536_btnOptions_onClick_seq0(eventobject) {
    var skin = frmAccountStatementMB.btnOptions.skin
    if (frmAccountStatementMB.hbxOnHandClick.isVisible == false) {
        frmAccountStatementMB.hbxOnHandClick.isVisible = true;
        frmAccountStatementMB.imgHeaderRight.src = "arrowtop.png"
        frmAccountStatementMB.imgHeaderMiddle.src = "empty.png"
        frmAccountStatementMB.btnOptions.skin = "btnShareFoc";
    } else {
        frmAccountStatementMB.hbxOnHandClick.isVisible = false;
        frmAccountStatementMB.imgHeaderMiddle.src = "arrowtop.png"
        frmAccountStatementMB.imgHeaderRight.src = "empty.png"
        frmAccountStatementMB.btnOptions.skin = "btnShare";
    }
};

function p2kwiet2012247633536_btnpdf_onClick_seq0(eventobject) {
    savePDFMutualFundFullStatementServiceMB.call(this, "pdf", "MFFullStatementTemplate");
};

function p2kwiet2012247633536_linkactivity_onClick_seq0(eventobject) {
    onLinkMyActivitiesClickMB.call(this);
};

function p2kwiet2012247633536_btnunbilled_onClick_seq0(eventobject) {
    frmMFFullStatementMB.segcredit.removeAll();
    frmMFFullStatementMB.btnunbilled.skin = "btnunbilledfoc";
    frmMFFullStatementMB.btnunbilled.focusSkin = "btnunbilledfoc";
    frmMFFullStatementMB.btnbilled.skin = "btnunbilled";
    frmMFFullStatementMB.cmbMFDates.selectedKey = 0;
    frmMFFullStatementMB.hbxbilled.setVisibility(false);
    gblViewType = "F";
    currentpageStmt = 1;
    setSortBtnSkinMBMF();
    frmMBMFAcctFullStatementPreShow();
};

function p2kwiet2012247633536_btnbilled_onClick_seq0(eventobject) {
    frmMFFullStatementMB.segcredit.removeAll();
    frmMFFullStatementMB.btnunbilled.skin = "btnunbilled";
    frmMFFullStatementMB.btnbilled.skin = "btnunbilledfoc";
    frmMFFullStatementMB.hbxaccnt.setVisibility(false);
    frmMFFullStatementMB.hbxSegHeader.setVisibility(false);
    frmMFFullStatementMB.hbxOnHandClick.setVisibility(false);
    gblViewType = "O";
    onCCMonth1ClickMF();
    //startCCstatementservice("billed", "1");
};

function p2kwiet2012247633536_cmbMFDates_onSelection_seq0(eventobject) {
    onRowSelectMFMonth.call(this);
};

function p2kwiet2012247633536_segcredit_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    getdescriptionMFMB.call(this);
};

function p2kwiet2012247633536_linkMore_onClick_seq0(eventobject) {
    loadMoreData.call(this);
    if (isMenuShown == false) {
        /* 
onClickOfAccountDetailsMore.call(this);

 */
    } else {
        /* 
frmAccountDetailsMB.scrollboxMain.scrollToEnd();
isMenuShown=false;

 */
    }
};

function p2kwiet2012247633536_btnBack_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        /* 
onClickOfAccountDetailsBack.call(this);

 */
    } else {
        /* 
//kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
showLoadingScreen();
frmAccountDetailsMB.scrollboxMain.scrollToEnd();
isMenuShown=false;
kony.application.dismissLoadingScreen();

 */
    }
    //frmMBMutualFundDetails.hbxOnHandClick.isVisible = false;
    //frmMBMutualFundDetails.segcredit.removeAll();
    gblStmntSessionFlag = "2";
    //TMBUtil.DestroyForm(frmAccountStatementMB);
    frmMBMutualFundDetails.show();
    //frmAccountStatementMBDummy.show();
};

function p2kwiet2012247633569_frmMutualFundsSummaryLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633569_frmMutualFundsSummaryLanding_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633569_frmMutualFundsSummaryLanding_preshow_seq0(eventobject, neworientation) {
    frmMutualFundsSummaryLandingMenuPreshow.call(this);
};

function p2kwiet2012247633569_frmMutualFundsSummaryLanding_postshow_seq0(eventobject, neworientation) {
    frmMutualFundsSummaryLandingMenuPostshow.call(this);
};

function p2kwiet2012247633569_hboxRight_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633569_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633569_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633569_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633569_hbox475124774240_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633569_segAccountDetails_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    callMBMutualFundsDetails.call(this, frmMutualFundsSummaryLanding.segAccountDetails.selectedItems[0].lblunitHolderNumber, frmMutualFundsSummaryLanding.segAccountDetails.selectedItems[0].fundCode);
};

function p2kwiet2012247633569_btnBack1_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    showAccuntSummaryScreen();
};

function p2kwiet2012247633659_frmMyAccntAddAccount_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633659_frmMyAccntAddAccount_preshow_seq0(eventobject, neworientation) {
    frmMyAccntAddAccountMenuPreshow.call(this);
};

function p2kwiet2012247633659_frmMyAccntAddAccount_postshow_seq0(eventobject, neworientation) {
    frmMyAccntAddAccountMenuPostshow.call(this);
};

function p2kwiet2012247633659_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633659_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633659_btnBanklist_onClick_seq0(eventobject) {
    popBankList.lblFlag.text = "Bank"
    popBankList.segBanklist.setData(bankListArray);
};

function p2kwiet2012247633659_btnacntlist_onClick_seq0(eventobject) {
    popBankList.lblFlag.text = "Account";
    popBankList.segBanklist.setData(acctList);
};

function p2kwiet2012247633659_txtBxAccnt1_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    dontAllowNonNeumaric.call(this, txtBxAccnt1);
};

function p2kwiet2012247633659_txtBxAccnt1_SPA_onEndEditing_seq0(eventobject, changedtext) {
    dontAllowNonNeumaric.call(this, txtBxAccnt1);
};

function p2kwiet2012247633659_txtBxAccnt1_onTextChange_seq0(eventobject, changedtext) {
    depositAccntTextbox.call(this);
};

function p2kwiet2012247633659_txtBxAccnt2_onTextChange_seq0(eventobject, changedtext) {
    testfocusAccnt_two.call(this);
};

function p2kwiet2012247633659_txtBxAccnt3_onTextChange_seq0(eventobject, changedtext) {
    testfocusAccnt_three.call(this);
};

function p2kwiet2012247633659_txtBxAccnt4_onTextChange_seq0(eventobject, changedtext) {
    testfocusAccnt_four.call(this);
    testfocusAccnt_suffix.call(this);
};

function p2kwiet2012247633659_tbxSuffix_onTextChange_seq0(eventobject, changedtext) {
    testsuffix.call(this);
};

function p2kwiet2012247633659_textbx1_onTextChange_seq0(eventobject, changedtext) {
    fcsTxt.call(this);
};

function p2kwiet2012247633659_textbx2_onTextChange_seq0(eventobject, changedtext) {
    fcsTxt1.call(this);
};

function p2kwiet2012247633659_textbx3_onTextChange_seq0(eventobject, changedtext) {
    fcsTxt2.call(this);
};

function p2kwiet2012247633659_textbx4_onTextChange_seq0(eventobject, changedtext) {
    fcsTxt4.call(this);
};

function p2kwiet2012247633659_btnCancelSpa_onClick_seq0(eventobject) {
    /* 
showMyAccntList.call(this);

 */
    frmMyAccountList.show();
    TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
};

function p2kwiet2012247633659_btnAgreeSpa_onClick_seq0(eventobject) {
    swipeEnable = false;
    showConfirmationAddaccount.call(this);
};

function p2kwiet2012247633659_btnCancel_onClick_seq0(eventobject) {
    /* 
showMyAccntList.call(this);

 */
    var x = 0;
    if (frmMyAccntConfirmationAddAccount.segBankAccnt.data != null && frmMyAccntConfirmationAddAccount.segBankAccnt.data != undefined) {
        x = frmMyAccntConfirmationAddAccount.segBankAccnt.data.length;
    }
    kony.print("no accounts in bulk add value is" + x);
    if (x > 0) {
        frmMyAccntConfirmationAddAccount.show();
    } else {
        frmMyAccountList.show();
        TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
    }
};

function p2kwiet2012247633659_btnAgree_onClick_seq0(eventobject) {
    frmMyAccntAddAccount.tbxNickname.skin = txtNormalBG;
    frmMyAccntAddAccount.txtBxAccnt1.skin = txtNormalBG;
    frmMyAccntAddAccount.txtBxAccnt2.skin = txtNormalBG;
    frmMyAccntAddAccount.txtBxAccnt3.skin = txtNormalBG;
    frmMyAccntAddAccount.txtBxAccnt4.skin = txtNormalBG;
    swipeEnable = false;
    showConfirmationAddaccount.call(this);
};

function p2kwiet2012247633717_frmMyAccntConfirmationAddAccount_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633717_frmMyAccntConfirmationAddAccount_preshow_seq0(eventobject, neworientation) {
    frmMyAccntConfirmationAddAccountMenuPreshow.call(this);
};

function p2kwiet2012247633717_frmMyAccntConfirmationAddAccount_postshow_seq0(eventobject, neworientation) {
    frmMyAccntConfirmationAddAccountMenuPostshow.call(this);
};

function p2kwiet2012247633717_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633717_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633717_btnDelete_onClick_seq0(eventobject, context) {
    showDeletepop("delico.png", "Are you sure to delete this account ?", ConfirmBtnCache, CancelBtn)
};

function p2kwiet2012247633717_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247633717_btnCancelSpa_onClick_seq0(eventobject) {
    ftrCancelConfimation.call(this);
};

function p2kwiet2012247633717_btnAgreeSpa_onClick_seq0(eventobject) {
    /* 
ftrConfirm.call(this);

 */
    spaAddaccntToeknExchng.call(this);
    /* 
       

 /* 
if (gblIBFlowStatus == "04") {
   //showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
   popTransferConfirmOTPLock.show();
   kony.print("IB_USER_STATUS_ID is 04");
   //return false;
  }
else
{
var inputParams = {}
spaChnage="myacctsadd"
gblOTPFlag = true;
try {
 kony.timer.cancel("otpTimer")
}
catch (e) {
 kony.print("Timer doesnot exist");
}
//input params for SPA
gblSpaChannel="AddMyAccount";
if (kony.i18n.getCurrentLocale() == "en_US") {
        SpaEventNotificationPolicy = "MIB_AddMyAcc_EN";
        SpaSMSSubject = "MIB_AddMyAcc_EN";
} else {
        SpaEventNotificationPolicy = "MIB_AddMyAcc_TH";
        SpaSMSSubject = "MIB_AddMyAcc_TH";
}
gblAccountDetails = getAccountDetailMessageSpa(frmMyAccntConfirmationAddAccount.segBankAccnt.data);
onClickOTPRequestSpa();
}

 */
    * / 
};

function p2kwiet2012247633717_button473361467792207_onClick_seq0(eventobject) {
    ftrCancelConfimation.call(this);
};

function p2kwiet2012247633717_button473361467792209_onClick_seq0(eventobject) {
    myAccountListService.call(this);
    TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
    if (flowSpa) {
        frmMyAccntConfirmationAddAccount.hbox473361467792157.isVisible = true;
        frmMyAccntConfirmationAddAccount.hbox473361467792205.isVisible = false;
    } else {
        frmMyAccntConfirmationAddAccount.hbxcnfrmftr.isVisible = true;
        frmMyAccntConfirmationAddAccount.hbxftraddmore.isVisible = false;
    }
    // Clearing cache data
    NON_TMB_ADD = 0; // Non TMB accnt in cache - global var
    TOTAL_AC_ADD = 0; // Total accnt in cache - global var
    gblMyAccntAddTmpData = []; // Cache memory - global array
    //Navigate to add accnt form
    frmMyAccntAddAccount.show();
    //showfooter(kony.i18n.getLocalizedString("keyCancelButton"),kony.i18n.getLocalizedString("Next"),showMyAccntList,showConfirmationAddaccount);
};

function p2kwiet2012247633717_btnCancel_onClick_seq0(eventobject) {
    ftrCancelConfimation.call(this);
};

function p2kwiet2012247633717_btnAgree_onClick_seq0(eventobject) {
    /* 
ftrConfirm.call(this);

 */
    saveAddAccountDetals.call(this);
    var inputParams = {}
    spaChnage = "myacctsadd"
    gblOTPFlag = true;
    try {
        kony.timer.cancel("otpTimer")
    } catch (e) {
        kony.print("Timer doesnot exist");
    }
    //input params for SPA
    gblSpaChannel = "IB-NEW_RC_ADDITION";
    if (kony.i18n.getCurrentLocale() == "en_US") {
        SpaEventNotificationPolicy = "MIB_AddMyAcc_EN";
        SpaSMSSubject = "MIB_AddMyAcc_EN";
    } else {
        SpaEventNotificationPolicy = "MIB_AddMyAcc_TH";
        SpaSMSSubject = "MIB_AddMyAcc_TH";
    }
    onClickOTPRequestSpa();
};

function p2kwiet2012247633717_button450155142484043_onClick_seq0(eventobject) {
    ftrCancelConfimation.call(this);
};

function p2kwiet2012247633717_button450155142484045_onClick_seq0(eventobject) {
    myAccountListService.call(this);
    frmMyAccntConfirmationAddAccount.hbxftraddmore.setVisibility(false);
    frmMyAccntConfirmationAddAccount.hbxcnfrmftr.setVisibility(true);
    //
    TMBUtil.DestroyForm(frmMyAccntConfirmationAddAccount);
    // Clearing cache data
    NON_TMB_ADD = 0; // Non TMB accnt in cache - global var
    TOTAL_AC_ADD = 0; // Total accnt in cache - global var
    gblMyAccntAddTmpData = []; // Cache memory - global array
    //Navigate to add accnt form
    frmMyAccntAddAccount.show();
    //showfooter(kony.i18n.getLocalizedString("keyCancelButton"),kony.i18n.getLocalizedString("Next"),showMyAccntList,showConfirmationAddaccount);
};

function p2kwiet2012247633750_frmMyAccountEdit_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633750_frmMyAccountEdit_preshow_seq0(eventobject, neworientation) {
    frmMyAccountEditMenuPreshow.call(this);
};

function p2kwiet2012247633750_frmMyAccountEdit_postshow_seq0(eventobject, neworientation) {
    frmMyAccountEditMenuPostshow.call(this);
};

function p2kwiet2012247633750_frmMyAccountEdit_onhide_seq0(eventobject, neworientation) {
    removeMenu.call(this);
};

function p2kwiet2012247633750_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633750_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633750_txtEditAccntNickName_onDone_seq0(eventobject, changedtext) {
    accountNickNameMaxLength.call(this);
};

function p2kwiet2012247633750_tbxAccountName_onDone_seq0(eventobject, changedtext) {
    accountNickNameMaxLength.call(this);
};

function p2kwiet2012247633750_btnCancel_onClick_seq0(eventobject) {
    showfrmviewCancel.call(this);
};

function p2kwiet2012247633750_btnAgree_onClick_seq0(eventobject) {
    frmMyAccountEdit.txtEditAccntNickName.skin = txtNormalBG;
    //frmMyAccountView.imgcomplete.setVisibility(true);
    showfrmViewAccnt.call(this);
};

function p2kwiet2012247633781_frmMyAccountList_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633781_frmMyAccountList_preshow_seq0(eventobject, neworientation) {
    frmMyAccountListMenuPreshow.call(this);
};

function p2kwiet2012247633781_frmMyAccountList_postshow_seq0(eventobject, neworientation) {
    frmMyAccountListMenuPostshow.call(this);
};

function p2kwiet2012247633781_frmMyAccountList_onhide_seq0(eventobject, neworientation) {
    removeMenu.call(this);
};

function p2kwiet2012247633781_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633781_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633781_button450155142406426_onClick_seq0(eventobject) {
    if (checkMBUserStatus()) {
        frmMyAccntAddAccount.show();
    }
};

function p2kwiet2012247633781_segTMBAccntDetails_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    viewAccountTMB.call(this, frmMyAccountList.segTMBAccntDetails.selectedIndex[1], frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenAccountNo, eventobject["id"]);
    frmMyAccountView.imgcomplete.isVisible = false;
};

function p2kwiet2012247633781_segOtherBankAccntDetails_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    viewAccountTMB.call(this, frmMyAccountList.segOtherBankAccntDetails.selectedIndex[1], frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].hiddenAccountNo, eventobject["id"]);
    frmMyAccountView.imgcomplete.setVisibility(false);
};

function p2kwiet2012247633837_frmMyAccountView_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633837_frmMyAccountView_preshow_seq0(eventobject, neworientation) {
    frmMyAccountViewMenuPreshow.call(this);
};

function p2kwiet2012247633837_frmMyAccountView_postshow_seq0(eventobject, neworientation) {
    frmMyAccountViewMenuPostshow.call(this);
};

function p2kwiet2012247633837_frmMyAccountView_onhide_seq0(eventobject, neworientation) {
    removeMenu.call(this);
};

function p2kwiet2012247633837_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633837_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633837_btndelelteDepositAccnt_onClick_seq0(eventobject) {
    gblAcctNicName = frmMyAccountView.lblNickName.text;
    kony.print("gblAcctNicName is" + gblAcctNicName);
    gblAccNo = frmMyAccountView.lblAccntNoValue.text;
    kony.print("gblAccNo is" + gblAccNo);
    if (checkMBUserStatus()) {
        showDeletepop("delico.png", "", YesBtn, CancelBtn)
    }
};

function p2kwiet2012247633837_btnEditDepositAccnt_onClick_seq0(eventobject) {
    /* 
flow="myAccount";
accDetail="false";

 */
    if (gblSavingsCareFlow == "MyAccountFlow") {
        /* 
loadSavingsCareTermsNConditions.call(this);

 */
    } else {
        /* 
notSavingsCareMyAccountEdit.call(this);

 */
    }
    onMyAccountEdit.call(this);
};

function p2kwiet2012247633837_btnbackSpa_onClick_seq0(eventobject) {
    frmMyAccountList.show();
};

function p2kwiet2012247633837_btnBack_onClick_seq0(eventobject) {
    frmMyAccountList.show();
};

function p2kwiet2012247633925_frmMyProfile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633925_frmMyProfile_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633925_frmMyProfile_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633925_frmMyProfile_preshow_seq0(eventobject, neworientation) {
    frmMyProfileMenuPreshow.call(this);
};

function p2kwiet2012247633925_frmMyProfile_postshow_seq0(eventobject, neworientation) {
    frmMyProfileMenuPostshow.call(this);
};

function p2kwiet2012247633925_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633925_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247633925_hbox47502979411370_onClick_seq0(eventobject) {
    frmeditMyProfile.show();
};

function p2kwiet2012247633925_button47497021216166_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmeditMyProfile);
    editbuttonflag = "profile";
    getIBMBEditProfileStatus.call(this);
    /* 
frmeditMyProfile.show();
	
 */
};

function p2kwiet2012247633925_lnkExpand_onClick_seq0(eventobject) {
    /* 
var status = frmMyProfile.lnkExpand.text;
if(status=="More"){
frmMyProfile.lnlnkExpand.text="Hide";
frmMyProfile.lblContactAdd1.text=kony.i18n.getLocalizedString("keyContactAddress");
frmMyProfile.lblContactAdd2.text="Label";
frmMyProfile.lblContactAdd3.text="Label";
frmMyProfile.lblRegAddress1.text=kony.i18n.getLocalizedString("keyRegAddress");
frmMyProfile.lblRegAddress2.text="Label";
frmMyProfile.lblRegAddress3.text="Label";
frmMyProfile.hbxexpandAddr.setVisibility(true);
}
else{
frmMyProfile.hbxExpand.lnkExpand.text="More";
frmMyProfile.hbxexpandAddr.setVisibility(false);
}

 */
    onClickMoreMyProfile.call(this);
};

function p2kwiet2012247633925_btnSetID_onClick_seq0(eventobject) {
    onClickSetUrIdMyProfile.call(this);
};

function p2kwiet2012247633925_button47502979413638_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
    gblMobNoTransLimitFlag = true;
    editbuttonflag = "number";
    getIBMBEditProfileStatus.call(this);
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
};

function p2kwiet2012247633925_button47497021216176_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmCMChgAccessPin);
    gblChangePWDFlag = 0;
    //getHeader(mobileMethod, "Change Password" , 0,0,0);
    //frmAccTrcPwdInter.show();
    editbuttonflag = "pin";
    getIBMBEditProfileStatus.call(this);
};

function p2kwiet2012247633925_button104026732091045_onClick_seq0(eventobject) {
    if (gblIBFlowStatus == "04") {
        popTransferConfirmOTPLock.show();
        kony.print("IB_USER_STATUS_ID is 04");
        return false;
    } else {
        chngUseridspa = true
        if (spaCaptchaisVisible) {
            frmCMChgAccessPin.hboxCaptcha.setVisibility(true);
            frmCMChgAccessPin.hboxCaptchaText.setVisibility(true);
        } else {
            frmCMChgAccessPin.hboxCaptcha.setVisibility(false);
            frmCMChgAccessPin.hboxCaptchaText.setVisibility(false);
        }
    }
    frmCMChgAccessPin.show();
};

function p2kwiet2012247633925_button104026732090595_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmCMChgTransPwd);
    if (gblIBFlowStatus == "04") {
        popTransferConfirmOTPLock.show();
        kony.print("IB_USER_STATUS_ID is 04");
        return false;
    } else {
        chngUseridspa = false
        chngPwdspa = true
    }
    frmCMChgPwdSPA.show();
};

function p2kwiet2012247633925_button47497021216186_onClick_seq0(eventobject) {
    gblChangePWDFlag = 1;
    //getHeader(mobileMethod, "Change Password" , 0,0,0);
    //frmAccTrcPwdInter.show();
    editbuttonflag = "transpass";
    getIBMBEditProfileStatus.call(this);
};

function p2kwiet2012247633925_button47497021216196_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
    gblMobNoTransLimitFlag = false;
    editbuttonflag = "limit";
    getIBMBEditProfileStatus.call(this);
    /* 
frmChangeMobNoTransLimitMB.show();
	
 */
};

function p2kwiet2012247633925_hbxReq_onClick_seq0(eventobject) {
    requestHistoryMyProfile();
    frmRequestHistory.show();
};

function p2kwiet2012247633925_link508395714292481_onClick_seq0(eventobject) {
    viewRequestHistoryMB.call(this, 2);
};

function p2kwiet2012247633962_frmMyProfileReqHistory_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633962_frmMyProfileReqHistory_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247633962_frmMyProfileReqHistory_preshow_seq0(eventobject, neworientation) {
    frmMyProfileReqHistoryMenuPreshow.call(this);
};

function p2kwiet2012247633962_frmMyProfileReqHistory_postshow_seq0(eventobject, neworientation) {
    frmMyProfileReqHistoryMenuPostshow.call(this);
};

function p2kwiet2012247633962_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247633962_btnHdrMenu_onClick_seq0(eventobject) {
    //isMenuShown=false
    handleMenuBtn.call(this);
};

function p2kwiet2012247633962_combobox508395714279435_onSelection_seq0(eventobject) {
    /* 
sortedRequestDataMB.call(this);

 */
    viewRequestHistoryMB.call(this, frmMyProfileReqHistory.combobox508395714279435.selectedKey);
};

function p2kwiet2012247633962_button508395714293791_onClick_seq0(eventobject) {
    onClickMyProfileFlow.call(this);
};

function p2kwiet2012247634003_frmMyRecipientAddAcc_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634003_frmMyRecipientAddAcc_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634003_frmMyRecipientAddAcc_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddAccMenuPreshow.call(this);
};

function p2kwiet2012247634003_frmMyRecipientAddAcc_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddAccMenuPostshow.call(this);
};

function p2kwiet2012247634003_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634003_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634003_btnBanklist_onClick_seq0(eventobject) {
    getMBMyRecepientBankList.call(this);
};

function p2kwiet2012247634003_txtAccNo_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddAcc.txtAccNo.skin = "txtFocusBG";
};

function p2kwiet2012247634003_txtAccNo_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddAcc.txtAccNo.skin = "txtNormalBG";
};

function p2kwiet2012247634003_txtAccNo_onTextChange_seq0(eventobject, changedtext) {
    /* 
var accNoAsEntered = frmMyRecipientAddAcc.txtAccNo.text;

for(var i=0; i < (accNoAsEntered.length-1); i++)
 {
  if(accNoAsEntered[i] == '-'){
   accNoAsEntered = accNoAsEntered.replace("-","");
   
  } 
 }
if(accNoAsEntered.length<3){
 accNoWithDash=frmMyRecipientAddAcc.txtAccNo.text;
}
if(accNoAsEntered.length==3){
 accNoWithDash=accNoWithDash+"-";
}
if(accNoAsEntered.length==4){
 var sub1=accNoAsEntered.substr(0,3);
 var sub2=accNoAsEntered.substr(3,1);
 accNoWithDash=sub1+"-"+sub2+"-";
}
if(accNoAsEntered.length>4&&accNoAsEntered.length<9){
 accNoWithDash=accNoWithDash+accNoAsEntered.substr(5);
}
if(accNoAsEntered.length==9){
 var sub1=accNoAsEntered.substr(0,3);
 var sub2=accNoAsEntered.substr(3,1);
 var sub3=accNoAsEntered.substr(4,5);
 accNoWithDash=sub1+"-"+sub2+"-"+sub3+"-";
}
if(accNoAsEntered.length>9){
 accNoWithDash=accNoWithDash+accNoAsEntered.substr(10);
}
frmMyRecipientAddAcc.txtAccNo.text=accNoWithDash;

 */
    onEdittAccNum.call(this, frmMyRecipientAddAcc.txtAccNo.text);
    /* 
var lengthAccNo = null;
if(frmMyRecipientAddAcc.comboBank.selectedKey == "TMB1"){
 lengthAccNo = 13;
}
if(frmMyRecipientAddAcc.comboBank.selectedKey == "TMB2"){
 lengthAccNo = 14;
}
if(frmMyRecipientAddAcc.comboBank.selectedKey == "TMB3"){
 lengthAccNo = 15;
}
var lengtha = frmMyRecipientAddAcc.txtAccNo.text;
if(lengtha.length == lengthAccNo){
 frmMyRecipientAddAcc.txtNickname.setFocus(true);
}

 */
};

function p2kwiet2012247634003_txtNickname_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddAcc.txtNickname.skin = "txtFocusBG";
};

function p2kwiet2012247634003_txtNickname_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddAcc.txtNickname.skin = "txtNormalBG";
};

function p2kwiet2012247634003_tbxAccName_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddAcc.tbxAccName.skin = "txtFocusBG";
};

function p2kwiet2012247634003_tbxAccName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddAcc.tbxAccName.skin = "txtNormalBG";
};

function p2kwiet2012247634003_button4733076528913_onClick_seq0(eventobject) {
    frmMyRecipientAddAcc.btnBanklist.text = kony.i18n.getLocalizedString("keyBank");
    frmMyRecipientAddAcc.txtAccNo.text = null;
    frmMyRecipientAddAcc.txtNickname.text = null;
    if (AddAccountList.length > 0) {
        frmMyRecipientAddAccConf.show();
    } else {
        if (isRecipientNew) {
            gblRequestFromForm = "addaccount";
            frmMyRecipientAddProfile.show();
        } else {
            frmMyRecipientDetail.show();
        }
    }
};

function p2kwiet2012247634003_button4733076528925_onClick_seq0(eventobject) {
    frmMyRecipientAddAcc.txtNickname.skin = txtNormalBG;
    frmMyRecipientAddAcc.txtAccNo.skin = txtNormalBG;
    frmMyRecipientAddAcc.tbxAccName.skin = txtNormalBG;
    addNewAccountForReceipent.call(this);
};

function p2kwiet2012247634068_frmMyRecipientAddAccComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634068_frmMyRecipientAddAccComplete_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634068_frmMyRecipientAddAccComplete_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddAccCompleteMenuPreshow.call(this);
};

function p2kwiet2012247634068_frmMyRecipientAddAccComplete_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddAccCompleteMenuPostshow.call(this);
};

function p2kwiet2012247634068_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634068_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634068_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634068_hbox363582120525017_onClick_seq0(eventobject, context) {
    gblTransferFromRecipient = true
    getTransferFromAccounts.call(this);
};

function p2kwiet2012247634068_btnTransfer_onClick_seq0(eventobject, context) {
    gblTransferFromRecipient = true
    gbltransferFromForm = "frmMyRecipientAddAccComplete"
    getTransferFromAccounts.call(this);
};

function p2kwiet2012247634068_button47405242716713_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634068_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247634068_btnCancelSpa_onClick_seq0(eventobject) {
    frmMyRecipients.segMyRecipient.removeAll();
    frmMyRecipients.show();
};

function p2kwiet2012247634068_btnAgreeSpa_onClick_seq0(eventobject) {
    /* 
AddAccountList.length=0;

 */
    frmMyRecipientAddProfile.show();
    frmMyRecipientAddProfile.tbxRecipientName.text = "";
    frmMyRecipientAddProfile.tbxMobileNo.text = "";
    frmMyRecipientAddProfile.tbxEmail.text = "";
    frmMyRecipientAddProfile.tbxFbID.text = "";
    frmMyRecipientAddProfile.imgProfilePic.src = "avatar.png";
    frmMyRecipientsListing.call(this);
};

function p2kwiet2012247634068_btnCancel_onClick_seq0(eventobject) {
    gblAddMoreRcTrack = 0;
    gblAddMoreFromManualCache.length = 0;
    frmMyRecipients.show();
};

function p2kwiet2012247634068_btnAgree_onClick_seq0(eventobject) {
    AddAccountList.length = 0;
    frmMyRecipientAddProfile.tbxEmail.text = "";
    frmMyRecipientAddProfile.tbxFbID.text = "";
    frmMyRecipientAddProfile.tbxMobileNo.text = "";
    frmMyRecipientAddProfile.tbxRecipientName.text = "";
    frmMyRecipientAddProfile.imgProfilePic.src = "avatar.png";
    gblAddMoreRcTrack = gblAddMoreRcTrack + 1;
    if (gblAddMoreRcTrack + myRecipientsRs.length >= gblMAXRecipientCount) {
        alert99LimitReached.call(this);
    } else {
        gblAddMoreFromManualCache.push(frmMyRecipientAddAccComplete.lblName.text);
        myRecipientsRs.push({
            "lblName": {
                "text": frmMyRecipientAddAccComplete.lblName.text
            }
        })
        frmMyRecipientAddProfile.show();
    }
};

function p2kwiet2012247634132_frmMyRecipientAddAccConf_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634132_frmMyRecipientAddAccConf_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddAccConfMenuPreshow.call(this);
};

function p2kwiet2012247634132_frmMyRecipientAddAccConf_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    frmMyRecipientAddAccConf.scrollboxMain.scrollToEnd();
};

function p2kwiet2012247634132_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634132_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634132_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634132_btnAddAcc_onClick_seq0(eventobject) {
    if (AddAccountList.length >= MAX_BANK_ACCNT_BULK_ADD || (recipientAccountsRs.length + AddAccountList.length) >= gblMAXAccountCount) {
        //frmMyRecipientAddAccConf.btnAddAcc.onClick = null;
        if (flowSpa) {
            //frmMyRecipientAddAccConf.btnAddAcc.onClick = null;
        } else {}
    } else {
        //frmMyRecipientAddAccConf.btnAddAcc.onClick = btnAddAccOnclick;
        frmMyRecipientAddAcc.show();
    }
};

function p2kwiet2012247634132_button47408221857154_onClick_seq0(eventobject, context) {
    deleteAccountFromRecipientConfirmation.call(this);
};

function p2kwiet2012247634132_btnCancelSpa_onClick_seq0(eventobject) {
    if (recipientAddFromTransfer) {
        frmTransferLanding.show();
    } else {
        AddAccountList.length = 0;
        isChMyRecipientsRs = false;
        frmMyRecipients.show();
    }
};

function p2kwiet2012247634132_btnAgreeSpa_onClick_seq0(eventobject) {
    /* 
saveRecipientDeatilsMB.call(this);

 */
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        var inputParams = {}
        spaChnage = "addrecipients"
        gblFlagTransPwdFlow = "addAccount"
        gblOTPFlag = true;
        locale = kony.i18n.getCurrentLocale();
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {
            kony.print("Timer doesnot exist");
        }
        if (isRecipientNew) {
            gblSpaChannel = "AddRecipient";
        } else {
            gblSpaChannel = "EditRecipient";
        }
        if (isRecipientNew == false) {
            if (locale == "en_US") {
                SpaEventNotificationPolicy = "MIB_EditRecipient_EN";
                SpaSMSSubject = "MIB_EditRecipient_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_EditRecipient_TH";
                SpaSMSSubject = "MIB_EditRecipient_TH";
            }
        } else {
            if (locale == "en_US") {
                SpaEventNotificationPolicy = "MIB_AddRecipient_EN";
                SpaSMSSubject = "MIB_AddRecipient_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_AddRecipient_TH";
                SpaSMSSubject = "MIB_AddRecipient_TH";
            }
        }
        var mobNo = frmMyRecipientAddAccConf.lblMobile.text;
        if (mobNo != null && mobNo != "" && mobNo != undefined) {
            mobNo = kony.string.replace(mobNo, "-", "");
            gblAccountDetails = "Mobile x" + mobNo.substring(6, 10);
        }
        gblToSpaAccountName = frmMyRecipientAddAccConf.lblName.text;
        gblAccountDetails = getAccountDetailMessageSpa(frmMyRecipientAddAccConf.segMyRecipientDetail.data) + " " + gblAccountDetails;
        saveRecipientDeatilsMB.call(this);
    }
};

function p2kwiet2012247634132_btnCancel_onClick_seq0(eventobject) {
    if (recipientAddFromTransfer) {
        frmTransferLanding.show();
    } else {
        AddAccountList.length = 0;
        isChMyRecipientsRs = false;
        frmMyRecipients.show();
    }
};

function p2kwiet2012247634132_btnAgree_onClick_seq0(eventobject) {
    /* 
saveRecipientDeatilsMB.call(this);

 */
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        var inputParams = {}
        spaChnage = "addrecipients"
        gblOTPFlag = true;
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {
            kony.print("Timer doesnot exist");
        }
        gblSpaChannel = "IB-NEW_RC_ADDITION";
        if (locale == "en_US") {
            SpaEventNotificationPolicy = "MIB_AddRecipient_EN";
            SpaSMSSubject = "MIB_AddRecipient_EN";
        } else {
            SpaEventNotificationPolicy = "MIB_AddRecipient_TH";
            SpaSMSSubject = "MIB_AddRecipient_TH";
        }
        saveRecipientDeatilsMB();
    }
};

function p2kwiet2012247634182_frmMyRecipientAddProfile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634182_frmMyRecipientAddProfile_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634182_frmMyRecipientAddProfile_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddProfileMenuPreshow.call(this);
};

function p2kwiet2012247634182_frmMyRecipientAddProfile_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddProfileMenuPostshow.call(this);
};

function p2kwiet2012247634182_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634182_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634182_vbox475893575411221_onClick_seq0(eventobject) {};

function p2kwiet2012247634182_tbxRecipientName_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddProfile.tbxRecipientName.skin = "txtFocusBG";
};

function p2kwiet2012247634182_tbxRecipientName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddProfile.tbxRecipientName.skin = "txt142";
};

function p2kwiet2012247634182_tbxRecipientName_onDone_seq0(eventobject, changedtext) {
    var ch = charCapitalize(frmMyRecipientAddProfile.tbxRecipientName.text);
    frmMyRecipientAddProfile.tbxRecipientName.text = ch.replace(/^\s+|\s+$/g, "");
};

function p2kwiet2012247634182_btnMobileNumber_onClick_seq0(eventobject) {
    requestFromForm = "addprofile";
    gblAddContactFlow = true;
    checkContactPermissionsInAddProfile.call(this);
    /* 
frmMyRecipientSelectMobile.show();
	
 */
};

function p2kwiet2012247634182_btnFB_onClick_seq0(eventobject) {
    frmMyRecipientAddProfile.hbox474136033103966.isVisible = true;
    frmMyRecipientAddProfile.line474136033132274.isVisible = true;
    requestFromForm = "addprofile";
    frmMyRecipientSelectFbID.show();
};

function p2kwiet2012247634182_tbxMobileNo_onTextChange_seq0(eventobject, changedtext) {
    onEditMobileNumberRecp(frmMyRecipientAddProfile.tbxMobileNo.text);
};

function p2kwiet2012247634182_tbxEmail_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddProfile.tbxEmail.skin = "txtFocusBG";
};

function p2kwiet2012247634182_tbxEmail_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientAddProfile.tbxEmail.skin = "txtNormalBG";
};

function p2kwiet2012247634182_tbxEmail_onTextChange_seq0(eventobject, changedtext) {
    if (frmMyRecipientAddProfile.tbxEmail.text.trim() == "") {
        frmMyRecipientAddProfile.tbxEmail.text = "";
    }
};

function p2kwiet2012247634182_button59324032018935_onClick_seq0(eventobject) {
    AddAccountList.length = 0;
    addNewRecipientAddNewAcc.call(this, eventobject);
};

function p2kwiet2012247634182_link59324032018947_onClick_seq0(eventobject) {
    AddAccountList.length = 0;
    addNewRecipientAddNewAcc.call(this, eventobject);
};

function p2kwiet2012247634182_lnkAddBankAcc_onClick_seq0(eventobject) {
    AddAccountList.length = 0;
    addNewRecipientAddNewAcc.call(this, eventobject);
};

function p2kwiet2012247634182_btnNext_onClick_seq0(eventobject) {
    frmMyRecipientAddProfile.tbxRecipientName.skin = txtNormalBG;
    frmMyRecipientAddProfile.tbxMobileNo.skin = txtNormalBG;
    frmMyRecipientAddProfile.tbxEmail.skin = txtNormalBG;
    frmMyRecipientAddProfile.tbxFbID.skin = txtNormalBG;
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        AddAccountList.length = 0;
        addNewRecipientAddNewAcc.call(this, eventobject);
    }
};

function p2kwiet2012247634223_frmMyRecipientAddProfileComp_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634223_frmMyRecipientAddProfileComp_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddProfileCompPreShow.call(this);
    gblIndex = -1;
    isMenuShown = false;
    isSignedUser = true;
    DisableFadingEdges.call(this, frmMyRecipientAddProfileComp);
};

function p2kwiet2012247634223_frmMyRecipientAddProfileComp_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientAddProfileCompMenuPostshow.call(this);
};

function p2kwiet2012247634223_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634223_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634223_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634223_button101086657957077_onClick_seq0(eventobject) {
    /* 
if(gbltranRecip==1)
 getToRecipients();
else
 frmMyRecipientAddProfile.show()




 */
    /* 
onClickConfirmAddRcManually.call(this);

 */
    frmMyRecipients.show();
};

function p2kwiet2012247634290_frmMyRecipientDetail_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634290_frmMyRecipientDetail_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634290_frmMyRecipientDetail_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientDetailMenuPreshow.call(this);
};

function p2kwiet2012247634290_frmMyRecipientDetail_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientDetailMenuPostshow.call(this);
};

function p2kwiet2012247634290_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634290_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634290_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634290_button47327209489401_onClick_seq0(eventobject) {
    //popDelRecipientProfile.btnPopDeleteCancel.onClick = deleteRecipientCancel;
    //popDelRecipientProfile.btnPopDeleteYEs.onClick = deleteRecipientConfirm;
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    }
};

function p2kwiet2012247634290_button47327209489402_onClick_seq0(eventobject) {
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        gblFlagTransPwdFlow = "editProfile";
        onclickeditinRecipientDetail.call(this);
    }
};

function p2kwiet2012247634290_btnAddAcc_onClick_seq0(eventobject) {
    /* 
currentRecipient = frmMyRecipientDetail.lblName.text;
isRecipientNew = false;

 */
    /* 
frmMyRecipientAddAcc.show();
	
 */
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        var canAddMoreAcc = null;
        if (recipientAccountsRs.length >= gblMAXAccountCount) {
            //frmMyRecipientDetail.btnAddAcc.onClick = null;
            canAddMoreAcc = false;
        } else {
            canAddMoreAcc = true;
        }
        AddAccountList.length = 0;
        if (canAddMoreAcc) {
            //currentRecipient = frmMyRecipientDetail.lblName.text;
            isRecipientNew = false;
            frmMyRecipientAddAcc.show();
        }
    }
};

function p2kwiet2012247634290_segMyRecipientDetail_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    frmMyRecipientViewAccount.lblBankName.text = frmMyRecipientDetail["segMyRecipientDetail"]["selectedItems"][0]["lblBankName"]["text"];
    frmMyRecipientViewAccount.bankLogo.src = frmMyRecipientDetail["segMyRecipientDetail"]["selectedItems"][0]["bankLogo"]["src"];
    frmMyRecipientViewAccount.lblAccountNo.text = frmMyRecipientDetail["segMyRecipientDetail"]["selectedItems"][0]["lblAccountNo"]["text"];
    frmMyRecipientViewAccount.lblNick.text = frmMyRecipientDetail["segMyRecipientDetail"]["selectedItems"][0]["lblNick"]["text"];
    frmMyRecipientViewAccount.lblAcctName.text = frmMyRecipientDetail["segMyRecipientDetail"]["selectedItems"][0]["lblAcctName"]["text"];
    frmMyRecipientViewAccount.show();
};

function p2kwiet2012247634290_hbox47327209471043_onClick_seq0(eventobject, context) {
    setAccountAsFavoriteMB.call(this);
};

function p2kwiet2012247634290_btnFav_onClick_seq0(eventobject, context) {
    setAccountAsFavoriteMB.call(this);
};

function p2kwiet2012247634290_btnTransfer_onClick_seq0(eventobject, context) {
    onTransferShortcutClick.call(this);
};

function p2kwiet2012247634290_button474076451737588_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634290_button101086657957077_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634343_frmMyRecipientEditAccComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634343_frmMyRecipientEditAccComplete_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634343_frmMyRecipientEditAccComplete_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientEditAccCompleteMenuPreshow.call(this);
};

function p2kwiet2012247634343_frmMyRecipientEditAccComplete_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientEditAccCompleteMenuPostshow.call(this);
};

function p2kwiet2012247634343_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634343_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634343_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634343_button473272094894041_onClick_seq0(eventobject) {
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        deleteAccountFromRecipientView.call(this);
    }
};

function p2kwiet2012247634343_button473272094894051_onClick_seq0(eventobject) {
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        frmMyRecipientEditAccount.txtAccountNickName.text = frmMyRecipientEditAccComplete.lblNick.text
        frmMyRecipientEditAccount.show();
    }
};

function p2kwiet2012247634343_button47327209489401_onClick_seq0(eventobject) {
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        deleteAccountFromRecipientView.call(this);
    }
};

function p2kwiet2012247634343_button47327209489405_onClick_seq0(eventobject) {
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        frmMyRecipientEditAccount.txtAccountNickName.text = frmMyRecipientEditAccComplete.lblNick.text
        frmMyRecipientEditAccount.show();
    }
};

function p2kwiet2012247634343_btnTransfer_onClick_seq0(eventobject) {
    gblTransferFromRecipient = true;
    gbltransferFromForm = "frmMyRecipientEditAccComplete";
    getTransferFromAccounts.call(this);
};

function p2kwiet2012247634343_button101086657957077_onClick_seq0(eventobject) {
    frmMyRecipientDetail.show();
};

function p2kwiet2012247634373_frmMyRecipientEditAccount_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634373_frmMyRecipientEditAccount_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634373_frmMyRecipientEditAccount_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientEditAccountMenuPreshow.call(this);
};

function p2kwiet2012247634373_frmMyRecipientEditAccount_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    frmMyRecipientEditAccount.scrollboxMain.scrollToEnd();
};

function p2kwiet2012247634373_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634373_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634373_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634373_txtAccountNickName_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditAccount.txtAccountNickName.skin = "txtFocusBG";
};

function p2kwiet2012247634373_txtAccountNickName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditAccount.txtAccountNickName.skin = "tbxPopupBlue";
};

function p2kwiet2012247634373_tbxAccName_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditAccount.txtAccountNickName.skin = "txtFocusBG";
};

function p2kwiet2012247634373_tbxAccName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditAccount.txtAccountNickName.skin = "tbxPopupBlue";
};

function p2kwiet2012247634373_btnCancel_onClick_seq0(eventobject) {
    /* 
originalAccNickname=false;

 */
    frmMyRecipientViewAccount.show();
};

function p2kwiet2012247634373_btnAgree_onClick_seq0(eventobject) {
    /* 
var success=false;
//var recipientNickname =/^[a-zA-Z0-9]{3,20}$/ig;
var resultRecipientNickname =ThaiOrEnglish(frmMyRecipientEditAccount.txtAccountNickName.text);
 
if(!(resultRecipientNickname)){
 alert("Recipient Nickname Invalid");
 success=false;
}
else{
 success=true;
 //add logic to check if recipient nickname is unique
 isChRecipientAccountsRs=true;
 for(var i in recipientAccountsRs){
  if(recipientAccountsRs[i]["lblAccountNickname"]==originalAccNickname){
   recipientAccountsRs[i]["lblAccountNickname"]=frmMyRecipientEditAccount.txtAccountNickName.text;
  }
 }
 originalAccNickname=false;
 frmMyRecipientViewAccount.lblNick.text=frmMyRecipientEditAccount.txtAccountNickName.text;
 frmMyRecipientViewAccount.show();
 alert("Edit Recipient account process has been completed");
}
   

 */
    frmMyRecipientEditAccount.txtAccountNickName.skin = txtNormalBG;
    onClickeditAccountConfirm.call(this);
};

function p2kwiet2012247634417_frmMyRecipientEditProfile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634417_frmMyRecipientEditProfile_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634417_frmMyRecipientEditProfile_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientEditProfileMenuPreshow.call(this);
};

function p2kwiet2012247634417_frmMyRecipientEditProfile_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientEditProfileMenuPostshow.call(this);
};

function p2kwiet2012247634417_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634417_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634417_vbox4733076528532_onClick_seq0(eventobject) {};

function p2kwiet2012247634417_tbxRecipientName_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditProfile.tbxRecipientName.skin = "txtFocusBG";
};

function p2kwiet2012247634417_tbxRecipientName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditProfile.tbxRecipientName.skin = "tbx142";
};

function p2kwiet2012247634417_tbxRecipientName_onDone_seq0(eventobject, changedtext) {
    var ch = charCapitalize(frmMyRecipientEditProfile.tbxRecipientName.text);
    frmMyRecipientEditProfile.tbxRecipientName.text = ch.replace(/^\s+|\s+$/g, "");;
};

function p2kwiet2012247634417_btnFaceBook_onClick_seq0(eventobject) {
    requestFromForm = "editprofile";
    checkContactPermissionsInAddProfile.call(this);
    /* 
frmMyRecipientSelectMobile.show();
	
 */
};

function p2kwiet2012247634417_button47327209489408_onClick_seq0(eventobject) {
    requestFromForm = "editprofile";
    frmMyRecipientSelectFbID.show();
};

function p2kwiet2012247634417_tbxMobileNo_onTextChange_seq0(eventobject, changedtext) {
    onEditMobileNumberRecp(frmMyRecipientEditProfile.tbxMobileNo.text)
};

function p2kwiet2012247634417_tbxEmail_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditProfile.tbxEmail.skin = "txtFocusBG";
};

function p2kwiet2012247634417_tbxEmail_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyRecipientEditProfile.tbxEmail.skin = "txtNormalBG";
};

function p2kwiet2012247634417_tbxEmail_onTextChange_seq0(eventobject, changedtext) {
    if (frmMyRecipientEditProfile.tbxEmail.text.trim() == "") {
        frmMyRecipientEditProfile.tbxEmail.text = "";
    }
};

function p2kwiet2012247634417_btnCancel_onClick_seq0(eventobject) {
    /* 
originalAccNickname=false;

 */
    frmMyRecipients.show();
};

function p2kwiet2012247634417_button447478018365122_onClick_seq0(eventobject) {
    /* 
var success=false;

var recipientName =/^[a-zA-Z\s]{1,40}$/ig;
 var resultRecipientName = recipientName.test(frmMyRecipientEditProfile.tbxRecipientName.text);
 
 if(frmMyRecipientEditProfile.tbxMobileNo.text=="" || frmMyRecipientEditProfile.tbxMobileNo.text==null){
  resultRecipientMobile=true;
 }else{
  var recipientMobile = /^[0-9]{10}$/g;
  var resultRecipientMobile = recipientMobile.test(frmMyRecipientEditProfile.tbxMobileNo.text);
 }
 
 if(frmMyRecipientEditProfile.tbxEmail.text=="" || frmMyRecipientEditProfile.tbxEmail.text==null){
  resultRecipientEmail=true;
 }else{
  var resultRecipientEmail = emailValidatn(frmMyRecipientEditProfile.tbxEmail.text);
 }
 
 if(!(resultRecipientName && resultRecipientEmail && resultRecipientMobile)){
  alert("Recipient Name:"+resultRecipientName+"\nRecipient Mobile"+resultRecipientMobile+"\nRecipient Email:"+resultRecipientEmail);
  success=false;
 }else{
  success=true;
 }
  
 

 */
    /* 
var success=false;

var recipientName = recipientNameVal(frmMyRecipientEditProfile.tbxRecipientName.text);
var recipientMobile = null;
var recipientEmail = null;

if(frmMyRecipientEditProfile.tbxMobileNo.text=="" || frmMyRecipientEditProfile.tbxMobileNo.text==null){
 recipientMobile=true;
}else{
 var ph = frmMyRecipientEditProfile.tbxMobileNo.text;
 ph = stripDashPh(ph);
 recipientMobile = recipientMobileVal(ph);
}

if(frmMyRecipientEditProfile.tbxEmail.text=="" || frmMyRecipientEditProfile.tbxEmail.text==null){
 recipientEmail=true;
}else{
 if(recipientMobileVal(frmMyRecipientEditProfile.tbxMobileNo.text)==false){
  recipientEmail=false;
 }else{
  recipientEmail=true;
 }
} 
 
if(!(recipientName && recipientEmail && recipientMobile)){
 var i=0,j=0,k=0;
 if(recipientName==false){
  i="Recipient Name Invalid\n";
 }else{
  i="";
 }
 if(recipientEmail==false){
  j="Recipient Email Invalid\n";
 }else{
  j="";
 }
 if(recipientMobile==false){
  i="Recipient Mobile Invalid\n";
 }else{
  k="";
 }
 alert(i + j + k);
 success=false;
}else{
 success=true; 
}

 */
    frmMyRecipientEditProfile.tbxRecipientName.skin = txtNormalBG;
    frmMyRecipientEditProfile.tbxEmail.skin = txtNormalBG;
    frmMyRecipientEditProfile.tbxFbID.skin = txtNormalBG;
    /* 
saveRecipientDeatilsMB.call(this);

 */
    if (gblIBFlowStatus == "04") {
        alertUserStatusLocked.call(this);
    } else {
        onClickeditProfileConfirm.call(this);
    }
};

function p2kwiet2012247634458_frmMyRecipientEditProfileComp_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634458_frmMyRecipientEditProfileComp_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientEditProfileCompMenuPreshow.call(this);
};

function p2kwiet2012247634458_frmMyRecipientEditProfileComp_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
};

function p2kwiet2012247634458_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634458_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634458_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634458_button101086657957077_onClick_seq0(eventobject) {
    frmMyRecipientDetail.show();
};

function p2kwiet2012247634505_frmMyRecipients_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247634505_frmMyRecipients_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634505_frmMyRecipients_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientsMenuPreshow.call(this);
};

function p2kwiet2012247634505_frmMyRecipients_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientsMenuPostshow.call(this);
};

function p2kwiet2012247634505_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634505_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634505_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
    if (myRecipientsRs.length == gblMAXRecipientCount) {
        //frmMyRecipients.button47428498625.setEnabled(false);
        frmMyRecipients.btnEmailto.setEnabled(false);
        frmMyRecipients.btnPhoto.setEnabled(false);
    } else {
        //frmMyRecipients.button47428498625.setEnabled(true);
        frmMyRecipients.btnEmailto.setEnabled(true);
        frmMyRecipients.btnPhoto.setEnabled(true);
    }
    /* 
frmMyRecipientSelectContacts.show();
	
 */
};

function p2kwiet2012247634505_button47428498625_onClick_seq0(eventobject) {
    checkNoOfRecipientsMax.call(this, eventobject);
    gblAddMoreRcTrack = 0;
};

function p2kwiet2012247634505_btnPhoto_onClick_seq0(eventobject) {
    checkNoOfRecipientsMax.call(this, eventobject);
    gblAddMoreRcTrack = 0;
};

function p2kwiet2012247634505_btnEmailto_onClick_seq0(eventobject) {
    checkNoOfRecipientsMax.call(this, eventobject);
    gblAddMoreRcTrack = 0;
};

function p2kwiet2012247634505_btnSearchRecipeints_onClick_seq0(eventobject) {
    searchForMyRecipient.call(this, eventobject);
};

function p2kwiet2012247634505_textbox247327209467957_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipients.textbox247327209467957.skin = "txtSearchboxBigFont";
    frmMyRecipients.textbox247327209467957.text = ""
};

function p2kwiet2012247634505_textbox247327209467957_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    /* 
frmMyRecipients.textbox247327209467957.skin="txtSearchboxBigFont";
frmMyRecipients.textbox247327209467957.text=""

 */
};

function p2kwiet2012247634505_textbox247327209467957_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipients.textbox247327209467957.skin = "txtSearchboxBigFont";
    frmMyRecipients.textbox247327209467957.text = "";
};

function p2kwiet2012247634505_textbox247327209467957_onDone_seq0(eventobject, changedtext) {
    searchForMyRecipient.call(this, eventobject);
};

function p2kwiet2012247634505_textbox247327209467957_onTextChange_seq0(eventobject, changedtext) {
    searchForMyRecipient.call(this, eventobject);
};

function p2kwiet2012247634505_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634505_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634505_segMyRecipient_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    frmMyRecipientDetail.lblName.text = "";
    frmMyRecipientDetail.lblEmail.text = "";
    frmMyRecipientDetail.lblMobile.text = "";
    frmMyRecipientDetail.lblFbstudio3.text = "";
    frmMyRecipientDetail.lblFBstudio4.text = "";
    frmMyRecipientDetail.label47502979411852.text = "";
    frmMyRecipientDetail.label47502979412689.text = "";
    frmMyRecipientDetail.imgprofilepic.src = "";
    frmMyRecipientDetail.segMyRecipientDetail.data = {};
    isChMyRecipientsRs = false;
    populateCurrentRecipientCache.call(this);
    frmMyRecipientDetail.show();
};

function p2kwiet2012247634505_btnFav_onClick_seq0(eventobject, context) {
    /* 
var items = frmMyRecipients.segMyRecipient.selectedItems;
kony.print(items);
var row = frmMyRecipients.segMyRecipient.selectedIndex;
var length = RecipientListFav.length;
if (items[0]["btnFav"]["skin"] == "btnStar") {
    var tempRecord = {
        "lblName": {
            "text": items[0]["lblName"]["text"]
        },
        "btnFav": {
            "skin": "btnStarFoc"
        },
        "imgfb": {
            "src": items[0]["imgfb"]["src"]
        },
        "imgemail": {
            "src": items[0]["imgemail"]["src"]
        },
        "imgmob": {
            "src": items[0]["imgmob"]["src"]
        },
        "imgArrow": {
            "src": "navarrow3.png"
        },
        "lblAccountNum": {
            "text": items[0]["lblAccountNum"]["text"]
        },
        "imgprofilepic": {
            "src": items[0]["imgprofilepic"]["src"]
        }
    }
    var index = row[1] - length;
    RecipientList.splice(index, 1);
    RecipientListFav.push(tempRecord);
    RecipientList.sort(dynamicSort("lblName"));
    RecipientListFav.sort(dynamicSort("lblName"));

    frmMyRecipients.segMyRecipient.removeAll();
    frmMyRecipients.segMyRecipient.setData(RecipientListFav);
    frmMyRecipients.segMyRecipient.addAll(RecipientList);
    frmMyRecipients.show();

} else {
    var tempRecord = {
        "lblName": {
            "text": items[0]["lblName"]["text"]
        },
        "btnFav": {
            "skin": "btnStar"
        },
        "imgfb": {
            "src": items[0]["imgfb"]["src"]
        },
        "imgemail": {
            "src": items[0]["imgemail"]["src"]
        },
        "imgmob": {
            "src": items[0]["imgmob"]["src"]
        },
        "imgArrow": {
            "src": "navarrow3.png"
        },
        "lblAccountNum": {
            "text": items[0]["lblAccountNum"]["text"]
        },
        "imgprofilepic": {
            "src": items[0]["imgprofilepic"]["src"]
        }
    }
    RecipientListFav.splice(row[1], 1);
    RecipientList.push(tempRecord);
    RecipientListFav.sort(dynamicSort("lblName"));
    RecipientList.sort(dynamicSort("lblName"));

    frmMyRecipients.segMyRecipient.removeAll();
    frmMyRecipients.segMyRecipient.setData(RecipientListFav);
    frmMyRecipients.segMyRecipient.addAll(RecipientList);
    frmMyRecipients.show();
    
}

 */
    setReceipentAsFavoriteMB.call(this);
};

function p2kwiet2012247634553_frmMyRecipientSelectContacts_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634553_frmMyRecipientSelectContacts_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectContactsMenuPreshow.call(this);
};

function p2kwiet2012247634553_frmMyRecipientSelectContacts_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectContactsMenuPostshow.call(this);
};

function p2kwiet2012247634553_vbox475959051238712_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634553_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634553_btnSearchRecipeints_onClick_seq0(eventobject) {
    var search = frmMyRecipientSelectContacts.textbox286685406430472.text
    frmMyRecipientSelectContacts.label475095112172022.setVisibility(false);
    frmMyRecipientSelectContacts.segMyRecipient.setVisibility(true);
    if (search != null && search != "") {
        if (kony.string.containsChars(frmMyRecipientSelectContacts.textbox286685406430472.text, charsArr)) {
            alert(kony.i18n.getLocalizedString("InvalidSearch"));
            return;
        }
    }
    var manualSearch = false;
    if (eventobject.id == "btnSearchRecipeints") {
        manualSearch = true;
    }
    if (search.length >= 3 || manualSearch) {
        var showList = new Array();
        var searchtxt = search;
        var j = 0;
        var regexp = new RegExp("(" + searchtxt + ")", "ig");
        for (var i = 0; i < ContactList1.length; i++) {
            if (ContactList1[i]["lblName"]) {
                if (regexp.test(ContactList1[i]["lblName"]) == true) {
                    showList[j] = ContactList1[i];
                    j++;
                }
            }
        }
        if (showList.length == 0 || showList.length == undefined) {
            frmMyRecipientSelectContacts.label475095112172022.setVisibility(true);
            frmMyRecipientSelectContacts.segMyRecipient.setVisibility(false);
            frmMyRecipientSelectContacts.label475095112172022.text = kony.i18n.getLocalizedString("keybillernotfound");
            //showAlertRcMB(kony.i18n.getLocalizedString("keybillernotfound"), kony.i18n.getLocalizedString("info"), "info")
        } else {
            frmMyRecipientSelectContacts.segMyRecipient.setData(showList);
        }
    } else {
        frmMyRecipientSelectContacts.segMyRecipient.setData(ContactList1);
    }
};

function p2kwiet2012247634553_textbox286685406430472_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientSelectContacts.textbox286685406430472.skin = "txtSearchboxBigFont";
    //frmMyRecipientSelectContacts.textbox286685406430472.text=""
};

function p2kwiet2012247634553_textbox286685406430472_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientSelectContacts.textbox286685406430472.skin = "txtSearchboxBigFont";
    frmMyRecipientSelectContacts.textbox286685406430472.text = ""
};

function p2kwiet2012247634553_textbox286685406430472_onTextChange_seq0(eventobject, changedtext) {
    var search = frmMyRecipientSelectContacts.textbox286685406430472.text
    frmMyRecipientSelectContacts.label475095112172022.setVisibility(false);
    frmMyRecipientSelectContacts.segMyRecipient.setVisibility(true);
    if (search != null && search != "") {
        if (kony.string.containsChars(frmMyRecipientSelectContacts.textbox286685406430472.text, charsArr)) {
            alert(kony.i18n.getLocalizedString("InvalidSearch"));
            return;
        }
    }
    var manualSearch = false;
    if (eventobject.id == "btnSearchRecipeints") {
        manualSearch = true;
    }
    if (search.length >= 3 || manualSearch) {
        var showList = new Array();
        var searchtxt = search;
        var j = 0;
        var regexp = new RegExp("(" + searchtxt + ")", "ig");
        for (var i = 0; i < ContactList1.length; i++) {
            if (ContactList1[i]["lblName"]) {
                if (regexp.test(ContactList1[i]["lblName"]) == true) {
                    showList[j] = ContactList1[i];
                    j++;
                }
            }
        }
        if (showList.length == 0 || showList.length == undefined) {
            frmMyRecipientSelectContacts.label475095112172022.setVisibility(true);
            frmMyRecipientSelectContacts.segMyRecipient.setVisibility(false);
            frmMyRecipientSelectContacts.label475095112172022.text = kony.i18n.getLocalizedString("keybillernotfound");
            //showAlertRcMB(kony.i18n.getLocalizedString("keybillernotfound"), kony.i18n.getLocalizedString("info"), "info")
        } else {
            frmMyRecipientSelectContacts.segMyRecipient.setData(showList);
        }
    } else {
        if (ContactList1 != null && frmMyRecipientSelectContacts.segMyRecipient.data != null && ContactList1.length != frmMyRecipientSelectContacts.segMyRecipient.data.length) frmMyRecipientSelectContacts.segMyRecipient.setData(ContactList1);
    }
};

function p2kwiet2012247634553_segMyRecipient_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    ContactsMSRowfunc.call(this);
};

function p2kwiet2012247634553_btnMobileNo1_onClick_seq0(eventobject, context) {
    ContactsMSfunc.call(this, "one");
};

function p2kwiet2012247634553_btnMobileNo2_onClick_seq0(eventobject, context) {
    ContactsMSfunc.call(this, "two");
};

function p2kwiet2012247634553_btnMobileNo3_onClick_seq0(eventobject, context) {
    ContactsMSfunc.call(this, "three");
};

function p2kwiet2012247634553_btnReturn_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634553_btnNext_onClick_seq0(eventobject) {
    selectedContactsfn.call(this);
};

function p2kwiet2012247634590_frmMyRecipientSelectContactsComp_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634590_frmMyRecipientSelectContactsComp_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634590_frmMyRecipientSelectContactsComp_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectContactsCompMenuPreshow.call(this);
};

function p2kwiet2012247634590_frmMyRecipientSelectContactsComp_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectContactsCompMenuPostshow.call(this);
};

function p2kwiet2012247634590_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634590_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634590_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634590_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247634590_button4733076529054_onClick_seq0(eventobject) {
    gblAddMoreFromContactsCache.length = 0;
    gblAddMoreRcTrack = 0;
    frmMyRecipients.show();
};

function p2kwiet2012247634590_button4733076529055_onClick_seq0(eventobject) {
    gblAddMoreRcTrack = gblAddMoreRcTrack + multiSelectedContacts.length;
    if ((gblAddMoreRcTrack + myRecipientsRs.length) >= gblMAXRecipientCount) {
        alert99LimitReached.call(this);
    } else {
        for (var i = 0; i < multiSelectedContacts.length; i++) {
            gblAddMoreFromContactsCache.push(multiSelectedContacts[i]);
        }
        frmMyRecipientSelectContacts.show();
    }
};

function p2kwiet2012247634624_frmMyRecipientSelectContactsConf_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634624_frmMyRecipientSelectContactsConf_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectContactsConfMenuPreshow.call(this);
};

function p2kwiet2012247634624_frmMyRecipientSelectContactsConf_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectContactsConfMenuPostshow.call(this);
};

function p2kwiet2012247634624_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634624_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634624_btnRight_onClick_seq0(eventobject) {
    gblEditContactList = true;
    onClickContactList.call(this);
    frmMyRecipientSelectContacts.show();
};

function p2kwiet2012247634624_button4733076529054_onClick_seq0(eventobject) {
    multiSelectedContacts.length = 0;
    frmMyRecipients.show();
};

function p2kwiet2012247634624_button4733076529055_onClick_seq0(eventobject) {
    saveRecipientDeatilsMB.call(this);
    if (gblUserLockStatusMB == gblFinancialTxnMBLock) {
        alertUserStatusLocked.call(this);
    } else {
        /* 
popTransactionPwd.btnPopTransactionCancel.onClick = transCancelSelectContacts;
popTransactionPwd.btnPopTransactionConf.onClick = transConfirmSelectContactsPre;
popTransactionPwd.tbxPopTransactionPwd.text = "";
popTransactionPwd.lblPopTranscationMsg.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
popTransactionPwd.lblPopTransationPwd.text = kony.i18n.getLocalizedString("transPasswordSub");
popTransactionPwd.btnPopTransactionCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
popTransactionPwd.btnPopTransactionConf.text = kony.i18n.getLocalizedString("keyConfirm");

 */
        showOTPPopup.call(this, kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", transConfirmSelectContactsPre, 3);
    }
    /* 
if(flowSpa)
{
if(gblIBFlowStatus == "04")
alertUserStatusLocked()

}
else
{
 if(gblUserLockStatusMB == "03")
 alertUserStatusLocked()
 else
 {
 popTransactionPwd.btnPopTransactionCancel.onClick = transCancelSelectContacts;
 popTransactionPwd.btnPopTransactionConf.onClick = transConfirmSelectContactsPre;
 popTransactionPwd.tbxPopTransactionPwd.text = "";
 popTransactionPwd.lblPopTranscationMsg.text = "Please enter your transaction password";
 popTransactionPwd.show();
 }
}

 */
};

function p2kwiet2012247634671_frmMyRecipientSelectFacebook_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634671_frmMyRecipientSelectFacebook_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectFacebookMenuPreshow.call(this);
};

function p2kwiet2012247634671_frmMyRecipientSelectFacebook_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectFacebookMenuPostshow.call(this);
};

function p2kwiet2012247634671_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634671_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634671_btnSearchRecipeints_onClick_seq0(eventobject) {
    if (frmMyRecipientSelectFacebook.textbox247327209467957.text.length >= 3) {
        if (kony.string.containsChars(frmMyRecipientSelectFacebook.textbox247327209467957.text, charsArr)) {
            alert("Enter Valid Search String");
            return;
        }
        gblOffsetMB = "0";
        gblLimitMB = "0";
        gblStart = "1";
        gblSearch = frmMyRecipientSelectFacebook.textbox247327209467957.text;
        getFriendListFromFacebook.call(this, "0");
    }
};

function p2kwiet2012247634671_textbox247327209467957_onTextChange_seq0(eventobject, changedtext) {
    if (frmMyRecipientSelectFacebook.textbox247327209467957.text.length >= 3) {
        if (kony.string.containsChars(frmMyRecipientSelectFacebook.textbox247327209467957.text, charsArr)) {
            alert("Enter Valid Search String");
            return;
        }
        gblOffsetMB = "0";
        gblLimitMB = "0";
        gblStart = "1";
        gblSearch = frmMyRecipientSelectFacebook.textbox247327209467957.text;
        getFriendListFromFacebook.call(this, "0");
        gblFBListChanged = true;
    } else {
        if (gblFBListChanged) {
            gblOffsetMB = "0";
            gblLimitMB = "99";
            gblStart = "1";
            gblSearch = "0";
            getFriendListFromFacebook.call(this, "0");
            gblFBListChanged = false;
        }
    }
};

function p2kwiet2012247634671_btnMore_onClick_seq0(eventobject) {
    var tempOffset = parseInt(gblOffsetMB) + 100;
    var tempLimit = parseInt(gblLimitMB) + 100;
    gblOffsetMB = tempOffset.toString();
    gblLimitMB = tempLimit.toString();
    gblSearch = "0";
    gblStart = "1";
    gblMORE = true;
    getFriendListFromFacebook.call(this, "0");
};

function p2kwiet2012247634671_btnReturnSpa_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634671_btnNextSpa_onClick_seq0(eventobject) {
    selectedFacebookfn.call(this);
};

function p2kwiet2012247634671_btnReturn_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634671_btnNext_onClick_seq0(eventobject) {
    selectedFacebookfn.call(this);
};

function p2kwiet2012247634709_frmMyRecipientSelectFBComp_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634709_frmMyRecipientSelectFBComp_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634709_frmMyRecipientSelectFBComp_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectFBCompMenuPreshow.call(this);
};

function p2kwiet2012247634709_frmMyRecipientSelectFBComp_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectFBCompMenuPostshow.call(this);
};

function p2kwiet2012247634709_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
    handleMenuBtn.call(this);
};

function p2kwiet2012247634709_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634709_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634709_hbox50285458165_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634709_hbxAdv_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
    getCampaignResult.call(this);
};

function p2kwiet2012247634709_button474225406726366_onClick_seq0(eventobject) {
    gblAddMoreFromFacebookCache.length = 0;
    gblAddMoreRcTrack = 0;
    frmMyRecipients.show();
};

function p2kwiet2012247634709_button474225406726368_onClick_seq0(eventobject) {
    gblAddMoreRcTrack = gblAddMoreRcTrack + multiSelectedFacebook.length;
    if ((gblAddMoreRcTrack + myRecipientsRs.length) >= gblMAXRecipientCount) {
        alert99LimitReached.call(this);
    } else {
        for (var i = 0; i < multiSelectedFacebook.length; i++) {
            gblAddMoreFromFacebookCache.push(multiSelectedFacebook[i]);
        }
        frmMyRecipientSelectFacebook.show();
    }
};

function p2kwiet2012247634709_button4733076529054_onClick_seq0(eventobject) {
    gblAddMoreFromFacebookCache.length = 0;
    gblAddMoreRcTrack = 0;
    frmMyRecipients.show();
};

function p2kwiet2012247634709_button4733076529055_onClick_seq0(eventobject) {
    gblAddMoreRcTrack = gblAddMoreRcTrack + multiSelectedFacebook.length;
    if ((gblAddMoreRcTrack + myRecipientsRs.length) >= gblMAXRecipientCount) {
        alert99LimitReached.call(this);
    } else {
        for (var i = 0; i < multiSelectedFacebook.length; i++) {
            gblAddMoreFromFacebookCache.push(multiSelectedFacebook[i]);
        }
        frmMyRecipientSelectFacebook.show();
    }
};

function p2kwiet2012247634744_frmMyRecipientSelectFBConf_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634744_frmMyRecipientSelectFBConf_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634744_frmMyRecipientSelectFBConf_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectFBConfMenuPreshow.call(this);
};

function p2kwiet2012247634744_frmMyRecipientSelectFBConf_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    frmMyRecipientSelectFBConf.scrollboxMain.scrollToEnd();
};

function p2kwiet2012247634744_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634744_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634744_btnRight_onClick_seq0(eventobject) {
    /* 
onClickContactList.call(this);

 */
    gblEditFacebookSelection = true;
    frmMyRecipientSelectFacebook.show();
};

function p2kwiet2012247634744_button104411051441408_onClick_seq0(eventobject) {
    multiSelectedFacebook.length = 0;
    frmMyRecipients.show();
};

function p2kwiet2012247634744_button104411051441410_onClick_seq0(eventobject) {
    /* 
saveRecipientDeatilsMB.call(this);

 */
    if (gblIBFlowStatus == "04") {
        showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
        kony.print("IB_USER_STATUS_ID is 04");
        //return false;
    } else {
        var inputParams = {}
        var locale = kony.i18n.getCurrentLocale();
        spaChnage = "addRcFacebook"
        gblFlagTransPwdFlow = "addRcFacebook"
        gblOTPFlag = true;
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {
            kony.print("Timer doesnot exist");
        }
        //input params for SPA OTP
        gblSpaChannel = "AddRecipient";
        if (locale == "en_US") {
            SpaEventNotificationPolicy = "MIB_AddRecipient_EN";
            SpaSMSSubject = "MIB_AddRecipient_EN";
        } else {
            SpaEventNotificationPolicy = "MIB_AddRecipient_TH";
            SpaSMSSubject = "MIB_AddRecipient_TH";
        }
        fbRc = frmMyRecipientSelectFBConf.segment24733076528972.data;
        //AccountName=""//frmIBMyReceipentsAddContactManually.lblrecipientname.text;
        var AccntDetailsMessage2 = "";
        var rcount = 0;
        if (fbRc != null && fbRc != undefined) {
            rcount = fbRc.length;
            for (var t = 0; t < fbRc.length; t++) {
                AccntDetailsMessage2 = AccntDetailsMessage2 + fbRc[t]["lblName"] + ", "
            }
        }
        if (AccntDetailsMessage2 != null && AccntDetailsMessage2 != undefined) {
            AccntDetailsMessage2 = AccntDetailsMessage2.substring(0, AccntDetailsMessage2.length - 2)
        }
        gblAccountDetails = gblAccountDetails + rcount + " Recipient (" + AccntDetailsMessage2 + ").";
        //onClickOTPRequestSpa();
    }
    saveRecipientDeatilsMB.call(this);
};

function p2kwiet2012247634744_button4733076529054_onClick_seq0(eventobject) {
    multiSelectedFacebook.length = 0;
    frmMyRecipients.show();
};

function p2kwiet2012247634744_button4733076529055_onClick_seq0(eventobject) {
    /* 
saveRecipientDeatilsMB.call(this);

 */
    if (gblIBFlowStatus == "04") {
        showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
        kony.print("IB_USER_STATUS_ID is 04");
        //return false;
    } else {
        var inputParams = {}
        spaChnage = "addRcFacebook"
        gblFlagTransPwdFlow = "addRcFacebook"
        gblOTPFlag = true;
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {
            kony.print("Timer doesnot exist");
        }
        //input params for SPA OTP
        gblSpaChannel = "IB-NEW_RC_ADDITION";
        if (locale == "en_US") {
            SpaEventNotificationPolicy = "MIB_AddRecipient_EN";
            SpaSMSSubject = "MIB_AddRecipient_EN";
        } else {
            SpaEventNotificationPolicy = "MIB_AddRecipient_TH";
            SpaSMSSubject = "MIB_AddRecipient_TH";
        }
        fbRc = frmMyRecipientSelectFBConf.segment24733076528972.data;
        //AccountName=""//frmIBMyReceipentsAddContactManually.lblrecipientname.text;
        var AccntDetailsMessage2 = "";
        var rcount = 0;
        if (fbRc != null && fbRc != undefined) {
            rcount = fbRc.length;
            for (var t = 0; t < fbRc.length; t++) {
                AccntDetailsMessage2 = AccntDetailsMessage2 + fbRc[t]["lblName"] + ", "
            }
        }
        if (AccntDetailsMessage2 != null && AccntDetailsMessage2 != undefined) {
            AccntDetailsMessage2 = AccntDetailsMessage2.substring(0, AccntDetailsMessage2.length - 2)
        }
        gblAccountDetails = gblAccountDetails + rcount + " Recipient (" + AccntDetailsMessage2 + ").";
        onClickOTPRequestSpa();
    }
    //onClickOTPRequestSpa();
}
saveRecipientDeatilsMB.call(this);
};

function p2kwiet2012247634789_frmMyRecipientSelectFbID_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634789_frmMyRecipientSelectFbID_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectFbIDMenuPreshow.call(this);
};

function p2kwiet2012247634789_frmMyRecipientSelectFbID_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    frmMyRecipientSelectFbID.scrollboxMain.scrollToEnd();
    gblOffsetMB = "0";
    gblLimitMB = "99";
    gblSearch = "0";
    gblStart = "0";
    multiSelectedFacebook.length = 0;
    getFriendListFromFacebook.call(this, "0");
};

function p2kwiet2012247634789_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634789_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634789_btnSearchRecipeints_onClick_seq0(eventobject) {
    if (frmMyRecipientSelectFbID.textbox247327209467957.text.length > 0) {
        if (kony.string.containsChars(frmMyRecipientSelectFbID.textbox247327209467957.text, charsArr)) {
            alert("Enter Valid Search String");
            return;
        }
        gblOffsetMB = "0";
        gblLimitMB = "0";
        gblStart = "1";
        gblSearch = frmMyRecipientSelectFbID.textbox247327209467957.text;
        getFriendListFromFacebook.call(this, "0");
    }
};

function p2kwiet2012247634789_textbox247327209467957_onTextChange_seq0(eventobject, changedtext) {
    if (frmMyRecipientSelectFbID.textbox247327209467957.text.length >= 3) {
        if (kony.string.containsChars(frmMyRecipientSelectFbID.textbox247327209467957.text, charsArr)) {
            alert("Enter Valid Search String");
            return;
        }
        gblOffsetMB = "0";
        gblLimitMB = "0";
        gblStart = "1";
        gblSearch = frmMyRecipientSelectFbID.textbox247327209467957.text;
        getFriendListFromFacebook.call(this, "0");
        gblFBListChanged = true;
    } else {
        if (gblFBListChanged) {
            gblOffsetMB = "0";
            gblLimitMB = "99";
            gblStart = "1";
            gblSearch = "0";
            getFriendListFromFacebook.call(this, "0");
            gblFBListChanged = false;
        }
    }
};

function p2kwiet2012247634789_btnMore_onClick_seq0(eventobject) {
    var tempOffset = parseInt(gblOffsetMB) + 100;
    var tempLimit = parseInt(gblLimitMB) + 100;
    gblOffsetMB = tempOffset.toString();
    gblLimitMB = tempLimit.toString();
    gblSearch = "0";
    gblStart = "1";
    gblMORE = true;
    getFriendListFromFacebook.call(this, "0");
};

function p2kwiet2012247634789_btnReturnSpa_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634789_btnNextSpa_onClick_seq0(eventobject) {
    FacebookManualFolwNext.call(this);
};

function p2kwiet2012247634789_btnReturn_onClick_seq0(eventobject) {
    frmMyRecipients.show();
};

function p2kwiet2012247634789_btnNext_onClick_seq0(eventobject) {
    FacebookManualFolwNext.call(this);
};

function p2kwiet2012247634837_frmMyRecipientSelectMobile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634837_frmMyRecipientSelectMobile_preshow_seq0(eventobject, neworientation) {
    /* 
multipleSelectRecipientData.sort(dynamicSort("lblName"));

frmMyRecipientMultipleSelect.segMyRecipient.widgetDataMap = { lblName:"lblName",lblMobile:"lblMobile",lblEmail:"lblEmail",imgprofilepic:"imgprofilepic",
             imgchbxbtn:"imgchbxbtn" }

frmMyRecipientMultipleSelect.segMyRecipient.setData(multipleSelectRecipientData);

 */
    gblIndex = -1;
    isMenuShown = false;
    isSignedUser = true;
    frmMyRecipientSelectMobile.textbox247327209467957.skin = "txtSearchbox";
    frmMyRecipientSelectMobile.textbox247327209467957.text = ""
    frmMyRecipientSelectMobile.label475095112172022.text = kony.i18n.getLocalizedString("NoContacts");
    frmMyRecipientSelectMobile.segMyRecipient.removeAll();
    DisableFadingEdges.call(this, frmMyRecipientSelectMobile);
};

function p2kwiet2012247634837_frmMyRecipientSelectMobile_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientSelectMobileMenuPostshow.call(this);
};

function p2kwiet2012247634837_vbox475959051238576_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634837_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634837_btnSearchRecipeints_onClick_seq0(eventobject) {
    searchContacts.call(this, eventobject);
};

function p2kwiet2012247634837_textbox247327209467957_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientSelectMobile.textbox247327209467957.skin = "txtSearchboxBigFont";
    frmMyRecipientSelectMobile.textbox247327209467957.text = ""
};

function p2kwiet2012247634837_textbox247327209467957_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyRecipientSelectMobile.textbox247327209467957.skin = "txtSearchboxBigFont";
    frmMyRecipientSelectMobile.textbox247327209467957.text = ""
};

function p2kwiet2012247634837_textbox247327209467957_onTextChange_seq0(eventobject, changedtext) {
    searchContacts.call(this, eventobject);
};

function p2kwiet2012247634837_segMyRecipient_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    var temp = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    temp = temp[1];
    if ((fl || previousSelectedIndex) && previousSelectedIndex != temp) {
        kony.print("not coming here");
        previousSelectedItems["imgchbxbtn"] = "radiobtn2.png";
        if (previousSelectedItems["btnMobileNo1"]) {
            previousSelectedItems["btnMobileNo1"]["skin"] = "btnRadioFoc";
        }
        if (previousSelectedItems["btnMobileNo2"]) {
            previousSelectedItems["btnMobileNo2"]["skin"] = "btnRadio";
        }
        if (previousSelectedItems["btnMobileNo3"]) {
            previousSelectedItems["btnMobileNo3"]["skin"] = "btnRadio";
        }
        frmMyRecipientSelectMobile.segMyRecipient.setDataAt(previousSelectedItems, previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.removeAt(previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(previousSelectedItems, previousSelectedIndex);
    }
    var index = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    index = index[1];
    previousSelectedIndex = index;
    fl = 1;
    var items = frmMyRecipientSelectMobile.segMyRecipient.selectedItems;
    previousSelectedItems = items[0];
    items[0]["imgchbxbtn"] = "radiobtn.png";
    if (items[0]["lblMobileNo1"]) selectedMobile = items[0]["lblMobileNo1"];
    else if (items[0]["lblMobileNo2"]) selectedMobile = items[0]["lblMobileNo2"];
    else if (items[0]["lblMobileNo3"]) selectedMobile = items[0]["lblMobileNo3"];
    var slectedEmail = items[0]["lblEmail"]
    kony.print(selectedMobile);
    kony.print(slectedEmail)
    kony.print("vinod name " + items[0]["lblName"])
    var base64Contact = null;
    gblRcBase64ListFromContacts = "";
    kony.print("vinod name " + items[0].imgprofilepic.rawBytes)
    if (items[0].imgprofilepic.rawBytes != null && kony.convertToBase64(items[0].imgprofilepic.rawBytes) != null) {
        gblRcBase64ListFromContacts = kony.convertToBase64(items[0].imgprofilepic.rawBytes).replace(/[\n\r\s\f\t\v]+/g, '');
        if (requestFromForm == "editprofile") frmMyRecipientEditProfile.imgprofilepic.base64 = gblRcBase64ListFromContacts
        else if (requestFromForm == "addprofile") frmMyRecipientAddProfile.imgProfilePic.base64 = gblRcBase64ListFromContacts
    } else {
        if (requestFromForm == "editprofile") frmMyRecipientEditProfile.imgprofilepic.src = "avatar_dis.png"
        else if (requestFromForm == "addprofile") frmMyRecipientAddProfile.imgProfilePic.src = "avatar_dis.png"
    }
    frmMyRecipientAddProfile.tbxRecipientName.text = items[0]["lblName"]
    frmMyRecipientAddProfile.tbxEmail.text = slectedEmail
    frmMyRecipientSelectMobile.segMyRecipient.setDataAt(items[0], index);
    //frmMyRecipientSelectMobile.segMyRecipient.removeAt(index);
    //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(items[0], index);
};

function p2kwiet2012247634837_btnMobileNo1_onClick_seq0(eventobject, context) {
    var temp = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    temp = temp[1];
    if ((fl || previousSelectedIndex) && previousSelectedIndex != temp) { //previously selected
        previousSelectedItems["imgchbxbtn"] = "radiobtn2.png";
        if (previousSelectedItems["btnMobileNo1"]) {
            previousSelectedItems["btnMobileNo1"]["skin"] = "btnRadioFoc";
        }
        if (previousSelectedItems["btnMobileNo2"]) {
            previousSelectedItems["btnMobileNo2"]["skin"] = "btnRadio";
        }
        if (previousSelectedItems["btnMobileNo3"]) {
            previousSelectedItems["btnMobileNo3"]["skin"] = "btnRadio";
        }
        //frmMyRecipientSelectMobile.segMyRecipient.removeAt(previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(previousSelectedItems, previousSelectedIndex);
        frmMyRecipientSelectMobile.segMyRecipient.setDataAt(previousSelectedItems, previousSelectedIndex);
    }
    var index = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    index = index[1];
    previousSelectedIndex = index;
    fl = 1;
    var items = frmMyRecipientSelectMobile.segMyRecipient.selectedItems;
    previousSelectedItems = items[0];
    items[0]["btnMobileNo1"]["skin"] = "btnRadioFoc";
    if (items[0]["btnMobileNo2"]) {
        items[0]["btnMobileNo2"]["skin"] = "btnRadio";
    }
    if (items[0]["btnMobileNo3"]) {
        items[0]["btnMobileNo3"]["skin"] = "btnRadio";
    }
    items[0]["imgchbxbtn"] = "radiobtn.png";
    selectedMobile = items[0]["lblMobileNo1"]; //check if getting this
    kony.print(selectedMobile);
    frmMyRecipientSelectMobile.segMyRecipient.setDataAt(items[0], index);
    //frmMyRecipientSelectMobile.segMyRecipient.removeAt(index);
    //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(items[0], index);
};

function p2kwiet2012247634837_btnMobileNo2_onClick_seq0(eventobject, context) {
    var temp = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    temp = temp[1];
    if ((fl || previousSelectedIndex) && previousSelectedIndex != temp) {
        previousSelectedItems["imgchbxbtn"] = "radiobtn2.png";
        if (previousSelectedItems["btnMobileNo1"]) {
            previousSelectedItems["btnMobileNo1"]["skin"] = "btnRadioFoc";
        }
        if (previousSelectedItems["btnMobileNo2"]) {
            previousSelectedItems["btnMobileNo2"]["skin"] = "btnRadio";
        }
        if (previousSelectedItems["btnMobileNo3"]) {
            previousSelectedItems["btnMobileNo3"]["skin"] = "btnRadio";
        }
        //frmMyRecipientSelectMobile.segMyRecipient.removeAt(previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(previousSelectedItems, previousSelectedIndex);
        frmMyRecipientSelectMobile.segMyRecipient.setDataAt(previousSelectedItems, previousSelectedIndex);
    }
    var index = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    index = index[1];
    previousSelectedIndex = index;
    fl = 1;
    var items = frmMyRecipientSelectMobile.segMyRecipient.selectedItems;
    previousSelectedItems = items[0];
    items[0]["btnMobileNo2"]["skin"] = "btnRadioFoc";
    if (items[0]["btnMobileNo1"]) {
        items[0]["btnMobileNo1"]["skin"] = "btnRadio";
    }
    if (items[0]["btnMobileNo3"]) {
        items[0]["btnMobileNo3"]["skin"] = "btnRadio";
    }
    items[0]["imgchbxbtn"] = "radiobtn.png";
    selectedMobile = items[0]["lblMobileNo2"]; //check if getting this
    kony.print(selectedMobile);
    frmMyRecipientSelectMobile.segMyRecipient.setDataAt(items[0], index);
    //frmMyRecipientSelectMobile.segMyRecipient.removeAt(index);
    //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(items[0], index);
};

function p2kwiet2012247634837_btnMobileNo3_onClick_seq0(eventobject, context) {
    var temp = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    temp = temp[1];
    if ((fl || previousSelectedIndex) && previousSelectedIndex != temp) {
        previousSelectedItems["imgchbxbtn"] = "radiobtn2.png";
        if (previousSelectedItems["btnMobileNo1"]) {
            previousSelectedItems["btnMobileNo1"]["skin"] = "btnRadioFoc";
        }
        if (previousSelectedItems["btnMobileNo2"]) {
            previousSelectedItems["btnMobileNo2"]["skin"] = "btnRadio";
        }
        if (previousSelectedItems["btnMobileNo3"]) {
            previousSelectedItems["btnMobileNo3"]["skin"] = "btnRadio";
        }
        frmMyRecipientSelectMobile.segMyRecipient.setDataAt(previousSelectedItems, previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.removeAt(previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(previousSelectedItems, previousSelectedIndex);
    }
    var index = frmMyRecipientSelectMobile.segMyRecipient.selectedIndex;
    index = index[1];
    previousSelectedIndex = index;
    fl = 1;
    var items = frmMyRecipientSelectMobile.segMyRecipient.selectedItems;
    previousSelectedItems = items[0];
    items[0]["btnMobileNo3"]["skin"] = "btnRadioFoc";
    if (items[0]["btnMobileNo1"]) {
        items[0]["btnMobileNo1"]["skin"] = "btnRadio";
    }
    if (items[0]["btnMobileNo2"]) {
        items[0]["btnMobileNo2"]["skin"] = "btnRadio";
    }
    items[0]["imgchbxbtn"] = "radiobtn.png";
    selectedMobile = items[0]["lblMobileNo3"]; //check if getting this
    kony.print(selectedMobile);
    frmMyRecipientSelectMobile.segMyRecipient.setDataAt(items[0], index);
    //frmMyRecipientSelectMobile.segMyRecipient.removeAt(index);
    //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(items[0], index);
};

function p2kwiet2012247634837_btnReturn_onClick_seq0(eventobject) {
    frmMyRecipientAddProfile.show();
};

function p2kwiet2012247634837_btnNext_onClick_seq0(eventobject) {
    if (fl == 1) {
        previousSelectedItems["imgchbxbtn"] = "radiobtn2.png";
        if (previousSelectedItems["btnMobileNo1"]) {
            previousSelectedItems["btnMobileNo1"]["skin"] = "btnRadioFoc";
        }
        if (previousSelectedItems["btnMobileNo2"]) {
            previousSelectedItems["btnMobileNo2"]["skin"] = "btnRadio";
        }
        if (previousSelectedItems["btnMobileNo3"]) {
            previousSelectedItems["btnMobileNo3"]["skin"] = "btnRadio";
        }
        frmMyRecipientSelectMobile.segMyRecipient.setDataAt(previousSelectedItems, previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.removeAt(previousSelectedIndex);
        //frmMyRecipientSelectMobile.segMyRecipient.addDataAt(previousSelectedItems, previousSelectedIndex);
    }
    previousSelectedIndex = false;
    previousSelectedItems = false;
    fl = null;
    if (requestFromForm == "editprofile") {
        if (selectedMobile) {
            frmMyRecipientEditProfile.tbxMobileNo.text = selectedMobile;
        } else {
            frmMyRecipientEditProfile.tbxMobileNo.text = "";
        }
        frmMyRecipientEditProfile.show();
    } else if (requestFromForm == "addprofile") {
        if (selectedMobile) {
            frmMyRecipientAddProfile.tbxMobileNo.text = selectedMobile;
        } else {
            frmMyRecipientAddProfile.tbxMobileNo.text = "";
        }
        gblRequestFromForm = "addprofile";
        frmMyRecipientAddProfile.show();
    }
    contactListToggle = true;
};

function p2kwiet2012247634886_frmMyRecipientViewAccount_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634886_frmMyRecipientViewAccount_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634886_frmMyRecipientViewAccount_preshow_seq0(eventobject, neworientation) {
    frmMyRecipientViewAccountMenuPreshow.call(this);
};

function p2kwiet2012247634886_frmMyRecipientViewAccount_postshow_seq0(eventobject, neworientation) {
    frmMyRecipientViewAccountMenuPostshow.call(this);
};

function p2kwiet2012247634886_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634886_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634886_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247634886_button47327209489399_onClick_seq0(eventobject) {
    if (gblUserLockStatusMB == gblFinancialTxnMBLock) {
        alertUserStatusLocked.call(this);
    } else {
        deleteAccountFromRecipientView.call(this);
    }
};

function p2kwiet2012247634886_button47327209489400_onClick_seq0(eventobject) {
    if (gblUserLockStatusMB == gblFinancialTxnMBLock) {
        alertUserStatusLocked.call(this);
    } else {
        frmMyRecipientEditAccount.txtAccountNickName.text = frmMyRecipientViewAccount.lblNick.text;
        frmMyRecipientEditAccount.tbxAccName.text = frmMyRecipientViewAccount.lblAcctName.text;
        frmMyRecipientEditAccount.txtAccountNickName.text = frmMyRecipientViewAccount.lblNick.text;
        frmMyRecipientEditAccount.show();
    }
};

function p2kwiet2012247634886_btnTransfer_onClick_seq0(eventobject) {
    gblTransferFromRecipient = true
    getTransferFromAccounts.call(this);
};

function p2kwiet2012247634886_button101086657957077_onClick_seq0(eventobject) {
    frmMyRecipientDetail.show();
};

function p2kwiet2012247634934_frmMyTopUpComplete_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634934_frmMyTopUpComplete_preshow_seq0(eventobject, neworientation) {
    frmMyTopUpCompleteMenuPreshow.call(this);
};

function p2kwiet2012247634934_frmMyTopUpComplete_postshow_seq0(eventobject, neworientation) {
    frmMyTopUpCompleteMenuPostshow.call(this);
};

function p2kwiet2012247634934_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634934_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634934_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247634934_btnCancelSpa_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        frmMyTopUpList.show();
        TMBUtil.DestroyForm(frmMyTopUpComplete);
    } else {
        frmMyTopUpComplete.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247634934_btnAgreeSpa_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
    if (addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT)) kony.print("addAnotherValidValue TRUE ------" + addAnotherValidValue);
    else kony.print("addAnotherValidValue  FALSE------" + addAnotherValidValue);
    if (addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT)) {
        frmAddTopUpToMB.imgAddedBiller.src = "";
        frmAddTopUpToMB.lblAddbillerName.text = "";
        frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
        frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
        frmAddTopUpToMB.show();
    } else {
        var alert_seq6_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
    }
};

function p2kwiet2012247634934_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        frmMyTopUpList.show();
        TMBUtil.DestroyForm(frmMyTopUpComplete);
    } else {
        frmMyTopUpComplete.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247634934_btnAgree_onClick_seq0(eventobject) {
    TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
    if (addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT)) kony.print("addAnotherValidValue TRUE ------" + addAnotherValidValue);
    else kony.print("addAnotherValidValue  FALSE------" + addAnotherValidValue);
    if (addAnotherValidValue < kony.os.toNumber(GLOBAL_MAX_BILL_COUNT)) {
        frmAddTopUpToMB.imgAddedBiller.src = "";
        frmAddTopUpToMB.lblAddbillerName.text = "";
        frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
        frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
        frmAddTopUpToMB.show();
    } else {
        var alert_seq6_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
    }
};

function p2kwiet2012247634975_frmMyTopUpCompleteCalendar_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247634975_frmMyTopUpCompleteCalendar_preshow_seq0(eventobject, neworientation) {
    frmMyTopUpCompleteCalendarMenuPreshow.call(this);
};

function p2kwiet2012247634975_frmMyTopUpCompleteCalendar_postshow_seq0(eventobject, neworientation) {
    frmMyTopUpCompleteCalendarMenuPostshow.call(this);
};

function p2kwiet2012247634975_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247634975_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247634975_btnCancel_onClick_seq0(eventobject) {
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate, frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247635003_frmMyTopUpEditScreens_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635003_frmMyTopUpEditScreens_preshow_seq0(eventobject, neworientation) {
    frmMyTopUpEditScreensMenuPreshow.call(this);
};

function p2kwiet2012247635003_frmMyTopUpEditScreens_postshow_seq0(eventobject, neworientation) {
    frmMyTopUpEditScreensMenuPostshow.call(this);
};

function p2kwiet2012247635003_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635003_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635003_txtEditName_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmMyTopUpEditScreens.txtEditName.skin = "txtLightBlue";
};

function p2kwiet2012247635003_txtEditName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmMyTopUpEditScreens.txtEditName.skin = "SkinDefaults";
};

function p2kwiet2012247635003_btnCancel_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        frmViewTopUpBiller.imgComplete.setVisibility(false);
        frmViewTopUpBiller.show();
    } else {
        frmMyTopUpEditScreens.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247635003_btnAgree_onClick_seq0(eventobject) {
    frmMyTopUpEditScreens.txtEditName.skin = txtNormalBG;
    frmMyTopUpEditScreens.txtEditName.focusSkin = txtNormalBG;
    if (checkDuplicateBillerNickCheck(frmMyTopUpEditScreens.txtEditName.text)) {
        editTopUpBillerValidation.call(this);
    } else {
        var alert_seq6_act0 = kony.ui.Alert({
            "message": kony.i18n.getLocalizedString("Valid_DuplicateNickname"),
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Ok",
            "noLabel": "",
            "alertIcon": "",
            "alertHandler": null
        }, {});
    }
};

function p2kwiet2012247635060_frmMyTopUpList_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635060_frmMyTopUpList_preshow_seq0(eventobject, neworientation) {
    frmMyTopUpListMenuPreshow.call(this);
};

function p2kwiet2012247635060_frmMyTopUpList_postshow_seq0(eventobject, neworientation) {
    frmMyTopUpListMenuPostshow.call(this);
};

function p2kwiet2012247635060_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635060_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635060_button1010778103103263_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == 2) {
        /* 
myTopUpAddButton.call(this);

 */
        /* 
if(segData == 50 && segSugData == 50)
{
alert("User has already added 50 top up billers");
}
else{
frmAddTopUpBiller.show();
}



 */
        TMBUtil.DestroyForm(frmAddTopUpToMB)
        gblBillerCompCodeBBMB = ""
        showAccountListBBMB.call(this);
    } else {
        /* 
myTopUpAddButton.call(this);

 */
        /* 
if(segData == 50 && segSugData == 50)
{
alert("User has already added 50 top up billers");
}
else{
frmAddTopUpBiller.show();
}



 */
        if (kony.string.equals(gblIBFlowStatus, "04")) {
            alert(kony.i18n.getLocalizedString("Receipent_OTPLocked"))
        } else {
            if (checkMaxBillerCountMB()) {
                frmAddTopUpToMB.imgAddedBiller.src = "empty.png";
                frmAddTopUpToMB.lblAddbillerName.text = "";
                frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString("keyRef1");
                frmAddTopUpToMB.lblAddedRef2.text = kony.i18n.getLocalizedString("keyRef2");
                frmAddTopUpToMB.show();
            } else {
                var alert_seq28_act0 = kony.ui.Alert({
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "",
                    "alertIcon": "",
                    "alertHandler": null
                }, {});
            }
        }
    }
};

function p2kwiet2012247635060_txbSearch_onTextChange_seq0(eventobject, changedtext) {
    if (gblMyBillerTopUpBB == 2) {
        validateSearch.call(this, null);
        searchSuggestedBillersBBMB.call(this);
        /* 
searchSugBillerMBBB.call(this);

 */
        /* 
billerCatChangeMB.call(this);

 */
    } else {
        validateSearch.call(this, null);
        searchSugBillerMB.call(this);
        billerCatChangeMB.call(this);
    }
};

function p2kwiet2012247635060_btnSelectCat_onClick_seq0(eventobject) {
    if (gblMyBillerTopUpBB == 2) {
        popUpMyBillers.lblFlag.text = "frmMyTopUpList";
        popUpMyBillers.segPop.setData(gblBillerCategoriesBBMB);
        popUpMyBillers.show();
    } else {
        popUpMyBillers.lblFlag.text = "frmMyTopUpList";
        startDisplayTopUpCategoryService.call(this);
    }
};

function p2kwiet2012247635060_segBillersList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    if (gblMyBillerTopUpBB == 2) {
        /* 
//if(gblMyBillerTopUpBB == 0){
 //viewMybillTopUpdetail();
 //selectBillerToViewBiller();
//}
//else if(gblMyBillerTopUpBB == 1){
 //selectTopUpBillerToViewTopUp();
// viewMybillTopUpdetail();
//}else if (gblMyBillerTopUpBB == 2){
// selectBillerToViewBB();
//}

selectBillerToViewBB();

 */
        /* 
viewMybillTopUpdetail.call(this);

 */
        selectBillerToViewBB.call(this);
    } else {
        if (gblMyBillerTopUpBB == 0) {
            viewMybillTopUpdetail();
            //selectBillerToViewBiller();
        } else if (gblMyBillerTopUpBB == 1) {
            //selectTopUpBillerToViewTopUp();
            viewMybillTopUpdetail();
        } else if (gblMyBillerTopUpBB == 2) {
            selectBillerToViewBB();
        }
        /* 
viewMybillTopUpdetail.call(this);

 */
    }
};

function p2kwiet2012247635060_hbxMoreBB_onClick_seq0(eventobject) {
    onMyBBSelectBillersMoreDynamicMB.call(this);
};

function p2kwiet2012247635060_segSuggestedBillers_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    if (gblMyBillerTopUpBB == 2) {
        TMBUtil.DestroyForm(frmAddTopUpToMB);
        /* 
onclickOfSuggestedBillersAddBB.call(this);

 */
        onSelectSugBillerBB.call(this);
        showAccountListBBMB.call(this);
    } else {
        if (gblBlockBillerTopUpAdd) {
            kony.print("Adding biller is disabled");
        } else {
            if (checkMaxBillerCountMB()) {
                onclickOfSuggestedBillersAdd.call(this);
            } else {
                var alert_seq14_act0 = kony.ui.Alert({
                    "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                    "alertType": constants.ALERT_TYPE_ERROR,
                    "alertTitle": "",
                    "yesLabel": "Ok",
                    "noLabel": "",
                    "alertIcon": "",
                    "alertHandler": null
                }, {});
            }
        }
    }
};

function p2kwiet2012247635060_btnAddBiller_onClick_seq0(eventobject, context) {
    if (gblMyBillerTopUpBB == 2) {
        TMBUtil.DestroyForm(frmAddTopUpToMB);
        //if(flowSpa)
        //{
        //frmAddTopUpToMBGlobals();
        //}
        /* 
onclickOfSuggestedBillersAddBB.call(this);

 */
        onSelectSugBillerBB.call(this);
        showAccountListBBMB.call(this);
    } else {
        if (gblBlockBillerTopUpAdd) {
            kony.print("Adding biller is disabled");
        } else {
            if (kony.string.equals(gblIBFlowStatus, "04")) {
                alert(kony.i18n.getLocalizedString("Receipent_OTPLocked"))
            } else {
                if (checkMaxBillerCountMB()) {
                    onclickOfSuggestedBillersAdd.call(this);
                } else {
                    var alert_seq32_act0 = kony.ui.Alert({
                        "message": kony.i18n.getLocalizedString("Valid_MoreThan50"),
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "",
                        "yesLabel": "Ok",
                        "noLabel": "",
                        "alertIcon": "",
                        "alertHandler": null
                    }, {});
                }
            }
        }
    }
};

function p2kwiet2012247635060_hbxSugMoreBB_onClick_seq0(eventobject) {
    onMyBBSelectMoreDynamicMB.call(this);
};

function p2kwiet2012247635060_hbxMore_onClick_seq0(eventobject) {
    onMyTopUpSelectMoreDynamic.call(this, gblTopUpSelectFormDataHolder);
};

function p2kwiet2012247635100_frmMyTopUpSelect_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635100_frmMyTopUpSelect_preshow_seq0(eventobject, neworientation) {
    frmMyTopUpSelectMenuPreshow.call(this);
};

function p2kwiet2012247635100_frmMyTopUpSelect_postshow_seq0(eventobject, neworientation) {
    frmMyTopUpSelectMenuPostshow.call(this);
};

function p2kwiet2012247635100_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635100_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635100_txbSearch_onTextChange_seq0(eventobject, changedtext) {
    if (gblMyBillerTopUpBB == 2) {
        validateSearch.call(this, null);
        searchSugBillerMBBB.call(this);
        /* 
billerCatChangeMB.call(this);

 */
    } else {
        validateSearch.call(this, null);
        searchSugBillerMB.call(this);
    }
};

function p2kwiet2012247635100_btnSelectCat_onClick_seq0(eventobject) {
    popUpMyBillers.lblFlag.text = "frmMyTopUpSelect";
    startDisplayTopUpCategoryService();
};

function p2kwiet2012247635100_segSelectList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    if (gblMyBillerTopUpBB == 2) {
        onSelectBillerBB.call(this);
        /* 
selctedTopUpBiller.call(this);

 */
    } else {
        selctedTopUpBiller.call(this);
    }
};

function p2kwiet2012247635100_btnAddBiller_onClick_seq0(eventobject, context) {
    if (gblMyBillerTopUpBB == 2) {
        selctedTopUpBiller.call(this);
    } else {
        selctedTopUpBiller.call(this);
    }
};

function p2kwiet2012247635100_lnkMore_onClick_seq0(eventobject) {
    onMyTopUpSelectMoreDynamic.call(this, gblTopUpSelectFormDataHolder);
};

function p2kwiet2012247635149_frmMyTransferRecipient_preshow_seq0(eventobject, neworientation) {
    segTransferRecipientData.call(this);
};

function p2kwiet2012247635149_frmMyTransferRecipient_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
};

function p2kwiet2012247635149_btnRight_onClick_seq0(eventobject) {
    transferRecipientContactList.call(this);
};

function p2kwiet2012247635149_hboxaddfblist_onClick_seq0(eventobject) {
    /* 
frm2.hbox50285458127.setVisibility(true)

 */
    ehFrmTMBTransAck_hboxaddfblist_onClick.call(this);
};

function p2kwiet2012247635149_txbXferSearch_onTextChange_seq0(eventobject, changedtext) {
    TransferRecipientSearch1.call(this, eventobject);
};

function p2kwiet2012247635149_segMyRecipient_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    segEditTransferRecipientData.call(this);
};

function p2kwiet2012247635149_btnA_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnB_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnC_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnD_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnE_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnF_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnG_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnH_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnI_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnJ_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnK_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnL_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnM_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnN_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnO_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnP_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnQ_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnR_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnT_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnU_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnV_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnS_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnW_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnX_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnY_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635149_btnZ_onClick_seq0(eventobject) {
    TransferRecipientSearch.call(this, eventobject);
};

function p2kwiet2012247635184_frmNotificationDetails_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247635184_frmNotificationDetails_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635184_frmNotificationDetails_preshow_seq0(eventobject, neworientation) {
    frmNotificationDetailsMenuPreshow.call(this);
};

function p2kwiet2012247635184_frmNotificationDetails_postshow_seq0(eventobject, neworientation) {
    frmNotificationDetailsMenuPostshow.call(this);
};

function p2kwiet2012247635184_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635184_btnHdrMenu_onClick_seq0(eventobject) {
    unreadInboxMessagesTrackerLocalDBSync.call(this);
};

function p2kwiet2012247635184_btnCancelSpa_onClick_seq0(eventobject) {
    gblNotificationHistoryModified = true;
    frmNotificationHome.show();
};

function p2kwiet2012247635184_btnAgreeSpa_onClick_seq0(eventobject) {
    var inputParams = {}
    spaChnage = "addRcFacebook"
    gblFlagTransPwdFlow = "addRcFacebook"
    gblOTPFlag = true;
    try {
        kony.timer.cancel("otpTimer")
    } catch (e) {
        kony.print("Timer doesnot exist");
    }
    onClickOTPRequestSpa();
};

function p2kwiet2012247635184_btnbackSpa_onClick_seq0(eventobject) {
    gblNotificationHistoryModified = true;
    frmNotificationHome.show();
};

function p2kwiet2012247635184_button4733076529054_onClick_seq0(eventobject) {
    gblNotificationHistoryModified = true;
    frmNotificationHome.show();
};

function p2kwiet2012247635184_button4733076529055_onClick_seq0(eventobject) {
    var inputParams = {}
    spaChnage = "addRcFacebook"
    gblOTPFlag = true;
    try {
        kony.timer.cancel("otpTimer")
    } catch (e) {
        kony.print("Timer doesnot exist");
    }
    onClickOTPRequestSpa();
};

function p2kwiet2012247635184_btnBackRC_onClick_seq0(eventobject) {
    gblNotificationHistoryModified = true;
    frmNotificationHome.show();
};

function p2kwiet2012247635219_frmNotificationHome_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247635219_frmNotificationHome_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635219_frmNotificationHome_preshow_seq0(eventobject, neworientation) {
    frmNotificationHomeMenuPreshow.call(this);
};

function p2kwiet2012247635219_frmNotificationHome_postshow_seq0(eventobject, neworientation) {
    frmNotificationHomeMenuPostshow.call(this);
};

function p2kwiet2012247635219_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635219_btnHdrMenu_onClick_seq0(eventobject) {
    frmNotificationHome_btnHdrMenu_onClick.call(this);
};

function p2kwiet2012247635219_btnCancel_onClick_seq0(eventobject) {
    CancelDeleteOnClick.call(this, eventobject);
};

function p2kwiet2012247635219_btnRight_onClick_seq0(eventobject) {
    NotificationDeleteOnClick.call(this, eventobject);
};

function p2kwiet2012247635219_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635219_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635272_frmOccupationInfo_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635272_frmOccupationInfo_SPA_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635272_frmOccupationInfo_preshow_seq0(eventobject, neworientation) {
    frmOccupationInfoMenuPreshow.call(this);
};

function p2kwiet2012247635272_frmOccupationInfo_postshow_seq0(eventobject, neworientation) {
    frmOccupationInfoMenuPostshow.call(this);
};

function p2kwiet2012247635272_frmOccupationInfo_onhide_seq0(eventobject, neworientation) {
    /* 
removeMenu.call(this);

 */
    ehFrmTransfersAck_frmTransfersAck_onhide.call(this);
};

function p2kwiet2012247635272_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635272_btnback_onClick_seq0(eventobject) {
    /* 
frmTransferLanding.destroy();
frmAccountSummaryLanding.destroy();
gblPaynow = true;

 */
    /* 
cleanUpGlobalVariableTransfersMB.call(this);

 */
    /* 
showAccuntSummaryScreen.call(this);

 */
    onClickCheckContactInfo.call(this);
};

function p2kwiet2012247635272_btnconfirm_onClick_seq0(eventobject) {
    /* 
makeAnotherTransfer.call(this);

 */
    onClickAgreeOPenAcnt.call(this);
};

function p2kwiet2012247635380_frmOpenAccountNSConfirmation_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635380_frmOpenAccountNSConfirmation_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635380_frmOpenAccountNSConfirmation_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635380_frmOpenAccountNSConfirmation_preshow_seq0(eventobject, neworientation) {
    frmOpenAccountNSConfirmationMenuPreshow.call(this);
};

function p2kwiet2012247635380_frmOpenAccountNSConfirmation_postshow_seq0(eventobject, neworientation) {
    frmOpenAccountNSConfirmationMenuPostshow.call(this);
};

function p2kwiet2012247635380_vbxContainer_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635380_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635380_btnRight_onClick_seq0(eventobject) {
    if (frmOpenAccountNSConfirmation.btnRight.skin == btnEdit) {
        if (flowSpa) {
            //alert("the setting is"+gblspaSelIndex)
            gblspaSelIndex = gblnormSelIndex;
        }
        frmOpnActSelAct.show();
    } else {
        enableAckList.call(this, frmOpenAccountNSConfirmation);
    }
};

function p2kwiet2012247635380_button47428498625_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActNS.call(this, "pdf");
};

function p2kwiet2012247635380_btnPhoto_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActNS.call(this, "png");
};

function p2kwiet2012247635380_btnEmailto_onClick_seq0(eventobject) {
    requestFromForm = "frmOpenAccountNSConfirmation";
    postOnWall.call(this);
};

function p2kwiet2012247635380_hbox101271281131304_onClick_seq0(eventobject) {
    hideUnhideOA.call(this, frmOpenAccountNSConfirmation.lblOpenActCnfmBal, frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt, frmOpenAccountNSConfirmation.lblHide);
};

function p2kwiet2012247635380_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247635380_btnCancelSpa_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247635380_btnConfirmSpa_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247635380_buttonBackSpa_onClick_seq0(eventobject) {
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247635380_btnOpenMoreSpa_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247635380_btnCancel_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247635380_btnConfirm_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247635380_buttonBack_onClick_seq0(eventobject) {
    frmAccountSummaryLanding = null;
    frmAccountSummaryLandingGlobals();
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247635380_btnOpenMore_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247635471_frmOpenAccountNSConfirmationCalendar_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635471_frmOpenAccountNSConfirmationCalendar_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635471_frmOpenAccountNSConfirmationCalendar_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635471_frmOpenAccountNSConfirmationCalendar_preshow_seq0(eventobject, neworientation) {
    frmOpenAccountNSConfirmationCalendarMenuPreshow.call(this);
};

function p2kwiet2012247635471_frmOpenAccountNSConfirmationCalendar_postshow_seq0(eventobject, neworientation) {
    frmOpenAccountNSConfirmationCalendarMenuPostshow.call(this);
};

function p2kwiet2012247635471_vbxContainer_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635471_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635471_btnRight_onClick_seq0(eventobject) {
    enableAckList.call(this, frmOpenAccountNSConfirmationCalendar);
};

function p2kwiet2012247635471_button47428498625_onClick_seq0(eventobject) {
    savePDFExecTOANSMB.call(this, "pdf");
};

function p2kwiet2012247635471_btnPhoto_onClick_seq0(eventobject) {
    savePDFExecTOANSMB.call(this, "png");
};

function p2kwiet2012247635471_btnEmailto_onClick_seq0(eventobject) {
    fbShareExecutedTxnMB.call(this);
};

function p2kwiet2012247635471_hbox101271281131304_onClick_seq0(eventobject) {
    if (frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt.isVisible == true) {
        kony.print("@@ into if");
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt.setVisibility(false);
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBal.setVisibility(false)
        frmOpenAccountNSConfirmationCalendar.lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
    } else {
        kony.print("@@ into else");
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt.setVisibility(true);
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBal.setVisibility(true)
        frmOpenAccountNSConfirmationCalendar.lblHide.text = kony.i18n.getLocalizedString("Hide")
    }
};

function p2kwiet2012247635471_btnCancel_onClick_seq0(eventobject) {
    frmMBMyActivities.show();
    showCalendar(gsSelectedDate, frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247635627_frmOpenAccountSavingCareMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635627_frmOpenAccountSavingCareMB_preshow_seq0(eventobject, neworientation) {
    frmOpenAccountSavingCareMBMenuPreshow.call(this);
};

function p2kwiet2012247635627_frmOpenAccountSavingCareMB_postshow_seq0(eventobject, neworientation) {
    frmOpenAccountSavingCareMBMenuPostshow.call(this);
};

function p2kwiet2012247635627_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635627_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635627_segSlider_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectSeg.call(this);
};

function p2kwiet2012247635627_segSlider_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247635627_txtAmount_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAccountSavingCareMB.txtAmount.text = autoFormatAmount(frmOpenAccountSavingCareMB.txtAmount.text);
};

function p2kwiet2012247635627_txtAmount_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAccountSavingCareMB.txtAmount.text = autoFormatAmount(frmOpenAccountSavingCareMB.txtAmount.text);
};

function p2kwiet2012247635627_txtAmount_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAccountSavingCareMB.txtAmount.text = autoFormatAmount(frmOpenAccountSavingCareMB.txtAmount.text);
};

function p2kwiet2012247635627_txtAmount_onDone_seq0(eventobject, changedtext) {
    frmOpenAccountSavingCareMB.txtAmount.text = autoFormatAmount(frmOpenAccountSavingCareMB.txtAmount.text);
};

function p2kwiet2012247635627_btnOASCBenDel1_onClick_seq0(eventobject) {
    onClickDelBefOpenSavingAct.call(this, 1);
};

function p2kwiet2012247635627_btnBefRs1_onClick_seq0(eventobject) {
    gblSelRel = "1";
    onClickSelRelation.call(this);
};

function p2kwiet2012247635627_btnOASCBenDel2_onClick_seq0(eventobject) {
    onClickDelBefOpenSavingAct.call(this, 2);
};

function p2kwiet2012247635627_btnBefRs2_onClick_seq0(eventobject) {
    gblSelRel = "2";
    onClickSelRelation.call(this);
};

function p2kwiet2012247635627_btnOASCBenDel3_onClick_seq0(eventobject) {
    onClickDelBefOpenSavingAct.call(this, 3);
};

function p2kwiet2012247635627_btnBefRs3_onClick_seq0(eventobject) {
    gblSelRel = "3";
    onClickSelRelation.call(this);
};

function p2kwiet2012247635627_btnOASCBenDel4_onClick_seq0(eventobject) {
    onClickDelBefOpenSavingAct.call(this, 4);
};

function p2kwiet2012247635627_btnBefRs4_onClick_seq0(eventobject) {
    gblSelRel = "4";
    onClickSelRelation.call(this);
};

function p2kwiet2012247635627_btnOASCBenDel5_onClick_seq0(eventobject) {
    onClickDelBefOpenSavingAct.call(this, 5);
};

function p2kwiet2012247635627_btnBefRs5_onClick_seq0(eventobject) {
    gblSelRel = "5";
    onClickSelRelation.call(this);
};

function p2kwiet2012247635627_linkMore3_onClick_seq0(eventobject) {
    onClickMoreSavings.call(this);
};

function p2kwiet2012247635627_btnPopUpTerminationSpa_onClick_seq0(eventobject) {
    onClickSavingCareLanding.call(this);
};

function p2kwiet2012247635627_btnPopUpTermination_onClick_seq0(eventobject) {
    onClickSavingCareLanding.call(this);
};

function p2kwiet2012247635689_frmOpenAccTermDeposit_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635689_frmOpenAccTermDeposit_preshow_seq0(eventobject, neworientation) {
    frmOpenAccTermDepositMenuPreshow.call(this);
};

function p2kwiet2012247635689_frmOpenAccTermDeposit_postshow_seq0(eventobject, neworientation) {
    frmOpenAccTermDepositMenuPostshow.call(this);
};

function p2kwiet2012247635689_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635689_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635689_segSliderTDFrom_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectTDFrmSeg.call(this);
};

function p2kwiet2012247635689_segSliderTDFrom_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247635689_txtTDamount_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAccTermDeposit.txtTDamount.text = autoFormatAmount(frmOpenAccTermDeposit.txtTDamount.text)
};

function p2kwiet2012247635689_txtTDamount_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAccTermDeposit.txtTDamount.text = autoFormatAmount(frmOpenAccTermDeposit.txtTDamount.text)
};

function p2kwiet2012247635689_txtTDamount_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAccTermDeposit.txtTDamount.text = autoFormatAmount(frmOpenAccTermDeposit.txtTDamount.text)
};

function p2kwiet2012247635689_txtTDamount_onDone_seq0(eventobject, changedtext) {
    frmOpenAccTermDeposit.txtTDamount.text = autoFormatAmount(frmOpenAccTermDeposit.txtTDamount.text)
};

function p2kwiet2012247635689_hbxTranLandToSel_onClick_seq0(eventobject) {};

function p2kwiet2012247635689_btnTranLandToSel_onClick_seq0(eventobject) {
    showTDToActsToSpa.call(this);
};

function p2kwiet2012247635689_segToTDAct_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectTDToSeg.call(this);
};

function p2kwiet2012247635689_segToTDAct_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247635689_btnTDSelAccNxtSpa_onClick_seq0(eventobject) {
    showTDConfirmation.call(this);
};

function p2kwiet2012247635689_btnOATDConfCancel_onClick_seq0(eventobject) {
    frmOccupationInfo.show();
};

function p2kwiet2012247635689_btnTDSelAccNxt_onClick_seq0(eventobject) {
    showTDConfirmation.call(this);
};

function p2kwiet2012247635689_btnTDSelAccNxt123_onClick_seq0(eventobject) {
    showTDConfirmation.call(this);
};

function p2kwiet2012247635696_frmOpenAccTermDepositToSpa_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635696_frmOpenAccTermDepositToSpa_preshow_seq0(eventobject, neworientation) {
    frmOpenAccTermDepositToSpaMenuPreshow.call(this);
};

function p2kwiet2012247635696_frmOpenAccTermDepositToSpa_postshow_seq0(eventobject, neworientation) {
    frmOpenAccTermDepositToSpaMenuPostshow.call(this);
};

function p2kwiet2012247635696_segToTDActSpa_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    selActTDToIndex = frmOpenAccTermDepositToSpa.segToTDActSpa.selectedIndex;
    frmOpenAccTermDeposit.lblTDTransToNickName.text = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].lblCustName;
    frmOpenAccTermDeposit.lblTDTransToActType.text = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].lblSliderAccN2;
    frmOpenAccTermDeposit.lblTDTransToAccBal.text = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].lblActNoval;
    frmOpenAccTermDeposit.imgOAMnthlySavPic.src = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].img1;
    frmOpenAccTermDeposit.lblOATDBranchVal.text = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].lblRemainFeeValue;
    frmOpenAccTermDeposit.hiddenActType.text = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].hiddenActType;
    frmOpenAccTermDeposit.hiddenFiident.text = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].hiddenFiident;
    gblFinActivityLogOpenAct["branchENTO"] = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].hiddenBranchNameEN;
    gblFinActivityLogOpenAct["branchTHTO"] = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].hiddenBranchNameTH;
    gblFinActivityLogOpenAct["nickTHTO"] = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].hiddenproductThaiAssign;
    gblFinActivityLogOpenAct["nickENTO"] = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].hiddenprodENGAssign;
    gblFinActivityLogOpenAct["actTypeToo"] = frmOpenAccTermDepositToSpa["segToTDActSpa"]["selectedItems"][0].hiddenActType;
    gblnormSelIndex = gbltranFromSelIndex[1];
    gblspaSelIndex = gblnormSelIndex;
    frmOpenAccTermDeposit.show();
    TMBUtil.DestroyForm(frmOpenAccTermDepositToSpa);
};

function p2kwiet2012247635762_frmOpenAcDreamSaving_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635762_frmOpenAcDreamSaving_preshow_seq0(eventobject, neworientation) {
    frmOpenAcDreamSavingMenuPreshow.call(this);
};

function p2kwiet2012247635762_frmOpenAcDreamSaving_postshow_seq0(eventobject, neworientation) {
    frmOpenAcDreamSavingMenuPostshow.call(this);
};

function p2kwiet2012247635762_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635762_btnHdrMenu_onClick_seq0(eventobject) {
    kony.print("onclick menu" + frmOpenAcDreamSaving.txtODMyDream.text)
    handleMenuBtn.call(this);
};

function p2kwiet2012247635762_btnLtArrowOD_onClick_seq0(eventobject) {
    onClickLeftDS.call(this, frmOpenAcDreamSaving.segSliderOpenDream);
};

function p2kwiet2012247635762_segSliderOpenDream_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectDSSeg.call(this, frmOpenAcDreamSaving.segSliderOpenDream);
};

function p2kwiet2012247635762_segSliderOpenDream_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247635762_btnRtArrowOD_onClick_seq0(eventobject) {
    onClickRightDS.call(this, frmOpenAcDreamSaving.segSliderOpenDream);
};

function p2kwiet2012247635762_txtODNickNameVal_onDone_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODNickNameVal.skin = txtNormalBG
};

function p2kwiet2012247635762_txtODTargetAmt_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODTargetAmt.skin = "txtFocusBG"
    var removeBahtDreamOpen = frmOpenAcDreamSaving.txtODTargetAmt.text;
    frmOpenAcDreamSaving.txtODTargetAmt.text = removeBahtDreamOpen.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
};

function p2kwiet2012247635762_txtODTargetAmt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    var entAmt = frmOpenAcDreamSaving.txtODTargetAmt.text;
    var isCrtFormt;
    isCrtFormt = amountValidationMB(entAmt);
    if (!isCrtFormt) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        return false
    }
    var indexdot = entAmt.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = entAmt.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 3) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    frmOpenAcDreamSaving.txtODTargetAmt.skin = "txtNormalBG"
    var AddOpenBahtDreamOpen = frmOpenAcDreamSaving.txtODTargetAmt.text;
    frmOpenAcDreamSaving.txtODTargetAmt.text = commaFormattedOpenAct(AddOpenBahtDreamOpen) + kony.i18n.getLocalizedString("currencyThaiBaht")
};

function p2kwiet2012247635762_txtODTargetAmt_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODTargetAmt.skin = "txtFocusBG"
    var removeBahtDreamOpen = frmOpenAcDreamSaving.txtODTargetAmt.text;
    frmOpenAcDreamSaving.txtODTargetAmt.text = removeBahtDreamOpen.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
};

function p2kwiet2012247635762_txtODTargetAmt_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODTargetAmt.skin = "txtNormalBG"
    var AddOpenBahtDreamOpen = frmOpenAcDreamSaving.txtODTargetAmt.text;
    AddOpenBahtDreamOpen1 = AddOpenBahtDreamOpen.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    var num = new Number(AddOpenBahtDreamOpen1);
    if (AddOpenBahtDreamOpen1 == null || AddOpenBahtDreamOpen1 == "" || !kony.string.isNumeric(num)) {} else {
        frmOpenAcDreamSaving.txtODTargetAmt.text = commaFormattedOpenAct(AddOpenBahtDreamOpen) + kony.i18n.getLocalizedString("currencyThaiBaht")
    }
};

function p2kwiet2012247635762_txtODTargetAmt_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODTargetAmt.skin = "txtFocusBG"
    var removeBahtDreamOpen = frmOpenAcDreamSaving.txtODTargetAmt.text;
    frmOpenAcDreamSaving.txtODTargetAmt.text = removeBahtDreamOpen.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
};

function p2kwiet2012247635762_txtODTargetAmt_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODTargetAmt.skin = "txtNormalBG"
    var AddOpenBahtDreamOpen = frmOpenAcDreamSaving.txtODTargetAmt.text;
    frmOpenAcDreamSaving.txtODTargetAmt.text = commaFormattedOpenAct(AddOpenBahtDreamOpen) + kony.i18n.getLocalizedString("currencyThaiBaht")
};

function p2kwiet2012247635762_txtODTargetAmt_onDone_seq0(eventobject, changedtext) {
    var entAmt = frmOpenAcDreamSaving.txtODTargetAmt.text;
    entAmt = entAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
    var isCrtFormt;
    isCrtFormt = amountValidationMB(entAmt);
    if (!isCrtFormt) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        return false
    }
    var indexdot = entAmt.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = entAmt.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 3) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = "";
};

function p2kwiet2012247635762_txtODTargetAmt_onTextChange_seq0(eventobject, changedtext) {
    if (frmOpenAcDreamSaving.txtODTargetAmt.text == "0" || frmOpenAcDreamSaving.txtODTargetAmt.text == "" || frmOpenAcDreamSaving.txtODTargetAmt.text == null) {
        frmOpenAcDreamSaving.hbxODMnthSavAmt.setEnabled(false);
        frmOpenAcDreamSaving.hbxODMyDream.setEnabled(false);
    } else {
        frmOpenAcDreamSaving.hbxODMnthSavAmt.setEnabled(true);
        frmOpenAcDreamSaving.hbxODMyDream.setEnabled(true);
    }
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = "";
};

function p2kwiet2012247635762_txtODMnthSavAmt_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODMnthSavAmt.skin = "txtFocusBG"
    var removeBahtDreamOpenMnth = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = removeBahtDreamOpenMnth.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
};

function p2kwiet2012247635762_txtODMnthSavAmt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    var myDreamAmtBaht = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    var tarAmount = myDreamAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
    var targetAmtBaht = frmOpenAcDreamSaving.txtODTargetAmt.text;
    var entAmount = targetAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
        //var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
        //var entAmount = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    if (kony.string.isNumeric(tarAmount) == false) {
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false
    }
    if (tarAmount.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmOpenAcDreamSaving.txtODMyDream.text = "";
        return false
    }
    var indexdot = tarAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = tarAmount.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 4) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    var noOfMnths;
    tarAmount = kony.os.toNumber(tarAmount);
    entAmount = kony.os.toNumber(entAmount);
    if (tarAmount > entAmount) {
        showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    noOfMnths = entAmount / tarAmount;
    kony.print("noOfMnths before rounding" + noOfMnths);
    noOfMnths = Math.round(noOfMnths);
    kony.print("noOfMnths after rounding" + noOfMnths);
    var deviceinfo = kony.os.deviceInfo();
    if (deviceinfo["name"] == "android") {
        noOfMnths = noOfMnths + "";
    }
    kony.print("noOfMnths" + noOfMnths);
    //if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
    frmOpenAcDreamSaving.txtODMyDream.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = noOfMnths + kony.i18n.getLocalizedString("keymonths");
    gblDreamMnths = noOfMnths;
    //}
    frmOpenAcDreamSaving.txtODMnthSavAmt.skin = "txtFocusBG"
    var AddOpenBahtDreamMnth = frmOpenAcDreamSaving.txtODMnthSavAmt.text
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = commaFormattedOpenAct(AddOpenBahtDreamMnth) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
};

function p2kwiet2012247635762_txtODMnthSavAmt_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODMnthSavAmt.skin = "txtFocusBG"
    var removeBahtDreamOpenMnth = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = removeBahtDreamOpenMnth.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
};

function p2kwiet2012247635762_txtODMnthSavAmt_SPA_onEndEditing_seq0(eventobject, changedtext) {
    var myDreamAmtBaht = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    var tarAmount = myDreamAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
    var targetAmtBaht = frmOpenAcDreamSaving.txtODTargetAmt.text;
    var entAmount = targetAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
        //var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
        //var entAmount = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    if (kony.string.isNumeric(tarAmount) == false) {
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false
    }
    if (tarAmount.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmOpenAcDreamSaving.txtODMyDream.text = "";
        return false
    }
    var indexdot = tarAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = tarAmount.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 4) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    var noOfMnths;
    tarAmount = kony.os.toNumber(tarAmount);
    entAmount = kony.os.toNumber(entAmount);
    if (tarAmount > entAmount) {
        showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    noOfMnths = entAmount / tarAmount;
    kony.print("noOfMnths before rounding" + noOfMnths);
    noOfMnths = Math.round(noOfMnths);
    kony.print("noOfMnths after rounding" + noOfMnths);
    var deviceinfo = kony.os.deviceInfo();
    if (deviceinfo["name"] == "android") {
        noOfMnths = noOfMnths + "";
    }
    kony.print("noOfMnths" + noOfMnths);
    //if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
    frmOpenAcDreamSaving.txtODMyDream.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = noOfMnths + kony.i18n.getLocalizedString("keymonths");
    gblDreamMnths = noOfMnths;
    //}
    frmOpenAcDreamSaving.txtODMnthSavAmt.skin = "txtFocusBG"
    var AddOpenBahtDreamMnth = frmOpenAcDreamSaving.txtODMnthSavAmt.text
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = commaFormattedOpenAct(AddOpenBahtDreamMnth) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
};

function p2kwiet2012247635762_txtODMnthSavAmt_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODMnthSavAmt.skin = "txtFocusBG"
    var removeBahtDreamOpenMnth = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = removeBahtDreamOpenMnth.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
};

function p2kwiet2012247635762_txtODMnthSavAmt_Android_onEndEditing_seq0(eventobject, changedtext) {
    var myDreamAmtBaht = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    var tarAmount = myDreamAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
    var targetAmtBaht = frmOpenAcDreamSaving.txtODTargetAmt.text;
    var entAmount = targetAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
        //var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
        //var entAmount = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    if (kony.string.isNumeric(tarAmount) == false) {
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false
    }
    if (tarAmount.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmOpenAcDreamSaving.txtODMyDream.text = "";
        return false
    }
    var indexdot = tarAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = tarAmount.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 4) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    var noOfMnths;
    tarAmount = kony.os.toNumber(tarAmount);
    entAmount = kony.os.toNumber(entAmount);
    if (tarAmount > entAmount) {
        showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    noOfMnths = entAmount / tarAmount;
    kony.print("noOfMnths before rounding" + noOfMnths);
    noOfMnths = Math.round(noOfMnths);
    kony.print("noOfMnths after rounding" + noOfMnths);
    var deviceinfo = kony.os.deviceInfo();
    if (deviceinfo["name"] == "android") {
        noOfMnths = noOfMnths + "";
    }
    kony.print("noOfMnths" + noOfMnths);
    //if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
    frmOpenAcDreamSaving.txtODMyDream.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = noOfMnths + kony.i18n.getLocalizedString("keymonths");
    gblDreamMnths = noOfMnths;
    //}
    frmOpenAcDreamSaving.txtODMnthSavAmt.skin = "txtFocusBG"
    var AddOpenBahtDreamMnth = frmOpenAcDreamSaving.txtODMnthSavAmt.text
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = commaFormattedOpenAct(AddOpenBahtDreamMnth) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
};

function p2kwiet2012247635762_txtODMnthSavAmt_onDone_seq0(eventobject, changedtext) {
    /* 
var myDreamAmtBaht = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
var entAmount= myDreamAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
var targetAmtBaht = frmOpenAcDreamSaving.txtODTargetAmt.text;
var tarAmount= targetAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
//var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;

//var entAmount = frmOpenAcDreamSaving.txtODMnthSavAmt.text;

if (kony.string.isNumeric(entAmount) ==  false){
showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
return false
}


if (entAmount.indexOf("0") == 0 ) {
      showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
      frmOpenAcDreamSaving.txtODMyDream.text="";
 return false
}


var indexdot=entAmount.indexOf(".");
  var decimal="";
  var remAmt="";
  kony.print("indexdot" + indexdot);
  if(indexdot > 0)
  { decimal=entAmount.substr(indexdot);
  kony.print("decimal@@" + decimal);
    if (decimal.length > 3){
    alert("Enter only 2 decimal values");
    return false;
    }
  }


var noOfMnths;
tarAmount = kony.os.toNumber(tarAmount);
entAmount = kony.os.toNumber(entAmount);

 if (entAmount  > tarAmount){
 showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
 return false;
 }
 
noOfMnths = tarAmount/entAmount;
kony.print("noOfMnths before rounding" + noOfMnths);
noOfMnths = Math.round(noOfMnths);
kony.print("noOfMnths after rounding" + noOfMnths);

var deviceinfo = kony.os.deviceInfo();
 if (deviceinfo["name"] == "android") {
 noOfMnths = noOfMnths+ "";
 }
 
kony.print("noOfMnths" + noOfMnths);
//if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
frmOpenAcDreamSaving.txtODMyDream.text = "";
frmOpenAcDreamSaving.txtODMyDream.text = noOfMnths + kony.i18n.getLocalizedString("keymonths");
gblDreamMnths = noOfMnths;
//}
frmOpenAcDreamSaving.txtODMnthSavAmt.skin= "txtFocusBG"
var AddOpenBahtDreamMnth =frmOpenAcDreamSaving.txtODMnthSavAmt.text
frmOpenAcDreamSaving.txtODMnthSavAmt.text = commaFormattedOpenAct(AddOpenBahtDreamMnth)+ kony.i18n.getLocalizedString("currencyThaiBaht")

 */
    kony.print("firing ondone @@@")
        /* 
checkDreamOpen.call(this);

 */
};

function p2kwiet2012247635762_txtODMyDream_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODMyDream.skin = "txtFocusBG"
    kony.print("firing on beign edi of months" + frmOpenAcDreamSaving.txtODMyDream.text)
    var removeOpenBahtDreamMntmTODream = frmOpenAcDreamSaving.txtODMyDream.text
    frmOpenAcDreamSaving.txtODMyDream.text = removeOpenBahtDreamMntmTODream.replace(kony.i18n.getLocalizedString("keymonths"), "")
    kony.print("firing on beign edi of months" + frmOpenAcDreamSaving.txtODMyDream.text)
};

function p2kwiet2012247635762_txtODMyDream_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    calMonthlySavingCalculatorOpen.call(this);
};

function p2kwiet2012247635762_txtODMyDream_SPA_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODMyDream.skin = "txtFocusBG"
    var removeOpenBahtDreamMntmTODream = frmOpenAcDreamSaving.txtODMyDream.text
    frmOpenAcDreamSaving.txtODMyDream.text = removeOpenBahtDreamMntmTODream.replace(kony.i18n.getLocalizedString("keymonths"), "")
};

function p2kwiet2012247635762_txtODMyDream_SPA_onEndEditing_seq0(eventobject, changedtext) {
    calMonthlySavingCalculatorOpen.call(this);
};

function p2kwiet2012247635762_txtODMyDream_Android_onBeginEditing_seq0(eventobject, changedtext) {
    frmOpenAcDreamSaving.txtODMyDream.skin = "txtFocusBG"
    var removeOpenBahtDreamMntmTODream = frmOpenAcDreamSaving.txtODMyDream.text
    frmOpenAcDreamSaving.txtODMyDream.text = removeOpenBahtDreamMntmTODream.replace(kony.i18n.getLocalizedString("keymonths"), "")
};

function p2kwiet2012247635762_txtODMyDream_Android_onEndEditing_seq0(eventobject, changedtext) {
    calMonthlySavingCalculatorOpen.call(this);
};

function p2kwiet2012247635762_txtODMyDream_onDone_seq0(eventobject, changedtext) {
    /* 
calMonthlySavingCalculatorOpen.call(this);

 */
};

function p2kwiet2012247635762_btnDSNextSpa_onClick_seq0(eventobject) {
    setDreamSaveSetData.call(this);
};

function p2kwiet2012247635762_btnOATDConfCancel_onClick_seq0(eventobject) {
    frmOccupationInfo.show();
};

function p2kwiet2012247635762_btnDSNext_onClick_seq0(eventobject) {
    setDreamSaveSetData.call(this);
};

function p2kwiet2012247635845_frmOpenActDSAck_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635845_frmOpenActDSAck_preshow_seq0(eventobject, neworientation) {
    frmOpenActDSAckMenuPreshow.call(this);
};

function p2kwiet2012247635845_frmOpenActDSAck_postshow_seq0(eventobject, neworientation) {
    frmOpenActDSAckCalendarMenuPostshow.call(this);
};

function p2kwiet2012247635845_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635845_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635845_btnRight_onClick_seq0(eventobject) {
    enableAckList.call(this, frmOpenActDSAck);
    /* 
iPhoneDisplay.call(this,frmOpenActDSAck,    80,    81);

 */
};

function p2kwiet2012247635845_button47428498625_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActDS.call(this, "pdf");
};

function p2kwiet2012247635845_btnPhoto_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActDS.call(this, "png");
};

function p2kwiet2012247635845_btnEmailto_onClick_seq0(eventobject) {
    requestFromForm = "frmOpenActDSAck";
    postOnWall.call(this);
};

function p2kwiet2012247635845_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247635845_btnOADSAckReturnSpa_onClick_seq0(eventobject) {
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247635845_btnOADSAckOpenMoreSpa_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247635845_btnOADSAckReturn_onClick_seq0(eventobject) {
    frmAccountSummaryLanding = null;
    frmAccountSummaryLandingGlobals();
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247635845_btnOADSAckOpenMore_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247635925_frmOpenActDSAckCalendar_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635925_frmOpenActDSAckCalendar_preshow_seq0(eventobject, neworientation) {
    frmOpenActDSAckCalendarMenuPreshow.call(this);
};

function p2kwiet2012247635925_frmOpenActDSAckCalendar_postshow_seq0(eventobject, neworientation) {
    frmOpenActDSAckCalendarMenuPostshow.call(this);
};

function p2kwiet2012247635925_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635925_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635925_btnRight_onClick_seq0(eventobject) {
    enableAckList.call(this, frmOpenActDSAck);
    /* 
iPhoneDisplay.call(this,frmOpenActDSAck,    80,    81);

 */
};

function p2kwiet2012247635925_button47428498625_onClick_seq0(eventobject) {
    savePDFExecTOADSMB.call(this, "pdf");
};

function p2kwiet2012247635925_btnPhoto_onClick_seq0(eventobject) {
    savePDFExecTOADSMB.call(this, "png");
};

function p2kwiet2012247635925_btnEmailto_onClick_seq0(eventobject) {
    fbShareExecutedTxnMB.call(this);
};

function p2kwiet2012247635925_btnOADSAckReturn_onClick_seq0(eventobject) {
    //frmMBMyActivities.show();
    //showCalendar(gsSelectedDate,frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247635994_frmOpenActDSConfirm_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247635994_frmOpenActDSConfirm_preshow_seq0(eventobject, neworientation) {
    frmOpenActDSConfirmMenuPreshow.call(this);
};

function p2kwiet2012247635994_frmOpenActDSConfirm_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
    if (flowSpa) {
        isMenuRendered = false;
        isMenuShown = false;
        frmOpenActDSConfirm.scrollboxMain.scrollToEnd();
    }
};

function p2kwiet2012247635994_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247635994_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247635994_button447264549152907_onClick_seq0(eventobject) {
    frmOpnActSelAct.show();
};

function p2kwiet2012247635994_btnODConfCancelSpa_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247635994_btnODConfConfirmSpa_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247635994_btnODConfCancel_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247635994_btnODConfConfirm_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247636133_frmOpenActSavingCareCnfNAck_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636133_frmOpenActSavingCareCnfNAck_preshow_seq0(eventobject, neworientation) {
    frmOpenActSavingCareCnfNAckMenuPreshow.call(this);
};

function p2kwiet2012247636133_frmOpenActSavingCareCnfNAck_postshow_seq0(eventobject, neworientation) {
    frmOpenActSavingCareCnfNAckMenuPostshow.call(this);
};

function p2kwiet2012247636133_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636133_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636133_btnRight_onClick_seq0(eventobject) {
    if (frmOpenActSavingCareCnfNAck.btnRight.skin == btnEdit) {
        if (flowSpa) {
            //alert("the setting is"+gblspaSelIndex)
            gblspaSelIndex = gblnormSelIndex;
        }
        frmOpenAccountSavingCareMB.show();
    } else {
        enableAckList.call(this, frmOpenActSavingCareCnfNAck);
    }
};

function p2kwiet2012247636133_button47428498625_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActSavingCare.call(this, "pdf");
};

function p2kwiet2012247636133_btnPhoto_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActSavingCare.call(this, "png");
};

function p2kwiet2012247636133_btnEmailto_onClick_seq0(eventobject) {
    requestFromForm = "frmOpenActSavingCareCnfNAck";
    postOnWall.call(this);
};

function p2kwiet2012247636133_hbox101271281131304_onClick_seq0(eventobject) {
    hideUnhideOA.call(this, frmOpenActSavingCareCnfNAck.lblOpenActCnfmBal, frmOpenActSavingCareCnfNAck.lblOpenActCnfmBalAmt, frmOpenActSavingCareCnfNAck.lblHide);
};

function p2kwiet2012247636133_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247636133_btnOATDAckReturnSpa_onClick_seq0(eventobject) {
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247636133_btnOATDAckOpenMoreSpa_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247636133_btnOATDConfCancelSpa_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636133_btnOATDConfConfirmSpa_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247636133_btnOATDAckReturn_onClick_seq0(eventobject) {
    frmAccountSummaryLanding = null;
    frmAccountSummaryLandingGlobals();
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247636133_btnOATDAckOpenMore_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247636133_btnOATDConfCancel_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636133_btnOATDConfConfirm_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247636261_frmOpenActSavingCareCnfNAckCalendar_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636261_frmOpenActSavingCareCnfNAckCalendar_preshow_seq0(eventobject, neworientation) {
    frmOpenActSavingCareCnfNAckCalendarMenuPreshow.call(this);
};

function p2kwiet2012247636261_frmOpenActSavingCareCnfNAckCalendar_postshow_seq0(eventobject, neworientation) {
    frmOpenActSavingCareCnfNAckCalendarMenuPostshow.call(this);
};

function p2kwiet2012247636261_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636261_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636261_btnRight_onClick_seq0(eventobject) {
    enableAckList.call(this, frmOpenActSavingCareCnfNAckCalendar);
};

function p2kwiet2012247636261_button47428498625_onClick_seq0(eventobject) {
    savePDFExecTOASCMB.call(this, "pdf");
};

function p2kwiet2012247636261_btnPhoto_onClick_seq0(eventobject) {
    savePDFExecTOASCMB.call(this, "png");
};

function p2kwiet2012247636261_btnEmailto_onClick_seq0(eventobject) {
    fbShareExecutedTxnMB.call(this);
};

function p2kwiet2012247636261_hbox101271281131304_onClick_seq0(eventobject) {
    if (frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBalAmt.isVisible == true) {
        kony.print("@@ into if");
        frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBal.setVisibility(false);
        frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBalAmt.setVisibility(false)
        frmOpenActSavingCareCnfNAckCalendar.lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
    } else {
        kony.print("@@ into else");
        frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBal.setVisibility(true);
        frmOpenActSavingCareCnfNAckCalendar.lblOpenActCnfmBalAmt.setVisibility(true)
        frmOpenActSavingCareCnfNAckCalendar.lblHide.text = kony.i18n.getLocalizedString("Hide")
    }
};

function p2kwiet2012247636261_btnOATDAckReturn_onClick_seq0(eventobject) {
    //frmMBMyActivities.show();
    //showCalendar(gsSelectedDate,frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247636306_frmOpenActSelProd_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636306_frmOpenActSelProd_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636306_frmOpenActSelProd_preshow_seq0(eventobject, neworientation) {
    frmOpenActSelProdMenuPreshow.call(this);
};

function p2kwiet2012247636306_frmOpenActSelProd_postshow_seq0(eventobject, neworientation) {
    frmOpenActSelProdMenuPostshow.call(this);
};

function p2kwiet2012247636306_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636306_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636306_btnRight_onClick_seq0(eventobject) {
    onClickContactList.call(this);
};

function p2kwiet2012247636306_vbxOpenActUse_onClick_seq0(eventobject) {
    setDataForUse.call(this);
};

function p2kwiet2012247636306_vbxOpenActSave_onClick_seq0(eventobject) {
    setDataForSave.call(this);
};

function p2kwiet2012247636306_vbxOpenActTerm_onClick_seq0(eventobject) {
    setDataForTD.call(this);
};

function p2kwiet2012247636306_vbox473361467796307_onClick_seq0(eventobject) {
    setDataForUse.call(this);
};

function p2kwiet2012247636306_vbox473361467796315_onClick_seq0(eventobject) {
    setDataForSave.call(this);
};

function p2kwiet2012247636306_vbox473361467796311_onClick_seq0(eventobject) {
    setDataForTD.call(this);
};

function p2kwiet2012247636306_segOpenActSelProd_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    snippetCodeOnClickOfAcctSegmentMB.call(this);
};

function p2kwiet2012247636405_frmOpenActTDAck_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636405_frmOpenActTDAck_preshow_seq0(eventobject, neworientation) {
    frmOpenActTDAckMenuPreshow.call(this);
};

function p2kwiet2012247636405_frmOpenActTDAck_postshow_seq0(eventobject, neworientation) {
    frmOpenActTDAckMenuPostshow.call(this);
};

function p2kwiet2012247636405_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636405_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636405_btnRight_onClick_seq0(eventobject) {
    enableAckList.call(this, frmOpenActTDAck);
};

function p2kwiet2012247636405_button47428498625_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActTD.call(this, "pdf");
};

function p2kwiet2012247636405_btnPhoto_onClick_seq0(eventobject) {
    saveAsPdfImageOpenActTD.call(this, "png");
};

function p2kwiet2012247636405_btnEmailto_onClick_seq0(eventobject) {
    requestFromForm = "frmOpenActTDAck";
    postOnWall.call(this);
};

function p2kwiet2012247636405_hbox101271281131304_onClick_seq0(eventobject) {
    hideUnhideOA.call(this, frmOpenActTDAck.label47505874739957, frmOpenActTDAck.lblOpenActCnfmBalAmt, frmOpenActTDAck.lblHide);
};

function p2kwiet2012247636405_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247636405_btnOATDAckReturnSpa_onClick_seq0(eventobject) {
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247636405_btnOATDAckOpenMoreSpa_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247636405_btnOATDAckReturn_onClick_seq0(eventobject) {
    frmAccountSummaryLanding = null;
    frmAccountSummaryLandingGlobals();
    showAccuntSummaryScreen.call(this);
};

function p2kwiet2012247636405_btnOATDAckOpenMore_onClick_seq0(eventobject) {
    ivokeCustActInqForOpenAct.call(this, null);
};

function p2kwiet2012247636497_frmOpenActTDAckCalendar_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636497_frmOpenActTDAckCalendar_preshow_seq0(eventobject, neworientation) {
    frmOpenActTDAckCalendarMenuPreshow.call(this);
};

function p2kwiet2012247636497_frmOpenActTDAckCalendar_postshow_seq0(eventobject, neworientation) {
    frmOpenActTDAckCalendarMenuPostshow.call(this);
};

function p2kwiet2012247636497_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636497_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636497_btnRight_onClick_seq0(eventobject) {
    enableAckList.call(this, frmOpenActTDAckCalendar);
};

function p2kwiet2012247636497_button47428498625_onClick_seq0(eventobject) {
    savePDFExecTOATDMB.call(this, "pdf");
};

function p2kwiet2012247636497_btnPhoto_onClick_seq0(eventobject) {
    savePDFExecTOATDMB.call(this, "png");
};

function p2kwiet2012247636497_btnEmailto_onClick_seq0(eventobject) {
    fbShareExecutedTxnMB.call(this);
};

function p2kwiet2012247636497_hbox101271281131304_onClick_seq0(eventobject) {
    if (frmOpenActTDAckCalendar.lblOpenActCnfmBalAmt.isVisible == true) {
        kony.print("@@ into if");
        frmOpenActTDAckCalendar.lblOpenActCnfmBal.setVisibility(false);
        frmOpenActTDAckCalendar.lblOpenActCnfmBalAmt.setVisibility(false)
        frmOpenActTDAckCalendar.lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
    } else {
        kony.print("@@ into else");
        frmOpenActTDAckCalendar.lblOpenActCnfmBal.setVisibility(true);
        frmOpenActTDAckCalendar.lblOpenActCnfmBalAmt.setVisibility(true)
        frmOpenActTDAckCalendar.lblHide.text = kony.i18n.getLocalizedString("Hide")
    }
};

function p2kwiet2012247636497_btnOATDAckReturn_onClick_seq0(eventobject) {
    //frmMBMyActivities.show();
    //showCalendar(gsSelectedDate,frmMBMyActivities);
    MBMyActivitiesShowCalendar();
};

function p2kwiet2012247636564_frmOpenActTDConfirm_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636564_frmOpenActTDConfirm_preshow_seq0(eventobject, neworientation) {
    frmOpenActTDConfirmMenuPreshow.call(this);
};

function p2kwiet2012247636564_frmOpenActTDConfirm_postshow_seq0(eventobject, neworientation) {
    frmOpenActTDConfirmMenuPostshow.call(this);
};

function p2kwiet2012247636564_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636564_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636564_button447264549152907_onClick_seq0(eventobject) {
    if (flowSpa) {
        gblspaSelIndex = gblnormSelIndex;
    }
    frmOpenAccTermDeposit.show();
};

function p2kwiet2012247636564_btnOATDConfCancelSpa_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636564_btnOATDConfConfirmSpa_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247636564_btnOATDConfCancel_onClick_seq0(eventobject) {
    frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
    frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
    if (flowSpa) {
        frmOpenActSelProd.label473361467796309.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label473361467796317.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label473361467796313.text = kony.i18n.getLocalizedString("TermDepositMB");
    } else {
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
    }
    setDataForUse();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636564_btnOATDConfConfirm_onClick_seq0(eventobject) {
    showTransPwdPopForOpenDS.call(this);
};

function p2kwiet2012247636606_frmOpenProdDetnTnC_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636606_frmOpenProdDetnTnC_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636606_frmOpenProdDetnTnC_preshow_seq0(eventobject, neworientation) {
    frmOpenProdDetnTnCMenuPreshow.call(this);
};

function p2kwiet2012247636606_frmOpenProdDetnTnC_postshow_seq0(eventobject, neworientation) {
    frmOpenProdDetnTnCMenuPostshow.call(this);
};

function p2kwiet2012247636606_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636606_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636606_btnRight_onClick_seq0(eventobject) {
    var skin = frmOpenProdDetnTnC.btnRight.skin
    if (frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible == false) {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = true;
        frmOpenProdDetnTnC.imgHeaderRight.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShareFoc";
    } else {
        frmOpenProdDetnTnC.hboxSaveCamEmail.isVisible = false;
        frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
        frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
        frmOpenProdDetnTnC.btnRight.skin = "btnShare";
    }
};

function p2kwiet2012247636606_button47505874736294_onClick_seq0(eventobject) {
    var pdfFlowType = "";
    /* 
if(gblSelOpenActProdCode == "200"){pdfFlowType = "200NormalSaving"} 
else if(gblSelOpenActProdCode == "220"){pdfFlowType = "220TMBNoFeeSavingAccount"} 
else if(gblSelOpenActProdCode == "221"){pdfFlowType = "221TMBNoFixedAccount"}
else if(gblSelOpenActProdCode == "222" ){pdfFlowType = "222TMBFreeFlow"}
else if(gblSelOpenActProdCode == "206"){pdfFlowType = "206TMBDreamSaving"}
else if(gblSelOpenActProdCode == "203"){pdfFlowType = "203TMBSavingsCare"}
else if(gblSelOpenActProdCode == "300"){pdfFlowType = "300TD3Month"}
else if(gblSelOpenActProdCode == "301"){pdfFlowType = "301TD6Month"}
else if(gblSelOpenActProdCode == "302"){pdfFlowType = "302TD12Month"}
else if(gblSelOpenActProdCode == "601"){pdfFlowType = "601TD24Month"}
else if(gblSelOpenActProdCode == "602"){pdfFlowType = "602TD36Month"}
else if(gblSelOpenActProdCode == "659"){pdfFlowType = "659TMBUpUpTD24Month"}
else if(gblSelOpenActProdCode == "664"){pdfFlowType = "664TMBQuickInterestTD12Month"}
else if(gblSelOpenActProdCode == "666"){pdfFlowType = "666TMBQuickInterestTD3Month"}


 */
    passingProdDescTnCEmailMB.call(this);
    onClickDownloadTnC.call(this, "pdf", prodDescTnC);
};

function p2kwiet2012247636606_button47505874736298_onClick_seq0(eventobject) {
    passingProdDescTnCEmailMB.call(this);
    onClickEmailTnCMBOpenAccount.call(this, prodDescTnC, "Mobile Banking", "02");
};

function p2kwiet2012247636606_btnOpenProdContinueSpa_onClick_seq0(eventobject) {
    /* 
frmOpenProdDetnTnC.btnRight.setVisibility(true);
frmOpenProdDetnTnC.hbxOpnProdDet.setVisibility(false);
frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
frmOpenProdDetnTnC.lblOpenActDesc.text=kony.i18n.getLocalizedString('TCMessage')
frmOpenProdDetnTnC.lblOpenActDescSubTitle.setFocus(true);
if(flowSpa)
{
  frmOpenProdDetnTnC.btnOpenProdAgreeSpa.setVisibility(true);
  frmOpenProdDetnTnC.btnOpenProdCancelSpa.setVisibility(true);
  frmOpenProdDetnTnC.btnOpenProdContinueSpa.setVisibility(false);
    frmOpenProdDetnTnC.btnOpenProdAgreeSpa.text=kony.i18n.getLocalizedString('keyAgreeButton');
   frmOpenProdDetnTnC.btnOpenProdCancelSpa.text=kony.i18n.getLocalizedString('keyCancelButton');
}
else
{
  frmOpenProdDetnTnC.btnOpenProdAgree.setVisibility(true);
  frmOpenProdDetnTnC.btnOpenProdCancel.setVisibility(true);
  frmOpenProdDetnTnC.btnOpenProdContinue.setVisibility(false);
}
frmOpenProdDetnTnC.show();

 */
    loadTermsNConditionOpenAccountMB.call(this);
};

function p2kwiet2012247636606_btnOpenProdCancelSpa_onClick_seq0(eventobject) {
    //frmOpenProdDetnTnC.scrollbox474135225108084.containerHeight = 81
    frmOpenActSelProdPreShow();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636606_btnOpenProdAgreeSpa_onClick_seq0(eventobject) {
    onClickAgreeOPenAcnt.call(this);
};

function p2kwiet2012247636606_btnTermsBack_onClick_seq0(eventobject) {
    //frmOpenProdDetnTnC.scrollbox474135225108084.containerHeight = 81
    frmOpenActSelProdPreShow();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636606_btnOpenProdContinue_onClick_seq0(eventobject) {
    loadTermsNConditionOpenAccountMB.call(this);
};

function p2kwiet2012247636606_btnOpenProdCancel_onClick_seq0(eventobject) {
    //frmOpenProdDetnTnC.scrollbox474135225108084.containerHeight = 81
    frmOpenActSelProdPreShow();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636606_btnOpenProdAgree_onClick_seq0(eventobject) {
    /* 
onClickAgreeOPenAcnt.call(this);

 */
    onClickCheckContactInfo.call(this);
};

function p2kwiet2012247636648_frmOpenProdEditAddress_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636648_frmOpenProdEditAddress_Windows8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636648_frmOpenProdEditAddress_WinPhone8_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636648_frmOpenProdEditAddress_preshow_seq0(eventobject, neworientation) {
    frmOpenProdEditAddressMenuPreshow.call(this);
};

function p2kwiet2012247636648_frmOpenProdEditAddress_postshow_seq0(eventobject, neworientation) {
    frmOpenProdEditAddressMenuPostshow.call(this);
};

function p2kwiet2012247636648_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636648_hbox4758937265922_onClick_seq0(eventobject) {
    gblAddressFlag = 1;
    gblMyProfileAddressFlag = "state";
    editMyProvince.call(this);
};

function p2kwiet2012247636648_hbox4758937266356_onClick_seq0(eventobject) {
    gblAddressFlag = 2;
    gblMyProfileAddressFlag = "district";
    editMyDistrict.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247636648_hbox4758937266374_onClick_seq0(eventobject) {
    gblAddressFlag = 3;
    gblMyProfileAddressFlag = "subdistrict";
    editMySubDistrict.call(this);
    /* 
populateAddressFieldsinPopup.call(this);

 */
};

function p2kwiet2012247636648_hbox4758937266440_onClick_seq0(eventobject) {
    gblAddressFlag = 4;
    gblMyProfileAddressFlag = "zipcode";
    editMyZipcode.call(this);
};

function p2kwiet2012247636648_btnCancelSpa_onClick_seq0(eventobject) {
    showAddressForPackageProductMB.call(this);
};

function p2kwiet2012247636648_btnSaveSpa_onClick_seq0(eventobject) {
    openProductAddressValidation.call(this);
};

function p2kwiet2012247636648_btnCancelEdit_onClick_seq0(eventobject) {
    showAddressForPackageProductMB.call(this);
};

function p2kwiet2012247636648_btnEditSave_onClick_seq0(eventobject) {
    openProductAddressValidation.call(this);
};

function p2kwiet2012247636677_frmOpenProdEditAddressConfirm_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636677_frmOpenProdEditAddressConfirm_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636677_frmOpenProdEditAddressConfirm_preshow_seq0(eventobject, neworientation) {
    frmOpenProdEditAddressConfirmMenuPreshow.call(this);
};

function p2kwiet2012247636677_frmOpenProdEditAddressConfirm_postshow_seq0(eventobject, neworientation) {
    frmOpenProdEditAddressConfirmMenuPostshow.call(this);
};

function p2kwiet2012247636677_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636677_btnOpenProdCancelSpa_onClick_seq0(eventobject) {
    showAddressForPackageProductMB.call(this);
};

function p2kwiet2012247636677_btnOpenProdAgreeSpa_onClick_seq0(eventobject) {
    openProdSaveAddressOTP.call(this);
};

function p2kwiet2012247636677_btnOpenProdCancel_onClick_seq0(eventobject) {
    showAddressForPackageProductMB.call(this);
};

function p2kwiet2012247636677_btnOpenProdAgree_onClick_seq0(eventobject) {
    openProdSaveAddressOTP.call(this);
};

function p2kwiet2012247636716_frmOpenProdViewAddress_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636716_frmOpenProdViewAddress_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636716_frmOpenProdViewAddress_preshow_seq0(eventobject, neworientation) {
    frmOpenProdViewAddressMenuPreshow.call(this);
};

function p2kwiet2012247636716_frmOpenProdViewAddress_postshow_seq0(eventobject, neworientation) {
    frmOpenProdViewAddressMenuPostshow.call(this);
};

function p2kwiet2012247636716_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636716_btnEdit_onClick_seq0(eventobject) {
    /* 
editAddressInOpenProduct.call(this);

 */
    editbuttonflag = "profile";
    gblOpenProdAddress = false;
    getMBEditContactStatus.call(this);
};

function p2kwiet2012247636716_btnOpenProdCancelSpa_onClick_seq0(eventobject) {
    frmOpenActSelProdPreShow();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636716_btnOpenProdAgreeSpa_onClick_seq0(eventobject) {
    gotoForUseOpenAccountForm.call(this);
};

function p2kwiet2012247636716_btnOpenProdCancel_onClick_seq0(eventobject) {
    frmOpenActSelProdPreShow();
    frmOpenActSelProd.show();
};

function p2kwiet2012247636716_btnOpenProdAgree_onClick_seq0(eventobject) {
    gotoForUseOpenAccountForm.call(this);
};

function p2kwiet2012247636763_frmOpnActSelAct_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636763_frmOpnActSelAct_preshow_seq0(eventobject, neworientation) {
    frmOpnActSelActMenuPreshow.call(this);
};

function p2kwiet2012247636763_frmOpnActSelAct_postshow_seq0(eventobject, neworientation) {
    frmOpnActSelActMenuPostshow.call(this);
};

function p2kwiet2012247636763_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636763_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636763_segNSSlider_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickConnectDSSeg.call(this, frmOpnActSelAct.segNSSlider);
};

function p2kwiet2012247636763_segNSSlider_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247636763_txtOpenActSelAmt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    frmOpnActSelAct.txtOpenActSelAmt.text = autoFormatAmount(frmOpnActSelAct.txtOpenActSelAmt.text);
};

function p2kwiet2012247636763_txtOpenActSelAmt_SPA_onEndEditing_seq0(eventobject, changedtext) {
    frmOpnActSelAct.txtOpenActSelAmt.text = autoFormatAmount(frmOpnActSelAct.txtOpenActSelAmt.text);
};

function p2kwiet2012247636763_txtOpenActSelAmt_Android_onEndEditing_seq0(eventobject, changedtext) {
    frmOpnActSelAct.txtOpenActSelAmt.text = autoFormatAmount(frmOpnActSelAct.txtOpenActSelAmt.text);
};

function p2kwiet2012247636763_txtOpenActSelAmt_onDone_seq0(eventobject, changedtext) {
    frmOpnActSelAct.txtOpenActSelAmt.text = autoFormatAmount(frmOpnActSelAct.txtOpenActSelAmt.text);
};

function p2kwiet2012247636763_btnDreamSavecombo_onClick_seq0(eventobject) {
    showDateDreamSav.call(this);
};

function p2kwiet2012247636763_btnPopUpTerminationSpa_onClick_seq0(eventobject) {
    onClickNextselAct.call(this);
};

function p2kwiet2012247636763_btnOATDConfCancel_onClick_seq0(eventobject) {
    frmOccupationInfo.show();
};

function p2kwiet2012247636763_btnPopUpTermination_onClick_seq0(eventobject) {
    onClickNextselAct.call(this);
};

function p2kwiet2012247636823_frmPostLoginMenuNew_preshow_seq0(eventobject, neworientation) {
    frmPostLoginMenuNewMenuPreshow.call(this);
};

function p2kwiet2012247636823_frmPostLoginMenuNew_postshow_seq0(eventobject, neworientation) {
    frmPostLoginMenuNewMenuPostshow.call(this);
};

function p2kwiet2012247636823_hbox47425438924813_onClick_seq0(eventobject) {
    getTransferFromAccounts.call(this);
};

function p2kwiet2012247636849_FlexContainer0139edae06eff48_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onClickPreLoginBtn.call(this);
};

function p2kwiet2012247636849_segPreLogin_onRowClick_seq0(eventobject, sectionNumber, rowNumber, selectedState) {
    onClickMenuPreMenu.call(this);
};

function p2kwiet2012247636849_btnEng_onClick_seq0(eventobject) {
    changeLocale.call(this, eventobject);
};

function p2kwiet2012247636849_btnThai_onClick_seq0(eventobject) {
    changeLocale.call(this, eventobject);
};

function p2kwiet2012247636849_FlexContainer06852832624d145_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onClickClosebuttonMenu.call(this);
};

function p2kwiet2012247636849_flexContainer968116430376702_onTouchStart_seq0(eventobject, x, y, contextInfo) {
    onClickClosebuttonMenu.call(this);
};

function p2kwiet2012247636882_frmPreTransferMB_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636882_frmPreTransferMB_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636882_frmPreTransferMB_preshow_seq0(eventobject, neworientation) {
    frmPreTransferMBMenuPreshow.call(this);
};

function p2kwiet2012247636882_frmPreTransferMB_postshow_seq0(eventobject, neworientation) {
    frmPreTransferMBMenuPostshow.call(this);
};

function p2kwiet2012247636882_frmPreTransferMB_init_seq0(eventobject, neworientation) {
    ehFrmTransferLanding_frmTransferLanding_init.call(this);
};

function p2kwiet2012247636882_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636882_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636882_vbox48748658383009_onClick_seq0(eventobject) {
    displayImgToAccount.call(this, true);
    toTransferPageFromPreLandingPage.call(this);
};

function p2kwiet2012247636882_vbox48748658385763_onClick_seq0(eventobject) {
    displayImgToMobile.call(this, true);
    toTransferPageFromPreLandingPage.call(this);
};

function p2kwiet2012247636882_vbox48748658387422_onClick_seq0(eventobject) {
    displayImgToCitizen.call(this, true);
    toTransferPageFromPreLandingPage.call(this);
};

function p2kwiet2012247636921_frmPromotion_Android_onDeviceBack_seq0(eventobject, neworientation) {
    callDummy.call(this);
};

function p2kwiet2012247636921_frmPromotion_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636921_frmPromotion_preshow_seq0(eventobject, neworientation) {
    frmPromotionMenuPreshow.call(this);
};

function p2kwiet2012247636921_frmPromotion_postshow_seq0(eventobject, neworientation) {
    frmPromotionMenuPostshow.call(this);
};

function p2kwiet2012247636921_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636921_btnHdrMenu_onClick_seq0(eventobject) {
    frmPromotion_btnHdrMenu_onClick.call(this);
};

function p2kwiet2012247636921_cbxPromotion_onSelection_seq0(eventobject) {
    onSelectPromtionPopUpdrop.call(this);
};

function p2kwiet2012247636921_cbxPromotionbtn_onClick_seq0(eventobject) {
    setDatatoSeg.call(this);
};

function p2kwiet2012247636921_hbox474969373109363_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636921_vbox474969373109364_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636921_segPromotions_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickPromotionSeg.call(this);
};

function p2kwiet2012247636959_frmPromotionDetails_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636959_frmPromotionDetails_preshow_seq0(eventobject, neworientation) {
    frmPromotionDetailsMenuPreshow.call(this);
};

function p2kwiet2012247636959_frmPromotionDetails_postshow_seq0(eventobject, neworientation) {
    frmPromotionDetailsMenuPostshow.call(this);
};

function p2kwiet2012247636959_vbox10402376864305_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247636959_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247636959_btnSave_onClick_seq0(eventobject) {
    onClickShareIcon.call(this);
};

function p2kwiet2012247636959_btnPDF_onClick_seq0(eventobject) {
    savePromotionsPDF.call(this, "pdf");
};

function p2kwiet2012247636959_btnPhoto_onClick_seq0(eventobject) {
    savePromotionsPDF.call(this, "png");
};

function p2kwiet2012247636959_btnFB_onClick_seq0(eventobject) {
    sharePromotionOnFB.call(this);
};

function p2kwiet2012247636959_bttnBack_onClick_seq0(eventobject) {
    onClickBackPromotionDetails.call(this);
    stopPromotionsTimer.call(this);
};

function p2kwiet2012247636959_button1399939188243213_onClick_seq0(eventobject) {
    onClickBackPromotionDetails.call(this);
    stopPromotionsTimer.call(this);
};

function p2kwiet2012247636959_btnMBpromo_onClick_seq0(eventobject) {
    getHotPromotionsResult.call(this);
};

function p2kwiet2012247636969_frmRegAnyIdAnnouncement_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247636969_frmRegAnyIdAnnouncement_preshow_seq0(eventobject, neworientation) {
    frmRegAnyIdAnnouncementMenuPreshow.call(this);
};

function p2kwiet2012247636969_frmRegAnyIdAnnouncement_postshow_seq0(eventobject, neworientation) {
    frmRegAnyIdAnnouncementMenuPostshow.call(this);
};

function p2kwiet2012247636969_btnLater_onClick_seq0(eventobject) {
    onClickOfLaterFrmRegAnyIdAnnouncement.call(this);
};

function p2kwiet2012247636969_btnRegNow_onClick_seq0(eventobject) {
    onClickOfRegNowFrmRegAnyIdAnnouncement.call(this);
};

function p2kwiet2012247636978_frmRegistrationPopup_preshow_seq0(eventobject) {
    frmRegistrationPopupMenuPreshow.call(this);
};

function p2kwiet2012247636978_btnContinue_onClick_seq0(eventobject) {
    showFPSettingForm.call(this);
};

function p2kwiet2012247636978_btnCancel_onClick_seq0(eventobject) {
    showAcntSummary.call(this);
};

function p2kwiet2012247637014_frmRequestHistory_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637014_frmRequestHistory_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637014_frmRequestHistory_preshow_seq0(eventobject, neworientation) {
    frmRequestHistoryMenuPreshow.call(this);
};

function p2kwiet2012247637014_frmRequestHistory_postshow_seq0(eventobject, neworientation) {
    frmRequestHistoryMenuPostshow.call(this);
};

function p2kwiet2012247637014_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637014_btnHdrMenu_onClick_seq0(eventobject) {
    onClickMenuBtnInHdr.call(this);
};

function p2kwiet2012247637067_frmSchedule_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637067_frmSchedule_preshow_seq0(eventobject, neworientation) {
    frmScheduleMenuPreshow.call(this);
};

function p2kwiet2012247637067_frmSchedule_postshow_seq0(eventobject, neworientation) {
    frmScheduleMenuPostshow.call(this);
};

function p2kwiet2012247637067_btnHdrMenu_onClick_seq0(eventobject) {
    returnFromScheduleForm.call(this);
};

function p2kwiet2012247637067_calScheduleStartDate_onSelection_seq0(eventobject, isValidDateSelected) {
    validateOnSelectDate.call(this);
};

function p2kwiet2012247637067_btnDaily_onClick_seq0(eventobject) {
    repeatScheduleButtonSet.call(this, eventobject);
};

function p2kwiet2012247637067_btnWeekly_onClick_seq0(eventobject) {
    repeatScheduleButtonSet.call(this, eventobject);
};

function p2kwiet2012247637067_btnMonthly_onClick_seq0(eventobject) {
    repeatScheduleButtonSet.call(this, eventobject);
};

function p2kwiet2012247637067_btnYearly_onClick_seq0(eventobject) {
    repeatScheduleButtonSet.call(this, eventobject);
};

function p2kwiet2012247637067_btnNever_onClick_seq0(eventobject) {
    endScheduleButtonSet.call(this, eventobject);
};

function p2kwiet2012247637067_btnAfter_onClick_seq0(eventobject) {
    endScheduleButtonSet.call(this, eventobject);
};

function p2kwiet2012247637067_btnOnDate_onClick_seq0(eventobject) {
    endScheduleButtonSet.call(this, eventobject);
};

function p2kwiet2012247637067_tbxAfterTimes_onTextChange_seq0(eventobject, changedtext) {
    if (frmSchedule.tbxAfterTimes.text.length > 2) {
        frmSchedule.tbxAfterTimes.text = frmSchedule.tbxAfterTimes.text.substring(0, 2);
    }
};

function p2kwiet2012247637067_btnCancel_onClick_seq0(eventobject) {
    returnFromScheduleForm.call(this);
};

function p2kwiet2012247637067_btnAgree_onClick_seq0(eventobject) {
    scheduleBillPaymentSaveValidations.call(this);
};

function p2kwiet2012247637118_frmScheduleBillPayEditFuture_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637118_frmScheduleBillPayEditFuture_preshow_seq0(eventobject, neworientation) {
    frmScheduleBillPayEditFuturePreShow.call(this);
    frmScheduleBillPayEditFuture.calScheduleStartDate.validStartDate = currentDateForcalender();
    frmScheduleBillPayEditFuture.calScheduleEndDate.validStartDate = currentDateForcalender();
    frmScheduleBillPayEditFuture.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    if (flowSpa) frmScheduleBillPayEditFuture.onDeviceBack = onDeviceBck
};

function p2kwiet2012247637118_frmScheduleBillPayEditFuture_postshow_seq0(eventobject, neworientation) {
    commonMBPostShow.call(this);
};

function p2kwiet2012247637118_btnDaily_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
    frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
    frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
    repeatScheduleFrequencyMB.call(this, eventobject);
};

function p2kwiet2012247637118_btnWeekly_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
    frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
    frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
    repeatScheduleFrequencyMB.call(this, eventobject);
};

function p2kwiet2012247637118_btnMonthly_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
    frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
    frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
    repeatScheduleFrequencyMB.call(this, eventobject);
};

function p2kwiet2012247637118_btnYearly_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
    frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
    frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
    repeatScheduleFrequencyMB.call(this, eventobject);
};

function p2kwiet2012247637118_btnNever_onClick_seq0(eventobject) {
    endingScheduleFrequencyMB.call(this, eventobject);
};

function p2kwiet2012247637118_btnAfter_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.hboxtimes.setVisibility(true);
    frmScheduleBillPayEditFuture.lblNumberOfTimes.setVisibility(true);
    frmScheduleBillPayEditFuture.linetimes.setVisibility(true);
    frmScheduleBillPayEditFuture.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    endingScheduleFrequencyMB.call(this, eventobject);
};

function p2kwiet2012247637118_btnOnDate_onClick_seq0(eventobject) {
    frmScheduleBillPayEditFuture.hboxUntil.setVisibility(true);
    frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
    frmScheduleBillPayEditFuture.hboxtimes.setVisibility(false);
    frmScheduleBillPayEditFuture.lblNumberOfTimes.setVisibility(false);
    frmScheduleBillPayEditFuture.linetimes.setVisibility(false);
    endingScheduleFrequencyMB.call(this, eventobject);
};

function p2kwiet2012247637118_btnSaveSchedule_onClick_seq0(eventobject) {
    onClickSaveSchedulefrm.call(this);
};

function p2kwiet2012247637118_btnCancel_onClick_seq0(eventobject) {
    frmBillPaymentEdit.show();
    frmBillPaymentEdit.lblStartDate.text = gblTempTPStartOnDateMB
    frmBillPaymentEdit.lblEndOnDate.text = gblTempTPEndOnDateMB
    frmBillPaymentEdit.lblExecutetimes.text = gblTempTPRepeatAsMB
    repeatAsMB = gblTemprepeatAsTPMB
    endFreqSaveMB = gblTempendFreqSaveTPMB;
    if (repeatAsMB == "" || endFreqSaveMB == "") {
        if (gblEndingFreqOnLoadMB == "Never") {
            OnClickRepeatAsMB = "";
            OnClickEndFreqMB = "";
        } else if (gblOnLoadRepeatAsMB == "Once") {
            OnClickRepeatAsMB = repeatAsIB;
            OnClickEndFreqMB = endFreqSave;
        } else {
            OnClickRepeatAsMB = "";
            OnClickEndFreqMB = "";
        }
    } else {
        OnClickRepeatAsMB = repeatAsMB;
        OnClickEndFreqMB = endFreqSaveMB;
    }
};

function p2kwiet2012247637118_btnSave_onClick_seq0(eventobject) {
    onClickSaveSchedulefrm.call(this);
};

function p2kwiet2012247637149_frmSelectBiller_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637149_frmSelectBiller_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637149_frmSelectBiller_preshow_seq0(eventobject, neworientation) {
    frmSelectBillerMenuPreshow.call(this);
};

function p2kwiet2012247637149_frmSelectBiller_postshow_seq0(eventobject, neworientation) {
    frmSelectBillerMenuPostshow.call(this);
};

function p2kwiet2012247637149_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637149_btnHdrMenu_onClick_seq0(eventobject) {
    if (GblBillTopFlag) {
        frmSelectBillerLanding.show();
    } else {
        frmTopUp.show();
    }
};

function p2kwiet2012247637149_tbxSearch_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    onClickSelectBillerSearch.call(this);
};

function p2kwiet2012247637149_tbxSearch_Android_onBeginEditing_seq0(eventobject, changedtext) {
    onClickSelectBillerSearch.call(this);
};

function p2kwiet2012247637149_tbxSearch_onTextChange_seq0(eventobject, changedtext) {
    displaySelectBillerCancelBtn.call(this);
    searchMyBillsAndSuggestedBillers.call(this);
};

function p2kwiet2012247637149_vbxCancel_onClick_seq0(eventobject) {
    onClickSelectBillerSearchCancelBtn.call(this);
};

function p2kwiet2012247637149_hbox38848739538560_onClick_seq0(eventobject) {
    onClickSelectbillerCategory.call(this);
};

function p2kwiet2012247637149_segMyBills_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    if (GblBillTopFlag) {
        myBillersSegmentRowSelct.call(this);
    } else {
        myTopUpSegmentRowSelct.call(this);
    }
};

function p2kwiet2012247637180_frmSelectBillerLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637180_frmSelectBillerLanding_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637180_frmSelectBillerLanding_preshow_seq0(eventobject, neworientation) {
    frmSelectBillerLandingMenuPreshow.call(this);
};

function p2kwiet2012247637180_frmSelectBillerLanding_postshow_seq0(eventobject, neworientation) {
    frmSelectBillerLandingMenuPostshow.call(this);
};

function p2kwiet2012247637180_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637180_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247637180_segMyBills_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    myBillsRowSelectedOnPreLanding.call(this);
};

function p2kwiet2012247637180_vbxSelectBillers_onClick_seq0(eventobject) {
    gotoSelectBillerForm.call(this);
};

function p2kwiet2012247637180_vbxScanBarCode_onClick_seq0(eventobject) {
    scanBarcode.call(this);
};

function p2kwiet2012247637191_button763357907444132_onClick_seq0(eventobject) {
    gblCCDBCardFlow = "CREDIT_CARD_BLOCK";
    success = true;
    navigateforCard.call(this);
};

function p2kwiet2012247637191_button763357907444134_onClick_seq0(eventobject) {
    gblCCDBCardFlow = "DEBIT_CARD_REISSUE";
    success = true;
    navigateforCard.call(this);
};

function p2kwiet2012247637191_button96913044044962_onClick_seq0(eventobject) {
    gblCCDBCardFlow = "DEBIT_CARD_BLOCK";
    success = true;
    navigateforCard.call(this);
};

function p2kwiet2012247637191_button96913044063340_onClick_seq0(eventobject) {
    gblCCDBCardFlow = "CREDIT_CARD_BLOCK";
    success = false;
    navigateforCard.call(this);
};

function p2kwiet2012247637191_button96913044063342_onClick_seq0(eventobject) {
    gblCCDBCardFlow = "DEBIT_CARD_REISSUE";
    success = false;
    navigateforCard.call(this);
};

function p2kwiet2012247637191_button96913044063344_onClick_seq0(eventobject) {
    gblCCDBCardFlow = "DEBIT_CARD_BLOCK";
    success = false;
    navigateforCard.call(this);
};

function p2kwiet2012247637230_frmTimeDepositAccoutDetails_preshow_seq0(eventobject, neworientation) {
    frmTimeDepositAccoutDetailsMenuPreshow.call(this);
};

function p2kwiet2012247637230_frmTimeDepositAccoutDetails_postshow_seq0(eventobject, neworientation) {
    frmTimeDepositAccoutDetailsMenuPostshow.call(this);
};

function p2kwiet2012247637335_frmTopUp_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637335_frmTopUp_preshow_seq0(eventobject, neworientation) {
    frmTopUpMenuPreshow.call(this);
};

function p2kwiet2012247637335_frmTopUp_postshow_seq0(eventobject, neworientation) {
    frmTopUpMenuPostshow.call(this);
};

function p2kwiet2012247637335_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637335_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247637335_segSlider_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    TopUpcvindex = frmTopUp.segSlider.selectedIndex;
};

function p2kwiet2012247637335_segSlider_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {
    var modScroll = new getTopUpIndex();
    modScroll.paginationSwitch(sectionIndex, rowIndex);
};

function p2kwiet2012247637335_lblRef1Value_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    getTopUpBillerNickNameMB.call(this);
};

function p2kwiet2012247637335_lblRef1Value_Android_onEndEditing_seq0(eventobject, changedtext) {
    getTopUpBillerNickNameMB.call(this);
};

function p2kwiet2012247637335_lblRef1Value_onTextChange_seq0(eventobject, changedtext) {
    frmTopUp.txtNickName.text = "";
};

function p2kwiet2012247637335_tbxExcludingBillerOne_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneEditingAmountTopUp.call(this);
};

function p2kwiet2012247637335_tbxExcludingBillerOne_onDone_seq0(eventobject, changedtext) {
    onDoneEditingAmountTopUp.call(this);
};

function p2kwiet2012247637335_tbxExcludingBillerOne_onTextChange_seq0(eventobject, changedtext) {
    formatTopUpAmountOnTextChange.call(this);
};

function p2kwiet2012247637335_btnFull2_onClick_seq0(eventobject) {
    fullPress.call(this);
    fullSpecButtonColorSet.call(this, eventobject);
};

function p2kwiet2012247637335_btnSpecified2_onClick_seq0(eventobject) {
    fullSpecButtonColorSet.call(this, eventobject);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    frmBillPayment.tbxAmount.placeholder = commaFormatted(fullAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.text = commaFormatted(fullAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
    gblFullPayment = false;
};

function p2kwiet2012247637335_tbxExcludingBillerOneOld_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    if (frmTopUp.tbxExcludingBillerOne.text == "") {
        frmTopUp.tbxExcludingBillerOne.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmTopUp.tbxExcludingBillerOne.text = commaFormatted(parseFloat(removeCommos(frmTopUp.tbxExcludingBillerOne.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
};

function p2kwiet2012247637335_tbxExcludingBillerOneOld_Android_onEndEditing_seq0(eventobject, changedtext) {
    if (frmTopUp.tbxExcludingBillerOne.text == "") {
        frmTopUp.tbxExcludingBillerOne.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmTopUp.tbxExcludingBillerOne.text = commaFormatted(parseFloat(removeCommos(frmTopUp.tbxExcludingBillerOne.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
};

function p2kwiet2012247637335_hbxTopUpamntComboBox_onClick_seq0(eventobject) {
    if (frmTopUp.lblTopUpName.text == null || frmTopUp.lblTopUpName.text == "" || frmTopUp.lblTopUpName.text == " ") {
        alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
    } else {
        popUpMyBillers.lblFlag.text = "frmTopUp";
        populateAmountForTopUp();
        //popUpMyBillers.show();
        //popBankList.show();
    }
};

function p2kwiet2012247637335_hbxPayBillOn_onClick_seq0(eventobject) {
    if ((frmTopUp.lblTopUpName.text == null || frmTopUp.lblTopUpName.text == "" || frmTopUp.lblTopUpName.text == " ")) {
        alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
    } else {
        gblScheduleEndBPTmp = gblScheduleEndBP;
        gblScheduleRepeatBPTmp = gblScheduleRepeatBP;
        frmSchedule.show();
    }
};

function p2kwiet2012247637335_txtNickName_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneEditingAmountTopUp.call(this);
};

function p2kwiet2012247637335_txtNickName_onDone_seq0(eventobject, changedtext) {
    onDoneEditingAmountTopUp.call(this);
};

function p2kwiet2012247637335_txtNickName_onTextChange_seq0(eventobject, changedtext) {
    formatTopUpAmountOnTextChange.call(this);
};

function p2kwiet2012247637335_tbxMyNoteValue_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637335_tbxMyNoteValue_Android_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637335_tbxMyNoteValue_onTextChange_seq0(eventobject, changedtext) {
    notAllowEmojiChars.call(this, eventobject);
};

function p2kwiet2012247637335_btnback_onClick_seq0(eventobject) {
    gotoTopUpSelectBillerForm.call(this);
};

function p2kwiet2012247637335_btnNext_onClick_seq0(eventobject) {
    validateChannelMBTopUp.call(this);
    /* 
callValidationTopupMB.call(this);

 */
    getAccountNameOfUser.call(this, frmTopUp["segSlider"]["selectedItems"][0].lblActNoval);
};

function p2kwiet2012247637348_frmTopUpAmount_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637348_frmTopUpAmount_preshow_seq0(eventobject, neworientation) {
    frmTopUpAmountMenuPreshow.call(this);
};

function p2kwiet2012247637348_frmTopUpAmount_postshow_seq0(eventobject, neworientation) {
    frmTopUpAmountMenuPostshow.call(this);
};

function p2kwiet2012247637348_btnClose_onClick_seq0(eventobject) {
    frmTopUp.show();
};

function p2kwiet2012247637348_segPop_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    amountTopUpSelect.call(this);
};

function p2kwiet2012247637360_frmTouchIdIntermediateLogin_preshow_seq0(eventobject, neworientation) {
    frmTouchIdIntermediateLoginMenuPreshow.call(this);
};

function p2kwiet2012247637360_frmTouchIdIntermediateLogin_postshow_seq0(eventobject, neworientation) {
    frmTouchIdIntermediateLoginMenuPostshow.call(this);
};

function p2kwiet2012247637360_btnNotNow_onClick_seq0(eventobject) {
    onCancelOfTouchFrm.call(this);
};

function p2kwiet2012247637360_btnContinue_onClick_seq0(eventobject) {
    showTouchIdSettingForm.call(this);
};

function p2kwiet2012247637385_frmTouchIdSettings_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637385_frmTouchIdSettings_preshow_seq0(eventobject, neworientation) {
    frmTouchIdSettingsMenuPreshow.call(this);
};

function p2kwiet2012247637385_frmTouchIdSettings_postshow_seq0(eventobject, neworientation) {
    frmTouchIdSettingsMenuPostshow.call(this);
};

function p2kwiet2012247637385_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637385_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247637385_hbxTouchLabel_onClick_seq0(eventobject) {
    getCampaignMBPreResult.call(this);
};

function p2kwiet2012247637385_chkBoxGrp_onslide_seq0(eventobject) {
    onClickToggle.call(this);
};

function p2kwiet2012247637385_btnTouchID_onClick_seq0(eventobject) {
    /* 
onCancelOfTouchFrm.call(this);

 */
    TMBUtil.DestroyForm(frmAccountSummaryLanding);
    showAccuntSummaryScreen();
    gblActivationFlow = false;
};

function p2kwiet2012247637405_frmTranfersToRecipents_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637405_frmTranfersToRecipents_preshow_seq0(eventobject, neworientation) {
    frmTranfersToRecipentsMenuPreshow.call(this);
};

function p2kwiet2012247637405_frmTranfersToRecipents_postshow_seq0(eventobject, neworientation) {
    frmTranfersToRecipentsMenuPostshow.call(this);
};

function p2kwiet2012247637405_btnHdrMenu_onClick_seq0(eventobject) {
    ehFrmTranfersToRecipents_button156335099532181_onClick.call(this);
};

function p2kwiet2012247637405_btnRight_onClick_seq0(eventobject) {
    eh_frmSelectBiller_btnRight_onClick.call(this);
};

function p2kwiet2012247637405_txbXferSearch_onTextChange_seq0(eventobject, changedtext) {
    searchTransferToRecipient.call(this, eventobject, "1");
};

function p2kwiet2012247637405_segTransferToRecipients_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    /* 
editTransferToRecipientsData.call(this);

 */
    getToRecipientsAccounts.call(this);
};

function p2kwiet2012247637527_frmTransferConfirm_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637527_frmTransferConfirm_SPA_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637527_frmTransferConfirm_preshow_seq0(eventobject, neworientation) {
    frmTransferConfirmMenuPreshow.call(this);
};

function p2kwiet2012247637527_frmTransferConfirm_postshow_seq0(eventobject, neworientation) {
    frmTransferConfirmMenuPostshow.call(this);
};

function p2kwiet2012247637527_frmTransferConfirm_onhide_seq0(eventobject, neworientation) {
    /* 
removeMenu.call(this);

 */
    ehFrmTransfersAck_frmTransfersAck_onhide.call(this);
};

function p2kwiet2012247637527_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247637527_btnEditMenu_onClick_seq0(eventobject) {
    onEditTransConfirm.call(this);
};

function p2kwiet2012247637527_hbxBalAfterTransfer_onClick_seq0(eventobject) {
    hideUnhide.call(this);
};

function p2kwiet2012247637527_hbxTransCnfmBalBefVal_onClick_seq0(eventobject) {
    hideUnhide.call(this);
};

function p2kwiet2012247637527_hbxAmountFeeSplit_onClick_seq0(eventobject) {
    showAmountFeeSplit.call(this);
};

function p2kwiet2012247637527_btnTransCnfrmCancel_onClick_seq0(eventobject) {
    showTransferLandingHome.call(this);
};

function p2kwiet2012247637527_btnTransCnfrmConfirm_onClick_seq0(eventobject) {
    ehFrmTransferConfirm_btnTransCnfrmConfirmSpa_onClick.call(this);
};

function p2kwiet2012247637673_frmTransferLanding_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637673_frmTransferLanding_Android_onDeviceMenu_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637673_frmTransferLanding_preshow_seq0(eventobject, neworientation) {
    frmTransferLandingMenuPreshow.call(this);
};

function p2kwiet2012247637673_frmTransferLanding_postshow_seq0(eventobject, neworientation) {
    frmTransferLandingMenuPostshow.call(this);
};

function p2kwiet2012247637673_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637673_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247637673_segTransFrm_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    /* 
onClickCoverFlowTransfersMB.call(this);

 */
    ehFrmTransferLanding_segTransFrm_onRowClick.call(this);
};

function p2kwiet2012247637673_segTransFrm_onswipe_seq0(seguiWidget, sectionIndex, rowIndex) {};

function p2kwiet2012247637673_hboxTD_onClick_seq0(eventobject) {
    selectTDDetails.call(this);
};

function p2kwiet2012247637673_txtCitizenID_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditCitizenID.call(this);
};

function p2kwiet2012247637673_txtCitizenID_Android_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditCitizenID.call(this);
};

function p2kwiet2012247637673_txtCitizenID_onDone_seq0(eventobject, changedtext) {
    onDoneValidateCITransfer.call(this, frmTransferLanding.txtCitizenID.text);
};

function p2kwiet2012247637673_txtCitizenID_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeToCitizenIDP2P.call(this, frmTransferLanding.txtCitizenID.text);
};

function p2kwiet2012247637673_txtOnUsMobileNo_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    clearNotifyRecipientfields.call(this);
};

function p2kwiet2012247637673_txtOnUsMobileNo_Android_onBeginEditing_seq0(eventobject, changedtext) {
    clearNotifyRecipientfields.call(this);
};

function p2kwiet2012247637673_txtOnUsMobileNo_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeToMobileNoP2P.call(this, frmTransferLanding.txtOnUsMobileNo.text);
};

function p2kwiet2012247637673_vbxContactList_onClick_seq0(eventobject) {
    populateContactsInToList.call(this);
};

function p2kwiet2012247637673_hbxPleaseSelectBank_onClick_seq0(eventobject) {
    populateMBMyRecepientBankList.call(this);
};

function p2kwiet2012247637673_hbxSelectBank_onClick_seq0(eventobject) {
    populateMBMyRecepientBankList.call(this);
};

function p2kwiet2012247637673_tbxAccountNumber_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditAccountNumber.call(this);
};

function p2kwiet2012247637673_tbxAccountNumber_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneToVerifyAccountNumber.call(this);
};

function p2kwiet2012247637673_tbxAccountNumber_Android_onBeginEditing_seq0(eventobject, changedtext) {
    onBeginEditAccountNumber.call(this);
};

function p2kwiet2012247637673_tbxAccountNumber_onDone_seq0(eventobject, changedtext) {
    onDoneToVerifyAccountNumber.call(this);
};

function p2kwiet2012247637673_tbxAccountNumber_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeToAccountNumber.call(this);
    retrieveRecipientList.call(this);
};

function p2kwiet2012247637673_hbxSelRecipientNumberAndName_onClick_seq0(eventobject) {
    onclickRecipientVBox.call(this);
};

function p2kwiet2012247637673_vbxSelRecipient_onClick_seq0(eventobject) {
    ehFrmTransferLanding_btnTranLandToSel_onClick.call(this);
};

function p2kwiet2012247637673_vbxTransferAmt_onClick_seq0(eventobject) {
    onClickVbxEnterAmountFocus.call(this);
};

function p2kwiet2012247637673_txtTranLandAmt_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    transAmountOnClickMB.call(this);
};

function p2kwiet2012247637673_txtTranLandAmt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    transAmountOnDone.call(this, true, true);
};

function p2kwiet2012247637673_txtTranLandAmt_Android_onBeginEditing_seq0(eventobject, changedtext) {
    transAmountOnClickMB.call(this);
};

function p2kwiet2012247637673_txtTranLandAmt_Android_onEndEditing_seq0(eventobject, changedtext) {
    transAmountOnDone.call(this, true, true);
};

function p2kwiet2012247637673_txtTranLandAmt_onDone_seq0(eventobject, changedtext) {
    transAmountOnDone.call(this, true, true);
};

function p2kwiet2012247637673_txtTranLandAmt_onTextChange_seq0(eventobject, changedtext) {
    formatAmountOnTextChange.call(this);
};

function p2kwiet2012247637673_btnTransLndORFT_onClick_seq0(eventobject) {
    /* 
if  (gblTrasORFT==0){
frmTransferLanding.btnTransLndORFT.skin=btnfeeLeftFocus
frmTransferLanding.btnTransLndSmart.skin=btnFeeRight
gblTrasORFT=gblTrasORFT+1;
gblTransSMART=0;
}
else{
frmTransferLanding.btnTransLndORFT.skin=btnFeeLeft
gblTrasORFT=0;
gblTransSMART=0;
}
frmTransferLanding.enabledForIdleTimeout = true

 */
    ehFrmTransferLanding_btnTransLndORFT_onClick.call(this);
};

function p2kwiet2012247637673_btnTransLndSmart_onClick_seq0(eventobject) {
    ehFrmTransferLanding_btnTransLndSmart_onClick.call(this);
};

function p2kwiet2012247637673_hbxTransCalendar_onClick_seq0(eventobject) {
    ehFrmTransferLanding_btnSchedTo_onClick.call(this);
};

function p2kwiet2012247637673_hbxTransMyNoteTxt_onClick_seq0(eventobject) {
    ehFrmTransferLanding_hbxTranLandMyNote_onClick.call(this);
};

function p2kwiet2012247637673_txtTranLandMyNote_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637673_txtTranLandMyNote_Android_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637673_txtTranLandMyNote_onTextChange_seq0(eventobject, changedtext) {
    notAllowEmojiChars.call(this, eventobject);
};

function p2kwiet2012247637673_vbxOnUsMobileSmS_onClick_seq0(eventobject) {
    ehFrmTransferLandingBtnTranLandSmsOnClick.call(this);
};

function p2kwiet2012247637673_vbxTranLandSms_onClick_seq0(eventobject) {
    ehFrmTransferLandingBtnTranLandSmsOnClick.call(this);
};

function p2kwiet2012247637673_vbxTranLandEmail_onClick_seq0(eventobject) {
    ehFrmTransferLandingBtnTranLandEmailOnClick.call(this);
};

function p2kwiet2012247637673_txtTransLndSmsNEmail_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    ehFrmTransferLandingHbxRecNoteEmailOnClick.call(this);
};

function p2kwiet2012247637673_txtTransLndSmsNEmail_Android_onEndEditing_seq0(eventobject, changedtext) {
    ehFrmTransferLandingHbxRecNoteEmailOnClick.call(this);
};

function p2kwiet2012247637673_txtTransLndSmsNEmail_onDone_seq0(eventobject, changedtext) {
    ehFrmTransferLandingHbxRecNoteEmailOnClick.call(this);
};

function p2kwiet2012247637673_txtTransLndSms_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    ehFrmTransferLandingHbxRecNoteSmsOnClick.call(this);
};

function p2kwiet2012247637673_txtTransLndSms_Android_onEndEditing_seq0(eventobject, changedtext) {
    ehFrmTransferLandingHbxRecNoteSmsOnClick.call(this);
};

function p2kwiet2012247637673_txtTransLndSms_onDone_seq0(eventobject, changedtext) {
    ehFrmTransferLandingHbxRecNoteSmsOnClick.call(this);
};

function p2kwiet2012247637673_txtTransLndSms_onTextChange_seq0(eventobject, changedtext) {
    onTextChangeToMobileNoSmS(frmTransferLanding.txtTransLndSms.text);
};

function p2kwiet2012247637673_hbxRecNoteEmail_onClick_seq0(eventobject) {
    ehFrmTransferLandingHbxRecNoteEmailOnClick.call(this);
};

function p2kwiet2012247637673_textRecNoteEmail_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637673_textRecNoteEmail_Android_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637673_textRecNoteEmail_onTextChange_seq0(eventobject, changedtext) {
    notAllowEmojiChars.call(this, eventobject);
};

function p2kwiet2012247637673_hbxTranLandRecNote_onClick_seq0(eventobject) {
    ehFrmTransferLandingHbxRecNoteSmsOnClick.call(this);
};

function p2kwiet2012247637673_txtTranLandRecNote_iPhone_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637673_txtTranLandRecNote_Android_onBeginEditing_seq0(eventobject, changedtext) {
    assignTextNoEmoji.call(this, eventobject);
};

function p2kwiet2012247637673_txtTranLandRecNote_onTextChange_seq0(eventobject, changedtext) {
    notAllowEmojiChars.call(this, eventobject);
    onTextChangetxtTranLandRecNote.call(this);
};

function p2kwiet2012247637673_btnTransCnfrmCancel_onClick_seq0(eventobject) {
    cleanUpGlobalVariableTransfersMB.call(this);
    onClickCancelBtnTransferLandingMB.call(this);
};

function p2kwiet2012247637673_btnTranLandNext_onClick_seq0(eventobject) {
    ehFrmTransferLanding_btnTranLandNext_onClick.call(this);
};

function p2kwiet2012247637818_frmTransfersAck_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637818_frmTransfersAck_SPA_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637818_frmTransfersAck_preshow_seq0(eventobject, neworientation) {
    frmTransfersAckMenuPreshow.call(this);
};

function p2kwiet2012247637818_frmTransfersAck_postshow_seq0(eventobject, neworientation) {
    frmTransfersAckMenuPostshow.call(this);
};

function p2kwiet2012247637818_frmTransfersAck_onhide_seq0(eventobject, neworientation) {
    /* 
removeMenu.call(this);

 */
    ehFrmTransfersAck_frmTransfersAck_onhide.call(this);
};

function p2kwiet2012247637818_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247637818_btnRight_onClick_seq0(eventobject) {
    shareButtonOnClick.call(this);
};

function p2kwiet2012247637818_vbxImage_onClick_seq0(eventobject) {};

function p2kwiet2012247637818_vbxMessenger_onClick_seq0(eventobject) {
    shareIntentCall.call(this, "facebook", "Transfer");
};

function p2kwiet2012247637818_vbxLine_onClick_seq0(eventobject) {
    shareIntentCall.call(this, "line", "Transfer");
};

function p2kwiet2012247637818_vbxOthers_onClick_seq0(eventobject) {};

function p2kwiet2012247637818_hbox47792425956439_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637818_hbxBalAfterTransfer_onClick_seq0(eventobject) {
    hideUnhide.call(this);
};

function p2kwiet2012247637818_hbxTransCnfmBalBefVal_onClick_seq0(eventobject) {
    hideUnhide.call(this);
};

function p2kwiet2012247637818_hbxImageAddRecipient_onClick_seq0(eventobject) {
    onClickAddToMyRecipient.call(this);
};

function p2kwiet2012247637818_hbxAmountFeeSplit_onClick_seq0(eventobject) {
    showAmountFeeSplitTransferComp.call(this);
};

function p2kwiet2012247637818_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247637818_btnTransNPbAckReturn_onClick_seq0(eventobject) {
    ehFrmTransfersAck_btnTransNPbAckReturn_onClick.call(this);
};

function p2kwiet2012247637818_btnTransNPbAckMakeAnthr_onClick_seq0(eventobject) {
    makeAnotherTransfer.call(this);
};

function p2kwiet2012247637941_frmTransfersAckCalendar_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637941_frmTransfersAckCalendar_SPA_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637941_frmTransfersAckCalendar_preshow_seq0(eventobject, neworientation) {
    frmTransfersAckCalendarMenuPreshow.call(this);
};

function p2kwiet2012247637941_frmTransfersAckCalendar_postshow_seq0(eventobject, neworientation) {
    frmTransfersAckCalendarMenuPostshow.call(this);
};

function p2kwiet2012247637941_frmTransfersAckCalendar_onhide_seq0(eventobject, neworientation) {
    /* 
removeMenu.call(this);

 */
    ehFrmTransfersAck_frmTransfersAck_onhide.call(this);
};

function p2kwiet2012247637941_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247637941_btnRight_onClick_seq0(eventobject) {
    shareButtonCalendarOnClick.call(this);
};

function p2kwiet2012247637941_hbox47792425956439_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247637941_vbxImage_onClick_seq0(eventobject) {};

function p2kwiet2012247637941_vbxMessenger_onClick_seq0(eventobject) {
    shareIntentCall.call(this, "facebook", "Transfer");
};

function p2kwiet2012247637941_vbxLine_onClick_seq0(eventobject) {
    shareIntentCall.call(this, "line", "Transfer");
};

function p2kwiet2012247637941_vbxOthers_onClick_seq0(eventobject) {};

function p2kwiet2012247637941_hbxBalAfterTransfer_onClick_seq0(eventobject) {
    showHideTransferAfterBalance.call(this);
};

function p2kwiet2012247637941_hbxTransCnfmBalBefVal_onClick_seq0(eventobject) {
    hideUnhide.call(this);
};

function p2kwiet2012247637941_hbxMoreHide_onClick_seq0(eventobject) {
    showHideMegaTransferDetails.call(this);
};

function p2kwiet2012247637941_hbxAdv_onClick_seq0(eventobject) {
    getCampaignResult.call(this);
};

function p2kwiet2012247637941_btnTransNPbAckReturn_onClick_seq0(eventobject) {
    MBMyActivitiesShowCalendar.call(this);
};

function p2kwiet2012247637941_btnTransNPbAckMakeAnthr_onClick_seq0(eventobject) {
    transferAgainFromCalenderTransaction.call(this);
};

function p2kwiet2012247637975_frmTransferToRecipentsMobile_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247637975_frmTransferToRecipentsMobile_preshow_seq0(eventobject, neworientation) {
    frmTransferToRecipentsMobileMenuPreshow.call(this);
};

function p2kwiet2012247637975_frmTransferToRecipentsMobile_postshow_seq0(eventobject, neworientation) {
    frmTransferToRecipentsMobileMenuPostshow.call(this);
};

function p2kwiet2012247637975_btnHdrMenu_onClick_seq0(eventobject) {
    ehFrmTranfersToRecipents_button156335099532181_onClick.call(this);
};

function p2kwiet2012247637975_btnRight_onClick_seq0(eventobject) {
    refreshContactsFromServer.call(this);
};

function p2kwiet2012247637975_txbXferSearch_onTextChange_seq0(eventobject, changedtext) {
    searchP2PContacts.call(this);
};

function p2kwiet2012247637975_vbxContact_onClick_seq0(eventobject) {
    onClickDeviceContacts.call(this);
};

function p2kwiet2012247637975_vbxRecipient_onClick_seq0(eventobject) {
    onClickMibRecipients.call(this);
};

function p2kwiet2012247637975_segTransferToRecipients_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onRowClickMibRecipientSegment.call(this);
};

function p2kwiet2012247637975_button967430408331992_onClick_seq0(eventobject) {
    refreshContactsFromServer.call(this);
};

function p2kwiet2012247637975_btnTranLandNext_onClick_seq0(eventobject) {
    refreshContactsFromServer.call(this);
};

function p2kwiet2012247638017_frmViewTopUpBiller_Android_onDeviceBack_seq0(eventobject, neworientation) {
    disableBackButton.call(this);
};

function p2kwiet2012247638017_frmViewTopUpBiller_preshow_seq0(eventobject, neworientation) {
    frmViewTopUpBillerMenuPreshow.call(this);
};

function p2kwiet2012247638017_frmViewTopUpBiller_postshow_seq0(eventobject, neworientation) {
    frmViewTopUpBillerMenuPostshow.call(this);
};

function p2kwiet2012247638017_frmViewTopUpBiller_init_seq0(eventobject, neworientation) {
    frmViewTopUpBiller.imgComplete.isVisible = false;
};

function p2kwiet2012247638017_vbox4751247744173_onClick_seq0(eventobject) {
    onClickForInnerBoxes.call(this);
};

function p2kwiet2012247638017_btnHdrMenu_onClick_seq0(eventobject) {
    handleMenuBtn.call(this);
};

function p2kwiet2012247638017_btnDlt_onClick_seq0(eventobject) {
    if (checkMBUserStatus()) {
        deleteOnTopupConfiramtion(deleteViewTopUpMB)
    }
};

function p2kwiet2012247638017_btnEdit_onClick_seq0(eventobject) {
    if (checkMBUserStatus()) {
        frmMyTopUpEditScreens.txtEditName.text = frmViewTopUpBiller.lblCnfrmNickName.text;
        frmMyTopUpEditScreens.show();
        //frmViewTopUpBiller.destroy();
    }
};

function p2kwiet2012247638017_lnkConfirmBB_onClick_seq0(eventobject) {
    if (gblCustomerBBMBInqRs.length == 0) {
        /* 
updateBeepAndBillList.call(this);

 */
    }
    onClickBillersApplyBBLink.call(this);
    /* 
showAccountListBBMB.call(this);

 */
};

function p2kwiet2012247638017_btnBack_onClick_seq0(eventobject) {
    if (isMenuShown == false) {
        viewTopupBack.call(this);
    } else {
        frmViewTopUpBiller.scrollboxMain.scrollToEnd();
        isMenuShown = false;
    }
};

function p2kwiet2012247649810_btnPopUpTractCancel_onClick_seq0(eventobject) {
    /* 

    */
    /* 
onClickOTPRequest.call(this,null);

 */
    ehFAccntTransPwd_btnPopUpTractCancel_onClick.call(this);
};

function p2kwiet2012247649810_btnPopupTractConf_onClick_seq0(eventobject) {
    /* 
OTPValdatn.call(this,popupTractPwd.tbxPopupTractPwdtxt.text);

 */
    /* 
popUpLogout.dismiss();

 */
    ehAccntTransPwd_btnPopupTractConf_onClick.call(this);
};

function p2kwiet2012247649815_btnPopUpTermination_onClick_seq0(eventobject) {
    BillerNotAvailable.dismiss();
};

function p2kwiet2012247649843_btnAddBillCheckBox_onClick_seq0(eventobject) {
    /* 
addToMyBillsButtonClick.call(this);

 */
};

function p2kwiet2012247649843_btnPopUpTractCancel_onClick_seq0(eventobject) {
    BillPayTopUpConf.destroy();
    clearBillerDetails.call(this);
};

function p2kwiet2012247649843_btnPopupTractConf_onClick_seq0(eventobject) {
    populateFromScanBiller.call(this);
};

function p2kwiet2012247649850_btnPopDeleteCancel_onClick_seq0(eventobject) {
    DeletePop.dismiss();
};

function p2kwiet2012247649850_btnPopDeleteYEs_onClick_seq0(eventobject) {
    DeletePop.dismiss();
    //MyAccnt2.destroy();
    frmMyAccountView.show();
};

function p2kwiet2012247649856_btnClose_onClick_seq0(eventobject) {
    onClickFATCAInfoClose.call(this);
};

function p2kwiet2012247649862_btnNext_onClick_seq0(eventobject) {
    FATCAHelpPopup.dismiss();
};

function p2kwiet2012247649868_button474999996106_onClick_seq0(eventobject) {
    //helpMessage.onHide = true;
    helpMessage.dismiss();
    popHelpMessageTrans.dismiss();
    popupActivationHelp.dismiss();
};

function p2kwiet2012247649880_btnPopUpTractCancel_onClick_seq0(eventobject) {
    frmMyAccntConfirmationAddAccount.show();
    popAccntConfirmation.dismiss();
};

function p2kwiet2012247649880_btnPopupTractConf_onClick_seq0(eventobject) {
    onpopconfirmaccnt.call(this);
    popAccntConfirmation.dismiss();
    hbxfooter.setVisibility(false);
    frmMyAccntConfirmationAddAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
    frmMyAccntConfirmationAddAccount.btnAddHeader.setVisibility(false);
    hideDelete.call(this);
};

function p2kwiet2012247649889_segBanklist_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onRowSelectAddrPopUp.call(this);
};

function p2kwiet2012247649901_btnDontAllow_onClick_seq0(eventobject) {
    popAllowOrNot.dismiss();
    popAllowOrNot.imgIcon.setVisibility(true);
    popAllowOrNot.lblTitle.setVisibility(true);
};

function p2kwiet2012247649910_btnGetDirection_onClick_seq0(eventobject) {
    onClickGetDirectionMap.call(this);
};

function p2kwiet2012247649910_btnDtls_onClick_seq0(eventobject) {
    onClickDetails.call(this);
};

function p2kwiet2012247649917_segBanklist_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    if (frmSelectBiller != null && frmSelectBiller.btnSelectCategory != null) frmSelectBiller.btnSelectCategory.text = popBankList.segBanklist.selectedItems[0].bankCode
    popBankList.dismiss();
    banklist.call(this, popBankList.lblFlag.text);
};

function p2kwiet2012247649928_btnPopTransactionCancel_onClick_seq0(eventobject) {
    /* 
popTransactionPwd.dismiss();

 */
    ehPopBPTransactionPwd_btnPopTransactionCancel_onClick.call(this);
};

function p2kwiet2012247649928_btnPopTransactionConf_onClick_seq0(eventobject) {
    onClickBillPayConfirmPop.call(this);
};

function p2kwiet2012247649941_segBanklist_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onRowSelectContactsAddrPopUp.call(this);
};

function p2kwiet2012247649953_btnPopDeleteCancel_onClick_seq0(eventobject) {
    popDelRecipient.dismiss();
};

function p2kwiet2012247649953_btnPopDeleteYEs_onClick_seq0(eventobject) {
    deletePopupYes.call(this);
};

function p2kwiet2012247649963_btnPopDeleteCancel_onClick_seq0(eventobject) {
    popDelRecipientProfile.dismiss();
};

function p2kwiet2012247649963_btnPopDeleteYEs_onClick_seq0(eventobject) {
    popDelRecipientProfile.dismiss();
    //MyAccnt2.destroy();
    startRcMBDeleteService.call(this);
    /* 
frmMyRecipients.show();
	
 */
};

function p2kwiet2012247649973_btnPopDeleteCancel_onClick_seq0(eventobject) {
    popDelTopUp.dismiss();
};

function p2kwiet2012247649987_btnPopTransactionCancel_onClick_seq0(eventobject) {
    popEditBillPayTransactionPwd.dismiss();
};

function p2kwiet2012247649987_btnPopTransactionConf_onClick_seq0(eventobject) {
    onClickNextEditBPPopUp.call(this);
    popEditBillPayTransactionPwd.dismiss();
};

function p2kwiet2012247649999_btnClose_onClick_seq0(eventobject) {
    popExgRateDisclaimer.dismiss();
};

function p2kwiet2012247650006_button476108006293989_onClick_seq0(eventobject) {
    FeedbackLater = "1";
    popFeedBack.dismiss();
    frmFeedbackComplete.show();
    if (gblDeviceInfo["name"] == "android") {
        kony.application.openURL(kony.i18n.getLocalizedString("googlePlayStore"));
    } else if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad") {
        kony.application.openURL(kony.i18n.getLocalizedString("appleStore"));
    }
};

function p2kwiet2012247650006_button476108006294001_onClick_seq0(eventobject) {
    FeedbackLater = "2";
    popFeedBack.dismiss();
    frmFeedbackComplete.show();
};

function p2kwiet2012247650006_button476108006294007_onClick_seq0(eventobject) {
    FeedbackLater = "3";
    popFeedBack.dismiss();
    frmFeedbackComplete.show();
};

function p2kwiet2012247650014_btnForPassCancel_onClick_seq0(eventobject) {
    popForgotPass.destroy();
};

function p2kwiet2012247650014_btnPopupTractConf_onClick_seq0(eventobject) {
    popForgotPass.dismiss();
    try {
        var number = "1558";
        kony.phone.dial(number);
    } catch (err) {
        alert("error in dial:: " + err);
    }
};

function p2kwiet2012247650020_popGoback_Android_onDeviceBack_seq0(eventobject, neworientation) {
    try {
        popGoback.destroy();
        popGoback.dismiss();
        kony.timer.cancel("btnTimer");
        gblTimerFlg = false;
        onLogoutPopUpConfrm();
    } catch (ex) {
        alert(ex);
    }
};

function p2kwiet2012247650028_segBanklist_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    popupSortTypeFunc.call(this);
};

function p2kwiet2012247650034_segBanklist_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    popupSortOrderFunc.call(this);
};

function p2kwiet2012247650042_segordToBePrcd_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onRowSelectMFMonth.call(this);
};

function p2kwiet2012247650076_txtOTP_onTextChange_seq0(eventobject, changedtext) {
    otpConfirmEnable.call(this);
    dontAllowNonNeumaricSPAMB(popOtpSpa.txtOTP)
};

function p2kwiet2012247650076_txtIncorrectOTP_onTextChange_seq0(eventobject, changedtext) {
    dontAllowNonNeumaricSPAMB(popOtpSpa.txtIncorrectOTP)
};

function p2kwiet2012247650076_btnPopUpTractCancel_onClick_seq0(eventobject) {
    /* 

if(spaChnage == "activation")
{
onClickOTPSpaAactivate()
}
else
{
 //onClickOTPRequestSpa()
 if(spaChnage=="editmyprofile")
 {
  if(caseTrans=="Number")
  {
    generateOtpUserSpaNewMob();
  }
  else
  {
   generateOtpUserSpa()
   popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
   popOtpSpa.btnPopUpTractCancel.focusSkin = btnDisabledGray;
   popOtpSpa.btnPopUpTractCancel.setEnabled(false);
  }
 }
 else
 {
 generateOtpUserSpa()
 popOtpSpa.btnPopUpTractCancel.skin = btnDisabledGray;
 popOtpSpa.btnPopUpTractCancel.focusSkin = btnDisabledGray;
 popOtpSpa.btnPopUpTractCancel.setEnabled(false); 
 } 
}


 */
    popOtpSpaOnClickOfRequestOTPButton.call(this);
};

function p2kwiet2012247650076_txttokenspa_onTextChange_seq0(eventobject, changedtext) {
    dontAllowNonNeumaricSPAMB(popOtpSpa.txttokenspa)
};

function p2kwiet2012247650076_button474165697415035_onClick_seq0(eventobject) {
    gblSwitchToken = true;
    gblTokenSwitchFlag = false;
    onClickOTPRequestSpa.call(this);
};

function p2kwiet2012247650076_button474046533325957_onClick_seq0(eventobject) {
    popOtpSpa.dismiss()
    popOtpSpa.hbxTokenMsg.setVisibility(false);
    popOtpSpa.txttokenspa.text = "";
};

function p2kwiet2012247650076_btnPopupTractConf_onClick_seq0(eventobject) {
    //popOtpSpa.dismiss()
    if (gblTokenSwitchFlag == true) {
        text = popOtpSpa.txttokenspa.text;
        popOtpSpa.lblTokenMsg.text = ""
    } else {
        text = popOtpSpa.txtOTP.text;
    }
    OTPValdatn(text);
};

function p2kwiet2012247650086_button47826097218737_onClick_seq0(eventobject) {
    popProfilePic.dismiss();
};

function p2kwiet2012247650086_btnPopDeleteCancel_onClick_seq0(eventobject) {
    if ((kony.application.getCurrentForm().id == "frmeditMyProfile")) {
        openGalleryinEditProfilePopUp.call(this);
    } else {
        openMediaGalleryinPopUp.call(this);
    }
};

function p2kwiet2012247650086_camera1_onCapture_seq0(eventobject) {
    if ((kony.application.getCurrentForm().id == "frmeditMyProfile")) {
        /* 
invokeCameraFromEditProfilePopup.call(this);

 */
    } else {
        /* 
invokeCameraFromPopup.call(this);

 */
    }
    invokeCameraFromPopup.call(this);
};

function p2kwiet2012247650090_segPromtions_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onSelectPromtionPopUp.call(this);
};

function p2kwiet2012247650098_segBanklist_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    popupBankListSelect.call(this);
};

function p2kwiet2012247650107_btnFtr_onClick_seq0(eventobject) {
    popS2SAmntHelp.dismiss();
};

function p2kwiet2012247650114_segDreamSaveDate_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    frmOpenAccDreamSelAct.btnDreamSavecombo.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
    popSelDate.dismiss();
};

function p2kwiet2012247650125_popTransactionPwd_init_seq0(eventobject, neworientation) {
    /* 
popTransactionPwd.tbxPopTransactionPwd.skin = tbxPopupBlue;
popTransactionPwd.tbxPopTransactionPwd.focusSkin = tbxPopupBlue;
popTransactionPwd.lblPopTranscationMsg.skin = lblPopupLabelTxt;

 */
    ehPopTransactionPwd_popTransactionPwd_init.call(this);
};

function p2kwiet2012247650125_tbxPopTransactionPwd_Android_onBeginEditing_seq0(eventobject, changedtext) {
    /* 
popTransactionPwd.hbxPopTranscationPwd.skin = "hbxPopupTrnsPwdBlue"

 */
    ehPopTransactionPwd_tbxPopTransactionPwd_onBigningEdit.call(this);
};

function p2kwiet2012247650125_tbxPopTransactionPwd_Android_onEndEditing_seq0(eventobject, changedtext) {
    /* 
popTransactionPwd.hbxPopTranscationPwd.skin = "hbxPopupTrnsPwdBlue"

 */
    ehPopTransactionPwd_tbxPopTransactionPwd_onEndingEdit.call(this);
};

function p2kwiet2012247650125_btnPopTransactionCancel_onClick_seq0(eventobject) {
    /* 
popTransactionPwd.dismiss();

 */
    ehPopTransactionPwd_btnPopTransactionCancel_onClick.call(this);
};

function p2kwiet2012247650125_btnPopTransactionConf_onClick_seq0(eventobject) {
    /* 
popTransactionPwd.tbxPopTransactionPwd.skin = tbxPopupBlue;
popTransactionPwd.tbxPopTransactionPwd.focusSkin = tbxPopupBlue;
popTransactionPwd.lblPopTranscationMsg.skin = lblPopupLabelTxt;

 */
    /* 
if(gblS2SHiddenStatus == "true") {
 onClickContinuePopForS2S();
 gblS2SHiddenStatus = false;
} else {
 onClickConfirmPop(); 
}

 */
    ehPopTransactionPwd_btnPopTransactionConf_onClick.call(this);
};

function p2kwiet2012247650131_popTransferConfirmOTPLock_init_seq0(eventobject, neworientation) {
    /* 
if(flowSpa)
{
gblIBFlowStatus="04";
}

if(flowSpa)
 popTransferConfirmOTPLock.label58862488528485.text=kony.i18n.getLocalizedString("keyOTPLckMsg");
else
 popTransferConfirmOTPLock.label58862488528485.text=kony.i18n.getLocalizedString("keyYourTransactionPasswordhasbeen");

if(flowSpa) {
 popTransferConfirmOTPLock.containerHeight = 45;
 }else{
 popTransferConfirmOTPLock.containerHeight = 38;
 }
    popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;

 */
    ehPopTransferConfirmOTPLock_popTransferConfirmOTPLock_init.call(this);
};

function p2kwiet2012247650131_button58862488528490_onClick_seq0(eventobject) {
    /* 
popTransferConfirmOTPLock.dismiss();
popTransferConfirmOTPLock.dismiss();
    if (flowSpa) {
  if((kony.application.getCurrentForm().id =="frmMBActiConfirm")){
  gblIBFlowStatus = "";
  frmSPALogin.show();
  }
  else if((kony.application.getCurrentForm().id =="frmAccountSummaryLanding")){
   //popTransferConfirmOTPLock.dismiss();
   //frmSPABlank.show();      
   //frmAccountSummaryLanding = null;
   //frmAccountSummaryLandingGlobals();
   //gblAccountTable = "";
   //callPartyInquiryService();
   frmAccountSummaryLanding.SegAboutMenu.removeAll();
   frmAccountSummaryLanding.SegMyInbox.removeAll();
   frmAccountSummaryLanding.vbxMyinbox.skin = "vbxMenuInvoxDis";
   frmAccountSummaryLanding.vboxAbtMe.skin = "vbxMenuAbtMeDis";
   frmAccountSummaryLanding.vboxConvService.skin = "vbxConvServiceDis";
 }else{
  frmAccountSummaryLanding.destroy();
        isMenuShown = false
        frmAccountSummaryLanding = null;
        frmAccountSummaryLandingGlobals();
        gblAccountTable = "";
        callCustomerAccountService()
 }
    } else {
        showAccuntSummaryScreen();
    }

 */
    ehPopTransferConfirmOTPLock_button58862488528490_onClick.call(this);
};

function p2kwiet2012247650138_popTransferConfirmOTPLockNew_init_seq0(eventobject, neworientation) {
    /* 
if(flowSpa)
{
gblIBFlowStatus="04";
}

if(flowSpa)
 popTransferConfirmOTPLock.label58862488528485.text=kony.i18n.getLocalizedString("keyOTPLckMsg");
else
 popTransferConfirmOTPLock.label58862488528485.text=kony.i18n.getLocalizedString("keyYourTransactionPasswordhasbeen");

if(flowSpa) {
 popTransferConfirmOTPLock.containerHeight = 45;
 }else{
 popTransferConfirmOTPLock.containerHeight = 38;
 }
    popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;

 */
    ehPopTransferConfirmOTPLock_popTransferConfirmOTPLock_init.call(this);
};

function p2kwiet2012247650138_button58862488528490_onClick_seq0(eventobject) {
    /* 
popTransferConfirmOTPLock.dismiss();
popTransferConfirmOTPLock.dismiss();
    if (flowSpa) {
  if((kony.application.getCurrentForm().id =="frmMBActiConfirm")){
  gblIBFlowStatus = "";
  frmSPALogin.show();
  }
  else if((kony.application.getCurrentForm().id =="frmAccountSummaryLanding")){
   //popTransferConfirmOTPLock.dismiss();
   //frmSPABlank.show();      
   //frmAccountSummaryLanding = null;
   //frmAccountSummaryLandingGlobals();
   //gblAccountTable = "";
   //callPartyInquiryService();
   frmAccountSummaryLanding.SegAboutMenu.removeAll();
   frmAccountSummaryLanding.SegMyInbox.removeAll();
   frmAccountSummaryLanding.vbxMyinbox.skin = "vbxMenuInvoxDis";
   frmAccountSummaryLanding.vboxAbtMe.skin = "vbxMenuAbtMeDis";
   frmAccountSummaryLanding.vboxConvService.skin = "vbxConvServiceDis";
 }else{
  frmAccountSummaryLanding.destroy();
        isMenuShown = false
        frmAccountSummaryLanding = null;
        frmAccountSummaryLandingGlobals();
        gblAccountTable = "";
        callCustomerAccountService()
 }
    } else {
        showAccuntSummaryScreen();
    }

 */
    ehPopTransferConfirmOTPLock_button58862488528490_onClick.call(this);
};

function p2kwiet2012247650145_segTransfersTD_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onSelectTDDetailsPOP.call(this, false);
};

function p2kwiet2012247650156_btnPopupConfCancel_onClick_seq0(eventobject) {
    popupAccesesPinLocked.dismiss();
};

function p2kwiet2012247650156_btnCallTMB_onClick_seq0(eventobject) {
    CallTheNumber.call(this);
};

function p2kwiet2012247650162_richTxtActivationHelp_onClick_seq0(eventobject, linktext, attributes) {
    onClickLinkActMB.call(this, attributes);
};

function p2kwiet2012247650162_button474999996106_onClick_seq0(eventobject) {
    //helpMessage.onHide = true;
    helpMessage.dismiss();
    popHelpMessageTrans.dismiss();
    popupActivationHelp.dismiss();
};

function p2kwiet2012247650194_popupAddToMyBills_init_seq0(eventobject, neworientation) {
    popupAddToMyBills.enabledForIdleTimeout = true
};

function p2kwiet2012247650194_vbxBillerNickName_onClick_seq0(eventobject) {
    onClickVbxBillerNickName.call(this);
};

function p2kwiet2012247650194_txtBillerNickName_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddToMyBillsInputBillerNickName.call(this);
};

function p2kwiet2012247650194_txtBillerNickName_Android_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddToMyBillsInputBillerNickName.call(this);
};

function p2kwiet2012247650194_txtBillerNickName_onTextChange_seq0(eventobject, changedtext) {
    onDoneAddToMyBillsInputBillerNickName.call(this);
};

function p2kwiet2012247650194_btnAddCancel_onClick_seq0(eventobject) {
    popupAddToMyBills.destroy();
};

function p2kwiet2012247650194_btnAddConfirm_onClick_seq0(eventobject) {
    onClickSaveAddToMyBills.call(this);
};

function p2kwiet2012247650226_popupAddToMyRecipient_init_seq0(eventobject, neworientation) {
    popupAddToMyRecipient.enabledForIdleTimeout = true
};

function p2kwiet2012247650226_vbxRecipientDetailsBox_onClick_seq0(eventobject) {
    /* 
onclickRecipientVBox.call(this);

 */
};

function p2kwiet2012247650226_tbxRecipientName_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientName.call(this);
};

function p2kwiet2012247650226_tbxRecipientName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientName.call(this);
};

function p2kwiet2012247650226_tbxRecipientName_Android_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientName.call(this);
};

function p2kwiet2012247650226_tbxRecipientName_onDone_seq0(eventobject, changedtext) {
    onDoneAddRecipientName.call(this);
};

function p2kwiet2012247650226_tbxRecipientName_onTextChange_seq0(eventobject, changedtext) {
    searchMyRecipients.call(this);
};

function p2kwiet2012247650226_hbxSelectedRecipientValues_onClick_seq0(eventobject) {
    onClickSelRecipientHBox.call(this);
};

function p2kwiet2012247650226_btnRecipientList_onClick_seq0(eventobject) {
    onClickOfAddRecipientListDropDown.call(this);
};

function p2kwiet2012247650226_segRecipientList_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onRowSelectRecipientToAdd.call(this);
};

function p2kwiet2012247650226_vbxAccountNickName_onClick_seq0(eventobject) {
    /* 
onclickRecipientVBox.call(this);

 */
};

function p2kwiet2012247650226_tbxAccountNickName_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntNickName.call(this);
};

function p2kwiet2012247650226_tbxAccountNickName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntNickName.call(this);
};

function p2kwiet2012247650226_tbxAccountNickName_Android_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntNickName.call(this);
};

function p2kwiet2012247650226_tbxAccountNickName_onDone_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntNickName.call(this);
};

function p2kwiet2012247650226_vbxAccountName_onClick_seq0(eventobject) {
    /* 
onclickRecipientVBox.call(this);

 */
};

function p2kwiet2012247650226_tbxAccountName_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntName.call(this);
};

function p2kwiet2012247650226_tbxAccountName_SPA_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntName.call(this);
};

function p2kwiet2012247650226_tbxAccountName_Android_onEndEditing_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntName.call(this);
};

function p2kwiet2012247650226_tbxAccountName_onDone_seq0(eventobject, changedtext) {
    onDoneAddRecipientAcntName.call(this);
};

function p2kwiet2012247650226_btnAddCancel_onClick_seq0(eventobject) {
    popupAddToMyRecipient.destroy();
};

function p2kwiet2012247650226_btnAddConfirm_onClick_seq0(eventobject) {
    callAddToRecipientService.call(this);
};

function p2kwiet2012247650236_segBanklist_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    selectRecepientBank.call(this);
};

function p2kwiet2012247650252_btnPopDeleteCancel_onClick_seq0(eventobject) {
    popUpCallCancel.dismiss();
};

function p2kwiet2012247650252_btnPopDeleteYEs_onClick_seq0(eventobject) {
    callToCustCare.call(this);
};

function p2kwiet2012247650261_btnPopupConfCancel_onClick_seq0(eventobject) {
    popupConfirmation.dismiss();
};

function p2kwiet2012247650270_btnPopupConfCancel_onClick_seq0(eventobject) {
    popupConfrmDelete.dismiss();
};

function p2kwiet2012247650277_btnPopupConfCancel_onClick_seq0(eventobject) {
    popupConfirmation.dismiss();
};

function p2kwiet2012247650286_popupDeRegister_init_seq0(eventobject, neworientation) {
    popupDeRegister.enabledForIdleTimeout = true
};

function p2kwiet2012247650286_btnPopupTractConf_onClick_seq0(eventobject) {
    popupDeRegister.dismiss();
};

function p2kwiet2012247650286_btnPopupConfCancel_onClick_seq0(eventobject) {
    popupDeRegister.dismiss();
};

function p2kwiet2012247650286_btnpopConfConfirm_onClick_seq0(eventobject) {
    combinationTypes.call(this);
};

function p2kwiet2012247650293_buttonCancel_onClick_seq0(eventobject) {
    popupEditBPDele.dismiss();
};

function p2kwiet2012247650293_buttonConfirm_onClick_seq0(eventobject) {
    popupEditBPDele.dismiss();
    deleteFutureBillPaymentFromViewPageMB.call(this);
    /* 
frmMBMyActivities.show();

 */
};

function p2kwiet2012247650301_btnClose_onClick_seq0(eventobject) {
    popupFirstimeActivationSpa.destroy();
};

function p2kwiet2012247650301_btnContinue_onClick_seq0(eventobject) {
    registerForTimeOut.call(this);
    navigateToTandC.call(this);
};

function p2kwiet2012247650308_buttonCancel_onClick_seq0(eventobject) {
    popupFTdel.dismiss();
};

function p2kwiet2012247650308_buttonConfirm_onClick_seq0(eventobject) {
    popupFTdel.dismiss();
    gblFtDelMB = true;
    srvFutureTransferDeleteMB.call(this);
};

function p2kwiet2012247650317_camera1_onCapture_seq0(eventobject) {
    popUploadPic.dismiss();
    if ((kony.application.getCurrentForm().id == "frmeditMyProfile")) {
        /* 
invokeCameraFromEditProfilePopup.call(this);

 */
    } else {
        /* 
invokeCameraFromPopup.call(this);

 */
    }
    invokeCameraFromEditProfilePopup.call(this);
};

function p2kwiet2012247650317_btnPopDeleteCancel_onClick_seq0(eventobject) {
    popUploadPic.dismiss();
    if ((kony.application.getCurrentForm().id == "frmeditMyProfile")) {
        openGalleryinEditProfilePopUp.call(this);
    } else {
        openMediaGalleryinPopUp.call(this);
    }
};

function p2kwiet2012247650317_button474076475461493_onClick_seq0(eventobject) {
    //frmeditMyProfile.imgprofpic.src = "avatar.png";
    popUploadPic.dismiss();
    deleteProfPic.call(this);
};

function p2kwiet2012247650317_button474076475461503_onClick_seq0(eventobject) {
    popUploadPic.dismiss();
};

function p2kwiet2012247650331_btnPopUpLogoutCancel_onClick_seq0(eventobject) {
    popUpLogout.destroy();
};

function p2kwiet2012247650331_btnPopuplogoutConf_onClick_seq0(eventobject) {
    onLogoutPopUpConfrm.call(this);
};

function p2kwiet2012247650353_popupMEADetails_init_seq0(eventobject, neworientation) {
    popupAddToMyBills.enabledForIdleTimeout = true
};

function p2kwiet2012247650353_btnClosePop_onClick_seq0(eventobject) {
    closeMEAPopup.call(this);
};

function p2kwiet2012247650358_segPop_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    if (gblMyBillerTopUpBB == 2) {
        categoryTopUpSelect.call(this, popUpMyBillers.lblFlag.text);
        validateSearch.call(this, null);
        searchSuggestedBillersBBMB.call(this);
    } else {
        kony.print("value of GblBillTopFlag is  " + GblBillTopFlag)
        if (GblBillTopFlag) {
            categoryTopUpSelect.call(this, popUpMyBillers.lblFlag.text);
            sugBillerCatChangeMB()
                /* 
sugBillerCatChangeMB.call(this);

 */
            billerCatChangeMB.call(this);
        } else {
            categoryTopUpSelect.call(this, popUpMyBillers.lblFlag.text);
            //if(gblsearchtxt.length>=3)
            sugBillerCatChangeMB()
                /* 
sugBillerCatChangeMB.call(this);

 */
            billerCatChangeMB.call(this);
        }
    }
};

function p2kwiet2012247650367_btnPopDeleteCancel_onClick_seq0(eventobject) {
    popUpCallCancel.dismiss();
};

function p2kwiet2012247650367_btnPopDeleteYEs_onClick_seq0(eventobject) {
    callToCustCare.call(this);
};

function p2kwiet2012247650395_btnAddBillCheckBox_onClick_seq0(eventobject) {
    /* 
addToMyBillsButtonClick.call(this);

 */
};

function p2kwiet2012247650395_btnPayBillCancil_onClick_seq0(eventobject) {
    popUpPayBillFromSummary.destroy();
};

function p2kwiet2012247650395_btnPayBillConfirm_onClick_seq0(eventobject) {
    onConfirmClickAddFromAccountSummaryPopUpMB.call(this);
};

function p2kwiet2012247650401_segProvinceData_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    onClickSegProvinceDistrict.call(this);
};

function p2kwiet2012247650411_btnForPassCancel_onClick_seq0(eventobject) {
    popupSpaForgotPassword.dismiss();
    if (gblSetPwdSpa) {
        frmSPALogin.show();
    }
};

function p2kwiet2012247650418_btnPopUpTermination_onClick_seq0(eventobject) {
    popUpTermination.dismiss();
};

function p2kwiet2012247650422_segPop_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    amountTopUpSelect.call(this);
};

function p2kwiet2012247650433_btnPopTouchCancel_onClick_seq0(eventobject) {
    onCancelTouchTransPwd.call(this);
};

function p2kwiet2012247650433_btnPopTouchConf_onClick_seq0(eventobject) {
    onConfirmGetDeviceID.call(this);
};

function p2kwiet2012247650469_popupTractPwd_init_seq0(eventobject, neworientation) {};

function p2kwiet2012247650469_txtOTP_onTextChange_seq0(eventobject, changedtext) {
    otpConfirmEnable.call(this);
};

function p2kwiet2012247650469_btnOtpRequest_onClick_seq0(eventobject) {
    gblMobileNewChange = "Y";
    ehPopupTractPwd_btnPopUpTractCancel_onClick.call(this);
};

function p2kwiet2012247650469_btnPopUpTractCancel_onClick_seq0(eventobject) {
    /* 

    */
    /* 
onClickOTPRequest.call(this,null);

 */
    ehPopupTractPwd_btnPopUpTractCancel_onClick.call(this);
};

function p2kwiet2012247650469_btnPopupTractConf_onClick_seq0(eventobject) {
    /* 
          

    */
    /* 
popupTractPwd.dismiss()

 */
    /* 
OTPValdatn.call(this,popupTractPwd.tbxPopupTractPwdtxt.text);

 */
    ehPopupTractPwd_btnPopupTractConf_onClick.call(this);
};

function p2kwiet2012247650480_button19723990786370_onClick_seq0(eventobject) {
    cbServerDetailsPreviewCodeClick.call(this);
};

function p2kwiet2012247650480_button200183618910538_onClick_seq0(eventobject) {
    cbCloseServerDetailsPopup.call(this);
};

function p2kwiet2012247650486_button506459299657128_onClick_seq0(eventobject) {
    TMBCallPopup.dismiss();
    frmContactUsMB.show();
};

function p2kwiet2012247650486_button506459299657130_onClick_seq0(eventobject) {
    callToCustCare.call(this);
};

function p2kwiet2012247651023_btnBack_onClick_seq0(eventobject) {
    frmTransApprove.destroy();
    frmTransFrom.show();
};

function p2kwiet2012247651023_btnLogout_onClick_seq0(eventobject) {
    frmLogin.destroy();
    frmLogin.show();
};

function p2kwiet2012247651023_vbxAccount_onClick_seq0(eventobject) {
    var alert_seq0_act0 = kony.ui.Alert({
        "message": "Transfers Menu item",
        "alertType": constants.ALERT_TYPE_ERROR,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "",
        "alertIcon": "",
        "alertHandler": null
    }, {});
};

function p2kwiet2012247651038_btnRight_onClick_seq0(eventobject) {
    onEditTransConfirm.call(this);
};

function p2kwiet2012247651051_button156335099532181_onClick_seq0(eventobject) {
    frmOpenAccTermDeposit.show();
};

function p2kwiet2012247651061_button156335099532181_onClick_seq0(eventobject) {
    frmTransferLanding.show();
};

function p2kwiet2012247651061_btnRight_onClick_seq0(eventobject) {
    enableTransferToRecipientsContactList.call(this);
};

function p2kwiet2012247651206_btnPopUpTermination_onClick_seq0(eventobject) {
    onClickNextPwdRules.call(this);
};

function p2kwiet2012247651223_btnCancel_onClick_seq0(eventobject) {
    frmMyAccountList.show();
};

function p2kwiet2012247651223_btnNext_onClick_seq0(eventobject) {
    /* 
frmMyAccntConfirmationAddAccount.show();
	
 */
    /* 
addAccount.call(this,null);

 */
};

function p2kwiet2012247651223_btnReturn_onClick_seq0(eventobject) {
    onreturnAccnt.call(this);
};

function p2kwiet2012247651223_btnAddAnother_onClick_seq0(eventobject) {
    gblAddAccntVar.call(this);
    hbxReturnFtr.setVisibility(false);
    hbxfooter.setVisibility(true);
    //Navigate to add accnt form
    frmMyAccntAddAccount.show();
    //showfooter(kony.i18n.getLocalizedString("keyCancelButton"),kony.i18n.getLocalizedString("Next"),showMyAccntList,showConfirmationAddaccount);
};

function p2kwiet2012247651223_btnBack_onClick_seq0(eventobject) {
    frmMyAccountList.show();
};

function p2kwiet2012247651231_btnCancel_onClick_seq0(eventobject) {
    frmMyAccountList.show();
};

function p2kwiet2012247651231_btnConfirm_onClick_seq0(eventobject) {};

function p2kwiet2012247651231_btnReturn_onClick_seq0(eventobject) {
    onreturnAccnt.call(this);
};

function p2kwiet2012247651231_btnAddAnother_onClick_seq0(eventobject) {};

function p2kwiet2012247651236_btnCancel_onClick_seq0(eventobject) {
    frmMyAccountView.show();
};

function p2kwiet2012247651236_btnSave_onClick_seq0(eventobject) {
    frmViewAccount.imgcomplete.setVisibility(true);
};

function p2kwiet2012247651240_button474999996106_onClick_seq0(eventobject) {
    //helpMessage.onHide = true;
    helpMessage.dismiss();
    popHelpMessageTrans.dismiss();
    popupActivationHelp.dismiss();
};

function p2kwiet2012247651244_btnBack_onClick_seq0(eventobject) {
    frmMyAccountList.show();
};

function p2kwiet2012247651599_btnTranLandNext_onClick_seq0(eventobject, context) {
    refreshContactsFromServer.call(this);
};

function p2kwiet2012247651940_btntrash_onClick_seq0(eventobject, context) {
    performDeleteTransaction.call(this, eventobject);
};

function p2kwiet2012247652415_btnGetDirection_onClick_seq0(eventobject) {
    onClickGetDirectionMap.call(this);
};

function p2kwiet2012247652415_btnDetls_onClick_seq0(eventobject) {
    onClickDetails.call(this);
};

function p2kwiet2012247652415_btnAndroidDetails_onClick_seq0(eventobject) {
    onClickDetails.call(this);
};

function TMBpreappinit_seq0(params) {
    if (kony.os.deviceInfo().name == "thinclient") {
        kony.application.setApplicationBehaviors({
            skipEscapeHtml: true
        });
    }
    defineTMBGlobals.call(this);
    /* 
loadResourceBundle.call(this);

 */
    if (isMFEnabled) {
        appMFPostInit.call(this);
    } else {
        loadResourceBundle.call(this);
    }
};

function TMBpostappinit_seq0(params) {
    showStartUp.call(this);
};

function TMBappservice_seq0(params) {
    return processAppService(params);
};