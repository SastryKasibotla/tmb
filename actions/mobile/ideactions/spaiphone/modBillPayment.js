scannedresponseData = [];
gblBillerID = "00000";
gblScheduleEndBPTmp = "";
gblScheduleRepeatBPTmp = "";

function changeDateFormatForService(enteredDateString) {
    if (isNotBlank(enteredDateString)) {
        enteredDateString = getFormattedDate(enteredDateString.trim().substr(0, 10), "en_US");
        var tokenizedDate = new Array();
        tokenizedDate = enteredDateString.split("/");
        var dd = tokenizedDate[0];
        var mm = tokenizedDate[1];
        var yyyy = tokenizedDate[2];
        var formattedDate = yyyy + '-' + mm + '-' + dd;
        return formattedDate;
    }
    return enteredDateString;
}

function changeDateFormatForMBFBillPay(enteredDateString) {
    enteredDateString = getFormattedDate(enteredDateString.substr(0, 10), "en_US");
    var tokenizedDate = new Array();
    tokenizedDate = enteredDateString.split("/");
    var dd = tokenizedDate[0];
    var mm = tokenizedDate[1];
    var yyyy = tokenizedDate[2];
    var yearSplit = yyyy.split("");
    if (yearSplit.length > 4) {
        var formattedDate = yearSplit[0] + yearSplit[1] + yearSplit[2] + yearSplit[3] + "-" + mm + "-" + dd;
    } else {
        var formattedDate = yyyy + "-" + mm + "-" + dd;
    }
    return formattedDate;
}

function callMasterBillerService() {
    showLoadingScreen();
    var billerGroupType = "";
    if (gblMyBillerTopUpBB == 0) {
        billerGroupType = "0";
    } else {
        billerGroupType = "1";
    }
    var inputParams = {
        IsActive: "1",
        BillerGroupType: billerGroupType,
        clientDate: getCurrentDate(),
        flagBillerList: "MB"
    };
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, callMasterBillerServiceCallBack);
}

function callMasterBillerServiceCallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            if (gblMyBillerTopUpBB == 0) {
                showBillPaymentLandingForm();
            } else {
                gotoTopUpSelectBillerForm()
            }
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function showBillPaymentForm() {
    gblFlagMenu = "";
    gblScannedBiller = false;
    if (checkMBUserStatus()) {
        destroyMenuSpa()
        isfirstCallToMaster = true;
        gblFromAccountSummary = false;
        gblFromAccountSummaryBillerAdded = false;
        gblMyBillerTopUpBB = 0;
        //gblFullPayment = false;
        gblFirstTimeBillPayment = true;
        gblRetryCountRequestOTP = "0";
        callBillPaymentCustomerAccountService();
    }
}

function showTopUpForm() {
    gblFlagMenu = "";
    if (checkMBUserStatus()) {
        isfirstCallToMaster = true;
        gblMyBillerTopUpBB = 1
        destroyMenuSpa();
        gblRetryCountRequestOTP = "0";
        callBillPaymentCustomerAccountService();
    }
}

function returnFromScheduleForm() {
    var prevForm = kony.application.getPreviousForm();
    gblScheduleEndBP = gblScheduleEndBPTmp;
    gblScheduleRepeatBP = gblScheduleRepeatBPTmp;
    if (prevForm["id"] == "frmBillPayment") {
        frmBillPayment.show();
    } else if (prevForm["id"] == "frmTopUp") {
        frmTopUp.show();
    } else if (prevForm["id"] == "frmTransferLanding") {
        gblScheduleTransfers = false;
        isFromEdit = true;
        frmTransferLanding.lblSchedSel.text = frmSchedule.lblSetSchePrev.text;
        frmTransferLanding.show();
    }
}

function endDateCalculator(staringDateFromCalendar, repeatTimesTextBoxValue) {
    var endingDate = staringDateFromCalendar;
    var numberOfDaysToAdd = 0;
    if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
        numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
        numberOfDaysToAdd = numberOfDaysToAdd - 1;
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
        numberOfDaysToAdd = ((repeatTimesTextBoxValue - 1) * 7);
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
        endingDate.setDate(endingDate.getDate());
        var dd = endingDate.getDate();
        var mm = endingDate.getMonth() + 1;
        var newmm = parseFloat(mm.toString()) + parseFloat(repeatTimesTextBoxValue - 1);
        var newmmadd = newmm % 12;
        if (newmmadd == 0) {
            newmmadd = 12;
        }
        var yearAdd = Math.floor((newmm / 12));
        var y = endingDate.getFullYear();
        if (newmmadd == 12) {
            y = parseFloat(y) + parseFloat(yearAdd) - 1;
        } else y = parseFloat(y) + parseFloat(yearAdd);
        mm = parseFloat(mm.toString()) + newmmadd;
        if (newmmadd == 2) {
            if (dd > 28) {
                if (!leapYear(y)) {
                    dd = 28;
                } else {
                    dd = 29;
                }
            }
        }
        if (newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11) {
            if (dd > 30) {
                dd = 30;
            }
        }
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (newmmadd < 10) {
            newmmadd = '0' + newmmadd;
        }
        var someFormattedDate = dd + '/' + newmmadd + '/' + y;
        return someFormattedDate;
    }
    var dd = endingDate.getDate();
    var mm = endingDate.getMonth() + 1;
    var y = endingDate.getFullYear();
    if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
        y = parseFloat(y) + parseFloat(repeatTimesTextBoxValue - 1);
    }
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    if (dd > 28 && mm == "02") {
        if (leapYear(y)) {
            dd = "29";
        } else {
            dd = "28";
        }
    }
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}

function addToMyBillsButtonClick() {
    if (BillPayTopUpConf.btnAddBillCheckBox.skin == "btnCheck") {
        BillPayTopUpConf.btnAddBillCheckBox.skin = "btnCheckFoc";
        gblAddToMyBill = true;
        BillPayTopUpConf.tbxScanBillerNickname.setVisibility(true);
    } else {
        BillPayTopUpConf.btnAddBillCheckBox.skin = "btnCheck";
        gblAddToMyBill = false;
        //BillPayTopUpConf.tbxScanBillerNickname.setVisibility(false);
    }
}
//FFI for BarCode Scanner
function scanBarcode() {
    gblFromAccountSummary = false;
}

function chkRunTimePermissionsForCamera() {
    if (isAndroidM()) {
        kony.print("Brajesh: CALLIING PERMISSIONS FFI....");
        var permissionsArr = [gblPermissionsJSONObj.CAMERA_GROUP];
        //Creates an object of class 'PermissionFFI'
        var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
        //Invokes method 'checkpermission' on the object
        RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr, null, callBackMarshmallowPermissionCheckCamera);
    } else {
        androidScanBarcode();
    }
}

function callBackMarshmallowPermissionCheckCamera(result) {
    if (result == "1") {
        kony.print("Brajesh: IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
        androidScanBarcode();
    } else {
        kony.print("Brajesh: IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
        //var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
        //alert(messageErr);
    }
}
/*****************************************************************
 *	Name    : iphoneScanBarcode
 *	Author  : CPCG Kony Solutions
 *	Purpose : This function sends callback(JS Function)to FFI 
 *****************************************************************/
function iphoneScanBarcode() {
    BarScanObject.scanbarcode(callback_onSubmitasynciphoneJSClass);
}
/*****************************************************************
 *	Name    : callback_onSubmitasynciphoneJSClass
 *	Author  : CPCG Kony Solutions
 *	Purpose : This function serves as the callback for the FFI and the sentdata is returned as a JSObject(resultTable).
 *****************************************************************/
function callback_onSubmitasynciphoneJSClass(result) {
    frmSelectBillerLanding.show();
    androidBarcodeResults(result, "");
}

function androidScanBarcode() {
    //Creates an object of class 'Scanner'
    var BarScanObject = new barcode.BarScan();
    //Invokes method 'scanbarcode' on the object
    BarScanObject.scanbarcode(androidBarcodeResults);
    kony.application.getCurrentForm().scrollboxMain.scrollToEnd();
}

function androidBarcodeResults(result, resultFormat) {
    var inputParam = {};
    var currentScreen = kony.application.getCurrentForm();
    currentScreen.scrollboxMain.scrollToEnd();
    if (result == "" || result == null) {
        alert(kony.i18n.getLocalizedString("MB_BPScanNotFound"));
        currentScreen.scrollboxMain.scrollToEnd();
        return false;
    } else if (result == "Back") {
        currentScreen.scrollboxMain.scrollToEnd();
        return false;
    } else if (result == "Permission") {
        alert(kony.i18n.getLocalizedString("MB_BPErr_NotAllow"));
        currentScreen.scrollboxMain.scrollToEnd();
        return false;
    } else {
        currentScreen.scrollboxMain.scrollToEnd();
        inputParam["barcode"] = result;
        showLoadingScreen();
        invokeServiceSecureAsync("checkBiller", inputParam, scannedBiller);
    }
}

function scannedBiller(status, resulttable) {
    gblRef1ScanValue = "";
    gblRef2ScanValue = "";
    var scanAmount = "0.00";
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["additionalDS"][0]["statusCode"] == 0) {
                gblRef1ScanValue = resulttable["reference1"];
                gblRef2ScanValue = resulttable["reference2"];
                scanAmount = resulttable["amount"];
                taxId = resulttable["taxID"];
                gblScanAmount = commaFormatted(scanAmount);
                var inputparam = {};
                inputparam["BillerTaxID"] = taxId;
                inputparam["BillerGroupType"] = "0";
                inputparam["IsActive"] = "1";
                inputparam["flagBillerList"] = "MB";
                invokeServiceSecureAsync("masterBillerInquiry", inputparam, scanBillerCallback);
            } else {
                dismissLoadingScreen();
                alert(resulttable["errMsg"]);
            }
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("MB_BPScanNotFound"));
        }
    } else {
        dismissLoadingScreen();
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
    }
}

function scanBillerCallback(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == "statusDesc") {
                dismissLoadingScreen();
                var errMessage = kony.i18n.getLocalizedString("crmerrMsg");
                alert(errMessage);
            } else if (resulttable["statusCode"] == "errMsg") {
                dismissLoadingScreen();
                var errMessage = kony.i18n.getLocalizedString("errMsg");
                alert(errMessage);
            } else {
                var responseData = resulttable["MasterBillerInqRs"];
                //Assuming that each TAX ID will have unique Master Biller Record
                if (responseData.length > 0) {
                    //Biller Cache Expired, then need to scan again
                    if ("Y" == resulttable["billerCacheExpired"]) {
                        dismissLoadingScreen();
                        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                        return;
                    }
                    //Setting required global variables to complete the bill payment
                    gblSpecialBiller = responseData[0]["isSpecialBiller"];
                    gblScanCompCode = responseData[0]["BillerCompcode"];
                    gblCompCode = responseData[0]["BillerCompcode"];
                    gblBillerId = responseData[0]["BillerID"];
                    gblbillerGroupType = responseData[0]["BillerGroupType"];
                    gblBillerBancassurance = responseData[0]["IsBillerBancassurance"];
                    gblAllowRef1AlphaNum = responseData[0]["AllowRef1AlphaNum"];
                    var locale = kony.i18n.getCurrentLocale();
                    gblRef1LblEN = responseData[0]["LabelReferenceNumber1EN"];
                    gblRef2LblEN = responseData[0]["LabelReferenceNumber2EN"];
                    gblRef1LblTH = responseData[0]["LabelReferenceNumber1TH"];
                    gblRef2LblTH = responseData[0]["LabelReferenceNumber2TH"];
                    gblRef1LenMB = responseData[0]["Ref1Len"];
                    gblRef2LenMB = responseData[0]["Ref2Len"];
                    if (kony.string.startsWith(locale, "en", true) == true) {
                        gblRef1LabelScan = responseData[0]["LabelReferenceNumber1EN"];
                        gblRef2LabelScan = responseData[0]["LabelReferenceNumber2EN"];
                    } else if (kony.string.startsWith(locale, "th", true) == true) {
                        gblRef1LabelScan = responseData[0]["LabelReferenceNumber1TH"];
                        gblRef2LabelScan = responseData[0]["LabelReferenceNumber2TH"];
                    }
                    gblreccuringDisablePay = responseData[0]["IsRequiredRefNumber2Pay"];
                    gblreccuringDisableAdd = responseData[0]["IsRequiredRefNumber2Add"];
                    gblRef1LabelScan = responseData[0]["LabelReferenceNumber1EN"];
                    gblRef2LabelScan = responseData[0]["LabelReferenceNumber2EN"];
                    gblBillerNameScan = responseData[0]["BillerNameEN"];
                    //alert("gblBillerNameScan --->"+gblBillerNameScan);
                    gblBillerMethod = responseData[0]["BillerMethod"];
                    gblPayFull = responseData[0]["IsFullPayment"];
                    gblBillPayFromScan = true;
                    gblBillerCompCodeEN = responseData[0]["BillerNameEN"] + " (" + gblScanCompCode + ")";
                    gblBillerCompCodeTH = responseData[0]["BillerNameTH"] + " (" + gblScanCompCode + ")";
                    //Don't Add Scanned biller to My Bills if this flag is 'Y'
                    gblBarCodeOnly = responseData[0]["BarcodeOnly"];
                    gblToAccountKey = responseData[0]["ToAccountKey"];
                    //ENH113
                    loadMEARelatedData(responseData[0]);
                    if (gblCompCode == "2533") {
                        allowSchedule = gblSegBillerDataMB["billerAllowSetSched"]
                        if (allowSchedule == "N") {
                            frmBillPayment.hbxPayBillOn.setEnabled(false);
                        } else {
                            frmBillPayment.hbxPayBillOn.setEnabled(true);
                        }
                    } else {
                        frmBillPayment.hbxPayBillOn.setEnabled(true);
                    }
                    //ENH113 end
                    var inputParams = {
                        IsActive: "1"
                    };
                    invokeServiceSecureAsync("customerBillInquiry", inputParams, checkIfBillerPresentCallback);
                } else {
                    if (resulttable["validFor"] == "01") {
                        dismissLoadingScreen();
                        alert(kony.i18n.getLocalizedString("keyNotValidForBillPayScan"));
                    } else {
                        dismissLoadingScreen();
                        BillerNotAvailable.label4750048975945.text = kony.i18n.getLocalizedString("MB_BPScanNotFound");
                        BillerNotAvailable.btnPopUpTermination.text = kony.i18n.getLocalizedString("keyClose");
                        BillerNotAvailable.show();
                    }
                }
            }
        }
    }
}

function checkIfBillerPresentCallback(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == "statusDesc") {
                dismissLoadingScreen();
                var errMessage = kony.i18n.getLocalizedString("crmerrMsg");
                alert(errMessage);
            } else if (resulttable["statusCode"] == "errMsg") {
                dismissLoadingScreen();
                var errMessage = kony.i18n.getLocalizedString("errMsg");
                alert(errMessage);
            } else {
                var responseData = resulttable["CustomerBillInqRs"];
                gblMyBillList = [];
                if (responseData.length > 0) {
                    gblMyBillList = responseData;
                    var nickName = "";
                    //Checking whether Scanned biller is available in My Bills or not
                    for (var i = 0; i < responseData.length; i++) {
                        if ((responseData[i]["BillerCompcode"] == gblScanCompCode) && (responseData[i]["ReferenceNumber1"] == gblRef1ScanValue)) {
                            if (responseData[i]["IsRequiredRefNumber2Add"] == "Y") {
                                if (responseData[i]["ReferenceNumber2"] == gblRef2ScanValue) {
                                    gblBillerPresentInMyBills = true;
                                    nickName = responseData[i]["BillerNickName"];
                                    gblBillerID = responseData[i]["CustomerBillID"];
                                    break;
                                }
                            } else {
                                gblBillerPresentInMyBills = true;
                                nickName = responseData[i]["BillerNickName"];
                                gblBillerID = responseData[i]["CustomerBillID"];
                                break;
                            }
                        }
                    }
                }
                clearBillpaymentScreen();
                launchBillPaymentFirstTime();
                gblCompCode = gblScanCompCode;
                frmBillPayment.imgBillerImage.src = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblScanCompCode + "&modIdentifier=MyBillers";
                frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
                frmBillPayment.hbxRef.setVisibility(true);
                frmBillPayment.hbxRef1.setVisibility(true);
                frmBillPayment.hbxRef2.setVisibility(true);
                frmBillPayment.lblCompCode.text = shortenBillerName(gblBillerNameScan + " (" + gblScanCompCode + ")", 18);
                frmBillPayment.lblRef1.text = gblRef1LabelScan;
                if (isNotBlank(nickName)) {
                    frmBillPayment.lblCompCode.text = nickName;
                }
                if (gblBillerBancassurance == "Y" || gblAllowRef1AlphaNum == "Y") {
                    frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
                    frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT;
                } else {
                    frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
                    frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
                }
                frmBillPayment.lblRef1Value.text = gblRef1ScanValue;
                frmBillPayment.lblRef1Value.maxTextLength = parseInt(gblRef1LenMB);
                //below line is added for CR - PCI-DSS masked Credit card no
                frmBillPayment.lblRef1ValueMasked.text = maskCreditCard(gblRef1ScanValue);
                frmBillPayment.lblForFullPayment.text = gblScanAmount;
                if (!frmBillPayment.hbxRef1.isVisible) {
                    frmBillPayment.hbxRef1.setVisibility(true);
                    //frmBillPayment.lineRef2.setVisibility(true);
                }
                if (gblreccuringDisablePay == "Y") {
                    frmBillPayment.hbxRef2.setVisibility(true);
                    frmBillPayment.lineRef1.setVisibility(true);
                    frmBillPayment.lblRef2.text = appendColon(gblRef2LabelScan);
                    frmBillPayment.tbxRef2Value.text = gblRef2ScanValue;
                    frmBillPayment.tbxRef2Value.maxTextLength = parseInt(gblRef2LenMB);
                } else {
                    frmBillPayment.hbxRef2.setVisibility(false);
                    frmBillPayment.lineRef1.setVisibility(false);
                }
                if (gblSpecialBiller == "true") {
                    frmBillPayment.lblRef1Value.text = "";
                    frmBillPayment.lblRef1ValueMasked.text = "";
                    gblPenalty = false;
                    gblFullPayment = false;
                    frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
                    frmBillPayment.hbxPenalty.setVisibility(false);
                    frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
                    frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                    frmBillPayment.hbxFullSpecButton.setVisibility(false);
                    frmBillPayment.tbxAmount.text = gblScanAmount;
                    frmBillPayment.tbxAmount.setVisibility(true);
                    frmBillPayment.tbxAmount.setEnabled(true);
                    frmBillPayment.tbxRef2Value.setEnabled(true);
                    callBillPaymentCustomerAccountService();
                } else {
                    scanBillerPresentInMyBills();
                }
            }
        }
    }
}

function scanBillerPresentInMyBills() {
    if (gblBillerMethod == 0) {
        gblPenalty = false;
        gblFullPayment = false;
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        frmBillPayment.hbxPenalty.setVisibility(false);
        frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
        frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
        frmBillPayment.hbxFullSpecButton.setVisibility(false);
        frmBillPayment.tbxAmount.text = gblScanAmount;
        frmBillPayment.tbxAmount.setVisibility(true);
        frmBillPayment.tbxAmount.setEnabled(true);
        frmBillPayment.tbxRef2Value.setEnabled(true);
        if (gblBillerBancassurance == "Y") {
            gblPolicyNumber = "";
            var inputParam = {};
            inputParam["policyNumber"] = gblRef1ScanValue;
            inputParam["dataSet"] = "0";
            invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, BillPaymentBAPolicyDetailsServiceCallBackMB);
        }
        callBillPaymentCustomerAccountService();
    } else if (gblBillerMethod == 1) {
        //for Online Biller
        // added for integration
        showLoadingScreen();
        gblPenalty = false;
        var inputParam = {};
        inputParam["TrnId"] = "";
        inputParam["Amt"] = removeCommos(gblScanAmount).toFixed(2);
        inputParam["BankId"] = "011";
        inputParam["BranchId"] = "0001";
        inputParam["BankRefId"] = "";
        inputParam["Ref1"] = gblRef1ScanValue
        inputParam["Ref2"] = gblRef2ScanValue
        inputParam["Ref3"] = "";
        inputParam["Ref4"] = "";
        inputParam["MobileNumber"] = gblRef1ScanValue;
        inputParam["compCode"] = gblCompCode;
        inputParam["isChannel"] = "MB";
        inputParam["commandType"] = "Inquiry";
        invokeServiceSecureAsync("onlinePaymentInq", inputParam, BillPaymentOnlinePaymentInqServiceCallBack);
        frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
        if (gblCompCode == "2533" || gblCompCode == "0016" || gblCompCode == "0947") {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(false);
        } else {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            if (!frmBillPayment.hbxFullSpecButton.isVisible) {
                frmBillPayment.hbxFullSpecButton.setVisibility(true);
                frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
                frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
            }
        }
    } else if (gblBillerMethod == 2) {
        //for CreditCard and Ready cash
        showLoadingScreen();
        var inputParam = {};
        var toDayDate = getTodaysDate();
        var cardId = gblRef1ScanValue;
        inputParam["cardId"] = cardId;
        inputParam["tranCode"] = TRANSCODEMIN;
        ownCard = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable["custAcctRec"].length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable["custAcctRec"][i].accType == "CCA") {
                var accountNo = gblAccountTable["custAcctRec"][i].accId;
                accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
                if (accountNo == cardId) {
                    ownCard = true;
                    break;
                }
            }
        }
        if (!ownCard) {
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(true);
            frmBillPayment.tbxAmount.setEnabled(true);
            frmBillPayment.tbxAmount.text = ""; //fullAmt
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            callBillPaymentCustomerAccountService();
        } else {
            invokeServiceSecureAsync("creditcardDetailsInq", inputParam, BillPaymentcreditcardDetailsInqServiceCallBack);
        }
    } else if (gblBillerMethod == 3) {
        showLoadingScreen();
        //for TMB Loan
        var inputParam = {};
        //for TMB Loan
        var TMB_BANK_FIXED_CODE = "0001";
        var TMB_BANK_CODE_ADD = "0011";
        var ZERO_PAD = "0000";
        var BRANCH_CODE;
        var ref2 = gblRef2ScanValue
        var ref1AccountIDLoan = gblRef1ScanValue
        var fiident;
        if (ref1AccountIDLoan.length == 10) {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
            ref1AccountIDLoan = "0" + ref1AccountIDLoan + ref2;
        }
        if (ref1AccountIDLoan.length == 13) {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
            ref1AccountIDLoan = "0" + ref1AccountIDLoan
        } else {
            fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ref1AccountIDLoan[3] + ZERO_PAD;
        }
        inputParam["acctId"] = ref1AccountIDLoan;
        inputParam["acctType"] = "LOC";
        ownLoan = false;
        var accountDetails = gblAccountTable;
        var accountLength = gblAccountTable["custAcctRec"].length;
        for (var i = 0; i < accountLength; i++) {
            if (gblAccountTable["custAcctRec"][i].accType == "LOC") {
                var accountNo = gblAccountTable["custAcctRec"][i].accId;
                //accountNo = accountNo.substring(accountNo.length-13 , accountNo.length);
                if (accountNo == ref1AccountIDLoan) {
                    ownLoan = true;
                    break;
                }
            }
        }
        if (!ownLoan) {
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            gblPenalty = false;
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            frmBillPayment.tbxAmount.setVisibility(true);
            frmBillPayment.tbxAmount.setEnabled(true);
            callBillPaymentCustomerAccountService();
        } else {
            invokeServiceSecureAsync("doLoanAcctInq", inputParam, BillPaymentdoLoanAcctInqServiceCallBack);
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            gblPenalty = false;
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(true);
            frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
            frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
        }
    } else if (gblBillerMethod == 4) {
        //no idea what to do here 
        callBillPaymentCustomerAccountService();
    }
}
var billpaymentcvindex = [0, 0];

function launchBillPaymentFirstTime() {
    gblPaynow = true;
    gblFullPayment = false;
    setRepeatClickedToFalse();
    gblStartBPDate = "";
    gblScheduleRepeatBP = "Once";
    gblScheduleEndBP = "none";
    fn_resetMEABillerDetails();
    resetBillPayAmountDetailsMB();
    frmBillPayment.tbxMyNoteValue.text = "";
    frmBillPayment.lblPayBillOnValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
    frmBillPayment.lblPenaltyValue.text = "";
    frmBillPayment.lblAmountForPenaltyOptionValue.text = "";
}

function fn_resetMEABillerDetails() {
    frmBillPayment.lblMEACustName.text = "";
    frmBillPayment.lblMEACustNameValue.text = "";
    frmBillPayment.lblMeterNo.text = "";
    frmBillPayment.lblMeterNoVal.text = "";
    frmBillPayment.lblAmtLabel.text = "";
    frmBillPayment.lblAmtInterest.text = "";
    frmBillPayment.lblAmtDisconnected.text = "";
    frmBillPayment.lblAmtValue.text = "";
    frmBillPayment.lblAmtInterestValue.text = "";
    frmBillPayment.lblAmtDisconnectedValue.text = "";
    frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
    frmBillPayment.hbxPayBillOn.setEnabled(true);
    frmBillPayment.hbxAmountDetailsMEA.setVisibility(false);
    frmBillPayment.hbxMeaCustomerName.setVisibility(false);
    frmBillPayment.hbxMeterNo.setVisibility(false);
    frmBillPayment.lineMeterNo.setVisibility(false);
}

function fn_resetIBMEABillerDetails() {
    frmIBBillPaymentLP.lblAmtLabel.text = "";
    frmIBBillPaymentLP.lblAmtInterest.text = "";
    frmIBBillPaymentLP.lblAmtDisconnected.text = "";
    frmIBBillPaymentLP.lblAmtValue.text = "";
    frmIBBillPaymentLP.lblAmtInterestValue.text = "";
    frmIBBillPaymentLP.lblAmtDisconnectedValue.text = "";
    frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString("keyAmount");
    frmIBBillPaymentLP.btnBPPayOn.setEnabled(true);
    frmIBBillPaymentLP.hbxHideBtn.setVisibility(false);
    frmIBBillPaymentLP.hbxAmountDetailsMEA.setVisibility(false);
}

function validateChannelMBBillPay() {
    // Added gblEasyPassTopUp for bill pay flow as not required in this flow
    gblEasyPassTopUp = false;
    var inputParams = {
        billerCompcode: gblCompCode
    };
    showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, validateChannelMBBillPayCallBack);
}

function validateChannelMBBillPayCallBack(status, callBackResponse) {
    dismissLoadingScreen();
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length != undefined || responseData.length != null) {
                if (responseData.length > 0) {
                    gblBillerCategoryID = responseData[0]["BillerCategoryID"];
                    gblBillerId = responseData[0]["BillerID"];
                    validatingBPEnteredDetails();
                } else {
                    if (callBackResponse["validFor"] == "01") alert(kony.i18n.getLocalizedString("keyNotValidForMB"));
                    else alert(kony.i18n.getLocalizedString("keyNotValidForBillPay"));
                }
            } else {
                alert(kony.i18n.getLocalizedString("keyOpenActGenErr"));
                dismissLoadingScreen();
                return;
            }
        } else {
            alert("No Suggested Biller found");
        }
    } else {
        if (status == 300) {
            alert("No Suggested Biller found");
        }
    }
}

function validatingBPEnteredDetails() {
    if (gblPaynow) {
        gblStartBPDate = currentSystemDate();
    }
    var billerSelected = false;
    var amountSelected = false;
    billAmountBillPayMB = "0.00";
    if (!isNotBlank(frmBillPayment.lblCompCode.text)) {
        alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
        billerSelected = false;
        return;
    } else {
        billerSelected = true;
    }
    if (!isNotBlank(frmBillPayment.lblRef1Value.text)) {
        var noRefMsg = kony.i18n.getLocalizedString("MIB_BPNoRef");
        noRefMsg = noRefMsg.replace("{ref_label}", frmBillPayment.lblRef1.text + "");
        alert(noRefMsg);
        frmBillPayment.lblRef1Value.setFocus(true);
        return;
    }
    var ref1ValBillPay = frmBillPayment.lblRef1Value.text;
    if (!validateRef2ValueAll(ref1ValBillPay)) {
        var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef1Val");
        wrongRefMsg = wrongRefMsg.replace("{ref_label}", removeColonFromEnd(frmBillPayment.lblRef1.text + ""));
        showAlert(wrongRefMsg, kony.i18n.getLocalizedString("info"));
        frmBillPayment.lblRef1Value.setFocus(true);
        return;
    }
    if (gblreccuringDisablePay == "Y" && !isNotBlank(frmBillPayment.tbxRef2Value.text)) {
        var noRefMsg = kony.i18n.getLocalizedString("MIB_BPNoRef");
        noRefMsg = noRefMsg.replace("{ref_label}", frmBillPayment.lblRef2.text + "");
        alert(noRefMsg);
        frmBillPayment.tbxRef2Value.setFocus(true);
        return;
    }
    var ref2ValBillPay = "";
    if (gblreccuringDisablePay == "Y") {
        ref2ValBillPay = frmBillPayment.tbxRef2Value.text;
        if (!validateRef2ValueAll(ref2ValBillPay)) {
            var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef2Val");
            wrongRefMsg = wrongRefMsg.replace("{ref_label}", removeColonFromEnd(frmBillPayment.lblRef2.text + ""));
            showAlert(wrongRefMsg, kony.i18n.getLocalizedString("info"));
            frmBillPayment.tbxRef2Value.setFocus(true);
            return;
        }
    }
    if (gblFullPayment) {
        var fullpayment = parseFloat(removeCommos(frmBillPayment.lblForFullPayment.text)).toFixed(2);
        if (fullpayment == "0" || fullpayment == "0.0" || fullpayment == "0.00") {
            alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
            amountSelected = false;
            return;
        } else {
            var regex = /^(?:\d*\.\d{1,2}|\d+)$/
            if (regex.test(fullpayment)) {
                amountSelected = true;
                frmBillPaymentConfirmationFuture.lblAmountValue.text = frmBillPayment.lblForFullPayment.text;
                billAmountBillPayMB = fullpayment;
            } else {
                alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
                amountSelected = false;
                return;
            }
        }
    } else {
        onDoneEditingAmountBillPayment(); // format amount to have fix 2 digits.
        var amount = parseFloat(removeCommos(frmBillPayment.tbxAmount.text)).toFixed(2);
        if (amount == "0" || amount == "0.0" || amount == "0.00") {
            alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
            amountSelected = false;
            frmBillPayment.tbxAmount.setFocus(true);
            return;
        } else {
            var regex = /^(?:\d*\.\d{1,2}|\d+)$/
            if (regex.test(amount)) {
                amountSelected = true;
                billAmountBillPayMB = amount;
            } else {
                alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
                amountSelected = false;
                frmBillPayment.tbxAmount.setFocus(true);
                return;
            }
        }
    }
    var BalanceinCommo = frmBillPayment.segSlider.selectedItems[0].lblBalance;
    var amountBalance = BalanceinCommo.replace(/,/g, "");
    amountBalance = parseFloat(amountBalance);
    if (amountBalance < billAmountBillPayMB && gblPaynow) {
        alert(kony.i18n.getLocalizedString("MIB_BPLessAvailBal"));
        if (frmBillPayment.tbxAmount.isVisible) {
            frmBillPayment.tbxAmount.setFocus(true);
        }
        dismissLoadingScreen();
        return;
    }
    if (amountSelected && billerSelected) {
        //CRF71 Implementation for MB/SPA
        if (gblBillerMethod == 1) {
            if (gblPayFull != "Y") {
                if (isNotBlank(gblMinAmount) && isNotBlank(gblMaxAmount)) {
                    if (!(billAmountBillPayMB >= parseFloat(gblMinAmount) && billAmountBillPayMB <= parseFloat(gblMaxAmount))) {
                        var message = kony.i18n.getLocalizedString("PlzEnterValidMinMaxAmount");
                        message = message.replace("[min]", commaFormatted(parseFloat(gblMinAmount).toFixed(2)));
                        message = message.replace("[max]", commaFormatted(parseFloat(gblMaxAmount).toFixed(2)));
                        alert(message);
                        if (frmBillPayment.tbxAmount.isVisible) {
                            frmBillPayment.tbxAmount.setFocus(true);
                        }
                        return;
                    }
                }
            }
        }
        var newNickName = frmBillPayment.tbxBillerNickName.text;
        if (!gblPaynow && !gblBillerPresentInMyBills) {
            if (isNotBlank(newNickName)) {
                if (NickNameValid(newNickName)) {
                    if (checkBillerNicknameInMyBills(gblMyBillList, newNickName)) {
                        //nickname exists alert
                        showAlert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"), kony.i18n.getLocalizedString("info"));
                        frmBillPayment.tbxBillerNickName.setFocus(true);
                        return false;
                    }
                } else {
                    showAlert(kony.i18n.getLocalizedString("MIB_BPkeyInvalidNickName"), kony.i18n.getLocalizedString("info"));
                    frmBillPayment.tbxBillerNickName.setFocus(true);
                    return false;
                }
            } else {
                showAlert(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"), kony.i18n.getLocalizedString("info"));
                frmBillPayment.tbxBillerNickName.setFocus(true);
                return false;
            }
        }
        //MIB-4884-Allow special characters for My Note and Note to recipient field
        /*
        if(isNotBlank(frmBillPayment.tbxMyNoteValue.text)){
        	if(!MyNoteValid(frmBillPayment.tbxMyNoteValue.text)){
        		alert(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"));
        		frmBillPayment.tbxMyNoteValue.setFocus(true);
        		return false;
        	}
        }
        */
        if (isNotBlank(frmBillPayment.tbxMyNoteValue.text)) {
            if (checkSpecialCharMyNote(frmBillPayment.tbxMyNoteValue.text)) {
                alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
                frmBillPayment.tbxMyNoteValue.setFocus(true);
                return false;
            }
        }
        if (isNotBlank(frmBillPayment.tbxMyNoteValue.text) && frmBillPayment.tbxMyNoteValue.text.length > 50) {
            alert(kony.i18n.getLocalizedString("MaxlngthNoteError"));
            frmBillPayment.tbxMyNoteValue.setFocus(true);
            return;
        }
        //   var billAmountBillPay = frmBillPayment.tbxAmount.text;
        var scheduleDtBillPay = getFormattedDate(gblStartBPDate, "en_US");
        var billerTypeMB = "Biller";
        showLoadingScreen();
        callBillerValidationBillPayMBService(gblCompCode, ref1ValBillPay, ref2ValBillPay, billAmountBillPayMB, scheduleDtBillPay, billerTypeMB);
        // }
    }
}

function populateAmountForTopUp() {
    gblTopUpType = "";
    var groupType = "";
    gblTopUpType = gblBillerMethod;
    groupType = gblbillerGroupType;
    if (groupType == 1) {
        if (gblTopUpType == 1) {
            topupAmount();
        }
    }
}

function currentSystemDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    return today;
}

function segMyBillsData() {
    frmSelectBiller.segMyBills.widgetDataMap = {
        lblBillerName: "lblBillerName",
        lblRef1: "lblRef1",
        lblRef1Value: "lblRef1Value",
        lblRef2: "lblRef2",
        lblRef2Value: "lblRef2Value",
        imgBillerArrow: "imgBillerArrow",
        imgBillerLogo: "imgBillerLogo",
        billerMethod: "billerType",
        billerGroupType: "billerGroupType",
        waiverCode: "waiverCode",
        feeAmount: "feeAmount",
        fullAmount: "fullAmount"
    }
    var billPaymentBillsData = [{
        lblBillerName: "Offline",
        lblRef1: "Mobile:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "9087656764",
        lblRef2: "AccNo",
        lblRef2Value: "211-33243-1",
        imgBillerLogo: "img_bill_1.png",
        billerMethod: "0",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "1234567"
    }, {
        lblBillerName: "Online ",
        lblRef1: "MailID:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "poituny@guima.com",
        lblRef2: "TxnNo",
        lblRef2Value: "221-33243-2",
        imgBillerLogo: "img_bill_2.png",
        billerMethod: "1",
        billerGroupType: "0",
        waiverCode: true,
        feeAmount: "100.0",
        fullAmount: "9873456"
    }, {
        lblBillerName: "TMB Credit Card ",
        lblRef1: "PAN No:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "POIUC8978H",
        lblRef2: "TxnNo",
        lblRef2Value: "231-37653-3",
        imgBillerLogo: "img_bill_3.png",
        billerMethod: "2",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "2346790"
    }, {
        lblBillerName: "Loan ",
        lblRef1: "Mobile:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "6709347612",
        lblRef2: "PaymntNo",
        lblRef2Value: "241-34233-4",
        imgBillerLogo: "img_bill_4.png",
        billerMethod: "3",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "9898989"
    }, {
        lblBillerName: "Ready Cash ",
        lblRef1: "MailID:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "luckydf@yahrr@com",
        lblRef2: "LicenseNo",
        lblRef2Value: "251-76543-5",
        imgBillerLogo: "img_bill_5.png",
        billerMethod: "4",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "12212121212"
    }, {
        lblBillerName: "TMB Credit Card",
        lblRef1: "PassportID:",
        imgBillerArrow: "navarrow3.png",
        lblRef1Value: "897/899/4567",
        lblRef2: "MobileNo",
        lblRef2Value: "291-34356-9",
        imgBillerLogo: "img_bill_1.png",
        billerMethod: "2",
        billerGroupType: "0",
        waiverCode: false,
        feeAmount: "0",
        fullAmount: "675434678"
    }];
    frmSelectBiller.segMyBills.data = billPaymentBillsData;
}

function segSuggestedBillersData() {
    frmSelectBiller.segSuggestedBillers.widgetDataMap = {
        imgSuggestedBiller: "imgSuggestedBiller",
        lblSuggestedBiller: "lblSuggestedBiller"
    }
    frmSelectBiller.segSuggestedBillers.data = [{
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "Mobile:"
    }, {
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "MailID:"
    }, {
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "MailID:"
    }, {
        imgSuggestedBiller: "mes2.png",
        lblSuggestedBiller: "PassportID:"
    }]
}
//function onMoreClicksegSuggestedBillersData() {
//	frmSelectBiller.segSuggestedBillers.widgetDataMap = {
//		imgSuggestedBiller: "imgSuggestedBiller",
//		lblSuggestedBiller: "lblSuggestedBiller"
//	}
//	frmSelectBiller.segSuggestedBillers.data = [{
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "Mobile:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "PassportID:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "Mobile2:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID2:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID2:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "PassportID2:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "Mobile3:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID3:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID3:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "PassportID3:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "Mobile4:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID4:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID4:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "PassportID4:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "Mobile5:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID5:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "MailID5:"
//    }, {
//		imgSuggestedBiller: "mes2.png",
//		lblSuggestedBiller: "PassportID5:"
//    }]
//}
//Schedule Form Related Functions
function parseDate(str) {
    var mdy = str.split('/')
    return new Date(mdy[2], mdy[1] - 1, mdy[0]);
}

function differenceMilliSeconds(first, second) {
    // Convert both dates to milliseconds
    var first_ms = first.getTime();
    var second_ms = second.getTime();
    // Calculate the difference in milliseconds
    var difference_ms = Math.abs(first_ms - second_ms);
    return difference_ms;
}

function daydiff(first, second) {
    // The number of milliseconds in one day
    var ONE_DAY = 1000 * 60 * 60 * 24 * 1;
    // Convert back to weeks and return whole days
    var days = Math.floor(differenceMilliSeconds(first, second) / ONE_DAY);
    return (days + 1);
}

function weekdiff(first, second) {
    // The number of milliseconds in one week
    var ONE_WEEK = 1000 * 60 * 60 * 24 * 7;
    // Convert back to weeks and return whole weeks
    var weeks = Math.floor(differenceMilliSeconds(first, second) / ONE_WEEK);
    return (weeks + 1);
}

function monthdiff(from, to) {
    var months = to.getMonth() - from.getMonth() + (12 * (to.getFullYear() - from.getFullYear()));
    if (to.getDate() < from.getDate()) {
        months--;
    }
    return months + 1;
}

function yeardiff(first, second) {
    // The number of milliseconds in one year
    //var ONE_YEAR = 1000 * 60 * 60 * 24 * 365;
    //	// Convert back to weeks and return whole years
    //	var years = Math.floor(differenceMilliSeconds(first, second) / ONE_YEAR);
    //	return (years + 1);
    var dateDifference = second - first;
    dateDifference = dateDifference / 86400000;
    days = daydiff(first, second);
    dateDifference = dateDifference + 1;
    year = 1 + AvgYears(first, second, dateDifference);
    return year;
}

function numberOfDays(date1, date2) {
    var startDate = parseDate(date1);
    var endDate = parseDate(date2);
    if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
        gblNumberOfDays = daydiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
        gblNumberOfDays = weekdiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
        gblNumberOfDays = monthdiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
        gblNumberOfDays = yeardiff(startDate, endDate);
    }
    return gblNumberOfDays;
}

function frmInternalSchedulePreShow() {
    if (!isNotBlank(gblStartBPDate)) {
        gblBPScheduleFirstShow = true;
        if (gblPaynow) {
            gblScheduleRepeatBP = "Once";
            gblScheduleEndBP = "none";
        }
        frmSchedule.lblEnd.setVisibility(false);
        frmSchedule.hbxEndAfterButtonHolder.setVisibility(false);
        frmSchedule.hbxEndAfter.setVisibility(false);
        frmSchedule.hbxEndOnDate.setVisibility(false);
        frmSchedule.lblEndAfter.setVisibility(false);
        frmSchedule.btnDaily.skin = "btnScheduleLeft";
        frmSchedule.btnWeekly.skin = "btnScheduleMid";
        frmSchedule.btnMonthly.skin = "btnScheduleMid";
        frmSchedule.btnYearly.skin = "btnScheduleRight";
        frmSchedule.tbxAfterTimes.text = "";
        frmSchedule.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
        frmSchedule.calScheduleStartDate.dateComponents = currentDateForScheduleCalender();
        frmSchedule.calScheduleStartDate.validStartDate = currentDateForScheduleCalender();
        frmSchedule.calScheduleEndDate.dateComponents = currentDateForScheduleCalender();
        frmSchedule.calScheduleEndDate.validStartDate = currentDateForScheduleCalender();
    } else {
        populateSelectedScheduleDate();
    }
}

function endScheduleButtonSet(eventObject) {
    frmSchedule.btnNever.skin = "btnScheduleEndLeft";
    frmSchedule.btnAfter.skin = "btnScheduleEndMid";
    frmSchedule.btnOnDate.skin = "btnScheduleEndRight";
    var buttonId = eventObject.id;
    var day = new Date();
    var dd = day.getDate();
    var mm = day.getMonth() + 1;
    var yyyy = day.getFullYear();
    var hr = day.getHours();
    var min = day.getMinutes();
    var sec = day.getSeconds();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var isCurrentDate = validateCurrentDate("setSchedule");
    if (isCurrentDate) {
        return;
    }
    if (kony.string.equalsIgnoreCase(buttonId, "btnNever")) {
        frmSchedule.tbxAfterTimes.text = "";
        eventObject.skin = "btnScheduleEndLeftFocus";
        gblScheduleEndBP = "Never";
        frmSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmSchedule.calScheduleStartDate.dateComponents);
        frmSchedule.calScheduleEndDate.validStartDate = currentDateForScheduleCalender();
        if (frmSchedule.hbxEndAfter.isVisible) {
            frmSchedule.hbxEndAfter.setVisibility(false);
        }
        if (frmSchedule.hbxEndOnDate.isVisible) {
            frmSchedule.hbxEndOnDate.setVisibility(false);
        }
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnAfter")) {
        eventObject.skin = "btnScheduleEndMidFocus";
        gblScheduleEndBP = "After";
        frmSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmSchedule.calScheduleStartDate.dateComponents);
        frmSchedule.calScheduleEndDate.validStartDate = currentDateForScheduleCalender();
        frmSchedule.tbxAfterTimes.setFocus(true);
        if (!frmSchedule.hbxEndAfter.isVisible) {
            frmSchedule.hbxEndAfter.setVisibility(true);
            frmSchedule.tbxAfterTimes.setFocus(true);
        }
        if (frmSchedule.hbxEndOnDate.isVisible) {
            frmSchedule.hbxEndOnDate.setVisibility(false);
        }
        //
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnOnDate")) {
        //
        eventObject.skin = "btnScheduleEndMidRightFocus";
        frmSchedule.tbxAfterTimes.text = "";
        gblScheduleEndBP = "OnDate";
        if (frmSchedule.hbxEndAfter.isVisible) {
            frmSchedule.hbxEndAfter.setVisibility(false);
        }
        if (!frmSchedule.hbxEndOnDate.isVisible) {
            frmSchedule.hbxEndOnDate.setVisibility(true);
        }
    } else {
        gblScheduleEndBP = "Never";
    }
}

function fullSpecButtonColorSet(eventObject) {
    frmBillPayment.btnFull2.skin = "btnScheduleEndLeft128";
    frmBillPayment.btnSpecified2.skin = "btnScheduleEndRight128";
    var buttonId = eventObject.id;
    if (kony.string.equalsIgnoreCase(buttonId, "btnFull2")) {
        eventObject.skin = "btnScheduleEndLeftFocus128";
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnSpecified2")) {
        eventObject.skin = "btnScheduleEndRightFocus128";
    }
}

function repeatScheduleButtonSet(eventObject) {
    if (!gblScheduleTransfers) {
        if (gblreccuringDisableAdd == "N" && gblreccuringDisablePay == "Y") {
            alert(kony.i18n.getLocalizedString("Valid_RecurringDisabled"));
            return;
        }
    }
    var isCurrentDate = validateCurrentDate("setSchedule");
    if (isCurrentDate) {
        return;
    }
    if (gblBPScheduleFirstShow) {
        enableEndAfterButtonHolder();
    }
    if (gblScheduleEndBP == null || gblScheduleEndBP == "" || gblScheduleEndBP == "none") {
        gblScheduleEndBP = "Never";
    }
    setRepeatClickedToFalse();
    var buttonId = eventObject.id;
    if (kony.string.equalsIgnoreCase(buttonId, "btnDaily")) {
        if (kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleLeftFocus")) {
            frmSchedule.btnDaily.skin = "btnScheduleLeft";
            gblScheduleRepeatBP = "Once";
            disableEndAfterButtonHolder();
        } else {
            frmSchedule.btnDaily.skin = "btnScheduleLeftFocus";
            gblScheduleRepeatBP = "Daily";
            DailyClicked = true;
        }
        frmSchedule.btnWeekly.skin = "btnScheduleMid";
        frmSchedule.btnMonthly.skin = "btnScheduleMid";
        frmSchedule.btnYearly.skin = "btnScheduleRight";
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnWeekly")) {
        if (kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")) {
            frmSchedule.btnWeekly.skin = "btnScheduleMid";
            gblScheduleRepeatBP = "Once";
            disableEndAfterButtonHolder();
        } else {
            frmSchedule.btnWeekly.skin = "btnScheduleMidFocus";
            weekClicked = true;
            gblScheduleRepeatBP = "Weekly";
        }
        frmSchedule.btnDaily.skin = "btnScheduleLeft";
        frmSchedule.btnMonthly.skin = "btnScheduleMid";
        frmSchedule.btnYearly.skin = "btnScheduleRight";
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnMonthly")) {
        if (kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")) {
            frmSchedule.btnMonthly.skin = "btnScheduleMid";
            disableEndAfterButtonHolder();
            gblScheduleRepeatBP = "Once";
        } else {
            frmSchedule.btnMonthly.skin = "btnScheduleMidFocus";
            monthClicked = true;
            gblScheduleRepeatBP = "Monthly";
        }
        frmSchedule.btnDaily.skin = "btnScheduleLeft";
        frmSchedule.btnWeekly.skin = "btnScheduleMid";
        frmSchedule.btnYearly.skin = "btnScheduleRight";
    } else if (kony.string.equalsIgnoreCase(buttonId, "btnYearly")) {
        if (kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleRightFocus")) {
            frmSchedule.btnYearly.skin = "btnScheduleRight";
            gblScheduleRepeatBP = "Once";
            disableEndAfterButtonHolder();
        } else {
            frmSchedule.btnYearly.skin = "btnScheduleRightFocus";
            YearlyClicked = true;
            gblScheduleRepeatBP = "Yearly";
        }
        frmSchedule.btnDaily.skin = "btnScheduleLeft";
        frmSchedule.btnWeekly.skin = "btnScheduleMid";
        frmSchedule.btnMonthly.skin = "btnScheduleMid";
    } else {
        gblScheduleRepeatBP = "Once";
    }
}

function getFormattedDate(dateValue, locale) {
    locale = gblLocaleForFormatDate;
    if (dateValue != null && dateValue != "" && dateValue != undefined && locale != "") {
        var dateNow = new Date();
        var yearNow = dateNow.getFullYear();
        var dd = dateValue.substr(0, 2);
        var mm = dateValue.substr(3, 2);
        var yyyy = dateValue.substr(6, 4);
        if (locale == "en_US") {
            if ((yyyy - yearNow) >= 543) {
                yyyy = yyyy - 543;
            }
        } else {
            if ((yyyy - yearNow) < 543) {
                yyyy = parseInt(yyyy) + 543;
            }
        }
        dateValue = dd + "/" + mm + "/" + yyyy;
    }
    return dateValue;
}
/*function isDeviceCalBuddhist() {
     	var returnedValue = iPhoneCalendar.getDeviceDateLocale();
     	var deviceDate = returnedValue.split("|")[0];
     	yyyy = deviceDate.substr(0,4);
		mm = deviceDate.substr(5,2);
		dd = deviceDate.substr(8,2);
		hr = deviceDate.substr(11,2);
		min = deviceDate.substr(14,2);
		sec = deviceDate.substr(17,2);
}*/
function doRepeatingValidations() {
    var date1 = frmSchedule.calScheduleStartDate.formattedDate;
    var locale = kony.i18n.getCurrentLocale();
    date1 = getFormattedDate(date1, locale);
    gblStartBPDate = date1;
    var repeat = "";
    var repeatAsHidden = "";
    if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Once")) {
        repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
        gblEndBPDate = date1;
        repeatAsHidden = "1";
        gblGiveToConfirmationLabel = "1" + " " + kony.i18n.getLocalizedString("keyTimesMB");
    } else if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never") || gblScheduleEndBP == "none") {
        if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly");
        }
        //repeat = date1;
        repeatAsHidden = "1";
    } else if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "OnDate")) {
        var date2 = frmSchedule.calScheduleEndDate.formattedDate;
        date2 = getFormattedDate(date2, locale);
        gblEndBPDate = date2;
        var startDate = parseDate(date1);
        var endDate = parseDate(date2);
        if (endDate < startDate) {
            //alert(kony.i18n.getLocalizedString("Valid_SelectvalidUntilDate"));
            alert(kony.i18n.getLocalizedString("keySelectValidEndDateFT"));
            repeat = "stop";
        } else {
            if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
                repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily") + " " + gblNumberOfDays + " " + kony.i18n.getLocalizedString("keyTimesMB");
            } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
                repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly") + " " + gblNumberOfDays + " " + kony.i18n.getLocalizedString("keyTimesMB");
            } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
                repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly") + " " + gblNumberOfDays + " " + kony.i18n.getLocalizedString("keyTimesMB");
            } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
                repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2 + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly") + " " + gblNumberOfDays + " " + kony.i18n.getLocalizedString("keyTimesMB");
            }
            repeatAsHidden = "3";
            gblGiveToConfirmationLabel = gblNumberOfDays + " " + kony.i18n.getLocalizedString("keyTimesMB");
        }
    } else if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "After")) {
        var numOfTimes = frmSchedule.tbxAfterTimes.text;
        var startDate = parseDate(date1);
        gblEndBPDate = endDateCalculator(startDate, numOfTimes);
        if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + gblEndBPDate + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + gblEndBPDate + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + gblEndBPDate + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
            repeat = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + gblEndBPDate + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");
        }
        repeatAsHidden = "2";
        gblGiveToConfirmationLabel = frmSchedule.tbxAfterTimes.text + " " + kony.i18n.getLocalizedString("keyTimesMB");
    }
    return [repeat, repeatAsHidden];
}

function scheduleBillPaymentSaveValidations() {
    var date1 = frmSchedule.calScheduleStartDate.formattedDate;
    var locale = kony.i18n.getCurrentLocale();
    date1 = getFormattedDate(date1, locale);
    GBL_FTRANSFER_DATA["startDate"] = date1;
    var today = new Date();
    var times;
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    today = getFormattedDate(today, locale);
    if (date1 == "" || date1 == null) {
        alert(kony.i18n.getLocalizedString("KeySelStartDate"));
        return;
    } else if ((parseDate(date1) - parseDate(today)) < 0) {
        alert(kony.i18n.getLocalizedString("KeySelVldStartDate"));
        return;
    } else if ((parseDate(date1) - parseDate(today)) == 0) {
        gblPaynow = true;
        if (gblScheduleTransfers) {
            gblScheduleTransfers = false;
            frmTransferLanding.lblSchedSel.text = date1;
            frmTransferLanding.lblSchedSel2.text = "";
            frmTransferLanding.lblSchedSel2.setVisibility(false);
            frmTransferLanding.hbxRecievedBy.setVisibility(true);
            getTransferFeeMB();
            frmTransferLanding.show();
        } else if (GblBillTopFlag) {
            frmBillPayment.lblPayBillOnValue.text = date1;
            frmBillPayment.show();
        } else {
            frmTopUp.lblPayBillOnValue.text = date1;
            frmTopUp.show();
        }
        return;
    }
    if (DailyClicked) {
        if (frmSchedule.btnDaily.skin == "btnScheduleLeft") {
            DailyClicked = false;
        }
    }
    if (weekClicked) {
        if (frmSchedule.btnWeekly.skin == "btnScheduleMid") {
            weekClicked = false;
        }
    }
    if (monthClicked) {
        if (frmSchedule.btnMonthly.skin == "btnScheduleMid") {
            monthClicked = false;
        }
    }
    if (YearlyClicked) {
        if (frmSchedule.btnYearly.skin == "btnScheduleRight") {
            YearlyClicked = false;
        }
    }
    if (!(DailyClicked || weekClicked || monthClicked || YearlyClicked)) {
        gblScheduleRepeatBP = "Once";
        gblScheduleEndBP = "none";
    }
    if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "After")) {
        var times = frmSchedule.tbxAfterTimes.text;
        if (times == null || times == "" || times == "0" || times == "00") {
            if (gblScheduleTransfers) {
                alert(kony.i18n.getLocalizedString("keyPlzEnterFutureTrfrTimes"));
            } else if (GblBillTopFlag) {
                alert(kony.i18n.getLocalizedString("keyPlzEnterBillPayTimes"));
            } else {
                alert(kony.i18n.getLocalizedString("keyPlzEnterTopUpTimes"));
            }
            return;
        }
    }
    if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")) {
        gblNumberOfDays = "-1";
    } else if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "After")) {
        gblNumberOfDays = frmSchedule.tbxAfterTimes.text;
    } else if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "OnDate")) {
        //if (gblScheduleTransfers) {
        var startDate1 = getFormattedDate(frmSchedule.calScheduleStartDate.formattedDate, locale);
        var endDate1 = getFormattedDate(frmSchedule.calScheduleEndDate.formattedDate, locale);
        if ((parseDate(startDate1) - parseDate(endDate1)) >= 0) {
            alert(kony.i18n.getLocalizedString("keySelectValidEndDateFT"));
            return;
        }
        //}
        var startDateValue = getFormattedDate(frmSchedule.calScheduleStartDate.formattedDate, locale);
        var endDateValue = getFormattedDate(frmSchedule.calScheduleEndDate.formattedDate, locale);
        gblNumberOfDays = numberOfDays(startDateValue, endDateValue);
    }
    var repeatedReturnArray = doRepeatingValidations();
    repeatNumberOfTimesText = repeatedReturnArray[0];
    var repeatedHiddenText = repeatedReturnArray[1];
    if (gblScheduleTransfers) {
        if (kony.string.equalsIgnoreCase(repeatNumberOfTimesText, "stop")) {} else {
            gblPaynow = false;
            var scheduleValues = repeatNumberOfTimesText.split("" + kony.i18n.getLocalizedString("keyRepeat") + "");
            frmTransferLanding.lblSchedSel.text = scheduleValues[0];
            frmTransferLanding.lblSchedSel2.text = kony.i18n.getLocalizedString("keyRepeat") + scheduleValues[1];
            frmTransferLanding.lblScheduleEndContainerHidden.text = repeatedHiddenText;
            gblScheduleTransfers = false;
            GBL_SMART_DATE_FT = date1;
            getFeeforFutureTrans();
            isFromEdit = true;
            frmTransferLanding.show();
        }
    } else {
        if (GblBillTopFlag) {
            if (kony.string.equalsIgnoreCase(repeatNumberOfTimesText, "stop")) {} else {
                gblPaynow = false;
                frmBillPayment.lblPayBillOnValue.text = repeatNumberOfTimesText;
                frmBillPayment.lblScheduleEndContainerHidden.text = repeatedHiddenText;
                frmBillPayment.show();
            }
        } else {
            if (kony.string.equalsIgnoreCase(repeatNumberOfTimesText, "stop")) {} else {
                gblPaynow = false;
                frmTopUp.lblPayBillOnValue.text = repeatNumberOfTimesText;
                frmTopUp.lblScheduleEndContainerHidden.text = repeatedHiddenText;
                frmTopUp.show();
            }
        }
    }
}

function fullPress() {
    gblFullPayment = true;
    frmBillPayment.lblForFullPayment.setVisibility(true);
    frmBillPayment.tbxAmount.setVisibility(false);
    frmBillPayment.lblForFullPayment.text = numberWithCommas(removeCommos(fullAmt));
}

function minPress() {
    gblFullPayment = true;
    frmBillPayment.lblForFullPayment.setVisibility(true);
    frmBillPayment.tbxAmount.setVisibility(false);
    frmBillPayment.lblForFullPayment.text = numberWithCommas(removeCommos(minAmt));
}

function myBillsRowSelected() {
    gblBillerPresentInMyBills = true;
    clearBillpaymentScreen();
    launchBillPaymentFirstTime();
    gblFirstTimeBillPayment = false;
    gblBillPayFromScan = false;
    frmBillPayment.lblForFullPayment.setVisibility(false);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.hbxRef.setVisibility(true);
    gblFromAccountSummaryBillerAdded = false;
    gblToAccountKey = frmSelectBiller["segMyBills"]["selectedItems"][0]["ToAccountKey"];
    gblBillerID = frmSelectBiller["segMyBills"]["selectedItems"][0]["CustomerBillID"];
    gblBillerMethod = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerMethod"];
    gblPayFull = frmSelectBiller["segMyBills"]["selectedItems"][0]["IsFullPayment"];
    gblRef1LblTH = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1TH"];
    gblRef2LblTH = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef2TH"];
    gblRef1LblEN = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1EN"];
    gblRef2LblEN = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef2EN"];
    gblBillerCompCodeEN = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblBillerNameENFull"];
    gblBillerCompCodeTH = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblBillerNameTHFull"];
    gblbillerGroupType = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerGroupType"];
    gblreccuringDisableAdd = frmSelectBiller["segMyBills"]["selectedItems"][0]["IsRequiredRefNumber2Add"];
    gblreccuringDisablePay = frmSelectBiller["segMyBills"]["selectedItems"][0]["IsRequiredRefNumber2Pay"];
    gblBillerBancassurance = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerBancassurance"];
    gblAllowRef1AlphaNum = frmSelectBiller["segMyBills"]["selectedItems"][0]["allowRef1AlphaNum"];
    //var billerGroupType = 0;
    frmBillPayment.hbxRef1.setVisibility(true);
    //frmBillPayment.lineRef2.setVisibility(true);
    if (gblreccuringDisablePay == "Y") {
        frmBillPayment.hbxRef2.setVisibility(true);
        frmBillPayment.lineRef1.setVisibility(true);
    } else {
        frmBillPayment.hbxRef2.setVisibility(false);
        frmBillPayment.lineRef1.setVisibility(false);
    }
    //ENH_113
    gblCompCode = frmSelectBiller["segMyBills"]["selectedItems"][0]["BillerCompCode"]["text"]
    if (gblCompCode == "2533") {
        gblSegBillerDataMB = frmSelectBiller["segMyBills"]["selectedItems"][0];
        gBillerStartTime = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerStartTime"];
        gBillerEndTime = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerEndTime"];
        allowSchedule = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerAllowSetSched"]
        if (allowSchedule == "N") {
            frmBillPayment.hbxPayBillOn.setEnabled(false);
        } else {
            frmBillPayment.hbxPayBillOn.setEnabled(true);
        }
    } else {
        frmBillPayment.hbxPayBillOn.setEnabled(true);
    }
    gblbillerServiceType = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerServiceType"];
    gblbillerTransType = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerTransType"];
    gblMeaFeeAmount = frmSelectBiller["segMyBills"]["selectedItems"][0]["billerFeeAmount"];
    frmBillPayment.tbxRef2Value.setEnabled(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    if (gblbillerGroupType == 0) {
        // billerMethod = 1;
        if (gblBillerMethod == 0) {
            //for Offline Biller
            frmBillPayment.tbxAmount.setVisibility(true);
            gblPenalty = false;
            gblFullPayment = false;
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            if (gblBillerBancassurance == "Y") {
                showLoadingScreen();
                gblPolicyNumber = "";
                var inputParam = {};
                inputParam["policyNumber"] = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1Value"];
                inputParam["dataSet"] = "0";
                invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, BillPaymentBAPolicyDetailsServiceCallBackMB);
            }
            callBillPaymentCustomerAccountService();
        } else if (gblBillerMethod == 1) {
            //for Online Biller
            // added for integration
            showLoadingScreen();
            gblScanAmount = 0;
            gblPenalty = false;
            var inputParam = {};
            inputParam["TrnId"] = "";
            inputParam["Amt"] = "0.00";
            inputParam["BankId"] = "011";
            inputParam["BranchId"] = "0001";
            inputParam["BankRefId"] = "";
            inputParam["Ref1"] = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1Value"];
            inputParam["Ref2"] = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef2Value"];
            inputParam["Ref3"] = "";
            inputParam["Ref4"] = "";
            inputParam["MobileNumber"] = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1Value"];
            inputParam["compCode"] = frmSelectBiller["segMyBills"]["selectedItems"][0]["BillerCompCode"].text;
            inputParam["isChannel"] = "MB";
            inputParam["commandType"] = "Inquiry";
            if (gblCompCode == "2533") {
                inputParam["BillerStartTime"] = gBillerStartTime;
                inputParam["BillerEndTime"] = gBillerEndTime;
            }
            invokeServiceSecureAsync("onlinePaymentInq", inputParam, BillPaymentOnlinePaymentInqServiceCallBack);
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            if (gblCompCode == "2533" || gblCompCode == "0016" || gblCompCode == "0947") {
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                frmBillPayment.hbxFullSpecButton.setVisibility(false);
                frmBillPayment.tbxAmount.setVisibility(false);
            } else {
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                if (!frmBillPayment.hbxFullSpecButton.isVisible) {
                    frmBillPayment.hbxFullSpecButton.setVisibility(true);
                    frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
                    frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
                }
            }
        } else if (gblBillerMethod == 2) {
            //for CreditCard and Ready cash
            showLoadingScreen();
            var inputParam = {};
            var toDayDate = getTodaysDate();
            var cardId = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1Value"];
            inputParam["cardId"] = cardId;
            inputParam["tranCode"] = TRANSCODEMIN;
            ownCard = false;
            var accountDetails = gblAccountTable;
            var accountLength = gblAccountTable["custAcctRec"].length;
            for (var i = 0; i < accountLength; i++) {
                if (gblAccountTable["custAcctRec"][i].accType == "CCA") {
                    var accountNo = gblAccountTable["custAcctRec"][i].accId;
                    accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
                    if (accountNo == cardId) {
                        ownCard = true;
                        break;
                    }
                }
            }
            if (!ownCard) {
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                frmBillPayment.tbxAmount.setVisibility(true);
                frmBillPayment.tbxAmount.setEnabled(true);
                frmBillPayment.tbxAmount.text = ""; //fullAmt
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                callBillPaymentCustomerAccountService();
            } else invokeServiceSecureAsync("creditcardDetailsInq", inputParam, BillPaymentcreditcardDetailsInqServiceCallBack);
        } else if (gblBillerMethod == 3) {
            showLoadingScreen();
            //for TMB Loan
            var inputParam = {};
            //for TMB Loan
            var TMB_BANK_FIXED_CODE = "0001";
            var TMB_BANK_CODE_ADD = "0011";
            var ZERO_PAD = "0000";
            var BRANCH_CODE;
            var ref2 = frmSelectBiller.segMyBills.selectedItems[0].lblRef2Value;
            var ref1AccountIDLoan = frmSelectBiller.segMyBills.selectedItems[0].lblRef1Value;
            var fiident;
            if (ref1AccountIDLoan.length == 10) {
                fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
                ref1AccountIDLoan = "0" + ref1AccountIDLoan + ref2;
            }
            if (ref1AccountIDLoan.length == 13) {
                fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ZERO_PAD;
                ref1AccountIDLoan = "0" + ref1AccountIDLoan
            } else {
                fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] + ref1AccountIDLoan[3] + ZERO_PAD;
            }
            inputParam["acctId"] = ref1AccountIDLoan;
            inputParam["acctType"] = "LOC";
            ownLoan = false;
            var accountDetails = gblAccountTable;
            var accountLength = gblAccountTable["custAcctRec"].length;
            for (var i = 0; i < accountLength; i++) {
                if (gblAccountTable["custAcctRec"][i].accType == "LOC") {
                    var accountNo = gblAccountTable["custAcctRec"][i].accId;
                    //accountNo = accountNo.substring(accountNo.length-13 , accountNo.length);
                    if (accountNo == ref1AccountIDLoan) {
                        ownLoan = true;
                        break;
                    }
                }
            }
            if (!ownLoan) {
                frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
                gblPenalty = false;
                frmBillPayment.hbxPenalty.setVisibility(false);
                frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                frmBillPayment.hbxFullSpecButton.setVisibility(false);
                frmBillPayment.tbxAmount.setVisibility(true);
                frmBillPayment.tbxAmount.setEnabled(true);
                callBillPaymentCustomerAccountService();
            } else {
                invokeServiceSecureAsync("doLoanAcctInq", inputParam, BillPaymentdoLoanAcctInqServiceCallBack);
                frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
                gblPenalty = false;
                frmBillPayment.hbxPenalty.setVisibility(false);
                frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                frmBillPayment.hbxFullSpecButton.setVisibility(true);
                frmBillPayment.btnFull2.skin = btnScheduleEndLeftFocus128;
                frmBillPayment.btnSpecified2.skin = btnScheduleEndRight128;
            }
        } else if (gblBillerMethod == 4) {
            //for Old Ready Cash
            var inputParam = {};
            var ref1AccountIDDeposit = frmSelectBiller.segMyBills.selectedItems[0].lblRef1Value;
            inputparam["acctId"] = ref1AccountIDDeposit;
            ownRC = false;
            var accountDetails = gblAccountTable;
            var accountLength = gblAccountTable.length;
            for (var i = 0; i < accountLength; i++) {
                if (gblAccountTable.accType == "LOC") {
                    var accountNo = gblAccountTable.accId;
                    accountNo = accountNo.substring(accountNo.length - 16, accountNo.length);
                    if (accountNo == ref1AccountIDDeposit) {
                        ownRC = true;
                        break;
                    }
                }
            }
            if (!ownRC) {
                gblPenalty = false;
                frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
                frmBillPayment.hbxPenalty.setVisibility(false);
                frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
                frmBillPayment.hbxFullSpecButton.setVisibility(false);
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(false);
                frmBillPayment.tbxAmount.setEnabled(true);
                frmBillPayment.tbxAmount.setVisibility(true);
                callBillPaymentCustomerAccountService();
                return;
            }
            invokeServiceSecureAsync("depositAccountInquiry", inputParam, BillPaymentdepositAccountInquiryServiceCallBack);
            gblPenalty = false;
            frmBillPayment.lblAmount.text = kony.i18n.getLocalizedString("TREnter_PL_Amount");
            frmBillPayment.hbxPenalty.setVisibility(false);
            frmBillPayment.hbxMainAmountForPenalty.setVisibility(false);
            frmBillPayment.hbxFullSpecButton.setVisibility(false);
            if (!frmBillPayment.hbxFullMinSpecButtons.isVisible) {
                frmBillPayment.hbxFullMinSpecButtons.setVisibility(true);
                frmBillPayment.button475004897849.skin = btnScheduleEndLeftFocus;
                frmBillPayment.button475004897851.skin = btnScheduleMid;
                frmBillPayment.button475004897853.skin = btnScheduleEndRight;
            }
        }
    }
    frmBillPayment.lblCompCode.text = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblBillerNickname"].text;
    frmBillPayment.imgBillerImage.src = frmSelectBiller["segMyBills"]["selectedItems"][0]["imgBillerLogo"].src;
    if (gblBillerBancassurance == "Y" || gblAllowRef1AlphaNum == "Y") {
        frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
        frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT;
    } else {
        frmBillPayment.lblRef1Value.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
        frmBillPayment.lblRef1Value.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
    }
    frmBillPayment.lblRef1Value.text = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1Value"];
    frmBillPayment.lblRef1Value.maxTextLength = parseInt(frmSelectBiller["segMyBills"]["selectedItems"][0]["Ref1Len"]);
    //below line is added for CR - PCI-DSS masked Credit card no
    frmBillPayment.lblRef1ValueMasked.text = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1ValueMasked"];
    frmBillPayment.lblRef1.text = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef1"];
    if (frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef2Value"] == undefined || frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef2Value"] == null) {
        frmBillPayment.tbxRef2Value.text = ""
    } else {
        frmBillPayment.tbxRef2Value.text = frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef2Value"];
        frmBillPayment.tbxRef2Value.maxTextLength = parseInt(frmSelectBiller["segMyBills"]["selectedItems"][0]["Ref2Len"]);
    }
    frmBillPayment.lblRef2.text = appendColon(frmSelectBiller["segMyBills"]["selectedItems"][0]["lblRef2"]);
}

function BillPaymentBAPolicyDetailsServiceCallBackMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var billPaymentValueDS = resulttable["billPaymentValueDS"];
            var policyDetailsValueDS = resulttable["policyDetailsValueDS"];
            if (policyDetailsValueDS.length > 0) {
                if (policyDetailsValueDS[0]["value_EN"] == "1") {
                    if (billPaymentValueDS.length > 0) {
                        frmBillPayment.tbxRef2Value.text = billPaymentValueDS[3]["value_EN"];
                        frmBillPayment.tbxAmount.text = billPaymentValueDS[4]["value_EN"];
                        frmBillPayment.tbxRef2Value.setEnabled(true);
                        frmBillPayment.tbxAmount.setEnabled(true);
                        gblPayPremium = "Y";
                        dismissLoadingScreen();
                    } else {
                        gblPayPremium = "N";
                        //Biller details not found in system, allow payment as normal offline biller 
                        dismissLoadingScreen();
                    }
                } else {
                    gblPayPremium = "N";
                    launchBillPaymentFirstTime();
                    dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("BA_Policy_Already_Paid"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
            } else {
                gblPayPremium = "N";
                //Policy details not found in system, allow payment as normal offline biller 
                dismissLoadingScreen();
            }
        } else {
            gblPayPremium = "N";
            //Policy not found in system, allow payment as normal offline biller 
            dismissLoadingScreen();
        }
    }
}

function launchTopUpFirstTime() {
    frmTopUp.hbxTopUpamntComboBox.setVisibility(false);
    frmTopUp.hbxExclBillerOne.setVisibility(true);
    frmTopUp.lblTopUpName.text = "";
    //frmTopUp.lblBillerCompCode.text = ""; widget deleted
    frmTopUp.imgTopUpIcon.src = "";
    frmTopUp.tbxExcludingBillerOne.text = "";
    frmTopUp.tbxExcludingBillerOne.placeholder = "0.00";
    frmTopUp.tbxMyNoteValue.text = "";
    frmTopUp.lblPayBillOnValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
    gblPaynow = true;
    gblStartBPDate = "";
    setRepeatClickedToFalse();
    frmTopUp.btnTopUpAmountComboBox.text = ""; //kony.i18n.getLocalizedString("keyAmount");
    frmTopUp.lblComboAmount.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyAmount"));
}

function gotoBillPaymentConfirmation() {
    var activityTypeID = "";
    //billPayConfirmCheckChangeSkin();
    if (gblPaynow) {
        frmBillPaymentConfirmationFuture.hbxBalanceAfter.setVisibility(true)
            //make schedule fields invisible
        if (frmBillPaymentConfirmationFuture.hbxStartEndDate.isVisible && frmBillPaymentConfirmationFuture.hbxRepeatExcuteTimes.isVisible) {
            frmBillPaymentConfirmationFuture.hbxStartEndDate.setVisibility(false);
            frmBillPaymentConfirmationFuture.hbxRepeatExcuteTimes.setVisibility(false);
        }
    } else {
        if (!frmBillPaymentConfirmationFuture.hbxStartEndDate.isVisible && !frmBillPaymentConfirmationFuture.hbxRepeatExcuteTimes.isVisible) {
            frmBillPaymentConfirmationFuture.hbxStartEndDate.setVisibility(true);
            frmBillPaymentConfirmationFuture.hbxRepeatExcuteTimes.setVisibility(true);
        }
        frmBillPaymentConfirmationFuture.hbxBalanceAfter.setVisibility(false) //no amount balance field in future
    }
    if ((kony.string.equalsIgnoreCase(gblScheduleEndBP, "none") || kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")) && (!kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "none") && !kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Once"))) {
        //frmBillPaymentConfirmationFuture.hbxEndOn.setVisibility(true);
        //frmBillPaymentConfirmationFuture.hbxExecutionTimes.setVisibility(true);
        frmBillPaymentConfirmationFuture.lblExecuteValue.text = "-";
        frmBillPaymentConfirmationFuture.lblEndOnValue.text = "-";
        gblGiveToConfirmationLabel = "-";
        gblEndBPDate = "-";
    } else {
        //frmBillPaymentConfirmationFuture.hbxEndOn.setVisibility(true);
        //frmBillPaymentConfirmationFuture.hbxExecutionTimes.setVisibility(true);
    }
    var currentdate = new Date();
    if (GblBillTopFlag) {
        if (gblPaynow) {
            activityTypeID = "027";
        } else {
            activityTypeID = "028";
        }
        frmBillPaymentConfirmationFuture.lblAccUserName.text = frmBillPayment.segSlider.selectedItems[0].custAcctName;
        if (gblPaynow || gblBillerPresentInMyBills || !isNotBlank(frmBillPayment.tbxBillerNickName.text)) {
            frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text = frmBillPayment.lblCompCode.text;
            frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = frmBillPayment.lblCompCode.text;
            frmBillPaymentConfirmationFuture.lblBillerNickname.text = frmBillPayment.lblCompCode.text;
        } else {
            frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text = frmBillPayment.tbxBillerNickName.text;
            frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = frmBillPayment.tbxBillerNickName.text;
            frmBillPaymentConfirmationFuture.lblBillerNickname.text = frmBillPayment.tbxBillerNickName.text;
        }
        frmBillPaymentConfirmationFuture.lblPayToBillerName.text = kony.i18n.getLocalizedString("MIB_BPkeyPayTo");
        frmBillPaymentConfirmationFuture.lblHiddenVariableForScheduleEnd.text = frmBillPayment.lblScheduleEndContainerHidden.text;
        frmBillPaymentConfirmationFuture.imgBillerPic.src = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblCompCode + "&modIdentifier=MyBillers";
        frmBillPaymentConfirmationFuture.lblRef1.text = frmBillPayment.lblRef1.text;
        //frmBillPaymentConfirmationFuture.lblRef1Value.text = frmBillPayment.lblRef1Value.text;
        if (frmBillPayment.hbxRef2.isVisible == true) {
            frmBillPaymentConfirmationFuture.hbxRef2.setVisibility(true);
            frmBillPaymentConfirmationFuture.lblRef2.text = frmBillPayment.lblRef2.text;
            frmBillPaymentConfirmationFuture.lblRef2Value.text = frmBillPayment.tbxRef2Value.text;
        } else {
            frmBillPaymentConfirmationFuture.hbxRef2.setVisibility(false);
        }
        if (gblFullPayment) {
            frmBillPaymentConfirmationFuture.lblAmountValue.text = frmBillPayment.lblForFullPayment.text + kony.i18n.getLocalizedString("currencyThaiBaht");;
        } else {
            //var amount = parseFloat(removeCommos(frmBillPayment.tbxAmount.text));
            //amount = amount.toFixed(2);
            //frmBillPaymentConfirmationFuture.lblAmountValue.text = commaFormatted(amount.toString()) + kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        //frmBillPaymentConfirmationFuture.lblPaymentDateValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale())+ " [" + formatAMPM(currentdate) +"]";
        //frmBillPaymentConfirmationFuture.lblMyNoteValue.text = frmBillPayment.tbxMyNoteValue.text;
        /*
        if (flowSpa) {
            frmBillPaymentConfirmationFuture.imgFromAccount.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId=" + 11 + "&modIdentifier=BANKICON";;
            frmBillPaymentConfirmationFuture.lblAccountName.text = gblmbSpaselectedData.lblCustName;
            gblProductNameEN = gblmbSpaselectedData.lblAccountNickNameEN;
            gblProductNameTH = gblmbSpaselectedData.lblAccountNickNameTH;
            frmBillPaymentConfirmationFuture.lblAccountNum.text = gblmbSpaselectedData.lblActNoval;
            frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text = gblmbSpaselectedData.lblBalance;
            gblSelectedAccBal = gblmbSpaselectedData.lblBalance;
            if (gblPaynow && isNotBlank(gblmbSpaselectedData.lblRemainFee) && isNotBlank(gblmbSpaselectedData.lblRemainFeeValue)) {
                frmBillPaymentConfirmationFuture.lblFreeTran.text = gblmbSpaselectedData.lblRemainFee;
                frmBillPaymentConfirmationFuture.lblFreeTransValue.text = gblmbSpaselectedData.lblRemainFeeValue;
                frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(true);
                gblBillpaymentNoFee = true;
            } else {
                gblBillpaymentNoFee = false;
                frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(false)
            }
        } else {
        */
        frmBillPaymentConfirmationFuture.imgFromAccount.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + 11 + "&modIdentifier=BANKICON";;
        frmBillPaymentConfirmationFuture.lblAccountName.text = frmBillPayment["segSlider"]["selectedItems"][0].lblCustName;
        gblProductNameEN = frmBillPayment["segSlider"]["selectedItems"][0].lblAccountNickNameEN;
        gblProductNameTH = frmBillPayment["segSlider"]["selectedItems"][0].lblAccountNickNameTH;
        frmBillPaymentConfirmationFuture.lblAccountNum.text = frmBillPayment["segSlider"]["selectedItems"][0].lblActNoval;
        frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text = frmBillPayment["segSlider"]["selectedItems"][0].lblBalance;
        gblSelectedAccBal = frmBillPayment["segSlider"]["selectedItems"][0].lblBalance;
        if (gblPaynow && isNotBlank(frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFee) && isNotBlank(frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFeeValue)) {
            frmBillPaymentConfirmationFuture.lblFreeTran.text = frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFee;
            frmBillPaymentConfirmationFuture.lblFreeTransValue.text = frmBillPayment["segSlider"]["selectedItems"][0].lblRemainFeeValue;
            frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(true);
            gblBillpaymentNoFee = true;
        } else {
            gblBillpaymentNoFee = false;
            frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(false)
        }
        //}
    } else {
        if (gblPaynow) {
            activityTypeID = "030";
        } else {
            activityTypeID = "031";
        }
        if (gblEasyPassTopUp) {
            frmBillPaymentConfirmationFuture.hbxEasyPass.setVisibility(true);
            frmBillPaymentConfirmationFuture.hbxEasyPassTxnId.setVisibility(true);
        } else {
            frmBillPaymentConfirmationFuture.hbxEasyPass.setVisibility(false);
            frmBillPaymentConfirmationFuture.hbxEasyPassTxnId.setVisibility(false);
        }
        /*
        if (flowSpa) {
            frmBillPaymentConfirmationFuture.lblAccUserName.text = gblmbSpaselectedData.custAcctName;
        }
        */
        //else {
        frmBillPaymentConfirmationFuture.lblAccUserName.text = frmTopUp.segSlider.selectedItems[0].custAcctName;
        //}
        frmBillPaymentConfirmationFuture.lblPayToBillerName.text = kony.i18n.getLocalizedString("keyTopUpCompltPayTo");
        frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text = frmTopUp.lblTopUpName.text;
        frmBillPaymentConfirmationFuture.lblBillerNameCompCodeShrtn.text = frmTopUp.lblTopUpName.text;
        frmBillPaymentConfirmationFuture.lblBillerNickname.text = frmTopUp.lblTopUpName.text;
        frmBillPaymentConfirmationFuture.lblHiddenVariableForScheduleEnd.text = frmTopUp.lblScheduleEndContainerHidden.text;
        //frmBillPaymentConfirmationFuture.lblBillerNickname.text = frmTopUp.lblTopUpName.text;
        frmBillPaymentConfirmationFuture.imgBillerPic.src = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblCompCode + "&modIdentifier=MyBillers";
        frmBillPaymentConfirmationFuture.lblRef1.text = frmTopUp.lblRef1.text;
        //frmBillPaymentConfirmationFuture.lblPaymentDateValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale()) + " [" + formatAMPM(currentdate) + "]";
        //frmBillPaymentConfirmationFuture.lblMyNoteValue.text = frmTopUp.tbxMyNoteValue.text;
        /*
        if (flowSpa) {
            frmBillPaymentConfirmationFuture.imgFromAccount.src = gblmbSpaselectedData.img1;
            frmBillPaymentConfirmationFuture.lblAccountName.text = gblmbSpaselectedData.lblCustName;
            gblProductNameEN = gblmbSpaselectedData.lblAccountNickNameEN;
            gblProductNameTH = gblmbSpaselectedData.lblAccountNickNameTH;
            frmBillPaymentConfirmationFuture.lblAccountNum.text = gblmbSpaselectedData.lblActNoval;
            frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text = gblmbSpaselectedData.lblBalance;
            gblSelectedAccBal = gblmbSpaselectedData.lblBalance;
        } else {
        */
        frmBillPaymentConfirmationFuture.imgFromAccount.src = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + 11 + "&modIdentifier=BANKICON";
        frmBillPaymentConfirmationFuture.lblAccountName.text = frmTopUp["segSlider"]["selectedItems"][0].lblCustName;
        gblProductNameEN = frmTopUp["segSlider"]["selectedItems"][0].lblAccountNickNameEN;
        gblProductNameTH = frmTopUp["segSlider"]["selectedItems"][0].lblAccountNickNameTH;
        frmBillPaymentConfirmationFuture.lblAccountNum.text = frmTopUp["segSlider"]["selectedItems"][0].lblActNoval;
        frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text = frmTopUp["segSlider"]["selectedItems"][0].lblBalance;
        gblSelectedAccBal = frmTopUp["segSlider"]["selectedItems"][0].lblBalance;
        //}
        /*
        if (flowSpa) {
            if (gblPaynow && isNotBlank(gblmbSpaselectedData.lblRemainFee) && isNotBlank(gblmbSpaselectedData.lblRemainFeeValue)) {
                frmBillPaymentConfirmationFuture.lblFreeTran.text = gblmbSpaselectedData.lblRemainFee;
                frmBillPaymentConfirmationFuture.lblFreeTransValue.text = gblmbSpaselectedData.lblRemainFeeValue;
                frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(true);
                gblBillpaymentNoFee = true;
            } else {
            
                gblBillpaymentNoFee = false;
                frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(false)
          //  }
        } else {
        */
        if (gblPaynow && isNotBlank(frmTopUp["segSlider"]["selectedItems"][0].lblRemainFee) && isNotBlank(frmTopUp["segSlider"]["selectedItems"][0].lblRemainFeeValue)) {
            frmBillPaymentConfirmationFuture.lblFreeTran.text = frmTopUp["segSlider"]["selectedItems"][0].lblRemainFee;
            frmBillPaymentConfirmationFuture.lblFreeTransValue.text = frmTopUp["segSlider"]["selectedItems"][0].lblRemainFeeValue;
            frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(true);
            gblBillpaymentNoFee = true;
        } else {
            gblBillpaymentNoFee = false;
            frmBillPaymentConfirmationFuture.hbxFreeTrans.setVisibility(false)
        }
        //}
    }
    frmBillPaymentConfirmationFuture.lblStartOnValue.text = gblStartBPDate;
    frmBillPaymentConfirmationFuture.lblEndOnValue.text = gblEndBPDate;
    //frmBillPaymentConfirmationFuture.lblEveryMonth.text = gblScheduleRepeatBP;
    //frmBillPaymentConfirmationFuture.lblExecuteValue.text = gblGiveToConfirmationLabel;
    var timesVariable = gblGiveToConfirmationLabel;
    timesVariable = timesVariable.substr(0, timesVariable.indexOf(" "))
    frmBillPaymentConfirmationFuture.lblExecuteValue.text = timesVariable + " " + kony.i18n.getLocalizedString("keyTimesMB");
    if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
        frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyDaily");
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
        frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyWeekly");
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
        frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyMonthly");
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
        frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyYearly");
    } else {
        frmBillPaymentConfirmationFuture.lblEveryMonth.text = kony.i18n.getLocalizedString("keyOnce");
    }
    if (gblPaynow) {
        frmBillPaymentConfirmationFuture.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
    } else {
        frmBillPaymentConfirmationFuture.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
    }
    if (gblCompCode == "2533") {
        frmBillPaymentConfirmationFuture.lblAmount.text = kony.i18n.getLocalizedString("keyBillPayTotalAmountFee");
    } else {
        frmBillPaymentConfirmationFuture.lblAmount.text = kony.i18n.getLocalizedString("keyAmount");
    }
    //frmBillPaymentConfirmationFuture.line6801452681250922.setVisibility(false);
    frmBillPaymentConfirmationFuture.imgHeaderMiddle.setVisibility(false);
    //frmBillPaymentConfirmationFuture.hbxArrow.setVisibility(false);
    // added for extra space
    if (gblPaynow && !gblEasyPassTopUp && gblCompCode != "2533") {
        frmBillPaymentConfirmationFuture.hbxspace.setVisibility(true);
    } else {
        frmBillPaymentConfirmationFuture.hbxspace.setVisibility(false);
    }
    frmBillPaymentConfirmationFuture.show();
    dismissLoadingScreen();
}

function gotoBillPaymentCompleteMB(resulttable) {
    if ("Y" == resulttable["billerFound"]) {
        frmBillPaymentComplete.hbxAddToMyBills.setVisibility(false);
    } else {
        frmBillPaymentComplete.hbxAddToMyBills.setVisibility(true);
    }
    frmBillPaymentComplete.line968116430627578.setVisibility(false);
    frmBillPaymentComplete.lblHide.text = kony.i18n.getLocalizedString("Hide");
    if (gblPaynow && gblBillpaymentNoFee) {
        frmBillPaymentComplete.hbxFreeTrans.setVisibility(true);
        frmBillPaymentComplete.lblFreeTran.text = frmBillPaymentConfirmationFuture.lblFreeTran.text;
        var freeTran = parseFloat(frmBillPaymentConfirmationFuture.lblFreeTransValue.text);
        var prodCode = "";
        if (GblBillTopFlag) {
            prodCode = frmBillPayment.segSlider.selectedItems[0].prodCode;
        } else {
            prodCode = frmTopUp.segSlider.selectedItems[0].prodCode;
        }
        if (freeTran < 1) {
            //do nothing
            frmBillPaymentComplete.lblFreeTransValue.text = frmBillPaymentConfirmationFuture.lblFreeTransValue.text;
        } else {
            if ((prodCode == "225" || prodCode == "226") && (gblCompCode == "0699" || gblCompCode == "CC01" || gblCompCode == "AL02" || gblCompCode == "AL01")) frmBillPaymentComplete.lblFreeTransValue.text = frmBillPaymentConfirmationFuture.lblFreeTransValue.text;
            else if (resulttable["RemainingFee"] != undefined && resulttable["RemainingFee"] != null) frmBillPaymentComplete.lblFreeTransValue.text = resulttable["RemainingFee"];
        }
    } else {
        frmBillPaymentComplete.hbxFreeTrans.setVisibility(false)
    }
    //billPayCompleteCheckChangeSkin();
    //added for New UI
    frmBillPaymentComplete.lblPaymentDateValue.text = frmBillPaymentConfirmationFuture.lblPaymentDateValue.text;
    if (gblPaynow) {
        frmBillPaymentComplete.hbxBalanceAfter.setVisibility(true)
        frmBillPaymentComplete.vbox156335099531837.setVisibility(true);
        frmBillPaymentComplete.line6801452681250922.setVisibility(true);
        //make schedule fields invisible
        if (frmBillPaymentComplete.hbxStartEndDate.isVisible && frmBillPaymentComplete.hbxRepeatExcuteTimes.isVisible) {
            frmBillPaymentComplete.hbxStartEndDate.setVisibility(false);
            frmBillPaymentComplete.hbxRepeatExcuteTimes.setVisibility(false);
        }
    } else {
        if (!frmBillPaymentComplete.hbxStartEndDate.isVisible && !frmBillPaymentComplete.hbxRepeatExcuteTimes.isVisible) {
            frmBillPaymentComplete.hbxStartEndDate.setVisibility(true);
            frmBillPaymentComplete.hbxRepeatExcuteTimes.setVisibility(true);
        }
        frmBillPaymentComplete.hbxBalanceAfter.setVisibility(false); //no amount balance field in future
        frmBillPaymentComplete.vbox156335099531837.setVisibility(false);
        frmBillPaymentComplete.line6801452681250922.setVisibility(false);
    }
    frmBillPaymentComplete.lblBillerNickname.text = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
    frmBillPaymentComplete.lblRef1.text = frmBillPaymentConfirmationFuture.lblRef1.text;
    //below line is changed for CR - PCI-DSS masked Credit card no
    frmBillPaymentComplete.lblRef1ValueMasked.text = frmBillPaymentConfirmationFuture.lblRef1ValueMasked.text; //frmBillPaymentConfirmationFuture.lblRef1Value.text;
    if (frmBillPaymentConfirmationFuture.hbxRef2.isVisible == true) {
        frmBillPaymentComplete.hbxRef2.setVisibility(true);
        frmBillPaymentComplete.lblRef2.text = frmBillPaymentConfirmationFuture.lblRef2.text;
        frmBillPaymentComplete.lblRef2Value.text = frmBillPaymentConfirmationFuture.lblRef2Value.text;
    } else {
        frmBillPaymentComplete.hbxRef2.setVisibility(false);
    }
    if (gblEasyPassTopUp) {
        frmBillPaymentComplete.hbxEasyPass.setVisibility(true);
        frmBillPaymentComplete.hbxEasyPassTxnId.setVisibility(true);
        frmBillPaymentComplete.hbxCardBal.setVisibility(true);
    } else {
        frmBillPaymentComplete.hbxEasyPass.setVisibility(false);
        frmBillPaymentComplete.hbxEasyPassTxnId.setVisibility(false);
        frmBillPaymentComplete.hbxCardBal.setVisibility(false);
    }
    frmBillPaymentComplete.lblAccUserName.text = frmBillPaymentConfirmationFuture.lblAccUserName.text;
    frmBillPaymentComplete.lblPaymentFeeValue.text = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
    frmBillPaymentComplete.lblAmountValue.text = frmBillPaymentConfirmationFuture.lblAmountValue.text;
    //frmBillPaymentComplete.lblMyNoteValue.text = frmBillPayment.tbxMyNoteValue.text;
    frmBillPaymentComplete.imgBillerPic.src = frmBillPaymentConfirmationFuture.imgBillerPic.src;
    frmBillPaymentComplete.imgFromAccount.src = frmBillPaymentConfirmationFuture.imgFromAccount.src;
    frmBillPaymentComplete.lblAccountName.text = frmBillPaymentConfirmationFuture.lblAccountName.text;
    frmBillPaymentComplete.lblAccountNum.text = frmBillPaymentConfirmationFuture.lblAccountNum.text;
    frmBillPaymentComplete.lblBalBeforePayValue.text = commaFormatted(resulttable["availBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmBillPaymentComplete.lblStartOnValue.text = frmBillPaymentConfirmationFuture.lblStartOnValue.text;
    frmBillPaymentComplete.lblEndOnValue.text = frmBillPaymentConfirmationFuture.lblEndOnValue.text;
    frmBillPaymentComplete.lblEveryMonth.text = frmBillPaymentConfirmationFuture.lblEveryMonth.text;
    frmBillPaymentComplete.lblExecuteValue.text = frmBillPaymentConfirmationFuture.lblExecuteValue.text;
    frmBillPaymentComplete.lblTopUpRefNumValue.text = frmBillPaymentConfirmationFuture.lblTopUpRefNumValue.text;
    frmBillPaymentComplete.lblTxnNumValue.text = frmBillPaymentConfirmationFuture.lblTxnNumValue.text
    frmBillPaymentComplete.lblBillerNameCompCode.text = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text;
    if (gblCompCode == "2533") {
        frmBillPaymentComplete.hbxAmountDetailsMEA.setVisibility(true);
        frmBillPaymentComplete.hbox101271281131304.setVisibility(false); //Remove Show HIDE button for MEA
        frmBillPaymentComplete.lblAmtLabel.text = frmBillPaymentConfirmationFuture.lblAmtLabel.text + ":";
        frmBillPaymentComplete.lblAmountValue1.text = frmBillPaymentConfirmationFuture.lblAmountValue1.text;
        frmBillPaymentComplete.lblAmtInterest.text = frmBillPaymentConfirmationFuture.lblAmtInterest.text + ":";
        frmBillPaymentComplete.lblAmtInterestValue.text = frmBillPaymentConfirmationFuture.lblAmtInterestValue.text;
        frmBillPaymentComplete.lblAmtDisconnected.text = frmBillPaymentConfirmationFuture.lblAmtDisconnected.text + ":";
        frmBillPaymentComplete.lblAmtDisconnectedValue.text = frmBillPaymentConfirmationFuture.lblAmtDisconnectedValue.text;
        gblDisplayBalanceBillPayment = false;
    } else {
        frmBillPaymentComplete.hbox101271281131304.setVisibility(true);
        gblDisplayBalanceBillPayment = true;
    }
    frmBillPaymentComplete.imgHeaderMiddle.setVisibility(false);
    frmBillPaymentComplete.hbxShareOption.setVisibility(false);
    //frmBillPaymentComplete.hbxArrow.setVisibility(false);
    //added code for add space
    if (gblPaynow && !gblEasyPassTopUp && gblCompCode != "2533") {
        frmBillPaymentConfirmationFuture.hbxspace.setVisibility(true);
    } else {
        frmBillPaymentConfirmationFuture.hbxspace.setVisibility(false);
    }
    frmBillPaymentComplete.show();
    gblFirstTimeBillPayment = true;
    gblFirstTimeTopUp = true;
    gblBPScheduleFirstShow = true;
    gblScheduleEndBP = "none";
    gblPenalty = false;
    gblFullPayment = false;
    gblBillpaymentNoFee = false;
    dismissLoadingScreen();
}

function onclickOfSelectedSuggestedBillersAdd() {
    if (checkMaxBillerCountMB()) {
        var isBarCode = frmSelectBiller.segSuggestedBillers.selectedItems[0].BarcodeOnly;
        if (kony.string.equalsIgnoreCase(isBarCode, "1")) {
            alert("Cannot Add Biller - Only for Barcode");
        } else {
            frmAddTopUpBillerconfrmtn.segConfirmationList.removeAll();
            TMBUtil.DestroyForm(frmAddTopUpToMB);
            gblBillerIdMB = null;
            gblBillerMethod = null;
            gblRef2FlagMB = null;
            gblreccuringDisablePay = frmSelectBiller.segSuggestedBillers.selectedItems[0].IsRequiredRefNumber2Pay;
            gblreccuringDisableAdd = frmSelectBiller.segSuggestedBillers.selectedItems[0].IsRequiredRefNumber2Add;
            gblBillerIdMB = frmSelectBiller.segSuggestedBillers.selectedItems[0].BillerID;
            gblBillerMethod = frmSelectBiller.segSuggestedBillers.selectedItems[0].BillerMethod;
            gblPayFull = frmSelectBiller.segSuggestedBillers.selectedItems[0].IsFullPayment;
            gblBillerBancassurance = frmSelectBiller.segSuggestedBillers.selectedItems[0].billerBancassurance;
            gblAllowRef1AlphaNum = frmSelectBiller.segSuggestedBillers.selectedItems[0].allowRef1AlphaNum;
            gblbillerGroupType = frmSelectBiller.segSuggestedBillers.selectedItems[0].BillerGroupType;
            gblEffDtMB = frmSelectBiller.segSuggestedBillers.selectedItems[0].EffDt;
            gblCompCode = frmSelectBiller.segSuggestedBillers.selectedItems[0].BillerCompCode;
            frmAddTopUpToMB.lblAddedRef1.text = frmSelectBiller.segSuggestedBillers.selectedItems[0].Ref1Label + ":";
            gblRef1LenMB = frmSelectBiller.segSuggestedBillers.selectedItems[0].Ref1Len.text;
            gblBillerCategoryID = frmSelectBiller.segSuggestedBillers.selectedItems[0].BillerCategoryID.text;
            if (gblBillerBancassurance == "Y" || gblAllowRef1AlphaNum == "Y") {
                frmAddTopUpToMB.txtRef1.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
                frmAddTopUpToMB.txtRef1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT;
            } else {
                frmAddTopUpToMB.txtRef1.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
                frmAddTopUpToMB.txtRef1.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
            }
            frmAddTopUpToMB.txtRef1.maxTextLength = parseInt(gblRef1LenMB);
            //frmAddTopUpToMB.lblAddedRef1.text = kony.i18n.getLocalizedString('keyRef1');
            gblCurRef1LblEN = frmSelectBiller.segSuggestedBillers.selectedItems[0].hdnLabelRefNum1EN.text;
            gblCurRef1LblTH = frmSelectBiller.segSuggestedBillers.selectedItems[0].hdnLabelRefNum1TH.text;
            gblCurRef2LblEN = frmSelectBiller.segSuggestedBillers.selectedItems[0].hdnLabelRefNum2EN.text;
            gblCurRef2LblTH = frmSelectBiller.segSuggestedBillers.selectedItems[0].hdnLabelRefNum2TH.text;
            gblBillerCompCodeEN = frmSelectBiller.segSuggestedBillers.selectedItems[0].lblBillerNameEN;
            gblBillerCompCodeTH = frmSelectBiller.segSuggestedBillers.selectedItems[0].lblBillerNameTH;
            //ENh 113
            if (gblCompCode == "2533") {
                gblSegBillerDataMB = frmSelectBiller.segSuggestedBillers.selectedItems[0];
                gBillerStartTime = frmSelectBiller.segSuggestedBillers.selectedItems[0].billerStartTime;
                gBillerEndTime = frmSelectBiller.segSuggestedBillers.selectedItems[0].billerEndTime;
            }
            gblRef1LblEN = gblCurRef1LblEN;
            gblRef1LblTH = gblCurRef1LblTH;
            gblRef2LblEN = gblCurRef2LblEN;
            gblRef2LblTH = gblCurRef2LblTH;
            gblToAccountKey = frmSelectBiller.segSuggestedBillers.selectedItems[0].ToAccountKey;
            gblRef2LenMB = frmSelectBiller.segSuggestedBillers.selectedItems[0].Ref2Len.text;
            if (frmSelectBiller.segSuggestedBillers.selectedItems[0].IsRequiredRefNumber2Add != "Y") {
                frmAddTopUpToMB.lblAddedRef2.setVisibility(false);
                frmAddTopUpToMB.txtRef2.setVisibility(false);
                frmAddTopUpToMB.lineRef2.setVisibility(false);
                frmAddTopUpToMB.hbxref2.setVisibility(false);
                gblRef2FlagMB = false;
                //gblRef2LenMB = 0;
                gblIsRef2RequiredMB = "N";
            } else {
                frmAddTopUpToMB.lineRef2.setVisibility(true);
                frmAddTopUpToMB.hbxref2.setVisibility(true);
                frmAddTopUpToMB.lblAddedRef2.setVisibility(true);
                frmAddTopUpToMB.txtRef2.setVisibility(true);
                gblRef2FlagMB = true;
                gblIsRef2RequiredMB = "Y";
                frmAddTopUpToMB.lblAddedRef2.text = frmSelectBiller.segSuggestedBillers.selectedItems[0].Ref2Label + ":";
                //gblRef2LenMB = frmSelectBiller.segSuggestedBillers.selectedItems[0].Ref2Len.text;
                frmAddTopUpToMB.txtRef2.maxTextLength = parseInt(gblRef2LenMB);
            }
            var indexOfSelectedRow = frmSelectBiller.segSuggestedBillers.selectedIndex[1];
            frmAddTopUpToMB.imgAddedBiller.src = frmSelectBiller.segSuggestedBillers.selectedItems[0].imgSuggestedBiller.src;
            frmAddTopUpToMB.lblAddbillerName.text = frmSelectBiller.segSuggestedBillers.selectedItems[0].lblSuggestedBiller;
            frmAddTopUpToMB.show();
        }
    } else {
        alert(kony.i18n.getLocalizedString("Valid_MoreThan50"));
    }
}

function topupAmount() {
    var stepAmtTopupArray = [];
    if (mySelectBillerSuggestListMB != null) {
        for (var i = 0; i < mySelectBillerSuggestListMB.length; i++) {
            var billCompCode = mySelectBillerSuggestListMB[i]["BillerCompCode"];
            if (mySelectBillerSuggestListMB[i]["BillerCompCode"] != null && mySelectBillerSuggestListMB[i]["BillerCompCode"] != undefined && (mySelectBillerSuggestListMB[i]["BillerCompCode"]["text"] == gblCompCode.trim()) || (mySelectBillerSuggestListMB[i]["BillerCompCode"].toString().trim() == gblCompCode.trim())) {
                stepAmtTopupArray = mySelectBillerSuggestListMB[i]["StepAmount"];
                break;
            }
        }
    }
    if (stepAmtTopupArray.length == "0") {
        for (var i = 0; i < TempDataBillers.length; i++) {
            var billCompCode = TempDataBillers[i]["BillerCompCode"];
            if (TempDataBillers[i]["BillerCompCode"] != null && TempDataBillers[i]["BillerCompCode"] != undefined && (TempDataBillers[i]["BillerCompCode"]["text"] == gblCompCode.trim()) || (TempDataBillers[i]["BillerCompCode"].toString().trim() == gblCompCode.trim())) {
                stepAmtTopupArray = TempDataBillers[i]["StepAmount"];
                break;
            }
        }
    }
    var masterData = [];
    if (stepAmtTopupArray.length != 0) {
        for (i = 0; i < stepAmtTopupArray.length; i++) {
            var temp = {
                "lblAmount": commaFormatted(stepAmtTopupArray[i]["Amt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
            };
            masterData.push(temp);
        }
    }
    frmTopUpAmount.segPop.data = masterData;
    frmTopUpAmount.show();
}

function amountTopUpSelect() {
    //popUpMyBillers.show();
    var selected = frmTopUpAmount.segPop.selectedIndex[1];
    var data = frmTopUpAmount.segPop.data;
    var selAmount = data[selected].lblAmount;
    frmTopUp.btnTopUpAmountComboBox.text = selAmount;
    frmTopUp.show();
}
var billerTypeCheckMB = "";

function callBillerValidationBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay, scheduleDtBillPay, billerTypeMB) {
    inputParam = {};
    if (compCodeBillPay == "2533") {
        inputParam["BillerStartTime"] = gBillerStartTime;
        inputParam["BillerEndTime"] = gBillerEndTime;
    }
    inputParam["BillerCompcode"] = compCodeBillPay;
    inputParam["ReferenceNumber1"] = ref1ValBillPay;
    inputParam["ReferenceNumber2"] = ref2ValBillPay;
    inputParam["Amount"] = billAmountBillPay;
    amount = billAmountBillPay;
    inputParam["ScheduleDate"] = scheduleDtBillPay;
    inputParam["billerCategoryID"] = gblBillerCategoryID;
    inputParam["billerMethod"] = gblBillerMethod;
    inputParam["ModuleName"] = "BillPay";
    billerTypeCheckMB = billerTypeMB;
    AmountPaid = billAmountBillPay;
    invokeServiceSecureAsync("billerValidation", inputParam, callBillerValidationBillPayMBServiceCallBack);
}

function callBillerValidationBillPayMBServiceCallBack(status, result) {
    if (status == 400) //success responce
    {
        dismissLoadingScreen();
        var validationBillPayMBFlag = "";
        var isValidOnlineBiller = result["isValidOnlineBiller"];
        if (result["opstatus"] == 0) {
            validationBillPayMBFlag = result["validationResult"];
        } else {
            dismissLoadingScreen();
            validationBillPayMBFlag = result["validationResult"];
        }
        if (billerTypeCheckMB == "Biller") {
            if (validationBillPayMBFlag == "true") {
                if (gblBillerMethod == "0" || gblBillerMethod == "1") {
                    BillPaymentInquiryModule();
                } else {
                    frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    checkBillPaymentCrmProfileInqMB();
                }
            } else {
                dismissLoadingScreen();
                //ENH113
                if (result["isNotAllowedTime"] == "true") {
                    //alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
                    alert(result["errmsg"]);
                    return false;
                }
                //ENH113
                if (isValidOnlineBiller != undefined && isValidOnlineBiller == "true") {
                    alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
                } else {
                    var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef1Val");
                    wrongRefMsg = wrongRefMsg.replace("{ref_label}", removeColonFromEnd(frmBillPayment.lblRef1.text + ""));
                    showAlert(wrongRefMsg, kony.i18n.getLocalizedString("info"));
                }
                frmBillPayment.lblRef1Value.setFocus(true);
                return false;
            }
        }
        if (billerTypeCheckMB == "TopUp") {
            //validationBillPayMBFlag = "true";
            if (validationBillPayMBFlag == "true") {
                if (gblBillerMethod == "0" || gblBillerMethod == "1") {
                    BillPaymentInquiryModule();
                } else if (gblBillerMethod == "2") {
                    validateFleetCardTopUpMB();
                } else {
                    frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
                    checkBillPaymentCrmProfileInqMB();
                }
            } else {
                dismissLoadingScreen();
                if (isValidOnlineBiller != undefined && isValidOnlineBiller == "true") {
                    alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
                } else {
                    alert(kony.i18n.getLocalizedString("keyTopUpValidationFailed"));
                }
                return false;
            }
        }
    }
}

function validateChannelMBTopUp() {
    getTopUpBillerNickNameMB();
    if ((frmTopUp.lblTopUpName.text == null || frmTopUp.lblTopUpName.text == "" || frmTopUp.lblTopUpName.text == " ")) {
        alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
        return false;
    } else if (frmTopUp.tbxMyNoteValue.text.length > 50) {
        alert(kony.i18n.getLocalizedString("MaxlngthNoteError"));
        return false;
    } else if (!gblBillerPresentInMyBills && !gblPaynow) {
        var newNickName = frmTopUp.txtNickName.text;
        if (isNotBlank(newNickName)) {
            if (NickNameValid(newNickName)) {
                if (checkBillerNicknameInMyBills(gblMyBillList, newNickName)) {
                    //nickname exists alert
                    showAlert(kony.i18n.getLocalizedString("MIB_BPErr_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
            } else {
                showAlert(kony.i18n.getLocalizedString("MIB_BPkeyInvalidNickName"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            showAlert(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
    var inputParams = {
        billerCompcode: gblCompCode
    };
    showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, validateChannelMBTopUpCallBack);
}

function validateChannelMBTopUpCallBack(status, callBackResponse) {
    dismissLoadingScreen();
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                gblBillerCategoryID = responseData[0]["BillerCategoryID"];
                gblBillerId = responseData[0]["BillerID"];
                callValidationTopupMB();
            } else {
                if (flowSpa) alert(kony.i18n.getLocalizedString("keyNotValidForIB"));
                else alert(kony.i18n.getLocalizedString("keyNotValidForMB"));
            }
        } else {
            alert("No Suggested Biller found");
        }
    } else {
        if (status == 300) {
            alert("No Suggested Biller found");
        }
    }
}

function callValidationTopupMB() {
    if (flowSpa) {
        gblnormSelIndex = gbltranFromSelIndex[1];
        gblmbSpaselectedData = frmTopUp.segSliderSpa.data[gblnormSelIndex];
    }
    if (!isNotBlank(frmTopUp.lblRef1Value.text)) {
        var noRefMsg = kony.i18n.getLocalizedString("MIB_BPNoRef");
        noRefMsg = noRefMsg.replace("{ref_label}", frmTopUp.lblRef1.text + "");
        alert(noRefMsg);
        return;
    }
    if ((frmTopUp.lblTopUpName.text == null || frmTopUp.lblTopUpName.text == "" || frmTopUp.lblTopUpName.text == " ")) {
        alert(kony.i18n.getLocalizedString("KeyPlzSelBiller"));
    } else {
        if (frmTopUp.hbxTopUpamntComboBox.isVisible) {
            var comboAmount = parseFloat(removeCommos(frmTopUp.btnTopUpAmountComboBox.text)).toFixed(2);
            if (comboAmount == null || comboAmount == "" || comboAmount == " " || comboAmount == "0" || comboAmount == "0.0" || comboAmount == "0.00" || isNaN(comboAmount)) {
                alert(kony.i18n.getLocalizedString("KeyEnterVldAmtPopUP"));
                return;
            } else {
                var textnew = parseFloat(removeCommos(frmTopUp.btnTopUpAmountComboBox.text)).toFixed(2);
            }
        } else {
            onDoneEditingAmountTopUp();
            var amount = parseFloat(removeCommos(frmTopUp.tbxExcludingBillerOne.text)).toFixed(2);
            if (amount == null || amount == "" || amount == " " || amount == "0" || amount == "0.0" || amount == "0.00") {
                alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
                return;
            } else {
                var textnew = parseFloat(removeCommos(frmTopUp.tbxExcludingBillerOne.text)).toFixed(2);
            }
        }
        //MIB-4884-Allow special characters for My Note and Note to recipient field
        /*
        if(isNotBlank(frmTopUp.tbxMyNoteValue.text)){
        	if(!MyNoteValid(frmTopUp.tbxMyNoteValue.text)){
        		alert(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"));
        		frmTopUp.tbxMyNoteValue.setFocus(true);
        		return false;
        	}
        }
        */
        if (isNotBlank(frmTopUp.tbxMyNoteValue.text)) {
            if (checkSpecialCharMyNote(frmTopUp.tbxMyNoteValue.text)) {
                alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
                frmTopUp.tbxMyNoteValue.setFocus(true);
                return false;
            }
        }
        var regex = /^(?:\d*\.\d{1,2}|\d+)$/
        if (regex.test(textnew)) {
            if (gblBillerMethod != "1") {
                callBillerValidationTopupMB();
            } else {
                topupAmountAfter(gblBillerMethod);
            }
        } else {
            alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
            return;
        }
    }
}

function callBillerValidationTopupMB() {
    var text = frmTopUp.lblTopUpName.text;
    var compCodeBillPay = gblCompCode;
    var ref1ValBillPay = frmTopUp.lblRef1Value.text;
    var ref2ValBillPay = "";
    var billAmountBillPay; //= frmTopUp.tbxExcludingBillerOne.text;
    if (frmTopUp.hbxTopUpamntComboBox.isVisible) {
        billAmountBillPay = parseFloat(removeCommos(frmTopUp.btnTopUpAmountComboBox.text)).toFixed(2);
    } else {
        billAmountBillPay = parseFloat(removeCommos(frmTopUp.tbxExcludingBillerOne.text)).toFixed(2);
    }
    var scheduleDtBillPay = frmTopUp.lblPayBillOnValue.text;
    var billerTypeMB = "TopUp";
    //if (flowSpa) {
    //	var amountWithComma = gblmbSpaselectedData.lblBalance;
    //} else {
    var amountWithComma = frmTopUp.segSlider.selectedItems[0].lblBalance;
    //}
    var amount = amountWithComma;
    if (amountWithComma != null && amountWithComma != undefined) {
        amount = amountWithComma.replace(/,/g, "");
        amount = parseFloat(amount);
    }
    if (amount < billAmountBillPay && gblPaynow) {
        alert(kony.i18n.getLocalizedString("MIB_BPLessAvailBal"));
        dismissLoadingScreen();
        return;
    }
    callBillerValidationBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay, scheduleDtBillPay, billerTypeMB)
        //BillPaymentInquiryModule();
}

function createSegmentRecordBills(record, hbxSliderindex, icon) {
    var remFeeValue;
    var remFee;
    var accountName = record["acctNickName"];
    var accountNickNameEN = record["acctNickNameEN"];
    var accountNickNameTH = record["acctNickNameTH"];
    if (accountName == "" || accountName == undefined) {
        var acctnum = record.accId;
        var acctnumNew = acctnum.substring(acctnum.length - 4, acctnum.length);
        var accNewNameEN = isNotBlank(record["productNmeEN"]) ? record["productNmeEN"] : "";
        var accNewNameTH = isNotBlank(record["productNmeTH"]) ? record["productNmeTH"] : "";
        if (accNewNameEN.length > 15) {
            accNewNameEN = accNewNameEN.substring(0, 15);
        }
        if (accNewNameTH.length > 15) {
            accNewNameTH = accNewNameTH.substring(0, 15);
        }
        if (kony.i18n.getCurrentLocale() == "en_US") {
            accountName = accNewNameEN + " " + acctnumNew;
        } else {
            accountName = accNewNameTH + " " + acctnumNew;
        }
        accountNickNameEN = accNewNameEN + " " + acctnumNew;
        accountNickNameTH = accNewNameTH + " " + acctnumNew;
    }
    if (gblSMART_FREE_TRANS_CODES.indexOf(record.productID) >= 0) {
        remFeeValue = record.remainingFee;
        remFee = kony.i18n.getLocalizedString("keylblNoOfFreeTrans");
    } else {
        remFeeValue = " ";
        remFee = " "
    }
    var accountNo = record.accId;
    if (accountNo.length == 14) {
        accountNo = accountNo.substring(4, accountNo.length);
    }
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        var temp = [{
            lblACno: kony.i18n.getLocalizedString("AccountNo") + " ",
            img1: icon,
            lblCustName: accountName,
            lblAccountNickNameEN: accountNickNameEN,
            lblAccountNickNameTH: accountNickNameTH,
            lblBalance: commaFormatted(record.availableBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblActNoval: addHyphenMB(accountNo),
            lblSliderAccN1: kony.i18n.getLocalizedString("Product") + " ",
            lblSliderAccN2: isNotBlank(record.productNmeEN) ? record.productNmeEN : "",
            lblDummy: " ",
            tdFlag: record.accType,
            fromFIIdent: record.fiident,
            lblRemainFee: remFee,
            lblRemainFeeValue: remFeeValue,
            prodCode: record.productID,
            custAcctName: record.accountName,
            template: hbxSliderindex,
            accountNo: record.accId
        }]
    } else {
        var temp = [{
            lblACno: kony.i18n.getLocalizedString("AccountNo") + " ",
            img1: icon,
            lblCustName: accountName,
            lblAccountNickNameEN: accountNickNameEN,
            lblAccountNickNameTH: accountNickNameTH,
            lblBalance: commaFormatted(record.availableBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblActNoval: addHyphenMB(accountNo),
            lblSliderAccN1: kony.i18n.getLocalizedString("Product") + " ",
            lblSliderAccN2: isNotBlank(record.productNmeTH) ? record.productNmeTH : "",
            lblDummy: " ",
            tdFlag: record.accType,
            fromFIIdent: record.fiident,
            lblRemainFee: remFee,
            lblRemainFeeValue: remFeeValue,
            prodCode: record.productID,
            custAcctName: record.accountName,
            template: hbxSliderindex,
            accountNo: record.accId
        }]
    }
    return temp;
}

function savePDFBillPaymentIB(filetype) {
    var inputParam = {};
    inputParam["templatename"] = "ExecutedBillPaymentTemplate";
    inputParam["filetype"] = filetype;
    var accNo = frmIBBillPaymentCompletenow.lblAccountNo.text.split("-")[2];
    accNo = "XXX-X-" + accNo + "-X";
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "AccountNo": accNo,
        "AccountName": frmIBBillPaymentCompletenow.lblFromName.text,
        "BillerName": frmIBBillPaymentCompletenow.lblePayNickname.text,
        "Ref1Label": frmIBBillPaymentCompletenow.lblRef1.text,
        "Ref1Value": frmIBBillPaymentCompletenow.lblRef1Value.text,
        "Amount": frmIBBillPaymentCompletenow.lblAmtVal.text,
        "Fee": frmIBBillPaymentCompletenow.lblFeeVal.text,
        "PaymentOrderDate": frmIBBillPaymentCompletenow.lblePayDate.text,
        "MyNote": frmIBBillPaymentCompletenow.lblMNVal.text,
        "TransactionRefNo": frmIBBillPaymentCompletenow.lblTRNVal.text
    };
    if (frmIBBillPaymentCompletenow.hbxref2.isVisible) {
        pdfImagedata["Ref2Label"] = frmIBBillPaymentCompletenow.lblref2.text;
        pdfImagedata["Ref2Value"] = frmIBBillPaymentCompletenow.lblRef2Value.text;
    }
    if (gblPaynow) {
        //do nothing
        inputParam["outputtemplatename"] = "Bill_Payment_Details_" + kony.os.date("DDMMYYYY");
    } else {
        pdfImagedata["BillPaySchedule"] = "Payment Schedule";
        inputParam["outputtemplatename"] = "Future_Bill_Payment_Set_Details_" + kony.os.date("DDMMYYYY");
        if (frmIBBillPaymentCompletenow.lblEnDONValue.text == "-") {
            pdfImagedata["PaymentSchedule"] = frmIBBillPaymentCompletenow.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBBillPaymentCompletenow.lblRepeatValue.text;
        } else {
            pdfImagedata["PaymentSchedule"] = frmIBBillPaymentCompletenow.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBBillPaymentCompletenow.lblEnDONValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBBillPaymentCompletenow.lblRepeatValue.text + " for " + frmIBBillPaymentCompletenow.lblExecuteValue.text;
        }
    }
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    if (gblPaynow) {
        savePDFIB(filetype, "027", frmIBBillPaymentCompletenow.lblTRNVal.text);
    } else {
        saveFuturePDF(filetype, "028", frmIBBillPaymentCompletenow.lblTRNVal.text);
        //invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    }
}

function savePDFTopUpPaymentIB(filetype) {
    var inputParam = {}
    inputParam["templatename"] = "ExecutedTopUpPaymentComplete";
    inputParam["filetype"] = filetype;
    var accNo = frmIBTopUpComplete.lblAccountNo.text.split("-")[2];
    accNo = "XXX-X-" + accNo + "-X";
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "AccountNo": accNo,
        "AccountName": frmIBTopUpComplete.lblName.text,
        "BillerName": frmIBTopUpComplete.lblCompCode.text,
        "Ref1Label": frmIBTopUpComplete.lblref1.text,
        "Ref1Value": frmIBTopUpComplete.lblRef1Value.text,
        "Amount": frmIBTopUpComplete.lblAmtVal.text,
        "Fee": frmIBTopUpComplete.lblFeeVal.text,
        "PaymentOrderDate": frmIBTopUpComplete.lblePayDate.text,
        "MyNote": frmIBTopUpComplete.lblMNVal.text,
        "TransactionRefNo": frmIBTopUpComplete.lblTRNVal.text
    };
    if (gblPaynow) {
        //do nothing
        inputParam["outputtemplatename"] = "Top-Up_Payment_Details_" + kony.os.date("DDMMYYYY");
    } else {
        inputParam["outputtemplatename"] = "Future_Top-Up_Payment_Set_Details_" + kony.os.date("DDMMYYYY");
        pdfImagedata["TopUpSchedule"] = "Top-Up Schedule";
        if (frmIBTopUpComplete.lblEnDONValue.text == "-") {
            pdfImagedata["PaymentSchedule"] = frmIBTopUpComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBTopUpComplete.lblRepeatValue.text;
        } else {
            pdfImagedata["PaymentSchedule"] = frmIBTopUpComplete.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBTopUpComplete.lblEnDONValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBTopUpComplete.lblRepeatValue.text + " for " + frmIBTopUpComplete.lblExecuteValue.text + " " + kony.i18n.getLocalizedString("keyTimesMB");
        }
    }
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    if (gblPaynow) {
        savePDFIB(filetype, "030", frmIBTopUpComplete.lblTRNVal.text);
    } else {
        saveFuturePDF(filetype, "031", frmIBTopUpComplete.lblTRNVal.text);
        //invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    }
}

function savePDFIB(filetype, activityTypeId, TransactionRefNo) {
    var inputParam = {};
    inputParam["filetype"] = filetype;
    inputParam["activityTypeId"] = activityTypeId;
    inputParam["TransactionRefNo"] = TransactionRefNo;
    //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "android" || flowSpa == true) {
        invokeServiceSecureAsync("generateImagePdf", inputParam, mbRenderFileCallbackfunction);
    } else {
        invokeServiceSecureAsync("generateImagePdf", inputParam, ibRenderFileCallbackfunction);
    }
}

function saveFuturePDF(filetype, activityTypeId, TransactionRefNo) {
    var inputParam = {};
    inputParam["filetype"] = filetype;
    inputParam["activityTypeId"] = activityTypeId;
    inputParam["TransactionRefNo"] = TransactionRefNo;
    inputParam["futurePDF"] = "true";
    //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "android" || flowSpa == true) {
        invokeServiceSecureAsync("generateImagePdf", inputParam, mbRenderFileCallbackfunction);
    } else {
        invokeServiceSecureAsync("generateImagePdf", inputParam, ibRenderFileCallbackfunction);
    }
}

function saveOpenAccountPDFIB(filetype, activityTypeId, TransactionRefNo, openAcctType) {
    var inputParam = {};
    inputParam["filetype"] = filetype;
    inputParam["activityTypeId"] = activityTypeId;
    inputParam["TransactionRefNo"] = TransactionRefNo;
    inputParam["openAcctType"] = openAcctType;
    if (openAcctType == "DS") {
        inputParam["futurePDF"] = "true";
    }
    //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "android" || flowSpa == true) {
        invokeServiceSecureAsync("generateImagePdf", inputParam, mbRenderFileCallbackfunction);
    } else {
        invokeServiceSecureAsync("generateImagePdf", inputParam, ibRenderFileCallbackfunction);
    }
}

function onBillPaymentConfirm() {
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    var refNo = "";
    var mobNO = "";
    showLoadingScreen();
    showOTPPopup(lblText, refNo, mobNO, billPaymentConfirmation, 3);
}

function billPaymentConfirmation(tranPassword) {
    if (tranPassword == null || tranPassword == '') {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    } else {
        showLoadingScreen();
        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
        popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
        popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
        //verifyPasswordBillPayment(tranPassword);
        if (GblBillTopFlag) {
            onClickConfirmBillPaymentMB(tranPassword);
        } else {
            verifyPasswordBillPayment(tranPassword);
        }
    }
}
/*
 * Composite service call
 */
function onClickConfirmBillPaymentMB(pwd) {
    var inputParam = {};
    showLoadingScreen();
    inputParam["channel"] = "rc";
    inputParam["verifyPwdMB_appID"] = appConfig.appId;
    inputParam["verifyPwdMB_channel"] = "MB";
    inputParam["password"] = pwd;
    inputParam["verifyPwdMB_appID"] = appConfig.appId;
    inputParam["billerMethod"] = gblBillerMethod;
    inputParam["isPayNow"] = gblPaynow;
    inputParam["compCode"] = gblCompCode;
    inputParam["isPayPremium"] = gblPayPremium;
    inputParam["policyNumber"] = gblPolicyNumber;
    var transferFee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
    var transferAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)).toFixed(2); //+parseFloat(transferFee)
    //MIB-4884-Allow special characters for My Note and Note to recipient field
    var myNote = frmBillPayment.tbxMyNoteValue.text;
    if (gblPaynow == true) {
        inputParam["NotificationAdd_source"] = "billpaymentnow";
        var frmFiident;
        var selectedItem = frmBillPayment.segSlider.selectedIndex[1];
        var selectedData = frmBillPayment.segSlider.data[selectedItem];
        frmFiident = selectedData.fromFIIdent;
        if (gblBillerMethod == 0) {
            var invoice;
            if (gblreccuringDisablePay == "Y") {
                invoice = frmBillPaymentConfirmationFuture.lblRef2Value.text; //no ref 2 for topup?
            } else {
                invoice = ""; //workaround
            }
            inputParam["billpaymentAdd_fromAcctNo"] = frmBillPaymentConfirmationFuture.lblAccountNum.text;
            inputParam["billpaymentAdd_toAcctNo"] = gblToAccountKey;
            inputParam["billpaymentAdd_frmFiident"] = frmFiident; //frmBillPayment.segSlider.data[selectedItem].fromFIIdent; 
            inputParam["billpaymentAdd_transferAmt"] = transferAmount;
            inputParam["billpaymentAdd_billPmtFee"] = parseFloat(transferFee).toFixed(2);
            inputParam["billpaymentAdd_pmtRefIdent"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
            inputParam["billpaymentAdd_invoiceNum"] = invoice;
            inputParam["billpaymentAdd_EPAYCode"] = "";
            inputParam["billpaymentAdd_CompCode"] = gblCompCode;
            inputParam["billpaymentAdd_billPay"] = "billPay";
            inputParam["UsageLimit"] = "0";
            inputParam["billpaymentAdd_appID"] = appConfig.appId;
            inputParam["billpaymentAdd_channel"] = "MB";
        }
        if (gblBillerMethod == 1) {
            var ref1 = "";
            var ref2 = "";
            var ref3 = "";
            var ref4 = "";
            var bankrefID = "";
            if (gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185") {
                bankrefID = BankRefId;
            } else if (gblCompCode == "0014" || gblCompCode == "0249" || gblCompCode == "2270") {
                bankrefID = TranId;
            } else {
                bankrefID = "";
            }
            if (gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185") {
                ref1 = gblRef1;
                ref2 = gblRef2;
                ref3 = gblRef3;
                ref4 = gblRef4;
            } else if (gblCompCode == "2533") {
                ref3 = gblSegBillerDataMB["billerFeeAmount"];
            } else {
                ref1 = "";
                ref2 = "";
                ref3 = "";
                ref4 = "";
            }
            inputParam["onlinepaymentAdd_TrnId"] = TranId;
            inputParam["onlinepaymentAdd_BankId"] = "011";
            inputParam["onlinepaymentAdd_BranchId"] = "0001";
            inputParam["onlinepaymentAdd_BankRefId"] = bankrefID;
            inputParam["onlinepaymentAdd_Amt"] = transferAmount;
            inputParam["onlinepaymentAdd_Ref1"] = ref1; //frmIBBillPaymentLP.lblBPRef1Val.text;
            inputParam["onlinepaymentAdd_Ref2"] = ref2; //frmIBBillPaymentLP.lblRefVal2.text;
            inputParam["onlinepaymentAdd_Ref3"] = ref3;
            inputParam["onlinepaymentAdd_Ref4"] = ref4;
            inputParam["onlinepaymentAdd_MobileNumber"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
            inputParam["onlinepaymentAdd_fromAcctNo"] = frmBillPaymentConfirmationFuture.lblAccountNum.text;
            inputParam["onlinepaymentAdd_toAcctNo"] = gblToAccountKey;
            inputParam["onlinepaymentAdd_frmFiident"] = frmFiident; //frmBillPayment.segSlider.selectedItems[0].fromFIIdent;
            inputParam["onlinepaymentAdd_transferAmt"] = transferAmount;
            inputParam["onlinepaymentAdd_channelName"] = "MB";
            inputParam["onlinepaymentAdd_billPmtFee"] = parseFloat(transferFee).toFixed(2); //.substring(0, transferFee.indexOf(" "));
            inputParam["onlinepaymentAdd_pmtRefIdent"] = frmBillPayment.lblRef1Value.text;
            var invoice;
            if (gblCompCode == "0835") {
                invoice = "01" + TranId;
            } else if (gblCompCode == "2136" || gblCompCode == "0472") {
                invoice = TranId;
            } else if (gblCompCode == "0185" || gblCompCode == "0254" || gblCompCode == "0328" || gblCompCode == "2135" || gblCompCode == "0131") {
                invoice = gblRef2;
            } else if (gblCompCode == "2151") {
                invoice = BankRefId;
            } else if (gblCompCode == "2533") {
                invoice = TranId + gblSegBillerDataMB["billerServiceType"] + gblOnlinePmtType + gblSegBillerDataMB["billerTransType"];
                inputParam["onlinepaymentAdd_Ref3"] = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text));
                inputParam["onlinepaymentAdd_BankRefId"] = frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
            } else {
                if (gblreccuringDisablePay == "Y") {
                    invoice = frmBillPaymentConfirmationFuture.lblRef2Value.text;
                } else {
                    invoice = ""; //workaround
                }
            }
            inputParam["onlinepaymentAdd_invoiceNum"] = invoice;
            inputParam["onlinepaymentAdd_EPAYCode"] = ""; //"EPYS";
            inputParam["onlinepaymentAdd_compCode"] = gblCompCode;
            inputParam["onlinepaymentAdd_InterRegionFee"] = "0.00";
            inputParam["onlinepaymentAdd_billPay"] = "billPay";
            inputParam["onlinepaymentAdd_appID"] = appConfig.appId;
            inputParam["onlinepaymentAdd_channel"] = "MB";
        }
        if (gblBillerMethod == 2 || gblBillerMethod == 3 || gblBillerMethod == 4) {
            var toAcctNo = removeHyphenIB(frmBillPaymentConfirmationFuture.lblRef1Value.text);
            var fromAcctNo = frmBillPaymentConfirmationFuture.lblAccountNum.text;
            var transferAmt = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
            fromAcctNo = removeHyphenIB(fromAcctNo);
            var fromAccType;
            if (fromAcctNo.length == 14) {
                fromAccType = "SDA";
            } else {
                fourthDigit = fromAcctNo.charAt(3);
                if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
                    fromAccType = "SDA";
                    fromAcctNo = "0000" + fromAcctNo;
                } else {
                    fromAccType = "DDA";
                }
            }
            var fee = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text))
            inputParam["transferAdd_xferFee"] = fee;
            inputParam["transferAdd_fromAcctNo"] = frmBillPaymentConfirmationFuture.lblAccountNum.text;
            inputParam["transferAdd_fromAcctTypeValue"] = fromAccType;
            inputParam["transferAdd_toAcctNo"] = toAcctNo;
            inputParam["transferAdd_transferAmt"] = transferAmt.toFixed(2); //parseFloat(removeCommos(transferAmt));
            inputParam["transferAdd_orgin"] = "billpay";
            inputParam["transferAdd_billerMethod"] = gblBillerMethod;
            inputParam["transferAdd_ref2"] = frmBillPaymentConfirmationFuture.lblRef2Value.text;
            inputParam["transferAdd_appID"] = appConfig.appId;
            inputParam["transferAdd_channel"] = "MB";
            inputParam["transferAdd_CompCode"] = gblCompCode;
        }
    } else {
        inputParam["NotificationAdd_source"] = "FutureBillPaymentAndTopUp";
        var amount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
        var dateOrTimes = frmBillPaymentConfirmationFuture.lblHiddenVariableForScheduleEnd.text;
        var tranId = frmBillPaymentConfirmationFuture.lblTxnNumValue.text
        var pmtMethod = "BILL_PAYMENT";
        var fromAcctID;
        var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text;
        for (var i = 0; i < frmAcct.length; i++) {
            if (frmAcct[i] != "-") {
                if (fromAcctID == null) {
                    fromAcctID = frmAcct[i];
                } else {
                    fromAcctID = fromAcctID + frmAcct[i];
                }
            }
        }
        if (fromAcctID.length == 14) {
            fromAcctType = "SDA";
            fourthDigit = fromAcctID.charAt(7);
        } else {
            fourthDigit = fromAcctID.charAt(3);
            if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
                fromAcctType = "SDA";
                fromAcctID = "0000" + fromAcctID;
            } else {
                fromAcctType = "DDA";
            }
        }
        if (gblreccuringDisablePay == "Y") {
            inputParam["doPmtAdd_pmtRefNo2"] = frmBillPaymentConfirmationFuture.lblRef2Value.text;
        }
        if (dateOrTimes == "2") {
            inputParam["doPmtAdd_NumInsts"] = frmBillPaymentConfirmationFuture.lblExecuteValue.text.split(" ")[0].trim();
        } else if (dateOrTimes == "3") {
            inputParam["doPmtAdd_FinalDueDt"] = changeDateFormatForService(getFormattedDate(frmBillPaymentConfirmationFuture.lblEndOnValue.text, "en_US"));
        } else if (dateOrTimes == "1") {
            //inputParam["NumInsts"] = "99";
        }
        if (gblBillerMethod == 1) {
            pmtMethod = "ONLINE_PAYMENT";
            inputParam["doPmtAdd_PmtMiscType"] = "MOBILE";
            inputParam["doPmtAdd_MiscText"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
        } else if (gblBillerMethod == 2 || gblBillerMethod == 3 || gblBillerMethod == 4) {
            pmtMethod = "INTERNAL_PAYMENT"
        }
        inputParam["doPmtAdd_addBillerNickname"] = frmBillPayment.tbxBillerNickName.text;
        inputParam["doPmtAdd_rqUID"] = "";
        inputParam["doPmtAdd_toBankId"] = "011";
        inputParam["doPmtAdd_amt"] = amount.toFixed(2);
        inputParam["doPmtAdd_billerMethod"] = gblBillerMethod;
        inputParam["doPmtAdd_fromAcct"] = frmBillPaymentConfirmationFuture.lblAccountNum.text;
        inputParam["doPmtAdd_fromAcctType"] = fromAcctType;
        inputParam["doPmtAdd_toAcct"] = gblToAccountKey;
        inputParam["doPmtAdd_custPayeeId"] = gblBillerID;
        inputParam["doPmtAdd_dueDate"] = changeDateFormatForService(getFormattedDate(frmBillPaymentConfirmationFuture.lblStartOnValue.text, "en_US"));
        inputParam["doPmtAdd_pmtRefNo1"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
        inputParam["doPmtAdd_channelId"] = "MB";
        inputParam["doPmtAdd_curCode"] = "THB";
        inputParam["doPmtAdd_memo"] = myNote;
        inputParam["doPmtAdd_pmtMethod"] = pmtMethod;
        inputParam["doPmtAdd_extTrnRefId"] = "SB" + kony.string.sub(tranId, 2, tranId.length);
        inputParam["doPmtAdd_appID"] = appConfig.appId;
        inputParam["doPmtAdd_toNickName"] = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
        inputParam["doPmtAdd_channel"] = "MB";
        var frequency = frmBillPaymentConfirmationFuture.lblEveryMonth.text;
        inputParam["doPmtAdd_freq"] = frequency;
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDaily"))) {
            inputParam["doPmtAdd_freq"] = "Daily";
            //Fixed MIB-1396 Notify incorrect
            inputParam["NotificationAdd_recurring"] = "keyDaily";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
            inputParam["doPmtAdd_freq"] = "Weekly";
            inputParam["NotificationAdd_recurring"] = "keyWeekly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
            inputParam["doPmtAdd_freq"] = "Monthly";
            inputParam["NotificationAdd_recurring"] = "keyMonthly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly")) || kony.string.equalsIgnoreCase(frequency, "Yearly")) {
            inputParam["doPmtAdd_freq"] = "Annually";
            inputParam["NotificationAdd_recurring"] = "keyYearly";
        }
        if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce")) || kony.string.equalsIgnoreCase(frequency, "Once")) {
            inputParam["doPmtAdd_freq"] = "once";
            inputParam["NotificationAdd_recurring"] = "keyOnce";
        }
        inputParam["doPmtAdd_desc"] = "";
    }
    var platformChannel = gblDeviceInfo.name;
    if (platformChannel == "thinclient") inputParam["NotificationAdd_channelId"] = "Internet Banking";
    else inputParam["NotificationAdd_channelId"] = "Mobile Banking";
    inputParam["NotificationAdd_customerName"] = gblCustomerName;;
    inputParam["NotificationAdd_fromAcctNick"] = frmBillPaymentConfirmationFuture.lblAccountName.text;
    inputParam["NotificationAdd_fromAcctName"] = frmBillPaymentConfirmationFuture.lblAccUserName.text;
    inputParam["NotificationAdd_billerNick"] = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
    inputParam["NotificationAdd_billerName"] = gblBillerCompCodeEN;
    inputParam["NotificationAdd_billerNameTH"] = gblBillerCompCodeTH;
    inputParam["NotificationAdd_ref1EN"] = gblRef1LblEN + " : " + frmBillPaymentConfirmationFuture.lblRef1Value.text;
    inputParam["NotificationAdd_ref1TH"] = gblRef1LblTH + " : " + frmBillPaymentConfirmationFuture.lblRef1Value.text;
    if (frmBillPaymentConfirmationFuture.hbxRef2.isVisible) {
        ref2 = frmBillPaymentConfirmationFuture.lblRef2Value.text;
        inputParam["NotificationAdd_ref2EN"] = gblRef2LblEN + " : " + ref2;
        inputParam["NotificationAdd_ref2TH"] = gblRef2LblTH + " : " + ref2;
    } else {
        inputParam["NotificationAdd_ref2EN"] = "";
        inputParam["NotificationAdd_ref2TH"] = "";
    }
    inputParam["NotificationAdd_amount"] = frmBillPaymentConfirmationFuture.lblAmountValue.text;
    inputParam["NotificationAdd_fee"] = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
    inputParam["NotificationAdd_refID"] = frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
    inputParam["NotificationAdd_memo"] = myNote;
    inputParam["NotificationAdd_deliveryMethod"] = "Email";
    inputParam["NotificationAdd_noSendInd"] = "0";
    inputParam["NotificationAdd_notificationType"] = "Email";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    inputParam["NotificationAdd_notificationSubject"] = "billpay";
    inputParam["NotificationAdd_notificationContent"] = "billPaymentDone";
    inputParam["NotificationAdd_appID"] = appConfig.appId;
    inputParam["NotificationAdd_channel"] = "MB";
    inputParam["crmProfileMod_appID"] = appConfig.appId;
    inputParam["crmProfileMod_channel"] = "MB";
    if (gblPaynow) {
        inputParam["source"] = "billpaymentnow";
    } else {
        inputParam["NotificationAdd_source"] = "FutureBillPaymentAndTopUp";
        inputParam["NotificationAdd_startDt"] = frmBillPaymentConfirmationFuture.lblStartOnValue.text;
        inputParam["NotificationAdd_initiationDt"] = frmBillPaymentConfirmationFuture.lblPaymentDateValue.text;
        inputParam["NotificationAdd_todayTime"] = "23:59:59";
        //Fixed MIB-1396 Notify incorrect
        //inputParam["NotificationAdd_recurring"] = frmBillPaymentConfirmationFuture.lblEveryMonth.text 
        inputParam["NotificationAdd_endDt"] = frmBillPaymentConfirmationFuture.lblEndOnValue.text
        if (frmBillPaymentConfirmationFuture.lblEndOnValue.text == "-") {
            inputParam["NotificationAdd_PaymentSchedule"] = frmBillPaymentConfirmationFuture.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentConfirmationFuture.lblEveryMonth.text;
        } else {
            inputParam["NotificationAdd_PaymentSchedule"] = frmBillPaymentConfirmationFuture.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmBillPaymentConfirmationFuture.lblEndOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmBillPaymentConfirmationFuture.lblEveryMonth.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmBillPaymentConfirmationFuture.lblExecuteValue.text;
            inputParam["NotificationAdd_endDt"] = inputParam["NotificationAdd_endDt"] + " - " + frmBillPaymentConfirmationFuture.lblExecuteValue.text;
        }
        //inputParams["mynote"] = frmIBBillPaymentConfirm.lblMyNoteVal.text
    }
    var availableBal = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text));
    var billerName = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text;
    billerName = billerName.substring(0, billerName.lastIndexOf("("));
    inputParam["finActivityLog_billerRef1"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
    inputParam["finActivityLog_fromAcctName"] = frmBillPaymentConfirmationFuture.lblAccUserName.text;
    inputParam["finActivityLog_fromAcctNickname"] = frmBillPaymentConfirmationFuture.lblAccountName.text;
    inputParam["finActivityLog_toAcctName"] = billerName; //frmBillPaymentConfirmationFuture.lblBPBillerName.text;
    inputParam["finActivityLog_toAcctNickname"] = frmBillPaymentConfirmationFuture.lblBillerNickname.text
    inputParam["finActivityLog_billerRef2"] = frmBillPaymentConfirmationFuture.lblRef2Value.text;
    if (gblCompCode == "2533") {
        inputParam["finActivityLog_finFlexValues1"] = frmBillPaymentConfirmationFuture.lblCustNameValue.text;
        inputParam["finActivityLog_finFlexValues2"] = frmBillPaymentConfirmationFuture.lblMeterNumValue.text;
        inputParam["finActivityLog_finFlexValues3"] = frmBillPaymentConfirmationFuture.lblCustAddressValue.text;
    }
    inputParam["finActivityLog_availableBal"] = availableBal.toFixed(2);
    inputParam["finActivityLog_finTxnRefId"] = frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
    inputParam["finActivityLog_appID"] = appConfig.appId;
    inputParam["finActivityLog_channel"] = "MB";
    inputParam["finActivityLog_channelId"] = "02";
    inputParam["activityLog_appID"] = appConfig.appId;
    inputParam["activityLog_channel"] = "MB";
    inputParam["activityLog_channelId"] = "02";
    var activityFlexValues5 = "";
    if (gblreccuringDisablePay == "Y") {
        activityFlexValues5 = frmBillPaymentConfirmationFuture.lblRef2Value.text;
    } else {
        activityFlexValues5 = "";
    }
    var activityFlexValues1 = billerName;
    var activityFlexValues2 = frmBillPaymentConfirmationFuture.lblAccountNum.text;
    var activityFlexValues3 = frmBillPaymentConfirmationFuture.lblRef1Value.text;
    var amt = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text));
    var fee = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text));
    var activityFlexValues4 = amt.toFixed(2) + "+" + fee.toFixed(2);
    inputParam["activityLog_channelId"] = "02";
    inputParam["activityLog_activityFlexValues1"] = activityFlexValues1;
    inputParam["activityLog_activityFlexValues2"] = activityFlexValues2;
    inputParam["activityLog_activityFlexValues3"] = activityFlexValues3;
    inputParam["activityLog_activityFlexValues4"] = activityFlexValues4;
    inputParam["activityLog_activityFlexValues5"] = activityFlexValues5;
    inputParam["serviceID"] = "ConfirmBillPaymentService";
    inputParam["finActivityLog_billerCustomerName"] = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
    invokeServiceSecureAsync("ConfirmBillPaymentService", inputParam, callBackMBCompositeBillPay);
}

function callBackMBCompositeBillPay(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            popupTractPwd.dismiss();
            dismissLoadingScreen();
            gblRetryCountRequestOTP = 0;
            if (gblPaynow) {
                gotoBillPaymentCompleteMB(resulttable);
            } else {
                gblCurrentBillPayTime = resulttable["currentTime"];
                gotoBillPaymentCompleteMB(resulttable);
            }
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMessage"]);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                alert("" + resulttable["errMsg"]);
                return false;
            } else {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            popupTractPwd.dismiss();
            if (resulttable["opstatus"] == 1 && resulttable["errCode"] == "ERRDUPTR0001") {
                showAlertWithHandler(kony.i18n.getLocalizedString("keyErrDuplicatemt"), kony.i18n.getLocalizedString("info"), billpaymentExecutionErrCallBackMB);
                return;
            }
            var errorMsgTop = resulttable["errMsg"];
            if (resulttable["errMsg"] == null || resulttable["errMsg"] == undefined || resulttable["errMsg"] == "") {
                errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
            }
            alert("" + errorMsgTop);
            if (resulttable["isServiceFailed"] == "true") {
                gblMyBillerTopUpBB = 0;
                callBillPaymentCustomerAccountService();
            }
        }
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            popupTractPwd.dismiss();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}

function billpaymentExecutionErrCallBackMB() {
    gblMyBillerTopUpBB = 0;
    callBillPaymentCustomerAccountService();
}

function verifyPasswordBillPayment(pwd) {
    var inputParam = {}
    inputParam["loginModuleId"] = "MB_TxPwd";
    inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["password"] = pwd;
    if (!GblBillTopFlag) {
        svcModMBTopUpPaymentConfirm_verifyBillPaymentTopUp("MB_TxPwd", gblRtyCtrVrfyAxPin, gblRtyCtrVrfyTxPin, pwd);
    }
}

function clearBillpaymentScreen() {
    frmBillPayment.tbxRef2Value.text = "";
    frmBillPayment.tbxMyNoteValue.text = "";
    frmBillPayment.tbxAmount.text = "";
    frmBillPayment.tbxAmount.placeholder = "0.00";
    gblPaynow = true;
    gblBillPayFromScan = false;
}

function topupAmountAfter(billerMethod) {
    //popBankList.lblFlag.text = "TopUpType1";
    showLoadingScreen();
    var inputParam = {};
    if (gblCompCode == "2605") {
        var amount = parseFloat(frmTopUp.tbxExcludingBillerOne.text).toFixed(2);
    } else {
        var amount = parseFloat(removeCommos(frmTopUp.btnTopUpAmountComboBox.text)).toFixed(2);
    }
    inputParam["TrnId"] = "";
    inputParam["BankRefId"] = "";
    inputParam["BankId"] = "011";
    inputParam["BranchId"] = "0001";
    inputParam["Ref1"] = frmTopUp.lblRef1Value.text;
    inputParam["Ref2"] = ""; //frmBillPayment.lblRef2.text;
    inputParam["Ref3"] = "";
    inputParam["Ref4"] = "";
    inputParam["Amt"] = amount; //minimum amount
    inputParam["MobileNumber"] = frmTopUp.lblRef1Value.text;
    inputParam["compCode"] = gblCompCode;
    inputParam["isChannel"] = "MB";
    inputParam["commandType"] = "Inquiry";
    inputParam["BillerGroupType"] = "1";
    invokeServiceSecureAsync("onlinePaymentInq", inputParam, topupAmountServiceAsyncCallbackAfterAmount);
}

function topupAmountServiceAsyncCallbackAfterAmount(status, result) {
    if (status == 400) //success responce
    {
        TranId = ""; //remove comments after testing
        gblRef1 = "";
        gblRef2 = "";
        gblRef3 = "";
        gblRef4 = "";
        if (result["opstatus"] == 0) {
            if (result["StatusCode"] != 0) {
                dismissLoadingScreen();
                alert(" " + result["errMsg"]);
                return;
            }
            if (result["OnlinePmtInqRs"] != undefined || result["OnlinePmtInqRs"] != "") {
                BankRefId = result["OnlinePmtInqRs"][0]["BankRefId"];
                TranId = result["OnlinePmtInqRs"][0]["TrnId"];
                gblRef1 = result["OnlinePmtInqRs"][0]["Ref1"];
                gblRef2 = result["OnlinePmtInqRs"][0]["Ref2"];
                gblRef3 = result["OnlinePmtInqRs"][0]["Ref3"];
                gblRef4 = result["OnlinePmtInqRs"][0]["Ref4"];
                var easyPassName = "";
                var customerName = "CustomerName";
                if (gblEasyPassTopUp) {
                    var MiscDataLength = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length;
                    for (var i = 0; i < MiscDataLength; i++) {
                        if (kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], customerName)) {
                            // alert("customer Name Found");
                            easyPassName = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
                            //alert(easyPassNameEN);
                        }
                    }
                }
                frmBillPaymentConfirmationFuture.lblTopUpRefNumValue.text = BankRefId;
                frmBillPaymentComplete.lblTopUpRefNumValue.text = BankRefId;
                frmBillPaymentConfirmationFuture.lblEasyPassCustValue.text = easyPassName;
                frmBillPaymentComplete.lblEasyPassCustValue.text = easyPassName;
                if (isNotBlank(gblRef4)) {
                    frmBillPaymentComplete.lblCardBalVal.text = commaFormatted(parseFloat(gblRef4).toFixed(2)) + "" + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                callBillerValidationTopupMB();
            }
        } else {}
    }
}

function getBillPaymentIndex() {}
getBillPaymentIndex.prototype.paginationSwitch = function(sectionIndex, rowIndex) {
    var segdata = frmBillPayment.segSlider.data;
    rowIndex = parseFloat(rowIndex);
    frmBillPayment.segSlider.selectedIndex = [0, rowIndex];
    gbltranFromSelIndex = frmTransferLanding.segTransFrm.selectedIndex;
}

function getTopUpIndex() {}
getTopUpIndex.prototype.paginationSwitch = function(sectionIndex, rowIndex) {
    var segdata = frmTopUp.segSlider.data;
    rowIndex = parseFloat(rowIndex);
    frmTopUp.segSlider.selectedIndex = [0, rowIndex];
}

function saveToSessionBillPaymentMB() {
    inputParam = {};
    inputParam["toAccountNo"] = gblToAccountKey;
    inputParam["billerMethod"] = gblBillerMethod;
    inputParam["fromAccountNo"] = removeHyphenIB(frmBillPayment["segSlider"]["selectedItems"][0].lblActNoval);
    inputParam["ref1"] = frmBillPayment.lblRef1Value.text;
    inputParam["amount"] = parseFloat(removeCommos(AmountPaid.toString())).toFixed(2);
    inputParam["compCode"] = gblCompCode;
    var flex1 = "";
    if ("en_US" == kony.i18n.getCurrentLocale()) {
        flex1 = gblBillerCompCodeEN;
    } else {
        flex1 = gblBillerCompCodeTH;
    }
    flex1 = flex1.substring(0, flex1.lastIndexOf("("));
    inputParam["flex1"] = flex1.trim();
    inputParam["flex2"] = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text));
    if (gblreccuringDisablePay == "Y") inputParam["ref2"] = frmBillPayment.tbxRef2Value.text;
    if (gblPaynow) {
        inputParam["typeID"] = "027"
    } else {
        inputParam["typeID"] = "028"
    }
    if (gblreccuringDisablePay == "Y") inputParam["ref2"] = frmBillPayment.tbxRef2Value.text;
    else inputParam["ref2"] = "";
    inputParam["billerId"] = gblBillerId;
    invokeServiceSecureAsync("billPaymentValidationService", inputParam, saveToSessionBillPaymentcallBackMB);
}

function saveToSessionBillPaymentcallBackMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            frmBillPaymentConfirmationFuture.lblRef1Value.text = resulttable["ref1"];
            //below line is added for CR - PCI-DSS masked Credit card no
            frmBillPaymentConfirmationFuture.lblRef1ValueMasked.text = maskCreditCard(resulttable["ref1"]);
            frmBillPaymentConfirmationFuture.lblAmountValue.text = commaFormatted(parseFloat(resulttable["amount"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmBillPaymentConfirmationFuture.lblPaymentDateValue.text = resulttable["paymentServerDate"] + " [" + resulttable["paymentServerTime"] + "]";
        }
    }
}

function saveToSessionTopUpMB() {
    inputParam = {};
    inputParam["toAccountNo"] = gblToAccountKey;
    inputParam["billerMethod"] = gblBillerMethod;
    inputParam["fromAccountNo"] = removeHyphenIB(frmTopUp["segSlider"]["selectedItems"][0].lblActNoval);
    inputParam["ref1"] = frmTopUp.lblRef1Value.text;
    inputParam["amount"] = parseFloat(removeCommos(amount.toString())).toFixed(2);
    var flex1 = gblBillerCompCodeTH;
    if ("en_US" == kony.i18n.getCurrentLocale()) flex1 = gblBillerCompCodeEN;
    inputParam["compCode"] = gblCompCode;
    flex1 = flex1.substring(0, flex1.lastIndexOf("("));
    inputParam["flex1"] = flex1.trim();
    inputParam["flex2"] = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text));
    if (gblPaynow) {
        inputParam["typeID"] = "030"
    } else {
        inputParam["typeID"] = "031"
    }
    inputParam["billerId"] = gblBillerId;
    invokeServiceSecureAsync("billPaymentValidationService", inputParam, saveToSessionTopupMBCallBack);
}

function saveToSessionTopupMBCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            frmBillPaymentConfirmationFuture.lblRef1Value.text = resulttable["ref1"];
            //below line is added for CR - PCI-DSS masked Credit card no
            frmBillPaymentConfirmationFuture.lblRef1ValueMasked.text = maskCreditCard(resulttable["ref1"]);
            frmBillPaymentConfirmationFuture.lblAmountValue.text = commaFormatted(parseFloat(resulttable["amount"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmBillPaymentConfirmationFuture.lblPaymentDateValue.text = resulttable["paymentServerDate"] + " [" + resulttable["paymentServerTime"] + "]";
        }
    }
}

function clearBillerDetails() {
    launchBillPaymentFirstTime();
    frmBillPayment.hbxMyfreqSlide.setFocus(true);
}
/*************************************************************************

	Module	: validateCurrentDate
	Author  : Enhancement MIB-350
	Purpose : checking and display pop up when set schedule transfer and select current date 

***************************************************************************/
function validateCurrentDate(eventOption) {
    var selectDate = frmSchedule.calScheduleStartDate.formattedDate;
    var locale = kony.i18n.getCurrentLocale();
    selectDate = getFormattedDate(selectDate, locale);
    var today = currentSystemDate();
    today = getFormattedDate(today, locale);
    var returnVal;
    if (selectDate == "" || selectDate == null) {
        alert(kony.i18n.getLocalizedString("KeySelStartDate"));
        returnVal = true;
    } else if (((parseDate(selectDate) - parseDate(today)) <= 0) && eventOption == "setSchedule") {
        alert(kony.i18n.getLocalizedString("Schedule_TodayErr"));
        returnVal = true;
    } else if (((parseDate(selectDate) - parseDate(today)) < 0) && eventOption == "SelectDate") {
        alert(kony.i18n.getLocalizedString("KeySelStartDate"));
        returnVal = true;
    } else if (((parseDate(selectDate) - parseDate(today)) == 0) && eventOption == "SelectDate") {
        returnVal = true;
    } else {
        returnVal = false;
    }
    return returnVal;
}

function validateOnSelectDate() {
    var isCurrentDate = validateCurrentDate("SelectDate");
    if (isCurrentDate) {
        gblScheduleEndBP = "none";
        gblScheduleRepeatBP = "none";
        setRepeatClickedToFalse();
        gblStartBPDate = "";
        frmInternalSchedulePreShow();
        return;
    }
}

function disableEndAfterButtonHolder() {
    frmSchedule.hbxEndAfterButtonHolder.setVisibility(false);
    frmSchedule.lblEnd.setVisibility(false);
    frmSchedule.hbxEndOnDate.setVisibility(false);
    frmSchedule.hbxEndAfter.setVisibility(false);
    frmSchedule.tbxAfterTimes.text = "";
    gblBPScheduleFirstShow = true;
    frmSchedule.btnNever.skin = "btnScheduleEndLeft";
    frmSchedule.btnAfter.skin = "btnScheduleEndMid";
    frmSchedule.btnOnDate.skin = "btnScheduleEndRight";
}

function enableEndAfterButtonHolder() {
    setFocusOnEndNever();
}

function populateSelectedScheduleDate() {
    frmSchedule.calScheduleStartDate.dateComponents = currentDateForScheduleCalender(gblStartBPDate);
    frmSchedule.calScheduleStartDate.validStartDate = currentDateForScheduleCalender();
    frmSchedule.calScheduleEndDate.validStartDate = currentDateForScheduleCalender();
    populateRepateDetail();
    if (!kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Once")) {
        populateEndDetail();
    }
}

function populateRepateDetail() {
    if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Daily")) {
        setFocusOnRepeatDaily();
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
        setFocusOnRepeatWeekly();
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
        setFocusOnRepeatMonthly();
    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
        setFocusOnRepeatYearly();
    } else {
        disableOnRepeatOnce();
        setNoFocusOnRepeat();
        gblBPScheduleFirstShow = true;
    }
}

function populateEndDetail() {
    disableOnRepeatOnce();
    if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "Never")) {
        setFocusOnEndNever();
    } else if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "After")) {
        setFocusOnEndAfter();
        frmSchedule.hbxEndAfter.setVisibility(true);
        frmSchedule.tbxAfterTimes.text = gblNumberOfDays;
    } else if (kony.string.equalsIgnoreCase(gblScheduleEndBP, "OnDate")) {
        setFocusOnEndOnDate();
        frmSchedule.hbxEndOnDate.setVisibility(true);
        if (isNotBlank(gblEndBPDate)) {
            frmSchedule.calScheduleEndDate.dateComponents = currentDateForScheduleCalender(gblEndBPDate);
            frmSchedule.calScheduleEndDate.validStartDate = currentDateForScheduleCalender();
        }
    }
}

function disableOnRepeatOnce() {
    frmSchedule.lblEnd.setVisibility(false);
    frmSchedule.hbxEndAfterButtonHolder.setVisibility(false);
    frmSchedule.hbxEndAfter.setVisibility(false);
    frmSchedule.hbxEndOnDate.setVisibility(false);
    frmSchedule.lblEndAfter.setVisibility(false);
    frmSchedule.tbxAfterTimes.text = "";
    frmSchedule.tbxAfterTimes.keyBoardStyle = constants.TEXTBOX_KEY_BOARD_STYLE_NUMBER_PAD;
}

function setNoFocusOnRepeat() {
    frmSchedule.btnDaily.skin = btnScheduleLeft;
    frmSchedule.btnWeekly.skin = btnScheduleMid;
    frmSchedule.btnMonthly.skin = btnScheduleMid;
    frmSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnRepeatDaily() {
    frmSchedule.btnDaily.skin = btnScheduleLeftFocus;
    frmSchedule.btnWeekly.skin = btnScheduleMid;
    frmSchedule.btnMonthly.skin = btnScheduleMid;
    frmSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnRepeatWeekly() {
    frmSchedule.btnDaily.skin = btnScheduleEndLeft;
    frmSchedule.btnWeekly.skin = btnScheduleMidFocus;
    frmSchedule.btnMonthly.skin = btnScheduleMid;
    frmSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnRepeatMonthly() {
    frmSchedule.btnDaily.skin = btnScheduleEndLeft;
    frmSchedule.btnWeekly.skin = btnScheduleMid;
    frmSchedule.btnMonthly.skin = btnScheduleMidFocus;
    frmSchedule.btnYearly.skin = btnScheduleRight;
}

function setFocusOnRepeatYearly() {
    frmSchedule.btnDaily.skin = btnScheduleEndLeft;
    frmSchedule.btnWeekly.skin = btnScheduleMid;
    frmSchedule.btnMonthly.skin = btnScheduleMid;
    frmSchedule.btnYearly.skin = btnScheduleRightFocus;
}

function setFocusOnEndNever() {
    frmSchedule.lblEnd.setVisibility(true);
    frmSchedule.hbxEndAfterButtonHolder.setVisibility(true);
    frmSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmSchedule.calScheduleStartDate.dateComponents);
    gblBPScheduleFirstShow = false;
    gblScheduleEndBP = "Never";
    frmSchedule.btnNever.skin = btnScheduleEndLeftFocus;
    frmSchedule.btnAfter.skin = btnScheduleEndMid;
    frmSchedule.btnOnDate.skin = btnScheduleEndRight;
}

function setFocusOnEndAfter() {
    frmSchedule.lblEnd.setVisibility(true);
    frmSchedule.hbxEndAfterButtonHolder.setVisibility(true);
    frmSchedule.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmSchedule.calScheduleStartDate.dateComponents);
    gblBPScheduleFirstShow = false;
    gblScheduleEndBP = "After";
    frmSchedule.btnNever.skin = btnScheduleEndLeft;
    frmSchedule.btnAfter.skin = btnScheduleEndMidFocus;
    frmSchedule.btnOnDate.skin = btnScheduleEndRight;
}

function setFocusOnEndOnDate() {
    frmSchedule.lblEnd.setVisibility(true);
    frmSchedule.hbxEndAfterButtonHolder.setVisibility(true);
    gblBPScheduleFirstShow = false;
    gblScheduleEndBP = "OnDate";
    frmSchedule.btnNever.skin = btnScheduleEndLeft;
    frmSchedule.btnAfter.skin = btnScheduleEndMid;
    frmSchedule.btnOnDate.skin = btnScheduleEndMidRightFocus;
}

function currentDateForScheduleCalender(selectedDate) {
    var day = new Date(GLOBAL_TODAY_DATE);
    var dd = day.getDate() * 1;
    var mm = day.getMonth() + 1;
    var yyyy = day.getFullYear();
    var diff_yyyy = 0;
    if (isNotBlank(selectedDate)) {
        selectedDate = selectedDate.split("/", 3);
        diff_yyyy = selectedDate[2] - yyyy;
        dd = selectedDate[0] * 1;
        mm = selectedDate[1] * 1;
        yyyy = selectedDate[2];
    }
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    //var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
        var returnedValue = iPhoneCalendar.getDeviceDateLocale();
        var deviceDate = returnedValue.split("|")[0];
        var deviceDate_yyyy = deviceDate.substr(0, 4);
        yyyy = parseInt(deviceDate_yyyy) + diff_yyyy;
    }
    return [dd, mm, yyyy, 0, 0, 0];
}

function setRepeatClickedToFalse() {
    DailyClicked = false;
    monthClicked = false;
    weekClicked = false;
    YearlyClicked = false;
}