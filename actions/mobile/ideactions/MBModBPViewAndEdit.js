







gblEndingFreqOnLoadMB = "";
gblAmountSelectedMB = false;
//gblScheduleFreqChangedMB = false;
//gblexecutionOnLoadMB = "";
//gblendDateOnLoadMB = "";
/*
   **************************************************************************************
		Module	: numberOfExecution
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
// for Execution times 

function numberOfExecutionMB(date1, date2, repeatAs) {

	var startDate = parseDate(date1);
	var endDate = parseDate(date2);
	var executionTimes = "";
	if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyDaily")) || kony.string.equalsIgnoreCase(repeatAs, "Daily")) {
		executionTimes = daydiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyWeekly"))|| kony.string.equalsIgnoreCase(repeatAs, "Weekly") ) {
		executionTimes = weekdiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyMonthly")) || kony.string.equalsIgnoreCase(repeatAs, "Monthly")) {
		executionTimes = monthdiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("keyYearly")) || kony.string.equalsIgnoreCase(repeatAs, "Yearly")) {
		executionTimes = yeardiff(startDate, endDate);
	}
	return executionTimes;
}
/*
   **************************************************************************************
		Module	: endOnDateCalculator
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
// for end on date calculation

function endOnDateCalculatorMB(staringDateFromCalendar, repeatTimesTextBoxValue,repeatAs) {
    var endingDate = parseDate(staringDateFromCalendar);
    var numberOfDaysToAdd = 0;
    
	
    if (kony.string.equalsIgnoreCase(repeatAs, "Daily")) {
        numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
            endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd - 1);
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Weekly")) {
        numberOfDaysToAdd = ((repeatTimesTextBoxValue-1) * 7);
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Monthly")) {
            	endingDate.setDate(endingDate.getDate());
                var dd = endingDate.getDate();
                
    			var mm = endingDate.getMonth() + 1;
    			
    			var newmm = parseFloat(mm.toString())+ parseFloat(repeatTimesTextBoxValue - 1);
    			
    			var newmmadd = newmm % 12;
    			if(newmmadd == 0){
    				newmmadd = 12;
    			}
    			
    			
    			var yearAdd = Math.floor((newmm / 12));
    			
   				var y = endingDate.getFullYear();
   				if(newmmadd == 12){
   				   y = parseFloat(y )+ parseFloat(yearAdd)-1;	
   				}else
   				   y = parseFloat(y )+ parseFloat(yearAdd);
   				   
   				mm = parseFloat(mm.toString())+ newmmadd;
   				
   				if(newmmadd == 2){
   				if(dd > 28){
   				dd = 28;
   				}
   				}
   				if(newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11){
   				if(dd > 30){
   				dd = 30;
   				}
   				}
   				if (dd < 10) {
		        	dd = '0' + dd;
		   		 }
   				if(newmmadd < 10){
   					newmmadd = '0'+newmmadd;
   				}
   				
   				var someFormattedDate = dd + '/' + newmmadd + '/' + y;
   				return someFormattedDate;
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Yearly")) {
        var dd = endingDate.getDate();
   		var mm = endingDate.getMonth() + 1;
    	var y = endingDate.getFullYear();
    	var newYear = parseFloat(y)+ parseFloat(repeatTimesTextBoxValue - 1) ;
    	if (dd < 10) {
        	dd = '0' + dd
   		 }
   		 if (mm < 10) {
      	  mm = '0' + mm
   		 }
    var someFormattedDate = dd + '/' + mm + '/' + newYear;
    return someFormattedDate;
    }
    var dd = endingDate.getDate();
    var mm = endingDate.getMonth() + 1;
    var y = endingDate.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}


//gblBillerStatus = "1";


gblBillerStatus = "0";
gblBPflag = false; 
gblScheID = "";
function getMasterBillerDataForMB(scheIDBP){
  // alert("master biller service");
    gblBPflag = true; 
    gblScheID = scheIDBP;
    frmBillPaymentView.lblHead.text = kony.i18n.getLocalizedString("keyBillPaymentViewMB");
	var inputParam = {};
   // inputParam["IsActive"] = "1";
    inputParam["BillerGroupType"] = "0";
   	showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiryForEditBP", inputParam, callBackMasterBillerInqForEditBPMBService)
}

function callBackMasterBillerInqForEditBPMBService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["MasterBillerInqRs"].length > 0 && result["MasterBillerInqRs"] != null) {
				invokeCustomerBillInqForEditBPMBService();
			} else {
				dismissLoadingScreen();
				alert(result["errMsg"]);
			}
		}
	}
}


function getMasterBillerDataForMBTU(scheIDTU){
  // alert("master biller service");
   gblScheID = scheIDTU;
   gblBPflag = false; 
   frmBillPaymentView.lblHead.text = kony.i18n.getLocalizedString("keyMBViewTopUP");//"View Top Up";
	var inputParam = {};
   // inputParam["IsActive"] = "1";
    inputParam["BillerGroupType"] = "1";
	showLoadingScreen();
    invokeServiceSecureAsync("masterBillerInquiryForEditBP", inputParam, callBackMasterBillerInqForEditBPMBService)
}

function callBackMasterBillerInqForEditBPMBService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["MasterBillerInqRs"].length > 0 && result["MasterBillerInqRs"] != null) {
				invokeCustomerBillInqForEditBPMBService();
			} else {
				dismissLoadingScreen();
			}
		}else{
			alert(result["errMsg"]);
		}
	}
}


function invokeCustomerBillInqForEditBPMBService() {
	var inputParam = {
	  			IsActive: "1"
	   				 };
	invokeServiceSecureAsync("customerBillInquiry", inputParam, callBackCustomerBillInqForEditBPMBService)
}

function callBackCustomerBillInqForEditBPMBService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			callPaymentInqServiceForEditBPMB();
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	}
}

function callPaymentInqServiceForEditBPMB() {
	//alert("payment inq service ");
	var inputparam = [];
    inputparam["scheRefID"] = gblScheID;//"FB13000000005767";//"FP1300000001201600";//"FP130000000109280000"; // change this value when integrated with calender / future transactions page
   // inputparam["scheRefID"] = "SB1300000002171300";//"SB1300000002173500";
    inputparam["rqUID"] = "";
    invokeServiceSecureAsync("doPmtInqForEditBP", inputparam, callBackPaymenInqServiceForEditBPMB);

}



gblexecutionOnLoadMB = "";
gblendDateOnLoadMB = "";
function callBackPaymenInqServiceForEditBPMB(status, result) {
	if (status == 400) {
	
	
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			//if(StatusCode != 0){
			//		
			//}else {
			if (result["PmtInqRs"].length > 0 && result["PmtInqRs"] != null) {
								var channelID = result["PmtInqRs"][0]["ChannelID"];
				gblFromAccIdMB = result["PmtInqRs"][0]["FromAccId"];
				gblFromAccTypeMB = result["PmtInqRs"][0]["FromAccType"];
				gblAmountMB = result["PmtInqRs"][0]["Amt"];
				gblstartOnMB = result["PmtInqRs"][0]["Duedt"];
				gblTransCodeMB = result["PmtInqRs"][0]["TransCode"];
				gblBillerCategoryID = result["PmtInqRs"][0]["billerCategoryID"]; //BillerCategoryID for creditcard mask
				
				var myNote = result["PmtInqRs"][0]["MyNote"];
				var scheRefNo = result["PmtInqRs"][0]["scheRefNo"];
				var execTimesOnLoadMB = result["PmtInqRs"][0]["ExecutionTimes"];
				var onLoadRepeatAsMB = result["PmtInqRs"][0]["RepeatAs"];
				
				//gblBPViewRepatAsMB = result["PmtInqRs"][0]["RepeatAs"];
				
				gblToAccNoMB = result["PmtInqRs"][0]["ToAccId"];
				gblToAccTypeMB = result["PmtInqRs"][0]["ToAccType"];
				var endDateOnLoadMB = result["PmtInqRs"][0]["EndDate"];
				var pmtOrderdt = result["PmtInqRs"][0]["InitiatedDt"]; 
				var bankId = result["PmtInqRs"][0]["BankId"];
				gblCustPayeeIDMB = result["PmtInqRs"][0]["InternalBillerID"];
				gblPmtMethodFromPmt = result["PmtInqRs"][0]["PmtMethod"];
				
				gblRef2EditBPMB = "";
				if(result["PmtInqRs"][0]["Ref2"] != undefined && result["PmtInqRs"][0]["Ref2"] != "undefined"){
					gblRef2EditBPMB = result["PmtInqRs"][0]["Ref2"];
					frmBillPaymentView.lblRef2value.text = result["PmtInqRs"][0]["Ref2"];
				}else{
					gblRef2EditBPMB = "";
					frmBillPaymentView.lblRef2value.text = "";
				}
				
				frmBillPaymentView.lblamtvalue.text = commaFormatted(gblAmountMB) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				frmBillPaymentView.lblPaymentDateValue.text = dateFormatForDisplayWithTimeStampMB(pmtOrderdt);
	//			gblPmtOrderdt = dateFormatForDisplayWithTimeStampMB(pmtOrderdt);
				
				if(myNote != undefined){
					frmBillPaymentView.lblMyNotesDesc.text = myNote;
				}else
					frmBillPaymentView.lblMyNotesDesc.text = "-";
				
        		//frmBillPaymentView.lblMyNotesDesc.text = myNote;
				if(gblBPflag){
					frmBillPaymentView.lblScheduleRefValue.text = "SB" + scheRefNo.substring(2, scheRefNo.length);
				}else{
					frmBillPaymentView.lblScheduleRefValue.text = scheRefNo;//.replace("S", "F");
				}
				
				frmBillPaymentView.lblStartDate.text = dateFormatForDisplay(gblstartOnMB);
				
				if (onLoadRepeatAsMB != "" && onLoadRepeatAsMB != undefined && onLoadRepeatAsMB != null) {
					if (onLoadRepeatAsMB == "Annually") { //kony.i18n.getLocalizedString("KeyAnnually")
						 //kony.string.equalsIgnoreCase(onLoadRepeatAsMB, "Annually")
						
						onLoadRepeatAsMB = kony.i18n.getLocalizedString("keyYearly");
						gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
						frmBillPaymentView.lblrepeatasvalue.text = onLoadRepeatAsMB;
						
						
					} else {
						gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
						frmBillPaymentView.lblrepeatasvalue.text = onLoadRepeatAsMB;
					}
					if (endDateOnLoadMB != "" && endDateOnLoadMB != undefined && endDateOnLoadMB != null) {
						gblEndingFreqOnLoadMB = "On Date";    //kony.i18n.getLocalizedString("keyOnDate");	//"On Date"
						gblScheduleEndBPMB = gblEndingFreqOnLoadMB; 
						frmBillPaymentView.lblEndOnDate.text = dateFormatForDisplay(endDateOnLoadMB);
						
						var executiontimes = numberOfExecutionMB(frmBillPaymentView.lblStartDate.text, frmBillPaymentView.lblEndOnDate.text,gblOnLoadRepeatAsMB);
						frmBillPaymentView.lblExecutetimes.text = (executiontimes).toString();
						gblendDateOnLoadMB = dateFormatForDisplay(endDateOnLoadMB);
						gblexecutionOnLoadMB = executiontimes;
					} else if (execTimesOnLoadMB != "" & execTimesOnLoadMB != undefined && execTimesOnLoadMB != null) {
						gblEndingFreqOnLoadMB = "After";	//kony.i18n.getLocalizedString("keyAfter");	//"After"
						gblScheduleEndBPMB = gblEndingFreqOnLoadMB;
						frmBillPaymentView.lblExecutetimes.text = (execTimesOnLoadMB).toString();
						var endOnDate = endOnDateCalculatorMB(frmBillPaymentView.lblStartDate.text, execTimesOnLoadMB,gblOnLoadRepeatAsMB);
						frmBillPaymentView.lblEndOnDate.text = endOnDate;
						gblendDateOnLoadMB = endOnDate;
						gblexecutionOnLoadMB = execTimesOnLoadMB;
					} else {
						gblEndingFreqOnLoadMB = "Never";	//kony.i18n.getLocalizedString("keyNever");	//"Never"
						gblScheduleEndBPMB = gblEndingFreqOnLoadMB;
						//frmBillPaymentView.hbox58853551515106.setVisibility(false);
						//frmBillPaymentView.hbox58853551515107.setVisibility(false);
						frmBillPaymentView.lblEndOnDate.text = "-";
						frmBillPaymentView.lblExecutetimes.text = "-";
						gblexecutionOnLoadMB = "";
						gblendDateOnLoadMB = "";
					}
				}else{
					onLoadRepeatAsMB = "Once";
					gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
					frmBillPaymentView.lblEndOnDate.text = dateFormatForDisplay(gblstartOnMB); //"-";
					frmBillPaymentView.lblExecutetimes.text = "1"; //"-";
					frmBillPaymentView.lblrepeatasvalue.text = onLoadRepeatAsMB;
					gblexecutionOnLoadMB = "1";//"";
					gblendDateOnLoadMB = dateFormatForDisplay(gblstartOnMB); //"";
					gblEndingFreqOnLoadMB ="";
					gblScheduleEndBPMB = gblEndingFreqOnLoadMB;
				
				}
					
					gblRef1ValueBPMB = result["PmtInqRs"][0]["ref1ValueB"];
					if(result["PmtInqRs"][0]["ref1ValueB"] != null && result["PmtInqRs"][0]["ref1ValueB"] != undefined){
						var ref1Len = result["PmtInqRs"][0]["ref1ValueB"].length;
						//masking CR code
						//below line is changed for CR - PCI-DSS masked Credit card no
						if(result["PmtInqRs"][0]["maskedRef1ValueB"] != null && result["PmtInqRs"][0]["maskedRef1ValueB"] != undefined){
							frmBillPaymentView.lblRef1valueMasked.text = result["PmtInqRs"][0]["maskedRef1ValueB"];
						}							
						//below line is added for CR - PCI-DSS masked Credit card no
						frmBillPaymentView.lblRef1value.text = result["PmtInqRs"][0]["ref1ValueB"];
						/*
						if(ref1Len >= "10"){
							frmBillPaymentView.lblRef1value.text = result["PmtInqRs"][0]["ref1ValueB"];
						}else{
							frmBillPaymentView.lblRef1value.text = result["PmtInqRs"][0]["ref1ValueB"];
						}
						*/
					}else {
					
					frmBillPaymentView.lblRef1value.text = "";
					//below line is added for CR - PCI-DSS masked Credit card no
					frmBillPaymentView.lblRef1valueMasked.text = "";
					
					}
					gblBillerStatus = result["PmtInqRs"][0]["activeStatus"];
					if(gblBPflag){
						if (result["PmtInqRs"][0]["ref2ValueB"] != "" &&  result["PmtInqRs"][0]["ref2ValueB"] != undefined && result["PmtInqRs"][0]["ref2ValueB"] != "undefined" ) {
							var ref2Len = result["PmtInqRs"][0]["ref2ValueB"].length;
							if(ref2Len >= "10"){
								frmBillPaymentView.lblRef2value.text = result["PmtInqRs"][0]["ref2ValueB"];
							}else{
								frmBillPaymentView.lblRef2value.text = result["PmtInqRs"][0]["ref2ValueB"];
							}
						}	
					}else{
						frmBillPaymentView.hbox58862488827877.setVisibility(false);
					}
					if (result["PmtInqRs"][0]["billerNickName"] != null && result["PmtInqRs"][0]["billerNickName"] != "") {
						if (gblBillerStatus == 0) {
								frmBillPaymentView.lblBillerNickName.setVisibility(false);
						} else {
								frmBillPaymentView.lblBillerNickName.text = result["PmtInqRs"][0]["billerNickName"];
						}		
					}  else {
						//If Not avialble , show blank
						frmBillPaymentView.lblBillerNickName.text = "";
					}
							gblEditBillMethodMB = result["PmtInqRs"][0]["billerMethod"];
                            gblEditBillerGroupTypeMB = result["PmtInqRs"][0]["billerGroupType"];
                            billerShortNameMB = result["PmtInqRs"][0]["billerShortName"];
                            gblBillerCompcodeEditMB = result["PmtInqRs"][0]["billerCompcode"];
                            billerNamTH=result["PmtInqRs"][0]["billerNameTH"];
                            gblBPEditEffDtMB = result["PmtInqRs"][0]["effDt"];// get this value as we need for onlinepaymenyinq
							
                            var local = kony.i18n.getCurrentLocale();
                            var billerNameMB = "";
                            gblbillerNameMB_EN = "";
                            gblbillerNameMB_TH = "";
                            labelReferenceNumber1MB = "";
                            labelReferenceNumber2MB = "";
                            gblbillerNameMB_EN = result["PmtInqRs"][0]["billerNameEN"];
                            gblbillerNameMB_TH = result["PmtInqRs"][0]["billerNameTH"];
                            
                            if (local == "en_US") {
                                billerNameMB = result["PmtInqRs"][0]["billerNameEN"];
                                labelReferenceNumber1MB = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
                                labelReferenceNumber2MB = result["PmtInqRs"][0]["labelReferenceNumber2EN"];
                            } else if (local == "th_TH") {
                                billerNameMB = result["PmtInqRs"][0]["billerNameTH"];
                                labelReferenceNumber1MB = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                                labelReferenceNumber2MB = result["PmtInqRs"][0]["labelReferenceNumber2TH"];
                            }
                           
                           gblRef1LblEN = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
                           gblRef1LblTH = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                           gblRef2LblEN = result["PmtInqRs"][0]["labelReferenceNumber2EN"];
                           gblRef2LblTH = result["PmtInqRs"][0]["labelReferenceNumber2TH"];
                           
                           
                            if (gblBillerStatus == "0") {
                                frmBillPaymentView.lblBillerName.text = billerShortNameMB;
                                 if(labelReferenceNumber1MB != ""){
                                	frmBillPaymentView.lblref1.text = findColonLastString(labelReferenceNumber1MB) ? labelReferenceNumber1MB + ":" : labelReferenceNumber1MB;
                                	
                                	
                                }
                                frmBillPaymentView.btnedit.setEnabled(false);
                                frmBillPaymentView.btnedit.skin = "btnEdic";
                                frmBillPaymentView.lblref2.text = "";
                                frmBillPaymentView.hbox58862488827877.setVisibility(false);
                            } else {
                                frmBillPaymentView.lblBillerName.text = billerNameMB + " (" + gblBillerCompcodeEditMB + ") ";
                                if(labelReferenceNumber2MB != null && labelReferenceNumber2MB != undefined && labelReferenceNumber2MB != "" && 
                                (result["PmtInqRs"][0]["ref2ValueB"] != "" && result["PmtInqRs"][0]["ref2ValueB"] != "undefined")){
                                	
                                	frmBillPaymentView.lblref2.text = findColonLastString(labelReferenceNumber2MB) ? labelReferenceNumber2MB + ":" : labelReferenceNumber2MB;
                                }else{
                                	frmBillPaymentView.lblref2.text = "";
                                	frmBillPaymentView.hbox58862488827877.setVisibility(false);
                                }
                                 if(labelReferenceNumber1MB != ""){
                                	frmBillPaymentView.lblref1.text = findColonLastString(labelReferenceNumber1MB) ? labelReferenceNumber1MB + ":" : labelReferenceNumber1MB;
                                	
                                	
                                }
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                            //step amount 
							
							if(!gblBPflag && gblEditBillMethodMB ==1){
									if (result["StepAmount"].length > 0 && result["StepAmount"] != null){
										comboDataEditTPMB = [];
										var temp = [];
										var j = 1;	
                						for (var i = 0; i < result["StepAmount"].length; i++) {
                    						
                    						var tmpAmt = gblAmountMB+".00";
                    						var resultAmt = result["StepAmount"][i]["Amt"];
                    						//gblAmountMB+".00" == result["StepAmount"][i]["Amt"]
					
					
                    						if(parseFloat(tmpAmt.toString()) == parseFloat(resultAmt.toString())){
                    							temp = [ 0,gblAmountMB+".00"];
                    							//j--;
                    						} else {
                    							temp = [ j , result["StepAmount"][i]["Amt"]];
                    							j++;
                    						}
                     						 
                   							 comboDataEditTPMB.push(temp);
                					}
									 
									frmBillPaymentEdit.cbTPAmountVal.masterData = comboDataEditTPMB; 
                					frmBillPaymentEdit.cbTPAmountVal.selectedKey=0;
                					
                					
                				//frmIBTopUpViewNEdit.comboboxOnlineAmt.selectedKey=0;
								}
							
							}
                          	//BILLER_LOGO_URL="https://vit.tau2904.com/tmb/ImageRender";//hard code to check
							var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcodeEditMB+ "&modIdentifier=MyBillers";
							//BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcodeEditMB;
							
							frmBillPaymentView.imgBiller.src=imagesUrl;
							//frmBillPaymentEdit.imgBiller.src=imagesUrl;
							//frmEditFutureBillPaymentConfirm.imgBiller.src=imagesUrl;
							//frmEditFutureBillPaymentComplete.imgBillerPic.src=imagesUrl;
							//As per mail discussion don't call billpaymentInq service for TMB credit card and TMB Loan
							if (gblEditBillMethodMB == 0 || gblEditBillMethodMB ==1){
								
								forBPBillPmtInqMB();
							}else{
								frmBillPaymentView.lblPaymentFeeValue.text = "0.0" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                				forBPCallCustomerAccountInqMB(); 
							}					
					//forBPCallCustomerAccountInqMB(bankId);
				}else{
					dismissLoadingScreen();
					alert(result["AdditionalStatusDesc"]);
					//			
				}
				
			//	}
		} else {
			dismissLoadingScreen();
			//alert(result["errmsg"]);
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		}
	} else {
		if (status == 300) {
			dismissLoadingScreen();
			
		}
	}
}

function dateFormatForDisplayWithTimeStampMB(datetimestamp) {
	var year = datetimestamp.substring(0, 4);
	var month = datetimestamp.substring(5, 7);
	var date = datetimestamp.substring(8, 10);
	var TimeofActivation = datetimestamp.substring(11, 19);
	var formatedDatewithTimeStamp = date + "/" + month + "/" + year + " " + TimeofActivation;
	NormalDateMB=date + "/" + month + "/" + year;
	return formatedDatewithTimeStamp;
}
/*
   **************************************************************************************
		Module	: callPaymentInquiryService
		Author  : Kony
		Date    : 
		Purpose : to fetch the required data by scheduledRefNO
	***************************************************************************************
*/
/*
function callPaymentInquiryService() {
	//showLoadingScreen();
	var inputparam = [];
	inputparam["rqUID"] = "";
	inputparam["scheRefID"] = "8000000003";
	invokeServiceSecureAsync("doPmtInq", inputparam, callBackPaymentInquirySerivceMB)
}

function callBackPaymentInquirySerivceMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode != 0) {
				
			} else {
				if (result["PmtInqRs"].length > 0 && result["PmtInqRs"] != null) {
					
					var channelID = result["PmtInqRs"][0]["ChannelID"];
					gblfromAccId = result["PmtInqRs"][0]["FromAccId"];
					var fromAccType = result["PmtInqRs"][0]["FromAccType"];
					var amount = result["PmtInqRs"][0]["Amt"];
					var startOn = result["PmtInqRs"][0]["Duedt"];
					gblTransCode = result["PmtInqRs"][0]["TransCode"];
					var myNote = result["PmtInqRs"][0]["MyNote"];
					var scheRefNo = result["PmtInqRs"][0]["scheRefNo"];
					var execTimesOnLoadMB = result["PmtInqRs"][0]["ExecutionTimes"];
					var onLoadRepeatAsMB = result["PmtInqRs"][0]["RepeatAs"];
					var toAccNo = result["PmtInqRs"][0]["ToAccId"];
					var toAccType = result["PmtInqRs"][0]["ToAccType"];
					var endDateOnLoadMB = result["PmtInqRs"][0]["EndDate"];
					var pmtOrderdt = result["PmtInqRs"][0]["InitiatedDt"]; // check this with TL
					var bankId = result["PmtInqRs"][0]["BankId"];
					gblCustPayeeIDMB = result["PmtInqRs"][0]["InternalBillerID"];
					frmBillPaymentView.lblamtvalue.text = commaFormatted(amount) + " " + kony.i18n.getLocalizedString(
						"currencyThaiBaht");
					frmBillPaymentView.lblPaymentDateValue.text = dateFormatforDisplayWithTimeStamp(pmtOrderdt);
					gblPmtOrderdt = dateFormatforDisplayWithTimeStamp(pmtOrderdt);
					frmBillPaymentView.lblMyNotesDesc.text = myNote;
					frmBillPaymentView.lblScheduleRefValue.text = scheRefNo;
					frmBillPaymentView.lblStartDate.text = dateFormatForDisplay(startOn);
					
					if (onLoadRepeatAsMB != "" && onLoadRepeatAsMB != undefined && onLoadRepeatAsMB != null) {
						if (kony.string.equalsIgnoreCase(onLoadRepeatAsMB, "Annually")) { //kony.i18n.getLocalizedString("KeyAnnually")
							onLoadRepeatAsMB = kony.i18n.getLocalizedString("KeyYearly");
							gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
							frmBillPaymentView.lblrepeatasvalue.text = onLoadRepeatAsMB;
						} else {
							gblOnLoadRepeatAsMB = onLoadRepeatAsMB;
							frmBillPaymentView.lblrepeatasvalue.text = onLoadRepeatAsMB;
						}
						if (endDateOnLoadMB != "" && endDateOnLoadMB != undefined && endDateOnLoadMB != null) {
							gblEndingFreqOnLoadMB = kony.i18n.getLocalizedString("keyOnDate");
							frmBillPaymentView.lblEndOnDate.text = dateFormatForDisplay(endDateOnLoadMB);
							var executiontimes = numberOfExecutionMB(frmBillPaymentView.lblStartDate.text, frmBillPaymentView.lblEndOnDate.text,
								gblOnLoadRepeatAsMB);
							frmBillPaymentView.lblExecutetimes.text = executiontimes;
							gblendDateOnLoadMB = dateFormatForDisplay(endDateOnLoadMB);
							gblexecutionOnLoadMB = executiontimes;
						} else if (execTimesOnLoadMB != "" & execTimesOnLoadMB != undefined && execTimesOnLoadMB != null) {
							gblEndingFreqOnLoadMB = kony.i18n.getLocalizedString("keyAfter");
							frmBillPaymentView.lblExecutetimes.text = execTimesOnLoadMB;
							var endOnDate = endOnDateCalculatorMB(frmBillPaymentView.lblStartDate.text, execTimesOnLoadMB,
								gblOnLoadRepeatAsMB);
							frmBillPaymentView.lblEndOnDate.text = endOnDate;
							gblendDateOnLoadMB = endOnDate;
							gblexecutionOnLoadMB = execTimesOnLoadMB;
						} else {
							gblEndingFreqOnLoadMB = kony.i18n.getLocalizedString("keyNever");
							//frmBillPaymentView.hbox58853551515106.setVisibility(false);
							//frmBillPaymentView.hbox58853551515107.setVisibility(false);
							frmBillPaymentView.lblEndOnDate.text = "-";
							frmBillPaymentView.lblExecutetimes.text = "-";
						}
					} else {
						gblOnLoadRepeatAsMB = "once";
						//frmBillPaymentView.hbox58853551515106.setVisibility(false);//endon
						//frmBillPaymentView.hbox58853551515107.setVisibility(false); // executetimes
						//frmBillPaymentView.hbox58853551515104.setVisibility(false); // repeatAs
						frmBillPaymentView.lblEndOnDate.text = "-";
						frmBillPaymentView.lblrepeatasvalue.text = onLoadRepeatAsMB;
						frmBillPaymentView.lblExecutetimes.text = "-";
					}
					//	forBPCallCustomerAccountInqMB(bankId);
				} else {
					//dismissLoadingScreen();
					
				}
			}
		} else {
			//dismissLoadingScreen();
			
		}
	} else {
		if (status == 300) {
			
		}
	}
}
*/
function forBPCallCustomerAccountInqMB() {
	inputparam = [];
	inputparam["billPayInd"] = "billPayInd";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, forBPcusAccInqcallBackFunctionMB);
}

function forBPcusAccInqcallBackFunctionMB(status, result){
	if(status == 400){
		if(result["opstatus"] == 0){
			//alert("customer accout");
				//
				
				//alert("Len:    "+result["custAcctRec"].length);
				var ICON_ID = "";
				if(result["custAcctRec"].length >0 && result["custAcctRec"] != null){
					gblCustNickBP_TH = "";
					gblCustNickBP_EN = "";
					var accFound = false;
						for(var i=0; i < result["custAcctRec"].length ; i++ ){
							var cusAccNo = result["custAcctRec"][i]["accId"];
							//
							/*
							if(cusAccNo.length == "14"){
								cusAccNo = cusAccNo.substring(4, cusAccNo.length);
							}
							
							var pmtFromAcctNoMB = gblFromAccIdMB;
							//var pmtFromAcctNoMB = "";
							
							if(gblFromAccTypeMB == "SDA"){
                    			pmtFromAcctNoMB = "0000" + gblFromAccIdMB;
							} else {
								pmtFromAcctNoMB = gblFromAccIdMB;
							}
							*/
							var pmtFromAcctNoMB = gblFromAccIdMB;
							// Below code added due to  DEF734 defect for MB billpayment
							 if(gblFromAccTypeMB == "SDA" || gblFromAccTypeMB == "CDA" ){
                               if(pmtFromAcctNoMB.length == "10")
                                 pmtFromAcctNoMB = "0000" + gblFromAccIdMB;
                               } else {
                                  pmtFromAcctNoMB = gblFromAccIdMB;
                            }
							if(cusAccNo == pmtFromAcctNoMB){ 
								accFound = true;
								var fromAccNickName = result["custAcctRec"][i]["acctNickName"];
								gblCustNickBP_TH = result["custAcctRec"][i]["acctNickNameTH"];
								gblCustNickBP_EN = result["custAcctRec"][i]["acctNickNameEN"];
								
								var fromAcctStatus = result["custAcctRec"][i]["personalisedAcctStatusCode"];
								var fromAccName = result["custAcctRec"][i]["accountName"];
								gblAvailableBal = result["custAcctRec"][i]["availableBal"];
								var productIDMB =result["custAcctRec"][i]["productID"];
								ICON_ID = result["custAcctRec"][i]["ICON_ID"];
								if(fromAcctStatus == "02"){
									if (frmBillPaymentView.hboxFrom.isVisible) {
										frmBillPaymentView.hboxFrom.setVisibility(false);
									}
									if (!frmBillPaymentView.hboxFromAccDelMsg.isVisible) {
										frmBillPaymentView.hboxFromAccDelMsg.setVisibility(true);
									}
									frmBillPaymentView.richtext446432445207554.skin = rchBlack150;
									if(flowSpa)
									{
									var delAccMessage = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br>"+"<a onclick= \"return addMyAccntBPTPMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>"+"<br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";
									}
									else
									{
							//		var delAccMessage = kony.i18n.getLocalizedString("S2S_DelAcctMsg1") + ""+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+" "+ "<a onclick= \"return addMyAccntBPTPMB();\" href = \"#\"  >" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a>" +" "+ kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
								//	var delAccMessage = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br><a onclick= addMyAccntBPTPMB(); href = '#' style='font-size: 200%'>" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a><br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";
									var delAccMessage = kony.i18n.getLocalizedString("S2S_DelAcctMsg1")+"<br>"+kony.i18n.getLocalizedString("S2S_DelAcctMsg2")+"</br><a onclick= 'addMyAccntBPTPMB();'  style='display:block'>" + kony.i18n.getLocalizedString("S2S_MyAcctList") + "</a><br>"+kony.i18n.getLocalizedString("S2S_BeforeProcTrans")+"</br>";
									}
									frmBillPaymentView.hboxFromAccDelMsg.setVisibility(true);		
									frmBillPaymentView.richtext446432445207554.setVisibility(true);
									frmBillPaymentView.richtext446432445207554.text = delAccMessage;
									
                                    
									frmBillPaymentView.hboxFromAccDelMsg.onClick = addMyAccntBPTPMB;
									frmBillPaymentView.btnedit.setEnabled(false);
									frmBillPaymentView.btnedit.skin = "btnEdicDisabled"; // check for the skin - To Do 
								}else{
									frmBillPaymentView.btnedit.setEnabled(true);
									frmBillPaymentView.btnedit.skin = "btnEdic"; // check for the skin - To Do 
									frmBillPaymentView.richtext446432445207554.setVisibility(false);
									if (!frmBillPaymentView.hboxFrom.isVisible) {
										frmBillPaymentView.hboxFrom.setVisibility(true);
									}
									if (frmBillPaymentView.hboxFromAccDelMsg.isVisible) {
										frmBillPaymentView.hboxFromAccDelMsg.setVisibility(false);
									}
									if(fromAccNickName != "" &&  fromAccNickName != undefined){
										frmBillPaymentView.lblFromAccountNickname.text = fromAccNickName;
									} else {
									//for eng  :ProductNameEng
									//for TH : ProductNameThai
									var local = kony.i18n.getCurrentLocale();			
									var length = cusAccNo.length;	
									var accNum = cusAccNo.substring(length - 4, length); // last four digits of the account number
									if(local == "en_US"){
										var ProductNameEng = result["custAcctRec"][i]["productNmeEN"];
										//alert("prod : " +ProductNameEng);
										frmBillPaymentView.lblFromAccountNickname.text= ProductNameEng +" "+accNum ;
									}else if(local == "th_TH"){
										var ProductNameThai = result["custAcctRec"][i]["productNmeTH"];
										frmBillPaymentView.lblFromAccountNickname.text = ProductNameThai +" "+accNum ;
									}
								}
							}
							
							frmBillPaymentView.lblFromAccountName.text = fromAccName;
							var tmp = cusAccNo;
							tmp = tmp.substring(tmp.length-10,tmp.length);
							frmBillPaymentView.lblFromAccountNumber.text = addHyphenMB(tmp);
							
				
			  var frmProdIcon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+ICON_ID+"&modIdentifier=PRODICON";				
			  //var frmProdIcon = "https://vit.tau2904.com:443/tmb/ImageRender?crmId=&&personalizedId=&billerId="+ICON_ID+"&modIdentifier=PRODICON";				
					frmBillPaymentView.imgFrom.src = frmProdIcon+".png";		
							
							/*
							//Assigning from Account images
							if(gblFromAccTypeMB == "SDA"){
									if(productIDMB == "222"){
										    	frmBillPaymentView.imgFrom.src = "prod_nofee.png";
										    	
									 }else if(productIDMB == "221"){
										    	frmBillPaymentView.imgFrom.src = "piggyblueico.png";

									 }else if(productIDMB == "203" || productIDMB == "206"){
											    frmBillPaymentView.imgFrom.src = "piggyblueico.png";

									 }else{
										        frmBillPaymentView.imgFrom.src = "prod_currentac.png";
	
									 }
							}else if(gblFromAccTypeMB == "DDA"){
										 	frmBillPaymentView.imgFrom.src = "prod_currentac.png";

							}else if(gblFromAccTypeMB == "CDA"){
										    frmBillPaymentView.imgFrom.src = "prod_termdeposits.png";
			
							}
							
							*/
							
							//-- New change to handle easy Pass 
							if(gblBPflag == false && gblBillerCompcodeEditMB == "2151"){
							     srvOnlinePaymentInq()
							
							}else{
								frmBillPaymentView.hbxTPCusName.setVisibility(false);
								dismissLoadingScreen();
								frmBillPaymentView.show();
							
							}  //--
							//dismissLoadingScreen();
							//frmBillPaymentView.show();
						}else{
								if(result["custAcctRec"].length == i+1){ 
									if( !accFound){
										dismissLoadingScreen();
										//alert("No Records Found for the given Account num");
									}
								}
							}
						}
					}else{
						dismissLoadingScreen();
						alert(result["StatusDesc"]);
					}

			}else {
				dismissLoadingScreen();
				alert(result["errMsg"]);
				return false;
			}
	} else {
		if (status == 300) {
			dismissLoadingScreen();
			
		}
	}
}

function srvOnlinePaymentInq(){
	var inputparam = [];
	inputparam["rqUID"] = "";
	inputparam["EffDt"] = "";
	inputparam["TrnId"] = ""; // change this value when integrated with calender / future transactions page
	inputparam["Amt"] = gblAmountMB;//"0.00"; /// for billpayment it is 0.00
	inputparam["compCode"] = gblBillerCompcodeEditMB;
	//inputparam["TranCode"] = gblTransCodeMB;
	//inputparam["AcctId"] = gblFromAccIdMB;
	//inputparam["AcctTypeValue"] = gblFromAccTypeMB;
	inputparam["MobileNumber"] = removeHyphenIB(frmBillPaymentView.lblRef1value.text);
	inputparam["BankId"]="011";
	inputparam["BranchId"]= "0001";
	
	
	invokeServiceSecureAsync("onlinePaymentInqForEditBP", inputparam, srvOnlinePaymentInqCallBack);

}

function srvOnlinePaymentInqCallBack(status, result) {
	if (status == 400) {
	
		if (result["opstatus"] == 0) {
			if ( result["OnlinePmtInqRs"] != undefined && result["OnlinePmtInqRs"].length > 0 && result["OnlinePmtInqRs"] != null) {
					if(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length >0 && result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"] != null){
						for(var i=0; i < result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length ;i++ ){
							if(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"] == "CustomerName"){
								frmBillPaymentView.hbxTPCusName.setVisibility(true);
								frmBillPaymentView.lblTPCustomerName.text = kony.i18n.getLocalizedString("keyEasyPassCustomerName");
								frmBillPaymentView.lblTPCustomerNameVal.text = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
								dismissLoadingScreen();
								frmBillPaymentView.show();
							}	
						}
					}
					
			} else {
				dismissLoadingScreen();
				alert(""+result["errMsg"]);
				
			}
		}else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
	   }
	}
}




function addMyAccntBPTPMB(){
	//alert("My Accounts");
  frmMyAccountList.show();  
}
/*
function forBPMasterBillInqMB() {
	inputparam = [];
	//inputparam["billerCompcode"] = "" 
	inputparam["BillerGroupType"] = "0";
	inputparam["billerName"] = "";
	inputparam["billerID"] = gblCustPayeeID; //as per discussion pass custpayeeID 100142 //check this 
	inputparam["billerCategoryID"] = gblCustPayeeID;
	inputparam["BillerTaxID"] = "";
	inputparam["rqUID"] = "";
	invokeServiceSecureAsync("masterBillerInquiryBB", inputparam, forBPmasterBillerInqcallBackFunctionMB);
}

function forBPmasterBillerInqcallBackFunctionMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["MasterBillerInqRs"].length > 0 && result["MasterBillerInqRs"] != null) {
				gblEditBillMethod = result["MasterBillerInqRs"][0]["BillerMethod"];
				gblEditBillerGroupType = result["MasterBillerInqRs"][0]["BillerGroupType"];
				billerShortName = result["MasterBillerInqRs"][0]["BillerShortName"];
				gblbillerCompcode = result["MasterBillerInqRs"][0]["billerCompcode"];
				var local = kony.i18n.getCurrentLocale();
				var billerName = "";
				var LabelReferenceNumber1 = "";
				var LabelReferenceNumber2 = "";
				if (local == "en_US") {
					billerName = result["MasterBillerInqRs"][0]["BillerNameEN"];
					LabelReferenceNumber1 = result["MasterBillerInqRs"][0]["LabelReferenceNumber1EN"];
					LabelReferenceNumber2 = result["MasterBillerInqRs"][0]["LabelReferenceNumber2EN"];
				} else if (local == "th_TH") {
					billerName = result["MasterBillerInqRs"][0]["BillerNameTH"];
					LabelReferenceNumber1 = result["MasterBillerInqRs"][0]["LabelReferenceNumber1TH"];
					LabelReferenceNumber2 = result["MasterBillerInqRs"][0]["LabelReferenceNumber2TH"];
				}
				gblBPEditEffDt = result["MasterBillerInqRs"][0]["EffDt"];
				var expiredDt = result["MasterBillerInqRs"][0]["ExpDt"];
				var sysDt = getCurrentDate();
				gblbillerStatus = "";
				//Compare the system date with the effective date and expired date 
				//Active(1) = Effective date <= SystemDate < Expired date (or Expired date is null)
				//InActive(0) = SystemDate < Effective date or SystemDate >= Expired date
				if (gblBPEditEffDt <= sysDt && sysDt < expiredDt || expiredDt == null) {
					gblbillerStatus = "1";
				} else if (sysDt < gblBPEditEffDt || sysDt >= expiredDt) {
					gblbillerStatus = "0";
				}
				if (gblbillerStatus == "0") {
					frmBillPaymentView.lblBillerName.text = billerShortName;
					frmBillPaymentView.lblref1.text = LabelReferenceNumber1;
					frmBillPaymentView.btnedit.setEnabled(false);
					frmBillPaymentView.btnedit.skin = "btnIBediticonsmall";
					frmBillPaymentView.hbox58862488827877.setVisibility(false);
				} else {
					frmBillPaymentView.lblBillerName = billerName + " (" + gblbillerCompcode + ") ";
					frmBillPaymentView.lblref2.text = LabelReferenceNumber2;
					frmBillPaymentView.lblref1.text = LabelReferenceNumber1;
				}
				forBPCustomerBillInquiryMB();
			}
		} else {
			//dismissLoadingScreen();
			
		}
	} else {
		if (status == 300) {
			
		}
	}
}

function forBPCustomerBillInquiryMB() {
	inputparam = [];
	inputparam["crmId"] = "";
	invokeServiceSecureAsync("customerBillInquiry", inputparam, forBPcustomerBillInqcallBackFunctionMB);
}

function forBPcustomerBillInqcallBackFunctionMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["CustomerBillInqRs"].length > 0 && result["CustomerBillInqRs"] != null) {
				frmBillPaymentView.lblRef1value.text = result["CustomerBillInqRs"][0]["ReferenceNumber1"];
				if (frmBillPaymentView.hbox58862488827877.isVisible) {
					frmBillPaymentView.lblRef2value.text = result["CustomerBillInqRs"][0]["ReferenceNumber2"];
				}
				if (result["CustomerBillInqRs"][0]["BillerNickName"] != undefined && result["CustomerBillInqRs"][0][
					"BillerNickName"] != null && result["CustomerBillInqRs"][0]["BillerNickName"] != "") {
					if (gblbillerStatus == 0) {
						frmBillPaymentView.lblBillerNickName.setVisibility(false);
					} else {
						frmBillPaymentView.lblBillerNickName.text = result["CustomerBillInqRs"][0]["BillerNickName"];
					}
				} else {
					//If Not avialble , show blank
					frmBillPaymentView.lblBillerNickName.text = "-";
				}
			} else {
				// biller logo and short name from masterbillerInquiry (T.S)If My Biller/Top-Up is not found in CustomerBillerInquiry response
				frmBillPaymentView.lblBillerNickName.text = billerShortName;
				frmBillPaymentView.lblBillerNickName.setVisibility(false);
				frmBillPaymentView.lblRef1value.text = LabelReferenceNumber1;
				frmBillPaymentView.btnedit.setEnabled(false);
				frmBillPaymentView.btnedit.skin = "btnIBediticonsmall";
				frmBillPaymentView.hbox58862488827877.setVisibility(false);
			}
			forBPBillPmtInqMB();
		} else {
			//dismissLoadingScreen();
			
		}
	} else {
		if (status == 300) {
			
		}
	}
}
*/
function forBPBillPmtInqMB() {
	inputParam = [];
	var tranCodeMB;
	if (gblFromAccTypeMB == "DDA") {
		tranCodeMB = "88" + "10";
	} else {
		tranCodeMB = "88" + "20";
	}
	inputParam["compCode"] = gblBillerCompcodeEditMB; //get from masterbillInq
	inputParam["tranCode"] = tranCodeMB; // from paymentInq
	inputParam["fromAcctIdentValue"] = gblFromAccIdMB;
	inputParam["fromAcctTypeValue"] = gblFromAccTypeMB;
	inputParam["transferAmount"] = gblAmountMB;
	
	inputParam["pmtRefIdent"] = removeHyphenIB(frmBillPaymentView.lblRef1value.text);
	inputParam["invoiceNumber"] = "";
	inputParam["postedDate"] = gblstartOnMB; //changeDateFormatForService(gblstartOnMB);
	inputParam["ePayCode"] = "EPYS";
	inputParam["waiveCode"] = "I";
	inputParam["fIIdent"] = "";
	invokeServiceSecureAsync("billPaymentInquiry", inputParam, forBPbillPaymentInquiryServiceCallBackMB);
}

function forBPbillPaymentInquiryServiceCallBackMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["BillPmtInqRs"].length > 0 && result["BillPmtInqRs"] != null) {
				if (result["BillPmtInqRs"][0]["WaiveFlag"] == "Y") {
					frmBillPaymentView.lblPaymentFeeValue.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				} else {
					frmBillPaymentView.lblPaymentFeeValue.text = commaFormatted(result["BillPmtInqRs"][0]["FeeAmount"]) + " " + kony
						.i18n.getLocalizedString("currencyThaiBaht");
				}			
				forBPCallCustomerAccountInqMB();
			} else {
				dismissLoadingScreen();
				alert(result["StatusDesc"]);
			}
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	} else {
		if (status == 300) {
			
		}
	}
}

function checkMBStatusForEditBP() {
	var inputparam = [];
	inputparam["crmId"] = "";
	inputparam["rqUUId"] = "";
	showLoadingScreen();	
	invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForEditBPMB);
}
/*
   **************************************************************************************
		Module	: callBackCrmProfileInqForEditBPMB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCrmProfileInqForEditBPMB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var statusCode = result["statusCode"];
			var severity = result["Severity"];
			var statusDesc = result["StatusDesc"];
			if (statusCode != 0) {
				
			} else {
				var mbStat = result["mbFlowStatusIdRs"];
				var ibStat = result["ibUserStatusIdTr"];
				
				
				//var mbStat = "00";
				if (mbStat != null) {
					var inputParam = [];
					inputParam["mbStatus"] = mbStat; //check this inputparam
					inputParam["ibStatus"]= ibStat;
					invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckStatusMBForEditBP);
				}
			}
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreen();
			//
		}
	}
}
/*
   **************************************************************************************
		Module	: callBackCheckStatusMBForEditBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCheckStatusMBForEditBP(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (flowSpa) {
                if (result["ibStatusFlag"] == "true") {
                    //alert("SPA")
                    //showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
                    dismissLoadingScreen();
                    popTransferConfirmOTPLock.show();
                    
                    return false;
                } else {
                    
                    onClickEditButtonMB();
                }
            } else {
            	
                if (result["mbStatusFlag"] == "true") {
                  
                   // alert(kony.i18n.getLocalizedString("Error_UserStatusLocked")); //alert("Your transaction has been locked, so you can't Edit this transaction");
                     dismissLoadingScreen();
                     popTransferConfirmOTPLock.show();
                    return false;
                } else {
                    
                    onClickEditButtonMB();
                }

            }
        } else {
            dismissLoadingScreen();
            
        }
        //
    } else {
        if (status == 300) {
            dismissLoadingScreen();
            
        }
    }
}

// On click Edit button

function onClickEditButtonMB() {
	//alert("Edit button clicked ::: " +gblBPflag);

	if(gblBPflag){
		frmBillPaymentEdit.lblHead.text = kony.i18n.getLocalizedString("keyEditBillPaymentMB");
	}else{
		frmBillPaymentEdit.lblHead.text = kony.i18n.getLocalizedString("KeyMBEditTopUP");//"Edit Top UP";
	}
	// from Account details :
	frmBillPaymentEdit.imgFrom.src=frmBillPaymentView.imgFrom.src;
	frmBillPaymentEdit.imgBiller.src=frmBillPaymentView.imgBiller.src;
	frmBillPaymentEdit.lblFromAccountNickname.text = frmBillPaymentView.lblFromAccountNickname.text;
	frmBillPaymentEdit.lblFromAccountName.text = frmBillPaymentView.lblFromAccountName.text;
	frmBillPaymentEdit.lblFromAccountNumber.text = frmBillPaymentView.lblFromAccountNumber.text;
	// To biller details :
	frmBillPaymentEdit.hbxTPCusName.setVisibility(false);

	frmBillPaymentEdit.lblBillerName.text = frmBillPaymentView.lblBillerName.text;
	if (frmBillPaymentView.lblBillerNickName.isVisible) {
		frmBillPaymentEdit.lblBillerNickName.text = frmBillPaymentView.lblBillerNickName.text;
	} else {
		frmBillPaymentEdit.lblBillerNickName.setVisibility(false);
	}
	frmBillPaymentEdit.lblref1.text = frmBillPaymentView.lblref1.text;
	frmBillPaymentEdit.lblRef1value.text = frmBillPaymentView.lblRef1value.text;
	//below line is added for CR - PCI-DSS masked Credit card no
	frmBillPaymentEdit.lblRef1valueMasked.text = frmBillPaymentView.lblRef1valueMasked.text;
	if(gblBPflag){
		if (frmBillPaymentView.hbox58862488827877.isVisible) {
			frmBillPaymentEdit.lblref2.text = frmBillPaymentView.lblref2.text;
			frmBillPaymentEdit.lblRef2value.text = frmBillPaymentView.lblRef2value.text;
		} else {
			frmBillPaymentEdit.hbox58862488827905.setVisibility(false);
		}
	}else{
		frmBillPaymentEdit.hbox58862488827905.setVisibility(false);
		
		if(!frmBillPaymentEdit.hbox58853551515084.isVisible){
			frmBillPaymentEdit.hbox58853551515084.setVisibility(true); // to display total amount
	    }
	}
	//Payment details :
	frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
	frmBillPaymentEdit.lblPaymentDateValue.text = frmBillPaymentView.lblPaymentDateValue.text;
	frmBillPaymentEdit.lblPaymentFeeValue.text = frmBillPaymentView.lblPaymentFeeValue.text;
	//Schedule details :
	frmBillPaymentEdit.lblStartDate.text = frmBillPaymentView.lblStartDate.text;
	/*	if (!frmBillPaymentView.hbox58853551515104.isVisible){
		frmBillPaymentEdit.hbox58853551515104.setVisibility(false);
		frmBillPaymentEdit.hbox58853551515106.setVisibility(false);
		frmBillPaymentEdit.hbox58853551515107.setVisibility(false);
	} else{ */
	frmBillPaymentEdit.labelRepeatAsValue.text = frmBillPaymentView.lblrepeatasvalue.text;
	frmBillPaymentEdit.lblEndOnDate.text = frmBillPaymentView.lblEndOnDate.text;
	frmBillPaymentEdit.lblExecutetimes.text = frmBillPaymentView.lblExecutetimes.text;
	//	}
	//My note :
	frmBillPaymentEdit.lblMyNotesDesc.text = frmBillPaymentView.lblMyNotesDesc.text;
	//scheduled ref num :
	frmBillPaymentEdit.lblScheduleRefValue.text=frmBillPaymentView.lblScheduleRefValue.text;
	
	 frmBillPaymentEdit.lblAvailableBalValue.text = commaFormatted(gblAvailableBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    
    
    if(gblBPflag){
    
		if(frmBillPaymentEdit.hbox58788011388368.isVisible){			
			frmBillPaymentEdit.hbox58788011388368.setVisibility(false);			//ComboBox for topup
	    }
    
    	if (gblEditBillMethodMB == 0 && gblEditBillerGroupTypeMB == 0) {
    		var activityTypeID = "067";
        	var errorCode = "";
        	var activityStatus = "00";
        	var deviceNickName = "";
        	var activityFlexValues1 = "Edit";
        	var activityFlexValues2 = frmBillPaymentEdit.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentEdit.lblRef1value.text);    //Biller Name + Ref1
			var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
			var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
			var activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)		
       		var logLinkageId = "";
        	frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
        	frmBillPaymentEdit.txtAmtInput.setEnabled(true);
        	frmBillPaymentEdit.txtAmtInput.setVisibility(true);
        	
        	//New changes to handle visibility as per biller type
        	
        		if (frmBillPaymentEdit.hboxOnlineAmt.isVisible) {
					frmBillPaymentEdit.hboxOnlineAmt.setVisibility(false); // to display amount
				}
				if(frmBillPaymentEdit.hboxPenaltyAmt.isVisible){
					frmBillPaymentEdit.hboxPenaltyAmt.setVisibility(false);	// to display penalty amount
				}	
				if(frmBillPaymentEdit.hbox588565306196684.isVisible){
						frmBillPaymentEdit.hbox588565306196684.setVisibility(false); // to display buttons
				}
				if(!frmBillPaymentEdit.hbox58853551515084.isVisible){
						frmBillPaymentEdit.hbox58853551515084.setVisibility(true); // to display txtAmntInput amount
				}
				
				frmBillPaymentEdit.txtamountvalue.setVisibility(false);
				
				if (frmBillPaymentEdit.hbox58862488825172.isVisible) {
					frmBillPaymentEdit.hbox58862488825172.setVisibility(false);		//3 buttons bbox
				}
				
        	//--
			
        	
        	dismissLoadingScreen();
        	//activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	       	frmBillPaymentEdit.show();
    	} else if (gblEditBillMethodMB == 1 && gblEditBillerGroupTypeMB == 0) {
        	var inputparam = [];
        	inputparam["rqUID"] = "";
        	inputparam["EffDt"] = "";
        	inputparam["TrnId"] = ""; // change this value when integrated with calender / future transactions page
        	inputparam["Amt"] = "0.00"; /// for billpayment it is 0.00
        	inputparam["compCode"] = gblBillerCompcodeEditMB;
        	//inputparam["TranCode"] = gblTransCodeMB;
        	//inputparam["AcctId"] = gblFromAccIdMB;
        	//inputparam["AcctTypeValue"] = gblFromAccTypeMB;
        	inputparam["MobileNumber"] = removeHyphenIB(frmBillPaymentView.lblRef1value.text);
			inputparam["BankId"]="011";
			inputparam["BranchId"]= "0001";
			
			
        	invokeServiceSecureAsync("onlinePaymentInqForEditBP", inputparam, forBPMBOnlinePaymentInquiryServiceCallBack);
    	} else if (gblEditBillMethodMB == 2 && gblEditBillerGroupTypeMB == 0) {
       	 	var inputparam = [];
        	inputparam["cardId"] = gblRef1ValueBPMB; //"00000000"+removeHyphenIB(frmBillPaymentView.lblRef1value.text);
        	inputparam["waiverCode"] = "I";
        	inputparam["tranCode"] = TRANSCODEMIN; //"8290";
        	inputparam["postedDt"] = getTodaysDate(); // current date you inq for smt
        	inputparam["rqUUId"] = "";
        	var cardId  = gblRef1ValueBPMB;
        	gblownCardBPMB = false;
        	var accountLength =gblAccountTable["custAcctRec"].length;
    		for(var i=0;i < accountLength;i++){
				if(gblAccountTable["custAcctRec"][i].accType=="CCA"){
					var accountNo = gblAccountTable["custAcctRec"][i].accId;
					accountNo = accountNo.substring(accountNo.length-16 , accountNo.length);
					if(accountNo == cardId){
						gblownCardBPMB =true;
						break;
					}
				}
    		}
        	
        	if (frmBillPaymentEdit.hboxOnlineAmt.isVisible) {
					frmBillPaymentEdit.hboxOnlineAmt.setVisibility(false); // to display amount
			 }
			if(frmBillPaymentEdit.hboxPenaltyAmt.isVisible){
				frmBillPaymentEdit.hboxPenaltyAmt.setVisibility(false);	// to display penalty amount
			}	
        	
        	
        	if(gblownCardBPMB){
        		invokeServiceSecureAsync("creditcardDetailsInq", inputparam, forBPMBCreditCardDetailsInqServiceCallBack);
        	}else{
        		dismissLoadingScreen();
        		if (frmBillPaymentEdit.hbox58862488825172.isVisible) 
					frmBillPaymentEdit.hbox58862488825172.setVisibility(false);
				
				if (!frmBillPaymentEdit.hbox58853551515084.isVisible) 
					frmBillPaymentEdit.hbox58853551515084.setVisibility(true);
				
				if (frmBillPaymentEdit.txtamountvalue.isVisible) 
					frmBillPaymentEdit.txtamountvalue.setVisibility(false);
				
				if(frmBillPaymentEdit.hbox588565306196684.isVisible)
						frmBillPaymentEdit.hbox588565306196684.setVisibility(false); 
						
				frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
				dismissLoadingScreen();	
				frmBillPaymentEdit.show();
        		
        	}
        	
    	} else if (gblEditBillMethodMB == 3) {
			if (frmBillPaymentEdit.hbox58862488825172.isVisible) 
				frmBillPaymentEdit.hbox58862488825172.setVisibility(false);
			
			if (!frmBillPaymentEdit.hbox58853551515084.isVisible) 
				frmBillPaymentEdit.hbox58853551515084.setVisibility(true);
			
			if (frmBillPaymentEdit.txtamountvalue.isVisible) 
				frmBillPaymentEdit.txtamountvalue.setVisibility(false);
    	
        	var inputparam = [];
        	var ref2 = frmBillPaymentView.lblRef2value.text
        	var acctId = "0"+removeHyphenIB(frmBillPaymentView.lblRef1value.text)+ref2;
        	inputparam["acctId"] = "0"+removeHyphenIB(frmBillPaymentView.lblRef1value.text)+ref2;
        	inputparam["acctType"] = "LOC";   //kony.i18n.getLocalizedString("Loan");
        	inputparam["rqUUId"] = "";
        	var ownAccnt = false;
        	var accountLength =gblAccountTable["custAcctRec"].length;
    		for(var i=0;i < accountLength;i++){
				if(gblAccountTable["custAcctRec"][i].accType=="LOC"){
					var accountNo = gblAccountTable["custAcctRec"][i].accId;
					if(accountNo == acctId){
						ownAccnt =true;
						break;
					}
				}
    		}
        	
        	if (frmBillPaymentEdit.hboxOnlineAmt.isVisible) {
					frmBillPaymentEdit.hboxOnlineAmt.setVisibility(false); // to display amount
			 }
			if(frmBillPaymentEdit.hboxPenaltyAmt.isVisible){
				frmBillPaymentEdit.hboxPenaltyAmt.setVisibility(false);	// to display penalty amount
			}
        	
        	
        	if(ownAccnt){
	        	invokeServiceSecureAsync("doLoanAcctInq", inputparam, forBPMBLoanAccountInquiryServiceCallBack);
        	}else{
        		if(frmBillPaymentEdit.hbox588565306196684.isVisible)
						frmBillPaymentEdit.hbox588565306196684.setVisibility(false); // to display buttons full/spec
        		
        		if (!frmBillPaymentEdit.hbox58853551515084.isVisible) 
						frmBillPaymentEdit.hbox58853551515084.setVisibility(true);
        		
        		frmBillPaymentEdit.txtAmtInput.setVisibility(true);
        		frmBillPaymentEdit.txtAmtInput.setEnabled(true);
        		frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
				dismissLoadingScreen();
				frmBillPaymentEdit.show();
        	}
        	
    	}else{
    		dismissLoadingScreen();
    		alert("you can't edit this Bill Payment ");
    	}
    }else{
    	var activityTypeID = "068";
        var errorCode = "";
        var activityStatus = "00";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmBillPaymentEdit.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentEdit.lblRef1value.text);    //Biller Name + Ref1
		var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)		
       	var logLinkageId = "";
  		//activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	  
	  
	  //New changes to handle visibility as per BP/TP
        	
    		if (frmBillPaymentEdit.hboxOnlineAmt.isVisible) {
				frmBillPaymentEdit.hboxOnlineAmt.setVisibility(false); // to display amount
			}
			if(frmBillPaymentEdit.hboxPenaltyAmt.isVisible){
				frmBillPaymentEdit.hboxPenaltyAmt.setVisibility(false);	// to display penalty amount
			}	
			if(frmBillPaymentEdit.hbox588565306196684.isVisible){
					frmBillPaymentEdit.hbox588565306196684.setVisibility(false); // to display buttons
			}
			if(!frmBillPaymentEdit.hbox58853551515084.isVisible){
					frmBillPaymentEdit.hbox58853551515084.setVisibility(true); // to display total amount
			}
			
			frmBillPaymentEdit.txtamountvalue.setVisibility(false);
			
			if (frmBillPaymentEdit.hbox58862488825172.isVisible) {
				frmBillPaymentEdit.hbox58862488825172.setVisibility(false);		//3 buttons hbox BP
			}
				
      //--
	  
	  
	  //Diaplay of customer name for easy pass 
		
			if( gblBillerCompcodeEditMB == "2151" && frmBillPaymentView.hbxTPCusName.isVisible){
				frmBillPaymentEdit.hbxTPCusName.setVisibility(true);
				frmBillPaymentEdit.lblTPCustomerName.text = "Easy Pass Customer Name";
				frmBillPaymentEdit.lblTPCustomerNameVal.text = frmBillPaymentView.lblTPCustomerNameVal.text;
			}else
				frmBillPaymentEdit.hbxTPCusName.setVisibility(false);
		
	  
	    if (gblEditBillMethodMB == 0 && gblEditBillerGroupTypeMB == 1) {
        	frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
        	
        	if(frmBillPaymentEdit.hbox58788011388368.isVisible){
        		frmBillPaymentEdit.hbox58788011388368.setVisibility(false);
       		 }
       		 
       		 if(!frmBillPaymentEdit.hbox58853551515084.isVisible){
        		frmBillPaymentEdit.hbox58853551515084.setVisibility(true);
       		 }
        	
        	frmBillPaymentEdit.txtAmtInput.setEnabled(true);
        	frmBillPaymentEdit.txtAmtInput.setVisibility(true);
        	
        	frmBillPaymentEdit.show();
        	dismissLoadingScreen();
    	} else if (gblEditBillMethodMB == 1 && gblEditBillerGroupTypeMB == 1) {
    		//alert("online Top Up");
        	if(!frmBillPaymentEdit.hbox58788011388368.isVisible){
        		frmBillPaymentEdit.hbox58788011388368.setVisibility(true);
       		 }
       		 if(frmBillPaymentEdit.hbox58853551515084.isVisible){
        		frmBillPaymentEdit.hbox58853551515084.setVisibility(false);
       		 }
       		 
       		 
       		frmBillPaymentEdit.cbTPAmountVal.masterData = comboDataEditTPMB;
       		if(flowSpa)
       		{
       			frmBillPaymentEdit.cbTPAmountVal.selectedKey="0";
       		}
       		else
       		{
       	 		frmBillPaymentEdit.cbTPAmountVal.selectedKey=0;
       	 	}
       	 	
        	frmBillPaymentEdit.show();
        	dismissLoadingScreen();
   	 	} else if (gblEditBillMethodMB == 2 && gblEditBillerGroupTypeMB == 1) {
   	 		dismissLoadingScreen();
   	 		
   	 		if(frmBillPaymentEdit.hbox58788011388368.isVisible){
         		frmBillPaymentEdit.hbox58788011388368.setVisibility(false);
       	 	 }
       	 	 if(!frmBillPaymentEdit.hbox58853551515084.isVisible){
         		frmBillPaymentEdit.hbox58853551515084.setVisibility(true);
       	 	 }
   	 		
      		frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
      		
      		frmBillPaymentEdit.txtAmtInput.setEnabled(true);
        	frmBillPaymentEdit.txtAmtInput.setVisibility(true);
        	
      		frmBillPaymentEdit.show();
    	} else{
    		dismissLoadingScreen();
    		alert("you can't edit this transaction ");
    	}
   }
	//frmBillPaymentEdit.show();
}

function forBPMBOnlinePaymentInquiryServiceCallBack(status, result) {
	gblMinAmount = "";
	gblMaxAmount = "";
	gblPayFull = "";
	if (status == 400) {
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmBillPaymentEdit.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentEdit.lblRef1value.text);    //Biller Name + Ref1
		var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB);//gblstartOnMB; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)		
       	var logLinkageId = "";
		
		
		if (result["opstatus"] == 0) {
			if ( result["OnlinePmtInqRs"] != undefined && result["OnlinePmtInqRs"].length > 0 && result["OnlinePmtInqRs"] != null) {
				activityStatus = "00";
				if (!frmBillPaymentEdit.hboxOnlineAmt.isVisible) {
					frmBillPaymentEdit.hboxOnlineAmt.setVisibility(true); // to display amount
				}
				if(!frmBillPaymentEdit.hboxPenaltyAmt.isVisible){
					frmBillPaymentEdit.hboxPenaltyAmt.setVisibility(true);	// to display penalty amount
				}	
				if(!frmBillPaymentEdit.hbox588565306196684.isVisible){
						frmBillPaymentEdit.hbox588565306196684.setVisibility(true); // to display buttons
				}
				if(!frmBillPaymentEdit.hbox58853551515084.isVisible){
						frmBillPaymentEdit.hbox58853551515084.setVisibility(true); // to display total amount
				}
				if (frmBillPaymentEdit.hbox58862488825172.isVisible) {
					frmBillPaymentEdit.hbox58862488825172.setVisibility(false);
				}
			    if (frmBillPaymentEdit.txtamountvalue.isVisible) {
				frmBillPaymentEdit.txtamountvalue.setVisibility(false);
			    }
				var local = kony.i18n.getCurrentLocale();
				var fullAmtValueOnline = "";
				if(result["OnlinePmtMiscDataDisp"].length >0 && result["OnlinePmtMiscDataDisp"] != null){
					for(i=0; i < result["OnlinePmtMiscDataDisp"].length ;i++ ){
						if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "BilledAmt"){
							var billedAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
							var billedAmtLabelTH = result["OnlinePmtMiscDataDisp"][i]["MiscName_TH"];
							var billedAmtValue = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
							if (local == "en_US") {
								frmBillPaymentEdit.labelOnlineAmt.text = billedAmtLabelEN;
							}else if(local == "th_TH"){
								frmBillPaymentEdit.labelOnlineAmt.text = billedAmtLabelTH;
							}
							frmBillPaymentEdit.labelOnlineAmtValue.text = commaFormatted(billedAmtValue);
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "FullAmt"){
							//var fullAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
							//var fullAmtLabelTH = result["OnlinePmtMiscDataDisp"][i]["MiscName_TH"];
							fullAmtValueOnline = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
							//frmIBBillPaymentView.labelPenalty.text = fullAmtLabelEN;
							//frmIBBillPaymentView.textboxOnlineTotalAmt.text = fullAmtValue;
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "PenaltyAmt"){
							var penaltyAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
							var penaltyAmtLabelTH = result["OnlinePmtMiscDataDisp"][i]["MiscName_TH"];
							var penaltyAmtValue = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
							if (local == "en_US") {
								frmBillPaymentEdit.labelPenaltyAmt.text = penaltyAmtLabelEN;
							}else if(local == "th_TH"){
								frmBillPaymentEdit.labelPenaltyAmt.text = penaltyAmtLabelTH;
							}
							frmBillPaymentEdit.labelPenaltyAmtValue.text = commaFormatted(penaltyAmtValue);
						}//Getting Min Max amount from the service 
						else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "Minimum"){
							gblMinAmount = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "Maximum"){
							gblMaxAmount = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
						}   
					}
				
				}else{
					//New 
						
					var arrayTemp = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"];
					if(arrayTemp != undefined && arrayTemp.length > 0){
						for(var i=0; i < arrayTemp.length ;i++ ){
							if(arrayTemp[i]["MiscName"] == "BilledAmt"){
								var billedAmtValue = arrayTemp[i]["MiscText"];
								frmBillPaymentEdit.labelOnlineAmtValue.text = commaFormatted(billedAmtValue)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
							}else if(arrayTemp[i]["MiscName"] == "FullAmt"){
								fullAmtValueOnline = arrayTemp[i]["MiscText"];
							}else if(arrayTemp[i]["MiscName"] == "PenaltyAmt"){
								var penaltyAmtValue = arrayTemp[i]["MiscText"];
								frmBillPaymentEdit.labelPenaltyAmtValue.text = commaFormatted(penaltyAmtValue)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
							}//Get the min max value here
							else if(arrayTemp[i]["MiscName"] == "Minimum"){
								gblMinAmount = arrayTemp[i]["MiscText"];
							}else if(arrayTemp[i]["MiscName"] == "Maximum"){
								gblMaxAmount = arrayTemp[i]["MiscText"];
							}   
						}
					}	
					//	
				}
				//frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
				gblFullAmtVal = fullAmtValueOnline;
				
				
				
				if(parseFloat(fullAmtValueOnline.toString()) == parseFloat(frmBillPaymentView.lblamtvalue.text)){
					frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
					frmBillPaymentEdit.buttonOnlineFull.skin = 	"btnScheduleEndLeftFocus";
					frmBillPaymentEdit.buttonOnlineSpecified.skin ="btnScheduleRight";
					frmBillPaymentEdit.txtAmtInput.setEnabled(false);
					
				}else{
					frmBillPaymentEdit.buttonOnlineFull.skin = "btnScheduleEndLeft";
					frmBillPaymentEdit.buttonOnlineSpecified.skin ="btnScheduleRightFocus";
					
					frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
					//frmBillPaymentEdit.txtamountvalue.text = frmBillPaymentView.lblamtvalue.text;
					frmBillPaymentEdit.txtAmtInput.setEnabled(true);
					//frmBillPaymentEdit.txtamountvalue.setVisibility(true);
					frmBillPaymentEdit.txtAmtInput.setVisibility(true);
				}
				
				gblPayFull = result["isFullPayment"];
				if(gblPayFull == "Y") {
					frmBillPaymentEdit.hbox588565306196684.setVisibility(false);
					frmBillPaymentEdit.txtAmtInput.setEnabled(false);
				}
				dismissLoadingScreen();
				frmBillPaymentEdit.show();
			} else {
				activityStatus = "02";
				//dismissLoadingScreenPopup();
				dismissLoadingScreen();
				
				alert(result["errMsg"]);
			}

		}else {
			activityStatus = "02";
			//dismissLoadingScreenPopup();
			dismissLoadingScreen();
			
			alert(result["errMsg"]);
		}
		//activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}

function forBPMBCreditCardDetailsInqServiceCallBack(status, result) {
	if (status == 400) {
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmBillPaymentEdit.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentEdit.lblRef1value.text);    //Biller Name + Ref1
		var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)		
       	var logLinkageId = "";	
		if (result["opstatus"] == 0) {
			activityStatus = "00";
			fullAmountMB = "";
			minAmountMB = "";
			
			//
			if (result["fullPmtAmt"] != "0.00" && result["fullPmtAmt"] != undefined) {
				fullAmountMB = commaFormatted(result["fullPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				if(parseInt(fullAmountMB)<0)gblPaymentOverpaidFlag=true;
			} else {
				fullAmountMB = result["fullPmtAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			if (result["minPmtAmt"] != "0.00" && result["minPmtAmt"] != undefined) {
				minAmountMB = commaFormatted(result["minPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				minAmountMB = result["minPmtAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			
			
			if (!frmBillPaymentEdit.hbox58862488825172.isVisible) {
				frmBillPaymentEdit.hbox58862488825172.setVisibility(true);
			}
			if (frmBillPaymentEdit.hbox58853551515084.isVisible) {
			 	frmBillPaymentEdit.hbox58853551515084.setVisibility(false);
			}
			if (!frmBillPaymentEdit.txtamountvalue.isVisible) {
				frmBillPaymentEdit.txtamountvalue.setVisibility(true);
			}
			
			if(frmBillPaymentEdit.hbox588565306196684.isVisible)
				frmBillPaymentEdit.hbox588565306196684.setVisibility(false);
			
			
			frmBillPaymentEdit.btnspecpay.skin = btnScheduleRightFocus;
			frmBillPaymentEdit.btnminpay.skin = btnScheduleMid;
			frmBillPaymentEdit.btnfullpay.skin = btnScheduleLeft;
			//enable specified button and amt text
			frmBillPaymentEdit.txtamountvalue.setEnabled(true);
			frmBillPaymentEdit.txtamountvalue.text = frmBillPaymentView.lblamtvalue.text;
			dismissLoadingScreen();	
			frmBillPaymentEdit.show();
			paymentOverpaidCheck();
		}else{
			activityStatus = "02";
			dismissLoadingScreen();
			alert(result["errMsg"]);
			return false;
		}
		//activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}

function forBPMBLoanAccountInquiryServiceCallBack(status,result){


	if(status == 400){
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmBillPaymentEdit.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentEdit.lblRef1value.text);    //Biller Name + Ref1
		var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)		
       	var logLinkageId = "";
		if(result["opstatus"] == 0){
			activityStatus = "00";
			// display full and specified amt
			fullAmtLoanMB = "";
			if (result["regPmtCurAmt"] != "0.00") {
				fullAmtLoanMB = commaFormatted(result["regPmtCurAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				//	frmIBBillPaymentView.textboxOnlineTotalAmt.text = commaFormatted(result["regPmtCurAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				fullAmtLoanMB = result["regPmtCurAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				//	frmIBBillPaymentView.textboxOnlineTotalAmt.text = result["regPmtCurAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			
			if(!frmBillPaymentEdit.hbox588565306196684.isVisible)
						frmBillPaymentEdit.hbox588565306196684.setVisibility(true); // to display buttons full/spec
			
			if(!frmBillPaymentEdit.hbox58853551515084.isVisible)			
			     frmBillPaymentEdit.hbox58853551515084.setVisibility(true);			//hbox for txtAmtInput
				
			frmBillPaymentEdit.txtamountvalue.text = frmBillPaymentView.lblamtvalue.text;
			var fulAmt = fullAmtLoanMB;
			var amt = frmBillPaymentView.lblamtvalue.text;
			fulAmt = fulAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(fulAmt.indexOf(",") != -1)fulAmt = replaceCommon(fulAmt, ",", "");
			if(amt.indexOf(",") != -1)amt = replaceCommon(amt, ",", "");
			
			
			
			if(parseFloat(fulAmt) == parseFloat(amt)){
				frmBillPaymentEdit.buttonOnlineFull.skin = "btnScheduleEndLeftFocus";
				frmBillPaymentEdit.buttonOnlineSpecified.skin ="btnScheduleRight";
			}else{
				frmBillPaymentEdit.buttonOnlineFull.skin = "btnScheduleEndLeft";
				frmBillPaymentEdit.buttonOnlineSpecified.skin ="btnScheduleRightFocus";
			}
			frmBillPaymentEdit.txtAmtInput.text = frmBillPaymentView.lblamtvalue.text;
			frmBillPaymentEdit.txtAmtInput.setEnabled(false);
			dismissLoadingScreen();
			frmBillPaymentEdit.show();
		}else {
			activityStatus = "02";
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}

function onClickScheduleButtonMB() {
	
	
	frmScheduleBillPayEditFuture.calScheduleStartDate.dateComponents = dateFormatForDateComp(frmBillPaymentEdit.lblStartDate.text);
	//alert(" start date : " +frmScheduleBillPayEditFuture.calScheduleStartDate.dateComponents);
	//alert(" start date mapping :: " +frmBillPaymentEdit.lblStartDate.text);
	frmScheduleBillPayEditFuture.calScheduleStartDate.validStartDate = currentDateForcalender();
	frmScheduleBillPayEditFuture.calScheduleEndDate.validStartDate = currentDateForcalender();
	
	if (repeatAsMB == "" && endFreqSaveMB == "") {
		onLoadShowRepeatFreqMB(gblOnLoadRepeatAsMB);
		onLoadShowEndFreqMB(gblEndingFreqOnLoadMB);
	} else {
		onLoadShowRepeatFreqMB(repeatAsMB);
		onLoadShowEndFreqMB(endFreqSaveMB);
	}
	
	//New
	gblTemprepeatAsTPMB = repeatAsMB;
	gblTempendFreqSaveTPMB = endFreqSaveMB;
	
	frmScheduleBillPayEditFuture.show();
}

function onLoadShowEndFreqMB(endingFreqMB) {
	if(OnClickRepeatAsMB == "Once"){
		disableEndAfterBtnHolderBPEdit();
	}else{
		if ((endingFreqMB == kony.i18n.getLocalizedString("keyOnDate")) || (endingFreqMB == "On Date")) { //kony.i18n.getLocalizedString("keyOnDate")
			frmScheduleBillPayEditFuture.calScheduleEndDate.dateComponents = dateFormatForDateComp(frmBillPaymentEdit.lblEndOnDate.text);
			setFocusOnEndOnDateBPEdit();
		} else if ((endingFreqMB == kony.i18n.getLocalizedString("keyAfter")) || (endingFreqMB == "After")) { //kony.i18n.getLocalizedString("keyAfter")
			frmScheduleBillPayEditFuture.tbxAfterTimes.text = frmBillPaymentEdit.lblExecutetimes.text;
			setFocusOnEndAfterBPEdit();
		} else if ((endingFreqMB == kony.i18n.getLocalizedString("keyNever"))|| (endingFreqMB == "Never")) { //kony.i18n.getLocalizedString("keyNever")
			setFocusOnEndNeverBPEdit();
		}
	}
}

function onLoadShowRepeatFreqMB(repeatFreqMB) {
	disableRepeatBtnHolderBPEdit();
	setRepeatClickedToFalse();
	if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyDaily")) || kony.string.equalsIgnoreCase(repeatFreqMB, "Daily")) { //kony.i18n.getLocalizedString("keyDaily")
		frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
		frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
		frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
		frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleLeftFocus";
		OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyDaily");
		DailyClicked = true;
	} else if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyWeekly")) || kony.string.equalsIgnoreCase(repeatFreqMB, "Weekly")) { //kony.i18n.getLocalizedString("keyWeekly")
		frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
		frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
		frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
		frmScheduleBillPayEditFuture.btnWeekly.skin = "btnScheduleMidFocus";
		OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyWeekly");
		weekClicked = true;
	} else if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyMonthly"))|| kony.string.equalsIgnoreCase(repeatFreqMB, "Monthly")) { //kony.i18n.getLocalizedString("keyMonthly")
		frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
		frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
		frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
		frmScheduleBillPayEditFuture.btnMonthly.skin = "btnScheduleMidFocus";
		OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyMonthly");
		monthClicked = true;
	} else if (kony.string.equalsIgnoreCase(repeatFreqMB, kony.i18n.getLocalizedString("keyYearly")) || kony.string.equalsIgnoreCase(repeatFreqMB, "Yearly")) { //kony.i18n.getLocalizedString("keyYearly")
		frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
		frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
		frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
		frmScheduleBillPayEditFuture.btnYearly.skin = "btnScheduleRightFocus";
		OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyYearly");
		YearlyClicked = true;
	} else {
		OnClickRepeatAsMB = "Once";
		frmScheduleBillPayEditFuture.lblEnd.setVisibility(false);
		frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(false);
		frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
		frmScheduleBillPayEditFuture.hbxEndAfter.setVisibility(false);
		frmScheduleBillPayEditFuture.hboxUntil.setVisibility(false);
	
	}
}

function dateFormatForDateComp(date) {
	var currDate = new Date(GLOBAL_TODAY_DATE);
   	var currDate_yyyy = currDate.getFullYear();
   	var diff_yyyy = 0;
   	
	//Day format 12/07/2013
	var dateSplit = date.split("/");
	var dd = dateSplit[0];
	var mm = dateSplit[1];
	var yyyy = dateSplit[2];
	diff_yyyy = dateSplit[2] - currDate_yyyy;
	//var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
     	var returnedValue = iPhoneCalendar.getDeviceDateLocale();
       	var deviceDate = returnedValue.split("|")[0];
       	var deviceDate_yyyy = deviceDate.substr(0,4);
       	yyyy = parseInt(deviceDate_yyyy) + diff_yyyy;
    }
	var formattedDate = [dd, mm, yyyy, "0", "0", "0"]; // discuss about this format display 
	return formattedDate;
}
var OnClickRepeatAsMB = "";
var OnClickEndFreqMB = "";

function repeatScheduleFrequencyMB(eventObject) {
	var btnEventId = eventObject.id;
	if (kony.string.equalsIgnoreCase(btnEventId, "btnDaily")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleLeftFocus")){
			frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleLeft";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleLeftFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyDaily");
			DailyClicked = true;
		}
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnWeekly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")){
			frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleMid";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnWeekly.skin = "btnScheduleMidFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyWeekly");
			weekClicked = true;
		}
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnMonthly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleMidFocus")){
			frmScheduleBillPayEditFuture.btnMonthly.skin = "btnScheduleMid";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnMonthly.skin = "btnScheduleMidFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyMonthly");
			monthClicked = true;
		}
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnYearly")) {
		if(kony.string.equalsIgnoreCase(eventObject.skin, "btnScheduleRightFocus")){
			frmScheduleBillPayEditFuture.btnYearly.skin = "btnScheduleRight";
			disableRepeatBtnHolderBPEdit();
			disableEndAfterBtnHolderBPEdit();
			setRepeatClickedToFalse();
		}else{
			if(isScheduleFirstShow()){
    			enableEndAfterBtnHolderBPEdit();
    		}
			disableRepeatBtnHolderBPEdit();
			frmScheduleBillPayEditFuture.btnYearly.skin = "btnScheduleRightFocus";
			OnClickRepeatAsMB = kony.i18n.getLocalizedString("keyYearly");
			YearlyClicked = true;
		}
	}
}

function endingScheduleFrequencyMB(eventObject) {
	var btnEventId = eventObject.id;
	disableEndAfterBtnHolderBPEdit();
	if (kony.string.equalsIgnoreCase(btnEventId, "btnNever")) {
		setFocusOnEndNeverBPEdit();
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnAfter")) {
		setFocusOnEndAfterBPEdit();
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnOnDate")) {
		setFocusOnEndOnDateBPEdit();
	}
}

function enableEndAfterBtnHolderBPEdit(){
	setFocusOnEndNeverBPEdit();
}
function setFocusOnEndNeverBPEdit(){
	OnClickEndFreqMB = "Never";
	frmScheduleBillPayEditFuture.tbxAfterTimes.text = "";
	frmScheduleBillPayEditFuture.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmScheduleBillPayEditFuture.calScheduleStartDate.dateComponents);
	frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
	frmScheduleBillPayEditFuture.lineUntil.setVisibility(false);
	frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
	frmScheduleBillPayEditFuture.btnNever.skin = "btnScheduleEndLeftFocus";
	frmScheduleBillPayEditFuture.btnAfter.skin = "btnScheduleEndMid";
	frmScheduleBillPayEditFuture.btnOnDate.skin = "btnScheduleEndRight";
	
}
function setFocusOnEndAfterBPEdit(){
	OnClickEndFreqMB =  "After";
	frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
	frmScheduleBillPayEditFuture.hboxtimes.setVisibility(true);
	frmScheduleBillPayEditFuture.hbxEndAfter.setVisibility(true);
	frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
	frmScheduleBillPayEditFuture.calScheduleEndDate.dateComponents = getDefaultEndDateEditFT(frmScheduleBillPayEditFuture.calScheduleStartDate.dateComponents);
	frmScheduleBillPayEditFuture.btnNever.skin = "btnScheduleEndLeft";
	frmScheduleBillPayEditFuture.btnAfter.skin = "btnScheduleEndMidFocus";
	frmScheduleBillPayEditFuture.btnOnDate.skin = "btnScheduleEndRight";
	frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
	
}
function setFocusOnEndOnDateBPEdit(){
	OnClickEndFreqMB = "On Date";
	frmScheduleBillPayEditFuture.lblEnd.setVisibility(true);
	frmScheduleBillPayEditFuture.tbxAfterTimes.text = "";
	frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(true);
	frmScheduleBillPayEditFuture.hboxUntil.setVisibility(true);
	frmScheduleBillPayEditFuture.hbxEndOnDate.setVisibility(true);
	frmScheduleBillPayEditFuture.lineUntil.setVisibility(true);
	frmScheduleBillPayEditFuture.btnNever.skin = "btnScheduleEndLeft";
	frmScheduleBillPayEditFuture.btnAfter.skin = "btnScheduleEndMid";
	frmScheduleBillPayEditFuture.btnOnDate.skin = "btnScheduleRightFocus";
}
function disableRepeatBtnHolderBPEdit(){
	OnClickRepeatAsMB = "Once";
	frmScheduleBillPayEditFuture.btnDaily.skin = "btnScheduleLeft";
	frmScheduleBillPayEditFuture.btnWeekly.skin = "btnScheduleMid";
	frmScheduleBillPayEditFuture.btnMonthly.skin = "btnScheduleMid";
	frmScheduleBillPayEditFuture.btnYearly.skin = "btnScheduleRight";
}

function disableEndAfterBtnHolderBPEdit(){
	frmScheduleBillPayEditFuture.lineUntil.setVisibility(false);
	frmScheduleBillPayEditFuture.hboxUntil.setVisibility(false);
	frmScheduleBillPayEditFuture.hbxEndAfterButtonHolder.setVisibility(false);
	frmScheduleBillPayEditFuture.lblEnd.setVisibility(false);
	frmScheduleBillPayEditFuture.hbxEndOnDate.setVisibility(false);
	frmScheduleBillPayEditFuture.hbxEndAfter.setVisibility(false);
	disableEndBtnHolderBPEdit();
}
function disableEndBtnHolderBPEdit(){
	OnClickEndFreqMB = "";
	frmScheduleBillPayEditFuture.btnNever.skin = "btnScheduleEndLeft";
	frmScheduleBillPayEditFuture.btnAfter.skin = "btnScheduleEndMid";
	frmScheduleBillPayEditFuture.btnOnDate.skin = "btnScheduleEndRight";
}

//
gblScheduleFreqChangedMB = false;
var repeatAsMB = "";
var endFreqSaveMB = "";
function onClickSaveSchedulefrm() {
	var scheduleRepeatasEditMBFlag = false;
	var scheduleEndingEditMBFlag = false;
	var scheduleStartOnEditMBFlag = false;
	var startOnDateMB = frmScheduleBillPayEditFuture.calScheduleStartDate.formattedDate;
	startOnDateMB = getFormattedDate(startOnDateMB, kony.i18n.getCurrentLocale());
	var executionNumberOftimesMB = "";
	var endOnDateMB = "";
	//check if local variable is updated with the latest value else work with global variable
	//OnClickRepeatAsMB - repeat as (daily,weekly,monthly,yearly) -local variable
	//OnClickEndFreqMB - end freq (never,after and ondate) - local variable
	//gblOnLoadRepeatAsMB - repeat As - global variable
	//gblEndingFreqOnLoadMB - end freq - global variable
	if (OnClickRepeatAsMB != gblOnLoadRepeatAsMB && OnClickRepeatAsMB != "") {
		scheduleRepeatasEditMBFlag = true;
	} else {
		scheduleRepeatasEditMBFlag = false;
	}
	if (OnClickEndFreqMB != gblEndingFreqOnLoadMB && OnClickEndFreqMB != "") {
		scheduleEndingEditMBFlag = true;
	} else {
		scheduleEndingEditMBFlag = false;
	}
	if (OnClickRepeatAsMB != "") {
		repeatAsMB = OnClickRepeatAsMB;
	} else {
		repeatAsMB = gblOnLoadRepeatAsMB;
	}
	if (OnClickEndFreqMB != "") {
		endFreqSaveMB = OnClickEndFreqMB;
	} else {
		endFreqSaveMB = gblEndingFreqOnLoadMB;
	}
	
	if (repeatAsMB != "Once") {
		if (endFreqSaveMB == "") {
			//alert(" Please select the ending frequency");
			alert(kony.i18n.getLocalizedString("Error_NoEndingValueSelected"));
			repeatAsMB = "";
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			return false;
		}
	}
	
	if(startOnDateMB == null || startOnDateMB == ""){
		//alert("Please select valid start date"); 
		alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
		return false;
	}else{
		var curDt = currentDate();
		if( parseDate(curDt) >= parseDate(startOnDateMB)){ 
			alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
			return false;
		}
	}
	
	frmBillPaymentEdit.lblStartDate.text = startOnDateMB;
	//frmBillPaymentEdit.lblStartDate.text = startOnDateMB;
	// New changes for once display scenario
	if(repeatAsMB == "Once"){
		frmBillPaymentEdit.lblEndOnDate.text = frmBillPaymentEdit.lblStartDate.text;
		frmBillPaymentEdit.labelRepeatAsValue.text = "Once";
		frmBillPaymentEdit.lblExecutetimes.text = "1"; 
		endFreqSaveMB = "";
	}
	//--
	if (parseDate(startOnDateMB) > parseDate(frmBillPaymentView.lblStartDate.text) || parseDate(startOnDateMB) < parseDate(frmBillPaymentView.lblStartDate.text)) {
		scheduleStartOnEditMBFlag = true;
	}
	if (kony.string.equalsIgnoreCase(endFreqSaveMB, "Never")) { //kony.i18n.getLocalizedString("keyNever")
		frmBillPaymentEdit.lblExecutetimes.text = "-";
		frmBillPaymentEdit.lblEndOnDate.text = "-";
	} else if (kony.string.equalsIgnoreCase(endFreqSaveMB, "After")) { //kony.i18n.getLocalizedString("keyAfter")
		executionNumberOftimesMB = frmScheduleBillPayEditFuture.tbxAfterTimes.text;
		if (executionNumberOftimesMB == "" || executionNumberOftimesMB == "0" || executionNumberOftimesMB == "0.00") {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"));//alert("please enter valid number of times");
			endFreqSaveMB = "";
			repeatAsMB = "";
			return false;
		}
		
		if (executionNumberOftimesMB > 99) {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			//alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"));//alert("please enter valid number of times");
			alert("Recurring should be less than 99");
			endFreqSaveMB = "";
			repeatAsMB = "";
			return false;
		}
		
		if (gblexecutionOnLoadMB != "") {
			if (executionNumberOftimesMB > gblexecutionOnLoadMB || executionNumberOftimesMB < gblexecutionOnLoadMB) { //if selected execution times is greater than or less than the execution times from service then set the flag to true
				scheduleEndingEditFlag = true;
			}
		} else {
			scheduleEndingEditFlag = true;
		}
		endOnDateMB = endOnDateCalculatorMB(frmScheduleBillPayEditFuture.calScheduleStartDate.formattedDate,
			executionNumberOftimesMB, repeatAsMB);
			
		endOnDateMB = getFormattedDate(endOnDateMB, kony.i18n.getCurrentLocale());
		frmBillPaymentEdit.lblEndOnDate.text = endOnDateMB;
		frmBillPaymentEdit.lblExecutetimes.text = executionNumberOftimesMB;
	} else if (kony.string.equalsIgnoreCase(endFreqSaveMB, "On Date")) { //kony.i18n.getLocalizedString("keyOnDate")
		endOnDateMB = frmScheduleBillPayEditFuture.calScheduleEndDate.formattedDate;
		endOnDateMB = getFormattedDate(endOnDateMB, kony.i18n.getCurrentLocale());
		if (endOnDateMB == null || endOnDateMB == "") {
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));//alert("Please select valid end date");
			return false;
		}
		if (parseDate(endOnDateMB) <= parseDate(startOnDateMB)) {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));//alert("End date should be greater than Start Date");
			return false;
		}
		
		
		if (gblendDateOnLoadMB != "") {
			//alert(" gblendDateOnLoadMB :: " + gblendDateOnLoadMB);
			if (parseDate(endOnDateMB) > parseDate(gblendDateOnLoadMB) || parseDate(endOnDateMB) < parseDate(gblendDateOnLoadMB)) { //if selected end date is greater than or less than the end date from service then set the flag to true
				scheduleEndingEditMBFlag = true;
			}
		} else {
			scheduleEndingEditMBFlag = true;
		}
		executionNumberOftimesMB = numberOfExecutionMB(frmScheduleBillPayEditFuture.calScheduleStartDate.formattedDate,
			frmScheduleBillPayEditFuture.calScheduleEndDate.formattedDate, repeatAsMB);
		if (executionNumberOftimesMB > 99) {
			scheduleRepeatasEditMBFlag = false;
			scheduleEndingEditMBFlag = false;
			//alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));//alert("End date should be greater than Start Date");
			alert("Recurring should be less than 99");
			return false;
		}	
			
		frmBillPaymentEdit.lblEndOnDate.text = endOnDateMB;
		frmBillPaymentEdit.lblExecutetimes.text = executionNumberOftimesMB;
	}
	frmBillPaymentEdit.labelRepeatAsValue.text = repeatAsMB;
	
	if (scheduleRepeatasEditMBFlag == true || scheduleEndingEditMBFlag == true || scheduleStartOnEditMBFlag == true) {
		gblScheduleFreqChangedMB = true;
	} else {
		gblScheduleFreqChangedMB = false;
	}
	frmBillPaymentEdit.show();
	if(OnClickEndFreqMB != ""){
		gblScheduleEndBPMB = OnClickEndFreqMB;
	}
}

function onClickCancelEditPage() {
	//frmBillPaymentView.lblStartDate.text = frmBillPaymentEdit.lblStartDate.text;
	//frmBillPaymentView.lblEndOnDate.text = frmBillPaymentEdit.lblEndOnDate.text;
	//frmBillPaymentView.lblExecutetimes.text = frmBillPaymentEdit.lblExecutetimes.text;
	//frmBillPaymentView.lblrepeatasvalue.text = frmBillPaymentEdit.labelRepeatAsValue.text;
	//frmBillPaymentView.lblamtvalue.text= frmBillPaymentEdit.txtAmtInput.text;
	repeatAsMB = "";
	endFreqSaveMB = "";
	gblAmountSelectedMB = false;
	gblScheduleFreqChangedMB = false;
	frmBillPaymentView.show();
}

function onClickNextEditPage() {
	//validate User Entered Amount
	var userEnteredAmtMB = "";
	var amtMB = 0;
	if(gblBPflag){
		if (gblEditBillMethodMB == 0) {
			amtMB = frmBillPaymentEdit.txtAmtInput.text;
			userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
		} else if (gblEditBillMethodMB == 2) {
			if(gblownCardBPMB){
				amtMB = frmBillPaymentEdit.txtamountvalue.text;
			}else{
				amtMB = frmBillPaymentEdit.txtAmtInput.text;			
			}
			if(amtMB != null){
			   userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
			}
		} else if (gblEditBillMethodMB == 1 || gblEditBillMethodMB == 3) {
			amtMB = frmBillPaymentEdit.txtAmtInput.text;
			
			if(amtMB != null)
				 userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
			/*
			amtMB = frmBillPaymentEdit.txtamountvalue.text;
			if(gblEditBillMethodMB == 3){
				amtMB = frmBillPaymentEdit.txtAmtInput.text;
			}
			
			
			if(amtMB != null)
				 userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
			
			if(gblEditBillMethodMB == 1){
				if(frmBillPaymentEdit.txtamountvalue.isVisible){
					amtMB = frmBillPaymentEdit.txtamountvalue.text;
					userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
				}else if(frmBillPaymentEdit.txtAmtInput.isVisible){
					amtMB = frmBillPaymentEdit.txtAmtInput.text;
					userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
				}
			}
			*/
			
		}
		
	}else{
	
		if (gblEditBillMethodMB != 1) {
       		amtMB = frmBillPaymentEdit.txtAmtInput.text;
       	userEnteredAmtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim(); 
    	} else if (gblEditBillMethodMB == 1){
        	amtMB = frmBillPaymentEdit.cbTPAmountVal.selectedKeyValue[1];
      	 userEnteredAmtMB = amtMB;//replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim(); 
   	 	}
	
	}
	
	
		var tempAmt1 = userEnteredAmtMB;
		if(tempAmt1 != ""){
			if(tempAmt1.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
				tempAmt1.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			}
			var pat1= /[^0-9.,]/g
			var  pat2 = /[\s]/g
			var isAlpha = pat1.test(tempAmt1);
			var containsSpace = pat2.test(tempAmt1);  
			if(isAlpha == true || containsSpace == true){
			  dismissLoadingScreen();
			  
			  //frmMBFTEdit.txtEditAmnt.skin = txtErrorBG;
			  //alert("Enter Valid Amount!");
			  alert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"));
			  return false;
		     }
		}
	
	
	if (userEnteredAmtMB == "" || userEnteredAmtMB == "0" || userEnteredAmtMB == "0.0") {
		//alert("Please enter a valid amount");
		alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));	
		gblAmountSelectedMB = false;
		//dismissLoadingScreen();
		return false;
	} else {
		//if userEnteredAmtMB is not equal to amountMB then set the flag to true
		var amtvalidationMB = amtValidationIB(userEnteredAmtMB);
		if (amtvalidationMB == false) {
			//alert("Please enter a valid amount");
			alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));
			gblAmountSelectedMB = false;
			//dismissLoadingScreen();
			return false;
		} else {
			/*
			var amountMB = removeCommaIB(frmBillPaymentView.lblamtvalue.text);
			amountMB = amountMB.substring(0, amountMB.length - 1).trim();
			
			
			if (parseFloat(userEnteredAmtMB.toString()) != parseFloat(amountMB.toString())) { // check this for online,creditcard,loan
				gblAmountSelectedMB = true;
			} else {
				gblAmountSelectedMB = false;
			}
			gblUserEnteredAmtMB = userEnteredAmtMB;
			*/
			
			var amountMB = frmBillPaymentView.lblamtvalue.text;
			amountMB = amountMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(amountMB.indexOf(",") != -1)
				amountMB = replaceCommon(amountMB, ",", "");
			
			var amt = userEnteredAmtMB+"";
			amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(amt.indexOf(",") != -1)
				amt = replaceCommon(amt, ",", "");
			
						
			
			
			if (parseFloat(amt) != parseFloat(amountMB.toString())) { // check this for online,creditcard,loan
				gblAmountSelectedMB = true;
			} else {
				gblAmountSelectedMB = false;
			}
			gblUserEnteredAmtMB = userEnteredAmtMB;
			
		}
	}
	
		//Validation for min max amount
	if(gblPayFull != "Y") {
		var minAmount = parseFloat(gblMinAmount);
		var maxAmount = parseFloat(gblMaxAmount);
		if((parseFloat(userEnteredAmtMB) < minAmount) || (parseFloat(userEnteredAmtMB) > maxAmount)) {
			var message = kony.i18n.getLocalizedString("PlzEnterValidMinMaxAmount");
			message = message.replace("[min]", minAmount);
			message = message.replace("[max]", maxAmount);
			alert(message);
			return false;
		}
	}
	
	
	if(gblBPflag == true && gblEditBillMethodMB == 1 && frmBillPaymentEdit.txtAmtInput.isVisible){
		var amt1 = frmBillPaymentView.lblamtvalue.text
		amt1 = amt1.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		if(amt1.indexOf(",") != -1)replaceCommon(amt1, ",", "");
		var amt2 = frmBillPaymentEdit.txtAmtInput.text;
		amt2 = amt2.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		if(amt2.indexOf(",") != -1)replaceCommon(amt2, ",", "");
		
		var isChangedToFullAmt = parseFloat(amt1.toString())== parseFloat(amt2.toString());//(parseFloat(frmBillPaymentView.lblamtvalue.text) == parseFloat(frmBillPaymentEdit.txtAmtInput.text));
		
		if(frmBillPaymentEdit.txtAmtInput.isVisible && gblScheduleFreqChangedMB == false && isChangedToFullAmt == true ){
			alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));
			return false
		}else{
			// call billerValidation service here
			var compCodeBillPay = gblBillerCompcodeEditMB;
			var ref1ValBillPay = frmBillPaymentEdit.lblRef1value.text;
			var ref2ValBillPay = gblRef2EditBPMB;
			var billAmountBillPay = userEnteredAmtMB;
			var scheduleDtBillPay = frmBillPaymentEdit.lblStartDate.text;
			callBillerValidationEditBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
				scheduleDtBillPay);
		}
	}else{
		if (gblAmountSelectedMB == true || gblScheduleFreqChangedMB == true) {
			// call biller validation service here
			var compCodeBillPay = gblBillerCompcodeEditMB;
			var ref1ValBillPay = frmBillPaymentEdit.lblRef1value.text;
			var ref2ValBillPay = gblRef2EditBPMB;
			var billAmountBillPay = userEnteredAmtMB;
			var scheduleDtBillPay = frmBillPaymentEdit.lblStartDate.text;
			callBillerValidationEditBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
				scheduleDtBillPay);
		} else {
			//dismissLoadingScreen();
			//alert("Please Edit Amount or Schedule Frequency to proceed Further")
			alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));
			return false
		}
	
	}
	
	
	/*
	if (gblAmountSelectedMB == true || gblScheduleFreqChangedMB == true) {
		callCrmProfileInqServiceMBForEditBPAmt();
	} else {
		//dismissLoadingScreen();
		//alert("Please Edit Amount or Schedule Frequency to proceed Further")
		alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));
		return false
	}
	*/
}

/**
 * Biller validation for edit billers. Added as ENHANCEMENT on 18-02-2015
 */
function callBillerValidationEditBillPayMBService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
	scheduleDtBillPay) {
	showLoadingScreen();
	inputParam = {};
	inputParam["BillerCompcode"] = compCodeBillPay;
	inputParam["ReferenceNumber1"] = ref1ValBillPay;
	inputParam["ReferenceNumber2"] = ref2ValBillPay;
	inputParam["Amount"] = billAmountBillPay;
	inputParam["ScheduleDate"] = scheduleDtBillPay;
	inputParam["billerCategoryID"] = gblBillerCategoryID;
	inputParam["billerMethod"] = gblEditBillMethodMB;
	inputParam["ModuleName"] = "BillPay";
	invokeServiceSecureAsync("billerValidation", inputParam, callBillerValidationEditBillPayMBServiceCallBack);
}

/**
 * Biller validation call back for edit bill payment. Added as ENHANCEMENT on 18-02-2015
 */
function callBillerValidationEditBillPayMBServiceCallBack(status, result) {
	if (status == 400) {
		dismissLoadingScreen();
		var validationBillPayMBFlag = "";
		var isValidOnlineBiller = result["isValidOnlineBiller"];
		if (result["opstatus"] == 0) {
			validationBillPayMBFlag = result["validationResult"];
		} else {
			dismissLoadingScreen();
			validationBillPayMBFlag = result["validationResult"];
		}
		
		if (validationBillPayMBFlag == "true") {
			//added the below services for piblic key exchange on SPA
			if(flowSpa)	{
				spaEditBPToeknExchng()
			}
			else {
				callCrmProfileInqServiceMBForEditBPAmt();
			}
		} else {
			dismissLoadingScreen();
			if(isValidOnlineBiller != undefined && isValidOnlineBiller == "true"){
       	    	alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
            } else{
				alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
			}	
			return false;
		}
	}
}


//added the below services for piblic key exchange on SPA
function spaEditBPToeknExchng()
{
	var inputParam = [];
 	showLoadingScreen();
 	invokeServiceSecureAsync("tokenSwitching", inputParam, spaEditBPToeknExchngCallbackfunction);
}


function spaEditBPToeknExchngCallbackfunction(status,resulttable){
  if (status == 400) {
   if(resulttable["opstatus"] == 0){
    	callCrmProfileInqServiceMBForEditBPAmt();
   }else{
    dismissLoadingScreen();
    alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
   }
   }
}


function callCrmProfileInqServiceMBForEditBPAmt() {
	var inputparam = [];
	showLoadingScreen();
		var editedAmountMB = gblUserEnteredAmtMB;  //removeCommaIB(gblUserEnteredAmtMB);
		editedAmountMB = editedAmountMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
		//editedAmountMB = editedAmountMB.substring("0", editedAmountMB.length - 1);

		if (editedAmountMB.indexOf(",") != -1) {
       	    editedAmountMB = parseFloat(replaceCommon(editedAmountMB, ",", "")).toFixed(2);
   		}else
    	    editedAmountMB = parseFloat(editedAmountMB).toFixed(2);

		/*
		editedAmountMB = parseFloat(editedAmountMB.toString());
		editedAmountMB = "" + editedAmountMB;
		if (editedAmountMB.indexOf(".") != -1) {
			var tmp = editedAmountMB.split(".", 2);
			if (tmp[1].length == 1) {
				editedAmountMB = editedAmountMB + "0";
			} else if (tmp[1].length > 2) {
				tmp[1] = tmp[1].substring("0", "2")
				editedAmountMB = ""
				editedAmountMB = tmp[0] + "." + tmp[1];
			}
		}
		*/
		var billerName = frmBillPaymentEdit.lblBillerName.text;
		billerName = billerName.split("(")[0];
		billerName = billerName.toString().trim();
		
		inputparam["amtEdited"] = editedAmountMB;
		inputparam["billerName"] = billerName; //frmBillPaymentEdit.lblBillerNickName.text;
		inputparam["billerRef1"] = frmBillPaymentEdit.lblRef1value.text;
	if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == false) {
		if(gblBPflag){
			inputparam["extTrnRefID"] = "SB" + frmBillPaymentEdit.lblScheduleRefValue.text.substring(2, frmBillPaymentEdit.lblScheduleRefValue.text.length);
		}else{
			inputparam["extTrnRefID"] = frmBillPaymentEdit.lblScheduleRefValue.text.replace("F", "S");
		}
		inputparam["startOnDate"] = changeDateFormatForService(frmBillPaymentEdit.lblStartDate.text);
		inputparam["editEditBillPayFlag"] = "amtChangeEdit";
	}else if((gblAmountSelectedMB == true && gblScheduleFreqChangedMB == true) || (gblAmountSelectedMB == false && gblScheduleFreqChangedMB == true)){
		inputparam["editEditBillPayFlag"] = "freqChangeEdit";
		if(gblBPflag){
			inputparam["scheID"] = "SB" +frmBillPaymentEdit.lblScheduleRefValue.text.substring(2, frmBillPaymentEdit.lblScheduleRefValue.text.length);
		}else{
			inputparam["scheID"] = frmBillPaymentEdit.lblScheduleRefValue.text.replace("F", "S");
		}
	}
	invokeServiceSecureAsync("crmProfileInq", inputparam, forEditBPCrmProfileInqServiceCallBackMB);
}

function forEditBPCrmProfileInqServiceCallBackMB(status, result) {
	if (status == 400) {
		var activityTypeID = "";
		if(gblBPflag){
			activityTypeID = "067";
		}else{
			activityTypeID = "068";
		}
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmBillPaymentEdit.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentEdit.lblRef1value.text);    //Biller Name + Ref1
		var activityFlexValues3 = ""; 
		var activityFlexValues4 = "";
		var activityFlexValues5 = ""; 
		if(gblScheduleFreqChangedMB == true){
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB) +"+"+frmBillPaymentEdit.lblStartDate.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsMB +"+"+frmBillPaymentEdit.labelRepeatAsValue.text; //Old Frequency + New Frequency (Edit case)			
		}else{
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)	
		}

		if(gblAmountSelectedMB == true){
			activityFlexValues5 = gblAmountMB +"+"+gblUserEnteredAmtMB; //Old Amount + New Amount (Edit case)
		}else{
			activityFlexValues5 = gblAmountMB; //Old Amount + New Amount (Edit case)
		}
		var logLinkageId = "";
		if (result["opstatus"] == 0) {
			activityStatus = "00";
			var StatusCode = result["statusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				var dailyChannelLimitMB = result["ebMaxLimitAmtCurrent"];
				dailyChannelLimitMB = parseFloat(dailyChannelLimitMB.toString());
				var feeMB = frmBillPaymentEdit.lblPaymentFeeValue.text;
				var totalamtMB = parseInt(removeCommaIB(gblUserEnteredAmtMB)) + parseInt(feeMB.substring(0, feeMB.length - 1).trim());
					//alert("dailyChannelLimitMB:  "+dailyChannelLimitMB + "  totalamtMB:  "+totalamtMB +  "  Equals:  "+(dailyChannelLimitMB < totalamtMB) );
				if (dailyChannelLimitMB < totalamtMB) {
					activityStatus = "02";
					//alert("Entered Amount is exceeding the Daily Channel Limit");
					alert(kony.i18n.getLocalizedString("Error_Amount_Daily_Limit"));
					dismissLoadingScreen();
					return false;
				} else {
					activityStatus = "00";
					showConfirmationPageForEditBPMB();
				}
			} else {
				activityStatus = "02";
				dismissLoadingScreen();
				alert(" " + StatusDesc);
			}
		} else {
			activityStatus = "02";
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);	
	}
}

function showConfirmationPageForEditBPMB() {
	// from Account details :
	frmEditFutureBillPaymentConfirm.imgFrom.src=frmBillPaymentEdit.imgFrom.src;
	frmEditFutureBillPaymentConfirm.imgBiller.src=frmBillPaymentEdit.imgBiller.src;
	frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text = frmBillPaymentEdit.lblFromAccountNickname.text;
	frmEditFutureBillPaymentConfirm.lblFromAccountName.text = frmBillPaymentEdit.lblFromAccountName.text;
	frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text = frmBillPaymentEdit.lblFromAccountNumber.text;
	// To biller details :
	frmEditFutureBillPaymentConfirm.lblBillerName.text = frmBillPaymentEdit.lblBillerName.text;
	if (frmBillPaymentEdit.lblBillerNickName.isVisible) {
		frmEditFutureBillPaymentConfirm.lblBillerNickName.text = frmBillPaymentEdit.lblBillerNickName.text;
	} else {
		frmEditFutureBillPaymentConfirm.lblBillerNickName.setVisibility(false);
	}
	frmEditFutureBillPaymentConfirm.lblref1.text = frmBillPaymentEdit.lblref1.text;
	frmEditFutureBillPaymentConfirm.lblRef1Value.text = frmBillPaymentEdit.lblRef1value.text;
	//below line is added for CR - PCI-DSS masked Credit card no
	frmEditFutureBillPaymentConfirm.lblRef1ValueMasked.text = frmBillPaymentEdit.lblRef1valueMasked.text;
	
	if (frmBillPaymentEdit.hbox58862488827905.isVisible) {
		frmEditFutureBillPaymentConfirm.hbxRef2.setVisibility(true);
		frmEditFutureBillPaymentConfirm.lblRef2.text = frmBillPaymentEdit.lblref2.text;
		frmEditFutureBillPaymentConfirm.lblRef2Value.text = frmBillPaymentEdit.lblRef2value.text;
	} else {
		frmEditFutureBillPaymentConfirm.hbxRef2.setVisibility(false);
	}
	//Payment details :
	
	if(gblBPflag){
	if (gblEditBillMethodMB == 0 || gblEditBillMethodMB == 3 || gblEditBillMethodMB == 1) {
		var amount = frmBillPaymentEdit.txtAmtInput.text;
		if(amount.indexOf(",") != -1){
			amount = replaceCommon(amount, ",", "");
		}
		if (frmBillPaymentEdit.txtAmtInput.text.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1) {
			//frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtAmtInput.text +" "+kony.i18n.getLocalizedString("currencyThaiBaht");
			frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount) +" "+kony.i18n.getLocalizedString("currencyThaiBaht");	

		} else {
			amount = amount.trim();
			amount = amount.substring(0, amount.length-1).trim();
			frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht"); ;
		}
	} else {
		//
		var amount = "";
		if(gblEditBillMethodMB == 2 && !gblownCardBPMB){
		    amount = frmBillPaymentEdit.txtAmtInput.text;
		}else
			amount = frmBillPaymentEdit.txtamountvalue.text;
		
		//var amount = frmBillPaymentEdit.txtamountvalue.text;
		if(amount.indexOf(",") != -1){
			amount = replaceCommon(amount, ",", "");
		}
		
		//amount = amount.trim();
		//amount = amount.substring(0, amount.length-1).trim();
		amount = amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+ " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
		
		/*
		if (frmBillPaymentEdit.txtamountvalue.text.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1) {
			//frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtamountvalue.text +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
			frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
		} else {
			//frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtamountvalue.text;
			amount = amount.trim();
			amount = amount.substring(0, amount.length-1).trim();
			amount = amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+ " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
		}
		*/
		
		if(frmBillPaymentEdit.buttonOnlineFull.skin == "btnScheduleEndLeftFocus"){
			frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtAmtInput.text;
		}
		
	}
	}else{
		if (gblEditBillMethodMB != 1) {
			var amount = frmBillPaymentEdit.txtAmtInput.text; 
			if(amount.indexOf(",") != -1){
				amount = replaceCommon(amount, ",", "");
			}
		
			if (frmBillPaymentEdit.txtAmtInput.text.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1) {
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtAmtInput.text +" "+kony.i18n.getLocalizedString("currencyThaiBaht");
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				amount = amount.trim();
				amount = amount.substring(0, amount.length-1).trim();
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = frmBillPaymentEdit.txtAmtInput.text;
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+ " "+kony.i18n.getLocalizedString("currencyThaiBaht");;
			}
		}else{
			var amount = gblUserEnteredAmtMB;
			if(amount.indexOf(",") != -1){
				amount = replaceCommon(amount, ",", ""); 
			}
			
			if (gblUserEnteredAmtMB.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) == -1) {
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = gblUserEnteredAmtMB +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount) +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				amount = amount.trim();
				amount = amount.substring(0, amount.length-1).trim();
				//frmEditFutureBillPaymentConfirm.lblAmtValue.text = gblUserEnteredAmtMB;
				frmEditFutureBillPaymentConfirm.lblAmtValue.text = commaFormatted(amount)+" "+ kony.i18n.getLocalizedString("currencyThaiBaht");;
			}
		}
	}
	frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text = frmBillPaymentEdit.lblPaymentDateValue.text;
	frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text = frmBillPaymentEdit.lblPaymentFeeValue.text;
	//Schedule details :
	frmEditFutureBillPaymentConfirm.lblStartDate.text = frmBillPaymentEdit.lblStartDate.text;
	frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text = frmBillPaymentEdit.labelRepeatAsValue.text;
	frmEditFutureBillPaymentConfirm.lblEndOnDate.text = frmBillPaymentEdit.lblEndOnDate.text;
	frmEditFutureBillPaymentConfirm.lblExecutetimes.text = frmBillPaymentEdit.lblExecutetimes.text
	//My note :
	frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text = frmBillPaymentEdit.lblMyNotesDesc.text;
	//scheduled ref num :
	frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text = frmBillPaymentEdit.lblScheduleRefValue.text;
	
	if(gblBPflag == false ){
		if(gblBillerCompcodeEditMB == "2151" && frmBillPaymentView.hbxTPCusName.isVisible){
			frmEditFutureBillPaymentConfirm.hbxTPCusName.setVisibility(true);
			frmEditFutureBillPaymentConfirm.lblTPCustomerName.text = "Easy Pass Customer Name"; 
			frmEditFutureBillPaymentConfirm.lblTPCustomerNameVal.text = frmBillPaymentView.lblTPCustomerNameVal.text;
		}else
			frmEditFutureBillPaymentConfirm.hbxTPCusName.setVisibility(false);
	}else{
		frmEditFutureBillPaymentConfirm.hbxTPCusName.setVisibility(false);
	}
	
	
	dismissLoadingScreen();
	frmEditFutureBillPaymentConfirm.show();
}

function onClickNextEditBPConfirm() {
	/*
	if (popEditBillPayTransactionPwd.tbxPopTransactionPwd.text==""){
		alert("Please enter the transaction password");
		return false;
	}
	else 
		editBillPaymentFlowMB();
		*/
	if(flowSpa)
	{	
			if (gblIBFlowStatus == "04") {
			//showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
			popTransferConfirmOTPLock.show();
			
			return false;
		}
		else	
		{    var inputParams = {}
		    spaChnage = "editFutureBillpayments"
		    gblOTPFlag = true;
		    gblOnClickReq = false;
		    try {
		        kony.timer.cancel("otpTimer")
		    } catch (e) {
		        
		    }
			//gblSpaChannel="IB-NEW_RC_ADDITION";
			var locale = kony.i18n.getCurrentLocale();
			if(gblBPflag == false ){
				gblSpaChannel = "TopUpPayment";
			/*
			if (locale == "en_US") {
					SpaEventNotificationPolicy = "MIB_MobileTopup_EN";
					SpaSMSSubject = "MIB_MobileTopup_EN";
				} else {
					SpaEventNotificationPolicy = "MIB_MobileTopup_TH";
					SpaSMSSubject = "MIB_MobileTopup_TH";
				}
			*/
			}else{
				gblSpaChannel = "BillPayment";
			/*
			if (locale == "en_US") {
					SpaEventNotificationPolicy = "MIB_BillPayment_EN";
					SpaSMSSubject = "MIB_BillPayment_EN";
				} else {
					SpaEventNotificationPolicy = "MIB_BillPayment_TH";
					SpaSMSSubject = "MIB_BillPayment_TH";
				}*/
			}	
			
			gblSpaAmount = parseFloat(removeCommos(frmEditFutureBillPaymentConfirm.lblAmtValue.text)).toFixed(2);
    		gblSpaAmount = commaFormatted(gblSpaAmount);
			gblSpaBillerName = frmEditFutureBillPaymentConfirm.lblBillerNickName.text;
			gblSpaRef1Value = frmEditFutureBillPaymentConfirm.lblRef1Value.text;
	
		    onClickOTPRequestSpa();
		    }
		
	}
	else
	{
		if  (gblUserLockStatusMB == "03" ) {
			dismissLoadingScreen(); 
			popTransferConfirmOTPLock.show();
	        return false;		
		}	
		else{
		var lblText = kony.i18n.getLocalizedString("transPasswordSub");
		var refNo = "";
		var mobNO = "";
		popupTractPwd.hbxPopupTranscPwd.skin = hbxPopupTrnsPwdBlue;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue; 
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
		showOTPPopup(lblText, refNo, mobNO, editBPConfirmation, 3);
		}
		
	}
}

function editBPConfirmation(txnPwd) {
	//var transFlag = trassactionPwdValidatn(txnPwd);
	if (txnPwd != "") {
		checkEditBillPayVerifyPWDMB(txnPwd);
	} else {
		//alert("INVALID TRANSACTION PASSWORD");
		//alert(kony.i18n.getLocalizedString("invalidTxnPwd"));
		  setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
	}
}

function checkEditBillPayVerifyPWDMB(txnPwd) {
	var inputParam = [];
	inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin; // check values  -- To DO
	inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin; // check values  -- To DO
	inputParam["password"] = txnPwd;
	popupTractPwd.txtOTP.text = "";
	if(flowSpa){
		if(gblTokenSwitchFlag == true ) {
			inputParam["OTP_TOKEN_FLAG"] = "TOKEN";
		}else{
			 inputParam["OTP_TOKEN_FLAG"] = "OTP";
		}
	}
	showLoadingScreen();
	if ((gblAmountSelectedMB == true && gblScheduleFreqChangedMB == true) || (gblAmountSelectedMB == false && gblScheduleFreqChangedMB == true)) {
		inputParam["selectedEditOption"] = "Frequency";
		var amtMB = frmEditFutureBillPaymentConfirm.lblAmtValue.text;  //removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text);
		
		 if (amtMB.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) 
            amtMB = amtMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	    
	    if (amtMB.indexOf(",") != -1) {
       	    amtMB = parseFloat(replaceCommon(amtMB, ",", "")).toFixed(2);
   		}else
    	    amtMB = parseFloat(amtMB).toFixed(2);
		
		/*
		amtMB = amtMB.substring("0", amtMB.length - 1);
		amtMB = parseFloat(amtMB.toString());
		amtMB = "" + amtMB;
		if (amtMB.indexOf(".") != -1) {
			var tmp = amtMB.split(".", 2);
			if (tmp[1].length == 1) {
				amtMB = amtMB + "0";
			} else if (tmp[1].length > 2) {
				tmp[1] = tmp[1].substring("0", "2")
				amtMB = ""
				amtMB = tmp[0] + "." + tmp[1];
			}
		}
		*/
		
		inputParam["amt"] = amtMB;
		inputParam["fromAcct"] = gblFromAccIdMB; // from Account ID (from PaymentInq)
		inputParam["fromAcctType"] = gblFromAccTypeMB; // from Account Type (from PaymentInq)
		inputParam["toAcct"] = gblToAccNoMB; // TO Biller No (from PaymentInq)
		inputParam["toAccTType"] = gblToAccTypeMB; // To Biller Type ((from PaymentInq)
		inputParam["dueDate"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblStartDate.text); // Start On Date
		
		
		inputParam["pmtRefNo1"] = removeHyphenIB(frmEditFutureBillPaymentConfirm.lblRef1Value.text); // Ref 1 value
		inputParam["custPayeeId"] = gblCustPayeeIDMB; // check this with TL
		inputParam["transCode"] = gblTransCodeMB; //TransCode (from PaymentInq)
		inputParam["memo"] = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text; // My Note 
	    var ref1MB = frmEditFutureBillPaymentConfirm.lblRef1Value.text;
	    if(ref1MB.indexOf("-") != -1){
	      ref1MB = kony.string.replace(ref1MB, "-", "");
	    }
		if(gblEditBillMethodMB == "1"){
		 inputParam["PmtMiscType"] = "MOBILE";
	     inputParam["MiscText"] = ref1MB;
		}
		inputParam["billMethod"] = gblEditBillMethodMB;
		if(frmBillPaymentEdit.hbox58862488827905.isVisible){
			inputParam["pmtRefNo2"] = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
		}
		if( gblBPflag == true && gblRef2EditBPMB != ""){
			inputParam["pmtRefNo2"] = gblRef2EditBPMB;
		}
		inputParam["pmtMethod"] = gblPmtMethodFromPmt; // get this value from paymentInq
			
		if(gblBPflag){
			inputParam["extTrnRefId"] = "SB" +frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.substring(2, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.length); // Updated Schedule Ref No	
		}else{
			inputParam["extTrnRefId"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.replace("F", "S");
		}
		var endFreqFlag = "";
		if (endFreqSaveMB == "After") {
			inputParam["NumInsts"] = frmEditFutureBillPaymentConfirm.lblExecutetimes.text; // Execution times 
			endFreqFlag="After";
		}
		if (endFreqSaveMB == "On Date") {
			inputParam["FinalDueDt"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblEndOnDate.text); // End On Date
			endFreqFlag="OnDate";
		}
		inputParam["endFreqSave"] = endFreqFlag;
		var frequency = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
		inputParam["frequencyCode"] = "0";
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNE"))) {
			inputParam["dayOfWeek"] = "";
			frequency = "Daily";
			inputParam["frequencyCode"] = "4";
		}
		
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNE"))) {
			inputParam["dayOfWeek"] = "";
			frequency = "Weekly";
			inputParam["frequencyCode"] = "1";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNE"))) {
			inputParam["dayOfMonth"] = "";
			frequency = "Monthly";
			inputParam["frequencyCode"] = "2";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNE"))) {
			frequency = "Annually";
			inputParam["dayofMonth"] = "";
			inputParam["monthofYear"] = "";
			inputParam["frequencyCode"] = "3";
		}
		//if(kony.string.equalsIgnoreCase(frequency, "Once")) 
        //Modified for DEF590
		if(kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnceNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnceNE"))) 
		{
			inputParam["freq"] = "once";
		}else{
			inputParam["freq"] = frequency;
		}
		if(flowSpa){
			inputParam["channelId"]="IB";
		}else{
			inputParam["channelId"]="MB";
		}
		//inputParam["channelId"]="MB";
		inputParam["desc"] = "";
		if(gblBPflag){
			inputParam["scheID"] = "SB" +frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.substring(2, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.length);
		}else{
			inputParam["scheID"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.replace("F", "S");
		}
		if(gblBPflag){
			inputParam["transRefType"] = "SB";
		}else{
			inputParam["transRefType"] = "SU";
		}
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmEditFutureBillPaymentConfirm.lblBillerName.text +"+"+ removeHyphenIB(frmEditFutureBillPaymentConfirm.lblRef1Value.text);     //Biller Name + Ref1
		var activityFlexValues3 = "";
		var activityFlexValues4 = "";
		var activityFlexValues5 = "";
		if(gblScheduleFreqChangedMB == true){
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB)+"+"+frmEditFutureBillPaymentConfirm.lblStartDate.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsMB+"+"+frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text; //Old Frequency + New Frequency (Edit case)
		}else{
			activityFlexValues3 = dateFormatForDisplay(gblstartOnMB);
			activityFlexValues4 = gblOnLoadRepeatAsMB;
		}
		
		activityFlexValues5 = gblAmountMB+"+"+removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")); //Old Amount + New Amount (Edit case)
		
		/*
		if(gblAmountSelectedMB ==true){
			activityFlexValues5 = gblAmountMB+"+"+removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")); //Old Amount + New Amount (Edit case)
        }else{
            
        	activityFlexValues5 = gblAmountMB;
        }
       */
	} else if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == false) {
		//callPaymentUpdateServiceMB();
		var editedAmountMB = frmEditFutureBillPaymentConfirm.lblAmtValue.text;		//removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text);
		
		if (editedAmountMB.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) 
            editedAmountMB = editedAmountMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	    
	    if (editedAmountMB.indexOf(",") != -1) {
       	    editedAmountMB = parseFloat(replaceCommon(editedAmountMB, ",", "")).toFixed(2);
   		}else
    	    editedAmountMB = parseFloat(editedAmountMB).toFixed(2);
		
		if(gblBPflag){
			inputParam["extTrnRefID"] = "SB" + frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.substring(2, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.length);
		}else{
			inputParam["extTrnRefID"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text.replace("F", "S");
		}
		//alert(editedAmountMB);
		inputParam["Amt"] = editedAmountMB;
		inputParam["Starton"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblStartDate.text);
		inputParam["selectedEditOption"] = "Amount";
		
		var userEnteredAmt = frmEditFutureBillPaymentConfirm.lblAmtValue.text;
       	userEnteredAmt = userEnteredAmt.substring(0,userEnteredAmt.length-1);
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmEditFutureBillPaymentConfirm.lblBillerName.text +"+"+ removeHyphenIB(frmEditFutureBillPaymentConfirm.lblRef1Value.text);    //Biller Name + Ref1
		var activityFlexValues3 = frmEditFutureBillPaymentConfirm.lblStartDate.text; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB +"+"+userEnteredAmt; //Old Amount + New Amount (Edit case)		
	}
		var activityTypeID = "";
		if(gblBPflag){
			activityTypeID = "067";
		}else{
			activityTypeID = "068";
		}
		//inputParam["channelName"] = "MB";
		if(flowSpa){
			inputParam["channelName"]="IB";
		}else{
			inputParam["channelName"]="MB";
		}
		inputParam["notificationType"] = "Email"; // check this : which value we have to pass
		inputParam["phoneNumber"] = gblPHONENUMBER;
		inputParam["emailId"] = gblEmailId;
		inputParam["customerName"] = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text 
		var fromAccount = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text
		if(fromAccount.indexOf("-") != -1){
		    fromAccount = kony.string.replace(fromAccount, "-", "");
		}
		inputParam["fromAccount"] = fromAccount; 
		inputParam["fromAcctNick"] = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text;
		inputParam["fromAcctName"] = frmEditFutureBillPaymentConfirm.lblFromAccountName.text;
		inputParam["billerNick"] = frmEditFutureBillPaymentConfirm.lblBillerNickName.text ;
		inputParam["billerName"] = frmEditFutureBillPaymentConfirm.lblBillerName.text;
		inputParam["billerNameTH"]=billerNamTH;
	    inputParam["ref1EN"] = gblRef1LblEN+" : " + frmEditFutureBillPaymentConfirm.lblRef1Value.text;
	    inputParam["ref1TH"] = gblRef1LblTH+" : " + frmEditFutureBillPaymentConfirm.lblRef1Value.text;
		var ref2 = "";
		if(frmBillPaymentView.hbox58862488827877.isVisible){
			ref2 = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
			
			inputParam["ref2EN"] = gblRef2LblEN+" : " + ref2;
	        inputParam["ref2TH"] = gblRef2LblTH+" : " + ref2;
		}else{
			inputParam["ref2EN"] = "";
	        inputParam["ref2TH"] = "";
		}
		inputParam["amount"] =  frmEditFutureBillPaymentConfirm.lblAmtValue.text;
		inputParam["fee"] = frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text;
		inputParam["initiationDt"] =NormalDateMB;
		inputParam["recurring"] = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
		inputParam["startDt"] = frmEditFutureBillPaymentConfirm.lblStartDate.text;
		inputParam["endDt"] = frmEditFutureBillPaymentConfirm.lblEndOnDate.text;
		inputParam["refID"] = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text;
		inputParam["mynote"] = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text;
		inputParam["memo"] = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text;
		//inputParam["channelId"] = "MB";
		if(flowSpa){
			inputParam["channelId"]="IB";
		}else{
			inputParam["channelId"]="MB";
		}
		inputParam["source"] = "FutureBillPaymentAndTopUp";
		inputParam["Locale"] = kony.i18n.getCurrentLocale();
		
		inputParam["activityTypeID"] = activityTypeID;
		inputParam["activityFlexValues1"] = activityFlexValues1;
		inputParam["activityFlexValues2"] = activityFlexValues2;
		inputParam["activityFlexValues3"] = activityFlexValues3;
		inputParam["activityFlexValues4"] = activityFlexValues4;
		inputParam["activityFlexValues5"] = activityFlexValues5;
		
		var scheduleDetails = "";
		if(frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text == "Once"){
	 		scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
		 }else if(frmEditFutureBillPaymentConfirm.lblExecutetimes.text == "-"){
		 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
		 } else{
		 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text+ " " + kony.i18n.getLocalizedString("keyTo") + " " +frmEditFutureBillPaymentConfirm.lblEndOnDate.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmEditFutureBillPaymentConfirm.lblExecutetimes.text +" "+kony.i18n.getLocalizedString("keyTimesIB") ;
		 }
	 	inputParam["PaymentSchedule"] = scheduleDetails;
	 	
		invokeServiceSecureAsync("FutureBillPaymentEditExecute", inputParam, callBackEditBillPayVerifyPWDMB)
}

function callBackEditBillPayVerifyPWDMB(status, result) {


	if (status == 400) {
		if (result["opstatusVPX"] == 0) {
			popupTractPwd.dismiss();
			gblRetryCountRequestOTP = "0";
			editBillPaymentFlowMB(result);
		} else {
		
			if(flowSpa){
				dismissLoadingScreen();
				if(result["opstatusVPX"] == "8005"){
					
					if(result["errCode"] == "VrfyOTPErr00001"){
						gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
	                	alert("" + kony.i18n.getLocalizedString("invalidOTP"));
	                	return false;
					}else if(result["errCode"] == "VrfyOTPErr00002"){
	               		//alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
	               		popTransferConfirmOTPLock.show();
		                return false;
					}else if(result["errCode"] == "VrfyOTPErr00005"){
	               		alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
	                	return false;
					}else if(result["errCode"] == "VrfyOTPErr00006"){
	                	gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
	                	//alert("" + result["errmsg"]);
	                	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
	                	return false;
					}else{
						//alert("" + result["errmsg"]);
						showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
	                	return false;
					}
				}else{
					dismissLoadingScreen();
	            	alert(" "+result["errmsg"]);
				}
			
			}else{
					gblRtyCtrVrfyTxPin = result["retryCounterVerifyTransPwd"];
				
				if (result["errCode"] == "VrfyTxPWDErr00001") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen(); // keyECUserNotFound
					showAlert(kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
					return false;
				} else if (result["errCode"] == "VrfyTxPWDErr00002") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					//showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00001"), kony.i18n.getLocalizedString("info"));
					 popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
					 popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
					 popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
					 popupTractPwd.hbxPopupTranscPwd.skin = txtErrorBG;
					 popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
					 popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
					//
					 return false;
				} else if (result["errCode"] == "VrfyTxPWDErr00003") {
				
					showTranPwdLockedPopup();
					dismissLoadingScreen();
					return false;
				} else if (result["errCode"] == "JavaErr00001") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
					return false;
				}else if (result["errCode"] == "EditPaymentErr001") {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					showAlert("Transaction Can Not Be Preocessed", kony.i18n.getLocalizedString("info"));
					return false;
				}
				 else {
					kony.application.dismissLoadingScreen();
					dismissLoadingScreen();
					showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
					return false;
				 }
			
			}
		}
	}
}

/*function callBackCrmProfileModServiceForEditBPMBCallback(status, result) {
	
	
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var statusCode = result["StatusCode"];
			var severity = result["Severity"];
			var statusDesc = result["StatusDesc"];
			
			
			
			if (statusCode != 0) {
				alert(statusDesc);
			} else {
				//TODO: success scenario//TODO: success scenario 
			}
		} else {
			alert("LOCKED");
		}
	} else {
		if (status == 300) {
			alert("LOCKED");
		}
	}
	
}*/

function editBillPaymentFlowMB(result) {
	if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == true) {
		//callPaymentDeleteServiceMB();
		callBackPmtCanServiceForEditBPMB(result);
		//gblAmountSelectedMB = false;
		//gblScheduleFreqChangedMB = false;
	} else if (gblAmountSelectedMB == false && gblScheduleFreqChangedMB == true) {
		//callPaymentDeleteServiceMB();
		callBackPmtCanServiceForEditBPMB(result);
		//gblAmountSelectedMB = false;
		//gblScheduleFreqChangedMB = false;
	} else if (gblAmountSelectedMB == true && gblScheduleFreqChangedMB == false) {
		callBackPmtModServiceForEditBPMB(result);
		gblAmountSelectedMB = false;
		gblScheduleFreqChangedMB = false;
	}
}

function callBackPmtCanServiceForEditBPMB(result) {
	
		if (result["opstatusCancel"] == 0) {
			var StatusCode = result["statusCodeCancel"];
			var StatusDesc = result["statusDescCancel"];
			if (StatusCode == 0) {
				var refNum = result["transRefNum"];
				//refNum = refNum.replace("F", "S");
				frmEditFutureBillPaymentComplete.lblScheduleRefValue.text = refNum;
				callBackPmtAddServiceForEditBPMB(result) 
			} else {
				gblAmountSelectedMB = false;
				gblScheduleFreqChangedMB = false;
				alert(result["StatusDesc"]);
				dismissLoadingScreen();
				return false;
			}
		} else {
			gblAmountSelectedMB = false;
			gblScheduleFreqChangedMB = false;
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
	
}

/*function callBackGenerateNewTransRefIDServiceMB(status, result) {
	if (status == 400) //success responce
	{
		if (result["opstatus"] == 0) {
			var refNum = result["transRefNum"];
			frmEditFutureBillPaymentComplete.lblScheduleRefValue.text = refNum+"00";
			callPmtAddServiceForEditBPMB();
		}else{
			gblAmountSelectedMB = false;
			gblScheduleFreqChangedMB = false;
			dismissLoadingScreen();
			alert(" " + result["errMsg"]);
		}
	} else {
		//alert(" " + result["errMsg"]);
	}
}*/

/*function callPmtAddServiceForEditBPMB() {
	
	var inputparam = [];
	inputparam["rqUID"] = "";
	inputparam["crmId"] = "";
	inputparam["curCode"] = "THB";
	var amtMB = removeCommaIB(frmEditFutureBillPaymentConfirm.lblAmtValue.text);
	amtMB = amtMB.substring("0", amtMB.length - 1);
	amtMB = parseFloat(amtMB.toString());
	amtMB = "" + amtMB;
	if (amtMB.indexOf(".") != -1) {
		var tmp = amtMB.split(".", 2);
		if (tmp[1].length == 1) {
			amtMB = amtMB + "0";
		} else if (tmp[1].length > 2) {
			tmp[1] = tmp[1].substring("0", "2")
			amtMB = ""
			amtMB = tmp[0] + "." + tmp[1];
		}
	}
	
	inputparam["amt"] = amtMB;
	inputparam["fromAcct"] = gblFromAccIdMB; // from Account ID (from PaymentInq)
	inputparam["fromAcctType"] = gblFromAccTypeMB; // from Account Type (from PaymentInq)
	inputparam["toAcct"] = gblToAccNoMB; // TO Biller No (from PaymentInq)
	inputparam["toAccTType"] = gblToAccTypeMB; // To Biller Type ((from PaymentInq)
	inputparam["dueDate"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblStartDate.text); // Start On Date
	
	
	inputparam["pmtRefNo1"] = removeHyphenIB(frmEditFutureBillPaymentConfirm.lblRef1Value.text); // Ref 1 value
	inputparam["custPayeeId"] = gblCustPayeeIDMB; // check this with TL
	inputparam["transCode"] = gblTransCodeMB; //TransCode (from PaymentInq)
	inputparam["memo"] = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text; // My Note 
    var ref1MB = frmEditFutureBillPaymentConfirm.lblRef1Value.text;
    if(ref1MB.indexOf("-") != -1){
      ref1MB = kony.string.replace(ref1MB, "-", "");
    }

	if(gblEditBillMethodMB == "1"){
	 inputparam["PmtMiscType"] = "MOBILE";
     inputparam["MiscText"] = ref1MB;
	}


	if(frmBillPaymentEdit.hbox58862488827905.isVisible){
		inputparam["pmtRefNo2"] = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
	}
	
	if( gblBPflag == true && gblRef2EditBPMB != ""){
		inputparam["pmtRefNo2"] = gblRef2EditBPMB;
	}
	

	inputparam["pmtMethod"] = gblPmtMethodFromPmt; // get this value from paymentInq
		
	if(gblBPflag){
		inputparam["extTrnRefId"] = "SB" +frmEditFutureBillPaymentComplete.lblScheduleRefValue.text.substring(2, frmEditFutureBillPaymentComplete.lblScheduleRefValue.text.length); // Updated Schedule Ref No	
	}else{
		inputparam["extTrnRefId"] = frmEditFutureBillPaymentComplete.lblScheduleRefValue.text.replace("F", "S");
	}
	if (endFreqSaveMB == kony.i18n.getLocalizedString("keyAfter")) {
		inputparam["NumInsts"] = frmEditFutureBillPaymentConfirm.lblExecutetimes.text; // Execution times 
	}
	if (endFreqSaveMB == kony.i18n.getLocalizedString("keyOnDate")) {
		inputparam["FinalDueDt"] = changeDateFormatForService(frmEditFutureBillPaymentConfirm.lblEndOnDate.text); // End On Date
	}
	var frequency = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
	
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDaily"))) {
		inputparam["dayOfWeek"] = "";
		frequency = "Daily";
	}
	
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeekly"))) {
		inputparam["dayOfWeek"] = "";
		frequency = "Weekly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthly"))) {
		inputparam["dayOfMonth"] = "";
		frequency = "Monthly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearly"))) {
		frequency = "Annually";
		inputparam["dayofMonth"] = "";
		inputparam["monthofYear"] = "";
	}
	if(kony.string.equalsIgnoreCase(frequency, "Once")){
		inputparam["freq"] = "once";
	}else{
		inputparam["freq"] = frequency;
	}
	inputparam["channelId"]="MB";
	inputparam["desc"] = "";
	
	invokeServiceSecureAsync("doPmtAdd", inputparam, callBackPmtAddServiceForEditBPMB);
}*/

function callBackPmtAddServiceForEditBPMB(result) {
        gblAmountSelectedMB = false;
		gblScheduleFreqChangedMB = false;
		if (result["opstatusPayAdd"] == 0) {
			var StatusCode = result["statusCodeAdd"];
			var StatusDesc = result["statusDescAdd"];
			if (StatusCode == 0) {
				frmEditFutureBillPaymentComplete.lblPaymentDateValue.text = result["currentTime"];
				populateEditBillPaymentMBCompleteScreen();
				//callNotificationAddServiceMB();
			} else {
				alert(result["statusDescAdd"]);
				dismissLoadingScreen();
				return false;
			}
		} else {
			dismissLoadingScreen();
			alert(result["errMsg"]);
		}
}

function callBackPmtModServiceForEditBPMB(result) {
        var logLinkageId = "";
		if (result["opstatusBP"] == 0) {
			var StatusCode = result["StatusCode"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
  	 			 frmEditFutureBillPaymentComplete.lblScheduleRefValue.text = frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text;
  	 			 frmEditFutureBillPaymentComplete.lblPaymentDateValue.text = frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text;
				 populateEditBillPaymentMBCompleteScreen();
				// callNotificationAddServiceMB();
			} else {
				dismissLoadingScreen();
				alert(result["StatusDesc"]);
				return false;
			}
		} else {
			dismissLoadingScreen();
			alert(" " + result["opstatusBP"]);
		}
}

function populateEditBillPaymentMBCompleteScreen() {
	// from Account details :
	frmEditFutureBillPaymentComplete.imgTransNPbAckFrm.src =frmEditFutureBillPaymentConfirm.imgFrom.src;
	frmEditFutureBillPaymentComplete.imgBillerPic.src=frmEditFutureBillPaymentConfirm.imgBiller.src;
	//frmEditFutureBillPaymentComplete.llblBPAckFrmName.text = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text;
	frmEditFutureBillPaymentComplete.lblBPAckFrmName.text = frmEditFutureBillPaymentConfirm.lblFromAccountName.text;
	frmEditFutureBillPaymentComplete.lblBPAckFrmNum.text = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text;
	frmEditFutureBillPaymentComplete.lblFromAccountNickname.text = frmEditFutureBillPaymentConfirm.lblFromAccountNickname.text
	// To biller details :
	frmEditFutureBillPaymentComplete.lblBillerNameCompCode.text = frmEditFutureBillPaymentConfirm.lblBillerName.text;
	if (frmEditFutureBillPaymentConfirm.lblBillerNickName.isVisible) {
		frmEditFutureBillPaymentComplete.lblBillerNickname.text = frmEditFutureBillPaymentConfirm.lblBillerNickName.text;
	} else {
		frmEditFutureBillPaymentComplete.lblBillerNickname.setVisibility(false);
	}
	frmEditFutureBillPaymentComplete.lblRef1.text = frmEditFutureBillPaymentConfirm.lblref1.text;
	//below line is changed for CR - PCI-DSS masked Credit card no
	frmEditFutureBillPaymentComplete.lblRef1Value.text = frmEditFutureBillPaymentConfirm.lblRef1ValueMasked.text;
	if (frmEditFutureBillPaymentConfirm.hbxRef2.isVisible) {
		frmEditFutureBillPaymentComplete.lblRef2.text = frmEditFutureBillPaymentConfirm.lblRef2.text;
		frmEditFutureBillPaymentComplete.lblRef2Value.text = frmEditFutureBillPaymentConfirm.lblRef2Value.text;
	} else {
		frmEditFutureBillPaymentComplete.hbxRef2.setVisibility(false);
	}
	frmEditFutureBillPaymentComplete.lblAmountValue.text = frmEditFutureBillPaymentConfirm.lblAmtValue.text;
	//frmEditFutureBillPaymentComplete.lblPaymentDateValue.text = frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text;
	frmEditFutureBillPaymentComplete.lblPaymentFeeValue.text = frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text;
	//Schedule details :
	frmEditFutureBillPaymentComplete.lblStartDate.text = frmEditFutureBillPaymentConfirm.lblStartDate.text;
	frmEditFutureBillPaymentComplete.lblrepeatasvalue.text = frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
	frmEditFutureBillPaymentComplete.lblEndOnDate.text = frmEditFutureBillPaymentConfirm.lblEndOnDate.text;
	frmEditFutureBillPaymentComplete.lblExecutetimes.text = frmEditFutureBillPaymentConfirm.lblExecutetimes.text;
	//My note :
	frmEditFutureBillPaymentComplete.lblMyNoteValue.text = frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text;
	
	if(gblBPflag == false ){
		if(gblBillerCompcodeEditMB == "2151" && frmBillPaymentView.hbxTPCusName.isVisible){
			frmEditFutureBillPaymentComplete.hbxTPCusName.setVisibility(true);
			frmEditFutureBillPaymentComplete.lblTPCustomerName.text = "Easy Pass Customer Name";
			frmEditFutureBillPaymentComplete.lblTPCustomerNameVal.text = frmBillPaymentView.lblTPCustomerNameVal.text;
		}else
			frmEditFutureBillPaymentComplete.hbxTPCusName.setVisibility(false);
	}else{
		frmEditFutureBillPaymentComplete.hbxTPCusName.setVisibility(false);
	}
	
	dismissLoadingScreen();
	frmEditFutureBillPaymentComplete.show();
}

/*
   **************************************************************************************
		Module	: callBackNotificationAddService
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackNotificationAddServiceMB(status, result) {


	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				//populateEditBillPaymentCompleteScreen();
				
			} else {
				
				return false;
			}
		} else {
			
		}
	}
}









function deleteFutureBillPaymentFromViewPageMB() {
	var inputparam = [];
	inputparam["rqUID"] = "";
	//inputparam["PmtId"] = ""; 
	if(gblBPflag){
		inputparam["scheID"] ="SB"+frmBillPaymentView.lblScheduleRefValue.text.substring(2, frmBillPaymentView.lblScheduleRefValue.text.length); //get this value from Calender or future transactions
	}else{
		inputparam["scheID"] = frmBillPaymentView.lblScheduleRefValue.text.replace("F", "S");
	}
	
    var logLinkageId = "";
	if(gblBPflag){
		inputparam["EditFTFlow"] = "EditBP";
	}else
		inputparam["EditFTFlow"] = "EditTP";
	
	inputparam["isCancelFlow"] = "true";
	inputparam["activityFlexValues1"] = "Cancel";
	inputparam["activityFlexValues2"] = frmBillPaymentView.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentView.lblRef1value.text);     //Biller Name + Ref1
	inputparam["activityFlexValues3"] = dateFormatForDisplay(gblstartOnMB);
	inputparam["activityFlexValues4"] = gblOnLoadRepeatAsMB
	inputparam["activityFlexValues5"] = gblAmountMB;
	
	invokeServiceSecureAsync("doPmtCan", inputparam, callBackPmtCanServiceForDelBPMB)
}

function callBackPmtCanServiceForDelBPMB(status, result) {
	if (status == 400) {
		var activityTypeID = "";
		if(gblBPflag){
			activityTypeID = "067";
		}else{
			activityTypeID = "068";
		}
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Cancel";
        var activityFlexValues2 = frmBillPaymentView.lblBillerName.text +"+"+ removeHyphenIB(frmBillPaymentView.lblRef1value.text);     //Biller Name + Ref1
		var activityFlexValues3 = dateFormatForDisplay(gblstartOnMB); //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsMB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmountMB;  //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
		
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				activityStatus = "01";
				//Commented as  financial Activity is not required 
				/*
					var finSchduleRefId ="";	
				if(gblBPflag){
					finSchduleRefId = "SB" +frmBillPaymentView.lblScheduleRefValue.text.substring(2, frmBillPaymentView.lblScheduleRefValue.text.length);  
				}else{
					finSchduleRefId = frmBillPaymentView.lblScheduleRefValue.text.replace("F", "S"); 
				}
        	 	setFinancialActivityLogFTDelete(finSchduleRefId);
        	 	*/  
				//alert("Deleted Sucessfully");
				 alert(kony.i18n.getLocalizedString("ScheduledPayment_Delete")); 
				 /*
				 frmMBMyActivities.show();
				 showCalendar(gsSelectedDate,frmMBMyActivities,1);
				 */
				 MBMyActivitiesReloadAndShowCalendar();
			} else {
				activityStatus = "02";
				alert(result["StatusDesc"]);
				return false;
			}
		} else {
			activityStatus = "02";
			alert(result["errMsg"]);
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);	
	}
}


function checkMBStatusForDeleteMB(){
	var inputparam = [];
	inputparam["crmId"] = "";
	inputparam["rqUUId"] = "";
	showLoadingScreen();	
	invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForDeleteMB);

}

function callBackCrmProfileInqForDeleteMB(status,result){
 if (status == 400) {
  if (result["opstatus"] == 0) {
   var statusCode = result["statusCode"];
   var severity = result["Severity"];
   var statusDesc = result["StatusDesc"];
   if (statusCode != 0) {
    alert(statusDesc);
   } else {
    var mbStat = result["mbFlowStatusIdRs"];
    var ibStat = result["ibUserStatusIdTr"];
    if (mbStat != null) {
     var inputParam = [];
     inputParam["mbStatus"] = mbStat; //check this inputparam
	 inputParam["ibStatus"]= ibStat;
     invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckStatusMBForDelete);
    }
   }
  } else {
   dismissLoadingScreen();
   alert(result["errMsg"]);
  }
 } else {
  if (status == 300) {
   dismissLoadingScreen();
   //
  }
 }
}

function callBackCheckStatusMBForDelete(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
				if(flowSpa)
					{
						if (result["ibStatusFlag"] == "true") {
							//showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"),kony.i18n.getLocalizedString("info"));
							popTransferConfirmOTPLock.show();
							dismissLoadingScreen();	
							return false;
							}
						else
						{
							
							dismissLoadingScreen();	
						if(gblBPflag){
						popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Bill Payment transaction ?";
						}else{
						popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Top Up Payment transaction ?";
						}
						popupEditBPDele.show();
					}
					}
					else 
				{
				if (result["mbStatusFlag"] == "true") {
				popTransferConfirmOTPLock.show();
				//alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
				dismissLoadingScreen();	
				return false;
				} 
				else {
				
				dismissLoadingScreen();	
				if(gblBPflag){
					popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Bill Payment transaction ?";
				}else{
					popupEditBPDele.lblDeleteMsg.text =	kony.i18n.getLocalizedString("BPDeletesureMessage");//"Are you sure to delete this Top Up Payment transaction ?";
				}
				popupEditBPDele.show();
			}
			}
		} else {
			dismissLoadingScreen();	
			alert("  " + result["opstatus"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreen();	
			//
		}
	}
}



//Generating PDF and Image 
function onClickGeneratePDFImageForEditMB(filetype){
	var inputParam = {};
	var scheduleDetails ="";
	var ref2Flag = false;
	var pdfImagedata ={};
	//fileTypeForEdit = filetype;
	var AccountNo = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text;
	if(AccountNo.indexOf("-") != -1)AccountNo = replaceCommon(AccountNo, "-", "");
	var len = AccountNo.length;
	if(len == 10){
		AccountNo = "XXX-X-"+AccountNo.substring(len-6, len-1)+"-X";
	}else
	    AccountNo = frmEditFutureBillPaymentConfirm.lblFromAccountNumber.text;
	
	if(frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text == "Once"){
	 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " "+ " Repeat " +frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
	 }else if(frmEditFutureBillPaymentConfirm.lblExecutetimes.text == "-"){
	 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text + " "+ " Repeat " +frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text;
	 } else{
	 	scheduleDetails = frmEditFutureBillPaymentConfirm.lblStartDate.text +" to " +frmEditFutureBillPaymentConfirm.lblEndOnDate.text + " "+ " Repeat " +frmEditFutureBillPaymentConfirm.lblrepeatasvalue.text +" for "+frmEditFutureBillPaymentConfirm.lblExecutetimes.text+" times " ;
	 }
	 if(frmEditFutureBillPaymentConfirm.hbxRef2.isVisible){
	 	ref2Flag = true;
	 }
	 if(ref2Flag){
	 		 pdfImagedata = {
     			"AccountNo" : AccountNo,
     			"AccountName" :frmEditFutureBillPaymentConfirm.lblFromAccountName.text ,
     			"BillerName" : frmEditFutureBillPaymentConfirm.lblBillerName.text,
     			"Ref1Label" : frmEditFutureBillPaymentConfirm.lblref1.text,
	 			"Ref1Value" : frmEditFutureBillPaymentConfirm.lblRef1Value.text,
	 			"Ref2Label":frmEditFutureBillPaymentConfirm.lblRef2.text,
	 			"Ref2Value":frmEditFutureBillPaymentConfirm.lblRef2Value.text,
				"Amount" : frmEditFutureBillPaymentConfirm.lblAmtValue.text,
	 			"Fee" : frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text,	
	 			"PaymentOrderDate"	: frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text,
	 			"PaymentSchedule":scheduleDetails,
	 			"MyNote":frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text ,
	 			"TransactionRefNo": frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text,
	 			"localeId":kony.i18n.getCurrentLocale() 
   		 }
	 }else{
	 
	 		 pdfImagedata = {
    		 "AccountNo" : AccountNo,
     		 "AccountName" :frmEditFutureBillPaymentConfirm.lblFromAccountName.text ,
     		 "BillerName" : frmEditFutureBillPaymentConfirm.lblBillerName.text,
     		 "Ref1Label" : frmEditFutureBillPaymentConfirm.lblref1.text,
	 		 "Ref1Value" : frmEditFutureBillPaymentConfirm.lblRef1Value.text,
	         "Amount" : frmEditFutureBillPaymentConfirm.lblAmtValue.text,
	 		 "Fee" : frmEditFutureBillPaymentConfirm.lblPaymentFeeValue.text,	
	 		 "PaymentOrderDate"	: frmEditFutureBillPaymentConfirm.lblPaymentDateValue.text,
	 		 "PaymentSchedule":scheduleDetails,
			 "MyNote":frmEditFutureBillPaymentConfirm.lblMyNotesDesc.text ,
	 		"TransactionRefNo": frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text,
	 		"localeId":kony.i18n.getCurrentLocale() 
    	}
	 
	 }

    inputParam["filetype"] = filetype;
    var activityTypeId = "";
    if(gblBPflag){
    	activityTypeId = "067";
    	inputParam["templatename"] = "FutureBillPaymentTemplate";
    	inputParam["outputtemplatename"]= "Future_Bill_Payment_Set_Details_"+kony.os.date("ddmmyyyy");
    }else{
    	activityTypeId = "068";
    	inputParam["templatename"] = "FutureTopUpPaymentComplete";
    	inputParam["outputtemplatename"] =  "Future_Top_Up_Set_Details_"+kony.os.date("ddmmyyyy");
    }
    inputParam["Ref2Falg"] = ref2Flag;
   // inputParam["localeId"]= kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    // var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext+"/GenericPdfImageServlet";
	//var url = "http://10.11.12.212:9080/tmb/GenericPdfImageServlet";

	//invokeServiceSecureAsync("generatePdfImage", inputParam, mbRenderFileCallbackfunction);

 //	kony.net.invokeServiceAsync(url, inputParam, callbackfunctionForPDFImageMBBPTP);

	saveFuturePDF(filetype, activityTypeId, frmEditFutureBillPaymentConfirm.lblScheduleRefValue.text);
}



function shareFBMBBPTP(){

		url="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.tmbbank.com&t";
	//	frmTransferGeneratePDF.browTransferPDF.requestURLConfig={URL:url,requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"}
	    frmContactusFAQMB.browser506459299404741.requestURLConfig={URL:url,requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"}
	    frmContactusFAQMB.hbox475124774143.setVisibility(false);
	    frmContactusFAQMB.hbox476018633224052.setVisibility(false);
	    frmContactusFAQMB.button506459299404751.setVisibility(false);
		frmContactusFAQMB.show();
		return false;
}

/*
function frmBillPaymentViewPreShow(){
	frmBillPaymentView.lblHead.text = kony.i18n.getLocalizedString("keyBillPaymentViewMB");
	frmBillPaymentView.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmBillPaymentView.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	frmBillPaymentView.lblref1.text = kony.i18n.getLocalizedString("Ref1");
	frmBillPaymentView.lblref2.text = kony.i18n.getLocalizedString("Ref2");
	frmBillPaymentView.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	frmBillPaymentView.lblAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentView.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmBillPaymentView.lblPaymentDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmBillPaymentView.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmBillPaymentView.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmBillPaymentView.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmBillPaymentView.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmBillPaymentView.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmBillPaymentView.lblMyNotes.text = kony.i18n.getLocalizedString("keyMyNote");
	frmBillPaymentView.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	if(flowSpa){
	  frmBillPaymentView.btnreturnSPA.text = kony.i18n.getLocalizedString("keyXferBtnRetn");
	}else
	  frmBillPaymentView.btnreturn.text = kony.i18n.getLocalizedString("keyXferBtnRetn");
	  
	gblLocale = false;
}


function frmBillPaymentEditPreShow(){
    frmBillPaymentEdit.lblHead.text = kony.i18n.getLocalizedString("keyEditBillPaymentMB");
	frmBillPaymentEdit.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmBillPaymentEdit.lblAvailableBalance.text = kony.i18n.getLocalizedString("keyAvailableBalanceMB");
	frmBillPaymentEdit.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	frmBillPaymentEdit.lblref1.text = kony.i18n.getLocalizedString("Ref1");
	frmBillPaymentEdit.lblref2.text = kony.i18n.getLocalizedString("Ref2");
	frmBillPaymentEdit.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	frmBillPaymentEdit.labelOnlineAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentEdit.labelPenaltyAmt.text = kony.i18n.getLocalizedString("keyPenalty");
	frmBillPaymentEdit.buttonOnlineFull.text = kony.i18n.getLocalizedString("keyBillPaymentFull");
	frmBillPaymentEdit.buttonOnlineSpecified.text = kony.i18n.getLocalizedString("keyBillPaymentSpecified");
	frmBillPaymentEdit.btnfullpay.text = kony.i18n.getLocalizedString("keyBillPaymentFull");
	frmBillPaymentEdit.btnminpay.text = kony.i18n.getLocalizedString("keyBillPaymentMinimum");
	frmBillPaymentEdit.btnspecpay.text = kony.i18n.getLocalizedString("keyBillPaymentSpecified");
	frmBillPaymentEdit.lblTPamount.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentEdit.lblAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmBillPaymentEdit.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmBillPaymentEdit.lblPaymentDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmBillPaymentEdit.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmBillPaymentEdit.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmBillPaymentEdit.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmBillPaymentEdit.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmBillPaymentEdit.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmBillPaymentEdit.lblMyNotes.text = kony.i18n.getLocalizedString("keyMyNote");
	frmBillPaymentEdit.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	
	if(flowSpa){
	  frmBillPaymentEdit.btnCancelSPA.text = kony.i18n.getLocalizedString("keyCancelButton");
	  frmBillPaymentEdit.btnConfirmSPA.text = kony.i18n.getLocalizedString("Next");
	}else{
	  frmBillPaymentEdit.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	  frmBillPaymentEdit.btnConfirm.text = kony.i18n.getLocalizedString("Next");
	}
	gblLocale = false;
}

function frmEditFutureBillPaymentConfirmPreShow(){
    frmEditFutureBillPaymentConfirm.lblHead.text = kony.i18n.getLocalizedString("Confirmation");
	frmEditFutureBillPaymentConfirm.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmEditFutureBillPaymentConfirm.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	frmEditFutureBillPaymentConfirm.lblref1.text = kony.i18n.getLocalizedString("Ref1");
	frmEditFutureBillPaymentConfirm.lblref2.text = kony.i18n.getLocalizedString("Ref2");
	frmEditFutureBillPaymentConfirm.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	frmEditFutureBillPaymentConfirm.lblAmt.text = kony.i18n.getLocalizedString("keyAmount");
	frmEditFutureBillPaymentConfirm.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmEditFutureBillPaymentConfirm.lblPaymentDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmEditFutureBillPaymentConfirm.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmEditFutureBillPaymentConfirm.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmEditFutureBillPaymentConfirm.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmEditFutureBillPaymentConfirm.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmEditFutureBillPaymentConfirm.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmEditFutureBillPaymentConfirm.lblMyNotes.text = kony.i18n.getLocalizedString("keyMyNote");
	frmEditFutureBillPaymentConfirm.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	
	if(flowSpa){
	   frmEditFutureBillPaymentConfirm.btnCancelSPA.text = kony.i18n.getLocalizedString("keyCancelButton");
	   frmEditFutureBillPaymentConfirm.btnConfirmSPA.text = kony.i18n.getLocalizedString("keyConfirm");
	}else{
	   frmEditFutureBillPaymentConfirm.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	   frmEditFutureBillPaymentConfirm.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	}
	
	gblLocale = false;
}

function frmEditFutureBillPaymentCompletePreShow(){
    frmEditFutureBillPaymentComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
	frmEditFutureBillPaymentComplete.lblFrom.text = kony.i18n.getLocalizedString("keyFrom");
	frmEditFutureBillPaymentComplete.lblBiller.text = kony.i18n.getLocalizedString("keyBillPaymentConfToBiller");
	frmEditFutureBillPaymentComplete.lblRef1.text = kony.i18n.getLocalizedString("Ref1");
	frmEditFutureBillPaymentComplete.lblRef2.text = kony.i18n.getLocalizedString("Ref2");
	frmEditFutureBillPaymentComplete.lblPaymentDetails.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentDetails");
	frmEditFutureBillPaymentComplete.lblAmount.text = kony.i18n.getLocalizedString("keyAmount");
	frmEditFutureBillPaymentComplete.lblPaymentFee.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentFee");
	frmEditFutureBillPaymentComplete.lblPaymentOrderDate.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	frmEditFutureBillPaymentComplete.lblScheduleDetails.text = kony.i18n.getLocalizedString("keyBillPaymentScheduleDetails");
	frmEditFutureBillPaymentComplete.lblStartOn.text = kony.i18n.getLocalizedString("keyBillPaymentStartOn");
	frmEditFutureBillPaymentComplete.lblRepeatas.text = kony.i18n.getLocalizedString("keyBillPaymentRepeatAs");
	frmEditFutureBillPaymentComplete.lblEndOn.text = kony.i18n.getLocalizedString("keyBillPaymentEndOn");
	frmEditFutureBillPaymentComplete.lblExecute.text = kony.i18n.getLocalizedString("keyBillPaymentExecute");
	frmEditFutureBillPaymentComplete.lblMyNote.text = kony.i18n.getLocalizedString("keyMyNote");
	frmEditFutureBillPaymentComplete.lblScheduleRefNo.text = kony.i18n.getLocalizedString("keyScheduleRefNoMB");
	//frmEditFutureBillPaymentComplete.lblMsg.text = kony.i18n.getLocalizedString("ATMmsg");
	if(flowSpa){
	  frmEditFutureBillPaymentComplete.btnCancelSPA.text = kony.i18n.getLocalizedString("keyReturn");
	}else
	  frmEditFutureBillPaymentComplete.btnCancel.text = kony.i18n.getLocalizedString("keyReturn");
	
	gblLocale = false;
}

function frmScheduleBillPayEditFuturePreShow(){
 frmScheduleBillPayEditFuture.lblSchedule.text = kony.i18n.getLocalizedString("keyBillPaymentSchedule");
 frmScheduleBillPayEditFuture.lblDate.text = kony.i18n.getLocalizedString("Transfer_Date");
 frmScheduleBillPayEditFuture.lblRepeat.text = kony.i18n.getLocalizedString("keyRepeat");
 frmScheduleBillPayEditFuture.btnDaily.text = kony.i18n.getLocalizedString("keyDaily");
 frmScheduleBillPayEditFuture.btnWeekly.text = kony.i18n.getLocalizedString("keyWeekly");
 frmScheduleBillPayEditFuture.btnMonthly.text = kony.i18n.getLocalizedString("keyMonthly");
 frmScheduleBillPayEditFuture.btnYearly.text = kony.i18n.getLocalizedString("keyYearly");
 frmScheduleBillPayEditFuture.lblEnd.text = kony.i18n.getLocalizedString("Transfer_Ending");
 frmScheduleBillPayEditFuture.btnNever.text = kony.i18n.getLocalizedString("keyNever");
 frmScheduleBillPayEditFuture.btnAfter.text = kony.i18n.getLocalizedString("keyAfter");
 frmScheduleBillPayEditFuture.btnOnDate.text = kony.i18n.getLocalizedString("keyOnDate");
 frmScheduleBillPayEditFuture.lblTimesMB.text = kony.i18n.getLocalizedString("keyTimesMB");
 frmScheduleBillPayEditFuture.lblNumberOfTimes.text = kony.i18n.getLocalizedString("keyIncludeThisTime");
 frmScheduleBillPayEditFuture.lblScheduleUntil.text = kony.i18n.getLocalizedString("keyUntil");
 frmScheduleBillPayEditFuture.btnSaveSchedule.text = kony.i18n.getLocalizedString("keysave");
}

*/


function isBPScheduleEdited(){
	gblScheduleFreqChangedMB = false;

	
	var tmpDate = formatDateFT(gblstartOnMB);
	
	
	if(tmpDate != frmBillPaymentEdit.lblStartDate.text){
		gblScheduleFreqChangedMB = true;
	}else{
		gblScheduleFreqChangedMB = false;
		
  		
		if(gblOnLoadRepeatAsMB != frmBillPaymentEdit.labelRepeatAsValue.text){
			gblScheduleFreqChangedMB = true;
		}else{
			gblScheduleFreqChangedMB = false;
			
			if(gblEndingFreqOnLoadMB !=  gblScheduleEndBPMB){
			
				gblScheduleFreqChangedMB = true;  //OnClickEndFreqMB
			}else{
				 gblScheduleFreqChangedMB = false;
				if(gblEndingFreqOnLoadMB == kony.i18n.getLocalizedString("keyOnDate")){
					 var temp = gblendDateOnLoadMB;
					 if(temp.indexOf("-",0) != -1){
			         	temp = formatDateFT(gblFTViewEndDate);
			       	 }
			       	 if(temp != frmBillPaymentEdit.lblEndOnDate.text)
			       	 		 gblScheduleFreqChangedMB = true;
				}else if(gblEndingFreqOnLoadMB == kony.i18n.getLocalizedString("keyAfter")){
				
					if(parseFloat(gblexecutionOnLoadMB.toString()) != parseFloat(frmBillPaymentEdit.lblExecutetimes.text))
									gblScheduleFreqChangedMB = true;
					
				}
			}
		}
	}
	
	onClickNextEditPage();	
	}
