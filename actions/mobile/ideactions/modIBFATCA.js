/**
 * Function to show pop up info for flag '0' customers
 */
function showFACTAInfoIB() {
	if(gblFATCAFlow == "login") {
		popupIBFATCAInfo.btnInfoNext.skin= "btnIB289";
		popupIBFATCAInfo.btnInfoNext.focusSkin= "btnIB289";
		popupIBFATCAInfo.btnInfoNext.hoverSkin= "btnIB289";
	}
	else {
		popupIBFATCAInfo.btnInfoNext.skin="btnIB289";
		popupIBFATCAInfo.btnInfoNext.focusSkin= "btnIB289";
		popupIBFATCAInfo.btnInfoNext.hoverSkin= "btnIB289";
	}
	popupIBFATCAInfo.hbxInfoSkipNext.setVisibility(true);
	popupIBFATCAInfo.hbxInfoClose.setVisibility(false);
	popupIBFATCAInfo.lblText.text = kony.i18n.getLocalizedString("keyFATCATnCInfo");
	popupIBFATCAInfo.btnInfoNext.text = kony.i18n.getLocalizedString("Next");
	popupIBFATCAInfo.btnInfoClose.text = kony.i18n.getLocalizedString("keyFATCAClose");
	popupIBFATCAInfo.show();
}



/**
 * Function called when click on next button of FATCA info pop Up
 */
function onClickIBFATCAInfoClose() {
	if(gblFATCAUpdateError == true) {
		// navigate to home page
		popupIBFATCAInfo.dismiss();
		onclickActivationCompleteStart();
	}
	else {
		popupIBFATCAInfo.hbxInfoSkipNext.setVisibility(true);
		popupIBFATCAInfo.hbxInfoClose.setVisibility(false);
		popupIBFATCAInfo.btnInfoSkip.skin = "btnIBPopUp24pxInActive";
		popupIBFATCAInfo.btnInfoSkip.focusSkin = "btnIBPopUp24pxInActive";
		popupIBFATCAInfo.btnInfoSkip.hoverSkin = "btnIBPopUp24pxInActive";
		popupIBFATCAInfo.btnInfoSkip.setEnabled(false);
		popupIBFATCAInfo.lblText.text = kony.i18n.getLocalizedString("keyFATCATnCInfo");
	}
}

/**
 * Service call back to get terms and conditions for FATCA forms and to set terms and conditions.
 */
function setFATCAIBTermsAndConditions() {
	gblFATCAAnswers = {};
	gblFATCAQuestionOrder = [];
	gblFATCAQuestionEN = [];
	gblFATCAQuestionTH = [];
	
	frmIBFATCATandC.hbxFATCATnCContainer.setVisibility(true);
	frmIBFATCATandC.hbxAgreeCheck.setVisibility(true);
	frmIBFATCATandC.hbxFATCATnCNext.setVisibility(true);
	frmIBFATCATandC.hbxFATCABranch.setVisibility(false);
	frmIBFATCATandC.hbxFATCABranchInfo.setVisibility(false);
	frmIBFATCATandC.hbxFATCACloseBranch.setVisibility(false);
	frmIBFATCATandC.btnNext.text = kony.i18n.getLocalizedString("Next");
	frmIBFATCATandC.lblAgreeText.text = kony.i18n.getLocalizedString("keyFATCAAgree");
	frmIBFATCATandC.btnAgreeCheck.skin = "btnCheck";
	frmIBFATCATandC.btnNext.skin = "btnIB320dis";
	
	if(kony.i18n.getCurrentLocale() == "en_US")
		frmIBFATCATandC.lblTandCFATCA.text = GLOBAL_FATCA_TNC_en_US;
	else
		frmIBFATCATandC.lblTandCFATCA.text = GLOBAL_FATCA_TNC_th_TH;
	dismissLoadingScreenPopup();
	frmIBFATCATandC.show();
	if(document.getElementById("frmIBFATCATandC_btnNext") != null)
		document.getElementById("frmIBFATCATandC_btnNext").style.cursor = "default";
}

/**
 * On click function of TnC agree button which enable/disables the next button
 */
function onClickIBFATCATnCAgreeBtn(eventObject) {
	if(eventObject.skin == "btnCheck") {
		eventObject.skin = "btnCheckFoc";
		frmIBFATCATandC.btnNext.skin = "btnIB320";
		if(document.getElementById("frmIBFATCATandC_btnNext") != null)
			document.getElementById("frmIBFATCATandC_btnNext").style.cursor = "pointer";
		frmIBFATCATandC.btnNext.setEnabled(true);
	}
	else {
		eventObject.skin = "btnCheck";
		frmIBFATCATandC.btnNext.skin = "btnIB320dis";
		if(document.getElementById("frmIBFATCATandC_btnNext") != null)
			document.getElementById("frmIBFATCATandC_btnNext").style.cursor = "default";
		frmIBFATCATandC.btnNext.setEnabled(false);
	}
}

/**
 * On click function on next button on TnC form
 */
function onClickIBFATCATnCNext() {
	if(frmIBFATCATandC.btnAgreeCheck.skin == "btnCheckFoc") {
		gblFATCAPage = 1;
		getFATCAQuestionsIBServiceCall();
	}
}

/**
 * Function to set 'Go to branch' message for fatcaFlag = '8' or '9'
 */
function setFATCAIBGoToBranchInfoMessage(flag) {
	frmIBFATCATandC.imgFATCABranch.src = "icon_notcomplete.png";
	if(gblFATCAFlow == "login")
		frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_001");
	else {
		if(flag == "Y")
			frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_005");
		else
			frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_003");
	}
	frmIBFATCATandC.btnFATCACloseBranch.text = kony.i18n.getLocalizedString("keyFATCAClose");
	frmIBFATCATandC.hbxFATCATnCContainer.setVisibility(false);
	frmIBFATCATandC.hbxAgreeCheck.setVisibility(false);
	frmIBFATCATandC.hbxFATCATnCNext.setVisibility(false);
	frmIBFATCATandC.hbxFATCABranch.setVisibility(true);
	frmIBFATCATandC.hbxFATCABranchInfo.setVisibility(true);
	frmIBFATCATandC.hbxFATCACloseBranch.setVisibility(true);
	frmIBFATCATandC.show();
}

/**
 * Service call function for getting FATCA questions
 */
function getFATCAQuestionsIBServiceCall() {
	showLoadingScreenPopup();
	var inputparams = {};
	inputparams["page"] = gblFATCAPage;
	invokeServiceSecureAsync("getFATCAQuestions", inputparams, getFATCAQuestionsIBServiceCallBack);
}

/**
 * Service call back function for getting FATCA questions and populates questions to segment and setting skins for step indicator and button
 */
function getFATCAQuestionsIBServiceCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var quesListOrderArr = resulttable["FatcaQuesOrder"].split(",");
			gblFATCAQuestionOrder = [];
            gblFATCAQuestionEN = {};
            gblFATCAQuestionTH = {};
            for (var i = 0; i < quesListOrderArr.length; i++) {
                var quesCode = quesListOrderArr[i].trim();
                gblFATCAQuestionOrder.push(quesCode);
                if (resulttable["FatcaQues_EN"][0].hasOwnProperty(quesCode)) {
                    gblFATCAQuestionEN[quesCode] = resulttable["FatcaQues_EN"][0][quesCode];
                    gblFATCAQuestionTH[quesCode] = resulttable["FatcaQues_TH"][0][quesCode];
                }
            }
            populateIBFATCAQuestion();
            
            //ENH_221 Make 'No' as a default in FATCA flow
            frmIBFATCAQuestionnaire.btnFATCAQues1Next.skin = "btnIB320";
     		frmIBFATCAQuestionnaire.btnFATCAQues1Next.setEnabled(true);
			
			//frmIBFATCAQuestionnaire.btnFATCAQues1Next.skin = "btnIB320dis";
			//frmIBFATCAQuestionnaire.btnFATCAQues1Next.setEnabled(false);
			
            if(gblFATCAPage == 2) {
				frmIBFATCAQuestionnaire.hbxIndicatorStep1.skin = "hbxStepmidbggray";
				frmIBFATCAQuestionnaire.hbxIndicatorStep2.skin = "hbxStepLeftBG";
				frmIBFATCAQuestionnaire.lblIndicatorStep1.skin = "lblIB18pxGreyFATCA";
				frmIBFATCAQuestionnaire.lblIndicatorStep2.skin = "lblIB18pxBlack";
				frmIBFATCAQuestionnaire.lblImgIndicatorStep1.skin = "lblstepgray";
				frmIBFATCAQuestionnaire.lblImgIndicatorStep2.skin = "lblstepblue";
			}
			else {
				frmIBFATCAQuestionnaire.show();
			}
			if(document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next") != null)
				document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next").style.cursor = "default";
        }
		dismissLoadingScreenPopup();
	}
	dismissLoadingScreenPopup();
}

/**
 * Function to populate questions to segment
 */
function populateIBFATCAQuestion() {
    var segData = [];
	frmIBFATCAQuestionnaire.segQuestion.removeAll();
	for(var i=0; i<gblFATCAQuestionOrder.length; i++) {
    	var quesCode = gblFATCAQuestionOrder[i].trim();
    	var ques = "";
		var temp = {};
		var text = "";
		var help = "";
       	if(kony.i18n.getCurrentLocale() == "en_US") {
       		ques = gblFATCAQuestionEN[quesCode];;
       	}
       	else {
      		ques = gblFATCAQuestionTH[quesCode];;
       	}
      	var arr = ques.split("|||");
		text = arr[0].trim();
		if(arr[1] != undefined)
			help = arr[1].trim();
		else
			help = "";
		if(help != "") {
			if(window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8")
				text += ("<br/><div style='font-size:12px; line-height:120%;'>" + help + "</div>");
			else
				text += ("<br/><div style='font-size:16px; line-height:90%;'>" + help + "</div>");
		}
		
		temp = {
			"lblQues": text,
			"lblRadioNo": kony.i18n.getLocalizedString("keyFATCANo"),
			"lblRadioYes": kony.i18n.getLocalizedString("keyFATCAYes"),
			"btnRadioNo": {
				skin: "btnRadioFoc",
				focusskin: "btnRadioFoc"
			},
			"btnRadioYes": {
				skin: "btnRadio",
				focusskin: "btnRadio"
			},
			"quesCode": quesCode
		};
		
		//ENH_221 Make 'No' as a default in FATCA flow
		gblFATCAUpdateFlag = "N";
		gblFATCAAnswers[gblFATCAQuestionOrder[i]["quesCode"]] = "N";
		segData.push(temp);
	}	
	
	frmIBFATCAQuestionnaire.segQuestion.setData(segData);
	changeFATCAFormLocaleIB();	
	
}

/**
 * On click function for radio button inside segement which toggle between 'Yes' and 'No'
 * Also function calculates fatcaUpdateFlag and checks if 'Next' button can be enabled or not
 */
function onClickFATCAIBRadioBtn(eventObject) {
	var selectedIndex = frmIBFATCAQuestionnaire.segQuestion.selectedIndex[1];
    var segData = frmIBFATCAQuestionnaire.segQuestion.data;
	var clicked = 0;
	if(segData[selectedIndex]["btnRadioYes"]["skin"] == "btnRadioFoc")
		clicked = 1;
	else if(segData[selectedIndex]["btnRadioNo"]["skin"] == "btnRadioFoc")
		clicked = 2;
	else
		clicked = 0;
	
	if(eventObject.id == "btnRadioNo") {
		segData[selectedIndex]["btnRadioNo"]["skin"] = "btnRadioFoc";
		segData[selectedIndex]["btnRadioNo"]["focusskin"] = "btnRadioFoc";
		if(clicked == 1) {
			segData[selectedIndex]["btnRadioYes"]["skin"] = "btnRadio";
			segData[selectedIndex]["btnRadioYes"]["focusskin"] = "btnRadio";
		}
	}
	else if(eventObject.id == "btnRadioYes") {
		segData[selectedIndex]["btnRadioYes"]["skin"] = "btnRadioFoc";
		segData[selectedIndex]["btnRadioYes"]["focusskin"] = "btnRadioFoc";
		if(clicked == 2) {
			segData[selectedIndex]["btnRadioNo"]["skin"] = "btnRadio";
			segData[selectedIndex]["btnRadioNo"]["focusskin"] = "btnRadio";
		}
	}
	
	//Updating the fatca flag based on the answers by user
	gblFATCAUpdateFlag = "N";
	for(i=0; i<segData.length; i++) {
		if(segData[i]["btnRadioYes"]["skin"] == "btnRadioFoc") {
			if(gblFATCAPage == 1)
				gblFATCAUpdateFlag = "9";
			else if(gblFATCAPage == 2)
				gblFATCAUpdateFlag = "8";
			gblFATCAAnswers[segData[i]["quesCode"]] = "Y";
		}
		else if(segData[i]["btnRadioNo"]["skin"] == "btnRadioFoc") {
			gblFATCAAnswers[segData[i]["quesCode"]] = "N";
		}
		else {
			break;
		}
	}
	//Enabling 'Next' button when all answers are answered
	if(i == segData.length) {
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.skin = "btnIB320";
		if(document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next") != null)
			document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next").style.cursor = "pointer";
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.setEnabled(true);
	}
	else {
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.skin = "btnIB320dis";
		if(document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next") != null)
			document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next").style.cursor = "default";
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.setEnabled(false);
	}
	
    //frmIBFATCAQuestionnaire.segQuestion.removeAll();
    frmIBFATCAQuestionnaire.segQuestion.setData(segData);
}

/*
function onClickFATCAIBRadioBtn(eventObject) {
	var selectedIndex = frmIBFATCAQuestionnaire.segQuestion.selectedIndex[1];
	var segData = {};
	segData = frmIBFATCAQuestionnaire.segQuestion.data[selectedIndex];
	
	var clicked = 0;
	if(segData["btnRadioYes"]["skin"] == "btnRadioFoc")
		clicked = 1;
	else if(segData["btnRadioNo"]["skin"] == "btnRadioFoc")
		clicked = 2;
	else
		clicked = 0;
	
	if(eventObject.id == "btnRadioNo") {
		segData["btnRadioNo"]["skin"] = "btnRadioFoc";
		segData["btnRadioNo"]["focusskin"] = "btnRadioFoc";
		if(clicked == 1) {
			segData["btnRadioYes"]["skin"] = "btnRadio";
			segData["btnRadioYes"]["focusskin"] = "btnRadio";
		}
	}
	else if(eventObject.id == "btnRadioYes") {
		segData["btnRadioYes"]["skin"] = "btnRadioFoc";
		segData["btnRadioYes"]["focusskin"] = "btnRadioFoc";
		if(clicked == 2) {
			segData["btnRadioNo"]["skin"] = "btnRadio";
			segData["btnRadioNo"]["focusskin"] = "btnRadio";
		}
	}
	
	frmIBFATCAQuestionnaire.segQuestion.setDataAt(segData, selectedIndex);
	
	//Updating the fatca flag based on the answers by user
	segData = [];
	segData = frmIBFATCAQuestionnaire.segQuestion.data;
	gblFATCAUpdateFlag = "N";
	for(i=0; i<segData.length; i++) {
		if(segData[i]["btnRadioYes"]["skin"] == "btnRadioFoc") {
			if(gblFATCAPage == 1)
				gblFATCAUpdateFlag = "9";
			else if(gblFATCAPage == 2)
				gblFATCAUpdateFlag = "8";
			gblFATCAAnswers[segData[i]["quesCode"]] = "Y";
		}
		else if(segData[i]["btnRadioNo"]["skin"] == "btnRadioFoc") {
			gblFATCAAnswers[segData[i]["quesCode"]] = "N";
		}
		else {
			break;
		}
	}
	//Enabling 'Next' button when all answers are answered
	if(i == segData.length) {
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.skin = "btnIB320";
		if(document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next") != null)
			document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next").style.cursor = "pointer";
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.setEnabled(true);
	}
	else {
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.skin = "btnIB320dis";
		if(document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next") != null)
			document.getElementById("frmIBFATCAQuestionnaire_btnFATCAQues1Next").style.cursor = "default";
		frmIBFATCAQuestionnaire.btnFATCAQues1Next.setEnabled(false);
	}
}
*/


/**
 * on Click function of 'Next' button of Questionnaire forms. shows forms based of fatcaUpdate flag
 */
function populateIBFATCAPageTwo() {
	if(gblFATCAUpdateFlag == "8" || gblFATCAUpdateFlag == "9") {
		IBFATCAUpdateServiceCall();
	}
	else {
		if(gblFATCAPage == 1) {
			gblFATCAPage = 2;
			getFATCAQuestionsIBServiceCall();
		}
		else {
			IBFATCAUpdateServiceCall();
		}
	}
}

/**
 * Function to set 'FATCA finish' message for fatcaFlag = 'N'
 */
function setFATCAIBFormFinishInfoMessage() {
	frmIBFATCATandC.imgFATCABranch.src = "iconcomplete.png";
	if(gblFATCAFlow =="login")
		frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_002");
	else
		frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_006");
	frmIBFATCATandC.btnFATCACloseBranch.text = kony.i18n.getLocalizedString("keyFATCAClose");
	frmIBFATCATandC.hbxFATCATnCContainer.setVisibility(false);
	frmIBFATCATandC.hbxAgreeCheck.setVisibility(false);
	frmIBFATCATandC.hbxFATCATnCNext.setVisibility(false);
	frmIBFATCATandC.hbxFATCABranch.setVisibility(true);
	frmIBFATCATandC.hbxFATCABranchInfo.setVisibility(true);
	frmIBFATCATandC.hbxFATCACloseBranch.setVisibility(true);
	frmIBFATCATandC.show();
}

/**
 * Service call function for 'FatcaUpdate'
 */
function IBFATCAUpdateServiceCall() {
	showLoadingScreenPopup();
	var inputparams = {};
	inputparams["AnswerKey"] = JSON.stringify(gblFATCAAnswers);
	inputparams["fatcaFlow"] = gblFATCAFlow;
	invokeServiceSecureAsync("FatcaUpdate", inputparams, IBFATCAUpdateServiceCallBack);
}

/**
 * Service call back function for 'FatcaUpdate' which set 'Go to branch' or 'form finish' message based on fatcaFlag updated
 */
function IBFATCAUpdateServiceCallBack(status, resulttable) {
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	if(resulttable["FATCAFlag"] == "8" || resulttable["FATCAFlag"] == "9") {
	        	setFATCAIBGoToBranchInfoMessage("Y");
	        }
	        else {
	        	setFATCAIBFormFinishInfoMessage();
	        }
        }
        else {
        	// create a custom pop-up and on close button navigate to home page
			gblFATCAUpdateError = true;
			popupIBFATCAInfo.hbxInfoSkipNext.setVisibility(false);
			popupIBFATCAInfo.hbxInfoClose.setVisibility(true);
			popupIBFATCAInfo.btnInfoClose.text = kony.i18n.getLocalizedString("keyFATCAClose");
			popupIBFATCAInfo.lblText.text = kony.i18n.getLocalizedString("ECGenericError");
			popupIBFATCAInfo.show();
			// alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
        dismissLoadingScreenPopup();
	}
	dismissLoadingScreenPopup();
}

/**
 * Function called when locale change of fatca TnC form
 */
function changeFATCATnCLocaleIB() {
	frmIBFATCATandC.btnFATCACloseBranch.text = kony.i18n.getLocalizedString("keyFATCAClose");
	frmIBFATCATandC.btnNext.text = kony.i18n.getLocalizedString("Next");
	frmIBFATCATandC.lblAgreeText.text = kony.i18n.getLocalizedString("keyFATCAAgree");
	if(gblFATCAFlow == "login") {
		if(gblFATCAUpdateFlag == "N")
			frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_002");
		else
			frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_001");
	}
	else {
		if(gblFATCAUpdateFlag == "N")
			frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_006");
		else {
			if(JSON.stringify(gblFATCAAnswers).length <= 2)
				frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_005");
			else
				frmIBFATCATandC.lblFATCABranch.text = kony.i18n.getLocalizedString("keyFATCAError_003");
		}
	}
	if(kony.i18n.getCurrentLocale() == "en_US")
		frmIBFATCATandC.lblTandCFATCA.text = GLOBAL_FATCA_TNC_en_US;
	else
		frmIBFATCATandC.lblTandCFATCA.text = GLOBAL_FATCA_TNC_th_TH;
}

/**
 * Function called when locale change of fatca questionarre form
 */
function changeFATCAFormLocaleIB() {
	if(gblFATCAPage == 1) {
		frmIBFATCAQuestionnaire.lblQuestionHrd1.text = kony.i18n.getLocalizedString("keyFATCAPart1Info");
		frmIBFATCAQuestionnaire.lblQuestionHrd2.text = kony.i18n.getLocalizedString("keyFATCAPart1");
	}
	else {
		frmIBFATCAQuestionnaire.lblQuestionHrd1.text = kony.i18n.getLocalizedString("keyFATCAPart2Info");
		frmIBFATCAQuestionnaire.lblQuestionHrd2.text = kony.i18n.getLocalizedString("keyFATCAPart2");
	}

	frmIBFATCAQuestionnaire.btnFATCAQues1Next.text = kony.i18n.getLocalizedString("Next");
	frmIBFATCAQuestionnaire.lblIndicatorStep1.text = kony.i18n.getLocalizedString("keyStep1Label");
	frmIBFATCAQuestionnaire.lblIndicatorStep2.text = kony.i18n.getLocalizedString("keyStep2Label");
}

/**
 * Function called when locale change of fatca questionarre form to load question with changed locale
 */
function changeFATCAQuestionLocaleIB() {
	var segData = frmIBFATCAQuestionnaire.segQuestion.data;
	for(var i=0; i<segData.length; i++) {
		var quesCode = segData[i]["quesCode"];
		var ques = "";
		var text = "";
		var help = "";
		if(kony.i18n.getCurrentLocale() == "en_US")
			ques = gblFATCAQuestionEN[quesCode];
		else
			ques = gblFATCAQuestionTH[quesCode];
		var arr = ques.split("|||");
		text = arr[0].trim();
		if(arr[1] != undefined)
			help = arr[1].trim();
		else
			help = "";
		if(help != "") {
			var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
			if(isIE8)
				text += ("<br/><div style='font-size:12px; line-height:120%;'>" + help + "</div>");
			else
				text += ("<br/><div style='font-size:16px; line-height:90%;'>" + help + "</div>");
		}
		segData[i]["lblQues"] = text;
		segData[i]["lblRadioNo"] =  kony.i18n.getLocalizedString("keyFATCANo");
		segData[i]["lblRadioYes"] = kony.i18n.getLocalizedString("keyFATCAYes");
	}
	
    frmIBFATCAQuestionnaire.segQuestion.setData(segData);
}

/**
 * Function called to initialize FATCA global variables
 */
function initFATCAGlobalVars(){
	FatcaQues_EN = {};
	FatcaQues_TH = {};
	gblFATCAAnswers = {};
	gblFATCAQuestionOrder = [];
	gblFATCAUpdateFlag = "";
	gblFATCAQuestionEN = [];
	gblFATCAQuestionTH = [];
	gblFATCAPage = "";
	gblFATCAFlow = "";
	gblFATCASkipCounter = "";
	gblFATCAUpdateError = false;
}