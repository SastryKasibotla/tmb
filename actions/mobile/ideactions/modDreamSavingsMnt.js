gblDreamAct=""
var GBLgetDreamTargetID = "";
var gbllength = 0;
var gblFourteenDigAccount = "";
var gblpersonalizedId = "";
var prodCode = "";
var prodName = "";
var gblAccNo = "";
var gblAccntType = "";
var MonthlySavingAmntUpdate = "";
var ChannelID ;
var gblAccountFromforRexFor = "";
var gblAccountFromforRex= "";
var gblflagDreamMB = "false";
gblprodCodeSpa=null;
gblprodNameSpa=null;
var mnthlyTransferDate="";
var gblshowAccountBox=false;
var gblAccountSummaryFlagMB = "false"
var DreamProdNickname = "";
DreamCustomerNickname = "";
gblPersonalizedRecordIdFromDreamSavings="";
var avaliableCurrCode;
var FromProductIdDreamMB;
function popupdeleteDreamSave(imgIcon, confText, callBackOnConfirm, callBackCancel) {
    if(flowSpa)
    {
    	popupConfirmation.lblPopupConfText.text = kony.i18n.getLocalizedString("keylblDelAcc");
    }
    else
    {
   		popupConfirmation.lblPopupConfText.text = kony.i18n.getLocalizedString("keyMBDreamDeleteAcnt");
    }
    popupConfirmation.btnpopConfConfirm.onClick = callBackOnConfirm;
    popupConfirmation.btnPopupConfCancel.onClick = callBackCancel;
    popupConfirmation.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    popupConfirmation.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyYes");
    popupConfirmation.show();
}

function clearDreamDataMaintMB(){
		
		frmDreamSavingMB.lblAccountFrom.text="";
		frmDreamSavingMB.lblAccountNicknameValue.text="";
		frmDreamSavingMB.lblAcntNoValue.text="";
		frmDreamSavingMB.lblBranchVal.text="";
		frmDreamSavingMB.lblDreamDesc.text="";
		frmDreamSavingMB.lblfromNickname.text="";
		frmDreamSavingMB.lblInterestRateVal.text="";
		frmDreamSavingMB.lblMnthlySavingAmntVal.text="";
		frmDreamSavingMB.lblTranfrEveryMnthVal.text="";
		frmDreamSavingMB.lblTargetAmount.text="";
		frmDreamSavingMB.lblOpeningDateVal.text="";
		if (gblAccountSummaryFlagMB=="true"){
		}else{
		calldepositAccountInqDMS()
		}
		
}
function dreamSavingPopulateDate() {
	gblflagDreamMB = "true";
    frmDreamSavingMB.imgProd.src = frmDreamSavingEdit["segSlider"]["selectedItems"][0].img1;
    
  //  frmDreamSavingEdit.lblDreamDescVal.text = frmDreamSavingEdit.segSlider.selectedItems[0]["imgDreamName"];
	 gblProductImage1 = frmDreamSavingEdit["segSlider"]["selectedItems"][0].img1
	 gbltargetIdSelected =  frmDreamSavingEdit["segSlider"]["selectedItems"][0].lblhiddentarget;

    
}
function DreamActNickNameUniqMB(uniqText) {
    for (i = 0; i < DreamNicknameUniqMB["custAcctRec"].length; i++) {
        if ((DreamNicknameUniqMB.custAcctRec[i].acctNickName) != null && (DreamNicknameUniqMB.custAcctRec[i].acctNickName) != "") {
            //
            //
			var acountNo = frmDreamSavingMB.lblAcntNoValue.text
			 if (uniqText.toLowerCase() == DreamNicknameUniqMB["custAcctRec"][i]["acctNickName"].toLowerCase()) {
	            if(acountNo != DreamNicknameUniqMB["custAcctRec"][i]["accountNoFomatted"] ){
	                return false;
	            }
             }
          
        }
    }
}


function DreamActAccountNameUniqMB(uniqText) {
		

    for (i = 0; i < DreamNicknameUniqMB["custAcctRec"].length; i++) {
		var accountIdNAme = DreamNicknameUniqMB["custAcctRec"][i]["accId"];
                    accountIdNAme = accountIdNAme.substr(accountIdNAme.length - 4)
        if ((DreamNicknameUniqMB.custAcctRec[i].ProductNameEng) != null && (DreamNicknameUniqMB.custAcctRec[i].ProductNameEng) != "") {
            
			var uniqText = uniqText.replace(/ /g, "");
			var AccountNamecheck = DreamNicknameUniqMB["custAcctRec"][i]["ProductNameEng"].replace(/ /g, "");
            if (uniqText.toLowerCase() == AccountNamecheck.toLowerCase().trim()+accountIdNAme) {
                return false;
            }
        }
    }
}

function dreamSavingPopulateDateSpa(){
	if (gbltranFromSelIndex[1]== undefined || gbltranFromSelIndex[1] == "0")  {
	   gbltranFromSelIndex = [0, 0];
   	}
    if (flowSpa) {
       gblnormSelIndex = 0;
    }
	gblnormSelIndex = gbltranFromSelIndex[1];
    gblmbSpaselectedData = frmDreamSavingEdit.segSliderSpa.data[gblnormSelIndex];
	
	gblflagDreamMB = "true";
    frmDreamSavingMB.imgProd.src = gblmbSpaselectedData.img1;
    
  //  frmDreamSavingEdit.lblDreamDescVal.text = frmDreamSavingEdit.segSlider.selectedItems[0]["imgDreamName"];
	 gblProductImage1 = gblmbSpaselectedData.img1
	 gbltargetIdSelected = gblmbSpaselectedData.lblhiddentarget;
	//alert("gbltargetIdSelected"+gbltargetIdSelected)
    
}


function CancelBtnDream() {
    popupConfirmation.dismiss();
}

//function linkOnclick() {
//
//    frmMyAccountList.show();
//}

function validationForDSM() {
var myDreamTargetAMnt="";
var num= "";
var num1="";
var MnthlySAvingAmnt= "";
    var nickNameTxt = frmDreamSavingEdit.lblNicknameVal.text;

     myDreamTargetAMnt = frmDreamSavingEdit.lblTargetAmntVal.text;
    if(myDreamTargetAMnt!=null && myDreamTargetAMnt!="" && myDreamTargetAMnt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")!=-1))
    {
  
	    myDreamTargetAMnt = myDreamTargetAMnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	    myDreamTargetAMnt = myDreamTargetAMnt.replace(/,/g, "");
	      
	       
    }
     num = new Number(myDreamTargetAMnt.trim())
       
    MnthlySAvingAmnt = frmDreamSavingEdit.lblSetSavingVal.text;
    if(MnthlySAvingAmnt!=null && MnthlySAvingAmnt!="" && MnthlySAvingAmnt.indexOf("/")!=-1)
    {
    
     MnthlySAvingAmnt = MnthlySAvingAmnt.replace(/,/g, "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    }
    num1 = new Number(MnthlySAvingAmnt.trim())
    var DreamDesc = frmDreamSavingEdit.lblDreamDescVal.text;
   
    if (nickNameTxt == null || nickNameTxt == "" ) {
      frmDreamSavingEdit.lblNicknameVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreaminputNickname"), kony.i18n.getLocalizedString("info"))
         return false;
    }
    if (DreamDesc == null || DreamDesc == "") {
    frmDreamSavingEdit.lblDreamDescVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreaminputDreamDesc"), kony.i18n.getLocalizedString("info"))
         return false;
    }
    var nickName = DreamActNickNameUniqMB(frmDreamSavingEdit.lblNicknameVal.text);
	var accntName = DreamActAccountNameUniqMB(frmDreamSavingEdit.lblNicknameVal.text);
    if (nickName == false ||accntName==false) {
        showAlert(kony.i18n.getLocalizedString("keyDreamUniqueName"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblNicknameVal.skin = "txtErrorBG";
        return false
    }
  	if ((!(benNameAlpNumValidation(DreamDesc)))) {
  	  frmDreamSavingEdit.lblDreamDescVal.skin = txtErrorBG;
         alert("Dream Description is wrong or Should not  Empty");
        
        return false;
    }
    if ((!(NickNameValid(nickNameTxt)))) {
    frmDreamSavingEdit.lblNicknameVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamaccountnickname"), kony.i18n.getLocalizedString("info"));
        
        return false;
    }
    if (myDreamTargetAMnt == null || myDreamTargetAMnt == "") {
    	frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keyTargetAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (!kony.string.isNumeric(num)) {
    	frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if(frmDreamSavingEdit.btnDreamSavecombo.text == "Trasnfer every month on")
    {
    	//frmDreamSavingEdit.btnDreamSavecombo.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keypleaseSelecttransfer"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    
     var indexdot=myDreamTargetAMnt.indexOf(".");
		var decimal="";
		var remAmt="";
		
		if(indexdot > 0)
		{	decimal=myDreamTargetAMnt.substr(indexdot);
		
			 if (decimal.length > 4){
			 frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
			 alert("Enter only 2 decimal values");
			 return false;
			 }
		}
     if (!kony.string.isNumeric(num1)) {
    	frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false;
    }

    entAmount = MnthlySAvingAmnt;

    entAmount = entAmount.trim();
    var indexdot = entAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    
    if (indexdot > 0) {
        decimal = entAmount.substr(indexdot);
        
        if (decimal.length > 3) {
         frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
            alert("Enter only 2 decimal values");
            return false;
        }
    }
   
    if (MnthlySAvingAmnt == null || MnthlySAvingAmnt == "") {
    	frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
    	showAlert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), kony.i18n.getLocalizedString("info"));
        return false;
    }
   
    if ((kony.os.toNumber(myDreamTargetAMnt)) < (kony.os.toNumber(gblMinDreamAmt))){
		showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
		frmDreamSavingEdit.lblTargetAmntVal.skin = txtErrorBG;
		return false				
	}
	
	
		if ((kony.os.toNumber(MnthlySAvingAmnt)) < (kony.os.toNumber(gblMinDreamAmt))){
		showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
		frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
		return false				
	}
				
	if (gblMaxDreamAmt != "No Limit")
		{
			if ((kony.os.toNumber(MnthlySAvingAmnt)) > (kony.os.toNumber(gblMaxDreamAmt))){
			showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
			 frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
			return false				
			}
				
	}
    if (num1 > num) {
        showAlert(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        frmDreamSavingEdit.lblSetSavingVal.skin = txtErrorBG;
        return false;
    }
    /*	if(trasfrMnthOn == kony.i18n.getLocalizedString("keyMBTransferEvryMnth")){
   		alert("Please Select transfer Every Month On ")
   		return false;
   	}*/
   	if(flowSpa)
   	{
   		
   		
   	//	gblAccountNoDream=null;
		gblprodNameSpa=prodName;
		gblprodCodeSpa=prodCode;
		gblAccountNoDreamSpa=AccountNoDream;
		checkeditdreamBeforeSavingsMB()
   	}else{
   		saveAmountinSessionMB();
   	}	
	
    //	DreamSavingUpdate()
}
function checkeditdreamBeforeSavingsMB() {
	var inputParam = [];
	showLoadingScreen();
	invokeServiceSecureAsync("tokenSwitching", inputParam, checkeditdreamSavingsMBCallback);
}

function checkeditdreamSavingsMBCallback(status,resulttable){
	 if (status == 400) {
		 if(resulttable["opstatus"] == 0){
			 saveAmountinSessionMB();
		 }else{
			 dismissLoadingScreen();
			 alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
			}
		 }
}
/*function callDepositAccntsummmry() {

	gblAccountSummaryFlagMB= "true"
	clearDreamDataMaintMB();

	
    var inputparam = {};
    inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex][ "accId" ];
    gblAccNoAcbtSumry = inputparam["acctId"];
    gblAccNoAcbtSumry12 = gblAccNoAcbtSumry.length
    gblAccNo = gblAccNoAcbtSumry.substr(gblAccNoAcbtSumry12 - 14);
    gblPersonalizedRecordIdFromDreamSavings=gblAccNo;
    inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	  
    
    inputparam["spName"] = "com.fnf.xes.ST"
    inputparam["rqUUId"] = "";
    showLoadingScreen();
    invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAcctInqCallBack);
}*/

 function callDepositAccntsummmry() {

 gblAccountSummaryFlagMB= "true"
 clearDreamDataMaintMB();

 
    var inputparam = {};
    gblAccntType = gblAccountTable["custAcctRec"][gblIndex]["accType"];
    inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex][ "accId" ];
    gblAccNoAcbtSumry = inputparam["acctId"];
    var gblAccNoAcbtSumry12 = gblAccNoAcbtSumry.length
    gblAccNo = gblAccNoAcbtSumry.substr(gblAccNoAcbtSumry12 - 14);
    gblPersonalizedRecordIdFromDreamSavings=gblAccNo;
    inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
 
    
    inputparam["spName"] = "com.fnf.xes.ST"
    inputparam["rqUUId"] = "";
    showLoadingScreen();
    invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAcctInqCallBack);
}

function calldepositAccountInqDMS() {
    var inputparam = {};
    inputparam["acctId"] = gblAccNo;
    inputparam["acctType"] = gblAccntType;
    
    

    inputparam["spName"] = "com.fnf.xes.ST"
    inputparam["rqUUId"] = "";
    showLoadingScreen();
    invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAcctInqCallBack);
}
var availableBalDream;
var targetAmoutDream;
var percentageBar;

function depositAcctInqCallBack(status, resulttable) {
    if (status == 400) {
        //***** to get Avalible Bal ,For calculating the Bar % *********//	
        var result = resulttable;
        if (resulttable["AcctBal"] != null) {
            if (resulttable["AcctBal"].length > 0) {
                for (var i = 0; i < resulttable["AcctBal"].length; i++) {
                    if (resulttable["AcctBal"][i]["BalType"] == "Avail") {
                        availableBalDream = resulttable["AcctBal"][i]["Amt"];
                        avaliableCurrCode = resulttable["AcctBal"][i]["CurCode"]
                    }
                }
            }
        } else {
          //  alert("AcctBal is null");
        }

    //**** For Opening Date Account Fetch ***************//
        if (resulttable["OpeningDate"] != null && resulttable["OpeningDate"] != "") {
            frmDreamSavingMB.lblOpeningDateVal.text =dateformat(resulttable["OpeningDate"]);
        }
        linkedAccType = "";
        if (resulttable["linkedAccType"] != null && resulttable["linkedAccType"] != "") {
            linkedAccType = resulttable["linkedAccType"];
        }
        if (resulttable["AccountID"] != null && resulttable["AccountID"] != "") {
            gblDreamAct = resulttable["AccountID"];
        }
        DreamLinkAccount = "";
        DreamLinkAccount = resulttable["linkedacc"] 
        //**** For  AccountID  Fetch***************//
        if (resulttable["linkedacc"] != null && resulttable["linkedacc"] != "") {
            gblAccountFromfor = resulttable["linkedacc"];
            var AcountlenforRex = gblAccountFromfor.length
            var gblAccountFromforRex = gblAccountFromfor.substr(AcountlenforRex - 10);
			gblAccountFromforRexFor = gblAccountFromfor.substr(AcountlenforRex - 14);
			if (resulttable["linkedAccType"]=="SDA"){
                gblAccountFromIB = formatAccountNo(resulttable["linkedacc"].substring(20))
                 frmDreamSavingMB.lblAccountFrom.text = gblAccountFromIB;
              }else{
             	 gblAccountFromIB = formatAccountNo(resulttable["linkedacc"].substring(16))
                frmDreamSavingMB.lblAccountFrom.text = gblAccountFromIB;	
              }
        }else{
        gblAccountFromIB= "";
        //showAlert(kony.i18n.getLocalizedString("keyLinkAcntNotFound"), kony.i18n.getLocalizedString("info"));
        }
        var inputparam = {};

        if (resulttable["linkedAccType"] != null && resulttable["linkedAccType"] != "" && linkedAccType == "SDA") {
            inputparam["spName"] = "com.fnf.xes.ST"; //14digits
            gblFourteenDigAccount = gblAccountFromfor.substr(AcountlenforRex - 14);
        }
        if (resulttable["linkedAccType"] != null && resulttable["linkedAccType"] != "" && linkedAccType == "DDA") {
            inputparam["spName"] = "com.fnf.xes.IM"; //14digits
            gblFourteenDigAccount = gblAccountFromfor.substr(AcountlenforRex - 10);
        }
        inputparam["acctId"] = gblAccountFromforRex;
        //   
        inputparam["acctType"] = linkedAccType;
        
        inputparam["clientDt"] = "";
        inputparam["name"] = "MB-INQ";
        invokeServiceSecureAsync("RecurringFundTransinq", inputparam, ReqInquiryDreamCallBack);
    }
}

/**
 * Author  : Sweety
 * Date    :Aug 28, 2013
 * Purpose : get MonthlySAving Amount ,Monthly installmentDate & AccountID FRom
 * ServiceCalls: doRecXferInq
 * Called From : frmDreamSAvingMB Preprocessor;
 */


var acountDisplayFrom;

function ReqInquiryDreamCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var result = resulttable;
            if (resulttable["frequency"] != null && resulttable["frequency"] != "" && resulttable["frequency"] =="EndOfMonth") {
            frmDreamSavingMB.lblTranfrEveryMnthVal.text  = "31" + "st";
            mnthlyTransferDate = "31";
          }
 			if (resulttable["monIntDate"] != null && resulttable["monIntDate"] != "") {
                mnthlyTransferDate = resulttable["monIntDate"];
                if (mnthlyTransferDate == 1 || mnthlyTransferDate == 21|| mnthlyTransferDate == 31) {
                    frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlyTransferDate + "st";
                } else if (mnthlyTransferDate == 2 || mnthlyTransferDate == 22) {
                    frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlyTransferDate + "nd";
                } else if (mnthlyTransferDate == 3 || mnthlyTransferDate == 23) {
                    frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlyTransferDate + "rd";
                } else {
                    frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlyTransferDate + "th";
                }
               }
            if (resulttable["monInstAmt"] == null ||resulttable["monInstAmt"] == "") {
           
           } else {
           
                frmDreamSavingMB.lblMnthlySavingAmntVal.text = commaFormatted(resulttable["monInstAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                var MonthlySavingAmnt = resulttable["monInstAmt"]
                
            }
            if (resulttable["recXferId"] != null) {
                recXferId = resulttable["recXferId"];
                
            }
            if (resulttable["dueDate"] != null) {
                dueDate = resulttable["dueDate"];
                
            }

            if (resulttable["frmAccId"] != null) {
                recurringFrmAcctId = resulttable["frmAccId"];
                recurringToAcctId = resulttable["toAcctId"];
                
            }
            recurringFrmAcctId = resulttable["frmAccId"];
            recurringToAcctId = resulttable["toAcctId"];
           // recurringFrmAcctType = resulttable["frmAccType"];
           // recurringToAcctType = resulttable["toAcctType"];
		    recurringFrmAcctType = linkedAccType;
       		recurringToAcctType =gblAccntType;
        }
        var inputparam = {};

        var acountDisplay = gblAccNo;
        var Acountlengbl = acountDisplay.length
        x = acountDisplay.substr(Acountlengbl - 14);
        gblAccNo = x;
		gblPersonalizedRecordIdFromDreamSavings=gblAccNo;
        inputparam["PERSONALIZED_ACCT_ID"] = gblAccNo;
        //	inputparam["PERSONALIZED_ACCT_ID"] = "0632323176";

        invokeServiceSecureAsync("DreamSavingEnquiry", inputparam, DreamInquiry);

    }

}


function DreamInquiry(status, resulttable) {

    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
      var inputparam = {};
           dreamSAvingLength = resulttable["dreamsavingDS"].length;
       var result1 = resulttable;
            if (resulttable["dreamsavingDS"].length > 0) {
             gbldreamtargetID = resulttable["dreamsavingDS"][0]["DREAM_TARGET_ID"];
              ChannelID  = resulttable["dreamsavingDS"][0]["CHANNEL_ID"];
          //    alert("accnt status"+resulttable["dreamsavingDS"][0]["PERSONALIZED_ACCT_STATUS"]);
               if (resulttable["dreamsavingDS"][0]["PERSONALIZED_ACCT_STATUS"] == "02") {
                         gblshowAccountBox=true;
                        frmDreamSavingMB.hbox866795318717754.isVisible = false;
                        frmDreamSavingMB.linkaccount.isVisible = true;

                    } else {
                        frmDreamSavingMB.hbox866795318717754.isVisible = true;
                        frmDreamSavingMB.linkaccount.isVisible = false;
                 }
		
                if (ChannelID  == "01" || ChannelID  == "02") {
				                
						if (resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"] != null && resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"] != "") {
                        taramount = resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"];
                        
                        
                        frmDreamSavingMB.lblTargetAmount.text = commaFormattedOpenAct(taramount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                        targetAmoutDream = resulttable["dreamsavingDS"][0]["DREAM_TARGET_AMOUNT"];
                        
                    }
                     
                    if (resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"] != null && resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"] != "") {
                        frmDreamSavingMB.lblDreamDesc.text = resulttable["dreamsavingDS"][0]["DREAM_DESC"];
                        
                        gblpersonalizedId = resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"];
                      
                    }
                   
                    DreamcalculatePercentage();
					}else{     

                	frmDreamSavingMB.lblTargetAmount.isVisible = false;
                    frmDreamSavingMB.label866795318717734.isVisible = false;
                    frmDreamSavingMB.lblAccountNicknameValue.text = gblDreamNickname;
                    percentageBar = "0";
                    frmDreamSavingMB.btnPercentage.text = percentageBar+"%";
                    frmDreamSavingMB.vboxSkin.skin = "samplevbox00";
                    frmDreamSavingMB.vboxSkinVal.padding= [1,55, 1, 1];
                    frmDreamSavingEdit.lblTargetAmntVal.text = "";
                     
                    if (resulttable["dreamsavingDS"][0]["DREAM_DESC"] != null && resulttable["dreamsavingDS"][0]["DREAM_DESC"] != "") {
                    frmDreamSavingMB.lblDreamDesc.text = resulttable["dreamsavingDS"][0]["DREAM_DESC"];
                 //   DreamDescGbl = resulttable["dreamsavingDS"][0]["DREAM_DESC"];
                     
                    gblpersonalizedId = resulttable["dreamsavingDS"][0]["PERSONALIZED_ID"];
                   
                   }
                }
            }else{
            		if(DreamLinkAccount==null || DreamLinkAccount==""){
					DreamgetInterestRate();
					DreamcalculatePercentage();
				//	alert("yes");
            		}else{
            	//	showAlert(kony.i18n.getLocalizedString("keyLinkAcntNotFound"), kony.i18n.getLocalizedString("info"));
			        DreamgetInterestRate();
					DreamcalculatePercentage();
					}
					}
        
		}
    }
}

       
   
function DreamcalculatePercentageConfirm() {
	
	
    
    var dreamamntConfirm = frmDreamSavingEdit.lblTargetAmntVal.text;
    var targetconfirm = dreamamntConfirm.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var  targetconfirm1 = targetconfirm.replace(".00", "");
    targetconfirm2 = targetconfirm1.replace(/,/g, "");
	
    //  alert(targetconfirm2)
    if (dreamSAvingLength > 0) {
       var percentageBarbeforeRound = (availableBalDream / targetconfirm2) * 100;
        percentageBar = Math.round(percentageBarbeforeRound);
        
        if(isNaN(percentageBar)){
          percentageBar= "0";         
        }
		barGraph();
		
    }else{
    	percentageBar = "0";
    }
}

function DreamcalculatePercentage() {

    
	
	if (DreamLinkAccount==null || DreamLinkAccount==""){
		percentageBar = 0;
		 DreamgetInterestRate();
	}else{
	
    if (dreamSAvingLength > 0) {
        var percentageBarRound = (availableBalDream / targetAmoutDream) * 100;
        percentageBar = Math.round(percentageBarRound);
       
        if(isNaN(percentageBar)){
          percentageBar= "0";         
        }
        
        
        DreamgetInterestRate();
    } else {
      //  frmDreamSavingMB.imgProd.src = "prod_car_inv.png";
     	percentageBar = "0";
        DreamgetInterestRate();
        }
    }
}

function DreamgetInterestRate() {
    var inputparam = {};
    
    inputparam["productCode"] = "206";

    var status = invokeServiceSecureAsync("getInterestRate", inputparam, CustAcntEnqDreamInterestRate);
}

function CustAcntEnqDreamInterestRate(status, callBackResponse) {

    if (status == 400) {
        
        if (callBackResponse["opstatus"] == 0) {
		//	DreamInterestResponse = callBackResponse;
			if(callBackResponse["SavingTargets"]!=null && callBackResponse["SavingTargets"].length != 0 )
			{
	            frmDreamSavingMB.lblInterestRateVal.text = callBackResponse["SavingTargets"][0]["DATA_VALUE"];
	            var InterestRate = frmDreamSavingMB.lblInterestRateVal.text + "%";
	            frmDreamSavingMB.lblInterestRateVal.text = InterestRate;
	            if (kony.i18n.getCurrentLocale() == "en_US") 
	            {
					frmDreamSavingMB.lblInterestRate.text  = callBackResponse["SavingTargets"][0]["CATEGORY_EN"]+":";
				} 
				else 
				{
					frmDreamSavingMB.lblInterestRate.text  = callBackResponse["SavingTargets"][0]["CATEGORY_TH"] + ":";
				}
				DreamInterestRateEN = callBackResponse["SavingTargets"][0]["CATEGORY_EN"];
				DreamInterestRateTH= callBackResponse["SavingTargets"][0]["CATEGORY_TH"];
			}
			else
			{
				frmDreamSavingMB.lblInterestRateVal.text="0.00%";
			}
            
            gblMinDreamAmt = callBackResponse["minLimitOpenAct"];
            gblMaxDreamAmt = callBackResponse["maxLimitOpenAct"];
            //    
            
            DreamTargetID();
            // dreamCustomerEnquiry();
        }

    }
}

function DreamTargetID() {
    var inputparam = {};

    if (dreamSAvingLength > 0) {
        inputparam["dreamTargetID"] = gbldreamtargetID;
    }
    invokeServiceSecureAsync("getDreamDesc", inputparam, getTargetIDDream);
}

function getTargetIDDream(status, resulttable) {

    if (status == 400) {
		ResulTnotFoundMB = resulttable["dreamsavingDS"]
        
        if (resulttable["opstatus"] == 0) {
		if(ResulTnotFoundMB==null|| ResulTnotFoundMB==""){
			dreamCustomerEnquiry();
         } else{ 
          
            if(resulttable["dreamsavingDS"]!=null ||resulttable["dreamsavingDS"]!=""){

            
            if (dreamSAvingLength > 0) {
                gblProductImage = resulttable["dreamsavingDS"][0]["productimages"];
                targetIDDream = resulttable["dreamsavingDS"][0]["DREAM_TARGET_ID"];
                
			//	alert(gblProductImage);
                if (gbldreamtargetID == targetIDDream) {
                    frmDreamSavingMB.imgProd.src = resulttable["dreamsavingDS"][0]["productimages"] + "";
                    
                    
				}
                }
            }
            
            dreamCustomerEnquiry();
            }
        }
    
	}
}

function DreamTargetImageMB() {
    var inputparam = {};
 
    inputparam["dreamTargetID"] = gbldreamtargetID;
    invokeServiceSecureAsync("getDreamDesc", inputparam, getTargetIDDreamIBimageMB);
}
 
function getTargetIDDreamIBimageMB(status, resulttable) {
 
    if (status == 400) {
 
        
        if (resulttable["opstatus"] == 0) {
 
 
            
            gblProductImageinv = resulttable["dreamsavingDS"][0]["productimages"];
            targetIDDream = resulttable["dreamsavingDS"][0]["DREAM_TARGET_ID"];           
             
 
            if (gbldreamtargetID == targetIDDream) {
                frmDreamSavingMB.imgProd.src = gblProductImageinv;
                
            }
 
            
           
        }
       // DreamSavingUpdate();
    }
    
}
 


function dreamCustomerEnquiry() {

    var inputparam = {};
    inputparam["crmId"] = "";
    //inputparam["crmId"] = ""
   // inputparam["rqUUId"] = "";
   // inputparam["bankCd"] = gblTMBBankCD;
    inputparam["activationCompleteFlag"] = "true";
    var status = invokeServiceSecureAsync("customerAccountInquiry", inputparam, DreamCallBackCustomerEnquiry);

}

function DreamCallBackCustomerEnquiry(status, resulttable) {

var editDreamMB = "false";
    var iconDream = "";
    var iconDreamFrom = "";
    var accTypeDream= "";
    if (status == 400) {
        showLoadingScreen();
         DreamNicknameUniqMB = resulttable;
         
         			if(linkedAccType=="SDA")
                	{
                	  accTypeDream=kony.i18n.getLocalizedString("keySavingMB");
                	}else if (linkedAccType == "CDA"){
                		accTypeDream=kony.i18n.getLocalizedString("keyTermDepositMB");
                	}else if (linkedAccType == "DDA"){
                		accTypeDream=kony.i18n.getLocalizedString("keyCurrentMB");
                	}else if (linkedAccType == "LOC"){
                		accTypeDream=kony.i18n.getLocalizedString("keylblLoan");
                	}
           
				frmDreamSavingMB.lblAccType.text  = accTypeDream;
			
        
        
        if (resulttable["opstatus"] == 0) {
        	 
            for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                
                if (resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("Saving")) {
                    if (resulttable.custAcctRec[i].productID == "206") {
                        iconDream = "prod_savingd.png"
                        frmDreamSavingMB.imgDepositAccnt.src = iconDream;
                    }
                }
                
                
                //  if (resulttable["custAcctRec"][i]["accId"] == "00001749219018") { //gblAccountFrom{
				if(editDreamMB=="false"){
                if (resulttable["custAcctRec"][i]["accId"] == gblFourteenDigAccount) {
               	FromProductIdDreamMB =resulttable["custAcctRec"][i]["productID"];  
                //    accountStatusFlag = "false";
                    
                    
                    if (resulttable["custAcctRec"][i]["acctStatusCode"] == "deleted") {
                        frmDreamSavingMB.btnEditDepositAccnt.setEnabled(false);
                    } else {
                        frmDreamSavingMB.btnEditDepositAccnt.setEnabled(true);
                    }
                   
                    
                    if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFIXED" || resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_SAVINGCARE")
                        iconDreamFrom = "prod_savingd.png"
                    else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CREDITCARD_TABLE")
                        iconDreamFrom = "prod_creditcard.png"

                    else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE")

                        iconDreamFrom = "prod_termdeposits.png"
                    else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE")
                        iconDreamFrom = "prod_nofee.png"

                    else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_HOMELOAN")
                        iconDreamFrom = "prod_homeloan.png"
                    else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_OLDREADYCASH_TABLE" || resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE" || resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_CASH2GO")
                        iconDreamFrom = "prod_cash2go.png"
                    else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE" || resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE")
                        iconDreamFrom = "prod_currentac.png"
                    else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_READYCASH_TABLE")
                        iconDreamFrom = "prod_cash2go.png"
                        
                        var accountId = resulttable["custAcctRec"][i]["accId"];
					accountId = accountId.substr(accountId.length - 4)
					DreamCustomerNickname = resulttable.custAcctRec[i]["acctNickName"];
					DreamProdNickname = resulttable["custAcctRec"][i]["ProductNameEng"] + accountId
                	if(resulttable.custAcctRec[i]["acctNickName"]!=null)
                	{
                		frmDreamSavingMB.lblfromNickname.text = resulttable.custAcctRec[i]["acctNickName"];
						
                	}
                	else
                	{
						
                		
                		 DreamProductnameENGIB = resulttable["custAcctRec"][i]["ProductNameEng"] + accountId;
                            DreamProductnameTHIB = resulttable["custAcctRec"][i]["ProductNameThai"] + accountId;
                            if (kony.i18n.getCurrentLocale() == "en_US") {

                                frmDreamSavingMB.lblfromNickname.text = DreamProductnameENGIB;

                            } else {

                                frmDreamSavingMB.lblfromNickname.text = DreamProductnameTHIB;

                            }
                		
                	}
						frmDreamSavingMB.lblnotAvaliable.isVisible=false;
						if(gblshowAccountBox==true){
                    	 frmDreamSavingMB.hbox866795318717754.isVisible = false;
                    }else{
                   		 frmDreamSavingMB.hbox866795318717754.isVisible = true;
                    }
						
						  frmDreamSavingMB.btnEditDepositAccnt.skin = "btnEdic"
						  frmDreamSavingMB.btnEditDepositAccnt.setEnabled(true);
						   frmDreamSavingMB.imgfrom.src = iconDreamFrom;
						  editDreamMB = "true";
                }else{
                
                	frmDreamSavingMB.btnEditDepositAccnt.setEnabled(false);
                	 frmDreamSavingMB.btnEditDepositAccnt.skin = "editDisable"
                	 frmDreamSavingMB.lblnotAvaliable.isVisible=true;
                	 frmDreamSavingMB.hbox866795318717754.isVisible=false;
                	 frmDreamSavingMB.lblnotAvaliable.text=kony.i18n.getLocalizedString("keyAccntNotAvaliable");
                }
                }
               
               // var acctIdPersonalixed = gblAccNo;
                if (resulttable["custAcctRec"][i]["productID"] == "206" && resulttable["custAcctRec"][i]["persionlizeAcctId"] == gblAccNo.trim() && resulttable["custAcctRec"][i]["persionlizedId"] == gblpersonalizedId.trim()) {
                    prodCode = resulttable["custAcctRec"][i]["productID"];
              
                    if(flowSpa)
                    {
                    	gblprodCodeSpa = resulttable["custAcctRec"][i]["productID"];
                    }

                    frmDreamSavingMB.lblAcntNoValue.text = resulttable["custAcctRec"][i]["accId"];
                    var acountDisplay = frmDreamSavingMB.lblAcntNoValue.text;
                    var acountDisplay1=acountDisplay.replace(/-/g, "")
                    var Acountlen = acountDisplay1.length
                    AccountNoDream = acountDisplay1.substring(Acountlen - 10);
                    prodName = acountDisplay.substr(Acountlen - 4);
                    if(flowSpa)
                    {
                    	gblprodNameSpa = acountDisplay.substr(Acountlen - 4);;
                    }
                    frmDreamSavingMB.lblAcntNoValue.text =formatAccountNo( AccountNoDream);
                    
                    ToAccountType = resulttable["custAcctRec"][i]["accType"];
                    var branchName;
                    if (resulttable["custAcctRec"][i]["BranchNameEh"] != null && resulttable["custAcctRec"][i]["BranchNameEh"]!= "") {
                        branchName =resulttable["custAcctRec"][i]["BranchNameEh"];
                        frmDreamSavingMB.lblBranchVal.text = branchName;
                    }

                    if (resulttable["custAcctRec"][i]["acctNickName"] == null && resulttable["custAcctRec"][i]["acctNickName"] == "") {
                        gblDreamNickname = resulttable["custAcctRec"][i]["ProductNameEng"] + prodName;
                   //NicknameDream = resulttable["custAcctRec"][i]["ProductNameEng"];

                        frmDreamSavingMB.lblAccountNicknameValue.text = gblDreamNickname;
                        
                    } else if (resulttable["custAcctRec"][i]["acctNickName"] != null && resulttable["custAcctRec"][i]["acctNickName"] != "") {
                        frmDreamSavingMB.lblAccountNicknameValue.text = resulttable["custAcctRec"][i]["acctNickName"];
                 // NicknameDream = resulttable["custAcctRec"][i]["acctNickName"]
                        
                    } else {

                        frmDreamSavingMB.lblAccountNicknameValue.text = resulttable["custAcctRec"][i]["ProductNameEng"] + prodName;
                        
                    }
          //          gblFromAccntType = resulttable["custAcctRec"][i]["accType"];
                }
              else if (resulttable["custAcctRec"][i]["productID"] == "206" && resulttable["custAcctRec"][i]["accId"] == gblAccNo.trim() ) {
                 
                    prodCode = resulttable["custAcctRec"][i]["productID"];
                    openFrmBranchMB();
                    if(flowSpa)
                    {
                    	gblprodCodeSpa = resulttable["custAcctRec"][i]["productID"];
                    }

                    frmDreamSavingMB.lblAcntNoValue.text = resulttable["custAcctRec"][i]["accId"];
                    var acountDisplay = frmDreamSavingMB.lblAcntNoValue.text;
                    var acountDisplay1=acountDisplay.replace(/-/g, "")
                    var Acountlen = acountDisplay1.length
                    AccountNoDream = acountDisplay1.substring(Acountlen - 10);
                    prodName = acountDisplay.substr(Acountlen - 4);
                    if(flowSpa)
                    {
                    	gblprodNameSpa = acountDisplay.substr(Acountlen - 4);;
                    }
                    frmDreamSavingMB.lblAcntNoValue.text =formatAccountNo( AccountNoDream);
                    
                    ToAccountType = resulttable["custAcctRec"][i]["accType"];
                    var branchName;
                    if (resulttable["custAcctRec"][i]["BranchNameEh"] != null && resulttable["custAcctRec"][i]["BranchNameEh"]!= "") {
                        branchName =resulttable["custAcctRec"][i]["BranchNameEh"];
                        frmDreamSavingMB.lblBranchVal.text = branchName;
                    }

                    if (resulttable["custAcctRec"][i]["acctNickName"] == null && resulttable["custAcctRec"][i]["acctNickName"] == "") {
                        gblDreamNickname = resulttable["custAcctRec"][i]["ProductNameEng"] + prodName;
                   //NicknameDream = resulttable["custAcctRec"][i]["ProductNameEng"];

                        frmDreamSavingMB.lblAccountNicknameValue.text = gblDreamNickname;
                        
                    } else if (resulttable["custAcctRec"][i]["acctNickName"] != null && resulttable["custAcctRec"][i]["acctNickName"] != "") {
                        frmDreamSavingMB.lblAccountNicknameValue.text = resulttable["custAcctRec"][i]["acctNickName"];
                 // NicknameDream = resulttable["custAcctRec"][i]["acctNickName"]
                        
                    } else {

                        frmDreamSavingMB.lblAccountNicknameValue.text = resulttable["custAcctRec"][i]["ProductNameEng"] + prodName;
                        
                    }
          //          gblFromAccntType = resulttable["custAcctRec"][i]["accType"];
                }
                 
                dismissLoadingScreen();
            }
           // 
          	 if(ResulTnotFoundMB==null ||ResulTnotFoundMB==""){
             
             }else{
          	
          	if (ChannelID == "01" || ChannelID == "02")
			{
            	barGraph();
            	//alert("bargraph")
            }
            }
			     frmDreamSavingMB.show();
		
        }
    }

}
function clearTargetAmnt(){
	
    frmDreamSavingEdit.lblNicknameVal.text = frmDreamSavingMB.lblAccountNicknameValue.text;
    frmDreamSavingEdit.lblDreamDescVal.text = frmDreamSavingMB.lblDreamDesc.text;
 //  frmDreamSavingEdit.lblTargetAmntVal.text =  frmDreamSavingMB.lblTargetAmount.text;
	if(frmDreamSavingMB.lblTranfrEveryMnthVal.text ==""||frmDreamSavingMB.lblTranfrEveryMnthVal.text ==null){
	 frmDreamSavingEdit.btnDreamSavecombo.text = "Trasnfer every month on"
	 }else{
	 var mnthlyoldval = frmDreamSavingMB.lblTranfrEveryMnthVal.text;

	
                if (mnthlyoldval == "1st" || mnthlyoldval == "21st"|| mnthlyoldval == "31st") {
                     frmDreamSavingEdit.btnDreamSavecombo.text = mnthlyoldval.replace("st", "");
                } else if (mnthlyoldval == "2nd" || mnthlyoldval == "22nd") {
                  frmDreamSavingEdit.btnDreamSavecombo.text = mnthlyoldval.replace("nd", "");
                } else if (mnthlyoldval == "3rd" || mnthlyoldval == "23rd") {
                      frmDreamSavingEdit.btnDreamSavecombo.text = mnthlyoldval.replace("rd", "");
                } else {
                     frmDreamSavingEdit.btnDreamSavecombo.text = mnthlyoldval.replace("th", "");
                }
               
	 
    
    }
    frmDreamSavingEdit.lblSetSavingVal.text = frmDreamSavingMB.lblMnthlySavingAmntVal.text+"/"+ kony.i18n.getLocalizedString("keyCalendarMonth");
		if (ChannelID == "01" || ChannelID == "02"){
		frmDreamSavingEdit.lblTargetAmntVal.text = frmDreamSavingMB.lblTargetAmount.text;
	
	}else{ 
			frmDreamSavingEdit.lblTargetAmntVal.text = "";
	}
}
function checkUserStatusDSM() {
	// For fix of DEF229
	showLoadingScreen();
    var inputParam = {
        rqUUId: "",
        channelName: "MB-INQ",
        editDreamFlag: "true"
    }
   
   // callBackUSerStatusDSM();
   invokeServiceSecureAsync("crmProfileInq", inputParam, callBackUSerStatusDSM)
}
//function callBackUSerStatusDSM() {
function callBackUSerStatusDSM(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            
            var MBUserStatus = callBackResponse["mbFlowStatusIdRs"];
            
            
            if (MBUserStatus == "03") {
                gblUserLockStatusMB = "03";
                dismissLoadingScreen();
                // frmDreamSavingEdit.show();
                alertUserStatusLocked();
            } else {
                gblUserLockStatusMB = MBUserStatus;
                //  alert("success")
                //dismissLoadingScreen();
                
                
                			if(callBackResponse["editDreamBusinessHrsFlag"] == "true") {
								 DreamcheckOpenActBusHrs();
							}else{
								dismissLoadingScreen();
								if(GLOBAL_EDITDREAM_STARTTIME == null ||  GLOBAL_EDITDREAM_STARTTIME == ""){
									GLOBAL_EDITDREAM_STARTTIME = "03:00";
								}
								if(GLOBAL_EDITDREAM_ENDTIME == null ||  GLOBAL_EDITDREAM_ENDTIME == ""){
									GLOBAL_EDITDREAM_ENDTIME = "22:55";
								}
								showAlert(kony.i18n.getLocalizedString("keyDreamcheckhrs") , kony.i18n.getLocalizedString("info"));
								return false;
							}
                			
                			
                
                 // DreamcheckOpenActBusHrs();
               // DreamonClickAgreeOpenDSActs()
            }
            
        } else {
            	gblUserLockStatusMB = "03";
		 		dismissLoadingScreen();
		 	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
    
}


//function OnClickConfirmUpdate(status, resulttable) {
//    if (status == 400) {
//        
//
//        if (resulttable["opstatus"] == 0) {
//            
//
//            var StatusCode = resulttable["status"][0]["statusCode"];
//            var Severity = resulttable["status"][0]["severity"];
//            var StatusDesc = resulttable["status"][0]["statusDesc"];
//
//            
//            
//            
//
//            if (StatusCode == "0") {
//                productCodename = prodCode + prodName;
//             //   activityLogServiceCall("073", "", "01", "", productCodename, frmDreamSavingEdit.lblTargetAmntVal.text, AccountNoDream, frmDreamSavingEdit.lblSetSavingVal.text, frmDreamSavingEdit.lblDreamDescVal.text, "")
//                
//                if(gblflagDreamMB =="true"){
//                	
//            		
//                	gbldreamtargetID = gbltargetIdSelected;
//                	DreamTargetImageMB()
//                }
//                
//                DreamSavingUpdate();
//
//                // frmDreamSavingMB.show();
//            } else {
//                alert(" " + StatusDesc);
//                productCodename = prodCode + prodName;
//            //    activityLogServiceCall("073", "", "02", "", productCodename, frmDreamSavingEdit.lblTargetAmntVal.text, AccountNoDream, frmDreamSavingEdit.lblSetSavingVal.text, frmDreamSavingEdit.lblDreamDescVal.text, "")
//                // DreamSavingUpdate();
//                
//
//
//            }
//        }
//    }
//}

/*function DreamSavingUpdate() {

    var inputparam = {};
    //inputparam["CRM_ID"] = "";
    inputparam["PERSONALIZED_ACCT_ID"] = gblAccNo;
    
    inputparam["PERSONALIZED_ID"] = gblpersonalizedId;
    
    var removebhat = frmDreamSavingEdit.lblTargetAmntVal.text;
    removebhat1 = removebhat.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    inputparam["DREAM_TARGET_AMOUNT"] = removebhat1;
    
    inputparam["PERSONALIZED_ACCT_NICKNAME"] = frmDreamSavingEdit.lblNicknameVal.text;
    
      inputparam["DREAM_TARGET_ID"]= gbldreamtargetID;
            // added by considering DEF8301 
      inputparam["DREAM_DESC"]= frmDreamSavingEdit.lblDreamDescVal.text;
      //alert("dream Description: "+inputparam["DREAM_DESC"]);
      
    invokeServiceSecureAsync("updateEditDreamSaveInfo", inputparam, OncalBackDreamSavingUpdate);


}*/

/*function OncalBackDreamSavingUpdate(status, resulttable) {

    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            
            //  	
            // editDreamsavingNotification();
            DreamUpdateDataMB();
        }
    }
}*/
//*********************Delete Account ******************************//


//*************************************************************//
//*********************Delete  dreamsaving Account ******************************//

function ConfirmBtnDreamSaving() {
    popupConfirmation.dismiss();
    myAccountDreamDelete();
}
//*************************************************************//
function DreamcheckOpenActBusHrs() {

    var inputParam = {};
    invokeServiceSecureAsync("editDreamActBusinessHours", inputParam, DreamcallBackCheckBusHrs);

}

function DreamcallBackCheckBusHrs(status, callBackResponse) {
    if (status == 400) {
        
       if (callBackResponse["opstatus"] == 0)
			{
				
				if(callBackResponse["editDreamBusinessHrsFlag"] == "true") {
				 DreamonClickAgreeOpenDSActs();
				}else{
				dismissLoadingScreen();
				if(GLOBAL_EDITDREAM_STARTTIME == null ||  GLOBAL_EDITDREAM_STARTTIME == ""){
				GLOBAL_EDITDREAM_STARTTIME = "03:00";
				}
				if(GLOBAL_EDITDREAM_ENDTIME == null ||  GLOBAL_EDITDREAM_ENDTIME == ""){
				GLOBAL_EDITDREAM_ENDTIME = "22:55";
				}
				showAlert(kony.i18n.getLocalizedString("keyDreamcheckhrs") , kony.i18n.getLocalizedString("info"));
				return false;
				}
			}
			else{
				dismissLoadingScreen();	
				showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
				return false;
			}				 
	}
}

// ************* End Of service hours check ************************//

function DreamonClickAgreeOpenDSActs() {

    showLoadingScreen();
    var inputparam = {};
    invokeServiceSecureAsync("getDreamSavingTargets", inputparam, DreamcallBackOpenDreamSavingTargets);
}



function DreamcallBackOpenDreamSavingTargets(status, resulttable) {

    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            //frmDreamSavingMB.imgDepositAccnt.src = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
            //frmOpnActSelAct.lblNSProdName.text = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].lblOpnActSelProd;
            var targetSegIndex = 1;
            var temp_seg = [];
            if(flowSpa)
            {
            	gblspaSelIndex=0;
            	gblcarouselwidgetflow="Savings";
				gblmbSpatransflow=false;
				gblnormSelIndex = gbltranFromSelIndex[1];
				
	            frmDreamSavingEdit.segSliderSpa.widgetDataMap = {
	                img1: "img1",
                   	lblCustName: "lblCustName"	
	            }
            }
            else
            {            
	        //    frmDreamSavingEdit.segSlider.removeAll();
	            frmDreamSavingEdit.segSlider.containerWeight = 100;
	            frmDreamSavingEdit.segSlider.widgetDataMap = {
	                imgDreamPic: "imgDreamPic",
	                imgDreamName: "imgDreamName"
	
	            }
            }
            
            
            for (j = 0; j < resulttable["SavingTargets"].length; j++) {

                var dreamName;
                var dreamimage;
                var TargetHid;
                var locale = kony.i18n.getCurrentLocale();

                if (locale == "en_US") {
                    dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"];
                } else {
                    dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_TH"];
                }
                dreamimage = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_IMG"];
                TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
               /* if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Vacation") {
                    dreamimage = "prod_lrg_vacation.png";
                     //  frmDreamSavingEdit.lbldreamValue.text= "My Vacation"
           				TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Dream") {
                    dreamimage = "prod_lrg_dream.png";
                //    frmDreamSavingEdit.lbldreamValue.text= "My Dream"
           				TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Car") {
                    dreamimage = "prod_lrg_car.png";
                 //   frmDreamSavingEdit.lbldreamValue.text= "My Car"
           				TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Education") {
                    dreamimage = "prod_lrg_education.png";
                  //  frmDreamSavingEdit.lbldreamValue.text= "My Education"
           				TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Own House") {
                    dreamimage = "prod_lrg_homeloan.png";
                  //  frmDreamSavingEdit.lbldreamValue.text="My Own House"
           				TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Business") {
                    dreamimage = "prod_lrg_business.png";
                  //  frmDreamSavingEdit.lbldreamValue.text= "My Business"
           				TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Saving") {
                    dreamimage = "prod_lrg_saving.png";
                 //   frmDreamSavingEdit.lbldreamValue.text=  "My Saving"
           				TargetHid=resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                }*/
                if(TargetHid == gbldreamtargetID){
                	targetSegIndex = j;
                }
                
                
                if(flowSpa)
                {
                	temp_seg.push({
	                    "lblCustName": dreamName,
						"img1": dreamimage,
	                    "lblhiddentarget":TargetHid,
	                    template: hbxSliderDream
	                })
                }
                else
                {
	                temp_seg.push({
	                    "imgDreamName": dreamName,
	                    "imgDreamPic": dreamimage,
	                    "lblhiddentarget":TargetHid,
	                    template: hbxSliderDream
	                })
	            }

                




                /*if (dreamTargetListResultTable["dreamsavingDS"][0]["DREAM_DESC"]==resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"]){
            	          		frmDreamSavingMB.imgProd.src =dreamimage;
            		}*/
            }
			if(flowSpa)
			{
				frmDreamSavingEdit.segSliderSpa.data=temp_seg;
			}
			else
			{
	            frmDreamSavingEdit.segSlider.setData(temp_seg);
					
				//alert(gbldreamtargetID);
	            
	            frmDreamSavingEdit.segSlider.selectedIndex = [0,targetSegIndex];
	            dsSelActLength = frmDreamSavingEdit.segSlider.data.length - 1;
            }
         if (GLOBAL_DREAM_DESC_MAXLENGTH != null || GLOBAL_DREAM_DESC_MAXLENGTH != ""){
		frmDreamSavingEdit.lblDreamDescVal.maxTextLength = kony.os.toNumber(GLOBAL_DREAM_DESC_MAXLENGTH);
		}else{
		frmDreamSavingEdit.lblDreamDescVal.maxTextLength = 30;
		}
		if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != ""){
		frmDreamSavingEdit.lblNicknameVal.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
		}else{
		frmDreamSavingEdit.lblNicknameVal.maxTextLength = 20;
}
            dismissLoadingScreen();
            MnthlySAvingAmnt = frmDreamSavingEdit.lblSetSavingVal.text
   
    		MnthlySAvingAmnt = MnthlySAvingAmnt.replace(/,/g, "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
 
  			//numericMnthAmnt = MnthlySAvingAmnt;
  			
            frmDreamSavingEditPreshow();
            frmDreamSavingEdit.show();
        }
    }
}

function barGraph() {
    //   var percentage = frmDreamSavingMB.textpercentage.text;
    var percentage = percentageBar; //"50"//frmDreamSavingMB.textbox2866825109104393.text;


    var gblPlatformName = gblDeviceInfo
        .name;
    if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("android", gblPlatformName))) {
        var paddingZero = [1, 21, 1, 1];
        var paddingTen = [1, 19, 1, 1];
        var paddingTwenty = [1, 18, 1, 1];
        var paddingThirty = [1, 15, 1, 1];
        var paddingForty = [1, 13, 1, 1];
        var paddingFifty = [1, 6, 1, 1];
        var paddingSixty = [1,7, 1, 1];
        var paddingSeventy = [1, 5, 1, 1];
        var paddingEighty = [1, 2, 1, 1];
        var paddingNinty = [1, 0, 1, 1];
        var paddingHumdred = [1, 0, 1, 1];

    }
    if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName))) {

        var paddingZero = [1, 21, 1, 1];
        var paddingTen = [1, 20, 1, 1];
        var paddingTwenty = [1, 18, 1, 1];
        var paddingThirty = [1, 15, 1, 1];
        var paddingForty = [1, 12, 1, 1];
        var paddingFifty = [1, 11, 1, 1];
        var paddingSixty = [1, 7, 1, 1];
        var paddingSeventy = [1, 5, 1, 1];
        var paddingEighty = [1, 2, 1, 1];
        var paddingNinty = [1, 1, 1, 1];
        var paddingHumdred = [1, 0, 1, 1];
    }


    //var percentage = "40%";
    //gblProductImage = "My Education";
    //  frmDreamSavingMB.imgProd.src= "prod_car_inv.png";
    //var dreamPicture = "";
    /*	if(gblProductImage!=null && gblProductImage!="")
	{
		dreamPicture = gblProductImage;
	}*/
    if (percentage == 0) {

        frmDreamSavingMB.vboxSkin.skin = "samplevbox00";
        frmDreamSavingMB.vboxSkinVal.padding = paddingZero;
    } else if (percentage >= 1 && percentage <= 15) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox10";
        frmDreamSavingMB.vboxSkinVal.padding = paddingTen;

    } else if (percentage >= 16 && percentage <= 25) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox20";
        frmDreamSavingMB.vboxSkinVal.padding = paddingTwenty;
    } else if (percentage >= 26 && percentage <= 35) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox30";
        frmDreamSavingMB.vboxSkinVal.padding = paddingThirty;

    } else if (percentage >= 36 && percentage <= 45) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox40";
        frmDreamSavingMB.vboxSkinVal.padding = paddingForty;
    } else if (percentage >= 46 && percentage <= 55) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox50";
        frmDreamSavingMB.vboxSkinVal.padding = paddingFifty;
    } else if (percentage >= 56 && percentage <= 65) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox60";
        frmDreamSavingMB.vboxSkinVal.padding = paddingSixty;
    } else if (percentage >= 66 && percentage <= 75) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox70";
        frmDreamSavingMB.vboxSkinVal.padding = paddingSeventy;
    } else if (percentage >= 76 && percentage <= 85) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox80";
        frmDreamSavingMB.vboxSkinVal.padding = paddingEighty;
    } else if (percentage >= 86 && percentage <= 95) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox90";
        frmDreamSavingMB.vboxSkinVal.padding = paddingNinty;
    } else if (percentage >= 96 && percentage <= 100) {
        frmDreamSavingMB.vboxSkin.skin = "samplevbox100";
        frmDreamSavingMB.vboxSkinVal.padding = paddingHumdred;
    } else {
        frmDreamSavingMB.vboxSkin.skin = "skn100";
        frmDreamSavingMB.vboxSkinVal.padding = [1, 1, 1, 1];
    }
    // frmDreamSavingMB.btnPercentage.text = percentage
    
    
    frmDreamSavingMB.btnPercentage.text = percentage + "%";
    //	frmDreamSavingMB.btnPercentage.text=20+"%";
}

/*function sendDreamEmailNotification(status, resulttable) {
    NicknameDreamNotification = frmDreamSavingEdit.lblNicknameVal.text;
    DreamDescGblNotification = frmDreamSavingEdit.lblDreamDescVal.text;
    var targetAmnt = frmDreamSavingEdit.lblTargetAmntVal.text.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
    targetAmoutDreamNotification = targetAmnt;
    var mnthamount = frmDreamSavingEdit.lblSetSavingVal.text.replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "")
    MonthlySavingAmntNotification = mnthamount;
    mnthlyTransferDateNotification = frmDreamSavingEdit.btnDreamSavecombo.text;


    if (status == 400) {
         if (resulttable["opstatus"] == 0) {
             var inputParam = {}
             var deliveryMethod = "Email";
             inputParam["notificationType"] = deliveryMethod;
             var currentLocale = getCurrentLocale();
             inputParam["Locale"] = currentLocale;
             inputParam["source"] = "editDreamSaving"
            // inputParam["emailId"] = gblEmailId;//"sweety.bhagat@kony.com"//gblEmailId;
             gblCustomerName = resulttable["customerName"];
             inputParam["custName"] = gblCustomerName;
             //inputParam["channelName"] = "MB-INQ";
             inputParam["accountNo"] = AccountNoDream;
             inputParam["actno"] =AccountNoDream;
             inputParam["nickname"] = NicknameDreamNotification;
             inputParam["DREAM_DESC"] = DreamDescGblNotification;
             inputParam["targetAmt"] = targetAmoutDreamNotification;
             inputParam["monthlySavingAmt"] = MonthlySavingAmntNotification;
              inputParam["monthlyTransferDate"] = mnthlyTransferDateNotification;
             invokeServiceSecureAsync("NotificationAdd", inputParam, callBackNotificationEditDream)
        }
    }
}*/

/*function callBackNotificationEditDream(status, resulttable) {
    
    if (status == 400) {
        
        
        if (resulttable["opstatus"] == 0) {
            
            dismissLoadingScreen();
            popupTractPwd.dismiss();
        }
    }
}*/


/*
 * Name : getCurrentLocale
 * Author : Kony
 * Purpose : Method for getting current locale .
 */
function getCurrentLocale() {
    var currentLocale = "";
    try {
        currentLocale = kony.i18n.getCurrentLocale();
        if (kony.string.startsWith(currentLocale, "th", true)) {
            currentLocale = "th_TH";
        } else
            currentLocale = "en_US";
    } catch (e) {
        currentLocale = "en_US";
    }
    return currentLocale;
}


//composite service start


function DreamUpdateDataMB() {

    frmDreamSavingMB.lblDreamDesc.text = frmDreamSavingEdit.lblDreamDescVal.text;
    var targetAmountUpdate = frmDreamSavingEdit.lblTargetAmntVal.text;
    if (targetAmountUpdate.indexOf("?", 0) != -1 || targetAmountUpdate.indexOf(",", 0) != -1 || targetAmountUpdate.indexOf(".", 0) != -1 || targetAmountUpdate == null || targetAmountUpdate == "")
        frmDreamSavingMB.lblTargetAmount.text = targetAmountUpdate;
    else
        frmDreamSavingMB.lblTargetAmount.text = commaFormattedOpenAct(targetAmountUpdate) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        
         mnthlynewval = frmDreamSavingEdit.btnDreamSavecombo.text;

	
                
                if (mnthlynewval == 1 || mnthlynewval == 21 || mnthlynewval == 31){
                   frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlynewval + "st";
                } else if (mnthlynewval == 2 || mnthlynewval == 22) {
                 frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlynewval + "nd";
                } else if (mnthlynewval == 3 || mnthlynewval == 23) {
                   frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlynewval + "rd";
                } else {
                   frmDreamSavingMB.lblTranfrEveryMnthVal.text = mnthlynewval + "th";
                }

        
   
    frmDreamSavingMB.lblAccountNicknameValue.text = frmDreamSavingEdit.lblNicknameVal.text;
    //removeSymbol = frmDreamSavingEdit.lblSetSavingVal.text;
    
    frmDreamSavingMB.lblMnthlySavingAmntVal.text = commaFormattedOpenAct(MonthlySavingAmntUpdate) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");;
    DreamcalculatePercentageConfirm();
    //editDreamsavingNotification();
    frmDreamSavingMB.show();
    

}

function editDreamSavingCompositeServiceMB() {
    //
    curr_form = kony.application.getCurrentForm();
    var editDreamSavingCompositeService = {};
    //showLoadingScreenPopup();
    showLoadingScreen();
    if(flowSpa)
	{
		 if(gblTokenSwitchFlag == true ){
		 editDreamSavingCompositeService["verifyToken_loginModuleId"] = "IB_HWTKN";
		 editDreamSavingCompositeService["verifyToken_userStoreId"] = "DefaultStore";
		 editDreamSavingCompositeService["verifyToken_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
		 editDreamSavingCompositeService["verifyToken_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
		 editDreamSavingCompositeService["verifyToken_userId"] = gblUserName;
		 editDreamSavingCompositeService["verifyToken_password"] = popOtpSpa.txttokenspa.text;
		 editDreamSavingCompositeService["verifyToken_sessionVal"] = "";
		 editDreamSavingCompositeService["verifyToken_segmentId"] = "segmentId";
		 editDreamSavingCompositeService["verifyToken_segmentIdVal"] = "MIB";
		 editDreamSavingCompositeService["TokenSwitchFlag"] = true;
		  }else{
		  
			 editDreamSavingCompositeService["verifyPwd_retryCounterVerifyOTP"] = gblVerifyOTP;
				editDreamSavingCompositeService["verifyPwd_segmentId"] = "MIB"
				editDreamSavingCompositeService["verifyPwd_loginModuleId"] = "IBSMSOTP"
				editDreamSavingCompositeService["verifyPwd_userStoreId"] = "DefaultStore";
				editDreamSavingCompositeService["verifyPwd_password"] = popOtpSpa.txtOTP.text;
				editDreamSavingCompositeService["verifyPwd_userId"] = gblUserName;
		 }
	}
	else
	{
	if (popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text==""){
	   setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"))
	   return false;
 	 }
	editDreamSavingCompositeService["verifyPwd_loginModuleId"] = "MB_TxPwd";
	editDreamSavingCompositeService["verifyPwd_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    editDreamSavingCompositeService["verifyPwd_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    editDreamSavingCompositeService["verifyPwd_password"] = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
	}
    editDreamSavingCompositeService["appID"] = appConfig.appId;
    if(flowSpa)
	{
	editDreamSavingCompositeService["channel"] = "IB"
	}
	else
	{
	editDreamSavingCompositeService["channel"] = "MB"
    }
	editDreamSavingCompositeService["gblVerifyOTP"] = gblVerifyOTP;
    editDreamSavingCompositeService["prodCode"] = prodCode;
    editDreamSavingCompositeService["prodName"] = prodName;
    editDreamSavingCompositeService["gblDreamflag"] = gblflagDreamMB;
    //editDreamSavingCompositeService["gbltargetIdSelected"] =gbltargetIdSelected;			
    //activity logging
 newtarget1 = frmDreamSavingEdit.lblTargetAmntVal.text;
 newtarget1 = newtarget1.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
 newmnthamnt = frmDreamSavingEdit.lblSetSavingVal.text
  newmnthamnt = newmnthamnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
    editDreamSavingCompositeService["activityLog_activityTypeID"] = "073";
    editDreamSavingCompositeService["activityLog_errorCd"] = "";
    editDreamSavingCompositeService["activityLog_activityStatus"] = "";
    editDreamSavingCompositeService["activityLog_deviceNickName"] = "";
    editDreamSavingCompositeService["activityLog_activityFlexValues1"] = prodCode + prodName
   
     if((oldTargetAmnt)== (newtarget1)){
     editDreamSavingCompositeService["activityLog_activityFlexValues2"] = newtarget1;
    }else{
    editDreamSavingCompositeService["activityLog_activityFlexValues2"] = oldTargetAmnt + "+"+newtarget1;
    }
    editDreamSavingCompositeService["activityLog_activityFlexValues3"] = AccountNoDream;
    if( oldMnthlyAmnt ==  (newmnthamnt)){
   
    editDreamSavingCompositeService["activityLog_activityFlexValues4"] = newmnthamnt;
    }else{
      editDreamSavingCompositeService["activityLog_activityFlexValues4"] = oldMnthlyAmnt+" + "+newmnthamnt;
    }
    if( oldRecurringDate == frmDreamSavingEdit.btnDreamSavecombo.text){
    editDreamSavingCompositeService["activityLog_activityFlexValues5"] = frmDreamSavingEdit.btnDreamSavecombo.text
    }else{
    editDreamSavingCompositeService["activityLog_activityFlexValues5"] = oldRecurringDate+" + "+  frmDreamSavingEdit.btnDreamSavecombo.text
    }
    
    
    editDreamSavingCompositeService["activityLog_logLinkageId"] = "";
	if(flowSpa)
	{
	editDreamSavingCompositeService["finActivityLog_channelId"] = "01";
	}
	else
	{
    editDreamSavingCompositeService["finActivityLog_channelId"] = "02";
    }
	MonthlySavingAmntUpdate = frmDreamSavingEdit.lblSetSavingVal.text;
    MonthlySavingAmntUpdate = MonthlySavingAmntUpdate.replace(/,/g, "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "");
    var mnthlyTransferDateUpdate = frmDreamSavingEdit.btnDreamSavecombo.text;
    if(mnthlyTransferDateUpdate.indexOf("st") > -1)
    {
    	mnthlyTransferDateUpdate=mnthlyTransferDateUpdate.replace("st", "");
    }
    if(mnthlyTransferDateUpdate.indexOf("nd") > -1)
    {
    	mnthlyTransferDateUpdate=mnthlyTransferDateUpdate.replace("nd", "");
    }
    if(mnthlyTransferDateUpdate.indexOf("rd") > -1)
    {
    	mnthlyTransferDateUpdate=mnthlyTransferDateUpdate.replace("rd", "");
    }
    //RecurringFundTransModelUpdate inputs :
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_clientDt"] = "";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accIdFrom"] = recurringToAcctId;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accTypeFrom"] = recurringToAcctType;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accIdTo"] = recurringFrmAcctId;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_accTypeTo"] = recurringFrmAcctType;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_curAmt"] = MonthlySavingAmntUpdate.trim();
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_dayofMonth"] = mnthlyTransferDateUpdate;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_freq"] = "Monthly";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_curCode"] = avaliableCurrCode.trim();
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_frmAcctProdId"] = prodCode;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_toAcctProdId"] = FromProductIdDreamMB;
	if(flowSpa)
	{
	editDreamSavingCompositeService["RecurringFundTransModelUpdate_channel"] = "IB";
    }
	else
	{
	editDreamSavingCompositeService["RecurringFundTransModelUpdate_channel"] = "MB";
    }
	editDreamSavingCompositeService["RecurringFundTransModelUpdate_rqUID"] = "";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_recXferId"] = recXferId;
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_name"] = "MB-INQ";
    editDreamSavingCompositeService["RecurringFundTransModelUpdate_spName"] = "com.fnf.xes.ST";
    //getDreamDesc inputs
    if (gblflagDreamMB == "true") {
        
        gbldreamtargetID = gbltargetIdSelected;
        DreamTargetImageMB() 
    }
    
    
    editDreamSavingCompositeService["getDreamDesc_dreamTargetID"] = gbldreamtargetID;
    
    editDreamSavingCompositeService["updateEditDreamSaveInfo_CRM_ID"] = "";
    editDreamSavingCompositeService["updateEditDreamSaveInfo_PERSONALIZED_ACCT_ID"] = gblAccNo;
    editDreamSavingCompositeService["updateEditDreamSaveInfo_PERSONALIZED_ID"] = gblpersonalizedId;
    var removebhat = frmDreamSavingEdit.lblTargetAmntVal.text;
    var removebhat1 = removebhat.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    editDreamSavingCompositeService["updateEditDreamSaveInfo_DREAM_TARGET_AMOUNT"] = removebhat1;
    editDreamSavingCompositeService["updateEditDreamSaveInfo_PERSONALIZED_ACCT_NICKNAME"] = frmDreamSavingEdit.lblNicknameVal.text.trim();
    editDreamSavingCompositeService["updateEditDreamSaveInfo_DREAM_DESC"] = frmDreamSavingEdit.lblDreamDescVal.text.trim();
    editDreamSavingCompositeService["updateEditDreamSaveInfo_DREAM_TARGET_ID"] = gbldreamtargetID;
    //invokeServiceSecureAsync("updateEditDreamSaveInfo", inputparam, OncalBackDreamSavingUpdateIB);
    //notification
    var removebhatnotification = frmDreamSavingEdit.lblTargetAmntVal.text;
    var newtarget = removebhatnotification.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    var NicknameDreamNotification = frmDreamSavingEdit.lblNicknameVal.text;
    
    var DreamDescGblNotification = frmDreamSavingEdit.lblDreamDescVal.text;
    var mnthnotification = frmDreamSavingEdit.lblSetSavingVal.text;
    var newmnthNotification = mnthnotification.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(/,/g, "");
    mnthlyTransferDateNotification = frmDreamSavingEdit.btnDreamSavecombo.text;
    var deliveryMethod = "Email";
    editDreamSavingCompositeService["NotificationAdd_notificationType"] = deliveryMethod;
    var currentLocale = getCurrentLocale();
    editDreamSavingCompositeService["NotificationAdd_Locale"] = currentLocale;
    editDreamSavingCompositeService["NotificationAdd_source"] = "editDreamSaving";
    editDreamSavingCompositeService["NotificationAdd_customerName"] = ""
    editDreamSavingCompositeService["NotificationAdd_custName"] = "";
    editDreamSavingCompositeService["NotificationAdd_accountNo"] = AccountNoDream; // sunstringing is beinng done at both client and server side have to check
    editDreamSavingCompositeService["NotificationAdd_actno"] = AccountNoDream; //why this is being passed to service
    editDreamSavingCompositeService["NotificationAdd_nickname"] = NicknameDreamNotification;
    editDreamSavingCompositeService["NotificationAdd_DREAM_DESC"] = DreamDescGblNotification;
    editDreamSavingCompositeService["NotificationAdd_targetAmt"] = newtarget;
    editDreamSavingCompositeService["NotificationAdd_monthlySavingAmt"] = newmnthNotification;
    editDreamSavingCompositeService["NotificationAdd_monthlyTransferDate"] = mnthlyTransferDateNotification;
    if(flowSpa)
	{
	editDreamSavingCompositeService["NotificationAdd_channelName"] = "IB";
	}
	else
	{
	editDreamSavingCompositeService["NotificationAdd_channelName"] = "MB"; // added by considering DEF9140 
    }//crmProfileMod
    editDreamSavingCompositeService["crmProfileMod_mbUserStatusId"] = "";
    editDreamSavingCompositeService["crmProfileMod_actionType"] = "";
    invokeServiceSecureAsync("editDreamSavingComposite", editDreamSavingCompositeService, editDreamSavingCompositeCallBackMB);
}


 function editDreamSavingCompositeCallBackMB(status,callBackResponse ){
 var updateEditDreamSave = callBackResponse ["updateEditDreamSave"];
 	if(updateEditDreamSave !=null && updateEditDreamSave !="" && updateEditDreamSave=="success"){
      // showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"))
		if(flowSpa){
		gblVerifyOTP = 0;
		}
		
		DreamUpdateDataMB();
	   dismissLoadingScreen();
	  popupTractPwd.dismiss() 
 	}else if ((callBackResponse["opstatus"] == 8005)) {
 		if (flowSpa){
 		
						            if (callBackResponse["errCode"] == "VrfyOTPErr00001") {
						                gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
						                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
						                popOtpSpa.lblPopupTract4Spa.text = "";
						                popOtpSpa.txtOTP.text = "";
						                kony.application.dismissLoadingScreen();
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
						                gblVerifyOTPCounter = 0;
						                otplocked = true;
						                kony.application.dismissLoadingScreen();
						                popOtpSpa.dismiss();
						                popTransferConfirmOTPLock.show();
						                // calling crmprofileMod to update the user status
						                updteuserSpa();
						
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00003") {
						                popOtpSpa.txtOTP.text = "";
						                kony.application.dismissLoadingScreen();
						                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00004") {
						               	popOtpSpa.txtOTP.text = "";
						                kony.application.dismissLoadingScreen();
						                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00005") {
						               popOtpSpa.txtOTP.text = "";
						               kony.application.dismissLoadingScreen();
						               showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
						               return false;
						            } else if (callBackResponse["errCode"] == "depErr02") {
					                    kony.application.dismissLoadingScreen();
					                    showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
					                    return false;
					                }else if (callBackResponse["errCode"] == "depErr01") {
					                    kony.application.dismissLoadingScreen();
					                    showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
					                    return false;
					                } else {
						                kony.application.dismissLoadingScreen();
						                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
						                return false;
						            }
						       
						        kony.application.dismissLoadingScreen();
						
 				} else if (!flowSpa) {
                    if (callBackResponse["errCode"] == "VrfyTxPWDErr00003") {
                    	gblRtyCtrVrfyTxPin = "0";
						kony.application.dismissLoadingScreen();
						//showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00002"), kony.i18n.getLocalizedString("info"));
						popupTractPwd.lblPopupTract7.text = "";
						//var deviceInfo = kony.os.deviceInfo();
				      	var deviceHght = gblDeviceInfo["deviceHeight"];
		
						    if(deviceHght>480){
						    popTransferConfirmOTPLock.containerHeight = 50;
//						    kony_6.0 popUp issue fix
						    //popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
						    }else{
						    popTransferConfirmOTPLock.containerHeight = 62;
						    //popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
						    }
						popupTractPwd.dismiss();
						popTransferConfirmOTPLock.show();
						return false;
						
					}else if (callBackResponse["errCode"] == "VrfyTxPWDErr00001") {
						kony.application.dismissLoadingScreen();
						popupTractPwd.lblPopupTract7.text = "";
						showAlert(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"));
						return false;
					} else if (callBackResponse["errCode"] == "VrfyTxPWDErr00002") {
						kony.application.dismissLoadingScreen();
						popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
						popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
						popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd")
						//showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00001"), kony.i18n.getLocalizedString("info"));
						return false;
					} else if (callBackResponse["errCode"] == "depErr02") {
	                    kony.application.dismissLoadingScreen();
	                    showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
	                    return false;
	                }else if (callBackResponse["errCode"] == "depErr01") {
	                   	kony.application.dismissLoadingScreen();
	                    showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
	                    return false;
	                }
	                
	                else {
							kony.application.dismissLoadingScreen();
							popupTractPwd.lblPopupTract7.text = "";
							showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
							return false;
				    }
 				}
 	}
 	else if (callBackResponse["serverStatusCode"] == "TS0789"  )
           	{    	
           		dismissLoadingScreen();
            	showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
        	}
    else if (callBackResponse["serverStatusCode"] == "IM0013")
    {
        		 dismissLoadingScreen();
        		 showAlert(kony.i18n.getLocalizedString("keyCurrentDream"), kony.i18n.getLocalizedString("info"));
        		 popOtpSpa.dismiss();
     }	
     else
     {
     	dismissLoadingScreen();
         showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
     }		
 }
 /*
 
 	
 	
 	var keyECUserNotFound = result["keyECUserNotFound"];
 	var ECVrfyTrnPwdErr00001 = result["ECVrfyTrnPwdErr00001"];
 	var VrfyTxPWDErr00003 = result["VrfyTxPWDErr00003"];
 	var ECJavaErr00001 = result["ECJavaErr00001"];
 	var ECGenericError = result["ECGenericError"];
 	var updateEditDreamSave = result["updateEditDreamSave"];
 	var wrongOTP = result["wrongOTP"];
    if(keyECUserNotFound !=null && keyECUserNotFound !="" && keyECUserNotFound=="keyECUserNotFound"){
    
      showAlert(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"))
	   dismissLoadingScreen();
     }else if(ECVrfyTrnPwdErr00001 !=null && ECVrfyTrnPwdErr00001 !="" && ECVrfyTrnPwdErr00001=="ECVrfyTrnPwdErr00001"){
       
       popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00001"), kony.i18n.getLocalizedString("info");
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
	   dismissLoadingScreen();
	  
     }else if(VrfyTxPWDErr00003 !=null && VrfyTxPWDErr00003 !="" && VrfyTxPWDErr00003=="VrfyTxPWDErr00003"){
      
	  gblRtyCtrVrfyTxPin = 0;
		kony.application.dismissLoadingScreen();
						//showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00002"), kony.i18n.getLocalizedString("info"));
		popupTractPwd.lblPopupTract7.text = "";
		var deviceInfo = kony.os.deviceInfo();
				      	var deviceHght = deviceInfo["deviceHeight"];
		
						    if(deviceHght>480){
						    popTransferConfirmOTPLock.containerHeight = 50;
						    popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
						    }else{
						    popTransferConfirmOTPLock.containerHeight = 62;
						    popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
						    }
						popupTractPwd.dismiss();
						popTransferConfirmOTPLock.show();
						return false;
     }else if(ECJavaErr00001 !=null && ECJavaErr00001 !="" && ECJavaErr00001=="ECJavaErr00001"){
       
       showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"))
	   dismissLoadingScreen();
     }else if(ECGenericError !=null && ECGenericError !="" && ECGenericError=="ECGenericError"){
       
       showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"))
	   dismissLoadingScreen();
     }else if(updateEditDreamSave !=null && updateEditDreamSave !="" && updateEditDreamSave=="success"){
      // showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"))
		gblRetryCountRequestOTP = 0;
		DreamUpdateDataMB();
	   dismissLoadingScreen();
	  popupTractPwd.dismiss() 
     }
     {
       	if (gblTokenSwitchFlag == true) 
       	{
             popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
             kony.application.dismissLoadingScreen();
        } else {
             popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
             popOtpSpa.lblPopupTract4Spa.text = "";
             dismissLoadingScreen();
        }
     } else{
     
   			
		 var StatusDesc = result["statusDesc"];
		  if (StatusDesc == "General Error") {
            showAlertIB(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"))
            dismissLoadingScreen();
            }else{
		 	alert(" " + StatusDesc);
		 	}
		 	 var serverStatusCodeMB = result["serverStatusCode"];
		 	 if (StatusDesc == "General Error") {
            showAlertIB(kony.i18n.getLocalizedString("keyCurrentDream"), kony.i18n.getLocalizedString("info"))
            dismissLoadingScreen();
            }
		 	dismissLoadingScreen();
		 	popupTractPwd.dismiss() ;
	}
       
      //popIBChequeServiceLocked.show();
 }
 */
function myAccountDreamDelete() {
 showLoadingScreen();
 var mydream_inputparam = {}
 
 //mydream_inputparam["personalizedId"] =gblpersonalizedId;
    mydream_inputparam["personalizedAcctId"] = gblPersonalizedRecordIdFromDreamSavings;
  //  mydream_inputparam["recordPersonalizedId"] = gblpersonalizedId;
 mydream_inputparam["acctNickName"] =frmDreamSavingMB.lblAccountNicknameValue.text
 mydream_inputparam["bankCD"] = "11";
 var Number = mydream_inputparam["personalizedAcctId"];
 var NickName = mydream_inputparam["acctNickName"];
 mydream_inputparam["acctStatus"] = "";
 mydream_inputparam["BankName"] = "TMB"; 
    mydream_inputparam["Number"] = Number;  
 invokeServiceSecureAsync("MyAccountDeleteService", mydream_inputparam, myAccountDelServiceCallBack);
}
function DreamonClickAgreeOpenDSActsFori18() {
    showLoadingScreen();
    var inputparam = {};
    invokeServiceSecureAsync("getDreamSavingTargets", inputparam, DreamcallBackOpenDreamSavingTargets);
}

function DreamcallBackOpenDreamSavingTargets(status, resulttable) {
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            var targetSegIndex = 1;
            //frmDreamSavingMB.imgDepositAccnt.src = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
            //frmOpnActSelAct.lblNSProdName.text = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].lblOpnActSelProd;
            var temp_seg = [];
            if (flowSpa) {
                gblspaSelIndex = 0;
                gblcarouselwidgetflow = "Savings";
                gblmbSpatransflow = false;
                gblnormSelIndex = gbltranFromSelIndex[1];
                frmDreamSavingEdit.segSliderSpa.widgetDataMap = {
                    img1: "img1",
                    lblCustName: "lblCustName"
                }
            } else {
                //    frmDreamSavingEdit.segSlider.removeAll();
                frmDreamSavingEdit.segSlider.containerWeight = 100;
                frmDreamSavingEdit.segSlider.widgetDataMap = {
                    imgDreamPic: "imgDreamPic",
                    imgDreamName: "imgDreamName"
                }
            }
            for (j = 0; j < resulttable["SavingTargets"].length; j++) {
                var dreamName;
                var dreamimage;
                var TargetHid;
                var locale = kony.i18n.getCurrentLocale();
                if (locale == "en_US") {
                    dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"];
                } else {
                    dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_TH"];
                }
                TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                dreamimage = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_IMG"];
                
                
                /*if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Vacation") {
                    dreamimage = "prod_lrg_vacation.png";
                    //  frmDreamSavingEdit.lbldreamValue.text= "My Vacation"
                    TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Dream") {
                    dreamimage = "prod_lrg_dream.png";
                    //    frmDreamSavingEdit.lbldreamValue.text= "My Dream"
                    TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Car") {
                    dreamimage = "prod_lrg_car.png";
                    //   frmDreamSavingEdit.lbldreamValue.text= "My Car"
                    TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Education") {
                    dreamimage = "prod_lrg_education.png";
                    //  frmDreamSavingEdit.lbldreamValue.text= "My Education"
                    TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Own House") {
                    dreamimage = "prod_lrg_homeloan.png";
                    //  frmDreamSavingEdit.lbldreamValue.text="My Own House"
                    TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Business") {
                    dreamimage = "prod_lrg_business.png";
                    //  frmDreamSavingEdit.lbldreamValue.text= "My Business"
                    TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                } else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Saving") {
                    dreamimage = "prod_lrg_saving.png";
                    //   frmDreamSavingEdit.lbldreamValue.text=  "My Saving"
                    TargetHid = resulttable["SavingTargets"][j]["DREAM_TARGET_ID"];
                }*/
                if(TargetHid == gbldreamtargetID){
                	targetSegIndex = j;
                }
                if (flowSpa) {
                    temp_seg.push({
                        "lblCustName": dreamName,
                        "img1": dreamimage,
                        "lblhiddentarget": TargetHid,
                        template: hbxSliderDream
                    })
                } else {
                    temp_seg.push({
                        "imgDreamName": dreamName,
                        "imgDreamPic": dreamimage,
                        "lblhiddentarget": TargetHid,
                        template: hbxSliderDream
                    })
                }
                
                /*if (dreamTargetListResultTable["dreamsavingDS"][0]["DREAM_DESC"]==resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"]){
            	          		frmDreamSavingMB.imgProd.src =dreamimage;
            		}*/
            }
            if (flowSpa) {
                frmDreamSavingEdit.segSliderSpa.data = temp_seg;
            } else {
                frmDreamSavingEdit.segSlider.setData(temp_seg);
                //alert(gbldreamtargetID);
                frmDreamSavingEdit.segSlider.selectedIndex = [0, targetSegIndex];
                dsSelActSelIndex=[0,0];
                dsSelActLength = frmDreamSavingEdit.segSlider.data.length - 1;
            }
            
          
        }
    }
    frmDreamSavingEdit.show()
}

function myAcountSummaryDeleteMB(status, resulttable) {
dismissLoadingScreen()
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            
        }
    }
}

function openFrmBranchMB(){
var inputParams = {};
		
			if(flowSpa)
				{
					inputParams["dreamTargetId"] = "";//gbldreamtargetID;
					inputParams["channelId"] = "01";
				}
				else
				{
					inputParams["dreamTargetId"] ="";//gbldreamtargetID;
					inputParams["channelId"] = "02";
		        }
				inputParams["dreamDesc"] =""; //frmDreamSavingMB.lblDreamDesc.text;
				
			//	var tarAmountDream = frmDreamSavingMB.lblTargetAmount.text;
			    inputParams["dreamTargetAmnt"] = "0"; //tarAmountDream.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
				inputParams["flowIdOpenAct"] = "dreamSaving";
				inputParams["personalizedId"] = "";
				
        	inputParams["personalizedActId"] =gblAccNo
        	invokeServiceSecureAsync("getDreamActInfo", inputParams, BranchServiceMB);

}
function BranchServiceMB(status, resulttable) {
   //dismissLoadingScreenPopup();
var personalizedid = "";
var zeroPersonalizedId="";
var personalizedId = "";
var inputParams = {};
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        
        zeroPersonalizedId = resulttable["zeroPersonalizedId"];
        personalizedid = resulttable["PERSONALIZED_ID"];
        personalizedId = resulttable["personalizedId"];
        var lengthId = zeroPersonalizedId.length;
        var DreamlengthId = personalizedid.length;
          if(lengthId == 0 && DreamlengthId ==0)
          {
                inputParams["dreamTargetId"] = "";   //"1"//gbldreamtargetID;
				inputParams["dreamDesc"] ="";//frmDreamSavingMB.lblDreamDesc.text.trim();
				inputParams["personalizedActId"] =gblAccNo;
				//var tarAmountDream = frmDreamSavingMB.lblTargetAmount.text;
			    inputParams["dreamTargetAmnt"] =""; // tarAmountDream.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
				inputParams["flowIdOpenAct"] = "dreamSaving";
				inputParams["personalizedId"] = personalizedId;
				inputParams["channelId"] = "02";
				invokeServiceSecureAsync("addDreamSavingCareDataOpenAct", inputParams, callBackServiceDream);
			}
        }
    }
}


function saveAmountinSessionMB(){
    
     var removeSybolTargetMB = frmDreamSavingEdit.lblTargetAmntVal.text;
    removeSybolTargetMB = removeSybolTargetMB.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");
    
    var MonthlySavingAmntMB =frmDreamSavingEdit.lblSetSavingVal.text;
    MonthlySavingAmntMB = MonthlySavingAmntMB.replace(/,/g, "").replace("/", "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "");
    
    inputParam = {};
	
	inputParam["DreamMonthlyAmnt"] = MonthlySavingAmntMB;						//removeCommas(txtBalMaxVal);
	inputParam["DreamTargetAmnt"] = removeSybolTargetMB;	
				
	inputParam["DreamFromAccount"] = recurringToAcctId.toString();
	inputParam["DreamToAccount"] = recurringFrmAcctId.toString();
	
	//added below activity logging lines to fix DEF11 UAT defect
	   
    inputParam["activityFlexValues1"] = prodCode + prodName
    if((oldTargetAmnt)== (removeSybolTargetMB)){
    	inputParam["activityFlexValues2"] = removeSybolTargetMB;
    }else{
    	inputParam["activityFlexValues2"] = oldTargetAmnt + "+"+removeSybolTargetMB;
    }
    inputParam["activityFlexValues3"] = AccountNoDream;
    if( oldMnthlyAmnt ==  (MonthlySavingAmntMB)){
    	inputParam["activityFlexValues4"] = MonthlySavingAmntMB;
    }else{
      	inputParam["activityFlexValues4"] = oldMnthlyAmnt+" + "+MonthlySavingAmntMB;
    }
    if( oldRecurringDate == frmDreamSavingEdit.btnDreamSavecombo.text){
    	inputParam["activityFlexValues5"] = frmDreamSavingEdit.btnDreamSavecombo.text;
    }else{
    	inputParam["activityFlexValues5"] = oldRecurringDate+" + "+  frmDreamSavingEdit.btnDreamSavecombo.text;
    }
	
	invokeServiceSecureAsync("EditDreamSavingCheck", inputParam, saveInputinSessioncallBackIBDreamMB);
}
function saveInputinSessioncallBackIBDreamMB(status,result){
    if (status == 400) {
		
		
		if (result["opstatus"] == "0") {
		
		if(flowSpa)
	 		{	
	 		if(gblIBFlowStatus == "04")		
			{
				alertUserStatusLocked();
			}
			else
			{
			var inputParams = {}
			spaChnage = "dreamsavings"
			inputParams["eventNotificationId"] = "MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
			inputParams["smsSubject"] = "MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
			gblOTPFlag = true;
			gblOnClickReq = false;
			try {
				kony.timer.cancel("otpTimer")
			} catch (e) {
				
			}
			//input params for SPA Otp request
			gblSpaChannel = "UpdateDreamSavings";
			/*
		    if (kony.i18n.getCurrentLocale() == "en_US") {
		        SpaEventNotificationPolicy = "MIB_UpdateDreamSaving_EN";
		        SpaSMSSubject = "MIB_UpdateDreamSaving_EN";
		    } else {
		        SpaEventNotificationPolicy = "MIB_UpdateDreamSaving_TH";
		        SpaSMSSubject = "MIB_UpdateDreamSaving_TH";
		    }*/
			onClickOTPRequestSpa();
			//callbackOpennewAcct();
			}
		 }
		  else
		 {
		 	
		 	showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "",  editDreamSavingCompositeServiceMB, 3);
		 }
			   
		
	}
 }
}
function dreamSavingPerformBackButton(){
	if(gblPreviousForm == "frmMyAccountList") {
		frmMyAccountList.show();
	} else if(gblPreviousForm == "frmAccountDetailsMB"){
		frmAccountDetailsMB.show();
	}
}
