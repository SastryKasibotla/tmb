//global vars to be populated based on the service response of inquiry service.
gblCIAnyID=false;
gblMobileAnyID=false;
localCIAnyID=false;
localMobileAnyID=false;
gblCIAnyIDRegisterAllowed=true;
gblMobileAnyIDRegisterAllowed=true;
gblCIAnyIDRegisteredAccount={};
gblMobileAnyIDRegisteredAccount={};
gblSelectedAnyIDMobileAccount={};
gblSelectedAnyIDCitizenAccount={};
gblAnyIDInqData={};
gblAnyIDAccountTable=[];

// method to check and go to Any ID page. Used in menu
function navigatetoAnyIDBrief(){
	if(getCRMLockStatus()){
		popIBBPOTPLocked.show();
	}else{
		var anyIDProdFeature_image_name_EN = "AnyIdIntroEN";
		var	anyIDProdFeature_image_name_TH = "AnyIdIntroTH";
		var anyIDProdFeature_image_EN="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+anyIDProdFeature_image_name_EN+"&modIdentifier=PRODUCTPACKAGEIMG";
		var anyIDProdFeature_image_TH="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+anyIDProdFeature_image_name_TH+"&modIdentifier=PRODUCTPACKAGEIMG";
		frmIBAnyIDProdFeature.imgAnyIDProdFeatureEN.src = anyIDProdFeature_image_EN;
		frmIBAnyIDProdFeature.imgAnyIDProdFeatureTH.src = anyIDProdFeature_image_TH;
		languageToggleForAnyIDProdFeatureIB();
		frmIBAnyIDProdFeature.show();
	}
}

function languageToggleForAnyIDProdFeatureIB(){
	// all language toggle code for Prod Feature page to be put here
	var showThai = kony.i18n.getCurrentLocale() == "th_TH";
	frmIBAnyIDProdFeature.imgAnyIDProdFeatureEN.setVisibility(!showThai);
	frmIBAnyIDProdFeature.imgAnyIDProdFeatureTH.setVisibility(showThai);
}

function customerInqForAnyIDIB(){
	showLoadingScreenPopup();
	var inputparam = {};
	inputparam["accountsFlag"] = "true";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, customerInqForAnyIDIBcallback);
}
function customerInqForAnyIDIBcallback(status,resulttable){
	if(status == 400){
		if(resulttable["opstatus"] == 0){
			gblAnyIDAccountTable = resulttable;
			onClickAccountForAnyIDRegistration();
		}
	}
}

function anyIDRegisterInqServiceIB(){
	showLoadingScreenPopup();
	var inputParams = {};
    invokeServiceSecureAsync("AnyIDInqComposite", inputParams, anyIDRegisterInqServiceIBCallBack);
}

function anyIDRegisterInqServiceIBCallBack(status,resulttable){
	if(status == 400){
		dismissLoadingScreenPopup();
		if(resulttable["opstatus"] == 0){
			
			if(undefined != resulttable["registerAnyIdBusinessHrsFlag"])
			{
				if(resulttable["registerAnyIdBusinessHrsFlag"] == "false"){
					var startTime = resulttable["regAnyIdStartTime"];
		     		var endTime = resulttable["regAnyIdEndTime"];
		     		var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
		     		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
		     		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
		           	showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
		           	return false;
               	}
			}
			gblAnyIDInqData = resulttable;
			if(undefined == resulttable["MobileDS"]){
				gblMobileAnyID = false;
				localMobileAnyID = false;
				gblMobileAnyIDRegisterAllowed = true;
				gblMobileAnyIDRegisteredAccount = {};
			}else if(resulttable["MobileDS"].length >0){
				// Do Something for Mobile
				var MobileAnyIDData = resulttable["MobileDS"][0];
				if(MobileAnyIDData["iTMXFlag"] == "Y"){
					if(MobileAnyIDData["onUSFlag"] == "Y"){
						gblMobileAnyID = true;
						localMobileAnyID = true;
						gblMobileAnyIDRegisterAllowed = true;
						gblMobileAnyIDRegisteredAccount = searchAnyIDinCustActInqIB(MobileAnyIDData["acctIdentValue"]);
					}else{
						gblMobileAnyID = false;
						localMobileAnyID = false;
						gblMobileAnyIDRegisterAllowed = false;
						gblMobileAnyIDRegisteredAccount = {};
					}
				}else{
					gblMobileAnyID = false;
					localMobileAnyID = false;
					gblMobileAnyIDRegisterAllowed = true;
					gblMobileAnyIDRegisteredAccount = {};
				}
			}else{
				gblMobileAnyID = false;
				localMobileAnyID = false;
				gblMobileAnyIDRegisterAllowed = true;
				gblMobileAnyIDRegisteredAccount = {};
			}
			gblSelectedAnyIDMobileAccount = gblMobileAnyIDRegisteredAccount;
			
			if(undefined == resulttable["CIDS"]){
				gblCIAnyID = false;
				localCIAnyID = false;
				gblCIAnyIDRegisterAllowed = true;
				gblCIAnyIDRegisteredAccount = {};
			}else if(resulttable["CIDS"].length >0){
				var CIAnyIDData = resulttable["CIDS"][0];
				if(CIAnyIDData["iTMXFlag"] == "Y"){
					if(CIAnyIDData["onUSFlag"] == "Y"){
						gblCIAnyID = true;
						localCIAnyID = true;
						gblCIAnyIDRegisterAllowed = true;
						gblCIAnyIDRegisteredAccount = searchAnyIDinCustActInqIB(CIAnyIDData["acctIdentValue"]);
					}else{
						gblCIAnyID = false;
						localCIAnyID = false;
						gblCIAnyIDRegisterAllowed = false;
						gblCIAnyIDRegisteredAccount = {};
					}
				}else{
					// Not registered with any one
					gblCIAnyID = false;
					localCIAnyID = false;
					gblCIAnyIDRegisterAllowed = true;
					gblCIAnyIDRegisteredAccount = {};
				}
			}else{
				gblCIAnyID = false;
				localCIAnyID = false;
				gblCIAnyIDRegisterAllowed = true;
				gblCIAnyIDRegisteredAccount = {};
			}
			gblSelectedAnyIDCitizenAccount = gblCIAnyIDRegisteredAccount;
			
			navigateToAnyIDRegistration();
		}else 
		{
			if(resulttable["opstatus"] == 1)
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
		}
	}
}

function searchAnyIDinCustActInqIB(actid)
{
	if(gblAnyIDAccountTable!=null)
	{
		for (var i = 0; gblAnyIDAccountTable.custAcctRec != null && i < gblAnyIDAccountTable.custAcctRec.length; i++) 
		{
			if((gblAnyIDAccountTable.custAcctRec[i].accId.indexOf(actid))>-1)
			{
				var accountName="";
				var accountNameEN = "";
				var accountNameTH = "";
				var accountNo="";
				var imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblAnyIDAccountTable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
				if ((gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == null || (gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == '') {
					var sbStr = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
					var length = sbStr.length;
					sbStr = sbStr.substring(length - 4, length);
					if (kony.i18n.getCurrentLocale() == "th_TH")
							accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"]+ " " + sbStr;
					else
							accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"]+ " " + sbStr;
					
					accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"]+ " " + sbStr;
					accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"]+ " " + sbStr;
				}else {
					accountName = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
					accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
					accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
				}
				if (gblAnyIDAccountTable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard")){
					accountNo = gblAnyIDAccountTable["custAcctRec"][i]["accountNoFomatted"];
				}else{
					var accountNoUnformatted = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
					var accountNoTenDigit = accountNoUnformatted.substring(accountNoUnformatted.length - 10,accountNoUnformatted.length);
					accountNo = formatAccountNo(accountNoTenDigit);
				}
				var tempAnyIDRegisteredAccount={
					"imgAccountType":imageName,
					"lblAccountName":accountName,
					"lblAccountNumber":accountNo,
					"lblAccountNumberText":kony.i18n.getLocalizedString("keyAccountNoIB"),
					"lblAccountNameEN":accountNameEN,
					"lblAccountNameTH":accountNameTH,
					"hiddenAccountId":gblAnyIDAccountTable["custAcctRec"][i]["accId"],
					"accountType":gblAnyIDAccountTable["custAcctRec"][i]["accType"]
				}
				return tempAnyIDRegisteredAccount;
			}
		}
	}
}

function navigateToAnyIDRegistration(){
	if(gblCustomerIDType == "CI"){
		frmIBAnyIDRegistration.lblCitizenid.text = maskCitizenID(gblCustomerIDValue);
		hideCitizenIDAnyIDDetailsIB(true);
	}else{
		hideCitizenIDAnyIDDetailsIB(false);
		// have to hide citizen id data
	}
	frmIBAnyIDRegistration.lblPhoneNum.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);  //formatMobileNumber(gblPHONENUMBER) -- in case we have to show normal mobile number 
	if(gblMobileAnyID)
		frmIBAnyIDRegistration.btnCheckboxMobile.skin = btnCheckFocTransparent;
	else
		frmIBAnyIDRegistration.btnCheckboxMobile.skin = btnCheckTransparent;
	if(gblCIAnyID)
		frmIBAnyIDRegistration.btnCheckboxCitizenId.skin = btnCheckFocTransparent;
	else
		frmIBAnyIDRegistration.btnCheckboxCitizenId.skin = btnCheckTransparent;	
	
	if(frmIBAnyIDRegistration.btnCheckboxMobile.skin == btnCheckTransparent){
		frmIBAnyIDRegistration.hbxMobileAccountDetails.setVisibility(false);
	}else{
		frmIBAnyIDRegistration.hbxMobileAccountDetails.setVisibility(true);
		// have to populate account details here
	}
	if(frmIBAnyIDRegistration.btnCheckboxCitizenId.skin == btnCheckTransparent){
		frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.setVisibility(false);
	}else{
		frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.setVisibility(true);
		// have to populate account details here
	}
	frmIBAnyIDRegistration.hbxMobileInfo.setVisibility(gblMobileAnyIDRegisterAllowed);
	frmIBAnyIDRegistration.hbxMobileAlreadyRegistered.setVisibility(!gblMobileAnyIDRegisterAllowed);
	frmIBAnyIDRegistration.hbxMobileChangeHelp.setVisibility(!gblMobileAnyIDRegisterAllowed);
	frmIBAnyIDRegistration.hbxCitizenidInfo.setVisibility(gblCIAnyIDRegisterAllowed && gblCustomerIDType == "CI");
	frmIBAnyIDRegistration.hbxCitizenIdAlreadyRegistered.setVisibility(!gblCIAnyIDRegisterAllowed && gblCustomerIDType == "CI");
	frmIBAnyIDRegistration.hbxCitizenIdChangeHelp.setVisibility(!gblCIAnyIDRegisterAllowed && gblCustomerIDType == "CI");
	anyIDPageResetIB();
	showNextORAgreeAnyIDIB();
	enableDisableAnyIDAllClickableOnOTP(true);
	frmIBAnyIDRegistration.show();
}

function populateSelectedAccountUnderAnyIDIB(){
	var locale = kony.i18n.getCurrentLocale();
	if(isNotBlankObject(gblSelectedAnyIDCitizenAccount)){
		if(locale == "en_US")
			frmIBAnyIDRegistration.lblCitizenidAccountName.text=gblSelectedAnyIDCitizenAccount["lblAccountNameEN"];
		else
			frmIBAnyIDRegistration.lblCitizenidAccountName.text=gblSelectedAnyIDCitizenAccount["lblAccountNameTH"];
		frmIBAnyIDRegistration.lblCitizenidAccountNumber.text=gblSelectedAnyIDCitizenAccount["lblAccountNumber"];
		frmIBAnyIDRegistration.imgCitizenidAccountSymbol.src=gblSelectedAnyIDCitizenAccount["imgAccountType"];
	}else{
		frmIBAnyIDRegistration.lblCitizenidAccountName.text="";
		frmIBAnyIDRegistration.lblCitizenidAccountNumber.text="";
		frmIBAnyIDRegistration.imgCitizenidAccountSymbol.src="";
	}	
	if(isNotBlankObject(gblSelectedAnyIDMobileAccount)){
		if(locale == "en_US")
			frmIBAnyIDRegistration.lblMobileAccountName.text=gblSelectedAnyIDMobileAccount["lblAccountNameEN"];
		else
			frmIBAnyIDRegistration.lblMobileAccountName.text=gblSelectedAnyIDMobileAccount["lblAccountNameTH"];	
		frmIBAnyIDRegistration.lblMobileAccountNumber.text=gblSelectedAnyIDMobileAccount["lblAccountNumber"];
		frmIBAnyIDRegistration.imgMobileAccountSymbol.src=gblSelectedAnyIDMobileAccount["imgAccountType"];
	}else{
		frmIBAnyIDRegistration.lblMobileAccountName.text="";
		frmIBAnyIDRegistration.lblMobileAccountNumber.text="";
		frmIBAnyIDRegistration.imgMobileAccountSymbol.src="";
	}	
}

function hideCitizenIDAnyIDDetailsIB(showCI){
	frmIBAnyIDRegistration.hbxCitizenidAnyid.setVisibility(showCI);
	frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.setVisibility(showCI);
	frmIBAnyIDRegistration.lineCitizenid.setVisibility(showCI);
	frmIBAnyIDRegistration.hbxCitizenidInfo.setVisibility(showCI);
	frmIBAnyIDRegistration.hbxCitizenIdAlreadyRegistered.setVisibility(showCI);
	frmIBAnyIDRegistration.hbxCitizenIdChangeHelp.setVisibility(showCI);
}

function anyIDPageResetIB(){
	frmIBAnyIDRegistration.hbxwaterWheel.setVisibility(true);
	frmIBAnyIDRegistration.hbxAccountList.setVisibility(false);
	frmIBAnyIDRegistration.hbxTermsAndConditions.setVisibility(false);
	frmIBAnyIDRegistration.hbxOtpBox.setVisibility(false);
}

function languageToggleForAnyIDRegistrationIB(){
	// all language toggle code for Registration page to be put here
	if(showAgree){
		frmIBAnyIDRegistration.btnNext.text=kony.i18n.getLocalizedString("keyAgreeButton");
	}else{	
		frmIBAnyIDRegistration.btnNext.text=kony.i18n.getLocalizedString("Next");
	}
	if(frmIBAnyIDRegistration.hbxAccountList.isVisible){
		onClickAccountForAnyIDRegistration();
	}
	if(!gblMobileAnyIDRegisterAllowed){
		var MobileAlreadyRegText = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankMobileNo");
		var replacedMobileText = MobileAlreadyRegText.replace("<Bank short name",gblAnyIDInqData.MobileDS[0].bankShortName);
		if (kony.i18n.getCurrentLocale() == "th_TH") 
			var replacedMobileText = replacedMobileText.replace("Bank name>",gblAnyIDInqData.MobileDS[0].bankNameTH);
		else 
			var replacedMobileText = replacedMobileText.replace("Bank name>",gblAnyIDInqData.MobileDS[0].bankNameEN);
		frmIBAnyIDRegistration.lblMobileAlreadyRegistered.text = replacedMobileText;
	}
	if(!gblCIAnyIDRegisterAllowed){
		var CIAlreadyRegText = kony.i18n.getLocalizedString("MIB_P2PLinkOtherBankCitizenID");
		var replacedCIText = CIAlreadyRegText.replace("<Bank short name",gblAnyIDInqData.CIDS[0].bankShortName);
		if (kony.i18n.getCurrentLocale() == "th_TH") 
			var replacedCIText = replacedCIText.replace("Bank name>",gblAnyIDInqData.CIDS[0].bankNameTH);
		else 
			var replacedCIText = replacedCIText.replace("Bank name>",gblAnyIDInqData.CIDS[0].bankNameEN);
		frmIBAnyIDRegistration.lblCitizenIdAlreadyRegistered.text = replacedCIText;
	}
	populateSelectedAccountUnderAnyIDIB();
}

function onClickEditMobileNumberIBAnyID(){
	checkBusinessHoursIB();
	if(getCRMLockStatus())
	{
		curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}else if(s2sBusinessHrsFlag == "false"){
		// have to replace "s2sBusinessHrsFlag" with another variable from any id inquiry response. 
		// Also the start and end time have to be pushed for alert.
		showAlert(getErrorMsgForBizHrs(), kony.i18n.getLocalizedString("info"));
	}else{
		gblEditMobileFromAnyId=true;
		TMBUtil.DestroyForm(frmIBCMChgMobNoTxnLimit);
		TMBUtil.DestroyForm(frmIBCMEditMyProfile);
		IBtoCallViewProfileonSave();
	}
}

function checkBusinessHoursIB(){
	var inputParam={};
	invokeServiceSecureAsync("anyIDBusinessHrsCheck", inputParam, anyIDBusinessHrsCheckIBCallback);
}

function anyIDBusinessHrsCheckIBCallback(status, callBackResponse) {
	if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
        	if(callBackResponse["s2sBusinessHrsFlag"] != null){
        		s2sBusinessHrsFlag = callBackResponse["s2sBusinessHrsFlag"];
        	}
			if(callBackResponse["s2sStartTime"] != null){
				s2sStartTime = callBackResponse["s2sStartTime"];
			}
			if(callBackResponse["s2sEndTime"] != null){
				s2sEndTime = callBackResponse["s2sEndTime"];
			}
        }
	}
}

function onClickAnyIDTnC(){
	var currentLocale = kony.i18n.getCurrentLocale();
	var locale = kony.i18n.getCurrentLocale();
    var input_param = {};
    if (locale == "en_US") {
		input_param["localeCd"]="en_US";
	}else{
		input_param["localeCd"]="th_TH";
	}

	input_param["moduleKey"]='AnyIDRegistration';
	showLoadingScreenPopup();
	frmIBAnyIDRegistration.lblEStatementDesc.text="";
    invokeServiceSecureAsync("readUTFFile", input_param, setIBAnyIDTnC);
}
function setIBAnyIDTnC(status,result){
	if(status==400){
		dismissLoadingScreenPopup();
		if(result["opstatus"] == 0){
			frmIBAnyIDRegistration.hbxwaterWheel.setVisibility(false);
			frmIBAnyIDRegistration.hbxAccountList.setVisibility(false);
			frmIBAnyIDRegistration.hbxTermsAndConditions.setVisibility(true);
			frmIBAnyIDRegistration.hbxOtpBox.setVisibility(false);
			frmIBAnyIDRegistration.lblEStatementDesc.text = result["fileContent"];
		}
	}
}

function loadTermsNConditionAnyIDRegistrationIB(){
	var input_param = {};
	var locale = kony.i18n.getCurrentLocale();
	    if (locale == "en_US") {
		input_param["localeCd"]="en_US";
		}else{
		input_param["localeCd"]="th_TH";
		}
	input_param["moduleKey"]= 'AnyIDRegistration';
	
	invokeServiceSecureAsync("readUTFFile", input_param, loadTNCPointAnyIDRegistrationIBCallBack);
}

function loadTNCPointAnyIDRegistrationIBCallBack(status,result){
	if(status == 400){
		if(result["opstatus"] == 0){
			var data = result["fileContent"];
			var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
			if(tncTitle=="Terms & Conditions")
				{
				 	tncTitle = "TermsandConditions";
				 }
			showPopup(tncTitle,data);
		}else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function onClickEmailAnyIDRegistrationIB() {
	
	showLoadingScreenPopup()
	var inputparam = {};
	inputparam["channelName"] = "Internet Banking";
	inputparam["channelID"] = "01";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "AnyIDRegistration";
    //if (kony.i18n.getCurrentLocale() == "en_US") {
	inputparam["productName"] = "";
	//} else {
	inputparam["productNameTH"] = "";
	//}
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackEmailTnCAnyIDRegistrationIB);
	
}

function callBackEmailTnCAnyIDRegistrationIB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				showAlert(kony.i18n.getLocalizedString("keytermOpenAcnt"), kony.i18n.getLocalizedString("info"));
				dismissLoadingScreenPopup();
			} else {
				dismissLoadingScreenPopup();
				return false;
			}
		} else {
			dismissLoadingScreenPopup()
		}
	}
}

function unRegisterAnyIDCitizenIdIB(){
	frmIBAnyIDRegistration.btnCheckboxCitizenId.skin=btnCheckTransparent;
	frmIBAnyIDRegistration.lblCitizenidAccountName.text="";
	frmIBAnyIDRegistration.lblCitizenidAccountNumber.text="";
	frmIBAnyIDRegistration.imgCitizenidAccountSymbol.src="";
	gblSelectedAnyIDCitizenAccount={};
	frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.setVisibility(false);
	localCIAnyID=false;
	showNextORAgreeAnyIDIB();
}

function unRegisterAnyIDMobileIB(){
	frmIBAnyIDRegistration.btnCheckboxMobile.skin=btnCheckTransparent;
	frmIBAnyIDRegistration.lblMobileAccountName.text="";
	frmIBAnyIDRegistration.lblMobileAccountNumber.text="";
	frmIBAnyIDRegistration.imgMobileAccountSymbol.src="";
	gblSelectedAnyIDMobileAccount = {};
	frmIBAnyIDRegistration.hbxMobileAccountDetails.setVisibility(false);
	localMobileAnyID=false;
	showNextORAgreeAnyIDIB();
}

function onClickAccountForAnyIDRegistration(){
	
	gblAccountsAnyID=[];
	if(undefined != gblAnyIDAccountTable["custAcctRec"])
	{
		for(var i=0;i<gblAnyIDAccountTable["custAcctRec"].length; i++){
		
		var accountName;
		var accountNo;
		var accountNameEN;
		var accountNameTH;
		var accountNoTenDigit;
		var accountNoUnformatted;
		var isAnyIDAccount = gblAnyIDAccountTable["custAcctRec"][i]["allowRegisterAnyId"];
		var isAllowedForShowing = true;//(undefined == gblAnyIDAccountTable["custAcctRec"][i]["personalisedAcctStatusCode"]) || (gblAnyIDAccountTable["custAcctRec"][i]["personalisedAcctStatusCode"] == "01");
			
			if(parseInt(isAnyIDAccount) && isAllowedForShowing){
			
				if ((gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == null || (gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"]) == '') {
					var sbStr = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
					var length = sbStr.length;
					sbStr = sbStr.substring(length - 4, length);
					if (kony.i18n.getCurrentLocale() == "th_TH")
							accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"]+ " " + sbStr;
					else
							accountName = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"]+ " " + sbStr;
					
					accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameEng"]+ " " + sbStr;
					accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["ProductNameThai"]+ " " + sbStr;
				}else {
					accountName = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
					accountNameEN = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
					accountNameTH = gblAnyIDAccountTable["custAcctRec"][i]["acctNickName"];
				}
				
				if (gblAnyIDAccountTable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard")){
					accountNo = gblAnyIDAccountTable["custAcctRec"][i]["accountNoFomatted"];
				}else{
					accountNoUnformatted = gblAnyIDAccountTable["custAcctRec"][i]["accId"];
					accountNoTenDigit = accountNoUnformatted.substring(accountNoUnformatted.length - 10,accountNoUnformatted.length);
					accountNo = formatAccountNo(accountNoTenDigit);
				}
			
				imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+gblAnyIDAccountTable["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
				
				var tempToCashbackAccount={
					"imgAccountType":imageName,
					"lblAccountName":accountName,
					"lblAccountNumber":accountNo,
					"lblAccountNumberText":kony.i18n.getLocalizedString("keyAccountNoIB"),
					"lblAccountNameEN":accountNameEN,
					"lblAccountNameTH":accountNameTH,
					"hiddenAccountId":gblAnyIDAccountTable["custAcctRec"][i]["accId"],
					"accountType":gblAnyIDAccountTable["custAcctRec"][i]["accType"]
				}
	
			gblAccountsAnyID.push(tempToCashbackAccount);
			}
		}
	}	
	if(isNotBlankObject(gblAccountsAnyID)){
		if(kony.application.getCurrentForm().id == "frmIBAnyIDRegistration"){
			frmIBAnyIDRegistration.segAccountList.setData(gblAccountsAnyID);
			frmIBAnyIDRegistration.lblMesg.isVisible=false;
			frmIBAnyIDRegistration.hbxContainerAccount.isVisible=true;
		}
		if(kony.application.getCurrentForm().id == "frmIBAnyIDProdFeature"){
			if(gblAccountsAnyID.length == 1){
				frmIBAnyIDRegistration.imageRightArrow.setVisibility(false);
				frmIBAnyIDRegistration.image23902450651133616.setVisibility(false);
				frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.skin=hboxLightBlue;
				frmIBAnyIDRegistration.hbxMobileAccountDetails.skin=hboxLightBlue;
			}else{
				frmIBAnyIDRegistration.imageRightArrow.setVisibility(true);
				frmIBAnyIDRegistration.image23902450651133616.setVisibility(true);
				frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.skin=hboxLightBlueCursor;
				frmIBAnyIDRegistration.hbxMobileAccountDetails.skin=hboxLightBlueCursor;
			}
			anyIDRegisterInqServiceIB();
			return false;
		}
	}else{
		if(kony.application.getCurrentForm().id == "frmIBAnyIDProdFeature"){
			dismissLoadingScreenPopup();
			showAlert(kony.i18n.getLocalizedString("MIB_P2PErr_NoEliAcc"), kony.i18n.getLocalizedString("info"));
			return false;
		}else{
			frmIBAnyIDRegistration.lblMesg.isVisible=true;
			frmIBAnyIDRegistration.hbxContainerAccount.isVisible=false
		}	
	}
	if(gblAccountsAnyID.length == 1){
		// To auto populate the account
		if(kony.application.getCurrentForm().id == "frmIBAnyIDRegistration"){
			frmIBAnyIDRegistration.segAccountList.selectedIndex=[0,0];
			onSelectToAccountForAnyIDIB();
			frmIBAnyIDRegistration.hbxwaterWheel.setVisibility(true);
			frmIBAnyIDRegistration.hbxAccountList.setVisibility(false);
			frmIBAnyIDRegistration.hbxTermsAndConditions.setVisibility(false);
			frmIBAnyIDRegistration.hbxOtpBox.setVisibility(false);
			frmIBAnyIDRegistration.imageRightArrow.setVisibility(false);
			frmIBAnyIDRegistration.image23902450651133616.setVisibility(false);
			frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.skin=hboxLightBlue;
			frmIBAnyIDRegistration.hbxMobileAccountDetails.skin=hboxLightBlue;
		}
	}else{
		frmIBAnyIDRegistration.hbxwaterWheel.setVisibility(false);
		frmIBAnyIDRegistration.hbxAccountList.setVisibility(true);
		frmIBAnyIDRegistration.hbxTermsAndConditions.setVisibility(false);
		frmIBAnyIDRegistration.hbxOtpBox.setVisibility(false);
		frmIBAnyIDRegistration.imageRightArrow.setVisibility(true);
		frmIBAnyIDRegistration.image23902450651133616.setVisibility(true);
		frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.skin=hboxLightBlueCursor;
		frmIBAnyIDRegistration.hbxMobileAccountDetails.skin=hboxLightBlueCursor;
	}	
}

function onSelectToAccountForAnyIDIB(){
	if(clickFromCitizenAnyID){
		gblSelectedAnyIDCitizenAccount = frmIBAnyIDRegistration.segAccountList.data[frmIBAnyIDRegistration.segAccountList.selectedIndex[1]];
		frmIBAnyIDRegistration.lblCitizenidAccountName.text=gblSelectedAnyIDCitizenAccount["lblAccountName"];
		frmIBAnyIDRegistration.lblCitizenidAccountNumber.text=gblSelectedAnyIDCitizenAccount["lblAccountNumber"];
		frmIBAnyIDRegistration.imgCitizenidAccountSymbol.src=gblSelectedAnyIDCitizenAccount["imgAccountType"];
		frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.setVisibility(true);
		frmIBAnyIDRegistration.btnCheckboxCitizenId.skin=btnCheckFocTransparent;
		localCIAnyID=true;
		showNextORAgreeAnyIDIB();
	}else{
		gblSelectedAnyIDMobileAccount = frmIBAnyIDRegistration.segAccountList.data[frmIBAnyIDRegistration.segAccountList.selectedIndex[1]];
		frmIBAnyIDRegistration.lblMobileAccountName.text=gblSelectedAnyIDMobileAccount["lblAccountName"];
		frmIBAnyIDRegistration.lblMobileAccountNumber.text=gblSelectedAnyIDMobileAccount["lblAccountNumber"];
		frmIBAnyIDRegistration.imgMobileAccountSymbol.src=gblSelectedAnyIDMobileAccount["imgAccountType"];
		frmIBAnyIDRegistration.hbxMobileAccountDetails.setVisibility(true);
		frmIBAnyIDRegistration.btnCheckboxMobile.skin=btnCheckFocTransparent;
		localMobileAnyID=true;
		showNextORAgreeAnyIDIB();
	}
	frmIBAnyIDRegistration.hbxwaterWheel.setVisibility(true);
	frmIBAnyIDRegistration.hbxAccountList.setVisibility(false);
	//checkNextEnableFromRewardsLandingIB();
}

function showNextORAgreeAnyIDIB(){
	showAgree=false;
	if(!gblCIAnyID && localCIAnyID)showAgree=true;
	if(!gblMobileAnyID && localMobileAnyID)showAgree=true;
	
	if(showAgree){
		frmIBAnyIDRegistration.hbxTermsAndConditionsLink.setVisibility(true);
		frmIBAnyIDRegistration.btnNext.text=kony.i18n.getLocalizedString("keyAgreeButton");
		frmIBAnyIDRegistrationComplete.lblAnyIdSuccess.setVisibility(true);
		frmIBAnyIDRegistrationComplete.lblAnyIdSuccessEdit.setVisibility(false);
	}else{	
		frmIBAnyIDRegistration.hbxTermsAndConditionsLink.setVisibility(false);
		frmIBAnyIDRegistration.btnNext.text=kony.i18n.getLocalizedString("Next");
		frmIBAnyIDRegistrationComplete.lblAnyIdSuccess.setVisibility(false);
		frmIBAnyIDRegistrationComplete.lblAnyIdSuccessEdit.setVisibility(true);
	}
	enableNextButtonAnyIDRegistration();//enable next
}

function enableNextButtonAnyIDRegistration(){
	var enableNext=false;
	if(gblCIAnyID != localCIAnyID)enableNext=true;
	if(gblMobileAnyID != localMobileAnyID)enableNext=true;
	if(isNotBlankObject(gblSelectedAnyIDMobileAccount)){
		if(gblSelectedAnyIDMobileAccount["lblAccountNumber"] != gblMobileAnyIDRegisteredAccount["lblAccountNumber"])enableNext=true;
	}
	if(isNotBlankObject(gblSelectedAnyIDCitizenAccount)){
		if(gblSelectedAnyIDCitizenAccount["lblAccountNumber"] != gblCIAnyIDRegisteredAccount["lblAccountNumber"])enableNext=true;
	}
	frmIBAnyIDRegistration.btnNext.setEnabled(enableNext);
	if(enableNext)
		frmIBAnyIDRegistration.btnNext.skin=btnIB158;
	else
		frmIBAnyIDRegistration.btnNext.skin=btnIB158disabled;	
}

function myProfileAnyIDLinkShowHideIB(){
	// method is called in postshow of following forms - frmIBCMMyProfile,frmIBCMEditMyProfile,frmIBCMChgPwd,frmIBCMConfirmationPwd,frmIBCMChngUserID,frmIBCMConfirmation,frmIBCMChgTxnLimit,frmIBCMChgMobNoTxnLimit
	var showInProfile = true;
	if(gblShowAnyIDRegistration == "false"){
		showInProfile=false;
	}
	var myProfileForms = kony.application.getCurrentForm();
	myProfileForms.hbxAnyID.setVisibility(showInProfile);
	myProfileForms.lineAnyID.setVisibility(showInProfile);
}

function onClickAnyIDRegistrationNext(){
	anyIDPageResetIB();
	// register / de-register and verify otp service to be called in composite
	popupIBAnyIDDeRegistration.lblAnyIDDeRegMesg.text="";
	
	if(gblMobileAnyID && !localMobileAnyID)
		popupIBAnyIDDeRegistration.lblAnyIDDeRegMesg.text = kony.i18n.getLocalizedString("MIB_P2PDelPop"); // Mobile
	
	if(gblCIAnyID && !localCIAnyID)
		popupIBAnyIDDeRegistration.lblAnyIDDeRegMesg.text = kony.i18n.getLocalizedString("MIB_P2PDelPop"); // CI
	
	if(gblCIAnyID && gblMobileAnyID && !localCIAnyID && !localMobileAnyID)
		popupIBAnyIDDeRegistration.lblAnyIDDeRegMesg.text = kony.i18n.getLocalizedString("MIB_P2PDelPop"); // both

	if("" == popupIBAnyIDDeRegistration.lblAnyIDDeRegMesg.text){
		checkOtpTokenRequiredAnyID();	//No De registration is happening so need this popup
	}
	else{
		popupIBAnyIDDeRegistration.show();
	}	
}

function checkOtpTokenRequiredAnyID(){
	otpRequired=false;
	//check If anything has been enabled or modified - call OTP service.
	if(gblMobileAnyID && localMobileAnyID){
		if(isNotBlankObject(gblSelectedAnyIDMobileAccount)){
			if(gblSelectedAnyIDMobileAccount["lblAccountNumber"] != gblMobileAnyIDRegisteredAccount["lblAccountNumber"])
				otpRequired=true;
		}
	}else if(!gblMobileAnyID && localMobileAnyID){
		otpRequired=true;
	}
	if(gblCIAnyID && localCIAnyID){
		if(isNotBlankObject(gblSelectedAnyIDCitizenAccount)){
			if(gblSelectedAnyIDCitizenAccount["lblAccountNumber"] != gblCIAnyIDRegisteredAccount["lblAccountNumber"])
				otpRequired=true;
			}
	}else if(!gblCIAnyID && localCIAnyID){
		otpRequired=true;
	}
	popupIBAnyIDDeRegistration.dismiss();
	if(otpRequired)
		saveAnyIDSessionIB();// temporary call to allow OTP request to happen
	else
		anyIDRegDeRegService();
}

function anyIDRegDeRegService(){
	var otpText="";
	if(otpRequired){
		otpText=frmIBAnyIDRegistration.txtBxOTP.text;
		if(!isNotBlank(otpText)){
			showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
	showLoadingScreenPopup();
	var inputParam = {};
	var anyIDMBActNum = frmIBAnyIDRegistration.lblMobileAccountNumber.text;
	var anyIDCIActNum = frmIBAnyIDRegistration.lblCitizenidAccountNumber.text;
	
	inputParam["password"] = otpText;
    inputParam["notificationAdd_appID"] = appConfig.appId;
   
    //For Mobile Registration
    inputParam["mobileActNum"] = anyIDMBActNum.replace(/\-/g, "");
    
    //for CI Registration
    inputParam["CIActNum"] = anyIDCIActNum.replace(/\-/g, "");
    
    invokeServiceSecureAsync("anyIDRegisterComposite", inputParam, callBackanyIDRegDeRegServiceIB);
}
function callBackanyIDRegDeRegServiceIB(status,resulttable){
	if(status == 400){
		dismissLoadingScreenPopup();
		frmIBAnyIDRegistration.txtBxOTP.text="";
		abc=resulttable;
		if(resulttable["opstatus"] == 0){
			//businesshours check
        	if(resulttable["registerAnyIdBusinessHrsFlag"] == "false")
			{
				var startTime = resulttable["regAnyIdStartTime"];
         		var endTime = resulttable["regAnyIdEndTime"];
         		var messageUnavailable = kony.i18n.getLocalizedString("keySoGooODServiceUnavailable");
         		messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
         		messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
               	showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
               	return false;
           	}
           	
           	//for registration
			var CIRegisterResult="", MobileRegisterResult="", isSuccess=false;
			CIRegisterResult = resulttable["opstatusCIRegistration"];
			MobileRegisterResult = resulttable["opstatusMobileRegistration"];
			if(undefined != CIRegisterResult && undefined != MobileRegisterResult){
				if(CIRegisterResult == "0" && MobileRegisterResult == "0"){
					isSuccess=true;
				}
			}else if(undefined != CIRegisterResult){
				if(CIRegisterResult == "0"){
					isSuccess = true;
				}
			}else if(undefined != MobileRegisterResult){
				if(MobileRegisterResult == "0"){
					isSuccess = true;
				}
			}
			
			//for de-registration
			var CIDeRegisterResult="", MobileDeRegisterResult="";
	    	CIDeRegisterResult = resulttable["opstatusCIDeRegistration"];
			MobileDeRegisterResult = resulttable["opstatusMobileDeRegistration"];
			if(undefined != CIDeRegisterResult && undefined != MobileDeRegisterResult){
				if(CIDeRegisterResult == "0" && MobileDeRegisterResult == "0"){
					isSuccess=true;
				}
			}else if(undefined != CIDeRegisterResult){
				if(CIDeRegisterResult == "0"){
					isSuccess = true;
				}
			}else if(undefined != MobileDeRegisterResult){
				if(MobileDeRegisterResult == "0"){
					isSuccess = true;
				}
			}
			
			//for Modify account
	    	var CIDeModifyResult="", MobileModifyResult="";
	    	CIDeModifyResult = resulttable["opstatusCIActModify"];
			MobileModifyResult = resulttable["opstatusMobileActModify"];
			if(undefined != CIDeModifyResult && undefined != MobileModifyResult){
				if(CIDeModifyResult == "0" && MobileModifyResult == "0"){
					isSuccess=true;
				}
			}else if(undefined != CIDeModifyResult){
				if(CIDeModifyResult == "0"){
					isSuccess = true;
				}
			}else if(undefined != MobileModifyResult){
				if(MobileModifyResult == "0"){
					isSuccess = true;
				}
			}	
			
			if(isSuccess){
				frmIBAnyIDRegistrationComplete.lblAnyIdFailure.setVisibility(false);
				frmIBAnyIDRegistrationComplete.hbxSuccess.setVisibility(true);
				frmIBAnyIDRegistrationComplete.hbxFail.setVisibility(false);
				if(frmIBAnyIDRegistrationComplete.lblAnyIdSuccess.isVisible){
					//Based on response value of "regAnyIdWithITMX" we have to hide show the below 3 success messages.
					frmIBAnyIDRegistrationComplete.lblAnyIdSuccess.setVisibility(false);   // after Launch with ITMX
					frmIBAnyIDRegistrationComplete.lblAnyIdSuccess1stLaunch.setVisibility(false); // something Temp
					frmIBAnyIDRegistrationComplete.lblAnyIdSuccess2ndLaunch.setVisibility(false); // for Main Launch before ITMX
					var regAnyIdWithITMX = resulttable["regAnyIdWithITMX"]!=undefined?resulttable["regAnyIdWithITMX"]:"";
					regAnyIdWithITMX = regAnyIdWithITMX.toUpperCase();
					if("OFF" == regAnyIdWithITMX){
						frmIBAnyIDRegistrationComplete.lblAnyIdSuccess2ndLaunch.setVisibility(true);
					}else if("ON" == regAnyIdWithITMX){
						frmIBAnyIDRegistrationComplete.lblAnyIdSuccess.setVisibility(true);
					}else{
						frmIBAnyIDRegistrationComplete.lblAnyIdSuccess1stLaunch.setVisibility(true);
					}
				}else{
					frmIBAnyIDRegistrationComplete.lblAnyIdSuccess.setVisibility(false);
					frmIBAnyIDRegistrationComplete.lblAnyIdSuccess1stLaunch.setVisibility(false);
					frmIBAnyIDRegistrationComplete.lblAnyIdSuccess2ndLaunch.setVisibility(false);
				}
				frmIBAnyIDRegistrationComplete.show();
			}else{
				frmIBAnyIDRegistrationComplete.hbxSuccess.setVisibility(false);
				frmIBAnyIDRegistrationComplete.hbxFail.setVisibility(true);
				frmIBAnyIDRegistrationComplete.lblAnyIdFailure.setVisibility(true);
				frmIBAnyIDRegistrationComplete.lblAnyIdSuccess.setVisibility(false);
				frmIBAnyIDRegistrationComplete.lblAnyIdSuccess1stLaunch.setVisibility(false);
				frmIBAnyIDRegistrationComplete.lblAnyIdSuccess2ndLaunch.setVisibility(false);
				frmIBAnyIDRegistrationComplete.lblAnyIdSuccessEdit.setVisibility(false);
				var messageUnavailable = kony.i18n.getLocalizedString("keyOpenActGenErr");
				var registerCIITMXCode = (undefined != resulttable["registerCIITMXCode"])?resulttable["registerCIITMXCode"]:"";
				var registerMobileITMXCode = (undefined != resulttable["registerMobileITMXCode"])?resulttable["registerMobileITMXCode"]:"";
				if(registerMobileITMXCode == "800" && registerCIITMXCode == "800")
					messageUnavailable = kony.i18n.getLocalizedString("MIB_P2PErr_RejBoth");
				else if(registerMobileITMXCode == "800")
						messageUnavailable = kony.i18n.getLocalizedString("MIB_P2PErr_RejMobile");
				else if(registerCIITMXCode == "800")
						messageUnavailable = kony.i18n.getLocalizedString("MIB_P2PErr_RejCI");
				
				showAlertWithCallBack(messageUnavailable, kony.i18n.getLocalizedString("info"),function(){frmIBAnyIDRegistrationComplete.show();});
			}
		}else if (resulttable["opstatus"] == 8005) {
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                frmIBAnyIDRegistration.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone")+kony.i18n.getLocalizedString("invalidotptwo");
                //frmIBAnyIDRegistration.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                frmIBAnyIDRegistration.lblOTPinCurr.setVisibility(true);
				frmIBAnyIDRegistration.lblPlsReEnter.setVisibility(false);
				frmIBAnyIDRegistration.txtBxOTP.text="";
                //alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                handleOTPLockedIB(resulttable);
                //startRcCrmUpdateProfilBPIB("04");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMsg"]);
                return false;
            }
            else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
              }
            else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        }
	}	
}

function saveAnyIDSessionIB(){
	showLoadingScreenPopup();
	var inputParam = {};
	//params to be saved in session
	var anyIDMBActNum = frmIBAnyIDRegistration.lblMobileAccountNumber.text;
	var anyIDCIActNum = frmIBAnyIDRegistration.lblCitizenidAccountNumber.text;
    //For Mobile Registration
    inputParam["mobileActNum"] = anyIDMBActNum.replace(/\-/g, "");
    //for CI Registration
    inputParam["CIActNum"] = anyIDCIActNum.replace(/\-/g, "");
    invokeServiceSecureAsync("saveAnyIDRegistrationSession", inputParam, saveAnyIDSessionIBCallbackfunction);
}

function saveAnyIDSessionIBCallbackfunction(status, resulttable){
	if (status == 400) {
		dismissLoadingScreenPopup();
		if (resulttable["opstatus"] == "0") {
		
			var mobileAnyIDAccountInqStatusCode = resulttable["mobileAnyIDAccountInqStatusCode"];
			var CIAnyIDAccountInqStatusCode = resulttable["CIAnyIDAccountInqStatusCode"];
			
			if((mobileAnyIDAccountInqStatusCode=="" || mobileAnyIDAccountInqStatusCode=="0") && (CIAnyIDAccountInqStatusCode=="" || CIAnyIDAccountInqStatusCode == "0")){
				IBGenerateOTPAnyID();
			}else{
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			}	
		}
		else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}	
}

function IBGenerateOTPAnyID()
{
		showLoadingScreenPopup();
		frmIBAnyIDRegistration.lblOTPinCurr.text = "";
        frmIBAnyIDRegistration.lblPlsReEnter.text = "";
        frmIBAnyIDRegistration.hbxOtpBox.setVisibility(true);
		frmIBAnyIDRegistration.hbxTokenEntry.setVisibility(false);
		frmIBAnyIDRegistration.lblOTPinCurr.setVisibility(false);
		frmIBAnyIDRegistration.lblPlsReEnter.setVisibility(false);
		frmIBAnyIDRegistration.hbxOTPEntry.setVisibility(true);
		frmIBAnyIDRegistration.hbxOTPRef.setVisibility(true);
		frmIBAnyIDRegistration.hbxOTPsnt.setVisibility(true);
		frmIBAnyIDRegistration.txtBxOTP.setFocus(true);
		var inputParams = {};
		inputParams["Channel"] = "anyIDRegistration";
		inputParams["retryCounterRequestOTP"]=gblRetryCountRequestOTP;
	 	inputParams["locale"]=kony.i18n.getCurrentLocale();
	 
	 	//InputParms for activity logging
		inputParams["channelId"] = GLOBAL_IB_CHANNEL;
		inputParams["logLinkageId"] = "";
		inputParams["deviceNickName"] = "";
		inputParams["activityFlexValues2"] = "";
		invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackIBReqOTPIBAnyID);
}

function callBackIBReqOTPIBAnyID(status, callBackResponse) {
	if (status == 400) {
			if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
				//alert("Test1");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				//alert("Test2");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
              }
			
			else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}
			if (callBackResponse["opstatus"] == 0) {
			
			//Resetting the OTP screen
			frmIBAnyIDRegistration.hbxOTPEntry.setVisibility(true);
		    frmIBAnyIDRegistration.hbxOTPsnt.setVisibility(true);
		    frmIBAnyIDRegistration.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		    frmIBAnyIDRegistration.hbxOTPRef.isVisible=true;
		    frmIBAnyIDRegistration.hbxTokenEntry.setVisibility(false);
		    gblTokenSwitchFlag=false;
		    dismissLoadingScreenPopup();
		    //frmIBAnyIDRegistration.tbxToken.text ="";
		    //frmIBAnyIDRegistration.tbxToken.setFocus(true);
    		//Finish reset here

			//gblOTPdisabletime = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]) * 100;
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			kony.timer.schedule("OtpTimerAnyID", IBOTPTimercallbackAnyID, reqOtpTimer, false);
			   
        	    	
          	gblOTPReqLimit=gblOTPReqLimit+1;

			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
				}
			}
			gblestmt="verOTP";	 			
	   		frmIBAnyIDRegistration.btnOTPReq.skin = btnIBREQotp;
        	frmIBAnyIDRegistration.btnOTPReq.focusSkin = btnIBREQotp;
        	frmIBAnyIDRegistration.btnOTPReq.onClick = ""; 
        	frmIBAnyIDRegistration.lblBankRefVal.text = refVal;			
			//curr_form = kony.application.getCurrentForm();
			frmIBAnyIDRegistration.txtBxOTP.maxTextLength = gblOTPLENGTH;
			frmIBAnyIDRegistration.txtBxOTP.text = "";
			frmIBAnyIDRegistration.lblOTPMblNum.text =" " + maskingIB(gblPHONENUMBER);
			//can not click other areas when OTP screen is shown.
			enableDisableAnyIDAllClickableOnOTP(false);
			dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			alert("Error " + callBackResponse["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("Error");
		}
	}
}

function IBOTPTimercallbackAnyID(){
	frmIBAnyIDRegistration.btnOTPReq.skin = btnIBREQotpFocus;
	frmIBAnyIDRegistration.btnOTPReq.focusSkin = btnIBREQotpFocus;
	frmIBAnyIDRegistration.btnOTPReq.onClick = IBGenerateOTPAnyID;
	
	try {
		kony.timer.cancel("OtpTimerAnyID");
	} catch (e) {
		
	}
}

function enableDisableAnyIDAllClickableOnOTP(clickFlag){
	frmIBAnyIDRegistration.btnCheckboxMobile.setEnabled(clickFlag);
	frmIBAnyIDRegistration.btnCheckboxCitizenId.setEnabled(clickFlag);
	frmIBAnyIDRegistration.btnEditMobileNumber.setEnabled(clickFlag);
	frmIBAnyIDRegistration.hbxCitizenIdAccountDetails.setEnabled(clickFlag);
	frmIBAnyIDRegistration.hbxMobileAccountDetails.setEnabled(clickFlag);
	frmIBAnyIDRegistration.hbxTermsAndConditionsLink.setEnabled(clickFlag);
}

function settingAnyIdAnnoucementPageKeys(){
	frmIBAnyIdAnnoucment.btnCancels.text = kony.i18n.getLocalizedString("MIB_AnyIDLater");
 	frmIBAnyIdAnnoucment.btnSettings.text = kony.i18n.getLocalizedString("MIB_AnyIDNow");
	
	var anyIDAnnouncement_image_name_EN = "webAnyID_N_Eng_984x380pixel";
	var	anyIDAnnouncement_image_name_TH = "webAnyID_N_Thai_984x380pixel";
	var randomnum = Math.floor((Math.random() * 10000) + 1);
	if(kony.i18n.getCurrentLocale() == "en_US"){
 		//frmIBAnyIdAnnoucment.imgAnydAnnouPage.src = "tmb_logo_blue_new1.png";
		var anyIDAnnouncement_image_EN = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext 
									+ "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+anyIDAnnouncement_image_name_EN
									+"&modIdentifier=PRODUCTPACKAGEIMG&dummy=" + randomnum;
		frmIBAnyIdAnnoucment.imgAnydAnnouPage.src = anyIDAnnouncement_image_EN;
											
	}else {
		var anyIDAnnouncement_image_TH = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext 
									+ "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+anyIDAnnouncement_image_name_TH
									+"&modIdentifier=PRODUCTPACKAGEIMG&dummy=" + randomnum;
		frmIBAnyIdAnnoucment.imgAnydAnnouPage.src = anyIDAnnouncement_image_TH;
		
	}
	
}

