/**************************************************************************************
		Module	: 
		Author  : Kony
		Date    : June 13, 2013
		Purpose : 
*****************************************************************************************/
function validateIBChangeUserId() {
    var emptyFieldCheck = false;
    if (frmIBCMChngUserID.txtCurUserID.text != "")
        if (frmIBCMChngUserID.txtNewUserID.text != "")
            if (!(kony.string.containsChars(frmIBCMChngUserID.txtNewUserID.text, ["<"]) && kony.string.containsChars(frmIBCMChngUserID.txtNewUserID.text, [">"])))
                if (frmIBCMChngUserID.hboxCaptcha.isVisible)
                    if (frmIBCMChngUserID.txtCaptchaText.text != "") emptyFieldCheck = true;
                    else {
                        emptyFieldCheck = false;
                        frmIBCMChngUserID.txtCaptchaText.setFocus(true);
                    }
    else emptyFieldCheck = true;
    else {
        alert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"));
        frmIBCMChngUserID.txtNewUserID.setFocus(true);
        frmIBCMChngUserID.txtNewUserID.text = "";
        return;
    } else {
        emptyFieldCheck = false;
        frmIBCMChngUserID.txtNewUserID.setFocus(true);
    } else {
        emptyFieldCheck = false;
        frmIBCMChngUserID.txtCurUserID.setFocus(true);
    }
    if (emptyFieldCheck) {
        temp = kony.i18n.getLocalizedString("AccesPIN");
        var currentuser = frmIBCMChngUserID.txtCurUserID.text
        var newuser = frmIBCMChngUserID.txtNewUserID.text
        var currentpwd = frmIBCMChgPwd.tbxTranscCrntPwd.text
        var newpwd = frmIBCMChgPwd.txtPassword.text
            //Doubt whether this check is of any use at all
        if (currentuser == currentpwd || currentuser == newpwd || newuser == newpwd || newuser == currentpwd) {
            alert("User ID and Password should not be the same")
            return false;
        }
        //start
        var inputParams = {}
        inputParams["segmentId"] = "MIB";
        inputParams["userId"] = newuser;
        inputParams["currentUserId"] = currentuser;
        inputParams["captchaID"] = frmIBCMChngUserID.txtCaptchaText.text;
        showLoadingScreenPopup();
        invokeServiceSecureAsync("findUserByIdInSegment", inputParams, callBackfindUserByIdInSegment);
        //end
    }
    if (!emptyFieldCheck) {
        alert(kony.i18n.getLocalizedString("keyMandField"));
    }
}

function callBackfindUserByIdInSegment(status, resultable) {
    if (status == 400) {
        if (resultable["opstatus"] == 8005) {
            frmIBCMChngUserID.txtCaptchaText.text = "";
            if (resultable["code"] == "30001") {
                //var curr_user = resultable["currentUserId"];
                gblRetryCountRequestOTP = 0;
                frmIBCMConfirmation.lblIncorrectOtpMsg.text = "";
                gblOTPFlag = true;
                //requestOTPForChngUserId();
                //onMyProfileUserIdNextIB();
                var oldUserIdIBChange = frmIBCMChngUserID.txtCurUserID.text;
                var newUserIdIBChange = frmIBCMChngUserID.txtNewUserID.text;
                invokeSaveparamInSessionForUserChangeIB(oldUserIdIBChange, newUserIdIBChange, "1");
            } else if (resultable["errorReason"] != null) {
                if (resultable["errorReason"] == "existingUserID") {
                    alert(kony.i18n.getLocalizedString("existingUserID"));
                } else {
                    alert(kony.i18n.getLocalizedString("keyRepeatCharErr"));
                }
                if (resultable["captchaCode"] == "1") {
                    frmIBCMChngUserID.hboxCaptcha.setVisibility(true);
                    frmIBCMChngUserID.hboxCaptchaText.setVisibility(true);
                    frmIBCMChngUserID.imgcaptcha.base64 = resultable["captchaImage"];
                    frmIBCMChngUserID.txtCaptchaText.text = "";
                } else {
                    frmIBCMChngUserID.hboxCaptcha.setVisibility(false);
                    frmIBCMChngUserID.hboxCaptchaText.setVisibility(false);
                    //frmIBCMChngUserID.imgcaptcha.base64 = resultable["captchaImage"];
                    frmIBCMChngUserID.txtCaptchaText.text = "";
                }
                dismissLoadingScreenPopup();
            } else if (resultable["errorKey"] != undefined) {
                if (resultable["errorKey"] == "keyWrongCurrentUserId") {
                    alert(kony.i18n.getLocalizedString("keyWrongCurrentUserId"));
                } else {
                    alert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"));
                }
                if (resultable["captchaCode"] == "1") {
                    frmIBCMChngUserID.hboxCaptcha.setVisibility(true);
                    frmIBCMChngUserID.hboxCaptchaText.setVisibility(true);
                    frmIBCMChngUserID.imgcaptcha.base64 = resultable["captchaImage"];
                    frmIBCMChngUserID.txtCaptchaText.text = "";
                } else {
                    frmIBCMChngUserID.hboxCaptcha.setVisibility(false);
                    frmIBCMChngUserID.hboxCaptchaText.setVisibility(false);
                    //frmIBCMChngUserID.imgcaptcha.base64 = resultable["captchaImage"];
                    frmIBCMChngUserID.txtCaptchaText.text = "";
                }
                dismissLoadingScreenPopup();
            } else {
                dismissLoadingScreenPopup();
                alert("Error " + resultable["errMsg"]);
            }
        } else if (resultable["opstatus"] == 0) {
            alert(kony.i18n.getLocalizedString("existingUserID"));
            dismissLoadingScreenPopup();
        }
    } else dismissLoadingScreenPopup();
}
/**************************************************************************************
		Module	: 
		Author  : Kony
		Date    : June 13, 2013
		Purpose : 
*****************************************************************************************/
function requestOTPForChngUserId() {
    frmIBCMConfirmation.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBCMConfirmation.lblPlsReEnter.text = " ";
    frmIBCMConfirmation.hbxOTPincurrect.isVisible = false;
    frmIBCMConfirmation.hbox476047582127699.isVisible = true;
    frmIBCMConfirmation.hbox476047582115275.isVisible = true;
    if (gblOTPFlag) {
        var inputParams = {}
        inputParams["Channel"] = "ChangeUserId";
        inputParams["locale"] = kony.i18n.getCurrentLocale();
        inputParams["retryCounterRequestOTP"] = gblIBRetryCountRequestOTP["chngIBUserId"];
        //invokeServiceSecureAsync("generateOTP", inputParams, callBackChangeIBUserID);
        invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackChangeIBUserID);
    }
}
/**************************************************************************************
		Module	: 
		Author  : Kony
		Date    : June 13, 2013
		Purpose : 
*****************************************************************************************/
function callBackChangeIBUserID(status, resultable) {
    if (status == 400) {
        if (resultable["errCode"] == "GenOTPRtyErr00002") {
            dismissLoadingScreenPopup();
            gblOTPFlag = false;
            frmIBCMConfirmation.btnReqOTP.skin = btnIBgreyInactive;
            frmIBCMConfirmation.btnReqOTP.setEnabled(false);
            kony.timer.cancel("ProfileOTPTimer");
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (resultable["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            //gblOTPFlag = false;	
            //frmIBCMConfirmation.btnReqOTP.skin = btnIBgreyInactive;
            //				frmIBCMConfirmation.btnReqOTP.setEnabled(false);
            //				kony.timer.cancel("ProfileOTPTimer");
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (resultable["errCode"] == "JavaErr00001") {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (resultable["opstatus"] == 0) {
            var reqOtpTimer = kony.os.toNumber(resultable["requestOTPEnableTime"]);
            //gblRetryCountRequestOTP = kony.os.toNumber(resultable["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(resultable["otpLength"]);
            gblIBRetryCountRequestOTP["chngIBUserId"] = resultable["retryCounterRequestOTP"];
            dismissLoadingScreenPopup();
            frmIBCMConfirmation.lblCustName.text = frmIBCMChngUserID.lblCustName.text;
            frmIBCMConfirmation.lblEmailVal.text = frmIBCMChngUserID.lblEmailVal.text;
            frmIBCMConfirmation.lblno.text = frmIBCMChngUserID.lblnostudio11.text; //Modified by Studio Viz
            frmIBCMConfirmation.lblNewUserID.text = resultable["newUserId"];
            frmIBCMConfirmation.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg"); //added
            frmIBCMConfirmation.lblPhoneNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            frmIBCMConfirmation.lblBankRef.text = kony.i18n.getLocalizedString("BankRef.No"); //Added
            var refVal = "";
            for (var d = 0; d < resultable["Collection1"].length; d++) {
                if (resultable["Collection1"][d]["keyName"] == "pac") {
                    refVal = resultable["Collection1"][d]["ValueString"];
                    break;
                }
            }
            frmIBCMConfirmation.lblBankRefVal.text = refVal //resultable["bankRefNo"];
            frmIBCMConfirmation.txtOTP.maxTextLength = gblOTPLENGTH;
            frmIBCMConfirmation.txtOTP.text = "";
            //var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            kony.timer.schedule("ProfileOTPTimer", ProfleOTPTimerCallBack, reqOtpTimer, false)
            frmIBCMConfirmation.btnReqOTP.skin = btnIBgreyInactive;
            frmIBCMConfirmation.btnReqOTP.setEnabled(false);
            frmIBCMMyProfile.imgCompletion.setEnabled(true);
            frmIBCMConfirmation.txtOTP.setFocus(true);
            gblOTPFlag = false;
            frmIBCMConfirmation.show();
        } else {
            dismissLoadingScreenPopup();
            gblIBRetryCountRequestOTP["chngIBUserId"] = resultable["retryCounterRequestOTP"];
            alert("Error " + resultable["errMsg"]);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            gblIBRetryCountRequestOTP["chngIBUserId"] = resultable["retryCounterRequestOTP"];
            alert("Error");
        }
    }
}

function ProfleOTPTimerCallBack() {
    frmIBCMConfirmation.btnReqOTP.skin = "btnIBgreyActive";
    frmIBCMConfirmation.btnReqOTP.setEnabled(true);
    frmIBCMConfirmation.btnReqOTP.onClick = requestOTPForChngUserId;
    gblOTPFlag = true;
    try {
        kony.timer.cancel("ProfileOTPTimer")
    } catch (e) {}
}

function onMyProfileUserIdNextIB() {
    var currForm = kony.application.getCurrentForm();
    if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
        checkMyProfileUserIdTokenFlag();
    }
    var oldUserIdIBChange = frmIBCMChngUserID.txtCurUserID.text;
    var newUserIdIBChange = frmIBCMChngUserID.txtNewUserID.text;
    //invokeSaveparamInSessionForPwdUserChange(oldUserIdIBChange,newUserIdIBChange,"1");
    if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        frmIBCMConfirmation.hbxTokenEntry.setVisibility(true);
        frmIBCMConfirmation.tbxToken.setFocus(true);
        frmIBCMConfirmation.hbxOTPEntry.setVisibility(false);
        frmIBCMConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
        //frmIBCMConfirmation.label476047582115277.setVisibility(false);
        //frmIBCMConfirmation.lblPhoneNo.setVisibility(false);
        frmIBCMConfirmation.hbox476047582115275.setVisibility(false);
        //frmIBCMConfirmation.lblBankRef.setVisibility(false);
        frmIBCMConfirmation.hbox476047582127699.setVisibility(false);
        frmIBCMConfirmation.lblCustName.text = frmIBCMChngUserID.lblCustName.text;
        frmIBCMConfirmation.lblEmailVal.text = frmIBCMChngUserID.lblEmailVal.text;
        frmIBCMConfirmation.lblno.text = frmIBCMChngUserID.lblnostudio11.text; //Modified by Studio Viz
        frmIBCMConfirmation.lblNewUserID.text = frmIBCMChngUserID.txtNewUserID.text
        frmIBCMConfirmation.hbxOTPincurrect.setVisibility(false);
        frmIBCMConfirmation.show();
    } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
        frmIBCMConfirmation.hbxTokenEntry.setVisibility(false);
        frmIBCMConfirmation.hbxOTPEntry.setVisibility(true);
        //	frmIBCMConfirmation.label476047582115277.setVisibility(true);
        //	frmIBCMConfirmation.lblPhoneNo.setVisibility(true);
        frmIBCMConfirmation.hbox476047582115275.setVisibility(true);
        //frmIBCMConfirmation.lblBankRef.setVisibility(true);
        frmIBCMConfirmation.hbox476047582127699.setVisibility(true);
        frmIBCMConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        requestOTPForChngUserId();
    }
}

function checkMyProfileUserIdTokenFlag() {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, ibMyProfileUserIdTokenFlagCallbackfunction);
}

function ibMyProfileUserIdTokenFlagCallbackfunction(status, callbackResponse) {
    var currForm = kony.application.getCurrentForm().id;
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            if (callbackResponse["deviceFlag"].length == 0) {
                //return
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus == '02') {
                    dismissLoadingScreenPopup();
                    frmIBCMConfirmation.hbxTokenEntry.setVisibility(true);
                    frmIBCMConfirmation.tbxToken.setFocus(true);
                    frmIBCMConfirmation.hbxOTPEntry.setVisibility(false);
                    frmIBCMConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
                    gblTokenSwitchFlag = true;
                    frmIBCMConfirmation.label476047582115277.setVisibility(false);
                    frmIBCMConfirmation.lblPhoneNo.setVisibility(false);
                    frmIBCMConfirmation.lblBankRef.setVisibility(false);
                    frmIBCMConfirmation.lblCustName.text = frmIBCMChngUserID.lblCustName.text;
                    frmIBCMConfirmation.lblEmailVal.text = frmIBCMChngUserID.lblEmailVal.text;
                    frmIBCMConfirmation.lblno.text = frmIBCMChngUserID.lblnostudio11.text; //Modified by Studio Viz
                    frmIBCMConfirmation.lblNewUserID.text = frmIBCMChngUserID.txtNewUserID.text;
                    frmIBCMConfirmation.show();
                } else {
                    frmIBCMConfirmation.label476047582115277.setVisibility(true);
                    frmIBCMConfirmation.lblPhoneNo.setVisibility(true);
                    frmIBCMConfirmation.lblBankRef.setVisibility(true);
                    frmIBCMConfirmation.hbxTokenEntry.setVisibility(false);
                    frmIBCMConfirmation.hbxOTPEntry.setVisibility(true);
                    frmIBCMConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                    requestOTPForChngUserId();
                    gblTokenSwitchFlag = false;
                }
            } else {
                frmIBCMConfirmation.label476047582115277.setVisibility(true);
                frmIBCMConfirmation.lblPhoneNo.setVisibility(true);
                frmIBCMConfirmation.lblBankRef.setVisibility(true);
                frmIBCMConfirmation.hbxTokenEntry.setVisibility(false);
                frmIBCMConfirmation.hbxOTPEntry.setVisibility(true);
                frmIBCMConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                requestOTPForChngUserId();
            }
        } else dismissLoadingScreenPopup();
    } else dismissLoadingScreenPopup();
}

function changeUserIDComposite() {
    var inputParams = {}
        //  gblTokenSwitchFlag
        //TO pass input params for verifyToken
    if (gblTokenSwitchFlag == true) {
        var otpToken = frmIBCMConfirmation.tbxToken.text
        if (otpToken != null) otpToken = otpToken.trim();
        else {
            frmIBCMConfirmation.tbxToken.text = "";
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return;
        }
        if (otpToken == "") {
            frmIBCMConfirmation.tbxToken.text = "";
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return;
        }
        inputParams["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParams["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParams["password"] = frmIBCMConfirmation.tbxToken.text;
        inputParams["userId"] = frmIBCMChngUserID.txtCurUserID.text;
        inputParams["newId"] = frmIBCMChngUserID.txtNewUserID.text;
        inputParams["newName"] = frmIBCMChngUserID.txtNewUserID.text;
        inputParams["segmentIdVal"] = "MIB";
    } else {
        var otpText = frmIBCMConfirmation.txtOTP.text;
        if (otpText != null) otpText = otpText.trim();
        else {
            frmIBCMConfirmation.txtOTP.text = "";
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return;
        }
        if (otpText == "") {
            frmIBCMConfirmation.txtOTP.text = "";
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return;
        }
        inputParams["password"] = frmIBCMConfirmation.txtOTP.text;
        inputParams["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
    }
    // this flag determine otp or token flow
    inputParams["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
    inputParams["userId"] = frmIBCMChngUserID.txtCurUserID.text;
    inputParams["userId"] = frmIBCMChngUserID.txtCurUserID.text;
    inputParams["newId"] = frmIBCMChngUserID.txtNewUserID.text;
    inputParams["newName"] = frmIBCMChngUserID.txtNewUserID.text;
    inputParams["custName"] = customerNameIB;
    inputParams["Locale"] = kony.i18n.getCurrentLocale();
    var platformChannel = gblDeviceInfo.name;
    inputParams["activityFlexValues1"] = frmIBCMChngUserID.txtCurUserID.text;
    inputParams["activityFlexValues2"] = frmIBCMChngUserID.txtNewUserID.text;
    inputParams["ibUserStatusId"] = "02";
    inputParams["actionType"] = "00";
    inputParams["channel"] = "IB";
    showLoadingScreenPopup();
    // invoking call 
    invokeServiceSecureAsync("changeUserIDComposite", inputParams, callBackTMBcompositeChangeUserIDOTP);
}

function callBackTMBcompositeChangeUserIDOTP(status, resultable) {
    if (status == 400) {
        gblRetryCountRequestOTP = gblRetryCountRequestOTP + 1;
        frmIBCMConfirmation.lblIncorrectOtpMsg.text = "";
        frmIBCMConfirmation.txtOTP.text = "";
        frmIBCMConfirmation.tbxToken.text = "";
        if (resultable["opstatus_verifypwd"] == 0 || resultable["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            gblVerifyOTPCounter = "0";
            gblIBRetryCountRequestOTP["chngIBUserId"] = "0";
        } else if (resultable["opstatus_verifypwd"] != 0) {
            if (resultable["opstatus"] == 8005) {
                if (resultable["errCode"] == "VrfyOTPErr00001") {
                    gblRetryCountRequestOTP = resultable["retryCounterVerifyOTP"];
                    dismissLoadingScreenPopup();
                    //alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commented by swapna
                    frmIBCMConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone"); //kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBCMConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBCMConfirmation.hbxOTPincurrect.isVisible = true;
                    frmIBCMConfirmation.hbox476047582127699.isVisible = false;
                    frmIBCMConfirmation.hbox476047582115275.isVisible = false;
                    frmIBCMConfirmation.txtOTP.text = "";
                    frmIBCMConfirmation.tbxToken.text = "";
                    if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
                        frmIBCMConfirmation.tbxToken.setFocus(true);
                    } else frmIBCMConfirmation.txtOTP.setFocus(true);
                    return false;
                } else if (resultable["errCode"] == "VrfyOTPErr00002") {
                    dismissLoadingScreenPopup();
                    //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                    //startRcCrmUpdateProfilBPIB("04");
                    handleOTPLockedIB(resultable);
                    return false;
                } else if (resultable["errCode"] == "VrfyOTPErr00005") {
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("invalidOTP"));
                    return false;
                } else if (resultable["errCode"] == "VrfyOTPErr00006") {
                    gblVerifyOTPCounter = resultable["retryCounterVerifyOTP"];
                    dismissLoadingScreenPopup();
                    alert("" + resultable["errMsg"]);
                    return false;
                } else {
                    frmIBCMConfirmation.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBCMConfirmation.lblPlsReEnter.text = " ";
                    frmIBCMConfirmation.hbxOTPincurrect.isVisible = false;
                    frmIBCMConfirmation.hbox476047582127699.isVisible = true;
                    frmIBCMConfirmation.hbox476047582115275.isVisible = true;
                    dismissLoadingScreenPopup();
                    alert("" + resultable["errMsg"]);
                }
            } else if (resultable["code"] != null && resultable["code"] == "10403") {
                //popIBTransNowOTPLocked.show();
                //var inputParam = {}
                //inputParam["ibUserId"] = frmIBCMChngUserID.txtCurUserID.text;
                //inputParam["ibUserStatusId"] = "04";
                //inputParam["actionType"] = "32";
                dismissLoadingScreenPopup();
                frmIBCMConfirmation.txtOTP.text = "";
                gblVerifyOTPCounter = "0";
                handleOTPLockedIB(resultable);
                //gblIBRetryCountRequestOTP["chngIBUserId"] = "0";
                //showLoadingScreen();
                //invokeServiceSecureAsync("crmProfileMod", inputParam, callBackCrmProfileUpadateMB);
                frmIBCMConfirmation.btnReqOTP.skin = btnIBgreyInactive;
                frmIBCMConfirmation.btnReqOTP.setEnabled(false);
            } else if (resultable["code"] != null && resultable["code"] == "10020") {
                //frmIBCMConfirmation.lblIncorrectOtpMsg.text = kony.i18n.getLocalizedString("wrongOTP");
                frmIBCMConfirmation.txtOTP.text = "";
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
            } else {
                alert(resultable["errMsg"]);
                frmIBCMConfirmation.txtOTP.text = "";
            }
        }
        if (resultable["opstatus_changeuser"] == "0" || resultable["opstatus"] == 0) {
            successFlag = true;
            gblUserName = frmIBCMChngUserID.txtNewUserID.text;
            frmIBCMChngUserID.hboxCaptcha.setVisibility(false);
            frmIBCMChngUserID.hboxCaptchaText.setVisibility(false);
            frmIBCMChngUserID.txtCaptchaText.text = "";
            // showLoadingScreen();
            frmIBCMMyProfile.show();
            frmIBCMMyProfile.imgCompletion.setVisibility(true);
            frmIBCMMyProfile.lblupdatedprofile.setVisibility(true);
        } else if (resultable["opstatus_changeuser"] != "0" && resultable["code"] != null && resultable["code"] == "1000") {
            frmIBCMConfirmation.txtOTP.text = "";
            showAlert("" + kony.i18n.getLocalizedString("ECCreateUserExits"), null);
        }
    } else {
        if (status == 300) {
            alert("Error");
        }
    }
}

function invokeSaveparamInSessionForUserChangeIB(param1, param2, param3) {
    var inputParam = [];
    inputParam["param1"] = param1;
    inputParam["param2"] = param2;
    inputParam["param3"] = param3;
    invokeServiceSecureAsync("SaveParamsForChangePwdUserId", inputParam, callBackSaveparamInSessionIBUserChange);
}

function callBackSaveparamInSessionIBUserChange(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            onMyProfileUserIdNextIB();
        } else {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
        }
    }
}