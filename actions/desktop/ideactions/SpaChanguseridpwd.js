







    var personalizedID;
function validateSpaChangeUserId() {
    var emptyFieldCheck = false;
    if (frmCMChgAccessPin.txtCurUserID.text != "")
        if (frmCMChgAccessPin.txtNewUserID.text != "")
        	if(!(kony.string.containsChars(frmCMChgAccessPin.txtNewUserID.text, ["<"]) && 
			     kony.string.containsChars(frmCMChgAccessPin.txtNewUserID.text, [">"])))
            if (frmCMChgAccessPin.hboxCaptcha.isVisible)
                if (frmCMChgAccessPin.txtCaptchaText.text != "")
                    emptyFieldCheck = true;
                else {
                    emptyFieldCheck = false;
                    frmCMChgAccessPin.txtCaptchaText.setFocus(true);
                    alert("Field is Mandatory");
                } else
                    emptyFieldCheck = true;
                else{
					alert(kony.i18n.getLocalizedString("keyUserIdMinRequirement"));
					frmCMChgAccessPin.txtNewUserID.setFocus(true);
					frmCMChgAccessPin.txtNewUserID.text="";
					return;
				}
                else {
                    emptyFieldCheck = false;
                    frmCMChgAccessPin.txtNewUserID.setFocus(true);
                    alert("Field is Mandatory");
                } else {
                    emptyFieldCheck = false;
                    frmCMChgAccessPin.txtCurUserID.setFocus(true);
                    alert("Field is Mandatory");
                }
    if (emptyFieldCheck) {
        temp = kony.i18n.getLocalizedString("AccesPIN");
        var currentuser = frmCMChgAccessPin.txtCurUserID.text
        var newuser = frmCMChgAccessPin.txtNewUserID.text
        if(frmCMChgAccessPin.txtCaptchaText.isVisible==true)
        {
        	spaCaptchaisVisible=false;
        }
        else
        {
        	spaCaptchaisVisible=true;
        }
        var inputParams = {};
        inputParams["segmentId"] = "MIB";
        inputParams["userId"] = newuser;
        inputParams["currentUserId"] = currentuser;
        inputParams["captchaID"] = frmCMChgAccessPin.txtCaptchaText.text;
        showLoadingScreen();
        invokeServiceSecureAsync("findUserByIdInSegment", inputParams, callBackfindUserByIdInSegmentspa);
        if (!emptyFieldCheck) {
            alert("Field is mandatory");
        }
    }
}
    function callBackfindUserByIdInSegmentspa(status, resultable) {

        if (status == 400) {
            dismissLoadingScreen();
            if (resultable["opstatus"] == 8005) {
                if (resultable["code"] == "30001") {
                    
                    //var curr_user = resultable["currentUserId"];
                    gblOTPFlag = true;
                    gblRetryCountRequestOTP = 0;
                     var currentuser = frmCMChgAccessPin.txtCurUserID.text
       				 var newuser = frmCMChgAccessPin.txtNewUserID.text
                    invokeSaveparamInSessionForUserChangeSPA(currentuser,newuser,"1");
                   // requestOTPForChngUserIdspa();
                    //frmIBCMConfirmation.lblIncorrectOtpMsg.text = "";
                } else if (resultable["errorReason"] != null) {
                    if(resultable["errorReason"]="existingUserID")
					{
						alert(kony.i18n.getLocalizedString("existingUserID"));
					}
					else
					{
						alert(kony.i18n.getLocalizedString("keyRepeatCharErr"));
					}
                    if (resultable["captchaCode"] == "1") {
                    	spaCaptchaisVisible=true;
                        frmCMChgAccessPin.hboxCaptcha.setVisibility(true);
                        frmCMChgAccessPin.hboxCaptchaText.setVisibility(true);
                        frmCMChgAccessPin.imgcaptcha.base64 = resultable["captchaImage"];
                        frmCMChgAccessPin.txtCaptchaText.text = "";
                    } else {
                    	spaCaptchaisVisible=false;
                        frmCMChgAccessPin.hboxCaptcha.setVisibility(false);
                        frmCMChgAccessPin.hboxCaptchaText.setVisibility(false);
                        //frmIBCMChngUserID.imgcaptcha.base64 = resultable["captchaImage"];
                        frmCMChgAccessPin.txtCaptchaText.text = "";
                    }
                } else if (resultable["errorKey"] != undefined) {
                    if(resultable["errorKey"] == "keyCaptchaValueWorng"){
                    	alert(kony.i18n.getLocalizedString("keyCaptchaValueWorng"));
                    }
                    if(resultable["errorKey"] == "keySameUserIds"){
                    	alert(kony.i18n.getLocalizedString("keySameUserIds"));
                    }
                    if(resultable["errorKey"] == "keyUserIdUnderRestrictedList"){
                    	alert(kony.i18n.getLocalizedString("keyUserIdUnderRestrictedList"));
                    }
                    if(resultable["errorKey"] == "keyRepetativeChars"){
                    	alert(kony.i18n.getLocalizedString("keyRepetativeChars"));
                    }
                    if(resultable["errorKey"] == "keyWrongCurrentUserId"){
                    	alert(kony.i18n.getLocalizedString("keyWrongCurrentUserId"));
                    }
                    if(resultable["errorKey"] == "keyWrongNewUserId"){
                    	alert(kony.i18n.getLocalizedString("keyWrongNewUserId"));
                    }
                    
                    if (resultable["captchaCode"] == "1") {
                    	spaCaptchaisVisible=true;
                        frmCMChgAccessPin.hboxCaptcha.setVisibility(true);
                        frmCMChgAccessPin.hboxCaptchaText.setVisibility(true);
                        frmCMChgAccessPin.imgcaptcha.base64 = resultable["captchaImage"];
                        frmCMChgAccessPin.txtCaptchaText.text = "";
                    } else {
                    	spaCaptchaisVisible=false;
                        frmCMChgAccessPin.hboxCaptcha.setVisibility(false);
                        frmCMChgAccessPin.hboxCaptchaText.setVisibility(false);
                        //frmIBCMChngUserID.imgcaptcha.base64 = resultable["captchaImage"];
                        frmCMChgAccessPin.txtCaptchaText.text = "";
                    }

                } else {
                    dismissLoadingScreen();
                    alert("Error " + resultable["errMsg"]);
                }
            } else if (resultable["opstatus"] == 0) {
                alert("User id already exists. Choose new one");
                dismissLoadingScreen();
            }
        }
    }
function invokeSaveparamInSessionForUserChangeSPA(param1,param2,param3){
		var inputParam = [];
	
		inputParam["param1"] = param1;
		inputParam["param2"] = param2;
		inputParam["param3"] = param3;
	  invokeServiceSecureAsync("SaveParamsForChangePwdUserId", inputParam, callBackSaveparamInSessionUserChangeSPA);
}	


function callBackSaveparamInSessionUserChangeSPA(status,resulttable){
	 if (status == 400) {
		 if(resulttable["opstatus"] == 0){
			 
			  requestOTPForChngUserIdspa();
		 }else{
		 		dismissLoadingScreen();
			 alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
		 }
	 }
	
}


function requestOTPForChngUserIdspa() {
    if (gblIBFlowStatus == "04") {
        //showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));

        popTransferConfirmOTPLock.show();

        
        return false;
    } else {
        var inputParams = {}
        spaChnage = "chguseridib"
        inputParams["eventNotificationId"] = "MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
        inputParams["smsSubject"] = "MIB_ChangeUSERID_" + kony.i18n.getCurrentLocale();
        gblOTPFlag = true;
        gblOnClickReq = false;
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {
            
        }
        //input parasm for SPA OTP
        gblSpaChannel = "ChangeUserId";
        /*
        if (kony.i18n.getCurrentLocale() == "en_US") {
            SpaEventNotificationPolicy = "MIB_ChangeUSERID_EN";
            SpaSMSSubject = "MIB_ChangeUSERID_EN";
        } else {
            SpaEventNotificationPolicy = "MIB_ChangeUSERID_TH";
            SpaSMSSubject = "MIB_ChangeUSERID_TH";
        }*/
        onClickOTPRequestSpa();
    }
}

function svcModifyUserIdspa() {
    var inputParams = {}
    inputParams["userId"] = frmCMChgAccessPin.txtCurUserID.text;
    inputParams["newId"] = frmCMChgAccessPin.txtNewUserID.text;
    inputParams["userStoreId"] = "";
    inputParams["segmentId"] = "MIB";
    inputParams["newName"] = frmCMChgAccessPin.txtNewUserID.text;
    inputParams["newRealm"] = "SystemRealm";
    inputParams["unassignRealm"] = "40151";
    invokeServiceSecureAsync("modifyUser", inputParams, callBackModifyUserIDspa);
}

function callBackModifyUserIDspa(status, resultable) {
    if (status == 400) {
        if (resultable["opstatus"] == 0) {
            
            //Call CRMProfileUpdate and update user
            var inputParam = {}
            inputParam["ibUserId"] = frmCMChgAccessPin.txtNewUserID.text;
            inputParam["ibUserStatusId"] = "02";
            inputParam["actionType"] = "35";
            // showLoadingScreen();
            invokeServiceSecureAsync("crmProfileMod", inputParam, callBackCrmProfileUpadateCMspa)
            completeicon = true;
            frmMyProfile.show();
        } else {
            alert("Error " + resultable["errMsg"]);
        }
    } else {
        if (status == 300) {
            alert("Error");
        }
    }
}

function callBackCrmProfileUpadateCMspa(status, resulttable) {
    
    if (status == 400) {
        
        
        if (resulttable["opstatus"] == 0) {
            
            // fetching To Account Name for TMB Inq
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            //dismissLoadingScreen();
            
            //if(StatusCode!="0"){
            //alert(" "+StatusDesc)
            //showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
            //	return false;
            //}
            //TODo: Send Notification
            var inputParam = {}
            var deliveryMethod = "Email";
            var phoneNo = null
            var emailID = null
            //inputParam["channelName"] = "IB-INQ";
            inputParam["notificationType"] = deliveryMethod;
            //inputParam["phoneNumber"] =phoneNo;
            inputParam["noSendInd"] = 0;
            inputParam["source"] = "changeUserId";

            //inputParam["emailId"] =frmIBCMChngUserID.lblEmailVal.text;
            //inputParam["emailId"] = frmMyProfile.lblEmailVal.text;
            inputParam["notificationSubject"] = "Change UserID";
            inputParam["notificationContent"] = "Your UserID has been changed";
            invokeServiceSecureAsync("NotificationAdd", inputParam, callBackNotificationAddSPA)
        } else {
            dismissLoadingScreen();
            //alert(" "+resulttable["errMsg"]);
            /**dismissLoadingScreen();**/
        }
    }
}

function callBackNotificationAddSPA(status, resulttable) {
    
    if (status == 400) {
        
        
        if (resulttable["opstatus"] == 0) {
            
            //var StatusCode = resulttable["notificationAddRs"][0]["status"][0]["statusCode"];
            //var Severity = resulttable["notificationAddRs"][0]["status"][1]["severity"];
            //var StatusDesc = resulttable["notificationAddRs"][0]["status"][2]["statusDesc"];
            //
            //if(StatusCode!="0"){
            //	alert("----callBackFundTransferInqIB status code:"+StatusCode)
            return false;
            //}
        } else {
            dismissLoadingScreen();
            //alert(" "+resulttable["errMsg"]);
            /**dismissLoadingScreen();**/
        }
    }
}

function otpValidationspa(text) {
    
    //kony.application.showLoadingScreen(frmLoading, "",constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    showLoadingScreen();
    otpCodePattStr = "^[0-9]{" + gblOTPLENGTH + "}$";
    var otpCodePatt = new RegExp(otpCodePattStr, "g");
    resultOtpCodePatt = otpCodePatt.test(text);
    if (resultOtpCodePatt) {
      
        if (spaChnage == "topbillpayments" && GblBillTopFlag) {
            onClickConfirmBillPaymentMB(text);
            return;
        } else if (spaChnage == "topbillpayments" && !GblBillTopFlag) {
            verifyPasswordBillPayment(text);
            return;
        }
        else if (spaChnage == "SendtoSave") {
            
            
            
           
			s2sVerifyTxnPwdApply(text);
            return;
            
        }else if (spaChnage == "SendtoSaveEdit") {
            
            
         
			s2sVerifyTxnPwdEdit(text);
            return;
                
         } 
         else if (spaChnage == "SendtoSaveExecute") {
            
            
         
			s2sVerifyTxnPwdExecute(text);
            return;
                
         }else if (spaChnage == "addbillertopup" && GblBillTopFlag) {
            if (gblMyBillerTopUpBB == 0)
                module = "AddBillersMB"
            else
                module = "AddTopupsMB"
            validateOTPtextBillTopUpJavaService(text, module, true)

            return;
        }else if (spaChnage == "addbillertopup" && !GblBillTopFlag) {
            if (gblMyBillerTopUpBB == 0)
                module = "AddBillersMB"
            else
                module = "AddTopupsMB"
            validateOTPtextBillTopUpJavaService(text, module, true)

            return;
        } else if (spaChnage == "transfers") {
            verifyTransferMB();
            return;
        }else if (spaChnage == "editFutureTranspayments") {
            
            
            
           // changePasswordCompositeSPA();
			srvVerifyPasswordExFT_CS_MB(text);
            return;
        } else if (spaChnage == "chguseridib") {
            
            
            changeUserIDCompositeSPA();
            return;
        } else if (spaChnage == "chgpwdidib") {
            
            
            changePasswordCompositeSPA();
            return;
        } else if(spaChnage == "editAddressInOpenProd"){
        	compositeEditAddressOpenProdMB();
        	 return;
        } else if (spaChnage == "editmyprofile") {
			if(caseTrans == "profile")
			{
				if(profileEmailFlag == "email"||profileAddrFlag == "addr"||profileDeviceNameFlag == "device"||profilePicFlag == "picture")
				{
					verifyPWDMyProfileMB();
				}
			}	
 			if ((caseTrans == "limit1")|| (caseTrans == "limit2") || (caseTrans == "limit3") ){
 						CompositeChangeLimit();
 				
				} 	
				
			 if (caseTrans == "Number") {
 		
		 			if (gblTokenSwitchFlag == true) 
		 			{
				            var inputParam = {};
				            inputParam["loginModuleId"] = "IB_HWTKN";
				            inputParam["userStoreId"] = "DefaultStore";
				            inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
				            inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
				            inputParam["userId"] = gblUserName;
				            
				            inputParam["password"] = popOtpSpa.txttokenspa.text;
				            
				            inputParam["sessionVal"] = "";
				            inputParam["segmentId"] = "segmentId";
				            inputParam["segmentIdVal"] = "MIB";
				            inputParam["channel"] = "InterNet Banking";
				            
				            invokeServiceSecureAsync("verifyTokenEx", inputParam, callBackOTPVerifyspamob)
		        	} else
		        	{
				            kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
				            var inputParam = {};
				            //inputParam["serviceID"] = "verifyOTP";
							inputParam["retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
							inputParam["password"] = popOtpSpa.txtOTP.text;
							inputParam["userId"] = gblUserName;
							inputParam["flagVerify"] = "NEW";
					        invokeServiceSecureAsync("chgMobileNumberCompositeService", inputParam, callBackOTPVerifyspamob)
            
					}
			}
 			
 			if(caseTrans=="Number2")
 			{
 				CompositeChangeMobile();
 			}
            return;
        }else if(spaChnage == "editFutureBillpayments"){
 		 		var txnPwd = "";
 		 		if(gblTokenSwitchFlag == true ) {
 		 			txnPwd = popOtpSpa.txttokenspa.text;
 		 		}else{
 		 			txnPwd = text;
 		 		}
  				 checkEditBillPayVerifyPWDMB(txnPwd);
  				 return;
 		 }else if( spaChnage == "bbflow"){
 		 	
 		 	VerifyPasswordEx_BBCompositeMB(text);
 		 	return;
 		 }
 		 else if(spaChnage == "applyservices"){
 		 		var txnPwd = "";
 		 		if(gblTokenSwitchFlag == true ) {
 		 			txnPwd = popOtpSpa.txttokenspa.text;
 		 		}else{
 		 			txnPwd = text;
 		 		}
  				 modApplyServiceSPAdoRequest(txnPwd);
  				 return;
 		 }else if (spaChnage == "opennewAct"){
	 		openAccountCompositSpa(text);
	 		return;
 		 }
 		 else if (spaChnage == "myacctsadd") {
 			 srvVerifyTxnPassword();
 			 return;
 		 }else if(spaChnage == "addrecipients" || spaChnage == "editProfile" || spaChnage == "addRcManually" || spaChnage =="addRcFacebook"){
 		 		
 		 		executeRecipientMB();
 		 		return;
 			}
 		else if (spaChnage == "dreamsavings") {
        productCodename = gblprodCodeSpa + gblprodNameSpa;
        //activityLogServiceCall("073", "", "00", "", productCodename, frmDreamSavingEdit.lblTargetAmntVal.text, gblAccountNoDreamSpa, frmDreamSavingEdit.lblSetSavingVal.text, frmDreamSavingEdit.lblDreamDescVal.text, "")
        editDreamSavingCompositeServiceMB();
        return;
    	}else if(spaChnage == "ActivateCard"){
    		verifypwdForDebitCardActivation( popOtpSpa.txttokenspa.text);
    	}
        /*bill Payment module completed*/
        if (gblTokenSwitchFlag == true) {
            var inputParam = {};
            inputParam["loginModuleId"] = "IB_HWTKN";
            inputParam["userStoreId"] = "DefaultStore";
            inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
            inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
            inputParam["userId"] = gblUserName;
            
            inputParam["password"] = popOtpSpa.txttokenspa.text;
            
            inputParam["sessionVal"] = "";
            inputParam["segmentId"] = "segmentId";
            inputParam["segmentIdVal"] = "MIB";
            inputParam["channel"] = "InterNet Banking";
            
            invokeServiceSecureAsync("verifyTokenEx", inputParam, callBackOTPVerifyspaChngecreds)
        } else {
            kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
            var inputParam = {};
            inputParam["tokenUUID"] = gblTokenNum;
            inputParam["otp"] = text;
            inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
            inputParam["session"] = "session";
            invokeServiceSecureAsync("verifyPasswordEx", inputParam, callBackOTPVerifyspaChngecreds)
            /*inputParam = {
			//otp: frmIBTransferNowConfirmation.txtBxOTP.text,
			//tokenUUID: "585678",
			retryCounterVerifyOTP: gblVerifyOTPCounter,
			userId: gblUserName,
			userStoreId: "DefaultStore",
			loginModuleId: "IBSMSOTP",
			password: text,
			segmentId: "MIB"
			};
			invokeServiceSecureAsync("verifyPasswordEx", inputParam, callBackOTPVerifyspaChngecreds)
		*/
        }

    } else {
    	if(gblTokenSwitchFlag==true)
		{
			popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("invalidOTP");
    		kony.application.dismissLoadingScreen();
			return false;
		}
		else
		{
           	popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("invalidOTP");
           	popOtpSpa.lblPopupTract4Spa.text = "";
           	popOtpSpa.txtOTP.text = "";
			kony.application.dismissLoadingScreen();
			return false;
		}
        //var invlidOTP = kony.i18n.getLocalizedString("invalidOTP");
//        var info1 = kony.i18n.getLocalizedString("info");
//        var okk = kony.i18n.getLocalizedString("keyOK");
//        //Defining basicConf parameter for alert
//        var basicConf = {
//            message: invlidOTP,
//            alertType: constants.ALERT_TYPE_INFO,
//            alertTitle: info1,
//            yesLabel: okk,
//            noLabel: "",
//            alertHandler: handle2
//        };
//        kony.application.dismissLoadingScreen();
//        //Defining pspConf parameter for alert
//        var pspConf = {};
//        //Alert definition
//        var infoAlert = kony.ui.Alert(basicConf, pspConf);
//
//        function handle2(response) {}
//        //	alert("INVALID ACTIVATION CODE");
//        return false;
    }
}

function callBackOTPVerifyspaChngecreds(status, resulttable) {
popOtpSpa.txttokenspa.text = "";
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            otplocked = false;
            gblVerifyOTPCounter = "0";
            getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPasswordLabel"), 0, 0, 0);
            gblShowPinPwdSecs = kony.os.toNumber(resulttable["showPinPwdSecs"]);
            gblRetryCountRequestOTP = "0";
            gblShowPwdNo = kony.os.toNumber(resulttable["showPinPwdCount"]);
            popOtpSpa.dismiss();
            displayFormforSpa();
            //frmMyProfile.show();
            kony.application.dismissLoadingScreen();
        } else if (resulttable["opstatus"] == 8005) {

            if (gblVerifyOTPCounter >= 3) {
                modifyUserUpdate("Disabled", "3");
                alertUserStatusLocked()
            } else {
                popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
				kony.application.dismissLoadingScreen();
            }
            /*if (resulttable["errCode"] == "VrfyOTPCtrErr00001" || resulttable["errCode"] == "VrfyOTPCtrErr00002" || resulttable["errCode"] == "VrfyOTPCtrErr00003"){
					if(resulttable["errCode"] == "VrfyOTPCtrErr00001"){
						gblVerifyOTPCounter = "1";
					}else if(resulttable["errCode"] == "VrfyOTPCtrErr00002"){
						gblVerifyOTPCounter = "2";
					}else if(resulttable["errCode"] == "VrfyOTPCtrErr00003"){
						gblVerifyOTPCounter = "3";
					}*/
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];

                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                popOtpSpa.lblPopupTract4Spa.text = "";
				popOtpSpa.txtOTP.text = "";
                kony.application.dismissLoadingScreen();
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                gblVerifyOTPCounter = "0";
                otplocked = true;
                kony.application.dismissLoadingScreen();
                popOtpSpa.dismiss();
                if (gblSetPwdSpa) {
                	popTransferConfirmOTPLockNew.lblText.text = kony.i18n.getLocalizedString("keyOtpLocked");
                	popTransferConfirmOTPLockNew.show();
                } else {
                	popTransferConfirmOTPLock.show();
                }
                
                // calling crmprofileMod to update the user status
                updteuserSpa();

                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
               kony.application.dismissLoadingScreen();
               showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
               return false;
            } else if (successFlag == false && spaChnage == "activation") {
                if (gblActionCode == "11") {
                    activityLogServiceCall("004", "", "02", "", "", "Fail", "", "", "", "");
                } else if (gblActionCode == "12") {
                    activityLogServiceCall("014", "", "02", "", "", "Fail", "", "", "", "");
                }
            } else {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        kony.application.dismissLoadingScreen();
    }
}

function displayFormforSpa() {
    
     successFlag="";
    if (spaChnage == "chguseridib" || spaChnage == "chgpwdidib") {
        if (spaChnage == "chguseridib") {
            if (otplocked) {
                var inputParam = {}
                inputParam["ibUserId"] = frmIBCMChngUserID.txtCurUserID.text;
                inputParam["ibUserStatusId"] = "04";
                inputParam["actionType"] = "32";
                showLoadingScreen();
                invokeServiceSecureAsync("crmProfileMod", inputParam, callBackCrmProfileUpadateMB)
            } else {
                svcModifyUserIdspa();
            }
        }
        if (spaChnage == "chgpwdidib") {
            callBackVerifyChngPWD();
        }
    } else if (spaChnage == "activation") {
        successFlag = true;
        frmMBSetuseridSPA.show();
        if (gblActionCode == "11") {
            activityLogServiceCall("004", "", "01", "", "", "Success", "", "", "", "");
        } else if (gblActionCode == "12") {
            activityLogServiceCall("014", "", "01", "", "", "Success", "", "", "", "");
        }
    } else if (spaChnage == "addrecipients") {
        addrecipientsSpa();
        //addAccountRecipentComplete();
    } else if (spaChnage == "editProfile") {
        //startExistingRcMBProfileEditService();
        spaEditProfilepre();
        addrecipientsSpa();
    } else if (spaChnage == "addRcManually") {
        //addNewRecipientDetailsMB();
        addrecipientsSpa();
    } else if (spaChnage == "transfers") {
        if (otplocked == true) {
            updteuserSpa();
        } else {
            transferFlowAckMB();
        }
    } else if (spaChnage == "myacctsadd") {
        //addCompleteScreen();	
        srvAddCompleteProcess();
        popupTractPwd.dismiss();
    } else if (spaChnage == "applyservices") {
        //alert("spa apply services");
        applyServicesreqGenActCode();
    } else if (spaChnage == "addRcFacebook") {
        for (var i = 0; i < multiSelectedFacebook.length; i++) {
            var name = multiSelectedFacebook[i].lblName;
            var mobile = addCrossPh(stripDashPh(multiSelectedFacebook[i].lblMobile));
            var email = addCrossEmail(multiSelectedFacebook[i].lblEmail);
            var facebook = multiSelectedFacebook[i].lblFacebook;
            activityLogServiceCall("045", "", "01", "", "Add", name, mobile, email, facebook, "");
        }
        transConfirmSelectFacebook();
    } else if (spaChnage == "topbillpayments") {

        if (GblBillTopFlag) {
            //alert("inside bill payments")
            callBillPaymentServiceOnConfirmClick();
            popupTractPwd.dismiss();
        } else {
            //alert("inside top up payments")
            callTopUpPaymentServiceOnConfirmClick();
            popupTractPwd.dismiss();
        }
    } else if (spaChnage == "addbillertopup") {
        popupTractPwd.dismiss();
        dismissLoadingScreen();
        
        //
        if (gblMyBillerTopUpBB == 2) {
            addBBToList();
        } else {
            addBillersMB(); // adding biller on once password is verified.
        }
    } else if (spaChnage == "bbflow") {
        var currForm = kony.application.getCurrentForm();
        if (currForm.id == 'frmBBExecuteConfirmAndComplete') {
            frmBBExecuteConfirmAndComplete.hbxAdvertisement.setVisibility(true)
            frmBBExecuteConfirmAndComplete.hbxAdvertisement.isVisible = true;
            frmBBExecuteConfirmAndComplete.hbxStartImage.setVisibility(true);
            frmBBExecuteConfirmAndComplete.hbxStartImage.isVisible = true;
            frmBBExecuteConfirmAndComplete.hbxbpconfcancelspa.setVisibility(false);
            frmBBExecuteConfirmAndComplete.hbxbpconfcancelspa.isVisible = false;
            frmBBExecuteConfirmAndComplete.hbxReturnSpa.setVisibility(true);
            frmBBExecuteConfirmAndComplete.hbxReturnSpa.isVisible = true;

            frmBBExecuteConfirmAndComplete.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
        } else {
            showLoadingScreen();
            invokeApplyBeenAndBill();
            dismissLoadingScreen();
        }
    } else if (spaChnage == "editmyprofile") {
        //will write a new call back function
        callbackEditmyprofileSpa()
    } else if (spaChnage == "editFutureBillpayments") {
        editBillPaymentFlowMB();
    } else if (spaChnage == "editFutureTranspayments") {
        if (gblEditFTSchduleMB == true) {
            srvFutureTransferDeleteMB();
        } else {
            srvFutureTransferUpdateMB();
        }
    } else if (spaChnage == "SendtoSave") {
        if (gblSSServieHours == "dummyABX") {
            //modify S2S details using RetailLiquidityMod service
            
            invokeRetailLiquidityMod();
        } else if (gblSSServieHours == "dummyS2SExecuteTransfer") {
            
            invokeRetailLiquidityTransferAdd();
        } else if (gblSSServieHours == "applyServiceHours") {
            //add S2S details using liquidityAdd service
            
            addS2SDetailsToLiquidityAdd();
        }
    } else if (spaChnage == "opennewAct") {
        //editBillPaymentFlowMB();	
        callbackOpennewAcct();
    } else if (spaChnage == "dreamsavings") {
        productCodename = gblprodCodeSpa + gblprodNameSpa;
        activityLogServiceCall("073", "", "00", "", productCodename, frmDreamSavingEdit.lblTargetAmntVal.text, gblAccountNoDreamSpa, frmDreamSavingEdit.lblSetSavingVal.text, frmDreamSavingEdit.lblDreamDescVal.text, "")
        editDreamSavingCompositeServiceMB();
    }
}

function callbackOpennewAcct() {
    var finTxnRefId = callGenerateTransferRefNoserviceOpenActMBCallBack();
    var fromActId;
    var fromActName;
    var fromActNickName;
    var fromFiident;
    var fromActType;
    var toActNum;
    var toActType;
    var toFiident;
    var dreamAmt;
    var transferAmount;
    var fromProdId;
    if (gblSelProduct == "TMBDreamSavings") {
        frmOpenActDSAck.imgDSAckTitle.src = frmOpenActDSConfirm.imgDSCnfmTitle.src;
        frmOpenActDSAck.lblDSAckTitle.text = frmOpenActDSConfirm.lblDSCnfmTitle.text;
        frmOpenActDSAck.lblMyDreamDesValAck.text = frmOpenActDSConfirm.lblMyDreamDesVal.text;
        frmOpenActDSAck.imgOADreamDetailAck.src = frmOpenActDSConfirm.imgOADreamDetail.src;
        frmOpenActDSAck.lblOADreamDetailTarAmtValAck.text = frmOpenActDSConfirm.lblOADreamDetailTarAmtVal.text;
        frmOpenActDSAck.lblOADSNickNameValAck.text = frmOpenActDSConfirm.lblOADSNickNameVal.text;
        frmOpenActDSAck.lblOADSActNameValAck.text = frmOpenActDSConfirm.lblOADSActNameVal.text;
        frmOpenActDSAck.lblOADSAmtValAck.text = frmOpenActDSConfirm.lblOADSAmtVal.text;
        frmOpenActDSAck.lblOADSBranchValAck.text = frmOpenActDSConfirm.lblOADSBranchVal.text;
        frmOpenActDSAck.lblOADMnthValAck.text = frmOpenActDSConfirm.lblOADMnthVal.text;
        frmOpenActDSAck.lblOADIntRateValAck.text = frmOpenActDSConfirm.lblOADIntRateVal.text;
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmOpenActDSAck.lblOADIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
        } else {
            frmOpenActDSAck.lblOADIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
        }
        frmOpenActDSAck.lblDSOpenDateValAck.text = frmOpenActDSConfirm.lblDSOpenDateVal.text;
        frmOpenActDSAck.lblOAMnthlySavNameAck.text = frmOpenActDSConfirm.lblOAMnthlySavName.text;
        frmOpenActDSAck.lblOAMnthlySavTypeAck.text = frmOpenActDSConfirm.lblOAMnthlySavType.text;
        frmOpenActDSAck.imgOAMnthlySavPicAck.src = frmOpenActDSConfirm.imgOAMnthlySavPic.src;
        frmOpenActDSAck.lblOAMnthlySavNumAck.text = frmOpenActDSConfirm.lblOAMnthlySavNum.text
        fromActName = gblmbSpaselectedData.hiddenActName;
        gblFinActivityLogOpenAct["fromActName"] = fromActName;
        fromActId = gblmbSpaselectedData.lblActNoval;
        gblFinActivityLogOpenAct["fromActId"] = fromActId;
        fromActNickName = gblmbSpaselectedData.lblCustName;
        gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
        fromFiident = gblmbSpaselectedData.hiddenFiident;
        fromActType = gblmbSpaselectedData.hiddenActType;
        gblFinActivityLogOpenAct["fromActType"] = fromActType;
        fromProdId = gblmbSpaselectedData.hiddenProductId;
        gblFinActivityLogOpenAct["toActType"] = "SDA";
        gblFinActivityLogOpenAct["toActName"] = fromActName;
        fromActId = fromActId.toString().replace(/-/g, "");
        transferAmount = frmOpenActDSConfirm.lblOADSAmtVal.text;
        var bhat = kony.i18n.getLocalizedString("currencyThaiBaht");
        transferAmount = transferAmount.replace(bhat, "")
        
        //transferAmount = transferAmount.replace("/Month", "")
        transferAmount = transferAmount.replace(",", "");
        
        gblFinActivityLogOpenAct["transferAmount"] = transferAmount;
        dreamAmt = frmOpenActDSConfirm.lblOADSAmtVal.text;
        dreamAmt = dreamAmt.replace(bhat, "")
        dreamAmt = dreamAmt.replace(",", "")
        var dreamMonth = frmOpenActDSConfirm.lblOADMnthVal.text;
        //depositAOXfOpenActCallBack();
        invokeDepositAOXferOpenAct(fromActId, fromActName, fromFiident, "", "", "", fromActType, transferAmount, dreamAmt, dreamMonth, fromProdId)
        //setFinancialActivityLogOpenAct(finTxnRefId,fromActId,fromActName,fromActNickName,toActIdVal, toActName, toActNickName,finTxnAmount, finTxnFee, finTxnStatus, tellerId, finLinkageId, errorCd)
        //frmOpenActDSAck.show();
    } else if (gblSelProduct == "ForTerm") {
        frmOpenActTDAck.imgOATDAckTitle.src = frmOpenActTDConfirm.imgTDCnfmTitle.src;
        frmOpenActTDAck.lblOATDAckTitle.text = frmOpenActTDConfirm.lblTDCnfmTitle.text;
        frmOpenActTDAck.lblOATDNickNameValAck.text = frmOpenActTDConfirm.lblOATDNickNameVal.text;
        frmOpenActTDAck.lblOATDActNameValAck.text = frmOpenActTDConfirm.lblOATDActNameVal.text;
        frmOpenActTDAck.lblOATDBranchValAck.text = frmOpenActTDConfirm.lblOATDBranchVal.text;
        frmOpenActTDAck.lblOATDAmtValAck.text = frmOpenActTDConfirm.lblOATDAmtVal.text;
        frmOpenActTDAck.lblOATDIntRateValAck.text = frmOpenActTDConfirm.lblOATDIntRateVal.text;
        frmOpenActTDAck.lblTDOpenDateValAck.text = frmOpenActTDConfirm.lblTDOpenDateVal.text;
        frmOpenActTDAck.lblOATDTransRefAckVal.text = frmOpenActTDConfirm.lblTdTransRefVal.text;
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmOpenActTDAck.lblOATDIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
        } else {
            frmOpenActTDAck.lblOATDIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
        }

        if (gblisDisToActTD == "Y") {
            frmOpenActTDAck.lblOAPayIntAck.setVisibility(true);
            frmOpenActTDAck.hbxOAPayIntAck.setVisibility(true);
            frmOpenActTDAck.lblOATDToNicNam.text = frmOpenActTDConfirm.lblTDTransToNickName.text;
            frmOpenActTDAck.lblOATDToActType.text = frmOpenActTDConfirm.lblTDTransToActType.text;
            frmOpenActTDAck.lblOATDToAmt.text = frmOpenActTDConfirm.lblTDTransToAccBal.text;
            frmOpenActTDAck.imgOATDToAck.src = frmOpenActTDConfirm.imgOATDProTo.src;
            toActNum = frmOpenAccTermDeposit.lblTDTransToAccBal.text;
            toActType = frmOpenAccTermDeposit.hiddenActType.text;
            toActNum = toActNum.toString().replace(/-/g, "");
            toFiident = frmOpenAccTermDeposit.hiddenFiident.text;
        } else {
            frmOpenActTDAck.lblOAPayIntAck.setVisibility(false);
            frmOpenActTDAck.hbxOAPayIntAck.setVisibility(false);
            toActNum = "";
            toActType = "";
            toFiident = "";
        }
        frmOpenActTDAck.lblOAFrmNameAck.text = frmOpenActTDConfirm.lblTDTransFrmNickName.text;
        frmOpenActTDAck.lblOAFrmTypeAck.text = frmOpenActTDConfirm.lblTDTransFrmActType.text;
        frmOpenActTDAck.lblOAFrmNumAck.text = frmOpenActTDConfirm.lblTDTransFromAccBal.text;
        frmOpenActTDAck.imgOAFrmPicAck.src = frmOpenActTDConfirm.imgOAMnthlySavPic.src;
        fromActName = gblmbSpaselectedData.hiddenActName;
        fromActId = gblmbSpaselectedData.lblActNoval;
        fromActNickName = gblmbSpaselectedData.lblCustName;
        fromFiident = gblmbSpaselectedData.hiddenFiident;
        gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
        fromActType = gblmbSpaselectedData.hiddenActType;
        gblFinActivityLogOpenAct["fromActId"] = fromActId;
        fromActId = fromActId.toString().replace(/-/g, "");
        gblFinActivityLogOpenAct["fromActIdFormated"] = fromActId;
        gblFinActivityLogOpenAct["toActName"] = fromActName;
        gblFinActivityLogOpenAct["toFiident"] = toFiident;
        transferAmount = gblFinActivityLogOpenAct["transferAmount"];
        gblFinActivityLogOpenAct["fromActName"] = fromActName;
        gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
        gblFinActivityLogOpenAct["fromActType"] = fromActType;
        gblFinActivityLogOpenAct["toActType"] = "CDA";
        gblFinActivityLogOpenAct["transferAmount"] = transferAmount;
        gblFinActivityLogOpenAct["toActId"] = toActNum;
        gblFinActivityLogOpenAct["dreamAmt"] = "";
        gblFinActivityLogOpenAct["dreamMonth"] = "";
        gblFinActivityLogOpenAct["fromProdId"] = "";
        //checkIsEligibleToOpenAct(fromActId,fromActType);
        //setFinancialActivityLogOpenAct(finTxnRefId,fromActId,fromActName,fromActNickName,toActIdVal, toActName, toActNickName,finTxnAmount, finTxnFee, finTxnStatus, tellerId, finLinkageId, errorCd) 
        invokeDepositAOXferOpenAct(fromActId, fromActName, fromFiident, toFiident, toActNum, toActType, fromActType, transferAmount, "", "", "");
    } else if (gblSelProduct == "TMBSavingcare") {
        fromActName = gblmbSpaselectedData.hiddenActName;
        fromActId = gblmbSpaselectedData.lblActNoval;
        fromActNickName = gblmbSpaselectedData.lblCustName;
        fromFiident = gblmbSpaselectedData.hiddenFiident;
        fromActType = gblmbSpaselectedData.hiddenActType;
        transferAmount = gblFinActivityLogOpenAct["transferAmount"];
        gblFinActivityLogOpenAct["fromActName"] = fromActName;
        gblFinActivityLogOpenAct["fromActId"] = fromActId;
        gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
        gblFinActivityLogOpenAct["fromActType"] = fromActType;
        gblFinActivityLogOpenAct["toActType"] = "SDA";
        gblFinActivityLogOpenAct["transferAmount"] = transferAmount;
        gblFinActivityLogOpenAct["toActName"] = fromActName;
        fromActId = fromActId.toString().replace(/-/g, "");
        //setFinancialActivityLogOpenAct(finTxnRefId,fromActId,fromActName,fromActNickName,toActIdVal, toActName, toActNickName,finTxnAmount, finTxnFee, finTxnStatus, tellerId, finLinkageId, errorCd) 
        invokeDepositAOXferOpenAct(fromActId, fromActName, fromFiident, "", "", "", fromActType, transferAmount, "", "", "");
        //onClickPreShowSCAck();
    } else if (gblSelProduct == "ForUse") {
        fromActName = gblmbSpaselectedData.hiddenActName;
        fromActId = gblmbSpaselectedData.lblActNoval;
        fromActNickName = gblmbSpaselectedData.lblCustName;
        fromFiident = gblmbSpaselectedData.hiddenFiident;
        fromActType = gblmbSpaselectedData.hiddenActType;
        transferAmount = gblFinActivityLogOpenAct["transferAmount"];
        gblFinActivityLogOpenAct["fromActName"] = fromActName;
        gblFinActivityLogOpenAct["fromActId"] = fromActId;
        gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
        gblFinActivityLogOpenAct["fromActType"] = fromActType;
        gblFinActivityLogOpenAct["transferAmount"] = transferAmount;
        gblFinActivityLogOpenAct["toActType"] = "SDA";
        gblFinActivityLogOpenAct["toActName"] = fromActName;
        
        fromActId = fromActId.toString().replace(/-/g, "");
        //setFinancialActivityLogOpenAct(finTxnRefId,fromActId,fromActName,fromActNickName,toActIdVal, toActName, toActNickName,finTxnAmount, finTxnFee, finTxnStatus, tellerId, finLinkageId, errorCd) 
        invokeDepositAOXferOpenAct(fromActId, fromActName, fromFiident, "", "", "", fromActType, transferAmount, "", "", "");
        //onclickConfirmTransNSPwd();
    }

}



function callbackEditmyprofileSpa() {
    completeicon = true;
    if (caseTrans == "limit1") {
        activityLogServiceCall("052", "", "00", gblDeviceNickName, gblEBMaxLimitAmtCurrentOld, newlimit, "", "", "", "");
        crmProfileModEditMyProfile("reqCurr");
        var newlimit = frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text;
    } else if (caseTrans == "limit2") {

        activityLogServiceCall("052", "", "00", gblDeviceNickName, gblEBMaxLimitAmtCurrentOld, newlimit, "", "", "", "");
        retrieveCurrentServiceStatus();
        crmProfileModEditMyProfile("hist");
        var newlimit = frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text;

    } else if (caseTrans == "limit3") {

        activityLogServiceCall("052", "", "00", gblDeviceNickName, gblEBMaxLimitAmtCurrentOld, newlimit, "", "", "", "");
        retrieveCurrentServiceStatus();
        crmProfileModEditMyProfile("req");
        activityLogServiceCall("052", "", "00", gblDeviceNickName, gblEBMaxLimitAmtCurrentOld, newlimit, "", "", "", "");
        callLimitedit = true;

    } else if (caseTrans == "Number") {
        activityLogServiceCall("069", "", "00", gblDeviceNickName, gblPHONENUMBEROld, gblPhoneNumberReq, "", "", "", "");

        CustMobileUpdateNewMobileNum();

    } else if (caseTrans == "profile") {

        if (profileEmailFlag == "email") {
            crmProfileModEditMyProfile("email");
        }
        if (profileAddrFlag == "addr") {
            
            partyUpdateMyProfileService();
        }
        if (profileDeviceNameFlag == "device") {
            
            updateDeviceNameForProfile();
        }
        if (profilePicFlag == "picture") {
            
            //imgEditProfileServiceCall();
            imageServiceCallSpa();
        } else if (profileedit == false) {
            frmMyProfile.show();
        }
        NotificationEditProfileMBService();

        toCallViewProfileonSave();
    }
}

function updteuserSpa() {
    gblIBFlowStatus = "";
    var  inputParam = {};
    inputParam["ibUserStatusId"] = "04";
    inputParam["actionType"] = "32";
    //showLoadingScreen();
    //invokeServiceSecureAsync("crmProfileMod", inputParam, callBackCrmProfileUpadateMB)
    gblIBFlowStatus = "04";
    return;
}


function addrecipientsSpa() {
    if (gblFlagTransPwdFlow == "editProfile") {
        if (flowSpa) {
            if (profilePicFlag == "picture") {
                //alert("in img upload spa")
                var perID = currentRecipientDetails.rcId;
                startImageServiceCallRecipientsSpa(perID);
            } else {
                startExistingRcMBProfileEditService("imageNotAdded");
            }
        } else {
            if (gblRcBase64List != "") {
                gblRcPersonalizedIdList = currentRecipientDetails.rcId;
                imageServiceCall();
            } else {
                startExistingRcMBProfileEditService("imageNotAdded");
            }
        }
        //startExistingRcMBProfileEditService();
    } else if (gblFlagTransPwdFlow == "addAccount") {
        if (isRecipientNew == false) {
            for (var i = 0; i < AddAccountList.length; i++) {
                var NickName = AddAccountList[i].lblNick.text;
                var BankName = "";
                for (var j = 0; j < globalSelectBankData.length; j++) {
                    if (AddAccountList[i].bankCode == globalSelectBankData[j][0]) {
                        BankName = globalSelectBankData[j][2];
                        break;
                    }
                }
                var Number = stripDashAcc(AddAccountList[i].lblAccountNo.text);
                activityLogServiceCall("046", "", "01", "", "Add", NickName, BankName, Number, "", "");
            }
        } else {
            if (frmMyRecipientAddProfile.tbxRecipientName.text == "" || frmMyRecipientAddProfile.tbxRecipientName.text == undefined || frmMyRecipientAddProfile.tbxRecipientName.text == null) {
                var name = "";
            } else {
                var name = frmMyRecipientAddProfile.tbxRecipientName.text;
            }
            if (frmMyRecipientAddProfile.tbxMobileNo.text == "" || frmMyRecipientAddProfile.tbxMobileNo.text == undefined || frmMyRecipientAddProfile.tbxMobileNo.text == null) {
                var mobile = "";
            } else {
                var mobile = addCrossPh(stripDashPh(frmMyRecipientAddProfile.tbxMobileNo.text));
            }
            if (frmMyRecipientAddProfile.tbxEmail.text == "" || frmMyRecipientAddProfile.tbxEmail.text == undefined || frmMyRecipientAddProfile.tbxEmail.text == null) {
                var email = "";
            } else {
                var email = addCrossEmail(frmMyRecipientAddProfile.tbxEmail.text);
            }
            if (frmMyRecipientAddProfile.tbxFbID.text == "" || frmMyRecipientAddProfile.tbxFbID.text == undefined || frmMyRecipientAddProfile.tbxFbID.text == null) {
                var facebook = "";
            } else {
                var facebook = frmMyRecipientAddProfile.tbxFbID.text;
            }
            activityLogServiceCall("045", "", "01", "", "Add", name, mobile, email, facebook, "");
            if (AddAccountList.length > 0) {
                for (var i = 0; i < AddAccountList.length; i++) {
                    var NickName = AddAccountList[i].lblNick.text;
                    var BankName = "";
                    for (var j = 0; j < globalSelectBankData.length; j++) {
                        if (AddAccountList[i].bankCode == globalSelectBankData[j][0]) {
                            BankName = globalSelectBankData[j][2];
                            break;
                        }
                    }
                    var Number = AddAccountList[i].lblAccountNo.text;
                    activityLogServiceCall("046", "", "01", "", "Add", NickName, BankName, Number, "", "");
                }
            }
        }
        
        addAccountRecipentComplete();
    } else if (gblFlagTransPwdFlow == "addRcManually") {
        addNewRecipientDetailsMB();
        isRecipientNew = true;
    } else if (gblFlagTransPwdFlow == "addRcContacts") {
        for (var i = 0; i < multiSelectedContacts.length; i++) {
            var name = multiSelectedContacts[i].lblName;
            var mobile = addCrossPh(stripDashPh(multiSelectedContacts[i].lblMobile));
            var email = addCrossEmail(multiSelectedContacts[i].lblEmail);
            var facebook = multiSelectedContacts[i].lblFacebook;
            activityLogServiceCall("045", "", "01", "", "Add", name, mobile, email, facebook, "");
        }
        transConfirmSelectContacts();
    } else if (gblFlagTransPwdFlow == "addRcFacebook") {
        for (var i = 0; i < multiSelectedFacebook.length; i++) {
            var name = multiSelectedFacebook[i].lblName;
            var mobile = addCrossPh(stripDashPh(multiSelectedFacebook[i].lblMobile));
            var email = addCrossEmail(multiSelectedFacebook[i].lblEmail);
            var facebook = multiSelectedFacebook[i].lblFacebook;
            activityLogServiceCall("045", "", "01", "", "Add", name, mobile, email, facebook, "");
        }
        transConfirmSelectFacebook();
    }
}




function imageServiceCallSpa() {

    inputParam = {
        crmId: gblcrmId
    };
    showLoadingScreen();
    invokeServiceSecureAsync("confirmPicUpload", inputParam, imageServiceCallbackSpa);

}

function imageServiceCallbackSpa(status, callBackResponse) {
    //alert("in response callback...");

    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            //alert("success...");
            flag4 = true;
            gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&modIdentifier=MyProfile";
            frmeditMyProfile.imgprofpic.src = gblMyProfilepic;

        } else
            frmeditMyProfile.imgprofpic.src = gblMyProfilepic;
    }
}


function startImageServiceCallRecipientsSpa(perID) {


    personalizedID = perID;

    inputParam = {
        crmId: gblcrmId,
        personalizedId: personalizedID
    };
    try {
        invokeServiceSecureAsync("confirmPicUpload", inputParam, startImageServiceCallRecipientsSPAAsyncCallback)
    } catch (e) {
        // todo: handle exception
        //invokeCommonIBLogger("Exception in invoking service");
    }
}
/**
 * Image upload service async callback
 * @param {} status
 * @param {} callBackResponse
 * @returns {}
 */

function startImageServiceCallRecipientsSPAAsyncCallback(status, callBackResponse) {
    if (status == 400) {
    	var randomnum = Math.floor((Math.random()*10000)+1); 
    	if(gblFlagTransPwdFlow == "editProfile")
			frmMyRecipientDetail.imgprofilepic.src=gblRcImageUrlConstant + "&personalizedId=" +personalizedID+"&rr="+randomnum;
			
        else
           frmMyRecipientAddAccComplete.imgprofilepic.src =   gblRcImageUrlConstant + "&personalizedId=" +personalizedID+"&rr="+randomnum;
           
    } else {
        if (status == 300) {
            showCommonAlert("Error", null);
        }
    }
}



function spaEditProfilepre() {
    if (currentRecipientDetails["name"] == frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, "")) {
        var name = frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, "");;
    } else {
        var name = currentRecipientDetails["name"] + "+" + frmMyRecipientEditProfile.tbxRecipientName.text.replace(/^\s+|\s+$/g, "");;
    }
    if (currentRecipientDetails["mobile"] == frmMyRecipientEditProfile.tbxMobileNo.text) {
        var mobile = frmMyRecipientEditProfile.tbxMobileNo.text;
    } else {
        if (currentRecipientDetails["mobile"] == "" || currentRecipientDetails["mobile"] == undefined) {
            var mobile = addCrossPh(stripDashPh(frmMyRecipientEditProfile.tbxMobileNo.text));
        } else if (frmMyRecipientEditProfile.tbxMobileNo.text == "" || frmMyRecipientEditProfile.tbxMobileNo.text == undefined) {
            var mobile = addCrossPh(stripDashPh(currentRecipientDetails["mobile"]));
        } else {
            var mobile = addCrossPh(stripDashPh(currentRecipientDetails["mobile"])) + "+" + addCrossPh(stripDashPh(frmMyRecipientEditProfile.tbxMobileNo.text));
        }
    }
    if (currentRecipientDetails["email"] == frmMyRecipientEditProfile.tbxEmail.text) {
        var email = frmMyRecipientEditProfile.tbxEmail.text;
    } else {
        if (currentRecipientDetails["email"] == "" || currentRecipientDetails["email"] == undefined) {
            var email = addCrossEmail(frmMyRecipientEditProfile.tbxEmail.text);
        } else if (frmMyRecipientEditProfile.tbxEmail.text == "" || frmMyRecipientEditProfile.tbxEmail.text == undefined) {
            var email = addCrossEmail(currentRecipientDetails["email"]);
        } else {
            var email = addCrossEmail(currentRecipientDetails["email"]) + "+" + addCrossEmail(frmMyRecipientEditProfile.tbxEmail.text);
        }
    }
    if (currentRecipientDetails["facebook"] == frmMyRecipientEditProfile.tbxFbID.text) {
        var facebook = frmMyRecipientEditProfile.tbxFbID.text;
    } else {
        if (currentRecipientDetails["facebook"] == "" || currentRecipientDetails["facebook"] == undefined) {
            var facebook = frmMyRecipientEditProfile.tbxFbID.text;
        } else if (frmMyRecipientEditProfile.tbxFbID.text == "" || frmMyRecipientEditProfile.tbxFbID.text == undefined) {
            var facebook = currentRecipientDetails["email"];
        } else {
            var facebook = currentRecipientDetails["facebook"] + "+" + frmMyRecipientEditProfile.tbxFbID.text;
        }
    }
    activityLogServiceCall("045", "", "01", "", "Edit", name, mobile, email, facebook, "");
    gblFlagTransPwdFlow = "editProfile";
}


function changeUserIDCompositeSPA() {

    var inputParams = {}
    //  gblTokenSwitchFlag
    //TO pass input params for verifyToken

    // inputParams["userId"] = frmIBCMChngUserID.txtCurUserID.text;
    // inputParams["newId"] = frmIBCMChngUserID.txtNewUserID.text;

    if (gblTokenSwitchFlag == true) {

        inputParams["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParams["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;

        inputParams["password"] = popOtpSpa.txttokenspa.text;

        inputParams["newName"] = frmCMChgAccessPin.txtNewUserID.text;
		inputParams["segmentIdVal"] = "MIB";

    } else {
        inputParams["password"] = popOtpSpa.txtOTP.text;
        inputParams["retryCounterVerifyOTP"] = gblVerifyOTPCounter;

    }
    // this flag determine otp or token flow
    inputParams["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";

    inputParams["newName"] = frmCMChgAccessPin.txtNewUserID.text;
    inputParams["newId"] = frmCMChgAccessPin.txtNewUserID.text;
    //TODO Fix custName value  customerNameIB 
    inputParams["custName"] = customerNameIB;
    inputParams["Locale"] = kony.i18n.getCurrentLocale();

    var platformChannel = gblDeviceInfo.name;
    inputParams["activityFlexValues1"] = frmCMChgAccessPin.txtCurUserID.text;
    inputParams["activityFlexValues2"] = frmCMChgAccessPin.txtNewUserID.text;
    inputParams["ibUserStatusId"] = "02";
    inputParams["actionType"] = "00";
    inputParams["channel"] = "IB";




    // invoking call 
    invokeServiceSecureAsync("changeUserIDComposite", inputParams, callBackTMBcompositeChangeUserIDOTPSPA);

}


function callBackTMBcompositeChangeUserIDOTPSPA(status, resultable) {
    
    if (status == 400) {
    	if (flowSpa) 
    	{
          	popOtpSpa.txtOTP.text="";
           	popOtpSpa.txttokenspa.text="";
        }
        frmCMChgAccessPin.txtCurUserID.text = "";
        frmCMChgAccessPin.txtNewUserID.text = "";
        if (resultable["opstatus_verifypwd"] == 0 || resultable["opstatus"] == 0) {
            dismissLoadingScreen();
            gblVerifyOTPCounter = "0";
            gblIBRetryCountRequestOTP["chngIBUserId"] = "0";
        } else if (resultable["opstatus_verifypwd"] != 0) {
            if (resultable["opstatus"] == 8005) {
            	if (flowSpa) 
		    	{
		          	popOtpSpa.txtOTP.text="";
		           	popOtpSpa.txttokenspa.text="";
		        }
                if (resultable["errCode"] == "VrfyOTPErr00001") {
                    	gblVerifyOTPCounter = "0";
                    	if(gblTokenSwitchFlag==true)
                        {
							popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
    						kony.application.dismissLoadingScreen();
						}
						else
						{
                        	popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                        	popOtpSpa.lblPopupTract4Spa.text = "";
                        	popOtpSpa.txtOTP.text = "";
						}
                        kony.application.dismissLoadingScreen();
                        return false;
                } else if (resultable["errCode"] == "VrfyOTPErr00002") {
                	  gblVerifyOTPCounter = "0";
                    dismissLoadingScreen();
                    popOtpSpa.dismiss();
                    popTransferConfirmOTPLock.show();
                    return false;
                } else if (resultable["errCode"] == "VrfyOTPErr00005") {
                    dismissLoadingScreen();
                    alert(kony.i18n.getLocalizedString("invalidOTP"));
                    return false;
                } else if (resultable["errCode"] == "VrfyOTPErr00006") {
                    gblVerifyOTPCounter = resultable["retryCounterVerifyOTP"];
                    alert("" + resultable["errMessage"]);
                    return false;
                } else {
                    dismissLoadingScreen();
                    alert("" + resultable["errMessage"]);
                }
            } else if (resultable["code"] != null && resultable["code"] == "10403") {
                popOtpSpa.txtOTP.text = "";
                gblVerifyOTPCounter = "0";
                dismissLoadingScreen();
                popOtpSpa.dismiss();
                popTransferConfirmOTPLock.show();
            } else if (resultable["code"] != null && resultable["code"] == "10020") {
                popOtpSpa.txtOTP.text = "";
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
            } else {
                
                alert(resultable["errMsg"]);
                popOtpSpa.txtOTP.text = "";
            }
        }
        if (resultable["opstatus_changeuser"] == "0" || resultable["opstatus"] == 0) {
            dismissLoadingScreen();
            completeicon = true;
            gblRetryCountRequestOTP = "0";
            frmMyProfile.show();
        }
        else if(resultable["opstatus_changeuser"] == "0" && resultable["code"] != null && resultable["code"] == "1000")
		{
			dismissLoadingScreen();
			popOtpSpa.txtOTP.text = "";
            showAlert("" + kony.i18n.getLocalizedString("ECCreateUserExits"), null);
		}

    } 
    else {
        if (status == 300) {
            alert("Error");
        }
    }


}

function changePasswordCompositeSPA() {
    showLoadingScreen();
    var inputParams = {}
    if (gblTokenSwitchFlag == true) {
        inputParams["password"] = popOtpSpa.txttokenspa.text;
        inputParams["segmentIdVal"] = "MIB";
    } else {
        inputParams["password"] = popOtpSpa.txtOTP.text;
    }
    // this flag determine otp or token flow
    inputParams["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
    inputParams["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParams["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    //  inputParams["password"] = frmCMChgPwdSPA.tbxTranscCrntPwd.text;
    inputParams["userId"] = gblUserName;
    inputParams["retryCounterVerifyOTP"] = gblVerifyOTPCounter;



    inputParams["oldPassword"] = frmCMChgPwdSPA.tbxTranscCrntPwd.text;
    inputParams["newPassword"] = frmCMChgPwdSPA.txtConfirmPassword.text;
    // clear the text entries after input has been accepted 
    //frmCMChgPwdSPA.tbxTranscCrntPwd.text = "";
//    frmCMChgPwdSPA.txtConfirmPassword.text = "";
//    frmCMChgPwdSPA.txtTransPass.text = "";
    inputParams["custName"] = customerName;

    inputParams["Locale"] = kony.i18n.getCurrentLocale();
    var platformChannel = gblDeviceInfo.name;


    // invoking call 
    invokeServiceSecureAsync("changePasswordComposite", inputParams, callBackChangePasswordCompositeSPA);

}

function callBackChangePasswordCompositeSPA(status, resultable) {
    if (status == 400) {
        
        if (resultable["opstatus"] == 0) {
        	if (flowSpa) 
    		{
        	  	popOtpSpa.txtOTP.text="";
        	   	popOtpSpa.txttokenspa.text="";
        	}
            dismissLoadingScreen();
            completeicon = true;
            gblRetryCountRequestOTP = "0";
            frmMyProfile.show();

        } else if (resultable["opstatus_verifypwd"] != 0 && resultable["opstatus"] == 8005) {
            if (resultable["errCode"] == "VrfyOTPErr00001") {
               gblVerifyOTPCounter = "0";
			   if (gblTokenSwitchFlag == true) 
			   	{
					popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
					popOtpSpa.txttokenspa.text="";
				}
				else
				{
                	popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                	popOtpSpa.lblPopupTract4Spa.text = "";
                	popOtpSpa.txtOTP.text = "";
                }
               kony.application.dismissLoadingScreen();
               return false;
            } else if (resultable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                popOtpSpa.dismiss();
                popTransferConfirmOTPLock.show();
                return false;
            } else if (resultable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resultable["errCode"] == "VrfyOTPErr00006") {
                gblVerifyOTPCounter = resultable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                alert("" + resultable["errMsg"]);
                return false;

            } else {
                dismissLoadingScreen();
                alert("" + resultable["errMsg"]);
            }
        } else if (resultable["opstatus_verifypwd"] != 0 && resultable["code"] != null && resultable["code"] == "10403") {
	            dismissLoadingScreen();
                popOtpSpa.dismiss();
                popTransferConfirmOTPLock.show();

        } else if (resultable["opstatus_verifypwd"] != 0 && resultable["code"] != null && resultable["code"] == "10020") {
            gblVerifyOTPCounter = resultable["retryCounterVerifyOTP"];
            dismissLoadingScreen()
            showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
        } else if (resultable["opstatus_changepwd"] != 0) {
            popOtpSpa.dismiss();
            if (resultable["code"] != null && resultable["code"] == "10403") {
                popIBTransNowOTPLocked.btnClose.onClick = lockIBPwdChngOTpLockCrmUpdate;
                popIBTransNowOTPLocked.lblDetails1.text = kony.i18n.getLocalizedString("accLock");
                popIBTransNowOTPLocked.show();
            } else if (resultable["code"] != null && resultable["code"] == "10200") {
                dismissLoadingScreen();
                alert(resultable["errmsg"]);
            } else if (resultable["code"] != null && resultable["code"] == "10020") {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("keyInCrtCurPwd"), kony.i18n.getLocalizedString("info"));
            }else {
                dismissLoadingScreen();
                alert(resultable["errMsg"]);
            }
            popOtpSpa.txtOTP.text = "";
            popOtpSpa.txttokenspa.text="";
        }
    } else {
        if (status == 300) {
            alert("Error");
        }

    }

}


function callBackOTPVerifyspamob(status, resulttable) {
    popOtpSpa.txttokenspa.text = "";
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            otplocked = false;
            gblOTPFlag = true;
            gblSpaChannel = "ChangeMobileNumber";
            var locale=kony.i18n.getCurrentLocale();
            gblRetryCountRequestOTP = "0";
				
			   onClickOTPRequestSpa();
 	           caseTrans ="Number2"
            //frmMyProfile.show();
            kony.application.dismissLoadingScreen();
        } else if (resulttable["opstatus"] == 8005) {
            if (gblVerifyOTPCounter >= 3) {
                modifyUserUpdate("Disabled", "3");
                alertUserStatusLocked()
            } else {
                popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
                kony.application.dismissLoadingScreen();
            }
            /*if (resulttable["errCode"] == "VrfyOTPCtrErr00001" || resulttable["errCode"] == "VrfyOTPCtrErr00002" || resulttable["errCode"] == "VrfyOTPCtrErr00003"){
					if(resulttable["errCode"] == "VrfyOTPCtrErr00001"){
						gblVerifyOTPCounter = "1";
					}else if(resulttable["errCode"] == "VrfyOTPCtrErr00002"){
						gblVerifyOTPCounter = "2";
					}else if(resulttable["errCode"] == "VrfyOTPCtrErr00003"){
						gblVerifyOTPCounter = "3";
					}*/
            if (resulttable["errCode"] == "VrfyOTPErr00001" || resulttable["code"] == "10020") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                popOtpSpa.lblPopupTract4Spa.text = "";
                popOtpSpa.txtOTP.text = "";
                kony.application.dismissLoadingScreen();
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                gblVerifyOTPCounter = "0";
                otplocked = true;
                kony.application.dismissLoadingScreen();
                popOtpSpa.dismiss();
                popTransferConfirmOTPLock.show();
                // calling crmprofileMod to update the user status
                updteuserSpa();
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (successFlag == false && spaChnage == "activation") {
                if (gblActionCode == "11") {
                    activityLogServiceCall("004", "", "02", "", "", "Fail", "", "", "", "");
                } else if (gblActionCode == "12") {
                    activityLogServiceCall("014", "", "02", "", "", "Fail", "", "", "", "");
                }
            } else {
                kony.application.dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            kony.application.dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        kony.application.dismissLoadingScreen();
    }
}
