function postShowCardList(){
	gblManageCardFlow = kony.application.getCurrentForm().id;
}

function frmCardListLocalChange(){
	changeStatusBarColor();
	var cardType = "";
	var cardCode = "";
	
	if(gblCardList == null || gblCardList == "" || gblCardList == undefined){
		return;
	}
	
	frmMBCardList.lblCardList.text = kony.i18n.getLocalizedString("Menu_keyCard");
	
	if (gblCardList["creditCardRec"].length > 0) {
        cardType = gblCardList["creditCardRec"][0]["cardType"]; 
        cardCode = setCardId(cardType)
        frmMBCardList["lblSectionHeader" + cardCode].text = getCardHeaderText(cardType);
		for(var index = 0; index < gblCardList["creditCardRec"].length; index++){
			cardStatus = gblCardList["creditCardRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBCardList["lblLinkText" + cardCode + index].text = linkText;
		}
	}     
	if (gblCardList["debitCardRec"].length > 0) {
        cardType = gblCardList["debitCardRec"][0]["cardType"];
        cardCode = setCardId(cardType)
        frmMBCardList["lblSectionHeader" + cardCode].text = getCardHeaderText(cardType);
		for(var index = 0; index < gblCardList["debitCardRec"].length; index++){
			cardStatus = gblCardList["debitCardRec"][index]["cardStatus"];
			allowManageCard = gblCardList["debitCardRec"][index]["allowManageCard"];
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBCardList["lblLinkText" + cardCode + index].text = linkText;
		}
	}
	if (gblCardList["readyCashRec"].length > 0) {
        cardType = gblCardList["readyCashRec"][0]["cardType"];
        cardCode = setCardId(cardType)
        frmMBCardList["lblSectionHeader" + cardCode].text = getCardHeaderText(cardType);
    	for(var index = 0; index < gblCardList["readyCashRec"].length; index++){
			cardStatus = gblCardList["readyCashRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBCardList["lblLinkText" + cardCode + index].text = linkText;
		}
    }  
}
function frmDebitCardListLocalChange(){
	changeStatusBarColor();
	var cardType = "";
	var cardCode = "";
	
	if(gblCardList == null || gblCardList == "" || gblCardList == undefined){
		return;
	}
	
	frmMBListDebitCard.lblDebitCard.text = kony.i18n.getLocalizedString("DBA01_Title");
	 
	if (gblCardList["debitCardRec"].length > 0) {
        cardType = gblCardList["debitCardRec"][0]["cardType"];
        cardCode = setCardId(cardType)
		for(var index = 0; index < gblCardList["debitCardRec"].length; index++){
			cardStatus = gblCardList["debitCardRec"][index]["cardStatus"];
			allowManageCard = gblCardList["debitCardRec"][index]["allowManageCard"];
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBListDebitCard["lblLinkText" + cardCode + index].text = linkText;
		}
	}
	if(canIssueCard()){
		frmMBListDebitCard["btnIssueCard"].text = kony.i18n.getLocalizedString("keyIssueNC");
	}
	frmMBListDebitCard.buttonBack.text = kony.i18n.getLocalizedString("Back");
	
}
function frmCreditCardListLocalChange(){
	changeStatusBarColor();
	var cardType = "";
	var cardCode = "";
	
	if(gblCardList == null || gblCardList == "" || gblCardList == undefined){
		return;
	}
	 
	if (gblCardList["creditCardRec"].length > 0) {
		frmMBListCreditCard.lblCreditCard.text = kony.i18n.getLocalizedString("keyCreditCard");
        cardType = gblCardList["creditCardRec"][0]["cardType"];
        cardCode = setCardId(cardType)
		for(var index = 0; index < gblCardList["creditCardRec"].length; index++){
			cardStatus = gblCardList["creditCardRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBListCreditCard["lblLinkText" + cardCode + index].text = linkText;
		}
	} else if (gblCardList["readyCashRec"].length > 0) {
		frmMBListCreditCard.lblCreditCard.text = kony.i18n.getLocalizedString("keyReadyCashCard");
        cardType = gblCardList["readyCashRec"][0]["cardType"];
        cardCode = setCardId(cardType)
		for(var index = 0; index < gblCardList["readyCashRec"].length; index++){
			cardStatus = gblCardList["readyCashRec"][index]["cardStatus"];
			allowManageCard = "";
			linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
			frmMBListCreditCard["lblLinkText" + cardCode + index].text = linkText;
		}
	}
	frmMBListCreditCard.buttonBack.text = kony.i18n.getLocalizedString("Back");
}