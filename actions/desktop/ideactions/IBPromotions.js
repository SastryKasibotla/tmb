
function onClickPromotionsIB(){
	try{
		hbxFooterPrelogin.linkhotpromo.skin = footerLinkHP;
		hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
		gblVisible = false;
		var inputParams = {};
		showLoadingScreenPopup();
		promotionSearchKeyIB = "All Promotions";
		promotionFilterIB = false;
		selectedValue = 1;
		var temp=[];
		temp.push(["1",kony.i18n.getLocalizedString("Promotions_AllPromotions")]);
		temp.push(["2",kony.i18n.getLocalizedString("Promotions_Deposit")]);
		temp.push(["3",kony.i18n.getLocalizedString("Promotions_Loan")]);
		temp.push(["4",kony.i18n.getLocalizedString("Promotions_CreditCard")]);
		temp.push(["5",kony.i18n.getLocalizedString("Promotions_Others")]);
		frmIBPromotions.btnPromotions.masterData=temp;
		frmIBPromotions.btnPromotions.selectedKey= selectedValue;
		frmIBPromotions.btnPromotions.selectedKey= selectedValue;
		
		inputParams["crmId"] = gblcrmId;
		inputParams["appChannel"] = "I";
		invokeServiceSecureAsync("GetHotPromotion", inputParams, successCallBackPromotionsIB)
	}catch(e){
		
	}
}

function isValidPromotionCode(){
		var cmpType = gblCampaignResp.split("~")[1];
		var cmpCode = gblCampaignResp.split("~")[2];
		for(var i = 0; i < gblPromotionsIB["hotPromotions"].length; i++){
			if(cmpCode == gblPromotionsIB["hotPromotions"][i]["cmpCode"] && cmpType == gblPromotionsIB["hotPromotions"][i]["cmpType"]){
				return true;
			}
		}	
		alert(kony.i18n.getLocalizedString("keyCampaignInboxNotFound"));
		dismissLoadingScreenPopup();
		return false;
}

function successCallBackPromotionsIB(status, resulttable) {
    try {
        
        if (!promotionFilterIB) {
            gblPromotionsIB = resulttable;
            if(gblMyOffersDetails){
				if(!isValidPromotionCode()){
					return;
				}
            }
        }
        var promotionsReslutSetIB = [];
        
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                for (var i = 0; i < resulttable["hotPromotions"].length; i++) {
                    var tempPromotionData = [];
                    var subject = "" , shorMsg = "" , imageIcon = "";
                    var detailsImg1 = "" , detailsImg2 = "" , detailsImg3 = "" , description = "";
                    var searchKey = "" , prmText1 = "" , prmText2 = "" , prmText3 = "" , prmText4 = "" , prmText5 = "";
                    var cmpCode = "" , cmpType = "" , cmpEndDate = "" , cmpIntExt = "";
                    
                    if (promotionFilterIB == true) {
                        searchKey = resulttable["hotPromotions"][i]["productOffer"];
                    } else {
                        searchKey = "";
                    }
                    
                    if (promotionSearchKeyIB == "All Promotions") {
                        cmpEndDate = resulttable["hotPromotions"][i]["cmpEndDate"];
                        cmpCode = resulttable["hotPromotions"][i]["cmpCode"];
                        cmpType = resulttable["hotPromotions"][i]["cmpType"];
                        cmpIntExt = resulttable["hotPromotions"][i]["hotpromoLinkFlagInternet"];
                        if (kony.i18n.getCurrentLocale() == "en_US") {
                            subject = resulttable["hotPromotions"][i]["subjectEN"];
                            shorMsg = resulttable["hotPromotions"][i]["shortMsgEN"];
                            imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1InternetEN"];
                            detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1InternetEN"];
                            detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2InternetEN"];
                            detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3InternetEN"];
                            description = resulttable["hotPromotions"][i]["longMsgLine1EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine2EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine3EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine4EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine5EN"];
                            prmText1 = resulttable["hotPromotions"][i]["longMsgLine1EN"];
                            prmText2 = resulttable["hotPromotions"][i]["longMsgLine2EN"];
                            prmText3 = resulttable["hotPromotions"][i]["longMsgLine3EN"];
                            prmText4 = resulttable["hotPromotions"][i]["longMsgLine4EN"];
                            prmText5 = resulttable["hotPromotions"][i]["longMsgLine5EN"];
                            gblCampaignDataEN1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetEN"];
							gblCampaignDataTH1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetTH"];							
                        } else if (kony.i18n.getCurrentLocale() == "th_TH") {
                            subject = resulttable["hotPromotions"][i]["subjectTH"];
                            shorMsg = resulttable["hotPromotions"][i]["shortMsgTH"];
                            imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1InternetTH"];
                            detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1InternetTH"];
                            detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2InternetTH"];
                            detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3InternetTH"];
                            description = resulttable["hotPromotions"][i]["longMsgLine1TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine2TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine3TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine4TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine5TH"];
                            prmText1 = resulttable["hotPromotions"][i]["longMsgLine1TH"];
                            prmText2 = resulttable["hotPromotions"][i]["longMsgLine2TH"];
                            prmText3 = resulttable["hotPromotions"][i]["longMsgLine3TH"];
                            prmText4 = resulttable["hotPromotions"][i]["longMsgLine4TH"];
                            prmText5 = resulttable["hotPromotions"][i]["longMsgLine5TH"];
                            gblCampaignDataEN1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetEN"];
							gblCampaignDataTH1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetTH"];
                        }
                        tempPromotionData = {
                            imgprpic: {
                                src: imageIcon
                            },
                            lblPromTitle: subject,
                            lblPrmDetail: shorMsg,
                            imgArrow: "bg_arrow_right.png",
                            desc: description,
                            detailsImage1: detailsImg1,
                            detailsImage2: detailsImg2,
                            detailsImage3: detailsImg3,
                            promoText1: prmText1,
                            promoText2: prmText2,
                            promoText3: prmText3,
                            promoText4: prmText4,
                            promoText5: prmText5,
                            campCode: cmpCode,
                            camType: cmpType,
                            cmpIntExt: cmpIntExt,
                            gblCampaignDataEN2: gblCampaignDataEN1,
                            gblCampaignDataTH2: gblCampaignDataTH1,
                            cmpEndDate : cmpEndDate
                        };
                        promotionsReslutSetIB.push(tempPromotionData);
                    } else if (searchKey == promotionSearchKeyIB) {
                        cmpEndDate = resulttable["hotPromotions"][i]["cmpEndDate"];
                        cmpCode = resulttable["hotPromotions"][i]["cmpCode"];
                        cmpType = resulttable["hotPromotions"][i]["cmpType"];
                        cmpIntExt = resulttable["hotPromotions"][i]["hotpromoLinkFlagInternet"];
                        if (kony.i18n.getCurrentLocale() == "en_US") {
                            subject = resulttable["hotPromotions"][i]["subjectEN"];
                            shorMsg = resulttable["hotPromotions"][i]["shortMsgEN"];
                            imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1InternetEN"];
                            detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1InternetEN"];
                            detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2InternetEN"];
                            detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3InternetEN"];
                            description = resulttable["hotPromotions"][i]["longMsgLine1EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine2EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine3EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine4EN"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine5EN"];
                            prmText1 = resulttable["hotPromotions"][i]["longMsgLine1EN"];
                            prmText2 = resulttable["hotPromotions"][i]["longMsgLine2EN"];
                            prmText3 = resulttable["hotPromotions"][i]["longMsgLine3EN"];
                            prmText4 = resulttable["hotPromotions"][i]["longMsgLine4EN"];
                            prmText5 = resulttable["hotPromotions"][i]["longMsgLine5EN"];
                            gblCampaignDataEN1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetEN"];
							gblCampaignDataTH1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetTH"];
                        } else if (kony.i18n.getCurrentLocale() == "th_TH") {
                            subject = resulttable["hotPromotions"][i]["subjectTH"];
                            shorMsg = resulttable["hotPromotions"][i]["shortMsgTH"];
                            imageIcon = resulttable["hotPromotions"][i]["imgHotpromoIcon1InternetTH"];
                            detailsImg1 = resulttable["hotPromotions"][i]["imgHotpromoId1InternetTH"];
                            detailsImg2 = resulttable["hotPromotions"][i]["imgHotpromoId2InternetTH"];
                            detailsImg3 = resulttable["hotPromotions"][i]["imgHotpromoId3InternetTH"];
                            description = resulttable["hotPromotions"][i]["longMsgLine1TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine2TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine3TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine4TH"] + "<br/>" + resulttable["hotPromotions"][i]["longMsgLine5TH"];
                            prmText1 = resulttable["hotPromotions"][i]["longMsgLine1TH"];
                            prmText2 = resulttable["hotPromotions"][i]["longMsgLine2TH"];
                            prmText3 = resulttable["hotPromotions"][i]["longMsgLine3TH"];
                            prmText4 = resulttable["hotPromotions"][i]["longMsgLine4TH"];
                            prmText5 = resulttable["hotPromotions"][i]["longMsgLine5TH"];
                            gblCampaignDataEN1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetEN"];
							gblCampaignDataTH1 = resulttable["hotPromotions"][i]["hotpromoLinkInternetTH"];
                        }
                        tempPromotionData = {
                            imgprpic: {
                                src: imageIcon
                            },
                            lblPromTitle: subject,
                            lblPrmDetail: shorMsg,
                            imgArrow: "bg_arrow_right.png",
                            desc: description,
                            detailsImage1: detailsImg1,
                            detailsImage2: detailsImg2,
                            detailsImage3: detailsImg3,
                            promoText1: prmText1,
                            promoText2: prmText2,
                            promoText3: prmText3,
                            promoText4: prmText4,
                            promoText5: prmText5,
                            campCode: cmpCode,
                            camType: cmpType,
                            cmpIntExt: cmpIntExt,
                            gblCampaignDataEN2: gblCampaignDataEN1,
                            gblCampaignDataTH2: gblCampaignDataTH1,
                            cmpEndDate : cmpEndDate
                        };
                        promotionsReslutSetIB.push(tempPromotionData);
                    }
                   
                } //End of for loop.
                
                frmIBPromotions.segPromotions.setData(promotionsReslutSetIB);
                if (frmIBPromotions.hboxPromotionDetails.isVisible) {
                    frmIBPromotions.hboxImage.setVisibility(true);
                    frmIBPromotions.hboxPromotionDetails.setVisibility(false);
                }
                if(promotionLangToggle){
                	if(gblVisible == false)
                	{
                		frmIBPromotions.hboxImage.setVisibility(true);
                		frmIBPromotions.hboxPromotionDetails.setVisibility(false);
                	}
                	else
                	{
                		frmIBPromotions.segPromotions.selectedIndex = gblPromotionSelectedIndex;
                		if(gblMyOffersDetails){
                			directPromotionDetailsView('Y');
                		} else {
                			onClickPromotionSegIB('Y');
                		}                		
                		promotionLangToggle=false;
                	}
                } else {
                	if(gblMyOffersDetails){
                		directPromotionDetailsView('N');
                	}
                }
                if (promotionsReslutSetIB.length == 0) {
                    
                    dismissLoadingScreenPopup();
                    resetFooterSkins();
                    alert(kony.i18n.getLocalizedString("Promotions_noData"));
                } else {
                    dismissLoadingScreenPopup();
                    frmIBPromotions.show();
           		  } 
                }else {
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("ECGenericError"));
                    resetFooterSkins();
                }
            }
        } 
		catch (e) {
            
        }
    }


function onClickPromotionSegIB(langSwitch){
	try{
		gblMyOffersDetails = false;
		gblVisible=true;
		var cmpType=frmIBPromotions.segPromotions.selectedItems[0].camType;
		
		if(cmpType !='M'){
			if(langSwitch == 'N'){
				gblCampaignResp = "frmIBPromotions" + "~" + frmIBPromotions.segPromotions.selectedItems[0].camType + "~" + frmIBPromotions.segPromotions.selectedItems[0].campCode + "~" + frmIBPromotions.segPromotions.selectedItems[0].cmpEndDate + "~" + "01" + "~" + "O";
				campaignBannerIBCount("D","O");
			}
		}			
		 			
		if(frmIBPromotions.hboxImage.isVisible){
			frmIBPromotions.hboxImage.setVisibility(false);
			frmIBPromotions.hboxPromotionDetails.setVisibility(true);
		}
		frmIBPromotions.lblTitle.text = frmIBPromotions.segPromotions.selectedItems[0].lblPromTitle;
		frmIBPromotions.rchPromDetails.text = frmIBPromotions.segPromotions.selectedItems[0].desc;
		var imageData = [];
		imageURL1IB = frmIBPromotions.segPromotions.selectedItems[0].detailsImage1;
		imageURL2IB = frmIBPromotions.segPromotions.selectedItems[0].detailsImage2;
		imageURL3IB = frmIBPromotions.segPromotions.selectedItems[0].detailsImage3;
		
		//commented below line and added below if conditions to fix DEF419
		//imageData = [{imgProm: {src: imageURL1IB}},{imgProm: {src: imageURL2IB}},{imgProm: {src: imageURL3IB}}];
		if(imageURL1IB != ""){
			imageData.push({imgProm: {src: imageURL1IB}})
		}
		if(imageURL2IB != ""){
			imageData.push({imgProm: {src: imageURL2IB}})
		}
		if(imageURL3IB != ""){
			imageData.push({imgProm: {src: imageURL3IB}})
		}
		if(imageURL1IB != '' || imageURL2IB != '' || imageURL3IB != ''){
			frmIBPromotions.segPromImage.setVisibility(true);
			frmIBPromotions.segPromImage.setData(imageData);
			frmIBPromotions.segPromImage.selectedIndex = [0,0];
			setPromotionsTimerIB();
		} else {
			frmIBPromotions.segPromImage.setVisibility(false);
		}
		gblCmpIntExt=frmIBPromotions.segPromotions.selectedItems[0].cmpIntExt;
		if(gblCmpIntExt != "00" && gblCmpIntExt != "04") {
			 frmIBPromotions.hbxClick.setVisibility(true);
			 frmIBPromotions.btnIBpromo.setVisibility(true);
		} else {
			 frmIBPromotions.hbxClick.setVisibility(false);
		}
		if(gblCmpIntExt != "00" && gblCmpIntExt != "04"){
			gblCampaignDataEN = frmIBPromotions.segPromotions.selectedItems[0].gblCampaignDataEN2;
			gblCampaignDataTH = frmIBPromotions.segPromotions.selectedItems[0].gblCampaignDataTH2;
		} 
	}catch(e){
		
	}
}
gblOfferDetailsObj = {};
function directPromotionDetailsView(langSwitch){
	try{
		gblVisible=true;
		gblOfferDetailsObj = {};
		var cmpType = gblCampaignResp.split("~")[1];
		var cmpCode = gblCampaignResp.split("~")[2];
		//alert("cmpType : "+cmpType+" , cmpCode : "+cmpCode);
		var cmpEndDate  = gblCampaignResp.split("~")[3];		
		
		if(undefined != gblPromotionsIB && null != gblPromotionsIB){
			var count = 0;
			for(var i = 0; i < gblPromotionsIB["hotPromotions"].length; i++){
				if(cmpCode == gblPromotionsIB["hotPromotions"][i]["cmpCode"] && cmpType == gblPromotionsIB["hotPromotions"][i]["cmpType"]){
					count = 1;
					gblOfferDetailsObj.cmpCode = gblPromotionsIB["hotPromotions"][i]["cmpCode"];
					gblOfferDetailsObj.cmpType = gblPromotionsIB["hotPromotions"][i]["cmpType"];
					gblOfferDetailsObj.cmpEndDate = gblPromotionsIB["hotPromotions"][i]["cmpEndDate"];
					gblOfferDetailsObj.cmpIntExt = gblPromotionsIB["hotPromotions"][i]["hotpromoLinkFlagInternet"];
					if (kony.i18n.getCurrentLocale() == "en_US") {
						gblOfferDetailsObj.lblPromTitle = gblPromotionsIB["hotPromotions"][i]["subjectEN"];
						gblOfferDetailsObj.desc = gblPromotionsIB["hotPromotions"][i]["longMsgLine1EN"] + "<br/>" + gblPromotionsIB["hotPromotions"][i]["longMsgLine2EN"] 
											+ "<br/>" + gblPromotionsIB["hotPromotions"][i]["longMsgLine3EN"] + "<br/>" 
											+ gblPromotionsIB["hotPromotions"][i]["longMsgLine4EN"] 
											+ "<br/>" + gblPromotionsIB["hotPromotions"][i]["longMsgLine5EN"];
						gblOfferDetailsObj.detailsImage1 = gblPromotionsIB["hotPromotions"][i]["imgHotpromoId1InternetEN"];
						gblOfferDetailsObj.detailsImage2 = gblPromotionsIB["hotPromotions"][i]["imgHotpromoId2InternetEN"];
						gblOfferDetailsObj.detailsImage3 = gblPromotionsIB["hotPromotions"][i]["imgHotpromoId3InternetEN"];
						
						gblOfferDetailsObj.promoText1 = gblPromotionsIB["hotPromotions"][i]["longMsgLine1EN"];
						gblOfferDetailsObj.promoText2 = gblPromotionsIB["hotPromotions"][i]["longMsgLine2EN"];
						gblOfferDetailsObj.promoText3 = gblPromotionsIB["hotPromotions"][i]["longMsgLine3EN"];
						gblOfferDetailsObj.promoText4 = gblPromotionsIB["hotPromotions"][i]["longMsgLine4EN"];
						gblOfferDetailsObj.promoText4 = gblPromotionsIB["hotPromotions"][i]["longMsgLine5EN"];
					} else if (kony.i18n.getCurrentLocale() == "th_TH") {
						gblOfferDetailsObj.lblPromTitle = gblPromotionsIB["hotPromotions"][i]["subjectTH"];
						gblOfferDetailsObj.desc = gblPromotionsIB["hotPromotions"][i]["longMsgLine1TH"] + "<br/>" + gblPromotionsIB["hotPromotions"][i]["longMsgLine2TH"] 
											+ "<br/>" + gblPromotionsIB["hotPromotions"][i]["longMsgLine3TH"] + "<br/>" 
											+ gblPromotionsIB["hotPromotions"][i]["longMsgLine4TH"] 
											+ "<br/>" + gblPromotionsIB["hotPromotions"][i]["longMsgLine5TH"];
						gblOfferDetailsObj.detailsImage1 = gblPromotionsIB["hotPromotions"][i]["imgHotpromoId1InternetTH"];
						gblOfferDetailsObj.detailsImage2 = gblPromotionsIB["hotPromotions"][i]["imgHotpromoId2InternetTH"];
						gblOfferDetailsObj.detailsImage3 = gblPromotionsIB["hotPromotions"][i]["imgHotpromoId3InternetTH"];
						
						gblOfferDetailsObj.promoText1 = gblPromotionsIB["hotPromotions"][i]["longMsgLine1TH"];
						gblOfferDetailsObj.promoText2 = gblPromotionsIB["hotPromotions"][i]["longMsgLine2TH"];
						gblOfferDetailsObj.promoText3 = gblPromotionsIB["hotPromotions"][i]["longMsgLine3TH"];
						gblOfferDetailsObj.promoText4 = gblPromotionsIB["hotPromotions"][i]["longMsgLine4TH"];
						gblOfferDetailsObj.promoText4 = gblPromotionsIB["hotPromotions"][i]["longMsgLine5TH"];
					} 
					gblOfferDetailsObj.gblCampaignDataEN2 = gblPromotionsIB["hotPromotions"][i]["hotpromoLinkInternetEN"];
					gblOfferDetailsObj.gblCampaignDataTH2 = gblPromotionsIB["hotPromotions"][i]["hotpromoLinkInternetTH"];
				}
				//already one recod is matched with cmpCode and cmpType, no need to iterate entire records.
				if(count == 1){
					//if cmpType is not M then capturing display count for my offers
					if(cmpType != 'M'){
						if(langSwitch == 'N'){
							gblCampaignResp = "frmIBPromotions" + "~" + cmpType + "~" + cmpCode + "~" + cmpEndDate + "~" + "01" + "~" + "O";
							campaignBannerIBCount("D","O");
						}
					}
					break;
				}
			}
		}
					
		if(frmIBPromotions.hboxImage.isVisible){
			frmIBPromotions.hboxImage.setVisibility(false);
			frmIBPromotions.hboxPromotionDetails.setVisibility(true);
		}
		frmIBPromotions.lblTitle.text = gblOfferDetailsObj.lblPromTitle;
		frmIBPromotions.rchPromDetails.text = gblOfferDetailsObj.desc;
		var imageData = [];
		imageURL1IB = gblOfferDetailsObj.detailsImage1;
		imageURL2IB = gblOfferDetailsObj.detailsImage2;
		imageURL3IB = gblOfferDetailsObj.detailsImage3;
		
		//commented below line and added below if conditions to fix DEF419
		//imageData = [{imgProm: {src: imageURL1IB}},{imgProm: {src: imageURL2IB}},{imgProm: {src: imageURL3IB}}];
		if(imageURL1IB != ""){
			imageData.push({imgProm: {src: imageURL1IB}})
		}
		if(imageURL2IB != ""){
			imageData.push({imgProm: {src: imageURL2IB}})
		}
		if(imageURL3IB != ""){
			imageData.push({imgProm: {src: imageURL3IB}})
		}
		if(imageURL1IB != '' || imageURL2IB != '' || imageURL3IB != ''){
			frmIBPromotions.segPromImage.setVisibility(true);
			frmIBPromotions.segPromImage.setData(imageData);
			frmIBPromotions.segPromImage.selectedIndex = [0,0];
			setPromotionsTimerIB();
		} else {
			frmIBPromotions.segPromImage.setVisibility(false);
		}
		gblCmpIntExt = gblOfferDetailsObj.cmpIntExt;
		if(gblCmpIntExt != "00" && gblCmpIntExt != "04") {
			 frmIBPromotions.hbxClick.setVisibility(true);
			 frmIBPromotions.btnIBpromo.setVisibility(true);
		} else {
			 frmIBPromotions.hbxClick.setVisibility(false);
		}
		if(gblCmpIntExt != "00" && gblCmpIntExt != "04"){
			gblCampaignDataEN = gblOfferDetailsObj.gblCampaignDataEN2;
			gblCampaignDataTH = gblOfferDetailsObj.gblCampaignDataTH2;
		} 
	}catch(e){
		alert(e);
	}
}

function setPromotionsTimerIB(){
	try{
		
		kony.timer.schedule("promoTimerIB", setTimeforPromImageIB, 5, true);
	}catch(e){
		
	}
}

function setTimeforPromImageIB(){
	try{
		
		var index = frmIBPromotions.segPromImage.selectedIndex[1];
		if(index == 2){
			index = -1;
		}
		index = index+1;
		frmIBPromotions.segPromImage.selectedIndex = [0,index];
	}catch(e){
		
		//kony.timer.cancel("promoTimerIB");
	}
}

function onSelectPromtionSearch(){
	try{
		selectedValue = frmIBPromotions.btnPromotions.selectedKey;
		promotionFilterIB = true;
		if(selectedValue == 1){
			promotionSearchKeyIB = "All Promotions";
		}else if(selectedValue == 2){
			promotionSearchKeyIB = "Deposit";
		}else if(selectedValue == 3){
			promotionSearchKeyIB = "Loan";
		}else if(selectedValue == 4){
			promotionSearchKeyIB = "Credit Card";
		}else if(selectedValue == 5){
			promotionSearchKeyIB = "Others";
		}
		
		successCallBackPromotionsIB(400, gblPromotionsIB)
	}catch(e){
		
	}
}


function syncIBHotPromotions(){
	try{
		
		if (kony.application.getCurrentForm().id == "frmIBPromotions"){
			var temp=[];
			temp.push(["1",kony.i18n.getLocalizedString("Promotions_AllPromotions")]);
			temp.push(["2",kony.i18n.getLocalizedString("Promotions_Deposit")]);
			temp.push(["3",kony.i18n.getLocalizedString("Promotions_Loan")]);
			temp.push(["4",kony.i18n.getLocalizedString("Promotions_CreditCard")]);
			temp.push(["5",kony.i18n.getLocalizedString("Promotions_Others")]);
			frmIBPromotions.btnPromotions.masterData=temp;
			frmIBPromotions.btnPromotions.selectedKey= selectedValue;
			gblPromotionSelectedIndex = frmIBPromotions.segPromotions.selectedIndex;
            promotionLangToggle = true;    	
			successCallBackPromotionsIB(400, gblPromotionsIB)
		}
	}catch(e){
		
	}
}


function savePromotionsPDFIB(filetype) {
	try{
		
		var inputParam = {};
		var dynamicdata = {};
		showLoadingScreenPopup();
		var currentLocale = kony.i18n.getCurrentLocale();
		//gblMyOffersDetails is set to true, if my offer details page is set as internal link any campaign screen.
		if(gblMyOffersDetails){
			dynamicdata={
		   			 "image1URL" : imageURL1IB,
		      		 "image2URL" : imageURL2IB,
		   			 "image3URL" : imageURL3IB,
		       		 "para1text" : gblOfferDetailsObj.promoText1,
		  			 "para2text" : gblOfferDetailsObj.promoText2,
		  			 "para3text" : gblOfferDetailsObj.promoText3,
		  			 "para4text" : gblOfferDetailsObj.promoText4,
		  			 "para5text" : gblOfferDetailsObj.promoText5,
		  			 "localeId" : currentLocale,
		  			 "tittle" : gblOfferDetailsObj.lblPromTitle
		    		};
		} else {
			dynamicdata={
			   			 "image1URL" : imageURL1IB,
			      		 "image2URL" : imageURL2IB,
			   			 "image3URL" : imageURL3IB,
			       		 "para1text" : frmIBPromotions.segPromotions.selectedItems[0].promoText1,
			  			 "para2text" : frmIBPromotions.segPromotions.selectedItems[0].promoText2,
			  			 "para3text" : frmIBPromotions.segPromotions.selectedItems[0].promoText3,
			  			 "para4text" : frmIBPromotions.segPromotions.selectedItems[0].promoText4,
			  			 "para5text" : frmIBPromotions.segPromotions.selectedItems[0].promoText5,
			  			 "localeId" : currentLocale,
			  			 "tittle" : frmIBPromotions.segPromotions.selectedItems[0].lblPromTitle
			    		};
		}
		inputParam["outputtemplatename"]= "promotionaldetails";
		inputParam["filetype"] = filetype;
		inputParam["templatename"] = "PromotionDetailsTemplate";
		inputParam["nowatermark"] = "Y";
		inputParam["datajson"] = JSON.stringify(dynamicdata, "", "");
				
		invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
	}catch(e){
		
	}
}

function promotionShareOnFB(){
	try{
		
		var message = "TMBPromotions";//frmIBPromotions.segPromotions.selectedItems[0].promoText1+" "+frmIBPromotions.segPromotions.selectedItems[0].promoText2+" "+frmIBPromotions.segPromotions.selectedItems[0].promoText3+" "+frmIBPromotions.segPromotions.selectedItems[0].promoText4+" "+frmIBPromotions.segPromotions.selectedItems[0].promoText5;
		gblPOWcustNME = "TMB User"
		gblPOWtransXN = message;
		gblPOWchannel = "IB"
		requestfromform = "frmIBPromotions"
		postOnWall();
	}catch(e){
		
	}
}

function promotionsfooterLinks(){ 
	try{
		var locale = kony.i18n.getCurrentLocale();
		if(gblIsNewOffersExists){ //alert(" promotionsfooterLinks ");          	 
       		hbxFooterPrelogin.linkHotPromoImg.isVisible = true;
        } else {
        	hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
        	
        }
		//frmIBExchangeRates.hbxFooterPrelogin.linkhotpromo.skin = "footerLinkHPFoc";
		frmIBExchangeRates.hbxFooterPrelogin.linkFindTmb.skin = "footerLinkFind";
		frmIBExchangeRates.hbxFooterPrelogin.linkExchangeRate.skin = "footerLinkER";
		frmIBExchangeRates.hbxFooterPrelogin.linkSiteTour.skin = "footerLinkST";
		frmIBExchangeRates.hbxFooterPrelogin.linkTnC.skin = "footerLinkTC";
		frmIBExchangeRates.hbxFooterPrelogin.linkSecurity.skin = "footerLinkSecurity";
	}catch(e){
		
	}
}

function onClickOfMyOffersLink(){
	//below variable is set to true when internal link set to my offers details directly
	gblMyOffersDetails = false;
	gblIsNewOffersExists = false; 
	promotionsfooterLinks();
	onClickPromotionsIB();
}