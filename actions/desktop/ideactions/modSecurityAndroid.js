



gblPermissionsJSONObj = {
	"PHONE_GROUP":"android.permission.READ_PHONE_STATE",
	"STORAGE_GROUP":"android.permission.WRITE_EXTERNAL_STORAGE",
	"LOCATION_GROUP":"android.permission.ACCESS_FINE_LOCATION",
	"CAMERA_GROUP":"android.permission.CAMERA",
	"CONTACTS_GROUP":"android.permission.READ_CONTACTS"
}


function chkRunTimePermissionsForTrusteer(){
	if(isAndroidM()){
		kony.print("GOWRI: CALLIING PERMISSIONS FFI....");
		var permissionsArr=[gblPermissionsJSONObj.PHONE_GROUP,gblPermissionsJSONObj.STORAGE_GROUP];
		var launchPermMsg="Trusteer";
		//Creates an object of class 'PermissionFFI'
		var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
		//Invokes method 'checkpermission' on the object
		RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr,launchPermMsg,MarshmallowPermissionCheckResults);
	}else{
		trusteerIntialize();
	}
}

function isAndroidM(){
	//#ifdef android
		var droidVersion = kony.os.deviceInfo().version;
		kony.print("FPRINT here1 MARSHMALLOW....Checking.....droidVersion="+droidVersion);
		droidVersion = droidVersion.replace(/\./g, "");
		kony.print("FPRINT here2 MARSHMALLOW....Checking.....droidVersion="+droidVersion);
		if(droidVersion.length == 2) droidVersion = droidVersion + "0";
		kony.print("FPRINT here3 MARSHMALLOW....Checking.....droidVersion="+droidVersion);
		if(parseInt(droidVersion) < 600) {
			kony.print("FPRINT NOT A MARSHMALLOW DEVICE....");
			return false;
		}
		return true;
	//#else
		return false;
	//#endif
}


function MarshmallowPermissionCheckResults(result) {
	if(result == "1"){
		kony.print("GOWRI: IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
		trusteerIntialize();
		/*if(TmbTrusteerFFIObject == null)
			TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
			//Invokes method 'intialize_trusteer' on the object
			TmbTrusteerFFIObject.intialize_trusteer(callbackInit);*/
	}else{
		kony.print("GOWRI: IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
		var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
		showAlertForUniqueGenFail(messageErr);
	}
}



var TmbTrusteerFFIObject;

function intialize(){
showLoadingScreen();
kony.print("GOWRI: TRUSTEER INITIALIZATION HAS STARTED!")
chkRunTimePermissionsForTrusteer();
}

function trusteerIntialize(){
kony.print("GOWRI: IN FtrusteerIntialize");
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//Invokes method 'intialize_trusteer' on the object
TmbTrusteerFFIObject.intialize_trusteer(callbackInit);
}

/*

var TmbTrusteerFFIObject;
function intialize(){
showLoadingScreen();
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//Invokes method 'intialize_trusteer' on the object
TmbTrusteerFFIObject.intialize_trusteer(callbackInit);
}

*/


//function checkSuspiciousApp(){
////Creates an object of class 'TmbTrusteerFFI'
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.checkSuspiciousApp(
//		/**Function*/ callbackSuspicious);
//}
function checkMallware(){

//Creates an object of class 'TmbTrusteerFFI'
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//Invokes method 'checkMallware' on the object
TmbTrusteerFFIObject.checkMallware(
		/**Function*/ callbackMal);
}
//function checkRoot(){
//
//		//Creates an object of class 'TmbTrusteerFFI'
// if(TmbTrusteerFFIObject == null)
// TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
////Invokes method 'getRootValue' on the object
//TmbTrusteerFFIObject.getRoot(
//		/**Function*/ callbackRoot);
//
//}
//function checkRiskScore(){
//
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//////Invokes method 'getRiskAssessmentValue' on the object
//TmbTrusteerFFIObject.getAllResult(
//		/**Function*/ callbackRisk);
//
//}
//function getAllSecurityInfo(){
//
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.getAllResult(
//		/**Function*/ callbackAll);
//}
//function getAppRestrictValue(){
//
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.getAppRestrict(
//		/**Function*/ callbackApp);
//}
function getUniqueID(){
    if(TmbTrusteerFFIObject == null)
        TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
    else
        TmbTrusteerFFIObject.getUniqueID(/**Function*/ callbackUnique);
}

function getConfigUpdateValue(){
    if(TmbTrusteerFFIObject == null)
        TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
        TmbTrusteerFFIObject.getconfig(/**Function*/ callbackConfig);
}

function sslValidationAndroid(){
    if(TmbTrusteerFFIObject == null) TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
    var url = appConfig.serverIp;
    TmbTrusteerFFIObject.sslValidationAndroid(/**Function*/ callbackSSL,url);
}

function startConfigUpdate(){
//Invokes method 'startConfigupdate' on the object
if(TmbTrusteerFFIObject == null)
TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
TmbTrusteerFFIObject.startConfigupdate();
}
//function checkOSversion(){
//if(TmbTrusteerFFIObject == null)
//TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
//TmbTrusteerFFIObject.getOsUpdatedValue(
//		/**Function*/ callbackOsUpdate);
//}

function callbackOsUpdate(osResult){

}
//function callbackRisk(riskResult){
//
//}

function callbackRoot(rootResult){
	
	if(rootResult == 0){
		GBL_rootedFlag =0;
	}else{
		GBL_rootedFlag =1;
	}
}






function startSecurutyValidations()
{
	sslValidationAndroid();
}
function callbackSSL(sslResult){
	if(sslResult == 0){
		//getConfigUpdateValue();
	} else {
		/*showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		dismissLoadingScreen();*/
		
		//Network Down i.e, No Internet/WIFI - Should not display any message - TODO : Need to handle it properly
	//	alert(kony.i18n.getLocalizedString("keySSLValidationFailureMsg"));
		dismissLoadingScreen();
	}
	getConfigUpdateValue();
}
function callbackConfig(configResult){
	
	
	if(configResult == 0){
		
	}else{
		startConfigUpdate();
	}
	checkMallware();
}
function callbackMal(mallresult,info){
	
	
	if(mallresult == 0){
		//checkSuspiciousApp();
		//dismissLoadingScreen(); //dismissLoadingScreen should call after login success.  
		//GBL_Session_LOCK=false;
	}else{
		//GBL_Session_LOCK=true;
		dismissLoadingScreen();
		GBL_MalWare_Names = info;
		var messageErr = kony.i18n.getLocalizedString("keyMallwareFoundMsg");
		if(messageErr == null){
				messageErr = "The dangerous malware/suspicious app being present on device 1. The bank strongly recommend to cleansing malware before access to the application. Without cleansing the malware and access to the application, you can�t transfer money out. Please contact # Call center for more advice."	
			}
		messageErr = messageErr.replace("NAMES", info);
		var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
		if (getEncrKeyFromDevice != null) {
			showAlertForSecValidation(messageErr);
		} else {
			showAlertForUniqueGenFail(messageErr);
		}
		
		
	}
}
function callbackSuspicious(result,info){
		
		dismissLoadingScreen();
		
		if(result == 0){
			kony("Security Validation completed for android");
		}else{
			//GBL_Session_LOCK=true;
			var messageErr = kony.i18n.getLocalizedString("keyMallwareFoundMsg");
			if(messageErr == null){
				messageErr = "The dangerous malware/suspicious app being present on device 1. The bank strongly recommend to cleansing malware before access to the application. Without cleansing the malware and access to the application, you can�t transfer money out. Please contact # Call center for more advice."	
			}
			messageErr = messageErr.replace("NAMES", info);
			var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
			if (getEncrKeyFromDevice != null) {
				showAlertForSecValidation(messageErr);
			} else {
				showAlertForUniqueGenFail(messageErr);
			}
		}
}
function callbackInit(result){
	
	kony.print("Trusteer Initialisation Finish : " + result);
	if(result == "true"){
		startSecurutyValidations();
	}else{
		//showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keyPharmingErrMsg"));
		var messageErr = kony.i18n.getLocalizedString("keyTrusteerInitFailErrMsg");
		if(isNotBlank(messageErr)){
			messageErr = messageErr.replace("ErrCD", result);
		}
		showAlertForUniqueGenFail(messageErr);
		dismissLoadingScreen();
	}
	
}


function startRiskDataCollForAndroid()
{
	
	function getAllSecurityInfoMW(){

		if(TmbTrusteerFFIObject == null)
		TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		TmbTrusteerFFIObject.getAllResult(
				/**Function*/ callbackAllMW);
		}
	function callbackAllMW(str){
		GBL_Risk_DataTo_Log = str;
		checkRootMW();
	}
	function checkRootMW(){
			//Creates an object of class 'TmbTrusteerFFI'
	 if(TmbTrusteerFFIObject == null)
	 TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
	//Invokes method 'getRootValue' on the object
	TmbTrusteerFFIObject.getRoot(
			/**Function*/ callbackRootMW);
	
	}
	function callbackRootMW(rootResult){
		
		if(rootResult == 0){
			GBL_rootedFlag =0;
		}else{
			GBL_rootedFlag =1;
		}
		checkMallwareMW();
	}
	function checkMallwareMW(){
		//Creates an object of class 'TmbTrusteerFFI'
		if(TmbTrusteerFFIObject == null)
		TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
		//Invokes method 'checkMallware' on the object
		TmbTrusteerFFIObject.checkMallware(
				/**Function*/ callbackMalMW);
		}
	function callbackMalMW(mallresult,info){
		if(mallresult == 0){
			GBL_MalWare_Names = "";
			GBL_Session_LOCK=false;
		}else{
			GBL_Session_LOCK=true;
			dismissLoadingScreen();
			GBL_MalWare_Names = info;
		}
		callServiceToLogRiskInfo();
	}
	getAllSecurityInfoMW();
}

function callbackRisk(riskResult){
	
	//GBL_Risk_DataTo_Log=riskResult;
	//callServiceToLogRiskInfo();
}
