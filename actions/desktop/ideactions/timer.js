







function timerFunc() {
	onClickOffBtn();
}

function dummy() {}

function onClickOnBtn() {
	var currForm = kony.application.getCurrentForm();
	
	
	if (gblflag == 0 && gblShowPwd > 0 && (currForm.id == "frmCMChgTransPwd" || currForm.id == "frmMBsetPasswd" ||  currForm.id == "frmMBSetAccPinTxnPwd")) {
		gblShowPwd = gblShowPwd - 1;
		currForm.txtTransPass.setVisibility(false);
		if (currForm.id != "frmCMChgTransPwd")
			currForm.txtAccessPwd.setVisibility(false);
		else
			currForm.tbxTranscCrntPwd.setVisibility(false);
		currForm.txtTemp.setVisibility(true);
		if (currForm.id != "frmCMChgTransPwd")
			currForm.txtTempAccess.setVisibility(true);
		else
			currForm.tbxTranscCrntPwdTemp.setVisibility(true);
		currForm.txtTemp.text = currForm.txtTransPass.text;
		if (currForm.id != "frmCMChgTransPwd")
			currForm.txtTempAccess.text = currForm.txtAccessPwd.text;
		else
			currForm.tbxTranscCrntPwdTemp.text = currForm.tbxTranscCrntPwd.text;
		gblflag = 1;
		currForm.btnPwdOn.skin = btnOnFocus;
		currForm.btnPwdOff.skin = btnOffFocus;
		setFocusOnTB();
	} else {
		return;
	}
	currForm.btnPwdOn.onClick = dummy;
	//cancelTimer();
	kony.timer.schedule("mytimer", timerFunc, gblShowPinPwdSecs, false);
	currForm.btnPwdOn.onClick = onClickOnfrmSnip;
}

function onClickOffBtn() {
	var currForm = kony.application.getCurrentForm();
	
	
	if (gblflag == 1 && gblShowPwd > 0 && (currForm.id == "frmCMChgTransPwd" || currForm.id == "frmMBsetPasswd" ||  currForm.id == "frmMBSetAccPinTxnPwd")) {
		gblShowPwd = gblShowPwd - 1;
		currForm.txtTransPass.text = currForm.txtTemp.text;
		if (currForm.id != "frmCMChgTransPwd")
			currForm.txtAccessPwd.text = currForm.txtTempAccess.text;
		else
			currForm.tbxTranscCrntPwd.text = currForm.tbxTranscCrntPwdTemp.text;
		currForm.txtTemp.setVisibility(false);
		if (currForm.id != "frmCMChgTransPwd")
			currForm.txtTempAccess.setVisibility(false);
		else
			currForm.tbxTranscCrntPwdTemp.setVisibility(false);
		currForm.txtTransPass.setVisibility(true);
		if (currForm.id != "frmCMChgTransPwd")
			currForm.txtAccessPwd.setVisibility(true);
		else
			currForm.tbxTranscCrntPwd.setVisibility(true);
		gblflag = 0;
		currForm.btnPwdOn.skin = btnOnNormal;
		currForm.btnPwdOff.skin = btnOffNorm;
		cancelTimer();
		setFocusOnTB();
	}
}

function cancelTimer() {
	try {
		kony.timer.cancel("mytimer");
	} catch (err) {
		alert("error in cancelTimer() and err is:: " + err);
	}
}

function onClickOnfrmSnip() {
	gblTxtFocusFlag = 5;
	onClickOnBtn();
}

function onClickOnChnAxBtn() {
	var currForm = kony.application.getCurrentForm();
	
	
	
	//gblShowPinPwdSecs =  kony.os.toNumber(getContentUpdate["showPinPwdSecs"]);				not necessary.
	
	if (gblflag == 0 && gblShowPwd > 0 && (currForm.id == "frmCMChgAccessPin" || currForm.id == "frmMBsetPasswd" ||  currForm.id == "frmMBSetAccPinTxnPwd")) {
		gblShowPwd = gblShowPwd - 1;
		currForm.tbxCurAccPin.setVisibility(false);
		currForm.txtAccessPwd.setVisibility(false);
		currForm.tbxCurAccPinUnMask.setVisibility(true);
		currForm.txtAccessPwdUnMask.setVisibility(true);
		currForm.tbxCurAccPinUnMask.maxTextLength = 6;
		currForm.txtAccessPwdUnMask.maxTextLength = 6;
		currForm.tbxCurAccPinUnMask.text = currForm.tbxCurAccPin.text;
		currForm.txtAccessPwdUnMask.text = currForm.txtAccessPwd.text;
		gblflag = 1;
		currForm.btnPwdOn.skin = btnOnFocus;
		currForm.btnPwdOff.skin = btnOffFocus;
	} else {
		return;
	}
	currForm.btnPwdOn.onClick = dummy;
	//cancelTimer();
	kony.timer.schedule("mytimerChngAc", timerFuncChngAc, gblShowPinPwdSecs, false);
	currForm.btnPwdOn.onClick = onClickOnChnAxfrmSnip;
}

function onClickChnAxOffBtn() {
	var currForm = kony.application.getCurrentForm();
	
	
	if (gblflag == 1 && gblShowPwd > 0 && (currForm.id == "frmCMChgAccessPin" || currForm.id == "frmMBsetPasswd" ||  currForm.id == "frmMBSetAccPinTxnPwd")) {
		gblShowPwd = gblShowPwd - 1;
		currForm.tbxCurAccPin.text = currForm.tbxCurAccPinUnMask.text;
		currForm.txtAccessPwd.text = currForm.txtAccessPwdUnMask.text;
		currForm.tbxCurAccPinUnMask.setVisibility(false);
		currForm.txtAccessPwdUnMask.setVisibility(false);
		currForm.tbxCurAccPin.setVisibility(true);
		currForm.txtAccessPwd.setVisibility(true);
		gblflag = 0;
		currForm.btnPwdOn.skin = btnOnNormal;
		currForm.btnPwdOff.skin = btnOffNorm;
		cancelTimerChngAc();
	}
}

function cancelTimerChngAc() {
	try {
		kony.timer.cancel("mytimerChngAc");
	} catch (err) {
		
	}
return true;
}

function onClickOnChnAxfrmSnip() {
	gblTxtFocusFlag = 5;
	onClickOnChnAxBtn();
}

function timerFuncChngAc() {
	onClickChnAxOffBtn();
}

//Show Access PIN & Password
function cancelScheduledTimer(timerId) {
	try {
		kony.timer.cancel(timerId);
	} catch (err) {
		//alert("error in cancelScheduledTimer() : " + err);
	}
}

function setScheduledTimer(timerId) {
	try {
		kony.timer.schedule(timerId, showHidePin, gblShowPinPwdSecs, false);
	} catch (err) {
		//alert("error in setScheduledTimer() : " + err);
	}
}

var isShowPin = false;
var glbCountPin = 0;

function focusTexbox(){
  frmMBSetAccPinTxnPwd.txtAccessPin.setFocus(true);
  
}

function isDigitAndNotBlank(inputObj) {
	var isDigit = new RegExp("[0-9]");
	
	return isDigit.test(inputObj) && isNotBlank(inputObj);
}

function accessPinRender(pin){
	accessPinShow(pin);
	if(pin.length == 6) {
		frmMBSetAccPinTxnPwd.txtTransPass.setFocus(true);
	}
}

function accessPinShow(pin){
	var imgObject = [frmMBSetAccPinTxnPwd.imgPin1, frmMBSetAccPinTxnPwd.imgPin2, frmMBSetAccPinTxnPwd.imgPin3, frmMBSetAccPinTxnPwd.imgPin4, frmMBSetAccPinTxnPwd.imgPin5, frmMBSetAccPinTxnPwd.imgPin6];
	var emptyPin = "dot_inactive.png";
	var fillPin = "dot_active.png";
	
	if(isDigitAndNotBlank(pin)) {
		if(isShowPin && glbCountPin > pin.length){
				generateNumber(pin);
				showPinPwd();
		} else {
			for(var i=0; i < pin.length; i++){
				imgObject[i].src = fillPin;
			}
			for(var i=imgObject.length; i > pin.length; i--){
					imgObject[i-1].src = emptyPin;
			}
			isShowPin = false;
			hidePinPwd();
		}
		glbCountPin = pin.length;
			
	} else if(!isNotBlank(pin)){
		for(var i=0; i<imgObject.length; i++){
			imgObject[i].src = emptyPin;
		}
	}
	
}


function showHidePin(){
	var pin = frmMBSetAccPinTxnPwd.txtAccessPin.text;
		if(frmMBSetAccPinTxnPwd.btnCheckBox.skin == "btnCheck" && gblShowPwdNo > 0){
			showPinPwd();	
			generateNumber(pin);
			isShowPin = true;
		}else{
			hidePinPwd();
			accessPinShow(pin);
			isShowPin = false;
		}
		
}

function showPinPwd(){
		if(frmMBSetAccPinTxnPwd.btnCheckBox.skin == "btnCheck" && gblShowPwdNo > 0 ) {
			gblflag = 1;
			gblShowPwdNo = gblShowPwdNo -1;
			frmMBSetAccPinTxnPwd.btnCheckBox.skin = "btnCheckFoc";
			frmMBSetAccPinTxnPwd.hbxCheckbox.setEnabled(true);
			frmMBSetAccPinTxnPwd.btnCheckBox.setEnabled(true);
			//Canelling the timer
			cancelScheduledTimer("accPinPwdShowTimer");
			
			//Setting Timer
			setScheduledTimer("accPinPwdShowTimer");
			
			//Txn Password
			frmMBSetAccPinTxnPwd.txtTransPassShow.text = frmMBSetAccPinTxnPwd.txtTransPass.text;
			frmMBSetAccPinTxnPwd.txtTransPassShow.setVisibility(true);
			frmMBSetAccPinTxnPwd.txtTransPass.setVisibility(false);
		}		
}

function hidePinPwd(){
		
	if(frmMBSetAccPinTxnPwd.btnCheckBox.skin != "btnCheck"){
		gblflag = 0;
		
		//Txn Password
		frmMBSetAccPinTxnPwd.txtTransPass.text = frmMBSetAccPinTxnPwd.txtTransPassShow.text;
		frmMBSetAccPinTxnPwd.txtTransPassShow.setVisibility(false);
		frmMBSetAccPinTxnPwd.txtTransPass.setVisibility(true);
			
		if(gblShowPwdNo > 0) {
			frmMBSetAccPinTxnPwd.btnCheckBox.skin = "btnCheck";
			frmMBSetAccPinTxnPwd.hbxCheckbox.setEnabled(true);
			frmMBSetAccPinTxnPwd.btnCheckBox.setEnabled(true);
		} else {
			frmMBSetAccPinTxnPwd.btnCheckBox.skin = "btnCheckInactive";
			frmMBSetAccPinTxnPwd.hbxCheckbox.setEnabled(false);
			frmMBSetAccPinTxnPwd.btnCheckBox.setEnabled(false);
		}
		
		cancelScheduledTimer("accPinPwdShowTimer");	
	}
}

function generateNumber(pin){

	var imgObject = [frmMBSetAccPinTxnPwd.imgPin1, frmMBSetAccPinTxnPwd.imgPin2, frmMBSetAccPinTxnPwd.imgPin3, frmMBSetAccPinTxnPwd.imgPin4, frmMBSetAccPinTxnPwd.imgPin5, frmMBSetAccPinTxnPwd.imgPin6];
	var pinNumbers = ["zero.png", "one.png", "two.png", "three.png", "four.png", "five.png", "six.png", "seven.png", "eight.png", "nine.png"];
	var emptyPin = "dot_inactive.png";
	if(isDigitAndNotBlank(pin)) {
		for(var i=0; i < pin.length; i++){
			var pinValue = pin.substring(i, i+1);
			imgObject[i].src = pinNumbers[parseInt(pinValue)];
		}
		for(var i=imgObject.length; i > pin.length; i--){
				imgObject[i-1].src = emptyPin;
		}
	}
}
