







function showIBLoadingScreen() {
	kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
}

function dismissIBLoadingScreen() {
	kony.application.dismissLoadingScreen();
}
/**
 * Function to invoke the logout service call
 *  no return values
 */

function IBLogoutService() {
	showLoadingScreenPopup();
	inputParam = {};
	inputParam["channelId"] = "01";
	inputParam["timeOut"] = GBL_Time_Out;
	inputParam["deviceId"] = IBLogoutService.caller.name;	
	var locale = kony.i18n.getCurrentLocale();
			if (locale == "en_US") {
				inputParam["languageCd"] = "EN";
			} else {
				inputParam["languageCd"] = "TH";
			}
	invokeServiceSecureAsync("logOutTMB", inputParam, callBackIBLogoutService);
}
/**
 * call back to handle logout service responce.Usagetime is returned from logout service.
 *  no return values
 */

function callBackIBLogoutService(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			//activityLogServiceCall("010", "", "01", gblUserName, "", resulttable["usageTime"], "", "", "", "");
			//invokeIBCRMProfileUpdateLogout();
			gblLogoutTime = resulttable["logoutDate"];
		}
			ResetTransferHomePageIB(); //Clearing cache for Transfers Module
			resetAllRcCacheData();
			resetAllBillTopupCacheDataIB(); //Clear Caching for Billers And Topups.
			
		gblLoggedIn = "false";
		gblInitMyaccnt();
		curr_form = kony.application.getCurrentForm()
			.id;
		/* removing as per ENH_028 - IB Menu on Home Screen
		if (curr_form == "frmIBPostLoginDashboard")
			menuReset2();
		else
			menuReset();
		*/
		menuReset();	
		//kony.application.getCurrentForm().segMenuOptions.removeAll();
		//frmIBPreLogin.show();
		gblDuration=resulttable["usageTime"];
		frmIBLogoutLanding.show();
		hbxFooterPrelogin.linkhotpromo.setVisibility(false);
		hbxFooterPrelogin.linkHotPromoImg.setVisibility(false);
		gblIsNewOffersExists = false;
		//kony.os.endSecureTransaction();
		clearGlobalVariables();
		localStorage.clear();
	}
}

/**
 * description
 * @returns {}
 */

/*function invokeIBCRMProfileUpdateTimeoutLogout() {
	showIBLoadingScreen();
	inputParam = {};
	inputParam["actionType"] = "33";
	invokeServiceSecureAsync("crmProfileMod", inputParam, callBackinvokeIBCRMProfileUpdateLogoutTimeOut)
}*/
/**
 * description
 * @returns {}
 */

/*function callBackinvokeIBCRMProfileUpdateLogoutTimeOut(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			gblLogoutTime = resulttable["LastIBLogoutTimeOutDate"];
			IBLogoutService();
			ResetTransferHomePageIB(); //Clearing cache for Transfers Module
			resetAllRcCacheData();
			resetAllBillTopupCacheDataIB(); //Clear Caching for Billers And Topups.
		} else {
			
		}
		dismissIBLoadingScreen();
	}
}*/
/*function invokeIBCRMProfileUpdateLogout() {
	showLoadingScreenPopup();
	//inputParam = {};
	//inputParam["actionType"] = "23";
	//invokeServiceSecureAsync("crmProfileMod", inputParam, callBackinvokeIBCRMProfileUpdateLogout)
}*/
/**
 * description
 * @returns {}
 */

/*function callBackinvokeIBCRMProfileUpdateLogout(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
			gblLogoutTime = resulttable["LastIBLogoutDate"];
			campaignBannerCount();//check the banner count
			IBLogoutService();
			ResetTransferHomePageIB(); //Clearing cache for Transfers Module
			resetAllRcCacheData();
			resetAllBillTopupCacheDataIB(); //Clear Caching for Billers And Topups.
		} else {
			
		}
		dismissIBLoadingScreen();
	}
}*/
/*
************************************************************************
            Name    : IBcopyright_footer_display 
            Author  : Developer
            Date    : july 18, 2013
            Purpose : Copyright footer in IB screens
        	Input params: na
        	Output params: sets the footer copyright message
        	ServiceCalls: nil
        	Called From : postapp init and language toggle function
************************************************************************
 */

function IBcopyright_footer_display() {
	curr_lang = kony.i18n.getCurrentLocale();
	var date = new Date();
	copyright_year = date.getFullYear()
	if (curr_lang == "th_TH") {
		hbxFooterPrelogin.lblFooterCopyright.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " + (
			Number(copyright_year) + 543) + " " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");
	} else
		hbxFooterPrelogin.lblFooterCopyright.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " +
			copyright_year + ". " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");
	hbxFooterWithoutlinks.lblFooterCopyright.text = hbxFooterPrelogin.lblFooterCopyright.text;
}
/*
************************************************************************
            Name    : IBcopyright_footer_display 
            Author  : Developer
            Date    : july 26, 2013
            Purpose : App title in browsers
        	Input params: na
        	Output params: Sets the title of app as per locale
        	ServiceCalls: nil
        	Called From : postapp init and language toggle function
************************************************************************
 */

function IBAppTitle() {
	kony.globals["apptitle"] = kony.i18n.getLocalizedString("IbappTitle");
	document.title = kony.i18n.getLocalizedString("IbappTitle");
}

function registerForTimeOutIB() {
	idleTimeOut = 5;
	if (GLOBAL_KONY_IDLE_TIMEOUT != null && GLOBAL_KONY_IDLE_TIMEOUT != undefined) {
		
		idleTimeOut = kony.os.toNumber(GLOBAL_KONY_IDLE_TIMEOUT);
	}
	
	
	try{
	kony.application.unregisterForIdleTimeout();
	}
	catch(e){
	}
	
	try{
		kony.timer.cancel("idletimeoutcheck");
	}
	catch(e){
	}
	kony.timer.schedule("idletimeoutcheck", invokeidletimeoutIB, 1, false)
	
}

function invokeidletimeoutIB()
{
	//alert(idleTimeOut);
	kony.application.registerForIdleTimeout(idleTimeOut, onIdleTimeOutIB)
	
}

function onIdleTimeOutIB() {
	try
	{
		
		//isSignedUser = false;
		//Have to call the function which dissmisses all the popups
	 	GBL_Time_Out = "true";
	 	
		if (gblLoggedIn == true || gblLoggedIn == "true" ){
			IBLogoutService();
			//campaignBannerIBCount();
			
		}else{
			frmIBPreLogin.show();
		}					
		gblIsNewOffersExists = false;
	 	hbxFooterPrelogin.linkhotpromo.setVisibility(false);
		hbxFooterPrelogin.linkHotPromoImg.setVisibility(false);		
	}
	catch(e)
	{
		alert("Something Wrong")
	}
}
/*
************************************************************************
            Name    : IB Terms and Conditions display 
            Author  : Developer
            Date    : Aug 13, 2013
            Purpose : Displays the TnC of app as per locale
        	Input params: na
        	Output params: Terms and conditons in specific locale
        	ServiceCalls: nil
        	Called From : postapp init of frmIBActivationTnC form and language toggle function
************************************************************************
 */

function IBTnCText() {
	var input_param = {};
	input_param["localeCd"]=kony.i18n.getCurrentLocale();
	input_param["moduleKey"]="TermsAndConditions";
	showLoadingScreenPopup();
    invokeServiceSecureAsync("readUTFFile", input_param, setIBTnC);
	
}



function setIBTnC(status,resulttable){
		if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            
            frmIBActivationTandC.lblTnC.text = resulttable["fileContent"];
			dismissLoadingScreenPopup();
			frmIBActivationTandC.button502735421210984.setEnabled(true);            
			frmIBActivationTandC.show();
        }else{
        	if (kony.i18n.getCurrentLocale() == "th_TH") 
        	{
					frmIBActivationTandC.lblTnC.text = "";
			} else {
					frmIBActivationTandC.lblTnC.text = "";
			}
        	frmIBActivationTandC.button502735421210984.setEnabled(false);
        }
    }
}

/*
************************************************************************
            Name    : IB OTP Request counter 
            Author  : Ravi
            Date    : Oct 17, 2013
            Purpose : checks the no of OTP requests
        	Input params: na
        	Output params: na
        	ServiceCalls: nil
        	Called From : 
************************************************************************
 */
var gblOTPRequestCounter=0;
var gblOTPReqLimit=0;
function checkOtpRequestCounter(){
    if(gblOTPRequestCounter >= gblOTPReqLimit){
 		return true;
  	}
  	return false;
}
 
function showOTPLockedPop(){
 if(getCRMLockStatus())
	{
		curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}
}

function TMBLogoClickHandler()
{
	var currForm = kony.application.getCurrentForm().id;
	if(!(currForm == "frmIBFATCATandC" || currForm == "frmIBFATCAQuestionnaire")) {
		if(gblLoggedIn){
			onclickActivationCompleteStart();
    		/* removing as per ENH_028 - IB Menu on Home Screen
    		if (kony.application.getCurrentForm().id != "frmIBPostLoginDashboard") {
        		menuReset.call(this);
	    	} else {
    	    	menuReset2.call(this);
    		}*/
    		menuReset2.call(this);
		}else{
			window.location.reload(true)
		}
	}
}

function FooterFacebookHandler(){
	
	url = kony.i18n.getLocalizedString("footer_facebook");
	
	kony.application.openURL(url);
}	

function resetFooterSkins(){ 
	var locale = kony.i18n.getCurrentLocale();
   	if(gblIsNewOffersExists){
   		if(hbxFooterPrelogin.linkhotpromo.isvisible){
         	hbxFooterPrelogin.linkHotPromoImg.isVisible = true;
        }else{
        	hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
        }
    } else {
    	hbxFooterPrelogin.linkHotPromoImg.isVisible = false;
    	
    }
	hbxFooterPrelogin.linkFindTmb.skin = "footerLinkFind";
	hbxFooterPrelogin.linkExchangeRate.skin = "footerLinkER";
	hbxFooterPrelogin.linkSiteTour.skin = "footerLinkST";
	hbxFooterPrelogin.linkTnC.skin = "footerLinkTC";
	hbxFooterPrelogin.linkSecurity.skin = "footerLinkSecurity";
}

function onClickEntrust(){
	try{
		
		var entrustURL = kony.i18n.getLocalizedString("EntrustURL");
		
		kony.application.openURL(entrustURL);
	}catch(e){
		
	}
}

function hasWhiteSpaceIB(s) {
  return /\s/g.test(s);
}

function ActivationMBViaIBLogoutService() {
	showLoadingScreen();
	inputParam = {};
	inputParam["channelId"] = "01";
	inputParam["timeOut"] = GBL_Time_Out;
	inputParam["deviceId"] = ActivationMBViaIBLogoutService.caller.name;	
	var locale = kony.i18n.getCurrentLocale();
			if (locale == "en_US") {
				inputParam["languageCd"] = "EN";
			} else {
				inputParam["languageCd"] = "TH";
			}
	invokeServiceSecureAsync("logOutTMB", inputParam, callBackActivationMBViaIBLogoutService);
}
/**
 * call back to handle logout service responce.Usagetime is returned from logout service.
 *  no return values
 */

function callBackActivationMBViaIBLogoutService(status, resulttable) {
	if (status == 400) {
	dismissLoadingScreen();
		if (resulttable["opstatus"] == 0) {
			gblLogoutTime = resulttable["logoutDate"];
		}
			
		gblLoggedIn = "false";
		gblDuration=resulttable["usageTime"];
		frmMBanking.show();
	}
}
function showAlertWithHandler(keyMsg, KeyTitle,eventHandlerMIB) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: eventHandlerMIB
	};
	var pspConf = {};
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
}