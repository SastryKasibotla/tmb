function encryptForATMPinAndroid(pin) {
    var e2eeResult = E2EEConnector.encryptForLogin("", pin, gblPk, gblRand);
    kony.print("E2EE Return Code = " + e2eeResult[1]);
    kony.print("E2EE RPIN = " + e2eeResult[0]);
    return e2eeResult[0];
}

function encryptAtmPinForIphone(pin) {
    // RSA encryption using E2EEa AxMx library
    //var returnVal = AccessPinFFI.getEncrypt(algo,e2eesid,pinToEnc,publicKey,serverRandom);
    var e2eeResult = E2EEConnector.encryptPinForIphone('SHA1', '', pin, gblPk, gblRand);
    return e2eeResult;
}

function encryptForATMPinIB(pin) {
    var encrypedText = "";
    encrypedText = ame2eea.encryptPinForAM("", gblPk, gblRand, pin, gblAlgorithm);
    return encrypedText;
}

function encryptPinUsingE2EE(pin) {
    var e2eeResult = "";
    if (gblPk != "" && gblRand != "" && gblAlgorithm != "") {
        e2eeResult = encryptForATMPinIB(pin);
        if (undefined != e2eeResult) {
            assignATMPin(e2eeResult);
        } else {
            //Error handle when encryption fail
        }
    } else {
        //Error handle when init service failed
    }
}

function encryptVerifyPinUsingE2EE(pin) {
    var e2eeResult = "";
    if (gblPk != "" && gblRand != "" && gblAlgorithm != "") {
        if (undefined != e2eeResult) {
            validateAtmPin(e2eeResult);
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            //Error handle when encryption fail
        }
    } else {
        dismissLoadingScreen();
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        //Error handle when init service failed
    }
}

function initOnlineATMPIN() {
    showDismissLoadingMBIB(true);
    var inputParam = {};
    var status = invokeServiceSecureAsync("InitOnlineATMPIN", inputParam, initOnlineATMPINCallBack);
}

function initOnlineATMPINCallBack(status, resulttable) {
    if (status == 400) {
        showDismissLoadingMBIB(false);
        if (resulttable["opstatus"] == 0) {
            gblPk = resulttable["pubKey"];
            gblRand = resulttable["serverRandom"];
            gblAlgorithm = resulttable["hashAlgorithm"];
        } else {
            gblPk = "";
            gblRand = "";
            gblAlgorithm = "";
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        }
    }
}

function assignATMPin(encryptedPin) {
    showDismissLoadingMBIB(true);
    var inputParam = {};
    inputParam["rPin"] = encryptedPin;
    var status = invokeServiceSecureAsync("assignPin", inputParam, assignATMPinCallBack);
}

function assignATMPinCallBack(status, resulttable) {
    if (status == 400) {
        showDismissLoadingMBIB(false);
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == "0") {
            frmIBDebitCardActivateComplete.hbxSuccess.setVisibility(true);
            frmIBDebitCardActivateComplete.lblTrxStatusSuccess.setVisibility(true);
            frmIBDebitCardActivateComplete.hbxFail.setVisibility(false);
            frmIBDebitCardActivateComplete.lblTrxStatusFail.setVisibility(false);
            frmIBDebitCardActivateComplete.lblTrxStatusSuccess.text = kony.i18n.getLocalizedString("keyAssignPinCompleted").replace("[cardno]", maskCardNo);
            frmIBDebitCardActivateComplete.lblATMPinHeader.text = kony.i18n.getLocalizedString("Complete");
            frmIBDebitCardActivateComplete.show();
        } else {
            frmIBDebitCardActivateComplete.hbxSuccess.setVisibility(false);
            frmIBDebitCardActivateComplete.lblTrxStatusSuccess.setVisibility(false);
            frmIBDebitCardActivateComplete.hbxFail.setVisibility(true);
            frmIBDebitCardActivateComplete.lblTrxStatusFail.setVisibility(true);
            frmIBDebitCardActivateComplete.lblATMPinHeader.text = kony.i18n.getLocalizedString("keyCalendarFailed");
            frmIBDebitCardActivateComplete.show();
        }
    }
}