gblFundName="";
gblFundNickName="";
gblUnitHolderNumber="";
gblFundCode="";
gblViewType="";
gblSelFundNickNameEN="";
gblSelFundNickNameTH="";
var gblMaxRecordPerPage = 15;
var totalGridData = [];
var startDateIB = "";
var endDateIB = "";
var currentpageStmt = 1;
var startPageNo = 1;
var nextpageStmt = 1;
var totalPage = 0;
var totalRecord = 0;
var oldMFselectedInd  = 0;
var currMFselectedInd = 0;

function callMutualFundsSummary(){
	 showLoadingScreenPopup();
	 var inputParam = {};
	 currMFselectedInd = 1;
     invokeServiceSecureAsync("MFUHAccountSummaryInq", inputParam, callMutualFundsSummaryCallBack);
}

function callMutualFundsSummaryCallBack(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
	        var segmentData= populateDataMutualFundsSummary(resulttable);
	         
	        frmIBMutualFundsSummary.segAccountDetails.widgetDataMap ={
							lblHead: "lblHead",
					       	imgLogo:"imgLogo",
							lblfundName:"lblfundName",
							lblfundNickNameTH:"lblfundNickNameTH",
							lblfundNickNameEN:"lblfundNickNameEN",
							lblunitHolderNumber: "lblunitHolderNumber",
							lblunRealizedPL:"lblunRealizedPL",
							lblunRealizedPLValue:"lblunRealizedPLValue",
							lblinvestment:"lblinvestment",
							lblinvestmentValue:"lblinvestmentValue",
							fundCode:"fundCode"
					    };
	        frmIBMutualFundsSummary.segAccountDetails.removeAll();
	       	frmIBMutualFundsSummary.segAccountDetails.setData(segmentData);
	        frmIBMutualFundsSummary.hbxAccntDetails.setVisibility(false);
	        //begin MIB-3311
			frmIBMutualFundsSummary.imgProfile.src = frmIBAccntSummary.imgProfile.src;
	 		frmIBMutualFundsSummary.lblUsername.text = frmIBAccntSummary.lblUsername.text;
			frmIBMutualFundsSummary.lblInvestmentValue.text = gblAccountTable["mfTotalAmount"]+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	 		//end MIB-3311 
	        frmIBMutualFundsSummary.show();
	       	
	       	//details service
	       	callMutualFundsDetails(frmIBMutualFundsSummary.segAccountDetails.data[1].lblunitHolderNumber,frmIBMutualFundsSummary.segAccountDetails.data[1].fundCode);
	       	frmIBMutualFundsSummary.lblAccountNameHeader.text = frmIBMutualFundsSummary.segAccountDetails.data[1].lblfundName;
		    frmIBMutualFundsSummary.lblAccountBalanceHeader.text = frmIBMutualFundsSummary.segAccountDetails.data[1].lblinvestmentValue;
		    frmIBMutualFundsSummary.imgAccountDetailsPic.src = frmIBMutualFundsSummary.segAccountDetails.data[1].imgLogo;
		    gblSelFundNickNameTH = frmIBMutualFundsSummary.segAccountDetails.data[1].lblfundNickNameTH;
		    gblSelFundNickNameEN = frmIBMutualFundsSummary.segAccountDetails.data[1].lblfundNickNameEN;
        }else{
       		dismissLoadingScreenPopup();
       		showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
        }
     }else{
   	  	dismissLoadingScreenPopup();
     }
}
 
 

function  populateDataMutualFundsSummary(summaryDataTotal){
	var summaryData=summaryDataTotal["FundClassDS"];
	var segmentDataTemp = [];
	var segmentHeaderTemp = [];
	var segmentData=[];
	var logo;
	var fundName;
	var unitHolderNumber;
	var unRealizedPL = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
	var unRealizedPLValue;
	var investment = kony.i18n.getLocalizedString("MF_lbl_Market_value");
	var investmentValue;
	var segmentTemplate;
	var locale = kony.i18n.getCurrentLocale();
	var header ;
	for(var i=0;i<summaryData.length;i++){
	
		segmentDataTemp = [];
		segmentHeaderTemp = [];
		
		var fundhouseDS = summaryData[i].FundHouseDS
		var fundHouseLength = fundhouseDS.length;
		
			
			var noOfAccounts = 0;
		
			for(var j=0;j<fundHouseLength;j++){
			
					var FundCodeDS = fundhouseDS[j].FundCodeDS;
					var FundCodeDSLength = FundCodeDS.length;
					
					var fundHosueCode = fundhouseDS[j]["FundHouseCode"]
					for(var k=0; k < FundCodeDSLength;k++){
						
						noOfAccounts++;
						logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +
						 "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHosueCode+"&modIdentifier=MFLOGOS";
						unRealizedPLValue = FundCodeDS[k]["UnrealizedProfit"];
						var unRealizedPLValueSkin;
						
						if(unRealizedPLValue == "0"){
							unRealizedPLValueSkin = lblIB20pxBlack;
							unRealizedPLValue = commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						}else if(unRealizedPLValue.indexOf("-") < 0){
							unRealizedPLValueSkin = lblIB24pxRegGreen;
							unRealizedPLValue = "+" + commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						}else{
							unRealizedPLValueSkin = lblIB24pxRegRed;
							unRealizedPLValue = commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						}
						
						var fundName = FundCodeDS[k]["FundNickNameEN"];
						if(locale != "en_US"){
							fundName = FundCodeDS[k]["FundNickNameTH"];
						}
						var dataObject = {
							imgLogo:logo,
							lblfundName:fundName,
							lblfundNickNameEN:FundCodeDS[k]["FundNickNameEN"],
							lblfundNickNameTH:FundCodeDS[k]["FundNickNameTH"],
							lblfundNameEN:FundCodeDS[k]["FundNameEN"],
							lblfundNameTH:FundCodeDS[k]["FundNameTH"],
							lblunitHolderNumber: formatUnitHolderNumer(FundCodeDS[k]["UnitHolderNo"]),
							lblunRealizedPL:unRealizedPL,
							lblunRealizedPLValue:{"text":unRealizedPLValue,"skin":unRealizedPLValueSkin},
							lblinvestment:investment,
							lblinvestmentValue:commaFormatted(FundCodeDS[k]["MarketValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
							template:MFContent,
							fundCode:FundCodeDS[k]["FundCode"]
						};
					
						segmentDataTemp.push(dataObject);
						
					}//for loop end k
			}//for loop end j
			var accountTxt = kony.i18n.getLocalizedString("MF_lbl_Account");
			if(noOfAccounts > 1 && locale == "en_US"){
				accountTxt = accountTxt + "s";
			}
			if(locale == "en_US"){
				header = summaryData[i]["FundClassNameEN"] + " (" + noOfAccounts + " " + accountTxt +")";
			}else{
				header = summaryData[i]["FundClassNameTH"] + " (" + noOfAccounts + " " + accountTxt +")";
			}
			var dataObjectHeader = {
				lblHead: header,
				lblHeadEN: summaryData[i]["FundClassNameEN"],
				lblHeadTH: summaryData[i]["FundClassNameTH"],
				noOfAccts: noOfAccounts,
				template:MFHeader
			};
		segmentHeaderTemp.push(dataObjectHeader);

		segmentData = segmentData.concat(segmentHeaderTemp.concat(segmentDataTemp));
		
	}//for loop end i
	return segmentData;
}

function callMutualFundsDetails(unitHolderNuber,fundcode){
	if(unitHolderNuber == null || unitHolderNuber == undefined) {
		return;
	}
	
	gblUnitHolderNumber = unitHolderNuber;
	gblFundCode = fundcode;
		
	var inputParam = {};
	inputParam["unitHolderNo"] = unitHolderNuber;
	inputParam["funCode"] = fundcode;
	inputParam["serviceType"] = "1";
	showLoadingScreenPopup();
    invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callMutualFundsDetailsCallBack);
}

function callMutualFundsDetailsCallBack(status,resulttable){ 
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	dismissLoadingScreenPopup();
        	if(resulttable["TaxDoc"] == "N"){
	        	frmIBMutualFundsSummary.lnkTaxDoc.setVisibility(false);
        	}else{
	        	frmIBMutualFundsSummary.lnkTaxDoc.setVisibility(true);
        	}

			var locale = kony.i18n.getCurrentLocale();
			var fundName="";
			if (locale == "th_TH"){ 
				fundName = resulttable["FundNameTH"];
			}else{
				fundName = resulttable["FundNameEN"];
			}
			
	        frmIBMutualFundsSummary.lblUseful1.text = kony.i18n.getLocalizedString("MF_lbl_Fund_name");
	        frmIBMutualFundsSummary.lblUsefulValue1.text = fundName;
	        frmIBMutualFundsSummary.lblUseful2.text = kony.i18n.getLocalizedString("MF_lbl_Unit_holder_no");
	        frmIBMutualFundsSummary.lblUsefulValue2.text = resulttable["UnitHolderNo"];
	        frmIBMutualFundsSummary.lblUseful3.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
	        frmIBMutualFundsSummary.lblUsefulValue3.text = resulttable["DateAsOf"];
	        frmIBMutualFundsSummary.lblUseful4.text = kony.i18n.getLocalizedString("MF_lbl_Units");
	        frmIBMutualFundsSummary.lblUsefulValue4.text = verifyDisplayUnit(resulttable["Unit"]);
	        frmIBMutualFundsSummary.lblUseful5.text = kony.i18n.getLocalizedString("MF_lbl_NAV_unit");
	        frmIBMutualFundsSummary.lblUsefulValue5.text = verifyDisplayUnit(resulttable["Nav"]);
	        frmIBMutualFundsSummary.lblUseful6.text = kony.i18n.getLocalizedString("MF_lbl_Cost");
	        frmIBMutualFundsSummary.lblUsefulValue6.text = commaFormatted(resulttable["Cost"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	        frmIBMutualFundsSummary.lblUseful7.text = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
	        frmIBMutualFundsSummary.lblUsefulValue7.text = commaFormatted(resulttable["InvestmentValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	        frmIBMutualFundsSummary.lblUseful8.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
	        frmIBMutualFundsSummary.lblUsefulValue8.text = getValueForUnrealPL(resulttable["UnrealizedProfit"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	        frmIBMutualFundsSummary.lblUsefulValue8.skin = getSkinValueForUnrealPL(resulttable["UnrealizedProfit"]);
	        
	        frmIBMutualFundsSummary.lblUseful9.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_percent");
	        frmIBMutualFundsSummary.lblUsefulValue9.text = getValueForUnrealPL(resulttable["UnrealizedProfitPerc"]) + " %";
	       	frmIBMutualFundsSummary.lblUsefulValue9.skin = getSkinValueForUnrealPL(resulttable["UnrealizedProfitPerc"]);
	        
	        displayUnitLTF5Y(resulttable["UnitLTF5Y"]);
			frmIBMutualFundsSummary.hbxAccntDetails.setVisibility(true);
			
			swapSelectedMutualFundsSkin();
			
        } else {
        	dismissLoadingScreenPopup();
	        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
        }
     }else{
        dismissLoadingScreenPopup();
     }
}

function swapSelectedMutualFundsSkin(){
	var j = 0;
	for (var i=0;i<document.getElementsByTagName("li").length;i++) {
			var tmp = document.getElementsByTagName("li")[i];
			if(undefined != tmp && undefined != tmp.parentNode){
				if(tmp.childNodes[0].id == "MFContent_MFContent"){break;}
				if(tmp.childNodes[0].id != "MFContent_MFContent" && tmp.childNodes[0].id != "MFHeader_MFHeader"){
					if(tmp.childNodes[0].id.indexOf("loadingScreenDiv") == -1){
						j += 1;
					}
				}
			}
	}  
	if(j == 0){
		swapSelectedMutualFundsSkinSingle();
	} else {
		swapSelectedMutualFundsSkinMultiple(j);
	}
}

function swapSelectedMutualFundsSkinSingle(){
	if(null != frmIBMutualFundsSummary.segAccountDetails.selectedIndex && undefined != frmIBMutualFundsSummary.segAccountDetails.selectedIndex){
 		currMFselectedInd = frmIBMutualFundsSummary.segAccountDetails.selectedIndex[1];
 	}
 	if(currMFselectedInd != 0){
		var tempOld = document.getElementsByTagName("li")[oldMFselectedInd];
		if(undefined != tempOld && undefined != tempOld.parentNode){
			if(tempOld.parentNode.parentNode.id == "frmIBMutualFundsSummary_segAccountDetails"){		
				removeBGColor(tempOld.childNodes[0]);
				removeBGImage(tempOld.childNodes[0]);
				removeClass(tempOld.childNodes[0] ,"segSelectedRow");				
				removeClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");		
				tempOld.setAttribute("class","sknsegm");
				if(oldMFselectedInd != 0){
					if(!hasClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg")){
						addClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg");				
					}		
					tempOld.onmouseover = function() { 
						if(oldMFselectedInd != currMFselectedInd ){
							addClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");
						} 
					}
				}
			}
		}		
		
		var tempCurr = document.getElementsByTagName("li")[currMFselectedInd];
		if(undefined != tempCurr && undefined != tempCurr.parentNode){
			if(tempCurr.parentNode.parentNode.id == "frmIBMutualFundsSummary_segAccountDetails"){
				if(hasClass(tempCurr.childNodes[0],"segSelectedRow")){
					removeClass(tempCurr.childNodes[0],"segSelectedRow");	
				}else{	
					if(currMFselectedInd != 0){	
						addClass(tempCurr.childNodes[0],"segSelectedRow");				
						tempCurr.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					}		
				}
					
				tempCurr.onmouseover = function() { 
					removeClass(this ,"hbxAcctSummarySegItemFocus");
					removeClass(this.childNodes[0] ,"hbxAcctSummarySegItemFocus");
					this.childNodes[0].style.backgroundColor = '#e1eff8'; 
					this.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					if(oldMFselectedInd != currMFselectedInd){		
						addClass(this,"segSelectedRow");	
						addClass(this.childNodes[0],"segSelectedRow");	
					} 
				}
			}	
		}
		oldMFselectedInd = currMFselectedInd;
	}
}

function swapSelectedMutualFundsSkinMultiple(j){
	if(null != frmIBMutualFundsSummary.segAccountDetails.selectedIndex && undefined != frmIBMutualFundsSummary.segAccountDetails.selectedIndex){
 		currMFselectedInd = frmIBMutualFundsSummary.segAccountDetails.selectedIndex[1];
 	}	 
 	if(currMFselectedInd != 0){ 
 		currMFselectedInd += j;	
		oldMFselectedInd  += j;	
		var tempCurr = document.getElementsByTagName("li")[currMFselectedInd]; 
		if(undefined != tempCurr && undefined != tempCurr.parentNode){
			if(tempCurr.parentNode.parentNode.id == "frmIBMutualFundsSummary_segAccountDetails"){
				if(hasClass(tempCurr.childNodes[0],"segSelectedRow")){
					removeClass(tempCurr.childNodes[0],"segSelectedRow");	
				}else{	
					if(currMFselectedInd != 0){	
						addClass(tempCurr.childNodes[0],"segSelectedRow");
						tempCurr.childNodes[0].style.backgroundColor = '#e1eff8'; 				
						tempCurr.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					}		
				}
					
				tempCurr.onmouseover = function() { 
					removeClass(this ,"hbxAcctSummarySegItemFocus");
					removeClass(this.childNodes[0] ,"hbxAcctSummarySegItemFocus");
					this.childNodes[0].style.backgroundColor = '#e1eff8'; 
					this.childNodes[0].style.backgroundImage = "url(./desktopweb/images/bg_arrow_right_grayb2.png)";
					/*if(oldMFselectedInd != currMFselectedInd){		
						addClass(this,"segSelectedRow");	
						addClass(this.childNodes[0],"segSelectedRow");	
					} */
					if(!hasClass(tempCurr.childNodes[0],"segSelectedRow")){
						addClass(this,"segSelectedRow");	
						addClass(this.childNodes[0],"segSelectedRow");	
					}

					var c = document.getElementsByTagName("li");
					for(var i = 0; i < c.length; i++){  
						if(i != currMFselectedInd){
							var tmp = document.getElementsByTagName("li")[i];
							removeClass(tmp ,"hbxAcctSummarySegItemFocus");
							removeClass(tmp.childNodes[0] ,"hbxAcctSummarySegItemFocus");
						}
					}					
				}				
			}	
		}

		var tempOld = document.getElementsByTagName("li")[oldMFselectedInd];
		if(undefined != tempOld){removeClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");}
		if(undefined != tempOld && undefined != tempOld.parentNode){
			if(tempOld.parentNode.parentNode.id == "frmIBMutualFundsSummary_segAccountDetails"){		
				if(oldMFselectedInd != currMFselectedInd ){
					removeBGColor(tempOld.childNodes[0]);
					removeBGImage(tempOld.childNodes[0]);
					removeClass(tempOld.childNodes[0] ,"segSelectedRow");				
					removeClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");	
				}
				
				tempOld.setAttribute("class","sknsegm");
				if(oldMFselectedInd != 0){
					if(!hasClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg")){
						addClass(tempOld.childNodes[0] ,"hbxAcctSummarySeg");				
					}					
						
					tempOld.onmouseover = function() { 
						if(oldMFselectedInd != currMFselectedInd ){
							if(!hasClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus")){
								addClass(tempOld.childNodes[0] ,"hbxAcctSummarySegItemFocus");
							} 
						}
						var c = document.getElementsByTagName("li");
						for(var i = 0; i < c.length; i++){  
							//if(i != oldMFselectedInd){
								var tmp = document.getElementsByTagName("li")[i];
								removeClass(tmp ,"hbxAcctSummarySegItemFocus");
								removeClass(tmp.childNodes[0] ,"hbxAcctSummarySegItemFocus");
							//}
						}	
					}
				}
			}
		}
								
		oldMFselectedInd = currMFselectedInd -j;
	}
}

function displayUnitLTF5Y(data){
	if(isNotBlank(data)){
		frmIBMutualFundsSummary.hbxUseful10.setVisibility(true);
		frmIBMutualFundsSummary.lblUseful10.text = kony.i18n.getLocalizedString("MF_lbl_LTF_Units");
		frmIBMutualFundsSummary.lblUsefulValue10.text = verifyDisplayUnit(data);
	}else{
		frmIBMutualFundsSummary.hbxUseful10.setVisibility(false);
		frmIBMutualFundsSummary.lblUseful10.text = "";
	    frmIBMutualFundsSummary.lblUsefulValue10.text = "";
	}
}

function verifyDisplayUnit(data){
	if(parseFloat(data) == 0){
		 return data;
	}else{
		var value = parseFloat(data);
	    value = value.toFixed(4);
		return commaFormatted(value);
	}
}

function getValueForUnrealPL(data){
	if(parseFloat(data) > 0){
		return "+" + commaFormatted(data);
	}else{
		if(parseFloat(data) < 0 && commaFormatted(data) < 1 && commaFormatted(data) > 0){
			return "-" + commaFormatted(data);
		}else{
			return commaFormatted(data);
		}
	}
}

function getSkinValueForUnrealPL(data){
	if(parseFloat(data) > 0){
		return lblIB18pxGreenNoMed;
	}else if(parseFloat(data) < 0){
		return lblIB18pxRedNoMed;
	}else{
		return lblIB18pxBlackNoMed;
	}
}


function formatUnitHolderNumer(txt){

	if (txt == null) return "";
	
	if(txt.length == 10){
		return addHyphenIB(txt);
	}
	else if(txt.length == 12){
		var hyphenText = "";
		for (i = 0; i < txt.length; i++) {
			hyphenText += txt[i];
			if (i == 2 || i == 5 || i == 10) {
				hyphenText += '-';
			}
		}
		return hyphenText;
	}else{
		return txt;
	}
}

function frmIBMutualFundsSummaryPreShow(){
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US"){		      
      	frmIBMutualFundsSummary.lblUsername.text = gblCustomerName;		      
    }else{
  		frmIBMutualFundsSummary.lblUsername.text = gblCustomerNameTh;	
  	}
	// MF Summary
	frmIBMutualFundsSummary.lblSummaryHeader.text = kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
	frmIBMutualFundsSummary.lblnvestment.text = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h1");
	frmIBMutualFundsSummary.segAccountDetails.setData(updateLocaleDataSegMF(frmIBMutualFundsSummary.segAccountDetails.data));
	
	// MF Details
	frmIBMutualFundsSummary.label449290336675636.text = kony.i18n.getLocalizedString("MF_Acc_Detail_Title");
	if(isNotBlank(gblFundName)) {
		frmIBMutualFundsSummary.lblUsefulValue1.text = gblFundName;
	}
	if(isNotBlank(gblFundNickName)) {
		frmIBMutualFundsSummary.lblAccountNameHeader.text = gblFundNickName;
	}
	frmIBMutualFundsSummary.lblOrdToBProcess.text = kony.i18n.getLocalizedString("MF_lbl_Order_tobe_process");
	frmIBMutualFundsSummary.lnkFullStatement.text = kony.i18n.getLocalizedString("MF_lbl_Full_statement");
	frmIBMutualFundsSummary.lnkTaxDoc.text = kony.i18n.getLocalizedString("MF_lbl_Tax_doc");
			
	frmIBMutualFundsSummary.lblUseful1.text = kony.i18n.getLocalizedString("MF_lbl_Fund_name");
    frmIBMutualFundsSummary.lblUseful2.text = kony.i18n.getLocalizedString("MF_lbl_Unit_holder_no");
    frmIBMutualFundsSummary.lblUseful3.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
    frmIBMutualFundsSummary.lblUseful4.text = kony.i18n.getLocalizedString("MF_lbl_Units");
    frmIBMutualFundsSummary.lblUseful5.text = kony.i18n.getLocalizedString("MF_lbl_NAV_unit");
    frmIBMutualFundsSummary.lblUseful6.text = kony.i18n.getLocalizedString("MF_lbl_Cost");
    frmIBMutualFundsSummary.lblUseful7.text = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
    frmIBMutualFundsSummary.lblUseful8.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
    frmIBMutualFundsSummary.lblUseful9.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_percent");
	frmIBMutualFundsSummary.lblUseful10.text = kony.i18n.getLocalizedString("MF_lbl_LTF_Units");
}

function frmIBMFAcctFullStatementPreShow(){
	frmIBMFAcctFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
	frmIBMFAcctFullStatement.btnsubmit.text = kony.i18n.getLocalizedString("MyActivitiesIB_Submit");
	frmIBMFAcctFullStatement.btnback.text = kony.i18n.getLocalizedString("Receipent_Back");
	frmIBMFAcctFullStatement.linknext.text = kony.i18n.getLocalizedString("Next");
	frmIBMFAcctFullStatement.linkprev.text = kony.i18n.getLocalizedString("keyPrev");
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
		frmIBMFAcctFullStatement.lblname.text = gblSelFundNickNameEN;
	}else{
		frmIBMFAcctFullStatement.lblname.text = gblSelFundNickNameTH;
	}
	getMonthCycleMFIB();
	disableMFPageNumbers();
	nextpageStmt = 1;
	startPageNo = 1;
	currentpageStmt = 1;
	startDateIB = dateFormatChangeMF(gblTxnHistDate1);
	endDateIB = dateFormatChangeMF(gblTxnHistDate2);
	startFullStatementServiceIB(startDateIB, endDateIB, nextpageStmt);
	setTableHeaderLabelFullStmt();
}

function frmIBMFAcctOrderToBProceedPreShow(){
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
		frmIBMFAcctOrderToBProceed.lblname.text = gblSelFundNickNameEN;
	}else{
		frmIBMFAcctOrderToBProceed.lblname.text = gblSelFundNickNameTH;
	}
 	frmIBMFAcctOrderToBProceed.lblamt.text= frmIBMutualFundsSummary.lblAccountBalanceHeader.text; 
	frmIBMFAcctOrderToBProceed.imgacnt.src = frmIBMutualFundsSummary.imgAccountDetailsPic.src;
	frmIBMFAcctOrderToBProceed.btnback.text = kony.i18n.getLocalizedString("Receipent_Back");
	callOrderToBeProcessServiceIB();
	setTableHeaderLabelOrderToBProceed();
}

function updateLocaleDataSegMF(segData){
	var fundName = "";
	var fundNickName = "";
	if(segData != null){
		var locale = kony.i18n.getCurrentLocale();
		for (var i = 0; i < segData.length; i++) {
			var noOfAccts = segData[i].noOfAccts;
			if(locale == "en_US"){
				var accountTxt = "";
				if(isNotBlank(noOfAccts) && parseFloat(noOfAccts) > 1){
					accountTxt = " (" + noOfAccts + " " + kony.i18n.getLocalizedString("MF_lbl_Account") +"s)";
				}else{
					accountTxt = " (" + noOfAccts + " " + kony.i18n.getLocalizedString("MF_lbl_Account") +")";
				}
				segData[i].lblHead = segData[i].lblHeadEN + accountTxt;
				segData[i].lblfundName =  segData[i].lblfundNickNameEN;
				fundName = segData[i].lblfundNameEN;
				fundNickName = segData[i].lblfundNickNameEN;
			}else{
				var accountTxt = " (" + noOfAccts + " " + kony.i18n.getLocalizedString("MF_lbl_Account") +")";
				segData[i].lblHead = segData[i].lblHeadTH + accountTxt;
				segData[i].lblfundName =  segData[i].lblfundNickNameTH;
				fundName = segData[i].lblfundNameTH;
				fundNickName = segData[i].lblfundNickNameTH;
			}
			segData[i].lblunRealizedPL = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
			segData[i].lblinvestment = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
			
			if(segData[i].lblunitHolderNumber == gblUnitHolderNumber && segData[i].fundCode == gblFundCode) {
				gblFundName = fundName;
				gblFundNickName = fundNickName;
			}
	   }
	}
   	return segData;	
}

function setTableHeaderLabelFullStmt(){
 	var headertext = frmIBMFAcctFullStatement.dgfullstmt.columnHeadersConfig;
	headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Orderdate");
	headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_TransactionDate");
	headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Trans");
	headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Amount") + " (" + kony.i18n.getLocalizedString("currencyThaiBaht")+ ")";
	headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Unit");
	headertext[5].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Investment_value") + " (" + kony.i18n.getLocalizedString("currencyThaiBaht")+ ")";
	headertext[6].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Unit_balance");
	headertext[7].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Price");
	headertext[8].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Channel");
 }
 
 function setTableHeaderLabelOrderToBProceed(){
 	var headertext = frmIBMFAcctOrderToBProceed.dgOrder.columnHeadersConfig;
	headertext[0].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Order_no");
	headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Orderdate");
	headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Effectivedate");
	headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Amount") + " (" + kony.i18n.getLocalizedString("currencyThaiBaht")+ ")";
	headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Unit");
	headertext[5].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Trans");
	headertext[6].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Status");
	headertext[7].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MF_thr_Channel");
 }
 
function viewMFFullStatementIB(viewType) {
 	showLoadingScreenPopup();
 	totalGridData = [];
 	gblViewType = viewType;
 	var locale = kony.i18n.getCurrentLocale();
 	if(gblViewType == "F"){
 		frmIBMFAcctFullStatement.btnsubmit.text = kony.i18n.getLocalizedString("MyActivitiesIB_Submit");
		if(locale == "en_US"){
			frmIBMFAcctFullStatement.lblname.text = gblSelFundNickNameEN;
		}else{
			frmIBMFAcctFullStatement.lblname.text = gblSelFundNickNameTH;
		}
 		disableMFPageNumbers();
 		endDateIB = getTodaysDateStmtMF();
 		startDateIB = dateFormatChangeMF(new Date(new Date().getFullYear(), new Date().getMonth(), 1));
 		frmIBMFAcctFullStatement.show();
 	}else{
		if(locale == "en_US"){
			frmIBMFAcctOrderToBProceed.lblname.text = gblSelFundNickNameEN;
		}else{
			frmIBMFAcctOrderToBProceed.lblname.text = gblSelFundNickNameTH;
		}
 		frmIBMFAcctOrderToBProceed.show();
 	}
 	
}

function dateFormatChangeMF(dateIB) {
 	var d = new Date(dateIB);
 	var curr_date = d.getDate();
 	var curr_month = d.getMonth() + 1; //Months are zero based
 	var curr_year = d.getFullYear();
 	if (curr_month < 10)
 		curr_month = "0" + curr_month;
 	if (curr_date < 10)
 		curr_date = "0" + curr_date;
 	var finalDate = curr_date + "/" + curr_month + "/" + curr_year;
 	return finalDate;
}

function getTodaysDateStmtMF() {
	var day = new Date();
	var dd = day.getDate();
	var mm = day.getMonth() + 1; //January is 0!
	var yyyy = day.getFullYear();
	if (dd < 10)
		dd = '0' + dd;
	if (mm < 10)
		mm = '0' + mm;
	day = dd + "/" + mm + "/" + yyyy;
	return day;
}

function viewMFAccntFullStatementIB() {
	var locale = kony.i18n.getCurrentLocale();
	if(gblViewType == "O"){
		if(locale == "en_US"){
			frmIBMFAcctOrderToBProceed.lblname.text = gblSelFundNickNameEN;
		}else{
			frmIBMFAcctOrderToBProceed.lblname.text = gblSelFundNickNameTH;
		}
	 	frmIBMFAcctOrderToBProceed.lblamt.text= frmIBMutualFundsSummary.lblAccountBalanceHeader.text; 
		frmIBMFAcctOrderToBProceed.imgacnt.src = frmIBMutualFundsSummary.imgAccountDetailsPic.src;
		frmIBMFAcctOrderToBProceed.btnback.text = kony.i18n.getLocalizedString("Receipent_Back");
		onOrderToBProceedClick();
	}else{
		frmIBMFAcctFullStatement.lblname.text= frmIBMutualFundsSummary.lblAccountNameHeader.text; 
	 	frmIBMFAcctFullStatement.lblamt.text= frmIBMutualFundsSummary.lblAccountBalanceHeader.text; 
		frmIBMFAcctFullStatement.imgacnt.src = frmIBMutualFundsSummary.imgAccountDetailsPic.src;
	 	
	 	getMonthCycleMFIB();
	 	nextpageStmt = 1;
	 	disableMFPageNumbers();
	 	frmIBMFAcctFullStatement.dgfullstmt.removeAll();
		frmIBMFAcctFullStatement.hbxstmt.setVisibility(true);
		frmIBMFAcctFullStatement.dgfullstmt.setVisibility(true);
		frmIBMFAcctFullStatement.hbxslider.setVisibility(true);
		frmIBMFAcctFullStatement.hbxmonthcycle.setVisibility(true);
		frmIBMFAcctFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
		frmIBMFAcctFullStatement.btnback.text = kony.i18n.getLocalizedString("Receipent_Back");
		onMFFullStatementClick();
	}
 }

function startFullStatementServiceIB(startdateIB, enddateIB, pageNumber) {
           totalGridData=[];
            showLoadingScreenPopup();
            var inputParam = {};
            inputParam["dateFrom"] = startdateIB;
           inputParam["dateTo"] = enddateIB;
           inputParam["pageNumber"] = pageNumber;
           inputParam["fundCode"] = gblFundCode;
           inputParam["unitHolderNumber"] = removeHyphenIB(gblUnitHolderNumber);
           frmIBMFAcctFullStatement.dgfullstmt.removeAll();
           frmIBMFAcctFullStatement.hbxbottomfuture.setVisibility(true);
           frmIBMFAcctFullStatement.hbxNoRcdFound.setVisibility(false);
           frmIBMFAcctFullStatement.dgfullstmt.setVisibility(true);
           invokeServiceSecureAsync("getMFFullStamentAllRecords", inputParam, startFullStatementServiceIBCallBack);
    //invokeServiceSecureAsync("MFUHFullStmntInquiry", inputParam, startFullStatementServiceIBCallBack);
}


function startFullStatementServiceIBCallBack(status,resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
 			var tempData = [];
 			if(resulttable["StatusCode"] == 0){
	 			if (resulttable["listOrderDS"].length == 0) {
	 				disableMFPageNumbers();
	 				setDisablePDF();
	 				dismissLoadingScreenPopup();
	 				frmIBMFAcctFullStatement.hbxNoRcdFound.setVisibility(true);
	 				frmIBMFAcctFullStatement.dgfullstmt.setVisibility(false);
	 				return;
	 			}
	 			setEnablePDF();
	 			var locale = kony.i18n.getCurrentLocale();
	        	var tranTypeHub = "TranTypeHubEN";
	        	var channelHub = "ChannelHubEN";
				if (locale == "th_TH"){		      
			      	tranTypeHub = "TranTypeHubTH";
	        		channelHub = "ChannelHubTH";
			    }
				for (var i = 0; i < resulttable["listOrderDS"].length; i++) {
					var segTable = {
						"seqid" : i ,
						"column1": {
							"lbltransid": {
								"text": resulttable["listOrderDS"][i]["OrderDate"] 	// order date
							}
						},
						"column2": {
							"lbltransid": {
								"text": resulttable["listOrderDS"][i]["EffDate"]	// transaction date
							}
						},
						"column4": {
							"lbltransid": {
								"text": resulttable["listOrderDS"][i][tranTypeHub]	// transaction type
							}
						},
						"column5": {
							"lbltransid": {
								"text": commaFormatted(resulttable["listOrderDS"][i]["Amount"]) // amount
							}
						},
						"column6": {
							"lbltransid": {
								"text":  verifyDisplayUnit(resulttable["listOrderDS"][i]["Unit"])	// unit
							}
						},
						"column7": {
							"lbltransid": {
								"text": commaFormatted(resulttable["listOrderDS"][i]["InvestValue"]) // investment value
							}
						},
						"column8": {
							"lbltransid": {
								"text": verifyDisplayUnit(resulttable["listOrderDS"][i]["UnitBalance"]) // unit balance
							}
						},
						"column9": {
							"lbltransid": {
								"text":  verifyDisplayUnit(resulttable["listOrderDS"][i]["Price"])	// price
							}
						},
						"column10": {
							"lbltransid": {
								"text": resulttable["listOrderDS"][i][channelHub]
							}
						}
					}
					totalPage = resulttable["listOrderDS"][i]["TotalPage"];
					totalRecord = resulttable["listOrderDS"][i]["TotalRecord"];
					gblMaxRecordPerPage = resulttable["transPerPageIB"];
					if(!isNotBlank(gblMaxRecordPerPage)) gblMaxRecordPerPage = 15;
					tempData.push(segTable);
				}
				appendDataMF(tempData);
			 	dismissLoadingScreenPopup();
 			} else {
				dismissLoadingScreenPopup();
	        	showAlert(resulttable["StatusDesc"], kony.i18n.getLocalizedString("info"));
				return false;
			}
		} else {
			dismissLoadingScreenPopup();
	       	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	} else {
		dismissLoadingScreenPopup();
	}
}

//Order to be process
function callOrderToBeProcessServiceIB(){
	var inputParam = {};
	inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber);
	inputParam["funCode"] = gblFundCode;
	inputParam["serviceType"] = "2";
	showLoadingScreenPopup();
	frmIBMFAcctOrderToBProceed.dgOrder.removeAll();
	frmIBMFAcctOrderToBProceed.dgOrder.setVisibility(true);
	frmIBMFAcctOrderToBProceed.lblNotFound.text = "";
	frmIBMFAcctOrderToBProceed.hbxNoRcdFound.skin = hbxNoTopBorderGray;
    invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callOrderToBeProcessServiceCallBack);
}

function callOrderToBeProcessServiceCallBack(status, resulttable){
	if (status == 400) {
        if (resulttable["opstatus"] == 0) {
        	if(resulttable["StatusCode"] == 0){
        		if (resulttable["OrderToBeProcessDS"].length == 0) {
	 				disableMFPageNumbers();
	 				setDisablePDF();
	 				dismissLoadingScreenPopup();
					frmIBMFAcctOrderToBProceed.dgOrder.setVisibility(false);
					frmIBMFAcctOrderToBProceed.lblNotFound.text = kony.i18n.getLocalizedString("MF_MSG_No_Record");
					frmIBMFAcctOrderToBProceed.lblNotFound.setVisibility(true);
	 				return;
	 			}
	        	var tempData = [];
	        	var locale = kony.i18n.getCurrentLocale();
	        	var tranTypeHub = "TranTypeHubEN";
	        	var statusHub = "StatusHubEN";
	        	var channelHub = "ChannelHubEN";
				if (locale == "th_TH"){		      
			      	tranTypeHub = "TranTypeHubTH";
	        		statusHub = "StatusHubTH";
	        		channelHub = "ChannelHubTH";
			    }
				for (var i = 0; i < resulttable["OrderToBeProcessDS"].length; i++) {
					var segTable = {
						"seqid" : i ,
						"column1": {
							"lbltransid": {
								"text": resulttable["OrderToBeProcessDS"][i]["ItemNo"]		// Order No.
							}
						},
						"column2": {
							"lbltransid": {
								"text": resulttable["OrderToBeProcessDS"][i]["OrderDate"]	// Order Date
							}
						},
						"column4": {
							"lbltransid": {
								"text": resulttable["OrderToBeProcessDS"][i]["EfftDate"]	// Effect Date
							}
						},
						"column5": {
							"lbltransid": {
								"text": commaFormatted(resulttable["OrderToBeProcessDS"][i]["Amount"])// Amount
							}
						},
						"column6": {
							"lbltransid": {
								"text":  verifyDisplayUnit(resulttable["OrderToBeProcessDS"][i]["Unit"])	// Unit
							}
						},
						"column7": {
							"lbltransid": {
								"text": resulttable["OrderToBeProcessDS"][i][tranTypeHub]  	// Transaction Type
							}
						},
						"column8": {
							"lbltransid": {
								"text": resulttable["OrderToBeProcessDS"][i][statusHub] 	// Status
							}
						},
						"column9": {
							"lbltransid": {
								"text": resulttable["OrderToBeProcessDS"][i][channelHub]	// Channel
							}
						}
					}
					tempData.push(segTable);
				}
				appendDataMF(tempData);
	        	dismissLoadingScreenPopup();
        	}else{
        		dismissLoadingScreenPopup();
	        	showAlert(kony.i18n.getLocalizedString("keyOrderToBProError"), kony.i18n.getLocalizedString("info"));
				return false;
        	}
        } else {
        	dismissLoadingScreenPopup();
	        showAlert(kony.i18n.getLocalizedString("keyOrderToBProError"), kony.i18n.getLocalizedString("info"));
			return false;
        }
     }else{
        dismissLoadingScreenPopup();
     }
}

function onMFFullStatementClick() {
	gblViewType = "F";
	setSortBtnSkinMF();
	getMonthCycleMFIB();
	setDisablePDF();
	var locale = kony.i18n.getCurrentLocale();
	if(locale == "en_US"){
		frmIBMFAcctFullStatement.lblname.text = gblSelFundNickNameEN;
	}else{
		frmIBMFAcctFullStatement.lblname.text = gblSelFundNickNameTH;
	}
	frmIBMFAcctFullStatement.hbxstmt.skin = hbxborder;
	frmIBMFAcctFullStatement.dgfullstmt.setVisibility(true);
 	frmIBMFAcctFullStatement.hbxStmtPdf.setVisibility(true);
 	frmIBMFAcctFullStatement.lblselect.text = kony.i18n.getLocalizedString("MyActivitiesIB_Duration");
 	frmIBMFAcctFullStatement.hbxslider.setVisibility(true);
 	frmIBMFAcctFullStatement.hbxmonthcycle.setVisibility(true);
 	setTableHeaderLabelFullStmt();
 	totalGridData = [];
 	nextpageStmt = 1;
 	startPageNo = 1;
 	startDateIB = dateFormatChangeMF(gblTxnHistDate1);
 	endDateIB = dateFormatChangeMF(gblTxnHistDate2);
 	startFullStatementServiceIB(startDateIB, endDateIB, nextpageStmt);
 }
 
 function onOrderToBProceedClick() {
 	gblViewType = "O";
 	setTableHeaderLabelOrderToBProceed();
 	totalGridData = [];
 	callOrderToBeProcessServiceIB();
 }
  
function appendDataMF(tempData) {
	totalGridData = tempData;
	if(gblViewType == "F"){
		totalRecord = totalGridData.length;
		totalPage = Math.ceil(totalRecord / gblMaxRecordPerPage);
		if(totalPage > 1){
			frmIBMFAcctFullStatement.hbxbottomfuture.setVisibility(true);
		}else{
			frmIBMFAcctFullStatement.hbxbottomfuture.setVisibility(false);
		}
		if (currentpageStmt == 1) {
	 		frmIBMFAcctFullStatement.link1.skin = lnkNormal;
	 		frmIBMFAcctFullStatement.link2.skin = lnkIB18pxBlueUnderline;
	 		frmIBMFAcctFullStatement.link3.skin = lnkIB18pxBlueUnderline;
	 		frmIBMFAcctFullStatement.link4.skin = lnkIB18pxBlueUnderline;
	 		frmIBMFAcctFullStatement.link5.skin = lnkIB18pxBlueUnderline;
	 	}
		loadPagenumbersInMFStmt();
	}
  	loadsegdataInMFStmt(currentpageStmt);
}
 
function loadsegdataInMFStmt(page) {
	if(gblViewType == "F"){
	 	invokeCommonIBLogger("page " + page);
	 	nextpageStmt = page;
	 	if (parseInt(frmIBMFAcctFullStatement.link5.text) < totalPage) {
	 		frmIBMFAcctFullStatement.linknext.setVisibility(true);
	 	} else if (parseInt(frmIBMFAcctFullStatement.link5.text) >= totalPage) {
	 		frmIBMFAcctFullStatement.linknext.setVisibility(false);
	 	} else if (parseInt(frmIBMFAcctFullStatement.link5.text) >= totalPage) {
	 		frmIBMFAcctFullStatement.linknext.setVisibility(true);
	 	}
	 	if (parseInt(frmIBMFAcctFullStatement.link1.text) > 1 ) {
	 		frmIBMFAcctFullStatement.linkprev.setVisibility(true);
	 	} else {
	 		frmIBMFAcctFullStatement.linkprev.setVisibility(false);
	 	}
 		loadMFFullStmtData(page);
 	}else{
		frmIBMFAcctOrderToBProceed.dgOrder.setData(totalGridData);
 	}
}

function loadMFFullStmtData(){
	var fromIdx = (currentpageStmt * gblMaxRecordPerPage) - gblMaxRecordPerPage;
	var toIdx = (currentpageStmt * gblMaxRecordPerPage);
	if(fromIdx < 0) fromIdx = 0;
	if(toIdx > totalRecord) toIdx = totalRecord;
	var tmpData = [];
	for(var i = fromIdx; i < toIdx; i++){
		tmpData.push(totalGridData[i]);
	}
	frmIBMFAcctFullStatement.dgfullstmt.removeAll();
	frmIBMFAcctFullStatement.dgfullstmt.setData(tmpData);
}
  
function loadPagenumbersInMFStmt() {
 	frmIBMFAcctFullStatement.link1.text = "";
 	frmIBMFAcctFullStatement.link2.text = "";
 	frmIBMFAcctFullStatement.link3.text = "";
 	frmIBMFAcctFullStatement.link4.text = "";
 	frmIBMFAcctFullStatement.link5.text = "";
 	invokeCommonIBLogger("page nos  : " + totalPage);

	if (nextpageStmt <= totalPage) {
		frmIBMFAcctFullStatement.link1.setVisibility(true);
		frmIBMFAcctFullStatement.link1.text = startPageNo;
		nextpageStmt = startPageNo + 1;
	}
	if (nextpageStmt <= totalPage) {
		frmIBMFAcctFullStatement.link2.setVisibility(true);
		frmIBMFAcctFullStatement.link2.text = nextpageStmt;
		nextpageStmt = nextpageStmt + 1;
	}
	if (nextpageStmt <= totalPage) {
		frmIBMFAcctFullStatement.link3.setVisibility(true);
		frmIBMFAcctFullStatement.link3.text = nextpageStmt;
		nextpageStmt = nextpageStmt + 1;
	}
	if (nextpageStmt <= totalPage) {
		frmIBMFAcctFullStatement.link4.setVisibility(true);
		frmIBMFAcctFullStatement.link4.text = nextpageStmt;
		nextpageStmt = nextpageStmt + 1;
	}
	if (nextpageStmt <= totalPage) {
		frmIBMFAcctFullStatement.link5.setVisibility(true);
		frmIBMFAcctFullStatement.link5.text = nextpageStmt;
	}
}

function disableMFPageNumbers() {
 	frmIBMFAcctFullStatement.link1.setVisibility(false);
 	frmIBMFAcctFullStatement.link2.setVisibility(false);
 	frmIBMFAcctFullStatement.link3.setVisibility(false);
 	frmIBMFAcctFullStatement.link4.setVisibility(false);
 	frmIBMFAcctFullStatement.link5.setVisibility(false);
 	frmIBMFAcctFullStatement.linkprev.setVisibility(false);
 	frmIBMFAcctFullStatement.linknext.setVisibility(false);
}

function prevPageloadInMFStmt(prevpage) {
 	nextpageStmt = parseInt(prevpage) - 1;
 	startPageNo = nextpageStmt;
 	frmIBMFAcctFullStatement.link1.text = nextpageStmt;
 	frmIBMFAcctFullStatement.link2.text = nextpageStmt + 1;
 	frmIBMFAcctFullStatement.link3.text = nextpageStmt + 2;
 	frmIBMFAcctFullStatement.link4.text = nextpageStmt + 3;
 	frmIBMFAcctFullStatement.link5.text = nextpageStmt + 4;
 	frmIBMFAcctFullStatement.link1.skin = lnkNormal;
 	frmIBMFAcctFullStatement.link2.skin = lnkIB18pxBlueUnderline;
 	frmIBMFAcctFullStatement.link3.skin = lnkIB18pxBlueUnderline;
 	frmIBMFAcctFullStatement.link4.skin = lnkIB18pxBlueUnderline;
 	frmIBMFAcctFullStatement.link5.skin = lnkIB18pxBlueUnderline;
 	loadCurrentPage(startPageNo);
 }
 
function nextPageloadInMFStmt(nextpage) {
 	if(nextpage == "") nextpage = 5;
 	nextpageStmt = parseInt(nextpage) + 1;
 	startPageNo = nextpageStmt - 4;
 	frmIBMFAcctFullStatement.link5.skin = lnkNormal;
 	frmIBMFAcctFullStatement.link2.skin = lnkIB18pxBlueUnderline;
 	frmIBMFAcctFullStatement.link3.skin = lnkIB18pxBlueUnderline;
 	frmIBMFAcctFullStatement.link4.skin = lnkIB18pxBlueUnderline;
 	frmIBMFAcctFullStatement.link1.skin = lnkIB18pxBlueUnderline;
 	frmIBMFAcctFullStatement.link1.text = nextpageStmt - 4;
 	frmIBMFAcctFullStatement.link2.text = nextpageStmt - 3;
 	frmIBMFAcctFullStatement.link3.text = nextpageStmt - 2;
 	frmIBMFAcctFullStatement.link4.text = nextpageStmt - 1;
 	frmIBMFAcctFullStatement.link5.text = nextpageStmt;
 	loadCurrentPage(nextpageStmt);
 }
 
function loadCurrentPage(pageNo){
	currentpageStmt = pageNo;
	loadPagenumbersInMFStmt();
	loadsegdataInMFStmt(currentpageStmt);
}
  
function setSortBtnSkinMF() {
 	frmIBMFAcctFullStatement.dgfullstmt.removeAll();
 	disableMFPageNumbers();
 	frmIBMFAcctFullStatement.linknext.setVisibility(false);
 	frmIBMFAcctFullStatement.linkprev.setVisibility(false);
 }
 
function setDisablePDF(){
 	frmIBMFAcctFullStatement.btnpdf.setVisibility(false);
 	frmIBMFAcctFullStatement.btnprinter.setVisibility(false);
 	frmIBMFAcctFullStatement.btncsv.setVisibility(false);
}

function setEnablePDF(){
 	frmIBMFAcctFullStatement.btnpdf.setVisibility(true);
 	frmIBMFAcctFullStatement.btnprinter.setVisibility(true);
 	frmIBMFAcctFullStatement.btncsv.setVisibility(true);
}
 
function savePDFMutualFundFullStatement(fileType, templateName) {
	savePDFMutualFundFullStatementService(fileType, templateName);
}

function savePDFMutualFundFullStatementService(fileType, templateName) {
            var inputParam = {};     
    var totalData = {
             "localeId": kony.i18n.getCurrentLocale()         
     };

    if (gblDeviceInfo.name == "thinclient"){
            inputParam["channelId"] = GLOBAL_IB_CHANNEL;
    } else{
        inputParam["channelId"] = GLOBAL_MB_CHANNEL;
            }
    inputParam["filetype"] = fileType;
    inputParam["templatename"] = templateName;
    inputParam["unitHolderNo"] = gblUnitHolderNumber;
    inputParam["fundName"] = frmIBMFAcctFullStatement.lblname.text;
    inputParam["investmentValue"] = frmIBMFAcctFullStatement.lblamt.text;
    inputParam["datajson"] = JSON.stringify(totalData, "");
       
    var url = "";
    invokeServiceSecureAsync("generatePdfImageForMFFullStmts", inputParam, ibRenderFileCallbackfunction);
}


function generateTaxDocPdf(){
		
	var inputParam = {};
	inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber);
	inputParam["funCode"] = gblFundCode;
	showLoadingScreenPopup();
    invokeServiceSecureAsync("MFTaxDocumentView", inputParam, ibRenderFileCallbackfunction);
}

 /**
  * On submit button click
  */

function onMFSubmitbtnClick() {
 	var startdate = gblTxnHistDate1;
 	startDateIB = dateFormatChangeMF(startdate);
 	endDateIB = dateFormatChangeMF(gblTxnHistDate2);
 	
 	totalGridData = [];
 	currentpageStmt = 1;
 	nextpageStmt = 1;
 	startPageNo = 1;
 	setSortBtnSkinMF();
 	disableMFPageNumbers();
 	if(gblViewType == "F"){
 		frmIBMFAcctFullStatement.dgfullstmt.setVisibility(true);
 		startFullStatementServiceIB(startDateIB, endDateIB, nextpageStmt);
 	}
}

function getMonthCycleMFIB() {
 	var montharray = [];
    var locale = kony.i18n.getCurrentLocale();
     
 	montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
 		kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
 	 	kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
 	  	kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
 	var currentForm = kony.application.getCurrentForm();
 	
 	var month = new Date();
 	var day = month.getDate();
 	var mm = month.getMonth();
 	var yy = month.getFullYear();
 	var montharrayfull = [];
 		montharrayfull = [kony.i18n.getLocalizedString("keyCalendarJanuary"), kony.i18n.getLocalizedString("keyCalendarFebruary"), kony.i18n.getLocalizedString("keyCalendarMarch"), 
 		kony.i18n.getLocalizedString("keyCalendarApril"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJune"),
 		kony.i18n.getLocalizedString("keyCalendarJuly"), kony.i18n.getLocalizedString("keyCalendarAugust"),kony.i18n.getLocalizedString("keyCalendarSeptember"),
 	 	kony.i18n.getLocalizedString("keyCalendarOctober"), kony.i18n.getLocalizedString("keyCalendarNovember"), kony.i18n.getLocalizedString("keyCalendarDecember")];
 	
 	var dateToday = day + " " + montharrayfull[mm] + " " + yy;
 	currentForm.lbltoday.text = dateToday;
 	
 		currentForm.lblmonth7.text = montharray[mm];
 	
 	mm = mm - 1;
 	if (mm == -1) {
 		mm = 11;
 		yy = yy - 1;
 	}
 		currentForm.lblmonth6.text = montharray[mm];
 	mm = mm - 1;
 	if (mm == -1) {
 		mm = 11;
 		yy = yy - 1;
 	}
 	currentForm.lblmonth5.text = montharray[mm];
 	mm = mm - 1;
 	if (mm == -1) {
 		mm = 11;
 		yy = yy - 1;
 	}
 	currentForm.lblmonth4.text = montharray[mm];
 	mm = mm - 1;
 	if (mm == -1) {
 		mm = 11;
 		yy = yy - 1;
 	}
 	currentForm.lblmonth3.text = montharray[mm];
 	mm = mm - 1;
 	if (mm == -1) {
 		mm = 11;
 		yy = yy - 1;
 	}
 	currentForm.lblmonth2.text = montharray[mm];
 	mm = mm - 1;
 	if (mm == -1) {
 		mm = 11;
 		yy = yy - 1;
 	}
 	currentForm.lblmonth1.text = montharray[mm];
 }
 
 function initialDateSlider() { 
 	gblTxnHistDate1 = "";
 	gblTxnHistDate2 = "";
 }
	