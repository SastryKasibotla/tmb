







bindataChequeS="";
binLengthChequeS=""
gblChequeConfirm=""
noeligible= "false";
function callcustomerAccountChequeService() {
    showLoadingScreenPopup();
    var inputparam = {};
    inputparam["chequeServiceFlag"] = "true";
    var status = invokeServiceSecureAsync("customerAccountInquiry",inputparam,callcustomerAccountChequeServicecallBck);
}
function  callcustomerAccountChequeServicecallBck(status, resulttable){
if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {  
             if (resulttable["statusCode"] == 0)
             { 
                dismissLoadingScreenPopup();
              
                GLOBAL_CHEQUESERVICE_TABLE=resulttable; 
                populateDataForStopCheque(GLOBAL_CHEQUESERVICE_TABLE);
                if(noeligible=="true")
                eligibleFromAccount(resulttable);				
             } 
             else
             {
              dismissLoadingScreenPopup();
              alert(" " + resulttable["errMsg"]);
             }
            
        }
         else {
            dismissLoadingScreenPopup();
            alert(" " + resulttable["errMsg"]);
        }
   }
}
 
 function eligibleFromAccount(resulttable){
  var count=0;
  if (resulttable["custAcctRec"].length>0){
  
  for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
    
    if(resulttable["custAcctRec"][i]["accType"] =="DDA" && resulttable["custAcctRec"][i]["personalisedAcctStatusCode"] != "02" ){ 
	    count=count+1;
	    
     }               
     }  
   if(count==0)
    showAlert(kony.i18n.getLocalizedString("keyNoEligibleAct"), kony.i18n.getLocalizedString("info"));   
   }
 }
 function populateDataForStopCheque(resulttable){
 //frmIBChequeServiceStopChequeLanding.destroy();
						
                	 var list = []
            		 var count = 0
                     for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                	 if(resulttable["custAcctRec"][i]["StopChequeFlag"]=='Y'){
		               var imageName = "";
		               			var accountIdstop = resulttable["custAcctRec"][i]["accId"];
                    			accountIdstop = accountIdstop.substr(accountIdstop.length - 4);
                    			
		              
		              		nicknameStopEng="";
                              nicknameStopThai=""; 
                             if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]=="" ||GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]==null){
                           	 nicknameStopThai  =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"]+" "+accountIdstop;
                             nicknameStopEng = GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"]+" "+accountIdstop;
                            
                            }else{
                            nicknameStopEng =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]
                            nicknameStopThai=  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]
                            }		
				var prodName = "";
		         var locale = kony.i18n.getCurrentLocale();     
		              if (locale == "en_US") {
		               prodName = resulttable["custAcctRec"][i]["ProductNameEng"];
		              }else{
		               prodName = resulttable["custAcctRec"][i]["ProductNameThai"];
		              }
		              
		              imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
		               if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]=="" ||GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]==null){
		                	
		                	
                     			if (locale == "en_US") {
								nicknameStop =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"]+" "+accountIdstop
								} else {
								nicknameStop =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"]+" "+accountIdstop
								}
		                	
		                	
		                }else{
		                	nicknameStop =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]
		                }
					/*	if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || resulttable["custAcctRec"][i][
								"VIEW_NAME"] == "PRODUCT_CODE_NOFIXED" || resulttable["custAcctRec"][i]["VIEW_NAME"] ==
							"PRODUCT_CODE_SAVINGCARE")
							imageName = "prod_savingd.png"
						else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE")
							imageName = "prod_termdeposits.png"
						else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE")
							imageName = "prod_nofee.png"
						else if (resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE" || resulttable["custAcctRec"]
							[i]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE"||resulttable["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE")
							imageName = "prod_currentac.png"
						*/	 
					//	imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
						if (count == 0) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: prodName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: nicknameStop,
						        lblBalance: resulttable["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : formatAccountNo(resulttable["custAcctRec"][i]["accId"]),
						        lblSliderAccN2:resulttable["custAcctRec"][i]["accType"],
						        lblFidindent:resulttable["custAcctRec"][i]["fiident"],
						          lblCustNameEn:nicknameStopEng,
                                 lblCustNameTh:nicknameStopThai,
                                 lblProdNameEN:resulttable["custAcctRec"][i]["ProductNameEng"],
                                 lblProdNameTH:resulttable["custAcctRec"][i]["ProductNameThai"],
						        lblRemFeeval:"",
                                lblRemFee:""
							})
							count = count + 1
						}
						else if (count == 1) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: prodName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: nicknameStop,
						        lblBalance: resulttable["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : formatAccountNo(resulttable["custAcctRec"][i]["accId"]),
						        lblSliderAccN2:resulttable["custAcctRec"][i]["accType"],
						        lblFidindent:resulttable["custAcctRec"][i]["fiident"],
						          lblCustNameEn:nicknameStopEng,
                                 lblCustNameTh:nicknameStopThai,
                                 lblProdNameEN:resulttable["custAcctRec"][i]["ProductNameEng"],
                                 lblProdNameTH:resulttable["custAcctRec"][i]["ProductNameThai"],
		                        lblRemFeeval:"",
                                lblRemFee:""
							})
							count = count + 1
						}
						else if (count == 2) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: prodName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: nicknameStop,
						        lblBalance: resulttable["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblActNoval : formatAccountNo(resulttable["custAcctRec"][i]["accId"]),
						        lblSliderAccN2:resulttable["custAcctRec"][i]["accType"],
						        lblFidindent:resulttable["custAcctRec"][i]["fiident"],
						        lblCustNameEn:nicknameStopEng,
                               lblCustNameTh:nicknameStopThai,
                               lblProdNameEN:resulttable["custAcctRec"][i]["ProductNameEng"],
                               lblProdNameTH:resulttable["custAcctRec"][i]["ProductNameThai"],
						        lblRemFeeval:"",
                                lblRemFee:""
							})
							count = 0
						}
					}
				
                  } //for
            
            
            
            
        if(list.length>0)
		{
			frmIBChequeServiceStopChequeLanding.imageAcc.setVisibility(true);
			frmIBChequeServiceStopChequeLanding.imgtmblogo.setVisibility(false);
			frmIBChequeServiceStopChequeLanding.customFromAccountIB.setVisibility(true);
			frmIBChequeServiceStopChequeLanding.image247423104453863.setVisibility(true);
			frmIBChequeServiceStopChequeLanding.hbxBPAll.skin = "hbxIBSizelimiterRightGrey"
			frmIBChequeServiceStopChequeLanding.vboxBillers.skin="vbxBGGrey"
			frmIBChequeServiceStopChequeLanding.customFromAccountIB.data = list;  
			frmIBChequeServiceStopChequeLanding.hbox866854034714743.isVisible = true;
			frmIBChequeServiceStopChequeLanding.vboxBillers.margin=[0,0,0,0]; 
			noeligible="true";	
			frmIBChequeServiceStopChequeLanding.customFromAccountIB.onSelect = customWidgetSelectEventforChequeService;
        	frmIBChequeServiceStopChequeLanding.show();	
		}
		else
		{
		noeligible="false";
		frmIBChequeServiceStopChequeLanding.imageAcc.setVisibility(false);
		frmIBChequeServiceStopChequeLanding.imgtmblogo.setVisibility(true);
		frmIBChequeServiceStopChequeLanding.customFromAccountIB.setVisibility(false);
		frmIBChequeServiceStopChequeLanding.image247423104453863.setVisibility(false);
		frmIBChequeServiceStopChequeLanding.hbxBPAll.skin = "hbxIBSizelimiterNoLine"
		frmIBChequeServiceStopChequeLanding.vboxBillers.skin = "vbxRightColumn";
		//showAlert(kony.i18n.getLocalizedString("keyAccNotFound"), kony.i18n.getLocalizedString("info"));
		showAlertWithCallBack(kony.i18n.getLocalizedString("keyAccNotFound", kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart));
		frmIBChequeServiceStopChequeLanding.lblFromAccount.text = "";
		frmIBChequeServiceStopChequeLanding.lblAccNo.text = "";
		frmIBChequeServiceStopChequeLanding.imageAcc.src = "";
		frmIBChequeServiceStopChequeLanding.hbox866854034714743.isVisible = false;
		frmIBChequeServiceStopChequeLanding.customFromAccountIB.data=null;
		frmIBChequeServiceStopChequeLanding.vboxBillers.margin=[2,0,0,0];
		}
			
 }
 function populateDataForReturnedChequeService(GLOBAL_CHEQUESERVICE_TABLE){
 
 
// frmIBChequeServiceReturnedChequeLanding.destroy();
 var list = []
            		 var count = 0
            		 var locale = kony.i18n.getCurrentLocale();
                     for (var i = 0; i < GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"].length; i++) {
                	 if(GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ViewReturnChequeFlag"]=='Y'){
		                var imageName = "";
		                var accountId = GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accId"];
                    			accountId = accountId.substr(accountId.length - 4);
                    			
		                if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]=="" ||GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]==null){
		                	
                     		if (locale == "en_US") {
		                	nickname =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"]+" "+accountId;
		                	}else{
		                	nickname =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"]+" "+accountId;
		                	}
		                }else{
		                	nickname =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]
		                }
		                 if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]=="" ||GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]==null){
                   nicknameEn =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"]+" "+accountId;
                   nicknameTh =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"]+" "+accountId;
                  }else{
                   nicknameEn =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]
                   nicknameTh =  GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["acctNickName"]
                  }
                  
                  
                  var prodName = "";
		              if (locale == "en_US") {
		               prodName = GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"];
		              }else{
		               prodName = GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"];
		              }
		
					/*	if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i][
								"VIEW_NAME"] == "PRODUCT_CODE_NOFIXED" || GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["VIEW_NAME"] ==
							"PRODUCT_CODE_SAVINGCARE")
							imageName = "prod_savingd.png"
						else if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE")
							imageName = "prod_termdeposits.png"
						else if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE")
							imageName = "prod_nofee.png"
						else if (GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE" || GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"]
							[i]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE"|| GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE")
							imageName = "prod_currentac.png"
							*/
						imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ICON_ID"]+"&modIdentifier=PRODICON";
						if (count == 0) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: prodName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustName: nickname,
						         lblCustNameTh: nicknameTh,
             					 lblCustNameEn: nicknameEn,
						        lblBalance: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblHiddenActNo  : formatAccountNoNew(GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accId"]),
						        lblSliderAccN2:GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accType"],
						        lblFidindent:GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["fiident"],
						        lblRemFeeval:"",
                                lblRemFee:"",
                                lblProdTh: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"],
              					lblProdEn: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"],
                                lblActNoval:formatAccountNo(formatAccountNoNew(GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accId"]))
						        
							})
							count = count + 1
						}
						else if (count == 1) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: prodName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						         lblCustNameTh: nicknameTh,
              					lblCustNameEn: nicknameEn,
						        lblCustName:nickname,
						        lblBalance: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblHiddenActNo : formatAccountNoNew(GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accId"]),
						        lblSliderAccN2:GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accType"],
						        lblFidindent:GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["fiident"],
						        lblRemFeeval:"",
                                lblRemFee:"",
                                lblProdTh: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"],
              					lblProdEn: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"],
                                lblActNoval:formatAccountNo(formatAccountNoNew(GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accId"]))
							})
							count = count + 1
						}
						else if (count == 2) {
							list.push({
								lblProduct: kony.i18n.getLocalizedString("Product"),
        						lblProductVal: prodName,
						        lblACno: kony.i18n.getLocalizedString("AccountNo"),
						        img1: imageName,
						        lblCustNameTh: nicknameTh,
              					lblCustNameEn: nicknameEn,
						        lblCustName: nickname,
						        lblBalance: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
						        lblHiddenActNo : formatAccountNoNew(GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accId"]),
						        lblSliderAccN2:GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accType"],
						        lblFidindent:GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["fiident"],
						        lblRemFeeval:"",
                                lblRemFee:"",
                                lblProdTh: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameThai"],
              					lblProdEn: GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["ProductNameEng"],
                                lblActNoval:formatAccountNo(formatAccountNoNew(GLOBAL_CHEQUESERVICE_TABLE["custAcctRec"][i]["accId"]))
							})
							count = 0
						}
					}
				
                  } 
            
            
            
      if (list.length > 0) {
        frmIBChequeServiceReturnedChequeLanding.customFromAccountIB.data = list;
        frmIBChequeServiceReturnedChequeLanding.hbox866854034716429.isVisible = false;
        frmIBChequeServiceReturnedChequeLanding.hbox866854034716429.margin = [0, 0, 0, 0];
		frmIBChequeServiceReturnedChequeLanding.vboxBillers.skin="vbxBGGrey"
		frmIBChequeServiceReturnedChequeLanding.vboxBillers.margin=[0,0,0,0];
		 frmIBChequeServiceReturnedChequeLanding.customFromAccountIB.onSelect = customWidgetSelectEventforReturnCheque;
        frmIBChequeServiceReturnedChequeLanding.show();	
    } 
    	else {
    	  
    	//showAlert(kony.i18n.getLocalizedString("keyAccNotFound"), kony.i18n.getLocalizedString("info"));
showAlertWithCallBack(kony.i18n.getLocalizedString("keyAccNotFound", kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart));
        frmIBChequeServiceReturnedChequeLanding.hbox866854034716429.isVisible = true;
        frmIBChequeServiceReturnedChequeLanding.hbox866854034716429.margin = [0, 0, 0, 128];
		frmIBChequeServiceReturnedChequeLanding.vboxBillers.skin="vbxBGGrey"
		frmIBChequeServiceReturnedChequeLanding.vboxBillers.margin=[1,0,0,0];
    }
         //   frmIBChequeServiceReturnedChequeLanding.customFromAccountIB.data = list; 
           
 }
 function callOTPServiceSCS(){
  showLoadingScreenPopup();
  var chequeNo = frmIBChequeServiceStopChequeLanding.txtChequeNo.text;
    var inputParams = {};
    var locale = kony.i18n.getCurrentLocale();
	 inputParams["Channel"] = "StopCheque";
	 inputParams["locale"] = locale;
	 inputParams["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
	 //activity logging in service params
		 inputParams["activityTypeID"] = "044";
		  inputParams["deviceNickName"] = "";
		  inputParams["activityFlexValues1"] = chequeNo;
		  inputParams["activityFlexValues2"] = glb_selectedData.lblActNoval;
		  inputParams["activityFlexValues3"] = "";
		  inputParams["activityFlexValues4"] = "";
		  inputParams["activityFlexValues5"] = "";
		  inputParams["logLinkageId"] = "";
		  inputParams["channelId"] = "01";
		
    frmIBChequeServiceConfirmation.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBChequeServiceConfirmation.lblPlsReEnter.text = " "; 
    frmIBChequeServiceConfirmation.hbxOTPincurrect.isVisible = false;
    frmIBChequeServiceConfirmation.hbxOTPBankRef.isVisible = true;
    frmIBChequeServiceConfirmation.hbxOTPsnt.isVisible = true;
    frmIBChequeServiceConfirmation.txtotp.setFocus(true); 
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackIBreqOTPIBSCS);
 }
 function callBackIBreqOTPIBSCS(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            
            dismissLoadingScreenPopup();
            if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				//alert("Test1");
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (callBackResponse["errCode"] == "JavaErr00001") {
				//alert("Test2");
                //dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            
            frmIBChequeServiceConfirmation.btnOTPReq.skin = btnIBREQotp;
            frmIBChequeServiceConfirmation.btnOTPReq.focusSkin = btnIBREQotp;
            frmIBChequeServiceConfirmation.btnOTPReq.onClick = "";           
        	 if(gblRetryCountRequestOTP<4)
         	{
            kony.timer.schedule("OtpTimer", OTPTimercallbackCheckService, reqOtpTimer, false);
         	}         
         	else
         	{         	
			try {
                kony.timer.cancel("OtpTimer");
            } catch (e) {
                // todo: handle exception
            }
         	}
          	gblOTPReqLimit=gblOTPReqLimit+1;        
       //     setTimeout('OTPTimercallbackCheckService()',reqOtpTimer);
			var bankrefVal="";
   			for(var d=0;d<callBackResponse["Collection1"].length;d++)
   			{
   				 if(callBackResponse["Collection1"][d]["keyName"] == "pac")
   				 {
      				bankrefVal=callBackResponse["Collection1"][d]["ValueString"];
      				break;
     				}
    		}
            frmIBChequeServiceConfirmation.hbxOTPBankRef.setVisibility(true);
            frmIBChequeServiceConfirmation.hbxOTPsnt.setVisibility(true);
            frmIBChequeServiceConfirmation.bankRefVal.text = bankrefVal;
            frmIBChequeServiceConfirmation.hbxOtpBox.setVisibility(true);
            frmIBChequeServiceConfirmation.hbxSuccess.setVisibility(false);
             frmIBChequeServiceConfirmation.lblSuccess.setVisibility(false); 
              frmIBChequeServiceConfirmation.hbxBtn.setVisibility(true);
              frmIBChequeServiceConfirmation.btnGoto.setVisibility(false);
             frmIBChequeServiceConfirmation.lblBillersConfirm.text=kony.i18n.getLocalizedString("Confirmation");
            frmIBChequeServiceConfirmation.lblOtpMsg.text=kony.i18n.getLocalizedString("keyotpmsg");
            //alert("mobile no is:"+gblPHONENUMBER);
            frmIBChequeServiceConfirmation.lblPhnoValue.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            
        if(gblStopChequeOTPFlag!=null && gblStopChequeOTPFlag=="true")
		{
			//frmIBChequeServiceConfirmation.destroy();
			frmIBChequeServiceConfirmation.lblCustName.text=frmIBChequeServiceStopChequeAck.lblCustName.text;
			frmIBChequeServiceConfirmation.lblBalance.text=frmIBChequeServiceStopChequeAck.lblBalance.text;
			frmIBChequeServiceConfirmation.lblAccNo.text=frmIBChequeServiceStopChequeAck.lblAccNo.text;
			frmIBChequeServiceConfirmation.lblAccType2.text=frmIBChequeServiceStopChequeAck.lblAccType2.text
			frmIBChequeServiceConfirmation.lblAccNoVal.text=frmIBChequeServiceStopChequeAck.lblChequeVal.text;
			gblStopChequeOTPFlag="false";
			dismissLoadingScreenPopup();
			frmIBChequeServiceConfirmation.show();
		}
           
            dismissLoadingScreenPopup();
            frmIBChequeServiceConfirmation.txtotp.setFocus(true);
        }else if(callBackResponse["errCode"]=="GenOTPRtyErr00001"){
        	dismissLoadingScreenPopup();
        	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        }
        else {
            dismissLoadingScreenPopup();
            alert("Error " + callBackResponse["errMsg"]);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("Error");
        }
    }
}
 
function validateOTPforChequeService(){
	
    var otpText = frmIBChequeServiceConfirmation.txtotp.text;
    var tokenText=frmIBChequeServiceConfirmation.tbxToken.text;
     checkNumber=frmIBChequeServiceConfirmation.lblAccNoVal.text;
    
    var inputParam = {};
    if(gblTokenSwitchFlag == true ) {
			  inputParam["loginModuleId"] = "IB_HWTKN";
		      inputParam["userStoreId"] = "DefaultStore";
		      inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
		      inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
		      inputParam["userId"] = gblUserName;
		      
		      inputParam["password"] = tokenText
		      
		      inputParam["sessionVal"] = "";
		      inputParam["segmentId"] = "segmentId";
		      inputParam["segmentIdVal"] = "MIB"
		      inputParam["channel"] = "InterNet Banking";
		       inputParam["appID"]=app
		      
		      showLoadingScreenPopup();
    		  invokeServiceSecureAsync("verifyTokenEx", inputParam, startOTPValidationServiceCSAsyncCallback);
	} else {
			  inputParam["retryCounterVerifyOTP"] = gblVerifyOTP;
			  inputParam["password"] = otpText
			  inputParam["userId"] = gblUserName;
			  showLoadingScreenPopup();
              invokeServiceSecureAsync("verifyPasswordEx", inputParam, startOTPValidationServiceCSAsyncCallback);
	}
	
    
}

function startOTPValidationServiceCSAsyncCallback(status, resulttable) {

   
    if (status == 400) {
        
        
        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
             
            dismissLoadingScreenPopup();
            if (gblVerifyOTP <= 3) {
             
            activityLogServiceCall("044","","00","",checkNumber,glb_selectedData.lblActNoval,"","","","");
                callStopChequePaymentService()
                if(gblTokenSwitchFlag == true ) {
					modifyUserUpdate("Activated","0");
				}
            } else {
                popIBChequeServiceLocked.show();
                if(gblTokenSwitchFlag == true ) {
					modifyUserUpdate("Disabled","3");
				}
            }
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreenPopup();
            if (gblVerifyOTP >= 3) {
                popIBChequeServiceLocked.show();
                modifyUserUpdate("Disabled","3");
            } else {
            alert("" + kony.i18n.getLocalizedString("invalidOTP"));
            //showAlert(kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
                //alert("Please enter valid OTP");
            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + resulttable["errMsg"]);
        }
    }
}
 function callStopChequePaymentService(){
   showLoadingScreenPopup();
   var checkNumber = frmIBChequeServiceConfirmation.lblAccNoVal.text
    var inputParams = {};
    inputParams["clientDt"]="";
    inputParams["accId"]=glb_selectedData.lblActNoval.replace(/-/g, "");
    inputParams["acctType"]=glb_selectedData.lblSliderAccN2;
    inputParams["chkNum"]=checkNumber;
    invokeServiceSecureAsync("stopChequePayment", inputParams, stopChequePaymentServiceCallback);
 }
 function stopChequePaymentServiceCallback(status, resulttable){
 if (status == 400) {
        
        
        
        if (resulttable["opstatus"] == 0) {
        	var XPServerStatCode = resulttable["XPServerStatCode"];
            var StatusCode = resulttable["StatusCode"];
            var Severity = resulttable["Severity"];
            var StatusDesc = resulttable["StatusDesc"];
            dismissLoadingScreenPopup();
             if (StatusCode != "0") { 
            	 if(XPServerStatCode=="IV9898")
               	 {
               		 showAlert(kony.i18n.getLocalizedString("keyIncorrectCheque"), kony.i18n.getLocalizedString("info"));
               	 }
               	else
               	{
                alert(" " + resulttable["additionalDS"][resulttable["additionalDS"].length-1]["StatusDesc"]);
               	}
                 activityLogServiceCall("044","","02","",checkNumber,glb_selectedData.lblActNoval,"","","","");
                return false;
            }else{
             activityLogServiceCall("044","","01","",checkNumber,glb_selectedData.lblActNoval,"","","","");
             showStopChequeServiceComplete();  
             GLOBAL_CHEQUESERVICE_TABLE="";         
            }            
        }
        else
             dismissLoadingScreenPopup();    
     }   
 }
 function lockOTPIBStopChequeService() {
        var inputParam = {};
        inputParam["ibUserStatusId"] = "04";
        showLoadingScreenPopup();
        invokeServiceSecureAsync("crmProfileMod", inputParam, callBacklockStopChequeService)
    }

function callBacklockStopChequeService(status, resulttable) {  
    if (status == 400) {
        
        
        if (resulttable["opstatus"] == 0) {
            
            gblUserLockStatusIB=resulttable["IBUserStatusID"];
            frmIBChequeServiceConfirmation.btnConfirm.setEnabled(false);
            frmIBChequeServiceConfirmation.btnCancel.setEnabled(false);
            frmIBChequeServiceConfirmation.btnOTPReq.setEnabled(false);
            dismissLoadingScreenPopup();
            
        } else {
            dismissLoadingScreenPopup();
        }
    }
}
function callReturnChequeService(startDateIBR,endDateIBR) {
		
	var CASAinputParam = {};
	
	CASAinputParam["sessionFlag"] = gblStmntSessionFlag;
//	CASAinputParam["accidVal"] =glb_selectedDataRC.lblSliderAccN2;
//	CASAinputParam["fiIdent"] = glb_selectedDataRC.lblFidindent;
	CASAinputParam["clientDt"]="";
    CASAinputParam["accidVal"] = glb_selectedData.lblHiddenActNo;
    CASAinputParam["fiIdent"] = glb_selectedData.lblFidindent;
	CASAinputParam["BinData"] = bindataChequeS;
	CASAinputParam["BinLength"] = binLengthChequeS;
	CASAinputParam["startDate"] = startDateIBR;
	CASAinputParam["endDate"] = endDateIBR;
	CASAinputParam["locale"] =  kony.i18n.getCurrentLocale();
	CASAinputParam["spName"]="com.fnis.xes.TS";
	CASAinputParam["activityTypeID"] = "043";
	CASAinputParam["activityFlexValues1"] = glb_selectedData.lblActNoval;
	CASAinputParam["activityFlexValues3"] = "";
	CASAinputParam["activityFlexValues4"] = "";
	CASAinputParam["activityFlexValues5"] = "";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("returnChequeInquiry",CASAinputParam, callReturnChequeServiceCallBack);
    }
 function callReturnChequeServiceCallBack(status, callBackResponse){
     
	if (status == 400) {
		
		if (callBackResponse["opstatus"] == 0) {
		//if (callBackResponse["StatusCode"] == 0) { 
		
			dismissLoadingScreenPopup();
			bindataChequeS = callBackResponse["BinData"];
			binLengthChequeS = callBackResponse["BinLength"];
			recordcountStmt = callBackResponse["IBRecsCount"];//"5"
			var tempData = [];
			var resp = callBackResponse;
			//activityLogServiceCall("043","","01","",glb_selectedData.lblActNoval,"","","","","");
			if(callBackResponse["ReturnChequeInfo"].length == 0){
				
				disablePagenumbers();
				//showAlert(kony.i18n.getLocalizedString("keyChequeNoRecs"), kony.i18n.getLocalizedString("info"));
				frmIBChequeServiceViewReturnedCheque.dgfullstmt.removeAll();
				frmIBChequeServiceViewReturnedCheque.lblAlert.setVisibility(true);
				return;
			}
			else
			{
				frmIBChequeServiceViewReturnedCheque.lblAlert.setVisibility(false);
			}
			for (var i = 0; i < callBackResponse["ReturnChequeInfo"].length; i++) {
				var segTable = {
				"seqid" : i,
					            "column6": {
					                "lbltransid": {
					                    "text": callBackResponse["ReturnChequeInfo"][i]["returnReasonDesc"]+" "+callBackResponse["ReturnChequeInfo"][i]["returnReasonDescTH"]
					                }
					            },
					            "column1": {
					                "lbltransid": {
					                    "text": formatDateStmtIB(callBackResponse["ReturnChequeInfo"][i]["postedDt"])
					                }
					            },
					            "column2": {
					                "lbltransid": {
					                    "text": callBackResponse["ReturnChequeInfo"][i]["chequeNo"]
					                }
					            },
					            "column5": {
					                "lbltransid": {
					                    "text": callBackResponse["ReturnChequeInfo"][i]["amt"]
					                }
					            },
					            "column4": {
					                "lbltransid": {
					                    "text": callBackResponse["ReturnChequeInfo"][i]["BankNameEN"]
					                }
					            }
					}
					tempData.push(segTable);
				}
				
				appendDataInReturnCheque(tempData);
			//	frmIBChequeServiceViewReturnedCheque.dgfullstmt.setData(tempData);
	/*}
	else{
             alert(" "+callBackResponse["errMsg"]);
             dismissLoadingScreen();
             return false;       
            }  */
		}
		else {
			dismissLoadingScreen();//dismissLoadingScreenPopup();
			alert("" + kony.i18n.getLocalizedString("ECGenericError"));
			//alert("" + callBackResponse["errMsg"]);
		}
	}
	else {
		if (status == 300) {
			dismissLoadingScreen();//dismissLoadingScreenPopup();
			alert("" + kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
    }
    
function OTPTimercallbackCheckService() {
	frmIBChequeServiceConfirmation.btnOTPReq.skin = btnIBREQotpFocus;
	frmIBChequeServiceConfirmation.btnOTPReq.onClick = callOTPServiceSCS;
	
	try {
	
		kony.timer.cancel("OtpTimer");
	} catch (e) {
		
	}
}    
   
function formatAccountNoNew(accountNo) {
 var formatedValue = accountNo.substring(accountNo.length-10,accountNo.length);
 
 return formatedValue;
}



function stopChequeCompositeService()
 {
	 var inputParam = {};
	 showLoadingScreenPopup();
	 if(gblTokenSwitchFlag == true )
	 {
		 var otpToken = frmIBChequeServiceConfirmation.tbxToken.text;
	        if (otpToken != null) {
	            otpToken = otpToken.trim();
	        } else {
	            dismissLoadingScreenPopup();
	            showAlert(kony.i18n.getLocalizedString("Receipent_tokenId"), kony.i18n.getLocalizedString("info"));
	            //alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
	            return false;
	        }
	        if (otpToken == "") {
	            dismissLoadingScreenPopup();
	            showAlert(kony.i18n.getLocalizedString("Receipent_tokenId"), kony.i18n.getLocalizedString("info"));
	            //alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
	            return false;
	        }
		  inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
	      inputParam["verifyToken_userStoreId"] = "DefaultStore";
	      inputParam["verifyToken_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
	      inputParam["verifyToken_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
	      inputParam["verifyToken_password"] = otpToken;
	      
	      inputParam["verifyToken_segmentId"] = "segmentId";
	      inputParam["verifyToken_segmentIdVal"] = "MIB";
	      inputParam["TokenSwitchFlag"] = true;
	      inputParam["verifyToken_userId"] = gblUserName;
	      
		  //invokeServiceSecureAsync("verifyTokenEx", inputParam, startOTPValidationServiceCSAsyncCallback);
	 }
	 else
	{
		 var otpText = frmIBChequeServiceConfirmation.txtotp.text;
	        if (otpText != null) {
	            otpText = otpText.trim();
	        } else {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
	            return false;
	        }
	        if (otpText == "") {
	            dismissLoadingScreenPopup();
	            showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
	            return false;
	        }
			 
	      //verify password inputs 
		 inputParam["verifyPwd_retryCounterVerifyOTP"] = gblVerifyOTP;
		 inputParam["verifyPwd_password"] = otpText
		 inputParam["verifyPwd_userId"] = gblUserName;
		 
	}
		 inputParam["appID"] = appConfig.appId;
		 inputParam["channel"] ="IB"
		  inputParam["gblVerifyOTP"] =gblVerifyOTP;
		 
		 
		// cheque service params

			var checkNumber = frmIBChequeServiceConfirmation.lblAccNoVal.text
			inputParam["stopCheque_clientDt"]="";
			inputParam["stopCheque_accId"]=glb_selectedData.lblActNoval.replace(/-/g, "");
			inputParam["stopCheque_acctType"]=glb_selectedData.lblSliderAccN2;
			inputParam["stopCheque_chkNum"]=checkNumber;
		 
		 //activity logging in service params
		 inputParam["activityLog_activityTypeID"] = "044";
		  inputParam["activityLog_errorCd"] = ""; 
		  inputParam["activityLog_activityStatus"] ="";
		  inputParam["activityLog_deviceNickName"] = "";
		  inputParam["activityLog_activityFlexValues1"] = checkNumber;
		  inputParam["activityLog_activityFlexValues2"] = glb_selectedData.lblActNoval;
		  inputParam["activityLog_activityFlexValues3"] = "";
		  inputParam["activityLog_activityFlexValues4"] = "";
		  inputParam["activityLog_activityFlexValues5"] = "";
		  inputParam["activityLog_logLinkageId"] = "";
		  inputParam["finActivityLog_channelId"] = "01";
		
		
	    //invokeServiceSecureAsync("stopChequePayment", inputParams, stopChequePaymentServiceCallback);
		
		
		//modifyUpdateUser service params

		inputParam["modifyUpdateUser_userStoreId"] = gblUserName;
		inputParam["modifyUpdateUser_userId"] = gblUserName;
		inputParam["modifyUpdateUser_segmentId"] = "MIB";
		inputParam["modifyUpdateUser_loginModuleIdSMS"] = "IBSMSOTP";
		inputParam["modifyUpdateUser_loginModuleIdHW"] = "IB_HWTKN";
		inputParam["modifyUpdateUser_badLoginCount"] = gblVerifyOTP;
		inputParam["modifyUpdateUser_status"] = "";
		 
        invokeServiceSecureAsync("stopChequePaymentComposite", inputParam, StopChequePaymentCompositeServiceCallback);
 }
 
 function StopChequePaymentCompositeServiceCallback(status, resulttable){
	var invalidOTP = resulttable["invalidOTP"];
	
	var chequeVal = resulttable["errCodeIV9898"];
	
	var IV9898Value = resulttable["IV9898"];
	
	var successResp = resulttable["stopChequePayment"];
	
	var failResp = resulttable["ServerStatusCode"];
	
	if(successResp!=null && successResp!="" && successResp == "Success"){
		dismissLoadingScreenPopup();
		frmIBChequeServiceConfirmation.hboxStatus.setVisibility(false);
		frmIBChequeServiceConfirmation.hbxOtpBox.setVisibility(false);
		frmIBChequeServiceConfirmation.hbxSuccess.setVisibility(true);
		frmIBChequeServiceConfirmation.lblSuccess.setVisibility(true); 
		frmIBChequeServiceConfirmation.hbxBtn.setVisibility(false);
		frmIBChequeServiceConfirmation.btnGoto.setVisibility(true);
		frmIBChequeServiceConfirmation.lblBillersConfirm.text=kony.i18n.getLocalizedString("keylblComplete");
		//alert(frmIBChequeServiceConfirmation.lblBillersConfirm.text)
		//showStopChequeServiceComplete();  
		GLOBAL_CHEQUESERVICE_TABLE=""; 
	
	}else if(chequeVal!=null && chequeVal!="" && chequeVal == "keyIncorrectCheque"){
		showAlert(kony.i18n.getLocalizedString("keyIncorrectCheque"), kony.i18n.getLocalizedString("info"));
		dismissLoadingScreenPopup();
	}else if(IV9898Value!=null && IV9898Value!="" && IV9898Value == "keyIncorrectCheque"){
		showAlert(kony.i18n.getLocalizedString("keyChequeNoRecs"), kony.i18n.getLocalizedString("info"));
		dismissLoadingScreenPopup();
	} else if( failResp != null && failResp != "" && failResp == "IM5500"){
		showAlert(kony.i18n.getLocalizedString("keyCheque_Err_IM5500"), kony.i18n.getLocalizedString("info"));
		dismissLoadingScreenPopup();
	}else if (resulttable["opstatus"] == 8005) {
		//alert("8005")
         if (resulttable["errCode"] == "VrfyOTPErr00001") {
            gblVerifyOTP = resulttable["retryCounterVerifyOTP"];
            dismissLoadingScreenPopup();
            if(gblTokenSwitchFlag ==  true){
            	//alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commented by swapna
            	frmIBChequeServiceConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBChequeServiceConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                frmIBChequeServiceConfirmation.hbxOTPincurrect.isVisible = true;
                frmIBChequeServiceConfirmation.hbxOTPBankRef.isVisible = false;
                frmIBChequeServiceConfirmation.hbxOTPsnt.isVisible = false;
                frmIBChequeServiceConfirmation.txtotp.text = "";
                frmIBChequeServiceConfirmation.txtotp.setFocus(true);
            }else{
            	//alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commented by swapna
            	frmIBChequeServiceConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBChequeServiceConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                frmIBChequeServiceConfirmation.hbxOTPincurrect.isVisible = true;
                frmIBChequeServiceConfirmation.hbxOTPBankRef.isVisible = false;
                frmIBChequeServiceConfirmation.hbxOTPsnt.isVisible = false;
                frmIBChequeServiceConfirmation.txtotp.text = "";
                frmIBChequeServiceConfirmation.txtotp.setFocus(true);
            }
            
            
            return false;
        } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
            dismissLoadingScreenPopup();
            //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
            //startRcCrmUpdateProIB("04");
			handleOTPLockedIB(resulttable);
            return false;
    	} else if (resulttable["errCode"] == "VrfyOTPErr00004") {
			dismissLoadingScreenPopup();
			showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
			return false;
		}
    	else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
			dismissLoadingScreenPopup();
			showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
			return false;
		}
    	else if (resulttable["errCode"] == "VrfyOTPErr00005") {
			dismissLoadingScreenPopup();
			//showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
			if(gblTokenSwitchFlag ==  true){
            	alert("" + kony.i18n.getLocalizedString("invalidOTP"));
            }else{
            	alert("" + kony.i18n.getLocalizedString("invalidOTP"));
            }
			return false;
		} else {
		        frmIBChequeServiceConfirmation.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBChequeServiceConfirmation.lblPlsReEnter.text = " "; 
                frmIBChequeServiceConfirmation.hbxOTPincurrect.isVisible = false;
                frmIBChequeServiceConfirmation.hbxOTPBankRef.isVisible = true;
                frmIBChequeServiceConfirmation.hbxOTPsnt.isVisible = true;
                frmIBChequeServiceConfirmation.txtotp.setFocus(true); 
			dismissLoadingScreenPopup();
			//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			showAlert(resulttable["errMsg"],kony.i18n.getLocalizedString("info"));
			return false;
		}
    }else if(resulttable["opstatus"] == "1"){
    	dismissLoadingScreenPopup();
    	showAlert(kony.i18n.getLocalizedString("keyErrResponseOne"), kony.i18n.getLocalizedString("info"));
    }
    else {  
		dismissLoadingScreenPopup();
		//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		showAlert(resulttable["StatusDesc"],kony.i18n.getLocalizedString("info"));
		return false;
	}
}


 function syncIBChequeServices()
 {
 	if(kony.i18n.getCurrentLocale() == "en_US"){
 				frmIBChequeServiceConfirmation.label476047582115284.containerWeight=14;
 				frmIBChequeServiceConfirmation.txtotp.containerWeight=55;
 				
				
	 	} else {
	 			frmIBChequeServiceConfirmation.label476047582115284.containerWeight=24;
 				frmIBChequeServiceConfirmation.txtotp.containerWeight=47;
 			
	 	}
	 	
	 	if(kony.application.getCurrentForm().id == "frmIBChequeServiceConfirmation" && gblChequeConfirm=="false"){
	 	frmIBChequeServiceConfirmation.lblBillersConfirm.text = kony.i18n.getLocalizedString("keylblComplete");
		 	if(kony.i18n.getCurrentLocale() == "en_US"){
	 			frmIBChequeServiceConfirmation.lblCustName.text = glb_selectedData.lblCustNameEn;
				frmIBChequeServiceConfirmation.lblAccType2.text=glb_selectedData.lblProdNameEN;
				
		 	} else {
		 		frmIBChequeServiceConfirmation.lblCustName.text = glb_selectedData.lblCustNameTh;
		 		frmIBChequeServiceConfirmation.lblAccType2.text=glb_selectedData.lblProdNameTH;
		 	}
	 	
	 	}
	 		if(kony.application.getCurrentForm().id == "frmIBChequeServiceConfirmation" && gblChequeConfirm=="true"){
	 		frmIBChequeServiceConfirmation.lblBankRef.text=kony.i18n.getLocalizedString("keyIBbankrefno");
			if(gblSwitchToken != true){
			frmIBChequeServiceConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
			}
			else
			{
			frmIBChequeServiceConfirmation.label476047582115288.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
			}
	 				frmIBChequeServiceConfirmation.lblBillersConfirm.text = kony.i18n.getLocalizedString("Confirmation");	
	 				frmIBChequeServiceConfirmation.lblOtpMsg.text = kony.i18n.getLocalizedString("keyotpmsg");
		 	if(kony.i18n.getCurrentLocale() == "en_US"){
	 			frmIBChequeServiceConfirmation.lblCustName.text = glb_selectedData.lblCustNameEn;
				frmIBChequeServiceConfirmation.lblAccType2.text=glb_selectedData.lblProdNameEN;
				
		 	} else {
		 		frmIBChequeServiceConfirmation.lblCustName.text = glb_selectedData.lblCustNameTh;
		 		frmIBChequeServiceConfirmation.lblAccType2.text=glb_selectedData.lblProdNameTH;
		 	}
	 	
	 	}
	 	
	 	
	 	if(kony.application.getCurrentForm().id == "frmIBChequeServiceViewReturnedCheque"){
	 	getMonthCycleIB();
	 	frmIBChequeServiceViewReturnedCheque.label47336710533361.text = kony.i18n.getLocalizedString("AccountNo");
		frmIBChequeServiceViewReturnedCheque.lblProd.text = kony.i18n.getLocalizedString("Product");
		
		if(kony.i18n.getCurrentLocale() == "en_US"){
 			frmIBChequeServiceViewReturnedCheque.lblname.text=returnChequegblNameEn;
			frmIBChequeServiceViewReturnedCheque.lblAccType.text=returnChequegblProdNameEn;
				
	 	} else {
	 		frmIBChequeServiceViewReturnedCheque.lblname.text=returnChequegblNameTh;
			frmIBChequeServiceViewReturnedCheque.lblAccType.text=returnChequegblProdNameTh;
	 	}
		
	 	}
	 	
	 	if(kony.application.getCurrentForm().id == "frmIBChequeServiceReturnedChequeLanding"){
	 	populateDataForReturnedChequeService(GLOBAL_CHEQUESERVICE_TABLE);
	 	}
	 	
	 	if(kony.application.getCurrentForm().id == "frmIBChequeServiceStopChequeLanding"){
	 	populateDataForStopCheque(GLOBAL_CHEQUESERVICE_TABLE);
	 	frmIBChequeServiceStopChequeLanding.txtChequeNo.placeholder = kony.i18n.getLocalizedString("keyChequeNo");
	 	
	 	}
	
	if(kony.application.getCurrentForm().id == "frmIBChequeServiceStopChequeAck"){
		
		if(kony.i18n.getCurrentLocale() == "en_US"){
 			frmIBChequeServiceStopChequeAck.lblCustName.text = glb_selectedData.lblCustNameEn;
			frmIBChequeServiceStopChequeAck.lblAccType2.text=glb_selectedData.lblProdNameEN;
	 	} else {
	 		frmIBChequeServiceStopChequeAck.lblCustName.text = glb_selectedData.lblCustNameTh;
			frmIBChequeServiceStopChequeAck.lblAccType2.text=glb_selectedData.lblProdNameTH;
	 	}
		
	}
	
 
 }
