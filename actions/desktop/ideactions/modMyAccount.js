gblMyAccntAddTmpData = []; // Cache memory - global array
gblAccntData = []; // Store all accnt data TMb and NON TMB for further use in this module
NON_TMB_ADD = 0; // Non TMB accnt in cache - global var
TOTAL_NON_TMB_AC = 0;
MAX_ACC_LEN = 0;
TOTAL_AC_ADD = 0; // Total accnt in cache - global var
/*
************************************************************************
     	Module Name : My Account MB
            Author  : Nishant Kumar
            Date    : May 05, 2013
            Purpose : function used in My Account module
************************************************************************
*/
/**
 * Name    : viewAccountTMB()
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : call services based on account type clicked and show My Account view page
 * Input params: segment selected Index, account no.,segment ID
 * Output params: No
 * ServiceCalls: No
 * Called From : onClick of seg of My Account list
 */
 


function viewAccountTMB(index, accNo, segID) {
		frmMyAccountView.lblAccntNoValue.text = "";
		frmMyAccountView.lblAccountNo.isVisible = false;
		frmMyAccountView.lblAccntNoValue.isVisible = false;
		frmMyAccountView.lblAccountName.text = "";
		frmMyAccountView.lblAccntNameValue.text = "";
		frmMyAccountView.lblAccntSuffix.isVisible = false;
		frmMyAccountView.lblAccontSuffixValue.isVisible = false;
		frmMyAccountView.lblAccntSuffix.text = "";
		frmMyAccountView.lblAccontSuffixValue.text = "";
		frmMyAccountView.lblAccountName.isVisible = true;
		frmMyAccountView.lblAccntNameValue.isVisible = true;
		
		kony.print("resetting Benifeciary fields from frmMyAccountView");
		resetBeneficiaryFields(frmMyAccountView);
		
	
	gblsegID = segID;
	var accType = index;
	
	var isTMB = (segID == "segTMBAccntDetails") ? true : false;
	// dummy mapping
	if (isTMB){
		frmMyAccountView.lblNickName.text = frmMyAccountList.segTMBAccntDetails.selectedItems[0].lblTMBAccountDetails;
		
	}else
		frmMyAccountView.lblNickName.text = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].lblOtherBankAccntDetails;
	if (isTMB) {
		frmMyAccountView.imgDepositAccnt.src = frmMyAccountList.segTMBAccntDetails.selectedItems[0].imgAccountPicture;
		
		var accType = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenAcctType;
		gblAccNo=accNo;
		gblAccntType = accType;
		
		if (accType == "DDA") {
			var accntName = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenAcctName
			var accVal = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
			gblAccNo = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
			
			gblBankCD = frmMyAccountList.segTMBAccntDetails.selectedItems[0].bankCD;
			var accValLength = accVal.length;
			accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (
				accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((
				accValLength - 1), accValLength);
			setAccountView(kony.i18n.getLocalizedString("AccountNoLabel"), accVal, kony.i18n.getLocalizedString("AccountName"),
				accntName, "", "");
			frmMyAccountView.show();
		} else if (accType == "SDA" || accType == "CDA") {
			
			gblBankCD = frmMyAccountList.segTMBAccntDetails.selectedItems[0].bankCD;
			gblAccNo = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
			
			
			kony.print("GOWRI in accType="+accType);
			//alert( frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID);
			if (frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID == "206"){
			gblAccountSummaryFlagMB="false";
				//calldepositAccountInqDMS();
				clearDreamDataMaintMB();	
			}else{
			
			kony.print("GOWRI HERE IN ELSE");
			
			kony.print("GOWRI HERE IN ELSE frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID= "+frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID);
				//frmMyAccountView.show();
				//Code change for IOS9
				myAccountViewDepositeService();
				gblSavingsCareFlow="NotMyAccountFlow";
				if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID) >= 0){
				//if(frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID == "211" || frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID == "203"){
					showLoadingScreen();
					gblSelOpenActProdCode=frmMyAccountList.segTMBAccntDetails.selectedItems[0].productID;
					//gblSCFromProdId=gblSelOpenActProdCode;
					gblFinActivityLogOpenAct["prodNameEN"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenProductNameEng;
					gblFinActivityLogOpenAct["prodNameTH"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenProductNameThai;
					kony.print("GOWRI modMyAccount.js: prodNameEN="+gblFinActivityLogOpenAct["prodNameEN"]);
					kony.print("GOWRI modMyAccount.js: prodNameTH="+gblFinActivityLogOpenAct["prodNameTH"]);
					
					gblSCFromFiident= frmMyAccountList.segTMBAccntDetails.selectedItems[0].fiident;
					gblSCFromActName= frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenAcctName;
					gblSCFromActType=accType;
					gblBeneficiaryData="";
					var accid=frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
					//gblAccountDetailsNickName=frmMyAccountList.segTMBAccntDetails.selectedItems[0].lblTMBAccountDetails;
					gblOpenSavingsCareNickName=frmMyAccountList.segTMBAccntDetails.selectedItems[0].lblTMBAccountDetails;
					kony.print("GOWRI accid from MYACCOUNT="+accid);
					kony.print("GOWRI gblOpenSavingsCareNickName="+gblOpenSavingsCareNickName);
					kony.print("**callBeneficiaryInquiryService**");
					gblSavingsCareFlow="MyAccountFlow";
					gblAccountId=accid;
					//The below Gbl variables are used in Editing Nickname service
					gblSavingsCareAccId=accid;	
					gblSavingsCarepersonalizedId=frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedId;
					gblSavingsCareBankCD = frmMyAccountList.segTMBAccntDetails.selectedItems[0].bankCD;
					
					kony.print("**gblAccountId="+gblAccountId);
					kony.print("**gblSCFromProdId="+gblSCFromProdId);
					kony.print("**gblSCFromFiident="+gblSCFromFiident);
					kony.print("**gblSCFromActName="+gblSCFromActName);
					kony.print("**gblSCFromActType="+gblSCFromActType);
					
					callBeneficiaryInquiryService(accid);
				}else{
					gblSavingsCareFlow="";
				}
			}
			
		} else if (accType.toUpperCase() == "CCA") {
			
			myAccountViewCreditCardService();
		} else if (accType.toUpperCase() == "LOC") {
			
			myAccountViewLoanAccService();
		} else {
			
			//alert("Account type unknown");
			showAlert(kony.i18n.getLocalizedString("keyErrAccntType"), kony.i18n.getLocalizedString("info"));
		}
	} else {
		
		frmMyAccountView.imgDepositAccnt.src = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].imgotherBankAcntPic;
		var accntNo = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].hiddenAccountNo;
		if (accntNo.length == 10)
			accntNo = addDashAc(accntNo);
		setAccountView(kony.i18n.getLocalizedString("AccountNoLabel"), accntNo,kony.i18n.getLocalizedString("AccountName"), frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].hiddenAcctName, "", "");
		frmMyAccountView.show();
	}
}
/**
 * Name    : accNickName()
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : prepare nick name for accnts not having nick name
 * Input params: product name, account number
 * Output params: nick name
 * ServiceCalls: No
 * Called From : service call back of customerAcctInq
 */

function accNickName(prodName, accountNo) {
	var accNo = accountNo;
	accNo = accNo.substring(8, 5);
	accNo = accNo.replace("-", "");
	return prodName + " " + accNo;
}
/**
 * Name    : setAccountView()
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : Set My Account view field values based on services output
 * Input params: label name and label value
 * Output params: No
 * ServiceCalls: No
 * Called From : service call back of different account type service
 */

function setAccountView(key1, val1, key2, val2, key3, val3) {
	//if(lblAccountNo!=null && lblAccountNo!="" && lblAccountNo.indexOf(":")==-1)
	//lblAccountNo = lblAccountNo + ":";
	
	
	frmMyAccountView.lblAccountNo.isVisible = false;
	frmMyAccountView.lblAccntNoValue.isVisible = false;
	frmMyAccountView.lblAccntSuffix.isVisible = false;
	frmMyAccountView.lblAccontSuffixValue.isVisible = false;
	frmMyAccountView.lblAccountName.isVisible = true;
	frmMyAccountView.lblAccntNameValue.isVisible = true;
	if (key1 != "") {
		//
		
		frmMyAccountView.lblAccountNo.text = key1;
		frmMyAccountView.lblAccntNoValue.text = val1;
		frmMyAccountView.lblAccountNo.isVisible = true;
		frmMyAccountView.lblAccntNoValue.isVisible = true;
	}
	//if(lblAccountName !=null && lblAccountName !="" && lblAccountName.indexOf(":")==-1)
	//lblAccountName = lblAccountName + ":";
	if (val2 == "" || val2 ==null || val2 == undefined || val2 == "null") {
		//
		val2="";
	}	
		frmMyAccountView.lblAccountName.isVisible = true;
		frmMyAccountView.lblAccntNameValue.isVisible = true;
		frmMyAccountView.lblAccountName.text = key2;
		frmMyAccountView.lblAccntNameValue.text = val2;
	//if(lblAccntSuffix!=null && lblAccntSuffix !="" && lblAccntSuffix.indexOf(":")==-1)
	//lblAccntSuffix = lblAccntSuffix + ":";
	if (key3 != "") {
		//
		
		frmMyAccountView.lblAccntSuffix.isVisible = true;
		frmMyAccountView.lblAccontSuffixValue.isVisible = true;
		frmMyAccountView.lblAccntSuffix.text = key3;
		frmMyAccountView.lblAccontSuffixValue.text = val3;
	}
}

function showfrmViewAccntSavingsCare() {
	var newNickName = frmMBSavingsCareAddNickName.txtNickName.text;
	var oldNickName="";
	kony.print("GOWRI modMyAccount.js newNickName="+newNickName);
	if(gblSavingsCareFlow=="AccountDetailsFlow"){
		oldNickName = frmAccountDetailsMB.lblAccountNameHeader.text;
	}else{
		oldNickName = frmMyAccountView.lblNickName.text;
	}	
	kony.print("GOWRI modMyAccount.js oldNickName="+oldNickName);
	if (newNickName != oldNickName) {
		kony.print("GOWRI modMyAccount.js HERE 1 CHANGE IN NICKNAME... calling myAccountEditServiceNew");
		myAccountEditServiceSavingsCare();
	} else {
		kony.print("GOWRI modMyAccount.js HERE 2.3 EQUAL");
		kony.print("GOWRI modMyAccount.js HERE 3 not calling myAccountEditServiceNew");
		hbxfooter.isVisible = true;
		callBeneficiaryInquiryServiceOnEdit();
		//frmMyAccountView.show();
	}
}

/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : change nick name
 * Input params: No
 * Output params: No
 * ServiceCalls: No
 * Called From : my account edit
 */

function showfrmViewAccnt() {
	var newNickName = frmMyAccountEdit.txtEditAccntNickName.text;
	var oldNickName = frmMyAccountView.lblNickName.text;
	gblEditNickNameValue = oldNickName + " + " + newNickName;
	
	var textformat=NickNameValid(newNickName.trim());
	if(textformat== false) {
		TextBoxErrorSkin(frmMyAccountEdit.txtEditAccntNickName, txtErrorBG);
		showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
		dismissLoadingScreen()
		return false;
	}
	
	if(gblsegID == "segOtherBankAccntDetails" && getORFTFlagMB(frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD) == "N"){
		
		if (AccountNameValid(frmMyAccountEdit.tbxAccountName.text) == false) {
			TextBoxErrorSkin(frmMyAccountEdit.tbxAccountName, txtErrorBG);
	  		showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
	  		dismissLoadingScreen()
			return false;
		}
	}
	
	
	/*if(flowSpa)
	{
		var newNickNamenormLength=newNickName.length;
		var newNicktrimglength=newNickName.trim()
		newNicktrimglength=newNicktrimglength.length;
		
		if(newNickNamenormLength != newNicktrimglength)
		{
			TextBoxErrorSkin(frmMyAccountEdit.txtEditAccntNickName, txtErrorBG);
			showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}*/
	if (newNickName != oldNickName) {
		for (var i = 0; i < gblAccntData.length; i++) {
			 if (gblAccntData[i].accntStatus != "02") 
			 {
			 	if (gblAccntData[i].nickName == newNickName) {
                    //alert("Nick name already exists");
                    TextBoxErrorSkin(frmMyAccountEdit.txtEditAccntNickName, txtErrorBG);
                    showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
             }
		}
		frmMyAccountEdit.txtEditAccntNickName.text = newNickName.trim();
		myAccountEditServiceNew("edit");
	} else {
		if(gblsegID == "segOtherBankAccntDetails" && getORFTFlagMB(frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD) == "N"){
			if(frmMyAccountEdit.tbxAccountName.text == frmMyAccountView.lblAccntNameValue.text){
				hbxfooter.isVisible = true;
				frmMyAccountView.show();
			}else{
				myAccountEditServiceNew("edit");
			}
		
		}else{
		//frmMyAccountView.imgcomplete.isVisible = true;
			hbxfooter.isVisible = true;
			frmMyAccountView.show();
		}
	}
}
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : find a param in a given array
 * Input params: name, array
 * Output params: boolean
 * ServiceCalls: No
 * Called From : utility
 */

//function chkUnique(name, gblArray) {
//	// Need to check in both my account and my recipient list of nick names
//	
//	// checking in my account list of nick name
//	if (gblArray.indexOf(name) == -1)
//		return true;
//	else
//		return false;
//}
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : validating add account process
 * Input params: No
 * Output params: No
 * ServiceCalls: No
 * Called From : OnClick of Next button of My Account Add
 */

function showConfirmationAddaccount() {
	/*
	 * NON_TMB_ADD ;  Non TMB accnt in cache - global var
	 * TOTAL_AC_ADD;  Total accnt in cache - global var
	 * TOTAL_NON_TMB_AC; Total non tmb accnt already in list
	 * gblMyAccntAddTmpData ; Cache memory - global array
	 * MAX_ACC_LEN : maximum accnt no length
	 */
	gblDelteIcn = false;
	if (checkMBUserStatus() == false) {
		return false;
	}
	var bank = frmMyAccntAddAccount.btnBanklist.text; //selected bank on list
	var accType = frmMyAccntAddAccount.btnacntlist.text; //available only incase of tmb
	var accntNo = "";
	var nickName = frmMyAccntAddAccount.tbxNickname.text;
	var suffix = "";
	var isTMB = (bank == kony.i18n.getLocalizedString("keyTMBBank") || bank == kony.i18n.getLocalizedString(
		"keyTMBBankListVal")) ? true : false;
	var serach_accnt_no = "";
	var searchFunction = 01; //01 = Loan,02 = TD(term deposite)(first 10 digit) ,03 = Credit card,SA,CA
	// If TMB Bank, get accnt no. based on accnt type
	if (isTMB) {
		if (accType == kony.i18n.getLocalizedString("keylblDeposit")) {
			accntNo = "" + frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" +
				frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 02;
			gblAccountLen=10;
		} else if (accType == kony.i18n.getLocalizedString("keylblLoan")) {
			suffix = frmMyAccntAddAccount.tbxSuffix.text;
			accntNo = "0" + frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" +
				frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text + "-" + suffix;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 01;
			gblAccountLen=10;
		} else if (accType == kony.i18n.getLocalizedString("keyCreditCardIB")) {
			accntNo = "" + frmMyAccntAddAccount.textbx1.text + "-" + frmMyAccntAddAccount.textbx2.text + "-" +
				frmMyAccntAddAccount.textbx3.text + "-" + frmMyAccntAddAccount.textbx4.text;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 03;
			gblAccountLen=16;
		} else {
			accntNo = "" + frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" +
				frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text;
			serach_accnt_no = accntNo.replace(/-/g, "");
			searchFunction = 03;
			gblAccountLen=10;
		}
	} else {
		if (MAX_ACC_LEN == 10 && MAX_ACC_LEN_LIST.indexOf(",")<0) {
			accntNo = "" + frmMyAccntAddAccount.txtBxAccnt1.text + "-" + frmMyAccntAddAccount.txtBxAccnt2.text + "-" +
				frmMyAccntAddAccount.txtBxAccnt3.text + "-" + frmMyAccntAddAccount.txtBxAccnt4.text;
				gblAccountLen=10;
		} else
			accntNo = frmMyAccntAddAccount.tbxother.text;
	}
	
	// Validate bank name
	if (kony.i18n.getLocalizedString("keylblBankName") == bank) {
		//alert("Please select Bank");
		showAlert(kony.i18n.getLocalizedString("keySelectBank"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	// If selected bank is TMB, check account type
	if (isTMB && accType == kony.i18n.getLocalizedString("keybtnPleaseSelect")) {
		//alert("Please select Account Type");
		showAlert(kony.i18n.getLocalizedString("keySelectAcctType"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	// Printing max allowed accnt no. length and ORFT status. These value are set in call back of "getBankDetails" service every time when we
	// select bank from bank list
	
	var local_accnt_no = accntNo;
	local_accnt_no = local_accnt_no.replace(/-/g, "");
	
	var len = local_accnt_no.length;
	// Allowing only numeric value for accnt no.
	if (kony.string.isNumeric(local_accnt_no) == false) {
	
	//alert("gblAccountLen: "+gblAccountLen);
		//if(flowSpa)
		//{
		//	showAlert("Invalid Account Number", kony.i18n.getLocalizedString("info"));
		//	return false;
		//}
		//else
		{	
			var testAccount = local_accnt_no;
			var iCount = 0;
			for (iIndex in testAccount) {
  				  if (!isNaN(parseInt(testAccount[iIndex]))) {
       			 iCount++;
   			 	}
			}
			var charNum=local_accnt_no.length-iCount;
			if(charNum>3){
				var  invalidAccntErrMsg =kony.i18n.getLocalizedString("keyInvalidAccNo")
				invalidAccntErrMsg = invalidAccntErrMsg.replace("?", gblAccountLen);
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
				showAlert(invalidAccntErrMsg, kony.i18n.getLocalizedString("info"));
				return false;
			}
			//alert("Account no. must be numeric");
		}
	}
	// validate accnt. no. length for NON TMB

	var x=MAX_ACC_LEN_LIST
	var y=x.split(",");
	var z=parseInt(y[0])
	if (!isTMB && (MAX_ACC_LEN_LIST.indexOf(len) < 0 || len < z)) {
		//alert("Account no. length for this bank should be :" + MAX_ACC_LEN);
		TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
		TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
		TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
		TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
		TextBoxErrorSkin(frmMyAccntAddAccount.tbxother, txtErrorBG);
		showAlert(kony.i18n.getLocalizedString("keyErrAccntNoLen"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	// Check if this record ia already in cache or not based on Accnt no and nickName
	if(gblMyAccntAddTmpData!=null){
		for (var i = 0; i < gblMyAccntAddTmpData.length; i++) {
			var tmpAccntNo2 = gblMyAccntAddTmpData[i].lblAccntNoValue.replace(/-/g, "");
			if (accType == kony.i18n.getLocalizedString("keylblLoan")) {
				tmpAccntNo2 = "0" + tmpAccntNo2 + gblMyAccntAddTmpData[i].lblAccntsuffixValue;
			}
			
			if (tmpAccntNo2 == local_accnt_no || gblMyAccntAddTmpData[i].lblaccountNickNamevalue == nickName) {
				
				//alert("Data already exist with this Account no. or Nick name in Queue");
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
				TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
				TextBoxErrorSkin(frmMyAccntAddAccount.tbxother, txtErrorBG);
				showAlert(kony.i18n.getLocalizedString("keyErrDataExistTmp"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		}
	}
	
	/*{
	-------For TMB we should also check the max length .. refer FS-----------
	"System should check the defined account no length in Bank table even for TMB bank; if the value is other
	than 10 digits (if changed in future), display a single text box for input."
	}*/
	if(flowSpa)
	{
		if(isTMB)
		{
		gblsegID = "segTMBAccntDetails"
		}
		else
		{
		gblsegID = "segOtherBankAccntDetails"
		}
	}
	
	var isFound = false;
	//
	//Adding new logic
	if (isTMB) {
	var accNumb =accntNo;
	accNumb=accNumb.replace(/-/g,"");
	
	if(accNumb.length < 10){
			
		//	alert("This TMB Account no. doesn't have sufficient digits ***")
			var  invalidAccntErrMsg =kony.i18n.getLocalizedString("keyInvalidAccNo")
			invalidAccntErrMsg = invalidAccntErrMsg.replace("?", gblAccountLen);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
			TextBoxErrorSkin(frmMyAccntAddAccount.tbxother, txtErrorBG);
			showAlert(invalidAccntErrMsg, kony.i18n.getLocalizedString("info"));
			return false;
	
		}
		if (searchFunction == 01) {
			
			if(gblAccntData!=null){
				for (var i = 0; i < gblAccntData.length; i++) {
					if (gblAccntData[i].accntNo.replace(/-/g, "") == serach_accnt_no) {
						if (gblAccntData[i].accntStatus != HIDDEN_ACCT_VAL) {
							
							//alert("This Account no. is already added")
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.tbxother, txtErrorBG);
							showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
							return false;
						}
						gblAccntName = gblAccntData[i].accntName;
						isFound = true;
						break;
					}
				}
			}
			
		} else if (searchFunction == 02) {
			
			
			var gtacc="";
			if(gblAccntData!=null){
					for (var i = 0; i < gblAccntData.length; i++) {
						if(gblAccntData[i].accntNo.length>10)
						gtacc=gblAccntData[i].accntNo.substring(4, accntNo.length + 1);
						else
						gtacc=gblAccntData[i].accntNo;
					
					
					if (gtacc == serach_accnt_no) {
						
						if (gblAccntData[i].accntStatus != HIDDEN_ACCT_VAL) {
							
							//alert("This Account no. is already added")
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.tbxother, txtErrorBG);
							showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
							return false;
						}
						gblAccntName = gblAccntData[i].accntName;
						isFound = true;
						break;
					}
				}
			}
			
		} else {
			var len = serach_accnt_no.length;
			var index = -10;
			if (len == 16)
				index = -16;
			
			if(gblAccntData!=null){
				for (var i = 0; i < gblAccntData.length; i++) {
					if (gblAccntData[i].accntNo.substr(index) == serach_accnt_no) {
						if (gblAccntData[i].accntStatus != HIDDEN_ACCT_VAL) {
							
							//alert("This Account no. is already added");
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
							TextBoxErrorSkin(frmMyAccntAddAccount.tbxother, txtErrorBG);
							showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
							return false;
						}
						gblAccntName = gblAccntData[i].accntName;
						isFound = true;
						break;
					}
				}
			}
			
		}
		if (isFound == false) {
			
			//alert("This TMB Account no. doesn't belong to this user. ***")
			var  invalidAccntErrMsg =kony.i18n.getLocalizedString("keyErrAccntNoInvalid")
			invalidAccntErrMsg = invalidAccntErrMsg.replace("?", gblAccountLen);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
			TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
			showAlert(invalidAccntErrMsg, kony.i18n.getLocalizedString("info"));
			return false;
		}
	} else {
		if(gblAccntData!=null){
			for (var i = 0; i < gblAccntData.length; i++) {
				if (gblAccntData[i].accntNo.replace(/-/g, "") == local_accnt_no && gblBankCD==gblAccntData[i]["bankCD"]) {
					
					//alert("This Account no. is already added")
					TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt1, txtErrorBG);
					TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt2, txtErrorBG);
					TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt3, txtErrorBG);
					TextBoxErrorSkin(frmMyAccntAddAccount.txtBxAccnt4, txtErrorBG);
					TextBoxErrorSkin(frmMyAccntAddAccount.tbxother, txtErrorBG);
					showAlert(kony.i18n.getLocalizedString("keyErrDataExist"), kony.i18n.getLocalizedString("info"));
					return false;
				}
			}
		}
		
	}
	//End new logic
	// Check if account is already added
	/*
    for (var i = 0; i < gblAccntData.length; i++) {
        if (gblAccntData[i].accntNo.replace(/-/g, "") == local_accnt_no) {
            isFound = true;
            if (isTMB) {
                
                if (gblAccntData[i].accntStatus != HIDDEN_ACCT_VAL) {
                    
                    alert("This Account no. is already added")
                    return false;
                }
                break;
            } else {
                
                alert("This Account no. is already added")
                return false;
            }
        }
    }
*/
	//Commenting this code as user can add others TMB accounts
	/*
    if (isFound == false && isTMB == true) {
        
        alert("This TMB Account no. doesn't belong to this user.")
        return false;
        /*
    	if(accType == kony.i18n.getLocalizedString("keylblLoan") || accType == kony.i18n.getLocalizedString("keyCreditCardIB")){
    		
        	alert("This Account no. is not valid")
        	return false;
    	}
    	else{
    		
    		srvDepositeEnquiry(local_accnt_no);
    	}
    	*/
	//}
	// Validating nickName (alphanumeric and 1 to 20 length)
	// validateing the Nickname for SPACES
	var textformat=NickNameValid(nickName.trim());
	if(textformat== false) {
		TextBoxErrorSkin(frmMyAccntAddAccount.tbxNickname, txtErrorBG);
		showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	
	if (!isTMB && IS_ORFT != 'Y') {
		var AccountName = frmMyAccntAddAccount.tbxAccountName.text;
		gblAccntName = AccountName;
		if (AccountNameValid(AccountName) == false) {
  		showAlert(kony.i18n.getLocalizedString("keyInvalidaccountName"), kony.i18n.getLocalizedString("info"));
		return false;
	}
}
	// Verify duplicate nick name and account no (if previously added in both My account list and my receipient list)
	for (var i = 0; i < gblAccntData.length; i++) {
		if (gblAccntData[i].nickName == nickName && gblAccntData[i].accntStatus != "02") {
			
			//alert("Nick name already exists");
			TextBoxErrorSkin(frmMyAccntAddAccount.tbxNickname, txtErrorBG);
			showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
	// Other BANK -  ORFTA account inquiry
	if (!isTMB && IS_ORFT == 'Y') {
		
		srvOrftAccountInq(local_accnt_no);
	} else {
		addToCacheLastStep();
	}
	// For NON-TMB bank check if this accnt no already added by user
	// Assuming NON TMB deleted accnts are also coming in customerAccountEnquiry response. Although this logic works in either cases.
	/*
	if(!isTMB){
		for (var i = 0; i < gblMyAccntAccntNo.length; i++) {
			var local = gblMyAccntAccntNo[i].accntNo;
			//
			local = local.replace(/-/g, "");
			
			// This should return false in both cases if above assumption is wrong.otherwise uncomment below lines
            if (local == accntNo) {
            	
            	//comment this if elsestatement
            	if(gblMyAccntAccntNo[i].isTMB == true){
	                
	                alert("This accnt no:" + accntNo + " is belongs to TMB")
	                return false;
            	}
            	else
            	break;
            	
            	
                alert("This Account no. is already added or belongs to TMB")
                return false;
            } 
        }
	}
	*/
	// if TMB bank -  check if that account is of that account type (correponding account service call e.g deposite,credit card or loan)
	/*
	if(isTMB){	
		for (var i = 0; i < gblMyAccntAccntNo.length; i++) {
			if (gblMyAccntAccntNo[i].accntNo == accntNo) {
			
				var laccType = gblMyAccntAccntNo[i].accType;
				
				// Getting accnt type of this accnt no.
				if (laccType == "CA" || laccType == "SA" || laccType == "TD") {
            		
            		laccType = kony.i18n.getLocalizedString("keylblDeposit");
        		}
        		else if (laccType.toUpperCase() == "VISA" || laccType.toUpperCase == "MASTER") {
            		
            		laccType = kony.i18n.getLocalizedString("keyCreditCardIB");
        		}
        		else {
            		
            		laccType = kony.i18n.getLocalizedString("keylblLoan");
        		}
        		
        		//
				
        		// Is this accnt belongs to TMB
        		if(!gblMyAccntAccntNo[i].isTMB){
        			alert("Entered Account no. doesn't belongs to TMB");
        			
        			return false;
        		}
        		
        		// Comparing accnt type of entered accnt no. with user's selection of accnt type
        		if(laccType != accType){
        			alert("Account type of entered Account no. is not same as user's selection of account type");
        			
        			return false;
        		}
        		break;
			}
		}
	}
	*/
}

function addToCacheLastStep() {
	var bank = frmMyAccntAddAccount.btnBanklist.text;
	var isTMB = (bank == kony.i18n.getLocalizedString("keyTMBBank")) ? true : false;
	// If total Non TMB is more than MAX LIMIT
	if (!isTMB) {
		if ((NON_TMB_ADD + TOTAL_NON_TMB_AC) >= MAX_OTHER_BANK) {
			//alert("You can't add more Non TMB account. Other banks list has crossed the MAX LIMIT of " + MAX_OTHER_BANK + " account.")
			showAlert(kony.i18n.getLocalizedString("keyErrMaxNonTMBAccnt"), kony.i18n.getLocalizedString("info"));
			return false;
		} else
			NON_TMB_ADD++;
	}
	// Increasing count of total added entry in cache
	TOTAL_AC_ADD++;
	//getHeader(0, kony.i18n.getLocalizedString("keylblConfirmation"),btnAdd, btnAdd,cleardataAddAccnt);
	if (TOTAL_AC_ADD > MAX_ACC_ADD) {
		//alert("You can't add more TMB account. You have reached maximun add account at a time")
		showAlert(kony.i18n.getLocalizedString("keyErrMaxAccntAdd"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	if (TOTAL_AC_ADD == MAX_ACC_ADD) {
		// Disable Add more button
		frmMyAccntConfirmationAddAccount.btnAddHeader.onClick = null;
		frmMyAccntConfirmationAddAccount.btnAddHeader.skin = "btnAddDisableRect";
		frmMyAccntConfirmationAddAccount.btnAddHeader.focusSkin = "btnAddDisableRect";
		frmMyAccntConfirmationAddAccount.btnAddHeader.setVisibility(true);
		//getHeader(0, kony.i18n.getLocalizedString("keylblConfirmation"), btnAdd, btnAdd, dummy);
	} else {
		frmMyAccntConfirmationAddAccount.btnAddHeader.onClick = addMore;
		frmMyAccntConfirmationAddAccount.btnAddHeader.setVisibility(true);
		//getHeader(0, kony.i18n.getLocalizedString("keylblConfirmation"), btnAdd, btnAdd, addMore);
	}
	// Pushing object to global array and setting to segement
	addAccount(popBankList.lblFlag.text);
	dismissLoadingScreen();
	gblVerifyOTP = 0; //DEF3540
	// Show next form
	frmMyAccntConfirmationAddAccount.show();
}

function onClickEditMyaccountMB(){
	if(gblsegID == "segOtherBankAccntDetails" && getORFTFlagMB(frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD) == "N"){
		frmMyAccountEdit.tbxAccountName.text=frmMyAccountView.lblAccntNameValue.text;
		frmMyAccountEdit.lblAccountName.setVisibility(true);
		frmMyAccountEdit.tbxAccountName.setVisibility(true);
		frmMyAccountEdit.line143506360574486.setVisibility(true);
		
	}else{
		frmMyAccountEdit.tbxAccountName.text=frmMyAccountView.lblAccntNameValue.text;
		frmMyAccountEdit.lblAccountName.setVisibility(false);
		frmMyAccountEdit.tbxAccountName.setVisibility(false);
		frmMyAccountEdit.line143506360574486.setVisibility(false)
	}
}

function dummy() {}
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : clear cache data and navigate to add account form
 * Input params: No
 * Output params: No
 * ServiceCalls: No
 * Called From : OnClick of Add more of My account complete
 */

function addMore() {
	cleardataAddAccnt();
	frmMyAccntAddAccount.show();
}
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : Initializing global variable used in My Account module
 * Input params: No
 * Output params: No
 * ServiceCalls: No
 * Called From : preShow of my account list
 */

function gblAddAccntVar() {
	// Global Var that will be used in My Account Add
	MAX_OTHER_BANK = 50;
	MAX_ACC_ADD = 0; // Max no. of accnt that can be added at a time
	TMB_BANK_CD = ""; //TMB bannk ID value
	HIDDEN_ACCT_VAL = "";
	
	
	
	
	bankListArray = []; // Storing the list of Active Banks
	acctList = [];
	
	//gblcrmId = ""; //This should be deleted, Only for testing 001100000000000000000006716547
	gblAddAccnt = false; // Differentiate add account all to TMB view and del internal add process 
	gblPersonalizeID = ""; //This should be deleted, Only for testing
	gblRawDataTMB = [];
	gblMyAccountFlowFlag = 01; // 01 = deposite, 02 = loan, 03 = Credit card (used in function frmMyAccountViewPreShow())
	//gblPHONENUMBER = ""; // Delete this
	//MB_USER_STATUS_ID = "0";
	gblOverflowErrorFlag = false;
	
	IS_ORFT = "N";
	//flowSpa = false; // Delete This
	gblAccntType = "";
	gblAccntName = "";
	swipeEnable = false;
	gblMyAccntEmailId = "";
	gblEBStatusId = "";
	gblMBFlowStatus = "";
	//gblEmailId = ""; // Delete this
	//srvGetAcctType();
}

function clearSegDataAccntList() {
	// Clearing old data
	frmMyAccountList.segTMBAccntDetails.removeAll();
	frmMyAccountList.segOtherBankAccntDetails.removeAll();
	frmMyAccountList.lblOtherBankACCnt.isVisible = false;
	frmMyAccountList.lblTMBBank.isVisible = false;
	//frmMyAccountList.lblTMBBank.text = "";
}

function crmCallBackFunction(status, result) {
	
	if (status == 400) {
		
		
		//if(result["opstatus"]==0){
		//showAccountSummaryFromMenu();
		frmMyAccountList.show();
		//frmMyRecipients.show();
			//frmBillPayment.show();
	} else if (status == 300) {
		alert("status is not 300");
	}
}



function getAccountNo(accntNo) {
	var tmpAccntNo = accntNo; // Same for LOC
	var tmpAccntType = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenAcctType;
	
	if (tmpAccntType == "CCA") {
		tmpAccntNo = tmpAccntNo.substring(tmpAccntNo.length - 16, tmpAccntNo.length); // Last 16 digit
	} else if (tmpAccntType == "CDA") {
		tmpAccntNo = tmpAccntNo.substring(4, tmpAccntNo.length + 1); // 10 digit after first 0
	} else if (tmpAccntType == "SDA" || tmpAccntType == "DDA") {
		tmpAccntNo = tmpAccntNo.substring(tmpAccntNo.length - 10, tmpAccntNo.length); // Last 10 digit
	}
	
	return tmpAccntNo;
}

function checkMBUserStatus() {

	
	
	
	
	if(!flowSpa)
	{
		if (gblUserLockStatusMB == "03") {
			//showAlert(kony.i18n.getLocalizedString("keyErrTxnPasslock"), kony.i18n.getLocalizedString("info"));
			dismissLoadingScreen();
			popTransferConfirmOTPLock.show();
			
			return false;
		}
	}
	else
	{
		if (gblIBFlowStatus == "04") {
			//showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
			dismissLoadingScreen();
			popTransferConfirmOTPLock.show();

			
			return false;
		}
	}
	return true;
}

function swipeEventMyAccount(myWidget, gestureInfo) {
	if (swipeEnable == true && gestureInfo.gestureType == 2 && gestureInfo.swipeDirection == 2) {
		handleMenuBtn();
	}
	if(flowSpa)
	{
		if (swipeEnable == true && gestureInfo.gestureType == 2 && gestureInfo.swipeDirection == 1) {
			var CurrForm = kony.application.getCurrentForm();
			CurrForm.scrollboxMain.scrollToEnd();
    }
	}
}

function getBankName(bankCD) {
	
	if (bankListArray != undefined && bankListArray != null && bankListArray.length > 0) {
		
		for (var i = 0; i < bankListArray.length; i++) {
			if (bankCD == bankListArray[i].bankCode) {
				return bankListArray[i].lblBanklist;
			}
		}
	}
}
