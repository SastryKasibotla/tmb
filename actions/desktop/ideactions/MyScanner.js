function isAndroid(){
var gblDeviceInfo = kony.os.deviceInfo();
	if(gblDeviceInfo["name"] == "android")
		return true;
return false;	
}

function onClickActivate(){
	if(isAuthUsingTouchSupported()){
		///alert("Gowri Success111");
	}else{
		///alert("Gowri Crazy222");
	}
}

var gSamsungBioUtilFFI = null;
var gMarshmalloBioUtilFFI = "";

function isAuthUsingTouchSupported() {
    //log("^^^^^^^^^^^^device info^^^^^^^^^^^^^" + JSON.stringify(kony.os.deviceInfo()));
    //log("^^^^^^^^^^^^device info^^^^^^^^^^^^^" + JSON.stringify(kony.os.deviceInfo().manufacturer));
var gblDeviceInfo = kony.os.deviceInfo();
var manu=gblDeviceInfo["manufacturer"];
kony.print("FPRINT manufacturer1="+manu);
kony.print("FPRINT manufacturer2="+kony.os.deviceInfo().manufacturer);

    if (isAndroidM()) {
     if (kony.os.deviceInfo().manufacturer === "samsung") {
            //log("SAMSUNG........");
            //Creates an object of class 'SamsungUtil'
			kony.print("FPRINT: IN SAMSUNG");
			if (gSamsungBioUtilFFI == null || gSamsungBioUtilFFI == undefined) {
			  		kony.print("FPRINT: 1 NEW SAMSUNG OBJECT CREATED IN isAuthUsingTouchSupported!");
			       gSamsungBioUtilFFI = new AndroidTouchUtil.SamsungUtil();
			} 
            var flg1=gSamsungBioUtilFFI.isFingerPrintSupported();
            kony.print("FPRINT: isAuthUsingTouchSupported flg1="+flg1);
            return flg1;
        }else{
			if(isAndroidM()) {
				kony.print("FPRINT It's MARSHMALLOW or above...");
	            gMarshmalloBioUtilFFI = new AndroidTouchUtil.MarshmallowUtil();
	            var flg2=gMarshmalloBioUtilFFI.isFingerPrintSupported();
	            kony.print("FPRINT: isAuthUsingTouchSupported flg2="+flg2);
	            return flg2;
			}else{
				return false;
			}
		}
    } else {
        try {
        kony.print("FPRINT here5");
            var status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
            //log("isAuthUsingTouchSupported --- status: " + status);
            if (status === 5000) {
            kony.print("FPRINT here6");
                isTouchBeingUsedForLogin = true;
                return true;
            } else {
            kony.print("FPRINT here7");
                return false;
            }
        } catch (bioException) {
            //log("Bio Exception - Details: " + JSON.stringify(bioException));
        }
           kony.print("FPRINT here8");
        return false;
    }
}
function onClickhasRegisteredFinger(){
	if(hasRegisteredFinger()){
		///alert("HAS REGISTERED FINGER!");
	}else{
		///alert("NO REGISTERED FINGER!");
	}
}

function hasRegisteredFinger(){
 if (isAndroidM()) {
 
 if (kony.os.deviceInfo().manufacturer === "samsung") {
            //log("SAMSUNG........");
            //Creates an object of class 'SamsungUtil'
			kony.print("FPRINT here1");
if (gSamsungBioUtilFFI == null || gSamsungBioUtilFFI == undefined) {
  		kony.print("FPRINT: 2 NEW SAMSUNG OBJECT CREATED IN hasRegisteredFinger");
       gSamsungBioUtilFFI = new AndroidTouchUtil.SamsungUtil();
	} 
            var flg1=gSamsungBioUtilFFI.hasRegisteredFinger();
            kony.print("FPRINT: hasRegisteredFinger flg1="+flg1);
            return flg1;
        }else{
			if(isAndroidM()){
				kony.print("FPRINT: It's MARSHMALLOW or above...");
	            //Creates an object of class 'MarshmallowUtil'
	            gMarshmalloBioUtilFFI = new AndroidTouchUtil.MarshmallowUtil();
	            var flg2=gMarshmalloBioUtilFFI.hasRegisteredFinger();
	            kony.print("FPRINT: hasRegisteredFinger flg2="+flg2);
	            return flg2;
	        }else{
	        	return false;
	        }    
       } 
    } else {
        try {
        kony.print("FPRINT here5");
            var status = kony.localAuthentication.getStatusForAuthenticationMode(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID);
            //log("isAuthUsingTouchSupported --- status: " + status);
            if (status === 5000) {
            kony.print("FPRINT here6");
                isTouchBeingUsedForLogin = true;
                return true;
            } else {
            kony.print("FPRINT here7");
                return false;
            }
        } catch (bioException) {
            //log("Bio Exception - Details: " + JSON.stringify(bioException));
        }
           kony.print("FPRINT here8");
        return false;
    }

}


/**
 * @module bio.js
 *
 * @author Naveen Adepu <naveen.adepu@kony.com>
 */
var gTouchSelected = false;


/**
 * @function
 *
 */
function fingerPrintFirstTimeHandler(status) {
    //log("fingerPrintFirstTimeHandler --- " + status);

    if (status === true) {
        //log("Selected YES");
        if (readDS("isFirstTimeTouchHandled") === null || readDS("isFirstTimeTouchHandled") === "") {
            //log("setting... isFirstTimeTouchHandled to yes");
            saveDS("isFirstTimeTouchHandled", "yes");
        }

        showFingerprintOptions();
    } else {
        //log("Selected NO");
        saveDS("isFirstTimeTouchHandled", "no");

        setTouchIdToDefault("REGISTER_FALSE");
    }
}

function showFingerprintOptions() {
    //log("showFingerprintOptions - isAuthUsingTouchSupported -- true");
    if (isAndroid()) {
        frmLoginVA.chkbxTouchIdAndroid.isVisible = true;
    } else {
        frmLoginVA.chkbxRegisterTouchId.isVisible = true;
    }

	var tempPwd = readPwd(); 
    if (tempPwd === null || tempPwd  === undefined || tempPwd  === "") {
        //log("pwd is null ---- readDS(\"isFirstTimeTouchHandled\")" + readDS("isFirstTimeTouchHandled"));
        if (readDS("isFirstTimeTouchHandled") == "yes") {
            //log("Fingerprint firsttime handler answered as YES... so adjusting UI as per choice.");
            setTouchIdToDefault("REGISTER_TRUE");
            saveDS("isFirstTimeTouchHandled", "done");
        } else if (readDS("isFirstTimeTouchHandled") == "done") {
            //log("Fingerprint firsttime handler answered as DONE... so adjusting UI as per choice.");
            setTouchIdToDefault("REGISTER_TRUE");
        } else {
            setTouchIdToDefault("REGISTER_FALSE");
        }
    } else {
        //log("pwd is not null");
        setTouchIdToDefault("USE_TOUCH_TRUE");

        if (isAndroid()) {
            //log("Android platform - 1");
            //log("condition 1 --- " + (maskedUser !== null));
            //log("condition 2 -- " + (readDS("isTouchIdChecked") === "true"));

            if (maskedUser !== null && readDS("isTouchIdChecked") === "true") {
            	if(isSessionTimedOut) {
            		isSessionTimedOut = false;
            		saveDS("isTouchIdChecked", "false");
            		frmLoginVA.chkbxTouchIdAndroid.selectedKeys = null;
            		
            		if (kony.os.deviceInfo().manufacturer !== "samsung") {
            			frmLoginVA.flxTouchRoot.isVisible = false;
            		}
            	} else {
	                //log("loginPostEvents - check 1");
	                authUsingTouchID();
                }
            } 
        } else {
            //log("iOS platform - 1");
            //log("condition 1 --- " + (maskedUser !== null));
            //log("condition 2 -- " + readDS("isTouchIdChecked"));

            if (maskedUser !== null && readDS("isTouchIdChecked") === "true") {
            	if(isSessionTimedOut) {
            		isSessionTimedOut = false;
            		saveDS("isTouchIdChecked", "false");
            		frmLoginVA.chkbxRegisterTouchId.selectedKeys = null;
            	} else {
	                //log("loginPostEvents - check 1");
	                authUsingTouchID();
                }
            }
        }
    }
}

function loginVA(){
	kony.print("FPRINT in loginVA");
	gblSuccess = true;
	showLoadingScreen();
	swipeLoginSuccessSkinChange();
	kony.print("FPRINT AFTER swipeLoginSuccessSkinChange");
	accsPwdValidatnLogin(gblNum);
	kony.print("FPRINT AFTER accsPwdValidatnLogin");
}

function fpAnimate() {
function MOVE_ACTION____24dee32a85934481b1f123e83b5984d0_Callback(){

}
frmMBPreLoginAccessesPin["lblLoginDesc1"].animate(
kony.ui.createAnimation({"100":{"centerX":"50%","centerY":"54%","stepConfig":{"timingFunction":kony.anim.EASE}}}),
{"delay":0,"iterationCount":"6","fillMode":kony.anim.FILL_MODE_BOTH,"duration":0.1,"direction":kony.anim.DIRECTION_ALTERNATE},
 {"animationEnd" : MOVE_ACTION____24dee32a85934481b1f123e83b5984d0_Callback});
}

function fpFailed(msg,wdth,animate){
	frmMBPreLoginAccessesPin.lblLoginDesc1.text = msg;
	frmMBPreLoginAccessesPin.lblLoginDesc1.width=wdth+"%";
	frmMBPreLoginAccessesPin.lblLoginDesc1.skin="lblRedMed150New";
	frmMBPreLoginAccessesPin.imgFprint.src="finger_error.png";
	if(animate){
		fpAnimate();
	}
		
}

function fpdefault(){
if(!tooManyAtmFlag){
	frmMBPreLoginAccessesPin.lblLoginDesc1.text = kony.i18n.getLocalizedString("MB_AndFinPrintIns1");//"Place your Finger on the Device Now.";
	frmMBPreLoginAccessesPin.lblLoginDesc2.text = kony.i18n.getLocalizedString("MB_AndFinPrintIns2"); 
	frmMBPreLoginAccessesPin.lblLoginDesc1.width="70%";
	frmMBPreLoginAccessesPin.lblLoginDesc1.skin="lblBlackMed150New";
	frmMBPreLoginAccessesPin.imgFprint.src="fprint.png";
}
}
				
function stopListening(){
	if(kony.os.deviceInfo().manufacturer === "samsung" && kony.os.deviceInfo().model==="SM-N910C"){
		if (gSamsungBioUtilFFI !== null) {
		    gSamsungBioUtilFFI.stoplistening4Bio();
		    kony.print("FPRINT SCANNER STOPPED FOR NOTE4");
		}
	}else{
		if (gMarshmalloBioUtilFFI !== null) {
		    gMarshmalloBioUtilFFI.stopListening4Bio();
		    kony.print("FPRINT SCANNER STOPPED");
		}	
	}
}	

abruptStop=false;
	
function onClickEnterAccessPin(){
abruptStop=true;
stopListening();
showFPFlex(false);
}				
				
function showFPFlex(fp){
	if(fp){
		frmMBPreLoginAccessesPin.flexKeyboard.setVisibility(false);
		frmMBPreLoginAccessesPin.flexAccounts.setVisibility(false);
		frmMBPreLoginAccessesPin.flexQuickBalanceCancel.setVisibility(false);
		frmMBPreLoginAccessesPin.btnAccessPin.text =kony.i18n.getLocalizedString("enterpin");
		frmMBPreLoginAccessesPin.lblLoginDesc2.text = kony.i18n.getLocalizedString("MB_AndFinPrintIns2"); 
		frmMBPreLoginAccessesPin.flxFingerPrint.setVisibility(true);
	}else{
		frmMBPreLoginAccessesPin.flexKeyboard.setVisibility(true);
		//frmMBPreLoginAccessesPin.flexAccounts.setVisibility(true);
		//frmMBPreLoginAccessesPin.flexQuickBalanceCancel.setVisibility(true);
		frmMBPreLoginAccessesPin.flxFingerPrint.setVisibility(false);
		kony.print("FPRINT VISIBILITY FALSE DONE");
		
	}
}			
cnt=1;	
function chkCallTimer(){
	var t1=kony.store.getItem("TIMESTAMP1");
	var blockTime=30; //30 secs
	var callTimer=false;
	if(t1==null){
		t1=-1;
		kony.store.setItem("TIMESTAMP1", t1);
	}

	if(t1!=-1){
		var currTime=new Date();
		kony.print("FPRINT TIMESTAMP2="+currTime);
		var t2=currTime.getTime();
		var diff=-1;
		diff = Math.floor((t2 - t1) / 1000);
		if(diff>0 && diff<blockTime){
			callTimer=true;
			//blockedSeconds=blockTime-diff;
		}
	}
return callTimer;
}	



function authUsingTouchID() {
    if (isAndroidM()) {
    	if(kony.os.deviceInfo().manufacturer === "samsung" && kony.os.deviceInfo().model==="SM-N910C"){
    		kony.print("FPRINT authUsingTouchID - SAMSUNG........");
			kony.print("FPRINT model - SAMSUNG........"+kony.os.deviceInfo().model);
			kony.print("FPRINT vendor - SAMSUNG........"+kony.os.deviceInfo().customDeviceID);

            if (gSamsungBioUtilFFI == null || gSamsungBioUtilFFI == undefined) {
		  		kony.print("FPRINT: 3 NEW SAMSUNG OBJECT CREATED IN authUsingTouchID");
		       gSamsungBioUtilFFI = new AndroidTouchUtil.SamsungUtil();
			} 

            //log("SAMSUNG ---- 1111111");
            ////log("gSamsungBioUtilFFI --- " + JSON.stringify(gSamsungBioUtilFFI));
            gSamsungBioUtilFFI.startlistening4Bio();
            var callback = function(result) {
			  kony.print("FPRINT RESULT=="+result);
  				if (result === "STATUS_AUTHENTIFICATION_SUCCESS") {
                    loginVA();
					MarshmallowPopup.resultlbl.text="SUCCESS!" ;
                } else if (result === "STATUS_USER_CANCELLED" || result === "STATUS_QUALITY_FAILED" || result === "STATUS_TIMEOUT" || result === "STATUS_AUTHENTIFICATION_FAILED") {
                	//log("status user cancelled... runningg......");
					txt=kony.i18n.getLocalizedString("noMatch");
	               // MarshmallowPopup.resultlbl.text="Try again";
					 fpFailed(txt,width,true);
	                 gSamsungBioUtilFFI.startlistening4Bio();
					//authUsingTouchID();
                } else {
                	//MarshmallowPopup.show();
                	//do NOTHING, let it be handled by system.
                }
			}; 

            kony.print("FPRINT authUsingTouchID - callback="+callback);
			kony.print("FPRINT authUsingTouchID - AFTER callback");
            //log("SAMSUNG ---- 3333333");
          //  gSamsungBioUtilFFI.authenticateUsingFingerprint(callback);
			gSamsungBioUtilFFI.customizedDialogWithTransparency(callback);
			//gSamsungBioUtilFFI.startlistening4Bio(callback);
    	
    	}else{
            //log("WELCOME to MARSHMALLOW code....")
			kony.print("FPRINT WELCOME to MARSHMALLOW code....");
            gMarshmalloBioUtilFFI = new AndroidTouchUtil.MarshmallowUtil();
            var txt="";
            var width="";
            var skin="";
            abruptStop=false;
			//cnt=1;
            var mCallback = function(status) {
				kony.print("FPRINT HERE1....status="+status);
                if (kony.string.startsWith(status, "HELP: ", true)) {
                	//log("Help in...");
					kony.print("FPRINT HERE2....");
                    var helpStatus = (status + "").substring("HELP: ".length - 1);
                    var array = helpStatus.split(",");
                    kony.print("FPRINT HERE1....array[0]="+array[0]);
                    //log(">>>>>>>>>>" + array[0]);
                    handleMarshmallowCodes(array[0]);
                    //log(">>>>>>>>>> " + array[1]);
                } else if (kony.string.startsWith(status, "Authentication Acquired: ", true)) {
                	//log("auth acquired... in...");
					kony.print("FPRINT HERE3....");
                    var fingerprintAcquiredStatus = (status + "").substring("Authentication Acquired: ".length - 1);
                     kony.print("FPRINT HERE1....fingerprintAcquiredStatus=aa"+fingerprintAcquiredStatus+"bb");
                    // alert("fingerprintAcquiredStatus="+fingerprintAcquiredStatus);
                    handleMarshmallowCodes(fingerprintAcquiredStatus);
                    
                } else if (status === "STATUS_SENSOR_ERROR" && !abruptStop) { //Authentication Error
					//log("status sensor error... in...");
					kony.print("FPRINT HERE4....cnt="+cnt);
					kony.print("FPRINT Sensor is stopped working due to TOO many attempts. Please try after some time.");
					
                  	//log("test ......");
					var blockedSeconds = 30;
					var callback = function() {
						blockedSeconds--;
						//log("BEEP..." + blockedSeconds);
						//var txt1="Try again in"+" " ;
						var txt1=kony.i18n.getLocalizedString("tryAgain")+" " ;
						//var txt2=" "+"seconds";
						var txt2=" "+kony.i18n.getLocalizedString("seconds");
						kony.print("FPRINT txt1="+txt1+"txt2="+txt2);
						txt=txt1 + blockedSeconds + txt2; //54
						kony.print("FPRINT txt="+txt);
						width="80";
						fpFailed(txt,width,false);
						if(blockedSeconds <= 0) {
							//log("WAKEUP...");
							kony.timer.cancel("touch");
							tooManyAtmFlag=false;
							kony.store.setItem("TIMESTAMP1", -1);
							fpdefault(); //45
							kony.print("FPRINT: 1 DONE BLOCK SECONDS");
							cnt=1;
							authUsingTouchID();
						}
						 
					};
					
					var t1=kony.store.getItem("TIMESTAMP1");
					var blockTime=30; //30 secs
					var callTimer=false;
					if(t1==null){
						t1=-1;
						kony.store.setItem("TIMESTAMP1", t1);
					}

					if(t1!=-1){
						var currTime=new Date();
						kony.print("FPRINT TIMESTAMP2="+currTime);
						var t2=currTime.getTime();
						var diff=-1;
						diff = Math.floor((t2 - t1) / 1000);
						if(diff>0 && diff<blockTime){
							callTimer=true;
							blockedSeconds=blockTime-diff;
						}
					}


					kony.print("FPRINT status="+status+"cnt= "+cnt+" callTimer="+callTimer+" blockedSeconds="+blockedSeconds+" tooManyAtmFlag="+tooManyAtmFlag);					
					if(cnt>=5 || callTimer){
						if(cnt>=5){
						  	var stmp1=new Date();
						  	kony.print("FPRINT TIMESTAMP1="+stmp1);
						  	kony.store.setItem("TIMESTAMP1", stmp1.getTime());
						}
						tooManyAtmFlag=true;
						//kony.store.setItem("tooManyAtmFlag", tooManyAtmFlag);
						var timer = kony.timer.schedule("touch", callback, 1, true);
					  }else if(tooManyAtmFlag){
					  	kony.print("GOWRIFPRINT IN CALLING AGAIN");
					  	authUsingTouchID();
					  }
					kony.print("FPRINT: 2 DONE BLOCK SECONDS");
					//log("test ..... end.");
                  	
                } else if (status === "STATUS_TIMEOUT") { //Authentication Error
					cnt++;
					//log("status timeout in....");
					kony.print("FPRINT HERE5....");
					//alert("FPRINT HERE5....cnt="+cnt);
					kony.print("FPRINT Timed out. Please retry.");
					//txt="Timed out. Please retry.";
					//txt="No match!"; 
					txt=kony.i18n.getLocalizedString("noMatch");
					kony.print("FPRINT txt="+txt);
					width="70";
					fpFailed(txt,width,true);
                    //saveDS("isTouchIdChecked", "false");
                } else if (status === "STATUS_AUTHENTIFICATION_SUCCESS" && frmMBPreLoginAccessesPin.flxFingerPrint.isVisible) { //Authentication Error
					//log("status success in....");
					kony.print("FPRINT HERE6....");
					cnt=1;
					kony.print("FPRINT SUCCESS!");
					txt="SUCCESS!";
					fpdefault();
                    isTouchBeingUsedForLogin = true;
                    loginVA();
                    //log("Stopping bio hardware....333333333");
                    gMarshmalloBioUtilFFI.stopListening4Bio();
                } else if (status === "STATUS_AUTHENTIFICATION_FAILED") { //Authentication Error
					//log("auth failed.... in....");
					kony.print("FPRINT HERE7....cnt="+cnt);
					///alert("FPRINT HERE7....cnt="+cnt);
					cnt++;
					kony.print("FPRINT No Match!!! Authentication Failed.");
					//txt="Authentication Failed! Please try again."; //50
				//	txt="No match!"; //kony.i18n.getLocalizedString("noMatch");
					txt=kony.i18n.getLocalizedString("noMatch");
					kony.print("FPRINT txt="+txt);
					width="70";
					fpFailed(txt,width,true);
                }
            };
			///alert("FPRINT1 cnt="+cnt);
            //Invokes method 'startListening4Bio' on the object
			kony.print("FPRINT: 3 DONE BLOCK SECONDS");
			kony.print("FPRINT HERE8....");
            gMarshmalloBioUtilFFI.startListening4Bio(mCallback);
       	}
    } else {
        var config = {
            "promptMessage": geti18nValueVA("i18n.ios.bio.promptmessage")//"Please authenticate using your touch id"
        };
        kony.localAuthentication.authenticate(constants.LOCAL_AUTHENTICATION_MODE_TOUCH_ID, statusCB, config);
    }
}




/**
 * @function
 *
 */
function setTouchIdToDefault(flag) {
    //log("setTouchIdToDefault --- start, flag: " + flag);
    if (isAndroid()) {
        //log("1......");
        frmLoginVA.chkbxTouchIdAndroid.isVisible = true;
    } else {
        frmLoginVA.chkbxRegisterTouchId.isVisible = true;
    }
	/*
	 * NOTE: The checkbox value which is set programmatically is not coming as checked in Android and hence writing the logic to using a temp flag. 
 	 */
    switch (flag) {
        case "REGISTER_FALSE":
            //log("REGISTER_FALSE.....");
            if (isAndroid()) {
                frmLoginVA.chkbxTouchIdAndroid.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.registerprompt")]
                ];
                frmLoginVA.chkbxTouchIdAndroid.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to register touch id", 
			        "a11yHidden" : false 
                };
                frmLoginVA.chkbxTouchIdAndroid.selectedKeys = null;
                //log("isTouchIdChecked is set to FALSE");
                saveDS("isTouchIdChecked", "false");
            } else {
                frmLoginVA.chkbxRegisterTouchId.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.registerprompt") /*geti18nValueVA("i18n.common.bio.registerprompt")*/]
                ];
                frmLoginVA.chkbxRegisterTouchId.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to register touch id", 
			        "a11yHidden" : false 
                };
                frmLoginVA.chkbxRegisterTouchId.selectedKeys = null;
                //log("isTouchIdChecked is set to FALSE");
                saveDS("isTouchIdChecked", "false");
            }
            break;

        case "REGISTER_TRUE":
            //log("REGISTER_TRUE.....");
            if (isAndroid()) {
                frmLoginVA.chkbxTouchIdAndroid.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.registerprompt")]
                ];
                frmLoginVA.chkbxTouchIdAndroid.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to register touch id", 
			        "a11yHidden" : false 
                };
                frmLoginVA.chkbxTouchIdAndroid.selectedKeys = ["touch"];
                //log("isTouchIdChecked is set to TRUE");
                saveDS("isTouchIdChecked", "true");

                if (frmLoginVA.chkbxRememVA.selectedKeys === null) {
                    frmLoginVA.chkbxRememVA.selectedKeys = ["cbg1"];
                }
            } else {
                frmLoginVA.chkbxRegisterTouchId.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.registerprompt")]
                ];
                frmLoginVA.chkbxRegisterTouchId.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to register touch id", 
			        "a11yHidden" : false 
                };
                frmLoginVA.chkbxRegisterTouchId.selectedKeys = ["touch"];
                //log("isTouchIdChecked is set to TRUE");
                saveDS("isTouchIdChecked", "true");

                if (frmLoginVA.chkbxRememiPhone.selectedKeys === null) {
                    frmLoginVA.chkbxRememiPhone.selectedKeys = ["cbg1"];
                }
            }
            break;

        case "USE_TOUCH_TRUE":
            //log("USE_TOUCH_TRUE.....");
            if (isAndroid()) {
                frmLoginVA.chkbxTouchIdAndroid.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.useprompt")]
                ];
                frmLoginVA.chkbxTouchIdAndroid.selectedKeys = ["touch"];
                frmLoginVA.chkbxTouchIdAndroid.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to use touch id for login", 
			        "a11yHidden" : false 
                };
                
                //log("isTouchIdChecked is set to TRUE");
                saveDS("isTouchIdChecked", "true");
                if (frmLoginVA.chkbxRememVA.selectedKeys === null) {
                    frmLoginVA.chkbxRememVA.selectedKeys = ["cbg1"];
                }
            } else {
                frmLoginVA.chkbxRegisterTouchId.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.useprompt")]
                ];
                frmLoginVA.chkbxRegisterTouchId.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to use touch id for login", 
			        "a11yHidden" : false 
                };
                frmLoginVA.chkbxRegisterTouchId.selectedKeys = ["touch"];
                //log("isTouchIdChecked is set to TRUE");
                saveDS("isTouchIdChecked", "true");

                if (frmLoginVA.chkbxRememiPhone.selectedKeys === null) {
                    frmLoginVA.chkbxRememiPhone.selectedKeys = ["cbg1"];
                }
            }
            break;

        case "USE_TOUCH_FALSE":
            //log("USE_TOUCH_FALSE.....");
            if (isAndroid()) {
                frmLoginVA.chkbxTouchIdAndroid.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.useprompt")]
                ];
                frmLoginVA.chkbxTouchIdAndroid.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to use touch id for login", 
			        "a11yHidden" : false 
                };
                frmLoginVA.chkbxTouchIdAndroid.selectedKeys = null;
                saveDS("isTouchIdChecked", "false");
                //log("isTouchIdChecked is set to FALSE");
            } else {
                frmLoginVA.chkbxRegisterTouchId.masterData = [
                    ["touch", geti18nValueVA("i18n.common.bio.useprompt")]
                ];
                frmLoginVA.chkbxRegisterTouchId.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to use touch id for login", 
			        "a11yHidden" : false 
                };
                frmLoginVA.chkbxRegisterTouchId.selectedKeys = null;
                //log("isTouchIdChecked is set to FALSE");
                saveDS("isTouchIdChecked", "false");
            }
            break;

        default:
            /*do nothing.*/ break;
    }
}

/**
 * @function
 *
 */
 /*
function onSelectionTouchId() {
    if (isAndroid()) {
    	//This is a manual click and in this case the touch id value is coming, but not when set this programmatically.
        if (frmLoginVA.chkbxTouchIdAndroid.selectedKeys !== null) {
            saveDS("isTouchIdChecked", "true");
            if (frmLoginVA.chkbxRememVA.selectedKeys === null) {
                frmLoginVA.chkbxRememVA.selectedKeys = ["cbg1"];
                gRememberMe = true;
            }
        } else {
            //log("1 ************* isTouchIdChecked = false");
            saveDS("isTouchIdChecked", "false");
        }

        //log("touch id - on selection called....");
        var tempPwd = readPwd();
        if (maskedUser !== null && gRememberMe == true && (readDS("isTouchIdChecked") === "true") && tempPwd != null && tempPwd != "") {
            //log("frmLoginVA.chkbxRegisterTouchId.onSelection - check 1");
            authUsingTouchID();
        }
    } else {
        //log("touch id - on selection called....");
        if (frmLoginVA.chkbxRegisterTouchId.selectedKeys !== null) {
            saveDS("isTouchIdChecked", "true");
            if (frmLoginVA.chkbxRememiPhone.selectedKeys === null) {
                //alert(1);
                frmLoginVA.chkbxRememiPhone.selectedKeys = ["cbg1"];
                gRememberMe = true;
            }
        } else {
            //log("1 ************* isTouchIdChecked = false");
            saveDS("isTouchIdChecked", "false");
        }

		var tempPwd = readPwd();
        if (maskedUser !== null && gRememberMe == true && (readDS("isTouchIdChecked") === "true") && tempPwd != null && tempPwd != "") {
            //log("frmLoginVA.chkbxRegisterTouchId.onSelection - check 1");
            authUsingTouchID();
        }
    }
}


function marshmallowFingerprintUIReset() {
	//log("Resetting FP UI.......");
    frmLoginVA.flxTouchRoot.isVisible = true;
    frmLoginVA.lblNoMatch.text = geti18nValueVA("i18n.common.bio.popup.nomatch"); //"No Match!!!";
    frmLoginVA.lblNoMatch.isVisible = false;
    frmLoginVA.lblDescText.text = geti18nValueVA("i18n.common.bio.popup.description") ; //"Use your fingerprint to verify your identity.";
    frmLoginVA.lblDescText.isVisible = true;
    frmLoginVA.imgFingerPrint.src = "ic_fp_40px.png";
    frmLoginVA.btnCancel.onClick = marshmallowPopupCancel;
}

function marshmallowPopupCancel() {
	try {
		//frmLoginVA.chkbxTouchIdAndroid.selectedKeys = null; 
	   // saveDS("isTouchIdChecked", "false");
	    
	    //TODO: stop the hardware thread here.  
	    frmLoginVA.flxTouchRoot.isVisible = false;
	    
	    if (gMarshmalloBioUtilFFI === null) {
	        //log("creating new class instance...111111");
	        gMarshmalloBioUtilFFI = new AndroidTouchUtil.MarshmallowUtil();
	    }
	    
	    gMarshmalloBioUtilFFI.stopListening4Bio();
    } catch(exception) {
    	//log("Error occurred while cancelling the popup. details: " + exception);
	    saveDS("isTouchIdChecked", "false");
    }
}
*/
function handleMarshmallowCodes(statusCode) {
	//log( "handleMarshmallowCodes - start - statusCode: '" + statusCode + "'" );
    if(statusCode == 0 || statusCode === "0" ) {
    	//log("case 0 is running....");
    	//marshmallowFingerprintUnBlock();
        //frmLoginVA.lblDescText.text = frmLoginVA.lblDescText.text + geti18nValueVA("i18n.marshmallow.bio.help.matching"); //"\nFingerprint matching!!!";
		//cnt=1;
		kony.print("FPRINT P0 UNBLOCK cnt="+cnt);
		///alert("FPRINT P0 UNBLOCK cnt="+cnt);
	//	MarshmallowPopup.Mlabel.text("Fingerprint matching!!!");
    } else if(statusCode == 1 || statusCode === "1" ) {
    	//log("case 1 running....");
		cnt++;
		kony.print("FPRINT P1 BLOCK cnt="+cnt);
		///alert("FPRINT P1 BLOCK cnt="+cnt);
		
        //marshmallowFingerprintBlock();
       // frmLoginVA.lblNoMatch.text = geti18nValueVA("i18n.marshmallow.bio.help.partial"); //"No Match!!! Partial Fingerprint is given.";
		//MarshmallowPopup.Mlabel.text("No Match!!! Partial Fingerprint is given.");
	} else if(statusCode == 2 || statusCode === "2" ) {
	cnt++;
    	//log("case 2 running....");
kony.print("FPRINT P2 BLOCK cnt="+cnt);
///alert("FPRINT P2 BLOCK cnt="+cnt);
       // marshmallowFingerprintBlock();
       // frmLoginVA.lblNoMatch.text = geti18nValueVA("i18n.marshmallow.bio.help.insufficient"); //"No Match!!! Insufficient Fingerprint is given.";
	} else if(statusCode == 3 || statusCode === "3" ) {
		cnt++;
    	//log("case 3 running....");
kony.print("FPRINT P3 BLOCK cnt="+cnt);
///alert("FPRINT P3 BLOCK cnt="+cnt);
       // marshmallowFingerprintBlock();
        //frmLoginVA.lblNoMatch.text = geti18nValueVA("i18n.marshmallow.bio.help.dirty"); //"No Match!!! Dirty Fingerprint is given.";
	} else if(statusCode == 4 || statusCode === "4" ) {
		cnt++;
    	//log("case 4 running....");
kony.print("FPRINT P4 BLOCK cnt="+cnt);
///alert("FPRINT P4 BLOCK cnt="+cnt);
       // marshmallowFingerprintBlock();
        //frmLoginVA.lblNoMatch.text = geti18nValueVA("i18n.marshmallow.bio.help.tooslow"); //"No Match!!! You swiped TOO SLOW.";
	} else if(statusCode == 5 || statusCode === "5" ) {
		cnt++;
    	//log("case 5 running....");
kony.print("FPRINT P5 BLOCK cnt="+cnt);
///alert("FPRINT P5 BLOCK cnt="+cnt);
       // marshmallowFingerprintBlock();
       // frmLoginVA.lblNoMatch.text = geti18nValueVA("i18n.marshmallow.bio.help.toofast"); //"No Match!!! You swiped TOO FAST.";
    }
    kony.print("FPRINT P STOP");
    //log( "handleMarshmallowCodes - stop" );
}
/*
function marshmallowFingerprintBlock() {
    frmLoginVA.imgFingerPrint.src = "fpblock.png";
    frmLoginVA.lblDescText.isVisible = false;
    frmLoginVA.lblNoMatch.isVisible = true;
}

function marshmallowFingerprintUnBlock() {
    frmLoginVA.imgFingerPrint.src = "ic_fp_40px.png";
    frmLoginVA.lblDescText.isVisible = true;
    frmLoginVA.lblNoMatch.isVisible = false;
}
*/

/**
 * @function
 * iOS function
 */
function statusCB(status, message) {
    //log("Status: " + status + "; message: " + message);
    if (status === 5000) {
        //kony.ui.Alert({message: "AUTHENTICATION SUCCESSFULL", alertType: constants.ALERT_TYPE_INFO, yesLabel:"Close"}, {});
        loginVA();
    } else {
        var messg = status + message;
        //log(messg);
        frmLoginVA.chkbxRegisterTouchId.masterData = [
            ["touch", geti18nValueVA("i18n.common.bio.useprompt")]
        ];
        frmLoginVA.chkbxRegisterTouchId.accessibilityConfig = {
                	"a11yLabel": "Switch", 
			        "allyValue": "",
			        "a11yHint": "Allows you to use touch id for login", 
			        "a11yHidden" : false 
                };
        frmLoginVA.chkbxRegisterTouchId.selectedKeys = null;
    }
}

function updateTouchStatusOnServer(isEnabled) {
	var token = getDeviceToken();
	if(token == null)  {
		token = gDeviceToken;
	}
	//log("TTOOKKEENN ------ " + token);
	commonServiceInvokerHandler("Authentication", 'isTouchEnabled', null, {
                'userDeviceId': token, 'isEnabled' : isEnabled
            }, function(response) { //log("Update Status: " + response); 
				});
}

/**
 * This function is required to add to the transparant layer to NOT allow the background widgets' actions on touch popup
 * @returns NOTHING
 */
function doNothing() {
	return;
}

function savePwd(text) {
    try {
    	if(text != null && text != undefined && text != "") {
	        removePwd();
	        
	        var symmetricKey = getKey1VA();
	        var encText = encryptAESVA(text, symmetricKey);
	        var pwd = {
	            crypt: encText
	        };
			
			//log(">>>>>>>>>>>>>>encText" + JSON.stringify(encText));    
	        saveDS("pwd", [pwd]);
        }
    } catch (e) {
        log(">>>>>Exception" + e);
    }
}

/********************************************************************************************
  	Purpose : This module helps in retrieving the userID from the device store if found
*********************************************************************************************/
function readPwd() {
    var returnValue = null;
    var readDeviceTable = readDS("pwd");
    //log("readPwd -- start, readDeviceTable: " + readDeviceTable + "; JSON: " + JSON.stringify(readDeviceTable));
    
    if ((readDeviceTable !== null)) {
        if ((gDeviceNameVA != "thinclient")) {
            var cipherUID = readDeviceTable[0]["crypt"];
            //log("readPwd --- cipherUID: " + JSON.stringify(cipherUID));
            var symmetricKey = getKey1VA();
            //log("readPwd --- symmetricKey: " + JSON.stringify(symmetricKey));
            var decUID = decryptAESVA(cipherUID, symmetricKey);
            //log("readPwd --- decUID: " + JSON.stringify(decUID));
            returnValue = decUID;
        } else {
            returnValue = readDeviceTable["crypt"];
        }
    }

	//log(">>>>>>>>>>>>>>decText" + JSON.stringify(returnValue));
    return returnValue;
}

function removePwd() {
	removeDS("pwd");
}

 
