








/**************************************************************************************
		Module	: invokeRequestOtpChngPWD
		Author  : Kony
		Date    : July 21, 2013
		Purpose : OTP while changePassword
*****************************************************************************************/

function invokeRequestOtpChngPWD() {

    frmIBCMConfirmationPwd.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBCMConfirmationPwd.lblPlsReEnter.text = " "; 
    frmIBCMConfirmationPwd.hbxOTPincurrect.isVisible = false;
    frmIBCMConfirmationPwd.hbox476047582127699.isVisible = true;
    frmIBCMConfirmationPwd.hbox476047582115275.isVisible = true;

	if (gblOTPFlag) {
	var inputParams = {}
	inputParams["Channel"] = "ChangeUserPassword";
	inputParams["locale"] = kony.i18n.getCurrentLocale();
	inputParams["retryCounterRequestOTP"] = gblIBRetryCountRequestOTP["chngIBPwd"];
 	invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackReqOtpChngPWD);
 	}
}





/**************************************************************************************
		Module	: callBackReqOtpChngPWD
		Author  : Kony
		Date    : July 21, 2013
		Purpose : OTP Response while changePassword
*****************************************************************************************/

function callBackReqOtpChngPWD(status, resultable) {
	if (status == 400) {
		if (resultable["errCode"] == "GenOTPRtyErr00002") {
				dismissLoadingScreenPopup();
				gblOTPFlag = false;	
				frmIBCMConfirmationPwd.btnOTPreq.skin = btnIBREQotp;
			    frmIBCMConfirmationPwd.btnOTPreq.setEnabled(false);
			    kony.timer.cancel("PwdOTPTimer");
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		if (resultable["errCode"] == "GenOTPRtyErr00001") {
				dismissLoadingScreenPopup();
				//gblOTPFlag = false;	
//				frmIBCMConfirmationPwd.btnOTPreq.skin = btnIBREQotp;
//			    frmIBCMConfirmationPwd.btnOTPreq.setEnabled(false);
//			    kony.timer.cancel("PwdOTPTimer");
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}	
	    if (resultable["errCode"] == "JavaErr00001") {
				//alert("Test2");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}	
		if (resultable["opstatus"] == 0) {
			
			var reqOtpTimer = kony.os.toNumber(resultable["requestOTPEnableTime"]);
			var uuid = kony.os.toNumber(resultable["uuid"]);
			var pac = kony.os.toNumber(resultable["pac"]);
			var expirydate = kony.os.toNumber(resultable["expirydate"]);
			gblOTPLENGTH = kony.os.toNumber(resultable["otpLength"]);
			gblIBRetryCountRequestOTP["chngIBPwd"] = resultable["retryCounterRequestOTP"];
			//alert(uuid +" "+pac +" " +gblOTPLENGTH);
			//kony.timer.schedule("PwdOTPTimer", PwdChngOTPTimerCallBack, reqOtpTimer, false);
			frmIBCMConfirmationPwd.btnOTPreq.skin = btnIBREQotp;
			frmIBCMConfirmationPwd.btnOTPreq.setEnabled(false);
			frmIBCMConfirmationPwd.txtOTP.text = "";
		//	dismissLoadingScreenPopup();
			frmIBCMConfirmationPwd.label474288733630.text = frmIBCMMyProfile.lblCusName.text;//code modified
			frmIBCMConfirmationPwd.lblEmailVal.text = frmIBCMMyProfile.lblEmailVal.text;
			//alert(frmIBCMConfirmationPwd.lblEmailVal.text + frmIBCMMyProfile.lblEmailVal.text);
			frmIBCMConfirmation.hbox476047582127699.setVisibility(true);
			frmIBCMConfirmation.label476047582115288.setVisibility(true);
			frmIBCMConfirmation.hbox476047582115275.setVisibility(true);
			frmIBCMConfirmationPwd.lblFBIDVal.text = frmIBCMMyProfile.lblfbidstudio13.text;//code modified//Modified by Studio Viz
 			frmIBCMConfirmationPwd.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");//added
			frmIBCMConfirmationPwd.lblno.text = frmIBCMMyProfile.lblno.text;
			frmIBCMConfirmationPwd.lblPhoneNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			frmIBCMConfirmationPwd.lblBankRef.text=kony.i18n.getLocalizedString("keyIBbankrefno");//Added 

			var refVal="";
			for(var d=0;d<resultable["Collection1"].length;d++){
				if(resultable["Collection1"][d]["keyName"] == "pac"){
						refVal=resultable["Collection1"][d]["ValueString"];
						break;
					}
				}
		    kony.timer.schedule("PwdOTPTimer", PwdChngOTPTimerCallBack, reqOtpTimer, false);
			frmIBCMConfirmationPwd.lblBankRefVal.text = refVal;
			frmIBCMMyProfile.imgCompletion.setVisibility(true);
			frmIBCMConfirmationPwd.btnOTPreq.skin=btnIBREQotp;
			dismissLoadingScreenPopup();
			frmIBCMConfirmationPwd.txtOTP.setFocus(true);
			//frmIBCMConfirmationPwd.btnReqOTP.setEnabled(false);
			gblOTPFlag = false;			
			frmIBCMConfirmationPwd.show();
		} else {
			dismissLoadingScreen();
			gblIBRetryCountRequestOTP["chngIBPwd"] = resultable["retryCounterRequestOTP"];
			alert("Error " + resultable["errMsg"]);
		}
	} else {
		if (status == 300) {
			gblIBRetryCountRequestOTP["chngIBPwd"] = resultable["retryCounterRequestOTP"];
			dismissLoadingScreenPopup();
			alert("Error");
		}
	}
}

function PwdChngOTPTimerCallBack() {
	frmIBCMConfirmationPwd.btnOTPreq.skin = btnIBREQotpFocus;
	frmIBCMConfirmationPwd.btnOTPreq.setEnabled(true);
	frmIBCMConfirmationPwd.btnOTPreq.onClick = invokeRequestOtpChngPWD;
	gblOTPFlag = true;
	try {
		kony.timer.cancel("PwdOTPTimer")
	} catch (e) {
		
	}
}


function lockIBPwdChngOTpLockCrmUpdate(){
				var inputParam = {}
				inputParam["ibUserId"] = gblUserName;
				inputParam["ibUserStatusId"] = "05";
				inputParam["actionType"] = "32";
				frmIBCMConfirmationPwd.txtOTP.text = "";
				showLoadingScreen();
				invokeServiceSecureAsync("crmProfileMod", inputParam, callBackinvokeIBCRMProfileUpdateLogout);
}


/**************************************************************************************
		Module	: callBackChangeIBPwdNotification
		Author  : Kony
		Date    : July 21, 2013
		Purpose :  NotificationAdd service response
*****************************************************************************************/

function callBackChangeIBPwdNotification(status, resultable) {
	if (status == 400) {
		if (resultable["opstatus"] == 0) {
			
			
		} else {
 		}
	} else {
		if (status == 300) {
			//dismissLoadingScreenPopup();
 		}
	}
}

function onMyProfilePwdNextIB() {
   var currForm = kony.application.getCurrentForm();
	

	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		checkMyProfilePwdTokenFlag();
	} 
	//var oldPwdIBChange = frmIBCMChgPwd.tbxTranscCrntPwd.text;
   // var newPwdIBChange = frmIBCMChgPwd.txtPassword.text;
	//invokeSaveparamInSessionForPwdUserChange(oldPwdIBChange,newPwdIBChange,"2");
	
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
	
			frmIBCMConfirmationPwd.hbxTokenEntry.setVisibility(true);
			frmIBCMConfirmationPwd.tbxToken.setFocus(true);
			frmIBCMConfirmationPwd.hbxOTPEntry.setVisibility(false);
			frmIBCMConfirmationPwd.label476047582115288.text=kony.i18n.getLocalizedString("keyPleaseEnterToken");
			frmIBCMConfirmationPwd.label476047582115277.setVisibility(false);
			frmIBCMConfirmationPwd.lblPhoneNo.setVisibility(false);
			frmIBCMConfirmationPwd.lblBankRef.setVisibility(false);
			frmIBCMConfirmationPwd.show();
		
	} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
			frmIBCMConfirmationPwd.hbxTokenEntry.setVisibility(false);
			frmIBCMConfirmationPwd.hbxOTPEntry.setVisibility(true);
			frmIBCMConfirmationPwd.txtOTP.setFocus(true);
			frmIBCMConfirmationPwd.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
			frmIBCMConfirmationPwd.label476047582115277.setVisibility(true);
			frmIBCMConfirmationPwd.lblPhoneNo.setVisibility(true);
			frmIBCMConfirmationPwd.lblBankRef.setVisibility(true);
			invokeRequestOtpChngPWD();
	}

}
function checkMyProfilePwdTokenFlag() {
		var inputParam = [];
		inputParam["crmId"] = gblcrmId;
		showLoadingScreenPopup();
		invokeServiceSecureAsync("tokenSwitching", inputParam, ibMyProfilePwdTokenFlagCallbackfunction);
}
function ibMyProfilePwdTokenFlagCallbackfunction(status,callbackResponse){
		var currForm = kony.application.getCurrentForm().id;
		
		if(status==400){
				if(callbackResponse["opstatus"] == 0){
					if(callbackResponse["deviceFlag"].length==0){
						
						//return
					}
					if ( callbackResponse["deviceFlag"].length > 0 ){
							var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
							var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
							var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
							
							if ( tokenFlag=="Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02'){
									dismissLoadingScreenPopup();
									frmIBCMConfirmationPwd.hbxTokenEntry.setVisibility(true);
									frmIBCMConfirmationPwd.tbxToken.setFocus(true);
									frmIBCMConfirmationPwd.hbxOTPEntry.setVisibility(false);
									frmIBCMConfirmationPwd.label476047582115288.text=kony.i18n.getLocalizedString("keyPleaseEnterToken");
									gblTokenSwitchFlag = true;
									frmIBCMConfirmationPwd.label476047582115277.setVisibility(false);
									frmIBCMConfirmationPwd.lblPhoneNo.setVisibility(false);
									frmIBCMConfirmationPwd.lblBankRef.setVisibility(false);
									frmIBCMConfirmationPwd.show();
							} else {
									frmIBCMConfirmationPwd.hbxTokenEntry.setVisibility(false);
									frmIBCMConfirmationPwd.hbxOTPEntry.setVisibility(true);
									frmIBCMConfirmationPwd.txtOTP.setFocus(true);
									frmIBCMConfirmationPwd.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
								    invokeRequestOtpChngPWD();
									gblTokenSwitchFlag = false;
							}
						}
					 else
					{
					 
						frmIBCMConfirmationPwd.hbxTokenEntry.setVisibility(false);
						frmIBCMConfirmationPwd.hbxOTPEntry.setVisibility(true);
						frmIBCMConfirmationPwd.txtOTP.setFocus(true);
						frmIBCMConfirmationPwd.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
					    invokeRequestOtpChngPWD();
					 }
						
				}
				else dismissLoadingScreenPopup();
		}
		else dismissLoadingScreenPopup();
		
}

function changePasswordComposite() {
	
    var inputParams = {}
   	if(gblTokenSwitchFlag == true ) {
   		var otpToken = frmIBCMConfirmationPwd.tbxToken.text
   	  if(otpToken != null)
      	otpToken = otpToken.trim();
  	  else {
      	 frmIBCMConfirmationPwd.tbxToken.text = "";
       	alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
       	return ;
      }
  	 if(otpToken == ""){
    	frmIBCMConfirmationPwd.tbxToken.text = "";
  		alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
  			return ;
  	 }
      inputParams["password"] = frmIBCMConfirmationPwd.tbxToken.text;
      inputParams["segmentIdVal"] = "MIB";
   	}else{
   		var otpText = frmIBCMConfirmationPwd.txtOTP.text
		  if(otpText != null)
			otpText = otpText.trim();
		 else {
		    frmIBCMConfirmationPwd.txtOTP.text = "";
			alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
			return ;
	         } 
		if(otpText == ""){
		    frmIBCMConfirmationPwd.txtOTP.text = "";
			alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
	 		return ;
		}
   	    inputParams["password"] = frmIBCMConfirmationPwd.txtOTP.text;
   	}
     // this flag determine otp or token flow
    inputParams["gblTokenSwitchFlag"]=gblTokenSwitchFlag ? "true":"false";
    inputParams["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParams["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParams["userId"] = gblUserName;
    inputParams["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
    
    // clear the text entries after input has been accepted 
    frmIBCMConfirmationPwd.tbxToken.text="";
    frmIBCMConfirmationPwd.txtOTP.text = "";
   
	inputParams["oldPassword"] = frmIBCMChgPwd.tbxTranscCrntPwd.text;
    inputParams["newPassword"] = frmIBCMChgPwd.txtPassword.text;
    
    inputParams["custName"] = customerName;
   
    inputParams["Locale"] = kony.i18n.getCurrentLocale();
    var platformChannel = gblDeviceInfo.name;
  
    showLoadingScreenPopup();
    // invoking call 
    invokeServiceSecureAsync("changePasswordComposite", inputParams, callBackChangePasswordComposite);

}

function callBackChangePasswordComposite(status, resultable) {
    if (status == 400) {
        
        frmIBCMConfirmationPwd.txtOTP.text = "";
        frmIBCMConfirmationPwd.tbxToken.text = "";
        if (resultable["opstatus"] == 0) {
            frmIBCMMyProfile.show();
            frmIBCMMyProfile.imgCompletion.setVisibility(true);
            
            frmIBCMMyProfile.lblupdatedprofile.setVisibility(true);

        } else if (resultable["opstatus_verifypwd"] != 0 && resultable["opstatus"] == 8005) {
            if (resultable["errCode"] == "VrfyOTPErr00001") {
                gblRetryCountRequestOTP = resultable["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commented by swapna
                    frmIBCMConfirmationPwd.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBCMConfirmationPwd.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBCMConfirmationPwd.hbxOTPincurrect.isVisible = true;
                    frmIBCMConfirmationPwd.hbox476047582127699.isVisible = false;
                    frmIBCMConfirmationPwd.hbox476047582115275.isVisible = false;
                    frmIBCMConfirmationPwd.txtOTP.text = "";
                    frmIBCMConfirmationPwd.tbxToken.text ="";
                     if(gblTokenSwitchFlag == true)
                    frmIBCMConfirmationPwd.tbxToken.setFocus(true);
                      else
                     frmIBCMConfirmationPwd.txtOTP.setFocus(true);
                return false;
            } else if (resultable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProfilBPIB("04");
                handleOTPLockedIB(resultable);
                return false;
            } else if (resultable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreenPopup();
                alert(kony.i18n.getLocalizedString("invalidOTP"));
                return false;
            } else if (resultable["errCode"] == "VrfyOTPErr00006") {
                gblVerifyOTPCounter = resultable["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                alert("" + resultable["errMsg"]);
                return false;

            } else {
                frmIBCMConfirmationPwd.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBCMConfirmationPwd.lblPlsReEnter.text = " "; 
                frmIBCMConfirmationPwd.hbxOTPincurrect.isVisible = false;
                frmIBCMConfirmationPwd.hbox476047582127699.isVisible = true;
                frmIBCMConfirmationPwd.hbox476047582115275.isVisible = true;
                dismissLoadingScreenPopup();
                alert("" + resultable["errMsg"]);
            }
        } else if (resultable["opstatus_verifypwd"] != 0 && resultable["code"] != null && resultable["code"] == "10403") {

            frmIBCMConfirmationPwd.btnOTPreq.skin = btnIBgreyInactive;
            frmIBCMConfirmationPwd.btnOTPreq.setEnabled(false);
            frmIBCMConfirmationPwd.txtOTP.text = "";
            gblVerifyOTPCounter = "0";
            handleOTPLockedIB(resultable);

        } else if (resultable["opstatus_verifypwd"] != 0 && resultable["code"] != null && resultable["code"] == "10020") {
            //frmIBCMConfirmationPwd.lblIncorrectMsg.text = kony.i18n.getLocalizedString("wrongOTP");
            frmIBCMConfirmationPwd.txtOTP.text = "";
            gblVerifyOTPCounter = resultable["retryCounterVerifyOTP"];
            dismissLoadingScreenPopup()
            showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
        } else if(resultable["opstatus_changepwd"] != 0){
        	if (resultable["code"]!= null && resultable["code"] == "10403")	{
			popIBTransNowOTPLocked.btnClose.onClick = lockIBPwdChngOTpLockCrmUpdate;
			popIBTransNowOTPLocked.lblDetails1.text = kony.i18n.getLocalizedString("accLock");
			popIBTransNowOTPLocked.show();
			}
			else if(resultable["code"]!= null && resultable["code"] == "10200"){
			dismissLoadingScreenPopup();
			alert(resultable["errmsg"]);
			}else if (resultable["code"] != null && resultable["code"] == "10020") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("keyInCrtCurPwd"), kony.i18n.getLocalizedString("info"));
            }
			else if(resultable["code"]!= null && resultable["code"] == "56789"){
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
			}
			else{
			dismissLoadingScreenPopup();
			alert(resultable["errMsg"]);
			}
            frmIBCMConfirmationPwd.txtOTP.text = "";
        }
    } else {
        if (status == 300) {
            alert("Error");
        }

    }

}
	
function invokeSaveparamInSessionForPwdUserChange(param1,param2,param3){
		var inputParam = [];

		inputParam["param1"] = param1;
		inputParam["param2"] = param2;
		inputParam["param3"] = param3;
	  invokeServiceSecureAsync("SaveParamsForChangePwdUserId", inputParam, callBackSaveparamInSessionPwdUserChange);
}	


function callBackSaveparamInSessionPwdUserChange(status,resulttable){
	 if (status == 400) {
		 if(resulttable["opstatus"] == 0){
			 
			 onMyProfilePwdNextIB();
		 }else{
			 dismissLoadingScreenPopup();
			 alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
		 }
	 }
	
}
function checkMyProfileChangePwdTokenIB() {
	var inputParam = [];
	showLoadingScreenPopup();
	invokeServiceSecureAsync("tokenSwitching", inputParam, checkMyProfilePwdTokenIBCallbackfunction);
}

function checkMyProfilePwdTokenIBCallbackfunction(status,resulttable){
	 if (status == 400) {
		 if(resulttable["opstatus"] == 0){
			 var oldPwdIBChange = frmIBCMChgPwd.tbxTranscCrntPwd.text;
	   		  var newPwdIBChange = frmIBCMChgPwd.txtPassword.text;
			  invokeSaveparamInSessionForPwdUserChange(oldPwdIBChange,newPwdIBChange,"2");
		 }else{
			 dismissLoadingScreenPopup();
			 alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
			}
		 }
}
