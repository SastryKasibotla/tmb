	gblOpenAccountFail = false;
    function validateOTPtextOpenAccountsJavaService(otpPwd,mnthlyDreamAmt,dreamName,dreamTarget,savingCareText,dreamTargetID,channelCode,dreamDesc) {
        var inputParam = {};
        var Pwd = otpPwd;
        kony.print("gblSelOpenActProdCode in validateOTPtextOpenAccountsJavaService()---->" + gblSelOpenActProdCode);
        inputParam["password"] = otpPwd
        // TOKEN
        inputParam["TokenSwitchFlag"] = gblTokenSwitchFlag;
        inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
        inputParam["verifyToken_userStoreId"] = "DefaultStore";
        inputParam["verifyToken_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
        inputParam["verifyToken_userId"] = gblUserName;
        
        //inputParam["verifyToken_password"] = Pwd;
        inputParam["verifyToken_sessionVal"] = "";
        inputParam["verifyToken_segmentId"] = "segmentId";
        inputParam["verifyToken_segmentIdVal"] = "MIB";
        inputParam["verifyToken_channel"] = "Internet Banking";
        
        //OTP
        //inputParam["verifyPwd_TokenSwitchFlag"] = gblTokenSwitchFlag;
        inputParam["verifyPwd_retryCounterVerifyOTP"] = "0",
        inputParam["verifyPwd_userId"] = gblUserName,
        inputParam["verifyPwd_userStoreId"] = "DefaultStore",
        inputParam["verifyPwd_loginModuleId"] = "IBSMSOTP",
        //inputParam["verifyPwd_password"] = Pwd,
        inputParam["verifyPwd_segmentId"] = "MIB"
        //Mobile Pwd 
        inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
        inputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
        inputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
        //inputParam["verifyPwdMB_password"] = Pwd;

       //DepositAOXFER
       inputParam["productCode"] = gblSelOpenActProdCode;
       inputParam["isDisToActTD"] = gblisDisToActTD;
       
       inputParam["fromDepositacctVal"] = gblFinActivityLogOpenAct["fromActId"];
       inputParam["fromactFiidentvalue"] = gblFinActivityLogOpenAct["fromFiident"];
       inputParam["fromDepositActType"] = gblFinActivityLogOpenAct["fromActType"];       
       inputParam["toactFiidentvalue"] = gblFinActivityLogOpenAct["toFiident"];
       inputParam["toDepositActType"] = gblFinActivityLogOpenAct["toActType"];
       inputParam["toDepositActNum"] = gblFinActivityLogOpenAct["toActId"];       
       inputParam["transferAmount"] = gblFinActivityLogOpenAct["transferAmount"];
       inputParam["dreamAmt"] = mnthlyDreamAmt.trim();
       inputParam["dreamMonth"] = gblFinActivityLogOpenAct["dreamMonth"];
       inputParam["fromProdId"] = gblFinActivityLogOpenAct["fromProdId"];
       
      
      	
		
		if (gblPartyInqOARes["issuedIdentType"] == "CI" ){
		inputParam["acctTitle"] = gblPartyInqOARes["FullName"];
		}else{
		inputParam["acctTitle"] = gblPartyInqOARes["PrefName"];
		}
					
		inputParam["depositAoxfer_miscDate"] = gblPartyInqOARes["birthDate"];
		for (var i = 0; i < gblPartyInqOARes["Persondata"].length; i++) {
			if (gblPartyInqOARes["Persondata"][i]["AddrType"] != null && gblPartyInqOARes["Persondata"][i]["AddrType"] != "") 
			{
				if (gblPartyInqOARes["Persondata"][i]["AddrType"] == "Primary"){
				inputParam["depositAoxfer_addrType"] = gblPartyInqOARes["Persondata"][i]["AddrType"];
				inputParam["depositAoxfer_addr1"] = gblPartyInqOARes["Persondata"][i]["addr1"];
				inputParam["depositAoxfer_addr2"] = gblPartyInqOARes["Persondata"][i]["addr2"];
				inputParam["depositAoxfer_addr3"] = gblPartyInqOARes["Persondata"][i]["addr3"];
				inputParam["depositAoxfer_city"] = gblPartyInqOARes["Persondata"][i]["City"];
				if(gblPartyInqOARes["Persondata"][i]["PostalCode"] != "" && gblPartyInqOARes["Persondata"][i]["PostalCode"] != null && gblPartyInqOARes["Persondata"][i]["PostalCode"] != undefined){
				inputParam["depositAoxfer_postalCode"] = gblPartyInqOARes["Persondata"][i]["PostalCode"];
				}else{
				inputParam["depositAoxfer_postalCode"] = "";
				}
				inputParam["depositAoxfer_cntryCodeVal"] = "TH"//gblPartyInqOARes["Persondata"][i]["countryCode"];
				break;
				}
				
				if (gblPartyInqOARes["Persondata"][i]["AddrType"] == "Registered"){
				inputParam["depositAoxfer_addrType"] = gblPartyInqOARes["Persondata"][i]["AddrType"];
				inputParam["depositAoxfer_addr1"] = gblPartyInqOARes["Persondata"][i]["addr1"];
				inputParam["depositAoxfer_addr2"] = gblPartyInqOARes["Persondata"][i]["addr2"];
				inputParam["depositAoxfer_addr3"] = gblPartyInqOARes["Persondata"][i]["addr3"];
				inputParam["depositAoxfer_city"] = gblPartyInqOARes["Persondata"][i]["City"];
				if(gblPartyInqOARes["Persondata"][i]["PostalCode"] != "" && gblPartyInqOARes["Persondata"][i]["PostalCode"] != null && gblPartyInqOARes["Persondata"][i]["PostalCode"] != undefined){
				inputParam["depositAoxfer_postalCode"] = gblPartyInqOARes["Persondata"][i]["PostalCode"];
				}else{
				inputParam["depositAoxfer_postalCode"] = "";
				}
				inputParam["depositAoxfer_cntryCodeVal"] ="TH"// gblPartyInqOARes["Persondata"][i]["countryCode"];
				break;
				}
			
			}
		}
       
       
        //Notification ADD
        var platformChannel = "";
        platformChannel = gblDeviceInfo
            .name;

        if (platformChannel == "thinclient") {
            //inputParam["channel"] = GLOBAL_IB_CHANNEL;
            inputParam["notificationAdd_channel"] = "IB";
            inputParam["notificationAdd_channelId"] = "Internet Banking";
            //inputParam["notificationAdd_channelName"] = "Internet Banking";
        } else {
            //inputParam["channel"] = GLOBAL_MB_CHANNEL;
            inputParam["notificationAdd_channel"] = "MB";
            inputParam["notificationAdd_channelId"] = "Mobile Banking";
            // inputParam["notificationAdd_channelName"] = "Mobile Banking";
        }

        inputParam["notificationAdd_deliveryMethod"] = "Email";
        inputParam["notificationAdd_noSendInd"] = "0";

        inputParam["notificationAdd_notificationType"] = "Email";
        inputParam["notificationAdd_Locale"] = kony.i18n.getCurrentLocale();


        inputParam["notificationAdd_customerName"] = gblCustomerName;
        inputParam["notificationAdd_appID"] = appConfig.appId;
        inputParam["notificationAdd_nickname"] = gblFinActivityLogOpenAct["toActNickName"];
        
       	inputParam["notificationAdd_accountType"] = gblFinActivityLogOpenAct["accTypeValEN"];
		inputParam["notificationAdd_accountTypeTH"] = gblFinActivityLogOpenAct["accTypeValTH"];
		inputParam["notificationAdd_productName"] = gblFinActivityLogOpenAct["prodNameEN"];
		inputParam["notificationAdd_productNameTH"] = gblFinActivityLogOpenAct["prodNameTH"];
		inputParam["notificationAdd_tenor"] = gblFinActivityLogOpenAct["tdTerm"];
		inputParam["notificationAdd_amount"] = commaFormatted(gblFinActivityLogOpenAct["transferAmount"]);
		inputParam["notificationAdd_transrefno"] = gblTransferRefNo;	
		inputParam["notificationAdd_notificationSubject"] = "";
		inputParam["notificationAdd_notificationContent"] = "";
		inputParam["notificationAdd_custName"] = gblCustomerName;
		inputParam["notificationAdd_custNameTH"] = gblCustomerName;
		
		inputParam["notificationAdd_branchNameEN"] = gblFinActivityLogOpenAct["branchEN"];
		inputParam["notificationAdd_branchNameTH"] = gblFinActivityLogOpenAct["branchTH"];
		
		inputParam["notificationAdd_toAccountNameTH"] = gblPartyInqOARes["FullName"];
		inputParam["notificationAdd_toAccountNameEN"] = gblPartyInqOARes["PrefName"];
        //ACTIVITY LOG




			  if(gblSelOpenActProdCode == "200" || gblSelOpenActProdCode == "220"  || gblSelOpenActProdCode == "222" ){
				inputParam["activityLog_activityTypeID"] = "039";
				inputParam["openActModule"] = "openAccount";
			  }else if(gblSelOpenActProdCode == "221"){
				inputParam["activityLog_activityTypeID"] = "042";
				inputParam["openActModule"] = "openAccount";
			  }else if(gblSelOpenActProdCode == "206"){
				inputParam["activityLog_activityTypeID"] = "040";
				inputParam["openActModule"] = "openAccount";
				inputParam["activityLog_activityFlexValues1"] = gblFinActivityLogOpenAct["dreamMonth"];
				inputParam["activityLog_activityFlexValues3"] = mnthlyDreamAmt;
				inputParam["activityLog_activityFlexValues5"] = dreamTargetID +  "+" + dreamTarget;
			  }else if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0){
			  
			  //For activity logging
				/*
			  	var befPer = savingCareText[0][0];
				var befPerVthSym = "";
				befPer = befPer.replace("%", "");
				var befName = savingCareText[0][1] + " " + savingCareText[0][2] + " " + befPer;
				if (savingCareText[1][0] == true){
				befPerVthSym = savingCareText[1][1];
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + savingCareText[1][2] + " " + savingCareText[1][3] + " " + befPerVthSym;
				}
				if (savingCareText[2][0] == true){
				befPerVthSym = savingCareText[2][1];
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + savingCareText[2][2] + " " + savingCareText[2][3] + " " + befPerVthSym;
				}
				if (savingCareText[3][0] == true){
				befPerVthSym = savingCareText[3][1];
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + savingCareText[3][2] + " " + savingCareText[3][3] + " " + befPerVthSym;
				}
				if (savingCareText[4][0] == true){
				befPerVthSym = savingCareText[4][1];
				befPerVthSym =befPerVthSym.replace("%", "");
				befName = befName + "," + savingCareText[4][2] + " " + savingCareText[4][3] + " " + befPerVthSym;
				}
				
				//For inserting date into DB

				var insertbefName = savingCareText[0][1]; 
				
				
				
				var insertbefRel = savingCareText[0][2];
				var insertbefPer = savingCareText[0][0];
				var insertbefPerVthSym = "";
				insertbefPer = insertbefPer.replace("%", "");
				if (savingCareText[1][0] == true){
				insertbefName = insertbefName + "~" + savingCareText[1][2];
				insertbefRel = insertbefRel + "~" + savingCareText[1][3];
				insertbefPerVthSym = savingCareText[1][1];
				insertbefPerVthSym =befPerVthSym.replace("%", "");
				insertbefPer = insertbefPer + "~" + insertbefPerVthSym;
				}
				if (savingCareText[2][0] == true){
				insertbefName = insertbefName + "~" + savingCareText[2][2];
				insertbefRel = insertbefRel + "~" + savingCareText[2][3];
				insertbefPerVthSym = savingCareText[2][1];
				insertbefPerVthSym = insertbefPerVthSym.replace("%", "");
				insertbefPer = insertbefPer + "~" + insertbefPerVthSym;
				}
				if (savingCareText[3][0] == true){
				insertbefName = insertbefName + "~" + savingCareText[3][2];
				insertbefRel = insertbefRel + "~" + savingCareText[3][3];
				insertbefPerVthSym = savingCareText[3][1];
				insertbefPerVthSym = insertbefPerVthSym.replace("%", "");
				insertbefPer = insertbefPer + "~" + insertbefPerVthSym;
				}
				if (savingCareText[4][0] == true){
				insertbefName = insertbefName + "~" + savingCareText[4][2];
				insertbefRel = insertbefRel + "~" + savingCareText[4][3];
				insertbefPerVthSym = savingCareText[4][1];
				insertbefPerVthSym = insertbefPerVthSym.replace("%", "");
				insertbefPer = insertbefPer + "~" + insertbefPerVthSym;
				}
				
				**/
				var fname = "";
					var lname = "";
					var relation = ""
					var percentage = "";
					var bName = "";
					var relationCode = "";
					var beneFlag = "";
					var beneIds = "";
					 if (platformChannel != "thinclient"){
						for(var i = 0;i<arrayBenificiary.length;i++){
							var relationCode = arrayBenificiary[i]["relationCode"];
							/*
							if (getCurrentLocale() == "en_US") {
								relationCode=gblRelationDescAndCodeEN[arrayBenificiary[i]["relation"]];
								
							}else{
								relationCode=gblRelationDescAndCodeTH[arrayBenificiary[i]["relation"]];
							}
							*/
							 
							//fname = fname + arrayBenificiary[i]["firstName"] + arrayBenificiary[i]["lastName"]  + " " + arrayBenificiary[i]["relation"] + " " + arrayBenificiary[i]["percentage"] + "," ;
							fname = fname + arrayBenificiary[i]["firstName"] + arrayBenificiary[i]["lastName"]  + " " + relationCode + " " + arrayBenificiary[i]["percentage"] + "," ;
							//relation = relation + arrayBenificiary[i]["relation"]+",";
							relation = relation + relationCode +",";
							percentage = percentage + arrayBenificiary[i]["percentage"]+",";
							percentage = percentage.replace("%", "");
							bName = bName + arrayBenificiary[i]["firstName"]+ " " + arrayBenificiary[i]["lastName"] + ",";
							kony.print("GOWRI gblSavingsCareFlow="+gblSavingsCareFlow);
							/*if(gblSavingsCareFlow=="MyAccountFlow" || gblSavingsCareFlow=="AccountDetailsFlow"){
								kony.print("GOWRI123 gblBeneficiaryData["+i+"]="+JSON.stringify(gblBeneficiaryData[i]));
								kony.print("GOWRI456 UBCId="+gblBeneficiaryData[i]["UBCId"]);
								if(gblBeneficiaryData[i]["UBCId"]==undefined){
									beneIds=beneIds+"@"+ ",";
								}else{
									beneIds+=gblBeneficiaryData[i]["UBCId"]+ ",";
								}
									beneIds+=gblBeneficiaryData[i]["UBCId"]+ ",";
								if(gblBeneficiaryData[i]["uboActFlg"]==undefined){
									beneFlag+="Y"+ ",";
								}else{
									beneFlag+=gblBeneficiaryData[i]["uboActFlg"]+ ",";
								}
							}	*/	
						}
					 }else{
	                        for (var i = 0; i < gblBenificiaryCount; i++) {
	                        	
	                            if(frmIBOpenNewSavingsCareAcc["txtBenifit" + i] != undefined){
	                            if(frmIBOpenNewSavingsCareAcc["txtBenifit" + i].text != "0") {
		                        	kony.print("gblBenificiaryCount"+gblBenificiaryCount);
		                            fname = fname + frmIBOpenNewSavingsCareAcc["txtFirstName" + i].text + frmIBOpenNewSavingsCareAcc["txtLastName" + i].text  + " " + frmIBOpenNewSavingsCareAcc["comboRelationship" + i].selectedKey + " " + frmIBOpenNewSavingsCareAcc["txtBenifit" + i] + "," ;
		                            relation = relation + frmIBOpenNewSavingsCareAcc["comboRelationship" + i].selectedKey +",";
		                            percentage = percentage + frmIBOpenNewSavingsCareAcc["txtBenifit" + i].text.replace("%", "")+",";
		                            bName = bName +frmIBOpenNewSavingsCareAcc["txtFirstName" + i].text+ " " + frmIBOpenNewSavingsCareAcc["txtLastName" + i].text + ","
                                }    
                                }         
                     		}
					}
					bName = bName.substring(bName, bName.length - 1);
					relation = relation.substring(relation, relation.length - 1);					
					percentage = percentage.substring(percentage, percentage.length - 1);
					
				//kony.print("beneIds" + beneIds + "beneFlag" + beneFlag);	
				//kony.print("bNam111111e" + bName + "relation2222" + relation + "percentage3333333" +percentage);
				inputParam["beneficiaryfullNames"] = bName;
				inputParam["beneficiaryrelationship"] = relation;
				inputParam["beneficiarypercentage"] = percentage;
				
					
				inputParam["insertDataInDB_benName"] = bName;
				inputParam["insertDataInDB_benRelation"] = relation;
				
				inputParam["insertDataInDB_benPer"] = percentage;
			  	inputParam["activityLog_activityTypeID"] =	"081";
			  	inputParam["activityLog_activityFlexValues5"] = fname;
			  	inputParam["acctType"] = "SDA";
			 	inputParam["acctTypeValAcctRef"] = "SDA";
			 	 if (platformChannel == "thinclient") {
			 	 	inputParam["nickname"] = gblFinActivityLogOpenAct["toActNickName"];
			 	 }else{
			 	inputParam["nickname"] = frmMBSavingsCareConfirmation.lblnicknameVal.text;//gblFinActivityLogOpenAct["toActNickName"];
			 	}
			 	kony.print("inputParam[acctTypeValAcctRef---->]" + inputParam["acctTypeValAcctRef"]);
			 	kony.print("inputParam[acctType---->]" + inputParam["acctType"]);
			 	
			  }else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602" || gblSelOpenActProdCode == "659" || gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
				inputParam["activityLog_activityTypeID"] = "041";
				inputParam["openActModule"] = "openAccount";
			  }


        //For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
        inputParam["activityLog_errorCd"] = "";
        if (platformChannel == "thinclient")
            inputParam["activityLog_channelId"] = GLOBAL_IB_CHANNEL;
        else
            inputParam["activityLog_channelId"] = GLOBAL_MB_CHANNEL;
            
            inputParam["activityLog_logLinkageId"] = "";
			setFinancialActivityLogOpenActComp(gblFinActivityLogOpenAct["fromActId"],gblFinActivityLogOpenAct["fromActName"], gblFinActivityLogOpenAct["fromActNickName"], gblFinActivityLogOpenAct["fromActType"],  gblFinActivityLogOpenAct["toActName"], gblFinActivityLogOpenAct["toActNickName"],gblFinActivityLogOpenAct["toActType"], gblFinActivityLogOpenAct["transferAmount"],channelCode,inputParam);
		    inputParam["insertDataInDB_dreamTargetId"] = dreamTargetID;
		    inputParam["insertDataInDB_dreamDesc"] = dreamDesc;
		    inputParam["insertDataInDB_dreamTargetAmnt"] = dreamTarget;
		    inputParam["locale"] = kony.i18n.getCurrentLocale();
		    if(gblFinActivityLogOpenAct["channelName"] == "MB" || gblFinActivityLogOpenAct["channelName"] == "SPA"){
				showLoadingScreen();
			}else{
				showLoadingScreenPopup();
			}
		    
		if(gblSavingsCareFlow=="MyAccountFlow" || gblSavingsCareFlow=="AccountDetailsFlow"){
	 		inputParam["openActModule"] = "EditBeneficiary";
	 		inputParam["inqToActId"] = gblSavingsCareAccId;
	 	}else
	 		inputParam["openActModule"] = "openAccount";
		kony.print("inputParams before calling service" + JSON.stringify(inputParam));
        invokeServiceSecureAsync("openActConfirmCompositeService", inputParam, validateOTPtextOpenAccountJavaServiceCallback);

    }

function validateOTPtextOpenAccountJavaServiceCallback(status,callBackResponse) {
        if (status == 400) {
            if (callBackResponse["opstatus"] == 0) {
             
             //kony.print("Business Hours flag------>" + callBackResponse["openActBusinessHrsFlag"]);
             if(callBackResponse["openActBusinessHrsFlag"] == "false") {
             	var key = kony.i18n.getLocalizedString("keyBusinessHours");
           		var res = key.replace("<time>",callBackResponse["openActStartTime"] + "-" + callBackResponse["openActEndTime"]);
           		dismissLoadingScreen();
           		popupTractPwd.dismiss();
             	showAlertWithCallBack(res, kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
				return false;
             }
				if(gblFinActivityLogOpenAct["channelName"] == "MB" || gblFinActivityLogOpenAct["channelName"] == "SPA"){
							
							if(gblFinActivityLogOpenAct["channelName"] == "SPA"){
							 	otplocked = false;
					            gblVerifyOTPCounter = "0";
					            getHeader(mobileMethod, kony.i18n.getLocalizedString("keySetPasswordLabel"), 0, 0, 0);
					            gblShowPinPwdSecs = kony.os.toNumber(callBackResponse["showPinPwdSecs"]);
					            gblRetryCountRequestOTP = "0";
					            gblShowPwdNo = kony.os.toNumber(callBackResponse["showPinPwdCount"]);
					            popOtpSpa.dismiss();
							}else{
								popupTractPwd.dismiss();
							}
					
					
					if (gblSelProduct == "TMBDreamSavings") {
						
						frmOpenActDSAck.imgDSAckTitle.src = frmOpenActDSConfirm.imgDSCnfmTitle.src;
						frmOpenActDSAck.lblDSAckTitle.text = frmOpenActDSConfirm.lblDSCnfmTitle.text;
						frmOpenActDSAck.lblMyDreamDesValAck.text = frmOpenActDSConfirm.lblMyDreamDesVal.text;
						frmOpenActDSAck.imgOADreamDetailAck.src = frmOpenActDSConfirm.imgOADreamDetail.src;
						frmOpenActDSAck.lblOADreamDetailTarAmtValAck.text =frmOpenActDSConfirm.lblOADreamDetailTarAmtVal.text;
						frmOpenActDSAck.lblOADSNickNameValAck.text = frmOpenActDSConfirm.lblOADSNickNameVal.text;
						frmOpenActDSAck.lblOADSActNameValAck.text = frmOpenActDSConfirm.lblOADSActNameVal.text ;
						frmOpenActDSAck.lblOADSAmtValAck.text = frmOpenActDSConfirm.lblOADSAmtVal.text;
						frmOpenActDSAck.lblOADSBranchValAck.text = frmOpenActDSConfirm.lblOADSBranchVal.text;
						frmOpenActDSAck.lblOADMnthValAck.text = frmOpenActDSConfirm.lblOADMnthVal.text;
						frmOpenActDSAck.lblOADIntRateValAck.text = frmOpenActDSConfirm.lblOADIntRateVal.text;
							if (kony.i18n.getCurrentLocale() == "en_US") {
									frmOpenActDSAck.lblOADIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
								} else {
									frmOpenActDSAck.lblOADIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
								}
						frmOpenActDSAck.lblDSOpenDateValAck.text =frmOpenActDSConfirm.lblDSOpenDateVal.text;
						frmOpenActDSAck.lblOAMnthlySavNameAck.text = frmOpenActDSConfirm.lblOAMnthlySavName.text;
						frmOpenActDSAck.lblOAMnthlySavTypeAck.text =frmOpenActDSConfirm.lblOAMnthlySavType.text;
						frmOpenActDSAck.imgOAMnthlySavPicAck.src =frmOpenActDSConfirm.imgOAMnthlySavPic.src;
						frmOpenActDSAck.lblOAMnthlySavNumAck.text = frmOpenActDSConfirm.lblOAMnthlySavNum.text
						frmOpenActDSAck.lblDSAckActNoVal.text = callBackResponse["toActId"];
						gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
						frmOpenActDSAck.label47505874741650.text =  kony.i18n.getLocalizedString("keyOpenNotifyOne");
						frmOpenActDSAck.lblOADSAmtAck.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt"); //removed : as i18 has it + ":";
					frmOpenActDSAck.lblOADMnthAck.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth"); //removed : as i18 has it + ":";
					dismissLoadingScreen();
					frmOpenActDSAck.show();
					} else if (gblSelProduct == "ForTerm") {
							if(flowSpa)
								frmOpenActTDAck.btnOATDAckOpenMoreSpa.text = kony.i18n.getLocalizedString("btnOpenMore");
						if(!flowSpa)
								frmOpenActTDAck.btnOATDAckOpenMore.text = kony.i18n.getLocalizedString("btnOpenMore");
								
						frmOpenActTDAck.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi");
					   frmOpenActTDAck.imgOATDAckTitle.src = frmOpenActTDConfirm.imgTDCnfmTitle.src;	
						frmOpenActTDAck.lblOATDAckTitle.text = frmOpenActTDConfirm.lblTDCnfmTitle.text;	
						frmOpenActTDAck.lblOATDNickNameValAck.text = frmOpenActTDConfirm.lblOATDNickNameVal.text;	
						frmOpenActTDAck.lblOATDActNameValAck.text = frmOpenActTDConfirm.lblOATDActNameVal.text;	 
						frmOpenActTDAck.lblOATDBranchValAck.text = frmOpenActTDConfirm.lblOATDBranchVal.text;	
						frmOpenActTDAck.lblOATDAmtValAck.text = frmOpenActTDConfirm.lblOATDAmtVal.text;
						frmOpenActTDAck.lblOATDIntRateValAck.text = frmOpenActTDConfirm.lblOATDIntRateVal.text;	
						frmOpenActTDAck.lblTDOpenDateValAck.text = frmOpenActTDConfirm.lblTDOpenDateVal.text;	
						frmOpenActTDAck.lblOATDTransRefAckVal.text = frmOpenActTDConfirm.lblTdTransRefVal.text;	
						if (kony.i18n.getCurrentLocale() == "en_US") {
									frmOpenActTDAck.lblOATDIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
								} else {
									frmOpenActTDAck.lblOATDIntRateAck.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
								}
								
				if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602"){
				 frmOpenActTDAck.lblGreeting.text = kony.i18n.getLocalizedString('keyTermGreet');
				} else if(gblSelOpenActProdCode == "659"){
				frmOpenActTDAck.lblGreeting.text = kony.i18n.getLocalizedString('keyTermUpGreet');
				} else if(gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
				frmOpenActTDAck.lblGreeting.text = kony.i18n.getLocalizedString('keyTermQuickGreet');
				}
						
						if (gblisDisToActTD == "Y")
						{	
							frmOpenActTDAck.lblOAPayIntAck.setVisibility(true);
							frmOpenActTDAck.hbxOAPayIntAck.setVisibility(true);
							frmOpenActTDAck.lblOATDToNicNam.text = frmOpenActTDConfirm.lblTDTransToNickName.text;
							frmOpenActTDAck.lblOATDToActType.text = frmOpenActTDConfirm.lblTDTransToActType.text;	
							frmOpenActTDAck.lblOATDToAmt.text = frmOpenActTDConfirm.lblTDTransToAccBal.text;
							frmOpenActTDAck.imgOATDToAck.src =  frmOpenActTDConfirm.imgOATDProTo.src;
						}else{
							frmOpenActTDAck.lblOAPayIntAck.setVisibility(false);
							frmOpenActTDAck.hbxOAPayIntAck.setVisibility(false);
						}
							frmOpenActTDAck.lblOAFrmNameAck.text = frmOpenActTDConfirm.lblTDTransFrmNickName.text ;	
							frmOpenActTDAck.lblOAFrmTypeAck.text = frmOpenActTDConfirm.lblTDTransFrmActType.text;	
							frmOpenActTDAck.lblOAFrmNumAck.text = frmOpenActTDConfirm.lblTDTransFromAccBal.text;
							frmOpenActTDAck.imgOAFrmPicAck.src = frmOpenActTDConfirm.imgOAMnthlySavPic.src;
										
							frmOpenActTDAck.lblOATDAckActNoVal.text = callBackResponse["toActId"];
							gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
							frmOpenActTDAck.lblTDMatDateValAck.text = reformatDate(callBackResponse["nxtMaturityDate"]);
							frmOpenActTDAck.lblOATDTermValAck.text = callBackResponse["termCount"];
							frmOpenActTDAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
						if (callBackResponse["xferStatusCode"] != 0){
							frmOpenActTDAck.lblOpenActCnfmBalAmt.text = frmOpenActTDConfirm.lblOpenActCnfmBalAmt.text;
							frmOpenActTDAck.lblOATDAmtValAck.text = frmOpenActTDConfirm.lblOATDAmtVal.text;//commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
							frmOpenActTDAck.hbxCannotTrans.setVisibility(true);
							frmOpenActTDAck.lblNotAvailable.setVisibility(true);
						}
					   else{
						frmOpenActTDAck.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct(callBackResponse["fromActAvailBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
						frmOpenActTDAck.hbxCannotTrans.setVisibility(false);
						frmOpenActTDAck.lblNotAvailable.setVisibility(false);
						}
						dismissLoadingScreen();
						frmOpenActTDAck.show();
						
					
					
					}else if(gblSelProduct == "TMBSavingcare"){
						kony.print("Service Success");
						/*
						frmOpenActSavingCareCnfNAck.lblOASCActNumVal.text = callBackResponse["toActId"];
						frmOpenActSavingCareCnfNAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
						gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
						frmOpenActSavingCareCnfNAck.lblGreetingSavingCare.setVisibility(true);
						if (callBackResponse["xferStatusCode"] != 0){
							//frmOpenActSavingCareCnfNAck.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
							frmOpenActSavingCareCnfNAck.lblOASCOpenActVal.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
							frmOpenActSavingCareCnfNAck.hbxCannotTrans.setVisibility(true);
							frmOpenActSavingCareCnfNAck.lblNotAvailable.setVisibility(true);
						}else{
							frmOpenActSavingCareCnfNAck.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct(callBackResponse["fromActAvailBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
							frmOpenActSavingCareCnfNAck.hbxCannotTrans.setVisibility(false);
							frmOpenActSavingCareCnfNAck.lblNotAvailable.setVisibility(false);
						}
						onClickPreShowSCAck();
						**/
						dismissLoadingScreen();
						
						//fix MIB-4588
						if(gblSavingsCareFlow=="MyAccountFlow"){	
							callBeneficiaryInquiryService(gblSavingsCareAccId);
							frmMyAccountView.show();
						}
						else if	(gblSavingsCareFlow == "AccountDetailsFlow"){
							callBeneficiaryInquiryService(gblSavingsCareAccId);
							frmAccountDetailsMB.show();
						}else if(gblSavingsCareFlow == "openAccount"){ 
							if(callBackResponse["xferStatusCode"] != 0){
								gblOpenAccountFail = true;
								var fromActAvailBal = callBackResponse["fromActAvailBal"];
								if(fromActAvailBal == ""){
									fromActAvailBal = 0;
								}
								frmMBSavingsCareComplete.image2761510865404343.src = "iconnotcomplete.png";
								frmMBSavingsCareComplete.lblOpeningAmtVal.text=fromActAvailBal;				
							}else{
								var savingscareNewActNum = callBackResponse["toActId"];
								if(isNotBlank(savingscareNewActNum)){
									frmMBSavingsCareComplete.lblAccountNumVal.text = savingscareNewActNum;
								}
								gblOpenAccountFail = false;
								frmMBSavingsCareComplete.image2761510865404343.src = "completeico.png";
								frmMBSavingsCareComplete.lblOpeningAmtVal.text = frmMBSavingsCareConfirmation.lblOpeningAmtVal.text;
							}
							frmMBSavingsCareComplete.show();
							//showMBOpenSavingscareComplete();
						}
						
					}else if(gblSelProduct == "ForUse"){
					
						frmOpenAccountNSConfirmation.lblAccuntNoVal.text = callBackResponse["toActId"];
						frmOpenAccountNSConfirmation.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
							gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
						frmOpenAccountNSConfirmation.lblGreetingNS.setVisibility(true);
						if(gblSelOpenActProdCode == "220"){
						 frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyNoFeeGreet');
						} else if(gblSelOpenActProdCode == "200"){ 
						frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyNormalSavGreet');
						} else if(gblSelOpenActProdCode == "221"){ 
						frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyNoFixedGreet');
						} else if(gblSelOpenActProdCode == "222" ){
						frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyFreeFlowGreet');
						} else if(gblSelOpenActProdCode == "225"){
							frmOpenAccountNSConfirmation.lblGreetingNS.text = kony.i18n.getLocalizedString('keyAllFreeGreet');
						}
							
							
							
						if (callBackResponse["xferStatusCode"] != 0){
						//frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
						frmOpenAccountNSConfirmation.lblOpenAmtVal.text = commaFormatted(parseFloat(removeCommos("0")).toFixed(2)) + kony.i18n.getLocalizedString("currencyThaiBaht");
						frmOpenAccountNSConfirmation.hbxCannotTrans.setVisibility(true);
						frmOpenAccountNSConfirmation.lblNotAvailable.setVisibility(true);
						
						}else{
							if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
								frmOpenAccountNSConfirmation.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("keyBalanceAfterTrans");
								frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.text = commaFormattedOpenAct(callBackResponse["fromActAvailBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
								frmOpenAccountNSConfirmation.hbxTransRefNo.setVisibility(true);
								frmOpenAccountNSConfirmation.hbxOpenAmt.setVisibility(true);
								//aue
								frmOpenAccountNSConfirmation.hbxNickName.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxAccuntNo.skin = hboxWhite;
								frmOpenAccountNSConfirmation.hbox47420125326014.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxBranch.skin = hboxWhite;
								frmOpenAccountNSConfirmation.hbxOpenAmt.skin = hboxLightGrey;						
								frmOpenAccountNSConfirmation.hbxIntrstRate.skin = hboxWhite;
								frmOpenAccountNSConfirmation.hbxOpenDate.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxCardType.skin = hboxWhite;
								frmOpenAccountNSConfirmation.hbxCardFee.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxTransRefNo.skin = hboxWhite;
								//aue

								//frmOpenAccountNSConfirmation.hbxIntrstRate.skin = hboxLightGrey;
//								frmOpenAccountNSConfirmation.hbxOpenDate.skin = hboxWhite;
//								frmOpenAccountNSConfirmation.hbxCardType.skin = hboxLightGrey;
//								frmOpenAccountNSConfirmation.hbxCardFee.skin = hboxWhite;
//								frmOpenAccountNSConfirmation.hbxTransRefNo.skin = hboxLightGrey;
							} else {
								frmOpenAccountNSConfirmation.hbxTransRefNo.setVisibility(false);
								//frmOpenAccountNSConfirmation.lblOpenActCnfmBal.setVisibility(false);
								//frmOpenAccountNSConfirmation.lblOpenActCnfmBalAmt.setVisibility(false);
								frmOpenAccountNSConfirmation.hbxOpenAmt.setVisibility(false);
								//aue
								frmOpenAccountNSConfirmation.hbxNickName.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxAccuntNo.skin = hboxWhite;
								frmOpenAccountNSConfirmation.hbox47420125326014.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxBranch.skin = hboxWhite;
								frmOpenAccountNSConfirmation.hbxIntrstRate.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxOpenDate.skin = hboxWhite;
								frmOpenAccountNSConfirmation.hbxCardType.skin = hboxLightGrey;
								frmOpenAccountNSConfirmation.hbxCardFee.skin = hboxWhite;
								//aue
								
								//frmOpenAccountNSConfirmation.hbxIntrstRate.skin = hboxWhite;
//								frmOpenAccountNSConfirmation.hbxOpenDate.skin = hboxLightGrey;
//								frmOpenAccountNSConfirmation.hbxCardType.skin = hboxWhite;
//								frmOpenAccountNSConfirmation.hbxCardFee.skin = hboxLightGrey;
							}
						frmOpenAccountNSConfirmation.hbxCannotTrans.setVisibility(false);
						frmOpenAccountNSConfirmation.lblNotAvailable.setVisibility(false);
						
						}
						
						dismissLoadingScreen();	
						onclickConfirmTransNSPwd();
					}
					
					if(gblSelOpenActProdCode=="221"){
						campaginService("imgTwo","frmOpenAccountNSConfirmation","M");
					}
					if(gblSelOpenActProdCode=="200"){
						campaginService("imgOpnActNSAdd","frmOpenAccountNSConfirmation","M");
					}
					// added below if block for Open TMB All Free
					if(gblSelOpenActProdCode == "225"){
						campaginService("imgTwo","frmOpenAccountNSConfirmation_AllFree","M");
					}
				
				}else if(gblFinActivityLogOpenAct["channelName"] == "IB"){
						if (gblSelProduct == "TMBDreamSavings") {
						
							dataTransfrFromConfToComDream();
							var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
							var fdata1 = frmIBOpenNewDreamAcc.coverFlowTP.data[gblCWSelectedItem];
							var selvalue = frmIBOpenNewSavingsAcc.btnDreamSavecombo.selectedKeyValue;
							frmIBOpenNewDreamAccConfirmation.lblOADMnthVal.text = selvalue[1];
																
								if (kony.i18n.getCurrentLocale() == "en_US") {
									frmIBOpenNewDreamAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
								} else {
									frmIBOpenNewDreamAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
								}
								frmIBOpenNewDreamAccComplete.lblOASCActNumVal.text = callBackResponse["toActId"];
								gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
							frmIBOpenNewDreamAccComplete.imgDSAckTitle.src = producticonfordream;
							frmIBOpenNewDreamAccComplete.lblDSAckTitle.text = frmIBOpenNewDreamAccConfirmation.lblDreamSavings.text;
							frmIBOpenNewDreamAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
							frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text = frmIBOpenNewDreamAccConfirmation.lblDreamOpenActNameVal.text;
							var locale = kony.i18n.getCurrentLocale();
							if (locale == "th_TH") {
				    			frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text  = gblPartyInqOARes["FullName"];
				    		}else if (locale == "en_US") {
				    			frmIBOpenNewDreamAccComplete.lblDreamOpenAcctNamComVal.text  = gblPartyInqOARes["PrefName"];
				    		}
							dismissLoadingScreenPopup();	
							frmIBOpenNewDreamAccComplete.show();
					}else if (gblSelProduct == "TMBSavingcare") {
							dataTrasfrFromConfToCompSavingCare();							
							if (kony.i18n.getCurrentLocale() == "en_US") {
								frmIBOpenNewSavingsCareAccComplete.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
							} else {
								frmIBOpenNewSavingsCareAccComplete.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
							}
							frmIBOpenNewSavingsCareAccComplete.lblOASCActNumVal.text = callBackResponse["toActId"];
							gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
							frmIBOpenNewSavingsCareAccComplete.imgOASCTitle.src = frmIBOpenNewSavingsCareAccConfirmation.imgOASCTitle.src;
							frmIBOpenNewSavingsCareAccComplete.lblOASCTitle.text = frmIBOpenNewSavingsCareAccConfirmation.lblOASCTitle.text;
							frmIBOpenNewSavingsCareAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne") ;
							if (callBackResponse["xferStatusCode"] != 0){
							frmIBOpenNewSavingsCareAccComplete.lblOASCOpenActVal.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
							frmIBOpenNewSavingsCareAccComplete.hbox47505967399619.setVisibility(true);
							frmIBOpenNewSavingsCareAccComplete.lblIBOpenAccCnfmBalAmt.text = frmIBOpenNewSavingsCareAccConfirmation.lblBalance.text;
							}else{
							frmIBOpenNewSavingsCareAccComplete.hbox47505967399619.setVisibility(false);
							frmIBOpenNewSavingsCareAccComplete.lblIBOpenAccCnfmBalAmt.text=commaFormattedOpenAct(callBackResponse["fromActAvailBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
							}
							if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
							frmIBOpenNewSavingsCareAccComplete.hbxOASCOpenAct.setVisibility(true);
							frmIBOpenNewSavingsCareAccComplete.hbxOASCTranRefNo.setVisibility(true);
							frmIBOpenNewSavingsCareAccComplete.lblBalanceBefrTransfr.setVisibility(true);
							frmIBOpenNewSavingsCareAccComplete.lblIBOpenAccCnfmBalAmt.setVisibility(true);
							frmIBOpenNewSavingsCareAccComplete.vbxHideNS.setVisibility(true);
							frmIBOpenNewSavingsCareAccComplete.hbxOASCIntRate.skin = hbox320pxpadding;
							frmIBOpenNewSavingsCareAccComplete.hbxOASCOpenDate.skin = hboxLightGrey320px;
							}else{
							frmIBOpenNewSavingsCareAccComplete.hbxOASCOpenAct.setVisibility(false);
							frmIBOpenNewSavingsCareAccComplete.hbxOASCTranRefNo.setVisibility(false);
							frmIBOpenNewSavingsCareAccComplete.lblBalanceBefrTransfr.setVisibility(false);
							frmIBOpenNewSavingsCareAccComplete.lblIBOpenAccCnfmBalAmt.setVisibility(false);
							frmIBOpenNewSavingsCareAccComplete.vbxHideNS.setVisibility(false);
							frmIBOpenNewSavingsCareAccComplete.hbxOASCIntRate.skin = hboxLightGrey320px;
							frmIBOpenNewSavingsCareAccComplete.hbxOASCOpenDate.skin = hbox320pxpadding;
							}
					dismissLoadingScreenPopup();
					dismissLoadingScreen();
					setDynamicDatatoCompleteScreen();
					IBonClickPreShowSCAck();
					
					}else if (gblSelProduct == "ForTerm") {
							dataTransfrfromConfToCompTerm();
							frmIBOpenNewTermDepositAccComplete.lblAccNoVal.text = callBackResponse["toActId"];
							gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
							frmIBOpenNewTermDepositAccComplete.lblMaturityDateVal.text = reformatDate(callBackResponse["nxtMaturityDate"]);
							frmIBOpenNewTermDepositAccComplete.lblTermval.text = callBackResponse["termCount"];
							if(kony.i18n.getCurrentLocale() == "en_US"){
							frmIBOpenNewTermDepositAccComplete.lblInterst.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
							}else{
							frmIBOpenNewTermDepositAccComplete.lblInterst.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
							}
							frmIBOpenNewTermDepositAccComplete.imgTDCnfmTitle.src = frmIBOpenNewTermDepositAccConfirmation.imgTDCnfmTitle.src;
							frmIBOpenNewTermDepositAccComplete.lblTDCnfmTitle.text = frmIBOpenNewTermDepositAccConfirmation.lblTDCnfmTitle.text;
							frmIBOpenNewTermDepositAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
							if (callBackResponse["xferStatusCode"] != 0){
							frmIBOpenNewTermDepositAccComplete.hbox47505967399577.setVisibility(true);
							frmIBOpenNewTermDepositAccComplete.lblOpeningAmountValue.text = frmIBOpenNewTermDepositAccConfirmation.lblAccNameValue.text//commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
							frmIBOpenNewTermDepositAccComplete.lblIBOpenAccCnfmBalAmt.text = frmIBOpenNewTermDepositAccConfirmation.lblIBOpenAccCnfmBalAmt.text ; 
							}else{
							frmIBOpenNewTermDepositAccComplete.hbox47505967399577.setVisibility(false);
							frmIBOpenNewTermDepositAccComplete.lblIBOpenAccCnfmBalAmt.text = commaFormattedOpenAct(callBackResponse["fromActAvailBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht") ;
							}
							
							if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602"){
							 frmIBOpenNewTermDepositAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyTermGreet');
							} else if(gblSelOpenActProdCode == "659"){
							frmIBOpenNewTermDepositAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyTermUpGreet');
							} else if(gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
							frmIBOpenNewTermDepositAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyTermQuickGreet');
							}
							
							
							if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
							frmIBOpenNewTermDepositAccComplete.hbox866854059249871.setVisibility(true);
							frmIBOpenNewTermDepositAccComplete.hbox867569043311329.setVisibility(true);
							frmIBOpenNewTermDepositAccComplete.lblBalanceBefrTransfr.setVisibility(true);
							frmIBOpenNewTermDepositAccComplete.lblIBOpenAccCnfmBalAmt.setVisibility(true);
							frmIBOpenNewTermDepositAccComplete.vbxHideNS.setVisibility(true);
							frmIBOpenNewTermDepositAccComplete.hbox866854059249891.skin = hbox320pxpadding;
							frmIBOpenNewTermDepositAccComplete.hbox867569043311341.skin = hboxLightGrey320px;
							frmIBOpenNewTermDepositAccComplete.hbox867569043311353.skin = hbox320pxpadding;
							}else{
							frmIBOpenNewTermDepositAccComplete.hbox866854059249871.setVisibility(false);
							frmIBOpenNewTermDepositAccComplete.hbox867569043311329.setVisibility(false);
							frmIBOpenNewTermDepositAccComplete.lblBalanceBefrTransfr.setVisibility(false);
							frmIBOpenNewTermDepositAccComplete.lblIBOpenAccCnfmBalAmt.setVisibility(false);
							frmIBOpenNewTermDepositAccComplete.vbxHideNS.setVisibility(false);
							frmIBOpenNewTermDepositAccComplete.hbox866854059249891.skin = hboxLightGrey320px;
							frmIBOpenNewTermDepositAccComplete.hbox867569043311341.skin = hbox320pxpadding;
							frmIBOpenNewTermDepositAccComplete.hbox867569043311353.skin = hboxLightGrey320px;
							}
						dismissLoadingScreenPopup();
						dismissLoadingScreen();
						frmIBOpenNewTermDepositAccComplete.show();
									
					}else if (gblSelProduct == "ForUse") {
                    
							if (kony.i18n.getCurrentLocale() == "en_US") {
								frmIBOpenNewSavingsAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
							} else {
								frmIBOpenNewSavingsAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
							}
								frmIBOpenNewSavingsAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
								frmIBOpenNewSavingsAccComplete.label47505874741650.skin = lblIB20pxBlack;
								frmIBOpenNewSavingsAccComplete.label47505874741589.skin = lblIB20pxBlack;
								frmIBOpenNewSavingsAccComplete.lblAccuntNoVal.text = callBackResponse["toActId"];
								gblFinActivityLogOpenAct["toActId"] = callBackResponse["toActId"];
								gblFinActivityLogOpenAct["toActName"]=frmIBOpenNewSavingsAccConfirmation.lblActName.text;
								frmIBOpenNewSavingsAccComplete.imgNSProdName.src = frmIBOpenNewSavingsAccConfirmation.imgNSProdName.src;
								frmIBOpenNewSavingsAccComplete.lblNSProdName.text = frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text;
								if (callBackResponse["xferStatusCode"] != undefined && callBackResponse["xferStatusCode"] != 0){
								frmIBOpenNewSavingsAccComplete.hbox47505967399535.setVisibility(true);
								frmIBOpenNewSavingsAccComplete.lblOpenAmtVal.text = commaFormattedOpenAct("0") + kony.i18n.getLocalizedString("currencyThaiBaht");
								frmIBOpenNewSavingsAccComplete.lblIBOpenAccCnfmBalAmt.text = frmIBOpenNewSavingsAccConfirmation.lblIBOpenAccCnfmBalAmt.text;
								}else{
								frmIBOpenNewSavingsAccComplete.hbox47505967399535.setVisibility(false);
								frmIBOpenNewSavingsAccComplete.lblIBOpenAccCnfmBalAmt.text=commaFormattedOpenAct(callBackResponse["fromActAvailBal"]) + kony.i18n.getLocalizedString("currencyThaiBaht");
								}
								
								if(gblSelOpenActProdCode == "220"){
								 frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyNoFeeGreet');
								} else if(gblSelOpenActProdCode == "200"){ 
								frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyNormalSavGreet');
								} else if(gblSelOpenActProdCode == "221"){ 
								frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyNoFixedGreet');
								} else if(gblSelOpenActProdCode == "222" ){
								frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyFreeFlowGreet');
								} else if(gblSelOpenActProdCode == "225"){
								frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyAllFreeGreet');
								}
								
								
								if (kony.os.toNumber(grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"])) >0){
								  frmIBOpenNewSavingsAccComplete.hbox866854059249811.setVisibility(true);
								  frmIBOpenNewSavingsAccComplete.hbox866854059249891.setVisibility(true);
								  frmIBOpenNewSavingsAccComplete.lblBalanceBefrTransfr.setVisibility(true);
								  frmIBOpenNewSavingsAccComplete.lblIBOpenAccCnfmBalAmt.setVisibility(true);
								  frmIBOpenNewSavingsAccComplete.hbox866854059249897.skin = hboxLightGrey320px;
								  frmIBOpenNewSavingsAccComplete.hbox866854059249871.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccComplete.hbxCardType.skin = hboxLightGrey320px;
								  frmIBOpenNewSavingsAccComplete.hbxCardFee.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccComplete.vbxHideNS.setVisibility(true);
								}else{
								  frmIBOpenNewSavingsAccComplete.hbox866854059249811.setVisibility(false);
								  frmIBOpenNewSavingsAccComplete.hbox866854059249891.setVisibility(false);
								  frmIBOpenNewSavingsAccComplete.lblBalanceBefrTransfr.setVisibility(false);
								  frmIBOpenNewSavingsAccComplete.lblIBOpenAccCnfmBalAmt.setVisibility(false);
								  frmIBOpenNewSavingsAccComplete.vbxHideNS.setVisibility(false);
								  frmIBOpenNewSavingsAccComplete.hbox866854059249897.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccComplete.hbox866854059249871.skin = hboxLightGrey320px;
								  frmIBOpenNewSavingsAccComplete.hbxCardType.skin = hbox320pxpadding;
								  frmIBOpenNewSavingsAccComplete.hbxCardFee.skin = hboxLightGrey320px;
								  
								}
								if(gblIssueOnlineCard!= undefined && gblIssueOnlineCard=="1"){
									frmIBOpenNewSavingsAccComplete.hbxCardType.setVisibility(true);
									frmIBOpenNewSavingsAccComplete.hbxCardFee.setVisibility(true);
									frmIBOpenNewSavingsAccComplete.line474106367283368.setVisibility(true);
									frmIBOpenNewSavingsAccComplete.line476132054113112.setVisibility(true);
								}else{
									frmIBOpenNewSavingsAccComplete.hbxCardType.setVisibility(false);
									frmIBOpenNewSavingsAccComplete.hbxCardFee.setVisibility(false);
									frmIBOpenNewSavingsAccComplete.line474106367283368.setVisibility(false);
								}
						dismissLoadingScreen();
						frmIBOpenNewSavingsAccComplete.show();
						
						if(gblSelOpenActProdCode=="221"){
							campaginService("imgTwo","frmIBOpenNewSavingsAccComplete","I");
						}
						if(gblSelOpenActProdCode=="200"){
							campaginService("imgOpnActNSAdd","frmIBOpenNewSavingsAccComplete","I");
						}
						// added below if block for Open TMB All Free
						if(gblSelOpenActProdCode == "225"){
							campaginService("imgTwo","frmIBOpenNewSavingsAccComplete_AllFree","I");
						}
									
					}
		  
						
				}
                
               
            } else if ((callBackResponse["opstatus"] == 8005)) {

                if (gblFinActivityLogOpenAct["channelName"] == "IB") {
                	  curr_form.tbxToken.text = "";
             		  curr_form.txtOTP.text = "";
                     
	                    dismissLoadingScreenPopup();
	                    if (callBackResponse["errCode"] == "VrfyOTPErr00001" || callBackResponse["errCode"] == "VrfyOTPErr00006") {
	                        gblVerifyOTPCounter = "0";
	                        dismissLoadingScreenPopup();
								
								//Added code to fix ENH_87:DEF340 - OTP Incorrect inline message issue
											                    
			                    if (gblSelProduct == "TMBDreamSavings") {
				               		frmIBOpenNewDreamAccConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
				                    frmIBOpenNewDreamAccConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
				                    frmIBOpenNewDreamAccConfirmation.hbxOTPincurrect.isVisible = true;
				                    frmIBOpenNewDreamAccConfirmation.hbox866854059301598.isVisible = false;
				                    frmIBOpenNewDreamAccConfirmation.hbox866854059301638.isVisible = false;
				                    frmIBOpenNewDreamAccConfirmation.txtOTP.text= "";
				                    frmIBOpenNewDreamAccConfirmation.tbxToken.text= "";
				                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				                     frmIBOpenNewDreamAccConfirmation.tbxToken.setFocus(true);
				                    }else{
				                     frmIBOpenNewDreamAccConfirmation.txtOTP.setFocus(true);
				                    }
			                    } else if (gblSelProduct == "TMBSavingcare") {
			                    //Not in Scope
				               		
				               		frmIBOpenNewSavingsCareAccConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
				                    frmIBOpenNewSavingsCareAccConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
				                    frmIBOpenNewSavingsCareAccConfirmation.hbxOTPincurrect.isVisible = true;
				                    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059301598.isVisible = false;
				                    frmIBOpenNewSavingsCareAccConfirmation.hbox866854059301638.isVisible = false;
				                  
			                    } else if (gblSelProduct == "ForTerm") {
				               		frmIBOpenNewTermDepositAccConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
				                    frmIBOpenNewTermDepositAccConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
				                    frmIBOpenNewTermDepositAccConfirmation.hbxOTPincurrect.isVisible = true;
				                    frmIBOpenNewTermDepositAccConfirmation.hbox866854059301598.isVisible = false;
				                    frmIBOpenNewTermDepositAccConfirmation.hbox866854059301638.isVisible = false;
				                    frmIBOpenNewTermDepositAccConfirmation.txtOTP.text = "";
				                    frmIBOpenNewTermDepositAccConfirmation.tbxToken.text ="";
				                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				                     frmIBOpenNewTermDepositAccConfirmation.tbxToken.setFocus(true);
				                    }else{
				                     frmIBOpenNewTermDepositAccConfirmation.txtOTP.setFocus(true);
				                    }
				                    
			                    } else if (gblSelProduct == "ForUse") {
				               		frmIBOpenNewSavingsAccConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
				                    frmIBOpenNewSavingsAccConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
				                    frmIBOpenNewSavingsAccConfirmation.hbxOTPincurrect.isVisible = true;
				                    frmIBOpenNewSavingsAccConfirmation.hbox866854059301598.isVisible = false;
				                    frmIBOpenNewSavingsAccConfirmation.hbox866854059301638.isVisible = false;
				                    frmIBOpenNewSavingsAccConfirmation.txtOTP.text = "";
				                    frmIBOpenNewSavingsAccConfirmation.tbxToken.text = "";
				                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
				                     frmIBOpenNewSavingsAccConfirmation.tbxToken.setFocus(true);
				                    }else{
				                     frmIBOpenNewSavingsAccConfirmation.txtOTP.setFocus(true);
				                    }
			                    	
			                    }
	                        
	                        return false;
	                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
	                        dismissLoadingScreenPopup();
	                        // alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
	                        // startRcCrmUpdateProIB("04");
	                        handleOTPLockedIB(callBackResponse);
	                        return false;
	                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00005") {
	                        dismissLoadingScreenPopup();
	                       if(gblTokenSwitchFlag ==  true){
			                	showCommonAlertMYA("" + kony.i18n.getLocalizedString("invalidOTP"), null);
			                }else{
			                	showCommonAlertMYA("" + kony.i18n.getLocalizedString("invalidOTP"), null);
			                }
	                        return false;
	                    } else if (callBackResponse["errCode"] == "VrfyOTPErr00006") {
	                        //gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
	                        alert("" + callBackResponse["errMsg"]);
	                        return false;
	                    } else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
		                    dismissLoadingScreenPopup();
		                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
		                    return false;
	                   	}else if (callBackResponse["errCode"] == "depErr02") {
		                    dismissLoadingScreenPopup();
		                    showAlertIB(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
		                    return false;
	                   	}else if (callBackResponse["errCode"] == "depErr01") {
		                    dismissLoadingScreenPopup();
		                    showAlertIB(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
		                    return false;
	                   	}
	                   	else {
			                    
			                    //Added code to fix ENH_87:DEF340 - OTP Incorrect inline message issue
											                    
			                    if (gblSelProduct == "TMBDreamSavings") {
				               		frmIBOpenNewDreamAccConfirmation.lblOTPinCurr.text = "";
				                    frmIBOpenNewDreamAccConfirmation.lblPlsReEnter.text = ""; 
				                    frmIBOpenNewDreamAccConfirmation.hbxOTPincurrect.isVisible = false;
				                    frmIBOpenNewDreamAccConfirmation.hbox866854059301598.isVisible = true;
				                    frmIBOpenNewDreamAccConfirmation.hbox866854059301638.isVisible = true;
				                    frmIBOpenNewDreamAccConfirmation.txtOTP.setFocus(true);
			                    } else if (gblSelProduct == "TMBSavingcare") {
			                    //Not in Scope
				               
			                    } else if (gblSelProduct == "ForTerm") {
					                frmIBOpenNewTermDepositAccConfirmation.lblOTPinCurr.text = " ";
				                    frmIBOpenNewTermDepositAccConfirmation.lblPlsReEnter.text = " "; 
				                    frmIBOpenNewTermDepositAccConfirmation.hbxOTPincurrect.isVisible = false;
				                    frmIBOpenNewTermDepositAccConfirmation.hbox866854059301598.isVisible = true;
				                    frmIBOpenNewTermDepositAccConfirmation.hbox866854059301638.isVisible = true;
				                    frmIBOpenNewTermDepositAccConfirmation.txtOTP.setFocus(true);
			                    } else if (gblSelProduct == "ForUse") {
				               		frmIBOpenNewSavingsAccConfirmation.lblOTPinCurr.text = "";
				                    frmIBOpenNewSavingsAccConfirmation.lblPlsReEnter.text = ""; 
				                    frmIBOpenNewSavingsAccConfirmation.hbxOTPincurrect.isVisible = false;
				                    frmIBOpenNewSavingsAccConfirmation.hbox866854059301598.isVisible = true;
				                    frmIBOpenNewSavingsAccConfirmation.hbox866854059301638.isVisible = true;
				                    frmIBOpenNewSavingsAccConfirmation.txtOTP.setFocus(true);
			                    	
			                    }
			                    
			                dismissLoadingScreenPopup();
			                showCommonAlertMYA(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + callBackResponse["errMsg"], null);
			                return false;
			            }
                } else if (gblFinActivityLogOpenAct["channelName"] == "SPA") {


						            if (callBackResponse["errCode"] == "VrfyOTPErr00001") {
						                gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
						                popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
						                popOtpSpa.lblPopupTract4Spa.text = "";
						                popOtpSpa.txtOTP.text = "";
						                kony.application.dismissLoadingScreen();
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
						                gblVerifyOTPCounter = "0";
						                otplocked = true;
						                kony.application.dismissLoadingScreen();
						                popOtpSpa.dismiss();
						                popTransferConfirmOTPLock.show();
						                // calling crmprofileMod to update the user status
						                updteuserSpa();
						
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00003") {
						                popOtpSpa.txtOTP.text = "";
						                kony.application.dismissLoadingScreen();
						                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00004") {
						               	popOtpSpa.txtOTP.text = "";
						                kony.application.dismissLoadingScreen();
						                showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
						                return false;
						            } else if (callBackResponse["errCode"] == "VrfyOTPErr00005") {
						               popOtpSpa.txtOTP.text = "";
						               kony.application.dismissLoadingScreen();
						               showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
						               return false;
						            } else if (callBackResponse["errCode"] == "depErr02") {
					                    kony.application.dismissLoadingScreen();
					                    showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
					                    return false;
					                }else if (callBackResponse["errCode"] == "depErr01") {
					                    kony.application.dismissLoadingScreen();
					                    showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
					                    return false;
					                } else {
						                kony.application.dismissLoadingScreen();
						                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
						                return false;
						            }
						       
						        kony.application.dismissLoadingScreen();
						


                } else if (gblFinActivityLogOpenAct["channelName"] == "MB") {
                    if (callBackResponse["errCode"] == "VrfyTxPWDErr00003") {
                    	gblRtyCtrVrfyTxPin = "0";
                    	gblUserLockStatusMB = "03"; //DEF1124 Fix
						kony.application.dismissLoadingScreen();
						//showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00002"), kony.i18n.getLocalizedString("info"));
						popupTractPwd.lblPopupTract7.text = "";
						//var deviceInfo = kony.os.deviceInfo();
				      	var deviceHght = gblDeviceInfo["deviceHeight"];
		
						    if(deviceHght>480){
						    popTransferConfirmOTPLock.containerHeight = 50;
//						    kony_6.0 popUp issue fix
						    //popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
						    }else{
						    popTransferConfirmOTPLock.containerHeight = 62;
						    //popTransferConfirmOTPLock.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
						    }
						popupTractPwd.dismiss();
						popTransferConfirmOTPLock.show();
						return false;
						
					}else if (callBackResponse["errCode"] == "VrfyTxPWDErr00001") {
						kony.application.dismissLoadingScreen();
						popupTractPwd.lblPopupTract7.text = "";
						showAlert(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"));
						return false;
					} else if (callBackResponse["errCode"] == "VrfyTxPWDErr00002") {
						kony.application.dismissLoadingScreen();
						popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
						popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
						popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd")
						//showAlert(kony.i18n.getLocalizedString("ECVrfyTrnPwdErr00001"), kony.i18n.getLocalizedString("info"));
						return false;
					} else if (callBackResponse["errCode"] == "depErr02") {
	                    kony.application.dismissLoadingScreen();
	                    showAlert(callBackResponse["errMsg"], kony.i18n.getLocalizedString("info"));
	                    return false;
	                }else if (callBackResponse["errCode"] == "depErr01") {
	                   	kony.application.dismissLoadingScreen();
	                    showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
	                    return false;
	                }else {
							kony.application.dismissLoadingScreen();
							popupTractPwd.lblPopupTract7.text = "";
							showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
							return false;
				    }
	              }
	            } else {
	            	if (gblSavingsCareFlow == "openAccount" || gblSavingsCareFlow == "MyAccountFlow" || gblSavingsCareFlow == "AccountDetailsFlow"){
	            		kony.application.dismissLoadingScreen();
	            		showAlert(kony.i18n.getLocalizedString("keyBeneficiaryEditServiceFailed"), kony.i18n.getLocalizedString("info"));
	            		return false;
	            	}else{
	            		if (gblFinActivityLogOpenAct["channelName"] == "IB") {
		                    dismissLoadingScreenPopup();
		                    showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		                } else if (gblFinActivityLogOpenAct["channelName"] == "MB" || gblFinActivityLogOpenAct["channelName"] == "SPA") {
		                   dismissLoadingScreen();
		                   showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		                }
	            	}
	            }

        } else if (status == 300) {
            	if (gblFinActivityLogOpenAct["channelName"] == "IB") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                } else if (gblFinActivityLogOpenAct["channelName"] == "MB" || gblFinActivityLogOpenAct["channelName"] == "SPA") {
                   dismissLoadingScreen();
                   showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                }
        }
    }
    
    
    function setFinancialActivityLogOpenActComp(fromActIdVal,fromActName,fromActNickName,fromActType,toActName,toActNickName,toAccType,finTxnAmount,channelId,inputParam) {
	GBLFINANACIALACTIVITYLOG.finTxnRefId = gblTransferRefNo;
	GBLFINANACIALACTIVITYLOG.finTxnDate = "";
	GBLFINANACIALACTIVITYLOG.activityTypeId = "063";
	var txnCd = "";
	if (fromActType == "DDA") {
        txnCd = "881";
    } else {
        txnCd = "882";
    }
    if (toAccType == "DDA") {
        txnCd = txnCd + "1";
    }else if(toAccType == "SDA") {
        txnCd = txnCd + "2";
    }else{
        txnCd = txnCd + "3";
    }
	GBLFINANACIALACTIVITYLOG.txnCd = txnCd;
	//GBLFINANACIALACTIVITYLOG.tellerId = gblFinActivityLogOpenAct["tellerId"];
	GBLFINANACIALACTIVITYLOG.txnDescription = "done";
	GBLFINANACIALACTIVITYLOG.finLinkageId = "";
	//02 is for mobile banking
	GBLFINANACIALACTIVITYLOG.channelId = channelId;//"02";
	var fromAcctId = fromActIdVal;//.replace(/-/g, "");//kony.string.replace(fromActIdVal, "-", "");
	GBLFINANACIALACTIVITYLOG.fromAcctId = fromAcctId;
	GBLFINANACIALACTIVITYLOG.fromAcctName = fromActName + "";
	frmNickName = fromActNickName + "";
	frmNickName = frmNickName.substr(0, 20)
	GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmNickName;
	//
	//var toAcctId = toActIdVal.replace(/-/g, "");// kony.string.replace(toActIdVal, "-", "");
	//GBLFINANACIALACTIVITYLOG.toAcctId = toAcctId;
	GBLFINANACIALACTIVITYLOG.toAcctName = toActName + "";
	toNickName = toActNickName + "";
	toNickName = toNickName.substr(0, 20);
	GBLFINANACIALACTIVITYLOG.toAcctNickname = toNickName;	
	GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
	if(finTxnAmount!=null)
		finTxnAmount = finTxnAmount.replace(/-/g, "");
	else
		finTxnAmount = "";	
	
	GBLFINANACIALACTIVITYLOG.finTxnAmount = finTxnAmount;
	GBLFINANACIALACTIVITYLOG.finTxnFee = "";
	//GBLFINANACIALACTIVITYLOG.finTxnBalance = gblBalAfterXfer;
	GBLFINANACIALACTIVITYLOG.finTxnMemo = "";
	GBLFINANACIALACTIVITYLOG.billerCommCode = "";
	GBLFINANACIALACTIVITYLOG.billerRef1 = "";
	GBLFINANACIALACTIVITYLOG.billerRef2 = "";
	GBLFINANACIALACTIVITYLOG.billerCustomerName = "";
	GBLFINANACIALACTIVITYLOG.billerBalance = "";	
	GBLFINANACIALACTIVITYLOG.noteToRecipient = "";
	GBLFINANACIALACTIVITYLOG.recipientMobile = "";
	GBLFINANACIALACTIVITYLOG.recipientEmail = "";
	// 01 for success 02 for fail
	GBLFINANACIALACTIVITYLOG.finTxnStatus = "01";
	GBLFINANACIALACTIVITYLOG.smartFlag = "N";
	GBLFINANACIALACTIVITYLOG.clearingStatus = "";
	GBLFINANACIALACTIVITYLOG.errorCd = "0000";
	GBLFINANACIALACTIVITYLOG.finSchduleRefId = "";
	
	GBLFINANACIALACTIVITYLOG.activityRefId = ""; 	
	GBLFINANACIALACTIVITYLOG.eventId = gblTransferRefNo.substring(2, 18);
	
	GBLFINANACIALACTIVITYLOG.txnType = "009";
	//GBLFINANACIALACTIVITYLOG.openTdTerm = tdTerm;
	GBLFINANACIALACTIVITYLOG.openTdInterestRate = gblFinActivityLogOpenAct["tdInterestRate"];
	//GBLFINANACIALACTIVITYLOG.openTdMaturityDate = tdMaturityDate;
	GBLFINANACIALACTIVITYLOG.affiliatedAcctNickname = gblFinActivityLogOpenAct["affiliatedAcctNickname"];
	GBLFINANACIALACTIVITYLOG.affiliatedAcctId = gblFinActivityLogOpenAct["affiliatedAcctId"].replace(/-/g, "");
	GBLFINANACIALACTIVITYLOG.affiliatedAcctName = gblFinActivityLogOpenAct["affiliatedAcctName"];
	//GBLFINANACIALACTIVITYLOG.finFlexValues1 = "OWN";
	//GBLFINANACIALACTIVITYLOG.finFlexValues2 = fromAcctId;
	//GBLFINANACIALACTIVITYLOG.finFlexValues3 = toAcctId;
	//GBLFINANACIALACTIVITYLOG.finFlexValues4 = "TMB";
	//GBLFINANACIALACTIVITYLOG.finFlexValues5 = finTxnAmount;
	GBLFINANACIALACTIVITYLOG.openProdCode = gblSelOpenActProdCode;
	setInputParamsForFinancialActivityLogServiceCallOpenAct(GBLFINANACIALACTIVITYLOG,inputParam);
}
    
   

function setInputParamsForFinancialActivityLogServiceCallOpenAct(CRMFinancialActivityLogAddRequest,inputParam) {
	//inputParam = {};
	//inputParam["ipAddress"] = ""; 
	//inputParam["financialActivityLog_crmId"] = CRMFinancialActivityLogAddRequest.crmId;
	inputParam["financialActivityLog_finTxnRefId"] = CRMFinancialActivityLogAddRequest.finTxnRefId;
	inputParam["financialActivityLog_finTxnDate"] = CRMFinancialActivityLogAddRequest.finTxnDate;
	inputParam["financialActivityLog_activityTypeId"] = CRMFinancialActivityLogAddRequest.activityTypeId;
	inputParam["financialActivityLog_txnCd"] = CRMFinancialActivityLogAddRequest.txnCd;
	inputParam["financialActivityLog_tellerId"] = CRMFinancialActivityLogAddRequest.tellerId;
	inputParam["financialActivityLog_txnDescription"] = CRMFinancialActivityLogAddRequest.txnDescription;
	inputParam["financialActivityLog_finLinkageId"] = CRMFinancialActivityLogAddRequest.finLinkageId;
	inputParam["financialActivityLog_channelId"] = CRMFinancialActivityLogAddRequest.channelId;
	inputParam["financialActivityLog_fromAcctId"] = CRMFinancialActivityLogAddRequest.fromAcctId;
	//inputParam["financialActivityLog_toAcctId"] = CRMFinancialActivityLogAddRequest.toAcctId;
	inputParam["financialActivityLog_toBankAcctCd"] = CRMFinancialActivityLogAddRequest.toBankAcctCd;
	inputParam["financialActivityLog_finTxnAmount"] = CRMFinancialActivityLogAddRequest.finTxnAmount;
	inputParam["financialActivityLog_finTxnFee"] = CRMFinancialActivityLogAddRequest.finTxnFee;
	//inputParam["financialActivityLog_finTxnBalance"] = CRMFinancialActivityLogAddRequest.finTxnBalance;
	inputParam["financialActivityLog_finTxnMemo"] = CRMFinancialActivityLogAddRequest.finTxnMemo;
	inputParam["financialActivityLog_billerRef1"] = CRMFinancialActivityLogAddRequest.billerRef1;
	inputParam["financialActivityLog_billerRef2"] = CRMFinancialActivityLogAddRequest.billerRef2;
	inputParam["financialActivityLog_noteToRecipient"] = CRMFinancialActivityLogAddRequest.noteToRecipient;
	inputParam["financialActivityLog_recipientMobile"] = CRMFinancialActivityLogAddRequest.recipientMobile;
	inputParam["financialActivityLog_recipientEmail"] = CRMFinancialActivityLogAddRequest.recipientEmail;
	inputParam["financialActivityLog_finTxnStatus"] = CRMFinancialActivityLogAddRequest.finTxnStatus;
	inputParam["financialActivityLog_smartFlag"] = CRMFinancialActivityLogAddRequest.smartFlag;
	inputParam["financialActivityLog_clearingStatus"] = CRMFinancialActivityLogAddRequest.clearingStatus;
	inputParam["financialActivityLog_finSchduleRefId"] = CRMFinancialActivityLogAddRequest.finSchduleRefId;
	inputParam["financialActivityLog_billerCommCode"] = CRMFinancialActivityLogAddRequest.billerCommCode;
	inputParam["financialActivityLog_fromAcctName"] = CRMFinancialActivityLogAddRequest.fromAcctName;
	inputParam["financialActivityLog_fromAcctNickname"] = CRMFinancialActivityLogAddRequest.fromAcctNickname;
	inputParam["financialActivityLog_toAcctName"] = CRMFinancialActivityLogAddRequest.toAcctName;
	inputParam["financialActivityLog_toAcctNickname"] = CRMFinancialActivityLogAddRequest.toAcctNickname;
	inputParam["financialActivityLog_billerCustomerName"] = CRMFinancialActivityLogAddRequest.billerCustomerName;
	inputParam["financialActivityLog_billerBalance"] = CRMFinancialActivityLogAddRequest.billerBalance;
	inputParam["financialActivityLog_activityRefId"] = CRMFinancialActivityLogAddRequest.activityRefId;
	inputParam["financialActivityLog_eventId"] = CRMFinancialActivityLogAddRequest.eventId;
	inputParam["financialActivityLog_errorCd"] = CRMFinancialActivityLogAddRequest.errorCd;
	inputParam["financialActivityLog_txnType"] = CRMFinancialActivityLogAddRequest.txnType;
	inputParam["financialActivityLog_tdInterestAmount"] = CRMFinancialActivityLogAddRequest.tdInterestAmount;
	inputParam["financialActivityLog_tdTaxAmount"] = CRMFinancialActivityLogAddRequest.tdTaxAmount;
	inputParam["financialActivityLog_tdPenaltyAmount"] = CRMFinancialActivityLogAddRequest.tdPenaltyAmount;
	inputParam["financialActivityLog_tdNetAmount"] = CRMFinancialActivityLogAddRequest.tdNetAmount;
	inputParam["financialActivityLog_tdMaturityDate"] = CRMFinancialActivityLogAddRequest.tdMaturityDate;
	inputParam["financialActivityLog_remainingFreeTxn"] = CRMFinancialActivityLogAddRequest.remainingFreeTxn;
	inputParam["financialActivityLog_toAccountBalance"] = CRMFinancialActivityLogAddRequest.toAccountBalance;
	inputParam["financialActivityLog_sendToSavePoint"] = CRMFinancialActivityLogAddRequest.sendToSavePoint;
	//inputParam["financialActivityLog_openTdTerm"] = CRMFinancialActivityLogAddRequest.openTdTerm;
	inputParam["financialActivityLog_openTdInterestRate"] = CRMFinancialActivityLogAddRequest.openTdInterestRate;
	//inputParam["financialActivityLog_openTdMaturityDate"] = CRMFinancialActivityLogAddRequest.openTdMaturityDate;
	inputParam["financialActivityLog_affiliatedAcctNickname"] = CRMFinancialActivityLogAddRequest.affiliatedAcctNickname;
	inputParam["financialActivityLog_affiliatedAcctName"] = CRMFinancialActivityLogAddRequest.affiliatedAcctName;
	inputParam["financialActivityLog_affiliatedAcctId"] = CRMFinancialActivityLogAddRequest.affiliatedAcctId;
	inputParam["financialActivityLog_beneficialFirstname"] = CRMFinancialActivityLogAddRequest.beneficialFirstname;
	inputParam["financialActivityLog_beneficialLastname"] = CRMFinancialActivityLogAddRequest.beneficialLastname;
	inputParam["financialActivityLog_relationship"] = CRMFinancialActivityLogAddRequest.relationship;
	inputParam["financialActivityLog_percentage"] = CRMFinancialActivityLogAddRequest.percentage;
	inputParam["financialActivityLog_finFlexValues1"] = CRMFinancialActivityLogAddRequest.finFlexValues1;
	inputParam["financialActivityLog_finFlexValues2"] = CRMFinancialActivityLogAddRequest.finFlexValues2;
	inputParam["financialActivityLog_finFlexValues3"] = CRMFinancialActivityLogAddRequest.finFlexValues3;
	inputParam["financialActivityLog_finFlexValues4"] = CRMFinancialActivityLogAddRequest.finFlexValues4;
	inputParam["financialActivityLog_finFlexValues5"] = CRMFinancialActivityLogAddRequest.finFlexValues5;
	inputParam["financialActivityLog_dueDate"] = CRMFinancialActivityLogAddRequest.dueDate;
	inputParam["financialActivityLog_beepAndBillTxnId"] = CRMFinancialActivityLogAddRequest.beepAndBillTxnId;
	inputParam["financialActivityLog_openProdCode"] = CRMFinancialActivityLogAddRequest.openProdCode;
	
}
