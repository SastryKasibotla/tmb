var startDateIBR="";
var endDateIBR="";
var returnChequegblNameTh = "";
var returnChequegblNameEn= "";
var returnChequegblProdNameTh = "";
var returnChequegblProdNameEn= "";
function customWidgetSelectEventforReturnCheque	(){
   //   alert("customWidgetSelectEventforReturnCheque")
	  glb_selectedData=frmIBChequeServiceReturnedChequeLanding.customFromAccountIB.data[gblCWSelectedItem]
	  
	 frmIBChequeServiceReturnedChequeLanding.lblFromAccount.text=glb_selectedData.lblCustName;
	  frmIBChequeServiceReturnedChequeLanding.lblAccNo.text=glb_selectedData.lblActNoval;
	  frmIBChequeServiceReturnedChequeLanding.imageAcc.src=glb_selectedData.img1;
	  gblOnClickCoverflow="RETURNCHEQUE";
	returnChequegblNameTh= glb_selectedData.lblCustNameTh;
	returnChequegblNameEn = glb_selectedData.lblCustNameEn;
	returnChequegblProdNameTh = glb_selectedData.lblProdTh;
    returnChequegblProdNameEn = glb_selectedData.lblProdEn;
	
	}	

function viewReturnedChequeService() {
	if(gblOnClickCoverflow=="RETURNCHEQUE")
	
	{
	gblStmntSessionFlag = "2";
	totalGridData = [];
	disablePagenumbers();
	appendDATADG = "NO";
	//endDateIB = getTodaysDateStmt();
	//startDateIB = dateFormatChange(new Date(new Date().getFullYear(),new Date().getMonth(),1));
	//	
 	
	frmIBChequeServiceViewReturnedCheque.imgacnt.src=frmIBChequeServiceReturnedChequeLanding.imageAcc.src;
	frmIBChequeServiceViewReturnedCheque.lblname.text=frmIBChequeServiceReturnedChequeLanding.lblFromAccount.text;
	frmIBChequeServiceViewReturnedCheque.lblamt.text=glb_selectedData.lblBalance;
	frmIBChequeServiceViewReturnedCheque.lblAccountNo.text=glb_selectedData.lblActNoval;
	frmIBChequeServiceViewReturnedCheque.lblAccType.text=glb_selectedData.lblProductVal;
	frmIBChequeServiceViewReturnedCheque.show(); 
	}
	else 
	{
	showAlert(kony.i18n.getLocalizedString("keySelectFromAcc"), kony.i18n.getLocalizedString("info"));
	//alert("Please select an account");
	}
}
function showReturnChequedata(){
    frmIBChequeServiceViewReturnedCheque.dgfullstmt.removeAll();
    getMonthCycleIB();
	currentpageStmt = 1;
	nopagesStmt=0
	setSortBtnSkinFromReturnCheque();
	disablePagenumbers();
	callReturnChequeService(startDateIBR,endDateIBR);	
}
function onSubmitbtnClickFromReturnCheque() {
	//var startdate = frmIBChequeServiceViewReturnedCheque.dateslider.selectedDate1;
	var startdate = gblTxnHistDate1;
	//var startDateIB = dateFormatChange(startdate);
	var startDateIB = dateFormatChange(startdate);
	//var endDateIB = dateFormatChange(frmIBChequeServiceViewReturnedCheque.dateslider.selectedDate2);
	var endDateIB = dateFormatChange(gblTxnHistDate2);
			
	totalGridData = [];
 	currentpageStmt = 1;
 	setSortBtnSkinFromReturnCheque();
 	disablePagenumbers();
 	callReturnChequeService(startDateIB,endDateIB);				
}
function setSortBtnSkinFromReturnCheque() {
	sortOrderStmt = "descending";	
	appendDATADG = "NO";
    var sortBtnSkin = frmIBChequeServiceViewReturnedCheque.dgfullstmt.columnHeadersConfig;		
	sortBtnSkin[0].columnheadertemplate.data.btnsort.skin = "btnsortby";
	sortBtnSkin[0].columnheadertemplate.data.btnsort.focusSkin = "btnsortby";
	
}
function appendDataInReturnCheque(tempData) {
	
	
	if(sortOrderStmt == "descending")
	//tempData = sortDateDescend(tempData,"column1", "lbltransid" ,"seqid");
	tempData = sortDateDescendCheque(tempData,"column1", "lbltransid" ,"column2" , "lbltransid");
	else if(sortOrderStmt == "ascending")
	//tempData = sortDateAscend(tempData,"column1", "lbltransid","seqid");
	tempData = sortDateAscendCheque(tempData,"column1", "lbltransid","column2" , "lbltransid");
	
	
	var datalength = totalGridData.length;
	
	var tempDatalength = datalength + tempData.length;
	
	
	
	if (appendDATADG == "YES") {
 		for (i = 0; i < tempData.length; i++) {
 			totalGridData[datalength] = tempData[i];
 			datalength++;
 		}
 		var seglength1 = totalGridData.length;
 		nopagesStmt = Math.ceil(seglength1 / recordcountStmt);
 	} else if (appendDATADG == "NO") {
 			totalGridData = tempData;
 			loadPagenumbersInReturnCheque();
 			currentpageStmt = 1;
 		
 		
 		if (bindataIB != "" || bindataIB != null)
 			appendDATADG = "YES";
 		if (bindataIB == "" || bindataIB == null)
 			appendDATADG = "NO";
 	}
	loadsegdataInReturnCheque(currentpageStmt);	
}
function loadPagenumbersInReturnCheque() {
	frmIBChequeServiceViewReturnedCheque.link1.text="";
	frmIBChequeServiceViewReturnedCheque.link2.text="";
	frmIBChequeServiceViewReturnedCheque.link3.text="";
	frmIBChequeServiceViewReturnedCheque.link4.text="";
	frmIBChequeServiceViewReturnedCheque.link5.text="";
	var seglength= totalGridData.length;
    nopagesStmt =Math.ceil(seglength/recordcountStmt);
    var pageshown = currentpageStmt;
    invokeCommonIBLogger("page nos  " + nopagesStmt);
    if(nopagesStmt == 1)
    	frmIBChequeServiceViewReturnedCheque.link1.text="";
    else{
		if(currentpageStmt <= nopagesStmt){
			frmIBChequeServiceViewReturnedCheque.link1.setVisibility(true);
			frmIBChequeServiceViewReturnedCheque.link1.text = currentpageStmt;
			currentpageStmt = currentpageStmt+1;
		}
		if(currentpageStmt <= nopagesStmt){
			frmIBChequeServiceViewReturnedCheque.link2.setVisibility(true);
			frmIBChequeServiceViewReturnedCheque.link2.text = currentpageStmt;
			currentpageStmt = currentpageStmt+1;
		}
		if(currentpageStmt <= nopagesStmt){
			frmIBChequeServiceViewReturnedCheque.link3.setVisibility(true);
			frmIBChequeServiceViewReturnedCheque.link3.text = currentpageStmt;
			currentpageStmt = currentpageStmt+1;
		}
		if(currentpageStmt <= nopagesStmt){
			frmIBChequeServiceViewReturnedCheque.link4.setVisibility(true);
			frmIBChequeServiceViewReturnedCheque.link4.text = currentpageStmt;
			currentpageStmt = currentpageStmt+1;
		}
		if(currentpageStmt <= nopagesStmt){
			frmIBChequeServiceViewReturnedCheque.link5.setVisibility(true);
			frmIBChequeServiceViewReturnedCheque.link5.text = currentpageStmt;
		}
	}
	
}
function loadsegdataInReturnCheque(page) {

	if(page == 1){
		frmIBChequeServiceViewReturnedCheque.link1.skin = lnkNormal;
		frmIBChequeServiceViewReturnedCheque.link2.skin = lnkIB18pxBlueUnderline;
		frmIBChequeServiceViewReturnedCheque.link3.skin = lnkIB18pxBlueUnderline;
		frmIBChequeServiceViewReturnedCheque.link4.skin = lnkIB18pxBlueUnderline;
		frmIBChequeServiceViewReturnedCheque.link5.skin = lnkIB18pxBlueUnderline;
	}
	invokeCommonIBLogger("page " + page);
	currentpageStmt = page;
	if(parseInt(frmIBChequeServiceViewReturnedCheque.link5.text) < nopagesStmt)
	{
		frmIBChequeServiceViewReturnedCheque.linknext.setVisibility(true);
	}
	else if((parseInt(frmIBChequeServiceViewReturnedCheque.link5.text) >= nopagesStmt) && (bindataIB == "" || bindataIB == null)) {
		frmIBChequeServiceViewReturnedCheque.linknext.setVisibility(false);
	}
	else if((parseInt(frmIBChequeServiceViewReturnedCheque.link5.text) >= nopagesStmt) && (bindataIB != "" || bindataIB != null)){
		frmIBChequeServiceViewReturnedCheque.linknext.setVisibility(true);
	}
	if(parseInt(frmIBChequeServiceViewReturnedCheque.link1.text) > 1){
		frmIBChequeServiceViewReturnedCheque.linkprev.setVisibility(true);
	}
	else{
		frmIBChequeServiceViewReturnedCheque.linkprev.setVisibility(false);
	}

    var countdata =[];
    
    var dataindex = parseInt(page)*recordcountStmt - recordcountStmt; 
    
    
    
	for(i=0;i<recordcountStmt;i++)
	{	if(dataindex < totalGridData.length)
		countdata[i]=totalGridData[dataindex];
		dataindex++;
	}
	    
		frmIBChequeServiceViewReturnedCheque.dgfullstmt.setData(countdata);
	
	
}
function nextPageloadInReturnCheque(nextpage) {
    currentpageStmt = parseInt(nextpage) + 1;
 	var dataindex = parseInt(currentpageStmt) * recordcountStmt - recordcountStmt;
 	var dgdatalength = totalGridData.length - dataindex;
 	
 	if ((currentpageStmt > nopagesStmt) && (bindataIB != "" || bindataIB != null)) {
 		
 		
 		//currentpageStmt = currentpageStmt-4;
 		appendDATADG = "YES";
 		callReturnChequeService(startDateIBR,endDateIBR);
 		return;
 	}
    appendDATADG = "YES";
	var pageshown = currentpageStmt;
	frmIBChequeServiceViewReturnedCheque.link5.skin = lnkNormal;
	frmIBChequeServiceViewReturnedCheque.link2.skin = lnkIB18pxBlueUnderline;
	frmIBChequeServiceViewReturnedCheque.link3.skin = lnkIB18pxBlueUnderline;
	frmIBChequeServiceViewReturnedCheque.link4.skin = lnkIB18pxBlueUnderline;
	frmIBChequeServiceViewReturnedCheque.link1.skin = lnkIB18pxBlueUnderline;
	frmIBChequeServiceViewReturnedCheque.link1.text = currentpageStmt-4;
	frmIBChequeServiceViewReturnedCheque.link2.text = currentpageStmt-3;
	frmIBChequeServiceViewReturnedCheque.link3.text = currentpageStmt-2;
	frmIBChequeServiceViewReturnedCheque.link4.text = currentpageStmt-1;
	frmIBChequeServiceViewReturnedCheque.link5.text = currentpageStmt;
    loadsegdataInReturnCheque(pageshown);
}
function prevPageloadInReturnCheque(prevpage) {
	currentpageStmt = parseInt(prevpage)-1;
	var pageshown = currentpageStmt;
	frmIBChequeServiceViewReturnedCheque.link1.text = currentpageStmt;
	frmIBChequeServiceViewReturnedCheque.link2.text = currentpageStmt+1;
	frmIBChequeServiceViewReturnedCheque.link3.text = currentpageStmt+2;
	frmIBChequeServiceViewReturnedCheque.link4.text = currentpageStmt+3;
	frmIBChequeServiceViewReturnedCheque.link5.text = currentpageStmt+4;
	frmIBChequeServiceViewReturnedCheque.link1.skin = lnkNormal;
	frmIBChequeServiceViewReturnedCheque.link2.skin = lnkIB18pxBlueUnderline;
	frmIBChequeServiceViewReturnedCheque.link3.skin = lnkIB18pxBlueUnderline;
	frmIBChequeServiceViewReturnedCheque.link4.skin = lnkIB18pxBlueUnderline;
	frmIBChequeServiceViewReturnedCheque.link5.skin = lnkIB18pxBlueUnderline;
   	loadsegdataInReturnCheque(pageshown);
}
function sortDgDataForReturnCheque(columnid,columnsortby){

	var filterdata = totalGridData;		
	var sortBtnSkin = frmIBChequeServiceViewReturnedCheque.dgfullstmt.columnHeadersConfig;
	if(sortOrderStmt == "descending"){
	
		sortBtnSkin[0].columnheadertemplate.data.btnsort.skin = "btnSortUpIB";
		sortBtnSkin[0].columnheadertemplate.data.btnsort.focusSkin = "btnSortUpIB";
		var sorteddata = sortDateAscend(filterdata,columnid,columnsortby);
		sortOrderStmt = "acsending";
 		totalGridData = sorteddata;
 		
 	}
 	else if(sortOrderStmt == "acsending"){
 	
		sortBtnSkin[0].columnheadertemplate.data.btnsort.skin = "btnsortby";
		sortBtnSkin[0].columnheadertemplate.data.btnsort.focusSkin = "btnsortby";
		var sorteddata = sortDateDescend(filterdata,columnid,columnsortby);
		sortOrderStmt = "descending";
 		totalGridData = sorteddata;
 		
	}
	
	loadsegdataInReturnCheque(currentpageStmt);
	
}      
