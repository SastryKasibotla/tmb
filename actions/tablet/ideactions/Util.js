var gblPOWcustNME = null;
var gblPOWtransXN = null;
var gblPOWchannel = null;
var gblPOWstateTrack = false;
var gblDeviceInfo = null;
var KonyDMObject = null;
var imageDataEN=[];
var imageDataTH=[];
var bannerDataEN=[];
var bannerDataTH=[];
var bannerFlag="";
var androidFileType="png";
mfClient=null;
bbServiceObj=null;


/*
************************************************************************
            Name    : hideUsername()
            Author  : Shubhanshu Yadav
            Date    : January 02, 2013
            Purpose : Formats the username to show only the last 4 digits.
        Input params: accoutNumber
       Output params: na
        ServiceCalls: nil
        Called From : adddatainseg() function
************************************************************************
 */


/*
 ***********************************************************************
 * Name : getHeader
 * Author : Nishant Kumar
 * Date : April 2 2013
 * Purpose : Generate custom header from template header "hdrCommon"
 * Params: menuCallBack,lbltext,btnSkin,btnFocusSkin, btnCallBack (if not required pass 0)
 * ServiceCalls: nil
 * Called From : preShow of each form
 **********************************************************************
 */

function getHeader(menuCallBack, lbltext, btnSkin, btnFocusSkin, btnCallBack) {
	//Individual widgets can be accessed from template header "hdrCommon"
	if (menuCallBack != 0)
		hbxHdrCommon.btnHdrMenu.onClick = menuCallBack;
	//Reset label width,skin and alignment
	hbxHdrCommon.lblHdrTxt.skin = "lblHeader";
	hbxHdrCommon.lblHdrTxt.containerWeight = 70;
	//hbxHdrCommon.lblHdrTxt.margin = [0,3,0,0];
	//hbxHdrCommon.lblHdrTxt.contentAlignment = constants.CONTENT_ALIGN_CENTER;
	if (lbltext != 0)
		hbxHdrCommon.lblHdrTxt.text = lbltext;
	else
		hbxHdrCommon.lblHdrTxt.text = " ";
	if (btnSkin != 0 && btnCallBack != 0) {
		hbxHdrCommon.btnRight.isVisible = true;
		hbxHdrCommon.btnRight.skin = btnSkin;
		hbxHdrCommon.btnRight.focusSkin = btnFocusSkin;
		hbxHdrCommon.btnRight.onClick = btnCallBack;
	} else {
		hbxHdrCommon.btnRight.isVisible = false;
		//hbxHdrCommon.lblHdrTxt.containerWeight = 75;
	}
	// Default arrow position is middle
	// If you don't want arrowtop, make the src as "empty.png"
	// If you are customizing images in "hbxArrow" that line or method must be placed/called after getHeader() 
	hbxArrow.imgHeaderMiddle.src = "arrowtop.png";
	hbxArrow.imgHeaderRight.src = "empty.png";
}

function getServerImagePath(img){
	var randomnum = Math.floor((Math.random() * 10000) + 1);
		var imgStr = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext 
									+ "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+img
									+"&modIdentifier=PRODUCTPACKAGEIMG&dummy=" + randomnum;

return 	imgStr;										
}


/**
 * description
 * @returns {}
 */
/*
function invokeServiceSecureAsync(serviceID, inputParam, callBackFunction) {
	if (appConfig.secureServerPort != null) {
		//var url = "http://" + appConfig.serverIp + ":9080" + "/" + appConfig.middlewareContext + "/MWServlet";
		var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/MWServlet";
	} else {
		var url = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/MWServlet";
	}
	var sessionIdKey = "cacheid";
	inputParam.appID = appConfig.appId;
	inputParam.appver = appConfig.appVersion;
	inputParam["serviceID"] = serviceID;
	if (gblDeviceInfo.length == 0) {
		gblDeviceInfo = kony.os.deviceInfo();
	}
	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android") {
		inputParam["channel"] = "rc";
	} else {
		inputParam["channel"] = "wap";
	}
	inputParam["platform"] = gblDeviceInfo.name;
	inputParam[sessionIdKey] = sessionID;
	if (globalhttpheaders) {
		if (inputParam["httpheaders"]) {
			inputParam.httpheaders = mergeHeaders(inputParam.httpheaders, globalhttpheaders);
		} else {
			inputParam["httpheaders"] = globalhttpheaders;
		};
	};
	var connHandle = kony.net.invokeServiceAsync(url, inputParam, callBackFunction, null, DEFAULT_SERVICE_TIMEOUT);
	return connHandle;
}*/
/*
************************************************************************
            Name    : change i18n
            Author  : Gaurav Mandal	
            Date    : April 09, 2013
            Purpose : Changes the locale of the app
        Input params: na
       Output params: na
        ServiceCalls: nil
        Called From : language toggle buttons
************************************************************************
 */
// Sets app locale based on device locale at the startup

function startupLocale() {
	//#ifdef android
		var getcurAppLocale = kony.store.getItem("curAppLocale");
		if (getcurAppLocale != null) {
			kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccess(), onfailure(), "");
		} else {
			if (kony.i18n.getCurrentDeviceLocale() == "th_TH") {
			kony.i18n.setCurrentLocaleAsync("th_TH", onsuccess(), onfailure(), "");
			}
		}
	
	//#endif
}

function onsuccess() {
	
}

function onfailure() {
	
}
//To change the locale to THAI from button inside forms.

function setLocaleTH() {
	if(!flowSpa){
    if (kony.i18n.isLocaleSupportedByDevice("th_TH")) {
        kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallback, onfailurecallback, "")
    } else {
        //just for testin purposes in emulator
        //kony.i18n.setCurrentLocaleAsync("ru_RU", onsuccesscallback, onfailurecallback, "");
    }
	} else {
	 kony.i18n.setCurrentLocaleAsync("th_TH", onsuccesscallback, onfailurecallback, "")
	}
}
//To change the locale to ENGLISH from button inside forms.

function setLocaleEng() {
	//kony.application.showLoadingScreen("localeBlock","", "centre", true, true, false);
	kony.i18n.setCurrentLocaleAsync("en_US", onsuccesscallback, onfailurecallback, "");
}

function onsuccesscallback() {
	
	var currForm = kony.application.getCurrentForm();
	//currForm.destroy();
	//deviceInfo = kony.os.deviceInfo();
	var type = gblDeviceInfo["type"];
	var category = gblDeviceInfo["category"];
	if (type == "spa") {
		if(currForm!=frmSPALogin)
		{
		frmBlank.show()
		}
	}
	
	if(flowSpa)
	{
		if(currForm!=frmSPALogin && currForm!=frmFATCAQuestionnaire1)
		{
			currForm.show();
		}
		else if(currForm==frmFATCAQuestionnaire1)
		{
			setQuestionLocaleFATCAMB();
			currForm.show();
		}
	}
	else
	{
		if(currForm!=frmMBPreLoginAccessesPin && currForm!=frmFATCAQuestionnaire1)
		{
			currForm.show();
		}
		else if(currForm==frmFATCAQuestionnaire1)
		{
			setQuestionLocaleFATCAMB();
			currForm.show();
		}
	}
	kony.application.dismissLoadingScreen();
	if (type == "spa" || category == "android") {
		if (gblLang_flag == "en_US") {
			//alert("ENG");
			//kony.application.getCurrentForm().lblLocale.text = "en_US";
			frmMBTnC.btnEngR.skin = "btnOnFocus";
			frmMBTnC.btnThaiR.skin = "btnOffFocus";
		}
		if (gblLang_flag == "th_TH") {
			//alert("THAI");
			//kony.application.getCurrentForm().lblLocale.text = "th_TH";
			frmMBTnC.btnEngR.skin = "btnOnNormal";
			frmMBTnC.btnThaiR.skin = "btnOffNorm";
		}
	}
}

function onfailurecallback() {
	
	return 0;
}


function preShow(){
  if(kony.os.deviceInfo().name == "android"){
   // frmDreamCalculator.scrollbox156816275924507.containerHeight = 100 ;//60
   // frmDreamCalculator.scrollbox156816275924507.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
    frmDreamCalculator.hbox86682510926373.containerHeight = 80;
    frmDreamCalculator.scrollbox156816275924507.containerHeightReference = constants.HEIGHT_BY_DEVICE_REFERENCE;
    frmDreamCalculator.bounces = false;
    frmDreamCalculator.scrollbox156816275924507.bounces=false;
  }
}



/**
 * description
 * @returns {}
 */

function registerForTimeOut() {
	idleTimeOut = 5;
	//alert("In registerforTimeout");
	if (GLOBAL_KONY_IDLE_TIMEOUT != null && GLOBAL_KONY_IDLE_TIMEOUT != undefined) {
		
		idleTimeOut = kony.os.toNumber(GLOBAL_KONY_IDLE_TIMEOUT);
	}
	
	
	try{
	kony.application.unregisterForIdleTimeout();
	}
	catch(e){
	}
	try{
		kony.timer.cancel("idletimeoutcheck");
	}
	catch(e){
	}
	kony.timer.schedule("idletimeoutcheck", invokeidletimeout, 1, false)
	//alert(idleTimeOut);
	

}
function invokeidletimeout(){
	
	kony.application.registerForIdleTimeout(idleTimeOut, onIdleTimeOutCallBack);
	
}
/**
 * On idle time out, this function will be called.if activated user navigates to login screen else to frmBanking screen
 * @returns {}
 */

function onIdleTimeOutCallBack() {
	try{
					
					//alert("in idle timeout")
					isSignedUser = false;
					GBL_Time_Out = "true";
					var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
					disMissAllPopUps();
					if (getEncrKeyFromDevice != null) {
						/** calling this to remove the userid created while activation from ECAS*/
						//if(kony.application.getCurrentForm().id == "frmMBsetPasswd" ){
						//	invokeDeleteUserServiceTimeOut();	
						//}	
						if (flowSpa) {
							if(kony.application.getCurrentForm().id == "frmMBSetuseridSPA" ){
								invokeDeleteUserServiceTimeOut();	
							}
							isMenuShown = false;
							frmSPALogin.show();
						} else {
						
						      if (GBL_Time_Out == "true") {
				                 // frmMBPreLoginAccessesPin.vboxLeft.containerWeight = frmMBPreLoginAccessesPin.vboxLeft.containerWeight - 10;
				                  //frmMBPreLoginAccessesPin.vboxRight.containerWeight = frmMBPreLoginAccessesPin.vboxRight.containerWeight + 10;
			                          }
		
			gblTouchShow = false;
			

			if (GBL_Time_Out == "true") {
				//frmMBPreLoginAccessesPin.vboxLeft.containerWeight = frmMBPreLoginAccessesPin.vboxLeft.containerWeight + 10;
				 //frmMBPreLoginAccessesPin.vboxRight.containerWeight = frmMBPreLoginAccessesPin.vboxRight.containerWeight - 10;
			}
		

		clearGlobalVariables();
		spaFormGlobalsCall();
		cleanPreorPostForms();
		kony.application.dismissLoadingScreen();
						
						
							//frmAfterLogoutMB.show();
							//clearGlobalVariables();
							//spaFormGlobalsCall();
							//cleanPreorPostForms();
						}
						//campaignBannerCount();
						invokeLogoutService();
						//invokeCRMProfileUpdateTimeOutLogout();
					} else {
						if (flowSpa) {
							//campaignBannerCount();
							invokeLogoutService();
							//invokeLogoutService();
							//frmSPALogin.show();
						} else {
							popupTractPwd.dismiss();//DEF14068 Fix
							ActivationMBViaIBLogoutService();	// Fix for DEF1052
							//frmMBanking.show();
						}
					}
	}
	catch(e){
		alert("wrong in trigger")
	}
}

function invokeDeleteUserServiceTimeOut(){
	
	inputParam = {};
	inputParam["segmentId"] = "MIB";
	
	invokeServiceSecureAsync("deleteUser", inputParam, deleteUserFromECASCallBack);
}
/*
************************************************************************
            Name    : callDummy 
            Author  : Gayathri
            Date    : April 16, 2013
            Purpose : On click of form back button
        Input params: na
       Output params: na
        ServiceCalls: nil
        Called From : AccountSummaryLanding.
************************************************************************
 */

function callDummy() {}
/*
************************************************************************
            Name    : disableBackButton 
            Author  : Gaurav Mandal
            Date    : April 20, 2013
            Purpose : On click of form back button
        Input params: na
       Output params: na
        ServiceCalls: nil
        Called From : All forms onDeviceBack property
************************************************************************
 */
function closeApplicationBackBtn(){
	try{
		 kony.application.exit();
      }
   	catch(Error)
      {
         alert("Exception While getting exiting the application  : "+Error);
      }
}


function showToastMsg(){
	//Creates an object of class 'ToastMessager'
	var ToastMessagerObject = new Toast.ToastMessager();
	//Invokes method 'showToastMessage' on the object

	ToastMessagerObject.showToastMessage(kony.i18n.getLocalizedString("MIB_BackbuttonMSG"), 1);
}

function disableBackButton() {}
gblApplicationLaunch = false;
function showStartUp() {
	
	//deviceInfo = kony.os.deviceInfo();
	if(gblDeviceInfo.name == "android"){
	     kony.application.setApplicationProperties({
			    "statusBarColor": "007ABC",
			    "statusBarForegroundColor": "007ABC"
			});	
	}
	if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
		
		startup = frmSPALogin
		flowSpa = true;
		isIphone6=false;
	} else {
		flowSpa = false;
		//Code fix for defect Ticket51503. Earlier we were saving accessPIN in deviceDB during login. 
		//So we are making that to empty and removing that from DeviceDB
		kony.store.setItem("userPIN", "");
		try {
  			kony.store.removeItem("userPIN");
		}
		catch(err) {
		}
		
		
		var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
		if (getEncrKeyFromDevice != null) {
		   // if(gblDeviceInfo["name"] == "iPhone" && onClickCheckTouchIdAvailable()){
//				//showTouchIDPopup();
//				startup = frmMBPreLoginAccessesPin  // frmAfterLogoutMB
//			}else{
//				startup = frmMBPreLoginAccessesPin // frmAfterLogoutMB
//			}
		    startup = frmMBPreLoginAccessesPin;
			gblCurrentForm = "frmMBPreLoginAccessesPin";
		} else {
			if(kony.string.equalsIgnoreCase("iphone", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPad", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPod touch", gblDeviceInfo.name)){
			kony.application.setApplicationBadgeValue("");
			}
			startup = frmMBanking;
			gblCurrentForm = "frmMBanking";
		}
		if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "iPhone Simulator"){
		//Creates an object of class 'BarScan'(iPhone Barcode FFI)
			gblApplicationLaunch = true; // Use this variable in preshow access pin screen to show loading indicator.
			BarScanObject = new iphoneBarcode.BarScan();
		}
		
	}
	startupLocale()
	initFATCAGlobalVars();
	isSignedUser = false;
	return startup;
}

/**
 * description
 * @returns {}
 */
 
 function onClickDownloadPDFCommon(fullUrl){
 
 			if(gblDeviceInfo.name == "android"){
			if(KonyDMObject == null)
	 			KonyDMObject = new KonyandroidUtils.KonyDM();
				showLoadingScreen();
	 			KonyDMObject.downloadFile(
					/**String*/ fullUrl, 
					/**Function*/ callbackDownload);		
			}else{
			
			if(flowSpa && (gblDeviceInfo.category == "android"))
			{
			
			try {
		            var cwForm = document.createElement("form");
		        	cwForm.setAttribute('method',"post");
		        	cwForm.setAttribute('action',fullUrl);
		        	if(document.getElementsByTagName('body')!= null && document.getElementsByTagName('body')[0]!= null)
		        	{
		            	document.getElementsByTagName('body')[0].appendChild(cwForm);
		        	}
		        	kony.application.dismissLoadingScreen();            
		        	cwForm.submit();
	    		} catch(ex){
	       			
	    		}
			
			
			}else
			{
				kony.application.openURL(fullUrl);
			}
			}
 
 }

function onClickDownloadPDF() {
	gblLang_flag = kony.i18n.getCurrentLocale();
	androidFileType = "pdf";
	if (gblLang_flag == "en_US") {
		if (GLOBAL_PDF_DOWNLOAD_URL_en_US != null && GLOBAL_PDF_DOWNLOAD_URL_en_US.length > 0) {
			
			onClickDownloadPDFCommon(GLOBAL_PDF_DOWNLOAD_URL_en_US);
						
		} else {
			alert(kony.i18n.getLocalizedString("pdf_download_error"));
		}
	} else {
		if (GLOBAL_PDF_DOWNLOAD_URL_th_TH != null && GLOBAL_PDF_DOWNLOAD_URL_th_TH.length > 0) {
		
			onClickDownloadPDFCommon(GLOBAL_PDF_DOWNLOAD_URL_th_TH);
			
		} else {
			alert(kony.i18n.getLocalizedString("pdf_download_error"));
		}
	}
}

//e.g filetype=pdf or png filename=FirstTimeActivation lookup tmb_tnc.properties
function onClickDownloadTnC(filetype, filename) {
	gblLang_flag = kony.i18n.getCurrentLocale();
	androidFileType = filetype;
	//var pdfimageurl = "http://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/pdforimagerender";
	var pdfimageurl = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/pdforimagerender";
	filename = replaceAndSymbol(filename);//adding this condtion for DEF212
	var  uriquery = "?localID="+encodeURIComponent(gblLang_flag)+"&filetype="+encodeURIComponent(filetype)+"&filename="+encodeURIComponent(filename);
	onClickDownloadPDFCommon(pdfimageurl+uriquery);
	//alert(kony.i18n.getLocalizedString("pdf_download_error"));
}

/*replaceAndSymbol function will replace & symbol with And string
 * @param, fileName
 * @return replacedAndString
 */
function replaceAndSymbol(fileName){
 var newFileName = fileName;
 if(undefined != newFileName && null != newFileName){
    if(newFileName.indexOf("&") != -1){
      //newFileName = newFileName.replace("&","And")
      newFileName = newFileName.replace(/&/g,"And");
     }
 }
 return newFileName;
}


function onClickEmailTnC(tnckeyword) {
	//call notification add service for email with the tnc keyword
	//alert("tnckeyword::"+tnckeyword);
	var inputparam = {};
	inputparam["channelName"] = "Internet Banking";
	inputparam["channelID"] = "01";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
	if(tnckeyword == undefined || tnckeyword == "") {
		inputparam["moduleKey"] = "TermsAndConditions";
	} else if(gblActionCode == "12" ||gblActionCode == "13"){
	inputparam["moduleKey"] = "TermsAndConditionsReactivation";
	}else {
		inputparam["moduleKey"] = tnckeyword;
	}
	
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationAddService);
}


/*
************************************************************************
            Name    : showLoadingScreen 
            Author  : Developer
            Date    : May 3, 2013
            Purpose : On click of Next button or service call button
        	Input params: na
        	Output params: Blocks UI
        	ServiceCalls: nil
        	Called From : According need 
************************************************************************
 */
 
function showLoadingScreen() {

  if (gblDeviceInfo.name == "thinclient")
        if (document.getElementById("__loadingScreenDiv") != null)
            if (document.getElementById("__loadingScreenDiv").style['cssText'].length != 76) { //to prevent loading screen from being called again and again
                kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
            } else {
                kony.print("Loading indicator is already shown");
            } else
        kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);

    else if (gblDeviceInfo.name == "android") {
        kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
    } else
        kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {
            shouldShowLabelInBottom: "false",
            separatorHeight: 10
        });
}
/*
************************************************************************
            Name    : dismissLoadingScreen
            Author  : Developer
            Date    : May 3, 2013
            Purpose : to dismiss the loading screen before loading next screen
        	Input params: na
        	Output params: dismiss the loading screen
        	ServiceCalls: nil
        	Called From : According need 
************************************************************************
 */

function dismissLoadingScreen() {
	if(gblDeviceInfo["category"]=="IE" && gblDeviceInfo["version"]=="8"){
			popupIBLoading.dismiss();
	}else{
		kony.application.dismissLoadingScreen();
	}
}
/*
************************************************************************

	Module	: amountValidation
	Author  : Kony
	Purpose : Checking if the enter data contains only "0","1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."

************************************************************************
 */

function amountValidation(amount) {
	if (amount != null && amount != "") {
		if (kony.string.containsOnlyGivenChars(amount, ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ",", "."])) {
			amount = amount.replace(",", "");
			amount = amount.replace(".", "");
			//ADDED os.tonumber(amount)== nil TO CHECK IF THE USER ONLY ENTER "." or ","
			if (kony.os.toNumber(amount) <= 0 || kony.os.toNumber(amount) == null) {
				return false;
			} else
				return true;
		} else
			return false;
	} else
		return false;
}
/*
************************************************************************

	Module	: showAlert
	Author  : Kony
	Purpose : Common alert (AlertMessage,Alert type,Alert Title,Yes Label, No Label,Alert Handler)

****/

function showAlertForSplitTransfers(alertMsg, type, title, yesLbl, noLbl, callBackHandler) {
	var basicConf = {
		message: alertMsg,
		alertType: type,
		alertTitle: title,
		yesLabel: yesLbl,
		noLabel: noLbl,
		alertHandler: callBackHandler
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	kony.ui.Alert(basicConf, pspConf);
}
/*
************************************************************************

	Module	: commaFormatted
	Author  : Sujata
	Purpose : It will format amount with comma.

****/

function commaFormatted(amount) {
	
	var minus = '';
	
	if(amount == "" || amount == null || amount == undefined)
       return "";

	
	if(amount.indexOf(".")==0){
		amount="0"+amount;
	}
	amount = amount.concat(".00")
	
	if(parseFloat(amount) < 0) {
		minus = '-';
	}
	
	var delimiter = ","; // replace comma if desired
	amount = new String(amount);
	var a = amount.split('.', 2)
	var d = a[1];
	if(d.length==1){
		d=d+"0"
	}
	var i = parseInt(a[0]);
	if (isNaN(i)) {
		return '';
	}
	
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while (n.length > 3) {
		var nn = n.substr(n.length - 3);
		a.unshift(nn);
		n = n.substr(0, n.length - 3);
	}
	if (n.length > 0) {
		a.unshift(n);
	}
	n = a.join(delimiter);
	if (d.length < 1) {
		amount = n;
	} else {
		amount = n + '.' + d;
	}
	amount = minus + amount;
	return amount;
}

/*
************************************************************************
            Name    : removeMenu()
            Author  : Shubhanshu Yadav
            Date    : June 07, 2013
            Purpose : To remove the dynamic menu on hide of the form.
        Input params: Nil
       Output params: Nil
        ServiceCalls: nil
        Called From : onHide of the form
************************************************************************
 */

function removeMenu() {
	/*if(!flowSpa){
	var currentForm = kony.application.getCurrentForm();
	if ((isMenuRendered == true) || (isMenuShown == true)) {
		while (currentForm.vbox47407564342877.widgets()
			.length !== 0) {
			currentForm.vbox47407564342877.removeAt(0);
		}
		currentForm.vboxLeft.removeAt(0);
	}
	}*/
}
/*
************************************************************************
	Module	: financialActivityLogServiceCall
	Author  : Kony
	Purpose : Calling the financial activity log java service 
************************************************************************/

function financialActivityLogServiceCall(CRMFinancialActivityLogAddRequest) {
	inputParam = {};
	//inputParam["ipAddress"] = ""; 
	inputParam["crmId"] = CRMFinancialActivityLogAddRequest.crmId;
	inputParam["finTxnRefId"] = CRMFinancialActivityLogAddRequest.finTxnRefId;
	inputParam["finTxnDate"] = CRMFinancialActivityLogAddRequest.finTxnDate;
	inputParam["activityTypeId"] = CRMFinancialActivityLogAddRequest.activityTypeId;
	inputParam["txnCd"] = CRMFinancialActivityLogAddRequest.txnCd;
	inputParam["tellerId"] = CRMFinancialActivityLogAddRequest.tellerId;
	inputParam["txnDescription"] = CRMFinancialActivityLogAddRequest.txnDescription;
	inputParam["finLinkageId"] = CRMFinancialActivityLogAddRequest.finLinkageId;
	inputParam["channelId"] = CRMFinancialActivityLogAddRequest.channelId;
	inputParam["fromAcctId"] = CRMFinancialActivityLogAddRequest.fromAcctId;
	inputParam["toAcctId"] = CRMFinancialActivityLogAddRequest.toAcctId;
	inputParam["toBankAcctCd"] = CRMFinancialActivityLogAddRequest.toBankAcctCd;
	inputParam["finTxnAmount"] = CRMFinancialActivityLogAddRequest.finTxnAmount;
	inputParam["finTxnFee"] = CRMFinancialActivityLogAddRequest.finTxnFee;
	inputParam["finTxnBalance"] = CRMFinancialActivityLogAddRequest.finTxnBalance;
	inputParam["finTxnMemo"] = CRMFinancialActivityLogAddRequest.finTxnMemo;
	inputParam["billerRef1"] = CRMFinancialActivityLogAddRequest.billerRef1;
	inputParam["billerRef2"] = CRMFinancialActivityLogAddRequest.billerRef2;
	inputParam["noteToRecipient"] = CRMFinancialActivityLogAddRequest.noteToRecipient;
	inputParam["recipientMobile"] = CRMFinancialActivityLogAddRequest.recipientMobile;
	inputParam["recipientEmail"] = CRMFinancialActivityLogAddRequest.recipientEmail;
	inputParam["finTxnStatus"] = CRMFinancialActivityLogAddRequest.finTxnStatus;
	inputParam["smartFlag"] = CRMFinancialActivityLogAddRequest.smartFlag;
	inputParam["clearingStatus"] = CRMFinancialActivityLogAddRequest.clearingStatus;
	inputParam["finSchduleRefId"] = CRMFinancialActivityLogAddRequest.finSchduleRefId;
	inputParam["billerCommCode"] = CRMFinancialActivityLogAddRequest.billerCommCode;
	inputParam["fromAcctName"] = CRMFinancialActivityLogAddRequest.fromAcctName;
	inputParam["fromAcctNickname"] = CRMFinancialActivityLogAddRequest.fromAcctNickname;
	inputParam["toAcctName"] = CRMFinancialActivityLogAddRequest.toAcctName;
	inputParam["toAcctNickname"] = CRMFinancialActivityLogAddRequest.toAcctNickname;
	inputParam["billerCustomerName"] = CRMFinancialActivityLogAddRequest.billerCustomerName;
	inputParam["billerBalance"] = CRMFinancialActivityLogAddRequest.billerBalance;
	inputParam["activityRefId"] = CRMFinancialActivityLogAddRequest.activityRefId;
	inputParam["eventId"] = CRMFinancialActivityLogAddRequest.eventId;
	inputParam["errorCd"] = CRMFinancialActivityLogAddRequest.errorCd;
	inputParam["txnType"] = CRMFinancialActivityLogAddRequest.txnType;
	inputParam["tdInterestAmount"] = CRMFinancialActivityLogAddRequest.tdInterestAmount;
	inputParam["tdTaxAmount"] = CRMFinancialActivityLogAddRequest.tdTaxAmount;
	inputParam["tdPenaltyAmount"] = CRMFinancialActivityLogAddRequest.tdPenaltyAmount;
	inputParam["tdNetAmount"] = CRMFinancialActivityLogAddRequest.tdNetAmount;
	inputParam["tdMaturityDate"] = CRMFinancialActivityLogAddRequest.tdMaturityDate;
	inputParam["remainingFreeTxn"] = CRMFinancialActivityLogAddRequest.remainingFreeTxn;
	inputParam["toAccountBalance"] = CRMFinancialActivityLogAddRequest.toAccountBalance;
	inputParam["sendToSavePoint"] = CRMFinancialActivityLogAddRequest.sendToSavePoint;
	inputParam["openTdTerm"] = CRMFinancialActivityLogAddRequest.openTdTerm;
	inputParam["openTdInterestRate"] = CRMFinancialActivityLogAddRequest.openTdInterestRate;
	inputParam["openTdMaturityDate"] = CRMFinancialActivityLogAddRequest.openTdMaturityDate;
	inputParam["affiliatedAcctNickname"] = CRMFinancialActivityLogAddRequest.affiliatedAcctNickname;
	inputParam["affiliatedAcctId"] = CRMFinancialActivityLogAddRequest.affiliatedAcctId;
	inputParam["beneficialFirstname"] = CRMFinancialActivityLogAddRequest.beneficialFirstname;
	inputParam["beneficialLastname"] = CRMFinancialActivityLogAddRequest.beneficialLastname;
	inputParam["relationship"] = CRMFinancialActivityLogAddRequest.relationship;
	inputParam["percentage"] = CRMFinancialActivityLogAddRequest.percentage;
	inputParam["finFlexValues1"] = CRMFinancialActivityLogAddRequest.finFlexValues1;
	inputParam["finFlexValues2"] = CRMFinancialActivityLogAddRequest.finFlexValues2;
	inputParam["finFlexValues3"] = CRMFinancialActivityLogAddRequest.finFlexValues3;
	inputParam["finFlexValues4"] = CRMFinancialActivityLogAddRequest.finFlexValues4;
	inputParam["finFlexValues5"] = CRMFinancialActivityLogAddRequest.finFlexValues5;
	inputParam["dueDate"] = CRMFinancialActivityLogAddRequest.dueDate;
	inputParam["beepAndBillTxnId"] = CRMFinancialActivityLogAddRequest.beepAndBillTxnId;
	inputParam["openProdCode"] = CRMFinancialActivityLogAddRequest.openProdCode;
	invokeServiceSecureAsync("financialActivityLog", inputParam, callBackFinancialActivityLog)
}
/*
************************************************************************

	Module	: FianncialActivityLogServiceCall
	Author  : Kony
	Purpose : Calling the financial activity log java service

************************************************************************/

function callBackFinancialActivityLog(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			
		}
	}
}
/*
************************************************************************

	Module	: activityLogServiceCall
	Author  : Kony
	Purpose : Calling the activity log java service and sending the ipAddress,activityDate,appSessionID from preprocessor

************************************************************************/

function activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
	activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId) {
	
}

/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : sorting array based on given key
 * Input params: array, key
 * Output params: No
 * ServiceCalls: No
 * Called From : utility
 */

function sortByKey(array, key) {
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}

/**
 * description
 * @returns {}
 */

function setFocusOnTB() {
	switch (gblTxtFocusFlag) {
	case 1:
		frmMBsetPasswd.txtTempAccess.setFocus(true);
		gblTxtFocusFlag = 2;
		break;
	case 2:
		frmMBsetPasswd.txtAccessPwd.setFocus(true);
		gblTxtFocusFlag = 1;
		break;
	case 3:
		frmMBsetPasswd.txtTemp.setFocus(true);
		gblTxtFocusFlag = 4;
		break;
	case 4:
		frmMBsetPasswd.txtTransPass.setFocus(true);
		gblTxtFocusFlag = 3;
		break;
	}
}

function disMissAllPopUps() {
	AccntTransPwd.dismiss();
	BillerNotAvailable.dismiss();
	BillPayTopUpConf.dismiss();
	DeletePop.dismiss();
	helpMessage.dismiss();
	popAccessPinBubble.dismiss();
	popAccntConfirmation.dismiss();
	popAddrCmboBox.dismiss();
	popBankList.dismiss();
	popBPTransactionPwd.dismiss();
	popBubble.dismiss();
	popDelRecipient.dismiss();
	popDelRecipientProfile.dismiss();
	popDelTopUp.dismiss();
	popForgotPass.dismiss();
	popProfilePic.dismiss();
	popRecipientBankList.dismiss();
	popTransactionPwd.dismiss();
	popTransferConfirmOTPLock.dismiss();
	popTransfersTDAccount.dismiss();
	popupBubblePasswordRules.dismiss();
	popupBubbleUserId.dismiss();
	popupConfirmation.dismiss();
	popupConfrmYes.dismiss();
	popUpLogout.dismiss();
	popUpMyBillers.dismiss();
	popUpTermination.dismiss();
	popupTractPwd.dismiss();
	popupActivationHelp.dismiss();
	popAllowOrNot.dismiss();
	popATMBranch.dismiss();
	popDreamSaving.dismiss();
	popEditBillPayTransactionPwd.dismiss();
	popExgRateDisclaimer.dismiss();
	popFeedBack.dismiss();
	popHelpMessageTrans.dismiss();
	popInboxSortBy.dismiss();
	popInboxSortBy2.dismiss();
	popOtpSpa.dismiss();
	popPromotions.dismiss();
	popS2SAmntHelp.dismiss();
	popSelDate.dismiss();
	popUpCallCancel.dismiss();
	popupEditBPDele.dismiss();
	popupFTdel.dismiss();
	popUploadPic.dismiss();
	popUploadPicSpa.dismiss();
	popUpNotfnDelete.dismiss();
	//popUpPayBill.dismiss();
	popUpProvinceData.dismiss();
	popupSpaForgotPassword.dismiss();
	popUpTopUpAmount.dismiss();
	TAGCPServerDetailsPopup.dismiss();
	TMBCallPopup.dismiss();
	popUpTouchIdTransPwd.dismiss();
	popupAddToMyRecipient.dismiss();
    popupAddToMyBills.dismiss();
    popGeneralMsg.dismiss();
}


function showLoadingPopUpScreen() {
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
	//#ifdef android
		kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {
			transparencybehindloadingscreen: 20
		});
	//else if (kony.os.deviceInfo().name != "iPhone")
	//#else
		//#ifndef iphone
			kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
		//else
		//#else
		kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {
			shouldShowLabelInBottom: "false",
			separatorHeight: 10
		});
		//#endif
	//#endif
}
/*
************************************************************************
            Name    : emailProcessForTC()
            Author  : Dileep Somajohassula
            Date    : August 22, 2013
            Purpose : To trigger a mail when first time activations is done
        ServiceCalls: partyInquiry
        Called From : End of Device activation flow 
************************************************************************
 */

function emailProcessForTC() {
	
	
	if (gblEmailTCSelect && (gblActionCode != "22")){
		if (gblTCEmailTriggerFlag) {
			
			showLoadingScreen();
			var inputparam = {}
			
			invokeServiceSecureAsync("partyInquiry", inputparam, FstTmeActivatnPartySrviceCallback);
		}
	}
}

function FstTmeActivatnPartySrviceCallback(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			showLoadingScreen();
			var inputparam = {};		
			gblCustomerName = resulttable["customerName"];
			//gblEmailId = resulttable["emailId"];
			inputparam["channelName"] = "Mobile Banking";
			inputparam["channelID"] = "02";
			inputparam["notificationType"] = "Email"; // always email
			inputparam["phoneNumber"] = gblPHONENUMBER;
			inputparam["mail"] = gblEmailId;
			inputparam["customerName"] = gblCustomerName;
			inputparam["localeCd"] = kony.i18n.getCurrentLocale();
			//if(tnckeyword == undefined || tnckeyword == "") {
			if(gblActionCode == "12" ||gblActionCode == "13"){
			inputparam["moduleKey"] = "TermsAndConditionsReactivation";
			}else if(gblActionCode == "23"){
			inputparam["moduleKey"] = "TermsAndConditionsAddDevice";
			}
			else{
				inputparam["moduleKey"] = "TermsAndConditions";
			}
			
			invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationAddMBForMail)
		}
		dismissLoadingScreen();
	}
	dismissLoadingScreen();
}

function callBackNotificationAddMBForMail(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			
			dismissLoadingScreen();
		}
		dismissLoadingScreen();
	}
	dismissLoadingScreen();
}

function showLoadingScreenWithNoIndicator() {
	if (gblDeviceInfo.name == "thinclient"){
		kony.application.showLoadingScreen("frmLoadingNoIndicator", "", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, false, null);
		}
	else if (gblDeviceInfo.name == "android"){
		kony.application.showLoadingScreen("frmLoadingNoIndicator", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
		}
	else
		kony.application.showLoadingScreen("frmLoadingNoIndicator", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, {
			shouldShowLabelInBottom: "false",
			separatorHeight: 10
		});
}

function loggingDeviceStoreData() {
	var storeLen = kony.store.length();
	
	if (storeLen != null && storeLen != undefined) {
		var toBeLogged = "";
		for (var i = 0; i < storeLen; i++) {
			var key = kony.store.key(i);
			
			if (key == "encrytedText") {
				var value = kony.store.getItem(key);
				
				var readEncryVal = value["encKeyVal"]
				
				var retencrKeyFromDevice = value["enckey"]
				
				//  var decryptkey = kony.crypto.readKey(readEncryVal)
				toBeLogged += "Index: " + i + "; Key: " + key + "; Value: AES_Encrypt_Key - " + readEncryVal +
					" = encryptedSecretKey - " + retencrKeyFromDevice + "|";
				
			} else if (key == "curAppLocale") {
				var valu = kony.store.getItem(key);
				var apploc = valu["appLocale"]
				toBeLogged += "Index: " + i + "; Key: " + key + "; Value: " + apploc + "|";
			} else {
				toBeLogged += "Index: " + i + "; Key: " + key + "; Value: " + kony.store.getItem(key) + "|";
				
			}
		}
		var inputParams = {};
		inputParams["methodId"] = "logDeviceStoreData";
		inputParams["storeData"] = toBeLogged;
		var status = invokeServiceSecureAsync("logDeviceStoreData", inputParams, callbackLogDeviceStoreData);
	}
}

function callbackLogDeviceStoreData(status, resulttable) {
	if (status == 400) {
		
	} else {
		
	}
}


function printTermsNConditions() {
	 var tncTitle = kony.i18n.getLocalizedString("keyTermsNConditions");
	 if(tncTitle=="Terms & Conditions")
	 {
	 	tncTitle = "TermsandConditions";
	 }
	 
	 var tncContent = "";
	 
	 if(kony.application.getCurrentForm().id == "frmIBMigratedUserStep1"){
	  tncContent = frmIBMigratedUserStep1.lblTnC.text;
	 }else{
	  tncContent = frmIBActivationTandC.lblTnC.text;
	 }
	
	 showPopup(tncTitle, tncContent);
}
//function printDiv(title, control) {
//	 showPopup($(title, control).html());
//}

function showPopup(title, data) {
	 var mywindow = window.open('', title, 'height=400,width=600');
	 mywindow.document.write('<html><head><title>Terms & Conditions</title>');
	 /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
	 mywindow.document.write('</head><body><center>' + title + '</center><font><div overflow-y: auto; overflow-x: hidden; margin: 0%; padding: 0%; height: 328px;>');
	 mywindow.document.write(data);
	 mywindow.document.write('</div></font></body></html>');
	 mywindow.document.close();
	 mywindow.focus();
	 mywindow.print();
	 mywindow.close();
	 return true;
}

var gblFBLoginSameThreadTrack = false;

function postOnWall() {
		var prevform = kony.application.getPreviousForm();
		var currform = kony.application.getCurrentForm();
		//gblcalled=false;
		gblPOWstateTrack=false;
		if(gblFBCode == null || gblFBCode==""){
			if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android" || flowSpa==true){
					if(gblPOWstateTrack == true){
						prevform.show();
						//gblPOWstateTrack = false;
					}
					else{
						invokeFacebookSetUp();
					}
				}
				else{
					if(gblFBIBStateTrack == true){
						prevform.show();
					}
					else{
						startIBFBPOWService();
						//gblFBLoginSameThreadTrack = true;
					}
				}
		
		}else{
				showLoadingScreen();
				var inputParam = {}
				inputParam["locale"] = kony.i18n.getCurrentLocale();
				inputParam["custNME"] = gblPOWcustNME;
				inputParam["transXN"] = gblPOWtransXN;
				inputParam["channel"] = gblPOWchannel;
				invokeServiceSecureAsync("fbpostmessageonwall", inputParam, postOnWallCallback);
				if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android"){
					//MB does not require same thread execution of frmFBLogin.show()
				}
				else{
					/*
					var myVar = setTimeout(function(){if(gblFBLoginSameThreadTrack == true)clearTimeout(myVar);},18000);
					if(gblFBLoginSameThreadTrack == true)
						startIBFBPOWService();
					gblFBLoginSameThreadTrack = false;
					*/
				}
			}
	}


function postOnWallAfter() {
	showLoadingScreen();
	var inputParam = {}
	inputParam["locale"] = kony.i18n.getCurrentLocale();
	inputParam["custNME"] = gblPOWcustNME;
	inputParam["transXN"] = gblPOWtransXN;
	inputParam["channel"] = gblPOWchannel;
	var fbinputs={};
	invokeServiceSecureAsync("getfbUserCode", fbinputs, getfbcodecalback);
	invokeServiceSecureAsync("fbpostmessageonwall", inputParam, postOnWallCallback);
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android"){
		//MB does not require same thread execution of frmFBLogin.show()
	}
	else{
		/*
		var myVar = setTimeout(function(){if(gblFBLoginSameThreadTrack == true)clearTimeout(myVar);},18000);
		if(gblFBLoginSameThreadTrack == true)
			startIBFBPOWService();
		gblFBLoginSameThreadTrack = false;
		*/
	}
}

function postOnWallCallback(status, getFriends){
	
	dismissLoadingScreen();
	if(status == 400)
	{
		//gblFBCode=getFriends["facebookCode"];
		var prevform = kony.application.getPreviousForm();
		var currform = kony.application.getCurrentForm();
		if(getFriends["invokeServlet"] == "yes"){
			if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android" || flowSpa==true){
				if(gblPOWstateTrack == true){
					prevform.show();
					//gblPOWstateTrack = false;
				}
				else{
					invokeFacebookSetUp();
				}
			}
			else{
				if(gblFBIBStateTrack == true){
					prevform.show();
				}
				else{
					startIBFBPOWService();
					//gblFBLoginSameThreadTrack = true;
				}
			}
		}
		else{
			if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch" || gblDeviceInfo["name"] == "android" || flowSpa==true){
				if(gblPOWstateTrack == true)
					prevform.show();
				else
					currform.show();
			}
			else{
				if(gblFBIBStateTrack == true){
					prevform.show();
				}
				else{
					//DO NOTHING
				}
			}
			//if(getFriends["status"] == "success"){
			//	showAlertRcMB(kony.i18n.getLocalizedString("keyPOWSuccess"), kony.i18n.getLocalizedString("info"), "info")
			//}
			//else if(getFriends["status"] == "fail"){
			//	showAlertRcMB(kony.i18n.getLocalizedString("keyPOWFail"), kony.i18n.getLocalizedString("info"), "info")
			//}
			if(getFriends["status"] == "success"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyPOWSuccess"), kony.i18n.getLocalizedString("info"), "info")
			}
			else if(getFriends["status"] == "authenticationerror"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyPOWFail"), kony.i18n.getLocalizedString("info"), "info")
			}
			else if(getFriends["status"] == "duplicate"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyPOWDuplicate"), kony.i18n.getLocalizedString("info"), "info")
				//showAlertRcMB("This status update is identical to the last one you posted. Try posting something different, or delete your previous update.", kony.i18n.getLocalizedString("info"), "info")
			}
		}
		gblPOWstateTrack = false;
		gblFBIBStateTrack = false;
	}
	
}

function mbRenderFileCallbackfunction(status, resulttable) {
    
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var filePath = resulttable["ouputfilepath"];
            var fileType = resulttable["filetype"];
            var outputtemplatename = resulttable["outputtemplatename"];
            //  var url ="https://dev.tau2904.com:443/middleware/RenderGenericPdfImageServlet";
            url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/RenderGenericPdfImageServlet";
            var  uriquery = "filename="+encodeURIComponent(filePath)+"&filetype="+encodeURIComponent(fileType)+"&outputtemplatename="+encodeURIComponent(outputtemplatename);
            var totalURL = url + "?" + uriquery;
            if(kony.application.getCurrentForm() == frmAccountStatementMB){
		   	 	var accId=gblAccountTable["custAcctRec"][gblIndex]["accId"];
	            var activitytypeId;
		   	 	if(fileType == "pdf"){
		   	 		activitytypeId="084";
		   	 	}
		   	 		//activityLogServiceCall(activitytypeId, "", "01", "", accId, frmAccountStatementMB.lblname.text, gaccType, "", "", "");
	   	 	}
            if(flowSpa && (gblDeviceInfo.category == "android")) 
            {
           
            	
	            try {
		            var cwForm = document.createElement("form");
		        	cwForm.setAttribute('method',"post");
		        	cwForm.setAttribute('action',totalURL);
		        	if(document.getElementsByTagName('body')!= null && document.getElementsByTagName('body')[0]!= null)
		        	{
		            	document.getElementsByTagName('body')[0].appendChild(cwForm);
		        	}
		        	kony.application.dismissLoadingScreen();            
		        	cwForm.submit();
	    		} catch(ex){
	       			
	    		}
	    		
	    		//kony.application.openURL(totalURL);
    		} else {
	    		if ((gblDeviceInfo.name == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") && fileType == "png")
				{
					var myCustomString = (kony.i18n.getLocalizedString("keySaveasImage") + "#!(#&)!" + kony.i18n.getLocalizedString("keySaveImageMessage") + "#!(#&)!" + GLOBAL_KONY_IDLE_TIMEOUT + "#!(#&)!" + totalURL );
					saveImage.save(myCustomString);
	 			}else if(gblDeviceInfo.name == "android"){
	 			
	 			//Calling Android ffi funcion for downloading pdf or image file.
	 			if(KonyDMObject == null){
	 			KonyDMObject = new KonyandroidUtils.KonyDM();
	 			}
	 			if(fileType == "png"){
	 				androidFileType = "png";
	 			}else{
	 				androidFileType = "pdf";
	 			}
	 			showLoadingScreen();
	 			KonyDMObject.downloadFile(
					/**String*/ totalURL, 
					/**Function*/ callbackDownload);
	 			
	 			} 	 			
	 			else {
	    			kony.application.openURL(totalURL);
	    		}
    		}

        }
    }
}

function callbackDownload(result) {
    dismissLoadingScreen();
    var downloadStr = "";
    if (androidFileType == "pdf") {
        downloadStr = "" + kony.i18n.getLocalizedString("keySavePDFMessage");
         dismissLoadingScreen();
         showAlertWithCallBack(downloadStr, kony.i18n.getLocalizedString("info"), removeSpecificImgFromServer);
        // alert(downloadStr);
       // showAlert(downloadStr, kony.i18n.getLocalizedString("info"));
    } else if (androidFileType == "png") {
        downloadStr = "" + kony.i18n.getLocalizedString("keySaveImageMessage");
        dismissLoadingScreen();
        //showAlert(downloadStr, kony.i18n.getLocalizedString("info"));
        showAlertWithCallBack(downloadStr, kony.i18n.getLocalizedString("info"), removeSpecificImgFromServer);
        //invokeServiceSecureAsync("removeImgFrmServerTmp", inputParam, callBackRemoveImg)
    } else {
        downloadStr = "" + kony.i18n.getLocalizedString("keySaveImageMessage");
        //	alert(downloadStr);
        showAlert(downloadStr, kony.i18n.getLocalizedString("info"));
    }
}
function removeSpecificImgFromServer() {
    //alert("isSignedUser : "+isSignedUser);
    if(isSignedUser){
    	var inputParam = {};
    	invokeServiceSecureAsync("removeSpecificImgFromServer", inputParam, callBackRemoveImg);
    }   
}
function callBackRemoveImg() {
}
function ibRenderFileCallbackfunction(status, resulttable) {
    
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var filePath = resulttable["ouputfilepath"];
            var fileType = resulttable["filetype"];
            var outputtemplatename = resulttable["outputtemplatename"];
            var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/RenderGenericPdfImageServlet";
            var uriquery = "filename="+encodeURIComponent(filePath)+"&filetype="+encodeURIComponent(fileType)+"&outputtemplatename="+encodeURIComponent(outputtemplatename);
            var finalURL = url + "?" + uriquery;
            dismissLoadingScreenPopup();
             if(kony.application.getCurrentForm() == frmIBAccntFullStatement){
		   	 	var accId=gblAccountTable["custAcctRec"][gblIndex]["accId"];
		   	 	var activitytypeId;
		   	 	if(fileType == "pdf"){
		   	 		activitytypeId="084";
		   	 	}else if(fileType == "csv"){
		   	 		activitytypeId="085";
		   	 	}
		   	 	//	activityLogServiceCall(activitytypeId, "", "01", "", accId,
	 			//	frmIBAccntFullStatement.lblname.text, gaccType, "", "", "");
		   	 }
		   	 
		
	   	 if(fileType == "print"){
    		kony.application.openURL(finalURL);
    		
	   	 } else 
	   	 {
	   	 	if((navigator.userAgent.match(/iPhone/i)) ||  
				(navigator.userAgent.match(/iPad/i)) || 
				(navigator.userAgent.match(/iPod/i))) 
	   	 	{
	   	 		kony.application.openURL(finalURL);
	   	 		
	   	 	}
	   	 	else
	   	 	{
	            try {
		            var cwForm = document.createElement("form");
		        	cwForm.setAttribute('method',"post");
		        	cwForm.setAttribute('action',finalURL);
		        	
		        	//cwForm.setAttribute("accept", "application/pdf");	        	
		        	//cwForm.setAttribute("enctype", "multipart/form-data");        	
		        	
		        	
		        	if(document.getElementsByTagName('body')!= null && document.getElementsByTagName('body')[0]!= null)
		        	{
		        	   document.getElementsByTagName('body')[0].appendChild(cwForm);		        	   
		        	}
		        	kony.application.dismissLoadingScreen();		        		        	         
		        	cwForm.submit();

                 
                 
	    		} catch(ex){
	       			
	    		} 
    		}// end try catch 
	   	 } // for pdf and img 
    		
        } else {// END : opstatus check
			dismissLoadingScreenPopup();
		}
    } // END : status check end
}

function showAlertForUniqueGenFail(message)
{
	 var alert_seq0_act1 = kony.ui.Alert({
	        "message":message,
	        "alertType": constants.ALERT_TYPE_INFO,
	        "alertTitle": "",
	        "yesLabel": "Close",
	        "noLabel": "",
	        "alertIcon": "",
	        "alertHandler": onCallBackUniqueFailure
	    }, {});
}
function onCallBackUniqueFailure()
{
	try{
         kony.application.exit();
      }
   catch(Error)
      {
         
      }
}

function onConfirmationAlertCallBack(response) {
	GBL_Session_LOCK=true;
    if (response == true) {
    	//update session for session transaction lock
    } else {
    	onCallBackUniqueFailure();
    }
};

function showAlertForSecValidation(message) {
    var alert_seq0_act0 = kony.ui.Alert({
        "message": message,
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "",
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "",
        "alertHandler": onConfirmationAlertCallBack
    }, {});
};



function callServiceToLogRiskInfo()
{
				
		var inputParam ={}; 
		///var deviceInfo = kony.os.deviceInfo();
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			inputParam["deviceRiskScore"] = GBL_RiskScoreIph;
			inputParam["osType"] = "2";
		//#else
			//#ifdef android
			//}else if (deviceInfo["name"] == "android"){
				inputParam["deviceRiskScore"] =GBL_Risk_DataTo_Log;
				inputParam["osType"] = "1";
			//#endif
		//#endif
		//}
		if(GBL_MalWare_Names == "" || GBL_MalWare_Names == null){
			inputParam["malwareResult"] = "0";	
		}else{
			inputParam["malwareResult"] = "1";
		}
		inputParam["malwareName"]=GBL_MalWare_Names;
		inputParam["rootedFalg"]=GBL_rootedFlag.toString();
		inputParam["deviceUNId"]=GBL_UNIQ_ID;
		inputParam["sessionLock"]=GBL_Session_LOCK.toString();
		
		invokeServiceSecureAsync("updateRiskData", inputParam, callBackUpdateRiskData)
}
function callBackUpdateRiskData(status,resulttable){
	if(status == 400){
		
	}
}
function collectRiskData(){
		//if (deviceInfo["name"] == "iPhone" || deviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			startRiskDataCollForIphone();
		//}else if (deviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				startRiskDataCollForAndroid();
			//#endif
		//#endif
		//}
		
}

function passingProdDescTnCEmail()
{
	prodDescTnC = gblFinActivityLogOpenAct["prodDesc"]; 
	prodDescTnC = prodDescTnC.replace(/\s+/g, '');
}

function TextBoxErrorSkin(textBoxName, skinName)
{
textBoxName.skin = skinName
}
/**
 * description
 * returns {}
 */
function showActivationIncompletePage() {
    frmMBActiComplete.image250285458166.src = "iconnotcomplete.png";
    frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString("keyActFail");
    frmMBActiComplete.btnStart.setVisibility(false);
    frmMBActiComplete.show();
}

function onClickLinkActMB(rch){
	 if(rch["href"] !=null){
		  gblLatitude = "";
		  gblLongitude = "";
		 
		    gblCurrenLocLat = "";
		    gblCurrenLocLon = "";
		    provinceID = "";
		    districtID = "";
		    gblProvinceData = [];
		    gblDistrictData = [];
		    frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
		    frmATMBranch.btnCombDistrict.text =  kony.i18n.getLocalizedString("FindTMB_District");
		    frmATMBranch.txtKeyword.text = "";
		  onClickATM();
		  popupActivationHelp.dismiss();
	 }else{
	  	
	 }
}
function dontAllowNonNeumaric(textbozId){
	var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(!isIE8){
		var len=textbozId.text.length;
		var lastChar=textbozId.text.charAt(len-1);
	    if (!kony.string.isNumeric(lastChar)) 
			textbozId.text = textbozId.text.substring(0,len-1);
	}		
 }
 
 
function otplength(){
   var otplen = frmIBActivateIBankingStep2Confirm.txtOTP.text.length;
   if(otplen > 6){
   //alert("length is-->"+otplen);
       frmIBActivateIBankingStep2Confirm.txtOTP.text = frmIBActivateIBankingStep2Confirm.txtOTP.text.substring(0,6);
   }
 } 

function allowOnlyAlphaNumeric(textbozId) {
     var len = textbozId.text.length;
    var lastChar = textbozId.text.charAt(len - 1);
    
    if (!lastChar.match(/^[a-zA-Z0-9- ]*$/)) {
       textbozId.text = textbozId.text.substring(0, len - 1);
    } 
}



function firstNameAlpNumValidation(text){
      var pat1 = /^[A-Za-z\u0E00-\u0E7F\-]+$/
      var isValidFirstName = pat1.test(text);     
      return isValidFirstName;
}
function lastNameAlpNumValidation(text){
      var pat1 = /^[A-Za-z\u0E00-\u0E7F\s-]+$/
      var isValidLastName = pat1.test(text);
      return isValidLastName;
}

function handleOTPLockedIB(callBackResponse){
	gblOTPLockedForBiller = "04";
	gblUserLockStatusIB="04";
	gblRcOpLockStatus=true;
	if(callBackResponse["IBUserStatusID"]!= null || callBackResponse["IBUserStatusID"] != undefined)
		gblUserLockStatusIB= callBackResponse["IBUserStatusID"];
	popIBBPOTPLocked.show();
 }
 

function handleOTPLockedIBNew(){
	popIBBPOTPLockedNew.dismiss();
	if (gblSetPwd == true) {	
	 	frmIBPreLogin.show();
	} else {
		dynamicMenu_btnMenuMyAccountSummary_onClick_seq0();
		frmIBPostLoginDashboard.show();
	}
 }
 
 /*
************************************************************************
            Name    : DisableFadingEdges()
            Author  : Ankit Chandra
            Date    : March 14, 2014
            Purpose : To remove the fading edges of the form in android.
        Input params: CurrentformId
       Output params: Nil
        ServiceCalls: nil
        Called From : preShow of the form
************************************************************************
 */
 
 
 function DisableFadingEdges(CurrentformId)
 {
 	//#ifdef android
 		CurrentformId.sboxRight.showFadingEdges = false ;
		CurrentformId.scrollboxLeft.showFadingEdges = false;
		CurrentformId.scrollboxMain.showFadingEdges = false;
 	//#endif
 	
 }
 
 function dontAllowNonNeumaricSPAMB(textbozId){

        var len = textbozId.text.length;
        while(len >= 1 && (!kony.string.isNumeric(textbozId.text.charAt(len - 1)))){
        textbozId.text = textbozId.text.substring(0, len - 1);
        len = len - 1;
        }
 }
 
 function findColonLastString(text){
 
 	if(text == undefined || text == null)
 		return false;
 	
 	var len =text.trim().length; 
 	if(text.trim().charAt(len - 1) == ':')
 		return false;
 	else
 		return true;
 }
 
 function onDeviceBck()
 {
   	frmBillPaymentEdit.show();

	frmBillPaymentEdit.lblStartDate.text = gblTempTPStartOnDateMB
	frmBillPaymentEdit.lblEndOnDate.text = gblTempTPEndOnDateMB
	frmBillPaymentEdit.lblExecutetimes.text = gblTempTPRepeatAsMB
	
	repeatAsMB = gblTemprepeatAsTPMB
	endFreqSaveMB = gblTempendFreqSaveTPMB;


	if (repeatAsMB == "" || endFreqSaveMB == "") {
		if (gblEndingFreqOnLoadMB == "Never") {
			OnClickRepeatAsMB = "";
			OnClickEndFreqMB = "";
		}else if(gblOnLoadRepeatAsMB == "Once"){
			OnClickRepeatAsMB = repeatAsIB;
			OnClickEndFreqMB = endFreqSave;
		}else {
			OnClickRepeatAsMB = "";
			OnClickEndFreqMB = "";
		}
	} else {
		OnClickRepeatAsMB = repeatAsMB;
		OnClickEndFreqMB = endFreqSaveMB;
	}
 }
 
 function allowOnlyNumbers(evt) {
	var e = evt && evt.type == 'keydown' ? evt : window.event;
	
    if (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        
        if (charCode != 46 && charCode != 110 && charCode != 190 && charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)) {
              e.preventDefault();
        }
        if (e.shiftKey) {
			e.preventDefault();
		}
      }
}

function allowOnlyNumbersZeroToNine(evt) {
	var e = evt && evt.type == 'keydown' ? evt : window.event;
	
    if (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if (charCode != 8 &&(charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)) {
              e.preventDefault();
        }
        if (e.shiftKey) {
			e.preventDefault();
		}
      }
}

function addNumberCheckListner(widjet) {
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(!isIE8){
	    var elementID = kony.application.getCurrentForm().id + "_" + widjet
	    
	    var tNode = document.getElementById(elementID);
	    if (tNode) {
	        tNode.addEventListener('keydown', allowOnlyNumbers, false);
	    }
    }
}

function addNumberCheckListnerZeroToNine(widjet) {
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	if(!isIE8){
	    var elementID = kony.application.getCurrentForm().id + "_" + widjet
	    
	    var tNode = document.getElementById(elementID);
	    if (tNode) {
	        tNode.addEventListener('keydown', allowOnlyNumbersZeroToNine, false);
	    }
    }
}

function getfbcodecalback(status,callBack){
	if(status==400){
		
		if(callBack["fbuserCode"] != "notfound")
			gblFBCode=callBack["fbuserCode"]
		else
			gblFBCode="";
		
	}
}

function appendColon(input){
	if(input != null && input != undefined && (input.trim().charAt(input.trim().length-1) == ':' || input.trim()=="")){
		if(input.trim().charAt(input.trim().length-2) == ':'){
			return input.substring(0,input.trim().length-1);
		}
		return input;
	}else{		
		return input+':';
	}
}

function removeColonFromEnd(input){
	//Common method to remove any number of ':' from the end of an input string
	if(undefined == input || null == input)input="";
	if(input.trim().charAt(input.trim().length-1) == ':'){
		return removeColonFromEnd(input.substring(0,input.trim().length-1));
	}else{		
		return input;
	}
}


function SPAcopyright_text_display() {
	curr_lang = kony.i18n.getCurrentLocale();
	var date = new Date();
	copyright_year = date.getFullYear()
	if (curr_lang == "th_TH") {
		frmSPALogin.lblFooter.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " + (Number(
			copyright_year) + 543) + " " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");
	} else
		frmSPALogin.lblFooter.text = kony.i18n.getLocalizedString("keyIBFooterCopyrightFirstHalf") + " " +
			copyright_year + ". " + kony.i18n.getLocalizedString("keyIBFooterCopyrightSecondHalf");
}
//function to check for Tab/Blank spaces RegEX

function hasWhiteSpace(s) {
  return /\s/g.test(s);
}

function maskCreditCard(creditCardNo) {
    var formatedCCNo = "";
    if(undefined != creditCardNo && null != creditCardNo){
    	if ("" != creditCardNo && 16 == creditCardNo.length) {
	        var length = creditCardNo.length;
	        
	        var firstFour = creditCardNo.substring(0, 4);
	        var fiveSix = creditCardNo.substring(4, 6);
	        var lastFour = creditCardNo.substring(12,16);
	        var formatedValue = firstFour + "-" + fiveSix + "XX-XXXX-" + lastFour;
	        formatedCCNo = formatedValue;
	        
	    } else{
	    	formatedCCNo = creditCardNo;
	    }
    } else{
    	formatedCCNo = creditCardNo;
    }
    return formatedCCNo;
}

function iphone6check() {
	if (flowSpa) {
        if ((navigator.userAgent.match(/iPhone/i))) {
            var iphonescreenwidth = screen.width;
            if (iphonescreenwidth >= 375) {
                isIphone6=true;
            } else {
                isIphone6=false;
            }
        }
		else{
			isIphone6=false;
		}
    } else if (gblDeviceInfo.name == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
        if( gblDeviceInfo.model.indexOf("6") >= 0){
         	isIphone6MB=true;
        } else {
            isIphone6MB=false;
        }
    }else{
        isIphone6=false;
        isIphone6MB=false;
    }
}

function commonMBPostShow(){
       var headerhbx=document.getElementsByClassName("hboxHeader");
       if(headerhbx!= undefined && headerhbx[0] != undefined && headerhbx!= null)
       headerhbx[0].style.margin="0% 0% -1% 0%";
       if(kony.application.getCurrentForm().id == "frmSPALogin"){
              var headerhbx=document.getElementsByClassName("hboxHeader");
       if(headerhbx!= undefined && headerhbx[1] != undefined && headerhbx!= null)
              headerhbx[1].style.margin="0% 0% -1% 0%";
       }
       if(kony.application.getCurrentForm().id == "frmBillPayment"){
              var headerhbx=document.getElementsByClassName("hboxHeader");
              if(headerhbx!= undefined && headerhbx[1] != undefined && headerhbx!= null)
                     headerhbx[1].style.margin="0% 0% -1% 0%";
       }
       if(kony.application.getCurrentForm().id == "frmATMBranch"){
              var headerhbx=document.getElementsByClassName("hbxAccBgBlue");
              if(headerhbx!= undefined && headerhbx[0] != undefined && headerhbx!= null)
              headerhbx[0].style.margin="0% 0% -1% 0%";
       }
}

function loadMyBillPage(){
	gblBillerFromMenu = true;
    getMyBillSuggestListIB();
    //frmIBMyBillersHome.show();
    frmIBMyBillersHome.segMenuOptions.setData([{
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyProfile")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyAccountsIB")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("kelblOpenNewAccount")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("keyMyRecipients")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBsegMenuFocus",
            "text": kony.i18n.getLocalizedString("keyMyBills")
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": kony.i18n.getLocalizedString("myTopUpsMB")
        }
    }]);
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBMyBillersHome.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocus";
    else frmIBMyBillersHome.btnMenuAboutMe.skin = "btnIBMenuAboutMeFocusThai";
}

function OTPhbxBox_skinChange_onBeginEditing(hbxId){
	hbxId.skin = "hbxOtpTextField";
}

function OTPhbxBox_skinChange_onEndEditing(hbxId){
	hbxId.skin = "hbxOtpTextFieldNormal";
}

function AvgYears( startDate , endDate , dateDifference )
{ 
	num = endDate.getFullYear() - startDate.getFullYear();
	if((num *365 + count_Years(startDate , endDate) - dateDifference) <= 0)
		return num;
	else return num - 1;	
}	
		
function isLeapY( year , startDate , endDate )
{
  var tDate = parseDate("29/02/2014");
  tDate.setYear(year);
  if( ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) )
  {
     if( tDate > startDate && tDate < endDate )
		return true;
  }
  return false;
}		
		
		
function count_Years(startDate , endDate)
{
	var cnt = 0;
	for(i = startDate.getFullYear() ; i <= endDate.getFullYear() ; i++)
	{
		if(isLeapY(i , startDate , endDate ))
			cnt ++;
	}
	return cnt;
}

function PreencodeAccntNumbers(ValfrmAccnt)
{
	if (ValfrmAccnt == null) return false;
	if(ValfrmAccnt.length==14)
	{
		if(ValfrmAccnt.substring(0,4)=="0000")
		{
			return encodeAccntNumbers(ValfrmAccnt.substring(4,ValfrmAccnt.length));
		}

	}
    return encodeAccntNumbers(ValfrmAccnt);
}

//Common method to show hide loading indicator for both IB and MB
function showDismissLoadingMBIB(isShow){
	if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPod touch") {
		if(isShow)
        	showLoadingScreen();
        else
        	dismissLoadingScreen();	
    } else {
    	if(isShow)
        	showLoadingScreenPopup();
        else
        	dismissLoadingScreenPopup();	
    }
}

 function frmIBPreLoginGlobalsNonIE8() {
	 hbxIBPreLogin.setVisibility(true);
	 hbxiE8.setVisibility(false);
	 hbxClose.setVisibility(false);
	 frmIBPreLogin.txtUserId.setEnabled(true);
	 frmIBPreLogin.txtPassword.setEnabled(true);
};
function chekcingIE8()
{
	 chkIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
	 if(chkIE8) 
	 {
		 hbxIBPreLogin.setVisibility(false);
		 hbxiE8.setVisibility(true);
		 hbxClose.setVisibility(true);
		// frmIBPreLoginGlobalsforIE8();
		 frmIBPreLogin.txtUserId.setEnabled(false);
		 frmIBPreLogin.txtPassword.setEnabled(false);
	}
}

function formatCitizenID(unformattedCitizenID){
	if(null == unformattedCitizenID)return "";
	if(unformattedCitizenID.length != 13)return unformattedCitizenID;
	var iddata = unformattedCitizenID;
	var iphenText = "";
	var noniphendata = "";
	for (i = 0; i < iddata.length; i++)
		if (iddata[i] != "-") noniphendata += iddata[i];
	iddata = noniphendata;
	for (i = 0; i < iddata.length; i++) {
		iphenText += iddata[i];
		if (i == 0 || i == 4 || i == 9 || i == 11) {
			iphenText += '-';
		}
	}
	return iphenText;
}

function formatMobileNumber(mobileNumber){
	if (mobileNumber != "") {
		var hyphenText = "";
		for (i = 0; i < mobileNumber.length; i++) {
			hyphenText += mobileNumber[i];
			if (i == 2 || i == 5) {
				hyphenText += '-';
			}
		}
	}
	return hyphenText;
}
//below function for non-casa accounts.
function noActiveActs()
{
	var noCasaAct = false;
	var nonCASAAct = 0;
	if(null!=gblAccountTable)
	{
		for(var i=0; i < gblAccountTable.custAcctRec.length;i++)
		{	if(null!=gblAccountTable["custAcctRec"][i]["acctStatus"]){
				var accountStatus = gblAccountTable["custAcctRec"][i]["acctStatus"];
				if(accountStatus.indexOf("Active") == -1){
					nonCASAAct = nonCASAAct + 1;
				}
			}
		}
		if(nonCASAAct==gblAccountTable.custAcctRec.length)
			noCasaAct=true;
	}
	return noCasaAct;
}

//Below function is to truncate extra decimals in amount(amount digits should be less than 18 else it will roundoff) 
function fixedToTwoDecimal(amount) {
	if(isNotBlank(amount)) {
	    amount = removeCommos(amount);
	    amount = Number(amount);
	    amount = amount.toFixedDown(2);
	}
    return amount+"";
}

Number.prototype.toFixedDown = function(amount) {
    var re = new RegExp("(\\d+\\.\\d{" + amount + "})(\\d)"),
        m = this.toString().match(re);
    return m ? parseFloat(m[1]) : this.valueOf();
}

function shortenBillerName(str, len)
{ 	
	
	len = 100; //MIB-5334 - Allow customer to see full biller name on TMB Touch
	var newValue = str;
    var actlength =str.length;
    if(actlength>len)
    {
    	var cmpCode= str.substring(actlength-6, actlength);
    	var res = str.substring(0, actlength-6);
    	var newlength =res.length;
    	var shorten= res.substring(0,len);
    	newValue=shorten+".."+cmpCode;
    }
    else
    {
    	newValue=str;
    }
    
    return newValue;
}

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace);
}


//Sorting Array of Json Objects

// generic comparison function
cmp = function(x, y){
    return x > y ? 1 : x < y ? -1 : 0; 
};

function sortArrayOfJSONObjects(jsonArray,keyArray,order){
	jsonArray.sort(function(a, b){
    var arr1=[];
   	var arr2=[];
    for(var key in keyArray){
    	var val1=cmp(a[keyArray[key]], b[keyArray[key]]);
        var val2=cmp(b[keyArray[key]], a[keyArray[key]]);
        if(order){
        	arr1.push(val1);
        	arr2.push(val2);
        }else{
            //note the minus, for descending order
        	arr1.push(-val1);
        	arr2.push(-val2);      
        }
    }
    return cmp( arr1,arr2);
	});
}


function equals ( x, y ) {
    // If both x and y are null or undefined and exactly the same
    if ( x === y ) {
        return true;
    }

    // If they are not strictly equal, they both need to be Objects
    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) {
        return false;
    }

    // They must have the exact same prototype chain, the closest we can do is
    // test the constructor.
    if ( x.constructor !== y.constructor ) {
        return false;
    }

    for ( var p in x ) {
        // Inherited properties were tested using x.constructor === y.constructor
        if ( x.hasOwnProperty( p ) ) {
            // Allows comparing x[ p ] and y[ p ] when set to undefined
            if ( ! y.hasOwnProperty( p ) ) {
                return false;
            }

            // If they have the same strict value or identity then they are equal
            if ( x[ p ] === y[ p ] ) {
                continue;
            }

            // Numbers, Strings, Functions, Booleans must be strictly equal
            if ( typeof( x[ p ] ) !== "object" ) {
                return false;
            }

            // Objects and Arrays must be tested recursively
            if ( !equals( x[ p ],  y[ p ] ) ) {
                return false;
            }
        }
    }

    for ( p in y ) {
        // allows x[ p ] to be set to undefined
        if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) {
            return false;
        }
    }
    return true;
}

function maskCitizenID(citizenID) {
	if (isNotBlank(citizenID)) {
		citizenID = kony.string.replace(citizenID, "-", "");
		if (citizenID.length == 13) {
			citizenID = "x-xxxx-xxxx"+citizenID.substring(9, 10)+"-"+citizenID.substring(10, 12)+"-"+citizenID.substring(12, 13);
		}
	}
	return citizenID;
}

function maskMobileNumber(mobileNo) {
	if (isNotBlank(mobileNo)) {
		mobileNo = kony.string.replace(mobileNo, "-", "");
		if (mobileNo.length == 10) {
			mobileNo = "xxx-xxx-"+mobileNo.substring(6, 10);
		}
	}
	return mobileNo;
}

function checkReleaseTransferLandingMB(){
	var currentForm = kony.application.getCurrentForm().id;
	if(isNotBlank(currentForm) && kony.string.equals(currentForm, "frmTransferLanding")){
		setEnabledTransferLandingPage(true);
	}
}

function callNative(mobileno) {
	try {
		showLoadingScreen();
		if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad") {
			IphoneCall.dial(mobileno);
			dismissLoadingScreen();
		}else if (gblDeviceInfo["name"] == "android"){
			kony.phone.dial(mobileno);
			dismissLoadingScreen();
		}
	}catch(err){
		kony.print("error callNative: "+mobileno+", error: "+err);
	}
}

// Start : MIB-4884-Allow special characters for My Note and Note to recipient field
function replaceHtmlTagChars(inputString) {
 if(isNotBlank(inputString)) {
	inputString = replaceAll(inputString, "<" , "&lt;");
	inputString = replaceAll(inputString, ">" , "&gt;");
 }
 return inputString;
}

 function replaceHtmlTagsInObject(inputParam) {
	 var keyNames = GBL_ALLOW_SPECIAL_CHAR_KEY_NAMES.split(',');
	 var keyName = "";
	for (i in keyNames) {
		keyName = keyNames[i];
		if (inputParam.hasOwnProperty(keyName)) {
			inputParam[keyName] = replaceHtmlTagChars(inputParam[keyName]);
		}
	}
	
	return inputParam;
 }
 
 function checkSpecialCharMyNote(myNote) {
 	if(isNotBlank(myNote)) {
 		return myNote.indexOf("<%") > -1;
 	}
 	
 	return false;
 }
 
function decodeHtmlTagChars(inputString) {
	 if(isNotBlank(inputString)) {
		inputString = replaceAll(inputString, "&lt;" , "<");
		inputString = replaceAll(inputString, "&gt;" , ">");
	 }
	 return inputString;
}

function checkEmoji(inputData) {
	//Do not remove below commented code, keep it for future reference
	//var patternEmoji = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/;
	//return patternEmoji.test(inputData);
	return !validateSpecialChars(inputData);
}

function validateSpecialChars(inputData) {
	var patternEmojiNow =  new RegExp(GBL_VALID_CHARS_MY_NOTE_NOW);
	var patternEmojiFuture =  new RegExp(GBL_VALID_CHARS_MY_NOTE_FUTURE);
	
	if(gblPaynow) {
		return patternEmojiNow.test(inputData);
	} else {
		return patternEmojiFuture.test(inputData);
	}
}

function notAllowEmojiChars(eventObj) {
	var enteredText = eventObj.text;
	if(isNotBlank(enteredText)) {
		if(checkEmoji(enteredText)) {
			eventObj.text = gblPrevTextNoEmoji;
		} else {
			gblPrevTextNoEmoji = enteredText;
		}
	} else {
		gblPrevTextNoEmoji = "";
	}
}

function assignTextNoEmoji(eventObj) {
	gblPrevTextNoEmoji = eventObj.text;
}
 // End : MIB-4884-Allow special characters for My Note and Note to recipient field
 
function changeStatusBarColor(){
	try {
		kony.application.getCurrentForm().statusBarColor =  "007ABC00";
	} catch (e) {
		// todo: handle exception
	}
} 