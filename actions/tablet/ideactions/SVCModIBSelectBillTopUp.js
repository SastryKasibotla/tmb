var MySelectBillListRs = new Array();
var MySelectBillSuggestListRs = new Array();
//var MySelectBillerTopupList;

function getMySelectBillerListIB() {
	showLoadingScreenPopup();
	var inputParams = {
		crmId: gblcrmId,
		 IsActive: "1"
		//clientDate:getCurrentDate()not working on IE 8
	};
	invokeServiceSecureAsync("customerBillInquiry", inputParams, startMySelectBillListIBServiceAsyncCallback);
}

function startMySelectBillListIBServiceAsyncCallback(status, callBackResponse) {
	
	if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {
			var responseData = callBackResponse["CustomerBillInqRs"];
			gblMyBillList = [];
			if (responseData.length > 0) {
				gblMyBillList = responseData;
				frmIBBillPaymentLP.lblBPBillsList.setVisibility(true);
				populateMySelectBillIB(callBackResponse["CustomerBillInqRs"]);
			} else {
				frmIBBillPaymentLP.lblBPBillsList.setVisibility(false);
				dismissLoadingScreenPopup();
				//alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
			}
		} else {
			frmIBBillPaymentLP.lblBPBillsList.setVisibility(false);
			dismissLoadingScreenPopup();
			//alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("No Billers found");
		}
	}
}

function populateMySelectBillIB(collectionData) {
	MySelectBillListRs.length = 0;
	
	var MySelectBillerTopupList = collectionData;
	for (var i = 0; i < collectionData.length; i++) {
		var imageUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"]+"&modIdentifier=MyBillers";
		if (collectionData[i]["BillerGroupType"] == 0) {
		var isRef2Req=collectionData[i]["IsRequiredRefNumber2Pay"];
			var ref2,ref2Value,ref2LabelEN,ref2LabelTH
			
			if(isRef2Req=="Y"){
			ref2Value=isNotBlank(collectionData[i]["ReferenceNumber2"]) ? collectionData[i]["ReferenceNumber2"] : "";
			ref2LabelEN = collectionData[i]["LabelReferenceNumber2EN"];
			ref2LabelTH = collectionData[i]["LabelReferenceNumber2TH"];
			}
			else{
			ref2Value="";
			ref2LabelEN = "";
			ref2LabelTH ="";
			}
			var compcode = collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			var compcodeTH = collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			var ref1label;
			gblRef1EN = collectionData[i]["LabelReferenceNumber1EN"] +": ";
			gblRef1TH = collectionData[i]["LabelReferenceNumber1TH"]+": ";
			gblRef2EN = collectionData[i]["LabelReferenceNumber2EN"]+": ";
			gblRef2TH = collectionData[i]["LabelReferenceNumber2TH"]+": ";
			var compcodeEN = collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			compcodeTH = collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			if(kony.i18n.getCurrentLocale() == "en_US"){
				ref1label = collectionData[i]["LabelReferenceNumber1EN"] + ": ";
				compcode = collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			if(isRef2Req=="Y"){
				ref2Label= collectionData[i]["LabelReferenceNumber2EN"]+ ": ";
			}else{
				ref2Label= "";
			}
			}else {
				ref1label = collectionData[i]["LabelReferenceNumber1TH"]+ ": ";
				compcode = collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")";
				if(isRef2Req=="Y"){
					ref2Label= collectionData[i]["LabelReferenceNumber2TH"]+ ": ";
				}else{
					ref2Label= "";
			}
			}
			
			//ENH113 
			
            gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay= "";
            gblbillerAllowSetSched= "";
            gblbillerTotalPayAmtEn= "";
            gblbillerTotalPayAmtTh= "";
            gblbillerTotalIntEn= "";
            gblbillerTotalIntTh= "";
            gblbillerDisconnectAmtEn= "";
            gblbillerDisconnectAmtTh= "";
            gblbillerServiceType="";
            gblbillerTransType="";
            gblMeaFeeAmount="";
            gblbillerMeterNoEn="";
            gblbillerMeterNoTh="";
            gblbillerCustNameEn="";
            gblbillerCustNameTh="";
            gblbillerCustAddressEn="";
            gblbillerCustAddressTh="";
            
            if(collectionData[i]["BillerCompcode"]=="2533"){
            
            	if(collectionData[i]["ValidChannel"] != undefined && collectionData[i]["ValidChannel"].length > 0){
            		for (var t = 0; t < collectionData[i]["ValidChannel"].length; t++) {
            			if(collectionData[i]["ValidChannel"][t]["ChannelCode"] == "01"){
            				gblMeaFeeAmount=collectionData[i]["ValidChannel"][t]["FeeAmount"];
            			}
            		
            		}
            	}
            	
	            if (collectionData[i]["BillerMiscData"] != undefined && collectionData[i]["BillerMiscData"].length > 0) {
	
	                for (var j = 0; j < collectionData[i]["BillerMiscData"].length; j++) {
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
	                       gblbillerStartTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
	                        gblbillerEndTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
	                        gblbillerFullPay = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
	                        gblbillerAllowSetSched = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
						if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
	                        gblbillerMeterNoEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
	                        gblbillerMeterNoTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
	                        gblbillerCustNameEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
	                        gblbillerCustNameTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
	                        gblbillerCustAddressEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
	                        gblbillerCustAddressTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
	                        gblbillerTotalPayAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
	                        gblbillerTotalPayAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
	                        gblbillerAmountEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
	                        gblbillerAmountTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
	                        gblbillerTotalIntEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
	                        gblbillerTotalIntTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
	                        gblbillerDisconnectAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
	                        gblbillerDisconnectAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
	                        gblbillerServiceType = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
	                        gblbillerTransType = collectionData[i]["BillerMiscData"][j]["MiscText"];
	                    }

	                    
	                }
	             
	             
	                
	            }
	          }
            //ENH113 end

			
			// added this ccOrAccNumMasked hidden varible in below segment for masking CC number
			var tempRecord = {
				"crmId": collectionData[i]["CRMID"],
				"lblBillsName": collectionData[i]["BillerNickName"],
				"lblRef1": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
				"ccOrAccNumMasked": collectionData[i]["ReferenceNumber1"],
				"lblRef2": ref2Value,
				"imgBills": {
					"src": imageUrl
				},
				"imgArrow": {
					"src": "bg_arrow_right.png"
				},
				"lblRf1": appendColon(ref1label),
				"lblRf2": appendColon(ref2Label),
				"lblRf1TH": collectionData[i]["LabelReferenceNumber1TH"],
				"lblRf2TH":ref2LabelTH,
				"lblRf1EN": collectionData[i]["LabelReferenceNumber1EN"],
				"lblRf2EN":ref2LabelEN,
				"Ref2Len" :collectionData[i]["Ref2Len"],
				"Ref1Len": collectionData[i]["Ref1Len"],
				"BillerID": collectionData[i]["BillerID"],
				"BillerCompCode": collectionData[i]["BillerCompcode"],
				"BeepndBill": collectionData[i]["BeepNBillFlag"],
				"lblBillCompCode": compcode,
				"lblBillCompCodeTH": compcodeTH,
				"lblBillCompCodeEN": compcodeEN,
				"billerMethod": collectionData[i]["BillerMethod"],
				"billerGroupType": collectionData[i]["BillerGroupType"],
				"IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
				"IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
				"ToAccountKey": collectionData[i]["ToAccountKey"],
				"IsFullPayment" : collectionData[i]["IsFullPayment"],
				"CustomerBillID": collectionData[i]["CustomerBillID"],
				"BillerCategoryID": collectionData[i]["BillerCategoryID"],
				"billerStartTime": gblbillerStartTime,
                "billerEndTime": gblbillerEndTime,
                "billerFullPay": gblbillerFullPay,
                "billerAllowSetSched": gblbillerAllowSetSched,
                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                "billerTotalIntEn": gblbillerTotalIntEn,
                "billerTotalIntTh": gblbillerTotalIntTh,
                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                "billerServiceType": gblbillerServiceType,
                "billerTransType": gblbillerTransType,
                "billerFeeAmount":gblMeaFeeAmount,
                "billerAmountEn":gblbillerAmountEn,
                "billerAmountTh":gblbillerAmountTh,
                "billerMeterNoEn":gblbillerMeterNoEn,
				"billerMeterNoTh":gblbillerMeterNoTh,
				"billerCustNameEn":gblbillerCustNameEn,
				"billerCustNameTh":gblbillerCustNameTh,
				"billerCustAddressEn":gblbillerCustAddressEn,
			    "billerCustAddressTh":gblbillerCustAddressTh,
			    "billerBancassurance":collectionData[i]["IsBillerBancassurance"],
			    "allowRef1AlphaNum":collectionData[i]["AllowRef1AlphaNum"]
			}
			
			
			
			MySelectBillListRs.push(tempRecord);
		}
	}
	dismissLoadingScreenPopup();
	if(MySelectBillListRs.length == 0) {
		frmIBBillPaymentLP.lblBPBillsList.setVisibility(false);
	}
	frmIBBillPaymentLP.segBPBillsList.removeAll();
	frmIBBillPaymentLP.segBPBillsList.setData(MySelectBillListRs);
	
	if(isNotBlank(frmIBBillPaymentLP.txtBPSearch.text) && frmIBBillPaymentLP.txtBPSearch.text.length >= 3) {
		searchMyBillsAndSuggestedBillersIB();
	}
}

function getMySelectTopUpListIB() {
	var inputParams = {
		crmId: gblcrmId,
		IsActive: "1"
		//clientDate:getCurrentDate()not working on IE 8
	};
	invokeServiceSecureAsync("customerBillInquiry", inputParams, startMySelectTopUpListIBServiceAsyncCallback);
}

function startMySelectTopUpListIBServiceAsyncCallback(status, callBackResponse) {
	
	if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {
			
			var responseData = callBackResponse["CustomerBillInqRs"];
			gblMyBillList = [];
			if (responseData.length > 0) {
				gblMyBillList = responseData;
				populateMySelectTopUp(callBackResponse["CustomerBillInqRs"]);
				dismissLoadingScreenPopup();
			} else {
				dismissLoadingScreenPopup();
				kony.i18n.getLocalizedString('keyaddbillerstolist');//alert("No Biller  found");
			}
		} else {
			//dismiss loading screen
			dismissLoadingScreenPopup();
			kony.i18n.getLocalizedString('keyaddbillerstolist');
		}
	} else {
		if (status == 300) {
			//dismiss loading screen
			dismissLoadingScreenPopup();
			alert("No Recipient found");
		}
	}
}

function populateMySelectTopUp(collectionData) {
	
	
	MySelectBillListRs.length = 0;
	var MySelectBillerTopupList = collectionData;
	var locale=kony.i18n.getCurrentLocale();
	for (var i = 0; i < collectionData.length; i++) {
		if (collectionData[i]["BillerGroupType"] == 1) {
			var isRef2Req=collectionData[i]["IsRequiredRefNumber2Pay"];
			var ref2,ref2Value
			var ref1,Ref2,CompCodeName;
			if(isRef2Req=="Y"){
					Ref2=collectionData[i]["LabelReferenceNumber2TH"] +": ";
				}
				else{
					Ref2 = ""
				}
			if(kony.i18n.getCurrentLocale() == "en_US"){
				ref1 = collectionData[i]["LabelReferenceNumber1EN"]+": ";
				
				CompCodeName = collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			}
			else {
				ref1 = collectionData[i]["LabelReferenceNumber1TH"]+": ";
				
				CompCodeName = collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			}
			//alert(isRef2Req)
			
			//  gblCompCodeTopUp=collectionData[i]["BillerCompcode"]
			var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"]+"&modIdentifier=MyBillers";
			var compcode = collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			var compcodeEN = collectionData[i]["BillerNameEN"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			var compcodeTH = collectionData[i]["BillerNameTH"] + "(" + collectionData[i]["BillerCompcode"] + ")";
			var tempRecord = {
				"crmId": collectionData[i]["CRMID"],
				"lblBillerName": collectionData[i]["BillerNickName"],
				"lblRef1Value": collectionData[i]["ReferenceNumber1"],
				"lblRef1ValueMasked": maskCreditCard(collectionData[i]["ReferenceNumber1"]),
				"lblRef2Value": ref2Value,
				"imgBillerpic": {
					"src": imagesUrl
				},
				"imgBillersRtArrow": {
					"src": "bg_arrow_right.png"
				},
				"lblRef1": ref1,
				"lblRef1EN": collectionData[i]["LabelReferenceNumber1EN"]+": ",
				"lblRef1TH": collectionData[i]["LabelReferenceNumber1TH"]+": ",
				"lblRef2EN": collectionData[i]["LabelReferenceNumber2EN"]+": ",
				"lblRef2": Ref2,
				"lblRef2TH": collectionData[i]["LabelReferenceNumber2TH"]+": ",
				"BillerID": collectionData[i]["BillerID"],
				"BillerCompCode": CompCodeName,
				"BillerCompCodeEN": compcodeEN,
				"BillerCompCodeTH": compcodeTH,
				"BeepndBill": collectionData[i]["BeepNBillFlag"],
				"billerMethod": collectionData[i]["BillerMethod"],
				"billerGroupType": collectionData[i]["BillerGroupType"],
				"IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
				"IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
				"ToAccountKey": collectionData[i]["ToAccountKey"],
				"IsFullPayment" : collectionData[i]["IsFullPayment"],
				"CustomerBillID": collectionData[i]["CustomerBillID"],
				"BillerCategoryID": collectionData[i]["BillerCategoryID"],
				"Compcode": collectionData[i]["BillerCompcode"]
			}
			MySelectBillListRs.push(tempRecord);
		}
	}
	frmIBTopUpLandingPage.segBiller.removeAll();
	
	frmIBTopUpLandingPage.segBiller.setData(MySelectBillListRs);
}

function getSelectBillerCategoryService() {
	
	var inputParams = {
		BillerCategoryGroupType: gblBillerCategoryGroupType
		//clientDate:getCurrentDate()not working on IE 8
	};
	invokeServiceSecureAsync("billerCategoryInquiry", inputParams, getSelectBillerListServiceAsyncCallback);
}

function getSelectTopUpCategoryService() {
	
	var inputParams = {
		BillerCategoryGroupType: gblTopupCategoryGroupType
	};
	invokeServiceSecureAsync("billerCategoryInquiry", inputParams, getSelectBillerListServiceAsyncCallback);
}

function getSelectBillerListServiceAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			var collectionData = callBackResponse["BillerCategoryInqRs"];
			var masterData = [
                [0, kony.i18n.getLocalizedString("keyBillPaymentSelectCategory")]
            ];
			var tempData;
			for (var i = 0; i < collectionData.length; i++) {
				invokeCommonIBLogger("BILLER CODE " + collectionData[i]["BillerCategoryID"] + "BILLER NAME " + collectionData[i][
					"BillerCategoryDescEN"]);
					var categoryDesc;
					if(kony.i18n.getCurrentLocale()=="en_US"){
						categoryDesc = collectionData[i]["BillerCategoryDescEN"]
					}else {
						categoryDesc = collectionData[i]["BillerCategoryDescTH"]
					}
				tempData = [collectionData[i]["BillerCategoryID"], categoryDesc];
				masterData.push(tempData);
			}
			if (kony.application.getCurrentForm()
				.id == 'frmIBTopUpLandingPage') {
				//alert("inside frmIBTopUpLandingPage");
				frmIBTopUpLandingPage.combobox1010778103113652.masterData = masterData;
				getMySelectTopUpSuggestListIB()
				// getMySelectTopUpListIB();
			} else if (kony.application.getCurrentForm()
				.id == 'frmIBBillPaymentLP') {
				//alert("inside frmIBBillPaymentLP")
				frmIBBillPaymentLP.cbxBillCategory.masterData = masterData;
				getMySelectBillerSuggestListIB();
			} else {
				dismissLoadingScreenPopup();
			}
			//	frmIBTopUpLandingPage.combobox1010778103113652.masterData= masterData;        	
		} else {
			var masterData = [];
			var tempData = ["Biller", "Biller"];
			masterData.push(tempData);
			if (kony.application.getCurrentForm()
				.id == 'frmIBTopUpLandingPage') {
				frmIBTopUpLandingPage.combobox1010778103113652.masterData = masterData;
			} else if (kony.application.getCurrentForm()
				.id == 'frmIBBillPaymentLP') {
				frmIBBillPaymentLP.cbxBillCategory.masterData = masterData;
			}
		}
	} else {
		if (status == 300) {
			var masterData = [];
			var tempData = ["Biller", "Biller"];
			masterData.push(tempData);
			dismissLoadingScreenPopup();
			if (kony.application.getCurrentForm()
				.id == 'frmIBTopUpLandingPage') {
				frmIBTopUpLandingPage.combobox1010778103113652.masterData = masterData;
			} else if (kony.application.getCurrentForm()
				.id == 'frmIBBillPaymentLP') {
				frmIBBillPaymentLP.cbxBillCategory.masterData = masterData;
			}
		}
	}
}
var billerId;

function getMySelectBillerSuggestListIB() {
	var inputParams = {
		IsActive: "1",
		BillerGroupType: gblGroupTypeBiller,
		flagBillerList : "IB"
		//clientDate:getCurrentDate()not working on IE 8
	};
	showLoadingScreenPopup();
	invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMySelectBillSuggestListIBServiceAsyncCallback);
}

function startMySelectBillSuggestListIBServiceAsyncCallback(status, callBackResponse) {
	
	if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {
			var responseData = callBackResponse["MasterBillerInqRs"];
			if (responseData.length > 0) {
				populateMySelectBillSuggest(callBackResponse["MasterBillerInqRs"]);
				getMySelectBillerListIB();
			} else {
				dismissLoadingScreenPopup();
				alert("No Suggested Biller  found");
			}
		} else {
			dismissLoadingScreenPopup();
			alert("No Suggested Biller found");
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("No Suggested Biller found");
		}
	}
}

function populateMySelectBillSuggest(collectionDataa) {
	
	
	var masterData = [];
	var imagesUrl;
	var ref1;
	var ref2;
	var billerName;
	MySelectBillSuggestListRs.length = 0;
	for (var i = 0; i < collectionDataa.length; i++) {
		if (collectionDataa[i]["BillerGroupType"] == 0) {
//			gblCompCodeBillers = collectionDataa[i]["BillerCompcode"];
			gblRef1EN = collectionDataa[i]["LabelReferenceNumber1EN"]
			gblRef1TH = collectionDataa[i]["LabelReferenceNumber1TH"];
			gblRef2EN = collectionDataa[i]["LabelReferenceNumber2EN"]
			gblRef2TH = collectionDataa[i]["LabelReferenceNumber2TH"];
			if(kony.i18n.getCurrentLocale()=="en_US"){
				ref1 = 	collectionDataa[i]["LabelReferenceNumber1EN"];
				ref2 = collectionDataa[i]["LabelReferenceNumber2EN"];
				billerName = collectionDataa[i]["BillerNameEN"] + "(" + collectionDataa[i]["BillerCompcode"] + ")";
			}else {
				ref1 = 	collectionDataa[i]["LabelReferenceNumber1TH"];
				ref2 = collectionDataa[i]["LabelReferenceNumber2TH"];
				billerName = collectionDataa[i]["BillerNameTH"] + "(" + collectionDataa[i]["BillerCompcode"] + ")";
			}
			imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionDataa[i]["BillerCompcode"]+"&modIdentifier=MyBillers";
			//

		 //ENH113 
			
            gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay= "";
            gblbillerAllowSetSched= "";
            gblbillerTotalPayAmtEn= "";
            gblbillerTotalPayAmtTh= "";
            gblbillerTotalIntEn= "";
            gblbillerTotalIntTh= "";
            gblbillerDisconnectAmtEn= "";
            gblbillerDisconnectAmtTh= "";
 			gblbillerServiceType="";
            gblbillerTransType="";
            gblbillerAmountEn="";
   			gblbillerAmountTh="";
   			gblbillerMeterNoEn="";
            gblbillerMeterNoTh="";
            gblbillerCustNameEn="";
            gblbillerCustNameTh="";
            gblbillerCustAddressEn="";
            gblbillerCustAddressTh="";
            if(collectionDataa[i]["BillerCompcode"]=="2533"){
	            if (collectionDataa[i]["BillerMiscData"] != undefined && collectionDataa[i]["BillerMiscData"].length > 0) {
	
	                for (var j = 0; j < collectionDataa[i]["BillerMiscData"].length; j++) {
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
	                       gblbillerStartTime = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
	                        gblbillerEndTime = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
	                        gblbillerFullPay = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
	                        gblbillerAllowSetSched = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
	                        gblbillerMeterNoEn = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
	                        gblbillerMeterNoTh = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
	                        gblbillerCustNameEn = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
	                        gblbillerCustNameTh = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
	                        gblbillerCustAddressEn = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
	                        gblbillerCustAddressTh = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
	                        gblbillerTotalPayAmtEn = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
	                        gblbillerTotalPayAmtTh = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
	                        gblbillerAmountEn = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
	                        gblbillerAmountTh = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                     if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
	                        gblbillerTotalIntEn = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
	                        gblbillerTotalIntTh = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
	                        gblbillerDisconnectAmtEn = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
	                        gblbillerDisconnectAmtTh = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
	                        gblbillerServiceType = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    if (collectionDataa[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
	                        gblbillerTransType = collectionDataa[i]["BillerMiscData"][j]["MiscText"];
	                    }
	                    
	                }
	             
	                
	            }
	          }
            //ENH113 end
			
			var tempRecord = {
				"lblSgstdBillerName": billerName,
				"lblSgstdBillerNameEN": collectionDataa[i]["BillerNameEN"] + "(" + collectionDataa[i]["BillerCompcode"] + ")",
				"lblSgstdBillerNameTH": collectionDataa[i]["BillerNameTH"] + "(" + collectionDataa[i]["BillerCompcode"] + ")",
				"imgSgstdBiller": {
					"src": imagesUrl
				},
				"imgArrow": {
					"src": "bg_arrow_right.png"
				},
				"BillerID": collectionDataa[i]["BillerID"],
				"BillerCompCode": collectionDataa[i]["BillerCompcode"],
				"Ref1Label": ref1,
				"BarcodeOnly": collectionDataa[i]["BarcodeOnly"],
				"Ref2Label": ref2,
				"Ref1EN" : collectionDataa[i]["LabelReferenceNumber1EN"],
				"Ref1TH" : collectionDataa[i]["LabelReferenceNumber1TH"],
				"Ref2EN" : collectionDataa[i]["LabelReferenceNumber2EN"],
				"Ref2TH" : collectionDataa[i]["LabelReferenceNumber2TH"],
				"Ref2Len" :collectionDataa[i]["Ref2Len"],
				"Ref1Len": collectionDataa[i]["Ref1Len"],
				"BillerGroupType" : collectionDataa[i]["BillerGroupType"],
				"ToAccountKey" : collectionDataa[i]["ToAccountKey"],
				"IsFullPayment" : collectionDataa[i]["IsFullPayment"],
				"IsRequiredRefNumber2Add": collectionDataa[i]["IsRequiredRefNumber2Add"],
				"IsRequiredRefNumber2Pay": collectionDataa[i]["IsRequiredRefNumber2Pay"],
				"EffDt": collectionDataa[i]["EffDt"],
				"BillerMethod": collectionDataa[i]["BillerMethod"],
				"BillerCategoryID": {
					"text": collectionDataa[i]["BillerCategoryID"]
				} ,
				"billerStartTime": gblbillerStartTime,
                "billerEndTime": gblbillerEndTime,
                "billerFullPay": gblbillerFullPay,
                "billerAllowSetSched": gblbillerAllowSetSched,
                "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                "billerTotalIntEn": gblbillerTotalIntEn,
                "billerTotalIntTh": gblbillerTotalIntTh,
                "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                "billerServiceType": gblbillerServiceType,
                "billerTransType": gblbillerTransType,
                "billerAmountEn":gblbillerAmountEn,
   			    "billerAmountTh":gblbillerAmountTh,
   			    "billerMeterNoEn":gblbillerMeterNoEn,
				"billerMeterNoTh":gblbillerMeterNoTh,
				"billerCustNameEn":gblbillerCustNameEn,
				"billerCustNameTh":gblbillerCustNameTh,
				"billerCustAddressEn":gblbillerCustAddressEn,
			    "billerCustAddressTh":gblbillerCustAddressTh,
			    "billerBancassurance":collectionDataa[i]["IsBillerBancassurance"],
			    "allowRef1AlphaNum":collectionDataa[i]["AllowRef1AlphaNum"]
			}
			//
			//
			//
			MySelectBillSuggestListRs.push(tempRecord);
		}
	}
	frmIBBillPaymentLP.segBPSgstdBillerList.removeAll();
	frmIBBillPaymentLP.segBPSgstdBillerList.setData(MySelectBillSuggestListRs);
	gblBillerLastSearchNoCat = MySelectBillSuggestListRs.concat();
}


function getMySelectTopUpSuggestListIB() {
	var inputParams = {
		IsActive: "1",
		BillerGroupType: gblGroupTypeTopup,
		flagBillerList : "IB"
		//clientDate:getCurrentDate()not working on IE 8
	};
	invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMySelectTopUpSuggestListIBServiceAsyncCallback);
}

function startMySelectTopUpSuggestListIBServiceAsyncCallback(status, callBackResponse) {
	
	//
	//
	if (status == 400) {
		if (callBackResponse["opstatus"] == "0") {
			var responseData = callBackResponse["MasterBillerInqRs"];
			if (responseData.length > 0) {
				populateMySelectTopUpSuggest(callBackResponse["MasterBillerInqRs"]);
				getMySelectTopUpListIB();
			} else {
				dismissLoadingScreenPopup();
				alert("No Suggested Biller  found");
			}
		} else {
			dismissLoadingScreenPopup();
			alert("No Suggested Biller found");
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("No Suggested Biller found");
		}
	}
}

function populateMySelectTopUpSuggest(collectionDataa) {
	
	
	var masterData = [];
	MySelectBillSuggestListRs.length = 0;
	tempMinAmount = []
	for (var i = 0; i < collectionDataa.length; i++) {
		/* minimum amount locha */
		var billerMethod = collectionDataa[i]["BillerMethod"];
		
		
		if(billerMethod == "1" && collectionDataa[i]["BillerGroupType"] == "1" && collectionDataa[i]["BillerCompcode"]!=="2605"){
			
			var min = collectionDataa[i]["StepAmount"][0]["Amt"] ;
			
			tempMinAmount = {
				"compcode":collectionDataa[i]["BillerCompcode"],
				"minAmount":min
			}
		}else {
			tempMinAmount = [];
		}
		var billerName;// = collectionDataa[i]["BillerNameEN"] + "(" + collectionDataa[i]["BillerCompcode"] + ")";
		var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionDataa[i]["BillerCompcode"]+"&modIdentifier=MyBillers";
		var refValue = "";
		if(kony.i18n.getCurrentLocale() == "en_US"){
			billerName = collectionDataa[i]["BillerNameEN"] + "(" + collectionDataa[i]["BillerCompcode"] + ")";
			refValue = collectionDataa[i]["LabelReferenceNumber1EN"];
		}else{
			 refValue = collectionDataa[i]["LabelReferenceNumber1TH"];
			 billerName = collectionDataa[i]["BillerNameTH"] + "(" + collectionDataa[i]["BillerCompcode"] + ")";
		}
		
		//alert(imagesUrl);
		var tempRecord = {
			"lblSuggestedBiller": billerName,
			"lblSuggestedBillerEN": collectionDataa[i]["BillerNameEN"] + "(" + collectionDataa[i]["BillerCompcode"] + ")",
			"lblSuggestedBillerTH": collectionDataa[i]["BillerNameTH"] + "(" + collectionDataa[i]["BillerCompcode"] + ")",
			"btnAddSuggBiller": {
				"text": "Add",
				"skin": "btnIBaddsmall"
			},
			"BillerID": collectionDataa[i]["BillerID"],
			"BillerCompCode": collectionDataa[i]["BillerCompcode"],
			"BarcodeOnly": collectionDataa[i]["BarcodeOnly"],
			"Ref1Label": refValue,
			"Ref1Len": collectionDataa[i]["Ref1Len"],
			"Ref1EN" : collectionDataa[i]["LabelReferenceNumber1EN"],
			"Ref1TH" : collectionDataa[i]["LabelReferenceNumber1TH"],
			"BillerGroupType" : collectionDataa[i]["BillerGroupType"],
			"ToAccountKey" : collectionDataa[i]["ToAccountKey"],
			"IsFullPayment" : collectionDataa[i]["IsFullPayment"],
			"IsRequiredRefNumber2Add" : collectionDataa[i]["IsRequiredRefNumber2Add"],
			"IsRequiredRefNumber2Pay" : collectionDataa[i]["IsRequiredRefNumber2Pay"],
			//  "Ref2Label": {
			//	                "text": collectionDataa[i]["LabelReferenceNumber2EN"]
			//	            }
			"EffDt": collectionDataa[i]["EffDt"],
			"BillerMethod": collectionDataa[i]["BillerMethod"],
			"BillerCategoryID": {"text":collectionDataa[i]["BillerCategoryID"]},
			"imgSuggestedBiller": {
				"src": imagesUrl
			},
			"StepAmount": collectionDataa[i]["StepAmount"]
		}
		MySelectBillSuggestListRs.push(tempRecord);
		if(tempMinAmount.length !=0)
		gblstepMinAmount.push(tempMinAmount);
	}
	frmIBTopUpLandingPage.segSuggestedBiller.removeAll();
	frmIBTopUpLandingPage.segSuggestedBiller.setData(MySelectBillSuggestListRs);
	gblBillerLastSearchNoCat = MySelectBillSuggestListRs.concat();
	//dismissLoadingScreenPopup();
}

function onclickOfIBSelectedSuggestedTopupAdd() {
		gblSuggestedBiller =false;
	//frmIBMyBillersHome.show();
	gblAddBillerButtonClicked=false;
	frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
	frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(true);
	frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
	frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
	frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
	frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(false);
	frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
	
	frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(true);
	frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
	frmIBMyTopUpsHome.txtAddBillerNickName.setEnabled(true);
	frmIBMyTopUpsHome.txtAddBillerRef1.setEnabled(true);

	//alert("hi onclickOfIBSelectedSuggestedTopupAdd")     
	gblBillerIdMB = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0].BillerID;
	gblBillerMethod = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0].BillerMethod;
	gblEffDtMB = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0].EffDt;
	gblRef1LenIB = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0].Ref1Len;
	frmIBMyTopUpsHome.txtAddBillerRef1.maxTextLength = gblRef1LenIB;
	
	gblBillerCompCode = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0].BillerCompCode;
	
	
	frmIBMyTopUpsHome.lblAddBillerRef1.text = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0].Ref1Label;
	var indexOfSelectedRow = frmIBTopUpLandingPage.segSuggestedBiller.selectedIndex[1];
	var indexOfSelectedIndex = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0];
	frmIBMyTopUpsHome.imgAddBillerLogo.src = indexOfSelectedIndex.imgSuggestedBiller.src;	
	frmIBMyTopUpsHome.lblAddBillerName.text = indexOfSelectedIndex.lblSuggestedBiller;
	
	gblCurRef1LblEN = indexOfSelectedIndex.Ref1EN + ":";
    gblCurRef1LblTH = indexOfSelectedIndex.Ref1TH + ":";
    gblCurBillerNameEN = indexOfSelectedIndex.lblSuggestedBillerEN;
    gblCurBillerNameTH = indexOfSelectedIndex.lblSuggestedBillerTH;
    
	frmIBMyTopUpsHome.segBillersList.setVisibility(false);
	frmIBMyTopUpsHome.segSuggestedBillersList.setVisibility(true);
	frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(true);
	frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
	frmIBMyTopUpsHome.hbxMore.setVisibility(false);
	frmIBMyTopUpsHome.segSuggestedBillersList.removeAll();
	frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(true);
	isSelectTopupEnabled=true;
	gblTopUpMore=0;
	gblCustTopUpMore= 0;
	getMyTopUpSuggestListIB();
	//startDisplayBillerCategoryServiceIB()
	
	frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
	frmIBMyTopUpsHome.line588686174252372.setVisibility(false);
	//frmIBMyTopUpsHome.show();
	
}

function onclickOfIBSelectedSuggestedBillsAdd() {
	gblSuggestedBiller =false;
	//frmIBMyBillersHome.show();
	gblAddBillerButtonClicked=false;
	frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
	frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(true);
	frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
	frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
	frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
	frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
	frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
	
	frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
	frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
	frmIBMyBillersHome.txtAddBillerNickName.setEnabled(true);
	frmIBMyBillersHome.txtAddBillerRef1.setEnabled(true);

	//alert("hi onclickOfIBSelectedSuggestedTopupAdd")     
	gblBillerIdMB = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].BillerID;
	gblBillerMethod = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].BillerMethod;
	gblEffDtMB = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].EffDt;
	
	gblBillerCompCode = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].BillerCompCode;
	gblRef1LenIB = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0]["Ref1Len"];
	frmIBMyBillersHome.txtAddBillerRef1.maxTextLength = gblRef1LenIB;
	gblPayFull = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].IsFullPayment;
	gblBillerBancassurance = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].billerBancassurance;
	gblAllowRef1AlphaNum = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].allowRef1AlphaNum;	
	
	frmIBMyBillersHome.lblAddBillerRef1.text = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0].Ref1Label + ":";
	var indexOfSelectedRow = frmIBBillPaymentLP.segBPSgstdBillerList.selectedIndex[1];
	var indexOfSelectedIndex = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0];
	frmIBMyBillersHome.imgAddBillerLogo.src = indexOfSelectedIndex.imgSgstdBiller.src;	
	frmIBMyBillersHome.lblAddBillerName.text = indexOfSelectedIndex.lblSgstdBillerName
	gblIsRef2RequiredIB = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["IsRequiredRefNumber2Add"];
	gblRef2LenIB = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref2Len"];
	if (kony.string.equalsIgnoreCase(gblIsRef2RequiredIB, "N")) {
		frmIBMyBillersHome.lblAddBillerRef2.setVisibility(false);
		frmIBMyBillersHome.txtAddBillerRef2.setVisibility(false);
		frmIBMyBillersHome.line4belowRef2.setVisibility(false);
		//gblRef2LenIB = 0;
	} else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredIB, "Y")) {
		gblRef2Flag = true;
		frmIBMyBillersHome.lblAddBillerRef2.setVisibility(true);
		frmIBMyBillersHome.txtAddBillerRef2.setVisibility(true);
		frmIBMyBillersHome.txtAddBillerRef2.setEnabled(true);
		frmIBMyBillersHome.line4belowRef2.setVisibility(false);
		frmIBMyBillersHome.lblAddBillerRef2.text = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref2Label"] + ":";
		//gblRef2LenIB = frmIBBillPaymentLP["segBPSgstdBillerList"]["selectedItems"][0]["Ref2Len"];
		
		frmIBMyBillersHome.hbxBillerRef2.setVisibility(true);
		frmIBMyBillersHome.txtAddBillerRef2.maxTextLength = gblRef2LenIB;
	}
	gblCurRef1LblEN = indexOfSelectedIndex.Ref1EN + ":";
    gblCurRef1LblTH = indexOfSelectedIndex.Ref1TH + ":";
    gblCurRef2LblEN = indexOfSelectedIndex.Ref2EN + ":";
    gblCurRef2LblTH = indexOfSelectedIndex.Ref2TH + ":";
    gblCurBillerNameEN = indexOfSelectedIndex.lblSgstdBillerNameEN;
    gblCurBillerNameTH = indexOfSelectedIndex.lblSgstdBillerNameTH;
    
	frmIBMyBillersHome.segBillersList.setVisibility(false);
	frmIBMyBillersHome.segSuggestedBillersList.setVisibility(true);
	frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
	frmIBMyBillersHome.segBillersList.setVisibility(false);
	frmIBMyBillersHome.lblMyBills.setVisibility(false);
	frmIBMyBillersHome.hbxMore.setVisibility(false);
	frmIBMyBillersHome.segSuggestedBillersList.removeAll();
	frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
	isSelectBillerEnabled=true;
	gblTopUpMore=0;
	gblCustTopUpMore= 0;
	getMyBillSuggestListIB();
	//startDisplayBillerCategoryServiceIB()
	
	frmIBMyBillersHome.lblMyBills.setVisibility(false);
	frmIBMyBillersHome.line588686174252372.setVisibility(false);
	//addSuggestedBillPaymentIB();
}



