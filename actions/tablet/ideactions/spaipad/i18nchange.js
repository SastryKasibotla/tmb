function langchange(lang) {
    kony.i18n.setCurrentLocaleAsync(lang, allpagelocalecheck, onfailurecallback, "");
}

function onfailurecallback() {
    alert(kony.i18n.getLocalizedString("keyPOWFail"));
}

function allpagelocalecheck() {
    gblLocale = true;
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        hbxFooterPrelogin.linkhotpromo.containerWeight = 11;
        hbxFooterPrelogin.linkFindTmb.containerWeight = 12;
        hbxFooterPrelogin.linkExchangeRate.containerWeight = 16;
        hbxFooterPrelogin.linkSiteTour.containerWeight = 10;
        hbxFooterPrelogin.link506459299592952.containerWeight = 11;
        hbxFooterPrelogin.linkTnC.containerWeight = 38;
        hbxIBPreLogin.btnEng.skin = "btnEngLangOff";
        hbxIBPreLogin.btnThai.skin = "btnThaiLangOn";
        hbxIBPostLogin.btnEng.skin = "btnEngLangOff";
        hbxIBPostLogin.btnThai.skin = "btnThaiLangOn";
    } else {
        hbxFooterPrelogin.linkhotpromo.containerWeight = 17;
        hbxFooterPrelogin.linkFindTmb.containerWeight = 12;
        hbxFooterPrelogin.linkExchangeRate.containerWeight = 17;
        hbxFooterPrelogin.linkSiteTour.containerWeight = 11;
        hbxFooterPrelogin.link506459299592952.containerWeight = 13;
        hbxFooterPrelogin.linkTnC.containerWeight = 30;
        hbxIBPreLogin.btnEng.skin = "btnEngLangOn";
        hbxIBPreLogin.btnThai.skin = "btnThaiLangOff";
        hbxIBPostLogin.btnEng.skin = "btnEngLangOn";
        hbxIBPostLogin.btnThai.skin = "btnThaiLangOff";
    }
    hbxFooterPrelogin.linkhotpromo.text = kony.i18n.getLocalizedString("keyIBFooterHotPromotions");
    hbxFooterPrelogin.linkFindTmb.text = kony.i18n.getLocalizedString("keyIBFooterFindTMB");
    hbxFooterPrelogin.linkExchangeRate.text = kony.i18n.getLocalizedString("keyIBFooterExchange Rates");
    hbxFooterPrelogin.linkSiteTour.text = kony.i18n.getLocalizedString("keyIBFooterSiteTour");
    hbxFooterPrelogin.linkTnC.text = kony.i18n.getLocalizedString("keyIBFooterTermsnConditions");
    hbxFooterPrelogin.linkSecurity.text = kony.i18n.getLocalizedString("keySecurityAdvices");
    hbxFooterPrelogin.linkWebChat.text = kony.i18n.getLocalizedString("keyIBFooterWebChat");
    hbxFooterWithoutlinks.linkWebChat.text = kony.i18n.getLocalizedString("keyIBFooterWebChat");
    hbxIBPostLogin.lnkHome.text = kony.i18n.getLocalizedString('keyIBHome');
    hbxFooterPrelogin.link506459299592952.text = kony.i18n.getLocalizedString("keyContactUs");
    //getMonthCycleIB();
    // code to change size of logout link widget
    UiChangesOnLocaleChange();
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        hbxIBPostLogin.lnkLogOut.containerWeight = 9;
        hbxIBPostLogin.lnkLogOut.margin = [0, 0, 0, 6];
        hbxIBPostLogin.label502795003206049.containerWeight = 10;
        hbxIBPostLogin.lnkLogOut.text = kony.i18n.getLocalizedString("keyIBLogout");
        hbxIBPostLogin.lblLastLoginTime.text = kony.i18n.getLocalizedString("keyIBHeaderlblLastLogin");
        hbxIBPostLogin.lblName.text = gblCustomerNameTh;
    } else {
        hbxIBPostLogin.lnkLogOut.containerWeight = 5;
        hbxIBPostLogin.lnkLogOut.margin = [0, 0, 0, 11];
        hbxIBPostLogin.label502795003206049.containerWeight = 14;
        hbxIBPostLogin.lnkLogOut.text = kony.i18n.getLocalizedString("keyIBLogout");
        hbxIBPostLogin.lblLastLoginTime.text = kony.i18n.getLocalizedString("keyIBHeaderlblLastLogin");
        hbxIBPostLogin.lblName.text = gblCustomerName;
    }
    //updateLocaleCrmProfileIB();
    IBcopyright_footer_display();
    IBAppTitle();
    if ((gblLoggedIn == true || gblLoggedIn == "true") && kony.application.getCurrentForm().id != "frmIBPreLogin" && kony.application.getCurrentForm().id != "frmIBForceChangePassword" && kony.application.getCurrentForm().id != "frmIBMigratedUserStep1" && kony.application.getCurrentForm().id != "frmIBFATCATandC" && kony.application.getCurrentForm().id != "frmIBFATCAQuestionnaire") {
        try {
            if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
            else {
                langspecificmenu();
            }
            submenuLangChange();
        } catch (e) {}
    }
    if (kony.application.getCurrentForm().id == "frmIBTOASavingsExecutedTxn") {
        setDatatoSavingscareLabels();
    }
    if (kony.application.getCurrentForm().id == "frmIBPostLoginDashboard") {
        /* removing as per ENH_028 - IB Menu on Home Screen
        submenuLangChange();
        DashboardLangChange();
        */
        footerPagesMenu();
    }
    if (kony.application.getCurrentForm().id == "frmIBActivationTandC") {
        IBTnCText();
    }
    if (kony.application.getCurrentForm().id == "frmIBDreamSavingMaintenance") {
        frmIBDreamSavingMaintenance.label866795318717753.text = kony.i18n.getLocalizedString("KeyMBMonthlySavingfrm");
        frmIBDreamSavingMaintenance.lblOpeningDate.text = kony.i18n.getLocalizedString("keyMBOpeningDate");
        frmIBDreamSavingMaintenance.lblInterestRate.text = kony.i18n.getLocalizedString("InterestRate");
        frmIBDreamSavingMaintenance.lblTranfrEveryMnth.text = kony.i18n.getLocalizedString("keyMBTransferEvryMnth");
        frmIBDreamSavingMaintenance.lblMnthlySavingAmnt.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
        frmIBDreamSavingMaintenance.lblBranch.text = kony.i18n.getLocalizedString("Branch");
        frmIBDreamSavingMaintenance.label866795318717737.text = kony.i18n.getLocalizedString("keylblAccountDetails");
        frmIBDreamSavingMaintenance.label866795318717734.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");
        frmIBDreamSavingMaintenance.label866795318717733.text = kony.i18n.getLocalizedString("keyMBMydream");
        frmIBDreamSavingMaintenance.label474288733625.text = kony.i18n.getLocalizedString("keylblMyAccount");
        /*    if (DreamCustomerNicknameIB != null) {
                frmIBDreamSavingMaintenance.label866795318717757.text = DreamCustomerNicknameIB;
            } else {
                if (kony.i18n.getCurrentLocale() == "en_US") {

                    frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameENG;


                } else {
                    frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameTH;

                }
            }*/
    }
    if (kony.application.getCurrentForm().id == "frmIBDreamSAvingEdit") {
        frmIBDreamSAvingEdit.label474288733625.text = kony.i18n.getLocalizedString("keylblEditMyAccnt");
        frmIBDreamSAvingEdit.label86679531838298.text = kony.i18n.getLocalizedString("keyMBMydream");
        frmIBDreamSAvingEdit.lblNickname.text = kony.i18n.getLocalizedString("keyNickname");
        frmIBDreamSAvingEdit.lblDreamDesc.text = kony.i18n.getLocalizedString("DreamDes");
        frmIBDreamSAvingEdit.lblTargetAmntstudio20.text = kony.i18n.getLocalizedString("keyMbTargetAmnt"); //Modified by Studio Viz
        frmIBDreamSAvingEdit.lblSetSaving.text = kony.i18n.getLocalizedString("kayMBSetNewSaving");
        frmIBDreamSAvingEdit.button474970204318024.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBDreamSAvingEdit.button474970204318026.text = kony.i18n.getLocalizedString("keyConfirm");
        frmIBDreamSAvingEdit.label474076474302563.text = kony.i18n.getLocalizedString("keyIBbankrefno");
        frmIBDreamSAvingEdit.label474076474302550.text = kony.i18n.getLocalizedString("keyotpmsg");
        if (gblSwitchToken == false && gblTokenSwitchFlag == true) frmIBDreamSAvingEdit.lblOTPMsgReq.text = kony.i18n.getLocalizedString("Receipent_tokenId");
        else frmIBDreamSAvingEdit.lblOTPMsgReq.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBDreamSAvingEdit.lblHdrTxtview.text = kony.i18n.getLocalizedString("keylblCalculateMonthlySAving");
        frmIBDreamSAvingEdit.lbltargetamntstudio21.text = kony.i18n.getLocalizedString("keyMbTargetAmnt"); //Modified by Studio Viz
        frmIBDreamSAvingEdit.lblMnthlySaving.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
        frmIBDreamSAvingEdit.lblMnthTODReamIB.text = kony.i18n.getLocalizedString("keyMBMonthToMyDream");
        frmIBDreamSAvingEdit.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBDreamSAvingEdit.btnAgree.text = kony.i18n.getLocalizedString("Next");
        frmIBDreamSAvingEdit.lblor.text = kony.i18n.getLocalizedString("keyor");
        frmIBDreamSAvingEdit.lblmnthsaveval.text = amountDreamMnthRem + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "/" + kony.i18n.getLocalizedString("keyCalendarMonth");
        setDataforDreamActsIB(gblOpenDrmAcctIB);
    }
    if (kony.application.getCurrentForm().id == "frmIBChequeServiceViewReturnedCheque") {
        var headertext = frmIBChequeServiceViewReturnedCheque.dgfullstmt.columnHeadersConfig;
        headertext[0].columnheadertemplate.data.lbldate.text = kony.i18n.getLocalizedString("keyIBPostDate");
        headertext[1].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("lblChequeNoIB");
        headertext[2].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("keyBankName");
        headertext[3].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Amount");
        headertext[4].columnheadertemplate.data.lbltransid.text = kony.i18n.getLocalizedString("MyActivitiesIB_Reason");
        loadsegdataInReturnCheque(currentpageStmt);
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenProdDetnTnC") {
        //var selectedProdList = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["productcode"];
        var locale = kony.i18n.getCurrentLocale();
        if (frmIBOpenProdDetnTnC.richTextProdDetIB.isVisible == true) {
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
                frmIBOpenProdDetnTnC.lblOpenAccIntType.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
            } else {
                frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
                frmIBOpenProdDetnTnC.lblOpenAccIntType.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
            }
            if (gblSelOpenActProdCode == "220") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyNoFeeProdDet');
            } else if (gblSelOpenActProdCode == "200") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyNormalSavProdDet');
            } else if (gblSelOpenActProdCode == "221") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyNoFixedProdDet');
            } else if (gblSelOpenActProdCode == "222") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyFreeFlowProdDet');
            } else if (gblSelOpenActProdCode == "206") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyDreamProdDet');
            } else if (gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != null && gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != undefined && gblOpenActList["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0) {
                frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = "";
            } else if (gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301" || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermProdDet');
            } else if (gblSelOpenActProdCode == "659") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermUpProdDet');
            } else if (gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermQuickProdDet');
            } else if (gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined && gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0) {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString(product_details_i18n_key);
            }
        } else {
            frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
        }
        if (gblCardOpenAccount) {
            frmIBOpenProdDetnTnC.lblOpenNewAccount.text = kony.i18n.getLocalizedString("keyOpenNewAccountAndDebitCard");
        } else {
            frmIBOpenProdDetnTnC.lblOpenNewAccount.text = kony.i18n.getLocalizedString("kelblOpenNewAccount");
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewTermDepositAccConfirmation") {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewTermDepositAccConfirmation.lblIntrstRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewTermDepositAccConfirmation.lblPayingToName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"];
            frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenTDEN"]
        } else {
            frmIBOpenNewTermDepositAccConfirmation.lblIntrstRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewTermDepositAccConfirmation.lblPayingToName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOTH"];
            frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTH"];
        }
        if (savingMBTDOpen == "SDA") {
            frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccount.text = kony.i18n.getLocalizedString("SavingMB");
        } else if (currentMBTDOpen == "DDA") {
            frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccount.text = kony.i18n.getLocalizedString("CurrentMB");
        }
    }
    if ((kony.application.getCurrentForm().id == "frmIBDreamSavingMaintenance") && gblDreamAcountCheck == "false") {
        if (DreamCustomerNicknameIB != null) {
            frmIBDreamSavingMaintenance.label866795318717757.text = DreamCustomerNicknameIB;
        } else {
            if (kony.i18n.getCurrentLocale() == "en_US") frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameENG;
            else frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameTH;
        }
        if (linkedAccType == "SDA") {
            accTypeDream = kony.i18n.getLocalizedString("keySavingIB");
        } else if (linkedAccType == "CDA") {
            accTypeDream = kony.i18n.getLocalizedString("keyTermDepositIB");
        } else if (linkedAccType == "DDA") {
            accTypeDream = kony.i18n.getLocalizedString("keyCurrentIB");
        } else if (linkedAccType == "LOC") {
            accTypeDream = kony.i18n.getLocalizedString("keylblLoan");
        } else {
            accTypeDream = ""
        }
        frmIBDreamSavingMaintenance.lblAccType.text = accTypeDream;
    }
    if ((kony.application.getCurrentForm().id == "frmIBDreamSavingMaintenance") && gblDreamAcountCheck == "true") {
        if (DreamCustomerNicknameIB != null) {
            frmIBDreamSavingMaintenance.label866795318717757.text = DreamCustomerNicknameIB;
        } else {
            if (kony.i18n.getCurrentLocale() == "en_US") frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameENG;
            else frmIBDreamSavingMaintenance.label866795318717757.text = DreamProductnameTH;
        }
        if (kony.i18n.getCurrentLocale() == "en_US") {
            accTypeDream = gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
            //accTypeDream=gblAccountTable["custAcctRec"][gblIndex]["Type_EN"];
        } else {
            accTypeDream = gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
            //accTypeDream=gblAccountTable["custAcctRec"][gblIndex]["Type_TH"];
        }
        frmIBDreamSavingMaintenance.lblAccType.text = accTypeDream;
    }
    if (kony.application.getCurrentForm().id == "frmIBATMBranch" && gblLoggedIn == false) {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBATMBranch["imgActivate"]["src"] = "aw_banner01.jpg";
        } else {
            frmIBATMBranch["imgActivate"]["src"] = "aw_banner02.jpg";
        }
        if (gblBrachTypeIB == "ATM") {
            frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
        } else if (gblBrachTypeIB == "BRANCH") {
            frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
        } else if (gblBrachTypeIB == "EX") {
            frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
        }
        onSuccesATMSearchIB(400, gblFindTMBMapData);
    } else if (kony.application.getCurrentForm().id == "frmIBATMBranch" && gblLoggedIn == true) {
        footerPagesMenu();
        if (gblBrachTypeIB == "ATM") {
            frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ATMDetails");
        } else if (gblBrachTypeIB == "BRANCH") {
            frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_BranchDetails");
        } else if (gblBrachTypeIB == "EX") {
            frmIBATMBranch.lblTitle.text = kony.i18n.getLocalizedString("FindTMB_ExgBoothDetails");
        }
        onSuccesATMSearchIB(400, gblFindTMBMapData);
    }
    if (kony.application.getCurrentForm().id == "frmIBExchangeRates" && gblLoggedIn == false) {
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBExchangeRates["imgActivate"]["src"] = "aw_banner01.jpg";
        } else {
            frmIBExchangeRates["imgActivate"]["src"] = "aw_banner02.jpg";
        }
    } else if (kony.application.getCurrentForm().id == "frmIBExchangeRates" && gblLoggedIn == true) {
        footerPagesMenu();
    } else if (kony.application.getCurrentForm().id == "frmIBAnyIdAnnoucment") {
        settingAnyIdAnnoucementPageKeys();
    }
    //var myCurrentFrm = kony.application.getCurrentForm()
    var currentFrmWdgts = kony.application.getCurrentForm()["ownchildrenref"];
    var size = currentFrmWdgts.length;
    traverse(currentFrmWdgts, size)
        /*
         * Function which changes module specific forms as per current page
         */
    syncIBLocale();
    //myCurrentFrm.show() ; 
    //kony.application.getCurrentForm().preShow();
    //frmMBanking.preShow();
}

function traverse(currentFrmWdgts, size) {
    for (var i = 0; i < size; i++) {
        if (currentFrmWdgts[i]["ownchildrenref"] != undefined) {
            var childWidgets = currentFrmWdgts[i]["ownchildrenref"];
            var childSize = childWidgets.length;
            traverse(childWidgets, childSize);
        }
    }
    findi18(currentFrmWdgts);
}

function findi18(currentFrmWdgts) {
    for (var i = 0; i < currentFrmWdgts.length; i++) {
        if (currentFrmWdgts[i].i18n_text == undefined || currentFrmWdgts[i].i18n_text == null || currentFrmWdgts[i].i18n_text == "") continue;
        else {
            var temp = currentFrmWdgts[i].i18n_text;
            currentFrmWdgts[i].text = kony.i18n.getLocalizedString(temp.slice(30, (temp.length - 2)));
            currentFrmWdgts[i].i18n_text = temp;
        }
    }
}
//function startupSetLang(lang) {
//	
//	kony.i18n.setCurrentLocaleAsync(lang, onstartupSetLangsuccesscallback, onstartupSetLangfailurecallback, "");
//}
//function onstartupSetLangsuccesscallback() {
//	
//}
//function onstartupSetLangfailurecallback() {
//	
//}
/*function updateLocaleCrmProfileIB(){


	if (gblLoggedIn == true || gblLoggedIn == "true"){
	
		var inputParam = {};
		inputParam["channelName"] = "IB-INQ";
		inputParam["ibUserId"] = gblUserName;
		inputParam["actionType"] = "0";
		var locale = kony.i18n.getCurrentLocale();
			if (locale == "en_US") {
				inputParam["languageCd"] = "EN";
			} else {
				inputParam["languageCd"] = "TH";
			}
		
		invokeServiceSecureAsync("crmProfileMod", inputParam, callBackCrmProfileLocaleUpdateIB)
	}
}

function callBackCrmProfileLocaleUpdateIB(status,resultTable){
	if (status == 400) {
		
	} else {
		
	}
}*/
function UiChangesOnLocaleChange() {
    syncIBBeepAndBill();
    syncIBMyAccountSummary();
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBOpenNewSavingsAccConfirmation.lblInputOTP.containerWeight = 13;
        frmIBOpenNewSavingsAccConfirmation.txtOTP.containerWeight = 56;
        frmIBOpenNewDreamAccConfirmation.lblInputOTP.containerWeight = 13;
        frmIBOpenNewDreamAccConfirmation.txtOTP.containerWeight = 56;
        frmIBOpenNewTermDepositAccConfirmation.lblInputOTP.containerWeight = 13;
        frmIBOpenNewTermDepositAccConfirmation.txtOTP.containerWeight = 56;
        frmIBOpenNewSavingsCareAccConfirmation.lblInputOTP.containerWeight = 13;
        frmIBOpenNewSavingsCareAccConfirmation.txtOTP.containerWeight = 56;
        frmIBCMConfirmation.label476047582115284.containerWeight = 13;
        frmIBCMConfirmation.txtOTP.containerWeight = 56;
        //frmIBSSApplyCnfrmtn.lblOTPKey.containerWeight=13;
        //frmIBSSApplyCnfrmtn.txtBxOTP.containerWeight=56;
        //frmIBDreamSAvingEdit.label474076474302556.containerWeight=45;
        //frmIBDreamSAvingEdit.txtBxOTP.containerWeight=20;
        //frmIBBeepAndBillConfAndComp.label4510182417421.containerWeight=13;
        //frmIBBeepAndBillConfAndComp.txtBxOTPBB.containerWeight=30;
        frmIBOpenNewDreamAcc.lblPictureIT.containerWeight = 16;
        frmIBDreamSAvingEdit.lblDreamDesc.containerWeight = 37;
        frmIBTransferNowConfirmation.lblOTPKey.containerWeight = 11;
        frmIBBillPaymentCompletenow.lblBalance.containerWeight = 51
        frmIBBillPaymentCompletenow.lblBBPaymentValue.containerWeight = 27
        frmIBBillPaymentCompletenow.btnHide.containerWeight = 11
        frmIBBillPaymentCompletenow.lblHide.containerWeight = 11
        frmIBTranferLP.lblXferMyNote.containerWeight = 20;
        frmIBFTrnsrEditCnfmtn.lblOTP.containerWeight = 11;
    } else {
        frmIBBillPaymentCompletenow.lblBalance.containerWeight = 16
        frmIBDreamSAvingEdit.lblDreamDesc.containerWeight = 14;
        frmIBOpenNewDreamAcc.lblPictureIT.containerWeight = 23;
        frmIBOpenNewSavingsAccConfirmation.lblInputOTP.containerWeight = 23;
        frmIBOpenNewSavingsAccConfirmation.txtOTP.containerWeight = 50;
        frmIBOpenNewDreamAccConfirmation.lblInputOTP.containerWeight = 23;
        frmIBOpenNewDreamAccConfirmation.txtOTP.containerWeight = 50;
        frmIBOpenNewTermDepositAccConfirmation.lblInputOTP.containerWeight = 23;
        frmIBOpenNewTermDepositAccConfirmation.txtOTP.containerWeight = 50;
        frmIBOpenNewSavingsCareAccConfirmation.lblInputOTP.containerWeight = 23;
        frmIBOpenNewSavingsCareAccConfirmation.txtOTP.containerWeight = 50;
        frmIBCMConfirmation.label476047582115284.containerWeight = 23;
        frmIBCMConfirmation.txtOTP.containerWeight = 50;
        //frmIBSSApplyCnfrmtn.lblOTPKey.containerWeight=23;
        //frmIBSSApplyCnfrmtn.txtBxOTP.containerWeight=50;
        //frmIBDreamSAvingEdit.label474076474302556.containerWeight=45;
        //frmIBDreamSAvingEdit.txtBxOTP.containerWeight=20;
        //frmIBBeepAndBillConfAndComp.label4510182417421.containerWeight=25;
        //frmIBBeepAndBillConfAndComp.txtBxOTPBB.containerWeight=30;
        frmIBTranferLP.lblXferMyNote.containerWeight = 24;
        frmIBTransferNowConfirmation.lblOTPKey.containerWeight = 22;
        frmIBFTrnsrEditCnfmtn.lblOTP.containerWeight = 20;
        //frmIBTranferLP.lblXferMyNote.containerWeight=25;
    }
}