function onClickQuickBalanceMenu() {
    if (gblQuickBalanceFromLogin) {
        gblGotoQuickSetting = false;
        onClickQuickBalanceBriefNext();
        gblQuickBalanceFromLogin = false;
    } else {
        gblGotoQuickSetting = true;
        getQuickBalanceEligibleAccounts(); //Doing logic here to figure out if user has atleast one product enabled. Then skip Brief
    }
}

function frmMBQuickBalanceBriefPreShow() {
    changeStatusBarColor();
    var currentLocale = kony.i18n.getCurrentLocale();
    if (currentLocale == "th_TH") {
        frmMBQuickBalanceBrief.imgQuickBalanceFeature.src = "quick_balance_th.png";
    } else {
        frmMBQuickBalanceBrief.imgQuickBalanceFeature.src = "quick_balance.png";
    }
    frmMBQuickBalanceBrief.lblHdrTxt.text = ""; //kony.i18n.getLocalizedString("keyEStmt");
    frmMBQuickBalanceBrief.lblQuickBalanceText1.text = kony.i18n.getLocalizedString("BQB_MSG01");
    frmMBQuickBalanceBrief.lblQuickBalanceText2.text = kony.i18n.getLocalizedString("BQB_MSG02");
    frmMBQuickBalanceBrief.btnCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
    frmMBQuickBalanceBrief.btnNext.text = kony.i18n.getLocalizedString('Next');
    frmMBQuickBalanceBrief.scrollboxMain.scrollToEnd();
}

function onClickQuickBalanceBriefNext() {
    //changeLocalOnQuickBalanceBrief();
    frmMBQuickBalanceSetting.show();
}

function frmMBQuickBalanceSettingPreShow() {
    changeStatusBarColor();
    frmMBQuickBalanceSetting.lblHdrTxt.text = kony.i18n.getLocalizedString("SQB_Title");
    frmMBQuickBalanceSetting.lblQuickBalanceText.text = kony.i18n.getLocalizedString("SQB_MSG01");
    frmMBQuickBalanceSetting.btnReturnToHome.text = kony.i18n.getLocalizedString('SQB_BtnHome');
    if (!gblGotoQuickSetting) getQuickBalanceEligibleAccounts();
    gblGotoQuickSetting = false;
    frmMBQuickBalanceSetting.scrollboxMain.scrollToEnd();
    if (gblQuickBalanceAcctResponse != null) {
        changeLocalOnQuickBalanceBrief();
    }
}

function changeLocalOnQuickBalanceBrief() {
    if (undefined != gblQuickBalanceAcctResponse["custAcctRec"]) {
        callBackGetQuickBalanceEligibleAccounts(400, gblQuickBalanceAcctResponse);
    }
}

function onClickSwitchQuickBalanceSetting() {
    quickSelectedIndex = frmMBQuickBalanceSetting.segQuickBalanceSetting.selectedIndex[1];
    var quickSelectedDataObject = frmMBQuickBalanceSetting.segQuickBalanceSetting.data[quickSelectedIndex];
    var quickBalanceStatus = "01";
    showLoadingScreen();
    var input_param = {};
    input_param["accountNumber"] = quickSelectedDataObject.hdnAccountno;
    if (quickSelectedDataObject.btnPhCheckBox.skin == "btnCheckFoc" || quickSelectedDataObject.switchMobile.selectedIndex == 0) {
        quickBalanceStatus = "02";
    }
    input_param["quickBalanceStatus"] = quickBalanceStatus;
    invokeServiceSecureAsync("quickBalanceSetting", input_param, quickBalanceSettingCallBack);
}

function quickBalanceSettingCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            var selectedData = frmMBQuickBalanceSetting.segQuickBalanceSetting.data[quickSelectedIndex];
            var btnPhCheckBoxSkin = btnCheck,
                switchMobileValue = 1;
            if (selectedData.btnPhCheckBox.skin == "btnCheck" || selectedData.switchMobile.selectedIndex == 1) {
                btnPhCheckBoxSkin = btnCheckFoc;
                switchMobileValue = 0;
            }
            selectedData["btnPhCheckBox"] = {
                "skin": btnPhCheckBoxSkin
            };
            selectedData["switchMobile"] = {
                "selectedIndex": switchMobileValue
            };
            frmMBQuickBalanceSetting.segQuickBalanceSetting.setDataAt(selectedData, quickSelectedIndex);
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function getQuickBalanceEligibleAccounts() {
    showLoadingScreen();
    var inputParam = {}
    inputParam["quickBalanceFlag"] = "true";
    invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackGetQuickBalanceEligibleAccounts)
}

function callBackGetQuickBalanceEligibleAccounts(status, resulttable) {
    if (status == 400) {
        gblQuickBalanceAcctResponse = resulttable;
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            //populate something.
            quickSettingData = [];
            var iphoneSwitchVisibility = false,
                androidSwitchVisibility = false,
                atleastOneAccountEnabled = false;
            if (undefined != resulttable["custAcctRec"]) {
                for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                    var accountName = "",
                        accountNo = "",
                        seeBalanceKey = "",
                        quickBalanceSetting = "",
                        btnPhCheckBoxSkin = btnCheck,
                        switchMobileValue = 1;
                    var accId = resulttable["custAcctRec"][i]["accId"];
                    if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == '') {
                        var sbStr = accId;
                        var templength = sbStr.length;
                        sbStr = sbStr.substring(templength - 4, templength);
                        if (kony.i18n.getCurrentLocale() == "th_TH") accountName = resulttable["custAcctRec"][i]["ProductNameThai"] + " " + sbStr;
                        else accountName = resulttable["custAcctRec"][i]["ProductNameEng"] + " " + sbStr;
                    } else {
                        accountName = resulttable["custAcctRec"][i]["acctNickName"];
                    }
                    if (resulttable["custAcctRec"][i]["accType"] == kony.i18n.getLocalizedString("CreditCard")) {
                        accountNo = accId.substring(accId.length - 16, accId.length);
                        seeBalanceKey = "AvailableCreditLimit";
                    } else {
                        accountNo = accId.substring(accId.length - 10, accId.length);
                        seeBalanceKey = "Balance";
                    }
                    quickBalanceSetting = resulttable["custAcctRec"][i]["QuickBalanceEnabled"];
                    if ("01" == quickBalanceSetting) {
                        btnPhCheckBoxSkin = btnCheckFoc;
                        switchMobileValue = 0;
                        atleastOneAccountEnabled = true;
                    }
                    tempSegmentRecord = {
                        "lblAccountNickname": accountName,
                        "lblSeeBalance": removeColonFromEnd(kony.i18n.getLocalizedString(seeBalanceKey)),
                        "hbxAndroidSwitch": {
                            isVisible: androidSwitchVisibility
                        },
                        "hbxIphoneSwitch": {
                            isVisible: iphoneSwitchVisibility
                        },
                        "btnPhCheckBox": {
                            skin: btnPhCheckBoxSkin
                        },
                        "switchMobile": {
                            selectedIndex: switchMobileValue
                        },
                        "hdnAccountno": accountNo
                    };
                    quickSettingData.push(tempSegmentRecord);
                }
                frmMBQuickBalanceSetting.segQuickBalanceSetting.setData(quickSettingData);
                if (gblGotoQuickSetting && atleastOneAccountEnabled) {
                    onClickQuickBalanceBriefNext();
                } else if (gblGotoQuickSetting) {
                    frmMBQuickBalanceBrief.show();
                }
            } else {
                showAlertWithCallBack(kony.i18n.getLocalizedString("VQB_NoEligibleAccount"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
                return false;
            }
        }
    }
}