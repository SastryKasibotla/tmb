function frmMBCashAdvanceCardInfoMenuPreshow(){
	if (gblCallPrePost) 
	{
		
		frmMBCashAdvanceCardInfo.lblCardName.text = "";
		frmMBCashAdvanceCardInfo.lblCardNumber.skin = getCardNoSkin();
		frmMBCashAdvanceCardInfo.lblCardNumber.text = frmMBManageCard.lblCardNumber.text;
		frmMBCashAdvanceCardInfo.lblCardAcctName.text = frmMBManageCard.lblCardAccountName.text;
		
		frmMBCashAdvanceCardInfo.imgCard.src = frmMBManageCard.imgCard.src;
		frmMBCashAdvanceCardInfoLocalePreshow();
	}
}

function frmMBCashAdvanceCardInfoLocalePreshow(){
	changeStatusBarColor();
	frmMBCashAdvanceCardInfo.lblCashAdvanceTitle.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
	frmMBCashAdvanceCardInfo.lblRemDailyLimit.text = kony.i18n.getLocalizedString("CAV02_keyRCreditLimit");
	frmMBCashAdvanceCardInfo.lblAvailSpend.text = kony.i18n.getLocalizedString("CAV02_keyAvailToSpend");
	frmMBCashAdvanceCardInfo.lblAvailCashAdv.text = kony.i18n.getLocalizedString("CAV02_keyAvailCA");
	frmMBCashAdvanceCardInfo.btnBack.text = kony.i18n.getLocalizedString("CAV02_btnBack");
	frmMBCashAdvanceCardInfo.btnNext.text = kony.i18n.getLocalizedString("CAV02_btnNext");
}

function frmMBCashAdvanceCardInfoMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBCashAdvAcctSelectMenuPreshow(){
	if (gblCallPrePost) 
	{
		frmMBCashAdvAcctSelectLocalePreshow();
	}
}

function frmMBCashAdvAcctSelectLocalePreshow(){
	changeStatusBarColor();
	frmMBCashAdvAcctSelect.lblCashAdvanceTitle.text = kony.i18n.getLocalizedString("CAV06_keyTitle");
	frmMBCashAdvAcctSelect.lblCancel.text = kony.i18n.getLocalizedString("CAV06_btnCancel");
}

function frmMBCashAdvAcctSelectMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}


function frmMBCashAdvanceTnCMenuPreshow(){
	if (gblCallPrePost) 
	{
		frmMBCashAdvanceTnCLocalePreshow();
	}
}

function frmMBCashAdvanceTnCLocalePreshow(){
	changeStatusBarColor();
	frmMBCashAdvanceTnC.lblCashAdvanceHdrTxt.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
	frmMBCashAdvanceTnC.btnCashAdvanceBack.text = kony.i18n.getLocalizedString("CAV02_btnBack");
	frmMBCashAdvanceTnC.btnCashAdvanceAgree.text = kony.i18n.getLocalizedString("CAV03_btnAgree");
	frmMBCashAdvanceTnC.flxSaveCamEmail.isVisible = false;
	frmMBCashAdvanceTnC.btnRight.skin = "btnRightShare";
	loadTermsNConditionCashAdvanceMB();
}

function frmMBCashAdvanceTnCMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
		//loadTermsNConditionCashAdvanceMB();
    }
    assignGlobalForMenuPostshow();
}

function frmCashAdvancePlanDetailsMenuPreshow(){
	if (gblCallPrePost) 
	{
		frmCashAdvancePlanDetails.lblCardNum.text = frmMBCashAdvanceCardInfo.lblCardNumber.text;
		frmCashAdvancePlanDetails.lblCardNum.skin = getCardNoSkin();
		frmCashAdvancePlanDetails.lblCustName.text = frmMBCashAdvanceCardInfo.lblCardAcctName.text;
		frmCashAdvancePlanDetails.imgMainCard.src = frmMBCashAdvanceCardInfo.imgCard.src;
		frmCashAdvancePlanDetailsLocalePreshow();
	}
}

function frmCashAdvancePlanDetailsLocalePreshow(){
	changeStatusBarColor();
	frmCashAdvancePlanDetails.flexBodyScroll.scrollToWidget(frmCashAdvancePlanDetails.flxAmount);
	frmCashAdvancePlanDetails.lblHeader.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
	frmCashAdvancePlanDetails.lblAvailableCashDesc.text = kony.i18n.getLocalizedString("CAV04_keyAvail") + " " + gblAvailableBalance;
	frmCashAdvancePlanDetails.lblAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");
	frmCashAdvancePlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");
	frmCashAdvancePlanDetails.lblFullPayment.text = kony.i18n.getLocalizedString("CAV04_plFullPayment");
	frmCashAdvancePlanDetails.lblPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
	frmCashAdvancePlanDetails.lblFeeDesc.text = kony.i18n.getLocalizedString("CAV04_keyFee");
	frmCashAdvancePlanDetails.lblDebitCardDesc.text = kony.i18n.getLocalizedString("CAV04_keyDebitOn");
	frmCashAdvancePlanDetails.lblReveivedByDesc.text = kony.i18n.getLocalizedString("CAV04_keyReceivedBy");
	frmCashAdvancePlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime01");
	frmCashAdvancePlanDetails.lblAccount.text = kony.i18n.getLocalizedString("CAV04_keyToAct");
	frmCashAdvancePlanDetails.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
	if(frmCashAdvancePlanDetails.rchTxtNote.isVisible){
		var intRate = kony.string.replace(frmCashAdvancePlanDetails.lblAnnualRateVal.text, "%", "");
		var msgPro = kony.i18n.getLocalizedString("msgPromotionRateWarning");
		msgPro = msgPro.replace("{Annual_Rate}", intRate);
		frmCashAdvancePlanDetails.rchTxtNote.text = msgPro;
	}
	
	if(frmCashAdvancePlanDetails.flxEnterFullAmountDtls.isVisible){
		veriflyEnterAmountValueCA();
	}else{
		frmCashAdvancePlanDetails.flxAmount.setVisibility(true);
		frmCashAdvancePlanDetails.flxEnterFullAmountDtls.setVisibility(false);
	}
	
	if(isNotBlank(gblCASelectedToAcctNum)){
		if(kony.i18n.getCurrentLocale() == "en_US"){
			frmCashAdvancePlanDetails.lblSelectedAccount.text = gblCASelectedToAcctNameEN;
		}else{
			frmCashAdvancePlanDetails.lblSelectedAccount.text = gblCASelectedToAcctNameTH;
		}
	}else{
		frmCashAdvancePlanDetails.lblSelectedAccount.text = kony.i18n.getLocalizedString("CAV04_plToAct");
	}
	frmCashAdvancePlanDetails.lblReceivedDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
	frmCashAdvancePlanDetails.lblDebitCardDate.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
	frmCashAdvancePlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
	frmCashAdvancePlanDetails.btnBack.text = kony.i18n.getLocalizedString("CAV04_btnBack");
	frmCashAdvancePlanDetails.btnNext.text = kony.i18n.getLocalizedString("CAV04_btnNext");
	//frmCashAdvancePlanDetails.lblEnterAmount.text = commaFormatted(gblMinCashAdvanceAmount);
}

function frmCashAdvancePlanDetailsMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmCAPaymentPlanConfirmationPreshow(){
    if (gblCallPrePost) {
		frmCAPaymentPlanConfirmation.imgCard.src = frmCashAdvancePlanDetails.imgMainCard.src;
		frmCAPaymentPlanConfirmation.lblCardNumber.text = frmCashAdvancePlanDetails.lblCardNum.text;
		frmCAPaymentPlanConfirmation.lblCardNumber.skin = getCardNoSkin();
		frmCAPaymentPlanConfirmation.lblCardName.text = frmCashAdvancePlanDetails.lblCustName.text;
		
    	frmCAPaymentPlanConfirmationLocalePreshow();
    }
}

function frmCAPaymentPlanConfirmationLocalePreshow(){
	changeStatusBarColor();
	frmCAPaymentPlanConfirmation.lblHde.text = kony.i18n.getLocalizedString("CAV07_keyTitle");
	frmCAPaymentPlanConfirmation.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle") 
														+ " " + frmCashAdvancePlanDetails.lblEnterAmount.text
														+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	frmCAPaymentPlanConfirmation.lblFrom.text = kony.i18n.getLocalizedString("CAV07_keyFrom");
	frmCAPaymentPlanConfirmation.lblTo.text = kony.i18n.getLocalizedString("CAV07_keyTo");
	frmCAPaymentPlanConfirmation.lblReference.text = kony.i18n.getLocalizedString("CAV07_keyRefNo");
	frmCAPaymentPlanConfirmation.lblAmount.text = kony.i18n.getLocalizedString("CAV07_keyAmt");
	frmCAPaymentPlanConfirmation.lblFee.text = kony.i18n.getLocalizedString("CAV04_keyFee");
	frmCAPaymentPlanConfirmation.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
	frmCAPaymentPlanConfirmation.lblDebitCardOn.text = kony.i18n.getLocalizedString("CAV04_keyDebitOn");
	frmCAPaymentPlanConfirmation.lblRecieveDate.text = kony.i18n.getLocalizedString("CAV04_keyReceivedBy");
	frmCAPaymentPlanConfirmation.btnBack.text = kony.i18n.getLocalizedString("CAV07_btnCancel");
	frmCAPaymentPlanConfirmation.btnNext.text = kony.i18n.getLocalizedString("CAV07_btnConfirm");
}

function frmCAPaymentPlanConfirmationPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmCAPaymentPlanCompletePreshow(){
    if (gblCallPrePost) {
		frmCAPaymentPlanCompleteLocalePreshow();
    }
}

function frmCAPaymentPlanCompleteLocalePreshow(){
	changeStatusBarColor();
	frmCAPaymentPlanComplete.lblHde.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
	frmCAPaymentPlanComplete.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle") 
														+ " " + frmCAPaymentPlanComplete.lblAmountVal.text;
	frmCAPaymentPlanComplete.lblFrom.text = kony.i18n.getLocalizedString("CAV07_keyFrom");
	frmCAPaymentPlanComplete.lblTo.text = kony.i18n.getLocalizedString("CAV07_keyTo");
	frmCAPaymentPlanComplete.lblReference.text = kony.i18n.getLocalizedString("CAV07_keyRefNo");
	frmCAPaymentPlanComplete.lblAmount.text = kony.i18n.getLocalizedString("CAV07_keyAmt");
	frmCAPaymentPlanComplete.lblFee.text = kony.i18n.getLocalizedString("CAV04_keyFee");
	frmCAPaymentPlanComplete.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
	frmCAPaymentPlanComplete.lblDebitCardOn.text = kony.i18n.getLocalizedString("CAV04_keyDebitOn");
	frmCAPaymentPlanComplete.lblRecieveDate.text = kony.i18n.getLocalizedString("CAV04_keyReceivedBy");
	frmCAPaymentPlanComplete.lblManageOtherCards.text = kony.i18n.getLocalizedString("CAV08_btnManageOther");
}

function frmCAPaymentPlanCompletePostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

