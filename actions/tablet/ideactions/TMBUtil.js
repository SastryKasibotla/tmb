var TMBUtil = new function(){

	var controller = {};
	
	// To call this function: TMBUtil.DestroyForm(formObj)
	controller.DestroyForm = function(frmObj){
		frmObj.info = {};
		frmObj.destroy();
	}
	
	return controller;
}