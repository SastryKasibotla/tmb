/*
************************************************************************
            Name    : hideUsername()
            Author  : Shubhanshu Yadav
            Date    : January 02, 2013
            Purpose : Formats the username to show only the last 4 digits.
        Input params: nil
       Output params: na
        ServiceCalls: nil
        Called From : adddatainseg() function
************************************************************************
 */
//function formatUsername() {
//	str = "******";
//	return str.concat(userName.slice(6, 10));
//}
/*
************************************************************************
            Name    : adddatainseg()
            Author  : Shubhanshu Yadav
            Date    : January 02, 2013
            Purpose : Populates the segment in the frmTwo
        Input params: nil
       Output params: na
        ServiceCalls: nil
        Called From : preShow() event of frmTwo
************************************************************************
 */
//
//function adddatainseg() {
//	//var a = formatUsername();
//	var dataseg = [{
//		lblAccntType: " TMB Savings Account",
//		lblAccntTypeInfo: "",
//		lblAccntBal: "THB 2000 k",
//		lblAccntHolder: "John Doe",
//		imgCircle: "",
//		imgArrow: "chevron3g.png",
//		imgOne: "dep3g.png",
//		LblNumber: getMaskednumber(userName)
//	}, {
//		lblAccntType: " TMB Checking Account",
//		lblAccntTypeInfo: "",
//		lblAccntBal: "THB 2000 k",
//		lblAccntHolder: "Sam Adams",
//		imgCircle: "",
//		imgArrow: "chevron3g.png",
//		imgOne: "pb3g.png",
//		LblNumber: "123123123123"
//	}, {
//		lblAccntType: " TMB No Fee Account",
//		lblAccntTypeInfo: "",
//		lblAccntBal: "THB 2000 k",
//		lblAccntHolder: "Mike Keane",
//		imgCircle: "alert3g.png",
//		imgArrow: "chevron3g.png",
//		imgOne: "tb3g.png",
//		LblNumber: getMaskednumber(userName)
//	}];
//	frmTransFrom.segDetails.setData(dataseg);
//}
/*
************************************************************************
            Name    : populateDetails()
            Author  : Shubhanshu Yadav
            Date    : January 02, 2013
            Purpose : Populates the segment and enables the back button in the frmTransfer
        Input params: nil
       Output params: na
        ServiceCalls: nil
        Called From : rowClick() event of frmTwo
************************************************************************
 */
//function populateDetails() {
//	var a = frmTransApprove.headers;
//	a[0].btnBack.setEnabled(true);
//	var dataseg = [{
//		lbl1: "From",
//		lbl2: getMaskednumber(userName)
//	}, {
//		lbl1: "To",
//		lbl2: "******1234"
//	}, {
//		lbl1: "Amount",
//		lbl2: "THB 10000"
//	}, {
//		lbl1: "Fee",
//		lbl2: "NA"
//	}, {
//		lbl1: "Transfer Date",
//		lbl2: "Jan 14, 2011"
//	}, {
//		lbl1: "Notes",
//		lbl2: "Test"
//	}, {
//		lbl1: "Transfer Type",
//		lbl2: "TMB Transfer"
//	}];
//	frmTransApprove.segTransfer.setData(dataseg);
//}