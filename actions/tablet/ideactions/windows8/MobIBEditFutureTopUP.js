/*function callBackCustomerBillInqForEditTPService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			callPaymentInqServiceForEditTP();
		} else {
			dismissLoadingScreenPopup();
			
		}
	}
}*/
/*
   **************************************************************************************
		Module	: forTPBillPmtInq
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function forTPBillPmtInq() {
    var inputParam = [];
    var tranCodeTP;
    if (gblFromAccTypeTP == "DDA") {
        tranCodeTP = "88" + "10";
    } else {
        tranCodeTP = "88" + "20";
    }
    inputParam["compCode"] = gblBillerCompcodeTP; //get from masterbillInq
    inputParam["tranCode"] = tranCodeTP; // from paymentInq //Transaction Code
    inputParam["fromAcctIdentValue"] = gblFromAccNoTP; //From Account Number from paymentInq 
    inputParam["fromAcctTypeValue"] = gblFromAccTypeTP; //From Account Type from paymentInq (commented for testing) 
    inputParam["transferAmount"] = gblAmtFromServiceTP; //BillPayment Amount from paymentInq 
    inputParam["pmtRefIdent"] = removeHyphenIB(frmIBTopUpViewNEdit.lblBPRef1Val.text); // Refernece 1 
    inputParam["postedDate"] = changeDateFormatForService(gblStartOnTP); //Payment Date
    inputParam["ePayCode"] = "EPYS"; //Fixed value if ePayment	"value""EPYS"" - ePayment"
    inputParam["invoiceNumber"] = "";
    inputParam["waiveCode"] = "I";
    inputParam["fIIdent"] = ""; //Financial Identity Information of To Account
    invokeServiceSecureAsync("billPaymentInquiry", inputParam, forBPBillPaymentInquiryServiceForTPCallBack);
}
/*
   **************************************************************************************
		Module	: forBPBillPaymentInquiryServiceForTPCallBack
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function forBPBillPaymentInquiryServiceForTPCallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (result["BillPmtInqRs"].length > 0 && result["BillPmtInqRs"] != null) {
                if (result["BillPmtInqRs"][0]["WaiveProductCode"] == "Y") {
                    frmIBTopUpViewNEdit.lblBPPaymentFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                } else {
                    frmIBTopUpViewNEdit.lblBPPaymentFeeVal.text = commaFormatted(result["BillPmtInqRs"][0]["FeeAmount"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                forTPCallCustomerAccountInq();
                //dismissLoadingScreenPopup();
            } else {
                dismissLoadingScreenPopup();
                alert("No records found");
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}