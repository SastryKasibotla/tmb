var selActTDIndex = 0,
    dsSelActSelIndex = 0,
    TDActLen = 0,
    temp_seg = [];
var gblOldTargetAmount = "";

function IBsetDreamSaveSetData() {
    var dreamMnthText = frmIBOpenNewDreamAcc.txtODMyDream.text;
    var myDreamAmt = frmIBOpenNewDreamAcc.txtODMnthSavAmt.text;
    var dreamnickOpen = frmIBOpenNewDreamAcc.txtODNickNameVal.text;
    var DreamPicName = frmIBOpenNewDreamAcc.lblDreamSavings.text;
    myDreamAmt = myDreamAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    myDreamAmt = myDreamAmt.replace(/,/g, "");
    var targetAmt = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    targetAmt = targetAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    targetAmt = targetAmt.replace(/,/g, "");
    var CalMonthlySavingAmnt = frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text;
    CalMonthlySavingAmnt = CalMonthlySavingAmnt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    CalMonthlySavingAmnt = CalMonthlySavingAmnt.replace(/,/g, "");
    var CalMonthstoDream = frmIBOpenNewDreamAcc.txtODMyDreamSave.text;
    if (DreamPicName == "" || DreamPicName == null) {
        showAlert(kony.i18n.getLocalizedString("keyPictureIt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    if (dreamnickOpen == "" || dreamnickOpen == null) {
        showAlert(kony.i18n.getLocalizedString("keyDreamAlert"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODNickNameVal.skin = "txtErrorBG";
        return false;
    }
    if (targetAmt == "" || targetAmt == null) {
        showAlert(kony.i18n.getLocalizedString("keyTargetAmountAlert"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODTargetAmt.skin = "txtErrorBG";
        return false;
    }
    if (myDreamAmt == "" || myDreamAmt == null) {
        showAlert(kony.i18n.getLocalizedString("keyEnterMntRAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmt.skin = "txtErrorBG";
        return false;
    }
    if (CalMonthlySavingAmnt == "" || CalMonthlySavingAmnt == null) {
        showAlert(kony.i18n.getLocalizedString("keyEnterMntRAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
        return false;
    }
    if (CalMonthstoDream == "" || CalMonthstoDream == null) {
        showAlert(kony.i18n.getLocalizedString("keyEnterMntRAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMyDreamSave.skin = "txtErrorBG";
        return false;
    }
    if (dreamMnthText == "" || dreamMnthText == null) {
        showAlert(kony.i18n.getLocalizedString("keyEnterMntRAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMyDream.skin = "txtErrorBG";
        return false;
    }
    if (!kony.string.isNumeric(targetAmt)) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), "Info");
        frmIBOpenNewDreamAcc.txtODTargetAmt.skin = "txtErrorBG";
        return false;
    }
    //	var dreamnickOpen = frmIBOpenNewDreamAcc.txtODNickNameVal.text;
    /*
	var pat3 =  "*|!^-_+.=/?~,\":<>[]{}`\';()?฿ฺๅ@&$#%";
	var pat2 = /[\u0E4D]+/; 
	var isAlpha = pat2.test(dreamnickOpen);
	if (isAlpha){
	 showAlert(kony.i18n.getLocalizedString("keygenericalert"), kony.i18n.getLocalizedString("info"));
     frmIBOpenNewDreamAcc.txtODNickNameVal.skin = "txtErrorBG";
		return false;
	}
	
	for (var i = 0; i < dreamnickOpen.length; i++) {
    if (pat3.indexOf(dreamnickOpen.charAt(i)) != -1){
	 showAlert(kony.i18n.getLocalizedString("keygenericalert"), kony.i18n.getLocalizedString("info"));
     frmIBOpenNewDreamAcc.txtODNickNameVal.skin = "txtErrorBG";
		return false;
		}
		
		}
	*/
    if (!kony.string.isNumeric(CalMonthlySavingAmnt)) {
        showAlert(kony.i18n.getLocalizedString("keyEnterValidData"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
        return false;
    }
    if (!kony.string.isNumeric(CalMonthstoDream)) {
        showAlert(kony.i18n.getLocalizedString("keyEnterValidData"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMyDreamSave.skin = "txtErrorBG";
        return false;
    }
    if ((kony.os.toNumber(targetAmt)) < (kony.os.toNumber(gblMinOpenAmt))) {
        showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODTargetAmt.skin = "txtErrorBG";
        return false
    }
    if ((kony.os.toNumber(myDreamAmt)) < (kony.os.toNumber(gblMinOpenAmt))) {
        showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmt.skin = "txtErrorBG";
        return false
    }
    if (gblMaxOpenAmt != "No Limit") {
        if ((kony.os.toNumber(myDreamAmt)) > (kony.os.toNumber(gblMaxOpenAmt))) {
            showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewDreamAcc.txtODMnthSavAmt.skin = "txtErrorBG";
            return false
        }
    }
    if ((kony.os.toNumber(myDreamAmt)) > (kony.os.toNumber(targetAmt))) {
        showAlert(kony.i18n.getLocalizedString("keyDreamLessTarAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmt.skin = "txtErrorBG";
        return false;
    }
    if (dreamMnthText == "0" || kony.string.containsChars(dreamMnthText, ["."])) {
        showAlert(kony.i18n.getLocalizedString("keyValidMonths"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMyDream.skin = "txtErrorBG";
        return false;
    }
    //	frmIBOpenNewDreamAccConfirmation.imgOADreamDetail.src = frmIBOpenNewDreamAcc["segSliderOpenDream"]["selectedItems"][0].imgDreamPic;
    //	frmIBOpenNewDreamAccConfirmation.lblOADreamDetailTarAmtVal.text = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    frmIBOpenNewSavingsAcc.hbxSelActAmt.setVisibility(false);
    frmIBOpenNewSavingsAcc.hbxSelActSelNickName.setVisibility(true);
    frmIBOpenNewSavingsAcc.txtDSAmountSel.setVisibility(true);
    //frmIBOpenNewSavingsAcc.line1.setVisibility(false); ######removed line1, now using a different skin fo rthe hbox - hbxIBMyAccLightBlue
    frmIBOpenNewSavingsAcc.line2.setVisibility(false);
    frmIBOpenNewSavingsAcc.line3.setVisibility(true);
    frmIBOpenNewSavingsAcc.line4.setVisibility(false);
    frmIBOpenNewSavingsAcc.txtOpenActNicNam.text = "";
    frmIBOpenNewSavingsAcc.btnDreamSavecombo.setVisibility(true);
    var tempData;
    frmIBOpenNewSavingsAcc.lblOpengAccFrom.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
    if (kony.i18n.getCurrentLocale() == "en_US") {
        tempData = ["keyMBTransferEvryMnth", kony.i18n.getLocalizedString("keyTransferEvryMnthOpen"), true];
        frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
        frmIBOpenNewSavingsAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenEN"]
    } else {
        frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
        frmIBOpenNewSavingsAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenTH"]
        tempData = ["keyMBTransferEvryMnth", kony.i18n.getLocalizedString("keyTransferEvryMnthOpen"), true];
    }
    frmIBOpenNewSavingsAcc.btnDreamSavecombo.text = kony.i18n.getLocalizedString("keyTransferEvryMnthOpen");
    //frmIBOpenNewSavingsAcc.txtOpenActNicNam.placeholder = kony.i18n.getLocalizedString("keylblNickname");
    var stringstr = frmIBOpenNewDreamAcc.txtODMnthSavAmt.text;
    stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    frmIBOpenNewSavingsAcc.txtDSAmountSel.text = commaFormatted(stringstr) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "/" + kony.i18n.getLocalizedString("keyCalendarMonth");
    gblFinActivityLogOpenAct["dreamEditAmount"] = commaFormatted(stringstr);
    //frmIBOpenNewSavingsAcc.txtDSAmountSel.text = frmIBOpenNewDreamAcc.txtODMnthSavAmt.text + kony.i18n.getLocalizedString("currencyThaiBaht") +"/Month";
    frmIBOpenNewDreamAccConfirmation.lblOADSNickNameVal.text = frmIBOpenNewDreamAcc.txtODNickNameVal.text;
    //frmIBOpenNewSavingsAcc.show();
    showLoadingScreenPopup();
    nextOfDreamAcc();
}

function onClickConnectTDCoverFlowIB() {
    if (frmIBOpenNewTermDepositAcc.imgOpenAmount.isVisible == true) {
        var fdata = frmIBOpenNewTermDepositAcc.coverFlowTP.data[gblCWSelectedItem];
        gblFromTermActs = fdata;
        var accType;
        gblOnClickCoverflow = "TERMFROMACCOUNT"
            //alert("gblOnClickCoverflow"+gblOnClickCoverflow)
        frmIBOpenNewTermDepositAcc.lblFromAccount.text = fdata.lblCustName
        frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text = fdata.lblCustName
            /* if (kony.i18n.getCurrentLocale()  == "en_US"){
             frmIBOpenNewTermDepositAcc.lblFromAccount.text=fdata. fdata.lblCustNameEN;
             }else{
             frmIBOpenNewTermDepositAcc.lblFromAccount.text=fdata. fdata.lblCustNameTH;
             }*/
            /* if(fdata.lblaccType == "SDA"){
            	accType = kony.i18n.getLocalizedString("SavingMB");
            	}else if(fdata.lblaccType == "DDA"){
            	accType = kony.i18n.getLocalizedString("CurrentMB");
            	}
            	*/
        var accTypeTDOpen;
        savingMBTDOpen = "";
        currentMBTDOpen = "";
        if (fdata.lblaccType == "SDA") {
            savingMBTDOpen = "SDA";
            accTypeTDOpen = kony.i18n.getLocalizedString("SavingMB")
        } else if (fdata.lblaccType == "DDA") {
            currentMBTDOpen = "DDA";
            accTypeTDOpen = kony.i18n.getLocalizedString("CurrentMB");
        }
        gblFinActivityLogOpenAct["PrdctOpoenTDEN"] = fdata.hiddenProdENGSA;
        gblFinActivityLogOpenAct["PrdctOpoenTDTH"] = fdata.hiddenproductThaiSA;
        frmIBOpenNewTermDepositAccConfirmation.lblOpeningFromAccount.text = accTypeTDOpen;
        frmIBOpenNewTermDepositAccConfirmation.lblPayingFromAccountType.text = fdata.lblActNoval;
        frmIBOpenNewTermDepositAccConfirmation.lblIBOpenAccCnfmBalAmt.text = fdata.lblBalance;
        frmIBOpenNewTermDepositAccConfirmation.imageOpeningAmtFrom.src = fdata.img1;
        frmIBOpenNewTermDepositAccConfirmation.lblBranchVal.text = fdata.lblRemFeeval;
        gblFinActivityLogOpenAct["branchEN"] = fdata.hiddenBranchEN;
        gblFinActivityLogOpenAct["branchTH"] = fdata.hiddenBranchTH;
        gblFinActivityLogOpenAct["actTypeFromOpen"] = fdata.lblaccType;
        // frmIBOpenNewTermDepositAcc.lblFromAccount.text = fdata.lblProductVal;
        frmIBOpenNewTermDepositAcc.lblAccNo.text = fdata.lblActNoval;
        frmIBOpenNewTermDepositAcc.imageAcc.src = fdata.img1;
    }
    if (frmIBOpenNewTermDepositAcc.imgPayInterest.isVisible == true) {
        var fdata1 = frmIBOpenNewTermDepositAcc.coverFlowTP.data[gblCWSelectedItem];
        gblToTermActs = fdata1
        var accType;
        gblOnClickCoverflow = "TERMTOACCOUNT"
        var accTypeTDTOOpen;
        savingMBTDTOOpen = "";
        currentMBTDTOOpen = "";
        if (fdata1.lblaccType == "SDA") {
            savingMBTDTOOpen = "SDA";
            accTypeTDTOOpen = kony.i18n.getLocalizedString("SavingMB")
        } else if (fdata1.lblaccType == "DDA") {
            currentMBTDTOOpen = "DDA";
            accTypeTDTOOpen = kony.i18n.getLocalizedString("CurrentMB");
        }
        gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] = fdata1.lblCustNameEN
        gblFinActivityLogOpenAct["PrdctOpoenTDTOTH"] = fdata1.lblCustNameTH
        gblFinActivityLogOpenAct["actTypeTooOpen"] = fdata1.lblaccType;
        frmIBOpenNewTermDepositAccConfirmation.lblPayingToName.text = fdata1.lblCustName;
        frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccount.text = accTypeTDTOOpen;
        frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccountType.text = fdata1.lblActNoval;
        frmIBOpenNewTermDepositAccConfirmation.imagePayIntTo.src = fdata1.img1;
        frmIBOpenNewTermDepositAcc.lblToAcc.text = fdata1.lblCustName;
        frmIBOpenNewTermDepositAcc.lblToAccno.text = fdata1.lblActNoval;
        frmIBOpenNewTermDepositAcc.imgToAcc.setVisibility(true);
        frmIBOpenNewTermDepositAcc.imgToAcc.src = fdata1.img1;
        // alert("gblOnClickCoverflow"+gblOnClickCoverflow)
    }
}

function onClickDreamSCoverFlowIB() {
    gblOldTargetAmount = "";
    frmIBOpenNewDreamAcc.txtODTargetAmt.text = "";
    gblOnClickCoverflow = "DREAMACCOUNT"
    var fdata = frmIBOpenNewDreamAcc.coverFlowTP.data[gblCWSelectedItem];
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmIBOpenNewDreamAcc.lblPicturITText.text = fdata.lblDreamEn;
    } else {
        frmIBOpenNewDreamAcc.lblPicturITText.text = fdata.lblDreamTh;
    }
    gblDreamSelData = fdata;
    gblDreamCoverData = fdata.lblDream
    if (fdata.lblDreamEn == "My Own House") frmIBOpenNewDreamAcc.imgDream.src = "prod_homeloan_1.png";
    if (fdata.lblDreamEn == "My Education") frmIBOpenNewDreamAcc.imgDream.src = "prod_education_1.png";
    if (fdata.lblDreamEn == "My Car") frmIBOpenNewDreamAcc.imgDream.src = "prod_car_1.png";
    if (fdata.lblDreamEn == "My Vacation") frmIBOpenNewDreamAcc.imgDream.src = "prod_vacation_1.png";
    if (fdata.lblDreamEn == "My Business") frmIBOpenNewDreamAcc.imgDream.src = "prod_business_1.png";
    if (fdata.lblDreamEn == "My Saving") frmIBOpenNewDreamAcc.imgDream.src = "prod_saving_1.png";
    if (fdata.lblDreamEn == "My Dream") frmIBOpenNewDreamAcc.imgDream.src = "prod_dream_1.png";
}

function startdummycrmServiceTrIB() {
    //kony.os.startSecureTransaction();
    var inputParams = {
        crmId: frmIBEnterCustomerIdLogin.txtUserId.text
    };
    gblcrmId = frmIBEnterCustomerIdLogin.txtUserId.text;
    try {
        invokeServiceSecureAsync("dummycrmGenerator", inputParams, callBackdummycrmServiceIB);
    } catch (e) {
        invokeCommonIBLogger("Exception in invoking service");
    }
}

function callBackdummycrmServiceIB(status, resultable) {
    if (status == 400) {
        getFromXferAccountsIB();
        //IBcheckTranPwdLocked();	
    } else {
        if (status == 300) {
            alert("Error");
        }
    }
}
//function getFromXferAccountsIBIB() {
//	GBLFINANACIALACTIVITYLOG = {}
//	var inputParam = {};
//	inputParam["openActFlag"] = "true";
//	inputParam["selProductCode"] = gblSelOpenActProdCode;
//	if(gblSelProduct == "ForTerm" || gblSelProduct=="ForUse" || gblSelProduct=="TMBSavingcare")
//	{
//		invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackOpenAcntFromActsIB);
//	}
//	else
//	{ 
//		var inputparam = {};
//	 	invokeServiceSecureAsync("getDreamSavingTargets", inputparam, callBackOpenDreamSavingTargetsIB);
//	}
//}
//function callBackOpenAcntFromActsIB(status, resulttable) {
//	if (status == 400) {
//		
//		if (resulttable["opstatus"] == 0) {
//			gblOpenActList = resulttable;
//			//alert(resulttable.length);
//			//dismissLoadingScreen();
//		IBsetDataForUse();
//			//onClickAgreeOPenAcnt();
//           // setDataforOpenFromActsIB(gblOpenActList);
//		}
//		//else alert("opstatus not zero...");
//	}
//	//else alert("status not 400...");
//}
function setOpenActCoverFlowIB(resulttable) {
    var list = []
    var count = 0
    var actResults = resulttable["custAcctFromRec"];
    if (resulttable["custAcctFromRec"] == null || resulttable["custAcctFromRec"] == "" || resulttable["custAcctFromRec"] == undefined) {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    for (var i = 0; i < resulttable["custAcctFromRec"].length; i++) {
        var productName = "";
        var locale = kony.i18n.getCurrentLocale();
        if (locale == "en_US") {
            productType = actResults[i]["ProductNameEng"];
            productName = productType;
            branchName = actResults[i]["BranchNameEh"];
        } else {
            productType = actResults[i]["ProductNameThai"];
            productName = productType;
            branchName = actResults[i]["BranchNameTh"];
        }
        hiddenProdENGAssign = "";
        hiddenProductThaiAssign = "";
        if ((resulttable["custAcctFromRec"][i]["acctNickName"]) == null || (resulttable["custAcctFromRec"][i]["acctNickName"]) == '') {
            var sbStrOpenAct = resulttable["custAcctFromRec"][i]["accId"];
            var accLength = sbStrOpenAct.length;
            var thaiProd = resulttable["custAcctFromRec"][i]["ProductNameThai"];
            var engProd = resulttable["custAcctFromRec"][i]["ProductNameEng"];
            sbStrOpenAct = sbStrOpenAct.substring(accLength - 4, accLength);
            if (thaiProd.length > 15) {
                thaiProd = thaiProd.substring(0, 15);
            }
            if (engProd.length > 15) {
                engProd = engProd.substring(0, 15);
            }
            hiddenProductThaiAssignSave = thaiProd + " " + sbStrOpenAct;
            hiddenProdENGAssignSave = engProd + " " + sbStrOpenAct;
        } else {
            hiddenProdENGAssignSave = resulttable["custAcctFromRec"][i]["acctNickName"];
            hiddenProductThaiAssignSave = resulttable["custAcctFromRec"][i]["acctNickName"];
        }
        /*	var locale = kony.i18n.getCurrentLocale();
                     	if (locale == "en_US") {
						productType = actResults[i]["ProductNameEng"];
						branchName = actResults[i]["BranchNameEh"];
						} else {
						productType = actResults[i]["ProductNameThai"];
						branchName = actResults[i]["BranchNameTh"];
						}*/
        var acctNickName;
        if ((resulttable["custAcctFromRec"][i]["acctNickName"]) == null || (resulttable["custAcctFromRec"][i]["acctNickName"]) == '') {
            var sbStr = resulttable["custAcctFromRec"][i]["accId"];
            var accLength = sbStr.length;
            sbStr = sbStr.substring(accLength - 4, accLength);
            if (productType.length > 15) {
                productType = productType.substring(0, 15);
            }
            acctNickName = productType + " " + sbStr;
        } else {
            acctNickName = resulttable["custAcctFromRec"][i]["acctNickName"];
        }
        var accType = resulttable["custAcctFromRec"][i]["accType"];
        var accVal = resulttable["custAcctFromRec"][i]["accId"];
        var accValLength = accVal.length;
        if (accType != "CCA") {
            accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((accValLength - 1), accValLength);
        } else {
            accVal = accVal.substring((accValLength - 16), (accValLength - 12)) + "-" + accVal.substring((accValLength - 12), (accValLength - 8)) + "-" + accVal.substring((accValLength - 8), (accValLength - 4)) + "-" + accVal.substring((accValLength - 4), accValLength);
        }
        /*
				var  accTypeDisp;
				if(accType == "SDA"){
				accTypeDisp = kony.i18n.getLocalizedString("SavingMB");
				}else if(accType == "DDA"){
				accTypeDisp = kony.i18n.getLocalizedString("CurrentMB");
				}
				*/
        /* if(accType == "SDA"){
				accType = kony.i18n.getLocalizedString("SavingMB");
				}else if(accType == "DDA"){
				accType = kony.i18n.getLocalizedString("CurrentMB");
				}*/
        var imageName = "";
        /*if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFIXED" )
        	imageName = "prod_savingd.png"
        else if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_SAVINGCARE")
        	imageName = "product_ico_saving.png"
        else if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE")
        	imageName = "prod_termdeposits.png"
        else if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE")
        	imageName = "prod_nofee.png"
        else if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE" || resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE"||resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE")
        	imageName = "prod_currentac.png"
        else if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_CREDITCARD_TABLE")
        	imageName = "prod_creditcard.png"
        else if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_HOMELOAN")
        	imageName = "prod_homeloan.png"
        else if (resulttable["custAcctFromRec"][i]["VIEW_NAME"] == "PRODUCT_CODE_READYCASH_TABLE")
        	imageName = "prod_cash2go.png"*/
        imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["custAcctFromRec"][i]["ICON_ID"] + "&modIdentifier=PRODICON";
        if (count == 0) {
            list.push({
                lblProduct: kony.i18n.getLocalizedString("Product"),
                lblProductVal: productName,
                lblACno: kony.i18n.getLocalizedString("AccountNo"),
                img1: imageName,
                lblCustName: acctNickName,
                lblBalance: resulttable["custAcctFromRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                lblActNoval: accVal, //formatAccountNo(resulttable["custAcctRec"][i]["accId"]),
                lblRemFeeval: branchName,
                lblRemFee: kony.i18n.getLocalizedString("Branch"),
                lblhideaccountName: resulttable["custAcctFromRec"][i]["accountName"],
                lblcreatedDate: resulttable["custAcctFromRec"][i]["createdDate"],
                lblfiident: resulttable["custAcctFromRec"][i]["fiident"],
                lblaccType: accType, //resulttable["custAcctFromRec"][i]["accType"],
                lblPersId: resulttable["custAcctFromRec"][i]["persionlizeAcctId"],
                lblavailbal: resulttable["custAcctFromRec"][i]["availableBal"],
                lblProdID: resulttable["custAcctFromRec"][i]["productID"],
                hiddenProdENGSA: hiddenProdENGAssignSave,
                hiddenBranchEN: actResults[i]["BranchNameEh"],
                hiddenBranchTH: actResults[i]["BranchNameTh"],
                hiddenproductThaiSA: hiddenProductThaiAssignSave
            })
            count = count + 1
        } else if (count == 1) {
            list.push({
                lblProduct: kony.i18n.getLocalizedString("Product"),
                lblProductVal: productName,
                lblACno: kony.i18n.getLocalizedString("AccountNo"),
                img1: imageName,
                lblCustName: acctNickName,
                lblBalance: resulttable["custAcctFromRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                lblActNoval: accVal,
                lblRemFeeval: branchName,
                lblRemFee: kony.i18n.getLocalizedString("Branch"),
                lblhideaccountName: resulttable["custAcctFromRec"][i]["accountName"],
                lblcreatedDate: resulttable["custAcctFromRec"][i]["createdDate"],
                lblfiident: resulttable["custAcctFromRec"][i]["fiident"],
                lblaccType: accType, //resulttable["custAcctFromRec"][i]["accType"],
                lblPersId: resulttable["custAcctFromRec"][i]["persionlizeAcctId"],
                lblavailbal: resulttable["custAcctFromRec"][i]["availableBal"],
                lblProdID: resulttable["custAcctFromRec"][i]["productID"],
                hiddenProdENGSA: hiddenProdENGAssignSave,
                hiddenBranchEN: actResults[i]["BranchNameEh"],
                hiddenBranchTH: actResults[i]["BranchNameTh"],
                hiddenproductThaiSA: hiddenProductThaiAssignSave
            })
            count = count + 1
        } else if (count == 2) {
            list.push({
                lblProduct: kony.i18n.getLocalizedString("Product"),
                lblProductVal: productName,
                lblACno: kony.i18n.getLocalizedString("AccountNo"),
                img1: imageName,
                lblCustName: acctNickName,
                lblBalance: resulttable["custAcctFromRec"][i]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
                lblActNoval: accVal,
                lblRemFeeval: branchName,
                lblRemFee: kony.i18n.getLocalizedString("Branch"),
                lblhideaccountName: resulttable["custAcctFromRec"][i]["accountName"],
                lblcreatedDate: resulttable["custAcctFromRec"][i]["createdDate"],
                lblfiident: resulttable["custAcctFromRec"][i]["fiident"],
                lblaccType: accType, //resulttable["custAcctFromRec"][i]["accType"],
                lblPersId: resulttable["custAcctFromRec"][i]["persionlizeAcctId"],
                lblavailbal: resulttable["custAcctFromRec"][i]["availableBal"],
                lblProdID: resulttable["custAcctFromRec"][i]["productID"],
                hiddenBranchEN: actResults[i]["BranchNameEh"],
                hiddenBranchTH: actResults[i]["BranchNameTh"],
                hiddenProdENGSA: hiddenProdENGAssignSave,
                hiddenproductThaiSA: hiddenProductThaiAssignSave
            })
            count = 0
        }
    }
    if (list.length <= 0) {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    var data1 = list;
    JSON.stringify("custrec values::", data1);
    frmIBOpenNewTermDepositAcc.coverFlowTP.data = data1;
    frmIBOpenNewSavingsAcc.coverFlowTP.data = data1;
    frmIBOpenNewSavingsCareAcc.coverFlowTP.data = data1;
}

function setDataforOpenFromActsIB(resulttable) {
    //
    setOpenActCoverFlowIB(resulttable);
    if (gblSelProduct == "ForTerm") {
        //dismissLoadingScreenPopup();	
        if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != "") {
            frmIBOpenNewTermDepositAcc.txtDSNickName.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
        } else {
            frmIBOpenNewTermDepositAcc.txtDSNickName.maxTextLength = 20;
        }
        frmIBOpenNewTermDepositAcc.txtTDamount.text = commaFormattedOpenAct(gblMinOpenAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
        frmIBOpenNewTermDepositAcc.txtDSNickName.text = "";
        frmIBOpenNewTermDepositAcc.imageAcc.src = "transparent.png"
        frmIBOpenNewTermDepositAcc.lblFromAccount.text = "";
        frmIBOpenNewTermDepositAcc.lblAccNo.text = "";
        gblOnClickCoverflow = "";
        frmIBOpenNewTermDepositAcc.lblToAcc.text = "";
        frmIBOpenNewTermDepositAcc.lblToAccno.text = "";
        frmIBOpenNewTermDepositAcc.imgToAcc.src = "transparent.png"
        frmIBOpenNewTermDepositAcc.imgPayInterest.setVisibility(false);
        frmIBOpenNewTermDepositAcc.imgOpenAmount.setVisibility(true);
        frmIBOpenNewTermDepositAcc.vbxOpenAmount.skin = vboxLightBlue320px;
        frmIBOpenNewTermDepositAcc.vbxPayingTo.skin = "NoSkin";
        //frmIBOpenNewTermDepositAcc.txtDSNickName.placeholder = kony.i18n.getLocalizedString("keylblNickname");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewTermDepositAcc.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
        } else {
            frmIBOpenNewTermDepositAcc.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
        }
        try {
            if (null != frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"]) {
                frmIBOpenNewTermDepositAcc.imgOATDProd.src = frmIBOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
            } else {
                //when user come from open account brief screen from campaign
                //var prodD = gblProudctDetails.ProdDetails[gblProdCode];
                var prodD = gblProudctDetails[gblProdCode];
                if (undefined != prodD && null != prodD) {
                    frmIBOpenNewTermDepositAcc.imgOATDProd.src = prodD.imageProduct;
                }
            }
        } catch (e) {
            //
        }
        onClickConnectTDCoverFlowIB();
        frmIBOpenNewTermDepositAcc.txtDSNickName.skin = "txtIB20pxBlack";
        frmIBOpenNewTermDepositAcc.txtTDamount.skin = "txtIB20pxBlack";
        frmIBOpenNewTermDepositAcc.imgToAcc.setVisibility(false);
        frmIBOpenNewTermDepositAcc.show();
        gblFromAcccccc = frmIBOpenNewTermDepositAcc.coverFlowTP.data;
    } else if (gblSelProduct == "ForUse") {
        //dismissLoadingScreenPopup();	
        //New Form for Package products - ENH-087
        if (gblIssueOnlineCard == "1") showAddressForPackageProduct();
        else {
            frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtIB20pxBlack";
            frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtIB20pxBlack";
            setNormalSavingsDataIB();
        }
        /*
	frmIBOpenNewSavingsAcc.txtOpenActSelAmt.skin = "txtIB20pxBlack";
    frmIBOpenNewSavingsAcc.txtOpenActNicNam.skin = "txtIB20pxBlack";
    
		setNormalSavingsDataIB();
	*/
    } else if (gblSelProduct == "TMBSavingcare") {
        //dismissLoadingScreenPopup();
        resetTextBoxesSkinSavingCareIB();
        setSavingCareDataIB();
    }
}
//function callBackOpenDreamSavingTargetsIB(status, resulttable) {
//	if (status == 400) {
//		
//		if (resulttable["opstatus"] == 0) {
//		    //alert("result:"+resulttable["SavingTargets"]);
//			//dismissLoadingScreen();
//			gblOpenDrmAcct=resulttable;
//			setDataforDreamOpenFromActsIB(gblOpenDrmAcct);
//		  }
//     }
//}
/*function setDataforDreamOpenFromActsIB(resulttable)   {        			
			var list = [] ;
			var count = 0;
		         
			for (j = 0; j < resulttable["SavingTargets"].length; j++) {
				var dreamName;
				var dreamimage;
				var locale = kony.i18n.getCurrentLocale();
				if (locale == "en_US") {
					dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"];
				} else {
					dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_TH"];
				}
				if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Vacation") {
					dreamimage = "prod_vacation.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Dream") {
					dreamimage = "prod_dream.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Car") {
					dreamimage = "prod_car.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Education") {
					dreamimage = "prod_education.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Own House") {
					dreamimage = "prod_homeloan.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Business") {
					dreamimage = "prod_business.png";
				} else if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Saving") {
					dreamimage = "prod_saving.png";
				}
				if (count == 0) {
							list.push({
								
						        img1: dreamimage,
								lblDream:dreamName
						       
							})
							count = count + 1
						}
						else if (count == 1) {
							list.push({
								
						        img1: dreamimage,
						       lblDream:dreamName
							})
							count = count + 1
						}
						else if (count == 2) {
							list.push({
								img1: dreamimage,
						       lblDream:dreamName
						       })
							count = 0
						}
			}
			var data1=list;
			frmIBOpenNewDreamAcc.coverFlowTP.data=data1;
			dismissLoadingScreenPopup();	
			frmIBOpenNewDreamAcc.show();
		
	
} */
//function setDataforDreamSavingActsIB()
//{ 
//frmIBOpenNewDreamAcc.coverFlowTP.widgetDataMap = {
//		 img1: "img1",
//         lblDream:"lblDream"
//		
//	}
//	    var data1 = [{
//		 img1: "prod_car.png",
//         lblDream:"Car"
//	    
//	}, {
//		 img1: "prod_homeloan.png",
//         lblDream:"My Own House"
//	    
//		
//	}, {
//		 img1: "prod_education.png",
//         lblDream:"Education"
//	    
//	}, {
//		 img1: "prod_saving.png",
//         lblDream:"Saving"
//	 
//	}, {
//		 img1: "prod_vacation.png",
//         lblDream:"Vacation"
//	   
//	}]; 
//  frmIBOpenNewDreamAcc.coverFlowTP.data=data1;
//
//}
function IBopenActNickNameUniq(uniqText) {
    var nameFromService = "";
    for (i = 0; i < gblOpenActList["custAcctRec"].length; i++) {
        if ((gblOpenActList["custAcctRec"][i]["acctNickName"]) != null && (gblOpenActList["custAcctRec"][i]["acctNickName"]) != "") {
            //
            //
            nameFromService = gblOpenActList["custAcctRec"][i]["acctNickName"].toLowerCase()
        } else {
            if ((gblOpenActList["custAcctRec"][i]["ProductNameEng"]) != null && (gblOpenActList["custAcctRec"][i]["ProductNameEng"]) != "") {
                var productType = gblOpenActList["custAcctRec"][i]["ProductNameEng"].toLowerCase();
                var sbStr = gblOpenActList["custAcctRec"][i]["accId"];
                var accLength = sbStr.length;
                sbStr = sbStr.substring(accLength - 4, accLength);
                nameFromService = productType + " " + sbStr;
            }
        }
        if (uniqText.toLowerCase() == nameFromService) {
            return false;
        }
    }
}

function loadTermsNConditionOpenAccount() {
    var input_param = {};
    if (frmIBOpenProdDetnTnC.lblOpenAccInt.text == "0.00%") {
        showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
    //module key again is from tmb_tnc file
    var prodDescTnC = gblFinActivityLogOpenAct["prodDesc"]; //frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text
    prodDescTnC = prodDescTnC.replace(/\s+/g, '');
    input_param["moduleKey"] = prodDescTnC;
    showLoadingScreenPopup();
    //loadTNCOpenActCallBack();
    invokeServiceSecureAsync("readUTFFile", input_param, loadTNCOpenActCallBack);
}

function loadTNCOpenActCallBack(status, result) {
    //function loadTNCOpenActCallBack(){	
    if (status == 400) {
        if (result["opstatus"] == 0) {
            frmIBOpenProdDetnTnC.lblOpenActDesc.setVisibility(true);
            frmIBOpenProdDetnTnC.richTextProdDetIB.setVisibility(false);
            frmIBOpenProdDetnTnC.lblOpenActDesc.text = result["fileContent"];
            frmIBOpenProdDetnTnC.hboxSaveCamEmail.setVisibility(true);
            frmIBOpenProdDetnTnC.hbxConCanclRC.setVisibility(true);
            //frmIBOpenProdDetnTnC.btnOpenProdContinue.setVisibility(false);
            frmIBOpenProdDetnTnC.hbxOpenProdContinue.setVisibility(false);
            frmIBOpenProdDetnTnC.hbxOpnProdDet.setVisibility(false);
            frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.setFocus(true);
            frmIBOpenProdDetnTnC.hbox45734110037959.setVisibility(true);
            frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString("keyTermsNConditions");
            frmIBOpenProdDetnTnC.lblOpenAccInt.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text.replace("%", "");
            dismissLoadingScreenPopup();
            frmIBOpenProdDetnTnC.show();
            if (gblSelProduct == "ForUse") {
                frmIBOpenNewSavingsAcc.txtOpenActSelAmt.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBOpenNewSavingsAccConfirmation.lblInterestRateVal.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
                frmIBOpenNewSavingsAccComplete.lblInterestRateVal.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
            } else if (gblSelProduct == "ForTerm") {
                frmIBOpenNewTermDepositAcc.txtTDamount.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
                frmIBOpenNewTermDepositAccConfirmation.lblInterestVal.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
                frmIBOpenNewTermDepositAccComplete.lblInterestVal.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
            } else if (gblSelProduct == "TMBDreamSavings") {
                frmIBOpenNewDreamAcc.txtODTargetAmt.text = "0";
                frmIBOpenNewDreamAccConfirmation.lblInterestValue.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
                frmIBOpenNewDreamAccComplete.lblInterestRateVal.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
            } else {
                frmIBOpenNewSavingsCareAcc.txtAmount.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");;
                frmIBOpenNewSavingsCareAccConfirmation.lblOASCIntRateVal.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
                frmIBOpenNewSavingsCareAccComplete.lblOASCIntRateVal.text = frmIBOpenProdDetnTnC.lblOpenAccInt.text + "%";
            }
        } else {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function checkSymbolopenIB() {
    var stringstr = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    if (stringstr == null || stringstr == "") {
        showAlertIB(kony.i18n.getLocalizedString("keyTargetAmt"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODTargetAmt.skin = "txtErrorBG";
        return false;
    }
    var dotcount = stringstr.split(".");
    var lendot = dotcount.length - 1;
    if (lendot > 1) {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODTargetAmt.skin = "txtErrorBG";
        return false;
    }
    // var decimal= dotcout[1];
    var stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var name = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    var charExists;
    if (charExists = (name.indexOf("à¸¿") >= 0)) {} else {
        frmIBOpenNewDreamAcc.txtODTargetAmt.text = commaFormattedOpenAct(stringstr) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    var currentTargetAmount = kony.os.toNumber(stringstr);
    if (gblOldTargetAmount != "" && gblOldTargetAmount != currentTargetAmount) {
        gblOldTargetAmount = currentTargetAmount;
        onclickMnthlySaving();
    }
}

function checkSymbolopenMonthlyIB() {
    var removetar = frmIBOpenNewDreamAcc.lbltargettext.text;
    var targettextamnt = removetar.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var mnthtext = frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text;
    mnthtext = mnthtext.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    targetamnt = kony.os.toNumber(targettextamnt);
    mnthtextamnt = kony.os.toNumber(mnthtext);
    if (targetamnt < mnthtextamnt) {
        showAlertIB(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
        return false;
    }
    if (mnthtext.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
        return false;
    }
    //  if (frmIBOpenNewDreamAcc.txtODMyDreamSave.text == "" || frmIBOpenNewDreamAcc.txtODMyDreamSave.text == "0") {
    var Totamntnew = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    Totamnt = Totamntnew.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    //frmIBOpenNewDreamAcc.txtODTargetAmt.text = commaFormatted(Totamnt);
    var MontSvgamntnew = frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text;
    MontSvgamnt = MontSvgamntnew.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var stringstr = MontSvgamnt //frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text;
    var dotcount = stringstr.split(".");
    var lendot = dotcount.length - 1;
    if (lendot > 1) {
        showAlertIB(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
        return false;
    }
    var stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    if (stringstr == null || stringstr == "") {
        //frmDreamSavingEdit.lblTargetAmntVal.text = frmDreamSavingEdit.lblTargetAmntVal.text
    } else {
        frmIBOpenNewDreamAcc.txtODMyDreamSave.text = ((Math.round(Totamnt / MontSvgamnt)).toString());
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = commaFormatted(stringstr) + "" + kony.i18n.getLocalizedString("currencyThaiBaht");
        //} 
        frmIBOpenNewDreamAcc.hbox867539252328664.skin = "hbxProperFocus"
    }
}

function onclickMnthlySaving() {
    amt = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    frmIBOpenNewDreamAcc.vbxBPRight.hbxBuildMyDream.setVisibility(true);
    frmIBOpenNewDreamAcc.image24573708914676.setVisibility(true);
    frmIBOpenNewDreamAcc.image2867539252331117.setVisibility(false);
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.setEnabled(false);
    frmIBOpenNewDreamAcc.txtODMyDream.setEnabled(true);
    frmIBOpenNewDreamAcc.image24573708914676.isVisible = true
    frmIBOpenNewDreamAcc.image2867539252331117.isVisible = false
    frmIBOpenNewDreamAcc.vboxMiddle.skin = "vbxRightColumn"
    frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumn"
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.text = "";
    frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = "";
    frmIBOpenNewDreamAcc.txtODMyDream.text = "";
    frmIBOpenNewDreamAcc.txtODMyDreamSave.text = "";
    frmIBOpenNewDreamAcc.vboxMiddle.skin = "vbxRightColumn"
    frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumn"
    frmIBOpenNewDreamAcc.hbxPictureIt.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hbxNickVal.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxTarget.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthlySaving.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthsToDream.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hbxCoverflow.isVisible = false;
    var imageLarge = frmIBOpenNewDreamAcc.imgDream.src.replace("_1", "");
    frmIBOpenNewDreamAcc.imgDrm.src = imageLarge;
    frmIBOpenNewDreamAcc.hbxImage.isVisible = false;
    frmIBOpenNewDreamAccConfirmation.imgDrm.src = frmIBOpenNewDreamAcc.imgDream.src
    frmIBOpenNewDreamAccComplete.imgDSAckTitle.src = frmIBOpenNewDreamAcc.imgDream.src
    var stringstr = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    var stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    //if(stringstr3.indexOf("à¸¿",0)!=-1 || stringstr3 == null || stringstr3 == "" ){
    //frmDreamSavingEdit.lblTargetAmntVal.text = frmDreamSavingEdit.lblTargetAmntVal.text
    //}else{
    frmIBOpenNewDreamAcc.lbltargettext.text = commaFormatted(stringstr) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
}

function onclickMnthsave() {
    frmIBOpenNewDreamAcc.vbxBPRight.hbxBuildMyDream.setVisibility(true);
    frmIBOpenNewDreamAcc.txtODMyDream.setEnabled(false);
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.setEnabled(true);
    frmIBOpenNewDreamAcc.hbxPictureIt.skin = "hbxProperFocus"
    frmIBOpenNewDreamAcc.hbxNickVal.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxTarget.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthlySaving.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthsToDream.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hbxCoverflow.isVisible = false;
    var imageLarge = frmIBOpenNewDreamAcc.imgDream.src.replace("_1", "");
    frmIBOpenNewDreamAcc.imgDrm.src = imageLarge;
    frmIBOpenNewDreamAcc.hbxImage.isVisible = false;
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.text = "";
    frmIBOpenNewDreamAccConfirmation.imgDrm.src = frmIBOpenNewSavingsAcc.imgNSProdName.src
    frmIBOpenNewDreamAccComplete.imgDSAckTitle.src = frmIBOpenNewDreamAcc.imgDream.src
        //frmIBOpenNewDreamAcc.lbltargettext.text=commaFormatted(frmIBOpenNewDreamAcc.txtODTargetAmt.text)+kony.i18n.getLocalizedString("currencyThaiBaht")
    frmIBOpenNewDreamAcc.txtODMyDreamSave.text = "";
    frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = "";
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.text = "";
    frmIBOpenNewDreamAcc.txtODMyDreamSave.text = "";
    frmIBOpenNewDreamAcc.txtODMyDream.text = "";
    //frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin="txtDreamLightblue"
    //frmIBOpenNewDreamAcc.txtODMyDreamSave.skin="txtDreamLightblue"
    //frmIBOpenNewDreamAcc.vbox866794452610619.isVisible=false;
    var stringstr = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    var stringstr = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    //if(stringstr3.indexOf("à¸¿",0)!=-1 || stringstr3 == null || stringstr3 == "" ){
    //frmDreamSavingEdit.lblTargetAmntVal.text = frmDreamSavingEdit.lblTargetAmntVal.text
    //}else{
    frmIBOpenNewDreamAcc.lbltargettext.text = commaFormatted(stringstr) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
}

function CalOnMnth() {
    var TotamntMnth = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    var Totamnt = TotamntMnth.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var months = frmIBOpenNewDreamAcc.txtODMyDreamSave.text;
    if (months == "" || months == null) {
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = "";
    } else {
        var MnthlySavAMnt1 = (Totamnt / months);
        MnthlySavAMnt = MnthlySavAMnt1.toFixed(2);
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = commaFormatted(MnthlySavAMnt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    frmIBOpenNewDreamAcc.hbox867539252328683.skin = "hbxProperFocus"
}

function IBonclickSaveinDreamAccEdited() {
    var mnthToDream = frmIBOpenNewDreamAcc.txtODMyDreamSave.text;
    var stringstr = frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text
    var targetamnt = frmIBOpenNewDreamAcc.lbltargettext.text
    if (frmIBOpenNewDreamAcc.txtODMyDreamSave.text == "1") {
        gblMnthSaveDream = mnthToDream.replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "")
    } else {
        gblMnthSaveDream = mnthToDream.replace(kony.i18n.getLocalizedString("keymonths"), "")
    }
    mnthsave = stringstr.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var targetsave = targetamnt.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    if ((kony.os.toNumber(mnthsave)) < (kony.os.toNumber(gblMinOpenAmt))) {
        frmIBOpenNewDreamAcc.hbxImage.isVisible = false;
        showAlert(kony.i18n.getLocalizedString("keyMonthlyLimit"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
        return false
    } else if ((kony.os.toNumber(mnthsave)) > (kony.os.toNumber(gblMinOpenAmt))) {
        frmIBOpenNewDreamAcc.hbxImage.isVisible = false;
    }
    if (gblMaxOpenAmt != "No Limit") {
        if ((kony.os.toNumber(mnthsave)) > (kony.os.toNumber(gblMaxOpenAmt))) {
            showAlert(kony.i18n.getLocalizedString("keyMonthlyLimit"), kony.i18n.getLocalizedString("info"));
            frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
            return false
        } else if ((kony.os.toNumber(mnthsave)) < (kony.os.toNumber(gblMaxOpenAmt))) {
            frmIBOpenNewDreamAcc.hbxImage.isVisible = false;
        }
    }
    if ((kony.os.toNumber(targetsave)) < (kony.os.toNumber(mnthsave))) {
        showAlert(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtErrorBG";
        return false;
    } else if ((kony.os.toNumber(targetsave)) > (kony.os.toNumber(mnthsave))) {
        frmIBOpenNewDreamAcc.hbxImage.isVisible = false;
    }
    var months = frmIBOpenNewDreamAcc.txtODMyDreamSave.text;
    if (months == "" || months == null) {
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = "";
    } else {
        var TotamntMnth = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
        var Totamnt = TotamntMnth.replace(/,/g, "").replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        var MnthlySavAMnt1 = (Totamnt / months);
        MnthlySavAMnt = MnthlySavAMnt1.toFixed(2);
        frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text = commaFormatted(MnthlySavAMnt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    frmIBOpenNewDreamAcc.hbxImage.isVisible = true;
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.text = frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.text;
    if (frmIBOpenNewDreamAcc.txtODMyDreamSave.text == "1") {
        frmIBOpenNewDreamAcc.txtODMyDream.text = frmIBOpenNewDreamAcc.txtODMyDreamSave.text + " " + kony.i18n.getLocalizedString("keyCalendarMonth");
    } else {
        frmIBOpenNewDreamAcc.txtODMyDream.text = frmIBOpenNewDreamAcc.txtODMyDreamSave.text + " " + kony.i18n.getLocalizedString("keymonths");
    }
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.isVisible = true;
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.setEnabled(true);
    frmIBOpenNewDreamAcc.txtODMyDream.setEnabled(true);
    frmIBOpenNewDreamAcc.txtODMyDream.isVisible = true;
    frmIBOpenNewDreamAcc.hbxBuildMyDream.isVisible = false;
    //frmIBOpenNewDreamAcc.hbxImage.isVisible=true;
    frmIBOpenNewDreamAccConfirmation.lblOADreamDetailTarAmtVal.text = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    frmIBOpenNewDreamAccComplete.lblOADreamDetailTarAmtVal.text = frmIBOpenNewDreamAcc.txtODTargetAmt.text;
    frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumn";
    frmIBOpenNewSavingsAcc.lblNSProdName.text = frmIBOpenNewDreamAcc.lblDreamSavings.text;
    //	frmIBOpenNewDreamAcc.txtODMyDream.skin="txtIB20pxBlack";
    //	frmIBOpenNewDreamAcc.txtODMnthSavAmt.skin="txtIB20pxBlack";
    frmIBOpenNewDreamAcc.hbxPictureIt.skin = "hbxProperFocus"
    frmIBOpenNewDreamAcc.hbxNickVal.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxTarget.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthlySaving.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthsToDream.skin = "hbxProper"
    frmIBOpenNewDreamAcc.image24573708914676.isVisible = false;
    frmIBOpenNewDreamAcc.image2867539252331117.isVisible = false;
    //frmIBOpenNewDreamAcc.hbxImage.isVisible=false;	
    frmIBOpenNewDreamAcc.vbxBPMiddleRight.skin = "vbxMidRightMinHeight";
    frmIBOpenNewDreamAcc.txtODMnthSavAmtSave.skin = "txtBoxTransParentGrey";
    gblOldTargetAmount = kony.os.toNumber(targetsave);
}

function syncIBOpenAcctLocale() {
    pagespecificSubMenu();
    var p = kony.i18n.getLocalizedString("keyOpenRelation");
    var f = kony.i18n.getLocalizedString("keyFather");
    var m = kony.i18n.getLocalizedString("keyMother");
    var r = kony.i18n.getLocalizedString("keyRelative");
    var s = kony.i18n.getLocalizedString("keySpouseReg");
    var l = kony.i18n.getLocalizedString("keySpouseNL");
    var c = kony.i18n.getLocalizedString("keyChild");
    var o = kony.i18n.getLocalizedString("keyOtherRelation");
    //arrafter=[p,f,m,r,s,l,c,o];
    if (kony.application.getCurrentForm().id == "frmIBOpenNewTermDepositAccConfirmation") {
        frmIBOpenNewTermDepositAccConfirmation.lblRequest.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewDreamAcc") {
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmIBOpenNewDreamAcc.lblMonthlySaving.containerWeight = 20
            frmIBOpenNewDreamAcc.lblMonthsToMyDream.containerWeight = 26
            frmIBOpenNewDreamAcc.lblMonthsToMyDream.containerWeight = 26
        } else {
            frmIBOpenNewDreamAcc.lblMonthlySaving.containerWeight = 45
            frmIBOpenNewDreamAcc.lblMonthsToMyDream.containerWeight = 44
            frmIBOpenNewDreamAcc.lblNickName.containerWeight = 33
        }
    }
    if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmIBOpenNewDreamAcc.lblMthlySvngAmt.containerWeight = 20
        frmIBOpenNewDreamAcc.lblMnthsToMyDrm.containerWeight = 26
        frmIBOpenNewDreamAcc.lblNickName.containerWeight = 18;
    } else {
        frmIBOpenNewDreamAcc.lblMthlySvngAmt.containerWeight = 45
        frmIBOpenNewDreamAcc.lblMnthsToMyDrm.containerWeight = 44
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenProdDetnTnC") {
        frmIBOpenProdDetnTnC.lblOpenNewAccount.text = kony.i18n.getLocalizedString("kelblOpenNewAccount");
        try {
            if (gblSelOpenActProdCode == "220") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyNoFeeProdDet');
            } else if (gblSelOpenActProdCode == "200") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyNormalSavProdDet');
            } else if (gblSelOpenActProdCode == "221") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyNoFixedProdDet');
            } else if (gblSelOpenActProdCode == "222") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyFreeFlowProdDet');
            } else if (gblSelOpenActProdCode == "206") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyDreamProdDet');
            } else if (gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != null && gblOpenActList["SAVING_CARE_PRODUCT_CODES"] != undefined && gblOpenActList["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0) {
                var saving_care_image_name = "SavingsCareIntroEN";
                if (kony.i18n.getCurrentLocale() == "th_TH") saving_care_image_name = "SavingsCareIntroTH";
                var saving_care_image = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + saving_care_image_name + "&modIdentifier=PRODUCTPACKAGEIMG";
                frmIBOpenProdDetnTnC.imgProductPackage.src = saving_care_image;
            } else if (gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301" || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermProdDet');
            } else if (gblSelOpenActProdCode == "659") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermUpProdDet');
            } else if (gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666") {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString('keyTermQuickProdDet');
            } else if (gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined && gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0) {
                frmIBOpenProdDetnTnC.richTextProdDetIB.text = kony.i18n.getLocalizedString(product_details_i18n_key);
                if (gblCardOpenAccount) {
                    frmIBOpenProdDetnTnC.lblOpenNewAccount.text = kony.i18n.getLocalizedString("keyOpenNewAccountAndDebitCard");
                } else {
                    frmIBOpenProdDetnTnC.lblOpenNewAccount.text = kony.i18n.getLocalizedString("kelblOpenNewAccount");
                }
            }
        } catch (e) {}
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewDreamAcc") {
        frmIBOpenNewDreamAcc.lblTargetAccount.text = kony.i18n.getLocalizedString("keyMbTargetAmnt");
        var fdata = frmIBOpenNewDreamAcc.coverFlowTP.data[gblCWSelectedItem];
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewDreamAcc.image2867539252331117.padding = [240, 0, 0, 0];
            frmIBOpenNewDreamAcc.image24573708914676.padding = [195, 0, 0, 0]
            if (frmIBOpenNewDreamAcc.lblPicturITText.text != "") {
                frmIBOpenNewDreamAcc.lblPicturITText.text = fdata.lblDreamEn;
            }
            frmIBOpenNewDreamAcc.lblDreamSavings.text = gblFinActivityLogOpenAct["prodNameEN"];
            if (frmIBOpenNewDreamAcc.txtODMyDream.text == "" || frmIBOpenNewDreamAcc.txtODMyDream.text == null) {} else {
                if (frmIBOpenNewDreamAcc.txtODMyDreamSave.text == "1") {
                    frmIBOpenNewDreamAcc.txtODMyDream.text = "";
                    frmIBOpenNewDreamAcc.txtODMyDream.text = gblMnthSaveDream + " " + kony.i18n.getLocalizedString("keyCalendarMonth");
                } else {
                    frmIBOpenNewDreamAcc.txtODMyDream.text = "";
                    frmIBOpenNewDreamAcc.txtODMyDream.text = gblMnthSaveDream + " " + kony.i18n.getLocalizedString("keymonths");
                }
            }
        } else {
            frmIBOpenNewDreamAcc.image2867539252331117.padding = [256, 0, 0, 0]
            frmIBOpenNewDreamAcc.image24573708914676.padding = [169, 0, 0, 0]
            if (frmIBOpenNewDreamAcc.lblPicturITText.text != "") {
                frmIBOpenNewDreamAcc.lblPicturITText.text = fdata.lblDreamTh;
            }
            frmIBOpenNewDreamAcc.lblDreamSavings.text = gblFinActivityLogOpenAct["prodNameTH"];
            if (frmIBOpenNewDreamAcc.txtODMyDream.text == "" || frmIBOpenNewDreamAcc.txtODMyDream.text == null) {} else {
                if (frmIBOpenNewDreamAcc.txtODMyDreamSave.text == "1") {
                    frmIBOpenNewDreamAcc.txtODMyDream.text = "";
                    frmIBOpenNewDreamAcc.txtODMyDream.text = gblMnthSaveDream + " " + kony.i18n.getLocalizedString("keyCalendarMonth");
                } else {
                    frmIBOpenNewDreamAcc.txtODMyDream.text = "";
                    frmIBOpenNewDreamAcc.txtODMyDream.text = gblMnthSaveDream + " " + kony.i18n.getLocalizedString("keymonths");
                }
            }
        }
        if (gbldreamCoverflow == false) {
            var list = [];
            var count = 0;
            for (j = 0; j < gblOpenActListSavingTarget.length; j++) {
                dreamName = "";
                dreamNameEn = "";
                dreamNameTh = "";
                var dreamimage;
                var locale = kony.i18n.getCurrentLocale();
                if (locale == "en_US") {
                    dreamName = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"];
                } else {
                    dreamName = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_TH"];
                }
                dreamNameEn = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"];
                dreamNameTh = gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_TH"];
                if (gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"] == "My Vacation") {
                    dreamimage = "prod_vacation.png";
                } else if (gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"] == "My Dream") {
                    dreamimage = "prod_dream.png";
                } else if (gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"] == "My Car") {
                    dreamimage = "prod_car.png";
                } else if (gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"] == "My Education") {
                    dreamimage = "prod_education.png";
                } else if (gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"] == "My Own House") {
                    dreamimage = "prod_homeloan.png";
                } else if (gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"] == "My Business") {
                    dreamimage = "prod_business.png";
                } else if (gblOpenActListSavingTarget[j]["DREAM_TARGET_DESC_EN"] == "My Saving") {
                    dreamimage = "prod_saving.png";
                }
                if (count == 0) {
                    list.push({
                        img1: dreamimage,
                        lblDream: dreamName,
                        lblDreamEn: dreamNameEn,
                        lblDreamTh: dreamNameTh,
                        lblDreamTargetID: gblOpenActListSavingTarget[j]["DREAM_TARGET_ID"]
                    })
                    count = count + 1
                } else if (count == 1) {
                    list.push({
                        img1: dreamimage,
                        lblDream: dreamName,
                        lblDreamEn: dreamNameEn,
                        lblDreamTh: dreamNameTh,
                        lblDreamTargetID: gblOpenActListSavingTarget[j]["DREAM_TARGET_ID"]
                    })
                    count = count + 1
                } else if (count == 2) {
                    list.push({
                        img1: dreamimage,
                        lblDream: dreamName,
                        lblDreamEn: dreamNameEn,
                        lblDreamTh: dreamNameTh,
                        lblDreamTargetID: gblOpenActListSavingTarget[j]["DREAM_TARGET_ID"]
                    })
                    count = 0
                }
            }
            var data1 = list;
            frmIBOpenNewDreamAcc.coverFlowTP.data = data1;
            frmIBOpenNewDreamAcc.show();
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenActSelProd") {
        if (gblSelProduct == "ForUse") {
            IBsetDataForUse();
        } else if (gblSelProduct == "ForSave" || gblSelProduct == "TMBSavingcare") {
            IBsetDataForSave();
        } else if (gblSelProduct == "ForTerm") {
            IBsetDataForTD();
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewDreamAccConfirmation") {
        frmIBOpenNewDreamAccConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        frmIBOpenNewDreamAccConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBOpenNewDreamAccConfirmation.lblConfirmation.text = kony.i18n.getLocalizedString("keyConfirmation");
        //frmIBOpenNewDreamAccConfirmation.txtOTP.placeholder = kony.i18n.getLocalizedString("enterOTP");
        frmIBOpenNewDreamAccConfirmation.lblRequest.text = kony.i18n.getLocalizedString("keyOTPRequest");
        frmIBOpenNewDreamAccConfirmation.lblBankRefNo.text = kony.i18n.getLocalizedString("keyIBbankrefno");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewDreamAccConfirmation.lblMyDream.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewDreamAccConfirmation.lblInterest.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavName.text = gblFinActivityLogOpenAct["PrdctOpoenEN"];
            frmIBOpenNewDreamAccConfirmation.lblDreamSavings.text = gblFinActivityLogOpenAct["prodNameEN"];
            // frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavName.text = gblFinActivityLogOpenAct["nickEN"];
            frmIBOpenNewDreamAccConfirmation.lblOADSBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
        } else {
            frmIBOpenNewDreamAccConfirmation.lblMyDream.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewDreamAccConfirmation.lblInterest.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavName.text = gblFinActivityLogOpenAct["PrdctOpoenTH"];
            frmIBOpenNewDreamAccConfirmation.lblDreamSavings.text = gblFinActivityLogOpenAct["prodNameTH"];
            // frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavName.text = gblFinActivityLogOpenAct["nickTH"];
            frmIBOpenNewDreamAccConfirmation.lblOADSBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
        }
        if (savingMB == "SDA") {
            frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavNum.text = kony.i18n.getLocalizedString("SavingMB");
        } else if (currentMB == "DDA") {
            frmIBOpenNewDreamAccConfirmation.lblOAMnthlySavNum.text = kony.i18n.getLocalizedString("CurrentMB");
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewDreamAccComplete") {
        frmIBOpenNewDreamAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            //    frmIBOpenNewDreamAccComplete.lblMyDream.text = gblFinActivityLogOpenAct["prodNameEN"]; 
            frmIBOpenNewDreamAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewDreamAccComplete.lblNFAccName.text = gblFinActivityLogOpenAct["PrdctOpoenEN"];
            frmIBOpenNewDreamAccComplete.lblDSAckTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewDreamAccComplete.lblBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
            //frmIBOpenNewDreamAccComplete.lblNFAccName.text = gblFinActivityLogOpenAct["nickEN"];
        } else {
            //	       	frmIBOpenNewDreamAccComplete.lblMyDream.text = gblFinActivityLogOpenAct["prodNameTH"];  
            frmIBOpenNewDreamAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewDreamAccComplete.lblNFAccName.text = gblFinActivityLogOpenAct["PrdctOpoenTH"];
            frmIBOpenNewDreamAccComplete.lblDSAckTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewDreamAccComplete.lblBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
            //frmIBOpenNewDreamAccComplete.lblNFAccName.text =  gblFinActivityLogOpenAct["nickTH"];
        }
        if (savingMB == "SDA") {
            frmIBOpenNewDreamAccComplete.lblNFAccBal.text = kony.i18n.getLocalizedString("SavingMB");
        } else if (currentMB == "DDA") {
            frmIBOpenNewDreamAccComplete.lblNFAccBal.text = kony.i18n.getLocalizedString("CurrentMB");
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewSavingsAcc" && gblSelOpenActProdCode == "206") {
        var masterOpenData = [];
        var tempData;
        //    alert("cbcbcbcb")
        //frmIBOpenNewSavingsAcc.lblOpengAccFrom.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewSavingsAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenEN"]
            tempData = ["keyMBTransferEvryMnth", kony.i18n.getLocalizedString("keyTransferEvryMnthOpen"), true];
        } else {
            frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewSavingsAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenTH"]
            tempData = ["keyMBTransferEvryMnth", kony.i18n.getLocalizedString("keyTransferEvryMnthOpen"), true];
        }
        masterOpenData.push(tempData);
        for (i = 1; i <= 31; i++) {
            var trasDate;
            trasDate = [i, i, false];
            masterOpenData.push(trasDate);
        }
        if (frmIBOpenNewSavingsAcc.btnDreamSavecombo.selectedKeyValue[0] == "keyMBTransferEvryMnth") {
            frmIBOpenNewSavingsAcc.btnDreamSavecombo.masterData = masterOpenData;
        }
        //frmIBOpenNewSavingsAcc.txtOpenActNicNam.placeholder = kony.i18n.getLocalizedString("keylblNickname");
        frmIBOpenNewSavingsAcc.txtDSAmountSel.text = gblFinActivityLogOpenAct["dreamEditAmount"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "/" + kony.i18n.getLocalizedString("keyCalendarMonth");
        IBsetDataforOpenFromActsfromDream(gblOpenActList);
        //frmIBOpenNewSavingsAcc.show();
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewSavingsAcc" && gblSelOpenActProdCode != "206") {
        setOpenActCoverFlowIB(gblOpenActList);
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewSavingsAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenNSEN"]
            frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
        } else {
            frmIBOpenNewSavingsAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenNSTH"]
            frmIBOpenNewSavingsAcc.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
        }
        //frmIBOpenNewSavingsAcc.txtOpenActNicNam.placeholder = kony.i18n.getLocalizedString("keylblNickname");
        frmIBOpenNewSavingsAcc.show();
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewTermDepositAcc") {
        if (gblisDisToActTD == "Y") {
            if (frmIBOpenNewTermDepositAcc.imgPayInterest.isVisible == true) {
                setDataforTermDepositPayingAccts(gblOpenActList);
                /* if (kony.i18n.getCurrentLocale() == "en_US") {
				       	 	frmIBOpenNewTermDepositAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenTDEN"];
				   		} else {
				       	 	frmIBOpenNewTermDepositAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenTDTH"]
				    	}*/
            } else {
                setOpenActCoverFlowIB(gblOpenActList);
                /* if (kony.i18n.getCurrentLocale() == "en_US") {
		        	frmIBOpenNewTermDepositAcc.lblToAcc.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] ;
		   		} else {
		     		frmIBOpenNewTermDepositAcc.lblToAcc.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOTH"] ;
		    	}*/
            }
        } else {
            setOpenActCoverFlowIB(gblOpenActList);
        }
        if (kony.i18n.getCurrentLocale() == "en_US") {
            if (gblFinActivityLogOpenAct["PrdctOpoenTDEN"] != "" && gblFinActivityLogOpenAct["PrdctOpoenTDEN"] != null && gblFinActivityLogOpenAct["PrdctOpoenTDEN"] != undefined) frmIBOpenNewTermDepositAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenTDEN"];
            if (gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] != "" && gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] != null && gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] != undefined) frmIBOpenNewTermDepositAcc.lblToAcc.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"];
        } else {
            if (gblFinActivityLogOpenAct["PrdctOpoenTDEN"] != "" && gblFinActivityLogOpenAct["PrdctOpoenTDEN"] != null && gblFinActivityLogOpenAct["PrdctOpoenTDEN"] != undefined) frmIBOpenNewTermDepositAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenTDTH"]
            if (gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] != "" && gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] != null && gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"] != undefined) frmIBOpenNewTermDepositAcc.lblToAcc.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOTH"];
        }
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewTermDepositAcc.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
        } else {
            frmIBOpenNewTermDepositAcc.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
        }
        //frmIBOpenNewTermDepositAcc.txtDSNickName.placeholder = kony.i18n.getLocalizedString("keylblNickname");
        frmIBOpenNewTermDepositAcc.show();
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewSavingsAccConfirmation") {
        frmIBOpenNewSavingsAccConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        frmIBOpenNewSavingsAccConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBOpenNewSavingsAccConfirmation.lblConfirmation.text = kony.i18n.getLocalizedString("keyConfirmation");
        // 	frmIBOpenNewSavingsAccConfirmation.txtOTP.placeholder = kony.i18n.getLocalizedString("enterOTP");
        frmIBOpenNewSavingsAccConfirmation.lblRequest.text = kony.i18n.getLocalizedString("keyOTPRequest");
        frmIBOpenNewSavingsAccConfirmation.lblBankRefNo.text = kony.i18n.getLocalizedString("keyIBbankrefno");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewSavingsAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenNSEN"];
            frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewSavingsAccConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewSavingsAccConfirmation.lblBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
            //frmIBOpenNewSavingsAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["nickEN"];
        } else {
            frmIBOpenNewSavingsAccConfirmation.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewSavingsAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenNSTH"]
            frmIBOpenNewSavingsAccConfirmation.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewSavingsAccConfirmation.lblBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
            //frmIBOpenNewSavingsAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["nickTH"];
        }
        var accTypeDreamOpen;
        var savingMBSAves = "";
        var currentMBSAves = "";
        var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
        if (fdata.lblaccType == "SDA") {
            savingMBSAves = "SDA";
            accTypeDreamOpenSAves = kony.i18n.getLocalizedString("SavingMB")
        } else if (fdata.lblaccType == "DDA") {
            currentMBSAves = "DDA";
            accTypeDreamOpenSAves = kony.i18n.getLocalizedString("CurrentMB");
        }
        frmIBOpenNewSavingsAccConfirmation.lblActNoval.text = accTypeDreamOpenSAves;
        frmIBOpenNewSavingsAccConfirmation.lblAddressHeader.text = appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewSavingsAccComplete") {
        frmIBOpenNewSavingsAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
        if (gblSelOpenActProdCode == "220") {
            frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyNoFeeGreet');
        } else if (gblSelOpenActProdCode == "200") {
            frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyNormalSavGreet');
        } else if (gblSelOpenActProdCode == "221") {
            frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyNoFixedGreet');
        } else if (gblSelOpenActProdCode == "222") {
            frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyFreeFlowGreet');
        } else if (gblSelOpenActProdCode == "225") {
            frmIBOpenNewSavingsAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyAllFreeGreet');
        }
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewSavingsAccComplete.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenNSEN"];
            frmIBOpenNewSavingsAccComplete.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewSavingsAccComplete.lblProdNameVal.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewSavingsAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewSavingsAccComplete.lblBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
            //frmIBOpenNewSavingsAccComplete.lblAccNSName.text = gblFinActivityLogOpenAct["nickEN"];
        } else {
            frmIBOpenNewSavingsAccComplete.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenNSTH"]
            frmIBOpenNewSavingsAccComplete.lblProdNameVal.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewSavingsAccComplete.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewSavingsAccComplete.lblInterestRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewSavingsAccComplete.lblBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
            //frmIBOpenNewSavingsAccComplete.lblAccNSName.text = gblFinActivityLogOpenAct["nickTH"];
        }
        var accTypeDreamOpen;
        savingMBSAves = "";
        currentMBSAves = "";
        var fdata = frmIBOpenNewSavingsAcc.coverFlowTP.data[gblCWSelectedItem];
        if (fdata.lblaccType == "SDA") {
            savingMBSAves = "SDA";
            accTypeDreamOpenSAves = kony.i18n.getLocalizedString("SavingMB")
        } else if (fdata.lblaccType == "DDA") {
            currentMBSAves = "DDA";
            accTypeDreamOpenSAves = kony.i18n.getLocalizedString("CurrentMB");
        }
        frmIBOpenNewSavingsAccComplete.lblActNoval.text = accTypeDreamOpenSAves;
        frmIBOpenNewSavingsAccComplete.lblAddressHeader.text = appendColon(kony.i18n.getLocalizedString("keyAddressMailingCard"));
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewTermDepositAccConfirmation") {
        frmIBOpenNewTermDepositAccConfirmation.lblConfirmation.text = kony.i18n.getLocalizedString("keyConfirmation");
        frmIBOpenNewTermDepositAccConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBOpenNewTermDepositAccConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        frmIBOpenNewTermDepositAccConfirmation.button475119159465827.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBOpenNewTermDepositAccConfirmation.button475119159465829.text = kony.i18n.getLocalizedString("keyConfirm");
        //frmIBOpenNewTermDepositAccConfirmation.txtOTP.placeholder = kony.i18n.getLocalizedString("enterOTP");
        frmIBOpenNewTermDepositAccConfirmation.lblBankRefNo.text = kony.i18n.getLocalizedString("keyIBbankrefno");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewTermDepositAccConfirmation.lblIntrstRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewTermDepositAccConfirmation.lblPayingToName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"];
            frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenTDEN"]
            frmIBOpenNewTermDepositAccConfirmation.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewTermDepositAccConfirmation.lblBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
            //frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["nickEN"];
        } else {
            frmIBOpenNewTermDepositAccConfirmation.lblIntrstRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewTermDepositAccConfirmation.lblPayingToName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOTH"];
            frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTH"];
            frmIBOpenNewTermDepositAccConfirmation.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewTermDepositAccConfirmation.lblBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
            //frmIBOpenNewTermDepositAccConfirmation.lblAccNSName.text = gblFinActivityLogOpenAct["nickTH"];
        }
        if (savingMBTDOpen == "SDA") {
            frmIBOpenNewTermDepositAccConfirmation.lblOpeningFromAccount.text = kony.i18n.getLocalizedString("SavingMB");
        } else if (currentMBTDOpen == "DDA") {
            frmIBOpenNewTermDepositAccConfirmation.lblOpeningFromAccount.text = kony.i18n.getLocalizedString("CurrentMB");
        }
        if (gblisDisToActTD == "Y") {
            if (savingMBTDTOOpen == "SDA") {
                frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccount.text = kony.i18n.getLocalizedString("SavingMB");
            } else if (currentMBTDTOOpen == "DDA") {
                frmIBOpenNewTermDepositAccConfirmation.lblPayingToAccount.text = kony.i18n.getLocalizedString("CurrentMB");
            }
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewTermDepositAccComplete") {
        frmIBOpenNewTermDepositAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewTermDepositAccComplete.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewTermDepositAccComplete.lblInterst.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewTermDepositAccComplete.lblBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
            frmIBOpenNewTermDepositAccComplete.lblPayingToName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOEN"];
            frmIBOpenNewTermDepositAccComplete.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenTDEN"]
                //frmIBOpenNewTermDepositAccComplete.lblAccNSName.text = gblFinActivityLogOpenAct["nickEN"];
        } else {
            frmIBOpenNewTermDepositAccComplete.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewTermDepositAccComplete.lblInterst.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewTermDepositAccComplete.lblBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
            frmIBOpenNewTermDepositAccComplete.lblPayingToName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTOTH"];
            frmIBOpenNewTermDepositAccComplete.lblAccNSName.text = gblFinActivityLogOpenAct["PrdctOpoenTDTH"];
            // frmIBOpenNewTermDepositAccComplete.lblAccNSName.text  = gblFinActivityLogOpenAct["nickTH"];
        }
        if (gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301" || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602") {
            frmIBOpenNewTermDepositAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyTermGreet');
        } else if (gblSelOpenActProdCode == "659") {
            frmIBOpenNewTermDepositAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyTermUpGreet');
        } else if (gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666") {
            frmIBOpenNewTermDepositAccComplete.lblCongrats.text = kony.i18n.getLocalizedString('keyTermQuickGreet');
        }
        if (savingMBTDOpen == "SDA") {
            frmIBOpenNewTermDepositAccComplete.lblAccountBalance.text = kony.i18n.getLocalizedString("SavingMB");
        } else if (currentMBTDOpen == "DDA") {
            frmIBOpenNewTermDepositAccComplete.lblAccountBalance.text = kony.i18n.getLocalizedString("CurrentMB");
        }
        if (gblisDisToActTD == "Y") {
            if (savingMBTDTOOpen == "SDA") {
                frmIBOpenNewTermDepositAccComplete.lblPayingToAccount.text = kony.i18n.getLocalizedString("SavingMB");
            } else if (currentMBTDTOOpen == "DDA") {
                frmIBOpenNewTermDepositAccComplete.lblPayingToAccount.text = kony.i18n.getLocalizedString("CurrentMB");
            }
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewSavingsCareAcc") {
        setOpenActCoverFlowIB(gblOpenActList);
        frmIBOpenNewSavingsCareAcc.lblBeneficiaries.text = kony.i18n.getLocalizedString('keyBeneficiaries');
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewSavingsCareAcc.lblSavinCareSelProdTxt.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewSavingsCareAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenCareEN"];
        } else {
            frmIBOpenNewSavingsCareAcc.lblSavinCareSelProdTxt.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewSavingsCareAcc.lblFromAccount.text = gblFinActivityLogOpenAct["PrdctOpoenCareTH"]
        }
        //frmIBOpenNewSavingsCareAcc.txtNickName.placeholder = kony.i18n.getLocalizedString("keylblNickname");
        /* var cmbxOneSelKey =  frmIBOpenNewSavingsCareAcc.cmbxOne.selectedKeyValue[0];
			var cmbxTwoSelKey =  frmIBOpenNewSavingsCareAcc.cmbxTwo.selectedKeyValue[0];
			var cmbxThreeSelKey =  frmIBOpenNewSavingsCareAcc.cmbxThree.selectedKeyValue[0];
			var cmbxFourSelKey =  frmIBOpenNewSavingsCareAcc.cmbxFour.selectedKeyValue[0];
			var cmbxFiveSelKey =  frmIBOpenNewSavingsCareAcc.cmbxFive.selectedKeyValue[0];
			
			var masterOpenData = [];
        	var count = 0;
			for(i=1;i<=8;i++){
			var trasDate;
			
						
					if (count == 0) {
							trasDate = ["P",kony.i18n.getLocalizedString("keyOpenRelation")];
							count = count + 1
						}
						else if (count == 1) {
							trasDate = ["F",kony.i18n.getLocalizedString("keyFather")];
							count = count + 1
						}
						else if (count == 2) {
							trasDate = ["M",kony.i18n.getLocalizedString("keyMother")];
							count = count + 1
						}else if (count == 3) {
							trasDate = ["R",kony.i18n.getLocalizedString("keyRelative")];
							count = count + 1
						}else if (count == 4) {
							trasDate = ["S",kony.i18n.getLocalizedString("keySpouseReg")];
							count = count + 1
						}else if (count == 5) {
							trasDate = ["L",kony.i18n.getLocalizedString("keySpouseNL")];
							count = count + 1
						}
						else if (count == 6) {
							trasDate = ["C",kony.i18n.getLocalizedString("keyChild")];
							count = count + 1
						}else if (count == 7) {
							trasDate = ["O",kony.i18n.getLocalizedString("keyOtherRelation")];
							count = 0;
							break;
						}
						masterOpenData.push(trasDate);
			}
			frmIBOpenNewSavingsCareAcc.cmbxOne.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxTwo.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxThree.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxFour.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxFive.masterData = masterOpenData;
			frmIBOpenNewSavingsCareAcc.cmbxOne.selectedKeyValue[0] = "P";
			frmIBOpenNewSavingsCareAcc.cmbxTwo.selectedKeyValue[0] = "P";
			frmIBOpenNewSavingsCareAcc.cmbxThree.selectedKeyValue[0] = "P";
			frmIBOpenNewSavingsCareAcc.cmbxFour.selectedKeyValue[0] = "P";
			frmIBOpenNewSavingsCareAcc.cmbxFive.selectedKeyValue[0] = "P";
//			frmIBOpenNewSavingsCareAcc.txtBefFisrtName1.placeholder = kony.i18n.getLocalizedString("keyFSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefFisrtName2.placeholder = kony.i18n.getLocalizedString("keyFSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefFisrtName3.placeholder = kony.i18n.getLocalizedString("keyFSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefFisrtName4.placeholder = kony.i18n.getLocalizedString("keyFSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefFisrtName5.placeholder = kony.i18n.getLocalizedString("keyFSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefSecName1.placeholder = kony.i18n.getLocalizedString("keyLSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefSecName2.placeholder = kony.i18n.getLocalizedString("keyLSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefSecName3.placeholder = kony.i18n.getLocalizedString("keyLSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefSecName4.placeholder = kony.i18n.getLocalizedString("keyLSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefSecName5.placeholder = kony.i18n.getLocalizedString("keyLSNamePH");
//			frmIBOpenNewSavingsCareAcc.txtBefBen1.placeholder = kony.i18n.getLocalizedString("keyBenefitPH");
//			frmIBOpenNewSavingsCareAcc.txtBefBen2.placeholder = kony.i18n.getLocalizedString("keyBenefitPH");
//			frmIBOpenNewSavingsCareAcc.txtBefBen3.placeholder = kony.i18n.getLocalizedString("keyBenefitPH");
//			frmIBOpenNewSavingsCareAcc.txtBefBen4.placeholder = kony.i18n.getLocalizedString("keyBenefitPH");
//			frmIBOpenNewSavingsCareAcc.txtBefBen5.placeholder = kony.i18n.getLocalizedString("keyBenefitPH");*/
        frmIBOpenNewSavingsCareAcc.lblBeneficiaries.text = kony.i18n.getLocalizedString("keyBeneficiaries");
        frmIBOpenNewSavingsCareAcc.btnSpecify.text = kony.i18n.getLocalizedString("keyBeneficiariesSpecify");
        frmIBOpenNewSavingsCareAcc.btnNotSpecify.text = kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify");
        frmIBOpenNewSavingsCareAcc.btnKYCBack.text = kony.i18n.getLocalizedString("keyCancelButton");
        changeMasterdatainRelation();
        frmIBOpenNewSavingsCareAcc.show();
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewSavingsCareAccConfirmation") {
        kony.print("Coming here new" + gblOpenActBenList.length)
        frmIBOpenNewSavingsCareAccConfirmation.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBenificiaries");
        if (gblOpenActBenList.length != 0) {
            frmIBOpenNewSavingsCareAccConfirmation.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesSpecify")
        } else {
            frmIBOpenNewSavingsCareAccConfirmation.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify")
        }
        frmIBOpenNewSavingsCareAccConfirmation.lblSegBeneficiaryName.text = kony.i18n.getLocalizedString("keyBeneficiaryName");
        frmIBOpenNewSavingsCareAccConfirmation.lblSegBeneficiaryRelation.text = kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
        frmIBOpenNewSavingsCareAccConfirmation.lblSegBeneficiaryBenefit.text = kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
        frmIBOpenNewSavingsCareAccConfirmation.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
        frmIBOpenNewSavingsCareAccConfirmation.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        //	frmIBOpenNewSavingsCareAccConfirmation.txtOTP.placeholder = kony.i18n.getLocalizedString("enterOTP");
        frmIBOpenNewSavingsCareAccConfirmation.lblRequest.text = kony.i18n.getLocalizedString("keyOTPRequest");
        frmIBOpenNewSavingsCareAccConfirmation.lblBankRefNo.text = kony.i18n.getLocalizedString("keyIBbankrefno");
        /* if(arrbenf1.length > 0)
    	{frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal1.text=RelationI18n(arrbenf1);}
    	if(arrbenf2.length > 0)
    	{frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal2.text=RelationI18n(arrbenf2);}
    	if(arrbenf3.length > 0)
    	{frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal3.text=RelationI18n(arrbenf3);}
    	if(arrbenf4.length > 0)
    	{frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal4.text=RelationI18n(arrbenf4);}
    	if(arrbenf5.length > 0)
    	{frmIBOpenNewSavingsCareAccConfirmation.lblBefRsCnfrmVal5.text=RelationI18n(arrbenf5);} */
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNameAck.text = gblFinActivityLogOpenAct["PrdctOpoenCareEN"];
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
            // frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNameAck.text = gblFinActivityLogOpenAct["nickEN"];
        } else {
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNameAck.text = gblFinActivityLogOpenAct["PrdctOpoenCareTH"]
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
            //frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNameAck.text = gblFinActivityLogOpenAct["nickTH"];
        }
        if (savingMBCareOpen == "SDA") {
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNumAck.text = kony.i18n.getLocalizedString("SavingMB");
        } else if (currentMBCareOpen == "DDA") {
            frmIBOpenNewSavingsCareAccConfirmation.lblOASCFrmNumAck.text = kony.i18n.getLocalizedString("CurrentMB");
        }
        changeRelationTab();
        setBenificiariesSegment();
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenNewSavingsCareAccComplete") {
        kony.print("Coming here new" + gblOpenActBenList.length)
        if (gblOpenActBenList.length != 0) {
            frmIBOpenNewSavingsCareAccComplete.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesSpecify")
            changeRelationOnComplete();
        } else {
            frmIBOpenNewSavingsCareAccComplete.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBeneficiaries") + ":" + kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify")
        }
        /* frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrm1.text = kony.i18n.getLocalizedString("keyRelation");
        frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrm2.text = kony.i18n.getLocalizedString("keyRelation");
        frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrm3.text = kony.i18n.getLocalizedString("keyRelation");
        frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrm4.text = kony.i18n.getLocalizedString("keyRelation");
        frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrm5.text = kony.i18n.getLocalizedString("keyRelation"); */
        /* frmIBOpenNewSavingsCareAccComplete.lblBefNameCnfrm1.text = kony.i18n.getLocalizedString("keyNameOpen");
        frmIBOpenNewSavingsCareAccComplete.lblBefNameCnfrm2.text = kony.i18n.getLocalizedString("keyNameOpen");
        frmIBOpenNewSavingsCareAccComplete.lblBefNameCnfrm3.text = kony.i18n.getLocalizedString("keyNameOpen");
        frmIBOpenNewSavingsCareAccComplete.lblBefNameCnfrm4.text = kony.i18n.getLocalizedString("keyNameOpen");
        frmIBOpenNewSavingsCareAccComplete.lblBefNameCnfrm5.text = kony.i18n.getLocalizedString("keyNameOpen"); */
        /* if(arrbenf1.length > 0)
    	{frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrmVal1.text=RelationI18n(arrbenf1);}
    	if(arrbenf2.length > 0)
    	{frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrmVal2.text=RelationI18n(arrbenf2);}
    	if(arrbenf3.length > 0)
    	{frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrmVal3.text=RelationI18n(arrbenf3);}
    	if(arrbenf4.length > 0)
    	{frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrmVal4.text=RelationI18n(arrbenf4);}
    	if(arrbenf5.length > 0)
    	{frmIBOpenNewSavingsCareAccComplete.lblBefRsCnfrmVal5.text=RelationI18n(arrbenf5);} */
        frmIBOpenNewSavingsCareAccComplete.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne");
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmIBOpenNewSavingsCareAccComplete.lblNFAccName.text = gblFinActivityLogOpenAct["PrdctOpoenCareEN"];
            frmIBOpenNewSavingsCareAccComplete.lblOASCTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
            frmIBOpenNewSavingsCareAccComplete.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
            frmIBOpenNewSavingsCareAccComplete.lblOASCBranchVal.text = gblFinActivityLogOpenAct["branchEN"];
            //frmIBOpenNewSavingsCareAccComplete.lblNFAccName.text = gblFinActivityLogOpenAct["nickEN"];
        } else {
            frmIBOpenNewSavingsCareAccComplete.lblNFAccName.text = gblFinActivityLogOpenAct["PrdctOpoenCareTH"]
            frmIBOpenNewSavingsCareAccComplete.lblOASCTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
            frmIBOpenNewSavingsCareAccComplete.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
            frmIBOpenNewSavingsCareAccComplete.lblOASCBranchVal.text = gblFinActivityLogOpenAct["branchTH"];
            // frmIBOpenNewSavingsCareAccComplete.lblNFAccName.text = gblFinActivityLogOpenAct["nickTH"];
        }
        if (gblFinActivityLogOpenAct["toBranchName"] != undefined && gblFinActivityLogOpenAct["toBranchName"] != "") {
            gblFinActivityLogOpenAct["toBranchName"] = frmIBOpenNewSavingsCareAccComplete.lblOASCBranchVal.text;
        }
        if (savingMBCareOpen == "SDA") {
            frmIBOpenNewSavingsCareAccComplete.lblNFAccBal.text = kony.i18n.getLocalizedString("SavingMB");
        } else if (currentMBCareOpen == "DDA") {
            frmIBOpenNewSavingsCareAccComplete.lblNFAccBal.text = kony.i18n.getLocalizedString("CurrentMB");
        }
    }
    if (kony.application.getCurrentForm().id == "frmIBOpenProdMailAddressEdit") {
        if (frmIBOpenProdMailAddressEdit.hbxcnf2.isVisible) frmIBOpenProdMailAddressEdit.btnConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
        else {
            frmIBOpenProdMailAddressEdit.btnConfirm.text = kony.i18n.getLocalizedString("keysave");
            IBdropDownStatePopulate();
        }
    }
}