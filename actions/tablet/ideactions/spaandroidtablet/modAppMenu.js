function setDataToMenuSeg() {
    var segMenuTable;
    if (flowSpa) {
        segMenuTable = [{
            imgMenuIcon: "hotpromotions.png",
            lblMenuItem: "Hot Promotions",
            imgMenuChevron: "navarrow.png"
        }, {
            imgMenuIcon: "findatm.png",
            lblMenuItem: "Find TMB",
            imgMenuChevron: "navarrow.png"
        }, {
            imgMenuIcon: "exhrates.png",
            lblMenuItem: "Exchange Rates",
            imgMenuChevron: "navarrow.png"
        }, {
            imgMenuIcon: "apptour.png",
            lblMenuItem: "App Tour",
            imgMenuChevron: "navarrow.png"
        }, {
            imgMenuIcon: "",
            lblMenuItem: "",
            imgMenuChevron: "empty.png"
        }, {
            imgMenuIcon: "",
            lblMenuItem: "",
            imgMenuChevron: "empty.png"
        }]
    } else {
        segMenuTable = [{
            imgMenuIcon: "hotpromotions.png",
            lblMenuItem: "Hot Promotions",
            imgMenuChevron: "navarrow.png"
        }, {
            imgMenuIcon: "findatm.png",
            lblMenuItem: "Find TMB",
            imgMenuChevron: "navarrow.png"
        }, {
            imgMenuIcon: "exhrates.png",
            lblMenuItem: "Exchange Rates",
            imgMenuChevron: "navarrow.png"
        }, {
            imgMenuIcon: "apptour.png",
            lblMenuItem: "App Tour",
            imgMenuChevron: "navarrow.png"
        }]
    }
    frmPostLoginMenuNew.SegMainMenu.setData(segMenuTable);
}