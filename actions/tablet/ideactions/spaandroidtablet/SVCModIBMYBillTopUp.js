var myBillerList = new Array();
myBillerList = [];
var myBillerSuggestList = new Array();
var myTopupList = new Array();
var myTopupSuggestList = new Array();
var isSearchedBiller = false;
var isCatFilteredBiller = false;
var isSearchedTopup = false;
var isCatFilteredTopup = false;
var search = "";
var isSelectBillerEnabled = false;
var isSelectTopupEnabled = false;
var channelFlagBillerIB = 1;
var channelFlagTopupIB = 2;
var gblIsRef2RequiredIB = "N";
var channelFlagBillerMB = 3;
var channelFlagTopupMB = 4;
var gblSuggestedBiller = false;
//channelflagvalue = 1 for IB Biller, 2 for IB Topup ,3 for MB Biller , 4 for MB Topup
var gblBillerCompCode = "";
var gblBillerId = "";
var gblBillerMethod = "";
var gblRef2Flag = "";
var gblIsRef2RequiredIB = "";
var gblRef1LenIB = "";
var gblRef2LenIB = "";
var gblTopUpSelectFormDataHolderIB = [];
var gblCustTopUpSelectFormDataHolderIB = [];
var gblMasterBillerData = [];
var gblMasterTopupData = [];
var gblCustTopUpMore = 0;
var gblBillerFromMenu = false;
var gblTopupFromMenu = false;
var gblBillerLastSearchNoCat = [];
var gblTopupLastSearchNoCat = [];
var gblOTPLockedForBiller = "0";
var gblEmailIdBillerNotification = "";
var gblBillerName = "";
var gblBillerNickName = "";
var gblBillerNewNickName = "";
var gblBillerRef1 = "";
var gblBillerRef2 = "";
var gblMasterBillerSelectIB = []; //Added by Bhaskar
var gblMasterTopupSelectIB = []; //Added by Bhaskar
var callServiceFlag = "";
var callTopupServiceFlag = "";
var gblAddBillerButtonClicked = false;
var gblAddTopupButtonClicked = false;
gblstepMinAmount = []; //Added by kony
//Added by Ramanuja
var gblNickNameEN = "";
var gblNickNameTH = "";

function getMyBillListIB() {
    var inputParams = {
        IsActive: "1"
            //crmId: gblcrmId
            //clientDate: getCurrentDate() not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBillInquiry", inputParams, startMyBillListIBServiceAsyncCallback);
}

function startMyBillListIBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            frmIBMyBillersHome.segBillersList.removeAll();
            var responseData = callBackResponse["CustomerBillInqRs"];
            gblMyBillList = [];
            if (responseData.length > 0) {
                gblMyBillList = responseData;
                populateMyBill(callBackResponse["CustomerBillInqRs"]);
                dismissLoadingScreenPopup();
            } else {
                frmIBMyBillersHome.segBillersList.removeAll();
                myBillerList = [];
                dismissLoadingScreenPopup();
                //Added by Bhaskar
                frmIBMyBillersHome.lblMyBills.setVisibility(false);
                frmIBMyBillersHome.hbxMore.setVisibility(false);
                frmIBMyBillersHome.line588686174252372.setVisibility(false);
                frmIBMyBillersHome.lblNoBills.setVisibility(true);
                frmIBMyBillersHome.tbxSearch.setFocus(true)
                    //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
                    //alert(kony.i18n.getLocalizedString("ECGenericError"));
                    //Ended by Bhaskar
            }
        }
    } else {
        if (status == 300) {
            frmIBMyBillersHome.segBillersList.removeAll();
            myBillerList = [];
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}
var gbllocaleIBBillerTopup = "";

function getLocaleBillerIB() {
    gbllocaleIBBillerTopup = kony.i18n.getCurrentLocale();
}

function getLocaleBillerIBMyTopUp() {
    frmIBMyTopUpsHome.segBillersList.setVisibility(true);
    frmIBMyTopUpsHome.lblMyBills.setVisibility(true);
    frmIBMyTopUpsHome.hbxMore.setVisibility(true);
    frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
    isSelectTopupEnabled = false;
    frmIBMyTopUpsHome.tbxSearch.text = "";
    frmIBMyTopUpsHome.lblOTPinCurr.text = "";
    frmIBMyTopUpsHome.lblPlsReEnter.text = "";
    //frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (!isIE8) {
        frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
    }
    gblTopUpMore = 0;
    gblCustTopUpMore = 0;
    search = "";
    frmIBMyTopUpsHome.tbxSearch.text = ""
    gbllocaleIBBillerTopup = kony.i18n.getCurrentLocale();
}

function populateMyBill(collectionData) {
    search = "";
    search = frmIBMyBillersHome.tbxSearch.text;
    var searchtxt = search.toLowerCase();
    var i = 0;
    var tmpSearchList = [];
    var tmpCatList = [];
    tmpSearchList.length = 0;
    tmpCatList.length = 0;
    var tmpRef2label = "";
    var tmpRef2Len = 0;
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    var tmpRef2label = "";
    var ref2val = ""
    var Ref2LblEng = "";
    var Ref2LblThai = "";
    var tmpIsRef2Req = "";
    if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    //   if(!isSearchedBiller && !isCatFilteredBiller )
    //   {
    //  var i = 0;
    myBillerList.length = 0;
    for (i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BillerGroupType"] == gblGroupTypeBiller) {
            tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Add"];
            if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
                ref2val = collectionData[i]["ReferenceNumber2"];
                Ref2LblThai = collectionData[i]["LabelReferenceNumber2TH"];
                Ref2LblEng = collectionData[i]["LabelReferenceNumber2EN"];
            } else {
                ref2val = "";
                Ref2LblThai = "";
                Ref2LblEng = "";
                collectionData[i][ref2label] = "";
            }
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var tempRecord = {
                "crmId": collectionData[i]["CRMID"],
                "lblBillerNickname": {
                    "text": collectionData[i]["BillerNickName"]
                },
                "lblBillerName": {
                    "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "lblRef1Value": {
                    "text": collectionData[i]["maskedReferenceNumber1"]
                },
                // masking CR code
                "lblViewBillerRef1ValueActual": {
                    "text": collectionData[i]["ReferenceNumber1"]
                },
                "lblRef2Value": {
                    "text": ref2val
                },
                "imgBillerLogo": {
                    "src": imagesUrl
                },
                "imgBillersRtArrow": {
                    "src": "bg_arrow_right.png"
                },
                "lblRef1": {
                    "text": collectionData[i][ref1label] + " :"
                },
                "lblRef2": {
                    "text": collectionData[i][ref2label] + " :"
                },
                "BillerID": {
                    "text": collectionData[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "BillerGroupType": collectionData[i]["BillerGroupType"],
                "BillerMethod": collectionData[i]["BillerMethod"],
                "IsFullPayment": collectionData[i]["IsFullPayment"],
                "ToAccountKey": collectionData[i]["ToAccountKey"],
                "IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
                "IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
                "Ref2Len": collectionData[i]["Ref2Len"],
                "Ref1Len": collectionData[i]["Ref1Len"],
                "BeepndBill": {
                    "text": collectionData[i]["BeepNBillFlag"]
                },
                "CustomerBillerID": {
                    "text": collectionData[i]["CustomerBillID"]
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                },
                "Ref1LblEng": {
                    "text": collectionData[i]["LabelReferenceNumber1EN"]
                },
                "Ref1LblThai": {
                    "text": collectionData[i]["LabelReferenceNumber1TH"]
                },
                "Ref2LblEng": {
                    "text": Ref2LblEng
                },
                "Ref2LblThai": {
                    "text": Ref2LblThai
                },
                "BillerNameEng": {
                    "text": collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "BillerNameThai": {
                    "text": collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                }
            }
            myBillerList.push(tempRecord);
        }
    }
    if (myBillerList.length == 0) {
        frmIBMyBillersHome.segBillersList.removeAll();
        myBillerList = [];
        //Added by Bhaskar
        frmIBMyBillersHome.lblMyBills.setVisibility(false);
        frmIBMyBillersHome.hbxMore.setVisibility(false);
        frmIBMyBillersHome.line588686174252372.setVisibility(false);
        frmIBMyBillersHome.lblNoBills.setVisibility(true);
        //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
        //Ended by Bhaskar
    } else {
        searchCustomBillsIB();
    }
}

function getMyTopUpListIB() {
    var inputParams = {
        IsActive: "1"
    }
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBillInquiry", inputParams, startMyTopUpListIBServiceAsyncCallback);
}

function startMyTopUpListIBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            //if(callBackResponse["StatusCode"]
            frmIBMyTopUpsHome.segBillersList.removeAll();
            var responseData = callBackResponse["CustomerBillInqRs"];
            gblMyBillList = [];
            if (responseData.length > 0) {
                gblMyBillList = responseData;
                populateMyTopUp(callBackResponse["CustomerBillInqRs"]);
                var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
                if (!isIE8) {
                    frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
                }
                //frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
                frmIBMyTopUpsHome.tbxSearch.setFocus(true);
                dismissLoadingScreenPopup();
            } else {
                frmIBMyTopUpsHome.segBillersList.removeAll();
                var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
                if (!isIE8) {
                    frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
                }
                //frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
                frmIBMyTopUpsHome.tbxSearch.setFocus(true);
                myTopupList = [];
                dismissLoadingScreenPopup();
                //Added by Bhaskar
                frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
                frmIBMyTopUpsHome.hbxMore.setVisibility(false);
                frmIBMyTopUpsHome.line588686174252372.setVisibility(false);
                frmIBMyTopUpsHome.lblNoBills.setVisibility(true);
                //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
                //Ended by Bhaskar
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } else {
        if (status == 300) {
            frmIBMyTopUpsHome.segBillersList.removeAll();
            myTopupList = [];
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function populateMyTopUp(collectionData) {
    var billername = "";
    var ref1label = "";
    search = "";
    search = frmIBMyTopUpsHome.tbxSearch.text;
    var searchtxt = search.toLowerCase();
    var i = 0;
    var tmpSearchList = [];
    tmpSearchList.length = 0;
    var tmpCatList = [];
    tmpCatList.length = 0;
    if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
    } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
    }
    //if(!isSearchedTopup)
    //	{
    myTopupList.length = 0;
    for (var i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BillerGroupType"] == gblGroupTypeTopup) {
            var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            //below line is added for CR - PCI-DSS masked Credit card no
            var maskedRef1 = maskCreditCard(collectionData[i]["ReferenceNumber1"]);
            var tempRecord = {
                "crmId": collectionData[i]["CRMID"],
                "lblBillerNickname": {
                    "text": collectionData[i]["BillerNickName"]
                },
                "lblBillerName": {
                    "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "lblRef1Value": {
                    "text": maskedRef1
                },
                "ref1ActualVal": {
                    "text": collectionData[i]["ReferenceNumber1"]
                },
                "imgBillerLogo": {
                    "src": imagesUrl
                },
                "imgBillersRtArrow": {
                    "src": "bg_arrow_right.png"
                },
                "lblRef1": {
                    "text": collectionData[i][ref1label]
                },
                "BillerID": {
                    "text": collectionData[i]["BillerID"]
                },
                "BillerGroupType": collectionData[i]["BillerGroupType"],
                "ToAccountKey": collectionData[i]["ToAccountKey"],
                "IsFullPayment": collectionData[i]["IsFullPayment"],
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "BeepndBill": {
                    "text": collectionData[i]["BeepNBillFlag"]
                },
                "CustomerBillerID": {
                    "text": collectionData[i]["CustomerBillID"]
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                },
                "Ref1LblEng": {
                    "text": collectionData[i]["LabelReferenceNumber1EN"]
                },
                "Ref1LblThai": {
                    "text": collectionData[i]["LabelReferenceNumber1TH"]
                },
                "BillerNameEng": {
                    "text": collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "BillerNameThai": {
                    "text": collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                }
            }
            myTopupList.push(tempRecord);
        }
    }
    if (myTopupList.length == 0) {
        frmIBMyTopUpsHome.segBillersList.removeAll();
        myTopupList = [];
        //Added by Bhaskar
        frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
        frmIBMyTopUpsHome.hbxMore.setVisibility(false);
        frmIBMyTopUpsHome.line588686174252372.setVisibility(false);
        frmIBMyTopUpsHome.lblNoBills.setVisibility(true);
        //alert(kony.i18n.getLocalizedString("keyaddbillerstolist"));
        //Ended by Bhaskar
    } else {
        searchCustomTopUpsIB();
    }
}
var flagSearch = 0;
var flagCat = 0;
var currentData = 0;
var suggestedCatData = 0;
var catData = 0;
var suggestedSearchData = 0;
var searchData = 0;

function searchSuggestedBillers() {
    gblTopUpMore = 0;
    search = "";
    // alert("Search Text "+ frmIBMyBillersHome.tbxSearch.text)
    search = frmIBMyBillersHome.tbxSearch.text;
    if (search.length >= 3) {
        callServiceFlag = true;
        frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
        isSearchedBiller = true;
        //gblMasterBillerSelectIB = gblBillerLastSearchNoCat;
        getMyBillSuggestListIB();
    } else {
        if (!isSelectBillerEnabled) frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
        //gblTopUpSelectFormDataHolderIB = gblMasterBillerSelectIB;
        if (callServiceFlag && search.length >= 0) {
            getMasterBillSelectListIB();
            callServiceFlag = false;
        }
    }
}
//function sugBillerCatChangeNew() {
//	gblTopUpMore = 0;
//	var showCatSuggestList = [];
//	var tmpCatSelected = frmIBMyBillersHome.cbxSelectCat.selectedKey;
//	
//	
//	var j = 0;
//	var tmp = "";
//	var lengthBillersSuggestList = gblMasterBillerSelectIB.length;
//	if(isSelectBillerEnabled)
//		frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
//	if (null != tmpCatSelected && tmpCatSelected != 0) {
//		for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {
//			
//			if (gblMasterBillerSelectIB[i].BillerCategoryID.text == tmpCatSelected) {
//				showCatSuggestList[j] = gblMasterBillerSelectIB[i];
//				
//				j++;
//			}
//		}
//		if (showCatSuggestList.length == 0)
//			alert(kony.i18n.getLocalizedString("keybillernotfound"));
//		gblTopUpSelectFormDataHolderIB = showCatSuggestList;
//		onMyTopUpSelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB);
//		//frmIBMyBillersHome.segSuggestedBillersList.setData(showCatSuggestList);
//	} else {
//		if (gblMasterBillerSelectIB.length == 0)
//			alert(kony.i18n.getLocalizedString("keybillernotfound"));
//		gblTopUpSelectFormDataHolderIB = gblMasterBillerSelectIB;
//		onMyTopUpSelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB);
//		//frmIBMyBillersHome.segSuggestedBillersList.setData(gblMasterBillerSelectIB);
//	}
//}
function sugBillerCatChange() {
    gblTopUpMore = 0;
    search = "";
    // alert("Search Text "+ frmIBMyBillersHome.tbxSearch.text)
    search = frmIBMyBillersHome.tbxSearch.text;
    var showCatSuggestList = [];
    var tmpCatSelected = frmIBMyBillersHome.cbxSelectCat.selectedKey;
    var j = 0;
    var tmp = "";
    var lengthBillersSuggestList = gblBillerLastSearchNoCat.length;
    if (isSelectBillerEnabled) frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
    if (null != tmpCatSelected && tmpCatSelected != 0) {
        for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {
            if (gblBillerLastSearchNoCat[i].BillerCategoryID.text == tmpCatSelected) {
                showCatSuggestList[j] = gblBillerLastSearchNoCat[i];
                j++;
            }
        }
        if (gblSugBillers == 0 && search.length < 3) {
            if (frmIBMyBillersHome.lblNoBills.isVisible) frmIBMyBillersHome.lblNoBills.setVisibility(false);
            frmIBMyBillersHome.lblErrorMsg.setVisibility(true);
            gblSugBillers = 1;
        } else if (showCatSuggestList.length == 0 && gblSugBillers == 0) {
            //alert(kony.i18n.getLocalizedString("keybillernotfound"));
            if (frmIBMyBillersHome.lblNoBills.isVisible) frmIBMyBillersHome.lblNoBills.setVisibility(false);
            frmIBMyBillersHome.lblErrorMsg.setVisibility(true);
            gblSugBillers = 1;
        }
        gblTopUpSelectFormDataHolderIB = showCatSuggestList;
        onMyTopUpSelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB);
        //frmIBMyBillersHome.segSuggestedBillersList.setData(showCatSuggestList);
    } else {
        if (gblBillerLastSearchNoCat.length == 0 && gblSugBillers == 0) {
            //alert(kony.i18n.getLocalizedString("keybillernotfound"));
            if (frmIBMyBillersHome.lblNoBills.isVisible) frmIBMyBillersHome.lblNoBills.setVisibility(false);
            frmIBMyBillersHome.lblErrorMsg.setVisibility(true);
            gblSugBillers = 1;
        }
        gblTopUpSelectFormDataHolderIB = gblBillerLastSearchNoCat;
        onMyTopUpSelectMoreDynamicIB(gblTopUpSelectFormDataHolderIB);
        //frmIBMyBillersHome.segSuggestedBillersList.setData(gblBillerLastSearchNoCat);
    }
}

function searchCustomBillsIB() {
    gblCustTopUpMore = 0;
    var showSearchList = [];
    var showCatList = [];
    var searchCustomerData = [];
    var searchMasterData = [];
    var lowerCaseName = "";
    var showSearchSuggestList = [];
    search = "";
    // alert("Search Text "+ frmIBMyBillersHome.tbxSearch.text)
    search = frmIBMyBillersHome.tbxSearch.text;
    var tmpCatSelected = frmIBMyBillersHome.cbxSelectCat.selectedKey;
    var j = 0;
    var lengthBillersList = myBillerList.length;
    var lowerCaseNameBiller = "";
    if (search.length >= 3) {
        frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
        var searchtxt = search.toLowerCase();
        // var regexp = new RegExp("(" + searchtxt + ")", "ig");
        for (j = 0, i = 0; i < lengthBillersList; i++) {
            if (myBillerList[i]["lblBillerNickname"]) {
                lowerCaseName = myBillerList[i]["lblBillerNickname"]["text"].toLowerCase();
                lowerCaseNameBiller = myBillerList[i]["lblBillerName"]["text"].toLowerCase();
                //if (regexp.test(tmpBillerData[i]["lblBillerNickname"]["text"]) == true) {
                // if (lowerCaseName.contains(searchtxt)) {
                if (lowerCaseName.indexOf(searchtxt) >= 0 || lowerCaseNameBiller.indexOf(searchtxt) >= 0) {
                    showSearchList[j] = myBillerList[i];
                    j++;
                }
            }
        }
        searchCustomerData = showSearchList;
    } else {
        searchCustomerData = myBillerList;
        if (myBillerList.length == 0) frmIBMyBillersHome.lblNoBills.setVisibility(true);
        frmIBMyBillersHome.lblErrorMsg.setVisibility(false);
        if (!isSelectBillerEnabled) // added for suggested billers to be visible once selected biller mode is enabled
            frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
    }
    if (null != tmpCatSelected && tmpCatSelected != 0) {
        var searchLen = searchCustomerData.length;
        for (j = 0, i = 0; i < searchLen; i++) {
            tmp = searchCustomerData[i].BillerCategoryID.text
            if (tmpCatSelected == tmp) {
                showCatList[j] = searchCustomerData[i];
                j++;
            }
        }
        searchCustomerData = showCatList;
    } else {
        showCatList = searchCustomerData;
    }
    if (showCatList.length == 0) {
        frmIBMyBillersHome.segBillersList.removeAll();
        frmIBMyBillersHome.lblMyBills.setVisibility(false);
        frmIBMyBillersHome.hbxMore.setVisibility(false);
        frmIBMyBillersHome.line588686174252372.setVisibility(false);
        if (!isSelectBillerEnabled) gblSugBillers = 0;
        //alert(kony.i18n.getLocalizedString("keybillernotfound"));
    } else {
        gblCustTopUpSelectFormDataHolderIB = showCatList;
        onMyCustTopUpSelectMoreDynamicIB(showCatList);
        //frmIBMyBillersHome.segBillersList.setData(showCatList);
    }
}

function searchSuggestedTopUps() {
    search = "";
    // alert("Search Text "+ frmIBMyTopUpsHome.tbxSearch.text)
    search = frmIBMyTopUpsHome.tbxSearch.text;
    if (search.length >= 3) {
        callTopupServiceFlag = true;
        frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(true);
        isSearchedTopup = true;
        getMyTopUpSuggestListIB();
    } else {
        if (!isSelectTopupEnabled) frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
        if (callTopupServiceFlag && search.length >= 0) {
            getMyTopUpSelectListIB();
            callTopupServiceFlag = false;
        }
    }
}
gblSugBillers = 1;

function sugTopUpCatChange() {
    var showCatSuggestList = [];
    gblTopUpMore = 0;
    search = "";
    search = frmIBMyTopUpsHome.tbxSearch.text;
    var tmpCatSelected = frmIBMyTopUpsHome.cbxSelectCat.selectedKey;
    var j = 0;
    var tmp = "";
    var lengthBillersSuggestList = gblTopupLastSearchNoCat.length;
    if (isSelectTopupEnabled) frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(true);
    if (null != tmpCatSelected && tmpCatSelected != 0) {
        for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {
            if (gblTopupLastSearchNoCat[i].BillerCategoryID.text == tmpCatSelected) {
                showCatSuggestList[j] = gblTopupLastSearchNoCat[i];
                j++;
            }
        }
        //gblTopUpSelectFormDataHolderIB= showCatSuggestList;
        if (gblSugTopups == 0 && search.length < 3) {
            if (frmIBMyTopUpsHome.lblNoBills.isVisible) frmIBMyTopUpsHome.lblNoBills.setVisibility(false);
            frmIBMyTopUpsHome.lblErrorMsg.setVisibility(true);
            gblSugTopups = 1;
        } else if (showCatSuggestList.length == 0 && gblSugTopups == 0) {
            //alert(kony.i18n.getLocalizedString("keybillernotfound"));
            if (frmIBMyTopUpsHome.lblNoBills.isVisible) frmIBMyTopUpsHome.lblNoBills.setVisibility(false);
            frmIBMyTopUpsHome.lblErrorMsg.setVisibility(true);
            gblSugTopups = 1;
        }
        gblTopUpSelectFormDataHolderIB = showCatSuggestList;
        onMyTopUpSelectMoreDynamicIB(showCatSuggestList);
        //frmIBMyTopUpsHome.segSuggestedBillersList.setData(showCatSuggestList);
    } else {
        if (gblTopupLastSearchNoCat.length == 0 && gblSugTopups == 0) {
            if (frmIBMyTopUpsHome.lblNoBills.isVisible) frmIBMyTopUpsHome.lblNoBills.setVisibility(false);
            frmIBMyTopUpsHome.lblErrorMsg.setVisibility(true);
            //alert(kony.i18n.getLocalizedString("keybillernotfound"));
            gblSugTopups = 1;
        }
        gblTopUpSelectFormDataHolderIB = gblTopupLastSearchNoCat;
        onMyTopUpSelectMoreDynamicIB(gblTopupLastSearchNoCat);
        //frmIBMyTopUpsHome.segSuggestedBillersList.setData(gblTopupLastSearchNoCat);
    }
}
gblSugTopups = 1;

function searchCustomTopUpsIB() {
    gblCustTopUpMore = 0;
    var showSearchList = [];
    var showCatList = [];
    var searchCustomerData = [];
    var searchMasterData = [];
    var lowerCaseName = "";
    var showSearchSuggestList = [];
    search = "";
    // alert("Search Text "+ frmIBMyTopUpsHome.tbxSearch.text)
    search = frmIBMyTopUpsHome.tbxSearch.text;
    var tmpCatSelected = frmIBMyTopUpsHome.cbxSelectCat.selectedKey;
    var j = 0;
    var lengthBillersList = myTopupList.length;
    if (search.length >= 3) {
        frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(true);
        var searchtxt = search.toLowerCase();
        var lowerCaseNameBiller = "";
        // var regexp = new RegExp("(" + searchtxt + ")", "ig");
        for (j = 0, i = 0; i < lengthBillersList; i++) {
            if (myTopupList[i]["lblBillerNickname"]) {
                lowerCaseName = myTopupList[i]["lblBillerNickname"]["text"].toLowerCase();
                lowerCaseNameBiller = myTopupList[i]["lblBillerName"]["text"].toLowerCase();
                //if (regexp.test(tmpBillerData[i]["lblBillerNickname"]["text"]) == true) {
                // if (lowerCaseName.contains(searchtxt)) {
                if (lowerCaseName.indexOf(searchtxt) >= 0 || lowerCaseNameBiller.indexOf(searchtxt) >= 0) {
                    showSearchList[j] = myTopupList[i];
                    j++;
                }
            }
        }
        searchCustomerData = showSearchList;
    } else {
        searchCustomerData = myTopupList;
        if (myTopupList.length == 0) frmIBMyTopUpsHome.lblNoBills.setVisibility(true);
        frmIBMyTopUpsHome.lblErrorMsg.setVisibility(false);
        if (!isSelectTopupEnabled) frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
    }
    if (null != tmpCatSelected && tmpCatSelected != 0) {
        var searchLen = searchCustomerData.length;
        for (j = 0, i = 0; i < searchLen; i++) {
            tmp = searchCustomerData[i].BillerCategoryID.text
            if (tmpCatSelected == tmp) {
                showCatList[j] = searchCustomerData[i];
                j++;
            }
        }
        searchCustomerData = showCatList;
    } else {
        showCatList = searchCustomerData;
    }
    if (showCatList.length == 0) {
        frmIBMyTopUpsHome.segBillersList.removeAll();
        frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
        frmIBMyTopUpsHome.line588686174252372.setVisibility(false);
        frmIBMyTopUpsHome.hbxMore.setVisibility(false);
        if (!isSelectTopupEnabled) gblSugTopups = 0;
        //alert(kony.i18n.getLocalizedString("keybillernotfound"));
    } else {
        gblCustTopUpSelectFormDataHolderIB = showCatList;
        onMyCustTopUpSelectMoreDynamicIB(showCatList);
        //frmIBMyBillersHome.segBillersList.setData(showCatList);
    }
    //frmIBMyBillersHome.segSuggestedBillersList.setData(showSearchSuggestList);
    //frmIBMyTopUpsHome.segBillersList.setData(showCatList);
}

function startDisplayBillerCategoryServiceIB() {
    var inputParams = {
        BillerCategoryGroupType: gblBillerCategoryGroupType
            // clientDate: getCurrentDate() not working on IE 8
    };
    //alert("calling billerCategoryInquiry  from my biller");
    showLoadingScreenPopup();
    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, startDisplayBillerListServiceIBAsyncCallback);
}

function startDisplayBillerListServiceIBAsyncCallback(status, callBackResponse) {
    //alert("called billerCategoryInquiry  from my biller");
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            var collectionData = callBackResponse["BillerCategoryInqRs"];
            var selCat = kony.i18n.getLocalizedString('keyBillPaymentSelectCategory');
            var masterData = [
                [0, selCat]
            ];
            var tmpCatDesc = "";
            var tempData = 0;
            if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) tmpCatDesc = "BillerCategoryDescEN"
            else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) tmpCatDesc = "BillerCategoryDescTH"
            for (var i = 0; i < collectionData.length; i++) {
                invokeCommonIBLogger("BILLER CODE " + collectionData[i]["BillerCategoryID"] + "BILLER NAME " + collectionData[i][
                    tmpCatDesc
                ]);
                tempData = [collectionData[i]["BillerCategoryID"], collectionData[i][tmpCatDesc]];
                masterData.push(tempData);
            }
            frmIBMyBillersHome.cbxSelectCat.masterData = masterData;
            frmIBMyBillersHome.tbxSearch.setFocus(true);
            dismissLoadingScreenPopup();
        } else {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}
var billerId;

function viewMybilldetail() {
    gblCustomerBillerID = "";
    gblBillerNickName = "";
    gblBillerName = "";
    gblBillerRef1 = "";
    gblBillerRef2 = "";
    //if (kony.string.equalsIgnoreCase(frmIBMyBillersHome.segBillersList.selectedItems[0].BeepndBill.text, "Y")) {
    //		frmIBMyBillersHome.hbxApplyBeepNBill.setVisibility(true);
    //		//frmIBMyBillersHome.lnkViewBillerApplyBeep.setEnabled(true);
    //	} else {
    //		frmIBMyBillersHome.hbxApplyBeepNBill.setVisibility(false);
    //		// frmIBMyBillersHome.lnkViewBillerApplyBeep.setEnabled(false);
    //	}
    frmIBMyBillersHome.hbxApplyBeepNBill.setVisibility(false);
    frmIBMyBillersHome.lnkViewBillerApplyBeep.setEnabled(false);
    billerId = frmIBMyBillersHome.segBillersList.selectedItems[0].BillerID.text;
    gblCustomerBillerID = frmIBMyBillersHome.segBillersList.selectedItems[0].CustomerBillerID.text;
    gblBillerNickName = frmIBMyBillersHome.segBillersList.selectedItems[0].lblBillerNickname.text;
    gblBillerName = frmIBMyBillersHome.segBillersList.selectedItems[0].lblBillerName.text;
    gblBillerRef1 = frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef1Value.text;
    gblBillerRef2 = frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef2Value.text;
    frmIBMyBillersHome.lblViewBillerName.text = frmIBMyBillersHome.segBillersList.selectedItems[0].lblBillerNickname.text;
    frmIBMyBillersHome.lblViewBillerNameCompCode.text = frmIBMyBillersHome.segBillersList.selectedItems[0].lblBillerName.text;
    frmIBMyBillersHome.lblViewBillerRef1.text = frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef1.text;
    gblCurViewRef1Eng = frmIBMyBillersHome.segBillersList.selectedItems[0].Ref1LblEng.text;
    gblCurViewRef1Thai = frmIBMyBillersHome.segBillersList.selectedItems[0].Ref1LblThai.text;
    //below line is changed/added for CR - PCI-DSS masked Credit card no
    frmIBMyBillersHome.lblViewBillerRef1Value.text = frmIBMyBillersHome.segBillersList.selectedItems[0].lblViewBillerRef1ValueActual.text;
    frmIBMyBillersHome.lblViewBillerRef1ValMasked.text = frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef1Value.text;
    frmIBMyBillersHome.lblViewBillerRef2.text = "";
    if (!kony.string.equalsIgnoreCase(frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef2.text, "")) {
        if (frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef2.text.length > 2) {
            frmIBMyBillersHome.lblViewBillerRef2.text = frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef2.text;
        }
    } else {
        frmIBMyBillersHome.lblViewBillerRef2.text = "";
    }
    gblCurViewRef2Eng = frmIBMyBillersHome.segBillersList.selectedItems[0].Ref2LblEng.text;
    gblCurViewRef2Thai = frmIBMyBillersHome.segBillersList.selectedItems[0].Ref2LblThai.text;
    gblCurViewBillerEng = frmIBMyBillersHome.segBillersList.selectedItems[0].BillerNameEng.text;
    gblCurViewBillerThai = frmIBMyBillersHome.segBillersList.selectedItems[0].BillerNameThai.text;
    frmIBMyBillersHome.lblViewBillerRef2Value.text = frmIBMyBillersHome.segBillersList.selectedItems[0].lblRef2Value.text;
    frmIBMyBillersHome.imgViewBillerLogo.src = frmIBMyBillersHome.segBillersList.selectedItems[0].imgBillerLogo.src;
    //	gblTopupDeleteIB = frmIBMyBillersHome.segBillersList.selectedIndex[1];
}
var gblCurViewRef1Eng = "";
var gblCurViewRef1Thai = "";
var gblCurViewRef2Eng = "";
var gblCurViewRef2Thai = "";
var gblCurViewBillerEng = "";
var gblCurViewBillerThai = "";

function viewMyTopUpdetail() {
    gblCustomerBillerID = "";
    gblBillerNickName = "";
    gblBillerName = "";
    gblBillerRef1 = "";
    // if (frmIBMyTopUpsHome.segBillersList.selectedItems[0].BeepndBill.text == 0) {
    //
    //        frmIBMyTopUpsHome.lnkViewBillerApplyBeep.setVisibility(true);
    //        frmIBMyTopUpsHome.lnkViewBillerApplyBeep.setEnabled(true);
    //    } else {
    //
    //        frmIBMyTopUpsHome.lnkViewBillerApplyBeep.setVisibility(false);
    //        frmIBMyTopUpsHome.lnkViewBillerApplyBeep.setEnabled(false);
    //    }
    billerId = frmIBMyTopUpsHome.segBillersList.selectedItems[0].BillerID.text;
    gblCustomerBillerID = frmIBMyTopUpsHome.segBillersList.selectedItems[0].CustomerBillerID.text;
    gblBillerNickName = frmIBMyTopUpsHome.segBillersList.selectedItems[0].lblBillerNickname.text;
    gblBillerName = frmIBMyTopUpsHome.segBillersList.selectedItems[0].lblBillerName.text;
    //below line is changed for CR - PCI-DSS masked Credit card no
    gblBillerRef1 = frmIBMyTopUpsHome.segBillersList.selectedItems[0].ref1ActualVal.text;
    frmIBMyTopUpsHome.lblViewBillerName.text = frmIBMyTopUpsHome.segBillersList.selectedItems[0].lblBillerNickname.text;
    frmIBMyTopUpsHome.lblViewBillerNameCompCode.text = frmIBMyTopUpsHome.segBillersList.selectedItems[0].lblBillerName.text;
    frmIBMyTopUpsHome.lblViewBillerRef1.text = frmIBMyTopUpsHome.segBillersList.selectedItems[0].lblRef1.text + ":";
    gblCurViewRef1Eng = frmIBMyTopUpsHome.segBillersList.selectedItems[0].Ref1LblEng.text;
    gblCurViewRef1Thai = frmIBMyTopUpsHome.segBillersList.selectedItems[0].Ref1LblThai.text;
    gblCurViewBillerEng = frmIBMyTopUpsHome.segBillersList.selectedItems[0].BillerNameEng.text;
    gblCurViewBillerThai = frmIBMyTopUpsHome.segBillersList.selectedItems[0].BillerNameThai.text;
    frmIBMyTopUpsHome.lblViewBillerRef1Value.text = frmIBMyTopUpsHome.segBillersList.selectedItems[0].lblRef1Value.text;
    frmIBMyTopUpsHome.imgViewBillerLogo.src = frmIBMyTopUpsHome.segBillersList.selectedItems[0].imgBillerLogo.src;
}

function getMyBillSuggestListIB() {
    if (isSearchedBiller) var inputParams = {
        billerName: search,
        IsActive: "1",
        BillerGroupType: gblGroupTypeBiller,
        flagBillerList: "IB"
            //clientDate: getCurrentDate()not working on IE 8
    };
    else var inputParams = {
        IsActive: "1",
        BillerGroupType: gblGroupTypeBiller,
        flagBillerList: "IB"
            // clientDate: getCurrentDate()not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMyBillSuggestListIBServiceAsyncCallback);
}

function startMyBillSuggestListIBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (gblBillerFromMenu) {
                frmIBMyBillersHome.show();
                gblBillerFromMenu = false;
            }
            if (responseData.length > 0) {
                frmIBMyBillersHome.lblErrorMsg.setVisibility(false);
                populateMyBillSuggest(callBackResponse["MasterBillerInqRs"]);
                // getMyBillListIB(); // calling customer Bill Inquiry	
                dismissLoadingScreenPopup();
            } else {
                var collectionData = [];
                setBillerCollectionDataToGlobalVariable(collectionData)
                frmIBMyBillersHome.segSuggestedBillersList.removeAll();
                dismissLoadingScreenPopup();
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } else {
        if (status == 300) {
            var collectionData = [];
            setBillerCollectionDataToGlobalVariable(collectionData)
            frmIBMyBillersHome.segSuggestedBillersList.removeAll();
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function setBillerCollectionDataToGlobalVariable(collectionData) {
    var tmpRef2Len = "";
    var tmpRef2label = "";
    var tmpRef2Len = 0;
    var billername = "";
    var billerCatName = "";
    var ref1label = "";
    var ref2label = "";
    var tempRecord = [];
    var tmpIsRef2Req = "";
    if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
        billername = "BillerNameEN";
        billerCatName = "BillerCatgoryNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
        billername = "BillerNameTH";
        billerCatName = "BillerCatgoryNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    gblBillerLastSearchNoCat.length = 0;
    //	var preLoadedBillsInd = false;
    //	if(gblMasterBillerSelectIB.length == 0)
    //		preLoadedBillsInd = true;
    for (var i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BillerGroupType"] == gblGroupTypeBiller) {
            tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Add"];
            //if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
            tmpRef2Len = collectionData[i]["Ref2Len"];
            tmpRef2label = collectionData[i][ref2label];
            /*} else {
				tmpRef2Len = 0;
				tmpRef2Label = "";
			}*/
            //ENH113 
            gblbillerStartTime = "";
            gblbillerEndTime = "";
            gblbillerFullPay = "";
            gblbillerAllowSetSched = "";
            gblbillerTotalPayAmtEn = "";
            gblbillerTotalPayAmtTh = "";
            gblbillerTotalIntEn = "";
            gblbillerTotalIntTh = "";
            gblbillerDisconnectAmtEn = "";
            gblbillerDisconnectAmtTh = "";
            gblbillerServiceType = "";
            gblbillerTransType = "";
            gblMeaFeeAmount = "";
            gblbillerAmountEn = "";
            gblbillerAmountTh = "";
            gblbillerMeterNoEn = "";
            gblbillerMeterNoTh = "";
            gblbillerCustNameEn = "";
            gblbillerCustNameTh = "";
            gblbillerCustAddressEn = "";
            gblbillerCustAddressTh = "";
            if (collectionData[i]["BillerCompcode"] == "2533") {
                if (collectionData[i]["BillerMiscData"] != undefined && collectionData[i]["BillerMiscData"].length > 0) {
                    for (var j = 0; j < collectionData[i]["BillerMiscData"].length; j++) {
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.StartTime") {
                            gblbillerStartTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.EndTime") {
                            gblbillerEndTime = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.FullPayment") {
                            gblbillerFullPay = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.AllowSetSchedule") {
                            gblbillerAllowSetSched = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.EN") {
                            gblbillerMeterNoEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD1.LABEL.TH") {
                            gblbillerMeterNoTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.EN") {
                            gblbillerCustNameEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD2.LABEL.TH") {
                            gblbillerCustNameTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.EN") {
                            gblbillerCustAddressEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD3.LABEL.TH") {
                            gblbillerCustAddressTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.EN") {
                            gblbillerTotalPayAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD4.LABEL.TH") {
                            gblbillerTotalPayAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.EN") {
                            gblbillerAmountEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD5.LABEL.TH") {
                            gblbillerAmountTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.EN") {
                            gblbillerTotalIntEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD6.LABEL.TH") {
                            gblbillerTotalIntTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.EN") {
                            gblbillerDisconnectAmtEn = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "OLN.FIELD7.LABEL.TH") {
                            gblbillerDisconnectAmtTh = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.ServiceType") {
                            gblbillerServiceType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                        if (collectionData[i]["BillerMiscData"][j]["MiscName"] == "BILLER.TransactionType") {
                            gblbillerTransType = collectionData[i]["BillerMiscData"][j]["MiscText"];
                        }
                    }
                }
            }
            kony.print("gblbillerStartTime: " + gblbillerStartTime + "gblbillerEndTime :" + gblbillerEndTime);
            //ENH113 end
            imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            tempRecord = {
                    "lblSugBillerName": {
                        "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                    },
                    "lblSuggestedCatName": {
                        "text": collectionData[i][billerCatName]
                    },
                    "btnSugBillerAdd": {
                        "text": "Add",
                        "skin": "btnIBaddsmall"
                    },
                    "BillerID": {
                        "text": collectionData[i]["BillerID"]
                    },
                    "BillerCompCode": {
                        "text": collectionData[i]["BillerCompcode"]
                    },
                    "Ref1Label": {
                        "text": collectionData[i][ref1label]
                    },
                    "Ref2Label": {
                        "text": tmpRef2label
                    },
                    "IsRequiredRefNumber2Add": {
                        "text": collectionData[i]["IsRequiredRefNumber2Add"]
                    },
                    "IsRequiredRefNumber2Pay": {
                        "text": collectionData[i]["IsRequiredRefNumber2Pay"]
                    },
                    "EffDt": {
                        "text": collectionData[i]["EffDt"]
                    },
                    "BarcodeOnly": collectionData[i]["BarcodeOnly"],
                    "BillerGroupType": collectionData[i]["BillerGroupType"],
                    "ToAccountKey": collectionData[i]["ToAccountKey"],
                    "IsFullPayment": collectionData[i]["IsFullPayment"],
                    "BillerMethod": {
                        "text": collectionData[i]["BillerMethod"]
                    },
                    "Ref1Len": {
                        "text": collectionData[i]["Ref1Len"]
                    },
                    "Ref2Len": {
                        "text": tmpRef2Len
                    },
                    "BillerCategoryID": {
                        "text": collectionData[i]["BillerCategoryID"]
                    },
                    "imgSugBillerLogo": {
                        "src": imagesUrl
                    },
                    "Ref1LblEng": {
                        "text": collectionData[i]["LabelReferenceNumber1EN"]
                    },
                    "Ref1LblThai": {
                        "text": collectionData[i]["LabelReferenceNumber1TH"]
                    },
                    "Ref2LblEng": {
                        "text": collectionData[i]["LabelReferenceNumber2EN"]
                    },
                    "Ref2LblThai": {
                        "text": collectionData[i]["LabelReferenceNumber2TH"]
                    },
                    "BillerNameEng": {
                        "text": collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                    },
                    "BillerNameThai": {
                        "text": collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                    },
                    "billerStartTime": gblbillerStartTime,
                    "billerEndTime": gblbillerEndTime,
                    "billerFullPay": gblbillerFullPay,
                    "billerAllowSetSched": gblbillerAllowSetSched,
                    "billerTotalPayAmtEn": gblbillerTotalPayAmtEn,
                    "billerTotalPayAmtTh": gblbillerTotalPayAmtTh,
                    "billerTotalIntEn": gblbillerTotalIntEn,
                    "billerTotalIntTh": gblbillerTotalIntTh,
                    "billerDisconnectAmtEn": gblbillerDisconnectAmtEn,
                    "billerDisconnectAmtTh": gblbillerDisconnectAmtTh,
                    "billerServiceType": gblbillerServiceType,
                    "billerTransType": gblbillerTransType,
                    "billerFeeAmount": gblMeaFeeAmount,
                    "billerAmountEn": gblbillerAmountEn,
                    "billerAmountTh": gblbillerAmountTh,
                    "billerMeterNoEn": gblbillerMeterNoEn,
                    "billerMeterNoTh": gblbillerMeterNoTh,
                    "billerCustNameEn": gblbillerCustNameEn,
                    "billerCustNameTh": gblbillerCustNameTh,
                    "billerCustAddressEn": gblbillerCustAddressEn,
                    "billerCustAddressTh": gblbillerCustAddressTh,
                    "billerBancassurance": collectionData[i]["IsBillerBancassurance"],
                    "allowRef1AlphaNum": collectionData[i]["AllowRef1AlphaNum"]
                }
                //			if(preLoadedBillsInd){
                //				gblMasterBillerSelectIB.push(tempRecord);
                //			}
            gblBillerLastSearchNoCat.push(tempRecord);
        }
    }
    sugBillerCatChange();
}

function setTopUpCollectionDataToGlobalVariable(collectionData) {
    var billername = "";
    var categoryName = "";
    var ref1label = "";
    var ref2label = "";
    var tempRecord = [];
    if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
        billername = "BillerNameEN";
        categoryName = "BillerCatgoryNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
        billername = "BillerNameTH";
        categoryName = "BillerCatgoryNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    gblTopupLastSearchNoCat.length = 0;
    var tempMinAmount = [];
    for (var i = 0; i < collectionData.length; i++) {
        /* minimum amount locha */
        var billerMethod = collectionData[i]["BillerMethod"];
        if (billerMethod == "1" && collectionData[i]["BillerGroupType"] == gblGroupTypeTopup && collectionData[i]["BillerCompcode"] != "2605") {
            var min = collectionData[i]["StepAmount"][0]["Amt"];
            tempMinAmount = {
                "compcode": collectionData[i]["BillerCompcode"],
                "minAmount": min
            }
        } else {
            tempMinAmount = [];
        }
        if (collectionData[i]["BillerGroupType"] == gblGroupTypeTopup) {
            imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var tempRecord = {
                "lblSugBillerName": {
                    "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "lblSugBillerCatName": {
                    "text": collectionData[i][categoryName]
                },
                "btnSugBillerAdd": {
                    "text": "Add",
                    "skin": "btnIBaddsmall"
                },
                "BillerID": {
                    "text": collectionData[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "Ref1Label": {
                    "text": collectionData[i][ref1label]
                },
                "EffDt": {
                    "text": collectionData[i]["EffDt"]
                },
                "BillerMethod": {
                    "text": collectionData[i]["BillerMethod"]
                },
                "BarcodeOnly": collectionData[i]["BarcodeOnly"],
                "BillerGroupType": collectionData[i]["BillerGroupType"],
                "ToAccountKey": collectionData[i]["ToAccountKey"],
                "IsFullPayment": collectionData[i]["IsFullPayment"],
                "IsRequiredRefNumber2Add": collectionData[i]["IsRequiredRefNumber2Add"],
                "IsRequiredRefNumber2Pay": collectionData[i]["IsRequiredRefNumber2Pay"],
                "Ref1Len": {
                    "text": collectionData[i]["Ref1Len"]
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                },
                "imgSugBillerLogo": {
                    "src": imagesUrl
                },
                "Ref1LblEng": {
                    "text": collectionData[i]["LabelReferenceNumber1EN"]
                },
                "Ref1LblThai": {
                    "text": collectionData[i]["LabelReferenceNumber1TH"]
                },
                "BillerNameEng": {
                    "text": collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "BillerNameThai": {
                    "text": collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                }
            }
            if (tempMinAmount.length != 0) {
                gblstepMinAmount.push(tempMinAmount);
            }
            gblTopupLastSearchNoCat.push(tempRecord);
        }
    }
    sugTopUpCatChange();
}

function populateMyBillSuggest(collectionData) {
    setBillerCollectionDataToGlobalVariable(collectionData)
}

function startDisplayTopUpCategoryServiceIB() {
    var inputParams = {
        BillerCategoryGroupType: gblTopupCategoryGroupType
            //clientDate: getCurrentDate() not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, startDisplayMyTopUpListServiceIBAsyncCallback);
}

function startDisplayMyTopUpListServiceIBAsyncCallback(status, callBackResponse) {
    //alert("called billerCategoryInquiry  from my topup");
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            var collectionData = callBackResponse["BillerCategoryInqRs"];
            var selCat = kony.i18n.getLocalizedString('keyBillPaymentSelectCategory');
            var masterData = [
                [0, selCat]
            ];
            var tempData = 0;
            var tmpCatDesc = "";
            if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) tmpCatDesc = "BillerCategoryDescEN"
            else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) tmpCatDesc = "BillerCategoryDescTH"
            for (var i = 0; i < collectionData.length; i++) {
                invokeCommonIBLogger("BILLER cat Id " + collectionData[i]["BillerCategoryID"] + "BILLER NAME " + collectionData[i][
                    tmpCatDesc
                ]);
                tempData = [collectionData[i]["BillerCategoryID"], collectionData[i][tmpCatDesc]];
                masterData.push(tempData);
            }
            frmIBMyTopUpsHome.cbxSelectCat.masterData = masterData;
            var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
            if (!isIE8) {
                frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
            }
            //frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
            frmIBMyTopUpsHome.tbxSearch.setFocus(true);
            dismissLoadingScreenPopup();
        } else {
            var masterData = [];
            dismissLoadingScreenPopup();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    } else {
        if (status == 300) {
            var masterData = [];
            dismissLoadingScreenPopup();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}

function getMyTopUpSuggestListIB() {
    if (isSearchedTopup) var inputParams = {
        billerName: search,
        IsActive: "1",
        BillerGroupType: gblGroupTypeTopup,
        flagBillerList: "IB"
            //clientDate: getCurrentDate()not working on IE 8
    };
    else var inputParams = {
        IsActive: "1",
        BillerGroupType: gblGroupTypeTopup,
        flagBillerList: "IB"
            //clientDate: getCurrentDate()not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMyTopUpSuggestListIBServiceAsyncCallback);
}

function startMyTopUpSuggestListIBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (gblTopupFromMenu) {
                frmIBMyTopUpsHome.show();
                gblTopupFromMenu = false;
            }
            if (responseData.length > 0) {
                populateMyTopUpSuggest(callBackResponse["MasterBillerInqRs"]);
                // getMyTopUpListIB(); //Calling customer Bill Inquiry  
                dismissLoadingScreenPopup();
            } else {
                var collectionData = [];
                setTopUpCollectionDataToGlobalVariable(collectionData);
                frmIBMyTopUpsHome.segSuggestedBillersList.removeAll();
                dismissLoadingScreenPopup();
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } else {
        if (status == 300) {
            var collectionData = [];
            setTopUpCollectionDataToGlobalVariable(collectionData);
            frmIBMyTopUpsHome.segSuggestedBillersList.removeAll();
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function populateMyTopUpSuggest(collectionData) {
    setTopUpCollectionDataToGlobalVariable(collectionData)
}
var tmpBillerNewNickName = "";

function callCustomerBillUpdateIB() {
    tmpBillerNickName = frmIBMyBillersHome.lblViewBillerName.text;
    tmpBillerName = frmIBMyBillersHome.lblViewBillerNameCompCode.text;
    tmpBillerNewNickName = frmIBMyBillersHome.txtEditBillerNickName.text;
    tmpBillerRef1 = "";
    tmpBillerRef2 = "";
    var inputparam = {};
    //inputparam["crmId"] = gblcrmId;
    inputparam["BillerNickName"] = frmIBMyBillersHome.txtEditBillerNickName.text;
    inputparam["BillerID"] = billerId; //"BillerID"; //---->?
    inputparam["ReferenceNumber1"] = frmIBMyBillersHome.lblViewBillerRef1Value.text;
    tmpBillerRef1 = frmIBMyBillersHome.lblViewBillerRef1Value.text;
    inputparam["ReferenceNumber2"] = frmIBMyBillersHome.lblViewBillerRef2Value.text;
    tmpBillerRef2 = frmIBMyBillersHome.lblViewBillerRef2Value.text;
    inputparam["CustomerBillID"] = gblCustomerBillerID;
    //inputparam["clientDate"] = getCurrentDate();not working on IE 8
    var activityTypeID = "061";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Edit";
    var activityFlexValues2 = tmpBillerName;
    var activityFlexValues3 = tmpBillerNickName + "+" + tmpBillerNewNickName;
    var activityFlexValues4 = tmpBillerRef1;
    var activityFlexValues5 = tmpBillerRef2;
    var logLinkageId = "";
    inputparam["flex1"] = activityFlexValues1;
    inputparam["flex2"] = activityFlexValues2;
    inputparam["flex3"] = activityFlexValues3;
    inputparam["flex4"] = activityFlexValues4;
    inputparam["flex5"] = activityFlexValues5;
    inputparam["typeID"] = activityTypeID;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBillUpdate", inputparam, callBackCustomerBillUpdateIB)
}

function callBackCustomerBillUpdateIB(status, resulttable) {
    var activityTypeID = "061";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Edit";
    var activityFlexValues2 = tmpBillerName;
    var activityFlexValues3 = tmpBillerNickName + "+" + tmpBillerNewNickName;
    var activityFlexValues4 = tmpBillerRef1;
    var activityFlexValues5 = tmpBillerRef2;
    var logLinkageId = "";
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] != "0") {
                dismissLoadingScreenPopup();
                alert(resulttable["errMsg"]);
                activityStatus = "02";
            } else {
                frmIBMyBillersHome.lblViewBillerName.text = frmIBMyBillersHome.txtEditBillerNickName.text;
                frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
                frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyBillersHome.hbxOtpBox.setVisibility(false)
                frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(true);
                //frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(true);//for complete screen
                //frmIBMyBillersHome.hbxViewBillerNameDelEdit.setVisibility(true);//for complete screen
                //frmIBMyBillersHome.segAddBillerComplete.setVisibility(false);//for complete screen
                //frmIBMyBillersHome.hbxReturnAddMore.setVisibility(false);//for complete screen
                frmIBMyBillersHome.imgEditComplete.setVisibility(true); //added tickmark image 
                activityStatus = "01";
                dismissLoadingScreenPopup();
                postShowForBillers();
                //frmIBMyBillersHome.postShow();
            }
        } else {
            activityStatus = "02";
            dismissLoadingScreenPopup();
            /**kony.application.dismissLoadingScreen();**/
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        } //if
    } //else
} //callBackCustomerBillUpdateIB
function checkDuplicateNicknameOnEditBillerIB() {
    var BillerList = myBillerList;
    //var ConfirmationList = frmIBMyBillersHome.segBillersConfirm.data;
    var nickname = frmIBMyBillersHome.txtEditBillerNickName.text;
    var check = "";
    var val = "";
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {
        val = BillerList[i].lblBillerNickname.text;
        if (kony.string.equalsIgnoreCase(nickname, val)) {
            return false;
        }
    }
    //
    //    for (var j = 0;
    //        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
    //        val = ConfirmationList[j].lblConfirmBillerName;
    //        
    //        
    //        if (kony.string.equalsIgnoreCase(nickname, val)) {
    //            return false;
    //        }
    //    }
    return true;
}

function checkDuplicateNicknameOnEditTopUpIB() {
    var BillerList = myTopupList;
    //var ConfirmationList = frmIBMyTopUpsHome.segBillersConfirm.data;
    var nickname = frmIBMyTopUpsHome.txtEditBillerNickName.text;
    var check = "";
    var val = "";
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {
        val = BillerList[i].lblBillerNickname.text;
        if (kony.string.equals(nickname, val)) {
            return false;
        }
    }
    //
    //    for (var j = 0;
    //        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
    //        val = ConfirmationList[j].lblConfirmBillerName;
    //        
    //        
    //        if (kony.string.equalsIgnoreCase(nickname, val)) {
    //            return false;
    //        }
    //    }
    return true;
}

function deleteOnBillerConfirmationIB(callBackOnConfirm) {
    popupDeleteBiller.show();
    popupDeleteBiller.lblBillerDeleteMsg.text = kony.i18n.getLocalizedString("keyDeleteBillerSure");
    popupDeleteBiller.btnBillerDeleteYes.onClick = callBackOnConfirm;
}
//deleteBillerIB
function deleteBillerIB() {
    callCustomerBillDeleteIB();
}

function callCustomerBillDeleteIB() {
    popupDeleteBiller.destroy();
    var inputparam = {};
    inputparam["CustomerBillerID"] = gblCustomerBillerID;
    var activityTypeID = "061";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    tmpBillerNickName = "";
    tmpBillerNickName = frmIBMyBillersHome.lblViewBillerName.text;
    var activityFlexValues1 = kony.i18n.getLocalizedString("Receipent_delete");
    var activityFlexValues2 = gblBillerName;
    var activityFlexValues3 = tmpBillerNickName;
    var activityFlexValues4 = gblBillerRef1;
    var activityFlexValues5 = gblBillerRef2;
    inputparam["flex1"] = activityFlexValues1;
    inputparam["flex2"] = activityFlexValues2;
    inputparam["flex3"] = activityFlexValues3;
    inputparam["flex4"] = activityFlexValues4;
    inputparam["flex5"] = activityFlexValues5;
    inputparam["typeID"] = activityTypeID;
    //  inputparam["clientDate"] = getCurrentDate();not working on IE 8
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBillDelete", inputparam, callBackCustomerBillDeleteIB)
}

function callBackCustomerBillDeleteIB(status, resulttable) {
    var activityTypeID = "061";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    tmpBillerNickName = "";
    tmpBillerNickName = frmIBMyBillersHome.lblViewBillerName.text;
    var activityFlexValues1 = kony.i18n.getLocalizedString("Receipent_delete");
    var activityFlexValues2 = gblBillerName;
    var activityFlexValues3 = tmpBillerNickName;
    var activityFlexValues4 = gblBillerRef1;
    var activityFlexValues5 = gblBillerRef2;
    var logLinkageId = "";
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] != "0") {
                dismissLoadingScreenPopup();
                alert(resulttable["errMsg"]);
                activityStatus = "02";
            } else {
                frmIBMyBillersHome.imgTMBLogo.setVisibility(true);
                frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
                frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
                frmIBMyBillersHome.lblMyBills.setVisibility(false);
                activityStatus = "01";
                //if(myBillerList.length==1)
                //					myBillerList=[];
                dismissLoadingScreenPopup();
                //frmIBMyBillersHome.postShow();
                postShowForBillers();
            }
        } else {
            dismissLoadingScreenPopup();
            activityStatus = "02";
        }
        //activity log for deletion for biller and topup
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    } //else
}
//-----------------------------------------------------------------------------------
function callCustomerTopUpUpdateIB() {
    var inputparam = {};
    tmpBillerRef1 = "";
    tmpBillerNickName = frmIBMyTopUpsHome.lblViewBillerName.text;
    tmpBillerName = frmIBMyTopUpsHome.lblViewBillerNameCompCode.text;
    tmpBillerNewNickName = frmIBMyTopUpsHome.txtEditBillerNickName.text;
    //inputparam["crmId"] = gblcrmId;
    inputparam["BillerNickName"] = frmIBMyTopUpsHome.txtEditBillerNickName.text;
    inputparam["BillerID"] = billerId; //"BillerID"; //---->?
    //below 2 lines is changed for CR - PCI-DSS masked Credit card no
    inputparam["ReferenceNumber1"] = gblBillerRef1; //frmIBMyTopUpsHome.lblViewBillerRef1Value.text;
    tmpBillerRef1 = gblBillerRef1; //frmIBMyTopUpsHome.lblViewBillerRef1Value.text;
    //inputparam["ReferenceNumber2"] = frmIBMyTopUpsHome.lblViewBillerRef2Value.text;
    inputparam["CustomerBillID"] = gblCustomerBillerID;
    //inputparam["clientDate"] = getCurrentDate();not working on IE 8
    //alert("calling customerBillUpdate from topup");
    var activityTypeID = "062";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Edit";
    var activityFlexValues2 = tmpBillerName;
    var activityFlexValues3 = tmpBillerNickName + "+" + tmpBillerNewNickName;
    var activityFlexValues4 = tmpBillerRef1;
    var activityFlexValues5 = "";
    var logLinkageId = "";
    inputparam["flex1"] = activityFlexValues1;
    inputparam["flex2"] = activityFlexValues2;
    inputparam["flex3"] = activityFlexValues3;
    inputparam["flex4"] = activityFlexValues4;
    inputparam["flex5"] = activityFlexValues5;
    inputparam["typeID"] = activityTypeID;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBillUpdate", inputparam, callBackCustomerTopUpUpdateIB)
}

function callBackCustomerTopUpUpdateIB(status, resulttable) {
    var activityTypeID = "062";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Edit";
    var activityFlexValues2 = tmpBillerName;
    var activityFlexValues3 = tmpBillerNickName + "+" + tmpBillerNewNickName;
    var activityFlexValues4 = tmpBillerRef1;
    var activityFlexValues5 = "";
    var logLinkageId = "";
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] != "0") {
                dismissLoadingScreenPopup();
                alert(resulttable["errMsg"]);
                activityStatus = "02";
            } else {
                frmIBMyTopUpsHome.lblViewBillerName.text = frmIBMyTopUpsHome.txtEditBillerNickName.text;
                //alert(frmIBMyBillersHome.lblViewBillerName.text);	
                frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxOtpBox.setVisibility(false)
                    //frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(true);//for complete screen
                    //frmIBMyTopUpsHome.hbxViewBillerNameDelEdit.setVisibility(true);//for complete screen
                frmIBMyTopUpsHome.imgEditComplete.setVisibility(true); //added tickmark image 
                frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(true);
                activityStatus = "01";
                dismissLoadingScreenPopup();
                //frmIBMyTopUpsHome.postShow();
                postShowForTopups();
            }
        } else {
            //kony.application.dismissLoadingScreen();
            activityStatus = "02";
            dismissLoadingScreenPopup();
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
    } //status
    else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function deleteOnTopupConfirmationIB(callBackOnConfirm) {
    popupDeleteBiller.show();
    popupDeleteBiller.lblBillerDeleteMsg.text = kony.i18n.getLocalizedString("keyDeleteBillerSure");
    popupDeleteBiller.btnBillerDeleteYes.onClick = callBackOnConfirm;
}
//deleteViewTopUpIB
function deleteViewTopUpIB() {
    callCustomerTopupDeleteIB();
}

function callCustomerTopupDeleteIB() {
    popupDeleteBiller.destroy();
    var inputparam = {};
    inputparam["CustomerBillerID"] = gblCustomerBillerID;
    // inputparam["clientDate"] = getCurrentDate();not working on IE 8
    //alert("calling customerBillDelete from topup");
    var activityTypeID = "062";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    tmpBillerNickName = "";
    tmpBillerNickName = frmIBMyTopUpsHome.lblViewBillerName.text;
    var activityFlexValues1 = kony.i18n.getLocalizedString("Receipent_delete");
    var activityFlexValues2 = gblBillerName;
    var activityFlexValues3 = tmpBillerNickName;
    var activityFlexValues4 = gblBillerRef1;
    var activityFlexValues5 = "";
    inputparam["flex1"] = activityFlexValues1;
    inputparam["flex2"] = activityFlexValues2;
    inputparam["flex3"] = activityFlexValues3;
    inputparam["flex4"] = activityFlexValues4;
    inputparam["flex5"] = activityFlexValues5;
    inputparam["typeID"] = activityTypeID;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerBillDelete", inputparam, callBackCustomerTopUpDeleteIB)
}

function callBackCustomerTopUpDeleteIB(status, resulttable) {
    var activityTypeID = "062";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    tmpBillerNickName = "";
    tmpBillerNickName = frmIBMyBillersHome.lblViewBillerName.text;
    var activityFlexValues1 = kony.i18n.getLocalizedString("Receipent_delete");
    var activityFlexValues2 = gblBillerName;
    var activityFlexValues3 = tmpBillerNickName;
    var activityFlexValues4 = gblBillerRef1;
    var activityFlexValues5 = "";
    var logLinkageId = "";
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] != "0") {
                dismissLoadingScreenPopup();
                alert(resulttable["errMsg"]);
                activityStatus = "02";
            } else {
                frmIBMyTopUpsHome.imgTMBLogo.setVisibility(true);
                frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
                frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
                frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
                activityStatus = "01";
                dismissLoadingScreenPopup();
                //if(myTopupList.length==1)
                //					myTopupList=[];
                //frmIBMyTopUpsHome.postShow();
                postShowForTopups();
            }
        } //if
        else {
            dismissLoadingScreenPopup();
            activityStatus = "02";
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
    } //status if
    else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}
/**
 * Method to start the validation service before proceeding
 */
function startIBBillTopUpProfileInqService() {
    var currForm = kony.application.getCurrentForm().id;
    if (securityValFlag == 0) {
        var inputParams = {
            //crmId: gblcrmId,
            rqUUId: "",
            channelName: "IB-INQ"
        };
        showLoadingScreenPopup();
        invokeServiceSecureAsync("crmProfileInq", inputParams, startIBBillTopUpProfileInqServiceAsyncCallback);
    } else {
        switch (parseInt(securityValFlag.toString())) {
            case 1:
                if (currForm == "frmIBMyBillersHome") {
                    frmIBMyBillersHome.hbxBankDetails.setVisibility(false);
                    frmIBMyBillersHome.hbxOTPsnt.setVisibility(false);
                    //frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
                    //frmIBMyBillersHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_Token");
                    //frmIBMyBillersHome.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
                } else if (currForm == "frmIBMyTopUpsHome") {
                    frmIBMyTopUpsHome.hbxBankDetails.setVisibility(false);
                    frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(false);
                    //frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
                    //frmIBMyTopUpsHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_Token");
                    //frmIBMyTopUpsHome.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
                }
                break;
            case 2:
                if (currForm == "frmIBMyBillersHome") {
                    frmIBMyBillersHome.hbxBankDetails.setVisibility(true);
                    frmIBMyBillersHome.hbxOTPsnt.setVisibility(true);
                    frmIBMyBillersHome.txtotp.setFocus(true);
                    //frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                    //frmIBMyBillersHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_OTP");
                    //frmIBMyBillersHome.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
                } else if (currForm == "frmIBMyTopUpsHome") {
                    frmIBMyTopUpsHome.hbxBankDetails.setVisibility(true);
                    frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(true);
                    frmIBMyTopUpsHome.txtotp.setFocus(true);
                    //frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                    //frmIBMyTopUpsHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_OTP");
                    //frmIBMyTopUpsHome.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
                }
                break;
            default:
                break;
        }
    }
}
/**
 * Callback method for startRCTransactionSecurityValidationService()

 */
function startIBBillTopUpProfileInqServiceAsyncCallback(status, callBackResponse) {
    var currForm = kony.application.getCurrentForm().id;
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            gblOTPLockedForBiller = callBackResponse["ibUserStatusIdTr"];
            gblEmailIdBillerNotification = callBackResponse["emailAddr"];
            if (callBackResponse["tokenDeviceFlag"] == "N") {
                tokenFlag = false;
                securityValFlag = 2;
                if (currForm == "frmIBMyBillersHome") {
                    frmIBMyBillersHome.hbxBankDetails.setVisibility(true);
                    frmIBMyBillersHome.hbxOTPsnt.setVisibility(true);
                    frmIBMyBillersHome.txtotp.setFocus(true);
                    //frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                    //frmIBMyBillersHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_OTP");
                    //frmIBMyBillersHome.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
                } else if (currForm == "frmIBMyTopUpsHome") {
                    frmIBMyTopUpsHome.hbxBankDetails.setVisibility(true);
                    frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(true);
                    frmIBMyTopUpsHome.txtotp.setFocus(true);
                    //frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                    //frmIBMyTopUpsHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_OTP");
                    //frmIBMyTopUpsHome.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
                }
            } else {
                tokenFlag = true;
                securityValFlag = 1;
                if (currForm == "frmIBMyBillersHome") {
                    frmIBMyBillersHome.hbxBankDetails.setVisibility(false);
                    frmIBMyBillersHome.hbxOTPsnt.setVisibility(false);
                    //frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
                    //frmIBMyBillersHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_Token");
                    //frmIBMyBillersHome.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
                } else if (currForm == "frmIBMyTopUpsHome") {
                    frmIBMyTopUpsHome.hbxBankDetails.setVisibility(false);
                    frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(false);
                    //frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
                    //frmIBMyTopUpsHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_Token");
                    //frmIBMyTopUpsHome.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
                }
            }
        } else {
            dismissLoadingScreenPopup();
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
        }
    }
}
/**
 * Method to call generate OTP service on switching from token mode to OTP
 */
function startBillTopUpOTPRequestBackground() {
    var currForm = kony.application.getCurrentForm().id;
    var locale = kony.i18n.getCurrentLocale();
    var Channel = "";
    var eventNotificationPolicy = "";
    var SMSSubject = "";
    var curFormID = kony.application.getCurrentForm();
    var billerCount = "";
    if (curFormID.segBillersConfirm.data != null) billerCount = curFormID.segBillersConfirm.data.length;
    var tmpAccDetail = billerCount;
    Channel = "MyBiller";
    //eventNotificationPolicyId is needed temproarily it will be overrided in the preprocessor
    var inputParams = {
        retryCounterRequestOTP: gblRetryCountRequestOTP,
        Channel: Channel,
        locale: locale,
        AccDetailMsg: tmpAccDetail
    };
    showLoadingScreenPopup();
    var currForm = kony.application.getCurrentForm().id;
    if (currForm == "frmIBMyBillersHome") {
        // alert("in frmIBMyBillersHome");
        frmIBMyBillersHome.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBMyBillersHome.lblPlsReEnter.text = " ";
        frmIBMyBillersHome.hbxOTPincurrect.isVisible = false;
        frmIBMyBillersHome.hbxBankDetails.isVisible = true;
        frmIBMyBillersHome.hbxOTPsnt.isVisible = true;
        frmIBMyBillersHome.txtotp.setFocus(true);
    } else if (currForm == "frmIBMyTopUpsHome") {
        // alert("in frmIBMyTopUpsHome");
        frmIBMyTopUpsHome.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBMyTopUpsHome.lblPlsReEnter.text = " ";
        frmIBMyTopUpsHome.hbxOTPincurrect.isVisible = false;
        frmIBMyTopUpsHome.hbxBankDetails.isVisible = true;
        frmIBMyTopUpsHome.hbxOTPsnt.isVisible = true;
        frmIBMyTopUpsHome.txtotp.setFocus(true);
    }
    try {
        invokeServiceSecureAsync("generateOTPWithUser", inputParams, startBillTopUpOTPRequestServiceCallback);
    } catch (e) {
        // todo: handle exception
        invokeCommonIBLogger("Exception in invoking service");
    }
}
//function startBillTopUpOTPRequestBackgroundCallback() {
//	if (status == 400) {
//		if (callBackResponse["opstatus"] == 0) {
//			
//			if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
//				return false;
//			} else if (callBackResponse["errCode"] == "JavaErr00001") {
//				return false;
//			}
//		}
//	}
//}
/**
 * Method to start the service that request OTP

 */
function startBillTopUpOTPRequestService() {
    var currForm = kony.application.getCurrentForm().id;
    var locale = kony.i18n.getCurrentLocale();
    var Channel = "";
    var curFormID = kony.application.getCurrentForm();
    var billerCount = "";
    var eventNotificationPolicy = "";;
    var SMSSubject = ""
    if (curFormID.segBillersConfirm.data != null) billerCount = curFormID.segBillersConfirm.data.length;
    var tmpAccDetail = billerCount + " Biller"
    Channel = "MyBiller";
    // eventNotificationPolicyId will be overrided in preprocessor
    //alert("in startBillTopUpOTPRequestService");
    if (currForm == "frmIBMyBillersHome") {
        // alert("in frmIBMyBillersHome");
        frmIBMyBillersHome.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBMyBillersHome.lblPlsReEnter.text = " ";
        frmIBMyBillersHome.hbxOTPincurrect.isVisible = false;
        frmIBMyBillersHome.hbxBankDetails.isVisible = true;
        frmIBMyBillersHome.hbxOTPsnt.isVisible = true;
        frmIBMyBillersHome.txtotp.setFocus(true);
    } else if (currForm == "frmIBMyTopUpsHome") {
        // alert("in frmIBMyTopUpsHome");
        frmIBMyTopUpsHome.lblOTPinCurr.text = " "; //kony.i18n.getLocalizedString("invalidOTP"); //
        frmIBMyTopUpsHome.lblPlsReEnter.text = " ";
        frmIBMyTopUpsHome.hbxOTPincurrect.isVisible = false;
        frmIBMyTopUpsHome.hbxBankDetails.isVisible = true;
        frmIBMyTopUpsHome.hbxOTPsnt.isVisible = true;
        frmIBMyTopUpsHome.txtotp.setFocus(true);
    }
    var inputParams = {
        retryCounterRequestOTP: gblRetryCountRequestOTP,
        Channel: Channel,
        locale: locale,
        AccDetailMsg: tmpAccDetail
    };
    showLoadingScreenPopup();
    try {
        invokeServiceSecureAsync("generateOTPWithUser", inputParams, startBillTopUpOTPRequestServiceCallback);
    } catch (e) {
        // todo: handle exception
        invokeCommonIBLogger("Exception in invoking service");
    }
}
/**
 * Callback method for startOTPRequestService
 * @param {} callBackResponse
 */
function startBillTopUpOTPRequestServiceCallback(status, callBackResponse) {
    var currForm = kony.application.getCurrentForm().id;
    if (status == 400) {
        if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (callBackResponse["errCode"] == "JavaErr00001") {
                dismissLoadingScreenPopup();
                showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            gblOTPReqLimit = kony.os.toNumber(callBackResponse["otpRequestLimit"]);
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            try {
                kony.timer.cancel("OTPTimer");
            } catch (e) {
                // todo: handle exception
            }
            kony.timer.schedule("OtpTimer", OTPTimercallbackBillTopUp, reqOtpTimer, false);
            if (currForm == "frmIBMyBillersHome") {
                frmIBMyBillersHome.hbxBankDetails.setVisibility(true);
                frmIBMyBillersHome.hbxOTPsnt.setVisibility(true);
                //frmIBMyBillersHome.txtotp.setFocus(true);
                frmIBMyBillersHome.btnOTPReq.skin = btnIBREQotp;
                frmIBMyBillersHome.btnOTPReq.focusSkin = btnIBREQotp;
                frmIBMyBillersHome.btnOTPReq.onClick = "";
                frmIBMyBillersHome.lblBankRef.setVisibility(true);
                frmIBMyBillersHome.lblBankRefValue.setVisibility(true);
                frmIBMyBillersHome.lblOTPMsg.setVisibility(true);
                frmIBMyBillersHome.lblOTPMobileNo.setVisibility(true);
                //frmIBMyBillersHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_OTP");
                //frmIBMyBillersHome.lblBankRefValue.text = callBackResponse["bankRefNo"];
                var refVal = "";
                for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                    if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                        refVal = callBackResponse["Collection1"][d]["ValueString"];
                        break;
                    }
                }
                frmIBMyBillersHome.lblBankRefValue.text = refVal //callBackResponse["Collection1"][8]["ValueString"];
                frmIBMyBillersHome.lblOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsg");
                frmIBMyBillersHome.lblOTPMobileNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                frmIBMyBillersHome.txtotp.setFocus(true);
            } else if (currForm == "frmIBMyTopUpsHome") {
                frmIBMyTopUpsHome.hbxBankDetails.setVisibility(true);
                frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(true);
                //frmIBMyTopUpsHome.txtotp.setFocus(true);
                frmIBMyTopUpsHome.btnOTPReq.skin = btnIBREQotp;
                frmIBMyTopUpsHome.btnOTPReq.focusSkin = btnIBREQotp;
                frmIBMyTopUpsHome.btnOTPReq.onClick = "";
                frmIBMyTopUpsHome.lblBankRef.setVisibility(true);
                frmIBMyTopUpsHome.lblBankRefValue.setVisibility(true);
                frmIBMyTopUpsHome.lblOTPMsg.setVisibility(true);
                frmIBMyTopUpsHome.lblOTPMobileNo.setVisibility(true);
                //frmIBMyTopUpsHome.lblOTPToken.text = kony.i18n.getLocalizedString("Receipent_OTP");
                //frmIBMyTopUpsHome.lblBankRefValue.text = callBackResponse["bankRefNo"];
                var refVal = "";
                for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                    if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                        refVal = callBackResponse["Collection1"][d]["ValueString"];
                        break;
                    }
                }
                frmIBMyTopUpsHome.lblBankRefValue.text = refVal //callBackResponse["Collection1"][8]["ValueString"];
                frmIBMyTopUpsHome.lblOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsg");
                frmIBMyTopUpsHome.lblOTPMobileNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                frmIBMyTopUpsHome.txtotp.setFocus(true);
            }
            dismissLoadingScreenPopup();
            if (currForm == "frmIBMyBillersHome") {
                frmIBMyBillersHome.txtotp.setFocus(true);
            } else {
                frmIBMyTopUpsHome.txtotp.setFocus(true);
            }
            //frmIBMyTopUpsHome.txtotp.setFocus(true);
        } else {
            dismissLoadingScreenPopup();
            alert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + callBackResponse["errMsg"]);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }
}
/**
 * Cancelling the otpTimer
 */
function OTPTimercallbackBillTopUp() {
    var currForm = kony.application.getCurrentForm().id;
    if (gblRetryCountRequestOTP >= gblOTPReqLimit) {
        if (currForm == "frmIBMyBillersHome") {
            frmIBMyBillersHome.btnOTPReq.skin = btnIBREQotp;
            frmIBMyBillersHome.btnOTPReq.focusSkin = btnIBREQotp;
            frmIBMyBillersHome.btnOTPReq.onClick = "";
        } else if (currForm == "frmIBMyTopUpsHome") {
            frmIBMyTopUpsHome.btnOTPReq.skin = btnIBREQotp;
            frmIBMyTopUpsHome.btnOTPReq.focusSkin = btnIBREQotp;
            frmIBMyTopUpsHome.btnOTPReq.onClick = "";
        }
    } else {
        if (currForm == "frmIBMyBillersHome") {
            frmIBMyBillersHome.btnOTPReq.skin = btnIBREQotpFocus;
            frmIBMyBillersHome.btnOTPReq.focusSkin = btnIBREQotpFocus;
            frmIBMyBillersHome.btnOTPReq.onClick = startBillTopUpOTPRequestService;
        } else if (currForm == "frmIBMyTopUpsHome") {
            frmIBMyTopUpsHome.btnOTPReq.skin = btnIBREQotpFocus;
            frmIBMyTopUpsHome.btnOTPReq.focusSkin = btnIBREQotpFocus;
            frmIBMyTopUpsHome.btnOTPReq.onClick = startBillTopUpOTPRequestService;
        }
    }
    try {
        kony.timer.cancel("OtpTimer");
    } catch (e) {}
}

function verifyOTPBillTopUp() {
    validateOTPBillTopUp();
}

function validateOTPBillTopUp() {
    var currForm = kony.application.getCurrentForm().id;
    if (currForm == "frmIBMyBillersHome") {
        if (frmIBMyBillersHome.txtotp.text == null || frmIBMyBillersHome.txtotp.text == "" && gblTokenSwitchFlag == false) {
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
        } else if (frmIBMyBillersHome.tbxToken.text == null || frmIBMyBillersHome.tbxToken.text == "" && gblTokenSwitchFlag == true) {
            alert("" + kony.i18n.getLocalizedString("Receipent_tokenId"));
        } else {
            //var otptext = frmIBMyBillersHome.txtotp.text;
            //validateOTPtextBillTopUp(otptext);
            if (gblTokenSwitchFlag == true) validateOTPtextBillTopUpJavaService(frmIBMyBillersHome.tbxToken.text, "AddBillersIB", false)
            else validateOTPtextBillTopUpJavaService(frmIBMyBillersHome.txtotp.text, "AddBillersIB", false)
        }
    } else if (currForm == "frmIBMyTopUpsHome") {
        if (frmIBMyTopUpsHome.txtotp.text == null || frmIBMyTopUpsHome.txtotp.text == "" && gblTokenSwitchFlag == false) {
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
        } else if (frmIBMyTopUpsHome.tbxToken.text == null || frmIBMyTopUpsHome.tbxToken.text == "" && gblTokenSwitchFlag == true) {
            alert("" + kony.i18n.getLocalizedString("Receipent_tokenId"));
        } else {
            //var otptext = frmIBMyTopUpsHome.txtotp.text;
            //validateOTPtextBillTopUp(otptext);
            if (gblTokenSwitchFlag == true) validateOTPtextBillTopUpJavaService(frmIBMyTopUpsHome.tbxToken.text, "AddTopupsIB", false)
            else validateOTPtextBillTopUpJavaService(frmIBMyTopUpsHome.txtotp.text, "AddTopupsIB", false)
        }
    }
}
/**
 * Callback method for startTokenValidationService
 * @param {}
 */
/*function startTokenValidationServiceBillTopUpAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			var currForm = kony.application.getCurrentForm()
				.id;
			if (currForm == "frmIBMyBillersHome")
				addBillers();
			else if (currForm == "frmIBMyTopUpsHome")
				addTopups();
		} else {
			dismissLoadingScreenPopup();
			alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
		}
	}
}*/
/**
 * Callback method for startOTPValidationService
 * @param {}
 * @returns {}
 */
/*function startOTPValidationServiceBillTopUpAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		frmIBMyTopUpsHome.txtotp.text = "";
		if (callBackResponse["opstatus"] == 0) {
			gblVerifyOTPCounter = "0";
			gblRetryCountRequestOTP=0;
			dismissLoadingScreenPopup();
			//call functions that need to be executed after OTP verification
			var currForm = kony.application.getCurrentForm()
				.id;
			if (currForm == "frmIBMyBillersHome")
				addBillers();
			else if (currForm == "frmIBMyTopUpsHome")
				addTopups();
		} else if (callBackResponse["opstatus"] == 8005) {
			if (callBackResponse["errCode"] == "VrfyOTPErr00001") {
				gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
				dismissLoadingScreenPopup();
				alert("" + kony.i18n.getLocalizedString("invalidOTP"));
				return false;
			} else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
				dismissLoadingScreenPopup();
				//alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
				//startRcCrmUpdateProIB("04");
				handleOTPLockedIB(callBackResponse);
				return false;
			}else if (callBackResponse["errCode"] == "VrfyOTPErr00005") {
				dismissLoadingScreenPopup();
				alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
				return false;
			} else if (callBackResponse["errCode"] == "VrfyOTPErr00006") {
				gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
				alert("" + callBackResponse["errMsg"]);
				return false;
			}
		} else {
			dismissLoadingScreenPopup();
			alert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + callBackResponse["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
		}
	}
}*/
///////////////////////Functions from ModIBBiller 
/*
************************************************************************************************************************
		Module	: controlIBMenuBiller
		Author  : Kony
		Purpose : Called on Preshow of the Form to handle Menu Navigations 
************************************************************************************************************************
*/
function controlIBMenuBiller() {
    var curr_form = kony.application.getCurrentForm()
    curr_form.segMenuOptions.setData([{
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Profile"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Accounts"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "Open New Account"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Recipients"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBsegMenuFocus",
            "text": "My Bills"
        }
    }, {
        "lblSegData": {
            "skin": "lblIBSegMenu",
            "text": "My Top-Ups"
        }
    }]);
    gblMenuSelection = 1;
}

function mapSeg() {
    frmIBMyBillersHome.segAddBillerComplete.widgetDataMap = {
        "lblBillerNameComplete": "lblConfirmBillerName",
        "imgAddBillerComplete": "imgConfirmBillerLogo",
        "lblRef2Complete": "lblConfirmBillerRef2",
        "lblRef2CompleteValue": "lblConfirmBillerRef2Value",
        "lblBillerNameCompCodeComplete": "lblConfirmBillerNameCompCode",
        "lblRef1Complete": "lblConfirmBillerRef1",
        "lblRef1CompleteValue": "lblConfirmBillerRef1ValueMasked"
            //"btnPayBillComplete": "btnPayBillComplete"
    };
    var temp = frmIBMyBillersHome.segBillersConfirm.data;
    frmIBMyBillersHome.segAddBillerComplete.setData(temp);
    //frmIBMyBillersHome.show();  DEF5408
}

function mapSegTopUpComplete() {
    frmIBMyTopUpsHome.segAddBillerComplete.widgetDataMap = {
        "lblBillerNameComplete": "lblConfirmBillerName",
        "imgAddBillerComplete": "imgConfirmBillerLogo",
        "lblBillerNameCompCodeComplete": "lblConfirmBillerNameCompCode",
        "lblRef1Complete": "lblConfirmBillerRef1",
        "lblRef1CompleteValue": "lblConfirmBillerRef1Value"
            //"btnPayBillComplete": "btnPayBillComplete"
    };
    var temp = frmIBMyTopUpsHome.segBillersConfirm.data;
    frmIBMyTopUpsHome.segAddBillerComplete.setData(temp);
    //frmIBMyTopUpsHome.show();
}

function checkIfBarcodeOnlyIBBiller() {
    var isBarCode = "";
    if (frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems != null) {
        indexOfSelectedIndex = frmIBBillPaymentLP.segBPSgstdBillerList.selectedItems[0];
        isBarCode = indexOfSelectedIndex.BarcodeOnly;
    } else {
        isBarCode = frmIBMyBillersHome.segSuggestedBillersList.selectedItems[0].BarcodeOnly;
    }
    if (isBarCode == "1") {
        return false;
    } else {
        return true;
    }
}
/*
************************************************************************************************************************
		Module	: loadAddBillerValues
		Author  : Kony
		Purpose : Mapping of Suggested biller to Add Biller Text Box , on click of + button on suggested Biller Segment 
************************************************************************************************************************
*/
var gblCurRef1LblEN = "";
var gblCurRef2LblEN = "";
var gblCurRef1LblTH = "";
var gblCurRef2LblTH = "";
var gblCurBillerNameEN = "";
var gblCurBillerNameTH = "";

function loadAddBillerValues() {
    gblBillerCompCode = "";
    gblBillerId = "";
    gblBillerMethod = "";
    gblRef2Flag = "";
    gblIsRef2RequiredIB = "";
    gblRef1LenIB = "";
    gblRef2LenIB = "";
    gblBillerId = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["BillerID"]["text"];
    gblToAccountKey = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["ToAccountKey"];
    gblreccuringDisableAdd = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["IsRequiredRefNumber2Add"]["text"];
    gblreccuringDisablePay = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["IsRequiredRefNumber2Pay"]["text"];
    gblbillerGroupType = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["BillerGroupType"];
    gblBillerCategoryID = frmIBMyBillersHome.segSuggestedBillersList.selectedItems[0].BillerCategoryID.text;
    gblPayFull = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["IsFullPayment"];
    gblBillerMethod = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["BillerMethod"]["text"];
    gblEffDt = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["EffDt"]["text"];
    gblRef1LenIB = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1Len"]["text"];
    frmIBMyBillersHome.txtAddBillerRef1.maxTextLength = gblRef1LenIB;
    gblBillerCompCode = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["BillerCompCode"]["text"];
    frmIBMyBillersHome.lblAddBillerName.text = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["lblSugBillerName"]["text"];
    frmIBMyBillersHome.lblAddBillerRef1.text = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1Label"]["text"] + ":";
    gblCurRef1LblTH = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1LblThai"]["text"] + ":";
    gblCurRef1LblEN = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1LblEng"]["text"] + ":";
    gblCurBillerNameEN = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["BillerNameEng"]["text"];
    gblCurBillerNameTH = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["BillerNameThai"]["text"];
    frmIBMyBillersHome.imgAddBillerLogo.src = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["imgSugBillerLogo"]["src"];
    gblIsRef2RequiredIB = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["IsRequiredRefNumber2Add"]["text"];
    gblRef2LenIB = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref2Len"]["text"];
    //ENh 113
    if (gblBillerCompCode == "2533") {
        gblSegBillerData = "";
        gblSegBillerData = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0];
        gBillerStartTime = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["billerStartTime"];
        gBillerEndTime = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["billerEndTime"];
        gblBillerTransType = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["billerTransType"];
        gblBillerServiceType = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["billerServiceType"];
        gblMeaFeeAmount = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["billerFeeAmount"];
    } else {
        gblSegBillerData = "";
        frmIBBillPaymentLP.btnBPPayOn.setEnabled(true);
    }
    //ENh 113
    gblBillerBancassurance = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["billerBancassurance"];
    gblAllowRef1AlphaNum = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["allowRef1AlphaNum"];
    if (kony.string.equalsIgnoreCase(gblIsRef2RequiredIB, "N")) {
        frmIBMyBillersHome.txtAddBillerRef2.text = "";
        //gblCurRef2LblTH = ""
        //gblCurRef2LblEN = ""
        frmIBMyBillersHome.hbxBillerRef2.setVisibility(false);
        frmIBMyBillersHome.line4belowRef2.setVisibility(false); //Added for DF2248
        //gblRef2LenIB = 0;
    } else if (kony.string.equalsIgnoreCase(gblIsRef2RequiredIB, "Y")) {
        gblRef2Flag = true;
        frmIBMyBillersHome.txtAddBillerRef2.setEnabled(true);
        frmIBMyBillersHome.line4belowRef2.setVisibility(true); //Added for DF2248
        frmIBMyBillersHome.lblAddBillerRef2.text = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref2Label"]["text"] + ":";
        //gblCurRef2LblTH = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref2LblThai"]["text"] + ":";
        //gblCurRef2LblEN = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref2LblEng"]["text"] + ":";
        //gblRef2LenIB = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref2Len"]["text"];
        frmIBMyBillersHome.txtAddBillerRef2.maxTextLength = gblRef2LenIB;
        frmIBMyBillersHome.hbxBillerRef2.setVisibility(true);
    }
    gblCurRef2LblTH = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref2LblThai"]["text"] + ":";
    gblCurRef2LblEN = frmIBMyBillersHome["segSuggestedBillersList"]["selectedItems"][0]["Ref2LblEng"]["text"] + ":";
    gblRef1LblEN = gblCurRef1LblEN;
    gblRef2LblEN = gblCurRef2LblEN;
    gblRef1LblTH = gblCurRef1LblTH;
    gblRef2LblTH = gblCurRef2LblTH;
    gblBillerCompCodeEN = gblCurBillerNameEN;
    gblBillerCompCodeTH = gblCurBillerNameTH;
}

function tokenExchangeBeforeSaveParamsInSess() {
    var inputParam = [];
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, tokenExchangeBeforeSaveParamsInSessCallBack);
}

function tokenExchangeBeforeSaveParamsInSessCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreenPopup();
            onNextClickedAddBill();
        }
    }
}
/*
*************************************************************************************
		Module	: onNextClickedAddBill
		Author  : Kony
		Purpose : Performing Validations for Billers just before showing OTP HBx. 
****************************************************************************************
*/
gblFlagConfirmDataAdded = 0;

function onNextClickedAddBill() {
    //To fix Defect DEF14665 
    frmIBMyBillersHome.btnConfirmBillerAdd.setVisibility(true);
    // var billerCount = "";
    var duplicateNickNameFlag = "";
    var duplicateRef1CompCodeFlag = "";
    var billerMethodFlag = "";
    var ref2Val = "";
    var ref2 = "";
    var ref1value = frmIBMyBillersHome.txtAddBillerRef1.text;
    var ref2result = false;
    if (frmIBMyBillersHome.txtAddBillerRef1.text.length == 0) {
        alert(kony.i18n.getLocalizedString("keyPleaseEnter") + frmIBMyBillersHome.lblAddBillerRef1.text);
        return;
    }
    if (gblIsRef2RequiredIB != "Y") {
        ref2Val = "";
        ref2 = "";
        ref2result = true;
    } else {
        ref2Val = frmIBMyBillersHome.txtAddBillerRef2.text;
        ref2 = frmIBMyBillersHome.lblAddBillerRef2.text;
        if (frmIBMyBillersHome.txtAddBillerRef2.text.length == 0) {
            alert(kony.i18n.getLocalizedString("keyPleaseEnter") + frmIBMyBillersHome.lblAddBillerRef2.text);
            return;
        }
        ref2result = validateRef2Value();
    }
    duplicateNickNameFlag = checkDuplicateNicknameOnAddBillerIB();
    duplicateRef1CompCodeFlag = checkDuplicateRef1CompCode();
    //
    if (duplicateNickNameFlag && duplicateRef1CompCodeFlag && ref2result) {
        //start check biller validation in middleware
        var billerNickName = frmIBMyBillersHome.txtAddBillerNickName.text;
        var billerCompCode = gblBillerCompCode;
        var billerName = frmIBMyBillersHome.lblAddBillerName.text;
        //alert("calling validation service"+ channelFlagBillerIB);
        callBillerValidationService(billerCompCode, billerNickName, ref1value, ref2Val, billerName, channelFlagBillerIB);
        //verifyBillerMethod(1);
    } else {
        if (!duplicateNickNameFlag) {
            alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
            return;
        } else if (!duplicateRef1CompCodeFlag) {
            alert(kony.i18n.getLocalizedString("keyduplicatebiller"));
            return;
        } else if (!ref2result) {
            //alert(kony.i18n.getLocalizedString("keyWrngRef2Val")+frmIBMyBillersHome.lblAddBillerRef2.text);
            alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
            return;
        }
    }
}
/*
*************************************************************************************
		Module	: loadBillerConfirmSegmentData
		Author  : Kony
		Purpose : method for mapping of the biller, while adding, to confirmation page segment. 
****************************************************************************************
*/
function loadBillerConfirmSegmentData() {
    var billerRow = {};
    var ref2Val = "";
    var ref2 = "";
    //if (billerMethodFlag == true) {
    if (gblIsRef2RequiredIB != "Y") {
        ref2Val = "";
        ref2 = "";
    } else {
        ref2Val = frmIBMyBillersHome.txtAddBillerRef2.text;
        ref2 = frmIBMyBillersHome.lblAddBillerRef2.text;
    }
    //below line is added for CR - PCI-DSS masked Credit card no
    var maskedRef1 = maskCreditCard(frmIBMyBillersHome.txtAddBillerRef1.text);
    billerRow = [{
        lblConfirmBillerName: frmIBMyBillersHome.txtAddBillerNickName.text,
        lblConfirmBillerNameCompCode: frmIBMyBillersHome.lblAddBillerName.text,
        btnConfirmBillerDelete: {
            text: "del",
            skn: "btnIBdelicon"
        },
        imgConfirmBillerLogo: {
            src: frmIBMyBillersHome.imgAddBillerLogo.src
        },
        /*btnPayBillComplete: {
            text: kony.i18n.getLocalizedString("PayBill")
        },*/
        lblConfirmBillerRef1: frmIBMyBillersHome.lblAddBillerRef1.text,
        lblConfirmBillerRef1Value: frmIBMyBillersHome.txtAddBillerRef1.text,
        lblConfirmBillerRef1ValueMasked: maskedRef1,
        lblConfirmBillerRef2: ref2,
        lblConfirmBillerRef2Value: ref2Val,
        BillerCompCode: gblBillerCompCode,
        BillerId: gblBillerId,
        BillerNameEN: gblBillerCompCodeEN,
        BillerNameTH: gblBillerCompCodeTH,
        Ref1LblEN: gblCurRef1LblEN,
        Ref1LblTH: gblCurRef1LblTH,
        Ref2LblEN: gblCurRef2LblEN,
        Ref2LblTH: gblCurRef2LblTH
    }];
    frmIBMyBillersHome.segBillersConfirm.addAll(billerRow);
    gblFlagConfirmDataAdded = 1;
    var billerCount = "";
    billerCount = frmIBMyBillersHome.segBillersConfirm.data;
    var maxBulkAddCount = kony.os.toNumber(GLOBAL_MAX_BILL_ADD);
    if (billerCount.length >= maxBulkAddCount) {
        frmIBMyBillersHome.btnConfirmBillerAdd.setEnabled(false);
        frmIBMyBillersHome.btnAddBiller.setEnabled(false);
        frmIBMyBillersHome.segSuggestedBillersList.setEnabled(false);
    }
    frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(true);
    frmIBMyBillersHome.hbxOtpBox.setVisibility(false);
    frmIBMyBillersHome.btnConfirmBillerNext.setVisibility(true);
    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyBillersHome.txtAddBillerNickName.text = "";
    frmIBMyBillersHome.txtAddBillerRef1.text = "";
    frmIBMyBillersHome.txtAddBillerRef2.text = "";
}
/*
*************************************************************************************
		Module	: verifyBillerMethod
		Author  : Kony
		Purpose : Validation Biller Method for various Miller Methods mentioned below
****************************************************************************************
*/
/*

MasterBillerInfo.BillerMethod=0  Offline Biller
MasterBillerInfo.BillerMethod=1  Online Biller
MasterBillerInfo.BillerMethod=2  TMB Credit Card/Ready Cash
MasterBillerInfo.BillerMethod=3  TMB Loan
MasterBillerInfo.BillerMethod=4 Old Ready Cash (CBS)
*/
// pChannel = 1 for IB Biller, 2 for IB Topup ,3 for MB Biller , 4 for MB Topup
function verifyBillerMethod(pChannel) {
    //alert("pChannel value is "+ pChannel);
    gblBillerTopupType = pChannel;
    //gblCreditCardValidationFlag = "";
    //	gblLoanValidationFlag = "";
    //	gblOnlineBillerValidationFlag = "";
    //alert("gblBillerMethod value is "+ gblBillerMethod);    
    if (gblBillerMethod == "1") {
        validateOnlinePayInquiry();
    } else if (gblBillerMethod == "2") {
        validateCreditCard();
    } else if (gblBillerMethod == "3" && gblBillerTopupType != 2 && gblBillerTopupType != 4) {
        validateLoanAcc();
    } else if ((gblBillerMethod == "3" && (gblBillerTopupType == 2 || gblBillerTopupType == 4))) {
        if (gblBillerTopupType == 2) // for IB topup
        {
            loadTopupConfirmSegmentData();
        } else if (gblBillerTopupType == 4) // for MB  Topup
        {
            loadTopupConfirmSegmentDataMB();
        }
    } else {
        if (gblBillerTopupType == 1) //for IB Biller
        {
            loadBillerConfirmSegmentData();
        } else if (gblBillerTopupType == 2) // for IB topup
        {
            loadTopupConfirmSegmentData();
        } else if (gblBillerTopupType == 3) //for MB Biller 
        {
            loadBillerConfirmSegmentDataMB();
        } else if (gblBillerTopupType == 4) // for MB  Topup
        {
            loadTopupConfirmSegmentDataMB();
        }
    }
}
/*
*************************************************************************************
		Module	: validateOnlinePayInquiry
		Author  : Kony
		Purpose : Validation Biller Method for Online Payment Inquiry
****************************************************************************************
*/
function validateOnlinePayInquiry() {
    var amount = "0.00";
    if (gblBillerTopupType == "2") {
        var length = gblstepMinAmount.length;
        for (i = 0; i < length; i++) {
            if (gblstepMinAmount[i].compcode == gblBillerCompCode) {
                amount = gblstepMinAmount[i].minAmount;
                break;
            }
        }
    } else if (gblBillerTopupType == "4") {
        var length = gblstepMinAmount.length;
        for (i = 0; i < length; i++) {
            if (gblstepMinAmount[i].compcode == gblCompCode) {
                amount = gblstepMinAmount[i].minAmount;
                break;
            }
        }
    }
    var inputparam = {};
    inputparam["TrnId"] = "" // added in tracker
    inputparam["Amt"] = amount
    inputparam["BankId"] = "011"
    inputparam["BranchId"] = "0001"
    inputparam["BankRefId"] = "" // added in tracker
    inputParam["commandType"] = "Inquiry";
    if (gblBillerTopupType == 1) {
        inputparam["compCode"] = gblBillerCompCode;
        inputparam["Ref1"] = frmIBMyBillersHome.txtAddBillerRef1.text;
        inputparam["MobileNumber"] = frmIBMyBillersHome.txtAddBillerRef1.text;
        inputParam["isChannel"] = "IB";
        if (gblRef2Flag == true && gblBillerTopupType == 1) inputparam["Ref2"] = frmIBMyBillersHome.txtAddBillerRef2.text;
    } else if (gblBillerTopupType == 2) {
        inputparam["compCode"] = gblBillerCompCode;
        inputparam["Ref1"] = frmIBMyTopUpsHome.txtAddBillerRef1.text;
        inputParam["isChannel"] = "IB";
        inputparam["MobileNumber"] = frmIBMyTopUpsHome.txtAddBillerRef1.text;
    } else if (gblBillerTopupType == 3 || gblBillerTopupType == 4) {
        inputparam["compCode"] = gblCompCode;
        inputparam["isChannel"] = "MB";
        inputparam["Ref1"] = frmAddTopUpToMB.txtRef1.text;
        inputparam["MobileNumber"] = frmAddTopUpToMB.txtRef1.text;
        if (gblRef2FlagMB == true && gblBillerTopupType == 3) inputparam["Ref2"] = frmAddTopUpToMB.txtRef2.text;
    }
    //  alert("calling onlinePaymentInq ");
    if (glbChannelFlag == 1 || glbChannelFlag == 2) {
        showLoadingScreenPopup();
    } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
        showLoadingScreen();
    }
    invokeServiceSecureAsync("onlinePaymentInq", inputparam, validateOnlinePayInquiryCallBack);
}
/*
*************************************************************************************
		Module	: validateOnlinePayInquiryCallBack
		Author  : Kony
		Purpose : Validation Biller Method for Online Payment Inquiry
****************************************************************************************
*/
function validateOnlinePayInquiryCallBack(status, resulttable) {
    //alert("called onlinePaymentInq "+status+"  opstatus "+resulttable["opstatus"]+" status code "+resulttable["StatusCode"]);
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var addMEABiller = false;
            if (gblCompCode == "2533" || gblBillerCompCode == "2533" || gblCompCode == "2151" || gblBillerCompCode == "2151") {
                var statusCode = ["PGI001", "PGI002", "PGI003", "PGI004", "PGI005", "PGI006", "PGI007", "PGI008", "PGI009", "PGI010", "PG-20007"];
                for (i = 0; i < statusCode.length; i++) {
                    if (resulttable["XPServerStatCode"] == statusCode[i]) resulttable["StatusCode"] = "0";
                }
            }
            if (resulttable["StatusCode"] != 0) {
                if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                    dismissLoadingScreenPopup();
                } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                    dismissLoadingScreen();
                }
                if (resulttable["StatusCode"] == 100) {
                    if (resulttable["errMsg"] != undefined) {
                        alert(resulttable["errMsg"]);
                    } else {
                        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                    }
                } else {
                    alert(resulttable["errMsg"]);
                }
            } else {
                if (gblBillerTopupType == 1) //for IB Biller
                {
                    loadBillerConfirmSegmentData();
                } else if (gblBillerTopupType == 2) // for IB topup
                {
                    loadTopupConfirmSegmentData();
                } else if (gblBillerTopupType == 3) //for MB Biller 
                {
                    loadBillerConfirmSegmentDataMB();
                } else if (gblBillerTopupType == 4) // for MB  Topup
                {
                    loadTopupConfirmSegmentDataMB();
                }
                if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                    dismissLoadingScreenPopup();
                } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                    dismissLoadingScreen();
                }
            }
        } else {
            if (status == 300 || resulttable["opstatus"] == 1 || resulttable["opstatus"] == 9001 || resulttable["StatusCode"] == undefined) {
                if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                    dismissLoadingScreenPopup();
                } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                    dismissLoadingScreen();
                }
                alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
            // gblOnlineBillerValidationFlag = true;
        }
    }
}
/*
*************************************************************************************
		Module	: validateLoanAcc
		Author  : Kony
		Purpose : Validation Biller Method for Loan accounts
****************************************************************************************
*/
function validateLoanAcc() {
    var inputparam = {};
    if (gblBillerTopupType == 1) {
        var suffix = frmIBMyBillersHome.txtAddBillerRef2.text;
        inputparam["acctId"] = "0" + frmIBMyBillersHome.txtAddBillerRef1.text + suffix;
    } else if (gblBillerTopupType == 3) {
        var suffix = frmAddTopUpToMB.txtRef2.text;
        inputparam["acctId"] = "0" + frmAddTopUpToMB.txtRef1.text + suffix;
    }
    //inputparam["acctType"] = "LOC"
    if (glbChannelFlag == 1 || glbChannelFlag == 2) {
        showLoadingScreenPopup();
    } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
        showLoadingScreen();
    }
    invokeServiceSecureAsync("doLoanAcctInqNonSec", inputparam, validateLoanAccCallBack);
}
/*
*************************************************************************************
		Module	: validateLoanAccCallBack
		Author  : Kony
		Purpose : Validation Biller Method for Loan accounts
****************************************************************************************
*/
function validateLoanAccCallBack(status, resulttable) {
    // alert("called doLoanAcctInq ");
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] != 0) {
                alert(resulttable["errMsg"]);
            } else {
                if (gblBillerTopupType == 1) //for IB Biller
                {
                    loadBillerConfirmSegmentData();
                } else if (gblBillerTopupType == 3) //for MB Biller 
                {
                    loadBillerConfirmSegmentDataMB();
                }
            }
            //  gblLoanValidationFlag = true;
            if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                dismissLoadingScreenPopup();
            } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                dismissLoadingScreen();
            }
        }
    } else {
        if (status == 300) {
            if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                dismissLoadingScreenPopup();
            } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                dismissLoadingScreen();
            }
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}
/*
*************************************************************************************
		Module	: validateCreditCard
		Author  : Kony
		Purpose : Validation Biller Method for credit card
****************************************************************************************
*/
function validateCreditCard() {
    var inputparam = {};
    var toDayDate = getTodaysDate();
    if (gblBillerTopupType == 1) //for IB Biller
    {
        inputparam["cardId"] = frmIBMyBillersHome.txtAddBillerRef1.text;
    } else if (gblBillerTopupType == 3 || gblBillerTopupType == 4) // for MB Topup and Biller
    {
        inputparam["cardId"] = frmAddTopUpToMB.txtRef1.text;
    } else if (gblBillerTopupType == 2) // for IB topup
    {
        inputparam["cardId"] = frmIBMyTopUpsHome.txtAddBillerRef1.text;
    }
    //inputparam["waiverCode"] = WAIVERCODE;
    inputparam["tranCode"] = TRANSCODEUN;
    //inputparam["postedDt"] = toDayDate;
    //inputparam["rqUUId"] = "";
    // alert("calling creditcardDetailsInq ");
    if (glbChannelFlag == 1 || glbChannelFlag == 2) {
        showLoadingScreenPopup();
    } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
        showLoadingScreen();
    }
    invokeServiceSecureAsync("creditcardDetailsInqNonSec", inputparam, validateCreditCardCallBack);
}
/*
*************************************************************************************
		Module	: validateCreditCardCallBack
		Author  : Kony
		Purpose : Validation Biller Method for credit card
****************************************************************************************
*/
function validateCreditCardCallBack(status, resulttable) {
    //  alert("called creditcardDetailsInq ");
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                dismissLoadingScreenPopup();
            } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                dismissLoadingScreen();
            }
            if (resulttable["StatusCode"] != 0) {
                alert(resulttable["errMsg"]);
            } else {
                if (gblBillerTopupType == 1) //for IB Biller
                {
                    loadBillerConfirmSegmentData();
                } else if (gblBillerTopupType == 2) // for IB topup
                {
                    loadTopupConfirmSegmentData();
                } else if (gblBillerTopupType == 3) //for MB Biller 
                {
                    loadBillerConfirmSegmentDataMB();
                } else if (gblBillerTopupType == 4) // for MB  Topup
                {
                    loadTopupConfirmSegmentDataMB();
                }
                // gblCreditCardValidationFlag = true;
            }
        }
    } else {
        if (status == 300) {
            if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                dismissLoadingScreenPopup();
            } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                dismissLoadingScreen();
            }
            alert(kony.i18n.getLocalizedString("ECGenericError"));
            //  alert("Biller Method failed : credit card");
        }
    }
}
/*
*************************************************************************************
		Module	: addBillers
		Author  : Kony
		Purpose : Fucntion for adding billers , calling customerBillAdd service
****************************************************************************************
*/
var tmpBillerName = [];
var tmpBillerNickName = [];
var tmpBillerRef1 = [];
var tmpBillerRef2 = [];
var BatchLength = 0;
var billerCounterValue = 0;

function addBillers() {
    tmpBillerName = [];
    tmpBillerNickName = [];
    tmpBillerRef1 = [];
    tmpBillerRef2 = [];
    tmpBillerCompCode = [];
    tmpBillerRef1Lbl = [];
    tmpBillerRef2Lbl = [];
    BatchLength = 0;
    billerCounterValue = 0;
    var tmpAddBillData = frmIBMyBillersHome.segBillersConfirm.data;
    if (tmpAddBillData != null) BatchLength = tmpAddBillData.length;
    var inputParams = {};
    for (i = 0; i < tmpAddBillData.length; i++) {
        inputParams = {};
        //inputParams["crmId"] = gblcrmId;
        tmpBillerNickName[i] = tmpAddBillData[i]["lblConfirmBillerName"];
        tmpBillerName[i] = tmpAddBillData[i]["lblConfirmBillerNameCompCode"];
        tmpBillerRef1[i] = tmpAddBillData[i]["lblConfirmBillerRef1Value"];
        tmpBillerRef1Lbl[i] = tmpAddBillData[i]["lblConfirmBillerRef1"];
        inputParams["BillerNickName"] = tmpAddBillData[i]["lblConfirmBillerName"];
        inputParams["BillerId"] = tmpAddBillData[i]["BillerId"];
        inputParams["BillerCompcode"] = tmpAddBillData[i]["BillerCompCode"];
        tmpBillerCompCode[i] = tmpAddBillData[i]["BillerCompCode"];
        inputParams["ReferenceNumber1"] = tmpAddBillData[i]["lblConfirmBillerRef1Value"];
        if (kony.string.equalsIgnoreCase(gblIsRef2RequiredIB, "Y")) {
            tmpBillerRef2[i] = tmpAddBillData[i]["lblConfirmBillerRef2Value"];
            inputParams["ReferenceNumber2"] = tmpAddBillData[i]["lblConfirmBillerRef2Value"];
            tmpBillerRef2Lbl[i] = tmpAddBillData[i]["lblConfirmBillerRef2"];
        } else {
            tmpBillerRef2[i] = "";
            tmpBillerRef2Lbl[i] = "";
        }
        inputParams["httpheaders"] = {};
        inputParams["httpconfigs"] = {};
        showLoadingScreenPopup();
        invokeServiceSecureAsync("customerBillAdd", inputParams, addBillersCallback);
    }
}
/*
*************************************************************************************
		Module	: addBillersCallback
		Author  : Kony
		Purpose : Function for adding billers , calling customerBillAdd service
****************************************************************************************
*/
function addBillersCallback(status, callBackResponse) {
    //  alert("called customerBillAdd from my billers ");
    var activityTypeID = "061";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Add";
    var activityFlexValues2 = [];
    var activityFlexValues3 = [];
    var activityFlexValues4 = [];
    var activityFlexValues5 = [];
    var logLinkageId = "";
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            frmIBMyBillersHome.segBillersConfirm.removeAll();
            frmIBMyBillersHome.btnAddBiller.setEnabled(true); //Enabling add more since the segment is flushed
            frmIBMyBillersHome.btnConfirmBillerAdd.setEnabled(true); //Enabling add more since the segment is flushed	
            frmIBMyBillersHome.segSuggestedBillersList.setEnabled(true); //Enabling add more since the segment is flushed
            frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
            frmIBMyBillersHome.hbxOtpBox.setVisibility(false)
            frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
            frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
            frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
            frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
            frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(false);
            frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(true);
            frmIBMyBillersHome.imgArrowAddBiller.setVisibility(true);
            frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
            //to disable OTP related widgets
            frmIBMyBillersHome.lblBankRef.setVisibility(false);
            frmIBMyBillersHome.lblBankRefValue.setVisibility(false);
            frmIBMyBillersHome.lblOTPMsg.setVisibility(false);
            frmIBMyBillersHome.lblOTPMobileNo.setVisibility(false);
            frmIBMyBillersHome.txtotp.text = "";
            frmIBMyBillersHome.btnOTPReq.onClick = startBillTopUpOTPRequestService;
            frmIBMyBillersHome.btnOTPReq.skin = btnIBREQotpFocus;
            frmIBMyBillersHome.btnOTPReq.focusSkin = btnIBREQotpFocus;
            activityStatus = "01";
            dismissLoadingScreenPopup();
            if (billerCounterValue < BatchLength) {
                activityFlexValues2[billerCounterValue] = tmpBillerName[billerCounterValue]
                activityFlexValues3[billerCounterValue] = tmpBillerNickName[billerCounterValue]
                activityFlexValues4[billerCounterValue] = tmpBillerRef1[billerCounterValue]
                activityFlexValues5[billerCounterValue] = tmpBillerRef2[billerCounterValue]
                activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2[billerCounterValue], activityFlexValues3[billerCounterValue], activityFlexValues4[billerCounterValue], activityFlexValues5[billerCounterValue], logLinkageId);
                var tmpBillerNameNoCompCode = tmpBillerName[billerCounterValue].substring(0, tmpBillerName[billerCounterValue].lastIndexOf("("));
                sendBillTopNotification(tmpBillerNickName[billerCounterValue], tmpBillerNameNoCompCode, tmpBillerCompCode[billerCounterValue], tmpBillerRef1Lbl[billerCounterValue], tmpBillerRef1[billerCounterValue], tmpBillerRef2Lbl[billerCounterValue], tmpBillerRef2[billerCounterValue]);
                billerCounterValue = billerCounterValue + 1;
            } else {
                billerCounterValue = 0;
            }
            //frmIBMyBillersHome.postShow();
            postShowForBillers();
        } else {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
            activityStatus = "02";
            if (billerCounterValue < BatchLength) {
                activityFlexValues2[billerCounterValue] = tmpBillerName[billerCounterValue]
                activityFlexValues3[billerCounterValue] = tmpBillerNickName[billerCounterValue]
                activityFlexValues4[billerCounterValue] = tmpBillerRef1[billerCounterValue]
                activityFlexValues5[billerCounterValue] = tmpBillerRef2[billerCounterValue]
                activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2[billerCounterValue], activityFlexValues3[billerCounterValue], activityFlexValues4[billerCounterValue], activityFlexValues5[billerCounterValue], logLinkageId);
                billerCounterValue = billerCounterValue + 1;
            } else {
                billerCounterValue = 0;
            }
        }
        //activity log for add biller
        //activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
        //activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
            billerCounterValue = 1
        }
    }
}
/*
*************************************************************************************
		Module	: checkDuplicateNicknameOnAddBillerIB
		Author  : Kony
		Purpose : Validation function for checking Nickname duplicate
****************************************************************************************
*/
function checkDuplicateNicknameOnAddBillerIB() {
    var BillerList = myBillerList;
    var ConfirmationList = frmIBMyBillersHome.segBillersConfirm.data;
    var nickname = frmIBMyBillersHome.txtAddBillerNickName.text;
    var check = "";
    var val = "";
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {
        val = BillerList[i].lblBillerNickname.text;
        if (kony.string.equals(nickname, val)) {
            //   alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
            return false;
        }
    }
    for (var j = 0;
        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
        val = ConfirmationList[j].lblConfirmBillerName;
        if (kony.string.equals(nickname, val)) {
            //alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
            return false;
        }
    }
    return true;
}
/*
*************************************************************************************
		Module	: checkMaxBillerCountOnConfirm
		Author  : Kony
		Purpose : Checking Max no. of billers to be added as customer billers
****************************************************************************************
*/
function checkMaxBillerCountOnConfirm() {
    var maxCount = kony.os.toNumber(GLOBAL_MAX_BILL_COUNT);
    var BillerList = myBillerList;
    var ConfirmationList = "";
    // var BillerList = frmIBMyBillersHome.segBillersList.data;
    //if (gblFlagConfirmDataAdded == 1) {
    ConfirmationList = frmIBMyBillersHome.segBillersConfirm.data;
    //   
    //} else {
    //    frmIBMyBillersHome.segBillersConfirm.data = {};
    //    ConfirmationList.length = 0;
    //    
    //}
    var TotalBillerToBeAdded = BillerList.length + ConfirmationList.length;
    if (BillerList.length == 0) {
        return true;
    } else if (TotalBillerToBeAdded <= maxCount) return true
    else if (TotalBillerToBeAdded > maxCount) return false;
}
/*
*************************************************************************************
		Module	: checkMaxBillerCount
		Author  : Kony
		Purpose : Checking Max no. of billers to be added as customer billers
****************************************************************************************
*/
function checkMaxBillerCount() {
    var BillerList = myBillerList;
    //var BillerList = frmIBMyBillersHome.segBillersList.data;
    var maxCount = kony.os.toNumber(GLOBAL_MAX_BILL_COUNT);
    // if (gblFlagConfirmDataAdded == 1) {
    //        ConfirmationList = frmIBMyBillersHome.segBillersConfirm.data;
    //        
    //    } else {
    //        frmIBMyBillersHome.segBillersConfirm.data = {};
    //        ConfirmationList.length = 0;
    //        
    //    }
    //var TotalBillerToBeAdded = BillerList.length + ConfirmationList.length;
    if (BillerList.length == 0) {
        return true;
    } else if (BillerList.length < maxCount) return true
    else if (BillerList.length >= maxCount) return false;
}
/*
*************************************************************************************
		Module	: 	
		Author  : Kony
		Purpose : Validation function for checking Combination of Ref1 and comp code is unique
****************************************************************************************
*/
function checkDuplicateRef1CompCode() {     
    var BillerList  = [];     
    if (myBillerList != null) {        
        BillerList = myBillerList;
    } else {        
        BillerList = [];
    }                         
    var ConfirmationList = [];                       
    if (frmIBMyBillersHome.segBillersConfirm.data != null && frmIBMyBillersHome.segBillersConfirm.data != undefined) {                   
        ConfirmationList = frmIBMyBillersHome.segBillersConfirm.data;               
    } else {                               
        frmIBMyBillersHome.segBillersConfirm.data = [];                               
        ConfirmationList.length = 0;               
    }               
    var ref1 = frmIBMyBillersHome.txtAddBillerRef1.text;               
    var ref2 = frmIBMyBillersHome.txtAddBillerRef2.text;               
    var compCode = gblBillerCompCode;               
    var valCompCode = "";               
    var valRef1 = "";               
    var valRef2 = "";                             
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {                     
        valCompCode = BillerList[i].BillerCompCode.text;                        
        valRef1 = BillerList[i].lblRef1Value.text;                        
        valRef2 = BillerList[i].lblRef2Value.text;                                
        if (compCode == valCompCode && ref1 == valRef1) {
            if (gblIsRef2RequiredIB == "Y") {
                if (ref2 == valRef2) {
                    return false; //Duplicate Biller found
                }
            } else {
                return false; //Duplicate Biller found
            }
        }                                              
    }                                               
    for (var j = 0; 
        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {                    
        valCompCode = ConfirmationList[j].BillerCompCode;                        
        valRef1 = ConfirmationList[j].lblConfirmBillerRef1Value;                        
        valRef2 = ConfirmationList[j].lblConfirmBillerRef2Value;                                                                        
        if (compCode == valCompCode && ref1 == valRef1) {
            if (gblIsRef2RequiredIB == "Y") {
                if (ref2 == valRef2) {
                    return false; //Duplicate Biller found
                }
            } else {
                return false; //Duplicate Biller found
            }
        }                 
    }                           
    return true; //No Duplicate Biller
}
/*
*************************************************************************************
		Module	: validationNickNameIB
		Author  : Kony
		Purpose : Common Validation function for Nickname for various modules 
****************************************************************************************
*/
//function validationNickNameIB(text) {
//	if ((text == null) || (text == "")) return false;
//	var txtLen = text.length;
//	// 
//	// var alphaNumeric = kony.string.isAsciiAlphaNumeric(text);
//	//
//	//	|| !alphaNumeric
//	if (txtLen > 20) {
//		//alert("INVALID MOBILE NICK NAME, PLEASE RE-ENTER");
//		return false;
//	} else {
//		return true;
//	}
//}
/*
*************************************************************************************
		Module	: sendBillTopNotification
		Author  : Kony
		Purpose : called from My Biller and Topup to send notification to user after Adding Biller and Topup
****************************************************************************************
*/
function sendBillTopNotification(billerNickName, billerName, compCode, ref1Lbl, ref1Value, ref2lbl, ref2Value) {
    inputParams = {};
    var platformChannel = "";
    platformChannel = gblDeviceInfo.name;
    if (platformChannel == "thinclient") {
        //inputParams["channel"] = GLOBAL_IB_CHANNEL;
        inputParams["channel"] = "Internet Banking";
        inputParams["channelName"] = "Internet Banking";
    } else {
        //inputParams["channel"] = GLOBAL_MB_CHANNEL;
        inputParams["channel"] = "Mobile Banking";
        inputParams["channelName"] = "Mobile Banking";
    }
    inputParams["deliveryMethod"] = "Email";
    inputParams["noSendInd"] = "0";
    //inputParams["emailId"] = gblEmailIdBillerNotification;
    inputParams["notificationType"] = "Email";
    inputParams["Locale"] = kony.i18n.getCurrentLocale();
    //inputParams["notificationSubject"] = "Send Notification";
    //inputParams["notificationContent"] = "Biller/Topup Added Successfully";
    inputParams["customerName"] = gblCustomerName;
    inputParams["billerNickName"] = billerNickName;
    inputParams["billerName"] = billerName;
    inputParams["compCode"] = compCode;
    inputParams["ref1Lbl"] = ref1Lbl;
    inputParams["ref1Value"] = ref1Value;
    inputParams["ref2lbl"] = ref2lbl;
    inputParams["ref2Value"] = ref2Value;
    if (gblBillerTopupType == 1 || gblBillerTopupType == 3) //for Biller
        inputParams["source"] = "addBiller";
    else if (gblBillerTopupType == 2 || gblBillerTopupType == 4) // for Topup
        inputParams["source"] = "addTopup";
    invokeServiceSecureAsync("NotificationAdd", inputParams, sendBillTopNotificationCallBack);
}
/*
*************************************************************************************
		Module	: sendBillTopNotificationCallBack
		Author  : Kony
		Purpose : called from My Biller and Topup to send notification to user after Adding Biller and Topup
****************************************************************************************
*/
function sendBillTopNotificationCallBack(status, callBackResponse) {
    //  alert("called NotificationAdd ");
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            //  dismissLoadingScreenPopup();
        } else {
            //alert("Notification Failed");
        }
    } else {
        if (status == 300) {
            //dismissLoadingScreenPopup();
            //  alert("Notification Failed");
        }
    }
}
/*
*************************************************************************************
		Module	: deleteConfirmSegDataBillerTopupIB
		Author  : Kony
		Purpose : called from My Biller and Topup to delete local added biller in confirm segment
****************************************************************************************
*/
function deleteConfirmSegDataBillerTopupIB() {
    var curFormID = kony.application.getCurrentForm();
    var selIndex = curFormID.segBillersConfirm.selectedIndex;
    curFormID.segBillersConfirm.removeAt(selIndex[1]);
    //var billerCount = "";
    //billerCount = curFormID.segBillersConfirm.data;
    var tmpLength = 0;
    if (curFormID.segBillersConfirm.data != null) tmpLength = curFormID.segBillersConfirm.data.length;
    else tmpLength = 0;
    if (tmpLength < GLOBAL_MAX_BILL_ADD) {
        curFormID.btnConfirmBillerAdd.setEnabled(true);
        curFormID.btnAddBiller.setEnabled(true);
        curFormID.segSuggestedBillersList.setEnabled(true);
    }
    if (tmpLength == 0) {
        if (curFormID.id == "frmIBMyBillersHome") {
            preShowForBillers();
            postShowForBillers();
        } else if (curFormID.id == "frmIBMyTopUpsHome") {
            preShowForTopups();
            postShowForTopups();
        }
    }
}
////////////Functions from modIBTopup
function checkIfBarcodeOnlyIBTopUp() {
    var isBarCode = "";
    if (frmIBTopUpLandingPage.segSuggestedBiller.selectedItems != null) {
        indexOfSelectedIndex = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0];
        isBarCode = indexOfSelectedIndex.BarcodeOnly;
    } else {
        isBarCode = frmIBMyTopUpsHome.segSuggestedBillersList.selectedItems[0].BarcodeOnly;
    }
    if (isBarCode == "1") {
        return false;
    } else {
        return true;
    }
}
/*
************************************************************************************************************************
		Module	: loadAddTopupValues
		Author  : Kony
		Purpose : Mapping of Suggested biller to Add Biller Text Box , on click of + button on suggested Biller Segment 
************************************************************************************************************************
*/
function loadAddTopupValues() {
    gblBillerCompCode = "";
    gblBillerId = "";
    gblBillerMethod = "";
    gblRef2Flag = "";
    gblRef1LenIB = "";
    gblBillerId = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["BillerID"]["text"];
    gblBillerMethod = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["BillerMethod"]["text"];
    gblEffDt = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["EffDt"]["text"];
    gblRef1LenIB = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1Len"]["text"];
    gblbillerGroupType = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["BillerGroupType"];
    gblToAccountKey = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["ToAccountKey"];
    gblreccuringDisableAdd = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["IsRequiredRefNumber2Add"];
    gblreccuringDisablePay = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["IsRequiredRefNumber2Pay"];
    gblPayFull = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["IsFullPayment"];
    frmIBMyTopUpsHome.txtAddBillerRef1.maxTextLength = gblRef1LenIB;
    gblBillerCompCode = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["BillerCompCode"]["text"];
    gblBillerCategoryID = frmIBMyTopUpsHome.segSuggestedBillersList.selectedItems[0].BillerCategoryID.text;
    frmIBMyTopUpsHome.imgAddBillerLogo.src = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["imgSugBillerLogo"]["src"];
    frmIBMyTopUpsHome.lblAddBillerName.text = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["lblSugBillerName"]["text"];
    frmIBMyTopUpsHome.lblAddBillerRef1.text = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1Label"]
        ["text"] + ":";
    gblCurRef1LblTH = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1LblThai"]["text"] + ":";
    gblCurRef1LblEN = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["Ref1LblEng"]["text"] + ":";
    gblCurBillerNameEN = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["BillerNameEng"]["text"];
    gblCurBillerNameTH = frmIBMyTopUpsHome["segSuggestedBillersList"]["selectedItems"][0]["BillerNameThai"]["text"];
    gblRef1LblEN = gblCurRef1LblEN;
    gblRef1LblTH = gblCurRef1LblTH;
    gblBillerCompCodeEN = gblCurBillerNameEN;
    gblBillerCompCodeTH = gblCurBillerNameTH;
}

function tokenExchangeBeforeSaveTopUpParamsInSess() {
    var inputParam = [];
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, tokenExchangeBeforeSaveTopUpParamsInSessCallBack);
}

function tokenExchangeBeforeSaveTopUpParamsInSessCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            onNextClickedAddTopup();
        }
    }
}
/*
*************************************************************************************
		Module	: onNextClickedAddTopup
		Author  : Kony
		Purpose : Performing Validations for Billers just before showing OTP HBx. 
****************************************************************************************
*/
var gblFlagConfirmDataAddedTopup = 0;

function onNextClickedAddTopup() {
    //To fix Defect DEF14665 
    frmIBMyTopUpsHome.btnConfirmBillerAdd.setVisibility(true);
    // var billerRow = {};
    //var billerCount = "";
    var duplicateNickNameFlag = false;
    var duplicateRef1CompCodeFlag = false;
    var ref1value = frmIBMyTopUpsHome.txtAddBillerRef1.text;
    var ref1result = kony.string.isNumeric(ref1value);
    duplicateNickNameFlag = checkDuplicateNicknameOnAddTopupIB();
    duplicateRef1CompCodeFlag = checkDuplicateRef1CompCodeTopupIB();
    if (duplicateNickNameFlag && duplicateRef1CompCodeFlag && ref1result) {
        var billerNickName = frmIBMyTopUpsHome.txtAddBillerNickName.text;
        var billerName = frmIBMyTopUpsHome.lblAddBillerName.text;
        var ref2Val = "";
        callBillerValidationService(gblBillerCompCode, billerNickName, ref1value, ref2Val, billerName, channelFlagTopupIB);
    } else {
        if (!duplicateNickNameFlag) alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
        else if (!duplicateRef1CompCodeFlag) alert(kony.i18n.getLocalizedString("keyduplicatetopup"));
        else if (!ref1result) alert(kony.i18n.getLocalizedString("keyWrngRef1Val"));
        //alert(kony.i18n.getLocalizedString("Valid_EnterValidValues"));
        frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxOtpBox.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyTopUpsHome.btnConfirmBillerNext.setVisibility(false);
    }
}
/*
*************************************************************************************
		Module	: loadTopupConfirmSegmentData
		Author  : Kony
		Purpose : method for mapping of the topup, while adding, to confirmation page segment. 
****************************************************************************************
*/
function loadTopupConfirmSegmentData() {
    frmIBMyTopUpsHome.lblErrorMsg.setVisibility(false);
    var billerRow = {};
    //if(billerMethodFlag==true){
    billerRow = [{
        lblConfirmBillerName: frmIBMyTopUpsHome.txtAddBillerNickName.text,
        lblConfirmBillerNameCompCode: frmIBMyTopUpsHome.lblAddBillerName.text,
        btnConfirmBillerDelete: {
            text: "del",
            skn: "btnIBdelicon"
        },
        /*	btnPayBillComplete: {
            text: kony.i18n.getLocalizedString("TopUp")
        },*/
        imgConfirmBillerLogo: {
            src: frmIBMyTopUpsHome.imgAddBillerLogo.src
        },
        lblConfirmBillerRef1: frmIBMyTopUpsHome.lblAddBillerRef1.text,
        lblConfirmBillerRef1Value: frmIBMyTopUpsHome.txtAddBillerRef1.text,
        BillerCompCode: gblBillerCompCode,
        BillerId: gblBillerId,
        BillerNameEN: gblBillerCompCodeEN,
        BillerNameTH: gblBillerCompCodeTH,
        Ref1LblEN: gblCurRef1LblEN,
        Ref1LblTH: gblCurRef1LblTH,
        Ref2LblEN: "",
        Ref2LblTH: ""
    }];
    frmIBMyTopUpsHome.segBillersConfirm.addAll(billerRow);
    gblFlagConfirmDataAddedTopup = 1;
    var billerCount = "";
    billerCount = frmIBMyTopUpsHome.segBillersConfirm.data;
    var maxBulkAddCount = kony.os.toNumber(GLOBAL_MAX_BILL_ADD);
    if (billerCount.length >= maxBulkAddCount) {
        frmIBMyTopUpsHome.btnConfirmBillerAdd.setEnabled(false);
        frmIBMyTopUpsHome.btnAddBiller.setEnabled(false);
        frmIBMyTopUpsHome.segSuggestedBillersList.setEnabled(false);
    }
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(true);
    frmIBMyTopUpsHome.hbxOtpBox.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyTopUpsHome.btnConfirmBillerNext.setVisibility(true);
    frmIBMyTopUpsHome.txtAddBillerNickName.text = "";
    frmIBMyTopUpsHome.txtAddBillerRef1.text = "";
}
/*
*************************************************************************************
		Module	: addTopups
		Author  : Kony
		Purpose : Function for adding topup , calling customerBillAdd service
****************************************************************************************
*/
/*function addTopups() {
    tmpBillerName=[];
	tmpBillerNickName=[];
	tmpBillerRef1=[];
	tmpBillerRef1Lbl=[];
	tmpBillerCompCode=[];
	BatchLength= 0;
    billerCounterValue =0;
	var tmpAddBillData = frmIBMyTopUpsHome.segBillersConfirm.data;
	if(tmpAddBillData!=null)
	BatchLength = tmpAddBillData.length;
	var inputParams = {};
	for (i = 0; i < tmpAddBillData.length; i++) {
		inputParams = {};
		tmpBillerNickName[i]=tmpAddBillData[i]["lblConfirmBillerName"];
		tmpBillerName[i]=tmpAddBillData[i]["lblConfirmBillerNameCompCode"];
		tmpBillerRef1Lbl[i]=tmpAddBillData[i]["lblConfirmBillerRef1"];
		tmpBillerRef1[i]=tmpAddBillData[i]["lblConfirmBillerRef1Value"];
		//inputParams["crmId"] = gblcrmId;
		inputParams["BillerNickName"] = tmpAddBillData[i]["lblConfirmBillerName"];
		inputParams["BillerCompcode"] = tmpAddBillData[i]["BillerCompCode"];
		tmpBillerCompCode[i]=tmpAddBillData[i]["BillerCompCode"];
		inputParams["BillerId"] = tmpAddBillData[i]["BillerId"];
		inputParams["ReferenceNumber1"] = tmpAddBillData[i]["lblConfirmBillerRef1Value"];
		inputParams["httpheaders"] = {};
		inputParams["httpconfigs"] = {};
		// inputParams["clientDate"] = getCurrentDate();not working on IE 8
		showLoadingScreenPopup();
		invokeServiceSecureAsync("customerBillAdd", inputParams, addTopupsCallback);
	}
}*/
/*
*************************************************************************************
		Module	: addTopupsCallback
		Author  : Kony
		Purpose : Function for adding topups , calling customerBillAdd service
****************************************************************************************
*/
/*function addTopupsCallback(status, callBackResponse) {
	var activityTypeID = "062";
	var errorCode = "";
	var activityStatus = "";
	var deviceNickName = "";
	var activityFlexValues1 = "Add";
	var activityFlexValues2 = [];
	var activityFlexValues3 = [];
	var activityFlexValues4 = [];
	var activityFlexValues5 = [];
	
	var logLinkageId = "";
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			frmIBMyTopUpsHome.segBillersConfirm.removeAll();
			frmIBMyTopUpsHome.btnAddBiller.setEnabled(true); //Enabling add more since the segment is flushed
			frmIBMyTopUpsHome.btnConfirmBillerAdd.setEnabled(true); //Enabling add more since the segment is flushed
			frmIBMyTopUpsHome.segSuggestedBillersList.setEnabled(true); //Enabling add more since the segment is flushed
			frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
			frmIBMyTopUpsHome.hbxOtpBox.setVisibility(false)
			frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
			frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
			frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
			frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
			frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(false);
			frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(true);
			frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(true);
			frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
			//to disable OTP related widgets
			frmIBMyTopUpsHome.lblBankRef.setVisibility(false);
			frmIBMyTopUpsHome.lblBankRefValue.setVisibility(false);
			frmIBMyTopUpsHome.lblOTPMsg.setVisibility(false);
			frmIBMyTopUpsHome.lblOTPMobileNo.setVisibility(false);
			frmIBMyTopUpsHome.txtotp.text="";
			frmIBMyTopUpsHome.btnOTPReq.onClick = startBillTopUpOTPRequestService;
			 frmIBMyTopUpsHome.btnOTPReq.skin = btnIBREQotpFocus;
            frmIBMyTopUpsHome.btnOTPReq.focusSkin = btnIBREQotpFocus;
			
			activityStatus = "01";
			dismissLoadingScreenPopup();
			if(billerCounterValue < BatchLength)
			{	
					activityFlexValues2[billerCounterValue] = tmpBillerName[billerCounterValue]
					activityFlexValues3[billerCounterValue] =  tmpBillerNickName[billerCounterValue]
					activityFlexValues4[billerCounterValue] = tmpBillerRef1[billerCounterValue]
					activityFlexValues5[billerCounterValue] = ""
					
					activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
					activityFlexValues2[billerCounterValue], activityFlexValues3[billerCounterValue], activityFlexValues4[billerCounterValue], activityFlexValues5[billerCounterValue], logLinkageId);
					var tmpBillerNameNoCompCode =  tmpBillerName[billerCounterValue].substring(0, tmpBillerName[billerCounterValue].lastIndexOf("("));
					
					sendBillTopNotification(tmpBillerNickName[billerCounterValue],tmpBillerNameNoCompCode,tmpBillerCompCode[billerCounterValue],tmpBillerRef1Lbl[billerCounterValue],tmpBillerRef1[billerCounterValue],"","");
					billerCounterValue=billerCounterValue+1;
					
			}
			else{
						billerCounterValue=0;
					
			}
			//frmIBMyTopUpsHome.postShow();
			postShowForTopups();
		} else {
			alert(kony.i18n.getLocalizedString("ECGenericError"));
			activityStatus = "02";
			
			if(billerCounterValue < BatchLength)
			{	
					

					activityFlexValues2[billerCounterValue] = tmpBillerName[billerCounterValue]
					activityFlexValues3[billerCounterValue] =  tmpBillerNickName[billerCounterValue]
					activityFlexValues4[billerCounterValue] = tmpBillerRef1[billerCounterValue]
					activityFlexValues5[billerCounterValue] = ""
					activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
					activityFlexValues2[billerCounterValue], activityFlexValues3[billerCounterValue], activityFlexValues4[billerCounterValue], activityFlexValues5[billerCounterValue], logLinkageId);
					billerCounterValue=billerCounterValue+1;
			}
			else{
						billerCounterValue=0;
					
				}
			
			dismissLoadingScreenPopup();
		}
		//activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
		//	activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
		//activity log
	} else {
		if (status == 300) {
			billerCounterValue=0;
			dismissLoadingScreenPopup();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}*/
/*
*************************************************************************************
		Module	: checkDuplicateNicknameOnAddTopupIB
		Author  : Kony
		Purpose : Validation function for checking Nickname duplicate
****************************************************************************************
*/
function checkDuplicateNicknameOnAddTopupIB() {
    var BillerList = myTopupList;
    var ConfirmationList = frmIBMyTopUpsHome.segBillersConfirm.data;
    var nickname = frmIBMyTopUpsHome.txtAddBillerNickName.text;
    var check = "";
    var val = "";
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {
        val = BillerList[i].lblBillerNickname.text;
        if (kony.string.equals(nickname, val)) {
            return false;
        }
    }
    for (var j = 0;
        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
        val = ConfirmationList[j].lblConfirmBillerName;
        if (kony.string.equals(nickname, val)) {
            //alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
            return false;
        }
    }
    return true;
}
/*
*************************************************************************************
		Module	: checkMaxBillerCountOnConfirm
		Author  : Kony
		Purpose : Checking Max no. of billers to be added as customer billers
****************************************************************************************
*/
function checkMaxBillerCountOnConfirmTopupIB() {
    var maxCount = kony.os.toNumber(GLOBAL_MAX_BILL_COUNT);
    var ConfirmationList = "";
    var BillerList = myTopupList;
    //   var BillerList = frmIBMyTopUpsHome.segBillersList.data;
    //BillerList= myTopupList;
    // if (gblFlagConfirmDataAddedTopup== 1) {
    ConfirmationList = frmIBMyTopUpsHome.segBillersConfirm.data;
    //
    // } else {
    //     frmIBMyTopUpsHome.segBillersConfirm.data = {};
    //    ConfirmationList.length = 0;
    //     
    //}
    var TotalBillerToBeAdded = BillerList.length + ConfirmationList.length;
    if (BillerList.length == 0) {
        return true;
    } else if (TotalBillerToBeAdded <= maxCount) return true
    else if (TotalBillerToBeAdded > maxCount) return false;
}
/*
*************************************************************************************
		Module	: checkMaxBillerCountTopupIB
		Author  : Kony
		Purpose : Checking Max no. of topups to be added as customer billers
****************************************************************************************
*/
function checkMaxBillerCountTopupIB() {
    var BillerList = myTopupList;
    //BillerList= frmIBMyTopUpsHome.segBillersList.data;
    var maxCount = kony.os.toNumber(GLOBAL_MAX_BILL_COUNT);
    if (BillerList.length == 0) {
        return true;
    } else if (BillerList.length < maxCount) return true
    else if (BillerList.length >= maxCount) return false;
}
/*
*************************************************************************************
		Module	: checkDuplicateRef1CompCodeTopupIB
		Author  : Kony
		Purpose : Validation function for checking Combination of Ref1 and comp code is unique
****************************************************************************************
*/
function checkDuplicateRef1CompCodeTopupIB() {
    var ConfirmationList = [];
    var BillerList = [];
    if (frmIBMyTopUpsHome.segBillersConfirm.data != null && frmIBMyTopUpsHome.segBillersConfirm.data != undefined) {
        ConfirmationList = frmIBMyTopUpsHome.segBillersConfirm.data;
    } else {
        frmIBMyTopUpsHome.segBillersConfirm.data = [];
        ConfirmationList.length = 0;
    }
    if (myTopupList != null) BillerList = myTopupList;
    else BillerList = [];
    var ref1 = frmIBMyTopUpsHome.txtAddBillerRef1.text;
    var compCode = gblBillerCompCode;
    var valCompCode = "";
    var valRef1 = "";
    var duplicateFlag = false;
    var duplicateFlagConfirmSeg = false;
    if (BillerList.length == 0) {
        duplicateFlag = true;
    }
    for (var i = 0;
        (((BillerList) != null) && i < BillerList.length); i++) {
        valCompCode = BillerList[i].BillerCompCode.text;
        valRef1 = BillerList[i].lblRef1Value.text;
        if ((ref1 != valRef1) || (compCode != valCompCode)) {
            duplicateFlag = true;
        } else {
            duplicateFlag = false;
            return false;
        }
    }
    if (ConfirmationList.length == 0) {
        duplicateFlagConfirmSeg = true;
    }
    for (var j = 0;
        (((ConfirmationList) != null) && j < ConfirmationList.length); j++) {
        valCompCode = ConfirmationList[j].BillerCompCode;
        valRef1 = ConfirmationList[j].lblConfirmBillerRef1Value;
        if ((ref1 != valRef1) || (compCode != valCompCode)) {
            duplicateFlagConfirmSeg = true;
        } else {
            duplicateFlagConfirmSeg = false;
            return false;
        }
    }
    if (duplicateFlag && duplicateFlagConfirmSeg) return true;
    else {
        return false
    }
}
/*
*************************************************************************************
		Module	: callBillerValidationService
		Author  : Kony
		Purpose : Validation function for checking Combination of billernickname,Ref1 and comp code is unique
****************************************************************************************
*/
var glbChannelFlag = 0;

function callBillerValidationService(billerCompCode, billerNickName, ref1Val, ref2Val, billerName, channelFlag) {
    inputParam = {};
    if (ref2Val == null) {
        ref2Val = "";
    }
    inputParam["BillerCompcode"] = billerCompCode;
    inputParam["BillerNickName"] = billerNickName;
    inputParam["BillerName"] = billerName;
    inputParam["ReferenceNumber1"] = ref1Val;
    inputParam["ReferenceNumber2"] = ref2Val;
    inputParam["billerCategoryID"] = gblBillerCategoryID;
    inputParam["ModuleName"] = "BillAdd";
    if (billerCompCode == "2533") {
        inputParam["BillerStartTime"] = gBillerStartTime;
        inputParam["BillerEndTime"] = gBillerEndTime;
    }
    inputParam["billerMethod"] = gblBillerMethod;
    glbChannelFlag = channelFlag;
    // alert("glbChannelFlag is "+ glbChannelFlag);
    if (glbChannelFlag == 1 || glbChannelFlag == 2) {
        showLoadingScreenPopup();
    } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
        showLoadingScreen();
    }
    invokeServiceSecureAsync("billerValidation", inputParam, callBillerValidationServiceCallBack);
}

function callBillerValidationServiceCallBack(status, result) {
    if (status == 400) //success responce
    {
        var validationFlag = "";
        if (result["opstatus"] == 0) {
            if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                dismissLoadingScreenPopup();
            } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                dismissLoadingScreen();
            }
            validationFlag = result["validationResult"];
        } else {
            if (glbChannelFlag == 1 || glbChannelFlag == 2) {
                dismissLoadingScreenPopup();
            } else if (glbChannelFlag == 3 || glbChannelFlag == 4) {
                dismissLoadingScreen();
            }
            validationFlag = result["validationResult"];
        }
        if (validationFlag == "true") {
            verifyBillerMethod(glbChannelFlag);
        } else {
            if (result["isDuplicateNickName"] == "true") {
                alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
                return false;
            }
            //ENH113
            if (result["isNotAllowedTime"] == "true") {
                //alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
                alert("Not allowed to add biller now");
                return false;
            }
            //ENH113
            if (glbChannelFlag == 2 || glbChannelFlag == 4) {
                alert(kony.i18n.getLocalizedString("keyTopUpValidationFailed"));
            } else {
                alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
            }
        }
    }
}
/**
 * Method to reset all the Biller/Topups cached variables
 * @returns {}
 */
function resetAllBillTopupCacheDataIB() {
    if (myBillerList != null) {
        for (var i = 0; i < myBillerList.length; i++) {
            myBillerList.pop();
        }
        myBillerList = [];
    }
    if (myBillerSuggestList != null) {
        for (var i = 0; i < myBillerSuggestList.length; i++) {
            myBillerSuggestList.pop();
        }
        myBillerSuggestList = [];
    }
    if (myTopupList != null) {
        for (var i = 0; i < myTopupList.length; i++) {
            myTopupList.pop();
        }
        myTopupList = [];
    }
    if (myTopupSuggestList != null) {
        for (var i = 0; i < myTopupSuggestList.length; i++) {
            myTopupSuggestList.pop();
        }
        myTopupSuggestList = [];
    }
    TMBUtil.DestroyForm(frmIBMyBillersHome);
    TMBUtil.DestroyForm(frmIBMyTopUpsHome);
}

function syncIBTopupLocale() {
    getLocaleBillerIB();
    var currentFormId = kony.application.getCurrentForm().id;
    var locale = kony.i18n.getCurrentLocale();
    if (currentFormId == "frmIBTopUpLandingPage") {
        if (!gblPaynow) {
            var repeatedReturnArray = switchScheduleDetailsIB(frmIBTopUpLandingPage.lblDatesFuture.text, frmIBTopUpLandingPage.lblrepeats.text);
            frmIBTopUpLandingPage.lblDatesFuture.text = repeatedReturnArray[0];
            //frmIBTopUpLandingPage.lblDatesFuture.setVisibility(false);
            frmIBTopUpLandingPage.lblrepeats.text = repeatedReturnArray[1];
        }
        if (locale == "en_US") {
            frmIBTopUpLandingPage.ref1.text = gblRef1LblEN;
            frmIBTopUpLandingPage.lblCompCode.text = gblBillerCompCodeEN;
            frmIBTopUpLandingPage.lblName.text = gblNickNameEN;
            frmIBTopUpLandingPage.lblAcctType.text = gblProductNameEN
            frmIBTopUpLandingPage.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
        } else {
            frmIBTopUpLandingPage.ref1.text = gblRef1LblTH;
            frmIBTopUpLandingPage.lblCompCode.text = gblBillerCompCodeTH;
            frmIBTopUpLandingPage.lblName.text = gblNickNameTH;
            frmIBTopUpLandingPage.lblAcctType.text = gblProductNameTH;
            frmIBTopUpLandingPage.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
        }
        if (gblrepeat == 1) {
            frmIBTopUpLandingPage.lblENDtext.text = kony.i18n.getLocalizedString("keyEndAfter");
        } else if (gblrepeat == 2) {
            frmIBTopUpLandingPage.lblENDtext.text = kony.i18n.getLocalizedString("keyEndDate");
        }
        frmIBTopUpLandingPage.txtSearchBox.placeholder = kony.i18n.getLocalizedString('Receipent_Search');
        if (frmIBTopUpLandingPage.hbxBiller.isVisible) {
            showLoadingScreenPopup();
            getSelectTopUpCategoryService();
        }
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
        frmIBTopUpLandingPage.segMenuOptions.removeAll();
        if (kony.i18n.getCurrentLocale() != "th_TH") frmIBTopUpLandingPage.btnMenuTopUp.skin = "btnIBMenuTopUpFocus";
        else frmIBTopUpLandingPage.btnMenuTopUp.skin = "btnIBMenuTopUpFocusThai";
    }
    if (currentFormId == "frmIBTopUpConfirmation") {
        if (locale == "en_US") {
            frmIBTopUpConfirmation.lblRef1.text = gblRef1LblEN;
            frmIBTopUpConfirmation.lblCompCodeTo.text = gblBillerCompCodeEN;
            frmIBTopUpConfirmation.lblName.text = gblNickNameTH;
            if (billPayTopup) {
                frmIBTopUpConfirmation.lblFromAccountName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBTopUpConfirmation.lblFromAccountName.text = accntTopupName;
            }
            frmIBTopUpConfirmation.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
            //frmIBTopUpConfirmation.lbl.text 
        } else {
            frmIBTopUpConfirmation.lblRef1.text = gblRef1LblTH;
            frmIBTopUpConfirmation.lblCompCodeTo.text = gblBillerCompCodeTH;
            frmIBTopUpConfirmation.lblName.text = gblNickNameTH;
            if (billPayTopup) {
                frmIBTopUpConfirmation.lblFromAccountName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBTopUpConfirmation.lblFromAccountName.text = accntTopupName;
            }
            frmIBTopUpConfirmation.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
        }
        if (gblClicked == "Daily") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (gblClicked == "Weekly") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (gblClicked == "Monthly") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (gblClicked == "Yearly") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
        } else {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
        }
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
        frmIBTopUpConfirmation.segMenuOptions.removeAll();
        if (kony.i18n.getCurrentLocale() != "th_TH") frmIBTopUpConfirmation.btnMenuTopUp.skin = "btnIBMenuTopUpFocus";
        else frmIBTopUpConfirmation.btnMenuTopUp.skin = "btnIBMenuTopUpFocusThai";
    }
    if (currentFormId == "frmIBTopUpComplete") {
        var paymentOrderDate = kony.i18n.getLocalizedString('keyBillPaymentPaymentOrderDate');
        var paymentDate = kony.i18n.getLocalizedString('keyIBPaymentDate');
        if (locale == "en_US") {
            frmIBTopUpComplete.lblref1.text = gblRef1LblEN;
            frmIBTopUpComplete.lblCompCode.text = gblBillerCompCodeEN;
            frmIBTopUpComplete.lblName.text = gblProductNameEN;
            //frmIBTopUpComplete.lblFromName.text = gblCustomerName;
            if (billPayTopup) {
                frmIBTopUpComplete.lblFromName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBTopUpComplete.lblFromName.text = accntTopupName;
            }
            frmIBTopUpComplete.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
            if (gblPaynow) {
                frmIBTopUpComplete.lblTransfer.text = paymentDate;
            } else {
                frmIBTopUpComplete.lblTransfer.text = paymentOrderDate;
            }
        } else {
            frmIBTopUpComplete.lblref1.text = gblRef1LblTH;
            frmIBTopUpComplete.lblCompCode.text = gblBillerCompCodeTH;
            frmIBTopUpComplete.lblName.text = gblProductNameTH;
            //	frmIBTopUpComplete.lblFromName.text = gblCustomerNameTh;
            if (billPayTopup) {
                frmIBTopUpComplete.lblFromName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBTopUpComplete.lblFromName.text = accntTopupName;
            }
            frmIBTopUpComplete.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
            if (gblPaynow) {
                frmIBTopUpComplete.lblTransfer.text = paymentDate;
            } else {
                frmIBTopUpComplete.lblTransfer.text = paymentOrderDate;
            }
        }
        if (gblClicked == "Daily") {
            frmIBTopUpComplete.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (gblClicked == "Weekly") {
            frmIBTopUpComplete.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (gblClicked == "Monthly") {
            frmIBTopUpComplete.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (gblClicked == "Yearly") {
            frmIBTopUpComplete.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
        } else {
            frmIBTopUpComplete.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
        }
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
        frmIBTopUpComplete.segMenuOptions.removeAll();
        if (frmIBTopUpComplete.lblBBPaymentValue.isVisible) {
            frmIBTopUpComplete.lblHide.text = kony.i18n.getLocalizedString("keyHideIB");
        } else {
            frmIBTopUpComplete.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
        }
        if (kony.i18n.getCurrentLocale() != "th_TH") frmIBTopUpComplete.btnMenuTopUp.skin = "btnIBMenuTopUpFocus";
        else frmIBTopUpComplete.btnMenuTopUp.skin = "btnIBMenuTopUpFocusThai";
    }
    if (currentFormId == "frmIBBillPaymentLP") {
        //	var repeatedReturnArray = switchScheduleDetailsIB(frmIBBillPaymentLP.lblDatesFuture.text, frmIBBillPaymentLP.lblrepeats.text);
        //	frmIBBillPaymentLP.lblDatesFuture.text = repeatedReturnArray[0];
        if (!gblPaynow) {
            var repeatedReturnArray = switchScheduleDetailsIB(frmIBBillPaymentLP.lblDatesFuture.text, frmIBBillPaymentLP.lblrepeats.text);
            frmIBBillPaymentLP.lblDatesFuture.text = repeatedReturnArray[0];
            //frmIBBillPaymentLP.lblDatesFuture.setVisibility(false);
            frmIBBillPaymentLP.lblrepeats.text = repeatedReturnArray[1];
        }
        if (frmIBBillPaymentLP.hbxToBiller.isVisible) {
            getSelectBillerCategoryService();
        }
        frmIBBillPaymentLP.lblScheduleNickName.text = kony.i18n.getLocalizedString("keyNickname");
        frmIBBillPaymentLP.lblAddBillsBeforeSchedule.text = kony.i18n.getLocalizedString("MIB_BPNicknameSch");
        //frmIBBillPaymentLP.txtScheduleNickname.placeholder = kony.i18n.getLocalizedString("keyXferAmt");
        if (locale == "en_US") {
            if (gblCompCode != undefined && gblCompCode != "" && gblCompCode == "2533") {
                frmIBBillPaymentLP.lblBPAmount.text = gblSegBillerData["billerTotalPayAmtEn"] + ":";
            } else {
                frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString('keyXferAmt');
            }
            frmIBBillPaymentLP.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
            frmIBBillPaymentLP.txtBPSearch.placeholder = kony.i18n.getLocalizedString('Receipent_Search');
            frmIBBillPaymentLP.lblBPRef1.text = appendColon(gblRef1LblEN);
            frmIBBillPaymentLP.lblRef2.text = appendColon(gblRef2LblEN);
            frmIBBillPaymentLP.lblToCompCode.text = gblBillerCompCodeEN;
            if (isNotBlank(gblNickNameEN)) {
                frmIBBillPaymentLP.lblBPFromNameRcvd.text = gblNickNameEN;
            }
            frmIBBillPaymentLP.lblProductType.text = gblProductNameEN;
        } else {
            frmIBBillPaymentLP.txtBPSearch.placeholder = kony.i18n.getLocalizedString('Receipent_Search');
            if (gblCompCode != undefined && gblCompCode != "" && gblCompCode == "2533") {
                frmIBBillPaymentLP.lblBPAmount.text = gblSegBillerData["billerTotalPayAmtTh"] + ":";
            } else {
                frmIBBillPaymentLP.lblBPAmount.text = kony.i18n.getLocalizedString('keyXferAmt');
            }
            frmIBBillPaymentLP.lblBPRef1.text = appendColon(gblRef1LblTH);
            frmIBBillPaymentLP.lblRef2.text = appendColon(gblRef2LblTH);
            frmIBBillPaymentLP.lblToCompCode.text = gblBillerCompCodeTH;
            if (isNotBlank(gblNickNameTH)) {
                frmIBBillPaymentLP.lblBPFromNameRcvd.text = gblNickNameTH;
            }
            frmIBBillPaymentLP.lblProductType.text = gblProductNameTH
            frmIBBillPaymentLP.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
        }
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
        frmIBBillPaymentLP.segMenuOptions.removeAll();
        if (kony.i18n.getCurrentLocale() != "th_TH") frmIBBillPaymentLP.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
        else frmIBBillPaymentLP.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
    }
    if (currentFormId == "frmIBBillPaymentConfirm") {
        var paymentOrderDate = kony.i18n.getLocalizedString('keyBillPaymentPaymentOrderDate');
        var paymentDate = kony.i18n.getLocalizedString('keyIBPaymentDate');
        if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
            frmIBBillPaymentConfirm.label476047582115288.text = kony.i18n.getLocalizedString("emptyToken");
        } else {
            frmIBBillPaymentConfirm.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
            frmIBBillPaymentConfirm.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        }
        if (locale == "en_US") {
            if (gblPaynow) {
                frmIBBillPaymentConfirm.lblPaymentDate.text = paymentDate;
            } else {
                frmIBBillPaymentConfirm.lblPaymentDate.text = paymentOrderDate;
            }
            frmIBBillPaymentConfirm.lblBPRef1.text = appendColon(gblRef1LblEN);
            frmIBBillPaymentConfirm.lblBPRef2.text = appendColon(gblRef2LblEN);
            frmIBBillPaymentConfirm.lblFromAccName.text = gblProductNameEN;
            if (billPaySummary) {
                frmIBBillPaymentConfirm.lblCustomerName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBBillPaymentConfirm.lblCustomerName.text = accntNme;
            }
            frmIBBillPaymentConfirm.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
        } else {
            if (gblPaynow) {
                frmIBBillPaymentConfirm.lblPaymentDate.text = paymentDate;
            } else {
                frmIBBillPaymentConfirm.lblPaymentDate.text = paymentOrderDate;
            }
            if (billPaySummary) {
                frmIBBillPaymentConfirm.lblCustomerName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBBillPaymentConfirm.lblCustomerName.text = accntNme;
            }
            frmIBBillPaymentConfirm.lblBPRef1.text = appendColon(gblRef1LblTH);
            frmIBBillPaymentConfirm.lblBPRef2.text = appendColon(gblRef2LblTH);
            frmIBBillPaymentConfirm.lblFromAccName.text = gblProductNameTH;
            frmIBBillPaymentConfirm.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
        }
        //frmIBBillPaymentConfirm.txtotp.placeholder = kony.i18n.getLocalizedString('enterOTP')
        if (gblClicked == "Daily") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (gblClicked == "Weekly") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (gblClicked == "Monthly") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (gblClicked == "Yearly") {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
        } else {
            frmIBBillPaymentConfirm.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
        }
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
        frmIBBillPaymentConfirm.segMenuOptions.removeAll();
        if (kony.i18n.getCurrentLocale() != "th_TH") frmIBBillPaymentConfirm.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
        else frmIBBillPaymentConfirm.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
    }
    if (currentFormId == "frmIBBillPaymentCompletenow") {
        var paymentOrderDate = kony.i18n.getLocalizedString('keyBillPaymentPaymentOrderDate');
        var paymentDate = kony.i18n.getLocalizedString('keyIBPaymentDate');
        frmIBBillPaymentCompletenow.lblBalance.text = kony.i18n.getLocalizedString('keyBalanceAfterPayment');
        if (locale == "en_US") {
            if (gblPaynow) {
                frmIBBillPaymentCompletenow.lblTransfer.text = paymentDate;
            } else {
                frmIBBillPaymentCompletenow.lblTransfer.text = paymentOrderDate;
            }
            frmIBBillPaymentCompletenow.lblRef1.text = appendColon(gblRef1LblEN);
            frmIBBillPaymentCompletenow.lblref2.text = appendColon(gblRef2LblEN);
            frmIBBillPaymentCompletenow.lblFromName.text = gblProductNameEN;
            //frmIBBillPaymentCompletenow.lblCustName.text = gblCustomerName;
            if (billPaySummary) {
                frmIBBillPaymentCompletenow.lblCustName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBBillPaymentCompletenow.lblCustName.text = accntNme;
            }
            frmIBBillPaymentCompletenow.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
        } else {
            //	frmIBBillPaymentCompletenow.lblCustName.text = gblCustomerNameTh;
            if (billPaySummary) {
                frmIBBillPaymentCompletenow.lblCustName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"];
            } else {
                frmIBBillPaymentCompletenow.lblCustName.text = accntNme;
            }
            frmIBBillPaymentCompletenow.lblRef1.text = appendColon(gblRef1LblTH);
            frmIBBillPaymentCompletenow.lblref2.text = appendColon(gblRef2LblTH);
            frmIBBillPaymentCompletenow.lblFromName.text = gblProductNameTH;
            frmIBBillPaymentCompletenow.lblFreeTran.text = kony.i18n.getLocalizedString('remFreeTran');
            if (gblPaynow) {
                frmIBBillPaymentCompletenow.lblTransfer.text = paymentDate;
            } else {
                frmIBBillPaymentCompletenow.lblTransfer.text = paymentOrderDate;
            }
        }
        if (gblClicked == "Daily") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (gblClicked == "Weekly") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (gblClicked == "Monthly") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (gblClicked == "Yearly") {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
        } else {
            frmIBBillPaymentCompletenow.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
        }
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
        frmIBBillPaymentCompletenow.segMenuOptions.removeAll();
        if (kony.i18n.getCurrentLocale() != "th_TH") frmIBBillPaymentCompletenow.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
        else frmIBBillPaymentCompletenow.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
    }
    if (currentFormId == "frmIBMyBillersHome") {
        startDisplayBillerCategoryServiceIB();
        getMyBillSuggestListIB();
        getMyBillListIB();
        var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
        if (!isIE8) {
            frmIBMyBillersHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
        }
        if (!gblAddBillerButtonClicked) {
            if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
                frmIBMyBillersHome.lblAddBillerRef1.text = gblCurRef1LblEN;
                frmIBMyBillersHome.lblAddBillerRef2.text = gblCurRef2LblEN;
                frmIBMyBillersHome.lblAddBillerName.text = gblCurBillerNameEN;
            } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
                frmIBMyBillersHome.lblAddBillerRef1.text = gblCurRef1LblTH;
                frmIBMyBillersHome.lblAddBillerRef2.text = gblCurRef2LblTH;
                frmIBMyBillersHome.lblAddBillerName.text = gblCurBillerNameTH;
            }
        } else {
            frmIBMyBillersHome.lblAddBillerRef1.text = kony.i18n.getLocalizedString("keyRef1");
            frmIBMyBillersHome.lblAddBillerRef2.text = kony.i18n.getLocalizedString("keyRef2");
        }
        if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
            frmIBMyBillersHome.lblViewBillerNameCompCode.text = gblCurViewBillerEng;
            frmIBMyBillersHome.lblViewBillerRef1.text = gblCurViewRef1Eng + ":";
            if (!kony.string.equalsIgnoreCase(gblCurViewRef2Eng, "")) frmIBMyBillersHome.lblViewBillerRef2.text = gblCurViewRef2Eng + ":";
            else frmIBMyBillersHome.lblViewBillerRef2.text = ""
        } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
            frmIBMyBillersHome.lblViewBillerNameCompCode.text = gblCurViewBillerThai;
            frmIBMyBillersHome.lblViewBillerRef1.text = gblCurViewRef1Thai + ":";
            if (!kony.string.equalsIgnoreCase(gblCurViewRef2Thai, "")) frmIBMyBillersHome.lblViewBillerRef2.text = gblCurViewRef2Thai + ":";
            else frmIBMyBillersHome.lblViewBillerRef2.text = ""
        }
    }
    if (currentFormId == "frmIBMyTopUpsHome") {
        startDisplayTopUpCategoryServiceIB();
        getMyTopUpSuggestListIB();
        getMyTopUpListIB();
        var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
        if (!isIE8) {
            frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
        }
        frmIBMyTopUpsHome.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
        frmIBMyTopUpsHome.lblOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsg");
        frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        if (!gblAddTopupButtonClicked) {
            if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
                frmIBMyTopUpsHome.lblAddBillerRef1.text = gblCurRef1LblEN;
                frmIBMyTopUpsHome.lblAddBillerName.text = gblCurBillerNameEN;
            } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
                frmIBMyTopUpsHome.lblAddBillerRef1.text = gblCurRef1LblTH;
                frmIBMyTopUpsHome.lblAddBillerName.text = gblCurBillerNameTH;
            }
        } else {
            frmIBMyTopUpsHome.lblAddBillerRef1.text = kony.i18n.getLocalizedString("keyRef1");
        }
        if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
            frmIBMyTopUpsHome.lblViewBillerNameCompCode.text = gblCurViewBillerEng;
            frmIBMyTopUpsHome.lblViewBillerRef1.text = gblCurViewRef1Eng + ":";
        } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
            frmIBMyTopUpsHome.lblViewBillerNameCompCode.text = gblCurViewBillerThai;
            frmIBMyTopUpsHome.lblViewBillerRef1.text = gblCurViewRef1Thai + ":";
        }
        if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
        else langspecificmenu();
        frmIBMyTopUpsHome.segMenuOptions.removeAll();
        if (kony.i18n.getCurrentLocale() != "th_TH") frmIBMyTopUpsHome.btnMenuTopUp.skin = "btnIBMenuTopUpFocus";
        else frmIBMyTopUpsHome.btnMenuTopUp.skin = "btnIBMenuTopUpFocusThai";
    }
    if (currentFormId == "frmIBBillPaymentCW") {
        callBillPaymentCustomerAccountServiceIB();
        if (kony.i18n.getCurrentLocale() == "th_TH") {
            frmIBBillPaymentCW.lblBPRef1.text = appendColon(gblRef1LblTH);
        } else {
            frmIBBillPaymentCW.lblBPRef1.text = appendColon(gblRef1LblEN);
        }
        frmIBBillPaymentCW.lblBPAmount.text = kony.i18n.getLocalizedString('keyXferAmt');
    }
    if (currentFormId == "frmIBTopUpCW") {
        callTopUpPaymentCustomerAccountServiceIB();
    }
    if (currentFormId == 'frmIBBeepAndBillList') {
        onLocaleChangeBBListPage();
    }
    if (currentFormId == 'frmIBBeepAndBillApply') {
        onLocaleChangeBBApply();
    }
    if (currentFormId == 'frmIBBeepAndBillApplyCW') {
        onLocaleChangeBBApplyCW();
    }
    if (currentFormId == 'frmIBBeepAndBillConfAndComp') {
        onLocaleChangeBBConfirmPage();
    }
    if (currentFormId == 'frmIBBeepAndBillExecuteConfComp') {
        onLocaleChangeBBExecuteConfirmPage();
    }
    if (currentFormId == 'frmIBSSApply') {
        frmS2SApplyLocaleChange()
    }
    if (currentFormId == 'frmIBSSSDetails') {
        getCustomerAccountForIBInfo_LCforEdit();
    }
    if (currentFormId == 'frmIBSSApplyCnfrmtn') {
        changeLocaleApplyConfirm();
    }
    if (currentFormId == 'frmIBSSApplyCmplete') {
        changeLocaleApplyComplete();
    }
    if (currentFormId == 'frmIBSSSExcuteTrnsfr') {
        changeLocaleforExecuteDetailIB();
    }
    if (currentFormId == 'frmIBSSSExcuteTrnsfrCmplete') {
        changeLocaleforExecuteCompleteIB();
    }
}

function onMyTopUpSelectMoreDynamicIB(ArgTable) {
    var currentForm = kony.application.getCurrentForm();
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblTopUpMore to max length
    gblTopUpMore = gblTopUpMore + maxLength;
    var newData = [];
    var totalData = ArgTable; //data from static population 
    var totalLength = totalData.length;
    if (totalLength == 0) {
        currentForm.lblSuggestedBiller.setVisibility(false);
    } else {
        search = currentForm.tbxSearch.text;
        if (search.length >= 3) currentForm.lblErrorMsg.setVisibility(false);
        currentForm.lblSuggestedBiller.setVisibility(true);
    }
    if (totalLength > gblTopUpMore) {
        currentForm.hbxMoreSug.setVisibility(true);
        var endPoint = gblTopUpMore;
    } else {
        currentForm.hbxMoreSug.setVisibility(false);
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(ArgTable[i])
    }
    currentForm.segSuggestedBillersList.data = newData;
    dismissLoadingScreenPopup();
    currentForm.tbxSearch.setFocus(true);
}

function onMyCustTopUpSelectMoreDynamicIB(gblCustTopUpSelectFormDataHolderIB) {
    var currentForm = kony.application.getCurrentForm();
    var maxLength = kony.os.toNumber(GLOBAL_LOAD_MORE); //configurable parameter also initilaize gblCustTopUpMore to max length
    gblCustTopUpMore = gblCustTopUpMore + maxLength;
    var newData = [];
    var totalData = gblCustTopUpSelectFormDataHolderIB; //data from static population 
    var totalLength = totalData.length;
    if (totalLength == 0) {
        currentForm.lblMyBills.setVisibility(false);
    } else {
        currentForm.lblErrorMsg.setVisibility(false);
        if (currentForm["id"] == 'frmIBMyBillersHome') {
            if (!isSelectBillerEnabled) currentForm.lblMyBills.setVisibility(true);
        } else {
            if (!isSelectTopupEnabled) currentForm.lblMyBills.setVisibility(true);
        }
    }
    if (totalLength > gblCustTopUpMore) {
        if (currentForm["id"] == 'frmIBMyBillersHome') {
            if (!isSelectBillerEnabled) currentForm.hbxMore.setVisibility(true);
        } else {
            if (!isSelectTopupEnabled) currentForm.hbxMore.setVisibility(true);
        }
        var endPoint = gblCustTopUpMore;
    } else {
        currentForm.hbxMore.setVisibility(false);
        var endPoint = totalLength;
    }
    for (i = 0; i < endPoint; i++) {
        newData.push(gblCustTopUpSelectFormDataHolderIB[i])
    }
    currentForm.segBillersList.data = newData;
    if (currentForm.segBillersList.data == null || currentForm.segBillersList.data.length == 0) frmIBMyBillersHome.lblNoBills.setVisibility(true);
}
//function billerCatChangeNew() {
//	var showCatList = [];
//	var showCatSuggestList = [];
//	var tmpCatSelected = frmIBMyBillersHome.cbxSelectCat.selectedKey;
//	
//	var searchTxt = frmIBMyBillersHome.tbxSearch.text;
//	
//	tmpBillerData = myBillerList;
//	tmpSugBillerData = myBillerSuggestList;
//	lengthBillersList = myBillerList.length;
//	lengthBillersSuggestList = myBillerSuggestList.length;
//	// MyBills Filtering
//	
//	if (searchTxt.length == 0) {
//		// no text used, get list of All MYBillers. global list
//		tmpBillerData = myBillerList;
//	} else {
//		//filter global list by search text
//		var searchtxt = search.toLowerCase();
//		for (j = 0, i = 0; i < lengthBillersList; i++) {
//			if (myBillerList[i]["lblBillerNickname"]) {
//				var lBillerNickname = myBillerList[i]["lblBillerNickname"]["text"];
//				var lBillerName = myBillerList[i]["lblBillerName"]["text"];
//				if ((kony.string.startsWith(searchtxt, lBillerName, true)) || (kony.string.startsWith(searchtxt, lBillerNickname,
//					true))) {
//					tmpBillerData[j] = myBillerList[i];
//					j++;
//				}
//			}
//		}
//	}
//	
//	// Now use category ID to filter within the result MYBillers list
//	if (null == tmpCatSelected || tmpCatSelected == undefined || tmpCatSelected == 0) {
//		showCatList = tmpBillerData; // apply no category ;
//	} else { //apply selected category
//		for (j = 0, i = 0; i < tmpBillerData.length; i++) {
//			tmp = tmpBillerData[i].BillerCategoryID.text;
//			
//			if (kony.string.equalsIgnoreCase(tmp, tmpCatSelected)) {
//				
//				showCatList[j] = tmpBillerData[i];
//				j++;
//			}
//		}
//	}
//	
//	//Suggested Bills Filtering
//	
//	if (searchTxt.length == 0) {
//		// no text used, get list of All SuggBillers. global list
//		tmpSugBillerData = myBillerSuggestList;
//	} else {
//		//filter global list by search text
//		var searchtxt = search.toLowerCase();
//		for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {
//			if (myBillerSuggestList[i]["lblBillerName"]) {
//				var lSuggestedBillerName = myBillerSuggestList[i]["lblBillerName"]["text"];
//				if (kony.string.startsWith(searchtxt, lSuggestedBillerName, true)) {
//					tmpSugBillerData[j] = myBillerSuggestList[i];
//					j++;
//				}
//			}
//		}
//	}
//	
//	// Now use category ID to filter within the result SuggBillers list
//	if (null == tmpCatSelected || tmpCatSelected == undefined || tmpCatSelected == 0) {
//		showCatSuggestList = tmpSugBillerData; // apply no category ;
//	} else { //apply selected category
//		for (j = 0, i = 0; i < tmpSugBillerData.length; i++) {
//			tmp = tmpSugBillerData[i].BillerCategoryID.text;
//			
//			if (kony.string.equalsIgnoreCase(tmp, tmpCatSelected)) {
//				
//				showCatSuggestList[j] = tmpSugBillerData[i];
//				j++;
//			}
//		}
//	}
//	
//	frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(true);
//	frmIBMyBillersHome.segSuggestedBillersList.setData(showCatSuggestList);
//	frmIBMyBillersHome.segBillersList.setData(showCatList);
//}
//Added by Bhaskar - Start
function getMasterBillSelectListIB() {
    var inputParams = {
        IsActive: "1",
        BillerGroupType: gblGroupTypeBiller,
        flagBillerList: "IB"
            // clientDate: getCurrentDate()not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMasterBillSelectListIBServiceAsyncCallback);
}

function startMasterBillSelectListIBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (responseData.length > 0) {
                populateMasterBillSelectToGblVariable(callBackResponse["MasterBillerInqRs"]);
                // getMyBillListIB(); // calling customer Bill Inquiry	
                onMyTopUpSelectMoreDynamicIB(gblMasterBillerSelectIB);
                gblBillerLastSearchNoCat = gblMasterBillerSelectIB;
                sugBillerCatChange();
                dismissLoadingScreenPopup();
            } else {
                gblMasterBillerSelectIB = [];
                dismissLoadingScreenPopup();
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } else {
        if (status == 300) {
            gblMasterBillerSelectIB = [];
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function populateMasterBillSelectToGblVariable(collectionData) {
    var tmpRef2Len = "";
    var tmpRef2label = "";
    var tmpRef2Len = 0;
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    gblMasterBillerSelectIB = [];
    var tempRecord = [];
    var tmpIsRef2Req = "";
    if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    for (var i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BillerGroupType"] == gblGroupTypeBiller) {
            tmpIsRef2Req = collectionData[i]["IsRequiredRefNumber2Add"];
            if (tmpIsRef2Req == "Y" || tmpIsRef2Req == "y") {
                tmpRef2Len = collectionData[i]["Ref2Len"];
                tmpRef2label = collectionData[i][ref2label];
            } else {
                tmpRef2Len = 0;
                tmpRef2Label = "";
            }
            imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            tempRecord = {
                "lblSugBillerName": {
                    "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "btnSugBillerAdd": {
                    "text": "Add",
                    "skin": "btnIBaddsmall"
                },
                "BillerID": {
                    "text": collectionData[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "Ref1Label": {
                    "text": collectionData[i][ref1label]
                },
                "Ref2Label": {
                    "text": tmpRef2label
                },
                "IsRequiredRefNumber2Add": {
                    "text": collectionData[i]["IsRequiredRefNumber2Add"]
                },
                "IsRequiredRefNumber2Pay": {
                    "text": collectionData[i]["IsRequiredRefNumber2Pay"]
                },
                "EffDt": {
                    "text": collectionData[i]["EffDt"]
                },
                "BarcodeOnly": collectionData[i]["BarcodeOnly"],
                "BillerGroupType": collectionData[i]["BillerGroupType"],
                "ToAccountKey": collectionData[i]["ToAccountKey"],
                "IsFullPayment": collectionData[i]["IsFullPayment"],
                "BillerMethod": {
                    "text": collectionData[i]["BillerMethod"]
                },
                "Ref1Len": {
                    "text": collectionData[i]["Ref1Len"]
                },
                "Ref2Len": {
                    "text": tmpRef2Len
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                },
                "imgSugBillerLogo": {
                    "src": imagesUrl
                },
                "Ref1LblEng": {
                    "text": collectionData[i]["LabelReferenceNumber1EN"]
                },
                "Ref1LblThai": {
                    "text": collectionData[i]["LabelReferenceNumber1TH"]
                },
                "Ref2LblEng": {
                    "text": collectionData[i]["LabelReferenceNumber2EN"]
                },
                "Ref2LblThai": {
                    "text": collectionData[i]["LabelReferenceNumber2TH"]
                },
                "BillerNameEng": {
                    "text": collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "BillerNameThai": {
                    "text": collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                }
            }
            gblMasterBillerSelectIB.push(tempRecord);
        }
    }
}
//Added by Bhaskar - End
function getMyTopUpSelectListIB() {
    var inputParams = {
        IsActive: "1",
        BillerGroupType: gblGroupTypeTopup,
        flagBillerList: "IB"
            //clientDate: getCurrentDate()not working on IE 8
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("masterBillerInquiry", inputParams, startMyTopUpSelectListIBServiceAsyncCallback);
}

function startMyTopUpSelectListIBServiceAsyncCallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == "0") {
            var responseData = callBackResponse["MasterBillerInqRs"];
            if (gblTopupFromMenu) {
                frmIBMyTopUpsHome.show();
                gblTopupFromMenu = false;
            }
            if (responseData.length > 0) {
                frmIBMyTopUpsHome.lblErrorMsg.setVisibility(false);
                populateSelectTopUpCollectionDataToGlobalVariable(callBackResponse["MasterBillerInqRs"]);
                // getMyTopUpListIB(); //Calling customer Bill Inquiry  
                onMyTopUpSelectMoreDynamicIB(gblMasterTopupSelectIB);
                gblTopupLastSearchNoCat = gblMasterTopupSelectIB;
                sugTopUpCatChange();
                dismissLoadingScreenPopup();
            } else {
                var collectionData = [];
                setTopUpCollectionDataToGlobalVariable(collectionData);
                frmIBMyTopUpsHome.segSuggestedBillersList.removeAll();
                dismissLoadingScreenPopup();
                //alert(kony.i18n.getLocalizedString("ECGenericError"));
            }
        }
    } else {
        if (status == 300) {
            var collectionData = [];
            setTopUpCollectionDataToGlobalVariable(collectionData);
            frmIBMyTopUpsHome.segSuggestedBillersList.removeAll();
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("ECGenericError"));
        }
    }
}

function populateSelectTopUpCollectionDataToGlobalVariable(collectionData) {
    var billername = "";
    var ref1label = "";
    var ref2label = "";
    var tempRecord = [];
    gblMasterTopupSelectIB = [];
    if (kony.string.startsWith(gbllocaleIBBillerTopup, "en", true) == true) {
        billername = "BillerNameEN";
        ref1label = "LabelReferenceNumber1EN";
        ref2label = "LabelReferenceNumber2EN";
    } else if (kony.string.startsWith(gbllocaleIBBillerTopup, "th", true) == true) {
        billername = "BillerNameTH";
        ref1label = "LabelReferenceNumber1TH";
        ref2label = "LabelReferenceNumber2TH";
    }
    for (var i = 0; i < collectionData.length; i++) {
        if (collectionData[i]["BillerGroupType"] == gblGroupTypeTopup) {
            imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + collectionData[i]["BillerCompcode"] + "&modIdentifier=MyBillers";
            var tempRecord = {
                "lblSugBillerName": {
                    "text": collectionData[i][billername] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "btnSugBillerAdd": {
                    "text": "Add",
                    "skin": "btnIBaddsmall"
                },
                "BillerID": {
                    "text": collectionData[i]["BillerID"]
                },
                "BillerCompCode": {
                    "text": collectionData[i]["BillerCompcode"]
                },
                "Ref1Label": {
                    "text": collectionData[i][ref1label]
                },
                "EffDt": {
                    "text": collectionData[i]["EffDt"]
                },
                "BillerMethod": {
                    "text": collectionData[i]["BillerMethod"]
                },
                "BarcodeOnly": collectionData[i]["BarcodeOnly"],
                "BillerGroupType": collectionData[i]["BillerGroupType"],
                "ToAccountKey": collectionData[i]["ToAccountKey"],
                "IsFullPayment": collectionData[i]["IsFullPayment"],
                "Ref1Len": {
                    "text": collectionData[i]["Ref1Len"]
                },
                "BillerCategoryID": {
                    "text": collectionData[i]["BillerCategoryID"]
                },
                "imgSugBillerLogo": {
                    "src": imagesUrl
                },
                "Ref1LblEng": {
                    "text": collectionData[i]["LabelReferenceNumber1EN"]
                },
                "Ref1LblThai": {
                    "text": collectionData[i]["LabelReferenceNumber1TH"]
                },
                "BillerNameEng": {
                    "text": collectionData[i]["BillerNameEN"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                },
                "BillerNameThai": {
                    "text": collectionData[i]["BillerNameTH"] + " (" + collectionData[i]["BillerCompcode"] + ")"
                }
            }
            gblMasterTopupSelectIB.push(tempRecord);
        }
    }
}

function onAddBillTopUpNextIB() {
    var currForm = kony.application.getCurrentForm().id;
    if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
        checkBillTopUpTokenFlag();
    }
    if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        if (currForm == "frmIBMyBillersHome") {
            //To fix Defect DEF14665 
            frmIBMyBillersHome.btnConfirmBillerAdd.setVisibility(false);
            var len = document.getElementsByClassName("btnIBdelicon").length;
            for (var i = 0; i < len; i++) {
                document.getElementsByClassName("btnIBdelicon")[0].className = "btnIBDelNoSkin";
            }
            frmIBMyBillersHome.hbxBankDetails.setVisibility(false);
            frmIBMyBillersHome.hbxOTPsnt.setVisibility(false);
            frmIBMyBillersHome.hbxTokenEntry.setVisibility(true);
            //frmIBMyBillersHome.tbxToken.setFocus(true);
            frmIBMyBillersHome.hbxOTPEntry.setVisibility(false);
            frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
            frmIBMyBillersHome.lblToken.text = kony.i18n.getLocalizedString("keyToken");
            frmIBMyBillersHome.tbxToken.setFocus(true);
        }
        if (currForm == "frmIBMyTopUpsHome") {
            //To fix Defect DEF14665 
            frmIBMyTopUpsHome.btnConfirmBillerAdd.setVisibility(false);
            var len = document.getElementsByClassName("btnIBdelicon").length;
            for (var i = 0; i < len; i++) {
                document.getElementsByClassName("btnIBdelicon")[0].className = "btnIBDelNoSkin";
            }
            frmIBMyTopUpsHome.hbxBankDetails.setVisibility(false);
            frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(false);
            frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(true);
            //frmIBMyTopUpsHome.tbxToken.setFocus(true);
            frmIBMyTopUpsHome.hbxOTPEntry.setVisibility(false);
            frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
            frmIBMyTopUpsHome.lblToken.text = kony.i18n.getLocalizedString("keyToken");
            frmIBMyTopUpsHome.tbxToken.setFocus(true);
        }
    } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
        if (currForm == "frmIBMyBillersHome") {
            frmIBMyBillersHome.hbxBankDetails.setVisibility(true);
            frmIBMyBillersHome.txtotp.setFocus(true);
            frmIBMyBillersHome.hbxOTPsnt.setVisibility(true);
            frmIBMyBillersHome.hbxTokenEntry.setVisibility(false);
            frmIBMyBillersHome.hbxOTPEntry.setVisibility(true);
            frmIBMyBillersHome.txtotp.setFocus(true);
            frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyOTPRequest");
            frmIBMyBillersHome.lblToken.text = kony.i18n.getLocalizedString("keyOTP");
        }
        if (currForm == "frmIBMyTopUpsHome") {
            frmIBMyTopUpsHome.hbxBankDetails.setVisibility(true);
            frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(true);
            frmIBMyTopUpsHome.txtotp.setFocus(true);
            frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(false);
            frmIBMyTopUpsHome.hbxOTPEntry.setVisibility(true);
            frmIBMyTopUpsHome.txtotp.setFocus(true);
            frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyOTPRequest");
            frmIBMyTopUpsHome.lblToken.text = kony.i18n.getLocalizedString("keyOTP");
        }
        startBillTopUpOTPRequestBackground();
    }
}

function checkBillTopUpTokenFlag() {
    var inputParam = [];
    //inputParam["crmId"] = gblcrmId;
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, ibBillTopUpTokenFlagCallbackfunction);
}

function ibBillTopUpTokenFlagCallbackfunction(status, callbackResponse) {
    var currForm = kony.application.getCurrentForm().id;
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            dismissLoadingScreen();
            if (callbackResponse["deviceFlag"].length == 0) {
                //return
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus == '02') {
                    if (currForm == "frmIBMyBillersHome") {
                        frmIBMyBillersHome.hbxBankDetails.setVisibility(false);
                        frmIBMyBillersHome.hbxOTPsnt.setVisibility(false);
                        frmIBMyBillersHome.hbxTokenEntry.setVisibility(true);
                        frmIBMyBillersHome.hbxOTPEntry.setVisibility(false);
                        frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
                        frmIBMyBillersHome.lblToken.text = kony.i18n.getLocalizedString("keyToken");
                        frmIBMyBillersHome.tbxToken.setFocus(true);
                    }
                    if (currForm == "frmIBMyTopUpsHome") {
                        frmIBMyTopUpsHome.hbxBankDetails.setVisibility(false);
                        frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(false);
                        frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(true);
                        frmIBMyTopUpsHome.hbxOTPEntry.setVisibility(false);
                        frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
                        frmIBMyTopUpsHome.lblToken.text = kony.i18n.getLocalizedString("keyToken");
                        frmIBMyTopUpsHome.tbxToken.setFocus(true);
                    }
                    gblTokenSwitchFlag = true;
                } else {
                    if (currForm == "frmIBMyBillersHome") {
                        frmIBMyBillersHome.hbxBankDetails.setVisibility(true);
                        frmIBMyBillersHome.hbxOTPsnt.setVisibility(true);
                        frmIBMyBillersHome.hbxTokenEntry.setVisibility(false);
                        frmIBMyBillersHome.hbxOTPEntry.setVisibility(true);
                        frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyOTPRequest");
                        frmIBMyBillersHome.lblToken.text = kony.i18n.getLocalizedString("keyOTP");
                        frmIBMyBillersHome.txtotp.setFocus(true);
                    }
                    if (currForm == "frmIBMyTopUpsHome") {
                        frmIBMyTopUpsHome.hbxBankDetails.setVisibility(true);
                        frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(true);
                        frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(false);
                        frmIBMyTopUpsHome.hbxOTPEntry.setVisibility(true);
                        frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyOTPRequest");
                        frmIBMyTopUpsHome.lblToken.text = kony.i18n.getLocalizedString("keyOTP");
                        frmIBMyTopUpsHome.txtotp.setFocus(true);
                    }
                    gblTokenSwitchFlag = false;
                }
            } else {
                if (currForm == "frmIBMyBillersHome") {
                    frmIBMyBillersHome.hbxBankDetails.setVisibility(true);
                    frmIBMyBillersHome.hbxOTPsnt.setVisibility(true);
                    frmIBMyBillersHome.hbxTokenEntry.setVisibility(false);
                    frmIBMyBillersHome.hbxOTPEntry.setVisibility(true);
                    frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyOTPRequest");
                    frmIBMyBillersHome.lblToken.text = kony.i18n.getLocalizedString("keyOTP");
                    frmIBMyBillersHome.txtotp.setFocus(true);
                }
                if (currForm == "frmIBMyTopUpsHome") {
                    frmIBMyTopUpsHome.hbxBankDetails.setVisibility(true);
                    frmIBMyTopUpsHome.hbxOTPsnt.setVisibility(true);
                    frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(false);
                    frmIBMyTopUpsHome.hbxOTPEntry.setVisibility(true);
                    frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyOTPRequest");
                    frmIBMyTopUpsHome.lblToken.text = kony.i18n.getLocalizedString("keyOTP");
                    frmIBMyTopUpsHome.txtotp.setFocus(true);
                }
                startBillTopUpOTPRequestBackground();
                //dismissLoadingScreen();
            }
        }
    }
}

function preShowForBillers() {
    frmIBMyBillersHome.segBillersList.setVisibility(true);
    frmIBMyBillersHome.lblMyBills.setVisibility(true);
    frmIBMyBillersHome.hbxMore.setVisibility(true);
    frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
    isSelectBillerEnabled = false;
    frmIBMyBillersHome.tbxSearch.text = "";
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (!isIE8) {
        frmIBMyBillersHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
    }
    gblTopUpMore = 0;
    gblCustTopUpMore = 0;
    search = "";
    frmIBMyBillersHome.lblNoBills.setVisibility(false);
    frmIBMyBillersHome.lblErrorMsg.setVisibility(false);
    frmIBMyBillersHome.hbxTokenEntry.setVisibility(false);
    gblRetryCountRequestOTP = 0;
    frmIBMyBillersHome.imgTMBLogo.setVisibility(true);
    //frmIBMyBillersHome.hbxTMBLogo.setVisibility(true);
    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
    frmIBMyBillersHome.lblMyBills.setVisibility(false);
    frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyBillersHome.imgArrowAddBiller.setVisibility(false);
    frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
    frmIBMyBillersHome.txtotp.maxTextLength = 6;
    getLocaleBillerIB();
}

function postShowForBillers() {
    addIBMenu()
    segAboutMeLoad();
    pagespecificSubMenu();
    startDisplayBillerCategoryServiceIB()
    getMyBillListIB()
    frmIBMyBillersHome.lblBankRef.setVisibility(false);
    frmIBMyBillersHome.lblBankRefValue.setVisibility(false);
    frmIBMyBillersHome.lblOTPMsg.setVisibility(false);
    frmIBMyBillersHome.lblOTPMobileNo.setVisibility(false);
    frmIBMyBillersHome.txtotp.text = "";
    callServiceFlag = false;
}

function postShowForTopups() {
    addIBMenu();
    segAboutMeLoad();
    pagespecificSubMenu();
    startDisplayTopUpCategoryServiceIB();
    getMyTopUpListIB();
    frmIBMyTopUpsHome.lblBankRef.setVisibility(false);
    frmIBMyTopUpsHome.lblBankRefValue.setVisibility(false);
    frmIBMyTopUpsHome.lblOTPMsg.setVisibility(false);
    frmIBMyTopUpsHome.lblOTPMobileNo.setVisibility(false);
    frmIBMyTopUpsHome.txtotp.text = "";
    callTopupServiceFlag = false;
}

function preShowForTopups() {
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(true);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
    frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyTopUpsHome.txtotp.maxTextLength = 6;
    frmIBMyTopUpsHome.lblNoBills.setVisibility(false);
    frmIBMyTopUpsHome.lblErrorMsg.setVisibility(false);
    frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(false);
    gblRetryCountRequestOTP = 0;
    frmIBMyTopUpsHome.segBillersList.setVisibility(true);
    frmIBMyTopUpsHome.lblMyBills.setVisibility(true);
    frmIBMyTopUpsHome.hbxMore.setVisibility(true);
    frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
    isSelectTopupEnabled = false;
    frmIBMyTopUpsHome.tbxSearch.text = "";
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    //if(!isIE8){
    frmIBMyTopUpsHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
    //}
    gblTopUpMore = 0;
    gblCustTopUpMore = 0;
    search = "";
    getLocaleBillerIB();
}

function switchScheduleDetailsIB(dateText, repeatText) {
    if (kony.string.equalsIgnoreCase(gblClicked, "Once") || kony.string.equalsIgnoreCase(gblClicked, "once")) {
        var dateArray = dateText.split(" ");
        var date1 = dateArray[0];
        var date2 = dateArray[2];
        dateText = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2;
        repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyOnce");
    } else if (kony.string.equalsIgnoreCase(gblrepeat.toString(), "3")) {
        dateText = dateText;
        if (kony.string.equalsIgnoreCase(gblClicked, "Daily")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Weekly")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Monthly")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly");
        } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatBP, "Yearly")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly");
        }
    }
    if (kony.string.equalsIgnoreCase(gblrepeat.toString(), "1") || kony.string.equalsIgnoreCase(gblrepeat.toString(), "2")) {
        var dateArray = dateText.split(" ");
        var date1 = dateArray[0];
        var date2 = dateArray[2];
        dateText = date1 + " " + kony.i18n.getLocalizedString("keyTo") + " " + date2;
        var repeatArray = repeatText.split(" ");
        var numOfTimes = repeatArray[2];
        if (kony.string.equalsIgnoreCase(gblClicked, "Daily")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyDaily") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
        } else if (kony.string.equalsIgnoreCase(gblClicked, "Weekly")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyWeekly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
        } else if (kony.string.equalsIgnoreCase(gblClicked, "Monthly")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyMonthly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
        } else if (kony.string.equalsIgnoreCase(gblClicked, "Yearly")) {
            repeatText = kony.i18n.getLocalizedString("keyRepeat") + " " + kony.i18n.getLocalizedString("keyYearly") + " " + numOfTimes + " " + kony.i18n.getLocalizedString("keyTimesMB");;
        }
    }
    return [dateText, repeatText];;
}