/*frmDreamCalculator*/

function frmDreamCalculatorMenuPreshow() {
    if (gblCallPrePost) {
        frmDreamCalculator.lblMnthlySavingVal.skin = "txtNormalBG"
        frmDreamCalculator.lblMnthTODReamVal.skin = "txtNormalBG"
        frmDreamCalculator.lblMnthlySaving.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
        frmDreamCalculator.lblTargetAmount.text = frmDreamCalculator.lblTargetAmount.text.replace(":", "");
        var deviceInfo = kony.os.deviceInfo();
        var deviceHght = deviceInfo["deviceHeight"];
        if (deviceHght > "799") {
            frmDreamCalculator.hbox50285458120.margin = [0, 80, 0, 0]
        }
    }
}

function frmDreamCalculatorMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmDreamSavingEdit*/

function frmDreamSavingEditMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        frmDreamSavingEdit.lblDreamDescVal.skin = "txtNormalBG"
        frmDreamSavingEdit.lblNicknameVal.skin = "txtNormalBG"
        frmDreamSavingEdit.lblSetSavingVal.skin = "txtNormalBG"
        frmDreamSavingEdit.lblTargetAmntVal.skin = "txtNormalBG"
        frmDreamSavingEdit.button86682510925150.setEnabled(true);
        frmDreamSavingEdit.button86682510925150.skin = "btnCalculate";
        frmDreamSavingEdit.scrollboxMain.scrollToEnd();
        if (kony.os.deviceInfo().name == "android") {
            frmDreamSavingEdit.segSlider.viewConfig = {
                coverflowConfig: {
                    projectionAngle: 60,
                    rowItemRotationAngle: 45,
                    spaceBetweenRowItems: 0,
                    rowItemWidth: 80,
                    isCircular: true
                }
            }
        }
        DisableFadingEdges.call(this, frmDreamSavingEdit);
    }
}

function frmDreamSavingEditMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmDreamSavingMB*/

function frmDreamSavingMBMenuPreshow() {
    if (gblCallPrePost) {
        isMenuShown = false;
        gblflagDreamMB = "false";
        //var a= frmDreamSavingMB.lblTargetAmount.text.replace(":", "");
        //frmDreamSavingMB.lblTargetAmount.text = a;
        //var b= frmDreamSavingMB.label866795318717733.text.replace(":", "");
        //frmDreamSavingMB.label866795318717733.text = b;
        barGraph.call(this);
        frmDreamSavingMB.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmDreamSavingMB);
        frmDreamSavingMaintainPreShow.call(this);
    }
}

function frmDreamSavingMBMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmFATCAQuestionnaire1*/

function frmFATCAQuestionnaire1MenuPreshow() {
    if (gblCallPrePost) {
        //ResetTransferHomePage.call(this);
        if (gblshotcutToACC) {} else {
            ResetTransferHomePage.call(this);
        }
    }
}

function frmFATCAQuestionnaire1MenuPostshow() {
    if (gblCallPrePost) {
        frmFATCAQuestionnaire1.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        DisableFadingEdges.call(this, frmFATCAQuestionnaire1);
    }
    assignGlobalForMenuPostshow();
}

/*frmFATCATnC*/

function frmFATCATnCMenuPreshow() {
    if (gblCallPrePost) {
        frmFATCATnC.btnAgreeCheck.skin = "btnCheck"
        frmFATCATnC.btnAgreeCheck.focusSkin = "btnCheck"
    }
}

function frmFATCATnCMenuPostshow() {
    if (gblCallPrePost) {
        frmFATCATnC.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        frmFATCATnCPreShow.call(this);
        DisableFadingEdges.call(this, frmFATCATnC);
    }
    assignGlobalForMenuPostshow();
}

/*frmOccupationInfo*/

function frmOccupationInfoMenuPreshow() {
    if (gblCallPrePost) {
        frmCheckOccupationInfoPreShow.call(this);
    }
}

function frmOccupationInfoMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenAccountNSConfirmation*/

function frmOpenAccountNSConfirmationMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenAccountNSConfirmation.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        var deviceHght = deviceInfo["deviceHeight"];
        if (deviceInfo.name == "android") {
            kony.print("Inside Android")
            if (deviceHght > 1000) {
                frmOpenAccountNSConfirmation.hbxCnfrmCncl.margin = [0, 15, 0, 0];
            }
        }
        //Code for personalized banner
        try {
            frmOpenAccountNSConfirmation.hbxAdv.setVisibility(false);
            frmOpenAccountNSConfirmation.hbxCommon.setVisibility(false);
            frmOpenAccountNSConfirmation.imgOpnActNSAdd.src = "";
            frmOpenAccountNSConfirmation.imgTwo.src = "";
            frmOpenAccountNSConfirmation.gblBrwCmpObject.handleRequest = "";
            frmOpenAccountNSConfirmation.gblBrwCmpObject.htmlString = "";
            frmOpenAccountNSConfirmation.gblVbxCmp.remove(gblBrwCmpObject);
            frmOpenAccountNSConfirmation.hbxAdv.remove(gblVbxCmp);
            frmOpenAccountNSConfirmation.hbxCommon.remove(gblVbxCmp);
        } catch (e) {}
        DisableFadingEdges.call(this, frmOpenAccountNSConfirmation);
        if (flowSpa) {
            swipeEnable = true;
            var setupTblTap = {
                fingers: 1,
                swipedistance: 50,
                swipevelocity: 75
            }
            swipeGesture = frmOpenAccountNSConfirmation.sboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
            swipeGesture = frmOpenAccountNSConfirmation.scrollboxLeft.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
        }
    }
}

function frmOpenAccountNSConfirmationMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenAccountNSConfirmationCalendar*/

function frmOpenAccountNSConfirmationCalendarMenuPreshow() {
    if (gblCallPrePost) {
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        frmOpenAccountNSConfirmationCalendar.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmOpenAccountNSConfirmationCalendar);
    }
}

function frmOpenAccountNSConfirmationCalendarMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenAccTermDeposit*/

function frmOpenAccTermDepositMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenAccTermDeposit.imgHeaderMiddle.src = "arrowtop.png"
        frmOpenAccTermDeposit.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        if (flowSpa) {
            frmOpenAccTermDeposit.hbxSliderToTD.setVisibility(false);
        }
        DisableFadingEdges.call(this, frmOpenAccTermDeposit);
    }
}

function frmOpenAccTermDepositMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenAccTermDepositToSpa*/

function frmOpenAccTermDepositToSpaMenuPreshow() {
    if (gblCallPrePost) {
        setDataforOpenToActs(frmOpenAccTermDepositToSpa.segToTDActSpa)
    }
}

function frmOpenAccTermDepositToSpaMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenAcDreamSaving*/

function frmOpenAcDreamSavingMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenAcDreamSaving.imgHeaderMiddle.src = "arrowtop.png";
        frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtFocusBG
        frmOpenAcDreamSaving.txtODMyDream.skin = txtFocusBG
        frmOpenAcDreamSaving.txtODNickNameVal.skin = txtNormalBG;
        frmOpenAcDreamSaving.txtODTargetAmt.skin = txtNormalBG;
        frmOpenAcDreamSaving.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        if (kony.i18n.getCurrentLocale() == "en_US") {
            frmOpenAcDreamSaving.lblAccountNicknameValue.text = gblFinActivityLogOpenAct["prodNameEN"];
        } else {
            frmOpenAcDreamSaving.lblAccountNicknameValue.text = gblFinActivityLogOpenAct["prodNameTH"];
        }
        frmOpenAcDreamSaving.lblODMnthSavAmt.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
        if (gblDeviceInfo["name"] == "android") {
            frmOpenAcDreamSaving.segSliderOpenDream.viewConfig = {
                coverflowConfig: {
                    projectionAngle: 60,
                    rowItemRotationAngle: 45,
                    spaceBetweenRowItems: 0,
                    rowItemWidth: 80,
                    isCircular: true
                }
            }
        }
        DisableFadingEdges.call(this, frmOpenAcDreamSaving);
    }
}

function frmOpenAcDreamSavingMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenActDSAck*/

function frmOpenActDSAckMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenActDSAck.scrollboxMain.scrollToEnd();
        if (!(LocaleController.isFormUpdatedWithLocale(frmOpenActDSAck.id))) {
            frmOpenActDSAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne")
            frmOpenActDSAck.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi")
            LocaleController.updatedForm(frmOpenActDSAck.id);
        }
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        if (flowSpa) {
            swipeEnable = true;
            var setupTblTap = {
                fingers: 1,
                swipedistance: 50,
                swipevelocity: 75
            }
            swipeGesture = frmOpenActDSAck.sboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
            swipeGesture = frmOpenActDSAck.scrollboxLeft.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
        }
        //code for personalized banner display
        try {
            frmOpenActDSAck.hbxAdv.setVisibility(false);
            frmOpenActDSAck.imgOpnActDSAdd.src = "";
            frmOpenActDSAck.gblBrwCmpObject.handleRequest = "";
            frmOpenActDSAck.gblBrwCmpObject.htmlString = "";
            frmOpenActDSAck.gblVbxCmp.remove(gblBrwCmpObject);
            frmOpenActDSAck.hbxAdv.remove(gblVbxCmp);
        } catch (e) {}
        DisableFadingEdges.call(this, frmOpenActDSAck);
    }
}

function frmOpenActDSAckMenuPostshow() {
    if (gblCallPrePost) {
        campaginService.call(this, "imgOpnActDSAdd", "frmOpenActDSAck", "M");
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenActDSAckCalendar*/

function frmOpenActDSAckCalendarMenuPreshow() {
    if (gblCallPrePost) {
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        frmOpenActDSAckCalendar.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmOpenActDSAckCalendar);
    }
}

function frmOpenActDSAckCalendarMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenActDSConfirm*/

function frmOpenActDSConfirmMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenActDSConfirm.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmOpenActDSConfirm);
    }
}

function frmOpenActDSConfirmMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenActSelProd*/

function frmOpenActSelProdMenuPreshow() {
changeStatusBarColor();
    if (gblCallPrePost) {
        frmOpenActSelProd.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
        frmOpenActSelProd.label47505874734422.text = kony.i18n.getLocalizedString("keyopenselectCategory");
        frmOpenActSelProd.scrollboxMain.scrollToEnd();
        frmOpenActSelProd.label47505874734002.text = kony.i18n.getLocalizedString("forUse");
        frmOpenActSelProd.label47505874734014.text = kony.i18n.getLocalizedString("forSave");
        frmOpenActSelProd.label47505874734008.text = kony.i18n.getLocalizedString("TermDepositMB");
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmOpenActSelProd);
        //frmOpenActSelProdPreshow();
        frmOpenActSelProdPreShow();
    }
}

function frmOpenActSelProdMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenActTDAck*/

function frmOpenActTDAckMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenActTDAck.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        if (flowSpa) {
            swipeEnable = true;
            var setupTblTap = {
                fingers: 1,
                swipedistance: 50,
                swipevelocity: 75
            }
            swipeGesture = frmOpenActTDAck.sboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
            swipeGesture = frmOpenActTDAck.scrollboxLeft.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
        }
        //code for personalized banner display
        try {
            frmOpenActTDAck.hbxAdv.setVisibility(false);
            frmOpenActTDAck.imgOpnActTDAdd.src = "";
            frmOpenActTDAck.gblBrwCmpObject.handleRequest = "";
            frmOpenActTDAck.gblBrwCmpObject.htmlString = "";
            frmOpenActTDAck.gblVbxCmp.remove(gblBrwCmpObject);
            frmOpenActTDAck.hbxAdv.remove(gblVbxCmp);
        } catch (e) {}
        DisableFadingEdges.call(this, frmOpenActTDAck);
    }
}

function frmOpenActTDAckMenuPostshow() {
    if (gblCallPrePost) {
        campaginService.call(this, "imgOpnActTDAdd", "frmOpenActTDAck", "M");
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenActTDAckCalendar*/

function frmOpenActTDAckCalendarMenuPreshow() {
    if (gblCallPrePost) {
        gblIndex = -1
        isMenuShown = false;
        isSignedUser = true;
        frmOpenActTDAckCalendar.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmOpenActTDAckCalendar);
    }
}

function frmOpenActTDAckCalendarMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenActTDConfirm*/

function frmOpenActTDConfirmMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenActTDConfirm.scrollboxMain.scrollToEnd();
        gblIndex = -1;
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmOpenActTDConfirm);
    }
}

function frmOpenActTDConfirmMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenProdDetnTnC*/

function frmOpenProdDetnTnCMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenProdDetnTnC.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmOpenProdDetnTnC);
    }
}

function frmOpenProdDetnTnCMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenProdEditAddress*/

function frmOpenProdEditAddressMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenProdEditAddress.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmOpenProdEditAddress);
        frmOpenProdEditAddressPreShow.call(this);
    }
}

function frmOpenProdEditAddressMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenProdEditAddressConfirm*/

function frmOpenProdEditAddressConfirmMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenProdEditAddressConfirm.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmOpenProdEditAddressConfirm);
    }
}

function frmOpenProdEditAddressConfirmMenuPostshow() {
    if (gblCallPrePost) {
        frmOpenProdEditAddressConfirmPreShow.call(this);
    }
    assignGlobalForMenuPostshow();
}

/*frmOpenProdViewAddress*/

function frmOpenProdViewAddressMenuPreshow() {
    if (gblCallPrePost) {
        frmOpenProdViewAddress.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmOpenProdViewAddress);
        frmOpenProdViewAddressPreShow.call(this);
    }
}

function frmOpenProdViewAddressMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmOpnActSelAct*/

function frmOpnActSelActMenuPreshow() {
    if (gblCallPrePost) {
        frmOpnActSelAct.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmOpnActSelAct);
    }
}

function frmOpnActSelActMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}

/*frmTimeDepositAccoutDetails*/

function frmTimeDepositAccoutDetailsMenuPreshow() {
    if (gblCallPrePost) {
        //..
    }
}

function frmTimeDepositAccoutDetailsMenuPostshow() {
    if (gblCallPrePost) {
        //..
    }
    assignGlobalForMenuPostshow();
}




/*frmMBSavingsCareAddBeneficiary.kl*/

function frmMBSavingsCareAddBeneficiaryMenuPreshow() {
    if (gblCallPrePost) {
	    /* 
	frmMBSavingsCareAddBeneficiary.txtfirstName.placeholder=removeColonFromEnd(kony.i18n.getLocalizedString("keyFirstName"));
	frmMBSavingsCareAddBeneficiary.txtlastName.placeholder=removeColonFromEnd(kony.i18n.getLocalizedString("keyLastName"));
	frmMBSavingsCareAddBeneficiary.lblrelation.text=kony.i18n.getLocalizedString("keyOpenRelation");
	
	 */
    preShowAddBenefic.call(this);
    }
}

function frmMBSavingsCareAddBeneficiaryMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}



/*frmMBSavingsCareAddNickName.kl*/

function frmMBSavingsCareAddNickNameMenuPreshow() {
    if (gblCallPrePost) {
		preshowfrmMBSavingsCareAddNickName.call(this);
    }
}

function frmMBSavingsCareAddNickNameMenuPostshow() {
    if (gblCallPrePost) {
		 postshowSavingsCare.call(this);
    }
    assignGlobalForMenuPostshow();
}


/*frmMBSavingsCareComplete.kl*/

function frmMBSavingsCareCompleteMenuPreshow() {
	changeStatusBarColor();
    if (gblCallPrePost) {
    	showMBOpenSavingscareComplete.call(this);
	    /* 
		setSegdatacompletescreen.call(this);
       */
    }
}

function frmMBSavingsCareCompleteMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}



/*frmMBSavingsCareConfirmation.kl*/

function frmMBSavingsCareConfirmationMenuPreshow() {
	changeStatusBarColor();
    if (gblCallPrePost) {
	    preshowfrmSCConfirmation.call(this);
		    /* 
	setSegdata.call(this);
	
	 */
    }
}

function frmMBSavingsCareConfirmationMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}




/*frmMBSavingsCareContactInfo.kl*/

function frmMBSavingsCareContactInfoMenuPreshow() {
    if (gblCallPrePost) {
		frmMBSavingsCareContactInfoPreShow.call(this);
    }
}

function frmMBSavingsCareContactInfoMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}



/*frmMBSavingsCareOccupationInfo.kl*/

function frmMBSavingsCareOccupationInfoMenuPreshow() {
    if (gblCallPrePost) {
		frmMBSavingsCareOccupationInfoPreShow.call(this);
    }
}

function frmMBSavingsCareOccupationInfoMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}



/*frmMBSavingsCareProdBrief.kl*/

function frmMBSavingsCareProdBriefMenuPreshow() {
    if (gblCallPrePost) {
	    //frmMBSavingsCareProdBrief.scrollboxMain.scrollToEnd();
	    isMenuShown = false;
	    isSignedUser = true;
	    //DisableFadingEdges.call(this, frmMBSavingsCareProdBrief);
	    frmMBSavingsCareProdBriefPreShow.call(this);
    }
}

function frmMBSavingsCareProdBriefMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}

/*frmMBSavingsCareTnC.kl*/

function frmMBSavingCareTnCMenuPreshow() {
	changeStatusBarColor();
    if (gblCallPrePost) {
    	
	    /*frmMBSavingsCareTnC.scrollboxMain.scrollToEnd();
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmMBSavingsCareTnC);
	    frmMBSavingsCareTnCLocale.call(this);*/
	   if(gblSavingsCareFlow=="MyAccountFlow" || gblSavingsCareFlow=="AccountDetailsFlow" || flow=="myAccount"){
			frmMBSavingCareTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyMyAccountIB");
		}else{
				frmMBSavingCareTnC.lblHdrTxt.text = kony.i18n.getLocalizedString("keyOpenAcc");
		}
		frmMBSavingCareTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString("keyTermsNConditions");
		frmMBSavingCareTnC.btnback.text= kony.i18n.getLocalizedString("Back");
		frmMBSavingCareTnC.btnnext.text= kony.i18n.getLocalizedString("keyAgreeButton");
		frmMBSavingCareTnC.flxSaveCamEmail.setVisibility(false);
		frmMBSavingCareTnC.flexBodyScroll.minHeight="100%";
		frmMBSavingCareTnC.btnRight.skin="btnRightShare";
		frmMBSavingCareTnC.flexBodyScroll.setContentOffset({"x":"0dp", "y":"0dp"});
		//frmMBSavingCareTnC.flexBodyScroll.scrollToWidget(frmMBSavingCareTnC.lblOpenActDescSubTitle);
		
    }
}

function frmMBSavingCareTnCMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
    frmMBSavingCareTnC.flexBodyScroll.setContentOffset({"x":"0dp", "y":"0dp"});
    //frmMBSavingCareTnC.flexBodyScroll.scrollToWidget(frmMBSavingCareTnC.lblOpenActDescSubTitle);
}


/*frmOpenAccountSavingCareMB.kl*/

function frmOpenAccountSavingCareMBMenuPreshow() {
    if (gblCallPrePost) {
	    frmOpenAccountSavingCareMB.scrollboxMain.scrollToEnd();
	    DisableFadingEdges.call(this, frmOpenAccountSavingCareMB);
    }
}

function frmOpenAccountSavingCareMBMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}


/*frmOpenActSavingCareCnfNAck.kl*/

function frmOpenActSavingCareCnfNAckMenuPreshow() {
    if (gblCallPrePost) {
	    frmOpenActSavingCareCnfNAck.scrollboxMain.scrollToEnd();
	    if (!(LocaleController.isFormUpdatedWithLocale(frmOpenActSavingCareCnfNAck.id))) {
	        frmOpenActSavingCareCnfNAck.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi")
	        LocaleController.updatedForm(frmOpenActSavingCareCnfNAck.id);
	    }
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    if (flowSpa) {
	        swipeEnable = true;
	        var setupTblTap = {
	            fingers: 1,
	            swipedistance: 50,
	            swipevelocity: 75
	        }
	        swipeGesture = frmOpenActSavingCareCnfNAck.sboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
	        swipeGesture = frmOpenActSavingCareCnfNAck.scrollboxLeft.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
	    }
	    /* 
	 
	
	    */
	    DisableFadingEdges.call(this, frmOpenActSavingCareCnfNAck);
    }
}

function frmOpenActSavingCareCnfNAckMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}



/*frmOpenActSavingCareCnfNAckCalendar.kl*/

function frmOpenActSavingCareCnfNAckCalendarMenuPreshow() {
    if (gblCallPrePost) {
	    frmOpenActSavingCareCnfNAckCalendar.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmOpenActSavingCareCnfNAckCalendar);
    }
}

function frmOpenActSavingCareCnfNAckCalendarMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}



/*frmMBSavingsCareRelationshipBeneficiary.kl*/

function frmMBSavingsCareRelationshipBeneficiaryMenuPreshow() {
    if (gblCallPrePost) {
		//preshowfrmRelationship();
    }
}

function frmMBSavingsCareRelationshipBeneficiaryMenuPostshow() {
    if (gblCallPrePost) {

    }
    assignGlobalForMenuPostshow();
}
