function loginPProcessCallBack(resulttable){
		//crmprofileinq result set
		displayAnnoucementtoUser = false;
		if (resulttable["opstatuscrm"] == 0) {
			gblFBCode=resulttable["facebookId"];
			
			//MIB-1207
			if(resulttable["setyourID"] != undefined && resulttable["setyourID"] != null){
				gblShowAnyIDRegistration = resulttable["setyourID"];		// value taken from tmb.properties file
				//gblShowAnyIDRegistration =false;
			}
			if(resulttable["anyIdAnnouncementCount"] != undefined && resulttable["anyIdAnnouncementCount"] != null){
				gblAnyIdAnnouceCount = parseInt(resulttable["anyIdAnnouncementCount"]);  // value taken from tmb.properties file
			}
			// retriving the count from store
			//if(isNotBlank(kony.store.getItem("AnyIdAnnoceShowedCount"))){
//				gblAnyIdAnnoceShowedCount=kony.store.getItem("AnyIdAnnoceShowedCount");
//				gblAnyIdAnnoceShowedCount=parseInt(gblAnyIdAnnoceShowedCount);
//			}
//			else
//			{
//				gblAnyIdAnnoceShowedCount=gblAnyIdCount;
//			}
			if(resulttable["registerWithAnyId"] != undefined && resulttable["registerWithAnyId"] != null){
				gblRegisterWithAnyId = resulttable["registerWithAnyId"]; 
				// posible values N or Y
				// user should not register with any id, and announcement showed count should be less than max count.
				//if(gblRegisterWithAnyId == "N" && (gblAnyIdAnnoceShowedCount < gblAnyIdAnnouceCount)){
				if(gblRegisterWithAnyId == "N"){
					//TODO show the frmRegAnyIdAnnouncement form and handle the navigations
					displayAnnoucementtoUser=true;
				}
				else
				{
					displayAnnoucementtoUser=false;
				}
			}
			
			//below code is added to implement MIB-945
			gblIsNewOffersExists = false;
			if(undefined != resulttable["unreadMyOffers"] && null != resulttable["unreadMyOffers"]){
				//unreadMyOffers is Y means user has new offers
		   		//alert("unreadMyOffers : "+resulttable["unreadMyOffers"]);
		   		if("Y" == resulttable["unreadMyOffers"]){
					gblIsNewOffersExists = true;		   		 
		   		}
			}
			
			if (resulttable["errCode"] == "XPErr0001") {
			
					//showFPFlex(false);
				kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
				isUserStatusActive=false;
				onClickChangeTouchIcon();	
				popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
				popupAccesesPinLocked.btnPopupConfCancel.text= kony.i18n.getLocalizedString("keyCancelButton");
				popupAccesesPinLocked.btnCallTMB.text= kony.i18n.getLocalizedString("keyCall");
				popupAccesesPinLocked.show();
				//showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
                resetValues();
				kony.application.dismissLoadingScreen();
				return false;
			} else if (resulttable["errCode"] == "XPErr0002") {
				 onClickChangeTouchIcon();
				 kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
				 isUserStatusActive=false;
			     popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
			     popupAccesesPinLocked.btnPopupConfCancel.text= kony.i18n.getLocalizedString("keyCancelButton");
				 popupAccesesPinLocked.btnCallTMB.text= kony.i18n.getLocalizedString("keyCall");
			     popupAccesesPinLocked.show();
				//showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
                resetValues();
   				kony.application.dismissLoadingScreen();
				return false;
			} else {
				gblCustomerStatus = resulttable["ebCustomerStatusId"]
				gblIBFlowStatus = resulttable["ibUserStatusIdTr"]
				gblMBFlowStatus = resulttable["mbFlowStatusIdRs"]
				gblUserLockStatusMB = resulttable["mbFlowStatusIdRs"];
				//updating the user locked status to globalvariable "gblUserLockStatusMB" 		

                 if(gblSuccess == true && (gblUserLockStatusMB == "05" || gblUserLockStatusMB == "06")){
                 	 onClickChangeTouchIcon();
                 	 kony.print("FPRINT SETTING isUserStatusActive TO FALSE");
                 	 isUserStatusActive=false;
                     popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
                     popupAccesesPinLocked.btnPopupConfCancel.text= kony.i18n.getLocalizedString("keyCancelButton");
				     popupAccesesPinLocked.btnCallTMB.text= kony.i18n.getLocalizedString("keyCall");
			         popupAccesesPinLocked.show();
			         resetValues();
                   //showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
                   return false;
                 }
                 
	
				var list;
				
				if (resulttable["languageCd"] != null) {
					if (resulttable["languageCd"] == "TH") {
						list = {
							appLocale: "th_TH"
						}
						setLocaleTH();
					} else {
						list = {
							appLocale: "en_US"
						}
						setLocaleEng();					
					}
				} else {
					list = {
						appLocale: "en_US"
					}
					setLocaleEng();						
				}
				
				kony.store.removeItem("curAppLocale");
				kony.store.setItem("curAppLocale", list);
				
				var getcurAppLocale = kony.store.getItem("curAppLocale");
				if (getcurAppLocale["appLocale"] != null) {
					if (getcurAppLocale["appLocale"] == "en_US") {
						//setLocaleEng();
					} else if (getcurAppLocale["appLocale"] == "th_TH") {
						//setLocaleTH();
					}
				}
				if (resulttable["languageCd"] == null) {
					if(getcurAppLocale["appLocale"] != null){
						kony.i18n.setCurrentLocaleAsync(getcurAppLocale["appLocale"], onsuccess(), onfailure(), "");
					}
					
				}
				//partyinq result set
				if (resulttable["opstatusPartyInq"] == 0) {
					gblCustomerName = resulttable["customerName"];
					gblCustomerNameTh=resulttable["customerNameTH"];
					//for (var i = 0; i < resulttable["ContactNums"].length; i++) {
						//if (resulttable["ContactNums"][i]["PhnType"] == 'Mobile')
							gblPHONENUMBER = resulttable["PhnNum"];
							gblCustomerIDType=resulttable["customerIDType"];
							if(gblCustomerIDType=="CI")
							{
								gblCustomerIDValue=resulttable["customerIDValue"];
							}
							
					//}
					//frmAccountSummaryLanding.lblAccntHolderName.text = resulttable["customerName"];
					cleanPreorPostForms();
					if(gblPushNotificationFlag){
						GBL_Fatca_Flow = "pushnote";
						FatcaFlow(resulttable);
					}
					else{
						
						//Login composite service will return the messages count
						//unreadInboxMessagesMBLogin();
						if(resulttable["statusInboxUnread"] == "1"){
							gblUnreadCount = "0";
						}
						else if(resulttable["statusInboxUnread"] == "0"){
							gblUnreadCount = resulttable["unreadInboxUnread"];
						}
						else{
							gblUnreadCount = "0";
						}
						if(resulttable["statusMessagesUnread"] == "1"){
							gblMessageCount = "0";
						}
						else if(resulttable["statusMessagesUnread"] == "0"){
							gblMessageCount = resulttable["messagesUnread"];
						}
						else{
							gblMessageCount = "0";
						}
						var badgeCount =  parseInt(gblUnreadCount) + parseInt(gblMessageCount);
						gblMyInboxTotalCountMB=badgeCount;
						if(kony.string.equalsIgnoreCase("iphone", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPad", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPod touch", gblDeviceInfo.name)){
							kony.application.setApplicationBadgeValue(badgeCount);
						}
						GBL_Fatca_Flow = "login";
						FatcaFlow(resulttable);
						//callCustomerAccountService();
					}
				} else {
					alert(" " + resulttable["errMsg"]);
					kony.application.dismissLoadingScreen();
					return false;
				}
			}
		} else {
			alert(" " + resulttable["errMsg"]);
			// Hard coding alert msg  for debugging prod issue 51680 - Unknown error while Re activation of User.
			// As the issue is occuring intermediate, to debug and capture the logs this alert msg is coded.
			alert("Sorry for inconvinience caused");
			kony.application.dismissLoadingScreen();
			return false;
		}
	
}

function FatcaFlow(resulttable)
{
kony.print("FPRINT: IN FatcaFlow");
	TMBUtil.DestroyForm(frmFATCAQuestionnaire1);
	TMBUtil.DestroyForm(frmFATCATnC);
	gblFATCAAnswers = {};
	FatcaQues_EN = {};
	FatcaQues_TH = {};
	//GLOBAL_FATCA_MAX_SKIP_COUNT = 5;
	gblFATCASkipCounter = parseInt(resulttable["FATCASkipCounter"]);
	gblLoginType = "";
	//resulttable["FatcaFlag"] = "9";	//Comment this line when service response comes
	if(resulttable["FatcaFlag"] == "0") {
		dismissLoadingScreen();
		gblFATCAUpdateFlag = "0";
		showFACTAInfo();
	}
	else if(resulttable["FatcaFlag"] == "8" || resulttable["FatcaFlag"] == "9"
		|| resulttable["FatcaFlag"] == "P" || resulttable["FatcaFlag"] == "R" || resulttable["FatcaFlag"] == "X") {
		dismissLoadingScreen();
		gblFATCAUpdateFlag = "Z";
		setFATCAMBGoToBranchInfoMessage();
	}
	else /*if(resulttable["FATCAFlag"] == "N" || resulttable["FATCAFlag"] == "I" || resulttable["FATCAFlag"] == "U") */{
		showLoadingScreen();
		kony.print("FPRINT: Calling LoginProcessServExecMB");
		LoginProcessServExecMB("");
	}
}

function LoginProcessServExecMB(upgradeSkip){
kony.print("FPRINT: Calling getValidTouchCounter");
	touchflag = getValidTouchCounter();
	kony.print("FPRINT: touchflag="+touchflag);
	kony.print("FPRINT: gblNewCrmId="+gblNewCrmId);
	//We show frmRegistrationPopup for new CrmId also.
	if(touchflag && gblTouchStatus == "N" && gblActivationFlow == true){
		isSignedUser = true;
		if(gblDeviceInfo["name"] == "iPhone"){
		  frmTouchIdIntermediateLogin.show();
		}else{
		  frmRegistrationPopup.show();
		}
		 gblActivationFlow = false;
		dismissLoadingScreen();
	}else{
		if(GBL_Fatca_Flow == "pushnote")
		{	
			gblPushNotificationFlag = false;
			pushNotificationAllow(gblPayLoadKpns,upgradeSkip);
		}
		else{
			//Adding code to remove the dynamically added menu widgets if any and add them again
			inboxclickedClicked=1;
		    settingsClicked=1;
		    moreClicked=1;
			frmMenu.flexdynamicContainer.removeAll();
			callCustomerAccountService(upgradeSkip);
		}
	}
}
