var glbPin = "";
var glbPinArr = [];
var glbRandomRang = [];
var glbDigitWeight = [];
var glbIndex = 0;
var maxLenPin = 4;
var iFlag = true;
var localeIndex = 0;
isConfirmPin = false;
var emptyPinSkin = "btnIBEmpyATMPin";
var fillPinSkin = "btnIBFillATMPin";
var disablePinSkin = "btnIBEmptyATMPINDisable";
var maskCardNo;

function init_frmMBChangePINConfirm_Function() {
    initializeCPPinAssignNew();
    frmMBChangePINConfirm.btn0.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn1.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn2.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn3.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn4.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn5.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn6.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn7.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn8.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btn9.onClick = btnConfirmPinClick;
    frmMBChangePINConfirm.btnClr.onClick = btnCPPinClear;
    frmMBChangePINConfirm.btnDel.onClick = btnCPPinDelete;
    frmMBChangePINConfirm.btnCancel.onClick = btnCancelEvent;
    frmMBChangePINConfirm.btnMainMenu.onClick = handleMenuBtn;
}

function btnCPPinClear(eventobject) {
    clearCPPINMBNew.call(this);
};

function clearCPPINMBNew() {
    glbPin = "";
    glbIndex = 0;
    glbPinArr = [];
    toggleCPConfirmPINMBNew(false);
    isConfirmPin = false;
    assignCPPINRenderMBNew("");
}

function btnCPPinDelete(eventobject) {
    deleteCPPINMBNew.call(this);
};

function deleteCPPINMBNew() {
    if (isConfirmPin) {
        if (isNotBlank(glbPin)) {
            decreaseCPPINValMBNew();
        } else {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
            toggleCPConfirmPINMBNew(false);
            isConfirmPin = false;
            decreaseCPPINValMBNew();
        }
    } else {
        if (glbPinArr.length > 0 && glbPinArr[0].length == maxLenPin) {
            glbPin = glbPinArr[0];
            glbIndex = glbPinArr[0].length;
            glbPinArr = [];
        }
        if (isNotBlank(glbPin)) {
            decreaseCPPINValMBNew();
            toggleCPConfirmPINMBNew(false);
            isConfirmPin = false;
        }
    }
}

function decreaseCPPINValMBNew() {
    --glbIndex;
    glbPin = glbPin.substring(0, glbPin.length - 1);
    assignCPPINRenderMBNew("");
}
//PIN Setting
function btnConfirmPinClick(eventobject) {
    assignCPPINRenderMBNew.call(this, eventobject["text"]);
};

function assignCPPINRenderMBNew(pin) {
    if (isNotBlank(pin)) {
        if (isConfirmPin) {
            if (glbPin.length < maxLenPin) {
                addCPConfirmPinMBNew(pin, ++glbIndex);
            }
        } else {
            addCPPinMBNew(pin, ++glbIndex);
        }
    } else {
        renderCPPINMBNew();
    }
}

function addCPConfirmPinMBNew(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderCPPINMBNew();
    if (glbPin.length == maxLenPin) {
        verifyCPCardPinServiceForCreditCardActivation();
    }
}

function addCPPinMBNew(pin, position) {
    var pinVal = parseInt(pin.charCodeAt(0));
    pinVal += parseInt(glbRandomRang[position - 1]);
    pinVal += parseInt(glbDigitWeight[position - 1]);
    glbPin += String.fromCharCode(pinVal);
    renderCPPINMBNew();
    if (glbPin.length == maxLenPin) {
        //toggleKeyPad(true);
        toggleCPConfirmPINMBNew(true);
        isConfirmPin = true;
        glbPinArr[0] = glbPin;
        glbPin = "";
        glbIndex = 0;
        renderCPPINMBNew();
    }
}

function toggleCPConfirmPINMBNew(command) {
    frmMBChangePINConfirm.hbxAtmPinConfirm.setVisibility(command);
    if (!command) {
        frmMBChangePINConfirm.flxBody.top = "25.0%";
        frmMBChangePINConfirm.flxBody.height = "66.0%";
        frmMBChangePINConfirm.flxConfirmPinTxt.top = "0.0%";
        frmMBChangePINConfirm.hbxAtmPin.top = "11.0%";
        frmMBChangePINConfirm.hbxAtmPin.height = "18.0%";
        frmMBChangePINConfirm.flxConfirmPinTxt.top = "2.00%";
        frmMBChangePINConfirm.flxPinPad.top = "32.00%";
        frmMBChangePINConfirm.flxPinPad.height = "65.00%";
        frmMBChangePINConfirm.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("SetPIN_NewPINTopic");
        frmMBChangePINConfirm.flxMainCard.setVisibility(true);
    } else {
        frmMBChangePINConfirm.flxBody.top = "11.0%";
        frmMBChangePINConfirm.flxBody.height = "80.0%";
        frmMBChangePINConfirm.flxConfirmPinTxt.top = "17.0%";
        frmMBChangePINConfirm.hbxAtmPin.top = "0.0%";
        frmMBChangePINConfirm.hbxAtmPin.height = "16.0%";
        frmMBChangePINConfirm.hbxAtmPinConfirm.top = "25.0%"
        frmMBChangePINConfirm.hbxAtmPinConfirm.height = "16.0%";
        frmMBChangePINConfirm.flxPinPad.top = "45.00%";
        frmMBChangePINConfirm.flxPinPad.height = "52.00%";
        frmMBChangePINConfirm.lblConfirmAtmPin.text = kony.i18n.getLocalizedString("SetPIN_ConfirmPINTopic");
        frmMBChangePINConfirm.flxMainCard.setVisibility(false);
    }
}

function renderCPPINMBNew() {
    var btnObject = [];
    if (isConfirmPin) {
        btnObject = [frmMBChangePINConfirm.imgPin1Confirm, frmMBChangePINConfirm.imgPin2Confirm, frmMBChangePINConfirm.imgPin3Confirm, frmMBChangePINConfirm.imgPin4Confirm];
    } else {
        btnObject = [frmMBChangePINConfirm.imgPin1, frmMBChangePINConfirm.imgPin2, frmMBChangePINConfirm.imgPin3, frmMBChangePINConfirm.imgPin4];
    }
    var emptyPinDis = "pin_input.png";
    var emptyPin = "pin_input_active.png";
    var fillPin = "pin_input_filled.png";
    for (var i = 0; i < glbPin.length; i++) {
        btnObject[i].src = fillPin;
    }
    for (var i = btnObject.length; i > glbPin.length; i--) {
        btnObject[i - 1].src = emptyPin;
        if (i > glbPin.length + 1) {
            btnObject[i - 1].src = emptyPinDis;
        }
    }
}

function verifyCPCardPinServiceForCreditCardActivation() {
    glbPinArr[1] = glbPin;
    if (glbPinArr[0] != glbPinArr[1]) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("keyAssignPinNotMatch"), kony.i18n.getLocalizedString("info"), clearConfirmPINVal); //clearCPPINMBNew
    } else {
        //TO-DO, Implement the asign pin service call.
        initOnlineChangeNewPin();
        //NavigateToCPCompleteScreen(); // Temporarily we are navigating to complete page, Need to integrate with servcie and uncomment the above line
    }
}

function NavigateToCPCompleteScreen() {
    var randNumber = parseInt((Math.random() * 1000000) + "") + "";
    //alert("randNumber"+randNumber)
    kony.timer.schedule(randNumber, showCPComplete, 2, false);
}

function showCPComplete() {
    frmMBChangePinSuccess.show();
}

function gotoChangePinFailScreen() {
    frmMBChangePinFailure.show();
}

function assignCPCardPinService() {
    //service call shoud be done
    //Sample value
    encryptCPCardPinUsingE2EE(decryptPinDigit(glbPin));
    //			var RPIN = encryptPinUsingE2EE(glbPin);
    //			alert("RPIN: "+RPIN);
}

function encryptCPCardPinUsingE2EE(pin) {
    var e2eeResult = "";
    if (gblPk != "" && gblRand != "" && gblAlgorithm != "") {
        if (undefined != e2eeResult) {
            assignCPCardPin(e2eeResult);
        } else {
            //Error handle when encryption fail
        }
    } else {
        //Error handle when init service failed
    }
}

function assignCPCardPin(encryptedPin) {
    showDismissLoadingMBIB(true);
    var inputParam = {};
    inputParam["rPin"] = encryptedPin;
    showCPComplete();
    //var status = invokeServiceSecureAsync("assignPinCardActivation", inputParam, assignCPPinCallBack);
}

function assignCPPinCallBack(status, resulttable) {
    if (status == 400) {
        showDismissLoadingMBIB(false);
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == "0") {
            showCPComplete();
        } else {
            var errMsg = resulttable["faultstring"] + glbPin;
            showAlert(errMsg, kony.i18n.getLocalizedString("info"));
        }
    }
}

function initializeCPPinAssignNew() {
    initialParam();
    clearCPPINMBNew();
}
// Added to clear only Confirm PIN values
function clearConfirmPINVal() {
    glbPin = "";
    glbIndex = 0;
    //glbPinArr = [];
    //toggleCPConfirmPINMBNew(false);
    isConfirmPin = true;
    assignCPPINRenderMBNew("");
}