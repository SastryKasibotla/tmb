gblPersonalizeID = "";

function srvVerifyTxnPassword() {
    showLoadingScreen();
    var inputParam = {}
    if (flowSpa) {
        if (gblTokenSwitchFlag == true) {
            inputParam["password"] = popOtpSpa.txttokenspa.text;
            inputParam["segmentIdVal"] = "MIB";
            inputParam["loginModuleId"] = "IB_HWTKN";
            inputParam["userStoreId"] = "DefaultStore";
            inputParam["segmentId"] = "segmentId";
            inputParam["channel"] = "InterNet Banking";
            inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag; //future
            popOtpSpa.txttokenspa.text = "";
        } else {
            inputParam["password"] = popOtpSpa.txtOTP.text;
            popOtpSpa.txtOTP.text = "";
        }
    } else {
        inputParam["loginModuleId"] = "MB_TxPwd";
        inputParam["password"] = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
    inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    inputParam["userStoreId"] = "DefaultStore";
    inputParam["userId"] = "";
    inputParam["sessionVal"] = "";
    inputParam["segmentId"] = "segmentId";
    inputParam["segmentIdVal"] = "MIB";
    inputParam["personalizedAccList"] = [];
    var tmp_array = [];
    gblPersonalizeID = gblPersonalizeID
    var collection = gblMyAccntAddTmpData;
    for (var i = 0; i < collection.length; i++) {
        var accntNo = collection[i].lblAccntNoValue.replace(/-/g, "");
        // for loan account
        if (collection[i].lblAccntSuffix != null && collection[i].lblAccntSuffix != undefined && collection[i].lblAccntSuffix != "") accntNo = "0" + accntNo + collection[i].lblAccntsuffixValue;
        var acccname = collection[i].lblbanknamevalue;
        if (acccname == null || acccname == undefined || acccname == "") acccname = "Not returned from GSB";
        var tmpArray = ["", gblPersonalizeID, collection[i].bankCD, accntNo.trim(), collection[i].lblaccountNickNamevalue.trim(), "added", acccname.trim()];
        tmp_array.push(tmpArray);
        var Number = accntNo;
        var NickName = collection[i].lblaccountNickNamevalue;
        var BankName = getBankName(collection[i].bankCD);
    }
    gblAddAccnt = true;
    inputParam["personalizedAccList"] = tmp_array.toString();
    invokeServiceSecureAsync("ExecuteMyAccountAddService", inputParam, AddCompleteProcessCallBack)
}

function AddCompleteProcessCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (flowSpa) {
                gblRetryCountRequestOTP = "0";
                popOtpSpa.dismiss();
            } else {
                popupTractPwd.dismiss();
            }
            addCompleteScreen();
            dismissLoadingScreen();
        } else if (resulttable["opstatus"] == 1) {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) alert(" " + resulttable["errMsg"]);
            else alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        } else {
            dismissLoadingScreen();
            if (flowSpa) {
                if (resulttable["errCode"] == "VrfyOTPErr00002") {
                    popOtpSpa.dismiss();
                    kony.application.dismissLoadingScreen();
                    popTransferConfirmOTPLock.show();
                }
                if (resulttable["errCode"] == "VrfyOTPErr00005") {
                    popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
                    popOtpSpa.txttokenspa.text = "";
                    kony.application.dismissLoadingScreen();
                }
                if (resulttable["errCode"] == "VrfyOTPErr00001") {
                    if (gblTokenSwitchFlag) {
                        popOtpSpa.lblTokenMsg.text = kony.i18n.getLocalizedString("wrongOTP");
                        popOtpSpa.txttokenspa.text = "";
                    } else {
                        popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
                        popOtpSpa.lblPopupTract4Spa.text = "";
                    }
                    kony.application.dismissLoadingScreen();
                } else {
                    if (gblTokenSwitchFlag) {
                        popOtpSpa.lblTokenMsg.text = resulttable["errMsg"];
                        popOtpSpa.txttokenspa.text = "";
                    } else {
                        popOtpSpa.lblPopupTract2Spa.text = resulttable["errMsg"];
                        popOtpSpa.lblPopupTract4Spa.text = "";
                    }
                    kony.application.dismissLoadingScreen();
                }
            } else {
                if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
                    showTranPwdLockedPopup();
                } else setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"))
            }
        }
    }
}

function spaAddaccntToeknExchng() {
    var inputParam = [];
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, spaAddaccntToeknExchngCallbackfunction);
}

function spaAddaccntToeknExchngCallbackfunction(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            saveAddAccountDetals();
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
        }
    }
}

function saveAddAccountDetals() {
    var inputParam = {};
    inputParam["personalizedAccList"] = [];
    var tmp_array = [];
    var accountList = "";
    gblPersonalizeID = gblPersonalizeID;
    //Commenting as the security changes are not needed for MB
    /*
    var collection = gblMyAccntAddTmpData;
    for (var i = 0; i < collection.length; i++) {
    	var accntNo = collection[i].lblAccntNoValue.replace(/-/g, "");
    	// for loan account
    	if (collection[i].lblAccntSuffix != null && collection[i].lblAccntSuffix != undefined && collection[i].lblAccntSuffix !=
    		"")
    		accntNo = "0" + accntNo + collection[i].lblAccntsuffixValue;
    		
    	var acccname=collection[i].lblbanknamevalue;
    		if(acccname == null || acccname ==undefined )
    		acccname="null";	
    	var tmpArray = [gblcrmId, gblPersonalizeID, collection[i].bankCD, accntNo, collection[i].lblaccountNickNamevalue,
    		"added", acccname];
    	tmp_array.push(tmpArray);
    	
    }
    gblAddAccnt = true;
    inputParam["personalizedAccList"] = tmp_array.toString();
    */
    if (flowSpa) {
        spaChnage = "myacctsadd";
        tmp_array = [];
        var collection = gblMyAccntAddTmpData;
        for (var i = 0; i < collection.length; i++) {
            var accntNo = collection[i].lblAccntNoValue.replace(/-/g, "");
            // for loan account
            if (collection[i].lblAccntSuffix != null && collection[i].lblAccntSuffix != undefined && collection[i].lblAccntSuffix != "") accntNo = "0" + accntNo + collection[i].lblAccntsuffixValue;
            var acccname = collection[i].lblbanknamevalue;
            if (acccname == null || acccname == undefined || acccname == "") acccname = "null";
            var tmpArray = ["", gblPersonalizeID, collection[i].bankCD, accntNo.trim(), collection[i].lblaccountNickNamevalue.trim(), "added", acccname.trim()];
            tmp_array.push(tmpArray);
        }
        //var inputCValue = crc32(new String(tmp_array.toString().trim()));
        //var encCrcVal = encryptData(inputCValue.toString());
        //inputParam["encCrcpersonalizedAccList"]= encCrcVal;
        inputParam["personalizedAccList"] = tmp_array.toString();
        //inputParam["personalizedAccList"] = encMyAccountData(accountList);
        //OTP Changes
        gblAccountDetails = getAccountDetailMessageSpa(frmMyAccntConfirmationAddAccount.segBankAccnt.data);
        var AccountDetails = gblAccountDetails;
        //	var inputCVal = crc32(new String(AccountDetails.toString().trim()));
        //var encCrcValue = encryptData(inputCVal.toString());
        //inputParam["encCrcAccDetailMsg"] = encCrcValue;
        inputParam["AccDetailMsg"] = AccountDetails;
    } else inputParam["personalizedAccList"] = "";
    invokeServiceSecureAsync("saveAddAccountDetails", inputParam, saveAddAccountDetailsCallbackMB)
}

function saveAddAccountDetailsCallbackMB(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["addAccountList"] != null && callBackResponse["addAccountList"] != undefined) var accounts = callBackResponse["addAccountList"].split(",");
        if (flowSpa) {
            dismissLoadingScreen();
            if (gblIBFlowStatus == "04") {
                //showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
                popTransferConfirmOTPLock.show();
                //return false;
            } else {
                var inputParams = {}
                spaChnage = "myacctsadd"
                gblOTPFlag = true;
                try {
                    kony.timer.cancel("otpTimer")
                } catch (e) {}
                //input params for SPA
                gblSpaChannel = "AddMyAccount";
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    SpaEventNotificationPolicy = "MIB_AddMyAcc_EN";
                    SpaSMSSubject = "MIB_AddMyAcc_EN";
                } else {
                    SpaEventNotificationPolicy = "MIB_AddMyAcc_TH";
                    SpaSMSSubject = "MIB_AddMyAcc_TH";
                }
                gblAccountDetails = getAccountDetailMessageSpa(frmMyAccntConfirmationAddAccount.segBankAccnt.data);
                onClickOTPRequestSpa();
            }
        }
        /*for(var i=0,j=0;i<accounts.length;i+=6,j++)
        {	
        	if(j>=gblMyAccntAddTmpData.length)
        	break;
        	
        	gblMyAccntAddTmpData[j]["lblNNValue"]=accounts[i+4];
        	gblMyAccntAddTmpData[j]["lblAccntNumVal"]=addHyphenIB(accounts[i+3]);
        	gblMyAccntAddTmpData[j]["bankCD"]=accounts[i+2];
        	//gblMyAccntAddTmpData[j]["lblBankName"]=getBankNameCurrentLocale(accounts[i+2]);
        }*/
    }
}

function myAccountListService() {
    showLoadingScreen();
    var myAccountList_inputparam = {};
    myAccountList_inputparam["accountsFlag"] = true;
    var status = invokeServiceSecureAsync("customerAccountInquiry", myAccountList_inputparam, myAccountListCallBackAlt);
}

function getMyAccountListServiceAfterEditNickName() {
    showLoadingScreen();
    var myAccountList_inputparam = {};
    myAccountList_inputparam["accountsFlag"] = true;
    var status = invokeServiceSecureAsync("customerAccountInquiry", myAccountList_inputparam, callBankMyAccountListServiceAfterEditNickName);
}

function myAccountViewDepositeService() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["acctId"] = "" + frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
    invokeServiceSecureAsync("depositAccountInquiry", inputparam, myAccountViewDepositeCallBack);
}

function myAccountViewCreditCardService() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["cardId"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
    inputparam["tranCode"] = TRANSCODEUN;
    invokeServiceSecureAsync("creditcardDetailsInq", inputparam, myAccountViewCreditCardCallBack);
}

function myAccountViewLoanAccService() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["acctId"] = "" + frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
    inputparam["acctType"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].hiddenAcctType;
    var status = invokeServiceSecureAsync("doLoanAcctInq", inputparam, myAccountViewLoanAccCallBack);
}
var myAccountEditService_inputparam = {};

function myAccountEditServiceSavingsCare() {
    showLoadingScreen();
    var BankName = "";
    myAccountEditService_inputparam["personalizedId"] = gblPersonalizeID;
    myAccountEditService_inputparam["acctNickName"] = frmMBSavingsCareAddNickName.txtNickName.text; //gblOpenSavingsCareNickName;
    kony.print("GOWRI srvMyAccount.js--gblsegID=" + gblsegID);
    kony.print("GOWRI srvMyAccount.js--HERE 3");
    myAccountEditService_inputparam["personalizedAcctId"] = gblSavingsCareAccId;
    myAccountEditService_inputparam["recordPersonalizedId"] = gblSavingsCarepersonalizedId;
    myAccountEditService_inputparam["bankCD"] = gblSavingsCareBankCD;
    myAccountEditService_inputparam["bankCD"] = "11";
    kony.print("GOWRI gblSavingsCareBankCD=" + gblSavingsCareBankCD);
    BankName = "TMB";
    //Logging activity
    var Number = myAccountEditService_inputparam["personalizedAcctId"];
    kony.print("GOWRI srvMyAccount.js--HERE 4 Number=" + Number);
    var NickName = myAccountEditService_inputparam["acctNickName"];
    myAccountEditService_inputparam["acctStatus"] = "";
    myAccountEditService_inputparam["acctName"] = frmMyAccountView.lblAccntNameValue.text;
    kony.print("GOWRI srvMyAccount.js--HERE 4 acctName=" + frmMyAccountView.lblAccntNameValue.text);
    myAccountEditService_inputparam["acctStatus"] = "";
    myAccountEditService_inputparam["BankName"] = BankName;
    myAccountEditService_inputparam["Number"] = Number;
    myAccountEditService_inputparam["gblIBEditNickNameValue"] = gblEditNickNameValue;
    kony.print("GOWRI srvMyAccount.js--HERE 4 gblEditNickNameValue=" + gblEditNickNameValue);
    myAccountEditService_inputparam["AccountNameEdit"] = frmMyAccountView.lblAccntNameValue.text;
    invokeServiceSecureAsync("MyAccountEditService", myAccountEditService_inputparam, myAccountEditServiceSavingsCareCallBack);
}

function myAccountEditServiceNew(flow) {
    showLoadingScreen();
    var BankName = "";
    myAccountEditService_inputparam["personalizedId"] = gblPersonalizeID;
    myAccountEditService_inputparam["acctNickName"] = frmMyAccountEdit.txtEditAccntNickName.text;
    if (gblsegID == "segTMBAccntDetails") {
        myAccountEditService_inputparam["personalizedAcctId"] = getAccountNo(frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId);
        myAccountEditService_inputparam["recordPersonalizedId"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedId;
        myAccountEditService_inputparam["bankCD"] = "11";
        BankName = "TMB";
    } else if (gblAccountSummaryFlagMB == "true") {
        myAccountEditService_inputparam["personalizedId"] = gblPersonalizeID;
        myAccountEditService_inputparam["personalizedAcctId"] = gblPersonalizedRecordIdFromDreamSavings;
        myAccountEditService_inputparam["recordPersonalizedId"] = gblPersonalizeID;
        myAccountEditService_inputparam["bankCD"] = "11";
        BankName = "TMB";
    } else {
        myAccountEditService_inputparam["personalizedAcctId"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].personalizedAcctId;
        myAccountEditService_inputparam["recordPersonalizedId"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].personalizedId;
        myAccountEditService_inputparam["bankCD"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD;
        BankName = getBankName(myAccountEditService_inputparam["bankCD"]);
    }
    //Logging activity
    var Number = myAccountEditService_inputparam["personalizedAcctId"];
    var NickName = myAccountEditService_inputparam["acctNickName"];
    myAccountEditService_inputparam["acctStatus"] = "";
    myAccountEditService_inputparam["acctName"] = frmMyAccountView.lblAccntNameValue.text;
    myAccountEditService_inputparam["acctStatus"] = "";
    myAccountEditService_inputparam["BankName"] = BankName;
    myAccountEditService_inputparam["Number"] = Number;
    if (flow == "edit") {
        myAccountEditService_inputparam["gblIBEditNickNameValue"] = gblEditNickNameValue;
        if (gblsegID == "segOtherBankAccntDetails" && getORFTFlagMB(frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD) == "N") myAccountEditService_inputparam["AccountNameEdit"] = frmMyAccountEdit.tbxAccountName.text;
        else myAccountEditService_inputparam["AccountNameEdit"] = frmMyAccountView.lblAccntNameValue.text;
        invokeServiceSecureAsync("MyAccountEditService", myAccountEditService_inputparam, myAccountEditServiceCallBack);
    } else if (flow == "delete") {
        /*if (gblsegID == "segTMBAccntDetails") {
        	myAccountEditService_inputparam["acctNickName"] =frmMyAccountView.lblNickName.text;
        }*/
        if (gblAccountSummaryFlagMB == "true") {
            myAccountEditService_inputparam["acctNickName"] = frmDreamSavingMB.lblAccountNicknameValue.text
        } else {
            myAccountEditService_inputparam["acctNickName"] = frmMyAccountView.lblNickName.text;
        }
        invokeServiceSecureAsync("MyAccountDeleteService", myAccountEditService_inputparam, myAccountDelServiceCallBack);
    }
}
var myAccountEditService_inputparam = {};
/*function myAccountEditService_inner() {
	showLoadingScreen();
	var BankName = "";
	myAccountEditService_inputparam["acctNickName"] = frmMyAccountEdit.txtEditAccntNickName.text;
	if (gblsegID == "segTMBAccntDetails") {
		myAccountEditService_inputparam["personalizedAcctId"] = getAccountNo(frmMyAccountList.segTMBAccntDetails.selectedItems[
			0].personalizedAcctId);
		myAccountEditService_inputparam["personalizedId"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedId;
		myAccountEditService_inputparam["bankCD"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].bankCD;
		BankName = "TMB";
	} else {
		myAccountEditService_inputparam["personalizedAcctId"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].personalizedAcctId;
		myAccountEditService_inputparam["personalizedId"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].personalizedId;
		myAccountEditService_inputparam["bankCD"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD;
		BankName = getBankName(myAccountEditService_inputparam["bankCD"]);
	}
	var Number = myAccountEditService_inputparam["personalizedAcctId"];
	var NickName = myAccountEditService_inputparam["acctNickName"];
	activityLogServiceCall("055", "", "01", "", "Edit", gblEditNickNameValue, BankName, Number, "", "");
	myAccountEditService_inputparam["acctStatus"] = "";	
	invokeServiceSecureAsync("crmAccountModKony", myAccountEditService_inputparam, myAccountEditServiceCallBack);
}*/
/*function myAccountDelService_inner() {
	showLoadingScreen();
	var myAccountDelService_inputparam = {};
	myAccountDelService_inputparam["acctNickName"]= frmMyAccountView.lblNickName.text;
	if (gblsegID == "segTMBAccntDetails") {
		myAccountDelService_inputparam["personalizedAcctId"] = getAccountNo(frmMyAccountList.segTMBAccntDetails.selectedItems[
			0].personalizedAcctId);
		myAccountDelService_inputparam["personalizedId"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedId;
		myAccountDelService_inputparam["bankCD"] = frmMyAccountList.segTMBAccntDetails.selectedItems[0].bankCD;
	} else {
		myAccountDelService_inputparam["personalizedAcctId"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].personalizedAcctId;
		myAccountDelService_inputparam["personalizedId"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].personalizedId;
		myAccountDelService_inputparam["bankCD"] = frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD;
	}
	myAccountDelService_inputparam["acctStatus"] = "Deleted";
	//Logging activity
	var Number = myAccountDelService_inputparam["personalizedAcctId"];
	var NickName = frmMyAccountView.lblNickName.text;
	var BankName = getBankName(myAccountDelService_inputparam["bankCD"]);
	activityLogServiceCall("055", "", "01", "", "Delete", NickName, BankName, Number, "", "");
	invokeServiceSecureAsync("crmAccountModKony", myAccountDelService_inputparam, myAccountDelServiceCallBack);
}*/
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : fetching deposite accnt data
 * Output params: deposite accnt data
 * ServiceCalls:
 * Called From : call back function of myAccountViewDeposite
 */
function myAccountViewDepositeCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblDepositAcclist = resulttable;
            gblMyAccountFlowFlag = 01;
            var accVal = frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId;
            var accValLength = accVal.length;
            accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((accValLength - 1), accValLength);
            setAccountView(kony.i18n.getLocalizedString("AccountNoLabel"), accVal, kony.i18n.getLocalizedString("AccountName"), resulttable["accountTitle"], "", "");
            dismissLoadingScreen();
            frmMyAccountView.show();
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function myAccountViewCreditCardCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblMyAccountFlowFlag = 03;
            var cardName = resulttable["fromAcctName"];
            var cardNo = resulttable["cardNo"];
            setAccountView(kony.i18n.getLocalizedString("keyCreditCardNo") + ":", cardNo, kony.i18n.getLocalizedString("keyCardHolderName") + ":", cardName, "", "");
            dismissLoadingScreen();
            frmMyAccountView.show();
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : fetching loan acct data
 * Output params: loan acct data
 * ServiceCalls:
 * Called From : call back function of myAccountViewLoanAcc
 */
function myAccountViewLoanAccCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblMyAccountFlowFlag = 02;
            var accntNo = resulttable["loanAccounNo"];
            var suffix = accntNo.substring(accntNo.length, accntNo.length - 3);
            var accVal = resulttable["loanAccounNo"];
            var accValLength = accVal.length - 3;
            accVal = accVal.substring((accValLength - 10), (accValLength - 7)) + "-" + accVal.substring((accValLength - 7), (accValLength - 6)) + "-" + accVal.substring((accValLength - 6), (accValLength - 1)) + "-" + accVal.substring((accValLength - 1), accValLength);
            setAccountView(kony.i18n.getLocalizedString("AccountNoLabel"), accVal, kony.i18n.getLocalizedString("keyAccntSuffix") + ":", suffix, kony.i18n.getLocalizedString("AccountName"), resulttable["accountName"]);
            dismissLoadingScreen();
            frmMyAccountView.show();
        } else {
            dismissLoadingScreen();
            alert(" " + resulttable["statusDesc"]);
        }
    }
}

function myAccountEditServiceSavingsCareCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            hbxfooter.isVisible = true;
            kony.print("GOWRI srvMyAccount.js EDIT MYACCOUNT SUCCESS");
            if (gblSavingsCareFlow == "MyAccountFlow") {
                frmMyAccountView.lblNickName.text = frmMBSavingsCareAddNickName.txtNickName.text;
            } else if (gblSavingsCareFlow == "AccountDetailsFlow") {
                //frmAccountDetailsMB.lblAccountNameHeader.text=frmMBSavingsCareAddNickName.txtNickName.text;
                gblAccountTable["custAcctRec"][gblIndex]["acctNickName"] = frmMBSavingsCareAddNickName.txtNickName.text
                kony.print("GOWRI in text2=" + gblAccountTable["custAcctRec"][gblIndex]["acctNickName"])
            }
            dismissLoadingScreen();
            getMyAccountListServiceAfterEditNickName();
            if (gblSavingsCareFlow == "MyAccountFlow" || gblSavingsCareFlow == "AccountDetailsFlow") {
                kony.print("GOWRI srvMyAccount.js NOW CALLING callBeneficiaryInquiryServiceOnEdit");
                callBeneficiaryInquiryServiceOnEdit();
            }
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function myAccountEditServiceCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            hbxfooter.isVisible = true;
            frmMyAccountView.lblNickName.text = frmMyAccountEdit.txtEditAccntNickName.text;
            if (gblsegID == "segOtherBankAccntDetails" && getORFTFlagMB(frmMyAccountList.segOtherBankAccntDetails.selectedItems[0].bankCD) == "N") frmMyAccountView.lblAccntNameValue.text = frmMyAccountEdit.tbxAccountName.text;
            dismissLoadingScreen();
            getMyAccountListServiceAfterEditNickName();
            frmMyAccountView.show();
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function myAccountDelServiceCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            frmMyAccountList.show();
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function srvGetBankList() {
    showLoadingScreen();
    var getBankList_inputparam = {};
    invokeServiceSecureAsync("getBankList", getBankList_inputparam, getBankListServiceCallBack);
}
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : get bank list
 * Output params: bank list
 * ServiceCalls:
 * Called From : call back function of getBankList
 */
function getBankListServiceCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            bankListArray = [];
            globalSelectBankData = [];
            // This locale is used to change bank name in list to current locale
            var locale = kony.i18n.getCurrentLocale();
            var collectionData = resulttable["Results"];
            for (var i = 0; i < resulttable.Results.length; i++) {
                var obj = {};
                //
                if ((resulttable.Results[i].bankStatus == "01" || resulttable.Results[i].bankStatus.toUpperCase() == "ACTIVE") && (resulttable.Results[i].smartFlag.toUpperCase() == "Y" || resulttable.Results[i].orftFlag.toUpperCase() == "Y")) {
                    if (kony.string.startsWith(locale, "en", true) == true) obj.lblBanklist = resulttable.Results[i].bankNameEng;
                    else obj.lblBanklist = resulttable.Results[i].bankNameThai;
                    obj.bankCode = resulttable.Results[i].bankCode;
                    obj.orftFlag = resulttable.Results[i].orftFlag;
                    //
                    bankListArray.push(obj);
                }
                if (locale == "en_US") {
                    tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
                        collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]
                    ];
                } else {
                    tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"],
                        collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]
                    ];
                }
                globalSelectBankData.push(tempRecord);
            }
            popBankList.segBanklist.setData(bankListArray);
            popBankList.lblFlag.text = "Bank";
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function srvGetBankDetail(bankCode) {
    showLoadingScreen();
    var inputparam = {};
    inputparam["bankCode"] = bankCode;
    invokeServiceSecureAsync("getBankDetails", inputparam, getBankDetailServiceCallBack);
}

function getBankDetailServiceCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            MAX_ACC_LEN = resulttable.Results[0].bankAccntLen;
            MAX_ACC_LEN_LIST = resulttable.Results[0].bankAccntLengths;
            if (MAX_ACC_LEN == 10 && MAX_ACC_LEN_LIST.indexOf(",") < 0) {
                //frmMyAccntAddAccount.btnacntlist.text= popBankList.segBanklist.selectedItems[0].lblBanklist;
                frmMyAccntAddAccount.lblAccntNo.setVisibility(true);
                frmMyAccntAddAccount.hbxAccntNovalue.setVisibility(true);
                frmMyAccntAddAccount.lblAccountSuffix.setVisibility(false);
                frmMyAccntAddAccount.hbxCreditAccoutNoValue.setVisibility(false);
                frmMyAccntAddAccount.vboxsuffix.setVisibility(false);
                frmMyAccntAddAccount.hbxOther.setVisibility(false);
            } else {
                frmMyAccntAddAccount.tbxother.maxTextLength = parseInt(MAX_ACC_LEN);
                //setting the textinput mode to ANY for SPA know issue
                if (flowSpa) {
                    frmMyAccntAddAccount.tbxother.textInputMode = constants.TEXTBOX_INPUT_MODE_ANY;
                } else {
                    frmMyAccntAddAccount.tbxother.textInputMode = constants.TEXTBOX_INPUT_MODE_NUMERIC;
                }
            }
            IS_ORFT = resulttable.Results[0].orftFlag;
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    } else if (status == 300) {
        dismissLoadingScreen();
    }
}

function srvGetAcctType() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["lang"] = kony.i18n.getCurrentLocale();
    var status = invokeServiceSecureAsync("getAccountList", inputparam, getAcctTypeCallBack);
}

function getAcctTypeCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            MAX_OTHER_BANK = resulttable["max_non_tmb_count"];
            MAX_ACC_ADD = resulttable["max_acc_add"]; // Max no. of accnt that can be added at a time
            TMB_BANK_CD = resulttable["tmb_bank_cd"]; //TMB bannk ID value
            HIDDEN_ACCT_VAL = resulttable["hidden_acct_val"];
            acctList = []; // Clearing it
            for (var i = 0; i < resulttable.AcctList.length; i++) {
                var acctObj = {};
                acctObj.lblBanklist = resulttable.AcctList[i].acctType;
                acctList.push(acctObj);
            }
            var frmName = kony.application.getCurrentForm();
            if (frmName["id"] != "frmMyAccntAddAccount") {
                crmProfileInqAccount();
            }
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}
/**
 * Author  : Nishant Kumar
 * Date    : May 05, 2013
 * Purpose : Adding account
 * Output params: No
 * ServiceCalls: getAccountList
 * Called From : gblAddAccntVar()
 */
function srvAddCompleteProcess() {
    showLoadingScreen();
    var inputparam = {};
    inputparam["personalizedAccList"] = [];
    var tmp_array = [];
    for (var i = 0; i < collection.length; i++) {
        var accntNo = collection[i].lblAccntNoValue.replace(/-/g, "");
        // for loan account
        if (collection[i].lblAccntSuffix != null && collection[i].lblAccntSuffix != undefined && collection[i].lblAccntSuffix != "") accntNo = "0" + accntNo + collection[i].lblAccntsuffixValue;
        var tmpArray = [gblcrmId, gblPersonalizeID, collection[i].bankCD, accntNo, collection[i].lblaccountNickNamevalue, "added", "null"];
        tmp_array.push(tmpArray);
        //Logging activity
        var Number = accntNo;
        var NickName = collection[i].lblaccountNickNamevalue;
        var BankName = getBankName(collection[i].bankCD);
        activityLogServiceCall("055", "", "01", "", "Add", NickName, BankName, Number, "", "");
    }
    gblAddAccnt = true;
    inputparam["personalizedAccList"] = tmp_array.toString();
    var status = invokeServiceSecureAsync("receipentAddBankAccntService", inputparam, AddCompleteProcessCallBack);
}

function srvOrftAccountInq(acctNo) {
    showLoadingScreen();
    var inputParam = {}
    inputParam["toAcctNo"] = acctNo;
    inputParam["toFIIdent"] = gblBankCD;
    invokeServiceSecureAsync("ORFTInq", inputParam, orftAccountInqCallBack)
}

function orftAccountInqCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
                ///fetching To Account Name for ORFT Inq
                gblAccntName = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];
                addToCacheLastStep();
            } else {
                dismissLoadingScreen();
                if (resulttable["errMsg"] == undefined || resulttable["errMsg"] == null) showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                else alert(" " + resulttable["errMsg"]);
            }
        } else {
            dismissLoadingScreen()
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
        dismissLoadingScreen();
    } else if (status == 300) {
        dismissLoadingScreen();
    }
}

function myAccountListOtherBankService(rawDataTMB) {
    var myAccountList_inputparam = {};
    myAccountList_inputparam["personalizedId"] = gblPersonalizeID; //gblPersonalizeID or "1300000062"
    myAccountList_inputparam["custAccntData"] = rawDataTMB.toString(); //rawDataTMB;
    myAccountList_inputparam["locale"] = kony.i18n.getCurrentLocale();
    invokeServiceSecureAsync("crmBankAccountInquiryKony", myAccountList_inputparam, myAccountListOtherBankCallBack);
}

function populateMyAccountsListScreenMB(resulttable) {
    var imageName = "piggyblueico.png";
    var rawDataTMB = [];
    var rawDataOther = [];
    gblAccntData = [];
    var locale = kony.i18n.getCurrentLocale();
    for (var i = 0; resulttable.custAcctRec != null && i < resulttable.custAcctRec.length; i++) {
        var nickName = resulttable.custAcctRec[i].acctNickName;
        if (nickName == undefined || nickName == "") {
            var accountId = resulttable.custAcctRec[i].accId;
            var startIndx = resulttable.custAcctRec[i].accId.length - 4;
            if (kony.string.startsWith(locale, "en", true) == true) {
                nickName = resulttable.custAcctRec[i].ProductNameEng;
            } else {
                nickName = resulttable.custAcctRec[i].ProductNameThai
            }
            if (nickName != null && nickName != undefined && nickName.length > 15) {
                nickName = nickName.substring(0, 15);
            }
            if (resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("Loan")) {
                nickName = nickName + " " + accountId.substring(7, 11);
            } else {
                nickName = nickName + " " + accountId.substring(startIndx, accountId.length);
            }
        }
        var imageName = "";
        imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["custAcctRec"][i]["ICON_ID"] + "&modIdentifier=PRODICON";
        gblAccntData.push({
            "nickName": nickName,
            "accntNo": resulttable.custAcctRec[i].accId,
            "bankCD": resulttable.custAcctRec[i].bankCD,
            "accntName": resulttable.custAcctRec[i].accountName,
            "accntStatus": resulttable.custAcctRec[i].personalisedAcctStatusCode,
            "productImage": imageName
        });
        if (resulttable.custAcctRec[i].personalisedAcctStatusCode != HIDDEN_ACCT_VAL) {
            rawDataTMB.push({
                "isTMB": "true",
                "lblTMBAccountDetails": nickName,
                "hiddenAcctType": resulttable.custAcctRec[i].accType,
                "hiddenAcctName": resulttable.custAcctRec[i].accountName,
                "imgAccountPicture": imageName,
                "SortId": resulttable.custAcctRec[i].SortId,
                "personalizedAcctId": resulttable.custAcctRec[i].accId,
                "hiddenAccountNo": resulttable.custAcctRec[i].accId,
                "personalizedId": resulttable.custAcctRec[i].persionlizedId,
                "bankCD": resulttable.custAcctRec[i].bankCD,
                "imgTMBSegArrow": "bg_arrow_right.png",
                "productID": resulttable.custAcctRec[i].productID,
                "openingMethod": resulttable.custAcctRec[i].openingMethod,
                "fiident": resulttable.custAcctRec[i].fiident,
                "hiddenProductNameEng": resulttable.custAcctRec[i].ProductNameEng,
                "hiddenProductNameThai": resulttable.custAcctRec[i].ProductNameThai
            });
        }
    }
    if (resulttable.Results != undefined) {
        for (var i = 0; resulttable.Results != null && i < resulttable.Results.length; i++) {
            var bnkCode = resulttable.Results[i].bankCD
            var bnkLogoURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/Bank-logo" + "/bank-logo-";
            var bankLogo = "";
            if (bnkCode == 11) bankLogo = "icon.png";
            else bankLogo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + bnkCode + "&modIdentifier=BANKICON";
            gblAccntData.push({
                "nickName": resulttable.Results[i].acctNickName,
                "accntNo": resulttable.Results[i].personalizedAcctId,
                "bankCD": resulttable.Results[i].bankCD,
                "accntName": resulttable.Results[i].accntName,
                "accntStatus": "NA"
            });
            if (resulttable.Results[i].acctStatus != HIDDEN_ACCT_VAL) {
                rawDataOther.push({
                    "isTMB": "false",
                    "lblOtherBankAccntDetails": resulttable.Results[i].acctNickName,
                    "imgotherBankAcntPic": bankLogo,
                    "hiddenAccountNo": resulttable.Results[i].personalizedAcctId,
                    "hiddenAcctName": resulttable.Results[i].accntName,
                    "personalizedAcctId": resulttable.Results[i].personalizedAcctId,
                    "personalizedId": resulttable.PersonalizedID,
                    "imgSegOtherBArrow": "bg_arrow_right.png",
                    "bankCD": resulttable.Results[i].bankCD,
                    "bankNameEng": resulttable.Results[i].bankNameEn,
                    "bankNameThi": resulttable.Results[i].bankNameTh
                });
                TOTAL_NON_TMB_AC++;
            }
        }
    }
    if (rawDataTMB.length > 0) {
        frmMyAccountList.lblTMBBank.isVisible = true;
        frmMyAccountList.lblTMBBank.text = kony.i18n.getLocalizedString("keylblTMBBank");
        rawDataTMB = sortByKey(rawDataTMB, 'SortId');
        frmMyAccountList.segTMBAccntDetails.setData(rawDataTMB);
    }
    if (rawDataOther.length > 0) {
        frmMyAccountList.lblOtherBankACCnt.isVisible = true;
        frmMyAccountList.lblOtherBankACCnt.text = kony.i18n.getLocalizedString("keylblOtherBankAccount");
        frmMyAccountList.segOtherBankAccntDetails.setData(rawDataOther);
    }
    if (rawDataTMB.length == 0 && rawDataOther.length == 0) {
        dismissLoadingScreen();
        frmMyAccountList.hbxErrorAccounts.setVisibility(true);
        frmMyAccountList.lblErrorAccnts.text = kony.i18n.getLocalizedString("keyAllAccntHidden");
    } else {
        //srvGetBankList();
    }
}

function crmProfileInqAccount() {
    invokeServiceSecureAsync("crmProfileInq", {}, crmProfileInqAccountCallBack);
}

function crmProfileInqAccountCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                gblMyAccntEmailId = resulttable["emailAddr"];
                myAccountListService();
            } else {
                dismissLoadingScreen();
                alert(" " + resulttable["statusDesc"]);
            }
        } else {
            dismissLoadingScreen()
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    } else if (status == 300) {
        dismissLoadingScreen();
    }
}

function srvAddOwnToRecepient() {
    var gblOverflowErrorFlag = true;
    var addRecipient_inputparam = {};
    var totalData = [];
    var mobileNo = gblPHONENUMBER;
    var personnelRcData = [gblCustomerName, , , mobileNo, , "Added", "Y"];
    totalData.push(personnelRcData);
    addRecipient_inputparam["addAccntFlag"] = "true";
    addRecipient_inputparam["receipentList"] = totalData.toString();
    invokeServiceSecureAsync("receipentNewAddService", addRecipient_inputparam, addOwnToRecepientCallBack);
}

function addOwnToRecepientCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            srvChekProfileExistMB();
        } else {
            dismissLoadingScreen()
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    } else if (status == 300) {
        dismissLoadingScreen();
    }
}

function myAccountListCallBackAlt(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                TOTAL_NON_TMB_AC = 0;
                NON_TMB_ADD = 0; // Non TMB accnt in cache - global var
                TOTAL_AC_ADD = 0;
                gblMyAccntAddTmpData = [];
                var rawDataTMB = [];
                var str = "";
                var imageName = "";
                MAX_OTHER_BANK = resulttable["max_non_tmb_count"];
                MAX_ACC_ADD = resulttable["max_acc_add"]; // Max no. of accnt that can be added at a time
                TMB_BANK_CD = resulttable["tmb_bank_cd"]; //TMB bannk ID value
                HIDDEN_ACCT_VAL = resulttable["hidden_acct_val"];
                //	gblCustAcctRecDreamsaving = resulttable.custAcctRec;			
                gblMBFlowStatus = resulttable["mbFlowStatusId"];
                gblEBStatusId = resulttable["customerStatusId"];
                loadBanklistconfiguration(resulttable);
                if (resulttable["ownProfile"] == 1) {
                    gblPersonalizeID = resulttable["PersonalizedID"];
                }
                if (resulttable.custAcctRec == null || resulttable.custAcctRec.length == 0) {
                    dismissLoadingScreen();
                    frmMyAccountList.hbxErrorAccounts.setVisibility(true);
                    frmMyAccountList.lblErrorAccnts.text = kony.i18n.getLocalizedString("keyAllAccntHidden");
                } else {
                    frmMyAccountList.hbxErrorAccounts.setVisibility(false);
                    populateMyAccountsListScreenMB(resulttable);
                }
            } else {
                dismissLoadingScreen();
                alert(" " + resulttable["statusDesc"]);
            }
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function callBankMyAccountListServiceAfterEditNickName(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                //loadBanklistconfiguration(resulttable);
                if (resulttable["ownProfile"] == 1) {
                    gblPersonalizeID = resulttable["PersonalizedID"];
                }
                if (resulttable.custAcctRec != null && resulttable.custAcctRec.length > 0) {
                    populateMyAccountsListAfterEdit(resulttable);
                }
            } else {
                alert(" " + resulttable["statusDesc"]);
            }
        } else {
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
    dismissLoadingScreen();
}

function populateMyAccountsListAfterEdit(resulttable) {
    var imageName = "piggyblueico.png";
    gblAccntData = [];
    var locale = kony.i18n.getCurrentLocale();
    for (var i = 0; resulttable.custAcctRec != null && i < resulttable.custAcctRec.length; i++) {
        var nickName = resulttable.custAcctRec[i].acctNickName;
        if (nickName == undefined || nickName == "") {
            var accountId = resulttable.custAcctRec[i].accId;
            var startIndx = resulttable.custAcctRec[i].accId.length - 4;
            if (kony.string.startsWith(locale, "en", true) == true) {
                nickName = resulttable.custAcctRec[i].ProductNameEng;
            } else {
                nickName = resulttable.custAcctRec[i].ProductNameThai
            }
            if (nickName != null && nickName != undefined && nickName.length > 15) {
                nickName = nickName.substring(0, 15);
            }
            if (resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("Loan")) {
                nickName = nickName + " " + accountId.substring(7, 11);
            } else {
                nickName = nickName + " " + accountId.substring(startIndx, accountId.length);
            }
        }
        imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["custAcctRec"][i]["ICON_ID"] + "&modIdentifier=PRODICON";
        gblAccntData.push({
            "nickName": nickName,
            "accntNo": resulttable.custAcctRec[i].accId,
            "bankCD": resulttable.custAcctRec[i].bankCD,
            "accntName": resulttable.custAcctRec[i].accountName,
            "accntStatus": resulttable.custAcctRec[i].personalisedAcctStatusCode,
            "productImage": ""
        });
    }
    if (resulttable.Results != undefined) {
        for (var i = 0; resulttable.Results != null && i < resulttable.Results.length; i++) {
            gblAccntData.push({
                "nickName": resulttable.Results[i].acctNickName,
                "accntNo": resulttable.Results[i].personalizedAcctId,
                "bankCD": resulttable.Results[i].bankCD,
                "accntName": resulttable.Results[i].accntName,
                "accntStatus": "NA"
            });
        }
    }
}
// Service call to chek whether the account is exsiting in Acc_Rel Table or not
/*function srvChekAccntExistMB(flag) {
	var myAccountList_inputparam = {};
	myAccountList_inputparam["personalizedId"] = gblPersonalizeID;
	myAccountList_inputparam["personalizedAcctId"] = getAccountNo(frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId);
	myAccountList_inputparam["bankCD"] = TMB_BANK_CD;
	
	if (flag == "01")
		invokeServiceSecureAsync("checkAccountExistAccnt", myAccountList_inputparam, srvChekAccntExistCallBackMBEdit);
	else
		invokeServiceSecureAsync("checkAccountExistAccnt", myAccountList_inputparam, srvChekAccntExistCallBackMBDel);
}*/
/*function srvChekAccntExistCallBackMBEdit(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			if (resulttable["accExist"].trim() == "false") {
				showLoadingScreen();
				var Number = getAccountNo(frmMyAccountList.segTMBAccntDetails.selectedItems[0].personalizedAcctId);
				var BankName = getBankName(TMB_BANK_CD);
				if (gblsegID == "segTMBAccntDetails") {
					BankName = "TMB";
				}
				activityLogServiceCall("055", "", "01", "", "Edit", gblEditNickNameValue, BankName, Number, "", "");
				var inputparam = {};
				inputparam["personalizedAccList"] = [];
				var tmpArray = [gblcrmId, gblPersonalizeID, TMB_BANK_CD, getAccountNo(frmMyAccountList.segTMBAccntDetails.selectedItems[
					0].personalizedAcctId), frmMyAccountEdit.txtEditAccntNickName.text, "added", frmMyAccountList.segTMBAccntDetails.selectedItems[
					0].hiddenAcctName + " "];
				inputparam["personalizedAccList"] = tmpArray.toString();
				invokeServiceSecureAsync("receipentAddBankAccntService", inputparam, AddCompleteProcessCallBack);
			} else {
				myAccountEditService_inner();
			}
		} else {
			dismissLoadingScreen();
			if(resulttable["errMsg"] != undefined)
	        	{
	        		alert(resulttable["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}		}
	}
}*/
/*function srvChekAccntExistCallBackMBDel(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblAccExist=resulttable["accExist"].trim();
			if (resulttable["accExist"].trim() == "false") {
				showLoadingScreen();
				var inputparam = {};
				inputparam["personalizedAccList"] = [];
				var tmpArray = [gblcrmId, gblPersonalizeID, TMB_BANK_CD, getAccountNo(frmMyAccountList.segTMBAccntDetails.selectedItems[
					0].personalizedAcctId), frmMyAccountEdit.txtEditAccntNickName.text, "added", frmMyAccountList.segTMBAccntDetails.selectedItems[
					0].hiddenAcctName + " "];
				inputparam["personalizedAccList"] = tmpArray.toString();
				invokeServiceSecureAsync("receipentAddBankAccntService", inputparam, AddCompleteProcessCallBack);
			}
			else{
			myAccountDelService_inner();
			}
		} else {
			dismissLoadingScreen();
			if(resulttable["errMsg"] != undefined)
	        	{
	        		alert(resulttable["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}		}
	}
}*/
function srvChekProfileExistMB() {
    var myAccountList_inputparam = {};
    myAccountList_inputparam["ownFlag"] = "Y";
    invokeServiceSecureAsync("checkUserExistRelInfoTable", myAccountList_inputparam, checkUserExistRelInfoTableCallBack);
}

function checkUserExistRelInfoTableCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["personalizedID"] == null) {
                if (gblOverflowErrorFlag == false) srvAddOwnToRecepient();
                else {
                    dismissLoadingScreen();
                    if (resulttable["errMsg"] != undefined && resulttable["errMsg"] != null) alert(" " + resulttable["errMsg"]);
                    else showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                }
            } else {
                gblOverflowErrorFlag = false;
                gblPersonalizeID = resulttable["personalizedID"];
                myAccountListOtherBankService(gblRawDataTMB);
            }
        } else {
            dismissLoadingScreen();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function getORFTFlagMB(bankCode) {
    var orftFlag = "N";
    for (var i = 0; i < bankListArray.length; i++) {
        if (bankCode == bankListArray[i].bankCode) {
            orftFlag = bankListArray[i].orftFlag;
            break;
        }
    }
    return orftFlag;
}

function loadBanklistconfiguration(resulttable) {
    bankListArray = [];
    // This locale is used to change bank name in list to current locale
    var locale = kony.i18n.getCurrentLocale();
    for (var i = 0; i < resulttable.BankList.length; i++) {
        var obj = {};
        if ((resulttable.BankList[i].bankStatus == "01" || resulttable.BankList[i].bankStatus.toUpperCase() == "ACTIVE") && (resulttable.BankList[i].smartFlag.toUpperCase() == "Y" || resulttable.BankList[i].orftFlag.toUpperCase() == "Y")) {
            if (kony.string.startsWith(locale, "en", true) == true) obj.lblBanklist = resulttable.BankList[i].bankNameEng;
            else obj.lblBanklist = resulttable.BankList[i].bankNameThai;
            obj.bankCode = resulttable.BankList[i].bankCode;
            obj.orftFlag = resulttable.BankList[i].orftFlag;
            //
            bankListArray.push(obj);
        }
    }
    popBankList.segBanklist.setData(bankListArray);
    popBankList.lblFlag.text = "Bank";
    dismissLoadingScreen();
}