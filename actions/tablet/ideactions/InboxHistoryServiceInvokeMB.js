







var gblUnreadCount = "0";
var gblInboxDetailsCache = new Array();
var gblInboxDetailsCacheForSort = new Array();
var tempPushArrayInbox = new Array();
var gblInboxHistoryModified = true;
var InboxHomePageState = false;
var ServiceResultInbox = new Array();
var SortByType = kony.i18n.getLocalizedString("MyActivitiesIB_Date");
var SortByOrder = kony.i18n.getLocalizedString("keyAscending");
var gblInboxSortedOrNormal = false;
var tempInboxDetails = null;
var gblMaxSetImportant = null;

//GET NOTIFICATION HISTORY
function getInboxHistoryMB(){
	var inputParams = {};
	showLoadingScreen();
	invokeServiceSecureAsync("inboxhistory", inputParams, getInboxHistoryMBCB)
}

function getInboxHistoryMBCB(status, resultset){
	if(status == 400){
		
		if(resultset["status"] == "1"){
			if(resultset["Results"] != null){
				//DO NOTHING
			}
			else{
				showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			}
			frmInboxHome.lblNoNotfn.isVisible = true;
			frmInboxHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoMessages");
			//frmInboxHome.btnCancel.text = "";
			frmInboxHome.btnCancel.isVisible = false;
			//frmInboxHome.btnCancel.skin = "btnBlueDark";
			//frmInboxHome.btnCancel.focusSkin = "btnBlueDark";
			//frmInboxHome.btnRight.text = "DeleteDisabled";
			frmInboxHome.btnRight.skin = "btnDeleteActive";
			frmInboxHome.btnRight.focusSkin = "btnDeleteActive";
			frmInboxHome.btnRight.setEnabled(false);
			frmInboxHome.btnRight.isVisible = false;
			if(flowSpa)
			frmInboxHome.hbox104116120741733.isVisible = false;
			else
			frmInboxHome.hbox477746511278608.isVisible = false;
			frmInboxHome.hbox477746511278660.isVisible = false;
			frmInboxHome.segMyRecipient.removeAll();
		}
		else if(resultset["status"] == "0"){
			//
			if(resultset["Results"].length > 0){
				ServiceResultInbox.length = 0;
				frmInboxHome.hbox477746511278608.isVisible = true;
				for(var i = 0; i < resultset["Results"].length; i++){
					ServiceResultInbox.push(resultset["Results"][i]);
				}
				gblMaxSetImportant = resultset["maxsetimportant"];
				frmInboxHome.lblNoNotfn.isVisible = false;
				/* -- TODAY
				if(gblInboxSortedOrNormal == false){
					if(flowSpa)
					frmInboxHome.hbox104116120741733.isVisible = true;
					else
					frmInboxHome.hbox477746511278608.isVisible = true;
					frmInboxHome.hbox477746511278660.isVisible = false;
				}
				else{
					//LET PREVIOUS STATE REMAIN
				}
				*/
				if(SortByType == kony.i18n.getLocalizedString("MyActivitiesIB_Date")){
					//frmInboxHome.btnDateTime.text = kony.i18n.getLocalizedString("MyActivitiesIB_Date");
					frmInboxHome.cbxDateTime.selectedKey = kony.i18n.getLocalizedString("MyActivitiesIB_Date");
					frmInboxHome.hbox477746511278660.isVisible = false;
				}
				else{
					if(SortByOrder == kony.i18n.getLocalizedString("keyAscending")){
						//frmInboxHome.btnDateTime.text = kony.i18n.getLocalizedString("keySender");
						frmInboxHome.cbxDateTime.selectedKey = kony.i18n.getLocalizedString("keySender");
						frmInboxHome.hbox477746511278660.isVisible = true;
						//frmInboxHome.btnAscenDescend.text = kony.i18n.getLocalizedString("keyAscending");
					}
					else if(SortByOrder == kony.i18n.getLocalizedString("keyDescending")){
						//frmInboxHome.btnDateTime.text = kony.i18n.getLocalizedString("keySender");
						frmInboxHome.cbxDateTime.selectedKey = kony.i18n.getLocalizedString("keySender");
						frmInboxHome.hbox477746511278660.isVisible = true;
						//frmInboxHome.btnAscenDescend.text = kony.i18n.getLocalizedString("keyDescending");
					}
				}
				populateInboxNormalState();
				//SortByType = "Date/Time";
			}
			else{
				frmInboxHome.lblNoNotfn.isVisible = true;
				frmInboxHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoMessages");
				ServiceResultInbox.length = 0;
				//frmInboxHome.btnCancel.text = "";
				frmInboxHome.btnCancel.isVisible = false;
				//frmInboxHome.btnCancel.skin = "btnBlueDark";
				//frmInboxHome.btnCancel.focusSkin = "btnBlueDark";
				//frmInboxHome.btnRight.text = "DeleteDisabled";
				frmInboxHome.btnRight.skin = "btnDeleteActive";
				frmInboxHome.btnRight.focusSkin = "btnDeleteActive";
				frmInboxHome.btnRight.setEnabled(false);
				frmInboxHome.btnRight.isVisible = false;
				frmInboxHome.hbox477746511278660.isVisible = false;
				frmInboxHome.hbox477746511278608.isVisible = false;
				
				frmInboxHome.segMyRecipient.removeAll();
			}
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			frmInboxHome.segMyRecipient.removeAll();
		}
		kony.application.dismissLoadingScreen();
	}
}

function populateInboxNormalState(){
	showLoadingScreen();
	locale = kony.i18n.getCurrentLocale();
	InboxHomePageState = false;
	populateInbox();
	frmInboxHome.segMyRecipient.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
	frmInboxHome.segMyRecipient.onRowClick = getInboxDetailsMB;
	//frmInboxHome.btnCancel.text = "";
	frmInboxHome.btnCancel.isVisible = false;
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad"|| gblDeviceInfo.name == "iPhone Simulator"){
		frmInboxHome.btnCancel.isVisible = true;
		frmInboxHome.btnCancel.skin = "btnBlueDark";
		frmInboxHome.btnCancel.focusSkin = "btnBlueDark";
		frmInboxHome.btnCancel.onClick = InboxCancelOnClick;
	}
	//frmInboxHome.btnRight.text = "Delete";
	frmInboxHome.btnRight.setEnabled(true);
	frmInboxHome.btnRight.skin = "btnDeleteDB";
	frmInboxHome.btnRight.focusSkin = "btnDeleteDB";
	frmInboxHome.btnRight.isVisible = true;
	kony.application.dismissLoadingScreen();
}

function populateInboxDeleteState(){
	showLoadingScreen();
	locale = kony.i18n.getCurrentLocale();
	InboxHomePageState = true;
	populateInbox();
	frmInboxHome.segMyRecipient.selectionBehavior = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
	frmInboxHome.segMyRecipient.onRowClick = SelectForDeleteTrackerInbox;
	//frmInboxHome.btnCancel.text = "Cancel";
	frmInboxHome.btnCancel.isVisible = true;
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo.name == "iPhone Simulator"){
		frmInboxHome.btnCancel.skin = "btnCancel";
		frmInboxHome.btnCancel.focusSkin = "btnCancel";
	}
	frmInboxHome.btnCancel.onClick = InboxCancelOnClick;
	//frmInboxHome.btnRight.text = "DeleteNo";
	frmInboxHome.btnRight.setEnabled(true);
	frmInboxHome.btnRight.skin = "btnDeleteActive";
	frmInboxHome.btnRight.focusSkin = "btnDeleteActive";
	frmInboxHome.btnRight.isVisible = true;
	frmInboxHome.btnRight.onClick=InboxDeleteOnClick;
	kony.application.dismissLoadingScreen();
}

function SelectForDeleteTrackerInbox(){
	if(frmInboxHome.segMyRecipient.selectedItems != null){
		//frmInboxHome.btnRight.text = "DeleteYes";
		frmInboxHome.btnRight.skin = "btnDeleteDB";
		frmInboxHome.btnRight.focusSkin = "btnDeleteDB";
	}
	else{
		//frmInboxHome.btnRight.text = "DeleteNo";
		frmInboxHome.btnRight.skin = "btnDeleteActive";
		frmInboxHome.btnRight.focusSkin = "btnDeleteActive";
	}
	//#ifdef android
	    //if(frmNotificationHome.segMyRecipient.selectedIndex != null)
		frmInboxHome.segMyRecipient.selectedIndex = frmInboxHome.segMyRecipient.selectedIndex;
	//#endif
}

function populateInbox(){
	gblInboxDetailsCache.length = 0;
	gblInboxDetailsCacheForSort.length = 0;
	var InboxOnDate = new Array(); 
	var localeSup = "EN";
	var skinImp = null;
	var skinRowRead = null;
	if(locale == "th_TH"){
		localeSup = "TH";
	}
	var onclickBTNI = null;
	if(!InboxHomePageState){
		onclickBTNI = markInboxImportantMB;
	}
	if(ServiceResultInbox.length > 0 ){
		var date = ServiceResultInbox[0]["date"]
		for(var i = 0; i < ServiceResultInbox.length; i++){
			skinImp = "btnImpN";
			//skinRowRead = "segRowReadN";
			skinRowRead = "hbxbggray";
			skinLblNotfnType="lblBlackMediumNormal";
			skinLblNotfnSub="lblBlack114Normal";
			
			if(ServiceResultInbox[i]["imp"] == "Y"){
				skinImp = "btnImpY";
			}
			if(ServiceResultInbox[i]["read"] == "Y"){
				skinRowRead = "hboxWhiteback";
				
				skinLblNotfnType="lblBlackMedium";
		        skinLblNotfnSub="lblBlack114";
			    
			}
			if(date == ServiceResultInbox[i]["date"]){
				var timeR = cutTime(ServiceResultInbox[i]["time"]);
				//var timeR = time24toAMPM(ServiceResultInbox[i]["time"]);
				var tempRecord = {
					hbox101086657956686: {
						skin: skinRowRead
					},	
					ID: ServiceResultInbox[i]["id"],
					lblNotfnFrom: {
						text: kony.i18n.getLocalizedString("Transfer_from") + " " + ServiceResultInbox[i]["sender"+localeSup],
						skin: skinLblNotfnType
					},
					lblNotfnSub: {
						text: ServiceResultInbox[i]["subject"+localeSup],
						skin: skinLblNotfnSub
					},
					lblTime: {
						text: timeR
						
					},
					btnImp: {
						skin: skinImp, onClick: onclickBTNI
					},
					imgArrow: {
						src: "navarrow3.png"
					}
				}
				if(InboxHomePageState) {
					tempRecord["imgDeleteSelect"] = "chkbox.png";
				}
				//
				InboxOnDate.push(tempRecord);
				gblInboxDetailsCacheForSort.push(tempRecord);
			}
			else{
				var tempPushArrayInbox = new Array();
				var dateR = dateFormatTMB(date);
				tempPushArrayInbox.push({lblDateHeader: dateR});
				tempPushArrayInbox.push(InboxOnDate);
				gblInboxDetailsCache.push(tempPushArrayInbox);
				var InboxOnDate = new Array();
				date = ServiceResultInbox[i]["date"];
				i = i - 1;
			}
		}
		var tempPushArrayInbox = new Array();
		var dateR = dateFormatTMB(date);
		tempPushArrayInbox.push({lblDateHeader: dateR});
		tempPushArrayInbox.push(InboxOnDate);
		gblInboxDetailsCache.push(tempPushArrayInbox);
		var InboxOnDate = new Array();
		// commented above line to fix DEF181
		//frmInboxHome.segMyRecipient.removeAll();
		
		//if(gblInboxSortedOrNormal == true){
		if(SortByType == "001"){
			if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo.name == "iPhone Simulator"){
				//frmInboxHome.scrollbox474135225108084.containerHeight = 71;
			}
			if(SortByOrder =="001"){
				gblInboxDetailsCacheForSort.sort(dynamicSort("-lblNotfnFrom"));
			}
			else if(SortByOrder == "000"){
				gblInboxDetailsCacheForSort.sort(dynamicSort("lblNotfnFrom"));
			}
			//frmInboxHome.segMyRecipient.data = gblInboxDetailsCacheForSort;
			//commented above 1 line and add below line to fix DEF181
			frmInboxHome.segMyRecipient.setData(gblInboxDetailsCacheForSort);
		}
		else{
			if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo.name == "iPhone Simulator"){
				//frmInboxHome.scrollbox474135225108084.containerHeight = 79;
			}
			//frmInboxHome.segMyRecipient.data = gblInboxDetailsCache;
			//commented above 1 line and add below line to fix DEF181
			frmInboxHome.segMyRecipient.setData(gblInboxDetailsCache);
		}
	} else {
				frmInboxHome.lblNoNotfn.isVisible = true;
				frmInboxHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoMessages");
				ServiceResultInbox.length = 0;
	}
	
}

//GET NOTIFICATION DETAILS
function getInboxDetailsMB(){
	showLoadingScreen();
	var inputParams = {
		messageId : frmInboxHome.segMyRecipient.selectedItems[0].ID
	};
	invokeServiceSecureAsync("inboxdetails", inputParams, getInboxDetailsMBCB)
}

function getInboxCampaignImageSize(){
	var screenwidth = gblDeviceInfo["deviceWidth"];
	//alert("screenwidth : "+screenwidth);
	var refHeight = 225;
	var refWidth = 300;
	if (gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"] == "spa") {
		if (screenwidth == 414) {
			refHeight = 291;
			refWidth = 1164;
		} else if (screenwidth == 375) {
			refHeight = 264;
			refWidth = 1056;
		} else if (screenwidth == 360) {
			refHeight = 252;
			refWidth = 1014;
		} else if (screenwidth == 384) {
			refHeight = 270;
			refWidth = 1080;
		}
	} else {
		if (gblDeviceInfo["name"] == "iPhone") {
			if (gblDeviceInfo["model"] == "iPhone 6 Plus") {
				refHeight = 291;
				refWidth = 388;
			} else if (gblDeviceInfo["model"] == "iPhone 6") {
				refHeight = 263;
				refWidth = 352;
			}
		} else if (gblDeviceInfo["name"] == "android") {	
			if (screenwidth == 1080) {	
				refHeight = 253;
				refWidth = 1012;
			} else if (screenwidth == 720) {
				refHeight = 253;
				refWidth = 675;
			} else if (screenwidth == 768) {
				refHeight = 253;
				refWidth = 675;
			}
		}
	}	
	//alert("refHeight : "+refHeight+" , refWidth : "+refWidth);
	//gblCampaignRefHeight = refHeight;
	//gblCampaignRefWidth = refWidth;
	//alert("gblCampaignRefHeight : "+gblCampaignRefHeight+" , gblCampaignRefWidth : "+gblCampaignRefWidth);
	
	return refWidth + "-" + refHeight;

}
function getInboxDetailsMBCB(status, resultset){
	gblCampaignDataENIL = "";
	gblCampaignDataTHIL = "";
	gblCampaignDataENEL = "";
	gblCampaignDataTHEL = "";
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			if(resultset["Result"].length > 0){
				//to fix DEF14743, uncommented the first four fields
				if(undefined != resultset["Result"][0]["INBOX_LINK_FLAG_MOB"]){
					gblCmpIntExt=resultset["Result"][0]["INBOX_LINK_FLAG_MOB"];
				}else {
					gblCmpIntExt = "00";
				}
				
				if(!flowSpa)
					{
						if(gblCmpIntExt != "00")
						 {
						 frmInboxDetails.hboxPromoMB.setVisibility(true);
						 frmInboxDetails.hboxMB.setVisibility(false);
						 }else{
						 frmInboxDetails.hboxPromoMB.setVisibility(false);
						 frmInboxDetails.hboxMB.setVisibility(true);
						  }
					 }
				 else{
				        if(gblCmpIntExt != "00")
						 {
						 frmInboxDetails.hboxPromoMB.setVisibility(true);
						 frmInboxDetails.hboxSPA.setVisibility(false);
						 }else{
						 frmInboxDetails.hboxPromoMB.setVisibility(false);
						 frmInboxDetails.hboxSPA.setVisibility(true);
						 
						  }
					 }
				 
					 
				if(gblCmpIntExt == "01")
				{
				  gblCampaignDataEN=resultset["Result"][0]["INBOX_INTERNAL_LINK_MOB_EN"];
				  gblCampaignDataTH=resultset["Result"][0]["INBOX_INTERNAL_LINK_MOB_TH"];
				}
				else if (gblCmpIntExt == "02")
				{
				  gblCampaignDataEN=resultset["Result"][0]["INBOX_EXTERNAL_LINK_MOB_EN"];
				  gblCampaignDataTH=resultset["Result"][0]["INBOX_EXTERNAL_LINK_MOB_TH"];
				} else if (gblCmpIntExt == "03")
				{
				  	gblCampaignDataENIL = resultset["Result"][0]["INBOX_INTERNAL_LINK_MOB_EN"];
				    gblCampaignDataTHIL = resultset["Result"][0]["INBOX_INTERNAL_LINK_MOB_TH"];
				    gblCampaignDataENEL = resultset["Result"][0]["INBOX_EXTERNAL_LINK_MOB_EN"];
				    gblCampaignDataTHEL = resultset["Result"][0]["INBOX_EXTERNAL_LINK_MOB_TH"];
				}
				tempInboxDetails = {
						lblNotfnSubEN : resultset["Result"][0]["subjectEN"],
						lblNotfnSubTH : resultset["Result"][0]["subjectTH"],
						lblNotfnMessageEN : resultset["Result"][0]["messageEN"],
						lblNotfnMessageTH : resultset["Result"][0]["messageTH"],
						imgDetailMBEN : resultset["Result"][0]["imageMBEN"],
						imgDetailMBTH : resultset["Result"][0]["imageMBTH"],
						lblNotfnFromEN :" " + resultset["Result"][0]["senderEN"],
						lblNotfnFromTH :" " + resultset["Result"][0]["senderTH"],
						cmpCode : resultset["Result"][0]["cmpCode"],
						cmpType : resultset["Result"][0]["cmpType"],
						lblNotfnDate :" " + dateFormatTMB(resultset["Result"][0]["date"]) + ", " + cutTime(resultset["Result"][0]["time"])
				}
				// below code is added to capture the campaign response for inbox
				var cmpCode = resultset["Result"][0]["cmpCode"];
				var cmpType = resultset["Result"][0]["cmpType"];
				var cmpEndDate = resultset["Result"][0]["cmpEndDate"];
				gblCampaignResp = "frmInboxHome" + "~" + cmpType + "~" + cmpCode + "~" + cmpEndDate + "~" + "02" + "~" + "I";
				if(cmpType != 'M'){
					//if(langSwitch == 'N'){
						campaignBannerCount("D","I");
					//}
				}
				
				//Code changed for IOS9 upgrade
				populateInboxDetails();
				frmInboxDetails.show();
			}
		}
		else if(resultset["status"] == "2"){
			showAlertRcMB(kony.i18n.getLocalizedString("keyMessageDeleted"), kony.i18n.getLocalizedString("info"), "info")
			gblInboxHistoryModified = true;
			frmInboxHome.show();
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	kony.application.dismissLoadingScreen();
}

function populateInboxDetails(){
	var localeSup = "EN";
	//Begin MIB-3282
	locale = kony.i18n.getCurrentLocale();
	//End MIB-3282
	if(locale == "th_TH"){
		localeSup = "TH";
	}
	if(tempInboxDetails["imgDetailMB"+localeSup] == null || tempInboxDetails["imgDetailMB"+localeSup] == undefined || tempInboxDetails["imgDetailMB"+localeSup] == ""){
		frmInboxDetails.imgDetail.isVisible = false;
	}
	else{
		//getting refHeight and refWidth by calling below function
		var refHeight =225;
		var refWidth =300;
        var refWidthAndHeight = getInboxCampaignImageSize();
		refWidth =  parseInt(refWidthAndHeight.split("-")[0]);
		refHeight = parseInt(refWidthAndHeight.split("-")[1]);
		
		if(flowSpa){
		frmInboxDetails.imgDetail.imageScaleMode= constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO;
		}
		frmInboxDetails.imgDetail.src = null;
		frmInboxDetails.imgDetail.referenceHeight = refHeight; //gblCampaignRefHeight;
		frmInboxDetails.imgDetail.referenceWidth = refWidth; //gblCampaignRefWidth;
		frmInboxDetails.imgDetail.src = tempInboxDetails["imgDetailMB"+localeSup];
		frmInboxDetails.imgDetail.isVisible = true;
	}
	//frmInboxDetails.imgDetail.src = tempInboxDetails["imgDetailMB"+localeSup];
	frmInboxDetails.lblNotfnFrom.text = tempInboxDetails["lblNotfnFrom"+localeSup];
	frmInboxDetails.lblNotfnSub.text = tempInboxDetails["lblNotfnSub"+localeSup];
	frmInboxDetails.lblNotfnDate.text = tempInboxDetails["lblNotfnDate"];
	frmInboxDetails.richNotfnMessage.text = tempInboxDetails["lblNotfnMessage"+localeSup];
	unreadInboxMessagesMBLogin();
}

//DELETE NOTIFICATION/S
function deleteInboxMB(){
	showLoadingScreen();
	var messages = "";
	var length = frmInboxHome.segMyRecipient.selectedItems.length;
	for(var i = 0; i < length; i++){
		if(i == (length - 1)){
			messages = messages + frmInboxHome.segMyRecipient.selectedItems[i].ID;
		}
		else{
			messages = messages + frmInboxHome.segMyRecipient.selectedItems[i].ID + ",";
		}
	}
	var inputParams = {
		messageId : messages
	};
	invokeServiceSecureAsync("inboxdelete", inputParams, deleteInboxMBCB)
}

function deleteInboxMBCB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			gblInboxHistoryModified = true;
			frmInboxHome.show();
			unreadInboxMessagesMBLogin();
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	kony.application.dismissLoadingScreen();
}

//MARK NOTIFICATION IMPORTANT
function markInboxImportantMB(){
	showLoadingScreen();
	var imp = frmInboxHome.segMyRecipient.selectedItems[0]["btnImp"]["skin"];
	var ind = frmInboxHome.segMyRecipient.selectedIndex;
	var imp1 = ind[0];
	var imp2 = ind[1];
	if(SortByType == kony.i18n.getLocalizedString("MyActivitiesIB_Date")){
		var msgID = gblInboxDetailsCache[imp1][1][imp2]["ID"];
	}
	else{            
		var msgID = frmInboxHome.segMyRecipient.selectedItems[0]["ID"];
	}
	var impStat = "Y";
	if(imp == "btnImpY"){
		impStat = "N";
	}
	var inputParams = {
		messageId : msgID,
		impStatus : impStat
	};
	invokeServiceSecureAsync("inboximportant", inputParams, markInboxImportantMBCB)
}

function markInboxImportantMBCB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			kony.application.dismissLoadingScreen();
		}
		else if(resultset["status"] == "0"){
		//	gblInboxnHistoryModified = true;
			frmInboxHome.show();
		}
		else if(resultset["status"] == "2"){
			if(resultset["error"] == "MaxLimitReached"){
				showAlertRcMB(kony.i18n.getLocalizedString("keyMessagesMaxLimit") + " " + gblMaxSetImportant + " " + kony.i18n.getLocalizedString("keyMessagesMaxLimit1"), kony.i18n.getLocalizedString("info"), "info");
				kony.application.dismissLoadingScreen();
			}
			else if(resultset["error"] == "InboxDeleted"){
				gblInboxHistoryModified = true;
				frmInboxHome.show();
			}
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			kony.application.dismissLoadingScreen();
		}
	}else{
		kony.application.dismissLoadingScreen();
	}
	//kony.application.dismissLoadingScreen();
}

function popupDeleteConfirmInbox(){
	popUpNotfnDelete.dismiss();
	deleteInboxMB();
}

function popupDeleteCancelInbox(){
	popUpNotfnDelete.dismiss();
}

function popupSortTypeFunc(){
	
    var sortBy = frmInboxHome.cbxDateTime.selectedKey; //popInboxSortBy.segBanklist.selectedItems[0].lblSortType;

	
	if(sortBy == SortByType){
		//NO CHANGE
	} else{
		if(sortBy == "000"){
			//FILL SERVICE RETURNED DATA
			SortByType = "000";
			if(InboxHomePageState){
				gblInboxSortedOrNormal = false;
				populateInboxDeleteState();
			}
			else{
				gblInboxSortedOrNormal = false;
				populateInboxNormalState();
			}
			frmInboxHome.hbox477746511278660.isVisible = false;
		}
		else if(sortBy == "001"){
			gblInboxDetailsCacheForSort.sort(dynamicSort("lblNotfnFrom"));
			SortByType = "001";
			
			if(InboxHomePageState){
				gblInboxSortedOrNormal = true;
				populateInboxDeleteState();
			}
			else{
				gblInboxSortedOrNormal = true;
				populateInboxNormalState();
			}
			frmInboxHome.hbox477746511278660.isVisible = true;
			//frmInboxHome.btnAscenDescend.text = kony.i18n.getLocalizedString("keyAscending");
			//frmInboxHome.cbxAscenDescend.selectedKey = kony.i18n.getLocalizedString("keyAscending");
		}
	}
	//popInboxSortBy.dismiss();
}

function popupSortOrderFunc(){

	var sortBy = frmInboxHome.cbxAscenDescend.selectedKey; //popInboxSortBy2.segBanklist.selectedItems[0].lblSortType;
	
	
	if(sortBy == SortByOrder){
		//NO CHANGE
	}
	else{
		if(sortBy == "000"){
			
			gblInboxDetailsCacheForSort.sort(dynamicSort("lblNotfnFrom"));
			
			SortByOrder = "000";
		}
		else if(sortBy == "001"){
		
			gblInboxDetailsCacheForSort.sort(dynamicSort("-lblNotfnFrom"));
			
			SortByOrder = "001";
		}
		if(InboxHomePageState){
			gblInboxSortedOrNormal = true;
			populateInboxDeleteState();
		}
		else{
			gblInboxSortedOrNormal = true;
			populateInboxNormalState();
		}
	}
	//popInboxSortBy2.dismiss();
}

function InboxDeleteOnClick(eventobject){
	if(eventobject.skin == "btnDeleteDB"){
		if(InboxHomePageState == false){
			populateInboxDeleteState();
		}
		else{
			//deleteNotificationMB();
			var msg = kony.i18n.getLocalizedString("keyNotSure")+" "+ frmInboxHome.segMyRecipient.selectedItems.length+" "+kony.i18n.getLocalizedString("keyMessages");
			popUpNotfnDelete.lblNotfnMsg.text = msg;
			popUpNotfnDelete.btnPopDeleteCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
			popUpNotfnDelete.btnPopDeleteYEs.text = kony.i18n.getLocalizedString("keyYes");
			popUpNotfnDelete.btnPopDeleteCancel.onClick = popupDeleteCancelInbox;
			popUpNotfnDelete.btnPopDeleteYEs.onClick = popupDeleteConfirmInbox;
			popUpNotfnDelete.show();
		}
	}
	else if(eventobject.skin == "btnDeleteActive"){
		//do nothing
	}
}

function InboxCancelOnClick(eventobject){
	if(eventobject.skin == "btnCancel"){
		InboxHomePageState = false;
		locale = kony.i18n.getCurrentLocale();
		populateInboxNormalState();
	}
	else{
		//do nothing
	}
}

function unreadInboxMessagesTrackerLocalDBSync(){
	//showLoadingScreen();
    //showLoadingScreenWithNoIndicator();
	//var inputParams = {};
	//invokeServiceSecureAsync("getinboxunreadcount", inputParams, unreadInboxMessagesTrackerLocalDBSyncCB)
	unreadInboxMessagesTrackerLocalDBSyncCB();
}

function unreadInboxMessagesTrackerLocalDBSyncCB(){

		/*if(resultset["status"] == "1"){
			gblUnreadCount = "0";
		}
		else if(resultset["status"] == "0"){
			gblUnreadCount = resultset["unread"];
		}
		else{
			gblUnreadCount = "0";
		}*/
		gblMyInboxTotalCountMB = parseInt(gblUnreadCount) + parseInt(gblMessageCount);
		/*
		if(gblFlagMenu == "MyInbox"){
			gblNotificationData = [];
	    	gblMessageData = [];
	    	//changes done for MyInbox badge requirement
	    	gblNotificationData = {
	            "text": gblUnreadCount,
	            "skin": "btnBadgeSmall"
	        };
	        gblMessageData = {
	            "text": gblMessageCount,
	            "skin": "btnBadgeSmall"
	        };
		    var SegMyinboxTable = [{
		        lblNotification: kony.i18n.getLocalizedString("keylblNotification"),
		        btnBadge: gblNotificationData,
		        imgArrow: "navarrow.png"
		    }, {
		        lblNotification: kony.i18n.getLocalizedString("keylblmessage"),
		        btnBadge: gblMessageData,
		        imgArrow: "navarrow.png"
		    }]
		}
		*/
		handleMenuBtn();
		
	
}


/*
function unreadMyInboxMessageCountMB(status, resultset){
	try{
		
		if(status == 400){
			if(resultset["status"] == "1"){
				gblMessageCount = "0";
			}
			else if(resultset["status"] == "0"){
				gblMessageCount = resultset["unread"];
			}
			else{
				gblMessageCount = "0";
			}
			gblMyInboxTotalCountMB = parseInt(gblUnreadCount) + parseInt(gblMessageCount);
			if(gblFlagMenu == "MyInbox"){
				gblNotificationData = [];
		    	gblMessageData = [];
		    	//changes done for MyInbox badge requirement
		    	if (gblMyInboxTotalCountMB <= 9) {
			        gblNotificationData = {
			            "text": gblUnreadCount,
			            "skin": "btnBadgeSmall"
			        };
			        gblMessageData = {
			            "text": gblMessageCount,
			            "skin": "btnBadgeSmall"
			        };
		    	} else {
			        gblNotificationData = {
			            "text": gblUnreadCount,
			            "skin": "btnBadgeLarge"
			        };
			        gblMessageData = {
			            "text": gblMessageCount,
			            "skin": "btnBadgeLarge"
			        };
			    }
			    var SegMyinboxTable = [{
			        lblNotification: kony.i18n.getLocalizedString("keylblNotification"),
			        btnBadge: gblNotificationData,
			        imgArrow: "navarrow.png"
			    }, {
			        lblNotification: kony.i18n.getLocalizedString("keylblmessage"),
			        btnBadge: gblMessageData,
			        imgArrow: "navarrow.png"
			    }]
			}
			kony.application.dismissLoadingScreen();
			handleMenuBtnCB();
		}
	}catch(e){
		
	}
}*/

function unreadInboxMessagesMBLogin(){
	//showLoadingScreen();
    //showLoadingScreenWithNoIndicator();
	var inputParams = {};
	invokeServiceSecureAsync("getinboxunreadcount", inputParams, unreadInboxMessagesMBLoginCallBack)

}

function unreadInboxMessagesMBLoginCallBack(status,resultset){
	if (status == 400) {
		if(resultset["status"] == "1"){
			gblUnreadCount = "0";
		}
		else if(resultset["status"] == "0"){
			gblUnreadCount = resultset["unread"];
		}
		else{
			gblUnreadCount = "0";
		}
		//if(kony.string.equalsIgnoreCase("iphone", kony.os.deviceInfo().name))
//			kony.application.setApplicationBadgeValue(gblUnreadCount);
		if(flowSpa)
			unreadMyInboxMessageCountMB(status, resultset);
		else	menuMBMessageUpdateCount();	
	}
}

function menuMBMessageUpdateCount()
{	
	//showLoadingScreen();
	var inputParams = {};
	invokeServiceSecureAsync("getInboxMessagesUnreadCount", inputParams, unreadMyInboxMessageCountMB)
}
function unreadMyInboxMessageCountMB(status, resultset){
	try{
		
		if(status == 400){
			if(resultset["status"] == "1"){
				gblMessageCount = "0";
			}
			else if(resultset["status"] == "0"){
				gblMessageCount = resultset["unread"];
			}
			else{
				gblMessageCount = "0";
			}
			var badgeCount =  parseInt(gblUnreadCount) + parseInt(gblMessageCount);
			if(kony.string.equalsIgnoreCase("iphone", gblDeviceInfo.name) || kony.string.equalsIgnoreCase("iPad", gblDeviceInfo.name)){
				kony.application.setApplicationBadgeValue(badgeCount);
			}
			//unreadInboxMessagesTrackerLocalDBSync();
			//kony.application.dismissLoadingScreen();
		}
	}catch(e){
		
	}
}
function frmInboxHome_btnHdrMenu_onClick() {
//	  frmInboxHome.scrollboxMain.scrollToEnd();
//    gblIndex = -1;
//    isMenuShown = false;
    unreadInboxMessagesTrackerLocalDBSync();
};