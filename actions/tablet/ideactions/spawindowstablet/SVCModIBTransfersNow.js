/**************************************************************************************
		Module	: callBackFromXferAccountsIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function pulls  eligible From accounts for  transfers
*****************************************************************************************/
function getFromXferAccountsIB() {
    if (!gblTransferFromRecipient) {
        frmIBTransferCustomWidgetLP.lblXferToNameRcvd.text = ""; // gblAccountTable["custAcctRec"][gblIndex]["accountName"];
        frmIBTransferCustomWidgetLP.lblXferToContactRcvd.text = ""; //gblAccountTable["custAcctRec"][gblIndex]["accountNoFomatted"];
        frmIBTransferCustomWidgetLP.lblXferToBankNameRcvd.text = ""; //"TMB";
        //Image SOURCE should NEVER be NULL. If it is NULL, then we'll get 404 error in IE8 DESKTOP WEB SCREENS
        frmIBTransferCustomWidgetLP.imgXferToImage.src = "transparent.png"; //gblMyProfilepic;
    }
    displayITMXFeeP2P(false);
    frmIBTranferLP.lblMobileNumberTemp.text = "";
    gblshotcutToACC = false;
    GBLFINANACIALACTIVITYLOG = {};
    var inputParam = {};
    inputParam["transferFlag"] = "true";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackTransferFromAccountsInBk)
}
var fromData = []

function callBackTransferFromAccountsInBk(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            masterConfigurationForTransfers(resulttable); // caching configuration
            if (resulttable["BankList"] != null) {
                loadBankListForTransfers(resulttable["BankList"]);
            } // transfer REdesign calling bank list in biggining and storing in global variable
            ITMX_TRANSFER_ENABLE = resulttable["ITMX_TRANSFER_ENABLE"];
            ITMX_TRANSFER_FEE_LIMITS = resulttable["ITMX_TRANSFER_FEE_LIMITS"];
            var setyourIDTransfer = resulttable["setyourIDTransfer"];
            enableP2PTxnIB(setyourIDTransfer);
            var count = 0;
            fromData = []
            gblTransfersOtherAccounts = [];
            if (resulttable.custAcctRec != null) {
                var length = resulttable["custAcctRec"].length;
                gblNoOfFromAcs = resulttable.custAcctRec.length;
                var nonCASAAct = 0;
                for (j = 0; j < length; j++) {
                    var accountStatus = resulttable["custAcctRec"][j].acctStatus;
                    var anyIDEligible = true;
                    if (gblSelTransferMode == 2 || gblSelTransferMode == 3) // if p2 p show only anyID eligible
                    {
                        anyIDEligible = resulttable.custAcctRec[j].anyIdAllowed == "Y";
                    }
                    if (accountStatus.indexOf("Active") == -1) {
                        nonCASAAct = nonCASAAct + 1;
                        //showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
                        //return false;
                    }
                    if (accountStatus.indexOf("Active") >= 0) {
                        var AcctStatus = resulttable.custAcctRec[j].personalisedAcctStatusCode;
                        var isAllowed = true;
                        if (recipientAddFromTransfer || gblTransferFromRecipient) {
                            if (gblisTMB == gblTMBBankCD) {
                                if (resulttable.custAcctRec[j].isOtherTMBAllowed != "Y") {
                                    isAllowed = false;
                                }
                            } else {
                                if (resulttable.custAcctRec[j].isOtherBankAllowed != "Y") {
                                    isAllowed = false;
                                }
                            }
                        }
                        if (AcctStatus == undefined || AcctStatus == "01" || AcctStatus == "") {
                            var availableBalance = resulttable["custAcctRec"][j]["availableBal"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
                                // code for product icons in custom widgets
                            var icon = "";
                            icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable["custAcctRec"][j]["ICON_ID"] + "&modIdentifier=PRODICON";
                            var acctID = resulttable.custAcctRec[j].accId;
                            if (acctID.length == 14) acctID = acctID.substr(4, 10);
                            var accountName = resulttable["custAcctRec"][j]["acctNickName"]
                            if (accountName == "" || accountName == undefined) {
                                var acctnum = acctID
                                acctnum = acctnum.substr(6, 4)
                                accountName = resulttable["custAcctRec"][j]["productNmeEN"] + " " + acctnum
                            }
                            //checking for Remaining Fee transactions	
                            var lblRem = ""
                            var remanFee = ""
                            if (resulttable.custAcctRec[j].keyremainingFree != "") {
                                lblRem = kony.i18n.getLocalizedString(resulttable.custAcctRec[j].keyremainingFree);
                                remanFee = parseFloat(resulttable["custAcctRec"][j]["remainingFee"]) < 0 ? 0 : resulttable["custAcctRec"][j]["remainingFee"];
                            } else {
                                lblRem = ""
                                remanFee = ""
                            }
                            var tempRecord = {};
                            if (count == 0) {
                                count = count + 1
                            } else if (count == 1) {
                                count = count + 1
                            } else if (count == 2) {
                                count = 0
                            }
                            tempRecord = createTransferSegmentRecordIB(resulttable["custAcctRec"][j], icon, lblRem, remanFee, accountName, acctID);
                            if (isAllowed && anyIDEligible) {
                                fromData.push(tempRecord);
                            }
                        } //end of checking for accounts status
                    }
                }
            } //
            if (nonCASAAct == resulttable.custAcctRec.length) {
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
                return false;
            }
            if (recipientAddFromTransfer || gblTransferFromRecipient) {
                if (fromData != null && fromData.length == 0) {
                    alert(kony.i18n.getLocalizedString("noEligibleFromAccount"));
                    recipientAddFromTransfer = false;
                    gblTransferFromRecipient = false;
                    resetContentsIB();
                    callBackTransferFromAccountsInBk(status, resulttable);
                    return false;
                }
            }
            if (fromData != null && fromData.length == 0) {
                showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
                return false;
            }
            populateOtherAccountsForRecipients(resulttable)
            frmIBTransferCustomWidgetLP.custom1016496273208904.data = fromData;
            frmIBTransferCustomWidgetLP.custom1016496273208904.onSelect = customWidgetSelectEventIBTransfer;
            dismissLoadingScreenPopup();
            setTabBankAccountOrMobile(false);
            frmIBTransferCustomWidgetLP.show();
            nameofform = kony.application.getCurrentForm();
            if (window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8") langspecificmenuIE8();
            else langspecificmenu();
            if (kony.i18n.getCurrentLocale() != "th_TH") nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocus"
            else nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocusThai"
            nameofform.segMenuOptions.removeAll();
            //frmIBTransferCustomWidgetLP.hbox47679117769442.skin = "hboxLightBlue";
        } else if (resulttable["opstatus"] == 1) {
            showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
        } else {
            dismissLoadingScreenPopup();
            alert(" " + resulttable["errMsg"]);
        }
    }
}
/*************************************************************************

	Module	: getTDAccountIB
	Author  : Kony
	Purpose : getting data of TD Account

****/
function getTDAccountIB() {
    var inputParam = {}
    var fromAcctID = gblcwselectedData.accountWOF;
    fromAcctID = replaceCommon(fromAcctID, "-", "");
    if (fromAcctID.length == 14) fromAcctID = fromAcctID.substr(4, 10);
    inputParam["acctIdentValue"] = fromAcctID;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tDDetailinq", inputParam, callBackTDAccountIB)
}
/**************************************************************************************
		Module	: callBackTDAccountIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : Fetches  TD Account details from tDDetailinq service
*****************************************************************************************/
function callBackTDAccountIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var key = "";
            var value = "";
            var tdData = [];
            var tdXferData = [];
            var temp = []
            temp[0] = "a," + "b," + 0;
            temp[1] = kony.i18n.getLocalizedString("keySelectMaturityDate");
            kony.table.insert(tdXferData, temp);
            for (var i = 0; i < resulttable.tdDetailsRec.length; i++) {
                var dumTemp = {
                    maturityDate: resulttable.tdDetailsRec[i].maturityDate,
                    depositAmtVal: resulttable.tdDetailsRec[i].depositAmtVal,
                    depositNo: resulttable.tdDetailsRec[i].depositNo
                }
                kony.table.insert(tdData, dumTemp);
            }
            tdData.sort(function(a, b) {
                var dateA = a.maturityDate;
                dateA = dateformatIB(dateA);
                var dateB = b.maturityDate;
                dateB = dateformatIB(dateB);
                return kony.os.compareDates(dateA, dateB, "dd/mm/yyyy") //default return value (no sorting)
            })
            for (var i = 0; i < tdData.length; i++) {
                var dummy = [];
                key = dateformatIB(tdData[i].maturityDate) + "," + tdData[i].depositAmtVal + "," + (i + 1);
                value = kony.i18n.getLocalizedString("keyMaturityDate") + dateformatIB(tdData[i].maturityDate) + " " + kony.i18n.getLocalizedString("keyPrincipal") + tdData[i].depositAmtVal + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                gblTDDepositNo[i] = tdData[i].depositNo;
                gblXferTDAmt[i] = tdData[i].depositAmtVal;
                dummy[0] = key;
                dummy[1] = value;
                kony.table.insert(tdXferData, dummy);
            }
            dismissLoadingScreenPopup();
            frmIBTranferLP.hbxInsideMaturity.setVisibility(true);
            frmIBTranferLP.cmbxMaturityDetails.masterData = tdXferData;
            frmIBTranferLP.btnScheduledTransfer.setEnabled(false);
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function dateformatIB(date) {
    var currentDate = date.toString();
    var yearTD = currentDate.substr(0, 4);
    var dateTD = currentDate.substr(8, 2);
    var monthTD = currentDate.substr(5, 2);
    var date1 = dateTD + "/" + monthTD + "/" + yearTD;
    return date1;
}
/*************************************************************************

	Module	: depositAccountInqDreamSav
	Author  : Kony
	Purpose : Displays linked account for dream savings account

****/
function depositAccountInqDreamSav() {
    var AccntSumFlow = false;
    var inputparam = {};
    var fromData = frmIBTransferCustomWidgetLP.custom1016496273208904.data[gblCWSelectedItem];
    if (fromData == undefined || fromData.lblDummy == undefined) AccntSumFlow = true;
    if (!AccntSumFlow) {
        var fiident = fromData.lblDummy;
        var accId = fromData.accountWOF;
        var accNO = replaceCommon(accId, "-", "");
        var accType = fromData.lblSliderAccN2;
        inputparam["acctId"] = accNO;
    }
    if (AccntSumFlow) inputparam["acctId"] = replaceCommon(gblcwselectedData.accountWOF, "-", "");
    var status = invokeServiceSecureAsync("depositAccountInquiry", inputparam, depositAccountInqDreamSavCallBack);
}

function depositAccountInqDreamSavCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["linkedacc"].length == 0) {
                gblXferLnkdAccnts = "";
                frmIBTranferLP.lblXferToContactRcvd.text = "";
                //frmIBTranferLP.lblXferToNameRcvd.text =accountName //"Linked Account";
                frmIBTranferLP.imgXferToImage.setVisibility(false);
                frmIBTranferLP.btnXferShowContact.setVisibility(false);
                frmIBTranferLP.hbxNotifyRecipent.setVisibility(false);
                frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
                frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
                frmIBTranferLP.hbxEmail.setVisibility(false);
                frmIBTranferLP.hbxSMS.setVisibility(false);
                frmIBTranferLP.lineOne.setVisibility(false);
                frmIBTranferLP.lineThree.setVisibility(false);
                frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
                frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
                frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
                frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);
                gblTransEmail = 1;
                gblTrasSMS = 1;
                alert("" + kony.i18n.getLocalizedString("linkedAccNotfound"));
            } else {
                var strlen = resulttable["linkedacc"].length - 10;
                var linkAccount = resulttable["linkedacc"].substr(strlen, resulttable["linkedacc"].length)
                gblXferLnkdAccnts = linkAccount;
                gblBANKREF = "TMB"
                gblisTMB = gblTMBBankCD;
                frmIBTranferLP.vbox47679117772050.skin = "vboxWhiteBg";
                var accountName = "";
                var icon = gblMyProfilepic;
                for (var l = 0; l < gblAccountTable["custAcctRec"].length; l++) {
                    var acctId = gblAccountTable["custAcctRec"][l]["accId"]
                    var LinkAccntIdPad = "0000" + gblXferLnkdAccnts
                    if (gblXferLnkdAccnts == acctId || LinkAccntIdPad == acctId) {
                        accountName = gblAccountTable["custAcctRec"][l]["acctNickName"]
                        if (accountName == "" || accountName == undefined) {
                            var acctnum = gblXferLnkdAccnts;
                            acctnum = acctnum.substr(6, 4)
                            if (gblAccountTable["custAcctRec"][l]["ProductNameEng"] != undefined) {
                                accountName = gblAccountTable["custAcctRec"][l]["ProductNameEng"] + " " + acctnum
                            }
                        }
                        break;
                    }
                }
                frmIBTranferLP.imgXferToImage.src = icon; //"avatar_dis.png"
                frmIBTranferLP.lblXferToContactRcvd.text = addHyphenIB(gblXferLnkdAccnts);
                frmIBTranferLP.imgXferToImage.setVisibility(true);
                frmIBTranferLP.lblXferToNameRcvd.text = accountName //"Linked Account";
                frmIBTranferLP.lblXferToBankNameRcvd.text = "TMB";
                frmIBTranferLP.btnXferShowContact.setVisibility(false);
                frmIBTranferLP.hbxNotifyRecipent.setVisibility(false);
                frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
                frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
                frmIBTranferLP.hbxEmail.setVisibility(false);
                frmIBTranferLP.hbxSMS.setVisibility(false);
                frmIBTranferLP.lineOne.setVisibility(false);
                frmIBTranferLP.lineThree.setVisibility(false);
                frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
                frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
                frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
                frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);
                gblTransEmail = 1;
                gblTrasSMS = 1;
                dismissLoadingScreenPopup();
                if (accountName == "") {
                    frmIBTranferLP.lblXferToContactRcvd.text = "";
                    frmIBTranferLP.lblXferToBankNameRcvd.text = "";
                    frmIBTranferLP.imgXferToImage.setVisibility(false);
                    alert("" + kony.i18n.getLocalizedString("linkedAccNotfound"));
                }
            }
        } else {
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
            dismissLoadingScreenPopup();
        }
    }
}
/**************************************************************************************
		Module	: getToRecipientsAccountsIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function pulls  FROM reciepients for  transfers
*****************************************************************************************/
var index = -1;

function getToRecipientsAccountsIB() {
    if (gblSelTransferMode != 1) {
        showHideBankDetails(false);
        onClickP2pRecipientSegment();
    } else {
        showHideBankDetails(true);
        var checkCon = frmIBTranferLP.segXferRecipentsList.selectedItems[0].category;
        if (checkCon == "main") {
            Recp_category = 1;
            gblXferRecImg = frmIBTranferLP.segXferRecipentsList.selectedItems[0].imgXferRecipentsImage
            gblRecipientname = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsName
                //gblSelectedRecipentName = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsName
            var noOfAccounts = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsAccount;
            noOfAccounts = noOfAccounts.split(" ", 1);
            getTransferToRecipientsAccountsNewIB();
            if (noOfAccounts[0] == 1) {
                var selectedData = frmIBTranferLP.segXferRecipentsList.data[selectedRow + 1];
                selectRecipientAccount(true, selectedData);
            }
        } else if (checkCon == "own") {
            Recp_category = 2;
            gblRecipientname = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsName;
            var noOfAccounts = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsAccount;
            noOfAccounts = noOfAccounts.split(" ", 1);
            //gblSelectedRecipentName = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblXferRecipentsName
            getTMBOWNAccountsNewIB();
            if (noOfAccounts[0] == 1) {
                var selectedData = frmIBTranferLP.segXferRecipentsList.data[selectedRow + 1];
                selectRecipientAccount(true, selectedData);
            }
        } else if (checkCon == "own1" || checkCon == "main1") {
            frmIBTranferLP.segXferRecipentsList.removeAll()
            dismissLoadingScreenPopup();
            frmIBTranferLP.segXferRecipentsList.setData(segData)
        } else {
            selectRecipientAccount(false, "");
        }
    }
}

function showHideBankDetails(flag) {
    frmIBTranferLP.lblBankName.setVisibility(flag);
    frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfBank.setVisibility(flag);
    frmIBTransferNowCompletion.hbox457341938143754.setVisibility(flag);
}

function selectRecipientAccount(isOneAccount, oneAccountData) {
    gblSelectedRecipentName = gblRecipientname;
    gblXferPhoneNo = gblXferPhoneNo1;
    gblXferEmail = gblXferEmail1;
    recipientAddFromTransfer = false; //DEF1158 to clear if user clicks clicks to recipients manually
    gblTransferFromRecipient = false;
    if (Recp_category == "1") {
        frmIBTranferLP.vbox47679117772050.skin = "vboxWhiteBg";
        frmIBTranferLP.hbxNotifyRecipent.setVisibility(true);
        frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(true);
        frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(true);
        frmIBTransferNowCompletion.hbxTransNotif.setVisibility(true);
        frmIBTransferNowCompletion.hbxTransNtr.setVisibility(true);
        frmIBTranferLP.lineThree.setVisibility(false);
        frmIBTranferLP.lineOne.setVisibility(true);
        gblTransEmail = 0;
        gblTrasSMS = 0;
    } else {
        frmIBTranferLP.hbxNotifyRecipent.setVisibility(false);
        frmIBTranferLP.hbxNoteToRecipent.setVisibility(false);
        frmIBTranferLP.hbxRecNoteEmail.setVisibility(false);
        frmIBTranferLP.hbxEmail.setVisibility(false);
        frmIBTranferLP.hbxSMS.setVisibility(false);
        frmIBTranferLP.lineOne.setVisibility(false);
        frmIBTranferLP.lineThree.setVisibility(false);
        frmIBTranferLP.hboxXferlblFee.setVisibility(false);
        frmIBTranferLP.vbox47679117772050.skin = "vboxWhiteBg";
        /*  frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
        	frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
        	frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
        	frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false); */
        frmIBTransferNowConfirmation.hbxSplitNtr.setVisibility(false);
        frmIBTransferNowConfirmation.hbxNotifyRecip.setVisibility(false);
        frmIBTransferNowConfirmation.hbxTransNotify.setVisibility(false);
        frmIBTransferNowConfirmation.hbxTransNtr.setVisibility(false);
        frmIBTransferNowCompletion.hbxSplitNtr.setVisibility(false);
        frmIBTransferNowCompletion.hbxNotifyRecip.setVisibility(false);
        frmIBTransferNowCompletion.hbxTransNotif.setVisibility(false);
        frmIBTransferNowCompletion.hbxTransNtr.setVisibility(false);
        gblTransEmail = 1;
        gblTrasSMS = 1;
    }
    resetContentsIB();
    gblPaynow = true;
    gblTrasORFT = 0;
    gblTransSMART = 0;
    if (gbltdFlag != "CDA") {
        onChangeToRecip();
    }
    if (isOneAccount) {
        var lblName = oneAccountData.lbl3;
        var lblAccountNum = oneAccountData.lbl4;
        gblBANKREF = oneAccountData.lblBnkName;
        gblisTMB = oneAccountData.lbl5;
        var bimg = oneAccountData.img2;
        gblSmartAccountName = oneAccountData.accountName;
    } else {
        var lblName = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lbl3;
        var lblAccountNum = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lbl4;
        gblBANKREF = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lblBnkName;
        gblisTMB = frmIBTranferLP.segXferRecipentsList.selectedItems[0].lbl5;
        var bimg = frmIBTranferLP.segXferRecipentsList.selectedItems[0].img2;
        gblSmartAccountName = frmIBTranferLP.segXferRecipentsList.selectedItems[0].accountName;
    }
    //Checking for elgible accounts to be selected
    if (gblisTMB != "") {
        if (gblTrasSMS == 1 && gblTransEmail == 1) {
            var fromData = frmIBTransferCustomWidgetLP.custom1016496273208904.data;
            var selectedItem = frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem;
            var prodCode = fromData[selectedItem].prodCode;
            var linkedAccnt = gblXferLnkdAccnts;
            var frmAccnt = fromData[selectedItem].accountWOF;
            frmAccnt = replaceCommon(frmAccnt, "-", "");
            lblAccountNum = replaceCommon(lblAccountNum, "-", "");
            if (lblAccountNum[3] == "3" && gblisTMB == gblTMBBankCD) { //to check To account is TD for eventnotificationpolocy
                gblToTDFlag = true
            } else {
                gblToTDFlag = false;
            }
            if (frmAccnt == linkedAccnt) {
                if (isOneAccount) {
                    var prdCode = oneAccountData.prodCode;
                } else {
                    var prdCode = frmIBTranferLP.segXferRecipentsList.selectedItems[0].prodCode;
                }
                if (prdCode == "206") return false;
            }
            if (lblAccountNum.length > 10) {
                lblAccountNum = replaceCommon(lblAccountNum, "-", "");
                if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                    if (lblAccountNum[3] == "3") return false;
                }
            }
            lblAccountNum = encodeAccntNumbers(lblAccountNum);
        } else //checking for savings and cuurent account
        {
            if (gblisTMB == gblTMBBankCD) {
                lblAccountNum = replaceCommon(lblAccountNum, "-", "");
                if (lblAccountNum[3] == "2" || lblAccountNum[3] == "7" || lblAccountNum[3] == "9" || lblAccountNum[3] == "1") {
                    lblAccountNum = encodeAccntNumbers(lblAccountNum);
                } else {
                    return false;
                }
            }
        }
        lblName = isNotBlank(lblName) ? lblName : " ";
        frmIBTranferLP.imgXferToImage.src = gblXferRecImg;
        frmIBTranferLP.lblXferToContactRcvd.text = lblAccountNum;
        frmIBTranferLP.lblXferToNameRcvd.text = lblName;
        frmIBTranferLP.lblXferToBankNameRcvd.text = gblBANKREF;
        frmIBTranferLP.imgXferToImage.setVisibility(true);
        closeRightPanelTransfer();
        dismissLoadingScreenPopup();
        //frmIBTranferLP.show();
        nameofform = kony.application.getCurrentForm();
        if (kony.i18n.getCurrentLocale() == "en_US") nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocus"
        else nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocusThai"
        nameofform.segMenuOptions.removeAll();
    }
}

function closeRightPanelTransfer() {
    frmIBTranferLP.hbxwaterWheel.setVisibility(true);
    frmIBTranferLP.TMBImage.setVisibility(true);
    frmIBTranferLP.hbxXferTo.setVisibility(false);
    frmIBTranferLP.hbxXferSegment.setVisibility(false);
    frmIBTranferLP.hbxScheduledTransfer.setVisibility(false);
    frmIBTranferLP.scrollbox21149857403622.setVisibility(false);
}
//adding function to reset the contents on click of mobile segemnt from recepient(issue fix for gblisTMB=""/bankcode empty for ORFT inq)
function resetContentsIB() {
    frmIBTranferLP.imgXferToImage.src = "";
    frmIBTranferLP.lblXferToContactRcvd.text = "";
    frmIBTranferLP.lblXferToNameRcvd.text = "";
    frmIBTranferLP.lblXferToBankNameRcvd.text = "";
    frmIBTranferLP.imgXferToImage.setVisibility(false);
    frmIBTranferLP.tbxEmail.text = "";
    frmIBTranferLP.txtTransLndSmsNEmail.text = "";
    frmIBTranferLP.lblP2PFeeVal.text = "";
}
//function getTransferToRecipientsAccountsIB() {
//
//var resulttable=[];
//    var pid = frmIBTranferLP.segXferRecipentsList.selectedItems[0]["personalizedId"];
//    
//    var ind = "Accounts" + frmIBTranferLP.segXferRecipentsList.selectedIndex[1]
//	var accounts = [];
//	for(var i=0;i<gblRcAllAccountsData.length;i++)
//	{
//		var ind = "Accounts"+i;
//		if(pid==gblRcAllAccountsData[i][ind][0].personalizedId){
//			accounts=gblRcAllAccountsData[i][ind]
//			break;
//		}
//	}
//	if((frmIBTranferLP.txbXferSearch.text != null) && (frmIBTranferLP.txbXferSearch.text != undefined) && (frmIBTranferLP.txbXferSearch.text.length > 2)){
//	segData=frmIBTranferLP.segXferRecipentsList.data;
//	alert("asas")
//	}
//	else
//	segData=gblTransferToRecipientData;
//   // accounts = gblRcAllAccountsData[frmIBTranferLP.segXferRecipentsList.selectedIndex[1]][ind];
//    var indexOfSelectedRow = frmIBTranferLP.segXferRecipentsList.selectedIndex[1];
//    var dataLen = segData.length;
//    var segLen = frmIBTranferLP.segXferRecipentsList.data.length;
//    
//    if (dataLen == segLen) {
//        selectedRow = indexOfSelectedRow
//    } else if (selectedRow < indexOfSelectedRow) {
//        indexOfSelectedRow = indexOfSelectedRow - (segLen - dataLen);
//        selectedRow = indexOfSelectedRow;
//    } else {
//        selectedRow = indexOfSelectedRow;
//    }
//    var indexOfSelectedIndex = frmIBTranferLP.segXferRecipentsList.selectedItems[0];
//    var mno = indexOfSelectedIndex.mobileNo;
//    var fbno = indexOfSelectedIndex.facebookNo;
//    var emailID = indexOfSelectedIndex.imgURL;
//    var mobileimg = ""
//    var fbimg = ""
//    if (mno != null && mno.length > 0) {
//        mobileimg = "phoneicon.png"
//        mno = replaceCommon(mno, "-", "")
//        gblXferPhoneNo = mno;
//        mno = maskingIB(mno);
//    } else {
//        mobileimg = ""
//        gblXferPhoneNo = ""
//    }
//    if (fbno != null && fbno.length > 0) {
//        fbimg = "facebookicon.png"
//    } else {
//        fbimg = ""
//    }
//    if (emailID != null && emailID.length > 0) {
//        gblXferEmail = emailID;
//    } else {
//        gblXferEmail = ""
//    }
//    var bnkLogoURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/Bank-logo" + "/bank-logo-";
//    var selectedData = [{
//        btnXferFb: fbimg,
//        btnXferMobile: mobileimg,
//        imgXferRecipentsImage: indexOfSelectedIndex.imgXferRecipentsImage,
//        imgXferArrow: {
//            src: "bg_arrow_bottom.png"
//        },
//        lblXferRecipentsName: indexOfSelectedIndex.lblXferRecipentsName,
//        lblXferRecipentsAccount: indexOfSelectedIndex.lblXferRecipentsAccount,
//        personalizedId: indexOfSelectedIndex.personalizedId,
//        imgURL: "",
//        isnonTMBAccount: "false",
//        category: "main1",
//        fbNo: indexOfSelectedIndex.fbNo,
//        mobileNo: indexOfSelectedIndex.mobileNo,
//        template: hbxXferSegmentSelected
//    }];
//    gblXferRecImg = indexOfSelectedIndex.imgXferRecipentsImage;
//    var temp = []
//    var tab1 = []
//    kony.table.append(tab1, selectedData)
//    frmIBTranferLP.segXferRecipentsList.widgetDataMap = {
//        btnXferFb: "btnXferFb",
//        btnXferMobile: "btnXferMobile",
//        imgXferRecipentsImage: "imgXferRecipentsImage",
//        imgXferArrow: "imgXferArrow",
//        lblXferRecipentsName: "lblXferRecipentsName",
//        lblXferRecipentsAccount: "lblXferRecipentsAccount",
//        lbl3: "lbl3",
//        lbl4: "lbl4",
//        lbl5: "lbl5",
//        lblBnkName: "lblBnkName",
//        img2: "img2",
//        img3: "img3"
//    }
//    resulttable.CRMAccountsInq = accounts;
//    for (var i = 0; i < resulttable.CRMAccountsInq.length; i++) {
//        var isAccntFav = resulttable.CRMAccountsInq[i]["acctFavFlag"];
//        //kony.type(accFavoriteFlag)
//        var btnFavSkin;
//        if (isAccntFav != null && isAccntFav.length > 0) {
//             if (isAccntFav.toString() == "y" || isAccntFav.toString() == "Y") {
//                btnFavSkin = "starblue.png"
//            } else {
//                btnFavSkin = "";
//            }
//        } else {
//            btnFavSkin = "";
//        }
//        
//        var bankName = getBankNameIB(resulttable.CRMAccountsInq[i].bankCde)
//        
//        var bankLogo = "";
//        if (resulttable.CRMAccountsInq[i].bankCde == 11) bankLogo = "icon.png";
//        else bankLogo = bnkLogoURL + resulttable.CRMAccountsInq[i].bankCde + ".jpg";
//        var acctNo = resulttable.CRMAccountsInq[i].acctId;
//        acctNo = replaceCommon(acctNo, "-", "")
//        var accountData = {
//            img2: bankLogo,
//            lbl3: resulttable.CRMAccountsInq[i].acctNickName,
//            lbl4: addHyphenIB(acctNo),
//            lbl5: resulttable.CRMAccountsInq[i].bankCde,
//            lblBnkName: bankName,
//            img3: btnFavSkin,
//            category: "sub",
//            template: hbxXferSegmentExpanded
//        }
//        
//        kony.table.insert(tab1, accountData);
//    }
//    var tempMbFb = [{
//        img2: "mobileappX.png",
//        lbl3: "Mobile",
//        lbl4: mno,
//        lbl5: "",
//        img3: "",
//        category: "sub",
//        template: hbxXferSegmentExpanded
//    }, {
//        img2: "facebookicoX.png",
//        lbl3: "Facebook",
//        lbl4: fbno,
//        lbl5: "",
//        img3: "",
//        category: "sub",
//        template: hbxXferSegmentExpanded
//    }]
//    for (var i = 0; i < tempMbFb.length; i++) {
//        if (i == 0) {
//            if (mno != null && mno.length > 0) kony.table.insert(tab1, tempMbFb[i])
//        } else if (i == 1) {
//            if (fbno != null && fbno.length > 0) kony.table.insert(tab1, tempMbFb[i])
//        } else kony.table.insert(tab1, tempMbFb[i])
//    } //for**/
//    
//    var tab2 = []
//    for (var i = 0; i < indexOfSelectedRow; i++) {
//        kony.table.insert(tab2, segData[i])
//    }
//    
//    //appending selected row and account list of selected row
//    kony.table.append(tab2, tab1)
//    
//    //adding data after selected row of segment
//    for (var j = selectedRow + 1; j < segData.length; j++) {
//        kony.table.insert(tab2, segData[j])
//    }
//    //checking selected row of segment hiddenmain and flag value if it is true,adding account list of selected row
//    frmIBTranferLP.segXferRecipentsList.removeAll()
//    dismissLoadingScreenPopup();
//    frmIBTranferLP.segXferRecipentsList.setData(tab2)
//    gblAckFlage = "false"
//	
//}
/**************************************************************************************
		Module	: callBackTransferToReciepientsAccountsIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : Fetches  To selected accountlist from receipentAllDetailsService
****************************************************************************************/
selectedRow = 0;

function getBankNameIB(bankCD) {
    var locale = kony.i18n.getCurrentLocale();
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (globalSelectBankData[i][0] == bankCD) {
            if (locale == 'th_TH') {
                return globalSelectBankData[i][8];
            } else {
                return globalSelectBankData[i][7];
            }
        }
    }
}
/**************************************************************************************
		Module	: callBackCrmProfileInqIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : cheking channel limit
****************************************************************************************/
function callBackCrmProfileInqIB(status, resulttable) {
    if (status == 400) {
        gblDPPk = undefined != resulttable["pk"] ? resulttable["pk"] : "";
        gblDPRandNumber = undefined != resulttable["randomNumber"] ? resulttable["randomNumber"] : "";
        var reload = undefined != resulttable["reload"] ? resulttable["reload"] : "";
        if (resulttable["opstatus"] == 0) {
            //please do not right any code above this check, can cause issues. - MIB-2280
            if ("true" == reload) {
                checkCrmProfileInqIB();
                return false;
            }
            frmIBTransferNowConfirmation.lblSplitXferDtVal.text = resulttable["serverDate"];
            frmIBTransferNowConfirmation.lblTransferVal.text = resulttable["serverDate"];
            frmIBTransferNowCompletion.lblTransferVal.text = resulttable["serverDate"];
            frmIBTransferNowCompletion.lblSplitXferDtVal.text = resulttable["serverDate"];
            var StatusCode = resulttable["statusCode"];
            //dismissLoadingScreenPopup();
            if (StatusCode != 0) {
                if (resulttable["errMsg"] != undefined) {
                    dismissLoadingScreenPopup();
                    alert(resulttable["errMsg"]);
                } else {
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                }
            }
            if (StatusCode == 0) {
                if (resulttable["ibUserStatusIdTr"] == "04") {
                    dismissLoadingScreenPopup();
                    alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                    return false;
                } else {
                    gblTransferRefNo = resulttable["acctIdentValue"]
                    var temp1 = [];
                    var temp = {
                        ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"]
                    }
                    kony.table.insert(temp1, temp);
                    gblCRMProfileData = temp1;
                    //dismissLoadingScreenPopup()
                    /*
                    if(gblSwitchToken == false && gblTokenSwitchFlag == false){
                    	checkTokenFlag();
                    }*/
                    if (gblTransEmail == 1 && gblTrasSMS == 1 && gblisTMB == gblTMBBankCD) {
                        if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                            checkTDBusinessHoursIB();
                        } else {
                            checkDepositAccountInqIB();
                        }
                    } else {
                        var temp1 = [];
                        var temp = {
                            ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"]
                        }
                        kony.table.insert(temp1, temp);
                        gblCRMProfileData = temp1;
                        var DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
                        var UsageLimit;
                        if (resulttable["ebAccuUsgAmtDaily"] == "" && resulttable["ebAccuUsgAmtDaily"].length == 0) {
                            UsageLimit = 0;
                        } else {
                            UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"].toString());
                        }
                        var amtEntered = frmIBTranferLP.txbXferAmountRcvd.text + "";
                        amtEntered = amtEntered.replace(/,/g, "");
                        var transferAmt = parseFloat(amtEntered.toString());
                        gblTransferRefNo = resulttable.acctIdentValue;
                        var channelLimit = DailyLimit - UsageLimit;
                        if (channelLimit < 0) {
                            channelLimit = 0;
                        }
                        var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
                        dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(channelLimit) + ""));
                        //P2P MIB-521 Check To Mobile TAB //
                        if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && !isOwnAccountP2P && gblisTMB != gblTMBBankCD && gblPaynow == true) { //Case Transfer to other bank by Prompt Pay	
                            var p2pTranLimit = 0;
                            if (isNotBlank(resulttable["P2P_TRANSACTION_LIMIT"])) {
                                p2pTranLimit = parseFloat(resulttable["P2P_TRANSACTION_LIMIT"]);
                            }
                            var p2pTranlimitExceedMsg = kony.i18n.getLocalizedString("MIB_P2PTRErr_TxnLimit")
                            p2pTranlimitExceedMsg = p2pTranlimitExceedMsg.replace("{transaction limit}", commaFormatted(p2pTranLimit + ""));
                            //Compare base on minimum value					
                            if (channelLimit < p2pTranLimit) {
                                //Display DailyLimit
                                if (transferAmt > channelLimit) {
                                    dismissLoadingScreenPopup();
                                    showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                            }
                            if (p2pTranLimit < channelLimit) {
                                //Display p2pTranLimit
                                if (transferAmt > p2pTranLimit) {
                                    dismissLoadingScreenPopup();
                                    showAlertWithCallBack(p2pTranlimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                            }
                            if (p2pTranLimit == channelLimit) {
                                //Display p2pTranLimit
                                if (transferAmt > p2pTranLimit) {
                                    dismissLoadingScreenPopup();
                                    showAlertWithCallBack(p2pTranlimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                            }
                        } else if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && isOwnAccountP2P) { //Transfer Own by Prompt Pay
                        } else if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && gblisTMB == gblTMBBankCD && gblPaynow == true) { //Transfer to other TMB by Prompt Pay						
                            //Check only daily limit
                            if (channelLimit < transferAmt) {
                                dismissLoadingScreenPopup();
                                showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                return false;
                            }
                        } else { //end P2P MIB-521
                            //Check To Account TAB //
                            //checkMaxLimitForTransfer//
                            // Transfer to other TMB						
                            if (!(gblTransEmail == 1 && gblTrasSMS == 1) && gblisTMB == gblTMBBankCD) {
                                if (channelLimit < transferAmt && gblPaynow == true) {
                                    dismissLoadingScreenPopup();
                                    showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                            }
                            // Transfer ORFT
                            if (gblisTMB != gblTMBBankCD && gblTrasORFT == 1) {
                                var maxTransferORFT = parseFloat(gblMaxTransferORFT).toFixed(2);
                                if ((channelLimit < maxTransferORFT) && (channelLimit < transferAmt) && gblPaynow == true) {
                                    dismissLoadingScreenPopup();
                                    showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                //For schedule ORFT								
                                if ((DailyLimit < transferAmt) && gblPaynow == false) {
                                    dismissLoadingScreen();
                                    var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
                                    dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(DailyLimit) + ""));
                                    showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                //For schedule ORFT
                                if ((maxTransferORFT < transferAmt) && gblPaynow == false) {
                                    dismissLoadingScreen();
                                    var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                    megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferORFT + ""));
                                    showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                if ((maxTransferORFT < channelLimit) && (maxTransferORFT < transferAmt)) {
                                    dismissLoadingScreenPopup();
                                    var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                    megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferORFT + ""));
                                    showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                if ((maxTransferORFT == channelLimit) && (maxTransferORFT < transferAmt)) {
                                    dismissLoadingScreenPopup();
                                    var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                    megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferORFT + ""));
                                    showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                            }
                            // Transfer SMART					
                            if (gblisTMB != gblTMBBankCD && gblTransSMART == 1) {
                                var maxTransferSMART = parseFloat(gblMaxTransferSMART).toFixed(2);
                                if ((channelLimit < maxTransferSMART) && (channelLimit < transferAmt) && gblPaynow == true) {
                                    dismissLoadingScreenPopup();
                                    showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                //For schedule SMART								
                                if ((DailyLimit < transferAmt) && gblPaynow == false) {
                                    dismissLoadingScreen();
                                    var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
                                    dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(DailyLimit) + ""));
                                    showAlertWithCallBack(dailylimitExceedMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                //For schedule SMART
                                if ((maxTransferSMART < transferAmt) && gblPaynow == false) {
                                    dismissLoadingScreen();
                                    var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                    megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferSMART + ""));
                                    showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                if ((maxTransferSMART < channelLimit) && (maxTransferSMART < transferAmt)) {
                                    dismissLoadingScreenPopup();
                                    var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                    megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferSMART + ""));
                                    showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                                if ((maxTransferSMART == channelLimit) && (maxTransferSMART < transferAmt)) {
                                    dismissLoadingScreenPopup();
                                    var megaExceedLimitMsg = kony.i18n.getLocalizedString("TRErr_MegaExceedLimit")
                                    megaExceedLimitMsg = megaExceedLimitMsg.replace("{MaxMegaAmount}", commaFormatted(maxTransferSMART + ""));
                                    showAlertWithCallBack(megaExceedLimitMsg, kony.i18n.getLocalizedString("info"), callBackP2PIBAmountFields);
                                    return false;
                                }
                            }
                        } //end To Account TAB						
                        dismissLoadingScreenPopup();
                        if ((gblSelTransferMode == 2 || gblSelTransferMode == 3) && gblisTMB != gblTMBBankCD) {
                            var amtEntered = frmIBTranferLP.txbXferAmountRcvd.text + "";
                            amtEntered = amtEntered.replace(/,/g, "");
                            checkpromptPayInqIB(amtEntered);
                        } else if (gblisTMB != gblTMBBankCD && gblTrasORFT == 1) { //other Bank Inquiry
                            var transORFTSplitAmnt = parseFloat(gblTransORFTSplitAmnt)
                            var amtEntered = frmIBTranferLP.txbXferAmountRcvd.text + "";
                            amtEntered = amtEntered.replace(/,/g, "");
                            var transferAmt = amtEntered;
                            if (transferAmt > transORFTSplitAmnt) transferAmt = transORFTSplitAmnt + "";
                            else transferAmt = transferAmt;
                            checkOrftAccountInqIB(transferAmt);
                        } else if (gblisTMB == gblTMBBankCD) //TMB Banking
                        {
                            //** below service confirms on transfer account really exist  */
                            checkDepositAccountInqIB();
                        }
                        /** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
                        else if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                            checkAccountWithdrawInqIB();
                        } else {
                            checkTransferTypeIB();
                        }
                    }
                }
            }
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["opstatus"] == "8005") {
                showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
            } else if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}
/**************************************************************************************
		Module	: callBackOrftAccountInqIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : cheking Account Name limit
*****************************************************************************************/
function callBackOrftAccountInqIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["StatusCode"];
            if (StatusCode != "0") {
                dismissLoadingScreenPopup();
                if (resulttable["errMsg"] != undefined) {
                    alert(resulttable["errMsg"]);
                } else {
                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                }
                return false;
            } else {
                var ToAccountName = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];
                frmIBTransferNowConfirmation.lblXferToAccTyp.text = "";
                frmIBTransferNowConfirmation.lblXferToName.text = frmIBTranferLP.lblXferToNameRcvd.text;
                frmIBTransferNowConfirmation.lblXferToAccTyp.text = ToAccountName;
                frmIBTransferNowConfirmation.image247327596554550.src = frmIBTranferLP.imgXferToImage.src;
                // commenting below code to allow evem account name is blank Ticket#28938
                /*if (ToAccountName == null || ToAccountName == "") {
                	dismissLoadingScreenPopup();
                	alert(" " + kony.i18n.getLocalizedString("keyNoAccountFoundwiththegivenNumber"));
                	return false;
                }*/
                /** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
                if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                    checkAccountWithdrawInqIB();
                } else {
                    checkTransferTypeIB();
                }
                dismissLoadingScreenPopup();
            }
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
            return false;
        }
    }
}
/**************************************************************************************
		Module	: callBackDepositAccountInqIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : cheking TMB Account Name
****************************************************************************************/
function callBackDepositAccountInqIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            // fetching To Account Name for TMB Inq
            var StatusCode = resulttable["StatusCode"];
            if (StatusCode != "0") {
                if (resulttable["errMsg"] != undefined) {
                    dismissLoadingScreenPopup();
                    alert(resulttable["errMsg"]);
                } else {
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                }
                return false;
            } else {
                var ToAccountName = resulttable["accountTitle"]
                if (ToAccountName != null && ToAccountName.length > 0) {
                    //happy Path;
                    if (isNotBlank(frmIBTranferLP.lblXferToNameRcvd.text)) {
                        frmIBTransferNowConfirmation.lblXferToName.text = frmIBTranferLP.lblXferToNameRcvd.text;
                    }
                    frmIBTransferNowConfirmation.lblXferToAccTyp.text = ToAccountName;
                } else {
                    dismissLoadingScreenPopup();
                    alert(kony.i18n.getLocalizedString("keyTransferSelectedPartyNotExist"));
                    return false;
                }
                /** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
                if (gbltdFlag == kony.i18n.getLocalizedString("termDeposit")) {
                    checkAccountWithdrawInqIB();
                } else {
                    /*** invoking fundTransferInquiry service to get the Fee Amnt fo Transaction.  */
                    checkFundTransferInqIB();
                }
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}
/**************************************************************************************
		Module	: callBackAccountWithdrawInqIB
		Author  : Kony
		Date    : June 07, 2013
		Purpose : cheking AccountPreWithdrawInq
*****************************************************************************************/
function callBackAccountWithdrawInqIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //checking Account PreWithDraw Inq
            var StatusCode = resulttable["StatusCode"];
            dismissLoadingScreenPopup();
            if (StatusCode != "0") {
                if (resulttable["errMsg"] != undefined) {
                    alert(resulttable["errMsg"]);
                } else {
                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                }
                return false;
            }
            var totalInterest = resulttable["AcctWithdrawalInqRs"][0].totalInterest;
            var penalityAmt = resulttable["AcctWithdrawalInqRs"][0].penaltyAmt;
            var taxAmt = resulttable["AcctWithdrawalInqRs"][0].taxAmt;
            var netAmt = resulttable["AcctWithdrawalInqRs"][0].OutstandingBalAmt;
            gblWithdrawalTotInterest = parseFloat(totalInterest).toFixed(2);
            gblWithdrawalTaxAmt = parseFloat(taxAmt).toFixed(2);
            frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfIntAmt.setVisibility(true);
            frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfTaxAmt.setVisibility(true);
            frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfPenaltyAmt.setVisibility(true);
            frmIBTransferNowConfirmation.hbxfrmIBTransferNowCnfNetAmt.setVisibility(true);
            gblTDDateFlag = true;
            var totInt = "";
            if (totalInterest < 0) {
                totInt = 0 + "";
            } else {
                totInt = totalInterest;
            }
            frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfIntAmtVal.text = commaFormatted(totInt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfPenaltyAmtVal.text = commaFormatted(penalityAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfTaxAmtVal.text = commaFormatted(taxAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmIBTransferNowConfirmation.lblfrmIBTransferNowCnfNetAmtVal.text = commaFormatted(netAmt) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            if (gblisTMB == gblTMBBankCD) {
                checkFundTransferInqIB()
            } else {
                checkTransferTypeIB();
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}
/*************************************************************************
	Module	: checkCrmProfileInqIB
	Author  : Kony
	Purpose : checking for channel limit
*************************************************************************/
function checkCrmProfileInqIB() {
    var inputParam = {}
    inputParam["transferFlag"] = "true";
    var transferAmt = frmIBTranferLP.txbXferAmountRcvd.text + ""
    replaceCommon(transferAmt, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    transferAmt = transferAmt.replace(/,/g, "");
    if (kony.string.containsChars(transferAmt, ["."])) transferAmt = transferAmt;
    else transferAmt = transferAmt + ".00";
    inputParam["transferAmt"] = transferAmt;
    var fee = "0";
    inputParam["gblisTMB"] = gblisTMB;
    if (gblisTMB == gblTMBBankCD) {
        inputParam["gblTMBBankCD"] = gblTMBBankCD;
    } else {
        if (gblTransSMART == 1) {
            fee = frmIBTranferLP.btnXferSMART.text;
            inputParam["gblTransSMART"] = gblTransSMART;
            var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
            fee = fee.substring(0, indexOfB);
        } else if (gblTrasORFT == 1) {
            inputParam["gblTrasORFT"] = gblTrasORFT;
            activityTypeID = "025"
                //	activitfeeyFlexValues5 = "ORFT"
            fee = frmIBTranferLP.btnXferORFT.text;
            var indexOfB = fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"))
            fee = fee.substring(0, indexOfB);
        } else {
            activityTypeID = "024"
            activityFlexValues5 = ""
            fee = 0
        }
    }
    inputParam["gblPaynow"] = gblPaynow;
    var frmID = frmIBTranferLP.lblTranLandFromNum.text;
    var frmAccnt = replaceCommon(frmID, "-", "");
    inputParam["frmAccnt"] = frmAccnt;
    inputParam["recipientMobile"] = frmIBTranferLP.txtTransLndSmsNEmail.text;
    inputParam["recipientEmailAddr"] = frmIBTranferLP.tbxEmail.text;
    var toNum = gblp2pAccountNumber; //frmIBTranferLP.lblXferToContactRcvd.text;
    toNum = replaceCommon(toNum, "-", "");
    inputParam["toNum"] = toNum;
    var activityFlexValues3 = getBankShortName(gblisTMB);
    inputParam["bankRef"] = activityFlexValues3;
    inputParam["fee"] = fee
    inputParam["FuturetransferDate"] = changeDateFormatForService(frmIBTranferLP.lblXferTransferRange.text);
    var userName = "";
    var userNum = "";
    var temptdFlag;
    temptdFlag = kony.i18n.getLocalizedString("termDeposit")
    if (gbltdFlag == temptdFlag) {
        userName = frmIBTranferLP.lblXferFromNameRcvd.text + "";
        userNum = frmIBTranferLP.lblTranLandFromNum.text + "";
    } else {
        if (gblSelTransferMode == 2 || gblSelTransferMode == 3) {
            userName = frmIBTransferNowConfirmation.lblXferToName.text;
            if (userName.length > 20) {
                userName = userName.substr(0, 20);
            }
        } else {
            userName = frmIBTranferLP.lblXferToNameRcvd.text + "";
            if (userName.length > 20) {
                userName = userName.substr(0, 20);
            }
        }
        userNum = gblp2pAccountNumber + "";
    }
    if (userNum.length > 0) {
        userNum = removeHyphenIB(userNum);
        userNum = "xxx-x-" + userNum.substring(userNum.length - 6, userNum.length - 1) + "-x";
    }
    var bankShortName = ""
    bankShortName = getBankShortName(gblisTMB);
    var module = "";
    if (gbltdFlag == "CDA") {
        module = "withdraw";
    } else if (gblSelTransferMode == 2) {
        module = "promptpay";
    } else if (gblSelTransferMode == 3) {
        module = "promptpayCI";
    } else {
        module = "execute";
    }
    if (gblToTDFlag) {
        module = "deposit";
    }
    if (!gblPaynow) {
        module = "schedule";
    }
    inputParam["accountName"] = userName;
    inputParam["accountNum"] = userNum;
    inputParam["bankName"] = bankShortName;
    inputParam["module"] = module;
    inputParam["remainingFree"] = gblcwselectedData.remainingFee;
    var transferData = "";
    var mobileOrCI = "";
    if (gblSelTransferMode == 2) {
        mobileOrCI = "02";
        var mobileNumber = "";
        if (frmIBTranferLP.hbxSelMobileRecipient.isVisible) {
            mobileNumber = frmIBTranferLP.lblToMobileNo.text;
        } else {
            mobileNumber = frmIBTranferLP.txtXferMobileNumber.text;
        }
        inputParam["P2P"] = removeHyphenIB(mobileNumber);
        transferData = frmAccnt + "|" + toNum + "|" + transferAmt + "|" + removeHyphenIB(mobileNumber);
    } else if (gblSelTransferMode == 3) {
        mobileOrCI = "01";
        var citizenID = removeHyphenIB(frmIBTranferLP.txtXferMobileNumber.text);
        inputParam["P2P"] = citizenID;
        transferData = frmAccnt + "|" + toNum + "|" + transferAmt + "|" + citizenID;
    } else {
        inputParam["P2P"] = "";
        transferData = frmAccnt + "|" + toNum + "|" + transferAmt + "|" + "";
    }
    inputParam["mobileOrCI"] = mobileOrCI;
    inputParam["encryptedData"] = encryptDPUsingE2EE(transferData);
    inputParam["bankShortName"] = getBankShortName(gblisTMB);
    showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackCrmProfileInqIB)
}
/*************************************************************************

	Module	: checkOrftAccountInqIB
	Author  : Kony
	Purpose : checking for channel limit

************************************************************************/
function checkOrftAccountInqIB(amt) {
    var inputParam = {}
    var fromAcctID;
    fromAcctID = gblcwselectedData.accountWOF;
    fromAcctID = replaceCommon(fromAcctID, "-", "");
    var toAcctID = gblp2pAccountNumber;
    toAcctID = replaceCommon(toAcctID, "-", "");
    if (kony.string.containsChars(amt, ["."])) amt = amt;
    else amt = amt + ".00";
    inputParam["fromAcctNo"] = fromAcctID;
    inputParam["toAcctNo"] = toAcctID;
    inputParam["toFIIdent"] = gblisTMB;
    inputParam["transferAmt"] = amt;
    var accName = "";
    if (gblshotcutToACC == false) {
        if (recipientAddFromTransfer || gblTransferFromRecipient) {
            if (gblTransferFromRecipient) {
                accName = accountData["lblAccountName"];
            } else {
                accName = frmIBMyReceipentsAddContactManually.segmentRcAccounts.data[0]["lblAccountName"];
            }
        } else {
            accName = gblSmartAccountName;
        }
        if (accName == null || accName == undefined || accName == "NONE" || accName == "NA") accName = "";
        frmIBTransferNowConfirmation.lblXferToAccTyp.text = accName;
        frmIBTransferNowCompletion.lblXferToAccTyp.text = accName;
    }
    inputParam["AccountName"] = accName; //frmIBTranferLP.segXferRecipentsList.selectedItems[0].accountName;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("ORFTInq", inputParam, callBackOrftAccountInqIB)
}
/************************************************************************

	Module	: checkDepositAccountInqIB
	Author  : Kony
	Purpose : checking for TMB account Name

**********************************************************************/
function checkDepositAccountInqIB() {
    var inputParam = {}
    var toAcctID = gblp2pAccountNumber;
    toAcctID = replaceCommon(toAcctID, "-", "")
    inputParam["acctId"] = toAcctID;
    //showLoadingScreenPopup();
    invokeServiceSecureAsync("depositAccountInquiryNonSec", inputParam, callBackDepositAccountInqIB)
}
/************************************************************************

	Module	: checkAccountWithdrawInqIB
	Author  : Kony
	Purpose : checking accountwithdrawInq

**********************************************************************/
function checkAccountWithdrawInqIB() {
    var inputParam = {}
    var fromAcctID = gblcwselectedData.accountWOF;
    fromAcctID = replaceCommon(fromAcctID, "-", "");
    var comboDataKey = frmIBTranferLP.cmbxMaturityDetails.selectedKey;
    var dateaAmount = comboDataKey.split(",");
    var keyVal = dateaAmount[2];
    var depositeNo = gblTDDepositNo[keyVal - 1];
    if (depositeNo.length == 1) {
        depositeNo = "00" + depositeNo;
    } else if (depositeNo.length == 2) {
        depositeNo = "0" + depositeNo;
    }
    if (fromAcctID.length == 14) {
        fromAcctID = fromAcctID.substr(4, 10);
    }
    var fromFIIdent = gblcwselectedData.lblDummy;
    var amt = frmIBTranferLP.txbXferAmountRcvd.text + ""
    amt = replaceCommon(amt, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    amt = amt.replace(/,/g, "");
    amt = amt + "";
    if (kony.string.containsChars(amt, ["."])) amt = amt;
    else amt = amt + ".00";
    inputParam["fromAcctNo"] = fromAcctID + depositeNo;
    inputParam["fIIdent"] = fromFIIdent;
    inputParam["withdrawlAmt"] = amt
    showLoadingScreenPopup();
    invokeServiceSecureAsync("AccountWithdrawInq", inputParam, callBackAccountWithdrawInqIB)
}
/**************************************************************************************
		Module	: startRequestOTPApplyServicesTransferIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : This function is used for genereating OTP
*****************************************************************************************/
function startRequestOTPApplyServicesTransferIB() {
    var eventNotificationPolicy;
    var SMSSubject;
    var Channel
    var locale = kony.i18n.getCurrentLocale();
    var nameFromRecipientList = "false";
    if (gbltdFlag == "CDA") {
        Channel = "WithdrawTD";
        //SMSSubject = "MIB_WithdrawTD_"
        //eventNotificationPolicy = "MIB_WithdrawTD_"
    } else {
        if (gblSelTransferMode == 2) {
            Channel = "PromptPay";
            if (frmIBTransferNowConfirmation.lblXferToName.isVisible) {
                nameFromRecipientList = "true";
            } else {
                nameFromRecipientList = "false";
            }
        } else if (gblSelTransferMode == 3) {
            Channel = "PromptPayCI";
            if (frmIBTransferNowConfirmation.lblXferToName.isVisible) {
                nameFromRecipientList = "true";
            } else {
                nameFromRecipientList = "false";
            }
        } else {
            Channel = "ExecuteTransfer";
        }
        //SMSSubject = "MIB_iORFT_"
        //eventNotificationPolicy = "MIB_iORFT_"
    }
    if (gblToTDFlag) {
        Channel = "DepositTD";
        //SMSSubject = "MIB_DepositTD_"	
        //eventNotificationPolicy = "MIB_DepositTD_"
    }
    var amount = frmIBTranferLP.txbXferAmountRcvd.text + "";
    var userName = "";
    var userNum = "";
    var temptdFlag;
    temptdFlag = kony.i18n.getLocalizedString("termDeposit")
    if (gbltdFlag == temptdFlag) {
        userName = frmIBTransferNowConfirmation.lblName.text + "";
        userNum = frmIBTransferNowConfirmation.lblAccountNo.text + "";
    } else {
        userName = frmIBTranferLP.lblXferToNameRcvd.text + "";
        userNum = gblp2pAccountNumber + "";
    }
    if (userNum.length > 0) {
        userNum = removeHyphenIB(userNum);
        userNum = "xxx-x-" + userNum.substring(userNum.length - 6, userNum.length - 1) + "-x";
    }
    if (!gblPaynow) {
        Channel = "FutureThirdPartyTransfer";
        //SMSSubject = "MIB_ScheThirdPartyTransfer_"
        //eventNotificationPolicy = "MIB_ScheThirdPartyTransfer_"
        //userName = frmIBTransferNowConfirmation.lblXferToAccTyp.text;
        userName = frmIBTransferNowConfirmation.lblXferToName.text;
    }
    var inputParams = {
        retryCounterRequestOTP: gblRetryCountRequestOTP,
        Channel: Channel,
        locale: locale,
        nameFromRecipientList: nameFromRecipientList
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, startRequestOTPApplyServicesAsyncCallbackTransferIB);
}
/**************************************************************************************
		Module	: startRequestOTPApplyServicesAsyncCallbackTransferIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : Callback method for startRequestOTPApplyServicesTransferIB()
*****************************************************************************************/
function startRequestOTPApplyServicesAsyncCallbackTransferIB(status, callBackResponse) {
    if (status == 400) {
        dismissLoadingScreenPopup();
        if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            frmIBTransferNowConfirmation.hbxOTPsnt.setVisibility(false);
            frmIBTransferNowConfirmation.hbox476047582127699.isVisible = false;
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            frmIBTransferNowConfirmation.hbxOTPsnt.setVisibility(false);
            frmIBTransferNowConfirmation.hbox476047582127699.isVisible = false;
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (callBackResponse["errCode"] == "JavaErr00001") {
                showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
            var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
            kony.timer.schedule("OTPTimerReq", OTPTimerCallBackTrans, reqOtpTimer, false);
            frmIBTransferNowConfirmation.hbox476047582127699.setVisibility(true);
            frmIBTransferNowConfirmation.hbxOTPsnt.setVisibility(true);
            frmIBTransferNowConfirmation.txtBxOTP.setFocus(true);
            frmIBTransferNowConfirmation.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
            var refVal = "";
            for (var d = 0; d < callBackResponse["Collection1"].length; d++) {
                if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                    refVal = callBackResponse["Collection1"][d]["ValueString"];
                    break;
                }
            }
            frmIBTransferNowConfirmation.lblBankRefVal.text = refVal //callBackResponse["Collection1"][8]["ValueString"];
            frmIBTransferNowConfirmation.lblKeyotpmg.text = kony.i18n.getLocalizedString("keyotpmsg");
            frmIBTransferNowConfirmation.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
            //frmIBTransferNowConfirmation.btnOTPReq.skin = btnIB158disabled;
            frmIBTransferNowConfirmation.btnOTPReq.skin = btnIBREQotp;
            frmIBTransferNowConfirmation.btnOTPReq.hoverSkin = btnIBREQotp;
            frmIBTransferNowConfirmation.btnOTPReq.setEnabled(false);
        } else if (callBackResponse["opstatus"] == 8005 || callBackResponse["code"] == 10403) {
            popIBTransNowOTPLocked.show();
        } else {
            if (callBackResponse["errMsg"] != undefined) {
                alert(callBackResponse["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function editConfirmOTPXfer() {
    frmIBTransferNowConfirmation.btnOTPReq.skin = btnIBREQotpFocus;
    frmIBTransferNowConfirmation.btnOTPReq.setEnabled(true);
}

function OTPTimerCallBackTrans() {
    //frmIBTransferNowConfirmation.btnOTPReq.skin = btnIB158active;
    frmIBTransferNowConfirmation.btnOTPReq.skin = btnIBREQotpFocus;
    frmIBTransferNowConfirmation.btnOTPReq.hoverSkin = btnibreqotpfoc
    frmIBTransferNowConfirmation.btnOTPReq.setEnabled(true);
    try {
        kony.timer.cancel("OTPTimerReq")
    } catch (e) {}
}
/*************************************************************************

	Module	: checkFundTransferInqIB
	Author  : Kony
	Purpose : getting fee for if ToAccount is TMB

*******************************************************************/
function checkFundTransferInqIB() {
    var inputParam = {}
    var k = 3;
    var fromAcctID;
    var depositeNo;
    fromAcctID = gblcwselectedData.accountWOF;
    fromAcctID = replaceCommon(fromAcctID, "-", "")
    var toAcctID = gblp2pAccountNumber;
    toAcctID = replaceCommon(toAcctID, "-", "")
    var amt = frmIBTranferLP.txbXferAmountRcvd.text + ""
    if (fromAcctID.length == 14) k = 7;
    if (fromAcctID[k] == "3") { //td
        var comboDataKey = frmIBTranferLP.cmbxMaturityDetails.selectedKey;
        var dateaAmount = comboDataKey.split(",");
        var keyVal = dateaAmount[2];
        depositeNo = gblTDDepositNo[keyVal - 1];
    }
    amt = parseFloat(removeCommos(amt)).toFixed(2);
    inputParam["transferAmt"] = amt;
    inputParam["transferDate"] = gblTransferDate;
    inputParam["depositeNo"] = depositeNo
    inputParam["fromAcctNo"] = fromAcctID
    inputParam["toAcctNo"] = toAcctID;
    invokeServiceSecureAsync("fundTransferInq", inputParam, callBackFundTransferInqIB)
}
/*
*************************************************************************************
		Module	: callBackFundTransferInqIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : callback function for callBackFundTransferInqIB
****************************************************************************************
*/
function callBackFundTransferInqIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var StatusCode = resulttable["StatusCode"];
            var fee = resulttable["transferFee"];
            var FlagFeeReg = resulttable["FlagFeeReg"];
            dismissLoadingScreenPopup();
            var temp1 = [];
            var temp = {
                FlagFeeReg: resulttable["FlagFeeReg"],
                tranCode: resulttable["TranCode"]
            }
            kony.table.insert(temp1, temp)
            gblFundXferData = temp1;
            if (StatusCode != "0") {
                if (resulttable["errMsg"] != undefined) {
                    alert(resulttable["errMsg"]);
                } else {
                    alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                }
                return false;
            } else {
                if (FlagFeeReg == "I") {
                    frmIBTransferNowConfirmation.lblSplitTotFeeVal.text = fee + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBTransferNowConfirmation.lblFeeVal.text = fee + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                } else {
                    frmIBTransferNowConfirmation.lblSplitTotFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    frmIBTransferNowConfirmation.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                checkTransferTypeIB()
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}
/*************************************************************************

	Module	: getTransferFeeIB
	Author  : Kony
	Purpose : getting transfer Fee for Orft and Smart

****/
function getTransferFeeIB() {
    var inputParam = {}
    var remainingFee = gblcwselectedData.remainingFee;
    var prodCode = gblcwselectedData.prodCode;
    var amtRcvd = replaceCommon(frmIBTranferLP.txbXferAmountRcvd.text, ",", "");
    inputParam["Amount"] = amtRcvd;
    if (gblPaynow) {
        inputParam["freeTransCnt"] = remainingFee;
    } else {
        inputParam["freeTransCnt"] = "0";
    }
    inputParam["prodCode"] = prodCode;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("getTransferFee", inputParam, callBackgetTransferFeeIB)
}
/**************************************************************************************
		Module	: callBackgetTransferFeeIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : getting transfer fee for Smart and Orft
*****************************************************************************************/
function callBackgetTransferFeeIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var orftFee = resulttable["orftFee"];
            var smartFee = resulttable["smartFee"];
            var currentDate = resulttable["orftFeeDate"];
            smartDate = resulttable["smartFeeDate"];
            var cutoffInd = resulttable["cutoffInd"];
            // changed/added below code to fix DEF1068
            var smartDateNew = resulttable["smartFeeDateNew"];
            //var smartFutureTime = resulttable["smartFuture"];
            var smartfuturetime = resulttable["smartFutureNew"];
            var orftFutureTime = resulttable["orftFuture"];
            orftFee = commaFormatted(orftFee);
            smartFee = commaFormatted(smartFee);
            var smartFlag = getSMARTFlagIB(gblisTMB)
            var orftFlag = getORFTFlagIB(gblisTMB)
                //ENH_129 SMART Transfer Add Date & Time
            frmIBTransferNowConfirmation.lblSmartTrfrDateVal.text = smartDate;
            frmIBTransferNowCompletion.lblSmartTrfrDateVal.text = smartDate;
            if (orftFlag == "N") {
                frmIBTranferLP.btnXferORFT.setVisibility(false);
            } else {
                frmIBTranferLP.btnXferORFT.setEnabled(true);
                frmIBTranferLP.btnXferORFT.setVisibility(true);
            }
            if (smartFlag == "N") {
                frmIBTranferLP.btnXferSMART.setVisibility(false);
            } else {
                if (orftFlag == "N") {
                    frmIBTranferLP.btnXferORFT.setEnabled(false);
                    frmIBTranferLP.btnXferSMART.setEnabled(false);
                    frmIBTranferLP.hbxFee.setVisibility(false);
                    frmIBTranferLP.hbxFeeBtns.setVisibility(false);
                    frmIBTranferLP.hboxXferlblFee.setVisibility(true)
                    if (gblPaynow) {
                        frmIBTranferLP.lblXferFeelblValue.text = smartFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "[" + smartDateNew + "]";
                    } else {
                        frmIBTranferLP.lblXferFeelblValue.text = smartFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "[" + smartfuturetime + "]";
                    }
                    gblTransSMART = 1;
                } else { //N
                    frmIBTranferLP.btnXferSMART.setEnabled(true);
                    frmIBTranferLP.btnXferSMART.setVisibility(true)
                }
            } //else
            if (smartFlag == "Y" && orftFlag == "Y") {
                frmIBTranferLP.hboxXferlblFee.setVisibility(false);
                gblTrasORFT = 0;
                gblTransSMART = 0;
                frmIBTranferLP.btnXferORFT.skin = btnFeeOrft;
                frmIBTranferLP.btnXferSMART.skin = btnFeeSmart;
            } //if
            if (orftFlag == "N" && smartFlag == "N" && gblisTMB != gblTMBBankCD) {
                alert("Selected account is not eligible for Transfers");
                frmIBTranferLP.btnXferNext.setEnabled(false);
            } else {
                frmIBTranferLP.btnXferNext.setEnabled(true);
            }
            if (gblPaynow) {
                frmIBTranferLP.btnXferSMART.text = smartFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "[" + smartDateNew + "]";
            } else {
                frmIBTranferLP.btnXferSMART.text = smartFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "[" + smartfuturetime + "]";
            }
            if (gblPaynow) frmIBTranferLP.btnXferORFT.text = orftFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "[" + "NOW" + "]"
            else frmIBTranferLP.btnXferORFT.text = orftFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + "[" + orftFutureTime + "]"
            dismissLoadingScreenPopup();
            var currentDate = currentDate.toString();
            var dateTD = currentDate.substr(0, 2);
            var yearTD = currentDate.substr(6, 4);
            var monthTD = currentDate.substr(3, 2);
            var completeDate = yearTD + "-" + monthTD + "-" + dateTD;
            gblTransferDate = completeDate;
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
    }
}

function getORFTFlagIB(bankCd) {
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (globalSelectBankData[i][0] == bankCd) return globalSelectBankData[i][3];
    }
}

function getSMARTFlagIB(bankCd) {
    for (var i = 0; i < globalSelectBankData.length; i++) {
        if (globalSelectBankData[i][0] == bankCd) return globalSelectBankData[i][4];
    }
}
/*************************************************************************

	Module	: getTransferRefrenceNoIB
	Author  : Kony
	Purpose : getting transfer Refrence No
****/
/*function getTransferRefrenceNoIB() {
	var inputParam = {}
	inputParam["transRefType"] = "ST";
	invokeServiceSecureAsync("generateTransferRefNo", inputParam, callBackgetTransferRefrenceNoIB)
 }

function callBackgetTransferRefrenceNoIB(status, resulttable) {
 	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			gblTransferRefNo = resulttable.acctIdentValue;
		} else {
			if(resulttable["errMsg"] != undefined)
	        	{
	        		alert(resulttable["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}		}
	}
}*/
function modifyUserUpdate(StatusDesc, gblVerifyOTP) {
    var inputParam = [];
    inputParam["userStoreId"] = gblUserName;
    inputParam["userId"] = gblUserName;
    inputParam["segmentId"] = "MIB";
    inputParam["loginModuleIdSMS"] = "IBSMSOTP";
    inputParam["loginModuleIdHW"] = "IB_HWTKN";
    inputParam["badLoginCount"] = gblVerifyOTP;
    inputParam["status"] = StatusDesc;
    showLoadingScreen();
    invokeServiceSecureAsync("modifyUpdateUser", inputParam, ibTokenUserUpdateCallbackfunction);
}

function ibTokenUserUpdateCallbackfunction(status, callbackResponse) {
    if (status == 400) {}
}
/**************************************************************************************
		Module	: lockOTPIBTransferAccounts
		Author  : Kony
		Date    : May 05, 2013
		Purpose : function for  LockingOTPIBTransfer
*****************************************************************************************/
function lockOTPIBTransferAccounts() {
    var inputParam = {};
    var ebAccuUsgAmtDaily;
    if (gblCRMProfileData[0]["ebAccuUsgAmtDaily"] == "" && gblCRMProfileData[0]["ebAccuUsgAmtDaily"].length == 0) {
        ebAccuUsgAmtDaily = 0;
    } else {
        ebAccuUsgAmtDaily = parseFloat(gblCRMProfileData[0]["ebAccuUsgAmtDaily"])
    }
    ebAccuUsgAmtDaily = ebAccuUsgAmtDaily;
    inputParam["ebAccuUsgAmtDaily"] = ebAccuUsgAmtDaily
    inputParam["actionType"] = "30";
    inputParam["ibUserStatusId"] = "04";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("crmProfileMod", inputParam, callBacklockOTPIBTransferAccount)
}
/**************************************************************************************
		Module	: callBacklockOTPIBTransferAccounts
		Author  : Kony
		Date    : May 05, 2013
		Purpose : callback function for lockOTPIBTransferAccounts
*****************************************************************************************/
function callBacklockOTPIBTransferAccount(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreenPopup();
        if (resulttable["opstatus"] == 0) {
            // fetching To Account Name for TMB Inq
            gblUserLockStatusIB = resulttable["IBUserStatusID"];
            frmIBTransferNowConfirmation.brnConfirm.setEnabled(false);
            frmIBTransferNowConfirmation.btnCancel.setEnabled(false);
            frmIBTransferNowConfirmation.btnOTPReq.setEnabled(false);
            accountsummaryLangToggleIB.call(this);
            frmIBAccntSummary.segAccountDetails.selectedIndex = [0, 0];
        }
    }
}

function validateSecurityForTransactionServiceIB() {
    frmIBTransferNowConfirmation.lblOTPinCurr.text = "";
    frmIBTransferNowConfirmation.lblPlsReEnter.text = "";
    //startRCTransactionSecurityValidationServiceIB();
}
/**************************************************************************************
		Module	: startTransactionSecurityValidationServiceAsyncCallbackIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : Call back for startRCTransactionSecurityValidationServiceIB
*****************************************************************************************/
/**************************************************************************************
		Module	: callbackGeneratePDFIB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : call back function for generateTransferPDF
*****************************************************************************************/
function savePDFTransferIB(filetype) {
    var inputParam = {}
    inputParam["templatename"] = "ExecutedTransferComplete";
    inputParam["filetype"] = filetype;
    var splitResult = [];
    var notifyVia = "";
    var notifyThru = frmIBTransferNowCompletion.label101640690016505.text;
    if (notifyThru == "-") {
        notifyVia = "";
    } else {
        if (gblTransEmail == 1) {
            notifyVia = kony.i18n.getLocalizedString("email") + " [" + frmIBTranferLP.tbxEmail.text + "]";
        } else if (gblTrasSMS == 1) {
            var phoneNo = removeHyphenIB(frmIBTranferLP.txtTransLndSmsNEmail.text);
            if (phoneNo != "") {
                notifyVia = kony.i18n.getLocalizedString("SMS") + " [xxx-xxx-" + phoneNo.substring(6, 10) + "]";
            }
        }
    }
    noteToRecipient = frmIBTransferNowCompletion.lblNTRVal.text
    if (gblTransEmail == 1 && gblTrasSMS == 1) {
        notifyVia = "";
        noteToRecipient = "";
    }
    var transferOrderDate = frmIBTransferNowCompletion.lblTransferVal.text;
    transferOrderDate = transferOrderDate.split(" ")[0];
    var transferSchedule = "";
    if (frmIBTransferNowCompletion.lblRepeatValue.text == "Once") {
        transferSchedule = frmIBTransferNowCompletion.lblStartOnValue.text + " " + " Repeat " + frmIBTransferNowCompletion.lblRepeatValue.text;
    } else if (frmIBTransferNowCompletion.lblExecuteValue.text == "-") {
        transferSchedule = frmIBTransferNowCompletion.lblStartOnValue.text + " " + " Repeat " + frmIBTransferNowCompletion.lblRepeatValue.text;
    } else {
        transferSchedule = frmIBTransferNowCompletion.lblStartOnValue.text + " to " + frmIBTransferNowCompletion.lblEnDONValue.text + " " + " Repeat " + frmIBTransferNowCompletion.lblRepeatValue.text + " for " + frmIBTransferNowCompletion.lblExecuteValue.text + " times ";
    }
    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "fromAcctNo": "xxx-x-" + frmIBTransferNowCompletion.lblAccountNo.text.substring(6, 11) + "-x",
        "fromAcctName": frmIBTransferNowConfirmation.lblProductName.text,
        //   "fromAcctName":gblCustomerName,
        "toAcctNo": frmIBTransferNowCompletion.lblXferAccNo.text,
        "toAcctName": frmIBTransferNowConfirmation.lblXferToAccTyp.text != "" ? frmIBTransferNowConfirmation.lblXferToAccTyp.text : frmIBTransferNowCompletion.lblXferToName.text,
        "bankName": frmIBTransferNowCompletion.lblXferToBankName.text,
        "splitResult": splitResult,
        "myNote": frmIBTransferNowCompletion.lblMNVal.text,
        "noteToRecipient": noteToRecipient,
        "notifyVia": notifyVia,
        "transferOrderDate": transferOrderDate
    };
    //if split transfer  
    if (frmIBTransferNowCompletion.hbxSplitXfer.isVisible == true) {
        var segData = frmIBTransferNowCompletion.segSplitDet.data;
        for (i = 0; i < segData.length; i++) {
            var splitAmtVal = commaFormatted(parseFloat(removeCommos(segData[i]["lblSplitAmtVal"])).toFixed(2));
            var splitFeeVal = commaFormatted(parseFloat(removeCommos(segData[i]["lblSplitFeeVal"])).toFixed(2));
            splitAmtVal = splitAmtVal.replace(/,/g, "");
            splitFeeVal = splitFeeVal.replace(/,/g, "");
            var tempRec = {
                "amount": commaFormatted(splitAmtVal), // + kony.i18n.getLocalizedString("currencyThaiBaht"),
                "fee": commaFormatted(splitFeeVal), // + kony.i18n.getLocalizedString("currencyThaiBaht"),
                "transRefId": segData[i]["lblSplitTRNVal"]
            }
            splitResult.push(tempRec);
        }
        pdfImagedata["totalAmount"] = commaFormatted(parseFloat(removeCommos(frmIBTransferNowCompletion.lblSplitTotAmtVal.text)).toFixed(2));
        pdfImagedata["totalFee"] = commaFormatted(parseFloat(removeCommos(frmIBTransferNowCompletion.lblSplitTotFeeVal.text)).toFixed(2));
        pdfImagedata["splitResult"] = splitResult;
        pdfImagedata["myNote"] = frmIBTransferNowCompletion.lblSplitMNVal.text;
        pdfImagedata["noteToRecipient"] = frmIBTransferNowCompletion.lblSplitNTRVal.text;
        pdfImagedata["bankName"] = frmIBTransferNowConfirmation.lblSplitToBankNameVal.text;
    }
    //if normal transfer
    else {
        var tempRec = {
            "amount": commaFormatted(parseFloat(removeCommos(frmIBTransferNowCompletion.lblAmtVal.text)).toFixed(2)),
            "fee": commaFormatted(parseFloat(removeCommos(frmIBTransferNowCompletion.lblFeeVal.text)).toFixed(2)),
            "transRefId": frmIBTransferNowCompletion.lblTRNVal.text
        }
        splitResult.push(tempRec);
        pdfImagedata["splitResult"] = splitResult;
    }
    if (gblPaynow) {
        inputParam["outputtemplatename"] = "Transfers_Details_" + kony.os.date("DDMMYYYY")
    } else {
        pdfImagedata["transferSchedule"] = transferSchedule;
        inputParam["outputtemplatename"] = "Future_Transfer_Set_Details_" + kony.os.date("DDMMYYYY")
    }
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/GenericPdfImageServlet";
    var transRefId = frmIBTransferNowCompletion.lblTRNVal.text;
    if (frmIBTransferNowCompletion.hbxSplitXfer.isVisible == true) {
        transRefId = frmIBTransferNowCompletion.segSplitDet.data[0].lblSplitTRNVal;
    } else {
        transRefId = frmIBTransferNowCompletion.lblTRNVal.text;
    }
    if (gblPaynow) {
        if (gblSelTransferMode == 2) {
            if (gblisTMB == "11") {
                savePDFIB(filetype, "604", transRefId); // Transfer ou us (TMB,Other TMB)
            } else {
                savePDFIB(filetype, "605", transRefId); // Prompt Pay (Other Bank)
            }
        } else if (gblSelTransferMode == 3) {
            if (gblisTMB == "11") {
                savePDFIB(filetype, "606", transRefId); // Citizen Transfer ou us (TMB,Other TMB)
            } else {
                savePDFIB(filetype, "607", transRefId); // Citizen Prompt Pay (Other Bank)
            }
        } else if (gblisTMB == "11") {
            if (gblTransEmail == 1 && gblTrasSMS == 1) {
                savePDFIB(filetype, "023", transRefId);
            } else {
                savePDFIB(filetype, "024", transRefId);
            }
        } else {
            if (gblTrasORFT == 1 || gblTransSMART == 1) {
                savePDFIB(filetype, "025", transRefId);
            }
        }
    } else {
        saveFuturePDF(filetype, "026", transRefId);
        //invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
    }
}
/*************************************************************************

	Module	: checkTDBusinessHoursIB
	Author  : Kony
	Purpose : checkTDBusinessHoursIB

****/
function checkTDBusinessHoursIB() {
    var inputParam = {}
    invokeServiceSecureAsync("checkTDBussinessHours", inputParam, callBackTDBussinessHoursIB)
}
/**************************************************************************************
		Module	: callBackTDBussinessHours
		Author  : Kony
		Date    : May 05, 2013
		Purpose : callback function for callBackTDBussinessHours
*****************************************************************************************/
function callBackTDBussinessHoursIB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var tdflag = resulttable["tdBusHrsFlag"];
            if (tdflag == "false") {
                dismissLoadingScreenPopup();
                alert("" + kony.i18n.getLocalizedString("keyTdBusinessHours"));
                return false;
            } else {
                checkDepositAccountInqIB();
            }
        } else {
            dismissLoadingScreenPopup();
            if (resulttable["errMsg"] != undefined) {
                alert(resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        } //end of else
    } //end of status=400
}

function createTransferSegmentRecordIB(record, icon, lblRem, remanFee, accountName, acctID) {
    var tempRec = {
        lblProduct: kony.i18n.getLocalizedString("Product"),
        lblProductVal: record["productNameCurrentLocale"],
        lblACno: kony.i18n.getLocalizedString("AccountNo"),
        lblRemFee: lblRem,
        lblRemFeeval: remanFee,
        img1: icon,
        lblCustName: accountName,
        lblBalance: commaFormatted(record["availableBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
        lblActNoval: addHyphenIB(acctID),
        lblSliderAccN1: kony.i18n.getLocalizedString("Product") + ".:",
        lblSliderAccN2: record["accType"],
        lblDummy: record["fiident"],
        remainingFee: record["remainingFee"],
        prodCode: record["productID"],
        accountWOF: addHyphenIB(record["accId"]),
        nickName: record["acctNickName"],
        custAcctName: record["accountName"],
        isOtherBankAllowed: record.isOtherBankAllowed,
        isOtherTMBAllowed: record.isOtherTMBAllowed,
        isAllowedSA: record.allowedSA,
        isAllowedCA: record.allowedCA,
        isAllowedTD: record.allowedTD
    }
    return tempRec;
}

function getTMBOWNAccountsNewIB() {
    selectedRow = 0;
    gblXferPhoneNo1 = "";
    gblXferEmail1 = "";
    var selectedFromacc = removeHyphenIB(gblcwselectedData.accountWOF);
    var tab1 = []
    var indexOfSelectedIndex = frmIBTranferLP.segXferRecipentsList.selectedItems[0];
    var selectedData = [{
        lblXferRecipentsName: indexOfSelectedIndex.lblXferRecipentsName,
        lblXferRecipentsAccount: indexOfSelectedIndex.lblXferRecipentsAccount,
        personalizedId: indexOfSelectedIndex.personalizedId,
        imgXferArrow: {
            src: "bg_arrow_bottom.png"
        },
        imgXferRecipentsImage: gblMyProfilepic,
        btnXferMobile: {
            src: "phoneicon.png"
        },
        mobileNo: indexOfSelectedIndex.mobileNo,
        //btnXferFb:fbimg,
        //facebookNo: indexOfSelectedIndex.facebookNo,
        category: "own1",
        template: hbxXferSegmentSelected
    }];
    if (gblMyProfilepic != undefined && gblMyProfilepic != "") gblXferRecImg = gblMyProfilepic;
    else gblXferRecImg = "avatar_dis.png";
    kony.table.insert(tab1, selectedData[0]);
    for (var i = 0; i < gblOwnRcData.length; i++) {
        var fromAcctID = gblOwnRcData[i].acctId;
        var custName = gblOwnRcData[i].acctNickName;
        var prdCode = gblOwnRcData[i].prdCode;
        if (custName == "" || custName == undefined) custName = gblOwnRcData[i].productNmeEN
        if (prdCode == "otherbank") {
            fromAcctID = encodeAccntNumbers(fromAcctID)
            var pic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblOwnRcData[i].bankCde + "&modIdentifier=BANKICON"; //""https://vit.tau2904.com/tmb/Bank-logo/bank-logo-22.jpg
            var bn = getBankNameIB(gblOwnRcData[i].bankCde);
            var accountData = {
                img2: pic,
                lbl3: custName,
                lbl4: fromAcctID,
                lbl5: gblOwnRcData[i].bankCde,
                lblBnkName: bn,
                img3: "",
                hiddenmain: "sub",
                accountName: gblOwnRcData[i].acctName,
                template: hbxXferSegmentExpanded
            }
        } else {
            if (fromAcctID.length == 14) {
                var frmid = ""
                for (var j = 4; j < 14; j++) frmid = frmid + fromAcctID[j];
                fromAcctID = frmid;
            }
            fromAcctID = addHyphenIB(fromAcctID)
            var imageName = "avatar_dis.png";
            imageName = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + gblOwnRcData[i]["ICON_ID"] + "&modIdentifier=PRODICON";
            var accountData = {
                img2: imageName,
                lbl3: custName,
                lbl4: fromAcctID,
                lbl5: gblTMBBankCD,
                lblBnkName: "TMB",
                img3: "",
                hiddenmain: "sub",
                template: hbxXferSegmentExpanded
            }
        }
        var fridhyph = removeHyphenIB(fromAcctID);
        if (fridhyph != selectedFromacc) kony.table.insert(tab1, accountData);
    }
    var indexOfSelectedRow = frmIBTranferLP.segXferRecipentsList.selectedIndex[1];
    var mno = frmIBTranferLP.segXferRecipentsList.selectedItems[0].mobileNo;
    var mobileimg = ""
    var fbimg = ""
    if (mno != null && mno.length > 0) {
        mno = removeHyphenIB(mno)
        mno = "xxx-xxx-" + mno.substring(6, 10);
        mobileimg = "phoneicon.png"
    } else {
        mobileimg = "empty.png"
    }
    var tempMbFb = [{
            img2: "mobileappX.png",
            lbl3: "Mobile",
            lbl4: mno,
            lbl5: "",
            img3: "",
            category: "sub",
            template: hbxXferSegmentExpanded
        }]
        /*for (var i = 0; i < tempMbFb.length; i++) {
        	if (i == 0) {
        		if (mno != null && mno.length > 0) kony.table.insert(tab1, tempMbFb[i])
        	} else kony.table.insert(tab1, tempMbFb[i])
        }*/
    frmIBTranferLP.segXferRecipentsList.widgetDataMap = {
        btnXferMobile: "btnXferMobile",
        btnXferFb: "btnXferFb",
        imgXferRecipentsImage: "imgXferRecipentsImage",
        imgXferArrow: "imgXferArrow",
        lblXferRecipentsName: "lblXferRecipentsName",
        lblXferRecipentsAccount: "lblXferRecipentsAccount",
        lbl3: "lbl3",
        lbl4: "lbl4",
        lbl5: "lbl5",
        lblBnkName: "lblBnkName",
        img2: "img2",
        img3: "img3"
    }
    var tab2 = []
    for (var i = 0; i < indexOfSelectedRow; i++) {
        kony.table.insert(tab2, gblTransferToRecipientData[i])
    }
    //appending selected row and account list of selected row
    kony.table.append(tab2, tab1);
    //adding data after selected row of segment
    for (var j = indexOfSelectedRow + 1; j < gblTransferToRecipientData.length; j++) {
        kony.table.insert(tab2, gblTransferToRecipientData[j]);
    }
    frmIBTranferLP.segXferRecipentsList.removeAll()
    dismissLoadingScreenPopup();
    frmIBTranferLP.segXferRecipentsList.setData(tab2)
}

function getTransferToRecipientsAccountsNewIB() {
    var resulttable = [];
    var pid = frmIBTranferLP.segXferRecipentsList.selectedItems[0]["personalizedId"];
    var ind = "Accounts" + frmIBTranferLP.segXferRecipentsList.selectedIndex[1]
    var accounts = [];
    for (var i = 0; i < gblRcAllAccountsData.length; i++) {
        var ind = "Accounts" + i;
        if (gblRcAllAccountsData[i][ind][0] != undefined || gblRcAllAccountsData[i][ind][0] != null) {
            if (pid == gblRcAllAccountsData[i][ind][0].personalizedId) {
                accounts = gblRcAllAccountsData[i][ind]
                break;
            }
        } else {
            accounts = [];
        }
    }
    if ((frmIBTranferLP.txbXferSearch.text != null) && (frmIBTranferLP.txbXferSearch.text != undefined) && (frmIBTranferLP.txbXferSearch.text.length > 2)) {
        segData = frmIBTranferLP.segXferRecipentsList.data;
    } else segData = gblTransferToRecipientData;
    var indexOfSelectedRow = frmIBTranferLP.segXferRecipentsList.selectedIndex[1];
    var dataLen = segData.length;
    var segLen = frmIBTranferLP.segXferRecipentsList.data.length;
    if (dataLen == segLen) {
        selectedRow = indexOfSelectedRow
    } else if (selectedRow < indexOfSelectedRow) {
        indexOfSelectedRow = indexOfSelectedRow - (segLen - dataLen);
        selectedRow = indexOfSelectedRow;
    } else {
        selectedRow = indexOfSelectedRow;
    }
    var indexOfSelectedIndex = frmIBTranferLP.segXferRecipentsList.selectedItems[0];
    var mno = indexOfSelectedIndex.mobileNo;
    var fbno = indexOfSelectedIndex.fbNo;
    var emailID = indexOfSelectedIndex.imgURL;
    var mobileimg = ""
    var fbimg = ""
    if (mno != undefined && mno != "undefined" && mno != null && mno.length > 0) {
        mobileimg = "phoneicon.png"
        mno = replaceCommon(mno, "-", "")
        gblXferPhoneNo1 = mno;
        mno = maskingIB(mno);
    } else {
        mobileimg = ""
        gblXferPhoneNo1 = ""
    }
    if (fbno != undefined && fbno != "undefined" && fbno != null && fbno.length > 0) {
        fbimg = "facebookicon.png"
    } else {
        fbimg = ""
    }
    if (emailID != null && emailID.length > 0) {
        gblXferEmail1 = emailID;
    } else {
        gblXferEmail1 = ""
    }
    var bnkLogoURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/Bank-logo" + "/bank-logo-";
    var selectedData = [{
        btnXferFb: fbimg,
        btnXferMobile: mobileimg,
        imgfav: indexOfSelectedIndex.onUsLogo,
        imgXferRecipentsImage: indexOfSelectedIndex.imgXferRecipentsImage,
        imgXferArrow: {
            src: "bg_arrow_bottom.png"
        },
        lblXferRecipentsName: indexOfSelectedIndex.lblXferRecipentsName,
        lblXferRecipentsAccount: indexOfSelectedIndex.lblXferRecipentsAccount,
        personalizedId: indexOfSelectedIndex.personalizedId,
        imgURL: "initial",
        isnonTMBAccount: "false",
        category: "main1",
        fbNo: indexOfSelectedIndex.fbNo,
        mobileNo: indexOfSelectedIndex.mobileNo,
        template: hbxXferSegmentSelected
    }];
    gblXferRecImg = indexOfSelectedIndex.imgXferRecipentsImage;
    var temp = []
    var tab1 = []
    kony.table.append(tab1, selectedData)
    frmIBTranferLP.segXferRecipentsList.widgetDataMap = {
        btnXferFb: "btnXferFb",
        btnXferMobile: "btnXferMobile",
        imgXferRecipentsImage: "imgXferRecipentsImage",
        imgXferArrow: "imgXferArrow",
        imgfav: "imgfav",
        onUsLogo: "onUsLogo",
        lblXferRecipentsName: "lblXferRecipentsName",
        lblXferRecipentsAccount: "lblXferRecipentsAccount",
        lbl3: "lbl3",
        lbl4: "lbl4",
        lbl5: "lbl5",
        lblBnkName: "lblBnkName",
        img2: "img2",
        img3: "img3"
    }
    resulttable.CRMAccountsInq = accounts;
    for (var i = 0; i < resulttable.CRMAccountsInq.length; i++) {
        var isAccntFav = resulttable.CRMAccountsInq[i]["acctFavFlag"];
        //kony.type(accFavoriteFlag)
        var btnFavSkin;
        if (isAccntFav != null && isAccntFav.length > 0) {
            if (isAccntFav.toString() == "y" || isAccntFav.toString() == "Y") {
                btnFavSkin = "starblue.png"
            } else {
                btnFavSkin = "";
            }
        } else {
            btnFavSkin = "";
        }
        var bankName = getBankNameIB(resulttable.CRMAccountsInq[i].bankCde)
        var bankLogo = "";
        bankLogo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + resulttable.CRMAccountsInq[i].bankCde + "&modIdentifier=BANKICON";
        var acctNo = resulttable.CRMAccountsInq[i].acctId;
        acctNo = replaceCommon(acctNo, "-", "")
        if (resulttable.CRMAccountsInq[i].bankCde == "11") {
            if (acctNo.length == 14) {
                var frmid = ""
                for (var j = 4; j < 14; j++) frmid = frmid + acctNo[j];
                acctNo = frmid;
            }
            acctNo = encodeAccntNumbers(acctNo)
        } else {
            acctNo = encodeAccntNumbers(acctNo);
        }
        var accountData = {
            img2: bankLogo,
            lbl3: resulttable.CRMAccountsInq[i].acctNickName,
            lbl4: acctNo,
            lbl5: resulttable.CRMAccountsInq[i].bankCde,
            lblBnkName: bankName,
            img3: btnFavSkin,
            category: "sub",
            template: hbxXferSegmentExpanded,
            accountName: resulttable.CRMAccountsInq[i].acctName
        }
        kony.table.insert(tab1, accountData);
    }
    var tempMbFb = [{
            img2: "mobileappX.png",
            lbl3: "Mobile",
            lbl4: mno,
            lbl5: "",
            img3: "",
            category: "sub",
            template: hbxXferSegmentExpanded
        }, {
            img2: "facebookicoX.png",
            lbl3: "Facebook",
            lbl4: fbno,
            lbl5: "",
            img3: "",
            category: "sub",
            template: hbxXferSegmentExpanded
        }]
        /* for (var i = 0; i < tempMbFb.length; i++) {
             if (i == 0) {
                 if (mno != null && mno.length > 0) kony.table.insert(tab1, tempMbFb[i])
             } else if (i == 1) {
                 if (fbno != null && fbno.length > 0) kony.table.insert(tab1, tempMbFb[i])
             } else kony.table.insert(tab1, tempMbFb[i])
         } */
    var tab2 = []
    for (var i = 0; i < indexOfSelectedRow; i++) {
        kony.table.insert(tab2, segData[i])
    }
    kony.table.append(tab2, tab1)
    for (var j = selectedRow + 1; j < segData.length; j++) {
        kony.table.insert(tab2, segData[j])
    }
    frmIBTranferLP.segXferRecipentsList.removeAll()
    dismissLoadingScreenPopup();
    frmIBTranferLP.segXferRecipentsList.setData(tab2)
    gblAckFlage = "false"
}

function getToRecipientsNewIB(prod, acc) {
    gblTransferToRecipientData = [];
    gblTransferToRecipientCache = [];
    if (frmIBTranferLP.lblXferFromNameRcvd.text != "") {
        var inputParam = {}
        inputParam["prodId"] = gblcwselectedData.prodCode;
        inputParam["accId"] = removeHyphenIB(gblcwselectedData.accountWOF);
        showLoadingScreenPopup();
        invokeServiceSecureAsync("getRecipientsForTransfer", inputParam, callBackTransferToReciepientsNewIB);
    }
    frmIBTranferLP.txbXferSearch.text = "";
}
gblRcAllAccountsData = []
gblOwnRcData = [];

function callBackTransferToReciepientsNewIB(status, resulttable) {
    if (status == 400) {
        if ((resulttable["OwnAccounts"].length > 0 || resulttable["RecepientsList"].length > 0)) {
            var mobileimg = "";
            var mobileno = "";
            var fbimg = "";
            var rcImg = "";
            var fbno = "";
            var favimg = "";
            var XferRcURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y&personalizedId=";
            var temp1 = [];
            //static data
            var randomnum = Math.floor((Math.random() * 10000) + 1);
            gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&dummy=" + randomnum;
            gblRcAllAccountsData = resulttable["AccountsList"];
            gblOwnRcData = resulttable["OwnAccounts"];
            var fromData = frmIBTransferCustomWidgetLP.custom1016496273208904.data;
            var selectedItem = frmIBTransferCustomWidgetLP.custom1016496273208904.selectedItem;
            var prodCode = fromData[selectedItem].prodCode;
            //var linkedAccnt= addHyphenIB(gblXferLnkdAccnts);
            var frmAccnt = fromData[selectedItem].accountWOF;
            frmAccnt = removeHyphenIB(frmAccnt);
            var mbimg;
            if (gblPHONENUMBER.length == 0 || gblPHONENUMBER == "") mbimg = "empty.png"
            else mbimg = "phoneicon.png";
            var temp = [{
                lblXferRecipentsName: gblCustomerName,
                lblXferRecipentsAccount: resulttable["ownCount"] + " " + kony.i18n.getLocalizedString("Receipent_Accounts"),
                imgXferArrow: {
                    src: "bg_arrow_right.png"
                },
                imgURL: "",
                imgfav: "",
                imgXferRecipentsImage: {
                    src: gblMyProfilepic
                },
                btnXferMobile: mbimg,
                mobileNo: gblPHONENUMBER,
                //imgfb: fbimg,
                //facebookNo: fbno,
                category: "own",
                personalizedId: "",
                template: hbxXferSegmentInitial
            }]
            if (resulttable["ownCount"] != "0") kony.table.insert(temp1, temp[0])
                //resulttable.CRMAccountList[i].personalizedPictureId
            if (gbltdFlag != kony.i18n.getLocalizedString("termDeposit") && fromData[selectedItem].prodCode != "206") {
                if (resulttable["RecepientsList"].length != 0) {
                    resulttable.CRMAccountList = resulttable["RecepientsList"];
                    for (var i = 0; i < resulttable.CRMAccountList.length; i++) {
                        //checking for mobile No exists or not
                        if (resulttable.CRMAccountList[i].personalizedMobileNum != null && resulttable.CRMAccountList[i].personalizedMobileNum.trim() != "" && resulttable.CRMAccountList[i].personalizedMobileNum.length > 0) {
                            mobileimg = "phoneicon.png";
                            mobileno = resulttable.CRMAccountList[i].personalizedMobileNum.trim();
                        } else {
                            mobileimg = "";
                            mobileno = resulttable.CRMAccountList[i].personalizedMobileNum;
                        }
                        if (resulttable.CRMAccountList[i].personalizedFavFlag != null && resulttable.CRMAccountList[i].personalizedFavFlag == "Y") {
                            favimg = "icon_star_focus.png";
                        } else {
                            favimg = "";
                        }
                        //checking for facebook ID exists or not	
                        if (resulttable.CRMAccountList[i].personalizedFbookId != null && resulttable.CRMAccountList[i].personalizedFbookId.trim() != "" && resulttable.CRMAccountList[i].personalizedFbookId.length > 0) {
                            fbimg = "facebookicon.png";
                            fbno = resulttable.CRMAccountList[i].personalizedFbookId.trim();
                        } else {
                            fbimg = "";
                            fbno = resulttable.CRMAccountList[i].personalizedFbookId;
                        }
                        //checking for Recipient images whether exists or not
                        var randomnum = Math.floor((Math.random() * 10000) + 1);
                        if (resulttable.CRMAccountList[i].personalizedPicId == null || resulttable.CRMAccountList[i].personalizedPicId == "") {
                            rcImg = XferRcURL + resulttable.CRMAccountList[i].personalizedId + "&rr=" + randomnum;
                        } else {
                            if (kony.string.endsWith(resulttable.CRMAccountList[i].personalizedPicId.toString(), "nouserimg.jpg")) {
                                rcImg = XferRcURL + resulttable.CRMAccountList[i].personalizedId + "&rr=" + randomnum;
                            } else {
                                if (resulttable.CRMAccountList[i].personalizedPicId.indexOf("fbcdn") >= 0) {
                                    rcImg = resulttable.CRMAccountList[i].personalizedPicId;
                                } else {
                                    rcImg = XferRcURL + resulttable.CRMAccountList[i].personalizedId + "&rr=" + randomnum;
                                }
                            }
                        }
                        if (resulttable.CRMAccountList[i].accountCount == 0 && (resulttable.CRMAccountList[i].personalizedMobileNum == null || resulttable.CRMAccountList[i].personalizedMobileNum == "No Number" || resulttable.CRMAccountList[i].personalizedMobileNum == "")) {}
                        //else{
                        var temp = [{
                            btnXferFb: fbimg,
                            btnXferMobile: mobileimg,
                            imgXferRecipentsImage: rcImg,
                            onUsLogo: favimg,
                            imgXferArrow: {
                                src: "bg_arrow_right.png"
                            },
                            lblXferRecipentsName: resulttable.CRMAccountList[i].personalizedName,
                            lblXferRecipentsAccount: resulttable.CRMAccountList[i].accountCount + " " + kony.i18n.getLocalizedString("Receipent_Accounts"),
                            personalizedId: resulttable.CRMAccountList[i].personalizedId,
                            imgURL: resulttable.CRMAccountList[i].personalizedEmailId,
                            isnonTMBAccount: "false",
                            category: "main",
                            fbNo: fbno,
                            mobileNo: mobileno,
                            template: hbxXferSegmentInitial,
                            recepientID: resulttable.CRMAccountList[i].personalizedId
                        }]
                        kony.table.insert(temp1, temp[0])
                            //}
                    } //for
                } //end of if(acctLen != 0)
            } //end of if(gbltdFlag		
            gblTransferToRecipientData = temp1; //caching for 
            gblTransferToRecipientCache = temp1; //caching for ToDo
            segData = temp1;
            frmIBTranferLP.lblMsg.setVisibility(false);
            frmIBTranferLP.segXferRecipentsList.setVisibility(true);
            frmIBTranferLP.segXferRecipentsList.removeAll();
            frmIBTranferLP.segXferRecipentsList.setData(gblTransferToRecipientData);
            // for row selection of segment
            //gblAckFlage="true";
            dismissLoadingScreenPopup();
            frmIBTranferLP.show();
            nameofform = kony.application.getCurrentForm();
            if (kony.i18n.getCurrentLocale() != "th_TH") nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocus"
            else nameofform.btnMenuTransfer.skin = "btnIBMenuTransferFocusThai"
            nameofform.segMenuOptions.removeAll();
            //frmIBTranferLP.vbox47679117772050.skin = "vboxLightBlue";
        } else {
            dismissLoadingScreenPopup();
            showAlert(newLineTextIB(kony.i18n.getLocalizedString("TRErr_NoEligibleToAcc")), kony.i18n.getLocalizedString("info"));
        }
    } //status 400
}

function masterConfigurationForTransfers(resulttable) {
    /*** below are configurable params driving from Backend and get Cached. **/
    gblFinancialTxnIBLock = resulttable.enableMaintainenceFinancialTxnIB;
    gblMaxTransferORFT = resulttable.ORFTTransLimit;
    gblMaxTransferSMART = resulttable.SMARTTransLimit;
    gblTransORFTSplitAmnt = resulttable.ORFTTransSplitAmnt;
    gblTransSMARTSplitAmnt = resulttable.SMARTTransAmnt
    gblLimitORFTPerTransaction = resulttable.ORFTTransSplitAmnt;
    gblLimitSMARTPerTransaction = resulttable.SMARTTransAmnt
    gblXerSplitData = [];
    var temp1 = [];
    var temp = {
        ORFTRange1Lower: resulttable.ORFTRange1Lower,
        ORFTRange1Higher: resulttable.ORFTRange1Higher,
        SMARTRange1Higher: resulttable.SMARTRange1Higher,
        SMARTRange1Lower: resulttable.SMARTRange1Lower,
        ORFTRange2Lower: resulttable.ORFTRange2Lower,
        ORFTRange2Higher: resulttable.ORFTRange2Higher,
        SMARTRange2Higher: resulttable.SMARTRange2Higher,
        SMARTRange2Lower: resulttable.SMARTRange2Lower,
        ORFTSPlitFeeAmnt1: resulttable.ORFTSPlitFeeAmnt1,
        ORFTSPlitFeeAmnt2: resulttable.ORFTSPlitFeeAmnt2,
        SMARTSPlitFeeAmnt1: resulttable.SMARTSPlitFeeAmnt1,
        SMARTSPlitFeeAmnt2: resulttable.SMARTSPlitFeeAmnt2,
        WithinBankOwnAccLimit: resulttable.WithinBankOwnAccLimit,
        WithinBankOwnAccLimitTransaction: resulttable.WithinBankOwnAccLimitTransaction
    }
    kony.table.insert(temp1, temp)
    gblXerSplitData = temp1;
}

function populateOtherAccountsForRecipients(resulttable) {
    for (var i = 0; i < resulttable.OtherAccounts.length; i++) {
        var prdCode = resulttable.OtherAccounts[i].productID;
        var otherAccID = resulttable.OtherAccounts[i].accId;
        if (otherAccID.length == 14) {
            otherAccID = otherAccID.substr(4, 10);
        }
        if (prdCode == 290 || prdCode == "otherbank") {
            var temp = [{
                custName: resulttable.OtherAccounts[i].accountName,
                acctNo: addHyphenMB(otherAccID),
                nickName: resulttable.OtherAccounts[i].acctNickName,
                productName: resulttable.OtherAccounts[i].productNmeEN,
                productID: resulttable.OtherAccounts[i].productID,
                bankCD: resulttable.OtherAccounts[i].bankCD
            }]
            kony.table.insert(gblTransfersOtherAccounts, temp[0])
        }
    } //for
}