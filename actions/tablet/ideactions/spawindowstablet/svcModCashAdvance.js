function customerAccountInquiryForSelectAccounts() {
    showLoadingScreen();
    var inputParam = {}
    inputParam["cashAdvanceFlag"] = "true";
    invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackGetToAccountCashAdvDetails);
}

function callBackGetToAccountCashAdvDetails(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (undefined != resulttable["custAcctRec"]) {
                var cashAdvanceAccounts = [];
                for (var i = 0; i < resulttable["custAcctRec"].length; i++) {
                    var accountName = "";
                    var accountNameTH = "";
                    var accountNameEN = "";
                    var accId = resulttable["custAcctRec"][i]["accId"];
                    var acctStatusCode = resulttable["custAcctRec"][i]["acctStatusCode"];
                    if ((resulttable["custAcctRec"][i]["acctNickName"]) == null || (resulttable["custAcctRec"][i]["acctNickName"]) == '') {
                        if (kony.i18n.getCurrentLocale() == "th_TH") {
                            accountName = resulttable["custAcctRec"][i]["acctNickNameTH"];
                        } else {
                            accountName = resulttable["custAcctRec"][i]["acctNickNameEN"];
                        }
                    } else {
                        accountName = resulttable["custAcctRec"][i]["acctNickName"];
                    }
                    if (isNotBlank(accountName)) {
                        accountNameEN = resulttable["custAcctRec"][i]["acctNickNameEN"];
                        accountNameTH = resulttable["custAcctRec"][i]["acctNickNameTH"];
                        var accountNo = accId.substring(accId.length - 10, accId.length);
                        var tempRecord = {
                            "imgSelect": "tick_icon.png",
                            "lblAccountNum": accountNo,
                            "lblAccountName": accountName,
                            "lblAccountNameTH": accountNameTH,
                            "lblAccountNameEN": accountNameEN,
                            "lblAccountStatus": acctStatusCode
                        };
                        cashAdvanceAccounts.push(tempRecord);
                    }
                }
                frmMBCashAdvAcctSelect.segAccounts.setData(cashAdvanceAccounts);
                frmMBCashAdvAcctSelect.segAccounts.setVisibility(true);
                frmMBCashAdvAcctSelect.lblErrMsg.setVisibility(false);
                dismissLoadingScreen();
                frmMBCashAdvAcctSelect.show();
            } else {
                dismissLoadingScreen();
                frmMBCashAdvAcctSelect.segAccounts.setVisibility(false);
                frmMBCashAdvAcctSelect.lblErrMsg.setVisibility(true);
                frmMBCashAdvAcctSelect.lblErrMsg.text = kony.i18n.getLocalizedString("CAV04_msgNoEligibleAcc");
                frmMBCashAdvAcctSelect.show();
                return false;
            }
        } else {
            //Handle Case No Valid Accounts Available for Transfer
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("CAV04_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}

function callCashAdvanceCompositeService(tranPassword) {
    var inputParam = {};
    inputParam["password"] = tranPassword;
    inputParam["XferAdd_fromAcctNo"] = gblCACardRefNumber;
    inputParam["XferAdd_toAcctNo"] = gblCASelectedToAcctNum;
    inputParam["XferAdd_fromAcctNickName"] = frmCAPaymentPlanConfirmation.lblCardName.text;
    inputParam["XferAdd_toAcctNickName"] = frmCAPaymentPlanConfirmation.lblToAcctName.text;
    var caAmount = frmCAPaymentPlanConfirmation.lblAmountVal.text;
    if (isNotBlank(caAmount)) {
        caAmount = caAmount.replace(/,/g, "");
        caAmount = kony.string.replace(caAmount, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        inputParam["XferAdd_amount"] = caAmount.trim();
        invokeServiceSecureAsync("cashAdvanceCompositeService", inputParam, callBackConfirmCashAdvanceService);
    } else {
        showAlert("Invalid amount.", kony.i18n.getLocalizedString("info"));
    }
}

function callBackConfirmCashAdvanceService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            popupTractPwd.dismiss();
            dismissLoadingScreen();
            if (resulttable["CA_BusinessHrsFlag"] == "false") {
                var CA_StartTime = resulttable["CA_StartTime"];
                var CA_EndTime = resulttable["CA_EndTime"];
                var errorMsg = kony.i18n.getLocalizedString("Card_Err_ServiceHour");
                errorMsg = errorMsg.replace("{service_hours}", CA_StartTime + " - " + CA_EndTime);
                showAlertWithCallBack(errorMsg, kony.i18n.getLocalizedString("info"), showAccuntSummaryScreen);
                return false;
            } else {
                gotoCAPaymentPlanCompleteMB("0");
            }
        } else if (resulttable["opstatus"] == 1) {
            popupTractPwd.dismiss();
            dismissLoadingScreen();
            gotoCAPaymentPlanCompleteMB("-1");
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                showAlert("" + resulttable["errMessage"], kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                if (isNotBlank(result["errMsg"])) {
                    showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                } else {
                    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                }
                return false;
            } else {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            popupTractPwd.dismiss();
            gotoCAPaymentPlanCompleteMB("-1");
            //var errorMsgTop = resulttable["errMsg"];
            //if(!isNotBlank(resulttable["errMsg"])){
            //	errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
            //}
            //showAlert(errorMsgTop, kony.i18n.getLocalizedString("info"));
        }
    } else {
        dismissLoadingScreen();
        popupTractPwd.dismiss();
        if (status == 300) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("info"));
        } else {
            showAlert("status : " + status + " errMsg: " + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }
    }
}

function callCashAdvanceSaveToSessionService() {
    showLoadingScreen();
    var inputParam = {};
    var caAmount = frmCashAdvancePlanDetails.lblEnterAmount.text;
    if (isNotBlank(caAmount)) {
        caAmount = caAmount.replace(/,/g, "");
    } else {
        caAmount = "0.00";
    }
    var caFee = "" + frmCashAdvancePlanDetails.lblFeeVal.text;
    caFee = parseFloat(removeCommos(caFee)).toFixed(2);
    var annualRate = frmCashAdvancePlanDetails.lblAnnualRateVal.text;
    if (isNotBlank(annualRate)) {
        annualRate = annualRate.replace("%", "");
    } else {
        annualRate = "0.00";
    }
    inputParam["amount"] = caAmount;
    inputParam["fee"] = caFee;
    inputParam["toAcctNo"] = gblCASelectedToAcctNum;
    inputParam["annualRate"] = annualRate;
    inputParam["cardRefId"] = gblCACardRefNumber;
    invokeServiceSecureAsync("cashAdvanceSaveToSession", inputParam, callBackCashAdvanceSaveToSessionService);
}

function callBackCashAdvanceSaveToSessionService(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            frmCAPaymentPlanConfirmation.lblFromAcctName.text = frmCashAdvancePlanDetails.lblCustName.text;
            var cardNumber = kony.string.replace(frmCashAdvancePlanDetails.lblCardNum.text, " ", "");
            frmCAPaymentPlanConfirmation.lblFromAcctNo.text = maskCreditCard(cardNumber);
            frmCAPaymentPlanConfirmation.lblToAcctName.text = frmCashAdvancePlanDetails.lblSelectedAccount.text;
            frmCAPaymentPlanConfirmation.lblToAcctNo.text = formatAccountNo(resulttable["toAcctNo"]);
            frmCAPaymentPlanConfirmation.lblReferenceVal.text = resulttable["referenceNum"];
            frmCAPaymentPlanConfirmation.lblAmountVal.text = commaFormatted(parseFloat(resulttable["transferAmt"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmCAPaymentPlanConfirmation.lblFeeVal.text = commaFormatted(parseFloat(resulttable["fee"]).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            frmCAPaymentPlanConfirmation.lblAnnualRateVal.text = resulttable["annualRate"] + "%";
            frmCAPaymentPlanConfirmation.lblDebitCardOnVal.text = returnDateForFT();
            frmCAPaymentPlanConfirmation.lblRecieveDateVal.text = returnDateForFT();
            frmCAPaymentPlanConfirmation.show();
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            if (isNotBlank(result["errMsg"])) {
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
            return false;
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}

function cashAdvanceCreditCardDetailsInqGetLimits() {
    showLoadingScreen();
    var inputParam = {};
    inputParam["cardId"] = gblCACardRefNumber;
    inputParam["tranCode"] = "CA";
    invokeServiceSecureAsync("creditcardDetailsInq", inputParam, callBackCashAdvanceCreditCardDetailsInqGetLimits);
}

function callBackCashAdvanceCreditCardDetailsInqGetLimits(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblMinCashAdvanceAmount = resulttable["cashAdvanceMinAmt"];
            var RemDailyLimit = resulttable["RemDailyLimit"];
            var AvailToSpend = resulttable["AvailToSpend"];
            var AvailCashAdv = parseFloat(RemDailyLimit) < parseFloat(AvailToSpend) ? RemDailyLimit : AvailToSpend;
            frmMBCashAdvanceCardInfo.lblRemDailyLimitVal.text = commaFormatted(RemDailyLimit);
            frmMBCashAdvanceCardInfo.lblAvailSpendVal.text = commaFormatted(AvailToSpend);
            frmMBCashAdvanceCardInfo.lblAvailCashAdvVal.text = commaFormatted(AvailCashAdv);
            dismissLoadingScreen();
            frmMBCashAdvanceCardInfo.show();
        } else {
            dismissLoadingScreen();
            if (isNotBlank(result["errMsg"])) {
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
            return false;
        }
    }
}

function cashAdvanceCreditCardDetailsInqGetInterest() {
    showLoadingScreen();
    var inputParam = {};
    inputParam["cardId"] = gblCACardRefNumber;
    inputParam["tranCode"] = "CA";
    inputParam["amount"] = removeCommos(frmCashAdvancePlanDetails.lblEnterAmount.text);
    invokeServiceSecureAsync("creditcardDetailsInq", inputParam, callBackCashAdvanceCreditCardDetailsInqGetInterest);
}

function callBackCashAdvanceCreditCardDetailsInqGetInterest(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            var InterestRate = resulttable["InterestRate"];
            var ActionFlag = resulttable["ActionFlag"];
            var Rate = resulttable["Rate"];
            var Amount = frmCashAdvancePlanDetails.lblEnterAmount.text;
            frmCashAdvancePlanDetails.lblAnnualRateVal.text = InterestRate + "%";
            frmCashAdvancePlanDetails.lblFeeVal.text = commaFormatted(calculateCashAdvanceFee(Amount, Rate));
            if (ActionFlag == "Y") {
                var msgPro = kony.i18n.getLocalizedString("msgPromotionRateWarning");
                msgPro = msgPro.replace("{Annual_Rate}", InterestRate);
                frmCashAdvancePlanDetails.rchTxtNote.text = msgPro;
                frmCashAdvancePlanDetails.rchTxtNote.setVisibility(true);
            } else {
                frmCashAdvancePlanDetails.rchTxtNote.text = "";
                frmCashAdvancePlanDetails.rchTxtNote.setVisibility(false);
            }
            if (kony.application.getCurrentForm().id != "frmCashAdvancePlanDetails") {
                frmCashAdvancePlanDetails.show();
                frmMBCashAdvanceTnC.destroy();
            }
            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            if (isNotBlank(result["errMsg"])) {
                showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
            return false;
        }
    }
}

function cashAdvanceServiceHoursCheck() {
    showLoadingScreen();
    var inputParam = {};
    invokeServiceSecureAsync("cashAdvanceBusinessHrs", inputParam, callBackCashAdvanceServiceHoursCheck);
}

function callBackCashAdvanceServiceHoursCheck(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            if (result["CA_BusinessHrsFlag"] == "true") {
                cashAdvanceCreditCardDetailsInqGetLimits();
            } else {
                var CA_StartTime = result["CA_StartTime"];
                var CA_EndTime = result["CA_EndTime"];
                var errorMsg = kony.i18n.getLocalizedString("Card_Err_ServiceHour");
                errorMsg = errorMsg.replace("{service_hours}", CA_StartTime + " - " + CA_EndTime);
                dismissLoadingScreen();
                showAlertWithCallBack(errorMsg, kony.i18n.getLocalizedString("info"), showAccuntSummaryScreen);
                return false;
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}