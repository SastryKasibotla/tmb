function checkBtnSkins() {
	var skinArray = [];
	var currentLocale = kony.i18n.getCurrentLocale();
	if (currentLocale == "en_US") {
		skinArray.push(btnOnFocus);
		skinArray.push(btnOffFocus);
	} else {
		skinArray.push(btnOnNormal);
		skinArray.push(btnOffNorm);
	}
	return skinArray;
}

function createMenuDynamically(eventObject) {
	var currentFormId = eventObject;
	//currentFormId.scrollboxMain.scrollToEnd();
	
	var btnThai = new kony.ui.Button({
		"id": "btnThai",
		"isVisible": true,
		"text": kony.i18n.getLocalizedString("languageThai"),
		"skin": checkBtnSkins()[1],
		//"focusSkin": checkBtnSkins()[1],
		"onClick": changeLocale
	}, {
		"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_LEFT,
		"vExpand": false,
		"hExpand": true,
		"margin": [0, 0, 0, 0],
		"padding": [1, 1, 1, 0],
		"contentAlignment": constants.CONTENT_ALIGN_CENTER,
		"displayText": true,
		"marginInPixel": false,
		"paddingInPixel": false,
		"containerWeight": 15
	}, {
		"glowEffect": false,
		"showProgressIndicator": false
	});
	var btnEng = new kony.ui.Button({
		"id": "btnEng",
		"isVisible": true,
		"text": kony.i18n.getLocalizedString("languageEng"),
		"skin": checkBtnSkins()[0],
		//"focusSkin": checkBtnSkins()[0],
		"onClick": changeLocale
	}, {
		"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
		"vExpand": false,
		"hExpand": true,
		"margin": [0, 0, 0, 0],
		"padding": [1, 1, 1, 0],
		"contentAlignment": constants.CONTENT_ALIGN_CENTER,
		"displayText": true,
		"marginInPixel": true,
		"paddingInPixel": false,
		"containerWeight": 16
	}, {
		"glowEffect": false,
		"showProgressIndicator": false
	});
	var lblEmpty = new kony.ui.Label({
		"id": "lblEmpty",
		"isVisible": true,
		"text": null,
		"skin": "lblNormal"
	}, {
		"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
		"vExpand": false,
		"hExpand": true,
		"margin": [0, 1, 0, 1],
		"padding": [3, 8, 3, 8],
		"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
		"marginInPixel": true,
		"paddingInPixel": true,
		"containerWeight": 30
	}, {
		"wrapping": constants.WIDGET_TEXT_WORD_WRAP
	});
	var gblPlatformName = gblDeviceInfo
		.name;
	if (gblPlatformName == "thinclient") {
		headerPadding = [6, 4, 3, 4];
		seglblpadding = [2, 6, 2, 6];
		seglblMargin = [1, 0, 1, 0];
		segAboutlblMargin = [4, 0, 1, 0];
		imgChevronmargin = [2, 5, 0, 0];
	} else {
		headerPadding = [12, 1, 12, 1];
		seglblpadding = [6, 10, 6, 10];
		seglblMargin = [1, 1, 1, 1];
		segAboutlblMargin = [13, 0, 1, 0];
		imgChevronmargin = [0, 5, 0, 0];
	}
	if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName))) {
		marginabtchevron = [7, 0, 0, 0];
		containerweightChevron = 38;
		containerWghtLogoutTxt = 50;
		containerweightChevronMain = 16;
		marginabtchevronMain = [0, 0, 0, 0];
	} else {
		marginabtchevron = [5, 0, 0, 0];
		containerweightChevron = 18;
		//if (kony.i18n.getCurrentLocale() == "th_TH") {
//           		 containerWghtLogoutTxt = 18;
//       		} else {
//          		 containerWghtLogoutTxt = 11;
//           }
		containerWghtLogoutTxt = 32;
		containerweightChevronMain = 16;
		marginabtchevronMain = [0, 0, 0, 0];
	}
	if (isSignedUser == true) {
		//invokeCRMProfileInqForIBStatusID();
		
	//	var gblDeviceInfo = kony.os.deviceInfo();
		if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("android", gblPlatformName))) {
			btnLogOutIconHExpand = true;
			btnLogOutIconcontainerWeight = 8;
			lblMyInboxMargin = [0, 0, 0, 0];
			lblbadgeMargin = [30, 28, 40, 0];
			segInboxArrowMargin = [0, 0, 5, 0]
		} else {
			btnLogOutIconHExpand = true;
			btnLogOutIconcontainerWeight = 6;
			if(flowSpa){
				lblMyInboxMargin = [0, 0, 0, 15];
				lblbadgeMargin = [30, 10, 50, 30];
			}else{
				lblMyInboxMargin = [0, 0, 0, 0];
				lblbadgeMargin = [37, 30, 40, 0];
			}
			
			segInboxArrowMargin = [0, 0, 5, 0]
		}
		if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName))) {
			marginabtchevron = [7, 0, 0, 0];
			containerweightChevron = 38;
			containerWghtLogoutTxt = 50;
			containerweightChevronMain = 16;
			marginabtchevronMain = [0, 0, 0, 0];
			segInboxArrowMargin = [0, 0, 7, 0]
		} else {
			marginabtchevron = [5, 0, 0, 0];
			containerweightChevron = 18;
		//	if (kony.i18n.getCurrentLocale() == "th_TH") {
//           		 containerWghtLogoutTxt = 19;
//       		} else {
//          		 containerWghtLogoutTxt = 11;
//           }
			containerWghtLogoutTxt = 32;
			containerweightChevronMain = 16;
			marginabtchevronMain = [0, 0, 0, 0];
			segInboxArrowMargin = [0, 2, 3, 0]
		}
		var btnLogOutIcon = new kony.ui.Button({
			"id": "btnLogOutIcon",
			"isVisible": true,
			"text": null,
			"skin": "btnMenuLogoutImg",
			"focusSkin": "btnMenuLogoutImg",
			"onClick": onLogoutPopUpConfrm
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
			"vExpand": false,
			"hExpand": btnLogOutIconHExpand,
			"margin": [0, 2, 0, 1],
			"padding": [0, 0, 0, 0],
			"contentAlignment": constants.CONTENT_ALIGN_CENTER,
			"displayText": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": btnLogOutIconcontainerWeight
		}, {
			"glowEffect": false,
			"showProgressIndicator": true
		});
		var btnLogOutText = new kony.ui.Button({
			"id": "btnLogOutText",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("keySPAmenuLogout"),
			"skin": "btnTranslogoutWBG",
			"focusSkin": "btnTranslogoutWBG",
			"onClick": onLogoutPopUpConfrm
	  }, {
			"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 3, 1, 1],
			"padding": [3, 12, 0, 10],
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"displayText": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": containerWghtLogoutTxt
		}, {
			"glowEffect": false,
			"showProgressIndicator": true
		});
		var hboxMenuHeader = new kony.ui.Box({
			"id": "hboxMenuHeader",
			"isVisible": true,
			"skin": "hboxHeader",
			"focusSkin": "hboxHeader",
			"position": constants.BOX_POSITION_AS_HEADER,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"containerWeight": 8,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": headerPadding,
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hboxMenuHeader.add(
			btnEng, btnThai, lblEmpty, btnLogOutIcon, btnLogOutText);
			

		var lblTransfer = new kony.ui.Label({
			"id": "lblTransfer",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("Transfer"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 0, 1, 0],
			"padding": [10, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 97
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var hbxtransfer = new kony.ui.Box({
			"id": "hbxtransfer",
			"isVisible": true,
			"skin": "hbxnewmenutransfer",
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL,
			"onClick": transferFromMenu
		}, {
			"containerWeight": 63,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxtransfer.add(
			lblTransfer);
		var lblCardLessWithDraw = new kony.ui.Label({
			"id": "lblCardLessWithDraw",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("MenuCardlessWithdrawal"),
			"skin": "lblEmptyCardless"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 0, 1, 0],
			"padding": [10, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 90
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var hbxCardless = new kony.ui.Box({
			"id": "hbxCardless",
			"isVisible": true,
			"skin": "hbxnewmenuCardlessMenu",
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL,
			"onClick": transferFromMenu
		}, {
			"containerWeight": 35,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxCardless.add(
			lblCardLessWithDraw);
		var vboxLeftMenu = new kony.ui.Box({
			"id": "vboxLeftMenu",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_VERTICAL
		}, {
			"containerWeight": 32,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		vboxLeftMenu.add(
			hbxtransfer, hbxCardless);
		var lblMyActivities = new kony.ui.Label({
			"id": "lblMyActivities",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("MyActivities"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 1, 1, 1],
			"padding": [10, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 99
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var hbxMenuMyActivities = new kony.ui.Box({
			"id": "hbxMenuMyActivities",
			"isVisible": true,
			"skin": "hbxnewmenuActivitymenu",
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL,
			"onClick": showFrmMBMyActivities
		}, {
			"containerWeight": 32,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxMenuMyActivities.add(
			lblMyActivities);
		var lblMyAccSumm = new kony.ui.Label({
			"id": "lblMyAccSumm",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("accountSummary"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 1, 1, 1],
			"padding": [2, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 98
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var hbxMenuMyAccountSummary = new kony.ui.Box({
			"id": "hbxMenuMyAccountSummary",
			"isVisible": true,
			"skin": "hbxnewmenuAcntSummary",
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL,
			"onClick": showAccountSummaryFromMenu
		}, {
			"containerWeight": 97,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxMenuMyAccountSummary.add(
			lblMyAccSumm);
		var vbxMenuMyAccountSummary = new kony.ui.Box({
			"id": "vbxMenuMyAccountSummary",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_VERTICAL
		}, {
			"containerWeight": 48,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		vbxMenuMyAccountSummary.add(
			hbxMenuMyAccountSummary);
		var lblBillPayment = new kony.ui.Label({
			"id": "lblBillPayment",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("MenuBillPayment"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 1, 1, 1],
			"padding": [10, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 97
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var hbxMenuBillPayment = new kony.ui.Box({
			"id": "hbxMenuBillPayment",
			"isVisible": true,
			"skin": "hbxnewmenuBillPay",
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL,
			"onClick": callBillPaymentFromMenu
		}, {
			"containerWeight": 47,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxMenuBillPayment.add(
			lblBillPayment);
		var lblTopUp = new kony.ui.Label({
			"id": "lblTopUp",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("TopUp"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 1, 1, 1],
			"padding": [10, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 97
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var hbxMenuTopUp = new kony.ui.Box({
			"id": "hbxMenuTopUp",
			"isVisible": true,
			"skin": "hbxnewmenuMenuTop",
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL,
			"onClick": callTopUpFromMainMenu
		}, {
			"containerWeight": 51,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxMenuTopUp.add(
			lblTopUp);
		var vbxBillTopUp = new kony.ui.Box({
			"id": "vbxBillTopUp",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_VERTICAL
		}, {
			"containerWeight": 52,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		vbxBillTopUp.add(
			hbxMenuBillPayment, hbxMenuTopUp);
		var hbxBillTopUp = new kony.ui.Box({
			"id": "hbxBillTopUp",
			"isVisible": true,
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"containerWeight": 68,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxBillTopUp.add(
			vbxMenuMyAccountSummary, vbxBillTopUp);
		var vboxRightMenu = new kony.ui.Box({
			"id": "vboxRightMenu",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_VERTICAL
		}, {
			"containerWeight": 68,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		vboxRightMenu.add(
			hbxMenuMyActivities, hbxBillTopUp);
		var hbxDown = new kony.ui.Box({
			"id": "hbxDown",
			"isVisible": true,
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"containerWeight": 67,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxDown.add(
			vboxLeftMenu, vboxRightMenu);
		var lblAbtMe = new kony.ui.Label({
			"id": "lblAbtMe",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("MenuAboutMe"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 1, 1, 1],
			"padding": [10, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 88
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var vboxAbtMe = new kony.ui.Box({
			"id": "vboxAbtMe",
			"isVisible": true,
			"skin": "vbxMenuAbtMe",
			"orientation": constants.BOX_LAYOUT_VERTICAL,
			"onClick": callOnClickAboutMenu
		}, {
			"containerWeight": 32,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		vboxAbtMe.add(
			lblAbtMe);
		var lblMyInbox = new kony.ui.Label({
			"id": "lblMyInbox",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("MenuMyInbox"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": lblMyInboxMargin,
			"padding": [10, 0, 0, 0],
			"contentAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 23
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var lblBadge = new kony.ui.Label({
			"id": "lblBadge",
			"isVisible": true,
			"text": gblMessageCount,
			"skin": "lblBadgeRed"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": lblbadgeMargin,
			"padding": [0, 0, 0, 0],
			"contentAlignment": constants.CONTENT_ALIGN_CENTER,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 32
		}, {
			"wrapping": constants.WIDGET_TEXT_CHAR_WRAP
		});
		var vbxMyinbox = new kony.ui.Box({
			"id": "vbxMyinbox",
			"isVisible": true,
			"skin": "vbxMenuInvox",
			"orientation": constants.BOX_LAYOUT_VERTICAL,
			"onClick": callOnClickMyInbox
		}, {
			"containerWeight": 33,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		vbxMyinbox.add(
			lblMyInbox, lblBadge);
		var lblConviServices = new kony.ui.Label({
			"id": "lblConviServices",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("MenuConvenientServices"),
			"skin": "lblNormalWhite"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 1, 1, 1],
			"padding": [10, 1, 1, 1],
			"contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"containerWeight": 92
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var vboxConvService = new kony.ui.Box({
			"id": "vboxConvService",
			"isVisible": true,
			"skin": "vbxConvService",
			"orientation": constants.BOX_LAYOUT_VERTICAL,
			"onClick": callOnClickService
		}, {
			"containerWeight": 35,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		vboxConvService.add(
			lblConviServices);
		var hbxup = new kony.ui.Box({
			"id": "hbxup",
			"isVisible": true,
			"position": constants.BOX_POSITION_AS_NORMAL,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"containerWeight": 23,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": false,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		hbxup.add(
			vboxAbtMe, vbxMyinbox, vboxConvService);
		var SegMainMenubox = new kony.ui.Box({
			"id": "SegMainMenubox",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
			"containerWeight": 12
		}, {});
		var SegMainMenu = new kony.ui.SegmentedUI2({
			"id": "SegMainMenu",
			"isVisible": true,
			"retainSelection": false,
			"widgetDataMap": {
				"imgMenuChevron": "imgMenuChevron",
				"lblMenuItem": "lblMenuItem",
				"imgMenuIcon": "imgMenuIcon"
			},
			"rowTemplate": SegMainMenubox,
			"widgetSkin": "segMenu",
			"rowSkin": "seg2Normal",
			"rowFocusSkin": "seg2Focus",
			"sectionHeaderSkin": "seg2Header",
			"separatorRequired": true,
			"separatorThickness": 1,
			"separatorColor": "2799D800",
			"showScrollbars": false,
			"groupCells": false,
			"screenLevelWidget": false,
			"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
			"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
			"onRowClick": onClickMainMenuSeg
		}, {
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 31
		}, {
			"indicator": constants.SEGUI_NONE,
			"enableDictionary": false,
			"showProgressIndicator": true,
			"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
			"bounces": true,
			"editStyle": constants.SEGUI_EDITING_STYLE_NONE
		});
		var imgMenuIcon = new kony.ui.Image2({
			"id": "imgMenuIcon",
			"isVisible": true,
			"imageWhenFailed": "transparent.png",
			"imageWhileDownloading": "transparent.png",
			"src": null
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"margin": [4, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"referenceWidth": null,
			"referenceHeight": null,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 10
		}, {
			"glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
		});
		// MIB-961
		/** change to use rich text below
		var lblMenuItem = new kony.ui.Label({
			"id": "lblMenuItem",
			"isVisible": true,
			"skin": "lblWhiteNormal"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": seglblMargin,
			"padding": seglblpadding,
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": 75
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});	*/
		
	    var lblMenuItem = new kony.ui.RichText({
			"id": "lblMenuItem",
			"isVisible": true,
			"skin": "rchTxtWhiteNormal"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": seglblMargin,
			"padding": seglblpadding,
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": 75
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});	
		// MIB-961
		var imgMenuChevron = new kony.ui.Image2({
			"id": "imgMenuChevron",
			"isVisible": true,
			"imageWhenFailed": "transparent.png",
			"imageWhileDownloading": "transparent.png",
			"src": null
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"margin": marginabtchevronMain,
			"padding": [0, 0, 0, 0],
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"referenceWidth": null,
			"referenceHeight": null,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": containerweightChevronMain
		}, {
			"glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
		});
		SegMainMenubox.add(imgMenuIcon, lblMenuItem, imgMenuChevron);
		// sarathi changes for About Menu
		var SegAboutMenubox = new kony.ui.Box({
			"id": "SegAboutMenubox",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
			"containerWeight": 12
		}, {});
		var SegAboutMenu = new kony.ui.SegmentedUI2({
			"id": "SegAboutMenu",
			"isVisible": true,
			"retainSelection": false,
			"widgetDataMap": {
				"imgAboutMenuChevron": "imgAboutMenuChevron",
				"lblAboutMenuItem": "lblAboutMenuItem"
			},
			"rowTemplate": SegAboutMenubox,
			"widgetSkin": "segAboutMenuMB",
			"rowSkin": "segAboutMenuMB",
			"rowFocusSkin": "segAboutMenuMB",
			"sectionHeaderSkin": "segAboutMenuMB",
			"separatorRequired": true,
			"separatorThickness": 1,
			"separatorColor": "013e6300",
			"showScrollbars": false,
			"groupCells": false,
			"screenLevelWidget": false,
			"onRowClick": onRowClickMenuSeg,
			"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
			"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW
		}, {
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 31
		}, {
			"indicator": constants.SEGUI_NONE,
			"enableDictionary": false,
			"showProgressIndicator": true,
			"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
			"bounces": true,
			"editStyle": constants.SEGUI_EDITING_STYLE_NONE
		});
		var lblAboutMenuItem = new kony.ui.Label({
			"id": "lblAboutMenuItem",
			"isVisible": true,
			"skin": "lblWhiteNormal"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": segAboutlblMargin,
			"padding": seglblpadding,
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": 72
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var imgAboutMenuChevron = new kony.ui.Image2({
			"id": "imgAboutMenuChevron",
			"isVisible": true,
			"imageWhenFailed": null,
			"imageWhileDownloading": null,
			"src": null
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"margin": marginabtchevron,
			"padding": [0, 0, 0, 0],
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"referenceWidth": null,
			"referenceHeight": null,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": containerweightChevron
		}, {
			"glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
		});
		SegAboutMenubox.add(lblAboutMenuItem, imgAboutMenuChevron);
		
		//Segment for MyInbox(Notifications, Messages)
		var SegMainInbox = new kony.ui.Box({
			"id": "SegMainInbox",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
			"containerWeight": 12
		}, {});
		
		var SegMyInbox = new kony.ui.SegmentedUI2({
			"id": "SegMyInbox",
			"isVisible": true,
			"retainSelection": false,
			"widgetDataMap": {
				"lblNotification": "lblNotification",
				"btnBadge": "btnBadge",
				"imgArrow": "imgArrow"
			},
			"rowTemplate": SegMainInbox,
			"widgetSkin": "segAboutMenuMB",
			"rowSkin": "segAboutMenuMB",
			"rowFocusSkin": "segAboutMenuMB",
			"sectionHeaderSkin": "segAboutMenuMB",
			"separatorRequired": true,
			"separatorThickness": 1,
			"separatorColor": "013e6300",
			"showScrollbars": false,
			"groupCells": false,
			"screenLevelWidget": false,
			"onRowClick": onRowClickMenuSeg,
			"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
			"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW
		}, {
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 31
		}, {
			"indicator": constants.SEGUI_NONE,
			"enableDictionary": false,
			"showProgressIndicator": true,
			"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
			"bounces": true,
			"editStyle": constants.SEGUI_EDITING_STYLE_NONE
		});
		var lblNotification = new kony.ui.Label({
			"id": "lblNotification",
			"isVisible": true,
			"skin": "lblWhiteNormal"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": segAboutlblMargin,
			"padding": seglblpadding,
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": 72
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var btnBadge = new kony.ui.Button({
			"id": "btnBadge",
			"isVisible": true,
			"skin": "",
			"focusSkin": ""
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
			"vExpand": false,
			"hExpand": false,
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"contentAlignment": constants.CONTENT_ALIGN_CENTER,
			"displayText": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": 8
		}, {
			"glowEffect": false,
			"showProgressIndicator": true
		});
		var imgArrow = new kony.ui.Image2({
			"id": "imgArrow",
			"isVisible": true,
			"imageWhenFailed": null,
			"imageWhileDownloading": null,
			"src": null
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_LEFT,
			"margin": segInboxArrowMargin,
			"padding": [0, 0, 0, 0],
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"referenceWidth": null,
			"referenceHeight": null,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": containerweightChevron
		}, {
			"glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
		});
		SegMainInbox.add(lblNotification, btnBadge, imgArrow)
		//currentFormId.vboxLeft.setVisibility(true);
		//currentFormId.scrollboxMain.scrollToBeginning();
		//currentFormId.vboxLeft.addAt(hboxMenuHeader, 0);
		//currentFormId.hboxLeft1.add(vbox47407564342877);
		//currentFormId.vbox47407564342877.add(hboxMenuHeader,hbxDown, hbxup, SegAboutMenu, SegMainMenu); // changed the name of vbox to scrollbolLeft
	if(currentFormId.id == "frmAccountSummaryLanding")
		  	{
		  	
		  	
		  	currentFormId.vbox47407564342877.add(hboxMenuHeader,hbxDown, hbxup, SegAboutMenu,SegMyInbox, SegMainMenu); //use below name for scrollbox
		  	}
		  else
		  	{
		  	
			currentFormId.scrollboxLeft.add(hboxMenuHeader,hbxDown, hbxup, SegAboutMenu,SegMyInbox, SegMainMenu);
		}
		//currentFormId.vboxLeft.addAt(hboxMenuHeader, 0);
		//currentFormId.vbox47407564342877.add(hbxDown, hbxup, SegAboutMenu, SegMyInbox, SegMainMenu);
         
         if(currentFormId.SegAboutMenu != null && currentFormId.SegAboutMenu != undefined){
             currentFormId.SegAboutMenu.setVisibility(false);
         }
		
		//currentFormId.SegMyinbox.setVisibility(false);
		//  currentFormId.segConvSer.setVisibility(false);
		var segMenuTable;
		if(flowSpa){
			segMenuTable = [{
					imgMenuIcon: "hotpromotions.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuHotPromotions"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "findatm.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "exhrates.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "apptour.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "icon_phone.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "termsconditions.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "lock.png",
					lblMenuItem:  kony.i18n.getLocalizedString("keySecurityAdvices"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "",
					lblMenuItem: "",
					imgMenuChevron: "empty.png"
				}
			]
		}else{
			if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName))) {
				segMenuTable = [{
						imgMenuIcon: "hotpromotions.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuHotPromotions"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "icon_ce_contactus.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					} 
				]
			}else{
				segMenuTable = [{
						imgMenuIcon: "hotpromotions.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuHotPromotions"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "icon_ce_contactus.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					} ,{
						imgMenuIcon: "",
						lblMenuItem: "",
						imgMenuChevron: "empty.png"
					}
				]
			}
			
		}
		currentFormId.SegMainMenu.setData(segMenuTable);
		
		//currentFormId.scrollboxMain.scrollToBeginning();
		// sarathi - about segment
	} else {
		//currentFormId.scrollboxMain.scrollToEnd();
		
		if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("android", gblPlatformName))) {
			btnLoginIconcontainerWeight = 8;
			lblMyInboxMargin = [0, 0, 0, 0];
			lblbadgeMargin = [30, 28, 40, 0];
			//segInboxArrowMargin = [0, 0, 5, 0]
		} else {
			btnLoginIconcontainerWeight = 6;
			if(flowSpa){
				lblMyInboxMargin = [0, 0, 0, 15];
				lblbadgeMargin = [30, 25, 50, 30];
			}else{
				lblMyInboxMargin = [0, 0, 0, 0];
				lblbadgeMargin = [37, 30, 40, 0];
			}
			
			//segInboxArrowMargin = [0, 0, 5, 0]
		}
		var hboxMenuHeader = new kony.ui.Box({
			"id": "hboxMenuHeader",
			"isVisible": true,
			"skin": "hboxHeader",
			"focusSkin": "hboxHeader",
			"position": constants.BOX_POSITION_AS_HEADER,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"containerWeight": 8,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [6, 8, 6, 4],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		
		var btnLoginIcon = new kony.ui.Button({
			"id": "btnLoginIcon",
			"isVisible": true,
			"text": null,
			"skin": "btnMenuLoginImg",
			"focusSkin": "btnMenuLoginImg",
			"onClick": onClickPreLoginBtn
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
			"vExpand": false,
			"hExpand": true,
			"margin": [0, 1, 0, 1],
			"padding": [0, 0, 0, 0],
			"contentAlignment": constants.CONTENT_ALIGN_CENTER,
			"displayText": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": btnLoginIconcontainerWeight
		}, {
			"glowEffect": false,
			"showProgressIndicator": true
		});
		
		var btnLoginText = new kony.ui.Button({
			"id": "btnLoginText",
			"isVisible": true,
			"text": kony.i18n.getLocalizedString("login"),
			"skin": "btnTranslogoutWBG",
			"focusSkin": "btnTranslogoutWBG",
			"onClick": onClickPreLoginBtn
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
			"vExpand": false,
			"hExpand": true,
			"margin": [1, 1, 1, 1],
			"padding": [6, 13, 0, 10],
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"displayText": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": containerWghtLogoutTxt
		}, {
			"glowEffect": false,
			"showProgressIndicator": true
		});
		hboxMenuHeader.add(btnEng, btnThai, lblEmpty, btnLoginIcon, btnLoginText);
		
		
		var SegMainMenubox = new kony.ui.Box({
			"id": "SegMainMenubox",
			"isVisible": true,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_LEFT,
			"containerWeight": 12
		}, {});
		var SegMainMenu = new kony.ui.SegmentedUI2({
			"id": "SegMainMenu",
			"isVisible": true,
			"retainSelection": false,
			"widgetDataMap": {
				"imgMenuChevron": "imgMenuChevron",
				"lblMenuItem": "lblMenuItem",
				"imgMenuIcon": "imgMenuIcon"
			},
			"rowTemplate": SegMainMenubox,
			"widgetSkin": "segMenu",
			"rowSkin": "seg2Normal",
			"rowFocusSkin": "seg2Focus",
			"sectionHeaderSkin": "seg2Header",
			"separatorRequired": true,
			"separatorThickness": 1,
			"separatorColor": "2799D800",
			"showScrollbars": false,
			"groupCells": false,
			"screenLevelWidget": false,
			"selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
			"viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
			"onRowClick": onClickMainMenuSeg
		}, {
			"margin": [0, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 31
		}, {
			"indicator": constants.SEGUI_NONE,
			"enableDictionary": false,
			"showProgressIndicator": true,
			"progressIndicatorColor": constants.PROGRESS_INDICATOR_COLOR_WHITE,
			"bounces": false,
			"editStyle": constants.SEGUI_EDITING_STYLE_NONE
		});
		var imgMenuIcon = new kony.ui.Image2({
			"id": "imgMenuIcon",
			"isVisible": true,
			"imageWhenFailed": "transparent.png",
			"imageWhileDownloading": "transparent.png",
			"src": null
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"margin": [4, 0, 0, 0],
			"padding": [0, 0, 0, 0],
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"referenceWidth": null,
			"referenceHeight": null,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": 12
		}, {
			"glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
		});
		var lblMenuItem = new kony.ui.Label({
			"id": "lblMenuItem",
			"isVisible": true,
			"skin": "lblWhiteNormal"
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"vExpand": false,
			"hExpand": true,
			"margin": seglblMargin,
			"padding": seglblpadding,
			"contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
			"marginInPixel": true,
			"paddingInPixel": true,
			"containerWeight": 72
		}, {
			"wrapping": constants.WIDGET_TEXT_WORD_WRAP
		});
		var imgMenuChevron = new kony.ui.Image2({
			"id": "imgMenuChevron",
			"isVisible": true,
			"imageWhenFailed": "transparent.png",
			"imageWhileDownloading": "transparent.png",
			"src": null
		}, {
			"widgetAlignment": constants.WIDGET_ALIGN_CENTER,
			"margin": marginabtchevronMain,
			"padding": [0, 0, 0, 0],
			"imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
			"referenceWidth": null,
			"referenceHeight": null,
			"marginInPixel": false,
			"paddingInPixel": false,
			"containerWeight": containerweightChevronMain
		}, {
			"glossyEffect": constants.IMAGE_GLOSSY_EFFECT_DEFAULT
		});
		SegMainMenubox.add(imgMenuIcon, lblMenuItem, imgMenuChevron);
		
		var lblAppVersion  = new kony.ui.Label({
        "id": "lblAppVersion",
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyVersion")+" "+ appConfig.appVersion,
        "skin": "lblGrayCopyrightCopy"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_BOTTOM_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [4, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "textCopyable": false
    });

		var lblCopyright = new kony.ui.Label({
        "id": "lblCopyright",
        "isVisible": true,
        "text": getCopyRightText(),
        "skin": "lblGrayCopyrightCopy"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_BOTTOM_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [4, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "textCopyable": false
    });
		
		var hboxMenuFooter1 = new kony.ui.Box({
			"id": "hboxMenuFooter1",
			"isVisible": true,
			"skin": "hboxHeader",
			"focusSkin": "hboxHeader",
			"position": constants.BOX_POSITION_AS_FOOTER,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"containerWeight": 8,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [6, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		
		var hboxMenuFooter2 = new kony.ui.Box({
			"id": "hboxMenuFooter2",
			"isVisible": true,
			"skin": "hboxHeader",
			"focusSkin": "hboxHeader",
			"position": constants.BOX_POSITION_AS_FOOTER,
			"orientation": constants.BOX_LAYOUT_HORIZONTAL
		}, {
			"containerWeight": 8,
			"percent": true,
			"widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
			"margin": [0, 0, 0, 0],
			"padding": [6, 0, 0, 0],
			"vExpand": false,
			"hExpand": true,
			"marginInPixel": true,
			"paddingInPixel": true,
			"layoutType": constants.CONTAINER_LAYOUT_BOX
		}, {});
		
		
		
		hboxMenuFooter1.add(lblAppVersion);
		hboxMenuFooter2.add(lblCopyright);
		//currentFormId.vboxLeft.setVisibility(true);
		//currentFormId.vboxLeft.addAt(hboxMenuHeader, 0);
        //currentFormId.hboxLeft1.add(vbox47407564342877);
		//currentFormId.vbox47407564342877.add(hboxMenuHeader,SegMainMenu);
		/*if(currentFormId == frmAccountSummaryLanding)
		  	{
		  	currentFormId.vbox47407564342877.add(hboxMenuHeader,SegMainMenu); //use below name for scrollbox
		  	}
		  else*/
		  	//{
			//}		
		if(flowSpa){
			if(currentFormId == frmAccountSummaryLanding)
		  	{
		  	currentFormId.vbox47407564342877.add(hboxMenuHeader,SegMainMenu); //use below name for scrollbox
		  	}
		  else{
		     currentFormId.scrollboxLeft.add(hboxMenuHeader,SegMainMenu);
		  }
			
		}else{
		if(currentFormId == frmAccountSummaryLanding)
		  	{
		  	currentFormId.vbox47407564342877.add(hboxMenuHeader,SegMainMenu,hboxMenuFooter1,hboxMenuFooter2); //use below name for scrollbox
		  	}
		  else{
		
			currentFormId.scrollboxLeft.add(hboxMenuHeader,SegMainMenu,hboxMenuFooter1,hboxMenuFooter2);
			}
		}
		var segMenuTable;
		if(flowSpa){
			segMenuTable = [{
					imgMenuIcon: "findatm.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "exhrates.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "apptour.png",
					lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "icon_ce_contactus.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "termsconditions.png",
					lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
					imgMenuChevron: "arrow_menu_white.png"
				},{
					imgMenuIcon: "lock.png",
					lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
					imgMenuChevron: "arrow_menu_white.png"
				}, {
					imgMenuIcon: "",
					lblMenuItem: "",
					imgMenuChevron: "empty.png"
				}
			]
		}else{
			if ((null != gblPlatformName) && ((kony.string.equalsIgnoreCase("iphone", gblPlatformName) || kony.string.equalsIgnoreCase("iPad", gblPlatformName)))) {
				segMenuTable = [{
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "icon_phone.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					}
				]
			}else{
				segMenuTable = [{
						imgMenuIcon: "findatm.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuFindTMB"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "exhrates.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuExRates"),
						imgMenuChevron: "arrow_menu_white.png"
					}, {
						imgMenuIcon: "apptour.png",
						lblMenuItem: kony.i18n.getLocalizedString("MenuTour"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "icon_phone.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyContactUs"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "termsconditions.png",
						lblMenuItem: kony.i18n.getLocalizedString("keyTermsNConditions"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "lock.png",
						lblMenuItem: kony.i18n.getLocalizedString("keySecurityAdvices"),
						imgMenuChevron: "arrow_menu_white.png"
					},{
						imgMenuIcon: "",
						lblMenuItem: "",
						imgMenuChevron: "empty.png"
					}
				]
			}
		}
		currentFormId.SegMainMenu.setData(segMenuTable);
		//currentFormId.scrollboxMain.scrollToBeginning();
	}
	currentFormId.info = {menuCreated:true};
}
/**
 * description
 * @returns {}
 */
function onClickMainMenuSeg(){
	try{
		//begin MIB-3530
		//gblFlagMenu = "";
		//end MIB-3530
		
		var currentFormId = kony.application.getCurrentForm();
		var selectedIndexVal = currentFormId.SegMainMenu.selectedIndex[1];
		
				
		if(isSignedUser == true){
			if(selectedIndexVal == 0){
				//Hot Promotionfunctionality.
				gblMyOffersDetails = false;
				onClickPromotions();
			}else if(selectedIndexVal == 1){
				//Find TMB.
				gblLatitude = "";
				gblLongitude = "";
				gblCurrenLocLat = "";
				gblCurrenLocLon = "";
				provinceID = "";
				districtID = "";
				gblProvinceData = [];
				gblDistrictData = [];
				frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
				frmATMBranch.btnCombDistrict.text =  kony.i18n.getLocalizedString("FindTMB_District");
				frmATMBranch.txtKeyword.text = "";
				onClickATM();
			}else if(selectedIndexVal == 2){
				//Exchange Rates.
				onClickExchangeRates();
				//callOnClickAboutMenu(); // call function due to DEF1405
				
			}else if(selectedIndexVal == 3){
				//App Tour.
				showAppTourForm();
				//callOnClickAboutMenu(); // call function due to DEF1405
			}
			else if (selectedIndexVal == 4) {
				conatctUsForm();
			}
			else if (selectedIndexVal == 5) {
				onClickDownloadTnC('pdf', 'TermsAndCondtionsFooter');
			}else if (selectedIndexVal == 6) {
				onClickSecurityAdvices();
				//callOnClickAboutMenu(); // call function due to DEF1405
			}
		}else if(isSignedUser == false){
			if(selectedIndexVal == 0){
				gblLatitude = "";
				gblLongitude = "";
				gblCurrenLocLat = "";
				gblCurrenLocLon = "";
				provinceID = "";
				districtID = "";
				gblProvinceData = [];
				gblDistrictData = [];
				frmATMBranch.btnCombProvince.text = kony.i18n.getLocalizedString("FindTMB_Province");
				frmATMBranch.btnCombDistrict.text =  kony.i18n.getLocalizedString("FindTMB_District");
				frmATMBranch.txtKeyword.text = "";
				onClickATM();
			}else if(selectedIndexVal == 1){
				onClickExchangeRates();
			}else if(selectedIndexVal == 2){
				//App Tour
				showAppTourForm();
			}
			else if (selectedIndexVal == 3) {
				conatctUsForm();
			}else if (selectedIndexVal == 4) {
				onClickDownloadTnC('pdf', 'TermsAndCondtionsFooter');
			}else if (selectedIndexVal == 5) {
				onClickSecurityAdvices();
			}
		}
		
	}catch(e){
		
	}
}
function conatctUsForm(){

	frmContactUsMB.show();
	
}
function callOnClickService() {
	gblFlagMenu = onClickServices()
}

function onClickServices() {
 	try{
			if(flowSpa)
			{
				var segConvSerTable = [{
				        lblAboutMenuItem: kony.i18n.getLocalizedString("kelblOpenNewAccount"),
				        imgAboutMenuChevron: "arrow_menu_white.png"
				    }, 
				    /*{
				        lblAboutMenuItem: kony.i18n.getLocalizedString("keylblbeep&Bill"),
				        imgAboutMenuChevron: "arrow_menu_white.png"
				    }, {
				        lblAboutMenuItem: kony.i18n.getLocalizedString("keylblsendToSave"),
				        imgAboutMenuChevron: "arrow_menu_white.png"
				    },*/ 
				    {
				        lblAboutMenuItem: kony.i18n.getLocalizedString("keyMBankinglblMobileBanking"),
				        imgAboutMenuChevron: "arrow_menu_white.png"
				    }]
			}
			else
			{
			      var segConvSerTable = [{
					lblAboutMenuItem: kony.i18n.getLocalizedString("MIB_AnyIDMenu"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				},{
					lblAboutMenuItem: kony.i18n.getLocalizedString("kelblOpenNewAccount"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, 
				//{
//					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblbeep&Bill"),
//					imgAboutMenuChevron: "arrow_menu_white.png"
//				}, {
//					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblsendToSave"),
//					imgAboutMenuChevron: "arrow_menu_white.png"
//				},
				 {
					lblAboutMenuItem: kony.i18n.getLocalizedString("InternetBanking"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				
				},{
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyEStmt"),
					imgAboutMenuChevron: "arrow_menu_white.png"
					}]
			}
	
			var currentFormId = kony.application.getCurrentForm();
			currentFormId.hbxtransfer.skin = "hbxnewmenutransferDis";
			currentFormId.hbxCardless.skin = "hbxnewmenuCardlessMenuDis";
			// changes done for golive 1- removed Cardless Withdrawal
			currentFormId.lblCardLessWithDraw.skin = "lblEmptyCardlessDisabled";
			currentFormId.hbxMenuMyActivities.skin = "hbxnewmenuActivitymenuDis";
			currentFormId.hbxMenuMyAccountSummary.skin = "hbxnewmenuAcntSummaryDis";
			currentFormId.hbxMenuBillPayment.skin = "hbxnewmenuBillPayDis";
			currentFormId.hbxMenuTopUp.skin = "hbxnewmenuMenuTopDis";
			currentFormId.vbxMyinbox.skin = "vbxMenuInvoxDis";
			currentFormId.vboxAbtMe.skin = "vbxMenuAbtMeDis";
			currentFormId.vboxConvService.skin = "vbxConvServiceFocus";
			currentFormId.lblBadge.skin = "lblbadgeTrasparent";
			currentFormId.SegAboutMenu.setData(segConvSerTable);
			currentFormId.SegAboutMenu.setVisibility(true);
		  	if(!flowSpa) {
		  		if(gblShowAnyIDRegistration=="false"){
						currentFormId.SegAboutMenu.removeAt(0);
				}
		 	    if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06'){
		 	    	if(gblShowAnyIDRegistration=="true")
		 	    	{
			    		currentFormId.SegAboutMenu.removeAt(2);
			    	}else{
			    		currentFormId.SegAboutMenu.removeAt(1);
			    	}	
			    }
		 	   if(!isShowEStatementMenu()){
			    	if (gblIBFlowStatus == '02' || gblIBFlowStatus == '04' || gblIBFlowStatus == '05' || gblIBFlowStatus == '06'){
						if(gblShowAnyIDRegistration=="true"){
			    			currentFormId.SegAboutMenu.removeAt(2);
			    		}	
			    		else{
			    			currentFormId.SegAboutMenu.removeAt(1);
			    		}
			    	}else{
			    		if(gblShowAnyIDRegistration=="true"){
			    			currentFormId.SegAboutMenu.removeAt(3);
			    		}	
			    		else{
			    			currentFormId.SegAboutMenu.removeAt(2);
			    		}
			    	}		
				}
			 }
			 else{
						 
						//Removed as part of ENH_207_7
			   }
			//currentFormId.SegMyinbox.setVisibility(false);
			//     currentFormId.segConvSer.setVisibility(true);
			currentFormId.SegMyInbox.setVisibility(false);
			}
	catch(e)
	    {
	        
	    }
	return "Services";
}
/*
function onClickForInnerBoxes() {
    
    
    if (isMenuShown == true) {
        isMenuShown = false;
        var currentFormID = kony.application.getCurrentForm();
        removeGestureForAccntSummary(currentFormID);
        currentFormID.scrollboxMain.scrollToEnd();
    }
}
*/

function onClickMenuBtnInHdr() {
	/*if (isMenuRendered == false) {
		
		createMenuDynamically(calledForm);
		
		isMenuRendered = true;
		isMenuShown = true;
		settingGestureForAccntSummary(calledForm);
	}
	else if (isMenuRendered == true && isMenuShown == true) {
		
		isMenuShown = false;
		calledForm.scrollboxMain.scrollToEnd();
		removeGestureForAccntSummary(calledForm);
	}
	else if (isMenuRendered == true && isMenuShown == false) {
		
		isMenuShown = true;
		//setMenuSegmentToFalse();
		calledForm.scrollboxMain.scrollToBeginning();
		settingGestureForAccntSummary(calledForm)
		
		
	}*/
	var calledForm = kony.application.getCurrentForm();
	if (isMenuShown == false) {
		if (!calledForm.hboxMenuHeader) {
			
			//alert("creating menu");
			createMenuDynamically(calledForm);
		}
		//calledForm.scrollboxMain.scrollToBeginning();
		menuLocaleChange(calledForm);
		settingGestureForAccntSummary(calledForm);
		isMenuShown = true;
	} else {
		calledForm.scrollboxMain.scrollToEnd();
		removeGestureForAccntSummary(calledForm);
		isMenuShown = false;
	}
}
/**
 * description
 * @returns {}
 */

function onClickMyProfileFlow() {
	isMenuShown = false;
	if (kony.application.getCurrentForm().id != "frmMyProfile") {
		showLoadingScreen();
		viewprofileServiceCall();
		//frmMyProfile.show();
		//currentForm.destroy();
	} else {
		frmMyProfile.scrollboxMain.scrollToEnd();
	}
}

function callOnClickAboutMenu() {
	gblFlagMenu = onClickAboutMenu();
}

function onClickAboutMenu() {
  	try{
		  if(flowSpa){
		  var segAboutMenuTable = [{
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyProfile"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMyAccount"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyRecipients"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMybills"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("myTopUpsMB"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
				            lblAboutMenuItem: kony.i18n.getLocalizedString("ActivateTokenIB"),
				            imgAboutMenuChevron: "arrow_menu_white.png"
				}]
		  }else if(gblTouchDevice == true) {
		  		
				var segAboutMenuTable = [{
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyProfile"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMyAccount"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyRecipients"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMybills"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("myTopUpsMB"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				},  {
					lblAboutMenuItem: kony.i18n.getLocalizedString("SQB_Title"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				},  {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyTouchIDMenu"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("UnlockIBanking"),
					imgAboutMenuChevron: "arrow_menu_white.png"	
				}]
			}else {
				var segAboutMenuTable = [{
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyProfile"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMyAccount"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keyMyRecipients"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblMybills"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("myTopUpsMB"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("SQB_Title"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("keylblUseFormultipleDevice"),
					imgAboutMenuChevron: "arrow_menu_white.png"
				}, {
					lblAboutMenuItem: kony.i18n.getLocalizedString("UnlockIBanking"),
					imgAboutMenuChevron: "arrow_menu_white.png"	
				}]
			}
	
			var currentFormId = kony.application.getCurrentForm();
			currentFormId.hbxtransfer.skin = "hbxnewmenutransferDis";
			currentFormId.hbxCardless.skin = "hbxnewmenuCardlessMenuDis";
			// changes done for golive 1- removed Cardless Withdrawal
			currentFormId.lblCardLessWithDraw.skin = "lblEmptyCardlessDisabled";
			currentFormId.hbxMenuMyActivities.skin = "hbxnewmenuActivitymenuDis";
			currentFormId.hbxMenuMyAccountSummary.skin = "hbxnewmenuAcntSummaryDis";
			currentFormId.hbxMenuBillPayment.skin = "hbxnewmenuBillPayDis";
			currentFormId.hbxMenuTopUp.skin = "hbxnewmenuMenuTopDis";
			currentFormId.vbxMyinbox.skin = "vbxMenuInvoxDis";
			currentFormId.vboxAbtMe.skin = "vbxMenuAbtMefocus";
			currentFormId.vboxConvService.skin = "vbxConvServiceDis";
			currentFormId.lblBadge.skin = "lblbadgeTrasparent";
			currentFormId.SegAboutMenu.setData(segAboutMenuTable);
			currentFormId.SegAboutMenu.setVisibility(true);
			currentFormId.SegMyInbox.setVisibility(false);
			// currentFormId.SegMyinbox.setVisibility(false);
			// currentFormId.segConvSer.setVisibility(false);
			// currentFormId.SegAboutMenu.setData(segConvSerTable);
			if(!flowSpa) {
				if (gblIBFlowStatus == '00' || gblIBFlowStatus == '01' || gblIBFlowStatus == '08' || gblIBFlowStatus == '07'){
					if(gblTouchDevice == true)
					{
							currentFormId.SegAboutMenu.removeAt(8);
					}
					else
					{
							currentFormId.SegAboutMenu.removeAt(7);
					}	
				}
				if(gblQuickBalanceEnable != "ON"){
					currentFormId.SegAboutMenu.removeAt(5);
				}
			}
			else {
				if(!gblTokenActivationFlag){
					currentFormId.SegAboutMenu.removeAt(5);
				}
				//Removed as part of ENH_207_7
		  }
		}
		catch(e)
        {
          
        }
	return "AboutMenu";
}

function onRowClickMenuSeg() {
	if(flowSpa){
		onClickAboutMeSegmentSPA(gblFlagMenu);
	}else{
		onClickAboutMeSegment(gblFlagMenu);
	}
}

function onClickAboutMeSegment(gblFlagMenu) {
	var currentFormId = kony.application.getCurrentForm();
	
	var selectedAboutIndex = 0;
	if(gblFlagMenu == "MyInbox"){
		selectedAboutIndex = currentFormId.SegMyInbox.selectedIndex[1];
	}else{
		selectedAboutIndex = currentFormId.SegAboutMenu.selectedIndex[1];
	}
	destroyMenuSpa();
	if (gblFlagMenu == "AboutMenu") {
		if (selectedAboutIndex == 0) {
			onClickMyProfileFlow()
		}
		if (selectedAboutIndex == 1) {
			onClickMyAccountsFlow()
		}
		if (selectedAboutIndex == 2) {
			onClickMyRecipientsFlow()
		}
		if (selectedAboutIndex == 3) {
			onClickMyBillerFlow()
		}
		if (selectedAboutIndex == 4) {
			onClickMyTopUpFlow()
		}
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("SQB_Title")) {
			onClickQuickBalanceMenu()
		}
		
		if (gblTouchDevice==true && currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("keyTouchIDMenu")) {
		  if(gblUserLockStatusMB == "03"){
		    //alert("Transation Password Locked")
            showTranPwdLockedPopup();
		  }else{
		     onClickTouchIdSettings();  
		  }
			
		}
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("keylblUseFormultipleDevice")) {
			//ActionCode is 23 for add device
			gblApplyServices = 3;
			if (checkMBUserStatus()){
				applyServicesCommon("23", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
			}
		}
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("UnlockIBanking")) {
		   //ActionCode is 12 for reset
	          gblApplyServices = 4;
	         if (checkMBUserStatus()){
	 
				applyServicesCommon("12", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
	 		 }	
		 }
	} else if (gblFlagMenu == "Services") {
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("MIB_AnyIDMenu")) {
	          getAnyIDBriefImage();
	          onClickSetUserID();
 		}
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("kelblOpenNewAccount")) {
		if(checkMBUserStatus()){
			//setting isCmpFlow to false as it is not internal link of campaign to product brief screnn MIB-971
			isCmpFlow = false;
			//  alert("This functionality will be implemented in codedrop 5");
			OnClickOpenNewAccount();
		}	
		}
		/*if (selectedAboutIndex == 1) {
		if(checkMBUserStatus()){
		
			checkUserStatusBBMB();
			}
			//onSelectBBSubMenuMB();
		}
		if (selectedAboutIndex == 2) {
		  if(checkMBUserStatus()){
			//Send To Save - checking whether s2s already applied or not
			getApplyS2SStatus();
			}
		}*/
		
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("InternetBanking")) {
				gblApplyServices = 2;
				//ActionCode is 11 for ApplyIB
	          if (checkMBUserStatus()){
	 				applyServicesCommon("11", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
			   }
 		}
 		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("keyEStmt")) {
		if (checkMBUserStatus()){
					checkpointMBestmtBousinessHrs();
				//onClickEStatementLink();
			}
		}
	} else if (gblFlagMenu == "MyInbox") {
		if (selectedAboutIndex == 0) {
			onClickNotifications();
		}
		if (selectedAboutIndex == 1) {
			onClickMessages();
		}
	}

}



function onClickAboutMeSegmentSPA(gblFlagMenu) {
	var currentFormId = kony.application.getCurrentForm();
	
	var selectedAboutIndex;
	
	if(gblFlagMenu == "MyInbox"){
		selectedAboutIndex = currentFormId.SegMyInbox.selectedIndex[1];
	}else{
		selectedAboutIndex = currentFormId.SegAboutMenu.selectedIndex[1];
	}
	
	//var selectedAboutIndex = currentFormId.SegAboutMenu.selectedIndex[1];
	
 	destroyMenuSpa();
	if (gblFlagMenu == "AboutMenu") {
		if (selectedAboutIndex == 0) {
			onClickMyProfileFlow()
		}
		if (selectedAboutIndex == 1) {
			onClickMyAccountsFlow()
		}
	
		if (selectedAboutIndex == 2) {
			onClickMyRecipientsFlow()
		}
		if (selectedAboutIndex == 3) {
			onClickMyBillerFlow()
		}
		if (selectedAboutIndex == 4) {
			onClickMyTopUpFlow()
		}
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("keylblUseFormultipleDevice")) {
			//ActionCode is 23 for add device
			gblApplyServices = 3;
		if (checkMBUserStatus()){
			applyServicesCommon("23", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
		                      }
		}
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("keyUnlockAppSpa")) {
		   //ActionCode is 12 for reset
            gblApplyServices = 4;
         if (checkMBUserStatus()){
			if(flowSpa)
			{
			applyServicesCommon("22", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
			}
			else
			{
			applyServicesCommon("12", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
			}
			                    }	
		   }
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("ActivateTokenIB")) {
			if(checkMBUserStatus()){
			frmSpaTokenactivationstartup.show();
			frmSpaTokenactivationstartup.tbxTokenSerialNo.setFocus(true);
			}
		}
	} else if (gblFlagMenu == "Services") {
				if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("kelblOpenNewAccount")) {
		if(checkMBUserStatus()){
			//  alert("This functionality will be implemented in codedrop 5");
			OnClickOpenNewAccount();
		}	
		}
		/*if (selectedAboutIndex == 1) {
		if(checkMBUserStatus()){
		
			checkUserStatusBBMB();
			}
			//onSelectBBSubMenuMB();
		}
		if (selectedAboutIndex == 2) {
		if(checkMBUserStatus()){
			//Send To Save - checking whether s2s already applied or not
			getApplyS2SStatus();
			}
		}*/
		
		//curr_form.segMenuOptions.data[curr_form.segMenuOptions.selectedIndex[1]].lblSegData.text
		//data[currentFormId.SegAboutMenu.selectedIndex[1]]
		if (currentFormId.SegAboutMenu.data[currentFormId.SegAboutMenu.selectedIndex[1]].lblAboutMenuItem == kony.i18n.getLocalizedString("keyMBankinglblMobileBanking")) {
			gblApplyServices = 2;
			//ActionCode is 11 for ApplyIB
          if (checkMBUserStatus()){
			if(flowSpa)
			{
			//applyServicesCommon("21", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus)
			//Removed as part of ENH_207_7
			getTMBTouch();
			}
			else
			{
			applyServicesCommon("11", gblIBFlowStatus, gblMBFlowStatus, gblCustomerStatus);
			}
		 }
		 if (selectedAboutIndex == 2) {
		if (checkMBUserStatus()){
					checkpointMBestmtBousinessHrs();
				//onClickEStatementLink();
			}
		}
		}
		
	} else if (gblFlagMenu == "MyInbox") {
		if (selectedAboutIndex == 0) {
			onClickNotifications();
		}
		if (selectedAboutIndex == 1) {
			onClickMessages();
		}
	}

}

function onClickNotifications() {
	if (kony.application.getCurrentForm()
		.id != "frmNotificationHome") {
		var currentForm = kony.application.getCurrentForm();
		//TMBUtil.DestroyForm(frmMyRecipients);
		locale = kony.i18n.getCurrentLocale();
		frmNotificationHome.show();
		//currentForm.destroy();
	} else {
		frmNotificationHome.scrollboxMain.scrollToEnd();
	}
}

function onClickMessages() {
	if (kony.application.getCurrentForm()
		.id != "frmInboxHome") {
		var currentForm = kony.application.getCurrentForm();
		//TMBUtil.DestroyForm(frmMyRecipients);
		locale = kony.i18n.getCurrentLocale();
		gblInboxSortedOrNormal = false;
		SortByType = "Date/Time";
		gblRetryCountRequestOTP = "0";
		frmInboxHome.show();
		//currentForm.destroy();
	} else {
		frmInboxHome.scrollboxMain.scrollToEnd();
	}
}

function onClickMyRecipientsFlow() {
	recipientAddFromTransfer=false;
	gblTransferFromRecipient=false;
	if (kony.application.getCurrentForm()
		.id != "frmMyRecipients") {
		var currentForm = kony.application.getCurrentForm();
		//TMBUtil.DestroyForm(frmMyRecipients);
		gblRetryCountRequestOTP = "0";
		frmMyRecipients.show();
		//currentForm.destroy();
	} else {
		frmMyRecipients.scrollboxMain.scrollToEnd();
	}
}

function onClickMyAccountsFlow() {
	
	isMenuShown = false;
	if (kony.application.getCurrentForm().id != "frmMyAccountList") {
		var currentForm = kony.application.getCurrentForm();
		//TMBUtil.DestroyForm(frmMyAccountList);
		gblRetryCountRequestOTP = "0";
		frmMyAccountList.show();
		//currentForm.destroy();
	} else {
		frmMyAccountList.scrollboxMain.scrollToEnd();
	}
}

function callOnClickMyInbox() {
	gblFlagMenu = onClickMyInbox();
}

function onClickMyInbox() {
  	 try{
			gblNotificationData = [];
			gblMessageData = [];
			//changes done for MyInbox badge requirement
			if(gblMyInboxTotalCountMB <= 9){
				gblNotificationData = {"text":gblUnreadCount, "skin":"btnBadgeSmall"};
				gblMessageData = {"text":gblMessageCount, "skin":"btnBadgeSmall"};
			}else{
				gblNotificationData = {"text":gblUnreadCount, "skin":"btnBadgeLarge"};
				gblMessageData = {"text":gblMessageCount, "skin":"btnBadgeLarge"};
			}
			var SegMyinboxTable = [{
				lblNotification: kony.i18n.getLocalizedString("keylblNotification"),
				btnBadge: gblNotificationData,
				imgArrow: "arrow_menu_white.png"
			}, {
				lblNotification: kony.i18n.getLocalizedString("keylblmessage"),
				btnBadge: gblMessageData,
				imgArrow: "arrow_menu_white.png"
			}]
		
			var currentFormId = kony.application.getCurrentForm();
			
			currentFormId.hbxtransfer.skin = "hbxnewmenutransferDis";
			currentFormId.hbxCardless.skin = "hbxnewmenuCardlessMenuDis";
			// changes done for golive 1- removed Cardless Withdrawal
			currentFormId.lblCardLessWithDraw.skin = "lblEmptyCardlessDisabled";
			currentFormId.hbxMenuMyActivities.skin = "hbxnewmenuActivitymenuDis";
			currentFormId.hbxMenuMyAccountSummary.skin = "hbxnewmenuAcntSummaryDis";
			currentFormId.hbxMenuBillPayment.skin = "hbxnewmenuBillPayDis";
			currentFormId.hbxMenuTopUp.skin = "hbxnewmenuMenuTopDis";
			currentFormId.vbxMyinbox.skin = "vbxMenuInvoxFocus";
			currentFormId.vboxAbtMe.skin = "vbxMenuAbtMeDis";
			currentFormId.vboxConvService.skin = "vbxConvServiceDis";
			currentFormId.lblBadge.skin = "lblBadgeRed";
			currentFormId.SegAboutMenu.setVisibility(false);
			
			currentFormId.SegMyInbox.setVisibility(true);
			currentFormId.SegMyInbox.setData(SegMyinboxTable);
	}
	catch(e) 
	{
	  
	}		
	return "MyInbox";
	
}

//function setMenuSegmentToFalse() {
//	var currentFormId = kony.application.getCurrentForm();
//	currentFormId.hbxtransfer.skin = "hbxnewmenutransfer";
//	currentFormId.hbxCardless.skin = "hbxnewmenuCardlessMenu";
//	currentFormId.hbxMenuMyActivities.skin = "hbxnewmenuActivitymenu";
//	currentFormId.hbxMenuMyAccountSummary.skin = "hbxnewmenuAcntSummary";
//	currentFormId.hbxMenuBillPayment.skin = "hbxnewmenuBillPay";
//	currentFormId.hbxMenuTopUp.skin = "hbxnewmenuMenuTop";
//	currentFormId.vbxMyinbox.skin = "vbxMenuInvox";
//	currentFormId.vboxAbtMe.skin = "vbxMenuAbtMe";
//	currentFormId.vboxConvService.skin = "vbxConvService";
//	currentFormId.lblBadge.skin = "lblBadgeRed";
//	currentFormId.SegAboutMenu.setVisibility(false);
//	//    currentFormId.segConvSer.setVisibility(false);
//	// currentFormId.SegMyinbox.setVisibility(false);
//}

function onClickMyBillerFlow() {
	gblTopUpMore = 0;
	isMenuShown = false;
	isfirstCallToMaster = true;
	if (kony.application.getCurrentForm()
		.id != "frmMyTopUpList") {
		//var currentForm = kony.application.getCurrentForm();
		gblMyBillerTopUpBB = 0;
		//TMBUtil.DestroyForm(frmMyTopUpList);
		frmMyTopUpList.show();
		//currentForm.destroy();
	} else {
		gblRetryCountRequestOTP = "0";
		if (gblMyBillerTopUpBB == 0) {
			frmMyTopUpList.scrollboxMain.scrollToEnd();
			isMenuShown = false;
		} else {
			gblMyBillerTopUpBB = 0;
			frmMyTopUpList.show();
			/*
			frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyMyBillsMB')
			frmMyTopUpList.scrollboxMain.scrollToEnd();
			isMenuShown = false;
			frmMyTopUpList.segBillersList.data = [];
			//myBillerPostshow();
			*/
		}
	}
}

function onClickMyTopUpFlow() {
	gblTopUpMore = 0;
	isMenuShown = false;
	isfirstCallToMaster = true;
	if (kony.application.getCurrentForm()
		.id != "frmMyTopUpList") {
		//var currentForm = kony.application.getCurrentForm();
		gblMyBillerTopUpBB = 1;
		//TMBUtil.DestroyForm(frmMyTopUpList);
		destroyMenuSpa();
		frmMyTopUpList.show();
		frmMyTopUpList.scrollboxMain.scrollToEnd();
		//currentForm.destroy();
	} else {
		gblRetryCountRequestOTP = "0";
		if (gblMyBillerTopUpBB == 1) {
			frmMyTopUpList.scrollboxMain.scrollToEnd();
			isMenuShown = false;
		} else {
			gblMyBillerTopUpBB = 1;
			destroyMenuSpa();
			frmMyTopUpList.show();
			frmMyTopUpList.scrollboxMain.scrollToEnd();
			/*
			frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('myTopUpsMB')
			frmMyTopUpList.scrollboxMain.scrollToEnd();
			isMenuShown = false;
			frmMyTopUpList.segBillersList.data = [];
			myTopupListPostshow();
			*/
		}
	}
	
}
/* 
Checking if the user is locked or not and then navigating to open account flow
*/

function OnClickOpenNewAccount() {
	isMenuShown = false;
	gblSelProduct = "TMBSavingcare";
	gblCCDBCardFlow = "";
	checkRiskDataMB();
}

function onClickSetUserID()
{
	if (checkMBUserStatus()){
					frmMBAnyIdRegTnC.show();
			   }
}
