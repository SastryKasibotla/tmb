function MBankingMenuDesign_preshow()
{
	frmMBankingPreShow.call(this);
    isMenuShown = false;
    frmMBanking.scrollboxMain.scrollToEnd();
    DisableFadingEdges.call(this, frmMBanking);

}

function MBankingMenuDesign_postshow()
{
	if (MBTnC) {
        frmMBTnC.destroy();
    }

}

function ATMBranchPreshowMenuDesign()
{
	if(gblCallPrePost)
	{
		frmATMBranch.scrollboxMain.scrollToEnd();
	    frmATMBranchPreShow.call(this);
	    DisableFadingEdges.call(this, frmATMBranch);
	}
}

function frmATMBranchMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmATMBranchesDetailsReDesign()
{
	if(gblCallPrePost)
	{
		frmATMBranchesDetails.scrollboxMain.scrollToEnd();
    	frmATMBranchesDetailsPreShow.call(this);
    	DisableFadingEdges.call(this, frmATMBranchesDetails);
    }	
}


function frmATMBranchesDetailsMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmATMBranchListReDesign()
{
	if(gblCallPrePost)
	{
		frmATMBranchList.scrollboxMain.scrollToEnd();
    	frmATMBranchListPreShow.call(this);
    	DisableFadingEdges.call(this, frmATMBranchList);
	}
}

function frmATMBranchListMenuPostshow() {
	assignGlobalForMenuPostshow();
}


// Pre Login ContactUS module
function frmContactUSMBPreshowMenu()
{
	if(gblCallPrePost)
	{
		isMenuShown = false;
	    frmContactUsMB.scrollboxMain.scrollToEnd();
	    frmContactUsMBPreShow.call(this);
	    DisableFadingEdges.call(this, frmContactUsMB);
	    if (isSignedUser == true) {
	        frmContactUsMB.hbxPostLogin.setVisibility(true);
	        frmContactUsMB.hboxContactSend.setVisibility(true);
	        frmContactUsMB.hbxContactMe.setVisibility(false);
	        frmContactUsMB.hbxRadioButton.setVisibility(false);
	        frmContactUsMB.cmbOption.selectedKey = 1;
	        frmContactUsMB.hbxContact.setVisibility(false);
	        frmContactUsMB.line47592361418663.setVisibility(false);
	        frmContactUsMB.line47592361418559.setVisibility(false);
	        frmContactUsMB.hbxFAQ.setVisibility(false);
	        frmContactUsMB.hbxFB.setVisibility(false);
	        frmContactUsMB.hbxFindTMB.setVisibility(false);
	        frmContactUsMB.hbxFeedback.setVisibility(false);
	        frmContactUsMB.hboxFeedBackSend.setVisibility(false);
	        //frmContactUsMB.tbxCNTBox.text ="";
	        var temp = [];
	        temp.push(["1", kony.i18n.getLocalizedString("cnt_topic")]);
	        temp.push(["2", kony.i18n.getLocalizedString("Cnt_IB")]);
	        temp.push(["3", kony.i18n.getLocalizedString("Cnt_DA")]);
	        temp.push(["4", kony.i18n.getLocalizedString("Cnt_LP")]);
	        temp.push(["5", kony.i18n.getLocalizedString("Cnt_Compliants")]);
	        frmContactUsMB.cmbOption.masterData = temp;
	    } else {
	        frmContactUsMB.hbxPostLogin.setVisibility(false);
	        frmContactUsMB.hboxContactSend.setVisibility(false);
	        frmContactUsMB.hbxContact.setVisibility(true);
	        frmContactUsMB.line47592361418663.setVisibility(true);
	        frmContactUsMB.line47592361418559.setVisibility(true);
	        frmContactUsMB.hbxFAQ.setVisibility(true);
	        frmContactUsMB.hbxFB.setVisibility(false);
	        frmContactUsMB.hbxFindTMB.setVisibility(false);
	        frmContactUsMB.hbxFeedback.setVisibility(false);
	        frmContactUsMB.hboxFeedBackSend.setVisibility(false);
	    }
	 }
}

function frmContactUsMBMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmAppTourMenuPreshow() {
	if(gblCallPrePost)
	{
		frmAppTour_PreShow.call(this);
    	DisableFadingEdges.call(this, frmAppTour);
	}
}

function frmAppTourMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMBTnCMenuPreshow() {
	if(gblCallPrePost)
	{
        isMenuShown = false;
	    frmMBTnC.scrollboxMain.scrollToEnd();
	    frmMBTnC.hboxSaveCamEmail.isVisible = false;
	    frmMBTnC.imgHeaderMiddle.src = "arrowtop.png"
	    frmMBTnC.imgHeaderRight.src = "empty.png"
	    frmMBTnC.btnRight.skin = "btnShare";
        frmMBTnC.lblHdrTxt.containerWeight = 83;
        frmMBTnC.lblHdrTxt.padding = [15, 0, 0, 0];
	    DisableFadingEdges.call(this, frmMBTnC);
	}
}

function frmMBTnCMenuPostshow() {
	if(gblCallPrePost)
	{
		MBTnC = true;
	}
	assignGlobalForMenuPostshow();
}

