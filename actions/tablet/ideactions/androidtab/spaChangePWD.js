function validatePasswordProfileSpa() {
    if (frmCMChgPwdSPA.tbxTranscCrntPwd.text == null || frmCMChgPwdSPA.tbxTranscCrntPwd.text == "" || frmCMChgPwdSPA.txtConfirmPassword.text == null || frmCMChgPwdSPA.txtConfirmPassword.text == "" || frmCMChgPwdSPA.txtTransPass.text == null || frmCMChgPwdSPA.txtTransPass.text == "") {
        alert(kony.i18n.getLocalizedString("keyPasswordRequired"));
        return;
    }
    var spaChngPwd = frmCMChgPwdSPA.txtTransPass.text
    if (spaChngPwd.length > 7 && spaChngPwd.match(/[a-z]/gi) != null && spaChngPwd.match(/[0-9]/g) != null) {
        if (kony.string.containsChars(spaChngPwd, ["<"]) && kony.string.containsChars(spaChngPwd, [">"])) {
            alert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"));
            return;
        }
        if (frmCMChgPwdSPA.tbxTranscCrntPwd.text == frmCMChgPwdSPA.txtTransPass.text || frmCMChgPwdSPA.tbxTranscCrntPwd.text == frmCMChgPwdSPA.txtConfirmPassword.text) {
            alert(kony.i18n.getLocalizedString("KeyIncorrectPWD"));
            return;
        }
        if (frmCMChgPwdSPA.txtTransPass.text == kony.string.trim(gblUserName)) {
            alert("Password cannot be same as User ID");
            return;
        }
        if (frmCMChgPwdSPA.txtTransPass.text == frmCMChgPwdSPA.txtConfirmPassword.text) {
            //invokeSaveparamInSessionForPwdUserChangeSPA(frmCMChgPwdSPA.tbxTranscCrntPwd.text,frmCMChgPwdSPA.txtTransPass.text,"2");
            checkMyProfileChangePwdTokenSPA();
            //requestOTPForChngPwdIdspa();
        } else {
            alert(kony.i18n.getLocalizedString("KeyIncorrectPWD"));
        }
    } else {
        alert("Invalid password format");
        return;
    }
}

function invokeSaveparamInSessionForPwdUserChangeSPA(param1, param2, param3) {
    var inputParam = [];
    inputParam["param1"] = param1;
    inputParam["param2"] = param2;
    inputParam["param3"] = param3;
    invokeServiceSecureAsync("SaveParamsForChangePwdUserId", inputParam, callBackSaveparamInSessionPwdUserChangeSPA);
}

function callBackSaveparamInSessionPwdUserChangeSPA(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            requestOTPForChngPwdIdspa();
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
        }
    }
}

function checkMyProfileChangePwdTokenSPA() {
    var inputParam = [];
    showLoadingScreen();
    invokeServiceSecureAsync("tokenSwitching", inputParam, checkMyProfilePwdTokenSPACallbackfunction);
}

function checkMyProfilePwdTokenSPACallbackfunction(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            invokeSaveparamInSessionForPwdUserChangeSPA(frmCMChgPwdSPA.tbxTranscCrntPwd.text, frmCMChgPwdSPA.txtTransPass.text, "2");
        } else {
            dismissLoadingScreen();
            alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
        }
    }
}
/**************************************************************************************
		Module	: requestOTPForChngPwdIdspa
		Author  : Kony
		Date    : July 21, 2013
		Purpose : OTP while changePassword
*****************************************************************************************/
function requestOTPForChngPwdIdspa() {
    if (gblIBFlowStatus == "04") {
        //showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
        popTransferConfirmOTPLock.show();
        return false;
    } else {
        var inputParams = {}
        spaChnage = "chgpwdidib"
        gblOTPFlag = true;
        gblOnClickReq = false;
        try {
            kony.timer.cancel("otpTimer")
        } catch (e) {}
        //iput params for SPA OTP
        gblSpaChannel = "ChangeUserPassword";
        /*
        if (kony.i18n.getCurrentLocale() == "en_US") {
        		SpaEventNotificationPolicy = "MIB_ChangeIBPWD_EN";
        		SpaSMSSubject = "MIB_ChangeIBPWD_EN";
        	} else {
        		SpaEventNotificationPolicy = "MIB_ChangeIBPWD_TH";
        		SpaSMSSubject = "MIB_ChangeIBPWD_TH";
        }*/
        onClickOTPRequestSpa();
    }
}
/**************************************************************************************
		Module	: requestOTPForChngPwdIdspa
		Author  : Kony
		Date    : July 21, 2013
		Purpose : OTP while changePassword
*****************************************************************************************/
function callBackVerifyChngPWD() {
    popupTractPwd.dismiss();
    //
    var inputParam = {}
    inputParam["loginModuleId"] = "IB_Pwd";
    inputParam["oldPassword"] = frmCMChgPwdSPA.tbxTranscCrntPwd.text;
    inputParam["newPassword"] = frmCMChgPwdSPA.txtConfirmPassword.text;
    // showLoadingScreen();
    invokeServiceSecureAsync("changePassword", inputParam, callbackChangePasswordspa);
}

function callbackChangePasswordspa(status, resultable) {
    if (status == 400) {
        if (resultable["opstatus"] == 0) {
            if (resultable["loginModuleId"] != null) {
                //dismissLoadingScreenPopup();
                completeicon = true;
                frmMyProfile.show();
                activityLogServiceCall("018", "", "01", "", "", "", "", "", "", "")
                    // activityLogServiceCall("018", errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)		
                var inputParams = {}
                inputParams["channelName"] = "IB-INQ";
                inputParams["notificationType"] = "Email";
                inputParams["noSendInd"] = "0";
                inputParams["custName"] = customerName;
                inputParams["source"] = "changePassword";
                //inputParams["emailId"] = frmIBCMChgPwd.lblEmailVal.text; //gblEmailId;
                inputParams["notificationSubject"] = "changePassword";
                inputParams["notificationContent"] = "";
                inputParams["Locale"] = kony.i18n.getCurrentLocale();
                invokeServiceSecureAsync("NotificationAdd", inputParams, callBackChangeIBPwdNotification);
            } else {
                dismissLoadingScreen();
                alert("unable to change pwd ");
            }
        } else {
            //dismissLoadingScreenPopup();
            alert(resultable["errMsg"]);
        }
    }
}