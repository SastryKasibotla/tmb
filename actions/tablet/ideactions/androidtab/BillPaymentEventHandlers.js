function eh_frmIBBillPaymentCW_Preshow() {
    var today = new Date(GLOBAL_TODAY_DATE);
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    frmIBBillPaymentCW.lblBPBillPayDate.text = today
    gblClicked = "once";
    if (!gblFromAccountSummary) {
        frmIBBillPaymentCW.hbxMinMax.setVisibility(false);
        frmIBBillPaymentCW.lblBPBillerName.text = "";
        frmIBBillPaymentCW.lblToCompCode.text = "";
        frmIBBillPaymentCW.lblFullPayment.setVisibility(false);
        frmIBBillPaymentCW.txtBillAmt.setVisibility(true);
        frmIBBillPaymentCW.txtBillAmt.setEnabled(false);
        frmIBBillPaymentCW.txtBillAmt.text = "";
        frmIBBillPaymentCW.txtBillAmt.placeholder = "";
        frmIBBillPaymentCW.lblBPRef1.text = kony.i18n.getLocalizedString("keyRef1");
        frmIBBillPaymentCW.lblBPRef1Val.text = "";
        frmIBBillPaymentCW.lblBPRef2.text = kony.i18n.getLocalizedString("keyRef2");;
        frmIBBillPaymentCW.txtBPRef2Val.text = ""
        frmIBBillPaymentCW.hbxRef2.setVisibility(false);
        frmIBBillPaymentCW.hbxRef1.setVisibility(false);
        frmIBBillPaymentCW.imgBPBillerImage.setVisibility(false);
        frmIBBillPaymentCW.line10136690395346.setVisibility(false);
        frmIBBillPaymentCW.lineRef1.setVisibility(false);
    } else {
        frmIBBillPaymentCW.imgBPBillerImage.setVisibility(true);
        frmIBBillPaymentCW.line10136690395346.setVisibility(true);
        frmIBBillPaymentCW.lineRef1.setVisibility(true);
        frmIBBillPaymentCW.hbxAmount.skin = "hbxProper";
        gblBillPaymentEdit = true
    }
    callcrmProfileInqForTopUP();
}

function eh_frmIBBillPaymentCW_Postshow() {
    addIBMenu();
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBBillPaymentCW.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
    else frmIBBillPaymentCW.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
    addNumberCheckListner("txtBillAmt");
}

function eh_frmIBBillPaymentCW_btnAmtFull_onClick() {
    gblFullPayment = true;
    frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
    frmIBBillPaymentCW.hbxAmtVal.setVisibility(false);
    frmIBBillPaymentCW.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBBillPaymentCW.btnAmtFull.skin = btnIBTab3LeftFocus77px;
    frmIBBillPaymentCW.btnAmtMinimum.skin = btnIBTab3MidNrml77px;
    frmIBBillPaymentCW.btnAmtSpecified.skin = btnIBTab3RightNrml77px;
}

function eh_frmIBBillPaymentCW_btnAmtMinimum_onClick() {
    gblFullPayment = true;
    frmIBBillPaymentCW.lblFullPayment.setVisibility(true);
    frmIBBillPaymentCW.hbxAmtVal.setVisibility(false);
    frmIBBillPaymentCW.lblFullPayment.text = numberWithCommas(removeCommos(minAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBBillPaymentCW.btnAmtFull.skin = btnIBTab3LeftNrml77px;
    frmIBBillPaymentCW.btnAmtMinimum.skin = btnIBTab3MidFocus77px;
    frmIBBillPaymentCW.btnAmtSpecified.skin = btnIBTab3RightNrml77px;
}

function eh_frmIBBillPaymentCW_btnAmtSpecified_onClick() {
    gblFullPayment = false;
    frmIBBillPaymentCW.hbxAmtVal.setVisibility(true);
    frmIBBillPaymentCW.txtBillAmt.setEnabled(true);
    frmIBBillPaymentCW.txtBillAmt.setVisibility(true);
    frmIBBillPaymentCW.lblFullPayment.setVisibility(false);
    frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBBillPaymentCW.btnAmtFull.skin = btnIBTab3LeftNrml77px;
    frmIBBillPaymentCW.btnAmtMinimum.skin = btnIBTab3MidNrml77px;
    frmIBBillPaymentCW.btnAmtSpecified.skin = btnIBTab3RightFocus77px;
}

function eh_frmIBBillPaymentCW_txtAreaBPMyNote_onEndEditing() {
    frmIBBillPaymentCW.hbxMyNote.skin = "hbxProper40px"
    frmIBBillPaymentCW.txtAreaBPMyNote.skin = "txtAreaIBNoScroll"
}

function eh_frmIBBillPaymentCW_txtAreaBPMyNote_onBeginEditing() {
    frmIBBillPaymentCW.hbxMyNote.skin = "hbxProperFocus40px"
    frmIBBillPaymentCW.txtAreaBPMyNote.skin = "txtAreaIBNoScrollFocus"
}

function eh_frmIBBillPaymentCW_txtBillAmt_onBeginEditing() {
    frmIBBillPaymentCW.hbxAmount.skin = "hbxProperFocus";
    frmIBBillPaymentCW.txtBillAmt.placeholder = frmIBBillPaymentCW.txtBillAmt.text;
    temp = frmIBBillPaymentCW.txtBillAmt.text;
    frmIBBillPaymentCW.txtBillAmt.text = "";
}

function eh_frmIBBillPaymentCW_txtBillAmt_onEndEditing() {
    frmIBBillPaymentCW.hbxAmount.skin = "hbxProper";
    if (frmIBBillPaymentCW.txtBillAmt.text.trim() == "") {
        frmIBBillPaymentCW.txtBillAmt.text = temp;
    } else {
        kony.print("in Else")
        frmIBBillPaymentCW.txtBillAmt.text = numberWithCommas(removeCommos(frmIBBillPaymentCW.txtBillAmt.text)) + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}

function eh_frmIBBillPaymentCW_txtAreaBPMyNote_onEndEditing() {
    frmIBBillPaymentCW.hbxMyNote.skin = "hbxProper40px"
    frmIBBillPaymentCW.txtAreaBPMyNote.skin = "txtAreaIBNoScroll"
}

function eh_frmIBBillPaymentCW_txtAreaBPMyNote_onBeginEditing() {
    frmIBBillPaymentCW.hbxMyNote.skin = "hbxProperFocus40px"
    frmIBBillPaymentCW.txtAreaBPMyNote.skin = "txtAreaIBNoScrollFocus"
}

function eh_frmIBBillPaymentCW_btnBPPayOn_onClick() {
    alert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"));
}

function eh_frmIBBillPaymentCW_btnBPNext_onClick() {
    alert(kony.i18n.getLocalizedString("keySelectAcct"));
}

function eh_frmIBBillPaymentLP_preshow() {
    frmIBBillPaymentLP.hbxFutureBillPayment.setVisibility(false);
    frmIBBillPaymentLP.hbxToBiller.setVisibility(false);
    frmIBBillPaymentLP.hbxWaterWheel.setVisibility(false);
    frmIBBillPaymentLP.arrowBPScheduleField.setVisibility(false);
    frmIBBillPaymentLP.imgArrowBPBillerField.setVisibility(false);
    frmIBBillPaymentLP.imgArrowBPFromField.setVisibility(false);
    //frmIBBillPaymentLP.hbxFrom.setVisibility(false);
    frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
    frmIBBillPaymentLP.lblMsg.setVisibility(false);
    frmIBBillPaymentLP.lblBPBillsList.setVisibility(true);
    frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
    frmIBBillPaymentLP.hbxAmountDetailsMEA.setVisibility(false);
    if (!gblBillPaymentEdit) {
        frmIBBillPaymentLP.hbxHideBtn.setVisibility(false);
        clearIBBillPaymentLP();
    }
    if (!isNotBlank(frmIBBillPaymentLP.lblBPBillPayDate.text)) {
        IBBillsCurrentSystemDate();
    }
    if (frmIBBillPaymentLP.hbxAmtVal.isVisible) {
        gblFullPayment = false;
    }
    frmIBBillPaymentLP.txtBPSearch.placeholder = kony.i18n.getLocalizedString('Receipent_Search');
    frmIBBillPaymentLP.lblTHB.text = " " + kony.i18n.getLocalizedString('currencyThaiBaht');
    showSchedBillerNicknameIB();
}

function eh_frmIBBillPaymentLP_postshow() {
    addIBMenu();
    frmIBBillPaymentLP.txtBPSearch.placeholder = kony.i18n.getLocalizedString('Receipent_Search');
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBBillPaymentLP.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
    else frmIBBillPaymentLP.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
    frmIBBillPaymentLP.hbxFromLabelContainer.skin = "hbxProperFocus"
        // ticket 28094
        //frmIBBillPaymentLP.CalendarXferDate.dateEditable=false;
        //frmIBBillPaymentLP.calendarXferUntil.dateEditable=false;
    try {
        document.getElementById("CalendarXferDate").setAttribute('readonly', "readonly");
        document.getElementById("calendarXferUntil").setAttribute('readonly', "readonly");
    } catch (e) {}
    // ticket 28094
    addNumberCheckListner("txtBillAmt");
}

function eh_frmIBBillPaymentLP_btnBPBiller_onClick() {
    frmIBBillPaymentLP.segBPBillsList.removeAll();
    frmIBBillPaymentLP.segBPSgstdBillerList.removeAll();
    frmIBBillPaymentLP.hbxFutureBillPayment.setVisibility(false);
    frmIBBillPaymentLP.hbxWaterWheel.setVisibility(false);
    frmIBBillPaymentLP.txtBPSearch.text = "";
    flagSearchBP = 0;
    flagCatBP = 0;
    frmIBBillPaymentLP.arrowBPScheduleField.setVisibility(false);
    frmIBBillPaymentLP.imgArrowBPBillerField.setVisibility(true);
    frmIBBillPaymentLP.imgArrowBPFromField.setVisibility(false);
    frmIBBillPaymentLP.hbxImage.setVisibility(false);
    frmIBBillPaymentLP.lblBPBillsList.setVisibility(true);
    frmIBBillPaymentLP.lblBPSgstdBillerList.setVisibility(true);
    frmIBBillPaymentLP.line10136690395346.setVisibility(false);
    onClickButnBillerIB();
}

function eh_frmIBBillPaymentLP_btnAmtFull_onClick() {
    gblFullPayment = true;
    frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
    frmIBBillPaymentLP.hbxAmtVal.setVisibility(false);
    frmIBBillPaymentLP.lblFullPayment.text = numberWithCommas(removeCommos(fullAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
    frmIBBillPaymentLP.btnAmtFull.skin = btnIBTab3LeftFocus77px;
    frmIBBillPaymentLP.btnAmtMinimum.skin = btnIBTab3MidNrml77px
    frmIBBillPaymentLP.btnAmtSpecified.skin = btnIBTab3RightNrml77px
}

function eh_frmIBBillPaymentLP_btnAmtMinimum_onClick() {
    gblFullPayment = true;
    frmIBBillPaymentLP.lblFullPayment.setVisibility(true);
    frmIBBillPaymentLP.hbxAmtVal.setVisibility(false);
    if (!isNotBlank(minAmt) || parseInt(minAmt) == 0) {
        frmIBBillPaymentLP.lblFullPayment.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmIBBillPaymentLP.lblFullPayment.text = numberWithCommas(removeCommos(minAmt)) + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
    frmIBBillPaymentLP.btnAmtFull.skin = btnIBTab3LeftNrml77px;
    frmIBBillPaymentLP.btnAmtMinimum.skin = btnIBTab3MidFocus77px;
    frmIBBillPaymentLP.btnAmtSpecified.skin = btnIBTab3RightNrml77px;
}

function eh_frmIBBillPaymentLP_btnAmtSpecified_onClick() {
    gblFullPayment = false;
    frmIBBillPaymentLP.hbxAmtVal.setVisibility(true);
    frmIBBillPaymentLP.txtBillAmt.setEnabled(true);
    frmIBBillPaymentLP.txtBillAmt.setVisibility(true);
    frmIBBillPaymentLP.lblTHB.setVisibility(true);
    frmIBBillPaymentLP.lblFullPayment.setVisibility(false);
    frmIBBillPaymentLP.txtBillAmt.text = numberWithCommas(removeCommos(fullAmt));
    frmIBBillPaymentLP.btnAmtFull.skin = btnIBTab3LeftNrml77px;
    frmIBBillPaymentLP.btnAmtMinimum.skin = btnIBTab3MidNrml77px;
    frmIBBillPaymentLP.btnAmtSpecified.skin = btnIBTab3RightFocus77px;
}

function eh_frmIBBillPaymentLP_txtBillAmt_onBeginEditing() {
    frmIBBillPaymentLP.hbxAmount.skin = "hbxProperFocus"
        //frmIBBillPaymentLP.txtBillAmt.placeholder = frmIBBillPaymentLP.txtBillAmt.text;
    temp = frmIBBillPaymentLP.txtBillAmt.text;
    //frmIBBillPaymentLP.txtBillAmt.text = "";
}

function eh_frmIBBillPaymentLP_txtBillAmt_onEndEditing() {
    frmIBBillPaymentLP.hbxAmount.skin = "hbxProper"
    if (frmIBBillPaymentLP.txtBillAmt.text.trim() == "") {
        //frmIBBillPaymentLP.txtBillAmt.text = temp;
    } else {
        kony.print("in Else")
        frmIBBillPaymentLP.txtBillAmt.text = onDoneEditingAmountValue(frmIBBillPaymentLP.txtBillAmt.text);
    }
}

function eh_frmIBBillPaymentLP_txtAreaBPMyNote_onEndEditing() {
    frmIBBillPaymentLP.hbxMyNote.skin = "hbxProper40px"
    frmIBBillPaymentLP.txtAreaBPMyNote.skin = "txtAreaIBNoScroll"
}

function eh_frmIBBillPaymentLP_txtAreaBPMyNote_onBeginEditing() {
    frmIBBillPaymentLP.hbxMyNote.skin = "hbxProperFocus40px"
    frmIBBillPaymentLP.txtAreaBPMyNote.skin = "txtAreaIBNoScrollFocus"
}

function eh_frmIBBillPaymentLP_btnBPPayOn_onClick() {
    if (frmIBBillPaymentLP.lblToCompCode.text == null || frmIBBillPaymentLP.lblToCompCode.text == "" || frmIBBillPaymentLP.lblToCompCode.text == " ") {
        alert(kony.i18n.getLocalizedString("Valid_SelBiller"));
        return;
    } else {
        frmIBBillPaymentLP.hbxWaterWheel.setVisibility(false);
        frmIBBillPaymentLP.hbxFutureBillPayment.setVisibility(true);
        frmIBBillPaymentLP.hbxToBiller.setVisibility(false);
        frmIBBillPaymentLP.arrowBPScheduleField.setVisibility(true);
        frmIBBillPaymentLP.imgArrowBPBillerField.setVisibility(false);
        frmIBBillPaymentLP.imgArrowBPFromField.setVisibility(false);
        frmIBBillPaymentLP.hbxImage.setVisibility(false);
        frmIBBillPaymentLP.btnDaily.skin = "btnIBTab4LeftNrml";
        frmIBBillPaymentLP.btnWeekly.skin = "btnIBTab4MidNrml";
        frmIBBillPaymentLP.btnMonthly.skin = "btnIBTab4MidNrml";
        frmIBBillPaymentLP.btnYearly.skin = "btnIbTab4RightNrml";
        frmIBBillPaymentLP.txtEndTimes.text = "";
        futureBillPayCalDateIB();
        populateSelectedScheduleBillPay();
    }
}

function eh_frmIBBillPaymentLP_btnCancel_onClick() {
    frmIBBillPaymentLP.hbxFutureBillPayment.setVisibility(false);
    frmIBBillPaymentLP.hbxImage.setVisibility(true);
}

function eh_frmIBBillPaymentConfirm_init() {
    frmIBBillPaymentConfirm.txtotp.maxTextLength = 6;
}

function eh_frmIBBillPaymentConfirm_preshow() {
    frmIBBillPaymentConfirm.lblBankRef.setVisibility(false);
    frmIBBillPaymentConfirm.label476047582127701.setVisibility(false);
    frmIBBillPaymentConfirm.label476047582115277.setVisibility(false);
    frmIBBillPaymentConfirm.label476047582115279.setVisibility(false);
    frmIBBillPaymentConfirm.txtotp.text = "";
    frmIBBillPaymentConfirm.tbxToken.text = "";
    frmIBBillPaymentConfirm.hbxOtpBox.setVisibility(false);
    frmIBBillPaymentConfirm.hbxBtn.setVisibility(false);
    frmIBBillPaymentConfirm.hbxNext.setVisibility(true);
    frmIBBillPaymentConfirm.hbxAmountDetailsMEA.setVisibility(false);
    if (gblCompCode == "2533") {
        frmIBBillPaymentConfirm.hbxHideBtn.setVisibility(true);
        frmIBBillPaymentConfirm.lnkHide.text = kony.i18n.getLocalizedString("show");
        frmIBBillPaymentConfirm.lblAmt.text = frmIBBillPaymentLP.lblBPAmount.text
        frmIBBillPaymentConfirm.lblAmtLabel.text = frmIBBillPaymentLP.lblAmtLabel.text;
        frmIBBillPaymentConfirm.lblAmtValue.text = frmIBBillPaymentLP.lblAmtValue.text;
        frmIBBillPaymentConfirm.lblAmtInterest.text = frmIBBillPaymentLP.lblAmtInterest.text;
        frmIBBillPaymentConfirm.lblAmtInterestValue.text = frmIBBillPaymentLP.lblAmtInterestValue.text;
        frmIBBillPaymentConfirm.lblAmtDisconnected.text = frmIBBillPaymentLP.lblAmtDisconnected.text;
        frmIBBillPaymentConfirm.lblAmtDisconnectedValue.text = frmIBBillPaymentLP.lblAmtDisconnectedValue.text;
        frmIBBillPaymentConfirm.hbxMEACustDetails.setVisibility(true);
        showMEACustDetails(frmIBBillPaymentConfirm);
    } else {
        frmIBBillPaymentConfirm.hbxHideBtn.setVisibility(false);
        frmIBBillPaymentConfirm.hbxMEACustDetails.setVisibility(false);
    }
    //startIBBillPaymentProfileInqService();
    onBillPaymentNextIB();
}

function eh_frmIBBillPaymentConfirm_postshow() {
    addIBMenu();
    if (kony.i18n.getCurrentLocale() != "th_TH") {
        frmIBBillPaymentConfirm.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
        // frmIBBillPaymentConfirm.label476047582115284.containerWeight = 11;
        //frmIBBillPaymentConfirm.txtotp.containerWeight = 48;
    } else {
        frmIBBillPaymentConfirm.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
        // frmIBBillPaymentConfirm.label476047582115284.containerWeight = 22;
        // frmIBBillPaymentConfirm.txtotp.containerWeight = 37;
    }
}

function eh_frmIBBillPaymentConfirm_txtotp_onBeginEditing() {
    frmIBBillPaymentConfirm.hbxOTPEntry.skin = "hbxOtpTextField"
}

function eh_frmIBBillPaymentConfirm_txtotp_onEndEditing() {
    frmIBBillPaymentConfirm.hbxOTPEntry.skin = "hbxOtpTextFieldNormal"
}

function eh_frmIBBillPaymentConfirm_tbxToken_onBeginEditing() {
    frmIBBillPaymentConfirm.hbxTokenEntry.skin = "hbxOtpTextField"
}

function eh_frmIBBillPaymentConfirm_tbxToken_onEndEditing() {
    frmIBBillPaymentConfirm.hbxTokenEntry.skin = "hbxOtpTextFieldNormal"
}

function eh_frmIBBillPaymentConfirm_switch2OTP_onClick() {
    frmIBBillPaymentConfirm.hbxTokenEntry.setVisibility(false);
    frmIBBillPaymentConfirm.hbxOTPEntry.setVisibility(true);
    frmIBBillPaymentConfirm.txtotp.setFocus(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    frmIBBillPaymentConfirm.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    startBillPaymentOTPRequestService();
}

function eh_frmIBBillPaymentConfirm_btnNext_onClick() {
    frmIBBillPaymentConfirm.hbxNext.setVisibility(false);
    frmIBBillPaymentConfirm.hbxBtn.setVisibility(true);
    if (!gblTokenSwitchFlag) {
        frmIBBillPaymentConfirm.txtotp.text = "";
        frmIBBillPaymentConfirm.hbxOtpBox.setVisibility(true);
        frmIBBillPaymentConfirm.txtotp.setFocus(true);
        startBillPaymentOTPRequestService();
    } else {
        frmIBBillPaymentConfirm.tbxToken.text = "";
        frmIBBillPaymentConfirm.hbxOtpBox.setVisibility(true);
        frmIBBillPaymentConfirm.txtotp.setFocus(true);
        frmIBBillPaymentConfirm.hbox476047582127699.setVisibility(false);
        frmIBBillPaymentConfirm.hbxOTPsnt.setVisibility(false);
        frmIBBillPaymentConfirm.hbxTokenEntry.setVisibility(true);
        frmIBBillPaymentConfirm.tbxToken.setFocus(true);
    }
}

function eh_frmIBBillPaymentCompletenow_postshow() {
    addIBMenu();
    campaginService('img1', 'frmIBBillPaymentCompletenow', 'I');
    if (kony.i18n.getCurrentLocale() != "th_TH") frmIBBillPaymentCompletenow.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocus";
    else frmIBBillPaymentCompletenow.btnMenuBillPayment.skin = "btnIBMenuBillPaymentFocusThai";
}

function eh_frmIBBillPaymentCompletenow_onhide() {
    gblPaynow = true
}

function eh_frmIBBillPaymentCompletenow_btnHide_onClick() {
    if (frmIBBillPaymentCompletenow.lblBBPaymentValue.isVisible) {
        frmIBBillPaymentCompletenow.lblBBPaymentValue.setVisibility(false);
        frmIBBillPaymentCompletenow.lblBalance.setVisibility(false)
        frmIBBillPaymentCompletenow.lblHide.text = "Unhide";
        frmIBBillPaymentCompletenow.btnHide.margin = [72, 3, 0, 3];
    } else {
        frmIBBillPaymentCompletenow.lblBBPaymentValue.setVisibility(true);
        frmIBBillPaymentCompletenow.lblBalance.setVisibility(true)
        frmIBBillPaymentCompletenow.lblHide.text = "Hide";
        frmIBBillPaymentCompletenow.btnHide.margin = [6, 3, 0, 3];
    }
}

function eh_frmIBBillPaymentCompletenow_vbox5056848206597_onClick() {
    if (frmIBBillPaymentCompletenow.lblBBPaymentValue.isVisible) {
        frmIBBillPaymentCompletenow.lblBBPaymentValue.setVisibility(false);
        frmIBBillPaymentCompletenow.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBBillPaymentCompletenow.lblBBPaymentValue.setVisibility(true);
        frmIBBillPaymentCompletenow.lblHide.text = kony.i18n.getLocalizedString("Hide");
    }
}

function eh_frmIBBillPaymentCompletenow_button1014589649373_onClick() {
    //TMBUtil.DestroyForm(frmIBBillPaymentLP);
    gblPaynow = true;
    gblFromAccountSummary = false;
    gblFromAccountSummaryBillerAdded = false;
    //TMBUtil.DestroyForm(frmIBBillPaymentConfirm);
    callBillPaymentCustomerAccountServiceIB();
}

function eh_frmIBBillPaymentCompletenow_btnRet_onClick() {
    gblPaynow = true;
    gblFirstTimeBillPayment = true;
    gblFromAccountSummary = false;
    gblFromAccountSummaryBillerAdded = false;
    onclickActivationCompleteStart();
    if (kony.application.getCurrentForm().id != "frmIBPostLoginDashboard") {
        menuReset();
    } else {
        menuReset2();
    }
}

function eh_frmIBBillPaymentCompletenow_button1014589649276_onClick() {
    gblPOWcustNME = gblCustomerName;
    gblPOWtransXN = "BillPayment";
    gblPOWchannel = "IB";
    requestfromform = "frmIBBillPaymentCompletenow";
    postOnWall();
}

function eh_frmIBEditFutureBillPaymentPrecnf_txtBxOTP_onBeginEditing() {
    frmIBEditFutureBillPaymentPrecnf.hbxOTPEntry.skin = "hbxOtpTextField";
}

function eh_frmIBEditFutureBillPaymentPrecnf_txtBxOTP_onEndEditing() {
    frmIBEditFutureBillPaymentPrecnf.hbxOTPEntry.skin = "hbxOtpTextFieldNormal";
}

function eh_frmIBEditFutureTopUpPrecnf_txtBxOTP_onBeginEditing() {
    frmIBEditFutureTopUpPrecnf.hbxOTPEntry.skin = "hbxOtpTextField";
}

function eh_frmIBEditFutureTopUpPrecnf_txtBxOTP_onEndEditing() {
    frmIBEditFutureTopUpPrecnf.hbxOTPEntry.skin = "hbxOtpTextFieldNormal";
}

function eh_frmIBFTrnsrEditCnfmtn_txtBxOTPFT_onBeginEditing() {
    frmIBFTrnsrEditCnfmtn.hbxOTPEntry.skin = "hbxOtpTextField";
}

function eh_frmIBFTrnsrEditCnfmtn_txtBxOTPFT_onEndEditing() {
    frmIBFTrnsrEditCnfmtn.hbxOTPEntry.skin = "hbxOtpTextFieldNormal";
}

function showMEACustDetails(frmName) {
    if (gblSegBillerData["MeterNo"] != undefined && gblSegBillerData["MeterNo"] != null && gblSegBillerData["MeterNo"] != "") {
        frmName.lblMeterNumValue.text = gblSegBillerData["MeterNo"];
        frmName.hbxMeterNum.setVisibility(true);
    } else {
        frmName.hbxMeterNum.setVisibility(false);
    }
    if (gblSegBillerData["CustName"] != undefined && gblSegBillerData["CustName"] != null && gblSegBillerData["CustName"] != "") {
        frmName.lblCustNameValue.text = gblSegBillerData["CustName"];
        frmName.hbxMEACustName.setVisibility(true);
    } else {
        frmName.hbxMEACustName.setVisibility(false);
    }
    if (gblSegBillerData["CustAddress"] != undefined && gblSegBillerData["CustAddress"] != null && gblSegBillerData["CustAddress"] != "") {
        frmName.lblCustAddressValue.text = gblSegBillerData["CustAddress"];
        frmName.hbxCustAddress.setVisibility(true);
    } else {
        frmName.hbxCustAddress.setVisibility(false);
    }
    if (kony.i18n.getCurrentLocale() != "th_TH") {
        frmName.lblMeterNum.text = gblSegBillerData["billerMeterNoEn"] + ":";
        frmName.lblNameCustomer.text = gblSegBillerData["billerCustNameEn"] + ":";
        frmName.lblCustAddress.text = gblSegBillerData["billerCustAddressEn"] + ":";
    } else {
        frmName.lblMeterNum.text = gblSegBillerData["billerMeterNoTh"] + ":";
        frmName.lblNameCustomer.text = gblSegBillerData["billerCustNameTh"] + ":";
        frmName.lblCustAddress.text = gblSegBillerData["billerCustAddressTh"] + ":";
    }
}

function editBillerNickNameValidationIB() {
    if (NickNameValid(frmIBMyBillersHome.txtEditBillerNickName.text)) {
        callCustomerBillUpdateIB();
    } else {
        alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
    }
}

function editTopUpNickNameValidationIB() {
    if (NickNameValid(frmIBMyTopUpsHome.txtEditBillerNickName.text)) {
        callCustomerTopUpUpdateIB();
    } else {
        alert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"));
    }
}