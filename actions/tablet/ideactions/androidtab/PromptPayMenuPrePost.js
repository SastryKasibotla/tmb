function frmMBAnyIdRegCompletedMenuPreshow() {
    if (gblCallPrePost) {
        frmMBAnyIdRegCompleted.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmMBAnyIdRegCompleted);
        frmMBAnyIdCompletePreShowLocale.call(this);
    }
}

function frmMBAnyIdRegCompletedMenuPostshow() {
    if (gblCallPrePost) {
        campaginService.call(this, 'imgAnyIdAd', 'frmMBAnyIdRegCompleted', 'M');
    }
    assignGlobalForMenuPostshow();
}

function frmMBAnyIdRegFailMenuPreshow() {
    if (gblCallPrePost) {
        frmMBAnyIdRegFail.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmMBAnyIdRegFail);
    }
}

function frmMBAnyIdRegFailMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBAnyIdRegSaveMenuPreshow() {
    if (gblCallPrePost) {
        frmMBAnyIdRegSave.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmMBAnyIdRegSave);
    }
}

function frmMBAnyIdRegTnCMenuPreshow() {
    if (gblCallPrePost) {
        frmMBAnyIdRegTnC.scrollboxMain.scrollToEnd();
        isMenuShown = false;
        isSignedUser = true;
        DisableFadingEdges.call(this, frmMBAnyIdRegTnC);
        frmMBAnyIdRegTnCPreShow.call(this);
    }
}

function frmMBAnyIdRegTnCMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmMBAnyIDSelectActsMenuPreshow() {
    if (gblCallPrePost) {
        frmMBAnyIDSelectActs.scrollboxMain.scrollToEnd();
        clearSegDataAnyIDAccntList.call(this);
        isMenuRendered = false;
        isMenuShown = false;
        frmAnyIDAccountListPreShow.call(this);
        DisableFadingEdges.call(this, frmMBAnyIDSelectActs);
    }
}

function frmMBAnyIDSelectActsMenuPostshow() {
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    }
    assignGlobalForMenuPostshow();
}

function frmRegAnyIdAnnouncementMenuPreshow() {
    if (gblCallPrePost) {}
}

function frmRegAnyIdAnnouncementMenuPostshow() {
    if (gblCallPrePost) {
        preShowfrmRegAnyIdAnnouncement.call(this);
    }
    assignGlobalForMenuPostshow();
}