function frmMBQuickBalanceBriefMenuPreshow() {
    if (gblCallPrePost) {
        var deviceInfo = kony.os.deviceInfo().name;
        var deviceInfo1 = kony.os.deviceInfo();
        var deviceHght = deviceInfo1["deviceHeight"];
        isMenuShown = false;
        frmMBQuickBalanceBrief.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmMBQuickBalanceBrief);
        frmMBQuickBalanceBriefPreShow.call(this);
    }
}

function frmMBQuickBalanceBriefMenuPostshow() {
    if (gblCallPrePost) {
        isMenuRendered = false;
        isMenuShown = false;
        frmMBQuickBalanceBrief.scrollboxMain.scrollToEnd();

    }
    assignGlobalForMenuPostshow();
}

function frmMBQuickBalanceSettingMenuPreshow() {
    if (gblCallPrePost) {
        var deviceInfo = kony.os.deviceInfo().name;
        var deviceInfo1 = kony.os.deviceInfo();
        var deviceHght = deviceInfo1["deviceHeight"];
        isMenuShown = false;
        frmMBQuickBalanceSetting.scrollboxMain.scrollToEnd();
        DisableFadingEdges.call(this, frmMBQuickBalanceSetting);
        frmMBQuickBalanceSettingPreShow.call(this);

    }
}

function frmMBQuickBalanceSettingMenuPostshow() {
    if (gblCallPrePost) {
        isMenuRendered = false;
        isMenuShown = false;
        frmMBQuickBalanceSetting.scrollboxMain.scrollToEnd();

    }
    assignGlobalForMenuPostshow();
}