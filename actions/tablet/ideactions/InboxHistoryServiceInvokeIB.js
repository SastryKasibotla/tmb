

var SortByTypeIN = "000";
var SortByOrderIN = "000";

var tempGloIN = null;
var gblDetailPageStateChangerIN = "start";
var gblCurrentNotfnOnDetailsIN = null;
var getDetailsOfNotfnIN = null;
var gblInboxMsgDetails;
//GET INBOX HISTORY
function getInboxHistoryIB(){
	var inputParams = {};
	showLoadingScreenPopup();
	invokeServiceSecureAsync("inboxhistory", inputParams, getInboxHistoryIBCB);
}

function getInboxHistoryIBCB(status, resultset){
	if(status == 400){
		
		if(resultset["status"] == "1"){
			if(resultset["Results"] != null){
				//DO NOTHING
			}
			else{
				showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			}
			frmIBInboxHome.lblNoNotfn.isVisible = true;
			frmIBInboxHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoMessages");
			frmIBInboxHome.btnCancel.setEnabled(true);
			frmIBInboxHome.btnCancel.skin = "lblWhiteBgBlackFnt";
			frmIBInboxHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
			frmIBInboxHome.btnCancel.hoverSkin = "lblWhiteBgBlackFnt";
			frmIBInboxHome.btnCancel.setEnabled(false);
			frmIBInboxHome.btnRight.skin = "btnDeleteActive";
			frmIBInboxHome.btnRight.focusSkin = "btnDeleteActive";
			frmIBInboxHome.btnRight.setEnabled(false);
			frmIBInboxHome.btnRight.isVisible = false;
			frmIBInboxHome.hboxTxnType.isVisible = false;
			frmIBInboxHome.hbox47515469456620.isVisible = false;
			frmIBInboxHome.vBoxRcRight.padding=[0,50,0,0];
			frmIBInboxHome.imgTMBLogo.isVisible = true;
			frmIBInboxHome.scrollbox474082218273129.isVisible = false;
			frmIBInboxHome.label476822990280082.isVisible = false;
			frmIBInboxHome.segRequestHistory.removeAll();
		}
		else if(resultset["status"] == "0"){
			//
			if(resultset["Results"].length > 0){
				gblInboxMsgDetails = resultset["Results"];
				ServiceResultInbox.length = 0;
				for(var i = 0; i < resultset["Results"].length; i++){
					ServiceResultInbox.push(resultset["Results"][i]);
				}
				gblMaxSetImportant = resultset["maxsetimportant"];
				frmIBInboxHome.lblNoNotfn.isVisible = false;
				if(SortByTypeIN == "000"){
					frmIBInboxHome.cmbSortType.selectedKey = "000";
					frmIBInboxHome.hbox47515469456620.isVisible = false;
				}
				//frmIBInboxHome.hboxTxnType.isVisible = true;
				//frmIBInboxHome.hbox47515469456620.isVisible = false;
				//gblInboxSortedOrNormal = false;
				populateInboxNormalStateIB();
				//SortByTypeIN = "Date/Time";
				//SortByTypeIN = "000";
 				gblMYInboxCountFrom = true;
				unreadInboxMessagesTrackerLocalDBSyncIB();
			}
			else{
				frmIBInboxHome.lblNoNotfn.isVisible = true;
				frmIBInboxHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoMessages");
				frmIBInboxHome.btnCancel.setEnabled(true);
				frmIBInboxHome.btnCancel.skin = "lblWhiteBgBlackFnt";
				frmIBInboxHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
				frmIBInboxHome.btnCancel.hoverSkin = "lblWhiteBgBlackFnt";
				frmIBInboxHome.btnCancel.setEnabled(false);
				frmIBInboxHome.btnRight.skin = "btnDeleteActive";
				frmIBInboxHome.btnRight.focusSkin = "btnDeleteActive";
				frmIBInboxHome.btnRight.setEnabled(false);
				frmIBInboxHome.btnRight.isVisible = false;
				frmIBInboxHome.hboxTxnType.isVisible = false;
				frmIBInboxHome.hbox47515469456620.isVisible = false;
				frmIBInboxHome.vBoxRcRight.padding=[0,50,0,0];
				frmIBInboxHome.imgTMBLogo.isVisible = true;
				frmIBInboxHome.scrollbox474082218273129.isVisible = false;
				frmIBInboxHome.label476822990280082.isVisible = false;
				frmIBInboxHome.segRequestHistory.removeAll();
			}
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
			frmIBInboxHome.segRequestHistory.removeAll();
		}
		dismissLoadingScreenPopup();
	}
}

function populateInboxNormalStateIB(){
	locale = kony.i18n.getCurrentLocale();
	InboxHomePageState = false;
	if(gblDetailPageStateChangerIN == "start"){
		frmIBInboxHome.vBoxRcRight.padding=[0,50,0,0];
		frmIBInboxHome.imgTMBLogo.isVisible = true;
		frmIBInboxHome.scrollbox474082218273129.isVisible = false;
		frmIBInboxHome.label476822990280082.isVisible = false;
	}
	else if(gblDetailPageStateChangerIN == "delete"){
		if(gblCurrentNotfnOnDetailsIN == null){
			//LET THE PREVIOUS STATE REMAIN
		}
		else{
			getInboxDetailsIB();
		}
	}
	else if(gblDetailPageStateChangerIN == "markimp" || gblDetailPageStateChangerIN == "notfndeletederror" || gblDetailPageStateChanger == "notfndetails"){
		//LET THE PREVIOUS STATE REMAIN
	}
	
	populateInboxIB();
	frmIBInboxHome.segRequestHistory.onRowClick = getInboxDetailsIB;
	frmIBInboxHome.segRequestHistory.selectionBehavior = constants.SEGUI_DEFAULT_BEHAVIOR;
	frmIBInboxHome.btnCancel.setEnabled(true);
	frmIBInboxHome.btnCancel.skin = "lblWhiteBgBlackFnt";
	frmIBInboxHome.btnCancel.focusSkin = "lblWhiteBgBlackFnt";
	frmIBInboxHome.btnCancel.hoverSkin = "lblWhiteBgBlackFnt";
	frmIBInboxHome.btnCancel.setEnabled(false);
	frmIBInboxHome.btnRight.setEnabled(true);
	frmIBInboxHome.btnRight.skin = "btnIBNotHomeDelNormal";
	frmIBInboxHome.btnRight.focusSkin = "btnIBNotHomeDelFocus";
	frmIBInboxHome.btnRight.isVisible = true;
}

function populateInboxDeleteStateIB(){
	locale = kony.i18n.getCurrentLocale();
	InboxHomePageState = true;
	frmIBInboxHome.segRequestHistory.onRowClick = SelectForDeleteTrackerInboxIB;
	frmIBInboxHome.segRequestHistory.selectionBehavior = constants.SEGUI_MULTI_SELECT_BEHAVIOR;
	
	populateInboxIB();
	frmIBInboxHome.btnCancel.setEnabled(true);
	frmIBInboxHome.btnCancel.skin = "btnCancel";
	frmIBInboxHome.btnCancel.focusSkin = "btnCancel";
	frmIBInboxHome.btnCancel.hoverSkin = "btnIBNotHomeBackFocus";
	frmIBInboxHome.btnRight.setEnabled(true);
	frmIBInboxHome.btnRight.skin = "btnDeleteActive";
	frmIBInboxHome.btnRight.focusSkin = "btnDeleteActive";
	frmIBInboxHome.btnRight.isVisible = true;
}

function SelectForDeleteTrackerInboxIB(){
	if(frmIBInboxHome.segRequestHistory.selectedItems != null){
		frmIBInboxHome.btnRight.skin = "btnIBNotHomeDelNormal";
		frmIBInboxHome.btnRight.focusSkin = "btnIBNotHomeDelFocus";
	}
	else{
		frmIBInboxHome.btnRight.skin = "btnDeleteActive";
		frmIBInboxHome.btnRight.focusSkin = "btnDeleteActive";
	}
}

function populateInboxIB(){
	gblInboxDetailsCache.length = 0;
	gblInboxDetailsCacheForSort.length = 0;
	var InboxOnDate = new Array(); 
	var localeSup = "EN";
	var skinImp = null;
	var skinRowRead = null;
	if(locale == "th_TH"){
		localeSup = "TH";
	}
	var onclickBTNI = null;
	if(!InboxHomePageState){
		onclickBTNI = markInboxImportantIB;
	}
		
	var date = ServiceResultInbox[0]["date"]
	for(var i = 0; i < ServiceResultInbox.length; i++){
		skinImp = "btnImpN";
		skinRowRead = "hbxSkinSegGrey280Hover";
		skinLblNotfnSub="lblNicknameBold";
		skinlblNotfnFrom="lbltxt16pxBold";
		if(ServiceResultInbox[i]["imp"] == "Y"){
			skinImp = "btnImpY";
		}
		if(ServiceResultInbox[i]["read"] == "Y"){
			skinRowRead = "hbxSkinSegNormal280";
			skinLblNotfnSub="lblNickname";
			skinlblNotfnFrom="lbltxt16px";
		}
		if(date == ServiceResultInbox[i]["date"]){
			var timeR = cutTime(ServiceResultInbox[i]["time"]);
			if (selectedNotification != null && selectedNotification.trim() != "" && ServiceResultInbox[i]["id"].trim() == selectedNotification.trim()) {
				if(skinImp == "btnImpY")
					skinImp = "btnImpY";
				var tempRecord = {
					hbox476793199281301: {
						skin: "hbxSkinSegFocus280"
					},
					ID: ServiceResultInbox[i]["id"],
					lblNotfnFrom: {
						text: kony.i18n.getLocalizedString("Transfer_from") + " " + ServiceResultInbox[i]["sender"+localeSup], skin: skinlblNotfnFrom
					},
					lblNotfnSub: {
						text: ServiceResultInbox[i]["subject"+localeSup], skin: skinLblNotfnSub
					},
					lblTime: {
						text: timeR, skin: "lblBlue80"
					},
					btnImp: {
						skin: skinImp, onClick: onclickBTNI
					},
					imgArrow: {
						src: "navarrow3.png"
					}
				}
				if(InboxHomePageState) {
					tempRecord["imgDeleteSelect"] = "chbox.png";
				}
			}
			else{
				var tempRecord = {
					hbox476793199281301: {
						skin: skinRowRead
					},
					ID: ServiceResultInbox[i]["id"],
					lblNotfnFrom: {
						text: kony.i18n.getLocalizedString("Transfer_from") + " " + ServiceResultInbox[i]["sender"+localeSup], skin: skinlblNotfnFrom
					},
					lblNotfnSub: {
						text: ServiceResultInbox[i]["subject"+localeSup], skin: skinLblNotfnSub
					},
					lblTime: {
						text: timeR, skin: "lblBlue80"
					},
					btnImp: {
						skin: skinImp, onClick: onclickBTNI
					},
					imgArrow: {
						src: "navarrow3.png"
					}
				}
				if(InboxHomePageState) {
					tempRecord["imgDeleteSelect"] = "chbox.png";
				}
			}
			//
			InboxOnDate.push(tempRecord);
			gblInboxDetailsCacheForSort.push(tempRecord);
		}
		else{
			var tempPushArrayInbox = new Array();
			var dateR = dateFormatTMB(date);
			tempPushArrayInbox.push({lblDateHeader: dateR});
			tempPushArrayInbox.push(InboxOnDate);
			gblInboxDetailsCache.push(tempPushArrayInbox);
			var InboxOnDate = new Array();
			date = ServiceResultInbox[i]["date"];
			i = i - 1;
		}
	}
	var tempPushArrayInbox = new Array();
	var dateR = dateFormatTMB(date);
	tempPushArrayInbox.push({lblDateHeader: dateR});
	tempPushArrayInbox.push(InboxOnDate);
	gblInboxDetailsCache.push(tempPushArrayInbox);
	var InboxOnDate = new Array();
	frmIBInboxHome.segRequestHistory.removeAll();
	
	//DECIDE WHICH ARRAY TO POPULATE ON THE SEGMENT
	if(SortByTypeIN == "001"){
		if(SortByOrderIN == "000"){
			gblInboxDetailsCacheForSort.sort(dynamicSort("lblNotfnFrom"));
			frmIBInboxHome.segRequestHistory.data = gblInboxDetailsCacheForSort;
		}
		else if(SortByOrderIN == "001"){
			gblInboxDetailsCacheForSort.sort(dynamicSort("-lblNotfnFrom"));
			frmIBInboxHome.segRequestHistory.data = gblInboxDetailsCacheForSort;
		}
	}
	else if(SortByTypeIN == "000"){
		frmIBInboxHome.segRequestHistory.data = gblInboxDetailsCache;
	}
}

//GET INBOX DETAILS
function getInboxDetailsIB(){
	showLoadingScreenPopup();
	var msg = null;
	if(getDetailsOfNotfnIN == null){
		if(frmIBInboxHome.segRequestHistory.selectedItems == null)
			frmIBInboxHome.segRequestHistory.selectedIndex = [0,0];	// to fix DEF17059
		msg = frmIBInboxHome.segRequestHistory.selectedItems[0].ID;
	}
	else{
		msg = getDetailsOfNotfnIN;
	}
	var inputParams = {
		messageId : msg
	};
	tempGloIN = msg;
	selectedNotification = msg;
	getDetailsOfNotfnIN = null;
	invokeServiceSecureAsync("inboxdetails", inputParams, getInboxDetailsIBCB)
}

function getInboxDetailsIBCB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			if(resultset["Result"].length > 0){
				frmIBInboxHome.vBoxRcRight.padding=[0,0,0,0];
				frmIBInboxHome.imgTMBLogo.isVisible = false;
				frmIBInboxHome.scrollbox474082218273129.containerHeight=140;
				frmIBInboxHome.scrollbox474082218273129.isVisible = true;
				frmIBInboxHome.label476822990280082.isVisible = true;
				//to fix DEF14742, uncommented the first four fields 
				if(undefined != resultset["Result"][0]["INBOX_LINK_FLAG_INTERNET"]){
					gblCmpIntExt = resultset["Result"][0]["INBOX_LINK_FLAG_INTERNET"];
				}else {
					gblCmpIntExt = "00";
				}
				
				if(gblCmpIntExt != "00"){
				  frmIBInboxHome.btnInbox.setVisibility(true);
				   }else{
				  frmIBInboxHome.btnInbox.setVisibility(false);
				   }
				if(gblCmpIntExt == "01" || gblCmpIntExt == "02" || gblCmpIntExt == "03")
				{
				  gblCampaignDataEN=resultset["Result"][0]["INBOX_LINK_INTERNET_EN"];
				  gblCampaignDataTH=resultset["Result"][0]["INBOX_LINK_INTERNET_TH"];
				}
				 
				tempInboxDetails = {
						lblNotfnSubEN : resultset["Result"][0]["subjectEN"],
						lblNotfnSubTH : resultset["Result"][0]["subjectTH"],
						lblNotfnMessageEN : resultset["Result"][0]["messageEN"],
						lblNotfnMessageTH : resultset["Result"][0]["messageTH"],
						imgDetailIBEN : resultset["Result"][0]["imageIBEN"],
						imgDetailIBTH : resultset["Result"][0]["imageIBTH"],
						lblNotfnFromEN : resultset["Result"][0]["senderEN"],
						lblNotfnFromTH : resultset["Result"][0]["senderTH"],
						cmpCode : resultset["Result"][0]["cmpCode"],
						cmpType : resultset["Result"][0]["cmpType"],
						lblNotfnDate : dateFormatTMB(resultset["Result"][0]["date"]) + ", " + cutTime(resultset["Result"][0]["time"])
				}
				// below code is added to capture the campaign response for inbox
				var cmpCode = resultset["Result"][0]["cmpCode"];
				var cmpType = resultset["Result"][0]["cmpType"];
				var cmpEndDate = resultset["Result"][0]["cmpEndDate"];
				gblCampaignResp = "frmIBInboxHome" + "~" + cmpType + "~" + cmpCode + "~" + cmpEndDate + "~" + "01" + "~" + "I";
				if(cmpType !='M'){
					//if(langSwitch == 'N'){
						campaignBannerIBCount("D","I");
					//}
				}
				
				gblDetailPageStateChangerIN = "notfndetails";
				populateInboxDetailsIB();//kony.i18n.getCurrentLocale());
				getInboxHistoryIB();
				gblCurrentNotfnOnDetailsIN = tempGloIN;
				
			}
		}
		else if(resultset["status"] == "2"){
			showAlertRcMB(kony.i18n.getLocalizedString("keyMessageDeleted"), kony.i18n.getLocalizedString("info"), "info")
			gblDetailPageStateChangerIN = "notfndeletederror";
			getInboxHistoryIB();
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	dismissLoadingScreenPopup();
	gblMYInboxCountFrom = true;
}

function populateInboxDetailsIB(){
	var localeSup = "EN";
	//Begin MIB-3282
	locale = kony.i18n.getCurrentLocale();
	//End MIB-3282
	if(locale == "th_TH"){
		localeSup = "TH";
	}
	
	if(undefined == tempInboxDetails || null == tempInboxDetails){
		frmIBInboxHome.imgDetail.isVisible = false;
	} else if(tempInboxDetails["imgDetailIB"+localeSup] == null || tempInboxDetails["imgDetailIB"+localeSup] == undefined || tempInboxDetails["imgDetailIB"+localeSup] == ""){
		frmIBInboxHome.imgDetail.isVisible = false;
	} else{
		frmIBInboxHome.imgDetail.src = null;
		frmIBInboxHome.imgDetail.src = tempInboxDetails["imgDetailIB"+localeSup];
		frmIBInboxHome.imgDetail.isVisible = true;
	}		
	if(undefined != tempInboxDetails && null != tempInboxDetails){
		frmIBInboxHome.label474082218273331.text = tempInboxDetails["lblNotfnFrom"+localeSup];
		frmIBInboxHome.label474082218274029.text = tempInboxDetails["lblNotfnSub"+localeSup];
		frmIBInboxHome.lblNotfnDate.text = tempInboxDetails["lblNotfnDate"];
		frmIBInboxHome.lblNotfnMessage.text = tempInboxDetails["lblNotfnMessage"+localeSup];	
	}
	
	
}

//DELETE INBOX/S
function deleteInboxIB(){
	showLoadingScreenPopup();
	var messages = "";
	var length = frmIBInboxHome.segRequestHistory.selectedItems.length;
	for(var i = 0; i < length; i++){
		if(i == (length - 1)){
			messages = messages + frmIBInboxHome.segRequestHistory.selectedItems[i].ID;
			if(SortByTypeIN == "000"){
				var idPrev = frmIBInboxHome.segRequestHistory.selectedItems[i].ID;
				for(var j = 0; j < frmIBInboxHome.segRequestHistory.data.length; j++){
					for (var j1 = 0; j1 < frmIBInboxHome.segRequestHistory.data[j][1].length; j1++) {
						
	                    if (frmIBInboxHome.segRequestHistory.data[j][1][j1].ID == idPrev) {
							if(j1 == (frmIBInboxHome.segRequestHistory.data[j][1].length - 1)){
								if(j == (frmIBInboxHome.segRequestHistory.data.length - 1)){//LAST
									getDetailsOfNotfn = frmIBInboxHome.segRequestHistory.data[0][1][0].ID;
								}
								else{//NOT LAST
									getDetailsOfNotfn = frmIBInboxHome.segRequestHistory.data[j+1][1][0].ID;
								}
							}
							else{
								getDetailsOfNotfn = frmIBInboxHome.segRequestHistory.data[j][1][j1 + 1].ID;
							}
	                        break;
	                    }
	                }
				}
			}
		}
		else{
			messages = messages + frmIBInboxHome.segRequestHistory.selectedItems[i].ID + ",";
		}
	}
	var inputParams = {
		messageId : messages
	};
	invokeServiceSecureAsync("inboxdelete", inputParams, deleteInboxIBCB)
}

function deleteInboxIBCB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			gblDetailPageStateChangerIN = "delete";
			getInboxHistoryIB();
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	dismissLoadingScreenPopup();
}

//MARK INBOX IMPORTANT
function markInboxImportantIB(){
	showLoadingScreenPopup();
	var imp = frmIBInboxHome.segRequestHistory.selectedItems[0]["btnImp"]["skin"];
	var ind = frmIBInboxHome.segRequestHistory.selectedIndex;
	var imp1 = ind[0];
	var imp2 = ind[1];
	if(SortByTypeIN == "000"){
		var msgID = gblInboxDetailsCache[imp1][1][imp2]["ID"];
	}
	else{
		var msgID = gblInboxDetailsCacheForSort[imp2]["ID"];
	}
	var impStat = "Y";
	if(imp == "btnImpY"){
		impStat = "N";
	}
	var inputParams = {
		messageId : msgID,
		impStatus : impStat
	};
	invokeServiceSecureAsync("inboximportant", inputParams, markInboxImportantIBCB)
}

function markInboxImportantIBCB(status, resultset){
	if(status == 400){
		if(resultset["status"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
		else if(resultset["status"] == "0"){
			gblDetailPageStateChangerIN = "markimp";
			getInboxHistoryIB();
		}
		else if(resultset["status"] == "2"){
			if(resultset["error"] == "MaxLimitReached"){
				dismissLoadingScreenPopup();
				showAlertRcMB(kony.i18n.getLocalizedString("keyMessagesMaxLimit") + " " + gblMaxSetImportant + " " + kony.i18n.getLocalizedString("keyMessagesMaxLimit1"), kony.i18n.getLocalizedString("info"), "info");
				frmIBInboxHome.segRequestHistory.removeAll();
				frmIBInboxHome.segRequestHistory.data = gblInboxDetailsCache;
			}
			else if(resultset["error"] == "InboxDeleted"){
				gblDetailPageStateChangerIN = "notfndeletederror";
				getInboxHistoryIB();
			}
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error")
		}
	}
	dismissLoadingScreenPopup();
}

function popupDeleteConfirmInboxIB(){
	popUpNotfnDeleteIB.dismiss();
	deleteInboxIB();
}

function popupDeleteCancelInboxIB(){
	popUpNotfnDeleteIB.dismiss();
}

function popupSortTypeFuncIB(){
	var sortBy = frmIBInboxHome.cmbSortType.selectedKey;
		if(sortBy == "000"){
			//FILL SERVICE RETURNED DATA
			SortByTypeIN = "000";
			if(InboxHomePageState){
				gblInboxSortedOrNormal = false;
				populateInboxDeleteStateIB();
			}
			else{
				gblInboxSortedOrNormal = false;
				populateInboxNormalStateIB();
			}
			frmIBInboxHome.hbox47515469456620.isVisible = false;
			SortByTypeIN = "000";
		}
		else if(sortBy == "001"){
			gblInboxDetailsCacheForSort.sort(dynamicSort("lblNotfnFrom"));
			
			SortByTypeIN = "001";
			if(InboxHomePageState){
				gblInboxSortedOrNormal = true;
				populateInboxDeleteStateIB();
			}
			else{
				gblInboxSortedOrNormal = true;
				populateInboxNormalStateIB();
			}
			frmIBInboxHome.hbox47515469456620.isVisible = true;
		}
}

function popupSortOrderFuncIB(){
	var sortBy = frmIBInboxHome.cmbSortOrder.selectedKey;
		if(sortBy.toLowerCase() == "000"){
			gblInboxDetailsCacheForSort.sort(dynamicSort("lblNotfnFrom"));
			
			SortByOrderIN = "000"
		}
		else if(sortBy.toLowerCase() == "001"){
			gblInboxDetailsCacheForSort.sort(dynamicSort("-lblNotfnFrom"));
			
			SortByOrderIN = "001"
		}
		if(InboxHomePageState){
			gblInboxSortedOrNormal = true;
			populateInboxDeleteStateIB();
		}
		else{
			gblInboxSortedOrNormal = true;
			populateInboxNormalStateIB();
		}
}

function InboxIBLocaleChanger(){
	if(locale == kony.i18n.getCurrentLocale()){
		//NO CHANGE
	}
	else{
		if(kony.i18n.getCurrentLocale() == "en_US"){
			locale = "en_US";
		}
		else{
			locale = "th_TH";
		}
		frmIBInboxHome.label474082218268567.text = kony.i18n.getLocalizedString("keylblmessage");
		frmIBInboxHome.lblSorBy.text = kony.i18n.getLocalizedString("keyCalendarSortBy");
		frmIBInboxHome.lblNoNotfn.text = kony.i18n.getLocalizedString("keyNoNotifications");
		frmIBInboxHome.label476822990280082.text = kony.i18n.getLocalizedString("keyMessageDatails");
		frmIBInboxHome.btnInbox.text = kony.i18n.getLocalizedString("keyClick");
		
		var masDorder= [["000", "kony.i18n.getLocalizedString(\"keyAscending\")"],
            ["001", "kony.i18n.getLocalizedString(\"keyDescending\")"]];
        frmIBInboxHome.cmbSortOrder.masterData= masDorder;
        
        var masDtype= [["000", "kony.i18n.getLocalizedString(\"MyActivitiesIB_Date\")"],
            ["001", "kony.i18n.getLocalizedString(\"keySender\")"]];
        frmIBInboxHome.cmbSortType.masterData= masDtype;
        frmIBInboxHome.cmbSortType.selectedKey= SortByTypeIN; //"000";
        frmIBInboxHome.cmbSortOrder.selectedKey= SortByOrderIN; //"000";

		if("001" == frmIBInboxHome.cmbSortType.selectedKey){
			popupSortTypeFuncIB();
			popupSortOrderFuncIB();
		}else {
			frmIBInboxHome.hbox47515469456620.isVisible = false;
		}		
        
		populateInboxIB();
		populateInboxDetailsIB();
	}
}

function frmIBInboxHome_frmIBInboxHome_postShow(){
	addIBMenu();
	selectedNotification = null;
	
	getInboxHistoryIB();
	populateInboxIB();
	destroyAllForms();
}
