package com.konylabs.ffi;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import com.konylabs.api.TableLib;
import com.konylabs.vm.LuaTable;



import com.kone.tmb.notification.NotificationUtils;
import com.konylabs.libintf.Library;
import com.konylabs.libintf.JSLibrary;
import com.konylabs.vm.LuaError;
import com.konylabs.vm.LuaNil;


public class N_PushNotificationUtils extends JSLibrary {

 
 
	public static final String areNotificationsEnabled = "areNotificationsEnabled";
 
 
	public static final String navigateToSettings = "navigateToSettings";
 
	String[] methods = { areNotificationsEnabled, navigateToSettings };


 Library libs[] = null;
 public Library[] getClasses() {
 libs = new Library[0];
 return libs;
 }



	public N_PushNotificationUtils(){
	}

	public Object[] execute(int index, Object[] params) {
		// TODO Auto-generated method stub
		Object[] ret = null;
 
		int paramLen = params.length;
 int inc = 1;
		switch (index) {
 		case 0:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.areNotificationsEnabled( );
 
 			break;
 		case 1:
 if (paramLen != 0){ return new Object[] {new Double(100),"Invalid Params"}; }
 ret = this.navigateToSettings( );
 
 			break;
 		default:
			break;
		}
 
		return ret;
	}

	public String[] getMethods() {
		// TODO Auto-generated method stub
		return methods;
	}
	public String getNameSpace() {
		// TODO Auto-generated method stub
		return "PushNotificationUtils";
	}


	/*
	 * return should be status(0 and !0),address
	 */
 
 
 	public final Object[] areNotificationsEnabled( ){
 
		Object[] ret = null;
 Boolean val = new Boolean(com.kone.tmb.notification.NotificationUtils.areNotificationsEnabled( ));
 
 			ret = new Object[]{val, new Double(0)};
 		return ret;
	}
 
 
 	public final Object[] navigateToSettings( ){
 
		Object[] ret = null;
 com.kone.tmb.notification.NotificationUtils.navigateToSettings( );
 
 ret = new Object[]{LuaNil.nil, new Double(0)};
 		return ret;
	}
 
};
