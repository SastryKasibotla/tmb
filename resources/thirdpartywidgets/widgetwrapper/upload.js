var imgCNT = 0;flag = 0;
upload = {
						
	initializeWidget : function(parentNode, widgetModel)
	{
		var key1="";
		key1=kony.i18n.getLocalizedString("keychooseFile");
		var imageUpload = 
			'<div id="form1">'+
			'<div style	="width:280px;height:35px;">'+	 
			'<span class="file-wrapper" style = "cursor: pointer; display: inline-block; overflow: hidden; position: relative;">'+
			'<input id="fileToUpload" type="file" name="fileToUpload" onchange="fileSelected();" style = "cursor: pointer; height: 100%; position: absolute; right: 0; top: 0; filter: alpha(opacity=1); -moz-opacity: 0.01; opacity: 0.01; font-size: 100px;" />'+
			'<input type="label" name="File Label" id="Choose" value="' + key1 + '" style="color:#ffffff;width:100%;height:100%; margin: 0px 0px 0px 15px; ;font-size: 150% ;font-family:DB Ozone X; background :url(desktopweb/images/btn_popup_dark.png) ; width: 280px; height: 35px; text-align :center; "/>'+
			'</span>'+
			'</div>'+    
			'</div>';
				
		parentNode.innerHTML = imageUpload;
	},

	modelChange : function(widgetModel, propertyChanged, propertyValue){}

}

function fileSelected() {
	try{
	   kony.print("entered...");
		var file = document.getElementById('fileToUpload');
		if (file) {
			var fileSize = 0;
			if (file.size > 1024 * 1024)
				fileSize = (Math.round(file.size * 5 * 100 / (1024 * 1024)) / 100).toString() + 'MB';
			else
				fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';

		}
		uploadFile();
		document.getElementById("fileToUpload").remove();
}catch(e){
		document.getElementById("fileToUpload").remove();
		kony.print("Exception in  remove element:"+e);
	}
}

function uploadFile() {
	try{
		kony.print("entered... uploadFile");
		if(document.getElementById('fileToUpload').value == ""){
			alert("Please select a file to upload.");
		}else{
			var method = "post"; 
			var url;
			if (appConfig.secureServerPort != null) {
				url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/UploadServlet";
				 
			}
			else {
				url = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/UploadServlet";
				 
			}
			kony.print("URL:"+url);
			var form = document.createElement("form");
			form.setAttribute("method", method);
			form.setAttribute("action", url);
			form.setAttribute("enctype", "multipart/form-data");
			form.setAttribute("encoding", "multipart/form-data");
			form.setAttribute("target","iframe_outs");
			var hiddenField = document.getElementById('fileToUpload');
			var hiddenField1 = document.createElement("input");
			hiddenField1.setAttribute("type", "hidden");
			hiddenField1.setAttribute("name", "crmId");
			hiddenField1.setAttribute("value", gblcrmId);
			form.appendChild(hiddenField);
			form.appendChild(hiddenField1);
			kony.print("Current form id:"+kony.application.getCurrentForm().id);
			if(kony.application.getCurrentForm().id == "frmIBMyReceipentsAddContactManually"){
				var hiddenField2 = document.createElement("input");
				hiddenField2.setAttribute("type", "hidden");
				hiddenField2.setAttribute("name", "personalizedId");
				hiddenField2.setAttribute("value", "temp");
				form.appendChild(hiddenField2);
			}
			document.body.appendChild(form);
			var iframe_out=document.createElement("iframe");
			iframe_out.name="iframe_outs";
			iframe_out.id="iframe_outs";
			iframe_out.setAttribute("src",url);
			if(document.getElementById('iframe_outs') !="" && document.getElementById('iframe_outs') !=null)
			{
			  var temp = document.getElementById('iframe_outs');
			  temp.parentNode.removeChild(temp);
			}
			document.body.appendChild(iframe_out);
			iframe_out.style.visibility="hidden";
			kony.print("before submit");
			form.submit();
			kony.print(document.getElementById('fileToUpload').files[0].name);
			if(document.getElementById('fileToUpload') !="" && document.getElementById('fileToUpload') !=null)
			{
			  var temp = document.getElementById('fileToUpload');
			  temp.parentNode.removeChild(temp);
			}
				profilePicFlag = "picture";
				profilePicFlagIB = false;
				showLoadingScreenPopup();
				imgCNT = 0;
				try{
					kony.timer.cancel("imagePicTime");
				}catch(err ){}
				kony.timer.schedule("imagePicTime",imagePicCallback ,5, true);
		
	}
  }catch(e){
	 kony.print("Exception in uploadFile method>>>"+e);
  }	  
}
        
function imagePicCallback()
{
  try{
	  kony.print("image pic call back fun");
	  var browserisIE = "";
	  if(kony.appinit.isIE9 || kony.appinit.isIE8)	
		browserisIE = "true";
	  imgCNT++;
	  var iframe = document.getElementById('iframe_outs');
	  kony.print("iframe>>"+iframe);
	  if(imgCNT == 18)
	  {
		  try{
			kony.timer.cancel("imagePicTime");
		  }catch(err ){}
		  imgCNT = 0;
		  showAlert(kony.i18n.getLocalizedString("keyImageSizeTooLarge"), "");
		  if(kony.application.getCurrentForm().id == "frmIBMyReceipentsAddContactManually"){
		  kony.print("If part of fail fun");
		  uploadrecipinetpictimerCallbackfail();
		  }
		  else{
		  kony.print("Else part of fail fun");	  
		  uploadprofpictimerCallbackfail();
		  }
	  }
	  if(browserisIE != "true" && iframe.contentDocument.document != null && iframe.contentDocument.document !=""  && iframe.contentDocument.document !=undefined)
	  {
		var temp = iframe.contentDocument.document.body.innerHTML;flag = 1;
	  }
	  else if(browserisIE != "true" && iframe.contentWindow.document != null && iframe.contentWindow.document !=""  && iframe.contentWindow.document !=undefined)
	  {
		var temp = iframe.contentWindow.document.body.innerHTML;flag = 1;
	  }
	  else if(browserisIE == "true" && iframe_outs.window.document != null && iframe_outs.window.document!="" && iframe_outs.window.document != undefined)
	  {
		 if(iframe_outs.window.document.body != null && iframe_outs.window.document.body != "" && iframe_outs.window.document.body != undefined)
		 {
			if(iframe_outs.window.document.body.innerHTML != null && iframe_outs.window.document.body.innerHTML != "" && iframe_outs.window.document.body.innerHTML != undefined)
			{
				var temp = iframe_outs.window.document.body.innerHTML;
				flag = 1;
			}
		 }	
	  }
	  kony.print("flag>>"+flag);
	  if(flag > 0)
	  {
		if(temp.search("Uploaded Filename")>= 0) 
		{
		 kony.print("inside Uploaded Filename");
		  imgCNT = 0;flag = 0;
		  try{
				kony.timer.cancel("imagePicTime");
			 }catch(err ){}
			 
			  kony.print("Form ID>>"+kony.application.getCurrentForm().id);
			  if(kony.application.getCurrentForm().id == "frmIBMyReceipentsAddContactManually"){
				   try{
						kony.timer.cancel("upload1");
					}
						catch(e){
					}
					showLoadingScreenPopup();
					kony.print("After upload 1 cancel timer");
					kony.timer.schedule("upload1",uploadRecipienttimerCallback , 10, false);
			   
			   }
			   else{
						  try{
								kony.timer.cancel("uploadprofilepic");
							 }catch(err ){}
						  frmIBCMEditMyProfile.imgprofpic.setVisibility(false);
						  frmIBCMEditMyProfile.cimgprofpic.setVisibility(false);   
						  kony.timer.schedule("uploadprofilepic",uploadprofpictimerCallback ,10, false);
				   }     
		}
		else if(temp.search("Upload")>= 0)
		{
		  kony.print("inside Uploaded");
		  imgCNT = 0; flag = 0;
		  showAlert(kony.i18n.getLocalizedString("keyImageSizeTooLarge"), ""); 
		  try{
				kony.timer.cancel("imagePicTime");
			 }catch(err ){}
		  if(kony.application.getCurrentForm().id == "frmIBMyReceipentsAddContactManually"){
			 uploadrecipinetpictimerCallbackfail();
		  }
		  else{
			 uploadprofpictimerCallbackfail();
		  }
		 
		}
	  }
	  
}catch(e){
	kony.print("Exception in imagePicCallback>>"+e);
}
  
}

function uploadProgress(evt) {
  try{	
	kony.print("uploadProgress>>"+evt)
	if (evt.lengthComputable) {
		var percentComplete = Math.round(evt.loaded * 100 / evt.total);
		document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
		document.getElementById('prog').value = percentComplete;
	}
	else {
		document.getElementById('progressNumber').innerHTML = 'unable to compute';
	}
  }catch(e){
	kony.print("Exception in uploadProgress>>"+e);
  }  
}

function uploadComplete(evt) {
	try{
	/* This event is raised when the server send back a response */
	kony.print("uploadComplete>>"+evt)
	gblRcBase64List = evt.target.responseText;
	 if(gblAdd_Receipent_State == gblEXISTING_RC_EDITION){
		startImageServiceCallRecipientsIB(null);
	}else if(gblAdd_Receipent_State == gblNEW_RC_ADDITION || gblAdd_Receipent_State == gblNEW_RC_ADDITION_PRECONFIRM){
		// Dont call upload service here
		frmIBMyReceipentsAddContactManually.image210107168135.src = gblRcBase64List;
	}
	}catch(e){
		kony.print("Exception in uploadComplete>>"+e);
	}		
}

function uploadFailed(evt) {
	alert("There was an error attempting to upload the file.");
}

function uploadCanceled(evt) {
	alert("The upload has been canceled by the user or the browser dropped the connection.");
}

		
	