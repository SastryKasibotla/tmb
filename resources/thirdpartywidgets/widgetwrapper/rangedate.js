rangedate = {
    		
    initializeWidget : function(parentNode, widgetModel)
    {	
    	var dateSlider = '<link href="desktopweb/jslib/tparty/libraries/TooltipSlider/slider.css" rel="stylesheet" type="text/css">'+
		  	'<style type="text/css">'+
			'</style>'+
			'<div id="slider1"></div>';
	
		parentNode.innerHTML = dateSlider;
		function addMonths(date, months) {
		 	date.setMonth(date.getMonth() + months);
		 	return date;
		}
		var initial_date = addMonths(new Date(), -6);
		var start_month= initial_date.getMonth();
		var start_year = initial_date.getFullYear();
		var present_month = new Date().getMonth();
		var minDate = new Date(start_year,start_month,1);
		var maxDate = new Date(new Date().getFullYear(),new Date().getMonth()+1,0);
		var selectedMinDate = new Date(new Date().getFullYear(),new Date().getMonth(),1);
		if(kony.application.getCurrentForm().id=="frmIBMFAcctFullStatement")
		{
			if(gblTxnHistDate1!=null && gblTxnHistDate1!="")
			{
				selectedMinDate = gblTxnHistDate1;
			}
			if(gblTxnHistDate2!=null && gblTxnHistDate2!="")
			{
				selectedMaxDate = gblTxnHistDate2;
			}
			else
			{
				selectedMaxDate = new Date();
			}
			widgetModel["selectedDate1"] = selectedMinDate;
			widgetModel["selectedDate2"] = selectedMaxDate;
			gblTxnHistDate1 = selectedMinDate ;
	  		gblTxnHistDate2 = selectedMaxDate ;
		}
		else
		{
			widgetModel["selectedDate1"] = selectedMinDate;
			widgetModel["selectedDate2"] = new Date();
			gblTxnHistDate1 = selectedMinDate ;
	  		gblTxnHistDate2 = new Date() ;
		}
		
	$(document).ready(function(){

  

	$("#slider1").dateRangeSlider({
    range: true,
    bounds: {min:minDate, max:maxDate},
    defaultValues: {min: selectedMinDate, max: gblTxnHistDate2}
 
	});
	
	
	
	$("#slider1").on("valuesChanged", function (e, data) {
   console.log("date "+ new Date ());
  
		widgetModel["selectedDate1"] = data.values.min;
		widgetModel["selectedDate2"] = data.values.max;
		gblTxnHistDate1 = data.values.min;
		gblTxnHistDate2 = data.values.max;
            if (data.values.max >new Date ()) {
					 $("#slider1").dateRangeSlider(	"max", new Date());
				}
				if (data.values.min != data.values.max) {
					console.log("equallllllllllllllllll");
				}
		    });
		});
	},
	
	modelChange : function(widgetModel, propertyChanged, propertyValue){}
 }
