function p2kwiet2012247643978_segAccountDetails_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    callMutualFundsDetails.call(this, frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].lblunitHolderNumber, frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].fundCode, null);
    if (frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].lblfundName != undefined && frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].lblfundName != null) {
        frmIBMutualFundsSummary.lblAccountBalanceHeader.text = frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].lblinvestmentValue;
        frmIBMutualFundsSummary.lblAccountNameHeader.text = frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].lblfundName;
        frmIBMutualFundsSummary.imgAccountDetailsPic.src = frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].imgLogo;
        gblSelFundNickNameTH = frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].lblfundNickNameTH;
        gblSelFundNickNameEN = frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].lblfundNickNameEN;
        gblFundShort = frmIBMutualFundsSummary.segAccountDetails.selectedItems[0].fundShortName;
    }
}