function p2kwiet2012247651174_btnThai_onClick_seq0(eventobject) {
    langchange.call(this, "th_TH");
    hbxIBPreLogin.btnEng.skin = "btnEngLangOff";
    hbxIBPreLogin.btnThai.skin = "btnThaiLangOn";
    hbxIBPostLogin.btnEng.skin = "btnEngLangOff";
    hbxIBPostLogin.btnThai.skin = "btnThaiLangOn";
}