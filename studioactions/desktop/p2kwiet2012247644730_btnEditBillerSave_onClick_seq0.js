function p2kwiet2012247644730_btnEditBillerSave_onClick_seq0(eventobject) {
    function alert_onClick_63617201224765265_True() {}
    if ((checkDuplicateNicknameOnEditBillerIB())) {
        editBillerNickNameValidationIB.call(this);
    } else {
        function alert_onClick_63617201224765265_Callback() {
            alert_onClick_63617201224765265_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": kony.i18n.getLocalizedString("Valid_DuplicateNickname"),
            "alertHandler": alert_onClick_63617201224765265_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
}