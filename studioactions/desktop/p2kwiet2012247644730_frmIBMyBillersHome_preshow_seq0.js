function p2kwiet2012247644730_frmIBMyBillersHome_preshow_seq0(eventobject, neworientation) {
    frmIBMyBillersHome.segBillersList.setVisibility(true);
    frmIBMyBillersHome.lblMyBills.setVisibility(true);
    frmIBMyBillersHome.hbxMore.setVisibility(true);
    frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
    isSelectBillerEnabled = false;
    frmIBMyBillersHome.tbxSearch.text = "";
    frmIBMyBillersHome.lblOTPinCurr.text = "";
    frmIBMyBillersHome.lblPlsReEnter.text = "";
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (!isIE8) {
        frmIBMyBillersHome.tbxSearch.placeholder = kony.i18n.getLocalizedString("Receipent_Search");
    }
    gblTopUpMore = 0;
    gblCustTopUpMore = 0;
    search = "";
    frmIBMyBillersHome.lblNoBills.setVisibility(false);
    frmIBMyBillersHome.lblErrorMsg.setVisibility(false);
    frmIBMyBillersHome.hbxTokenEntry.setVisibility(false);
    gblRetryCountRequestOTP = 0;
    frmIBMyBillersHome.imgTMBLogo.setVisibility(true);
    //frmIBMyBillersHome.hbxTMBLogo.setVisibility(true);
    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyBillersHome.hbxSugBillersContainer.setVisibility(false);
    frmIBMyBillersHome.lblMyBills.setVisibility(false);
    frmIBMyBillersHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyBillersHome.imgArrowAddBiller.setVisibility(false);
    frmIBMyBillersHome.imgArrowSegBiller.setVisibility(false);
    frmIBMyBillersHome.txtotp.maxTextLength = 6;
    frmIBMyBillersHome.tbxSearch.text = "";
    getLocaleBillerIB.call(this);
}