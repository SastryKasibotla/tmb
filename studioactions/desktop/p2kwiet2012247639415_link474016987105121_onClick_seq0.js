function p2kwiet2012247639415_link474016987105121_onClick_seq0(eventobject) {
    if (frmIBBeepAndBillConfAndComp.lblBalanceValue.isVisible) {
        frmIBBeepAndBillConfAndComp.lblBalanceValue.setVisibility(false);
        frmIBBeepAndBillConfAndComp.lblBalance.setVisibility(false);
    } else {
        frmIBBeepAndBillConfAndComp.lblBalanceValue.setVisibility(true);
        frmIBBeepAndBillConfAndComp.lblBalance.setVisibility(true);
    }
    if (frmIBBeepAndBillConfAndComp.link474016987105121.text == kony.i18n.getLocalizedString("keyUnhide")) {
        frmIBBeepAndBillConfAndComp.link474016987105121.text = kony.i18n.getLocalizedString("Hide");
    } else {
        frmIBBeepAndBillConfAndComp.link474016987105121.text = kony.i18n.getLocalizedString("keyUnhide");
    }
}