function p2kwiet2012247638599_frmIBActivateIBankingStep2Confirm_postshow_seq0(eventobject, neworientation) {
    frmIBActivateIBankingStep2Confirm.txtOTP.setFocus(true);
    var isIE8 = window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8";
    if (isIE8) {
        document.getElementById("frmIBActivateIBankingStep2Confirm_label507353026211739").style.paddingBottom = "0px";
    }
}