function p2kwiet2012247643366_vbox4740477151923676_onClick_seq0(eventobject) {
    if (frmIBExecutedTransaction.lblBBPaymentValue.isVisible) {
        frmIBExecutedTransaction.lblBBPaymentValue.setVisibility(false);
        frmIBExecutedTransaction.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBExecutedTransaction.lblBBPaymentValue.setVisibility(true);
        frmIBExecutedTransaction.lblHide.text = kony.i18n.getLocalizedString("Hide");
    }
}