function p2kwiet2012247650889_button44747680938122_onClick_seq0(eventobject) {
    deleteReceipentAccount.call(this, gblSelectedAccountCode, frmIBMyReceipentsAccounts.lblNumberAccntDetails.text, frmIBMyReceipentsAccounts.lblNickNameAccntDetails.text);
    frmIBMyReceipentsAccounts.hboxMyReceipents.setVisibility(true);
    frmIBMyReceipentsAccounts.hboxReceipentAccount.setVisibility(false);
    //frmIBMyReceipentsAccounts.hboxTMBWelcome.setVisibility(false);
    popupIBMyReceipentsAccntDelete.dismiss();
}