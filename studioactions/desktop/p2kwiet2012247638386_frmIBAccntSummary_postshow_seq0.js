function p2kwiet2012247638386_frmIBAccntSummary_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    syncIBMyAccountSummary();
    if (kony.i18n.getCurrentLocale() == "th_TH") frmIBAccntSummary.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocusThai";
    else frmIBAccntSummary.btnMenuMyAccountSummary.skin = "btnIBMenuMyAccountSummaryFocus";
    document.getElementById("frmIBAccntSummary_image25028248841260094_span").className = "imgRed";
    frmIBAccntSummary.hbxAccountDetails.skin = "hbxRed";
    gblSliderMonth = 6;
    gblSelTransferMode = 1;
    gblDreamAcountCheck = "true";
    frmIBAccntSummary.lnkFullStatement.focusSkin = "linkFullStatFoc";
    frmIBAccntSummary.lnkFullStatement.hoverSkin = "linkFullStatFoc";
    frmIBAccntSummary.lnkMyActivities.focusSkin = "linkMyActivitiesIconFoc";
    frmIBAccntSummary.lnkMyActivities.hoverSkin = "linkMyActivitiesIconFoc";
    swapSelectedIBAcctSkin.call(this);
    OTPcheckOnAccountSummary.call(this);
}