function p2kwiet2012247651202_btnEng_onClick_seq0(eventobject) {
    langchange.call(this, "en_US");
    hbxIBPreLogin.btnEng.skin = "btnEngLangOn";
    hbxIBPreLogin.btnThai.skin = "btnThaiLangOff";
    hbxIBPostLogin.btnEng.skin = "btnEngLangOn";
    hbxIBPostLogin.btnThai.skin = "btnThaiLangOff";
    if (!kony.appinit.isIE8) {
        frmIBPreLogin.txtUserId.placeholder = kony.i18n.getLocalizedString("keyUserIDPreLogin");
        frmIBPreLogin.txtPassword.placeholder = kony.i18n.getLocalizedString("keyPasswordPlaceHolder");
    }
}