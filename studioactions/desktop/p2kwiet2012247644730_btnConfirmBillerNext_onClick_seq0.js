function p2kwiet2012247644730_btnConfirmBillerNext_onClick_seq0(eventobject) {
    frmIBMyBillersHome.txtotp.text = "";
    frmIBMyBillersHome.imgTMBLogo.setVisibility(false);
    frmIBMyBillersHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersConfirmContainer.setVisibility(true);
    frmIBMyBillersHome.hbxOtpBox.setVisibility(true)
    frmIBMyBillersHome.hbxCanConfBtnContainer.setVisibility(true);
    frmIBMyBillersHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyBillersHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyBillersHome.btnConfirmBillerNext.setVisibility(false);
    onAddBillTopUpNextIB.call(this);
}