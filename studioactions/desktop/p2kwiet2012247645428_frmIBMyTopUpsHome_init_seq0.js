function p2kwiet2012247645428_frmIBMyTopUpsHome_init_seq0(eventobject, neworientation) {
    frmIBMyTopUpsHome.imgTMBLogo.setVisibility(true);
    frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
    frmIBMyTopUpsHome.hbxSugBillersContainer.setVisibility(false);
    frmIBMyTopUpsHome.lblMyBills.setVisibility(false);
    frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
    frmIBMyTopUpsHome.txtotp.maxTextLength = 6;
    frmIBMyTopUpsHome.lblMyBills.margin = [0, 0, 0, 0];
    getLocaleBillerIB.call(this);
}