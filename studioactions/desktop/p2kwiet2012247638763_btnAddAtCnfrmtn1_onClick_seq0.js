function p2kwiet2012247638763_btnAddAtCnfrmtn1_onClick_seq0(eventobject) {
    AccntNum = gblMyAccntAddTmpData.length;
    kony.print("AccntNum Length --> " + AccntNum);
    kony.print("gblMyAccntAddTmpData.length--> " + gblMyAccntAddTmpData.length);
    if ((gblMyAccntAddTmpData.length < MAX_ACC_ADD)) {
        frmIBMyAccnts.hbxTMBImg.setVisibility(false);
        //frmIBMyAccnts.imgArrow.setVisibility(true);
        frmIBMyAccnts.hboxAddNewAccnt.setVisibility(true);
        frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
        frmIBMyAccnts.lblCreditCardNum.setVisibility(false);
        frmIBMyAccnts.lblsuffix.setVisibility(false);
        frmIBMyAccnts.txtbxSuffix.setVisibility(false);
        frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
        frmIBMyAccnts.hbxAccntNum.setVisibility(true);
        frmIBMyAccnts.lblAccntNum.setVisibility(true);
        frmIBMyAccnts.txtbxOtherAccnt.setVisibility(false);
        frmIBMyAccnts.lineComboAccntype.setVisibility(false);
        frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
        frmIBMyAccnts.hbxAccountName.setVisibility(false);
        frmIBMyAccnts.line363582120273807.setVisibility(false);
        frmIBMyAccnts.txtAccNum1.text = ""
        frmIBMyAccnts.txtAccNum2.text = ""
        frmIBMyAccnts.txtAccNum3.text = ""
        frmIBMyAccnts.txtAccNum4.text = ""
        frmIBMyAccnts.txtNickNAme.text = ""
        frmIBMyAccnts.tbxAccountName.text = "";
        frmIBMyAccnts.show();
    } else {
        showAlert(kony.i18n.getLocalizedString("keyErrMaxAccntAdd"), kony.i18n.getLocalizedString("info"));
    }
}