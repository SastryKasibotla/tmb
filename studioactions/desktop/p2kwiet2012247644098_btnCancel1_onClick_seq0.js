function p2kwiet2012247644098_btnCancel1_onClick_seq0(eventobject) {
    if (frmIBAddMyAccnt.segAccntDetails.data == null || frmIBAddMyAccnt.segAccntDetails.data == undefined || frmIBAddMyAccnt.segAccntDetails.data.length < 1) {
        frmIBMyAccnts.hbxTMBImg.setVisibility(true);
        frmIBMyAccnts.hboxAddNewAccnt.setVisibility(false);
        frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
        frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
        frmIBMyAccnts.lineComboAccntype.setVisibility(false);
        frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);
        frmIBMyAccnts.cmbobxBankType.selectedKey = "key1";
        frmIBMyAccnts.cmbobxBankType.setVisibility(true);
        gblMyAccntAddTmpData.length = 0;
        NON_TMB_ADD = 0;
        frmIBMyAccnts.show();
    } else {
        frmIBAddMyAccnt.show();
    }
}