function p2kwiet2012247644864_btnAddAccount_onClick_seq0(eventobject) {
    if (getCRMLockStatus()) {
        curr_form = kony.application.getCurrentForm().id;
        popIBBPOTPLocked.show();
    } else {
        if (frmIBMyReceipentsAccounts.segmentRcAccounts.data != null && frmIBMyReceipentsAccounts.segmentRcAccounts.data.length > 0) {
            if (frmIBMyReceipentsAccounts.segmentRcAccounts.data.length >= MAX_BANK_ACCNT_PER_RC) {
                alert(kony.i18n.getLocalizedString("J200000003"));
            } else {
                gblAdd_Receipent_State = gblEXISTING_RC_BANKACCNT_ADDITION;
                gblRetryCountRequestOTP = 0;
                frmIBMyReceipentsAddBankAccnt.show();
            }
        } else {
            gblAdd_Receipent_State = gblEXISTING_RC_BANKACCNT_ADDITION;
            gblRetryCountRequestOTP = 0;
            frmIBMyReceipentsAddBankAccnt.show();
        }
    }
}