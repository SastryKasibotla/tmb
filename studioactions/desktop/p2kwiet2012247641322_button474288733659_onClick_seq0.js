function p2kwiet2012247641322_button474288733659_onClick_seq0(eventobject) {
    editbuttonflag = "password";
    gblShowPwd = 1;
    frmIBCMChgPwd.tbxTranscCrntPwd.text = ""
    frmIBCMChgPwd.txtPassword.text = ""
    frmIBCMChgPwd.txtRetypePwd.text = ""
    frmIBCMChgPwd.tbxTranscCrntPwd.setFocus(true);
    frmIBCMChgPwd.hbxRequest.setVisibility(false);
    frmIBCMChgPwd.lnkHeader.setVisibility(true);
    frmIBCMChgPwd.lblConfirmation.setVisibility(true);
    frmIBCMChgPwd.hbox47428873359193.setVisibility(true);
    frmIBCMChgPwd.hbxCancelSaveOld.setVisibility(true);
    frmIBCMChgPwd.ArrowEditProf.setVisibility(true);
    frmIBCMChgPwd.arrowrequest.setVisibility(false);
    getIBEditProfileStatus.call(this);
}