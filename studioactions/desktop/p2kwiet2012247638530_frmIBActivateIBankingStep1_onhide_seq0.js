function p2kwiet2012247638530_frmIBActivateIBankingStep1_onhide_seq0(eventobject, neworientation) {
    frmIBActivateIBankingStep1.txtAccountNumber.text = "";
    frmIBActivateIBankingStep1.txtActivationCode.text = "";
    frmIBActivateIBankingStep1.txtIDPass.text = "";
    frmIBActivateIBankingStep1.vbxRules.skin = "vbxBGGrey"
    frmIBActivateIBankingStep1.imgarrow1.setVisibility(false);
    frmIBActivateIBankingStep1.hbxPassRules.setVisibility(false);
    frmIBActivateIBankingStep1.vbxRules.setVisibility(false);
}