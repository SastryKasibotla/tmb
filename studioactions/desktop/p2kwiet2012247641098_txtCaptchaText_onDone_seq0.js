function p2kwiet2012247641098_txtCaptchaText_onDone_seq0(eventobject, changedtext) {
    function alert_onDone_35575201224764351_True() {}

    function alert_onDone_56314201224762306_True() {}

    function alert_onDone_4262020122476476_True() {}
    if ((frmIBPreLogin.txtUserId.text != "")) {
        if ((frmIBPreLogin.txtPassword.text != "")) {
            if ((frmIBPreLogin.hboxCaptchaText.isVisible)) {
                if ((frmIBPreLogin.txtCaptchaText.text != "")) {
                    captchaValidation.call(this, frmIBPreLogin.txtCaptchaText.text);
                } else {
                    function alert_onDone_35575201224764351_Callback() {
                        alert_onDone_35575201224764351_True();
                    }
                    kony.ui.Alert({
                        "alertType": constants.ALERT_TYPE_ERROR,
                        "alertTitle": "Alert",
                        "yesLabel": "Yes",
                        "noLabel": "No",
                        "message": "Field is mandatory",
                        "alertHandler": alert_onDone_35575201224764351_Callback
                    }, {
                        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                    });
                    frmIBPreLogin.txtCaptchaText.setFocus(true);
                }
            } else {
                IBLoginService.call(this);
            }
        } else {
            function alert_onDone_56314201224762306_Callback() {
                alert_onDone_56314201224762306_True();
            }
            kony.ui.Alert({
                "alertType": constants.ALERT_TYPE_ERROR,
                "alertTitle": "Alert",
                "yesLabel": "Yes",
                "noLabel": "No",
                "message": "Field is mandatory",
                "alertHandler": alert_onDone_56314201224762306_Callback
            }, {
                "iconPosition": constants.ALERT_ICON_POSITION_LEFT
            });
            frmIBPreLogin.txtPassword.setFocus(true);
        }
    } else {
        function alert_onDone_4262020122476476_Callback() {
            alert_onDone_4262020122476476_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "Alert",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": "Field is mandatory",
            "alertHandler": alert_onDone_4262020122476476_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
        frmIBPreLogin.txtUserId.setFocus(true);
    }
}