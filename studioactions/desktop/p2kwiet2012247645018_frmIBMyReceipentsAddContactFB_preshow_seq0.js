function p2kwiet2012247645018_frmIBMyReceipentsAddContactFB_preshow_seq0(eventobject, neworientation) {
    frmIBMyReceipentsAddContactFB.hbxOtpBox.setVisibility(true);
    frmIBMyReceipentsAddContactFB.lblBankRef.setVisibility(false);
    frmIBMyReceipentsAddContactFB.label476047582127701.setVisibility(false);
    frmIBMyReceipentsAddContactFB.label476047582115277.setVisibility(false);
    frmIBMyReceipentsAddContactFB.label476047582115279.setVisibility(false);
    frmIBMyReceipentsAddContactFB.textbox247428873338513.text = "";
    frmIBMyReceipentsAddContactFB.lblOTPinCurr.text = "";
    frmIBMyReceipentsAddContactFB.lblPlsReEnter.text = "";
    validateSecurityForTransactionServiceRc();
    if (gblfbSelectionState != gblFB_SINGLE_SELECTION) {
        gblAdd_Receipent_State = 0;
    }
    frmIBMyReceipentsAddContactFB.hbxOtpBox.setVisibility(false);
}