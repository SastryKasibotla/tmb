function p2kwiet2012247641211_button474288733664_onClick_seq0(eventobject) {
    editbuttonflag = "limit"
    flagMobNum = "new";
    gblMobNoTransLimitFlag = false;
    frmIBCMChgMobNoTxnLimit.hbxRequest.setVisibility(false);
    frmIBCMChgMobNoTxnLimit.lblHead.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.hbxChangeTransactionLimit.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.hbox47505957819121.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.arrowlimit.setVisibility(true);
    frmIBCMChgMobNoTxnLimit.arrowrequest.setVisibility(false);
    getIBEditProfileStatus.call(this);
}