function p2kwiet2012247645740_txtODNickNameVal_DESKTOPWEB_onBeginEditing_seq0(eventobject, changedtext) {
    frmIBOpenNewDreamAcc.vbxBPRight.hbxImage.setVisibility(false);
    frmIBOpenNewDreamAcc.vbxBPRight.hbxBuildMyDream.setVisibility(false);
    frmIBOpenNewDreamAcc.hbxCoverflow.setVisibility(true);
    frmIBOpenNewDreamAcc.txtODMnthSavAmt.setEnabled(true);
    frmIBOpenNewDreamAcc.txtODMyDream.setEnabled(true);
    frmIBOpenNewDreamAcc.hbxPictureIt.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hbxNickVal.skin = "hbxProperFocus"
    frmIBOpenNewDreamAcc.hboxTarget.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthlySaving.skin = "hbxProper"
    frmIBOpenNewDreamAcc.hboxMonthsToDream.skin = "hbxProper"
    frmIBOpenNewDreamAcc.vbxBPRight.skin = "vbxRightColumnGrey"
    frmIBOpenNewDreamAcc.vbxBPMiddleRight.skin = "vbxMidRight689px"
    frmIBOpenNewDreamAcc.image24573708914676.setVisibility(false);
    frmIBOpenNewDreamAcc.image2867539252331117.setVisibility(true);
}