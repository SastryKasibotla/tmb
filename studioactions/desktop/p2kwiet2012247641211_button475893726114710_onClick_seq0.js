function p2kwiet2012247641211_button475893726114710_onClick_seq0(eventobject) {
    editbuttonflag = "userid";
    gblCMUserIDRule = 1;
    frmIBCMChngUserID.txtCurUserID.text = ""
    frmIBCMChngUserID.txtNewUserID.text = ""
    frmIBCMChngUserID.txtCurUserID.setFocus(true);
    frmIBCMChngUserID.hbxRequest.setVisibility(false);
    frmIBCMChngUserID.lblChngUID.setVisibility(true);
    frmIBCMChngUserID.hbox47428873349614.setVisibility(true);
    frmIBCMChngUserID.hbxCancelSave.setVisibility(true);
    frmIBCMChngUserID.arrowUserId.setVisibility(true);
    frmIBCMChngUserID.arrowrequest.setVisibility(false);
    getIBEditProfileStatus.call(this);
}