function p2kwiet2012247644952_segmentReceipentListing_onRowClick_seq0(eventobject, sectionNumber, rowNumber) {
    //Store the selected Rc id
    gblselectedRcId = frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["receipentID"];
    if (checkActualRcSegItemIndex()) {
        //Index done in method
    } else {
        globalSeletedRcIndex = frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedIndex[1];
    }
    showLoadingScreenPopup();
    frmIBMyReceipentsAccounts.show();
    frmIBMyReceipentsAccounts.hboxMyReceipents.setVisibility(true);
    frmIBMyReceipentsAccounts.hboxReceipentAccount.setVisibility(false);
    //frmIBMyReceipentsAccounts.hboxTMBWelcome.setVisibility(false);
    //Code to set selected item name
    if (globalRcData[0][globalSeletedRcIndex].lblReceipentName == null) {
        frmIBMyReceipentsAccounts.lblRcName.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcName.text = globalRcData[0][globalSeletedRcIndex].lblReceipentName;
    }
    if (frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["mobileNumber"] == null || frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["mobileNumber"] == "No Number") {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcMobileNo.text = frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["mobileNumber"];
    }
    if (frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["emailId"] == null || frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["emailId"] == "No Mail") {
        frmIBMyReceipentsAccounts.lblRcEmail.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcEmail.text = frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["emailId"];
    }
    if (frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["facebookId"] == null) {
        frmIBMyReceipentsAccounts.lblRcFbId.text = "";
    } else {
        frmIBMyReceipentsAccounts.lblRcFbId.text = frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["facebookId"];
    }
    if (frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["imgReceipentPic"] == null) {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = "nouserimg.jpg";
    } else {
        frmIBMyReceipentsAccounts.imgReceipentProfile.src = frmIBMyReceipentsAddBankAccnt.segmentReceipentListing.selectedItems[0]["imgReceipentPic"];
    }
}