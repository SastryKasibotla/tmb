function p2kwiet2012247647593_btnNext_onClick_seq0(eventobject) {
    // start of on click of next button of frmIBSSApply
    var selectedItem = frmIBSSApply.customFromAccountIB.selectedItem;
    var selectedData = frmIBSSApply.customFromAccountIB.data[gblCWSelectedItem];
    var acctVal = frmIBSSApply.lblAcctNoValue.text
    kony.print("selectedItem" + selectedItem)
    if (acctVal == null || acctVal == "") {
        alert(kony.i18n.getLocalizedString("keyIBTransferFromSelect"))
        return false
    }
    var maxAmount = frmIBSSApply.txtBalMax.text;
    maxAmount = maxAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim()
    var minAmount = frmIBSSApply.txtBalMin.text;
    minAmount = minAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim()
    kony.print(" --maxAmount---: " + maxAmount + " ---minAmount---: " + minAmount);
    var lmtValid = validAmntLmtIB(maxAmount, minAmount);
    if (lmtValid) {
        frmIBSSApplyCnfrmtn.lblAmntLmtMax.text = frmIBSSApply.txtBalMax.text; //commaFormatted(maxAmount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
        frmIBSSApplyCnfrmtn.lblAmntLmtMin.text = frmIBSSApply.txtBalMin.text //commaFormatted(minAmount)+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
        if (gbls2sEditFlag != "confirmationEdit") {
            callApplyS2SIBActivityLog();
        }
        s2sSegmentSliderMappingForIBConfirm();
        s2sNoFixedAcctMappingForIBConfirm();
        saveInputinSessionIB();
        frmIBSSApply.hboxLinkedAcct.setVisibility(false);
    }
}