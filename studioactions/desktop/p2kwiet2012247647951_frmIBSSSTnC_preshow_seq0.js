function p2kwiet2012247647951_frmIBSSSTnC_preshow_seq0(eventobject, neworientation) {
    //frmIBSSSTnC.lblViewNickNameHdr.text = kony.i18n.getLocalizedString("applySSSrvce");
    //frmIBSSSTnC.lblSSSIntroData.text = kony.i18n.getLocalizedString("S2S_ApplyContent");
    //frmIBSSSTnC.lblSSSTnC.text = "TMB has to provide exact TnC for S2S";
    getMinMaxAmountsForIB();
    kony.print("gblNFOpnStats.. " + (gblNFOpnStats == "false"));
    kony.print("gblNFOpnStats without quotes.. " + (gblNFOpnStats == false));
    kony.print("gblNFHidden.. " + (gblNFHidden == "false"));
    kony.print("gblNFHidden without quotes.. " + (gblNFHidden == false));
    if (gblNFOpnStats == "false") {
        kony.print("inside if condition of gblNFOpnStats false.. ");
        frmIBSSSTnC.hbxOpenNoFixed.setVisibility(true);
        frmIBSSSTnC.buttonApplyNow.setVisibility(false);
    } else {
        frmIBSSSTnC.hbxOpenNoFixed.setVisibility(false);
        frmIBSSSTnC.buttonApplyNow.setVisibility(true);
    }
    frmIBSSSTnC.hboxTncContent.setVisibility(false);
}