function p2kwiet2012247648602_vbox4740477151928985_onClick_seq0(eventobject) {
    if (frmIBTopUpComplete.lblBBPaymentValue.isVisible) {
        frmIBTopUpComplete.lblBBPaymentValue.setVisibility(false);
        frmIBTopUpComplete.lblBalance.setVisibility(false);
        frmIBTopUpComplete.lblHide.text = kony.i18n.getLocalizedString("keyUnhide");
    } else {
        frmIBTopUpComplete.lblBBPaymentValue.setVisibility(true);
        frmIBTopUpComplete.lblBalance.setVisibility(true);
        frmIBTopUpComplete.lblHide.text = kony.i18n.getLocalizedString("keyHideIB");
    }
}