function p2kwiet2012247644098_frmIBMyAccnts_preshow_seq0(eventobject, neworientation) {
    document.oncontextmenu = function() {
        return false;
    }
    gblAccountSummaryFlag = "false";
    gblEditClickFromAccountDetails = false; //Used for Saving Care Edit
    frmIBMyAccnts.lnkDreamSaving.isVisible = false;
    //Initializing global variable used to keep track of max accnt. len of that bank
    MAX_ACC_LEN = 0;
    // Is selected bank ORFT bank
    IS_ORFT = "Y";
    gblEditClickFromAccountDetails = false;
    frmIBMyAccnts.txtAccNum1.maxTextLength = 3;
    frmIBMyAccnts.txtAccNum2.maxTextLength = 1;
    frmIBMyAccnts.txtAccNum3.maxTextLength = 5;
    frmIBMyAccnts.txtAccNum4.maxTextLength = 1;
    frmIBMyAccnts.lblTmbHeader.text = kony.i18n.getLocalizedString("keylblTMBBank");
    frmIBMyAccnts.lblOtherBankHdr.text = kony.i18n.getLocalizedString("keylblOtherBankAccount");
    frmIBMyAccnts.lblViewAccntName.text = kony.i18n.getLocalizedString("AccountName");
    frmIBMyAccnts.lblViewAccntNum.text = kony.i18n.getLocalizedString("keyAccountNoIB");
    frmIBMyAccnts.lblAccntNum.text = kony.i18n.getLocalizedString("AccountNumber") + ":";
    frmIBMyAccnts.lblNickName.text = kony.i18n.getLocalizedString("keyAddAccountNickname");
}