function p2kwiet2012247641322_button478902715248432_onClick_seq0(eventobject) {
    frmIBCMConfirmationPwd.hbxTokenEntry.setVisibility(false);
    frmIBCMConfirmationPwd.hbxOTPEntry.setVisibility(true);
    frmIBCMConfirmationPwd.label476047582115277.setVisibility(true);
    frmIBCMConfirmationPwd.lblPhoneNo.setVisibility(true);
    frmIBCMConfirmationPwd.lblBankRef.setVisibility(true);
    frmIBCMConfirmationPwd.label476047582115288.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBCMConfirmationPwd.txtOTP.setFocus(true);
    invokeRequestOtpChngPWD();
    gblTokenSwitchFlag = false
    gblSwitchToken = true
}