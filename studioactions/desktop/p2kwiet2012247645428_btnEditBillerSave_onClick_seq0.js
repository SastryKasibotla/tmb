function p2kwiet2012247645428_btnEditBillerSave_onClick_seq0(eventobject) {
    function alert_onClick_57844201224762231_True() {}
    if ((checkDuplicateNicknameOnEditTopUpIB())) {
        editTopUpNickNameValidationIB.call(this);
    } else {
        function alert_onClick_57844201224762231_Callback() {
            alert_onClick_57844201224762231_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_ERROR,
            "alertTitle": "",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": "\"+kony.i18n.getLocalizedString(\"Valid_DuplicateNickname\")+\"",
            "alertHandler": alert_onClick_57844201224762231_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    }
}