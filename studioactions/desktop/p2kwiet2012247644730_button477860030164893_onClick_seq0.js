function p2kwiet2012247644730_button477860030164893_onClick_seq0(eventobject) {
    frmIBMyBillersHome.hbxTokenEntry.setVisibility(false);
    frmIBMyBillersHome.hbxOTPEntry.setVisibility(true);
    frmIBMyBillersHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    frmIBMyBillersHome.txtotp.setFocus(true);
    startBillTopUpOTPRequestBackground();
}