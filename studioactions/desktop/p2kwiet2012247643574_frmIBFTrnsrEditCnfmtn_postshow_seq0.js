function p2kwiet2012247643574_frmIBFTrnsrEditCnfmtn_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    gblisConfrm = true
        //frmIBFTrnsrEditCnfmtn.hbxMenuMyActivities.skin = "hbxIBMyActivitiesMenuFocus"
    var prevFrm = kony.application.getPreviousForm().id;
    if (prevFrm != "frmIBFBLogin") {
        if (isTMB) {
            frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(true);
            frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(false);
        } else {
            frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(false);
            frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(true);
        }
    }
    pagespecificSubMenu.call(this);
}