function p2kwiet2012247645428_frmIBMyTopUpsHome_postshow_seq0(eventobject, neworientation) {
    addIBMenu.call(this);
    segAboutMeLoad.call(this);
    pagespecificSubMenu.call(this);
    startDisplayTopUpCategoryServiceIB.call(this);
    getMyTopUpListIB.call(this);
    frmIBMyTopUpsHome.lblBankRef.setVisibility(false);
    frmIBMyTopUpsHome.lblBankRefValue.setVisibility(false);
    frmIBMyTopUpsHome.lblOTPMsg.setVisibility(false);
    frmIBMyTopUpsHome.lblOTPMobileNo.setVisibility(false);
    frmIBMyTopUpsHome.txtotp.text = "";
    callTopupServiceFlag = false;
    startIBBillTopUpProfileInqService();
    frmIBMyTopUpsHome.lblMyBills.margin = [0, 0, 0, 0];
    frmIBMyTopUpsHome.txtotp.setFocus(true);
    if ((gblAddBillerFromPay)) {
        frmIBMyTopUpsHome.segBillersConfirm.removeAll();
        frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
        //frmIBMyTopUpsHome.hbxTMBLogo.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyTopUpsHome.hbxBillersConfirmContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersEditContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersViewContainer.setVisibility(false);
        frmIBMyTopUpsHome.hbxCanConfBtnContainer.setVisibility(false);
        frmIBMyTopUpsHome.imgArrowAddBiller.setVisibility(true);
        frmIBMyTopUpsHome.imgArrowSegBiller.setVisibility(false);
        frmIBMyTopUpsHome.hbxBillersCompleteContainer.setVisibility(false);
        frmIBMyTopUpsHome.txtAddBillerNickName.text = "";
        frmIBMyTopUpsHome.txtAddBillerRef1.text = "";
        if (gblAddTopupButtonClicked) {
            frmIBMyTopUpsHome.lblAddBillerName.text = "";
            frmIBMyTopUpsHome.txtAddBillerNickName.setEnabled(false);
            frmIBMyTopUpsHome.txtAddBillerRef1.setEnabled(false);
        }
        //frmIBMyTopUpsHome.imgAddBillerLogo.src="empty.png"
        //frmIBMyTopUpsHome.imgAddBillerLogo.src="";
        //frmIBMyTopUpsHome.lblAddBillerRef1.text=kony.i18n.getLocalizedString("keyRef1");
    } else {}
    if (gblSuggestedBiller && !gblAddBillerFromPay) {
        //alert("inside f")
        frmIBMyTopUpsHome.hbxBillersAddContainer.setVisibility(true);
        frmIBMyTopUpsHome.imgTMBLogo.setVisibility(false);
        frmIBMyTopUpsHome.lblAddBillerName.text = "";
        frmIBMyTopUpsHome.imgAddBillerLogo.src = "";
        frmIBMyTopUpsHome.txtAddBillerNickName.text = "";
        frmIBMyTopUpsHome.txtAddBillerRef1.text = "";
        frmIBMyTopUpsHome.lblMyBills.setVisibility(true);
        frmIBMyTopUpsHome.line588686174252372.setVisibility(false);
    }
    frmIBMyTopUpsHome.tbxSearch.setFocus(true)
}