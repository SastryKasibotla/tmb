function p2kwiet2012247645274_btnAddManually_onClick_seq0(eventobject) {
    if ((getCRMLockStatus())) {
        curr_form = kony.application.getCurrentForm().id;
        popIBBPOTPLocked.show();
    } else {
        gblAdd_Receipent_State = gblNEW_RC_ADDITION;
        clearDataOnRcNewRecipientAdditionForm();
        showLoadingScreenPopup();
        frmIBMyReceipentsAddContactManually.show();
        dismissLoadingScreenPopup();
        TMBUtil.DestroyForm(frmIBMyReceipentsAddBankAccnt);
        TMBUtil.DestroyForm(frmIBMyReceipentsAddContactFB);
        TMBUtil.DestroyForm(frmIBMyReceipentsHome);
        gblAdd_Receipent_State = gblNEW_RC_ADDITION;
    }
}