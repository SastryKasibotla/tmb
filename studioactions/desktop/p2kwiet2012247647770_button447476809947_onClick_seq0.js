function p2kwiet2012247647770_button447476809947_onClick_seq0(eventobject) {
    if (gblNFHidden == "noFixedAccDeleted" || gblSSLinkedStatus == "linkedAccDeleted") {
        showAlert("Please add your deleted Account/s first", kony.i18n.getLocalizedString("info"));
    } else {
        gblSSServieHours = "S2SEdit";
        s2s_ib_activityTypeID = "037";
        s2s_ib_errorCode = "";
        s2s_ib_activityStatus = "00";
        s2s_ib_deviceNickName = "";
        s2s_ib_activityFlexValues1 = gblPHONENUMBER;
        s2s_ib_activityFlexValues2 = ibLinkdAcct;
        s2s_ib_activityFlexValues3 = ibNoFixedAcct;
        //s2s_ib_activityFlexValues4 = ibLiqInqCollectionData["maxAmt"];
        var txtBalMax = frmIBSSSDetails.lblAmntLmtMax.text
        txtBalMax = txtBalMax.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        s2s_ib_activityFlexValues4 = removeCommas(txtBalMax);
        var txtBalMin = frmIBSSSDetails.lblAmntLmtMin.text
        txtBalMin = txtBalMin.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
        s2s_ib_activityFlexValues5 = removeCommas(txtBalMin);
        s2s_ib_logLinkageId = "";
        activityLogServiceCall(s2s_ib_activityTypeID, s2s_ib_errorCode, s2s_ib_activityStatus, s2s_ib_deviceNickName, s2s_ib_activityFlexValues1, s2s_ib_activityFlexValues2, s2s_ib_activityFlexValues3, s2s_ib_activityFlexValues4, s2s_ib_activityFlexValues5, s2s_ib_logLinkageId);
        checkBusinessHoursForIBS2SEditDel();
    }
}