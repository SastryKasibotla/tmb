function p2kwiet2012247645428_button477860030164893_onClick_seq0(eventobject) {
    frmIBMyTopUpsHome.hbxTokenEntry.setVisibility(false);
    frmIBMyTopUpsHome.hbxOTPEntry.setVisibility(true);
    frmIBMyTopUpsHome.lblOTPReqMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
    frmIBMyTopUpsHome.txtotp.setFocus(true);
    gblTokenSwitchFlag = false;
    gblSwitchToken = true;
    startBillTopUpOTPRequestBackground();
}