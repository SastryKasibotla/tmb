function p2kwiet2012247647593_txtBalMax_DESKTOPWEB_onEndEditing_seq0(eventobject, changedtext) {
    if (frmIBSSApply.txtBalMax.text == "") {
        frmIBSSApply.txtBalMax.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");
    } else {
        frmIBSSApply.txtBalMax.text = commaFormatted(parseFloat(removeCommos(frmIBSSApply.txtBalMax.text)).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    }
}