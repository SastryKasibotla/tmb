function p2kwiet2012247644098_btnAddNewAccnt_onClick_seq0(eventobject) {
    if (getCRMLockStatus()) {
        curr_form = kony.application.getCurrentForm().id;
        popIBBPOTPLocked.show();
    } else {
        frmIBMyAccnts.hbxTMBImg.setVisibility(false);
        //frmIBMyAccnts.imgArrow.setVisibility(true);
        frmIBMyAccnts.vbox447417227428995.skin = "vbxRightColumn15px";
        frmIBMyAccnts.hboxAddNewAccnt.setVisibility(true);
        frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
        frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);
        frmIBMyAccnts.lblOtherBankHdr.setVisibility(false);
        if (frmIBMyAccnts.segOtherBankAccntsList.data != null && frmIBMyAccnts.segOtherBankAccntsList.data != undefined && frmIBMyAccnts.segOtherBankAccntsList.data.length > 0) frmIBMyAccnts.lblOtherBankHdr.setVisibility(true);
        gblMyAccntAddTmpData.length = 0;
        frmIBAddMyAccnt.segAccntDetails.removeAll();
        NON_TMB_ADD = 0;
        frmIBMyAccnts.lblCreditCardNum.setVisibility(false);
        frmIBMyAccnts.lblsuffix.setVisibility(false);
        frmIBMyAccnts.txtbxSuffix.setVisibility(false);
        frmIBMyAccnts.hbxCreditCardNum.setVisibility(false);
        frmIBMyAccnts.hbxAccntNum.setVisibility(true);
        frmIBMyAccnts.lblAccntNum.setVisibility(true);
        frmIBMyAccnts.txtbxOtherAccnt.setVisibility(false);
        frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
        frmIBMyAccnts.lineComboAccntype.setVisibility(false);
        //frmIBMyAccnts.arrwImgSeg1.setVisibility(false);
        // frmIBMyAccnts.arrwImgSeg2.setVisibility(false);
        frmIBMyAccnts.hbxAccountName.setVisibility(false);
        frmIBMyAccnts.tbxAccountName.text = "";
        frmIBMyAccnts.txtAccNum1.text = ""
        frmIBMyAccnts.txtAccNum2.text = ""
        frmIBMyAccnts.txtAccNum3.text = ""
        frmIBMyAccnts.txtAccNum4.text = ""
        frmIBMyAccnts.txtNickNAme.text = ""
        frmIBMyAccnts.txtbxOtherAccnt.text = ""
        frmIBMyAccnts.cmbobxBankType.selectedKey = "key1"
        frmIBMyAccnts.cmbobxBankType.setVisibility(true);
        frmIBMyAccnts.line363582120273807.setVisibility(false);
    }
}