function p2kwiet2012247635762_txtODMnthSavAmt_iPhone_onEndEditing_seq0(eventobject, changedtext) {
    var myDreamAmtBaht = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    var tarAmount = myDreamAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
    var targetAmtBaht = frmOpenAcDreamSaving.txtODTargetAmt.text;
    var entAmount = targetAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
        //var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
        //var entAmount = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
    if (kony.string.isNumeric(tarAmount) == false) {
        showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
        return false
    }
    if (tarAmount.indexOf("0") == 0) {
        showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
        frmOpenAcDreamSaving.txtODMyDream.text = "";
        return false
    }
    var indexdot = tarAmount.indexOf(".");
    var decimal = "";
    var remAmt = "";
    kony.print("indexdot" + indexdot);
    if (indexdot > 0) {
        decimal = tarAmount.substr(indexdot);
        kony.print("decimal@@" + decimal);
        if (decimal.length > 4) {
            alert("Enter only 2 decimal values");
            return false;
        }
    }
    var noOfMnths;
    tarAmount = kony.os.toNumber(tarAmount);
    entAmount = kony.os.toNumber(entAmount);
    if (tarAmount > entAmount) {
        showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
        return false;
    }
    noOfMnths = entAmount / tarAmount;
    kony.print("noOfMnths before rounding" + noOfMnths);
    noOfMnths = Math.round(noOfMnths);
    kony.print("noOfMnths after rounding" + noOfMnths);
    var deviceinfo = kony.os.deviceInfo();
    if (deviceinfo["name"] == "android") {
        noOfMnths = noOfMnths + "";
    }
    kony.print("noOfMnths" + noOfMnths);
    //if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
    frmOpenAcDreamSaving.txtODMyDream.text = "";
    frmOpenAcDreamSaving.txtODMyDream.text = noOfMnths + kony.i18n.getLocalizedString("keymonths");
    gblDreamMnths = noOfMnths;
    //}
    frmOpenAcDreamSaving.txtODMnthSavAmt.skin = "txtFocusBG"
    var AddOpenBahtDreamMnth = frmOpenAcDreamSaving.txtODMnthSavAmt.text
    frmOpenAcDreamSaving.txtODMnthSavAmt.text = commaFormattedOpenAct(AddOpenBahtDreamMnth) + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
}