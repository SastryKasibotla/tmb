function p2kwiet2012247633381_btnThaiR_onClick_seq0(eventobject) {
    //#ifdef spaip
    //#define preprocessdecision_onClick_74089201224761741_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define preprocessdecision_onClick_74089201224761741_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define preprocessdecision_onClick_74089201224761741_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define preprocessdecision_onClick_74089201224761741_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define preprocessdecision_onClick_74089201224761741_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define preprocessdecision_onClick_74089201224761741_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef preprocessdecision_onClick_74089201224761741_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    if ((kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH")) {
        gblLang_flag = "th_TH";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeThaiMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        frmMBTnC.lblTandCSpa.text = "";
        setLocaleTH();
        //gblLang_flag = "th_TH";
        //kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmMBTnC.btnEngR.skin = btnOnNormal;
        frmMBTnC.btnThaiR.skin = btnOffNorm;
        frmMBTnCPreShow();
    } else {}
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_21197201224767927_android_iphone
    //#endif
    //#ifdef iphone
    //#define preprocessdecision_onClick_21197201224767927_android_iphone
    //#endif
    //#ifdef preprocessdecision_onClick_21197201224767927_android_iphone
    if ((kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH")) {
        gblLang_flag = "th_TH";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeThaiMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        showLoadingScreen();
        frmMBTnC.lblTandCTh.text = "";
        frmMBTnC.hbxTandCEng.setVisibility(false);
        frmMBTnC.hbxTandCTh.setVisibility(true);
        setLocaleTH();
        frmMBTnC.btnEngR.skin = btnOnNormal;
        frmMBTnC.btnThaiR.skin = btnOffNorm;
        var list = {
                appLocale: "th_TH"
            }
            //kony.store.removeItem("curAppLocale");
        kony.store.setItem("curAppLocale", list);
        frmMBTnCPreShow();
    } else {}
    //#endif
}