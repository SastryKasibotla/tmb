function frmQRSuccess_onClickSelAccount(eventobject) {
    return AS_FlexContainer_ef84c01fd9de44819a4f45825396e6ed(eventobject);
}

function AS_FlexContainer_ef84c01fd9de44819a4f45825396e6ed(eventobject) {
    if (isSignedUser) {
        onSelectFromAccntQRPay();
    } else {
        gblQrSetDefaultAccnt = true;
        frmMBPreLoginAccessesPin.show();
        accessPinOnClickLogin();
    }
}