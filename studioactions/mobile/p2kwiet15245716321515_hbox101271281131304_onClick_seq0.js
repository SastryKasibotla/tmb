function p2kwiet15245716321515_hbox101271281131304_onClick_seq0(eventobject) {
    if (gblBpBalHideUnhide == "true") {
        frmBillPaymentCompleteCalendar.lblBalanceAfterPayment.setVisibility(false);
        frmBillPaymentCompleteCalendar.lblBalAfterPayValue.setVisibility(false);
        frmBillPaymentCompleteCalendar.lblHide.text = kony.i18n.getLocalizedString("show");
        gblBpBalHideUnhide = "false";
    } else {
        frmBillPaymentCompleteCalendar.lblBalanceAfterPayment.setVisibility(true);
        frmBillPaymentCompleteCalendar.lblBalAfterPayValue.setVisibility(true);
        frmBillPaymentCompleteCalendar.lblHide.text = kony.i18n.getLocalizedString("Hide");
        gblBpBalHideUnhide = "true";
    }
}