function p2kwiet2012247635471_hbox101271281131304_onClick_seq0(eventobject) {
    if (frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt.isVisible == true) {
        kony.print("@@ into if");
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt.setVisibility(false);
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBal.setVisibility(false)
        frmOpenAccountNSConfirmationCalendar.lblHide.text = kony.i18n.getLocalizedString("keyUnhide")
    } else {
        kony.print("@@ into else");
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBalAmt.setVisibility(true);
        frmOpenAccountNSConfirmationCalendar.lblOpenActCnfmBal.setVisibility(true)
        frmOpenAccountNSConfirmationCalendar.lblHide.text = kony.i18n.getLocalizedString("Hide")
    }
}