function p2kwiet2012247628486_txtAccessPwd_onTextChange_seq0(eventobject, changedtext) {
    //#ifdef iphone
    var str = frmCMChgAccessPin.txtAccessPwd.text;
    if (str != null && str.length >= 1) {
        popAccessPinBubble.destroy();
    } else {
        var context = {
            "widget": frmCMChgAccessPin.hbox4769997143083,
            "anchor": "right",
            "sizetoanchorwidth": false
        };
        popAccessPinBubble.setContext(context);
        popAccessPinBubble.show();
    }
    //#endif
}