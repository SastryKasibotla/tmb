function p2kwiet2012247634343_button47327209489401_onClick_seq0(eventobject) {
    //#ifdef j2me
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef bb
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef bb10
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winphone8
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef palm
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef iphone
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winmobile
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef symbian
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef winmobile6x
    //#define preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    //#endif
    //#ifdef preprocessdecision_onClick_13235201224767645_j2me_bb_bb10_winphone8_palm_android_iphone_winmobile_symbian_winmobile6x
    if ((gblUserLockStatusMB == gblFinancialTxnMBLock)) {
        alertUserStatusLocked.call(this);
    } else {
        deleteAccountFromRecipientView.call(this);
    }
    //#endif
    //#ifdef spaip
    //#define preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef wap
    //#define preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    //#endif
    //#ifdef preprocessdecision_onClick_7522201224766762_spaip_spabbnth_spawinphone8_wap_spaan_spabb_spawindows
    if ((gblIBFlowStatus == "04")) {
        alertUserStatusLocked.call(this);
    } else {
        deleteAccountFromRecipientView.call(this);
    }
    //#endif
}