function p2kwiet15245716321099_button475004897853_onClick_seq0(eventobject) {
    fullMinSpecButtonColorSet.call(this, eventobject);
    frmBillPayment.tbxAmount.setVisibility(true);
    frmBillPayment.tbxAmount.setEnabled(true);
    frmBillPayment.tbxAmount.setFocus(true);
    frmBillPayment.tbxAmount.text = fullAmt;
    frmBillPayment.tbxAmount.placeholder = "0.00";
    frmBillPayment.lblForFullPayment.setVisibility(false);
    gblFullPayment = false;
}