function AS_Browser_b2f062638d0b4f7fab42aeb614fd0a50(eventobject, errorObject) {
    if (gblPlatformName == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblPlatformName == "iPhone Simulator") {
        callBackAlertLoadFail();
    } else {
        if (errorObject.errorCode != "1016") {
            callBackAlertLoadFail();
        }
    }
}