function frmBillPaymentEditFutureComplete_shareLine(eventobject) {
    return AS_VBox_h48ac71a07b64a2fab8e1c68db02de1e(eventobject);
}

function AS_VBox_h48ac71a07b64a2fab8e1c68db02de1e(eventobject) {
    if ((gblMyBillerTopUpBB == "0")) {
        shareIntentCall.call(this, "line", "BillPayment");
    } else {
        shareIntentCall.call(this, "line", "TOPUpPayment");
    }
}