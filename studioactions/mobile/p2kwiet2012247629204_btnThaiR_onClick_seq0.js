function p2kwiet2012247629204_btnThaiR_onClick_seq0(eventobject) {
    //#ifdef spaip
    //#define preprocessdecision_onClick_25152201224769746_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabbnth
    //#define preprocessdecision_onClick_25152201224769746_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawinphone8
    //#define preprocessdecision_onClick_25152201224769746_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spaan
    //#define preprocessdecision_onClick_25152201224769746_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spabb
    //#define preprocessdecision_onClick_25152201224769746_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef spawindows
    //#define preprocessdecision_onClick_25152201224769746_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    //#endif
    //#ifdef preprocessdecision_onClick_25152201224769746_spaip_spabbnth_spawinphone8_spaan_spabb_spawindows
    if ((kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH")) {
        gblLang_flag = "th_TH";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeThaiMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        //frmMBTnC.lblTandCSpa.text="";
        setLocaleTH();
        //gblLang_flag = "th_TH";
        //kony.i18n.setCurrentLocaleAsync("th_TH", onSuccessLocaleChange, onFailureLocaleChange, "");
        frmFATCAQuestionnaire1.btnEngR.skin = btnOnNormal;
        frmFATCAQuestionnaire1.btnThaiR.skin = btnOffNorm;
        if (gblFATCAPage == 1) frmFATCAQuestionnaire1PreShow();
        else frmFATCAQuestionnaire2PreShow();
    } else {}
    //#endif
    //#ifdef android
    //#define preprocessdecision_onClick_2246720122476293_android_iphone
    //#endif
    //#ifdef iphone
    //#define preprocessdecision_onClick_2246720122476293_android_iphone
    //#endif
    //#ifdef preprocessdecision_onClick_2246720122476293_android_iphone
    if ((kony.i18n.getCurrentLocale() != "ru_RU" && kony.i18n.getCurrentLocale() != "th_TH")) {
        gblLang_flag = "th_TH";
        //#ifdef android
        //kony.application.showLoadingScreen("LocBlock",kony.i18n.getLocalizedString("keyLocaleChangeThaiMessage"), "center" , true, true, false);
        showLoadingScreen();
        //#endif
        showLoadingScreen();
        setLocaleTH();
        frmFATCAQuestionnaire1.btnEngR.skin = btnOnNormal;
        frmFATCAQuestionnaire1.btnThaiR.skin = btnOffNorm;
    } else {}
    //#endif
}