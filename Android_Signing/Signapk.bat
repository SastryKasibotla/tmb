@echo off
SET DATE=%date:~-10,2%_%date:~-7,2%_%date:~-4,4%

cd %3\Android_Signing
echo %7_TMB_%2_%4_%DATE%.apk

del /q ".\build\*"
FOR /D %%p IN (".\build\*.*") DO rmdir "%%p" /s /q

cd unsignedapk
IF EXIST TMBUI-release-unsigned.apk (
		
		echo TMBUI-release-unsigned.apk already exists @%3\Android_Signing\unsignedapk
		echo deleting old TMBUI-release-unsigned.apk file
		del TMBUI-release-unsigned.apk
		echo deleted TMBUI-release-unsigned.apk files succesfully
      )
cd %1
IF EXIST TMBUI-release-unsigned.apk (
		copy %1\TMBUI-release-unsigned.apk %3\Android_Signing\unsignedapk
		echo copied lates TMBUI-release-unsigned.apk file from %1 to %3\Android_Signing\unsignedapk
			
    )ELSE (
				
			echo TMBUI-release-unsigned.apk is not available at provided location %1
			exit %ERRORLEVEL%
			)	  


cd %3\Android_Signing

IF "%2" == "PROD" (
echo APK_UNSIGNED: %1
echo BUILD_TYPE: %2
echo PROJECT_LOCATION: %3
::echo VIT_VER: %4
::echo UAT_VER: %5
echo PROD_VER: %6
echo RElEASE_VERSION: %7
echo Android %2 signing started, it will takes a while to complete .........
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore .\keystores\PROD.keystore -storepass ze1*89B! .\unsignedapk\TMBUI-release-unsigned.apk tmbtouch >> ./build/%2_jarsigner_log.txt

::jarsigner -verbose -keystore .\keystores\PROD.keystore -storepass ze1*89B! .\unsignedapk\TMBUI-release-unsigned.apk tmbtouch >> ./build/%2_jarsigner_log.txt
echo %2 keystore signing completed log file created @ ./build/%2_jarsigner_log.txt


echo Android %2 Zipalign started it takes a while to complete .........	
zipalign -v 4 .\unsignedapk\TMBUI-release-unsigned.apk ./build/%7_TMB_%2_%4_%DATE%.apk >> ./build/%2_zipalign_log.txt
echo zipalign completed and log file created @ ./build/%2_zipalign_log.txt

copy .\build\%7_TMB_%2_%4_%DATE%.apk %1\%7_TMB_%2_%4_%DATE%.apk
::start %1
start .\build

)

IF "%2" == "UAT" (
echo APK_UNSIGNED: %1
echo BUILD_TYPE: %2
echo PROJECT_LOCATION: %3
::echo VIT_VER: %4
echo UAT_VER: %5
::echo PROD_VER: %6
echo RElEASE_VERSION: %7
echo Android %2 signing started, it will takes a while to complete .........
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore .\keystores\UAT.keystore -storepass mibuatddi .\unsignedapk\TMBUI-release-unsigned.apk mib_uat >> .\build\%2_jarsigner.log
echo %2 keystore signing completed log file created @ ./build/%2_jarsigner_log.txt


echo Android %2 Zipalign started it takes a while to complete .........	
zipalign -v 4 .\unsignedapk\TMBUI-release-unsigned.apk  .\build\%7_TMB_%2_%4_%DATE%.apk >> .\build\%2_zipalign_log.txt
echo zipalign completed and log file created @ ./build/%2_zipalign_log.txt

copy .\build\%7_TMB_%2_%4_%DATE%.apk %1\%7_TMB_%2_%4_%DATE%.apk
::start %1
start .\build

)

IF "%2" == "VIT" (
echo APK_UNSIGNED: %1
echo BUILD_TYPE: %2
echo PROJECT_LOCATION: %3
echo VIT_VER: %4
::echo UAT_VER: %5
::echo PROD_VER: %6
echo RElEASE_VERSION: %7
echo Android %2 signing started, it will takes a while to complete .........
jarsigner -verbose -keystore .\keystores\VIT.keystore -storepass kony1234 .\unsignedapk\TMBUI-release-unsigned.apk kony >> .\build\%2_Jarsigner_log.txt
echo %2 keystore signing completed log file created @ ./build/%2_jarsigner_log.txt


echo Android %2 Zipalign started it takes a while to complete .........	
zipalign -v 4 .\unsignedapk\TMBUI-release-unsigned.apk .\build\%7_TMB_%2_%4_%DATE%.apk >> .\build\VIT_zipalign_log.txt
echo zipalign completed and log file created @ ./build/%2_zipalign_log.txt

copy .\build\%7_TMB_%2_%4_%DATE%.apk %1\%7_TMB_%2_%4_%DATE%.apk
::start %1
start .\build


)
pause