var glblResulttable;

function mySwipe(myWidget, gestureInfo) {
  if(allowOtherOpr == false)
    return;

  if( gestureInfo["swipeDirection"] ==1 || gestureInfo["swipeDirection"] == 2){

    if (isMenuShown == false) {
      if (gblIndex == -1) {

        var a = "";
        var hboxID = myWidget["id"];

        var indexString = hboxID.split("hboxSwipe");
        if(indexString[1] == undefined)
          indexString = hboxID.split("SwipeContainer");
        var index = indexString[1];
        glb_accountId = index;
        if(swipeCurrentIndex != -1 && swipeCurrentIndex != index){
          var hboxSwipe = "hboxSwipe"+swipeCurrentIndex;
          var SwipeContainer = "SwipeContainer"+swipeCurrentIndex;
          frmAccountSummaryLanding.vbox4751247744173[hboxSwipe].isVisible = true;
          frmAccountSummaryLanding.vbox4751247744173[SwipeContainer].isVisible = false;

          hboxSwipe = "hboxSwipe"+index;
          SwipeContainer = "SwipeContainer"+index;
          frmAccountSummaryLanding.vbox4751247744173[hboxSwipe].isVisible = false; 
          frmAccountSummaryLanding.vbox4751247744173[SwipeContainer].isVisible = true; 
          swipeCurrentIndex = index;
        }
        else{
          var hboxSwipe = "hboxSwipe"+index;
          var SwipeContainer = "SwipeContainer"+index;
          frmAccountSummaryLanding.vbox4751247744173[hboxSwipe].isVisible = !(frmAccountSummaryLanding.vbox4751247744173[hboxSwipe].isVisible); 
          frmAccountSummaryLanding.vbox4751247744173[SwipeContainer].isVisible = !(frmAccountSummaryLanding.vbox4751247744173[SwipeContainer].isVisible); 

          swipeCurrentIndex = index;
        }
      }
    }
  }
}  

var setSwipe = {
  fingers: 1,
  swipedistance: 75,
  swipevelocity: 75
};

function closeAllFlaps(){
  var hboxSwipe = "hboxSwipe"+swipeCurrentIndex;
  var SwipeContainer = "SwipeContainer"+swipeCurrentIndex;
  if (swipeCurrentIndex != -1 && null !== frmAccountSummaryLanding.vbox4751247744173[hboxSwipe] && frmAccountSummaryLanding.vbox4751247744173[hboxSwipe].isVisible == false){
    frmAccountSummaryLanding.vbox4751247744173[hboxSwipe].isVisible = true;
    frmAccountSummaryLanding.vbox4751247744173[SwipeContainer].isVisible = false;
  }
}

function createNoOfRowsDynamically(resulttable) {
  animation = false;
  var locale = kony.i18n.getCurrentLocale();

  if(isNotBlank(resulttable["totalBal"])){
    if( isLoginFlow == true && isCacheFlow == true)
      frmAccountSummaryLanding.lblBalanceValue.text = "";
    else
      frmAccountSummaryLanding.lblBalanceValue.text = resulttable["totalBal"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

  }  else
    frmAccountSummaryLanding.lblBalanceValue.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht")

    var crmIdIm= resulttable["crmId"];

  if(!flowSpa)

  {  
    var base64ProfilePic = kony.store.getItem("cachedProfilePic");
    var allowProfilePicUpdate = false;
    var profilePicSavedVersion = getCacheData("gblProfilePicSavedVersion");
    if(gblProfilePicCurrentVersion !== null && profilePicSavedVersion !== null && gblProfilePicCurrentVersion > profilePicSavedVersion){
      allowProfilePicUpdate = true;
      setCacheData("gblProfilePicSavedVersion", gblProfilePicCurrentVersion);
    }
      
    if(allowProfilePicUpdate || base64ProfilePic === null || base64ProfilePic === undefined){
      frmAccountSummaryLanding.imgProfile.src="avatar1.png";
      loadProfilePic();
    }
    else{
      frmAccountSummaryLanding.imgProfile.base64 = base64ProfilePic;
    }
  }
  var productName;
  var imageName;
  var btnText;
  var containerWt;
  var visibility_transfer=false;
  var visibility_paybill=false;
  var visibility_topup=false;
  var visibility_paymycredit=false;
  var visibility_paymyloan=false;
  var visibility_cardless=false;
  var hbxPercentage;
  var hbxlayoutAlignment;
  var hbxwidgetAlignment;
  var hbxvisibilityRemainingFee;
  var lblRemainingFeeVal;
  var lblBaltext;
  var onClickBtn;
  var margin;
  var spaPadding;
  if (flowSpa) {
    spaPadding = [2, 1, 2, 1];
  } else {
    spaPadding = [0, 0, 0, 0];
  }
  //var gblDeviceInfo = kony.os.deviceInfo();
  //var gblPlatformName = kony.os.deviceInfo().name;
  if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("android", gblPlatformName))) {
    margin = [0, 0, 8, 0]
  } else if ((null != gblPlatformName) && (kony.string.equalsIgnoreCase("thinclient", gblPlatformName))) {
    margin = [0, 0, 8, 0]
  } else {
    margin = [0, 0, 0, 0]
  }
  var availableBalace;

  try {
    glblResulttable = resulttable;
 //Load freshly account summary rows
     kony.print("gblAnnouncementFlow: "+gblAnnouncementFlow);
     kony.print("gblforcedCleanAS: "+gblforcedCleanAS);
    kony.print("cacheBasedActivationFlow: "+cacheBasedActivationFlow);
    kony.print("changeLanguage: "+changeLanguage);
    kony.print("login flow cond: "+(isLoginFlow == true && ((null != pinFlow && pinFlow == true) || isCacheFlow == true)));
    if(populateASRows()){
      glbl_i = 0;
      onScrollEndDynamicRows(); 
      cacheBasedActivationFlow = false;
      changeLanguage = false;
      gblforcedCleanAS = false;
      gblAnnouncementFlow = false;
      isLoginFlow = false;
      updateCache(resulttable);
    }  
    else
      loadBalancesDynamically(); // To update the balances of existing account rows.
    gblRefreshGraph = false;
  } catch (e) {
    kony.print("Exception in createNoOfRowsDynamically"+e);
  }
}

function createBarGraph(id, btnSkin, width) {
 //if(populateASRows() || gblRefreshGraph == true){
  var graphLabel = new kony.ui.Label({
    "height": "100%",
    "id": "lblDepositeBal" + id,
    "isVisible": true,
    "left": "0%",
    "skin": btnSkin,
    "text": " ",
    "textStyle": {
      "letterSpacing": 0,
      "strikeThrough": false
    },
    "top": "0%",
    "width": width+"%",
    "zIndex": 1
  }, {
    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
    "padding": [0, 0, 0, 0],
    "paddingInPixel": false
  }, {
    "textCopyable": false
  });

    frmAccountSummaryLanding.hbxBarGraph.add(graphLabel);
 // }
}


function onSwipe(myWidget, gestureInfo) {
  if (isMenuShown == true) {

    var currentFormID = kony.application.getCurrentForm();
    removeGestureForAccntSummary(currentFormID);
    currentFormID.scrollboxMain.scrollToEnd();
    isMenuShown = false;
  }
}

function showAccuntSummaryScreen() {
  //if(gblFromSwitchLanguage == true || isNotBlank(gbl3dTouchAction))  //added gbl3dTouchAction in condition to recreate AS page 
  //  TMBUtil.DestroyForm(frmAccountSummaryLanding);
  gblfromCalender =false;
  resetTransferRTPPush();
  callCustomerAccountService();
}
/**
 * description
 * returns {}
 */

function showAccountSummLndnd(uniqueID) {
  gblMBLoggingIn = true;
  kony.print("FPRINT: gblSuccess="+gblSuccess);
  kony.print("FPRINT: uniqueID="+uniqueID);
  if(gblSuccess == true || gblSuccess == "true"){
    //var inputParam ={};
    //inputParam["deviceId"] = uniqueID;
    kony.print("FPRINT: IN LoginTouchId");
    //var pushRegID=kony.store.getItem("kpnssubcriptiontoken");
    //if(pushRegID!=null && pushRegID!="" && pushRegID!=undefined && pushRegID!="undefined")
    //{
    //  inputParam["subscriptionToken"]=pushRegID;
    //} 
    //invokeServiceSecureAsync("LoginTouchId", inputParam, callBackMBLoginAuthTouch)
  	loginAuthNew(uniqueID);
  } else {
    kony.print("FPRINT: IN loginAuth");
    //Commentin below line and and new function login optimisation
    //loginAuth(uniqueID);
    loginAuthNew(uniqueID);
  }
}


function callBackMBLoginAuthTouch(status, resulttable){
  if (status == 400) {
    if(resulttable["opstatus"] == 0){
      if (resulttable["deviceStatus"] == "0") {
        showAlert(kony.i18n.getLocalizedString("ECKonyDeviceErr00002"), kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "2") {
        showAlert("Device is blocked", kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "3") {
        showAlert("Device is cancelled", kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["errCode"] != null && resulttable["errCode"] == "KONYERRTI001") {
        showAlert(kony.i18n.getLocalizedString("keyLoginTouchIDError"), kony.i18n.getLocalizedString("keyLoginTouchIDErrorTry"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      }
      var initVecValue = resulttable["vecValue"];
      var serverTime = resulttable["serverTime"];
      var touchCode = resulttable["touchCode"];

      var algo = "aes";
      var prptobj = {
        padding: "pkcs5",
        mode: "cbc",
        initializationvector: initVecValue
      };
      var encrKeyFromDevice = kony.store.getItem("encrytedText");
      var readEncryVal = encrKeyFromDevice["encKeyVal"];
      var decryptkey = kony.crypto.readKey(readEncryVal);

      var retencrKeyFromDevice = convertBase64ToRawBytes(encrKeyFromDevice["enckey"]);

      kony.print("KONYTOUCHID: encrKeyFromDevice >>>"+encrKeyFromDevice);
      kony.print("KONYTOUCHID: readEncryVal >>>"+readEncryVal);
      kony.print("KONYTOUCHID: decryptkey >>>"+decryptkey);
      kony.print("KONYTOUCHID: retencrKeyFromDevice >>>"+retencrKeyFromDevice);
      try{
        var myClearText = kony.crypto.decrypt(algo, decryptkey, retencrKeyFromDevice, prptobj);
      } catch(e) {
        kony.print("KONYTOUCHID: Exception Decrypt >>>"+e);
        kony.print("KONYTOUCHID: Exception Decrypt1 >>>"+e.name);
        kony.print("KONYTOUCHID: Exception Decrypt2 >>>"+e.message);
      }

      kony.print("KONYTOUCHID: myClearText >>>"+myClearText);
      if(decryptkey == null || myClearText == null){
        glbInitVector = resulttable["vecValue"];
        gblServerTimeTouch = resulttable["serverTime"];
        gblTouchCodeTouch = resulttable["touchCode"];
        getKeyForLoginTouch()
      }else{
        kony.print("KONYTOUCHID: loginTouchIdMobileBanking >>>");
        //commenting below line for new Optimisation Uncommnet in case you want to revert back
        //loginTouchIdMobileBanking(initVecValue,touchCode,serverTime);
        loginTouchIdMobileBankingnew(initVecValue,touchCode,serverTime);
      }
    }  else{
      resetValues();
      kony.application.dismissLoadingScreen();
      var errorMessage = resulttable["errMsg"];
      var middlewareErrorMessage = resulttable["errmsg"];
      if(errorMessage == null || errorMessage == undefined || errorMessage == "" ){
        if(middlewareErrorMessage != null && middlewareErrorMessage != undefined && middlewareErrorMessage != "" ){
          errorMessage =  middlewareErrorMessage;
        }else{
          errorMessage = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
        }
      }
      // alert(""+errorMessage);
      showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"),kony.i18n.getLocalizedString("deviceIDUnlock"));
    }

  }

} 

function invokeVerifyOTPKonyService(uniqueId)
{
  showLoadingScreen();
  var deviceGeneratedOTP = generateOTPWIthServerTime(GBL_TIMEFOR_LOGIN,uniqueId); //generateOTP();
  inputParam = {};
  inputParam["deviceId"] = uniqueId;
  inputParam["otp"] = deviceGeneratedOTP
  invokeServiceSecureAsync("verifyOTPKony", inputParam, callBackInvokeCrmProfileInq)	
}

function generateOTPWIthServerTime(time,uniqueID) {
  var secKeyVal = decrypt(glbInitVector);
  var hardwareID = uniqueID;
  var secKey = hardwareID + time + secKeyVal;
  var secKeyHash = kony.crypto.createHash("sha256", secKey);

  return secKeyHash
}

function callBackInvokeCrmProfileInq(status, resulttable) {
  if (status == 400) {

    if (resulttable["opstatus"] == 0) {

      showLoadingScreen();
      inputParam = {};
      inputParam["rqUUId"] = "";
      inputParam["LoginInd"] = "login";
      inputParam["TriggerEmail"]="yes";
      isFromLogin=false;
      inputParam["activationCompleteFlag"] = "true";
      inputParam["activityTypeId"] = "Login";
      invokeServiceSecureAsync("LoginProcessServiceExecute", inputParam, loginPProcessCallBack)
    } else if (resulttable["opstatus"] == 1) {
      showAlert(kony.i18n.getLocalizedString("accPwdNotValid"), kony.i18n.getLocalizedString("info"));
      kony.application.dismissLoadingScreen();
      return false;
    } else {
      alert(" " + resulttable["errMsg"]);
      kony.application.dismissLoadingScreen();
      return false;
    }
  }
}

function onClickForInnerBoxes() {


  if (isMenuShown == true) {
    isMenuShown = false;
    var currentFormID = kony.application.getCurrentForm();
    removeGestureForAccntSummary(currentFormID);
    currentFormID.scrollboxMain.scrollToEnd();
  }
}

function removeGestureForAccntSummary(calledForm) {
  try{

    calledForm.vboxRight.removeGestureRecognizer(swipeGesture);
    calledForm.vboxRight.removeGestureRecognizer(tapGesture);

  }catch(exception) {

  }
}

function settingGestureForAccntSummary(calledForm) {

  var setSwipe = {
    fingers: 1,
    swipedistance: 5,
    swipevelocity: 75
  };
  var setupTapGes = {
    fingers: 1,
    taps: 1
  };
  swipeGesture = calledForm.vboxRight.setGestureRecognizer(2, setSwipe, onSwipe);
  tapGesture = calledForm.vboxRight.setGestureRecognizer(1, setupTapGes, onClickForInnerBoxes);
}

function onclickgetBillPaymentFromAccountsFromAccSmry() {
  if(checkMBUserStatus()){
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    kony.print("selRow>>"+selectedRow);
    glb_accId = selectedRow["accId"];
      //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId]["accId"];
    // var jointActXfer  = gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId].partyAcctRelDesc;
     var jointActXfer = selectedRow["partyAcctRelDescr"];
    if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
      alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
      return;
    }
    var accountStatus = selectedRow["acctStatus"];
        //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId].acctStatusCode;

    if(accountStatus.indexOf("Active") == -1){
      alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
      return;
    }

    var noCASAAct = noActiveActs();
    if(noCASAAct)
    {
      showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
      return false;
    }

    gblMyBillerTopUpBB = 0;
    GblBillTopFlag = true;
    gblBillPayShortCut = true;
	loadFunctionalModuleSync("eDonationModule");
    callMasterBillerService();
  }
}

function onclickgetTopUpFromAccountsFromAccSmry() {
  gblRefreshGraph = true; //Refresh bar graph
  if(checkMBUserStatus()){
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    kony.print("selRow>>"+selectedRow);
    glb_accId = selectedRow["accId"];
     //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId]["accId"];
    var jointActXfer  = selectedRow["partyAcctRelDesc"];
     //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId].partyAcctRelDesc;

    if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
      alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
      return;
    }
    var accountStatus = selectedRow["acctStatus"];
        //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId].acctStatusCode;

    if(accountStatus.indexOf("Active") == -1){
      alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
      return;
    }
    var noCASAAct = noActiveActs();
    if(noCASAAct)
    {
      showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
      return false;
    }

    gblMyBillerTopUpBB = 1;
    GblBillTopFlag = false;
    callMasterBillerService(); // For Top Up Redesign PBI
  }
}


function onClickCardLessSwipeBtn(){   
   loadFunctionalModuleSync("cardlessModule");
   onclickgetCardlessFromAccountsFromAccSmry();
}


function onclickgetCardlessFromAccountsFromAccSmry() {
  showLoadingScreen();
  if(checkMBUserStatus()){
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    glb_accId =  selectedRow["accId"];
      //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId]["accId"];
    var accountStatus = selectedRow["acctStatus"];
        //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId].acctStatusCode;
    var jointActXfer  =  selectedRow["partyAcctRelDesc"];
      //gblAccountTable["accountSummary"][0]["custAcctRec"][glb_accountId].partyAcctRelDesc;
    if(jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN"){
      dismissLoadingScreen();
      alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
      return;
    }
    if(accountStatus.indexOf("Active") == -1){
      dismissLoadingScreen();
      alert(""+ kony.i18n.getLocalizedString('keyNotAllloedTransactions'));
      return;
    }
    var noCASAAct = noActiveActs();
    if(noCASAAct)
    {
      dismissLoadingScreen();
      showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
      return false;
    }
    gblCardlessFlowCurrentForm = kony.application.getCurrentForm();

    //5. Setting up VTAP SDK
    setUpVTapSDK();
	frmCardlessWithdrawSelectAccount.txtAmountVal.text = "";
    if(gblUVregisterMB == "Y"){
      checkUVActiveCode();
    }else{
      callUVMaintInqService(callBackUVMaintInqServiceForCardLessWDShortcut);
    }
  }
}


//////Login Auth Composite service call starts here////

function loginAuth(uniqueID){
  inputParam = {};
  inputParam["deviceId"] = uniqueID;
  kony.print("FPRINT: uniqueID is deviceId= "+uniqueID);
  var pushRegID=kony.store.getItem("kpnssubcriptiontoken");
  if(pushRegID!=null && pushRegID!="" && pushRegID!=undefined && pushRegID!="undefined")
  {
    inputParam["subscriptionToken"]=pushRegID;
  }
  //inputParam["osDeviceId"] = kony.os.deviceInfo().deviceid;
  // modifying the code for kony device id.
  inputParam["osDeviceId"] = getDeviceID();
  //kony.print("FPRINT: getDeviceID= "+getDeviceID);
  channelId = "02";
  if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
    channelId = GLOBAL_MB_CHANNEL;
  }
  inputParam["loginChannel"] = channelId;
  inputParam["nodeNum"] = "1";
  kony.print("FPRINT: gFromConnectAccount="+gFromConnectAccount);
  if (gFromConnectAccount == true) {
    inputParam["password"] = glbAccessPin
    kony.print("FPRINT: glbAccessPin="+glbAccessPin);
  } else {
    inputParam["password"] = gblNum;//frmAfterLogoutMB.tbxAccessPIN.text;
    kony.print("FPRINT: gblNum="+gblNum);
    gblNum ="";
    //frmAfterLogoutMB.tbxAccessPIN.text = "";
  }
  inputParam["activityTypeId"] = "Login";
  gblLoginType = "Login";

  if(gblSuccess == true || gblSuccess == "true"){
    inputParam["loginTypeTouch"] = "true";
  }
  else{ 
    inputParam["loginTypeTouch"] = "false";
  }
  inputParam["deviceOS"] = getDeviceOS();
  //Hashing deviceId  
  //var secKeyVal = decrypt(glbInitVector);
  //alert("secKeyVal--->"+secKeyVal);
  invokeServiceSecureAsync("LoginCompositeService", inputParam, callBackMBLoginAuth)
}
function callBackMBLoginAuth(status, resulttable) {
  kony.print("FPRINT: gblStartClickFromActivationMB="+gblStartClickFromActivationMB);
  kony.print("FPRINT: status="+status);
  kony.print("FPRINT: resulttable[opstatus]="+resulttable["opstatus"]); 
  kony.print("FPRINT: resulttable[deviceStatus]="+resulttable["deviceStatus"]); 
  kony.print("FPRINT: resulttable[errMsg]="+resulttable["errMsg"]); 
  if (status == 400) {
    gblStartClickFromActivationMB = false;
    //if(!gblStartClickFromActivationMB){
    /*	gblStartClickFromActivationMB=false;
			kpns_log("callBackMBLoginAuth: call case gblStartClickFromActivationMB");
			registerAppWithPushNotificationsInfrastructure();
		} else {
          */
    var ksid = kony.store.getItem("ksid");
    kony.print("KSID Val is" + ksid);
    var ksidUpdated = kony.store.getItem("ksidUpdated");
    //var is_updated_kpns = kony.store.getItem("is_updated_kpns");
    // subscription token or code
    //Date Check
    var currentDate=kony.os.date("dd/mm/yyyy");
    var savedKsidDate= kony.store.getItem("ksidsavedDate");
    var pushRegID = kony.store.getItem("kpnssubcriptiontoken");
    var kpns_refreshtime = "";
    kony.print("Value of KPNS Refresh is"+KPNS_REFRESH);
    if(isNotBlank(KPNS_REFRESH)){
      kpns_refreshtime = KPNS_REFRESH;
    }else{
      kpns_refreshtime = "15";
    }      	
    if(isNotBlank(savedKsidDate)){
      var dateDiff= kony.os.compareDates(currentDate, savedKsidDate, "dd/mm/yyyy")
      if(dateDiff>=kpns_refreshtime){
        kony.print("Call KPNS after certain refresh interval");
        registerAppWithPushNotificationsInfrastructure();
      }
    }else{
      kony.print("KPNS Called First Time");
      if (!isNotBlank(ksidUpdated) || !isNotBlank(pushRegID)) {
        kony.print("callBackMBLoginAuth: call case ksid is null or pushRegID is null");
        registerAppWithPushNotificationsInfrastructure();
      }  
    }       


    kony.print("KSID updated Val is"+kony.store.getItem("ksid"));
    //}
    if (resulttable["opstatus"] == 1234 || resulttable["opstatus"] == "1234") {
      kony.application.dismissLoadingScreen();
      checkUpgrades(resulttable["updatestatus"], resulttable["updateurl"]);
      resetValues();
      return false;

    } else if (resulttable["opstatus"] == 0) {

      /**
            	Device Store of Surrogate ID 
            **/

      if(null == tmbRetriveFromDevice("trusteerIdFromClient")){
        if(null != resulttable["trusteerIdFromClient"] && undefined != resulttable["trusteerIdFromClient"] )
          tmbSaveOnDevice("trusteerIdFromClient",resulttable["trusteerIdFromClient"] )
          }


      gblShowAnyIDRegistration = resulttable["setyourID"];
      gblCustomerIDType=resulttable["customerIDType"];
      gblCustomerIDValue=resulttable["customerIDValue"];
      gbllastMbLoginSucessDateEng=resulttable["lastMbLoginSucessDateEng"];
      gbllastMbLoginSucessDateThai=resulttable["lastMbLoginSucessDateThai"];
      if (resulttable["deviceStatus"] == "0") {
        showAlert(kony.i18n.getLocalizedString("ECKonyDeviceErr00002"), kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "2") {
        showAlert("Device is blocked", kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["deviceStatus"] == "3") {
        showAlert("Device is cancelled", kony.i18n.getLocalizedString("info"));
        resetValues();
        kony.application.dismissLoadingScreen();
        return false;
      }
      else if (resulttable["errMsg"] == "Password is verified") {
        var time = resulttable["time"];
        GBL_TIMEFOR_LOGIN = resulttable["time"]
        registerForTimeOut();
        gblTouchStatus = resulttable["usesTouchId"];
        gblActivationFlow = true;  
        kony.print("FPRINT: Password is verified ");
        if(gblTouchStatus == undefined || gblTouchStatus == "null" ||  gblTouchStatus == null || gblTouchStatus == "" || gblTouchStatus == "N"){
          gblTouchStatus = "N";
          kony.store.setItem("usesTouchId", "N");
        } else if(gblTouchStatus == "Y")
        {
          kony.store.setItem("usesTouchId", "Y");
        }
        kony.print("FPRINT: gblTouchStatus="+gblTouchStatus);
        setPostLoginGlobalVars(resulttable); 
        loginPProcessCallBack(resulttable);

      } else if (resulttable["errMsg"] == "Password Locked" || resulttable["errCode"] == "E10105") {
        resetValues();
        onClickChangeTouchIcon();
        kony.print("FPRINT 4 SETTING isUserStatusActive TO FALSE");
        isUserStatusActive=false;
        kony.print("FPRINT 4 setting isUserStatusActive to false");
        kony.store.setItem("isUserStatusActive", false);
        
        popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
        popupAccesesPinLocked.btnPopupConfCancel.text= kony.i18n.getLocalizedString("keyCancelButton");
        popupAccesesPinLocked.btnCallTMB.text= kony.i18n.getLocalizedString("keyCall");
        popupAccesesPinLocked.show();
        //alert(" " + kony.i18n.getLocalizedString("keyAccessPwdLocked"));
        kony.application.dismissLoadingScreen();
        return false;
      } else if (resulttable["errMsg"] == "User is already login to application") {
        resetValues();
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
        frmMBPreLoginAccessesPin.show();		//As per existing behaviour from R67 app. QA expectation
        return false;
      }else {
        resetValues();
        //showAlertRcMB(kony.i18n.getLocalizedString("keyWrongPwd"),kony.i18n.getLocalizedString("info"), "info");
        kony.application.dismissLoadingScreen();
        return false;
      }
    } else if (resulttable["errMsg"] == "User is already login to application") {
      resetValues();
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECUserLoggedIn"), kony.i18n.getLocalizedString("info"));
      frmMBPreLoginAccessesPin.show();		//As per existing behaviour from R67 app. QA expectation
      return false;
    } else {

      var errorMessage = resulttable["errMsg"];
      var middlewareErrorMessage = resulttable["errmsg"];

      kony.application.dismissLoadingScreen();

      if ((resulttable["opstatus"] == -1 || resulttable["opstatus"] == "-1") && (errorMessage == "deviceIdEnhancementUnlock" || middlewareErrorMessage == "deviceIdEnhancementUnlock")) {

        showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"),kony.i18n.getLocalizedString("deviceIDUnlock"));

      }else{

        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));

      }

      resetValues();
      return false;

    }
  }else{
    resetValues();	

  }
}


/*
 Puropse: To initialize the global variables 
*/
function setPostLoginGlobalVars(resultTable){
  //PostLogin GBL Variables
  GLOBAL_Prod_code_ME = resultTable["Prod_Code_MEACC"];
  GLOBAL_ERROR_CODES_FOR_TOPUP = resultTable["ERROR_CODES_FOR_TOPUP"];
  GLOBAL_KONY_IDLE_TIMEOUT = resultTable["KONY_IDLE_TIMEOUT"];
  GLOBAL_LOAD_MORE = resultTable["GLOBAL_LOAD_MORE"];
  GLOBAL_MAX_BILL_COUNT = resultTable["MAX_BILL_COUNT"];
  GLOBAL_MAX_BILL_ADD = resultTable["MAX_BILL_ADD"];
  BILLER_LOGO_URL = resultTable["BILLER_LOGO_URL"];
  GLOBAL_INI_MAX_LIMIT_AMT = resultTable["INI_MAX_LIMIT_AMT"];
  GLOBAL_CEILING_LIMIT = resultTable["CEILING_LIMIT"];
  GLOBAL_CEILING_LIMIT = resultTable["CEILING_LIMIT"];
  GLOBAL_MAX_LIMIT_HIST = resultTable["EB_MAX_LIMIT_AMT_HIST"];
  GLOBAL_NICKNAME_LENGTH = resultTable["NICKNAME_MAX_LENGTH_OPEN"];
  GLOBAL_DREAM_DESC_MAXLENGTH = resultTable["DREAM_DESC_MAX_LENGTH"];
  GLOBAL_EDITDREAM_STARTTIME = resultTable["EDIT_DREAM_STARTTIME"];
  GLOBAL_EDITDREAM_ENDTIME = resultTable["EDIT_DREAM_ENDTIME"];
  Gbl_Image_Size_Limit = resultTable["Max_FileSize"];
  GLOBAL_JOINT_CODES = resultTable["JOINT_ACCNT_CODES"];
  GLOBAL_MB_APPLY_CODES = resultTable["APPLY_MB_STATUS_CODES"];
  gblTMBBonusURL = resultTable["TMBBONUS_URL"];
  gblTMBBonusAuthorization = resultTable["TMBBONUS_AUTHEN"];
  gblTMBBonusflag = resultTable["TMBBONUS_FLAG"];
  gblALL_SMART_FREE_TRANS_CODES = resultTable["ALL_SMART_FREE_TRANS_CODES"];	
  gblSMART_FREE_TRANS_CODES = resultTable["SMART_FREE_TRANS_CODES"];	
  gblORFT_FREE_TRANS_CODES = resultTable["ORFT_FREE_TRANS_CODES"];
  gblORFT_ALL_FREE_TRANS_CODES = resultTable["ALL_ORFT_FREE_TRANS_CODES"];
  gblITMX_FREE_TRANS_CODES = resultTable["ITMX_FEE_TRANS_CODES"];
  gblTO_ACC_P2P_FEE_WAIVE_CODES = resultTable["TO_ACC_P2P_FEE_WAIVE_PROD_CODES"];
  Gbl_Prod_Code_MEACC = resultTable["Prod_Code_MEACC"];
  GLOBAL_SecurityAdvice_Link_EN = resultTable["SecurityAdvice_Link_EN"];
  GLOBAL_SecurityAdvice_Link_TH = resultTable["SecurityAdvice_Link_TH"];
  gblCardlesswithdrawEndTime = resultTable["CARDLESS_WITHDRAW_ENDINGTIMER"];
  gblCardlessWithDrawMaxAmt = resultTable["CARDLESS_WITHDRAW_MAXAMOUNT"];
  kony.print("gblCardlessWithDrawMaxAmt:" + gblCardlessWithDrawMaxAmt);
  // Online Payment based on MWA
  GLOBAL_ONLINE_PAYMENT_VERSION_MWA = resultTable["ONLINE_PAYMENT_VERSION_MWA"];
  GBL_VALID_CHARS_MY_NOTE_NOW = resultTable["VALID_CHARS_MY_NOTE_NOW"];
  GBL_VALID_CHARS_MY_NOTE_FUTURE = resultTable["VALID_CHARS_MY_NOTE_FUTURE"];  			

  if (resultTable["FATCA_MAX_SKIP_COUNTER"] != null && resultTable["FATCA_MAX_SKIP_COUNTER"] != undefined && resultTable["FATCA_MAX_SKIP_COUNTER"] != "")
    GLOBAL_FATCA_MAX_SKIP_COUNT = parseInt(resultTable["FATCA_MAX_SKIP_COUNTER"]);
  else
    GLOBAL_FATCA_MAX_SKIP_COUNT = 3;	

  if (resultTable["Start_Digs_MobileNum"] != null && resultTable["Start_Digs_MobileNum"] != undefined && resultTable["Start_Digs_MobileNum"] != "")
    Gbl_StartDigsMobileNum = resultTable["Start_Digs_MobileNum"];
  else
    Gbl_StartDigsMobileNum = "09,08,06";
  Gbl_StartDigsMobileNum.split(',');

  //FATCA TnC Add
  GLOBAL_FATCA_TNC_en_US = resultTable["FATCA_TERMS_CONDITIONS_ENGLISH"];
  GLOBAL_FATCA_TNC_th_TH = resultTable["FATCA_TERMS_CONDITIONS_THAI"]; 

  //RTP Enable Flag
  GLOBAL_RTP_ENABLE = resultTable["RTP_ENABLE"];

  //MF Temenos ON/OFF Support Flag
  GBL_MF_TEMENOS_ENABLE_FLAG = resultTable["MF_TERMINOS_FLAG"];

  //UV Status Flag
	GLOBAL_UV_STATUS_FLAG = resultTable["UV_STATUS_FLAG"];
	
  //MF Suitability ON/OFF Support Flag
  GBL_MF_SUITABILITY_FLAG = resultTable["MF_SUITABILITY_FLAG"];

  // Online Bill Payment for PEA
  	gblEA_BILLER_COMP_CODES = resultTable["EA_BILLER_COMPCODES"];
  	gblPEA_BILLER_COMP_CODE = resultTable["PEA_BILLER_COMPCODE"];
  // Call Me Now URL
  gblCallMeNowUrl = resultTable["CALL_ME_NOW_URL"];
  gblEnabledLoan = resultTable["ENABLED_LOAN_APPLICATION_STABLE"];
  gblMaximumLoanUploadDocumentSize = parseInt(resultTable["MAXIMUM_LOAN_UPLOAD_DOCUMENT_SIZE"]);
  if (resultTable["CURRENT_AAL"] != null){
    gblCurrentAAL = parseFloat(resultTable["CURRENT_AAL"]);
  }
  if (resultTable["CURRENT_AAL"] != null){
    gblCurrentIAL = parseFloat(resultTable["CURRENT_AAL"]);
  }
  //SaveAlert Schedule Monthly repeat Min & Max Values
  if(isNotBlank(resultTable["SAVE_ALERT_SCHED_TIMES_MIN"])){
    GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MIN = resultTable["SAVE_ALERT_SCHED_TIMES_MIN"];
  }
  if(isNotBlank(resultTable["SAVE_ALERT_SCHED_TIMES_MAX"])){
    GBL_SAVEALERT_SCHEDULE_MONTHLY_REPEAT_MAX = resultTable["SAVE_ALERT_SCHED_TIMES_MAX"];
  }
  gblAllowUpdateIAl = parseFloat(resultTable["UPDATE_IAL_MAX"]);
}
//////Login Auth Composite service call ends here////

function createVBox(objId){
  var a = new kony.ui.Box({
    "id": objId,
    "isVisible": true,
    "position": constants.BOX_POSITION_AS_NORMAL,
    "orientation": constants.BOX_LAYOUT_VERTICAL
  }, {
    "containerWeight": 100,
    "percent": true,
    "layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_CENTER,
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "margin": [0, 0, 0, 0],
    "padding": [0, 0, 0, 0],
    "vExpand": false,
    "hExpand": true,

    "marginInPixel": false,
    "paddingInPixel": false,
    "layoutType": constants.CONTAINER_LAYOUT_BOX
  }, {});
  return a;
}

function createHBoxBG(objId,event){
  var a = new kony.ui.Box({
    "id": objId,
    "isVisible": true,
    "skin": "hboxHeader",
    "position": constants.BOX_POSITION_AS_NORMAL,
    "orientation": constants.BOX_LAYOUT_HORIZONTAL,
    "onClick": event
  }, {
    "containerWeight": 28,
    "percent": true,
    "layoutAlignment": constants.BOX_LAYOUT_ALIGN_FROM_CENTER,
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "margin": [5, 0, 5, 0],
    "padding": [0, 2, 0, 2],
    "vExpand": false,
    "hExpand": true,

    "marginInPixel": false,
    "paddingInPixel": false,
    "layoutType": constants.CONTAINER_LAYOUT_BOX
  }, {});
  return a;
}

function createImageIcon(objId,img){
  var a = new kony.ui.Image2({
    "id": objId,
    "isVisible": true,
    "src": img,
    "imageWhenFailed": img,
    "imageWhileDownloading": img
  }, {
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "margin": [0, 0, 0, 0],
    "padding": [0, 0, 0, 0],
    "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
    "referenceWidth": null,
    "referenceHeight": null,
    "marginInPixel": false,
    "paddingInPixel": false,
    "containerWeight": 14
  }, {});
  return a;
}

function createObj(idObj,visibility,text){
  var a = new kony.ui.Label({
    "id": idObj,
    "isVisible": visibility,
    "text": text,
    "skin": "lbl20pxDBOzoneX255"
  }, {
    "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
    "vExpand": true,
    "hExpand": false,
    "margin": [0, 0, 0, 0],
    "padding": [0, 0, 0, 0],
    "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_CENTER,
    "marginInPixel": false,
    "paddingInPixel": false,
    "containerWeight": 50
  }, {});
  //"onClick": onclickgetBillPaymentFromAccountsFromAccSmry
  return a;
}

function truncateNumber(num) {
  var num1 = "";
  var num2 = "";
  var num1 = num.split('.')[0];
  num2 = num.split('.')[1];
  var decimalNum = num2.substring(0, 2);
  var strNum = num1 +"."+ decimalNum;
  var finalNum = parseFloat(strNum);
  return finalNum;
}

function showAccountSummaryWithoutRefresh(){
  //frmAccountSummaryLanding.show();
  showAccountSummaryNew();
}

function loadProfilePic(){
  var inputparam = {};
  invokeServiceSecureAsync("getBase64ProfilePicture", inputparam, loadProfilePicCallback);
}

function loadProfilePicCallback(status, resultset){
  if(status == 400 ){
    if(resultset["opstatus"] == 0){
      if(null != resultset.base64profile ){
        //frmAccountSummaryLanding.imgProfile.base64 = resultset.base64profile;
        frmAccountSummary.imgProfile.base64 = resultset.base64profile;
        kony.store.setItem("cachedProfilePic",resultset.base64profile); 
      }
    }
  }
}

//Function to check if new rows have to be created on account summary page.
function populateASRows(){
   return (gblAnnouncementFlow == true || 
           gblforcedCleanAS == true || 
           cacheBasedActivationFlow == true || 
           changeLanguage == true || 
          isLoginFlow == true && ((null != pinFlow && pinFlow == true) || isCacheFlow == true));
 }

function updateCache(resulttableUpdated){
  if(isCacheFlow == false && isLoginFlow == false){
    var resulttable = kony.store.getItem("cachedResulttableASLoginFlow");
    if(null !== resulttable && null !== resulttableUpdated && null !== resulttable["accountSummary"] && null !== resulttable["accountSummary"][0]["custAcctRec"]){
      resulttable["accountSummary"][0]["custAcctRec"] = resulttableUpdated["custAcctRec"];
    }
  }
}


//Function to load Account summary page with refreshed data after destroy.
function showAccountSummaryAfterDestroy(){

  gblforcedCleanAS = true;

  TMBUtil.DestroyForm(frmAccountSummaryLanding);

  showAccountSummaryFromMenu();
}

function forceAccountSummaryRefresh(){
  if(!isNotBlank(frmAccountSummaryLanding.vbox4751247744173) || !isNotBlank(frmAccountSummaryLanding.vbox4751247744173.hboxSwipe0)){
    showAccountSummaryAfterDestroy();
  }
}