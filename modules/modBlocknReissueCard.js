canReissueCard = "N";
blockNReissueAccId = "";
blockNReissueSuccess = true;
blockNReissueTryagain = false;
gblCCDBCardFlow = "";
gblofferCardDetails = "";
//gblCCDBCardFlow = "CREDIT_CARD_BLOCK";
//gblCCDBCardFlow= "DEBIT_CARD_BLOCK";
//gblCCDBCardFlow= "DEBIT_CARD_REISSUE";
//----- method will modified when js integration done
//frmMBNewTnc for Block CC
gbltryAgainCount = 0; // remove when service integrated

function frmMBNewTncBlockCCShow() {
    gblMBNewTncFlow = TNC_FLOW_TMB_BLOCK_CREDIT_CARD;
    callServiceReadUTFFile();
}

function frmMBNewTncBlockCCOnClickBtnNext() {
    getCardBlockReasons();
}

function frmMBNewTncBlockCCOnClickBtnBack() {
    //frmStart.show();
    frmMBManageCard.show();
}

function frmMBBlockCardRecommendationBtnbackOnClick() {
    frmMBManageCard.show();
}
//frmMBNewTnc for New DC
function debitCardInqServiceBlockCard() {
    kony.print("@@ debitCardInqServiceBlockCard fun @@@@");
	showLoadingScreen();
	var inputparam = {};
	gblDebitCardResult="";
	inputparam = {};
    inputparam["cardFlag"] = "2";
    inputparam["acctId"] = blockNReissueAccId;
    
	//set current account
	if (isNotBlank(blockNReissueAccId)) {
	    if (isNotBlank(gblAccountTable["custAcctRec"]) && gblAccountTable["custAcctRec"].length > 0) {
	        for (var i = 0; i < gblAccountTable["custAcctRec"].length; i++) {
	            if (gblAccountTable["custAcctRec"][i]["accId"] == blockNReissueAccId) {
	                gblIndex = i;
	                break;
	            }
	        }
	    }
    	//substring from 14 to 10 digit
    	blockNReissueAccId = blockNReissueAccId.substring(blockNReissueAccId.length - 10);
    }
    invokeServiceSecureAsync("customerCardList", inputparam, debitCardInqServiceBlockCardCallBack);
}
function debitCardInqServiceBlockCardCallBack(status, resulttable) {
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			if (resulttable["statusCode"] == 0) {
				gblDebitCardResult=resulttable;
				gblCardList=gblDebitCardResult;
				GLOBAL_DEBITCARD_TABLE = resulttable["debitCardRec"];
				performIssueNewCard();
				dismissLoadingScreen();
			} else {
				dismissLoadingScreen();
			}
		} else {
			dismissLoadingScreen();
		}
	}
	dismissLoadingScreen();
}
function frmMBNewTncNewDCShow() {
	//check office hour
		var inputparams = {};
	    showLoadingScreen();
	    invokeServiceSecureAsync("checkOpenActBusinessHours", inputparams, callbackFrmMBNewTncNewDCShow);
	
	
}
function callbackFrmMBNewTncNewDCShow(status, resulttable) {
	if (status == 400 && resulttable["opstatus"] == 0) {
        dismissLoadingScreen();
        if (resulttable["openActBusinessHrsFlag"] == "false") {
			var errorMsg = kony.i18n.getLocalizedString("Card_Err_ServiceHour");
			errorMsg = errorMsg.replace("{service_hours}", resulttable["openActStartTime"] + " - " + resulttable["openActEndTime"]);
			showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
        }else{
        	//open t&c
        	gblMBNewTncFlow = TNC_FLOW_TMB_NEW_DEBIT_CARD;
	 		callServiceReadUTFFile();
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}

function frmMBNewTncNewDCOnClickBtnNext() {
    getProductofferCard();
    //frmMBReIssueDBProduct.show();
}

function frmMBNewTncNewDCOnClickBtnBack() {
    //frmStart.show();
	preShowMBDebitCardListfromAccntSummary()
}

//frmMBBlockCCChangeAddress

function frmMBBlockCCChangeAddressShow() {
    frmMBBlockCCChangeAddress.show();
}

function frmMBBlockCCChangeAddressOnClickBtnClose() {
	if(kony.application.getPreviousForm().id == "frmMBBlockCardCCDBConfirm"){
		frmMBBlockCardCCDBConfirm.show();
	}else if(kony.application.getPreviousForm().id == "frmMBRequestNewPin"){
		frmMBRequestNewPin.show();
	}else{
		showAlert(kony.application.getPreviousForm().id, 
			kony.i18n.getLocalizedString("info"));
	}
}

function frmMBBlockCCChangeAddressOnClickBtnCallCenter() {
    callNative("1558");
}

function onclickTryAgainBlockCard() {
    callBlockCardCompositeService(""); //try again no need send transaction password
}
//frmMBBlockCCReason

function frmMBBlockCCReasonOnClickBtnBack() {
    frmMBNewTnC.show();
}

function getCardBlockReasons() {
    var input_param = {};
    showLoadingScreen();
    invokeServiceSecureAsync("getCardBlockReasons", input_param, getCardBlockReasonsCallBack);
}

function getCardBlockReasonsCallBack(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            cardBlockReasons = resulttable["cardBlockReasons"];
            frmMBBlockCCReason.show();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
    }
}

function onSelectReason() {
    //alert(frmMBBlockCCReason.segReason.selectedItems[0]);
    //frmMBBlockCardCCDBConfirm.show();
	var inputParams = {};
	inputParams["action"] = "activityLog_creditCard"; //just init log
	inputParams["cardRefId"] = gblCACardRefNumber;
	inputParams["password"] = "";
    inputParams["blockReason"] = frmMBBlockCCReason.segReason.selectedItems[0]["hdnlblReasonCode"];
    showLoadingScreen();
    invokeServiceSecureAsync("blockCardCompositeService", inputParams, callbackOnSelectReason);
}

function callbackOnSelectReason(status, resulttable){
	if (status == 400 && resulttable["opstatus"] == 0) {
		dismissLoadingScreen();
		getCreditcardlinkdetails();
    } else {
        dismissLoadingScreen();
		//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}

function getCreditcardlinkdetails() {
    var input_param = {};
    input_param["cardRefId"] = gblCACardRefNumber; // "4966941300203006";
    kony.print("gblCACardRefNumber" + gblCACardRefNumber);
    kony.print("cardRefId" + input_param["cardRefId"]);
    showLoadingScreen();
    invokeServiceSecureAsync("creditCardStatusInq", input_param, callbackgetCreditcardlinkdetails);
}

function callbackgetCreditcardlinkdetails(status, resulttable) {
    try {
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                dismissLoadingScreen();
                var addressValue = "";
                if (isNotBlank(resulttable["CardAddr"])) {
                    var cardAdd = resulttable["CardAddr"][0];
                    if (isNotBlank(cardAdd["Addr1"])) {
                        addressValue += " " + cardAdd["Addr1"];
                    }
                    if (isNotBlank(cardAdd["Addr2"])) {
                        addressValue += " " + cardAdd["Addr2"];
                    }
                    if (isNotBlank(cardAdd["City"])) {
                        addressValue += " " + cardAdd["City"];
                    }
                    if (isNotBlank(cardAdd["PostalCode"])) {
                        addressValue += " " + cardAdd["PostalCode"];
                    }
                }
                if (isNotBlank(addressValue)) {
                    frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text = kony.string.trim(addressValue);
                } else {
                    frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text = " ";
                }
                frmMBBlockCardCCDBConfirm.lblCardNumber.text = gblSelectedCard["cardNo"];
                frmMBBlockCardCCDBConfirm.show();
            } else {
                dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    } catch (e) {
        kony.print(e.message);
    }
}

//frmMBBlockCardCCDBConfirm

function callBackDebitCardContactAddress(status, resulttable) {
    try {
        if (status == 400) {
            if (resulttable["opstatus"] == 0) {
                dismissLoadingScreen();
                if (resulttable["s2sBusinessHrsFlag"] != null && resulttable["s2sBusinessHrsFlag"] != "" && resulttable["s2sBusinessHrsFlag"] != undefined) {
                    gblOpenActBusinessHrs = resulttable["s2sBusinessHrsFlag"];
                }
                if (resulttable["emailAddr"] != null) {
                    gblEmailAddr = resulttable["emailAddr"];
                }
                if (resulttable["Persondata"] != null || resulttable["Persondata"] != undefined) {

                    for (var i = 0; i < resulttable["Persondata"].length; i++) {
                        var addressType = resulttable["Persondata"][i]["AddrType"];
                        if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {

                            var adr3 = resulttable["Persondata"][i]["addr3"];
                            var reg = / {1,}/;
                            var tempArr = [];
                            tempArr = adr3.split(reg);

                            var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
                            var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
                            var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
                            var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";

                            if (addressType == "Primary") {
                                if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                    if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                                        if (tempArr[0].indexOf(subDistBan, 0) >= 0) gblsubdistrictValue = tempArr[0].substring(4);
                                        else if (tempArr[0].indexOf(subDistNotBan, 0) >= 0) gblsubdistrictValue = tempArr[0].substring(2);
                                        else gblsubdistrictValue = "";
                                        if (tempArr[1].indexOf(distBan, 0) >= 0) gbldistrictValue = tempArr[1].substring(3);
                                        else if (tempArr[1].indexOf(distNotBan, 0) >= 0) gbldistrictValue = tempArr[1].substring(2);
                                        else gbldistrictValue = "";
                                    }
                                } else {
                                    gblsubdistrictValue = "";
                                    gbldistrictValue = "";
                                }
                                if (tempArr[0] == undefined || tempArr[0] == "" || tempArr[0] == null || tempArr[0] == "undefined") gblViewsubdistrictValue = "";
                                else gblViewsubdistrictValue = tempArr[0];
                                if (tempArr[1] == undefined || tempArr[1] == "" || tempArr[1] == null || tempArr[1] == "undefined") gblViewdistrictValue = "";
                                else gblViewdistrictValue = tempArr[1];
                                if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) gblStateValue = resulttable["Persondata"][i]["City"];
                                else gblStateValue = "";
                                if (resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined) gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                                else
                                gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                                if (resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined) gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                                else gblcountryCode = "";
                                if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                    gblnotcountry = true;
                                } else {
                                    gblnotcountry = false;
                                }
                                gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                                gblAddress2Value = resulttable["Persondata"][i]["addr2"];
                                frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
                                frmMBBlockCardCCDBConfirm.show();
                            }
                        }
                    }
                }
            } else {
                dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    } catch (e) {
        kony.print(e.message);
    }
}

function frmMBBlockCardCCDBConfirmOnClickBtnChangeAdd() {
    if (gblCCDBCardFlow == "CREDIT_CARD_BLOCK") {
        frmMBBlockCCChangeAddressShow();
    } else if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
        //for using frmeditContactInfo
        gblOpenAccountFlow = false;
        gblOpenProdAddress = false;
        gblOpenActSavingCareEditCont = false;
        gblAddressOld=frmMBBlockCardCCDBConfirm.richtextSendAddressVal.text;
        getMBEditContactStatus();
    }
}

function onclickBackfrmMBReIssueDBProduct() {
    frmMBNewTnC.show();
}

function onclickNextfrmMBReIssueDBProduct() {
    gblCCDBCardFlow = "DEBIT_CARD_REISSUE"; //TODO: need to check where to modify global variable
    var inputParamActLog = {};
    inputParamActLog["activityLog_activityId"] = "143";
    inputParamActLog["activityLog_flex1"] = "Step 1/4 - Issue New Debit Card";
    inputParamActLog["activityLog_flex2"] = "Linked Account = "+maskAccount(blockNReissueAccId);
    inputParamActLog["activityLog_flex3"] = "Account Nickname = "+gblofferCardDetails["acctNickNameEN"];
    inputParamActLog["activityLog_flex4"] = "CurrentProduct =  "+gblofferCardDetails["productID"]+"+"+gblofferCardDetails["ProductNameEng"];
    inputParamActLog["activityLog_flex5"] = "";
    //inputParamActLog["action"]="activityLog"; 
    showLoadingScreen();
    invokeServiceSecureAsync("reissueCardActivityLog", inputParamActLog, callBackReissueCardActivityLog);
}

function callBackReissueCardActivityLog(){
	dismissLoadingScreen();
    invokePartyInqService();
}

function onclickBackfrmMBBlockCardCCDBConfirm() {
    if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
		preShowMBDebitCardListfromAccntSummary();        
    } else if (gblCCDBCardFlow == "DEBIT_CARD_BLOCK") {
        frmMBManageCard.show();
    } else if (gblCCDBCardFlow == "CREDIT_CARD_BLOCK") {
        frmMBBlockCCReason.show();
    }
}

function onclickNextfrmMBBlockCardCCDBConfirm() {
   	//frmMBBlockCardSuccess.destroy();
    //frmMBBlockCardSuccess.show();
    var inputParamActLog = {};
    kony.print("in gblCCDBCardFlow " + gblCCDBCardFlow);
    if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
        inputParamActLog["activityLog_activityId"] = "143";
        inputParamActLog["activityLog_flex1"] = "Step 2/4 - Confirm Issue new Debit Card";
        inputParamActLog["activityLog_flex2"] = "Linked Account = "+maskAccount(blockNReissueAccId);
        inputParamActLog["activityLog_flex3"] = "NewProduct = "+gblofferCardDetails["newProductID"]+"+"+gblofferCardDetails["productName"];
        inputParamActLog["activityLog_flex4"] = "Card = "+gblofferCardDetails["CardName"];
        inputParamActLog["activityLog_flex5"] = "";
        kony.print("in activity");
        showLoadingScreen();
        invokeServiceSecureAsync("reissueCardActivityLog", inputParamActLog, callbackReissueAct);
    } else {
        showBlockCardPwdPopup();
    }
}

function callbackReissueAct(status, resulttable) {
    kony.print("in call back activity");
    //kony.print("status="+status+",result=" + JSON.stringify(resulttable));
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            showBlockCardPwdPopup();

        } else {
            dismissLoadingScreen();
        }
    } else {
        dismissLoadingScreen();
    }
}

function onclickNextfrmMBBlockCardCCDNext() {
    frmMBBlockCardRecommendation.show();
}

function onclickManageCard() {
	showLoadingScreen();
	preShowMBCardList();
}

//------------

function frmMBManageCardFlxBlockCardOnClick() {
    kony.print("==> frmMBManageCardFlxBlockCardOnClick");
    if (gblSelectedCard["cardType"] == "Debit Card") {
        gblCCDBCardFlow = "DEBIT_CARD_BLOCK";
        frmMBBlockDebitCardConfirm.show();
    } else {
        gblCCDBCardFlow = "CREDIT_CARD_BLOCK";
        frmMBNewTncBlockCCShow();
    }
    // if (gblCCDBCardFlow == "CREDIT_CARD_BLOCK") {
    //        frmMBNewTncBlockCCShow();
    //    } else if (gblCCDBCardFlow == "DEBIT_CARD_BLOCK") {
    //        frmMBBlockDebitCardConfirm.show();
    //    } else if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
    //        frmMBNewTncNewDCShow();
    //    }
}
/* -- added to show block ribbon on blocked card 
	  Author: Brajesh
*/
function animateribbon() {
    var trans100 = kony.ui.makeAffineTransform();
    if (undefined != frmMBBlockCardSuccess.lblRibbon) {
        trans100.rotate(-45);
        frmMBBlockCardSuccess.lblRibbon.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "transform": trans100
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.25
        }, {});
    }
    frmMBBlockCardSuccess.lblRibbon.text = "Blocked"; //kony.i18n.getLocalizedString("Blocked");
}

function addblockribbon() {
	
    var lblRibbon = new kony.ui.Label({
        "id": "lblRibbon",
        "top": "0%", //10.41
        "right": "-6.0%", //-11.20
        "width": "60%",
        "height": "14.0%",
        "zIndex": 3,
        "isVisible": true,
        "text": "BLOCKED",
        "skin": "lblRibbonRed"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false,
        "wrapping": constants.WIDGET_TEXT_WORD_WRAP
    });
    frmMBBlockCardSuccess.flxCardImage.add(lblRibbon);
}

function showBlockCardPwdPopup() {
  if(gblAuthAccessPin == true){
    showAccesspinPopup();
  }else{
    var lblText = kony.i18n.getLocalizedString("transPasswordSub");
    var refNo = "";
    var mobNO = "";
	popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    //dismissLoadingScreen();
    showOTPPopup(lblText, "", "", onClickBlockCardConfirmPop, 3); 
  }
}

function onClickBlockCardConfirmPop(pwd) {
    if (isNotBlank(pwd)) {
        if(gblAuthAccessPin == true){
          popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
          popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
          popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
          popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
          popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;
        }       
        //callBlockCardCompositeService(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text);
        callBlockCardCompositeService(pwd);
    } else {
        setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
        return false;
    }
}

function callBlockCardCompositeService(tranPassword) {
    var inputParam = {};
    inputParam["password"] = tranPassword;
    canReissueCard = "N";
    kony.print("callBlockCardCompositeService gblCCDBCardFlow = "+gblCCDBCardFlow);
    if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE") {
        kony.print("in debit card reissue flow");
        inputParam["accountNumber"] = blockNReissueAccId;
        inputParam["issueNewBinNo"] = gblofferCardDetails["newCardBinNo"];
        if(gblofferCardDetails["EntranceFee"]=="" || gblofferCardDetails["EntranceFee"]==null){
        	gblofferCardDetails["EntranceFee"]="0";
        }
        inputParam["entranceFee"] =gblofferCardDetails["EntranceFee"];
        //inputParam["productID"] =blockNReissueProductId;
        showLoadingScreen();
        invokeServiceSecureAsync("reissueCardCompositeService", inputParam, callBackConfirmCardBlockService);
    } else {
        kony.print("in credit card  flow");
        inputParam["action"] = "blockCard";
        inputParam["cardRefId"] = gblCACardRefNumber; //816004
		inputParam["blockReason"] = "";
        if (gblCCDBCardFlow == "CREDIT_CARD_BLOCK") {
            inputParam["blockReason"] = frmMBBlockCCReason.segReason.selectedItems[0]["hdnlblReasonCode"];
        }
        showLoadingScreen();
        invokeServiceSecureAsync("blockCardCompositeService", inputParam, callBackConfirmCardBlockService);
    }
}

function callBackConfirmCardBlockService(status, resulttable) {
    kony.print("in callback reissue");
    if (status == 400) {
		
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
			popupTractPwd.dismiss();
            onClickCancelAccessPin();
            if (isNotBlank(resulttable["openActBusinessHrsFlag"]) && resulttable["openActBusinessHrsFlag"] == "false") {
                var CA_StartTime = resulttable["openActStartTime"];
                var CA_EndTime = resulttable["openActEndTime"];
                var errorMsg = kony.i18n.getLocalizedString("Card_Err_ServiceHour");
                errorMsg = errorMsg.replace("{service_hours}", CA_StartTime + " - " + CA_EndTime);
                showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
                return false;
            } else {
            	 kony.print("in callback else reissue");
            	blockNReissueAccId =  isNotBlank(resulttable["accId"]) ? resulttable["accId"] : "";
            	canReissueCard = isNotBlank(resulttable["canReissueCard"]) ? resulttable["canReissueCard"] : "N";
            	//blockNReissueProductId =  isNotBlank(resulttable["productId"]) ? resulttable["productId"] : "";
                blockNReissueSuccess = true;
                blockNReissueTryagain = false;
                frmMBBlockCardSuccess.show();
            }
        } else if (resulttable["opstatus"] == 1) {
            popupTractPwd.dismiss();
            onClickCancelAccessPin();
            dismissLoadingScreen();
            if (resulttable["tryAgainFlag"] == "Y") {
                blockNReissueSuccess = false;
                blockNReissueTryagain = true;
                frmMBBlockCardSuccess.show();
            } else if (resulttable["tryAgainFlag"] == "N") {
                blockNReissueSuccess = false;
                blockNReissueTryagain = false;
                frmMBBlockCardSuccess.show();
            } else {
                blockNReissueSuccess = false;
                blockNReissueTryagain = false;
                frmMBBlockCardSuccess.show();
            }
        }else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002"){
               popupEnterAccessPin.lblWrongPin.setVisibility(true);
               kony.print("invalid pin transfer flow"); 
               resetAccessPinImg(resulttable["badLoginCount"]);
        } else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
            onClickCancelAccessPin();
            gotoUVPINLockedScreenPopUp();
        }else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
            setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
            showTranPwdLockedPopup();
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                showAlert("" + resulttable["errMessage"], kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                showAlert("" + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        } else {
            dismissLoadingScreen();
            popupTractPwd.dismiss();
            onClickCancelAccessPin();
            var errorMsgTop = resulttable["errMsg"];
            if (!isNotBlank(resulttable["errMsg"])) {
                errorMsgTop = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
            }
            showAlert(errorMsgTop, kony.i18n.getLocalizedString("info"));
        }
    } else {
        dismissLoadingScreen();
        popupTractPwd.dismiss();
        onClickCancelAccessPin();
        if (status == 300) {
            showAlert(kony.i18n.getLocalizedString("Receipent_alert_Error"), kony.i18n.getLocalizedString("info"));
        } else {
            showAlert("status : " + status + " errMsg: " + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }
    }
}

function getProductofferCard() {
    var inputParams = {};
    var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
    //if (isNotBlank(gblAccountTable["custAcctRec"]) && isNotBlank(gblAccountTable["custAcctRec"][gblIndex])) {
		//blockNReissueAccId = gblAccountTable["custAcctRec"][gblIndex]["accId"];
     if(isNotBlank(selectedRow)){
        blockNReissueAccId = selectedRow["accId"];
  		blockNReissueAccId = blockNReissueAccId.substring(blockNReissueAccId.length-10);
    }
    inputParams["accId"] = blockNReissueAccId; //TODO:get account id
    //inputParams["productID"] = blockNReissueProductId; //productID;//TODO:get productID
    showLoadingScreen();
    invokeServiceSecureAsync("getOfferCardDetails", inputParams, getCardOfferDetailsCallBack);
}

function getCardOfferDetailsCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            gblofferCardDetails = resulttable;
            kony.print("gblofferCardDetails :: " + JSON.stringify(gblofferCardDetails));
            frmMBReIssueDBProduct.lblCardNumber.text = ""; //offerCardDetails["newBin"];
            frmMBReIssueDBProduct.lblCardAccountName.text = ""; //offerCardDetails["productName"];
            frmMBReIssueDBProduct.lblCardNumVal.text = resulttable["CardName"];
            frmMBReIssueDBProduct.imgCard.src = getImageSRC(resulttable["cardImage"]);
            frmMBReIssueDBProduct.show();

        }else{
        	dismissLoadingScreen();
        }
    } else {
        dismissLoadingScreen();
    }
}

function onClickBlockDebitCard() {

    /**
     * If its a debit card we need to log the activity on success of the result we need to navigate to show transaction password popup
     * 
     * For Credit card we need to show directly the transaction password screen.
     *
     */
    var inputParams = {};
	inputParams["action"] = "activityLog_debitCard"; //just log
	inputParams["cardRefId"] = gblCACardRefNumber;
	inputParams["password"] = "";
    inputParams["blockReason"] = "";
    showLoadingScreen();
    invokeServiceSecureAsync("blockCardCompositeService", inputParams, callBackActivityLogBlockDebitCard);
//frmMBBlockCardSuccess.show();
}
function callBackActivityLogBlockDebitCard(status, resulttable){
	if (status == 400 && resulttable["opstatus"] == 0) {
		dismissLoadingScreen();
		showBlockCardPwdPopup();
    } else {
        dismissLoadingScreen();
		//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}

function getDeviceType(){
	var deviceInfo = kony.os.deviceInfo();
	var screenwidth = deviceInfo["deviceWidth"];
	var screenheight = deviceInfo["deviceHeight"];
	var deviceType="";
	//#ifdef android
		if (screenwidth<= 768 && screenheight < 1280) {
			deviceType="AndroidSmall";
		}
	//#endif
	//#ifdef iphone
		if (screenwidth <= 730 && screenheight < 1280) {
			deviceType="iPhone5";
		}else {
			deviceType="iPhone6";
		} 
	//#else
		
	//#endif
	
	return deviceType;
}