







addBillerListLength = 0;
counterValue = 0;
addAnotherValidValue=0;
function addBillersMB() {
	//var segData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
	if(addAnotherValidValue==0)
	addAnotherValidValue = gblCustomerBillList.length;
	tmpBillerName=[];
	tmpBillerNickName=[];
	tmpBillerRef1=[];
	tmpBillerRef2=[];
	tmpBillerCompCode=[];
	tmpBillerRef1Lbl=[];
	tmpBillerRef2Lbl=[];
    counterValue=0;
    addBillerListLength = 0;
	var tmpAddBillData = "";
	if(frmAddTopUpBillerconfrmtn.segConfirmationList.data != null)
		tmpAddBillData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
	if (tmpAddBillData != null)
		addBillerListLength = tmpAddBillData.length;
	var inputParams = {};
	for (i = 0; i < tmpAddBillData.length; i++) {
		inputParams = {};
		inputParams["crmId"] = gblcrmId;
		inputParams["BillerNickName"] = tmpAddBillData[i]["lblCnfrmNickName"];
		
		//tmpBillerNickName = tmpAddBillData[i]["lblCnfrmNickName"];
		inputParams["BillerId"] = tmpAddBillData[i]["BillerId"];
		inputParams["BillerCompcode"] = tmpAddBillData[i]["BillerCompCode"];
		inputParams["ReferenceNumber1"] = tmpAddBillData[i]["lblcnfrmRef1Value"];
		//tmpBillerRef1 = tmpAddBillData[i]["lblcnfrmRef1Value"];
		tmpBillerNickName[i]=tmpAddBillData[i]["lblCnfrmNickName"];
		tmpBillerName[i]=tmpAddBillData[i]["lblNameComp"];
		tmpBillerRef1[i]=tmpAddBillData[i]["lblcnfrmRef1Value"];
		tmpBillerCompCode[i]=tmpAddBillData[i]["BillerCompCode"];
		tmpBillerRef1Lbl[i]=tmpAddBillData[i]["lblConfrmRef1"];
		if (gblMyBillerTopUpBB == 0 && kony.string.equalsIgnoreCase(gblIsRef2RequiredMB, "Y")){
				inputParams["ReferenceNumber2"] = tmpAddBillData[i]["lblcnfrmRef2Value"];
				tmpBillerRef2[i] = tmpAddBillData[i]["lblcnfrmRef2Value"];
				tmpBillerRef2Lbl[i]=tmpAddBillData[i]["lblConfrmRef2"];
			}
		else {
				tmpBillerRef2Lbl[i]="";
				tmpBillerRef2[i]="";
		}
			
		inputParams["httpheaders"] = {};
		inputParams["httpconfigs"] = {};
		//  inputParams["clientDate"] = getCurrentDate();
		invokeServiceSecureAsync("customerBillAdd", inputParams, addBillersCallbackMB);
	}
}

function addBillersCallbackMB(status, callBackResponse) {
	var activityTypeID = "";
	var errorCode = "";
	var activityStatus = "";
	var deviceNickName = "";
	var activityFlexValues1 = "Add";
	var activityFlexValues2 = [];
	var activityFlexValues3 = [];
	var activityFlexValues4 = [];
	var activityFlexValues5 = [];
	
	var logLinkageId = "";
	if (gblMyBillerTopUpBB == 0) {
		activityTypeID = "061";
		//activityFlexValues1 = "addBillerMB";
	} else if (gblMyBillerTopUpBB == 1) {
		activityTypeID = "062";
		activityFlexValues5[counterValue]="";
		//activityFlexValues1 = "addTop-UpMB";
	}
	if (status == 400) {
		if (callBackResponse["opstatus"] == 0) {
			activityStatus = "01";
			frmMyTopUpList.button1010778103103263.setEnabled(true);
			enableAddButtonTopUpBillerConfirm();
			frmMyTopUpList.segSuggestedBillers.setEnabled(true);
			gblBlockBillerTopUpAdd = false;
			if (counterValue < addBillerListLength) {
					activityFlexValues2[counterValue] = tmpBillerName[counterValue]
					activityFlexValues3[counterValue] =  tmpBillerNickName[counterValue]
					activityFlexValues4[counterValue] = tmpBillerRef1[counterValue]
					activityFlexValues5[counterValue] = tmpBillerRef2[counterValue]
					
					activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
					activityFlexValues2[counterValue], activityFlexValues3[counterValue], activityFlexValues4[counterValue], activityFlexValues5[counterValue], logLinkageId);
					var tmpBillerNameNoCompCode =  tmpBillerName[counterValue].substring(0, tmpBillerName[counterValue].lastIndexOf("("));
					
					sendBillTopNotification(tmpBillerNickName[counterValue], tmpBillerNameNoCompCode, tmpBillerCompCode[counterValue], tmpBillerRef1Lbl[counterValue],tmpBillerRef1[counterValue], tmpBillerRef2Lbl[counterValue], tmpBillerRef2[counterValue]);
				var completeSegData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
				frmMyTopUpComplete.segComplete.setData(completeSegData);
				frmMyTopUpComplete.show();
				//TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
				frmAddTopUpBillerconfrmtn.segConfirmationList.data = [];
				TMBUtil.DestroyForm(frmAddTopUpToMB);
				addAnotherValidValue =  addAnotherValidValue + 1;
				counterValue = counterValue + 1;
				
			} else {
				counterValue = 0;
			}
			gblFlagConfirmDataAddedMB = 0;
			
		} else {
		
			alert("MB Biller Add Failed");
			TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
			TMBUtil.DestroyForm(frmAddTopUpToMB);
			activityStatus = "02";
			if (counterValue < addBillerListLength) {
					activityFlexValues2[counterValue] = tmpBillerName[counterValue]
					activityFlexValues3[counterValue] =  tmpBillerNickName[counterValue]
					activityFlexValues4[counterValue] = tmpBillerRef1[counterValue]
					activityFlexValues5[counterValue] = tmpBillerRef2[counterValue]
					
					activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
					activityFlexValues2[counterValue], activityFlexValues3[counterValue], activityFlexValues4[counterValue], activityFlexValues5[counterValue], logLinkageId);
					
				var completeSegData = frmAddTopUpBillerconfrmtn.segConfirmationList.data;
				frmMyTopUpComplete.segComplete.setData(completeSegData);
				frmMyTopUpComplete.show();
				//TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
				frmAddTopUpBillerconfrmtn.segConfirmationList.data = [];
				TMBUtil.DestroyForm(frmAddTopUpToMB);
				counterValue = counterValue + 1;
			} else {
				counterValue = 0;
			}		
		}
		//activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
		//	activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
	} else {
		if (status == 300) {
			counterValue = 1;
			TMBUtil.DestroyForm(frmAddTopUpToMB);
			TMBUtil.DestroyForm(frmAddTopUpBillerconfrmtn);
			alert("MB Biller Add Failed");
		}
	}
}
