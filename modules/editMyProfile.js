







var partyUpdate = "";
var gblnewStateValue = "";
var t = [0, 0];
var ccode;
var GblMobileAL;
var gblFBCode = "";
var GBLemailALOld;
var gblViewsubdistrictValue = "";
var gblViewdistrictValue = "";
var addressedited = false;
var m = 0;
var serviceProfileFlag = "";
var idAddrFieldObj = {};
var lblAddrField;
var caseTrans;
var gblEditBase64List = "";
var gblEditPersonalizedIdList = "";
var lblbankListThaiField;
var gblEditPhotoSource = "png";
//var profileedited = "";
var customerName;
var profileDeviceNameFlag;
var profilePicFlag;
var profileEmailFlag;
var profileAddrFlag;
var editbuttonflag;
var profileedit;
var completeicon = false;
var callLimitedit = false;
var gblMyProfilepic;
var gblMyProfilepicURL;
var callNotifyEditFlag;
var gblAddress;
var gblEBMaxLimitAmtCurrentOld;
var txt1;
var txt2;
//var tempemail;
var province;
var district;
var subdistrict;
var zipcode;
var gblcountryCode = "";
var gblregcountryCode = "";
var StateValue;
var districtValue;
var subdistrictValue;
var zipcodeValue;
var templimit;
var flagnot1 = false;
var flagnot2 = false;
var flagnot3 = false;
var flagnot4 = false;
var flagbfnot1 = false;
var flagbfnot2 = false;
var flagbfnot3 = false;
var flagbfnot4 = false;
var profilePicFlagSPA = false;
var gblStateEng = "";
var gblDistEng = "";
var gblSubDistEng = "";
var provinceCD = "";
var DistrictCD = "";
var maskedNewMob = "";
var s2sBusinessHrsFlag = "";
var s2sStartTime = "";
var s2sEndTime = "";
var ibUserStatusId = "";
var mbUserStatusId = "";
var ibStatusFlag = "";
var mbStatusFlag = "";
var s2sstatuscode = "";
var s2sstatusdesc = "";
var gblZipEng = "";
var selectState = "";
var selectDist = "";
var selectSubDist = "";
var selectZip = "";
var confirmEdit = false;
var mptemp = "preshow";
var resulttableState = []; 
var resulttableDist	= [];	
var resulttableSubDist = [];	
var resulttableStateZip	= [];
var changeState = false;
var changedist = false;
var changeSubDist = false;
var changeZip = false;
var imageDeleted = false;

function frmViewMyProfilePreShow() {
  confirmEdit = false;
  isMenuShown = false;
  imageDeleted = false;
  if (completeicon == true) {
    completeicon = false;
  }
  gblMyProfilepic = "";
  var randomnum = Math.floor((Math.random() * 10000) + 1);
  gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&modIdentifier=MyProfile&dummy=" + randomnum;
  if (gblDeviceNickName == null || gblDeviceNickName == "") {
    frmMyProfiles.lblMyDeviceVal.text = "My Device";
  } else {
    frmMyProfiles.lblMyDeviceVal.text = gblDeviceNickName;
  }
  if (!isNotBlank(kony.store.getItem("cachedProfilePic"))) {
    frmMyProfiles.imgProfile.src = "avatar.png";
  } else {
    frmMyProfiles.imgProfile.base64 = kony.store.getItem("cachedProfilePic");
  }

  if (kony.i18n.getCurrentLocale() == "th_TH") {
    frmMyProfiles.lblProfileDescription.text = gblCustomerNameTh;
  } else {
    frmMyProfiles.lblProfileDescription.text = gblCustomerName;
  }
  frmMyProfiles.lblMobileVal.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
  frmMyProfiles.lblMyProfileHeader.text = kony.i18n.getLocalizedString("keyMyProfile");
  frmMyProfiles.lblEmailHeader.text = kony.i18n.getLocalizedString("keyEmail");
  frmMyProfiles.lblEmailVal.text = gblEmailAddr;
  frmMyProfiles.lblMobileHeader.text = kony.i18n.getLocalizedString("keyMyMobileNumber");
  frmMyProfiles.lblContactAddressTxt.text = kony.i18n.getLocalizedString("keyContactAddress");
  frmMyProfiles.lblRegisterAddress.text = kony.i18n.getLocalizedString("keyRegAddress");
  frmMyProfiles.lblAccessPinHeader.text = kony.i18n.getLocalizedString("keyChangeAccessPin");
  frmMyProfiles.lblContactVal.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
  frmMyProfiles.lblRegisterAddressVal.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue + " " + gblregdistrictValue + " " + gblregStateValue + " " + gblregzipcodeValue + " " + gblregcountryCode;
  frmMyProfiles.lblfbidstudio1.text = gblFacebookId;//Modified by Studio Viz
  var comcurr = ProfileCommFormat(gblEBMaxLimitAmtCurrent);
  frmMyProfiles.lblDailytransLimitvalue.text = comcurr + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  var commaamt = ProfileCommFormat(gblEBMaxLimitAmtHist);
  frmChangeMobNoTransLimitMB.lblMaxLimitDisplay.text = getDailyChangeLimtNote(ProfileCommFormat(GLOBAL_MAX_LIMIT_HIST), ProfileCommFormat(GLOBAL_CEILING_LIMIT));
  if(gblShowAnyIDRegistration == "true"){
    frmMyProfile.line47502979411519.setVisibility(true);
    frmMyProfile.hboxSetID.setVisibility(true);
  }else{
    frmMyProfile.line47502979411519.setVisibility(false);
    frmMyProfile.hboxSetID.setVisibility(false);
  }
}

function getDailyChangeLimtNote(maxLimit, ceilLimit) {

  var dailyLimitChangeNoteMsg = kony.i18n.getLocalizedString("keyDailyLimitChangeNote");
  dailyLimitChangeNoteMsg = replaceAll(dailyLimitChangeNoteMsg, "{maxLimit}", maxLimit);
  dailyLimitChangeNoteMsg = replaceAll(dailyLimitChangeNoteMsg, "{ceilLimit}", ceilLimit);

  return dailyLimitChangeNoteMsg;
}

function invokeCameraFromEditProfilePopup() {
  gblEditPhotoSource = "png";
  var rawBytesString = popUploadPic.camera1.rawBytes;
  gblEditBase64List = "";
  gblEditBase64List = kony.convertToBase64(rawBytesString);

  var curSize = (gblEditBase64List.length - 814)/1.37;		

  if (curSize > Gbl_Image_Size_Limit) {
    alert("" + kony.i18n.getLocalizedString("keyImageSizeTooLarge"));
    return;
  }
  if (kony.application.getCurrentForm().id == "frmeditMyProfile") {
    if (flowSpa) {
      gblEditBase64List = frmeditMyProfile.imgprofpic.base64;
      //gblEditBase64List = kony.convertToBase64(rawBytesString);
      //gblEditBase64List = gblEditBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
      gblEditPhotoSource = "png";
      //
    } else {
      //gblEditBase64List = kony.convertToBase64(rawBytesString);
      frmeditMyProfile.imgprofpic.rawBytes = rawBytesString;
      gblEditBase64List = gblEditBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
      frmeditMyProfile.scrollboxMain.scrollToEnd();
    }
  }
  popUploadPic.dismiss();
}

function deleteProfPic() {
  showLoadingScreen();
  inputParam = {};
  inputParam["crmId"] = gblcrmId;
  invokeServiceSecureAsync("imageDeleteService", inputParam, imageDeleteServiceServiceCallback)
}

function imageDeleteServiceServiceCallback(status, resulttable) {

  if (status == 400) {
    if (resulttable["deleteOpStatus"] == "Delete Success") {
      gblMyProfilepic = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
      dismissLoadingScreen();
      var currentForm = kony.application.getCurrentForm();
      if(isNotBlank(currentForm.id) && currentForm.id == "frmAccountSummary"){
        frmAccountSummary.imgProfile.src="avatar.png";
      }else{
      frmMyProfiles.imgProfile.src = "avatar.png";
      }
      imageDeleted = true;
      kony.store.removeItem("cachedProfilePic"); //Deleting the profile pic form cache.

    } else if (resulttable["deleteOpStatus"] == "Delete Failed") {
      dismissLoadingScreen();
      showAlert("Image to be deleted not found", "error");
      return false;
    } else {
      dismissLoadingScreen();
      return false;
    }
  } else {
    if (status == 300) {
      showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
      dismissLoadingScreen();
    }
  }
}

function saveEditedAddrMyProfile() {
  frmMyProfile.hbxexpandAddr.setVisibility(true);
  if ((frmeditMyProfile.lblsubdistrict.text == kony.i18n.getLocalizedString('keyIBPleaseSelect')) || (frmeditMyProfile.lbldistrict.text == kony.i18n.getLocalizedString('keyIBPleaseSelect')) || (frmeditMyProfile.lblzipcode.text == kony.i18n.getLocalizedString('keyIBPleaseSelect')) || (frmeditMyProfile.lblProvince.text == kony.i18n.getLocalizedString('keyIBPleaseSelect'))) {
    addressedited = false;
    var mess = kony.i18n.getLocalizedString("keyPleaseSelState");
    var mess1 = kony.i18n.getLocalizedString("keyPleaseSelDist");
    var mess2 = kony.i18n.getLocalizedString("keyPleaseSelSubDist");
    var mess3 = kony.i18n.getLocalizedString("keyPleaseSelZip");
    if(frmeditMyProfile.lblProvince.text == kony.i18n.getLocalizedString('keyIBPleaseSelect'))
      showAlert(mess, "Error");
    else if(frmeditMyProfile.lbldistrict.text == kony.i18n.getLocalizedString('keyIBPleaseSelect'))
      showAlert(mess1, "Error");
    else if(frmeditMyProfile.lblsubdistrict.text == kony.i18n.getLocalizedString('keyIBPleaseSelect'))
      showAlert(mess2, "Error");
    else if(frmeditMyProfile.lblzipcode.text == kony.i18n.getLocalizedString('keyIBPleaseSelect'))
      showAlert(mess3, "Error");			
    return false;
  }
  if (((frmeditMyProfile.lblsubdistrict.text) == gblsubdistrictValue || (frmeditMyProfile.lblsubdistrict.text) == gblSubDistEng) && ((frmeditMyProfile.lbldistrict.text) == gbldistrictValue || (frmeditMyProfile.lbldistrict.text) == gblDistEng) && ((frmeditMyProfile.lblzipcode.text) == gblzipcodeValue) && ((frmeditMyProfile.lblProvince.text) == gblStateValue || (frmeditMyProfile.lblProvince.text) == gblStateEng) && ((frmeditMyProfile.txtAddress1.text) == gblAddress1Value) && ((frmeditMyProfile.txtAddress2.text) == gblAddress2Value)) {

    addressedited = true;
    return true;
  } else if (((frmeditMyProfile.lblProvince.text) != gblStateValue && (frmeditMyProfile.lblProvince.text) != gblStateEng) || ((frmeditMyProfile.lbldistrict.text) != gbldistrictValue && (frmeditMyProfile.lbldistrict.text) != gblDistEng) || ((frmeditMyProfile.lblzipcode.text) != gblzipcodeValue) || ((frmeditMyProfile.lblsubdistrict.text) != gblsubdistrictValue && (frmeditMyProfile.lblsubdistrict.text) != gblSubDistEng))
  {
    if(((frmeditMyProfile.txtAddress1.text) != null && (frmeditMyProfile.txtAddress1.text) != "") && ((frmeditMyProfile.txtAddress2.text) != null && (frmeditMyProfile.txtAddress2.text) != "")) {
      addressedited = false;
      profileAddrFlag = "addr";
      return true;
    }
  }	
  else if (((frmeditMyProfile.lblsubdistrict.text) == gblsubdistrictValue || (frmeditMyProfile.lblsubdistrict.text) == gblSubDistEng) && ((frmeditMyProfile.lbldistrict.text) == gbldistrictValue || (frmeditMyProfile.lbldistrict.text) == gblDistEng) && ((frmeditMyProfile.lblzipcode.text) == gblzipcodeValue) && ((frmeditMyProfile.lblProvince.text) == gblStateValue || (frmeditMyProfile.lblProvince.text) == gblStateEng) && ((frmeditMyProfile.txtAddress1.text) != null && (frmeditMyProfile.txtAddress1.text) != "") && ((frmeditMyProfile.txtAddress2.text) != null && (frmeditMyProfile.txtAddress2.text) != "")) {
    addressedited = true;
    profileAddrFlag = "addr";
    return true; //tag
  } else {
    addressedited = false;
    var mess = kony.i18n.getLocalizedString("keyenterDetails");

    if ((frmeditMyProfile.txtAddress1.text) == null || (frmeditMyProfile.txtAddress1.text) == "") {
      frmeditMyProfile.txtAddress1.skin = txtErrorBG;
      frmeditMyProfile.txtAddress1.focusSkin = txtErrorBG;
    }
    if ((frmeditMyProfile.txtAddress2.text) == null || (frmeditMyProfile.txtAddress2.text) == "") {
      frmeditMyProfile.txtAddress2.skin = txtErrorBG;
      frmeditMyProfile.txtAddress2.focusSkin = txtErrorBG;
    }
    showAlert(mess, "Error");
    return false;
  }
}


function validateMobNuProf() {
  var mobNu = frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text;
  frmChangeMobNoTransLimitMB.txtChangeMobileNumber.skin = tbxPopupBlue;
  //popUpCallCancel.show();
  if(mobNu != null && mobNu != "")
  {
    mobNu = mobNu.toString().replace(/-/g, "");
    var isNum = kony.string.isNumeric(mobNu);
    var res = mobNu.substring(0, 2);
    var isNum = kony.string.isNumeric(mobNu);
  }
  if (mobNu == null || mobNu == "" || mobNu.length < 10) {
    var number1 = kony.i18n.getLocalizedString("keyenternumber");
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.skin = txtErrorBG;
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.focusSkin = txtErrorBG;
    showAlertWithCallBack(number1, kony.i18n.getLocalizedString("info"),loadingScreenDismiss);
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.setFocus(true);
    return false;

  } else if (!isNum) {
    var number1 = kony.i18n.getLocalizedString("keyenternumber");
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.skin = txtErrorBG;
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.focusSkin = txtErrorBG;
    showAlertWithCallBack(number1, kony.i18n.getLocalizedString("info"),loadingScreenDismiss);
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.setFocus(true);
    return false;
  } else if (Gbl_StartDigsMobileNum.indexOf(res) < 0) {
    var number1 = kony.i18n.getLocalizedString("keyIncorrectMobileNo"); //"Enter a number starting with 09 or 08"
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.skin = txtErrorBG;
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.focusSkin = txtErrorBG;
    showAlertWithCallBack(number1, kony.i18n.getLocalizedString("info"),loadingScreenDismiss);
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.setFocus(true);
    return false;
  } else if (mobNu == gblPHONENUMBER) {
    var number1 = kony.i18n.getLocalizedString("MIB_ErrExistMob");
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.skin = txtErrorBG;
    frmChangeMobNoTransLimitMB.txtChangeMobileNumber.focusSkin = txtErrorBG;
    showAlertWithCallBack(number1, kony.i18n.getLocalizedString("info"),chngMobileNumberCallBck);
    return false;   
  } else {
    showLoadingScreen();
    gblUpdateProfileFlag = "mobile";
    gblPhoneNumberReq = mobNu;
    onClickChnageSaveConfirm(); //uncomment this
    //showOTPPopup(kony.i18n.getLocalizedString("keyOTP"), "ABCD", "xxx-xxx-" + gblPHONENUMBER.substring(6, 10), otpNewMobNumberValidation(gblPhoneNumberReq), 1)
  }
}

function chngMobileNumberCallBck(){
  dismissLoadingScreen();
  frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text="";
  frmChangeMobNoTransLimitMB.txtChangeMobileNumber.setFocus(true);
}

function loadingScreenDismiss(){
  dismissLoadingScreen();
}

function openGalleryinEditProfilePopUp() {
  //gblEditPhotoSource = "jpeg";
  gblEditPhotoSource = "png";
  try {
    function onselectioncallback(rawbytes) {
      if (rawbytes == null) {
        showAlertRcMB(kony.i18n.getLocalizedString("KeyImageNotSelected"), kony.i18n.getLocalizedString("info"), "info")
        return;
      }
      gblEditBase64List = "";
      gblEditBase64List = kony.convertToBase64(rawbytes);

      var curSize = (gblEditBase64List.length - 814)/1.37;

      if (curSize > Gbl_Image_Size_Limit) {
        alert("" + kony.i18n.getLocalizedString("keyImageSizeTooLarge"));
        return;
      }
      frmMyProfiles.imgProfile.rawBytes = rawbytes;
      gblEditBase64List = gblEditBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
      kony.store.setItem("cachedProfilePic",gblEditBase64List);
    }
    var querycontext = {
      mimetype: "image/*"
    };
    returnStatus = kony.phone.openMediaGallery(onselectioncallback, querycontext);
  } catch (err) {
    showAlertRcMB(kony.i18n.getLocalizedString("KeyMediaGalleryError"), kony.i18n.getLocalizedString("info"), "info")
  }
}

function onClickMoreMyProfile() {
  var status = frmMyProfile.lnkExpand.text;

  if (status == kony.i18n.getLocalizedString("More")) {

    frmMyProfile.hbxexpandAddr.setVisibility(true);
    frmMyProfile.lblContactAdd1.text = kony.i18n.getLocalizedString("keyContactAddress");
    frmMyProfile.lblContactAdd2.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue;
    frmMyProfile.lblContactAdd3.text = gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
    frmMyProfile.lblRegAddress1.text = kony.i18n.getLocalizedString("keyRegAddress");
    frmMyProfile.lblRegAddress2.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue;
    frmMyProfile.lblRegAddress3.text = gblregdistrictValue + " " + gblregStateValue + " " + gblregzipcodeValue + " " + gblregcountryCode;
    frmMyProfile.hbxExpand.lnkExpand.text = kony.i18n.getLocalizedString("Hide");
  } else {

    frmMyProfile.lnkExpand.text = kony.i18n.getLocalizedString("More");
    frmMyProfile.hbxexpandAddr.setVisibility(false);
  }
}
/*
*************************************************************************************
    	Module	: onClickOTPRequest
		Author  : Kony
		Purpose : Defining onclick for next button in activation confirmation page
****************************************************************************************
*/

function onClickChnageSaveConfirm() {
  frmChangeMobNoTransLimitMB.txtChangeMobileNumber.skin = tbxPopupBlue;
  gblOTPFlag = true;
  gblOnClickReq = false;
  try {
    kony.timer.cancel("otpTimer");
  } catch (e) {

  }
  if (flowSpa) {
    spaChnage = null
    caseTrans = "Number";
    spaChangeMobToeknExchng();
  } else {
    changeMobileInSessionSPA(gblPhoneNumberReq)
    //generateOTPNewMobNumberService();
  }
}


function spaChangeMobToeknExchng()
{
  var inputParam = [];
  showLoadingScreen();
  invokeServiceSecureAsync("tokenSwitching", inputParam, spaChangeMobToeknExchngCallbackfunction);
}


function spaChangeMobToeknExchngCallbackfunction(status,resulttable){
  if (status == 400) {
    if(resulttable["opstatus"] == 0){
      requestOTPTxnspa();
    }else{
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
    }
  }
}
/*
*************************************************************************************
		Module	: onClickActiRequestOtp
		Author  : Kony
		Purpose : Defining onclick for Request button for OTP pop up
****************************************************************************************
*/

function onClickChangeMobRequestOtp() {
  gblOTPFlag = true;
  gblOnClickReq = true;//
  //    popupTractPwd.btnPopUpTractCancel.skin = btnDisabledGray;
  //    popupTractPwd.btnPopUpTractCancel.focusSkin = btnDisabledGray;
  //    popupTractPwd.btnPopUpTractCancel.setEnabled(false);
  popupTractPwd.btnOtpRequest.skin = btnDisabledGray;
  popupTractPwd.btnOtpRequest.focusSkin = btnDisabledGray;
  popupTractPwd.btnOtpRequest.setEnabled(false);
  generateOTPNewMobNumberService();
}

function otpTimerChangMobCallBack() {
  //popupTractPwd.btnPopUpTractCancel.skin = btnLightBlue;
  //    popupTractPwd.btnPopUpTractCancel.setEnabled(true);
  //    popupTractPwd.btnPopUpTractCancel.onClick = onClickChangeMobRequestOtp;
  popupTractPwd.btnOtpRequest.skin = btnLightBlue;
  popupTractPwd.btnOtpRequest.setEnabled(true);
  popupTractPwd.btnOtpRequest.onClick = onClickChangeMobRequestOtp;
  gblOTPFlag = true;
  try {
    kony.timer.cancel("otpTimer")
  } catch (e) {

  }
}

//paste here
function generateOTPNewMobNumberService() {
  if (gblOTPFlag) {
    //popupTractPwd.destroy();
    var mobNu = frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text;
    gblPhoneNumberReq = mobNu.toString().replace(/-/g, "");
    var inputParams = {
      Channel: "ChangeMobileNumberNew",
      Recipient_Name: customerName,
      retryCounterRequestOTP: gblRetryCountRequestOTP
    }; 
    kony.print("inputParams for generateOTP ----->" + JSON.stringify(inputParams)) ;      
    invokeServiceSecureAsync("generateOTP", inputParams, generateOTPNewMobNumberCallBack);
  }
}

function generateOTPNewMobNumberCallBack(status, resulttable) {

  kony.print("1111111111111111------> " + JSON.stringify(resulttable));
  if (status == 400) //success response
  {
    kony.print("inside status generateOTPNewMobNumberCallBack");

    if (resulttable["opstatus"] == 0) {
      // Added below condition if we get any error code with opstatus as 0
      if(resulttable["errCode"] != undefined && resulttable["errCode"] != null){
        if (resulttable["errCode"] == "GenOTPRtyErr00001") {
          kony.application.dismissLoadingScreen();
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
          gblOTPFlag = false;
          return false;
        }else if (resulttable["errCode"] == "GenOTPRtyErr00002") {
          kony.application.dismissLoadingScreen();
          showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
          return false;
        }

      }else{
        gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
        var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
        reqOtpTimer = kony.os.toNumber(reqOtpTimer);
        gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
        kony.timer.schedule("otpTimer", otpTimerChangMobCallBack, reqOtpTimer, false);
        var otptext = popupTractPwd.txtOTP.text;
        if (gblOnClickReq == false) {
          //showOTPPopup(kony.i18n.getLocalizedString("keyOTP"), "ABCD", "xxx-xxx-" + gblPhoneNumberReq.substring(6, 10), otpNewMobNumberValidation, 1)
          showOTPPopupForOTPValidation(kony.i18n.getLocalizedString("keyOTP"), "ABCD", maskedNewMob, otpNewMobNumberValidation);
        }
        var refVal="";
        for(var d=0;d < resulttable["Collection1"].length;d++){
          if(resulttable["Collection1"][d]["keyName"] == "pac"){
            refVal=resulttable["Collection1"][d]["ValueString"];
            break;
          }
        }
        popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + " " + refVal;
        //popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + resulttable["pac"];
        if (!flowSpa) {
          popupTractPwd.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
          popupTractPwd.lblPopupTract4.text = "xxx-xxx-" + maskedNewMob.substring(6, 10);
        }
        popupTractPwd.lblPopupTract7.text = "";
        otpConfirmEnable();
        gblOTPFlag = false;

      }
    } else {
      //popupTractPwd.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + resulttable["pac"];
      gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
      if(resulttable["requestOTPEnableTime"] == "undefined" || resulttable["requestOTPEnableTime"] == null || resulttable["requestOTPEnableTime"] == "")
        var reqOtpTimer = kony.os.toNumber(60);
      else{

      }
      //var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
      // gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
      // kony.timer.schedule("otpTimer", otpTimerCallBack, reqOtpTimer, false);

      if (resulttable["errCode"] == "GenOTPRtyErr00001") {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        gblOTPFlag = false;
        return false;
      }else if (resulttable["errCode"] == "GenOTPRtyErr00002") {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
        return false;
      }
      else {
        alert(" " + resulttable["errMsg"]);
        kony.application.dismissLoadingScreen();
        gblOTPFlag = false;
        return false;
      }
    }
  }
}

function otpNewMobNumberValidation() {
  showLoadingScreen();
  otpCodePattStr = "^[0-9]{" + 6 + "}$";
  var otpCodePatt = new RegExp(otpCodePattStr, "g");
  var text = popupTractPwd.txtOTP.text;
  var resultOtpCodePatt = otpCodePatt.test(text);
  if (resultOtpCodePatt) {
    if (flowSpa) {
      caseTrans = "Number";
      callbackEditmyprofileSpa();
    } else {
      var inputParam = {};
      inputParam["password"] = text;
      inputParam["flagVerify"] = "NEW";
      inputParam["moduleParam"] = "changeMobileNum";
      inputParam["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
      kony.print("before invoke service newChangeMobileComposite");
      invokeServiceSecureAsync("newChangeMobileComposite", inputParam, NewMobileNumOTPVerifycallBack)
    }
  } else {
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidOTP");
    popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
    popupTractPwd.lblPopupTract7.setVisibility(true);
    popupTractPwd.lblPopupTract2.text = "";
    popupTractPwd.lblPopupTract4.text = "";
    kony.application.dismissLoadingScreen();
    return false;
  }
}

function successMobileChangeActionsMB(){
  if(gblOpenAccountFlow){
    gblOpenAccountFlow = false;
    frmCheckContactInfo.show();
  }else if(gblRegisterWithAnyId == "true"){
    gblRegisterWithAnyId = "false";
    //frmMBActivateAnyId.show();   -- MIB2270
    callAnyIDInq();
  }else if(gblOpenActSavingCareEditCont){
    gblOpenActSavingCareEditCont=false;
    frmMBSavingsCareContactInfo.show();
  }else if(gblCCDBCardFlow=="DEBIT_CARD_REISSUE"){
    invokePartyInqService();
  }else{
    gblOpenAccountFlow = false;
    gblRegisterWithAnyId = "false";
    frmMyProfiles.show();
  }
}

function NewMobileNumOTPVerifycallBack(status, resulttable) {
  if (status == 400) {
    if (!flowSpa) {
      popupTractPwd.txtOTP.text = "";
      popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
    //kony.print("@@@@@@@@@@@@@@@@@@@@@@@@@@" + JSON.stringify(resulttable));
    if (resulttable["errCode"] == "VrfyOTPErr00001" || resulttable["code"] == "10020") {
      gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
      popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("wrongOTP");
      popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
      popupTractPwd.lblPopupTract7.setVisibility(true);
      popupTractPwd.lblPopupTract2.text = "";
      popupTractPwd.lblPopupTract4.text = "";
      popupTractPwd.txtOTP.text = "";
      kony.application.dismissLoadingScreen();
      return false;
    } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
      gblVerifyOTPCounter = "0";
      gblRetryCountRequestOTP = "0";                
      kony.application.dismissLoadingScreen();
      popupTractPwd.dismiss();                
      showTranPwdLockedPopup();
      return false;
    } else if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
      showTranPwdLockedPopup();
      popupTractPwd.dismiss();
      return false;
    } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
      return false;
    } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
      return false;
    } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
      return false;
    }
    if (resulttable["opstatus"] == 0) { 
      gblVerifyOTPCounter = "0";
      gblRetryCountRequestOTP = "0";
      dismissLoadingScreen();
      popupTractPwd.dismiss();
      caseTrans = "Number";
      kony.timer.cancel("otpTimer");
      completeicon = true;	            
      if(resulttable["newMobileNumber"] != null){
        gblPHONENUMBER = resulttable["newMobileNumber"];
      }
      var updateMobileMessage = kony.i18n.getLocalizedString(resulttable["updateMobileMessage"]);
      showAlertWithCallBack(updateMobileMessage, kony.i18n.getLocalizedString("info"), successMobileChangeActionsMB);

    } else if(undefined != resulttable["updateMobileMessage"]){
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString(resulttable["updateMobileMessage"]), kony.i18n.getLocalizedString("info"));
      return false;
    } else{
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
    kony.application.dismissLoadingScreen();
  }
}


function changeLimitNext()
{
  if(flowSpa)
  {
    spaChangeLimitToeknExchng();
  }
  else
  {
    validateTransDetailProf();
  }

}

function spaChangeLimitToeknExchng()
{
  var inputParam = [];
  showLoadingScreen();
  invokeServiceSecureAsync("tokenSwitching", inputParam, spaChangeLimitToeknExchngCallbackfunction);
}


function spaChangeLimitToeknExchngCallbackfunction(status,resulttable){
  if (status == 400) {
    if(resulttable["opstatus"] == 0){
      validateTransDetailProf();
    }else{
      dismissLoadingScreen();
      alert(kony.i18n.getLocalizedString("keyErrResponseOne"));
    }
  }
}


function validateTransDetailProf() {
  var val = frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text;
  if(val != null && val != "")
  {
    val =  ProfileCommFormat(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    inputParam = {};
    inputParam["amount"] = gblEBMaxLimitAmtCurrent;
    inputParam["amount2"] = val;

    invokeServiceSecureAsync("SaveChangeLimitParamsInSession", inputParam, changeLimitSessionCallBack);
  }
  else
  {
    var title3 = kony.i18n.getLocalizedString("keyPleaseentercorrectamount");
    frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.skin = txtErrorBG;
    frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtErrorBG;
    showAlert(title3, "Error");
    frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = val;
    dismissLoadingScreen();
    return false;
  }
}

function changeLimitSessionCallBack(status, result) {
  if (status == 400) {


    if (result["opstatus"] == 0) {

      validateTransDetailProfNext();
    }

  }
}

function validateTransDetailProfNext() {
  try{
    frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.skin = txtNormalBG;
    frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtFocusBG;
    var val = frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text;
    var DLimit = "";
    if(val != null && val != "") DLimit = ProfileCommFormat(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    //frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text; // get the entered amount
    if (val == null || val == "") {
      var title3 = kony.i18n.getLocalizedString("keyPleaseentercorrectamount");
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.skin = txtErrorBG;
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtErrorBG;
      showAlert(title3, "Error");
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = val;
      if(flowSpa)	dismissLoadingScreen();
      return false;
    } else if (val.indexOf(',') > 0) // replaces the commas if entered in amount
    {
      val = val.toString().replace(/,/g, "");
    }
    if (!(amountValidationMBOpenAct(val))) //amount validation for alphabets
    {
      var title3 = kony.i18n.getLocalizedString("keyPleaseentercorrectamount");
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.skin = txtErrorBG;
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtErrorBG;
      showAlert(title3, "Error");
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = DLimit;
      if(flowSpa)	dismissLoadingScreen();
      return false;
    } else {
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = ProfileCommFormat(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    }
    if (GLOBAL_INI_MAX_LIMIT_AMT == "" || GLOBAL_INI_MAX_LIMIT_AMT == null || GLOBAL_INI_MAX_LIMIT_AMT == undefined) {
      GLOBAL_INI_MAX_LIMIT_AMT = "4000"; // hardcoded 
      var iniLimit = GLOBAL_INI_MAX_LIMIT_AMT;
      iniLimit = kony.os.toNumber(iniLimit);
    } else {
      var iniLimit = kony.os.toNumber(GLOBAL_INI_MAX_LIMIT_AMT);
    }

    var ceilingLimit = kony.os.toNumber(GLOBAL_CEILING_LIMIT);
    var histLimit = kony.os.toNumber(GLOBAL_MAX_LIMIT_HIST);
    var currLimit = kony.os.toNumber(gblEBMaxLimitAmtCurrent);
    var info1 = kony.i18n.getLocalizedString("info");
    if (val > ceilingLimit) //GREATER THAn mAX limit
    {
      var commaceliingamnt=ProfileCommFormat(GLOBAL_CEILING_LIMIT);
      showAlert(kony.i18n.getLocalizedString("keyGblCeilingLimit") +" "+commaceliingamnt+ kony.i18n.getLocalizedString("currencyThaiBaht"), "Error");
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = ProfileCommFormat(val);
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.skin = txtErrorBG;
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.focusSkin = txtErrorBG;
      if(flowSpa)	dismissLoadingScreen();
      return false;
    } else if ((val <= histLimit) || (val <= iniLimit)) // less than hist limit
    {
      caseTrans = "limit1";
      gblUpdateProfileFlag = "limit";
      frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = ProfileCommFormat(val);
      if (flowSpa) {
        requestOTPTxnspa();
      } else {
        // showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", checkVerifyPWDProfileMB, 3);
        if(gblAuthAccessPin == true)
          showAccesspinPopup();
        else
          showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", CompositeChangeLimit, 3);
      }
    } else if ((val > histLimit) && (val > iniLimit)) { kony.print("MKi validateTransDetailProfNext6");
                                                       var info = "info";
                                                       callLimitedit = true;
                                                       var keymsg = kony.i18n.getLocalizedString("keyDailyLimitExceedErrMsg");
                                                       keymsg = replaceAll(keymsg, "{maxLimit}", ProfileCommFormat(GLOBAL_MAX_LIMIT_HIST));
                                                       caseTrans = "limit3";
                                                       showAlertWithCallBack(keymsg, info, callShowOTPPopUP);
                                                       //frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = val;
                                                      } else if ((val > histLimit) && (val < iniLimit)) //limit sent for approval 
                                                      { kony.print("MKi validateTransDetailProfNext7");
                                                       var title2 = kony.i18n.getLocalizedString("keyLimitExceededError");
                                                       //frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = val;
                                                       frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text = ProfileCommFormat(val);
                                                       caseTrans = "limit2";
                                                       if (flowSpa) {
                                                         requestOTPTxnspa();
                                                       } else {
                                                         // showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", checkVerifyPWDProfileMB, 3);
                                                         if(gblAuthAccessPin == true)
                                                           showAccesspinPopup();
                                                         else
                                                           showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", CompositeChangeLimit, 3);
                                                       }
                                                      }

  }
  catch(ex)
  {
    kony.print("MKI ex:"+ex.message);
  }
}

function generateOtpUserchangeMobileSpa() {

  Channel = "ChangeMobileNumber";
  var inputParam = {};
  if (gblOTPFlag) {
    inputParam["Channel"] = Channel;
    inputParam["locale"] = kony.i18n.getCurrentLocale();
    invokeServiceSecureAsync("generateOTPWithUser", inputParam, CallbackonClickOTPchangeMobileRequestSpa);
  }
}

function CallbackonClickOTPchangeMobileRequestSpa(status, resulttable) {
  if (resulttable["errCode"] == "GenOTPRtyErr00002") {
    kony.application.dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
    return false;
  }
  if (resulttable["errCode"] == "GenOTPRtyErr00001") {
    kony.application.dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
    return false;
  }
  if (status == 400) {

    if (resulttable["opstatus"] == 0) {
      gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
      var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
      gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
      kony.timer.schedule("otpTimer", otpTimerCallBackchangemobileSpa, reqOtpTimer, false);
      if (gblOnClickReq == false) {
        showOTPPopupSpa(kony.i18n.getLocalizedString("keyOTP"), "ABCD", "xxx-xxx-" + gblPhoneNumberReq.substring(6, 10), otpValidationspa, 1)
        //(kony.i18n.getLocalizedString("keyOTP"), "ABCD","xxx-xxx-" + gblPhoneNumberReq.substring(6, 10),otpValidationspa, 1)
      }
      var refVal = "";
      for (var d = 0; d < resulttable["Collection1"].length; d++) {
        if (resulttable["Collection1"][d]["keyName"] == "pac") {
          refVal = resulttable["Collection1"][d]["ValueString"];
          break;
        }
      }
      popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + refVal;
      if (flowSpa) {
        popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("keyotpmsg");
        popOtpSpa.lblPopupTract7.text = "";
        popOtpSpa.lblPopupTract4Spa.text = "xxx-xxx-" + gblPhoneNumberReq.substring(6, 10);
      } else {
        popOtpSpa.lblPopupTract2.text = kony.i18n.getLocalizedString("keyotpmsg");
        popOtpSpa.lblPopupTract7.text = "";
        popOtpSpa.lblPopupTract4.text = "xxx-xxx-" + gblPhoneNumberReq.substring(6, 10);
      }
      gblOTPFlag = false;
    } else {
      var refVal = "";
      for (var d = 0; d < resulttable["Collection1"].length; d++) {
        if (resulttable["Collection1"][d]["keyName"] == "pac") {
          refVal = resulttable["Collection1"][d]["ValueString"];
          break;
        }
      }
      popOtpSpa.lblPopupTract1.text = kony.i18n.getLocalizedString("keybankrefno") + refVal;
      gblRetryCountRequestOTP = resulttable["retryCounterRequestOTP"];
      var reqOtpTimer = kony.os.toNumber(resulttable["requestOTPEnableTime"]);
      gblOTPLENGTH = kony.os.toNumber(resulttable["otpLength"]);
      kony.timer.schedule("otpTimer", otpTimerCallBack, reqOtpTimer, false);
      if (resulttable["errCode"] == "GenOTPRtyErr00001") {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        gblOTPFlag = false;
        return false;
      } else {
        kony.application.dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        gblOTPFlag = false;
        return false;
      }
    }
    kony.application.dismissLoadingScreen();
  }
}

function otpTimerCallBackchangemobileSpa() {
  popOtpSpa.btnPopUpTractCancel.skin = btnLightBlue;
  popOtpSpa.btnPopUpTractCancel.setEnabled(true);
  popOtpSpa.btnPopUpTractCancel.onClick = onClickActichangemobileRequestOtpSpa;
  gblOTPFlag = true;
  try {
    kony.timer.cancel("otpTimer")
  } catch (e) {

  }
}

function onClickActiRequestOtpSpa() {
  gblOTPFlag = true;
  gblOnClickReq = true;
  popOtpSpa.btnPopUpTractCancel.setEnabled(false);
  onClickOTPRequestchangemobileSpa();
}

function onClickOTPRequestchangemobileSpa() {
  if (gblSwitchToken == false && gblTokenSwitchFlag == false && gblSpaTokenServFalg == true) {
    checkTokenFlagSpa();
  }
  if (gblTokenSwitchFlag == false && gblSwitchToken == false) {

    popOtpSpa.hbxtoken.setVisibility(false);
    popOtpSpa.hboxotp.setVisibility(true);
    generateOtpUserchangeMobileSpa();
  } else if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
    popOtpSpa.hbxtoken.setVisibility(true);
    popOtpSpa.hboxotp.setVisibility(false);
    popOtpSpa.txttokenspa.text="";
    popOtpSpa.show();
  } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
    popOtpSpa.hbxtoken.setVisibility(false);
    popOtpSpa.hboxotp.setVisibility(true);
    generateOtpUserchangeMobileSpa();
  }
}

function callShowOTPPopUP() {
  if (flowSpa) {
    requestOTPTxnspa();
  } else {
    // showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", checkVerifyPWDProfileMB, 3);
    if(gblAuthAccessPin == true)
      showAccesspinPopup();
    else
      showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", CompositeChangeLimit, 3);
  }
}

function requestOTPTxnspa() {
  if (gblIBFlowStatus == "04") {
    //showAlert(kony.i18n.getLocalizedString("Receipent_OTPLocked"), kony.i18n.getLocalizedString("info"));
    popTransferConfirmOTPLock.show();

    return false;
  } else {
    var inputParams = {}
    var locale = kony.i18n.getCurrentLocale();
    spaChnage = "editmyprofile"
    gblOTPFlag = true;
    gblOnClickReq = false;
    try {
      kony.timer.cancel("otpTimer")
    } catch (e) {

    }
    if (caseTrans == "limit1" || caseTrans == "limit2" || caseTrans == "limit3") {
      gblSpaChannel = "ChangeLimitOnline";
      /*
            if (locale == "en_US") {
                SpaEventNotificationPolicy = "MIB_ChangeLimitOnline_EN";
                SpaSMSSubject = "MIB_ChangeLimitOnline_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_ChangeLimitOnline_TH";
                SpaSMSSubject = "MIB_ChangeLimitOnline_TH";
            }*/
      onClickOTPRequestSpa();
    }
    if (caseTrans == "Number") {

      /*
            if (locale == "en_US") {
                SpaEventNotificationPolicy = "MIB_ChangeMobile_EN";
                SpaSMSSubject = "MIB_ChangeMobile_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_ChangeMobile_TH";
                SpaSMSSubject = "MIB_ChangeMobile_TH";
            }*/
      changeMobileInSessionSPA(gblPhoneNumberReq)
      //generateOtpUserSpaNewMob();
    }
    if (caseTrans == "profile") {
      gblSpaChannel = "ChangeProfile";
      /*
            if (locale == "en_US") {
                SpaEventNotificationPolicy = "MIB_ChangeProfile_EN";
                SpaSMSSubject = "MIB_ChangeProfile_EN";
            } else {
                SpaEventNotificationPolicy = "MIB_ChangeProfile_TH";
                SpaSMSSubject = "MIB_ChangeProfile_TH";
            }*/
      onClickOTPRequestSpa();
    }
  }
}

function extractToValue(flexString) {
  if (flexString != null && flexString != undefined && flexString != "") {
    var a = flexString.match(/To [0-9,.]*/i);
    var reqAmt = "";
    if (a != null) {
      reqAmt = a.toString();
    }
    if (reqAmt.length > 3) {
      return reqAmt.substring(3);
    }
  }
  return "";
}

function callPopChkServReq(PrevCCAmt) {
  var confText = kony.i18n.getLocalizedString("keyserviceReq1") + " " + PrevCCAmt + kony.i18n.getLocalizedString("currencyThaiBaht") + " " + kony.i18n.getLocalizedString("keyserviceReq2");
  if (serviceProfileFlag == "MB" || flowSpa) {
    popupConfirmation.imgPopConfirmIcon.setVisibility(false);
    popupConfirmation.lblPopupConfText.text = confText;
    popupConfirmation.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyOK");
    popupConfirmation.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    // popupConfirmation.btnpopConfConfirm.onClick = cancelCurrentServiceStatus;
    popupConfirmation.btnpopConfConfirm.onClick = commoncancelLimit;
    popupConfirmation.btnPopupConfCancel.onClick = onClickconfChkServReq;
    popupConfirmation.show();
  } else if (!flowSpa) {
    popupDeleAccnt.image244747680938084.setVisibility(false);
    popupDeleAccnt.lblConfoMsg.text = confText;
    popupDeleAccnt.button44747680938122.text = kony.i18n.getLocalizedString("keyOK");
    popupDeleAccnt.button44914510567703.text = kony.i18n.getLocalizedString("keyCancelButton");
    //popupDeleAccnt.button44747680938122.onClick = cancelCurrentServiceStatusIB;
    popupDeleAccnt.button44747680938122.onClick = commoncancelLimit;
    popupDeleAccnt.button44914510567703.onClick = onClickconfChkServReq;
    popupDeleAccnt.show();
  }
}

function onClickconfChkServReq() {
  if (serviceProfileFlag == "MB") {
    popupConfirmation.dismiss();
    popupTractPwd.dismiss();
  } else {
    frmIBCMChgMobNoTxnLimit.show();
    frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text = "";
    frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.setEnabled(true);
    popupDeleAccnt.dismiss();
  }
}
//--------edit fb Account-------------

function invokeFacebookIDEditSetUp() {

  if (frmContactusFAQMB != null && frmContactusFAQMB != undefined) {
    frmContactusFAQMB.browser506459299404741.clearHistory();
    TMBUtil.DestroyForm(frmContactusFAQMB);
  } else {}
  requestFromForm = "MyProfileFB";

  if (flowSpa) {
    var fburl = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/fbsetup?locale=" + kony.i18n.getCurrentLocale() + "&fbidentity=" + encryptCRMId(gblcrmId);
  } else {
    var fburl = "https://" + appConfig.serverIp + "/" + appConfig.middlewareContext + "/fbsetup?locale=" + kony.i18n.getCurrentLocale();
  }
  //var fburl = "https://" + appConfig.serverIp + "/TMBIB/fbsetup";
  frmContactusFAQMB.browser506459299404741.url = fburl;
  frmContactusFAQMB.hbox475124774143.setVisibility(false);
  frmContactusFAQMB.hbox476018633224052.setVisibility(true);
  frmContactusFAQMB.button506459299404751.setVisibility(false);
  assignHandleRequest();
  frmContactusFAQMB.show();
}
// To get province List for editMyProfile 
var resetState = false;
var resetdistrict = false;
var resetsubdistrict = false;

function editMyProvince() {
  resetState = true;
  gblMyProfileAddressFlag = "state";
  showLoadingScreen();
  var inputParams = {};
  invokeServiceSecureAsync("MyprofileAddressProvinceJavaService", inputParams, editMyAddressServiceCallBack);
}
// To get District List for editMyProfile 

function editMyDistrict() {
  resetdistrict = true;
  gblMyProfileAddressFlag = "district";
  showLoadingScreen();
  var inputParams = {};
  inputParams["provinceCD"] = gblmyProfileAddrState;
  invokeServiceSecureAsync("MyprofileAddressDistrictJavaService", inputParams, editMyAddressServiceCallBack);
}
// To get SubDistrict List for editMyProfile 

function editMySubDistrict() {
  resetsubdistrict = true;
  gblMyProfileAddressFlag = "subdistrict";
  showLoadingScreen();
  var inputParams = {};
  inputParams["districtCD"] = gblmyProfileAddrDistrict;
  invokeServiceSecureAsync("MyprofileAddressSubDistrictJavaService", inputParams, editMyAddressServiceCallBack);
}
// To get Zipcode List for editMyProfile 

function editMyZipcode() {
  gblMyProfileAddressFlag = "zipcode";
  showLoadingScreen();
  var inputParams = {};
  inputParams["districtCD"] = gblmyProfileAddrDistrict;
  invokeServiceSecureAsync("MyprofileAddressZipcodeJavaService", inputParams, editMyAddressServiceCallBack);
}

function editMyAddressServiceCallBack(status, resulttable) {

  var locale = kony.i18n.getCurrentLocale();
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      // 
      if (gblMyProfileAddressFlag == "state") {
        if (resulttable["state"][0].length != 0) {
          if(locale == "en_US")
            populateAddressFieldsData(kony.table.sort(resulttable["state"],"ProvinceNameEN"));
          else 	populateAddressFieldsData(resulttable["state"]);
        }
      } else if (gblMyProfileAddressFlag == "district") {
        if (resulttable["district"][0].length != 0) {
          if(locale == "en_US")
            populateAddressFieldsData(kony.table.sort(resulttable["district"],"DistrictNameEN"));
          else	populateAddressFieldsData(resulttable["district"]);
        }
      } else if (gblMyProfileAddressFlag == "subdistrict") {
        if (resulttable["subdistrict"][0].length != 0) {
          if(locale == "en_US")
            populateAddressFieldsData(kony.table.sort(resulttable["subdistrict"],"SubDistrictNameEN"));
          else	populateAddressFieldsData(resulttable["subdistrict"]);	
        }
      } else if (gblMyProfileAddressFlag == "zipcode") {
        if (resulttable["zipcode"][0].length != 0) {
          if(locale == "en_US")
            populateAddressFieldsData(kony.table.sort(resulttable["zipcode"],"ZipCode"));
          else	populateAddressFieldsData(resulttable["zipcode"]);	
        }
      } else {
        alert("gblMyProfileAddressFlag is not valid");
      }
    } else {
      alert("No address fields found opstatus is not 0");
    }
  } else {
    if (status == 300) {
      alert("No address fields found ststus 300");
    }
  }

}

function populateAddressFieldsData(resultDS) {
  var resulttableRS = [];
  //frmeditMyProfile.hbox4758937266440.setEnabled(true);
  //var resultDS = resulttable["state"];
  var currentLocales = kony.i18n.getCurrentLocale();

  for (var j = 0; j < resultDS.length; j++) {
    try {
      var tempRec = [];
      if (gblMyProfileAddressFlag == "state") {
        if (currentLocales == "th_TH") {
          tempRec = {
            lblbankListThai: resultDS[j].ProvinceNameTH,
            lblBanklist: resultDS[j].ProvinceNameTH,
            idhiddenAddr: resultDS[j].ProvinceCD
          }
        } else {
          tempRec = {
            lblbankListThai: resultDS[j].ProvinceNameTH,
            lblBanklist: resultDS[j].ProvinceNameEN,
            idhiddenAddr: resultDS[j].ProvinceCD
          }
        }
      } else if (gblMyProfileAddressFlag == "district") {
        if (currentLocales == "th_TH") {
          tempRec = {
            lblbankListThai: resultDS[j].DistrictNameTH,
            lblBanklist: resultDS[j].DistrictNameTH,
            idhiddenAddr: resultDS[j].DistrictCD
          }
        } else {
          tempRec = {
            lblbankListThai: resultDS[j].DistrictNameTH,
            lblBanklist: resultDS[j].DistrictNameEN,
            idhiddenAddr: resultDS[j].DistrictCD
          }
        }
      } else if (gblMyProfileAddressFlag == "subdistrict") {
        //frmeditMyProfile.hbox4758937266440.setEnabled(true);
        if (currentLocales == "th_TH") {
          tempRec = {
            lblbankListThai: resultDS[j].SubDistrictNameTH,
            lblBanklist: resultDS[j].SubDistrictNameTH,
            idhiddenAddr: resultDS[j].SubDistrictCD
          }
        } else {
          tempRec = {
            lblbankListThai: resultDS[j].SubDistrictNameTH,
            lblBanklist: resultDS[j].SubDistrictNameEN,
            idhiddenAddr: resultDS[j].SubDistrictCD
          }
        }
      } else if (gblMyProfileAddressFlag == "zipcode") {
        //alert("resultDS.length "+resultDS.length);
        if (resultDS.length == 1) {
          frmeditMyProfile.lblzipcode.text = resultDS[j].ZipCode;
          zipcodeValue = resultDS[j].ZipCode;
          gblmyProfileAddrZipCode = resultDS[j].ZipCode;
          frmeditMyProfile.hbox4758937266440.setEnabled(false);
          frmOpenProdEditAddress.hbox4758937266440.setEnabled(false);
          frmOpenProdEditAddress.lblzipcode.text = resultDS[j].ZipCode;
          dismissLoadingScreen();
          return false;
        } else {
          tempRec = {
            lblbankListThai: resultDS[j].ZipCode,
            lblBanklist: resultDS[j].ZipCode,
            idhiddenAddr: resultDS[j].ZipCode
          }
          frmeditMyProfile.hbox4758937266440.setEnabled(true);
          frmOpenProdEditAddress.hbox4758937266440.setEnabled(true);
        }
      } else {

      }
      resulttableRS.push(tempRec);
    } catch (i18nError) {
      alert("Exception While getting currentLocale  : " + i18nError);
    }
  }
  // if (currentLocales == "th_TH"){ 
  //   	popAddrCmboBox.segBanklist.lblBanklist.s
  popAddrCmboBox.segBanklist.removeAll();
  popAddrCmboBox.segBanklist.setData(resulttableRS);
  if(gblMyProfileAddressFlag == "state")
    resulttableState = resultDS;
  else	if(gblMyProfileAddressFlag == "district")
    resulttableDist = resultDS;
  else	if(gblMyProfileAddressFlag == "subdistrict")
    resulttableSubDist = resultDS;
  else	if(gblMyProfileAddressFlag == "zipcode")
    resulttableStateZip = resultDS;

  dismissLoadingScreen();
  popAddrCmboBox.show();
}

function onRowSelectAddrPopUp() {
  lblAddrField = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
  idHiddenAddrField = popAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
  lblbankListThaiField = popAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
  dismissLoadingScreen();
  if (gblMyProfileAddressFlag == "state") {
    confirmEdit = true;
    changeState = true;
    frmeditMyProfile.hbox4758937266356.setEnabled(true);
    frmOpenProdEditAddress.hbox4758937266356.setEnabled(true);
    StateValue = popAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
    gblmyProfileAddrState = popAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
    frmeditMyProfile.lblProvince.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    frmOpenProdEditAddress.lblProvince.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    if (resetState) {
      frmeditMyProfile.lbldistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmeditMyProfile.lblsubdistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmeditMyProfile.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmOpenProdEditAddress.lbldistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmOpenProdEditAddress.lblsubdistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmOpenProdEditAddress.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');

    }
  } else if (gblMyProfileAddressFlag == "district") {
    changedist = true;
    frmeditMyProfile.hbox4758937266374.setEnabled(true);
    frmOpenProdEditAddress.hbox4758937266374.setEnabled(true);
    districtValue = popAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
    gblmyProfileAddrDistrict = popAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
    frmeditMyProfile.lbldistrict.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    frmOpenProdEditAddress.lbldistrict.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    if (resetdistrict) {
      frmeditMyProfile.lblsubdistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmeditMyProfile.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmOpenProdEditAddress.lblsubdistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmOpenProdEditAddress.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
    }
    resetState = false;
  } else if (gblMyProfileAddressFlag == "subdistrict") {
    changeSubDist = true;
    frmeditMyProfile.hbox4758937266440.setEnabled(true);
    frmOpenProdEditAddress.hbox4758937266440.setEnabled(true);
    subdistrictValue = popAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
    gblmyProfileAddrSubDistrict = popAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
    frmeditMyProfile.lblsubdistrict.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    frmOpenProdEditAddress.lblsubdistrict.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    if (resetsubdistrict) {
      frmeditMyProfile.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
      frmOpenProdEditAddress.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
    }
    resetdistrict = false;
  } else if (gblMyProfileAddressFlag == "zipcode") {
    changeZip = true;
    zipcodeValue = popAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
    gblmyProfileAddrZipCode = popAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
    frmeditMyProfile.lblzipcode.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    frmOpenProdEditAddress.lblzipcode.text = popAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    resetsubdistrict = false
  }
  popAddrCmboBox.dismiss();
}

//paste here
function checkServiceHoursEditProfile() {
  if(gblOpenActBusinessHrs == "true"){
    s2sBusinessHrsFlag = "true";
  }
  kony.print("s2sBusinessHrsFlag in checkServiceHoursEditProfile" + s2sBusinessHrsFlag)
  if (s2sBusinessHrsFlag == "true") {
    // call if current time falls in between provided business hours
    //gblEditServiceHours = true;
    if (editbuttonflag == "profile") {
      //                   	if(gblOpenProdAddress) {
      //                       loadSearchAddress("openAccount");
      // //                       dropDownStatePopulate();
      //                     }else{
      if(gblLoanNav == ""){
        loadSearchAddress("myProfile");
      }else{
        loadSearchAddress("applyLoan");
      }
      //                     }
      //dropDownStatePopulate();  
    } else if (editbuttonflag == "number") {
      gblMobNoTransLimitFlag = true;
      frmChangeMobNoTransLimitMB.show();
    }
  }
  else
  {
    showAlert(getErrorMsgForBizHrs(), kony.i18n.getLocalizedString("info"));

  }  
}

function emailValidatnpro(txt) {
  if (txt == null || txt.trim() == "") {
    return false;
  }
  var txtLen = txt.length;
  var i, j;
  var pat1 = /[=<>()"',:;\s]/g;
  var noChars = pat1.test(txt);
  if (noChars == true) {
    return false;
  }
  var atIndex, count = 0;
  for (i = 0; i < txtLen; i++) {
    if (txt[i] == '@') {
      count++;
      atIndex = i;
    }
  }
  if ((atIndex < 1) || (atIndex == txtLen - 1)) return false;
  if (count != 1) {
    return false;
  } else {
    if ((atIndex + 13) <= txtLen) {
      temp = txt.substring(atIndex + 1, atIndex + 13);
      if (temp == "facebook.com") {
        return false;
      }
    }
    count = 0;
    for (i = atIndex; i < txtLen; i++) {
      if (txt[i] == '.') {
        count++;
        var temp = "";
        if ((i + 3) > txtLen) {
          return false;
        } else {
          temp = txt[i + 1] + txt[i + 2];
          var flag = kony.string.isAsciiAlphaNumeric(temp);
          var flag1 = kony.string.isAsciiAlpha(temp);
          var flag2 = kony.string.isNumeric(temp);
          if (!(flag || flag1 || flag2)) {
            return false;
          }
        }
      }
    }
    if (count == 0) {
      return false;
    }
  }
  return true;
}

function updateDeviceNameForProfile() {
  var deviceNickname = frmeditMyProfile.txtdevicenamevalue.text;
  flagbfnot4 = true;

  var inputParam = {};
  //inputParam["associationId"] = gblcrmId;
  inputParam["deviceId"] = GBL_UNIQ_ID;
  inputParam["deviceNickName"] = deviceNickname;
  invokeServiceSecureAsync("updateDeviceNickName", inputParam, updateDeviceNickNameProfilecallBack)
}

function updateDeviceNickNameProfilecallBack(status, resulttable) {
  if (status == 400) {
    flagnot4 = true;
    if (resulttable["errCode"] == "KonyDvMgmtErr00003") {
      showAlert(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00003"), kony.i18n.getLocalizedString("info"));
      dismissLoadingScreen();
      return false;
    } else {


    }
  }
}

function getDeviceNameForProfile(unqId) {
  //alert("in getDeviceNameForProfile");
  inputParam = {};
  //inputParam["associationId"] = gblcrmId;
  inputParam["deviceId"] = unqId;
  inputParam["profileFlag"] = "true";
  invokeServiceSecureAsync("deviceEnquiry", inputParam, getDeviceNameForProfilecallBack)
}

function forDeviceID(str) {
  GBL_UNIQ_ID = str;
  getDeviceNameForProfile(str);
}

function getDeviceNameForProfilecallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      if (resulttable["Results"][0]["deviceId"] != null) {
        gblDeviceNickName = resulttable["Results"][0]["deviceNickName"];
        frmMyProfiles.lblMyDeviceVal.text = gblDeviceNickName;
        frmMyProfiles.show();
        dismissLoadingScreen();
        return true;
      } else {
        return false;
      }
    }
  }
}

function MblNickNameMyProfile(text) {
  if ((text == null) || (text == "")) {
    return true;
  }
  var txtLen = text.length;
  var pat2 = /^[A-Za-z0-9\u0E00-\u0E7F\s.-]+$/
  var isAlphNum = pat2.test(text);
  if (isAlphNum) {
    if( txtLen <=20&&txtLen>=3){
      return true;
    }
    else
    {
      return false;
    }
  }
  return false;

}

function EditProfileValidatn() {
  profileedit = true;
  profileEmailFlag = "";
  profileAddrFlag = "";
  profilePicFlag = "";
  profileDeviceNameFlag = "";
  var email = frmeditMyProfile.txtemailvalue.text;
  if("" == frmeditMyProfile.txtemailvalue.text){
    email = frmeditMyProfile.txtemailvalue.placeholder
  }
  var emailFlag = validateEmail(email);
  var devname = frmeditMyProfile.txtdevicenamevalue.text;
  var devNameFlag = MblNickNameMyProfile(devname);
  var invalidEmail1 = kony.i18n.getLocalizedString("invalidEmail");
  var invalidDivName = kony.i18n.getLocalizedString("invalidDeviceName");
  var info1 = kony.i18n.getLocalizedString("info");
  var okk = kony.i18n.getLocalizedString("keyOK");
  var nodetails = kony.i18n.getLocalizedString("keyenterDetails");
  if (flowSpa == true) {
    if ((email == null || email == "") && ((frmeditMyProfile.lblsubdistrict.text) == gblsubdistrictValue || (frmeditMyProfile.lblsubdistrict.text) == gblSubDistEng) && ((frmeditMyProfile.lbldistrict.text) == gbldistrictValue || (frmeditMyProfile.lbldistrict.text) == gblDistEng) && ((frmeditMyProfile.lblzipcode.text) == gblzipcodeValue) && ((frmeditMyProfile.txtAddress1.text) == gblAddress1Value) && ((frmeditMyProfile.txtAddress2.text) == gblAddress2Value) && ((frmeditMyProfile.lblProvince.text) == gblStateValue || (frmeditMyProfile.lblProvince.text) == gblStateEng) && (frmeditMyProfile.imgprofpic.rawBytes == null)) {

      profileedit = false;
    }
  } else {
    if ((devname == null || devname == "") && (email == null || email == "") && ((frmeditMyProfile.lblsubdistrict.text) == gblsubdistrictValue || (frmeditMyProfile.lblsubdistrict.text) == gblSubDistEng) && ((frmeditMyProfile.lbldistrict.text) == gbldistrictValue || (frmeditMyProfile.lbldistrict.text) == gblDistEng) && ((frmeditMyProfile.lblzipcode.text) == gblzipcodeValue) && ((frmeditMyProfile.txtAddress1.text) == gblAddress1Value) && ((frmeditMyProfile.txtAddress2.text) == gblAddress2Value) && ((frmeditMyProfile.lblProvince.text) == gblStateValue || (frmeditMyProfile.lblProvince.text) == gblStateEng) && (frmeditMyProfile.imgprofpic.rawBytes == null)) {

      profileedit = false;
    }
    if(imageDeleted){
      profileedit = true;
    }else{
      profileedit = false;
    }
  }
  var saveaddrRes = saveEditedAddrMyProfile(); //saveEditedAddressMyProfile(); 
  if (saveaddrRes == false) {
    profileedit = false;
    return false;
  }
  if (frmeditMyProfile.lblfbidvalue.text != null && frmeditMyProfile.lblfbidvalue.text != "" && frmeditMyProfile.lblfbidvalue.text != undefined) {
    var fbvalueflag = true
    }
  if (devNameFlag == false && flowSpa == false) {
    frmeditMyProfile.txtdevicenamevalue.skin = txtErrorBG;
    frmeditMyProfile.txtdevicenamevalue.focusSkin = txtErrorBG;
    showAlert(invalidDivName, info1);
    profileedit = false;
    return false;
  } else if (emailFlag == false) {
    frmeditMyProfile.txtemailvalue.skin = txtErrorBG;
    frmeditMyProfile.txtemailvalue.focusSkin = txtErrorBG;
    showAlert(invalidEmail1, info1);
    profileedit = false;
    return false;
  }
  if (flowSpa) {
    if (frmeditMyProfile.imgprofpic.src != null) {
      profilePicFlag = "picture";
      //alert("i am in MyProfilepic");
      //imgEditProfileServiceCall();
    }
  } else {
    if (frmeditMyProfile.imgprofpic.rawBytes != null && frmeditMyProfile.imgprofpic.rawBytes != "") {
      profilePicFlag = "picture";
      //alert("i am in MyProfilepic");
      //imgEditProfileServiceCall();
    }
  }
  if ((devname != "") && (devname != null) && (flowSpa == false)) {
    profileDeviceNameFlag = "device";
  }
  if ((email != "") && (email != null) && emailFlag == true) {
    profileEmailFlag = "email";
    gblUpdateProfileFlag = "email";
    //
  }
  //profileedit = true;
  caseTrans = "profile";
  if (flowSpa) {
    var inputParams = {}
    spaChnage = "editmyprofile"
    gblOTPFlag = true;
    try {
      kony.timer.cancel("otpTimer")
    } catch (e) {

    }
    gblSpaChannel = "ChangeProfile";
    /*
        if (locale == "en_US") {
            SpaEventNotificationPolicy = "MIB_ChangeProfile_EN";
            SpaSMSSubject = "MIB_ChangeProfile_EN";
        } else {
            SpaEventNotificationPolicy = "MIB_ChangeProfile_TH";
            SpaSMSSubject = "MIB_ChangeProfile_TH";
        }*/
    checkMyProfileSaveAddrMB();
    //save2sessionaddrDetailsSpa();
  } else {
    //comment the below function and uncomment the one below it to invoke java service
    //showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", checkVerifyPWDProfileMB, 3);

    //partyUpdateMyProfileService();
    if(gblAuthAccessPin == true){
      showAccesspinPopup();
    }else{
      showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", verifyPWDMyProfileMB, 3);  
    }
  }
}

function checkMyProfileSaveAddrMB() {
  var inputParam = [];
  showLoadingScreen();
  invokeServiceSecureAsync("tokenSwitching", inputParam, checkMyProfileSaveAddrMBCallbackfunction);
}
function checkMyProfileSaveAddrMBCallbackfunction(status,resulttable){
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      save2sessionaddrDetailsSpa();
    }
  } 
}

function crmprofilemodCallStatusLock() {
  var inputParam = {};
  inputParam["mbUserStatusId"] = "04";
  inputParam["actionType"] = "333";    
  invokeServiceSecureAsync("crmProfileMod", inputParam, crmprofilemodCallStatusLockCallback);
}

function crmprofilemodCallStatusLockCallback(status, resulttable) {	
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      gblUserLockStatusIB = resulttable["IBUserStatusID"];
      dismissLoadingScreen();
      frmMyProfiles.show();
    }
  }
}

function fbprofileviewServiceCall() {
  //alert("viewprofileServiceCall");
  confirmEdit = true;
  selectState = frmeditMyProfile.lblProvince.text;
  selectDist = frmeditMyProfile.lbldistrict.text;
  selectSubDist = frmeditMyProfile.lblsubdistrict.text;
  selectZip = frmeditMyProfile.lblzipcode.text;
  var inputParams = {};
  //inputParams["profileFlag"] = "true"; 
  //inputParams["crmId"] = gblCrmId;
  invokeServiceSecureAsync("crmProfileInq", inputParams, fbprofileviewServiceCallBack);
}

function fbprofileviewServiceCallBack(status, resulttable) {
  if (status == 400) //success response
  {



    if (resulttable["opstatus"] == 0) {
      var StatusCode = resulttable["statusCode"];

      if (resulttable["statusCode"] == 0) {
        gblFBCode = resulttable["facebookId"];                
        gblFacebookId = resulttable.fbUsername;

        if (gblMyFBdelinkTrack == true) {
          frmeditMyProfile.show();
          gblMyFBdelinkTrack = false;
        }
        GBLemailAL = EmailHide(gblEmailAddr);
        var fbvalueflag = true;
        frmeditMyProfile.lblfbidvalue.text = gblFacebookId;
        frmMyProfiles.lblfbidstudio1.text =  gblFacebookId;
      }
    }
  }
}


function nulltheGlobalsMyProfile() {
  //tempOldAddress = "";
  province = "";
  district = "";
  subdistrict = "";
  zipcode = "";
  StateValue = "";
  districtValue = "";
  subdistrictValue = "";
  zipcodeValue = "";
  gbldistrictValue = "";
  gblsubdistrictValue = "";
  gblzipcodeValue = "";
  gblStateValue = "";
  gblDeviceNickName = "";
  gblMyProfilepic = "";
  gblPHONENUMBER = "";
  //gblEmailAddr = "";
  gblFacebookId = "";
  gblAddress1Value = "";
  gblAddress2Value = "";
  gblPhoneNumberReq = "";
  gblOTPFlag = "";
  gblOnClickReq = "";
  //gblUpdateProfileFlag = "";
  gblEditBase64List = "";
  gblDeviceNickName = "";
  //caseTrans = "";
  //profileAddrFlag = "";
  gblEBMaxLimitAmtCurrent = "";
  gblEBMaxLimitAmtCurrentOld = "";
  flagnot1 = false;
  flagnot2 = false;
  flagnot3 = false;
  flagnot4 = false;
  gblStateEng = "";
  gblDistEng = "";
  gblSubDistEng = "";
  provinceCD = "";
  DistrictCD = "";
  maskedNewMob = "";
  gblZipEng = "";
}

function viewprofileServiceCall() {
  gblRetryCountRequestOTP = "0";
  //nulltheGlobalsMyProfile();
  var inputParams = {};
  //inputParams["deviceID"] = GBL_UNIQ_ID ;
  //fix MIB-8330: Device Name wrong displaying on My Profile
  inputParams["deviceID"] = gblDeviceIDUnEncrypt; 
  invokeServiceSecureAsync("MyProfileViewCompositeService", inputParams, viewprofileServiceCallBack);
}

function viewprofileServiceCallBack(status, resulttable) {
  if (status == 400) //success response
  {
    var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
    var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
    var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
    var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";




    if (resulttable["opstatus"] == 0) {
      dismissLoadingScreen();
      if (resulttable["ebMaxLimitAmtCurrent"] != null) {

        gblEBMaxLimitAmtCurrent = resulttable.ebMaxLimitAmtCurrent;
        gblEBMaxLimitAmtCurrentOld = gblEBMaxLimitAmtCurrent;
        frmMyProfiles.lblDailytransLimitvalue.text = ProfileCommFormat(gblEBMaxLimitAmtCurrent)+ " " +kony.i18n.getLocalizedString("currencyThaiBaht");
      }
      if (resulttable["ebMaxLimitAmtHist"] != null) {

        gblEBMaxLimitAmtHist = resulttable.ebMaxLimitAmtHist;
      }
      if (resulttable["ebMaxLimitAmtRequest"] != null) {

        gblEBMaxLimitAmtReq = resulttable.ebMaxLimitAmtRequest;
      }
      if (resulttable["emailAddr"] != null) {

        gblEmailAddr = resulttable.emailAddr;
        gblEmailAddrOld = gblEmailAddr;
        frmMyProfiles.lblEmailVal.text = gblEmailAddr;
      }

      gblFacebookId = resulttable.fbUsername;
      gblFacebookIdOld = gblFacebookId;
      frmMyProfiles.lblfbidstudio1.text = gblFacebookId;//Modified by Studio Viz
      //}

      gblFBCode = resulttable["facebookId"];
      if (resulttable["ebTxnLimitAmt"] != null) {

        gblEbTxnLimitAmt = resulttable.ebTxnLimitAmt;
        gblEbTxnLimitAmtOld = gblEbTxnLimitAmt;
      }
      if(resulttable["deviceNickName"]!=null){
        gblDeviceNickName = resulttable["deviceNickName"];
        frmMyProfiles.lblMyDeviceVal.text = gblDeviceNickName;
      }			
      if(resulttable["ibUserStatusId"] != null)
        ibUserStatusId = resulttable.ibUserStatusId;
      if(resulttable["mbUserStatusId"] != null)
        mbUserStatusId = resulttable.mbUserStatusId;
      if(resulttable["ibStatusFlag"] != null)
        ibStatusFlag = resulttable.ibStatusFlag;
      if(resulttable["mbStatusFlag"] != null)
        mbStatusFlag = resulttable.mbStatusFlag;
      if(resulttable["s2sBusinessHrsFlag"] != null)
        s2sBusinessHrsFlag = resulttable.s2sBusinessHrsFlag;
      if(resulttable["s2sStartTime"] != null)
        s2sStartTime = resulttable.s2sStartTime;
      if(resulttable["s2sEndTime"] != null)
        s2sEndTime = resulttable.s2sEndTime;
      if(resulttable["S2SstatusCode"] != null)
        s2sstatuscode = resulttable.S2SstatusCode;
      if(resulttable["S2SstatusDesc"] != null)
        s2sstatusdesc = resulttable.S2SstatusDesc;		

      gblCustomerName = resulttable["customerName"];
      gblCustomerNameTh = resulttable["customerNameTH"];
      customerName = resulttable["customerName"];
      if (kony.i18n.getCurrentLocale() == "th_TH") {
        frmMyProfiles.lblProfileDescription.text = gblCustomerNameTh;
      } else {
        frmMyProfiles.lblProfileDescription.text = gblCustomerName;
      }
      for (var i = 0; i < resulttable["ContactNums"].length; i++) {
        var PhnType = resulttable["ContactNums"][i]["PhnType"];

        if (PhnType != null && PhnType != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined) {
          if (PhnType == "Mobile") {
            if ((resulttable["ContactNums"][i]["PhnNum"] != null) && (resulttable["ContactNums"][i]["PhnNum"] != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined)) {
              gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
              gblPHONENUMBEROld = resulttable["ContactNums"][i]["PhnNum"];

              frmMyProfiles.lblMobileVal.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
              GblMobileAL = HidePhnNum(gblPHONENUMBER);
            }
          }
        }
      }
      for (var i = 0; i < resulttable["Persondata"].length; i++) {
        var tempAddrtype = resulttable["Persondata"][i]["AddrType"];
        if (tempAddrtype == "Primary") {
          if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
            //if(resulttable["Persondata"][i]["addr3"]){}
            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
              var adr3 = resulttable["Persondata"][i]["addr3"];

              var reg = / {1,}/;
              var tempArr = [];
              tempArr = adr3.split(reg);

              //if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) 
              {
                if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                  if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                    if(tempArr[0].indexOf(subDistBan, 0) >= 0)
                      gblsubdistrictValue = tempArr[0].substring(4);
                    else  if(tempArr[0].indexOf(subDistNotBan, 0) >= 0) 
                      gblsubdistrictValue = tempArr[0].substring(2);
                    else 	gblsubdistrictValue = "";	
                    if(tempArr[1].indexOf(distBan, 0) >= 0)  	 
                      gbldistrictValue = tempArr[1].substring(3);
                    else   if(tempArr[1].indexOf(distNotBan, 0) >= 0) 
                      gbldistrictValue = tempArr[1].substring(2); 
                    else 	gbldistrictValue = "";
                  } 
                } else {
                  gblStateValue = "";
                  gblsubdistrictValue = "";
                  gbldistrictValue = "";
                }
                gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                gblAddress2Value = resulttable["Persondata"][i]["addr2"];
                if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
                  gblStateValue = resulttable["Persondata"][i]["City"];
                else	gblStateValue = "";
                if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
                  gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                else 	
                  gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
                  gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                else	gblcountryCode = "";
                if (tempArr[0] == "" || tempArr[0] == undefined) {
                  tempArr[0] = "";
                }
                if (tempArr[1] == "" || tempArr[1] == undefined) {
                  tempArr[1] = "";
                }
                gblViewsubdistrictValue = tempArr[0];
                gblViewdistrictValue = tempArr[1];
                gblnewStateValue = resulttable["Persondata"][i]["StateProv"];
                frmeditMyProfile.txtAddress1.text = gblAddress1Value;
                frmeditMyProfile.txtAddress2.text = gblAddress2Value;

                frmMyProfiles.lblContactVal.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
                frmeditMyProfiles.txtAddress1.text = gblAddress1Value;
                frmeditMyProfiles.txtAddress2.text = gblAddress2Value;
                frmeditMyProfiles.txtSubDistrict.text = gblsubdistrictValue;
                frmeditMyProfiles.lblDistrictVal.text = gbldistrictValue;
                frmeditMyProfiles.lblProvinceVal.text = gblStateValue;
                frmeditMyProfiles.lblZipCodeVal.text = gblzipcodeValue;
                gblnotcountry = false;
                if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                  gblnotcountry = true;
                } else {
                  gblnotcountry = false;
                }

                gblAddress = frmMyProfiles.lblContactVal.text;
                //

              }
            }
          }
        } else if (tempAddrtype == "Registered") {

          //alert("address3 of the person data"+resulttable["Persondata"][i]["addr3"]);
          if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
              var adr3 = resulttable["Persondata"][i]["addr3"];
              var reg = / {1,}/;
              var tempArr = [];
              tempArr = adr3.split(reg);
              //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 

              if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                if (resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
                  gblregsubdistrictValue = tempArr[0];
                  gblregdistrictValue = tempArr[1];
                } else {
                  gblregsubdistrictValue = tempArr[0];
                  gblregdistrictValue = tempArr[1];
                }
              }
              else {
                gblregStateValue = "";
                gblregsubdistrictValue = tempArr[0];
                gblregdistrictValue = tempArr[1];
              }
              gblregAddress1Value = resulttable["Persondata"][i]["addr1"];
              gblregAddress2Value = resulttable["Persondata"][i]["addr2"];
              if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
                gblregStateValue = resulttable["Persondata"][i]["City"];
              else gblregStateValue = "";
              if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
                gblregzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
              else 	gblregzipcodeValue = "";
              if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
                gblregcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
              else gblregcountryCode = "";									
              frmMyProfiles.lblRegisterAddressVal.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue + " " + gblregdistrictValue + " " + " " + gblregStateValue + " " + " " + gblregzipcodeValue + " " + " " + gblregcountryCode;
              //
            }
          }
        }
      }
    }
    frmMyProfiles.show();
  } else {
    dismissLoadingScreen(); 
    frmMyProfiles.show();

  }
}

function crmProfileModEditMyProfile(reqvar) {
  gblUpdateProfileFlag = reqvar;
  var inputParam = {};
  if (gblUpdateProfileFlag == "reqCurr") {
    var DLimit = frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text;
    DLimit = DLimit.toString().replace(/,/g, "");
    var text1 = DLimit;

    inputParam["ebMaxLimitAmtRequest"] = text1;
    inputParam["actionType"] = "333";
    inputParam["ebMaxLimitAmtCurrent"] = text1;
  } else if (gblUpdateProfileFlag == "hist") {
    var DLimit = frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text;
    DLimit = DLimit.toString().replace(/,/g, "");
    var text1 = DLimit;
    inputParam["ebMaxLimitAmtRequest"] = text1;
    inputParam["ebMaxLimitAmtHist"] = text1;
  } else if (gblUpdateProfileFlag == "req") {
    var DLimit = frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text;
    DLimit = DLimit.toString().replace(/,/g, "");
    var text1 = DLimit;
    inputParam["ebMaxLimitAmtRequest"] = text1;
    inputParam["actionType"] = "333";
  } else if (gblUpdateProfileFlag == "email") {
    flagbfnot1 = true;
    inputParam["emailAddr"] = frmeditMyProfile.txtemailvalue.text;
    inputParam["actionType"] = "333";
  } else {}
  invokeServiceSecureAsync("crmProfileMod", inputParam, crmProfileModEditMyProfileCallBack);
}

function crmProfileModEditMyProfileCallBack(status, resulttable) {
  if (status == 400) {

    if (resulttable["opstatus"] == 0) {


      gblUserLockStatusIB = resulttable["IBUserStatusID"];
      if (gblUpdateProfileFlag == "reqCurr") {
        var newlimit = ProfileCommFormat(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
        var oldTlimit = ProfileCommFormat(gblEBMaxLimitAmtCurrentOld);
        activityLogServiceCall("052", "", "01", gblDeviceNickName, oldTlimit, newlimit, "", "", "", "");
        NotificationChangeLimitMBService();
        crmProfileInqForLimitServiceCall();
      }
      if (gblUpdateProfileFlag == "hist") {
        var newlimit = ProfileCommFormat(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
        activityLogServiceCall("052", "", "01", gblDeviceNickName, oldTlimit, newlimit, "", "", "", "");
        NotificationChangeLimitMBService();
        crmProfileInqForLimitServiceCall();
      }
      if (gblUpdateProfileFlag == "email") {
        flagnot1 = true;
        //alert("  fbIdOld gblFacebookId  "+fbIdOld + " "+gblFacebookId);
        GBLemailAL = EmailHide(frmeditMyProfile.txtemailvalue.text);
        GBLemailALOld = EmailHide(gblEmailAddrOld);
        if (fbvalueflag == true) {
          if (fbIdOld != frmeditMyProfile.lblfbidvalue.text) { //activityLogServiceCall("071", "", "00", gblDeviceNickName, "Initial", GblMobileAL,GBLemailALOld +" " + "+" + " "+GBLemailAL,fbIdOld +" " + "+" + " "+ gblFacebookId, "", "");
            activityLogServiceCall("071", "", "01", gblDeviceNickName, "Edit", "", GBLemailALOld + " " + "+" + " " + GBLemailAL, fbIdOld + " " + "+" + " " + gblFacebookId, "", "");
          }
        } else {
          //activityLogServiceCall("071", "", "00", gblDeviceNickName, "Initial", GblMobileAL,GBLemailALOld +" " + "+" + " "+GBLemailAL, "", "", "");
          activityLogServiceCall("071", "", "01", gblDeviceNickName, "Edit", "", GBLemailALOld + " " + "+" + " " + GBLemailAL, "", "", "");
        }
        //gblEmailAddr = frmeditMyProfile.txtemailvalue.text;
        //crmProfileInqForEmailService();
        callNotifyEditFlag = true;
      }
    } else {
      dismissLoadingScreen();
      if (gblUpdateProfileFlag == "email") {
        //var emailAddress =  frmeditMyProfile.txtemailvalue.text;
        GBLemailAL = EmailHide(frmeditMyProfile.txtemailvalue.text);
        GBLemailALOld = EmailHide(gblEmailAddrOld);
        if (fbvalueflag == true) {
          if (fbIdOld != frmeditMyProfile.lblfbidvalue.text) activityLogServiceCall("071", "", "02", gblDeviceNickName, "Edit", "", GBLemailALOld + " " + "+" + " " + GBLemailAL, fbIdOld + " " + "+" + " " + gblFacebookId, "", "");
        } else {
          activityLogServiceCall("071", "", "02", gblDeviceNickName, "Edit", "", GBLemailALOld + " " + "+" + " " + GBLemailAL, gblFacebookId, "", "");
        }
      }

    }
  } else {
    if (status == 300) {
      dismissLoadingScreen();

    }
  }
}

function toCallViewProfileonSave() {
  nulltheGlobalsMyProfile();
  viewprofileServiceCall();
}


function callToCustCare() {
  try {
    var number = "1558";
    kony.phone.dial(number);
    popUpCallCancel.dismiss();
  } catch (err) {
    alert("error in dial:: " + err);
  }
} /** This method is used to get the ib, mb statuses **/

function getIBMBEditProfileStatus() {
  profilePicFlag = "";
  gblOpenProdAddress = false;
  gblOpenAccountFlow = false;
  if (s2sstatuscode != "0") 
    alert(s2sstatusdesc);
  else	checkStatusEditProfile();
}
/**************************************************************************************
		Module	: startcrmProfileInqServiceAsyncCallback
		Author  : Kony
		Date    : 
		Purpose : This method is used to get the ib and mb status values by inquiring the crmprofile
****************************************************************************************/

function checkStatusEditProfile() {
  kony.print("Edit Profile Action =" + editbuttonflag );
  if(checkMBUserStatus())
  {
    if (editbuttonflag == "pin") {
      gblChangePWDFlag = 0;
      frmAccTrcPwdInter.show();
    } else if (editbuttonflag == "transpass") {
      gblChangePWDFlag = 1;
      frmAccTrcPwdInter.show();
    } else if (editbuttonflag == "limit") {
      gblMobNoTransLimitFlag = false;
      frmChangeMobNoTransLimitMB.show();
    } else{
      checkServiceHoursEditProfile();
    }

  }
} 

function crmProfileInqForLimitServiceCall() {
  var inputParams = {};
  invokeServiceSecureAsync("crmProfileInq", inputParams, crmProfileInqForLimitServiceCallBack);
}

function crmProfileInqForLimitServiceCallBack(status, resulttable) {
  if (status == 400) //success response
  {
    if (resulttable["opstatus"] == 0) {
      if (resulttable["ebMaxLimitAmtCurrent"] != null) {

        gblEBMaxLimitAmtCurrent = resulttable.ebMaxLimitAmtCurrent;
        gblEBMaxLimitAmtCurrentOld = gblEBMaxLimitAmtCurrent;
        frmMyProfiles.lblDailytransLimitvalue.text = ProfileCommFormat(gblEBMaxLimitAmtCurrent)+ " " +kony.i18n.getLocalizedString("currencyThaiBaht");
        frmMyProfiles.show();
        if (resulttable["ebMaxLimitAmtRequest"] != null) {

          gblEBMaxLimitAmtReq = resulttable.ebMaxLimitAmtRequest;
        }
        //kony.alert("callLimitedit"+callLimitedit);
        if (callLimitedit == true) {
          callLimitedit = false;
          popUpCallCancel.show();
        }
        activityLogServiceCall("072", "", "01", gblDeviceNickName, "", "", "", "", "", "");
      }
    } else {

    }
  }
  dismissLoadingScreen();
  //status 400
}

function handleFacebookIDEdit() {
  if (gblFBCode == "" || gblFBCode == null) {
    invokeFacebookIDEditSetUp();
  } else {
    fbIdOld = frmMyProfiles.lblfbidstudio1.text;//Modified by Studio Viz
    var basicConfig = {};
    basicConfig.message = "Disconnect from facebook?";
    basicConfig.alertType = constants.ALERT_TYPE_CONFIRMATION;
    basicConfig.yesLabel = "Yes";
    basicConfig.noLabel = "No";
    basicConfig.alertHandler = handleFacebookIDEditPopup;
    var pspConfig = {};
    var FBDelinkPopup = kony.ui.Alert(basicConfig, pspConfig);
  }
}

function handleFacebookIDEditPopup(response) {
  //alert("response : " + response.toString());
  if (response == true) {
    delinkFacebookWrapper(gblFBCode);
  } else {
    //DO NOTHING
  }
}

function HidePhnNum(n) {
  var d = n; // "0813334444" to ; 
  var start = d.slice(0, 3);
  var hideStr = "XXX";
  var rest = d.slice(6, 10);
  var num = start + hideStr + rest;
  return num;
}

function EmailHide(d) {
  var atthe = d.indexOf("@") + 1;
  var dotindex = d.indexOf(".", atthe);
  for (var i = atthe; i < dotindex; i++) {
    d = d.substr(0, i) + 'x' + d.substr(i + 1);
  }
  return d;
}
//-----------------------Start of Change limit composite service --------------------

function verifyPWDMyProfileMB() {
  // alert("here")
  var trap;
  var password = null;
  if (flowSpa) {
    if (gblTokenSwitchFlag == true) {
      password = popOtpSpa.txttokenspa.text;
    } else {
      password = popOtpSpa.txtOTP.text;
    }
    trap = true;
  } else {
    if(gblAuthAccessPin == true)
      password = gblNum;
    else
      password = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;

    if (password == "" || password == null) {
      popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
      return;
    }

    if(kony.application.getCurrentForm().id == "frmOpenProdEditAddressConfirm" || kony.application.getCurrentForm().id == "frmeditMyProfile") {
      trap = true;
    } else {
      trap = trassactionPwdValidatn(password);
    }

  }
  showLoadingScreen();
  if (trap == false) {

    dismissLoadingScreen();
    popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
    popupTractPwd.lblPopupTract7.setVisibility(true);
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
    popupTractPwd.show();
    if(popupTractPwd.hbxOTP.isVisible){
      kony.print("$$$$$$$$$$$$$$$$$"+popupTractPwd.hbxPopupTranscPwd.isVisible)
      popupTractPwd.txtOTP.setFocus(true)

    }else if(popupTractPwd.hbxPopupTranscPwd.isVisible){
      popupTractPwd.tbxPopupTractPwdtxtTranscPwd.setFocus(true)
    }else if(popupTractPwd.hbxPoupAccesspin.isVisible){
      popupTractPwd.tbxPopupTractPwdtxtAccPin.setFocus(true)
    }
  } else {
    var verifyPWDMyProfileMB_InputParam = {}
    // sending the global params 
    txt1 = frmeditMyProfile.txtAddress1.text;
    txt2 = frmeditMyProfile.txtAddress2.text;
    if (addressedited == true) {
      province = gblStateValue;
      district = gbldistrictValue;
      subdistrict = gblsubdistrictValue;
      zipcode = gblzipcodeValue;
    } else {
      province = StateValue;
      district = districtValue;
      subdistrict = subdistrictValue;
      zipcode = zipcodeValue;
    }


    verifyPWDMyProfileMB_InputParam["globalvar_gblDeviceNickName"] = frmMyProfile.label47502979411853.text;
    verifyPWDMyProfileMB_InputParam["globalvar_gblDeviceNickNameNew"] = frmeditMyProfile.txtdevicenamevalue.text;
    verifyPWDMyProfileMB_InputParam["globalvar_GBLemailALOld"] = EmailHide(frmMyProfile.lblEmailVal.text);
    verifyPWDMyProfileMB_InputParam["globalvar_GBLemailAL"] = EmailHide(frmeditMyProfile.txtemailvalue.text); //if editited
    //email un masking for email notification
    verifyPWDMyProfileMB_InputParam["globalvar_UnMaskGBLemailALOld"] = frmMyProfile.lblEmailVal.text;
    verifyPWDMyProfileMB_InputParam["globalvar_UnMaskGBLemailAL"] = frmeditMyProfile.txtemailvalue.text; //if editited

    verifyPWDMyProfileMB_InputParam["globalvar_fbIdOld"] = gblFacebookIdOld;
    verifyPWDMyProfileMB_InputParam["globalvar_gblFacebookId"] = gblFacebookId; //new email
    if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) verifyPWDMyProfileMB_InputParam["globalvar_gblAddress"] = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district + " " + province + " " + zipcode; //editited
    else verifyPWDMyProfileMB_InputParam["globalvar_gblAddress"] = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district + " " + province + " " + zipcode; //editited
    verifyPWDMyProfileMB_InputParam["globalvar_gblAddressOld"] = frmMyProfile.lblContactAdd2.text + " " + frmMyProfile.lblContactAdd3.text;
    verifyPWDMyProfileMB_InputParam["globalvar_gblTokenSwitchFlag"] = gblTokenSwitchFlag;
    verifyPWDMyProfileMB_InputParam["globalvar_base64ImageString"] = gblEditBase64List;
    verifyPWDMyProfileMB_InputParam["globalvar_fileType"] = gblEditPhotoSource;
    verifyPWDMyProfileMB_InputParam["globalvar_profilePicFlagIB"] = profilePicFlagSPA;
    //ending the global params
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_loginModuleId"] = "MB_TxPwd";
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_userStoreId"] = "DefaultStore";
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_password"] = password;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_caseTrans"] = caseTrans;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileEmailFlag"] = profileEmailFlag;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileAddrFlag"] = profileAddrFlag;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileDeviceNameFlag"] = profileDeviceNameFlag;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profilePicFlag"] = profilePicFlag;
    verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileedit"] = profileedit;
    if (flowSpa) {
      verifyPWDMyProfileMB_InputParam["channelId"] = "IB";
    } else {
      verifyPWDMyProfileMB_InputParam["channelId"] = "MB";
    }
    //address flag
    //sending addresschnage
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
      verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Language"] = "EN";
    } else {
      verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Language"] = "TH";
    }
    var primaryaddrtype = "Primary";
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr1"] = txt1;
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr2"] = txt2;
    //verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = "testing";
    if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
      verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district;
    } else {
      verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district;
    }
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_City"] = province; //kony.i18n.getLocalizedString("BangkokThaiValue");
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_StateProv"] = gblnewStateValue;
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_PostalCode"] = zipcode;
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_AddrType"] = primaryaddrtype;
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_CountryCodeValue"] = kony.i18n.getLocalizedString("Thailand");
    //params for device update
    var deviceNickname = frmeditMyProfile.txtdevicenamevalue.text;
    verifyPWDMyProfileMB_InputParam["deviceNameUpdate_deviceId"] = GBL_UNIQ_ID;
    verifyPWDMyProfileMB_InputParam["deviceNameUpdate_deviceNickName"] = deviceNickname;
    //editemail
    //verifyPWDMyProfileMB_InputParam["crmProfileMod_actionType"] = "333";
    verifyPWDMyProfileMB_InputParam["crmProfileMod_emailAddr"] = frmeditMyProfile.txtemailvalue.text;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    invokeServiceSecureAsync("MyProfileModifyCompositeJavaService", verifyPWDMyProfileMB_InputParam, MyProfileModifyOperationcallBack)
  }
}

function MyProfileModifyOperationcallBack(status, resulttable) {
  var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
  var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
  var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
  var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";



  if (status == 400) {
    kony.application.dismissLoadingScreen();
    if (resulttable["opstatus"] == 0) {
      if (resulttable["statusCode"] == 0) {
        popupTractPwd.dismiss();
        onClickCancelAccessPin();
        completeicon = true;

        if(resulttable["errMsg_addr"] != null && resulttable["errMsg_addr"] != undefined )
        {

          if(resulttable["errMsg_addr"].trim() == "Maximum Length Exceeded")
            showAlert(kony.i18n.getLocalizedString("keyAdressMaxLengthExceeded"), kony.i18n.getLocalizedString("info"));
          else 	showAlert(kony.i18n.getLocalizedString("keyPartyUpdateFailure"), kony.i18n.getLocalizedString("info"));

          completeicon = false;
        }

        if (resulttable["ebMaxLimitAmtCurrent"] != null) {

          gblEBMaxLimitAmtCurrent = resulttable.ebMaxLimitAmtCurrent;
          gblEBMaxLimitAmtCurrentOld = gblEBMaxLimitAmtCurrent;
          frmMyProfiles.lblDailytransLimitvalue.text = ProfileCommFormat(gblEBMaxLimitAmtCurrent)+ " " +kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        if (resulttable["ebMaxLimitAmtHist"] != null) {

          gblEBMaxLimitAmtHist = resulttable.ebMaxLimitAmtHist;
        }
        if (resulttable["ebMaxLimitAmtRequest"] != null) {

          gblEBMaxLimitAmtReq = resulttable.ebMaxLimitAmtRequest;
        }
        if (resulttable["emailAddr"] != null) {

          gblEmailAddr = resulttable.emailAddr;
          gblEmailAddrOld = gblEmailAddr;
          frmMyProfiles.lblEmailVal.text = gblEmailAddr;
        }
        //if (resulttable["fbUsername"] != null && resulttable["fbUsername"] != "null" && resulttable["fbUsername"] != "")  {

        gblFacebookId = resulttable.fbUsername;
        gblFacebookIdOld = gblFacebookId;
        frmMyProfiles.lblfbidstudio1.text = gblFacebookId;//Modified by Studio Viz

        gblFBCode = resulttable["facebookId"];
        if (resulttable["ebTxnLimitAmt"] != null) {

          gblEbTxnLimitAmt = resulttable.ebTxnLimitAmt;
          gblEbTxnLimitAmtOld = gblEbTxnLimitAmt;
        }
        if(resulttable["deviceNickName"]!=null){
          gblDeviceNickName = resulttable["deviceNickName"];
          //gblDeviceNickNameOld = gblDeviceNickName;

          frmMyProfiles.lblMyDeviceVal.text = gblDeviceNickName;
        }			


        gblCustomerName = resulttable["customerName"];
        gblCustomerNameTh = resulttable["customerNameTH"];
        customerName = resulttable["customerName"];
        if (kony.i18n.getCurrentLocale() == "th_TH") {
          frmMyProfiles.lblProfileDescription.text = gblCustomerNameTh;
        } else {
          frmMyProfiles.lblProfileDescription.text = gblCustomerName;
        }
        for (var i = 0; i < resulttable["ContactNums"].length; i++) {
          var PhnType = resulttable["ContactNums"][i]["PhnType"];

          if (PhnType != null && PhnType != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined) {
            if (PhnType == "Mobile") {
              if ((resulttable["ContactNums"][i]["PhnNum"] != null) && (resulttable["ContactNums"][i]["PhnNum"] != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined)) {
                gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
                gblPHONENUMBEROld = resulttable["ContactNums"][i]["PhnNum"];
                frmMyProfiles.lblMobileVal.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                GblMobileAL = HidePhnNum(gblPHONENUMBER);
              }
            }
          }
        }
        for (var i = 0; i < resulttable["Persondata"].length; i++) {
          var tempAddrtype = resulttable["Persondata"][i]["AddrType"];
          if (tempAddrtype == "Primary") {
            if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
              //if(resulttable["Persondata"][i]["addr3"]){}
              if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                var adr3 = resulttable["Persondata"][i]["addr3"];

                var reg = / {1,}/;
                var tempArr = [];
                tempArr = adr3.split(reg);

                //if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) 
                {
                  if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                    if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                      if(tempArr[0].indexOf(subDistBan, 0) >= 0)
                        gblsubdistrictValue = tempArr[0].substring(4);
                      else  if(tempArr[0].indexOf(subDistNotBan, 0) >= 0)   
                        gblsubdistrictValue = tempArr[0].substring(2);
                      else gblsubdistrictValue = "";	
                      if(tempArr[1].indexOf(distBan, 0) >= 0)  	 
                        gbldistrictValue = tempArr[1].substring(3);
                      else   if(tempArr[1].indexOf(distNotBan, 0) >= 0)  
                        gbldistrictValue = tempArr[1].substring(2); 
                      else 	gbldistrictValue = "";
                    } 
                  } else {
                    gblStateValue = "";
                    gblsubdistrictValue = "";
                    gbldistrictValue = "";
                  }
                  gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                  gblAddress2Value = resulttable["Persondata"][i]["addr2"];
                  if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
                    gblStateValue = resulttable["Persondata"][i]["City"];
                  else	gblStateValue = "";
                  if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
                    gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                  else 	
                    gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                  if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
                    gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                  else	gblcountryCode = "";
                  if (tempArr[0] == "" || tempArr[0] == undefined) {
                    tempArr[0] = "";
                  }
                  if (tempArr[1] == "" || tempArr[1] == undefined) {
                    tempArr[1] = "";
                  }
                  gblViewsubdistrictValue = tempArr[0];
                  gblViewdistrictValue = tempArr[1];

                  frmMyProfiles.lblContactVal.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
                  frmeditMyProfile.txtAddress1.text = gblAddress1Value;
                  frmeditMyProfile.txtAddress2.text = gblAddress2Value;
                  frmeditMyProfiles.txtAddress1.text = gblAddress1Value;
                  frmeditMyProfiles.txtAddress2.text = gblAddress2Value;
                  frmeditMyProfiles.txtSubDistrict.text = gblsubdistrictValue;
                  frmeditMyProfiles.lblDistrictVal.text = gbldistrictValue;
                  frmeditMyProfiles.lblProvinceVal.text = gblStateValue;
                  frmeditMyProfiles.lblZipCodeVal.text = gblzipcodeValue;
                  gblnotcountry = false;
                  if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                    gblnotcountry = true;
                  } else {
                    gblnotcountry = false;
                  }
                  gblAddress = frmMyProfiles.lblContactVal.text;
                }
              }
            }
          } else if (tempAddrtype == "Registered") {

            //alert("address3 of the person data"+resulttable["Persondata"][i]["addr3"]);
            if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
              if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                var adr3 = resulttable["Persondata"][i]["addr3"];
                var reg = / {1,}/;
                var tempArr = [];
                tempArr = adr3.split(reg);
                //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 

                if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                  if (resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
                    gblregsubdistrictValue = tempArr[0];
                    gblregdistrictValue = tempArr[1];
                  } else {
                    gblregsubdistrictValue = tempArr[0];
                    gblregdistrictValue = tempArr[1];
                  }
                }
                else {
                  gblregStateValue = "";
                  gblregsubdistrictValue = tempArr[0];
                  gblregdistrictValue = tempArr[1];
                }
                gblregAddress1Value = resulttable["Persondata"][i]["addr1"];
                gblregAddress2Value = resulttable["Persondata"][i]["addr2"];
                if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
                  gblregStateValue = resulttable["Persondata"][i]["City"];
                else gblregStateValue = "";
                if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
                  gblregzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                else 	gblregzipcodeValue = "";
                if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
                  gblregcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                else gblregcountryCode = "";									
                frmMyProfiles.lblRegisterAddressVal.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue + " " + gblregdistrictValue + " " + " " + gblregStateValue + " " + " " + gblregzipcodeValue + " " + " " + gblregcountryCode;
              }
            }
          }
        }
      }
      if(gblLoanNav == ""){
        frmMyProfiles.show();
      }else{
        if (!isNotBlank(kony.store.getItem("cachedProfilePic"))) {
          frmMyProfiles.imgProfile.src = "avatar.png";
        } else {
          frmMyProfiles.imgProfile.base64 = kony.store.getItem("cachedProfilePic");
        }
        frmLoanPersonalInfo.lblEmailWithData.text = gblEmailAddr;
        frmLoanPersonalInfo.lblContactAddressInfo.text = gblAddress;
        completeicon = false;
        frmLoanPersonalInfo.show();
      }
    } else if(resulttable["errMsg"]=="duplicateDeviceName") {
      alert(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00003"))
    } else {
      //MB handling
      if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
        showTranPwdLockedPopup();
        popupTractPwd.dismiss();
        return;
      } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
        setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
        return false;
      }else if (resulttable["errCode"] == "VrfyAcPWDErr00003") {
        onClickCancelAccessPin();
        gotoUVPINLockedScreenPopUp();
        return false;
      } else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
        popupEnterAccessPin.lblWrongPin.setVisibility(true);
        kony.print("invalid pin transfer flow"); //To do : set red skin to enter access pin
        resetAccessPinImg(resulttable["badLoginCount"]);
        return false;
      } 
      if (resulttable["errmsg"] != undefined) {
        if (flowSpa) {
          popOtpSpa.dismiss();
        } else {
          popupTractPwd.dismiss();
          onClickCancelAccessPin();
        }
        alert(resulttable["errmsg"]);
      } else {
        popupTractPwd.dismiss();
        onClickCancelAccessPin();

        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
      }
    }
  } //status 400
}

//-------END of  Change limit composite service ------------------------------------------

function CompositeChangeMobile() {
  kony.print("inside CompositeChangeMobile");
  var newNum;
  var text1;
  var textpass = "";
  var inputParam = {};
  var chan = gblDeviceInfo.name;
  if (chan == "thinclient") {
    //inputParam["channelId"] = GLOBAL_IB_CHANNEL;
    if (!flowSpa) newNum = frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber.text;
    else newNum = frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text;
    ccode = "IB";
  } else {
    //inputParam["channelId"] = GLOBAL_MB_CHANNEL;
    text1 = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
    newNum = frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text;
    ccode = "MB";
  }

  /* if(ccode = "IB"){
   		alert("inside ccode=ib");
       frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
       alert("point 3");
    }*/
  inputParam["segmentIdVal"] = "MIB";
  if (flowSpa) {
    if (gblTokenSwitchFlag == true) {
      inputParam["password"] = popOtpSpa.txttokenspa.text;
      popOtpSpa.txttokenspa.text = "";
      inputParam["loginModuleId"] = "IB_HWTKN";
    } else {
      inputParam["password"] = popOtpSpa.txtOTP.text;
      popOtpSpa.txtOTP.text = "";
    }
    //inputParam["channel"] = "IB";
  }
  if (ccode == "MB") {
    // popupTractPwd.dismiss();
    if (text1 == "" || text1 == null) {
      popupTractPwd.show();
      setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
      return false;
    } else {
      //inputParam["channelName"] = "MB";
      //inputParam["channel"] = "MB";
      inputParam["password"] = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
      inputParam["loginModuleId"] = "MB_TxPwd";
    }
  } else if (gblTokenSwitchFlag == true && ccode == "IB" && !flowSpa) {
    textpass = frmIBCMChgMobNoTxnLimit.tbxToken.text;
    //inputParam["password"] = frmIBCMChgMobNoTxnLimit.tbxToken.text;
    frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
    inputParam["loginModuleId"] = "IB_HWTKN";
    //inputParam["channelName"] = "IB";
    //inputParam["channel"] = "Internet Banking";
  } else if (!flowSpa) {
    textpass = frmIBCMChgMobNoTxnLimit.txtBxOTP.text;
    //inputParam["password"] = frmIBCMChgMobNoTxnLimit.txtBxOTP.text;
    //inputParam["channelName"] = "IB";
    //inputParam["channel"] = "Internet Banking";
  }
  if(ccode == "IB" && !flowSpa)
  {
    if (textpass == "" || textpass == null) {
      if(gblTokenSwitchFlag == true)
      {
        frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
        alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
      }
      else
      {
        frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
        alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
      }
      return false;
    } else {
      inputParam["password"] = textpass;
    }	
  }
  inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
  tempOldPhoneNo = gblPHONENUMBEROld;
  var oldno = HidePhnNum(gblPHONENUMBEROld);
  var newno = HidePhnNum(gblPhoneNumberReq);
  newNum = newNum.toString().replace(/-/g, "");
  //verifypassword
  // inputParam["loginModuleId"] = "MB_TxPwd";
  inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
  inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
  inputParam["userStoreId"] = "DefaultStore";
  inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
  //custmobileno
  inputParam["MobileNoNew"] = newNum;
  if (flowSpa) {
    inputParam["ProdCode"] = "02";
  }
  //al
  /*if (kony.os.deviceInfo().name == "thinclient") inputParam["channelId"] = GLOBAL_IB_CHANNEL;
    else inputParam["channelId"] = GLOBAL_MB_CHANNEL;*/
  inputParam["logLinkageId"] = "";
  //notification
  inputParam["customerName"] = customerName;
  inputParam["flagVerify"] = "OLD";
  inputParam["source"] = "changeMobileNumberSMS";
  // inputParam["phoneNumber"] = tempOldPhoneNo;
  //inputParam["newMobileNumber"] = gblPhoneNumberReq;
  inputParam["notificationType"] = "Sms";
  inputParam["notificationSubject"] = "";
  inputParam["notificationContent"] = "";
  inputParam["Locale"] = kony.i18n.getCurrentLocale();
  if (ccode == "MB" || flowSpa) showLoadingScreen();
  else showLoadingScreenPopup();
  kony.print("before calling newChangeMobileComposite inputParam----->" + JSON.stringify(inputParam));
  invokeServiceSecureAsync("newChangeMobileComposite ", inputParam, callbackCompositeChangeMobile);
}

function callbackCompositeChangeMobile(status, resulttable) {

  kony.print("inside callbackCompositeChangeMobile" + JSON.stringify(resulttable));

  if (status == 400) {
    if (ccode == "IB" && !flowSpa) {
      frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
      frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
    }
    if (ccode == "MB") {
      popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
    if (flowSpa) {
      popOtpSpa.txtOTP.text = "";
      popOtpSpa.txttokenspa.text = "";
    }
    if (ccode == "MB" || flowSpa) dismissLoadingScreen();
    else dismissLoadingScreenPopup();

    if (resulttable["opstatus_verifypwd"] != 0) {
      if (resulttable["opstatus"] == 8005) {
        if (ccode == "MB" || flowSpa) {
          if (flowSpa) {
            popOtpSpa.txtOTP.text = "";
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
              gblVerifyOTPCounter = "0";
              popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
              popOtpSpa.lblPopupTract4Spa.text = "";
              kony.application.dismissLoadingScreen();
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
              gblVerifyOTPCounter = "0";
              otplocked = true;
              kony.application.dismissLoadingScreen();
              popOtpSpa.dismiss();
              popTransferConfirmOTPLock.show();
              // calling crmprofileMod to update the user status
              //updteuserSpa();
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
              //gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
              gblVerifyOTPCounter = "0";
              alert("" + resulttable["errMsg"]);
              return false;
            }
          } else {
            if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
              showTranPwdLockedPopup();
              popupTractPwd.dismiss();
              return false;
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
              setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
              return false;
            } else {
              dismissLoadingScreen();
              if (resulttable["errMsg"] != undefined) {
                alert("" + resulttable["errMsg"]);
              }
            }
          }
        } else if (!flowSpa) {
          MobileOTPCNT = 1;
          flagMobNum = "old";
          frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
          frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
          if (resulttable["errCode"] == "VrfyOTPErr00001") {
            gblRetryCountRequestOTP = resulttable["retryCounterVerifyOTP"];
            dismissLoadingScreenPopup();
            //  alert("" + kony.i18n.getLocalizedString("invalidOTP"));
            frmIBCMChgMobNoTxnLimit.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone"); //kony.i18n.getLocalizedString("invalidOTP"); //
            frmIBCMChgMobNoTxnLimit.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
            frmIBCMChgMobNoTxnLimit.hbxOTPincurrect.isVisible = true;
            frmIBCMChgMobNoTxnLimit.hbox476047582127699.isVisible = false;
            frmIBCMChgMobNoTxnLimit.hbxOTPsnt.isVisible = false;
            frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
            frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
            //frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
            if (gblTokenSwitchFlag == true) frmIBCMChgMobNoTxnLimit.tbxToken.setFocus(true);
            else frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
            dismissLoadingScreenPopup();
            handleOTPLockedIB(resulttable);
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("invalidOTP"));
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
            if (resulttable["errMsg"] != undefined) {
              alert("" + resulttable["errMsg"]);
            }
            return false;
          } else {
            dismissLoadingScreenPopup();
            if (resulttable["errMsg"] != undefined) {
              alert("" + resulttable["errMsg"]);
            }
          }
        }
      } else if (resulttable["code"] != null && resulttable["code"] == "10403") {
        if (ccode == "MB") {
          popupTractPwd.dismiss();
          gblVerifyOTPCounter = "0";
          showTranPwdLockedPopup();
        } else if (resulttable["code"] != null && resulttable["code"] == "10020") {
          showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
        } else {

          if (resulttable["errMsg"] != undefined) {
            alert("" + resulttable["errMsg"]);
          }
        }
      } else {
        if (resulttable["code"] != null && resulttable["code"] == "10403") {
          if (ccode == "IB") {
            frmIBCMConfirmation.txtOTP.text = "";
            gblVerifyOTPCounter = "0";
            handleOTPLockedIB(resulttable);
            frmIBCMConfirmation.btnReqOTP.skin = btnIBgreyInactive;
            frmIBCMConfirmation.btnReqOTP.setEnabled(false);
          }
        } else if (resulttable["code"] != null && resulttable["code"] == "10020") {
          if (ccode == "IB") {
            frmIBCMConfirmation.txtOTP.text = "";
            showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
          }
        } else {

          if (resulttable["errMsg "] != undefined) {
            alert("" + resulttable["errMsg "]);
          }
          if (ccode == "IB" && !flowSpa) {
            frmIBCMConfirmation.txtOTP.text = "";
          }
        }
      }
    } else if (resulttable["opstatus"] == "0") {
      kony.print("()()()()()()()()()()()()()()()()()()()()()()()()()");
      if (resulttable["newMobileNo"] != null || resulttable["newMobileNo"] != "") {
        gblPHONENUMBER = resulttable["newMobileNo"];
        gblPHONENUMBEROld = resulttable["newMobileNo"];
        if (ccode == "MB" || flowSpa) {
          popupTractPwd.dismiss();
          completeicon = true;
          gblRetryCountRequestOTP = "0";
          frmMyProfiles.show();
        } else {
          flagMobNum = "new";
          MobileOTPCNT = 0;
          completeicon = true;
          frmIBCMMyProfile.show();
        }
      }
    } else {
      if (resulttable["errMsg"] != undefined) {
        alert("" + resulttable["errMsg"]);
      } else {
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
      }
    }
  } else if (status == 300) {
    if (ccode == "MB" || flowSpa) {
      dismissLoadingScreen();
    } else {
      dismissLoadingScreenPopup();
    }
  }
}

function CompositeChangeLimit() {
  var inputParam = {};

  var chan = gblDeviceInfo.name;
  if (chan == "thinclient") {
    //inputParam["channelId"] = GLOBAL_IB_CHANNEL;
    ccode = "IB";
  } else {
    inputParam["channelId"] = GLOBAL_MB_CHANNEL;
    ccode = "MB";
  }
  var newlimit;
  if (flowSpa) {
    if (gblTokenSwitchFlag == true) {
      inputParam["password"] = popOtpSpa.txttokenspa.text;
      inputParam["loginModuleId"] = "IB_HWTKN";
      popOtpSpa.txttokenspa.text = "";
    } else {
      inputParam["password"] = popOtpSpa.txtOTP.text;
      popOtpSpa.txtOTP.text = "";
    }
    newlimit = UndocommaFormatted(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    nl = UndocommaFormatted(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    //inputParam["channel"] = "IB";
  }
  if (ccode == "MB") {
    // popupTractPwd.dismiss();
    //inputParam["channel"] = "MB";
    newlimit = UndocommaFormatted(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    nl = UndocommaFormatted(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    var pass = "";
    if(gblAuthAccessPin == true)
      pass = gblNum;
    else          
      pass = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;

    if (pass == null || pass == '') {
      setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
      return false;
    }
    inputParam["password"] = pass;   //popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
    inputParam["loginModuleId"] = "MB_TxPwd";
  } else if (gblTokenSwitchFlag == true && ccode == "IB" && !flowSpa) {
    newlimit = UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    nl = UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    inputParam["password"] = frmIBCMChgMobNoTxnLimit.tbxToken.text;
    inputParam["loginModuleId"] = "IB_HWTKN";
    //inputParam["channel"] = "Internet Banking";
  } else if (!flowSpa) {
    newlimit =  UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    nl =  UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    inputParam["password"] = frmIBCMChgMobNoTxnLimit.txtBxOTP.text;
    //inputParam["channel"] = "Internet Banking";
  }
  //yet to handle token ib otp and mb pswds
  inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag;
  inputParam["userStoreId"] = "DefaultStore";
  inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
  inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
  inputParam["retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
  inputParam["sessionVal"] = "";
  inputParam["segmentId"] = "segmentId";
  inputParam["segmentIdVal"] = "MIB";
  //crmprofilemod
  //notification
  gblProfileNotifyFlag = false;
  var diff;
  newlimit = newlimit.toString().replace(/,/g, "");
  newlimit = parseFloat(newlimit);
  diff = parseFloat(diff);
  var currLimit = parseFloat(gblEBMaxLimitAmtCurrent);
  if (newlimit != currLimit) {
    if (currLimit > newlimit) {
      diff = currLimit - newlimit;
    } else {
      diff = newlimit - currLimit;
    }
    diff = diff.toFixed(2);
  }
  diff = diff.toString();
  newlimit = newlimit.toString();
  currLimit = currLimit.toString();
  nl = nl.toString().replace(/,/g, "");
  diff = ProfileCommFormat(diff) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  newlimit = ProfileCommFormat(newlimit) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  currLimit = ProfileCommFormat(currLimit) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  var oldLimit = ProfileCommFormat(gblEBMaxLimitAmtCurrent) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  inputParam["newlimit"] = newlimit;
  inputParam["oldlimit"] = parseFloat(gblEBMaxLimitAmtCurrent);
  if (ccode == "IB") {
    inputParam["customerName"] = customerNameIB;
  } else {
    inputParam["customerName"] = customerName;
  }
  //inputParam["notificationType"] = "Email"; // check this : which value we have to pass
  //inputParam["notificationSubject"] = "";
  //    inputParam["notificationContent"] = "";
  inputParam["NoSendInd"] = "0";
  inputParam["Locale"] = kony.i18n.getCurrentLocale();
  inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
  //inputParam["oldlimit"] = oldLimit;
  //    inputParam["newlimit"] = newlimit;
  //    inputParam["diffrence"] = diff;
  //    inputParam["source"] = "changeChannelLimit";
  //    inputParam["iniLimit"] = kony.os.toNumber(GLOBAL_INI_MAX_LIMIT_AMT);
  //    inputParam["val"] = nl;
  //    inputParam["requestedAmt"] = nl;
  //    inputParam["histLimit"] = kony.os.toNumber(gblEBMaxLimitAmtHist);
  //al
  //var OldL = ProfileCommFormat(gblEBMaxLimitAmtCurrent);
  //    var newL = ProfileCommFormat(nl);
  //    inputParam["activityFlexValues"] = OldL + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + " to " + newL + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  //    inputParam["deviceNickName"] = "testing";
  //inputParam["logLinkageId"] = "";
  //    inputParam["activityFlexValues1"] = oldLimit;
  //    inputParam["activityFlexValues2"] = newlimit;
  if (ccode == "MB" || flowSpa) showLoadingScreen();
  else if (ccode == "IB") showLoadingScreenPopup();
  invokeServiceSecureAsync("chgTransLimitCompositeService", inputParam, callbackchgTransLimitCompositeService);
}

function callbackchgTransLimitCompositeService(status, resulttable) {

  if (status == 400) {
    if (ccode == "IB" && !flowSpa) {
      frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
      frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
    }
    if (ccode == "MB") {
      popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
    if (flowSpa) {
      popOtpSpa.txtOTP.text = "";
      popOtpSpa.txttokenspa.text = "";
    }
    if (ccode == "MB" || flowSpa) dismissLoadingScreen();
    else dismissLoadingScreenPopup();

    if (resulttable["opstatus_verifypwd"] != 0) {
      if (resulttable["opstatus"] == 8005) {
        if (ccode == "MB" || flowSpa) {
          if (flowSpa) {
            popOtpSpa.txtOTP.text = "";
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
              gblVerifyOTPCounter = "0";
              popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
              popOtpSpa.lblPopupTract4Spa.text = "";
              kony.application.dismissLoadingScreen();
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
              gblVerifyOTPCounter = "0";
              otplocked = true;
              kony.application.dismissLoadingScreen();
              popOtpSpa.dismiss();
              popTransferConfirmOTPLock.show();
              // calling crmprofileMod to update the user status
              //updteuserSpa();
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
              //gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
              gblVerifyOTPCounter = "0";
              alert("" + resulttable["errMsg"]);
              return false;
            }
          } else {
            if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
              showTranPwdLockedPopup();
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
              setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
              return false;
            } else if(resulttable["errCode"] == "VrfyAcPWDErr00003"){
              onClickCancelAccessPin();
              gotoUVPINLockedScreenPopUp();
              return false;
            } else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
              popupEnterAccessPin.lblWrongPin.setVisibility(true);
              kony.print("invalid pin transfer flow"); //To do : set red skin to enter access pin
              resetAccessPinImg(resulttable["badLoginCount"]);
              return false;
            }
            else {
              dismissLoadingScreen()
              alert("" + resulttable["errMsg"]);
            }
          }
        } else if (!flowSpa) {
          if (resulttable["errCode"] == "VrfyOTPErr00001") {
            gblRetryCountRequestOTP = resulttable["retryCounterVerifyOTP"];
            dismissLoadingScreenPopup();
            //alert("" + kony.i18n.getLocalizedString("invalidOTP"));
            /* if(ccode == "IB"){
                          frmIBCMChgMobNoTxnLimit.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");
	                      frmIBCMChgMobNoTxnLimit.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
	                      frmIBCMChgMobNoTxnLimit.hbxOTPincurrect.isVisible = true;
	                      frmIBCMChgMobNoTxnLimit.hbox476047582127699.isVisible = false;
	                      frmIBCMChgMobNoTxnLimit.hbxOTPsnt.isVisible = false;
	                      frmIBCMChgMobNoTxnLimit.txtBxOTP.text = " ";
	                      frmIBCMChgMobNoTxnLimit.tbxToken.text = " ";
	                      if(gblTokenSwitchFlag == true && gblSwitchToken == false){
                      frmIBCMChgMobNoTxnLimit.tbxToken.setFocus(true);
                    }else{
                       frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
                    }
                       }*/
            frmIBCMChgMobNoTxnLimit.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");
            frmIBCMChgMobNoTxnLimit.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
            frmIBCMChgMobNoTxnLimit.hbxOTPincurrect.isVisible = true;
            frmIBCMChgMobNoTxnLimit.hbox476047582127699.isVisible = false;
            frmIBCMChgMobNoTxnLimit.hbxOTPsnt.isVisible = false;
            frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
            dismissLoadingScreenPopup();
            handleOTPLockedIB(resulttable);
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("invalidOTP"));
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
            if (resulttable["errMsg"] != undefined) {
              alert("" + resulttable["errMsg"]);
            }
            return false;
          } else {
            //if(ccode == "IB"){
            frmIBCMChgMobNoTxnLimit.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
            frmIBCMChgMobNoTxnLimit.lblPlsReEnter.text = " "; 
            frmIBCMChgMobNoTxnLimit.hbxOTPincurrect.isVisible = false;
            frmIBCMChgMobNoTxnLimit.hbox476047582127699.isVisible = true;
            frmIBCMChgMobNoTxnLimit.hbxOTPsnt.isVisible = true;
            frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);   
            // }  
            dismissLoadingScreenPopup();
            if (resulttable["errMsg"] != undefined) {
              alert("" + resulttable["errMsg"]);
            }
          }
        }
      } else if (resulttable["code"] != null && resulttable["code"] == "10403") {
        if (ccode == "MB") {
          gblVerifyOTPCounter = "0";
          if(gblAuthAccessPin == true) {
            onClickCancelAccessPin();
            gotoUVPINLockedScreenPopUp();
          } else {
            showTranPwdLockedPopup();
          }

        } else if (resulttable["code"] != null && resulttable["code"] == "10020") {
          showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
        } else {

          alert(resulttable["errMsg"]);
        }
      } else {
        if (resulttable["code"] != null && resulttable["code"] == "10403") {
          if (ccode == "IB") {
            frmIBCMConfirmation.txtOTP.text = "";
            gblVerifyOTPCounter = "0";
            handleOTPLockedIB(resulttable);
            frmIBCMConfirmation.btnReqOTP.skin = btnIBgreyInactive;
            frmIBCMConfirmation.btnReqOTP.setEnabled(false);
          }
        } else if (resulttable["code"] != null && resulttable["code"] == "10020") {
          if (ccode == "IB") {
            frmIBCMConfirmation.txtOTP.text = "";
            showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null); 
          }
        } else {

          alert(resulttable["errMsg"]);
          if (ccode == "IB") {
            frmIBCMConfirmation.txtOTP.text = "";
          }
        }
      }
    } else if (resulttable["opstatus_changeTransLimit"] == 0 || resulttable["opstatus"] == 0) {
      if (flowSpa) popOtpSpa.dismiss();
      if(ccode == "MB" || flowSpa){
        onClickCancelAccessPin();
      }

      if (resulttable["ebMaxLimitAmtCurrent"] != null) {

        gblEBMaxLimitAmtCurrent = resulttable.ebMaxLimitAmtCurrent;
        gblEBMaxLimitAmtCurrentOld = gblEBMaxLimitAmtCurrent;
        if (ccode == "MB" || flowSpa) {
          frmMyProfiles.lblDailytransLimitvalue.text = ProfileCommFormat(gblEBMaxLimitAmtCurrent)+ " " +kony.i18n.getLocalizedString("currencyThaiBaht");
        } else if (!flowSpa) {
          frmIBCMMyProfile.lblDailytransLimitvalue.text = ProfileCommFormat(gblEBMaxLimitAmtCurrent)+ " " +kony.i18n.getLocalizedString("currencyThaiBaht");
        }
      }
      if (resulttable["ebMaxLimitAmtHist"] != null) {

        gblEBMaxLimitAmtHist = resulttable.ebMaxLimitAmtHist;
      }
      if (resulttable["opstatus_getServiceReq"] == 0) {
        if (ccode == "MB" || flowSpa) serviceProfileFlag = "MB";
        else serviceProfileFlag = "IB";
        var PrevCCAmt = resulttable["activity_flex_values"];
        PrevCCAmt = extractToValue(PrevCCAmt);
        callPopChkServReq(PrevCCAmt);
      } else {
        if (ccode == "MB" || flowSpa) {
          popupTractPwd.dismiss();
          popupTractPwd.destroy();
          onClickCancelAccessPin();
          completeicon = true;
          frmMyProfiles.show();
          gblRetryCountRequestOTP = "0";
        } else if (!flowSpa) {
          completeicon = true;
          frmIBCMMyProfile.show();
        }
      }
      if(resulttable["opstatus_addServiceReq"] == 0) {
        if (ccode == "MB") 
        {
          if (callLimitedit) {
            callLimitedit = false;
            popUpCallCancel.show();
          }
        }
      }
    } else {
      if (resulttable["errMsg"] != undefined) {
        alert(resulttable["errMsg"]);
      } else {
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
      }
    }
  } else if (status == 300) {
    if (ccode == "MB" || flowSpa) dismissLoadingScreen();
    else dismissLoadingScreenPopup();
  }
}

function commoncancelLimit() {
  var inputParam = {};

  var chan = gblDeviceInfo.name;
  if (chan == "thinclient" && !flowSpa) {
    //frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text = "";
    popupDeleAccnt.dismiss();
    // frmIBCMChgMobNoTxnLimit.show();
    //inputParam["channelId"] = GLOBAL_IB_CHANNEL;
    ccode = "IB";
  } else {
    popupConfirmation.dismiss();
    //inputParam["channelId"] = GLOBAL_MB_CHANNEL;
    ccode = "MB";
  }
  var newlimit;
  if (ccode == "MB") {
    // popupTractPwd.dismiss();
    //inputParam["channel"] = "MB";
    newlimit = UndocommaFormatted(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
    nl = UndocommaFormatted(frmChangeMobNoTransLimitMB.txtChangeTransactionLimit.text);
  } else if (gblTokenSwitchFlag == true && ccode == "IB") {
    //popupDeleAccnt.dismiss();
    newlimit = UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    nl = UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    //inputParam["channel"] = "Internet Banking";
  } else {
    newlimit = UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    nl = UndocommaFormatted(frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text);
    //inputParam["channel"] = "Internet Banking";
  }
  //yet to handle token ib otp and mb pswds
  var oldLimit = parseFloat(gblEBMaxLimitAmtCurrent);
  inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
  inputParam["userStoreId"] = "DefaultStore";
  inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
  inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
  inputParam["retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
  inputParam["sessionVal"] = "";
  inputParam["segmentId"] = "segmentId";
  inputParam["segmentIdVal"] = "MIB";
  inputParam["oldlimit"] = oldLimit;
  inputParam["newlimit"] = newlimit;
  //crmprofilemod
  //notification
  gblProfileNotifyFlag = false;
  var diff;
  newlimit = newlimit.toString().replace(/,/g, "");
  newlimit = parseFloat(newlimit);
  diff = parseFloat(diff);
  var currLimit = parseFloat(gblEBMaxLimitAmtCurrent);
  if (newlimit != currLimit) {
    if (currLimit > newlimit) {
      diff = currLimit - newlimit;
    } else {
      diff = newlimit - currLimit;
    }
    diff = diff.toFixed(2);
  }
  diff = diff.toString();
  newlimit = newlimit.toString();
  currLimit = currLimit.toString();
  nl = nl.toString().replace(/,/g, "");
  diff = ProfileCommFormat(diff) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  newlimit = ProfileCommFormat(newlimit) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  currLimit = ProfileCommFormat(currLimit) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  var oldLimit = ProfileCommFormat(gblEBMaxLimitAmtCurrent) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  if (ccode == "IB") {
    inputParam["customerName"] = customerNameIB;
  } else {
    inputParam["customerName"] = customerName;
  }
  //inputParam["notificationType"] = "Email"; // check this : which value we have to pass
  //    inputParam["notificationSubject"] = "";
  //    inputParam["notificationContent"] = "";
  //    inputParam["NoSendInd"] = "0";
  //    inputParam["Locale"] = kony.i18n.getCurrentLocale();
  //    inputParam["txnRefNo"] = "1234";
  //    inputParam["oldlimit"] = oldLimit;
  //    inputParam["newlimit"] = newlimit;
  //    inputParam["diffrence"] = diff;
  //    inputParam["source"] = "changeChannelLimit";
  //    inputParam["iniLimit"] = kony.os.toNumber(GLOBAL_INI_MAX_LIMIT_AMT);
  //    inputParam["val"] = nl;
  //    inputParam["requestedAmt"] = nl;
  //    inputParam["histLimit"] = kony.os.toNumber(gblEBMaxLimitAmtHist);
  //al
  var OldL = ProfileCommFormat(gblEBMaxLimitAmtCurrent);
  var newL = ProfileCommFormat(nl);
  //inputParam["activityFlexValues"] = OldL + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + " to " + newL + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  //    inputParam["deviceNickName"] = "testing";
  //    inputParam["logLinkageId"] = "";
  //    inputParam["activityFlexValues1"] = oldLimit;
  //    inputParam["activityFlexValues2"] = newlimit;
  inputParam["cancel"] = "true";
  if (ccode == "MB" || flowSpa) showLoadingScreen();
  else showLoadingScreenPopup();
  invokeServiceSecureAsync("chgTransLimitCompositeService", inputParam, callbackchgTransLimitCancelCompositeService);
}

function callbackchgTransLimitCancelCompositeService(status, resulttable) {
  if (status == 400) {
    if (ccode == "MB" || flowSpa) dismissLoadingScreen();
    else frmIBCMChgMobNoTxnLimit.txtChangeTransactionLimit.text = "";
    dismissLoadingScreen();
    if (resulttable["opstatus_changeTransLimit"] == 0 || resulttable["opstatus"] == 0) {
      if (flowSpa) popOtpSpa.dismiss();
      if (serviceProfileFlag == "IB") {
        completeicon = true;
        frmIBCMMyProfile.show();
      } else {
        if (callLimitedit) {
          callLimitedit = false;
          popUpCallCancel.show();
        }
        popupTractPwd.dismiss();
        popupTractPwd.destroy();
        onClickCancelAccessPin();
        completeicon = true;
        frmMyProfiles.show();
      }
    } else {
      if (resulttable["errMsg"] != undefined) {
        alert(resulttable["errMsg"]);
      } else {
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
      }
    }
  } else if (status == 300) {
    if (ccode == "MB" || flowSpa) dismissLoadingScreen();
    else dismissLoadingScreenPopup();
  }
}


function changeMobileInSessionSPA(newMObNo) {
  showLoadingScreen();
  inputParam = {};
  inputParam["newMobNo"] = newMObNo;

  invokeServiceSecureAsync("SaveChangeLimitParamsInSession", inputParam, changeLimitSaveInSessionCallBackSPA);
}

function changeLimitSaveInSessionCallBackSPA(status, result){
  if (status == 400) {


    if (result["opstatus"] == 0) {
      //onMyProfileMobTxLimitNextIB();
      if(flowSpa)
      {
        maskedNewMob = result["newMobNo"];
        gblSpaChannel = "ChangeMobileNumberNew";
        generateOtpUserSpaNewMob();

      }
      else
      {
        maskedNewMob = result["newMobNo"];
        //generateOTPNewMobNumberService(); 
        //showOTPSSLPopupTransPwd(CompositeChangeMobileMB); // -- Commented by vijay for change the flow from OTP to Transaction pwd
        //generateOTPNewMobNumberService();
        if(gblAuthAccessPin == true){
          showAccesspinPopup();
        }else{
          showOTPSSLPopupTransPwd(CompositeChangeMobileMB);
        }
      }
    }
    else if (result["opstatus"] == 1)
    {
      if(result["errCode"]!= null || result["errCode"]!= "")
      {
        var number1 = kony.i18n.getLocalizedString("keyenternumber");
        showAlert(number1, "Info");
      }
    }
  }
}

function UndocommaFormatted(amount) {

  var ifCommaExists = amount.indexOf(",");
  var ifdotExists = amount.indexOf(".");
  var newvalue, amountFormat;
  amount = amount.trim();
  if (ifCommaExists != -1){
    amount = amount.replace(/,/g, "");
  }
  if (ifCommaExists != -1){
    amount = amount.split(".");
    if(amount[1].length <= 2)   return amount[0]+"."+amount[1];
    else return amount[0]+"."+amount[1].split(2);
  }
  return amount;

} 
function dropDownStatePopulate()
{
  resetState = true;
  gblMyProfileAddressFlag = "state";
  showLoadingScreen();
  var inputParams = {};
  invokeServiceSecureAsync("MyprofileAddressProvinceJavaService", inputParams, dropDownPopulateCommonCallbck); 
}

function dropDowndistrictPopulate() 
{
  resetdistrict = true;
  gblMyProfileAddressFlag = "district";
  showLoadingScreen();
  var inputParams = {};
  inputParams["provinceCD"] = provinceCD;
  invokeServiceSecureAsync("MyprofileAddressDistrictJavaService", inputParams, dropDownPopulateCommonCallbck);
}

function dropDownsubDistrictPopulate()
{
  resetsubdistrict = true;
  gblMyProfileAddressFlag = "subdistrict";
  showLoadingScreen();
  var inputParams = {};
  inputParams["districtCD"] = DistrictCD;
  invokeServiceSecureAsync("MyprofileAddressSubDistrictJavaService", inputParams, dropDownPopulateCommonCallbck);
}

function dropDownzipCodetPopulate()
{
  gblMyProfileAddressFlag = "zipcode";
  showLoadingScreen();
  var inputParams = {};
  inputParams["districtCD"] = DistrictCD;
  invokeServiceSecureAsync("MyprofileAddressZipcodeJavaService", inputParams, dropDownPopulateCommonCallbck);
}

function dropDownPopulateCommonCallbck(status, resulttable) {

  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      // 
      if (gblMyProfileAddressFlag == "state") {
        setDropdowninEng(resulttable["state"]);
      } else if (gblMyProfileAddressFlag == "district") {
        setDropdowninEng(resulttable["district"]);
      } else if (gblMyProfileAddressFlag == "subdistrict") {
        setDropdowninEng(resulttable["subdistrict"]);
      } 
      else if (gblMyProfileAddressFlag == "zipcode") {
        setDropdowninEng(resulttable["zipcode"]);
      }
      else {

      }
    } else {

    }
  } else {
    if (status == 300) {

    }
  }

}

function setDropdowninEng(resultDS) {
  //frmeditMyProfile.hbox4758937266440.setEnabled(true);
  //var resultDS = resulttable["state"];
  var currentLocales = kony.i18n.getCurrentLocale();

  if(gblMyProfileAddressFlag == "state") {
    if(gblStateValue == "")
    {
      dismissLoadingScreen();
      if(gblOpenProdAddress) {
        frmOpenProdEditAddress.show();
      } else {
        frmeditMyProfile.show();
      }
      gblMyProfileAddressFlag = "";
    }
    else
    {
      for (var j = 0; j < resultDS.length; j++)
        if(resultDS[j].ProvinceNameTH == gblStateValue)
        {
          gblStateEng = resultDS[j].ProvinceNameEN;
          provinceCD = resultDS[j].ProvinceCD;
          break;
        }
      dropDowndistrictPopulate();
    }
    return;
  }

  if(gblMyProfileAddressFlag == "district") {
    if(gbldistrictValue!="")
    {
      for (var j = 0; j < resultDS.length; j++)
        if(resultDS[j].DistrictNameTH == gbldistrictValue)
        {
          gblDistEng = resultDS[j].DistrictNameEN;
          DistrictCD = resultDS[j].DistrictCD;
          break;
        }
      dropDownsubDistrictPopulate();
    }
    else
    {
      dismissLoadingScreen();
      if(gblOpenProdAddress) {
        frmOpenProdEditAddress.show();
      } else {
        frmeditMyProfile.show();
      }
      gblMyProfileAddressFlag = "";
    }
    return;
  }

  if(gblMyProfileAddressFlag == "subdistrict") {
    if(gblsubdistrictValue!="")
    {
      for (var j = 0; j < resultDS.length; j++)
        if(resultDS[j].SubDistrictNameTH == gblsubdistrictValue)
        {
          gblSubDistEng = resultDS[j].SubDistrictNameEN;
          break;
        }
      dropDownzipCodetPopulate();
    }
    else
    {
      dismissLoadingScreen();
      if(gblOpenProdAddress) {
        frmOpenProdEditAddress.show();
      } else {
        frmeditMyProfile.show();
      }
      gblMyProfileAddressFlag = "";
    } 
    return;
  }
  if(gblMyProfileAddressFlag == "zipcode") {
    if(gblzipcodeValue!="")
    {
      for (var j = 0; j < resultDS.length; j++)
      {
        if(resultDS[j].ZipCode == gblzipcodeValue)
        {
          gblZipEng = resultDS[j].ZipCode;
          break;
        }
      }
    }
    else
    {
      dismissLoadingScreen();
      if(gblOpenProdAddress) {
        frmOpenProdEditAddress.show();
      } else {
        frmeditMyProfile.show();
      }
      gblMyProfileAddressFlag = "";
    } 

  }
  if(gblMyProfileAddressFlag == "zipcode")
  {
    dismissLoadingScreen();
    if(gblOpenProdAddress) {
      frmOpenProdEditAddress.show();
    } else {
      frmeditMyProfile.show();
    }
    gblMyProfileAddressFlag = "";
  }
  if(currentLocales == "th_TH")
  {
    return;
  }	

}

//Commented below code. Using Kony API to request permissions instead of FFI
/*
function chkRunTimePermissionsForCameraEditPfl(){
      if(isAndroidM()){
            kony.print("Brajesh: CALLIING PERMISSIONS FFI....");
            var permissionsArr=[gblPermissionsJSONObj.CAMERA_GROUP];
            //Creates an object of class 'PermissionFFI'
            var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
            //Invokes method 'checkpermission' on the object
            RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr,null,callBackMarshmallowPermissionCheckCameraEditPfl);
      }else{
      		onClickCamMyProfile();
      }
}
function callBackMarshmallowPermissionCheckCameraEditPfl(result) {
	if(result == "1"){
		kony.print("Brajesh: IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
	      	onClickCamMyProfile();
	}else{
		kony.print("Brajesh: IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
		//var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
		//alert(messageErr);
	}
} */

//  profile pic updation in MB
function checkCameraPermissionsForEditProfile(){
  var options = {};
  var result = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, options);	
  kony.print("checkCameraPermissionsForScan result ###########  "+result.status);
  if (result.status == kony.application.PERMISSION_DENIED){
    kony.print("checkCameraPermissionsForScan Permission denied  ");
    if (result.canRequestPermission){
      kony.application.requestPermission(kony.os.RESOURCE_CAMERA, reqCameraPermissionCallback);
    }else{
      kony.print("checkCameraPermissionsForScan Permission denied. Cannot request  ");
    }
  } else if (result.status == kony.application.PERMISSION_GRANTED) {

    kony.print("checkCameraPermissionsForScan Permission granted  ");
    kony.print("Permission allowed. ");
    onClickCamMyProfile();     
  } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
    kony.print("reqCameraPermissionCallback-PERMISSION_RESTRICTED: ");
  }

  function reqCameraPermissionCallback(response) {
    kony.print("reqCameraPermissionCallback "+result.status);
    if (response.status == kony.application.PERMISSION_GRANTED) {
      kony.print("reqCameraPermissionCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
      kony.print("Permission allowed. ");
      onClickCamMyProfile();       
    } else if (response.status == kony.application.PERMISSION_DENIED) {
      kony.print("reqCameraPermissionCallback-PERMISSION_DENIED: " + JSON.stringify(response));

    }
  }
}
function onClickCamMyProfile(){
  popUploadPic.camera1.text = kony.i18n.getLocalizedString("keyTakeaPhoto");
  popUploadPic.btnPopDeleteCancel.text = kony.i18n.getLocalizedString("keyLibrary");
  popUploadPic.button474076475461493.text = kony.i18n.getLocalizedString("keyDelete");
  popUploadPic.button474076475461503.text = kony.i18n.getLocalizedString("keyCancelButton");
  popUploadPic.lblDeleteConfirmation.text = kony.i18n.getLocalizedString("keychoosefrombelow");
  popUploadPic.show();		
}


function save2sessionaddrDetailsSpa()
{
  var curForm = kony.application.getCurrentForm().id;
  var AddrNew = "";
  if(curForm == "frmeditMyProfile")
  {
    txt1 = frmeditMyProfile.txtAddress1.text;
    txt2 = frmeditMyProfile.txtAddress2.text;
    if (addressedited == true) {
      province = gblStateValue;
      district = gbldistrictValue;
      subdistrict = gblsubdistrictValue;
      zipcode = gblzipcodeValue;
    } else {
      province = StateValue;
      district = districtValue;
      subdistrict = subdistrictValue;
      zipcode = zipcodeValue;
    }
    var inputpar = {};
    if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile"))
      AddrNew =	txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district + " " + province + " " + zipcode;
    else
      AddrNew = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district + " " + province + " " + zipcode;
    inputpar["Addr1"] = txt1 ;
    inputpar["Addr2"] = txt2 ;	
    inputpar["email"] = EmailHide(frmeditMyProfile.txtemailvalue.text);
    inputpar["fbID"] = gblFacebookId ;
    inputpar["AddrNew"] = AddrNew ;
    var profileData = {
      "emailID" : EmailHide(frmeditMyProfile.txtemailvalue.text),
      "FbID" : gblFacebookId,
      "Addr1" : txt1,
      "Addr2" : txt2,
      "AddrNew" :	AddrNew
    }
    inputpar["datajson"] = JSON.stringify(profileData, "", "");
    invokeServiceSecureAsync("SaveChangeLimitParamsInSession", inputpar, saveAddrCallBackSpa);	

  } else if(curForm == "frmOpenProdEditAddress")
  {
    txt1 = frmOpenProdEditAddress.txtAddress1.text;
    txt2 = frmOpenProdEditAddress.txtAddress2.text;

    if(isNotBlank(StateValue)) {
      gblStateValue = StateValue;
    }
    if(isNotBlank(districtValue)) {
      gbldistrictValue = districtValue;
    }
    if(isNotBlank(subdistrictValue)) {
      gblsubdistrictValue = subdistrictValue;
    }
    if(isNotBlank(zipcodeValue)) {
      gblzipcodeValue = zipcodeValue;
    }

    province = gblStateValue;
    district = gbldistrictValue;
    subdistrict = gblsubdistrictValue;
    zipcode = gblzipcodeValue;

    var inputpar = {};
    if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile"))
      AddrNew =	txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district + " " + province + " " + zipcode;
    else
      AddrNew = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district + " " + province + " " + zipcode;
    inputpar["Addr1"] = txt1 ;
    inputpar["Addr2"] = txt2 ;	
    inputpar["AddrNew"] = AddrNew ;
    inputpar["subDistrict"]=subdistrict;
    inputpar["district"]=district;
    inputpar["province"]=province;
    inputpar["zipcode"]=zipcode;
    var profileData = {
      "Addr1" : txt1,
      "Addr2" : txt2,
      "AddrNew" :	AddrNew
    }
    inputpar["datajson"] = JSON.stringify(profileData, "", "");
    invokeServiceSecureAsync("SaveChangeLimitParamsInSession", inputpar, saveOpenProdAddrCallBack);	

  }
}

function saveAddrCallBackSpa(status, result){
  if (status == 400) {


    if (result["opstatus"] == 0) {
      onClickOTPRequestSpa();

    }
  }
}

function syncLocaleforMBProfile()
{
  selectState = frmeditMyProfile.lblProvince.text;
  selectDist = frmeditMyProfile.lbldistrict.text;
  selectSubDist = frmeditMyProfile.lblsubdistrict.text;
  selectZip = frmeditMyProfile.lblzipcode.text;
  mptemp = "locale";
}

function populateAddressLocaleFbConfirmEdit()
{
  var index = 0;
  var currentLocales = kony.i18n.getCurrentLocale();
  var psEn = kony.i18n.getLocalizedString('keyPleaseSelectEng');
  var psTh = kony.i18n.getLocalizedString('keyPleaseSelectTH');
  var key = selectState;
  for (; index < resulttableState.length; index++) {
    if (resulttableState[index].ProvinceNameTH == key || resulttableState[index].ProvinceNameEN == key) {
      if(currentLocales == "en_US")
        frmeditMyProfile.lblProvince.text = resulttableState[index].ProvinceNameEN;
      else if(currentLocales == "th_TH")
        frmeditMyProfile.lblProvince.text = resulttableState[index].ProvinceNameTH;
      break;	
    }
  }   
  if (key == psEn || key == psTh)
  {
    if(currentLocales == "en_US")
      frmeditMyProfile.lblProvince.text = psEn;
    else if(currentLocales == "th_TH")
      frmeditMyProfile.lblProvince.text = psTh;
  }			
  key = selectDist;
  index = 0;
  for (; index < resulttableDist.length; index++) {
    if (resulttableDist[index].DistrictNameTH == key || resulttableDist[index].DistrictNameEN == key) {
      if(currentLocales == "en_US")
        frmeditMyProfile.lbldistrict.text = resulttableDist[index].DistrictNameEN;
      else if(currentLocales == "th_TH")
        frmeditMyProfile.lbldistrict.text = resulttableDist[index].DistrictNameTH;
      break;	
    }
  }
  if (key == psEn || key == psTh) 
  {
    if(currentLocales == "en_US")
      frmeditMyProfile.lbldistrict.text = psEn;
    else if(currentLocales == "th_TH")
      frmeditMyProfile.lbldistrict.text = psTh;
  }		
  key = selectSubDist;
  index = 0;
  for (; index < resulttableSubDist.length; index++) {
    if (resulttableSubDist[index].SubDistrictNameTH == key || resulttableSubDist[index].SubDistrictNameEN == key) {
      if(currentLocales == "en_US")
        frmeditMyProfile.lblsubdistrict.text = resulttableSubDist[index].SubDistrictNameEN;
      else if(currentLocales == "th_TH")
        frmeditMyProfile.lblsubdistrict.text = resulttableSubDist[index].SubDistrictNameTH;
      break;	
    }
  }
  if (key == psEn || key == psTh)
  { 
    if(currentLocales == "en_US")
      frmeditMyProfile.lblsubdistrict.text = psEn;
    else if(currentLocales == "th_TH")
      frmeditMyProfile.lblsubdistrict.text = psTh;
  }		
  key = selectZip;
  index = 0;
  for (; index < resulttableStateZip.length; index++) {
    if (resulttableStateZip[index].ZipCode == key || resulttableStateZip[index].ZipCode == key) {
      if(currentLocales == "en_US")
        frmeditMyProfile.lblzipcode.text = resulttableStateZip[index].ZipCode;
      else if(currentLocales == "th_TH")
        frmeditMyProfile.lblzipcode.text = resulttableStateZip[index].ZipCode;
      break;	
    }
  }    
  if (key == psEn || key == psTh)
  {
    if(currentLocales == "en_US")
      frmeditMyProfile.lblzipcode.text = psEn;
    else if(currentLocales == "th_TH")
      frmeditMyProfile.lblzipcode.text = psTh;
  }			
  if(changeState == true)
    frmeditMyProfile.hbox4758937266356.setEnabled(true); 
  if(changedist == true)
    frmeditMyProfile.hbox4758937266374.setEnabled(true);
  if(changeSubDist == true)
    frmeditMyProfile.hbox4758937266440.setEnabled(true);
}

// Added by Vijay for OTP flow change
function CompositeChangeMobileMB() {
  kony.print("inside CompositeChangeMobile");
  var newNum;
  var text1;
  var textpass = "";
  var inputParam = {};   
  inputParam["notificationAdd_appID"] = appConfig.appId;   
  var chan = gblDeviceInfo.name;
  if (chan == "thinclient") {
    if (!flowSpa) newNum = frmIBCMChgMobNoTxnLimit.txtChangeMobileNumber.text;
    else newNum = frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text;
    ccode = "IB";
  } else {
    if(gblAuthAccessPin == true) 
      text1 = gblNum;
    else
      text1 = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;

    newNum = frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text;
    kony.print("Entering into IB ---->text1" + text1 + "newNum------>" + newNum);
    ccode = "MB";
  }
  inputParam["segmentIdVal"] = "MIB";
  if (flowSpa) {
    if (gblTokenSwitchFlag == true) {
      inputParam["password"] = popOtpSpa.txttokenspa.text;
      popOtpSpa.txttokenspa.text = "";
      inputParam["loginModuleId"] = "IB_HWTKN";
    } else {
      inputParam["password"] = popOtpSpa.txtOTP.text;
      popOtpSpa.txtOTP.text = "";
    }
  }
  if (ccode == "MB") {
    if (text1 == "" || text1 == null) {
      kony.print("inside text is empty");
      popupTractPwd.show();
      setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd")); // -- Need to change this Alert for OTP related
      return false;
    } else {
      kony.print("Coming to else part");
      inputParam["password"] = text1;  //popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
      inputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
    }
  } else if (gblTokenSwitchFlag == true && ccode == "IB" && !flowSpa) {
    textpass = frmIBCMChgMobNoTxnLimit.tbxToken.text;
    frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
    inputParam["loginModuleId"] = "IB_HWTKN";
  } else if (!flowSpa) {
    textpass = frmIBCMChgMobNoTxnLimit.tbxPopupTractPwdtxtTranscPwd.text;
  }
  if(ccode == "IB" && !flowSpa)
  {
    if (textpass == "" || textpass == null) {
      if(gblTokenSwitchFlag == true)
      {
        frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
        alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
      }
      else
      {
        frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
        alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
      }
      return false;
    } else {
      inputParam["password"] = textpass;
    }	
  }
  kony.print("After IF conditions ------>");   
  inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
  tempOldPhoneNo = gblPHONENUMBEROld;
  var oldno = HidePhnNum(gblPHONENUMBEROld);
  var newno = HidePhnNum(gblPhoneNumberReq);
  newNum = newNum.toString().replace(/-/g, "");
  kony.print("retry counts ------>gblRtyCtrVrfyAxPin " + gblRtyCtrVrfyAxPin + "gblRtyCtrVrfyTxPin.....>" + gblRtyCtrVrfyTxPin); 
  inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
  inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
  inputParam["userStoreId"] = "DefaultStore";
  inputParam["gblTokenSwitchFlag"] = gblTokenSwitchFlag ? "true" : "false";
  //custmobileno
  inputParam["MobileNoNew"] = newNum;
  if (flowSpa) {
    inputParam["ProdCode"] = "02";
  }
  inputParam["logLinkageId"] = "";
  //notification
  inputParam["customerName"] = customerName;
  inputParam["flagVerify"] = "OLD";
  inputParam["source"] = "changeMobileNumberSMS";
  inputParam["notificationType"] = "Sms";
  inputParam["notificationSubject"] = "";
  inputParam["notificationContent"] = "";
  inputParam["Locale"] = kony.i18n.getCurrentLocale();
  if (ccode == "MB" || flowSpa) showLoadingScreen();
  else showLoadingScreenPopup();
  kony.print("before calling newChangeMobileComposite inputParam----->" + JSON.stringify(inputParam));
  //alert("newChangeMobileComposite");
  invokeServiceSecureAsync("newChangeMobileComposite", inputParam, callbackCompositeChangeMobileMB);
}


function callbackCompositeChangeMobileMB(status, resulttable) {

  kony.print("inside callbackCompositeChangeMobile" + JSON.stringify(resulttable));
  kony.print("inside resulttable[opstatus]--------->" + resulttable["opstatus"]);

  if (status == 400) {
    if (ccode == "IB" && !flowSpa) {
      frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
      frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
    }
    if (ccode == "MB") {
      popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    }
    if (flowSpa) {
      popOtpSpa.txtOTP.text = "";
      popOtpSpa.txttokenspa.text = "";
    }
    if (ccode == "MB" || flowSpa) dismissLoadingScreen();
    else dismissLoadingScreenPopup();

    if (resulttable["opstatus_verifypwd"] != 0) {
      if (resulttable["opstatus"] == 8005) {
        if (ccode == "MB" || flowSpa) {
          if (flowSpa) {
            popOtpSpa.txtOTP.text = "";
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
              gblVerifyOTPCounter = "0";
              popOtpSpa.lblPopupTract2Spa.text = kony.i18n.getLocalizedString("wrongOTP");
              popOtpSpa.lblPopupTract4Spa.text = "";
              kony.application.dismissLoadingScreen();
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
              gblVerifyOTPCounter = "0";
              otplocked = true;
              kony.application.dismissLoadingScreen();
              popOtpSpa.dismiss();
              popTransferConfirmOTPLock.show();
              // calling crmprofileMod to update the user status
              //updteuserSpa();
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00003") {
              gblVerifyOTPCounter = "0";
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
              kony.application.dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
              return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
              //gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
              gblVerifyOTPCounter = "0";
              alert("" + resulttable["errMsg"]);
              return false;
            }
          } else {
            if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
              showTranPwdLockedPopup();
              popupTractPwd.dismiss();
              return false;
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
              setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
              return false;
            }else if(resulttable["errCode"] == "VrfyAcPWDErr00003"){
              onClickCancelAccessPin();
              gotoUVPINLockedScreenPopUp();
              return false;
            }else if (resulttable["errCode"] == "VrfyAcPWDErr00001" || resulttable["errCode"] == "VrfyAcPWDErr00002") {
              popupEnterAccessPin.lblWrongPin.setVisibility(true);
              kony.print("invalid pin transfer flow"); //To do : set red skin to enter access pin
              resetAccessPinImg(resulttable["badLoginCount"]);
              return false;
            }else {
              dismissLoadingScreen();
              if (resulttable["errMsg"] != undefined) {
                alert("" + resulttable["errMsg"]);
              }
            }
          }
        } else if (!flowSpa) {
          MobileOTPCNT = 1;
          flagMobNum = "old";
          frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
          frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
          if (resulttable["errCode"] == "VrfyOTPErr00001") {
            gblRetryCountRequestOTP = resulttable["retryCounterVerifyOTP"];
            dismissLoadingScreenPopup();
            //  alert("" + kony.i18n.getLocalizedString("invalidOTP"));
            frmIBCMChgMobNoTxnLimit.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone"); //kony.i18n.getLocalizedString("invalidOTP"); //
            frmIBCMChgMobNoTxnLimit.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
            frmIBCMChgMobNoTxnLimit.hbxOTPincurrect.isVisible = true;
            frmIBCMChgMobNoTxnLimit.hbox476047582127699.isVisible = false;
            frmIBCMChgMobNoTxnLimit.hbxOTPsnt.isVisible = false;
            frmIBCMChgMobNoTxnLimit.txtBxOTP.text = "";
            frmIBCMChgMobNoTxnLimit.tbxToken.text = "";
            //frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
            if (gblTokenSwitchFlag == true) frmIBCMChgMobNoTxnLimit.tbxToken.setFocus(true);
            else frmIBCMChgMobNoTxnLimit.txtBxOTP.setFocus(true);
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
            dismissLoadingScreenPopup();
            handleOTPLockedIB(resulttable);
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("invalidOTP"));
            return false;
          } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
            gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
            if (resulttable["errMsg"] != undefined) {
              alert("" + resulttable["errMsg"]);
            }
            return false;
          } else {
            dismissLoadingScreenPopup();
            if (resulttable["errMsg"] != undefined) {
              alert("" + resulttable["errMsg"]);
            }
          }
        }
      } else if (resulttable["code"] != null && resulttable["code"] == "10403") {
        if (ccode == "MB") {
          popupTractPwd.dismiss();
          onClickCancelAccessPin();
          gblVerifyOTPCounter = "0";
          if(gblAuthAccessPin == true) {
            onClickCancelAccessPin();
            gotoUVPINLockedScreenPopUp(); 
          }else{
            showTranPwdLockedPopup();
          }

        } else if (resulttable["code"] != null && resulttable["code"] == "10020") {
          showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
        } else {

          if (resulttable["errMsg"] != undefined) {
            alert("" + resulttable["errMsg"]);
          }
        }
      } else {
        if (resulttable["code"] != null && resulttable["code"] == "10403") {
          if (ccode == "IB") {
            frmIBCMConfirmation.txtOTP.text = "";
            gblVerifyOTPCounter = "0";
            handleOTPLockedIB(resulttable);
            frmIBCMConfirmation.btnReqOTP.skin = btnIBgreyInactive;
            frmIBCMConfirmation.btnReqOTP.setEnabled(false);
          }
        } else if (resulttable["code"] != null && resulttable["code"] == "10020") {
          if (ccode == "IB") {
            frmIBCMConfirmation.txtOTP.text = "";
            showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
          }
        } else {

          if (resulttable["errMsg "] != undefined) {
            alert("" + resulttable["errMsg "]);
          }
          if (ccode == "IB" && !flowSpa) {
            frmIBCMConfirmation.txtOTP.text = "";
          }
        }
      }
    } if (resulttable["opstatus"] == 0) {
      //kony.print("()()()()()()()()()()()()()()()()()()()()()()()()()");
      if (resulttable["isValidPasswordTokenOtp"] != null || resulttable["isValidPasswordTokenOtp"] != "" && resulttable["isValidPasswordTokenOtp"] == true) {
        if (ccode == "MB") {
          popupTractPwd.dismiss();
          onClickCancelAccessPin();
          gblRetryCountRequestOTP = "0";
          kony.print("Transaction pwd verify Completed");
          generateOTPNewMobNumberService();
        } else {
          flagMobNum = "new";
          MobileOTPCNT = 0;
          completeicon = true;
          frmIBCMMyProfile.show();
        }
      }
    } else {
      if (resulttable["errMsg"] != undefined) {
        alert("" + resulttable["errMsg"]);
      } else {
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
      }
    }
  } else if (status == 300) {
    if (ccode == "MB" || flowSpa) {
      dismissLoadingScreen();
    } else {
      dismissLoadingScreenPopup();
    }
  }
}

function frm_editMyProfiles_Init_Action(){
  frmeditMyProfiles.preShow = frmeditMyProfiles_PreShow_Action;
  frmeditMyProfiles.postShow = frmeditMyProfiles_PostShow_Action;
  frmeditMyProfiles.btnBack.onClick = editAddressBackAction;
  frmeditMyProfiles.onDeviceBack = disableBackButton;
  frmeditMyProfiles.btnSave.onClick = disableBackButton;
  //Form Field Actions
  frmeditMyProfiles.flxAddress1.onClick = flxField_onClick_Action;
  frmeditMyProfiles.flxAddress2.onClick = flxField_onClick_Action;
  frmeditMyProfiles.flxSubDistrict.onClick = flxField_onClick_Action;
  frmeditMyProfiles.flxDistrict.onClick = flxField_onClick_Action;
  frmeditMyProfiles.flxProvince.onClick = flxField_onClick_Action;
  frmeditMyProfiles.flxZipcode.onClick = flxField_onClick_Action;
  frmeditMyProfiles.txtAddress1.onDone = textBox_onDone_withNextFocus;
  frmeditMyProfiles.txtAddress1.onTextChange = enableSaveAddressBtn;
  frmeditMyProfiles.txtAddress1.onBeginEditing = textBox_onBeginCheckEmptyText;
  frmeditMyProfiles.txtAddress2.onDone = textBox_onDone_withNextFocus;
  frmeditMyProfiles.txtAddress2.onTextChange = enableSaveAddressBtn;
  frmeditMyProfiles.txtAddress2.onBeginEditing = textBox_onBeginCheckEmptyText;
  frmeditMyProfiles.txtSubDistrict.onDone = textBox_onDone_withNextFocus;
  frmeditMyProfiles.txtSubDistrict.onTextChange = onTextChangeSearchAddress;
  frmeditMyProfiles.txtSubDistrict.onBeginEditing = textBox_onBeginCheckEmptyText;
  frmeditMyProfiles.segSubDistrictSearch.onRowClick = selectFullAddressEditProfile;
}
function editAddressBackAction(){
  if(gblEditAddressFlow == "myProfile"){
    viewprofileServiceCall();
  }else if(gblEditAddressFlow == "openAccount"){
    onClickCheckContactInfo();
  }else if(gblEditAddressFlow == "issueCard"){
    onClickCheckContactInfo();
  }else if(gblEditAddressFlow == "applyLoan"){
    navigateToLoanPersonalInfo();
  }else if(gblEditAddressFlow == "mutualFund"){
    alert("not implement!");
  }
}
function frmeditMyProfiles_PreShow_Action(){
  showLoadingScreen();
  frmeditMyProfiles.lblModifyProfileHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("CTA_txtTitle"));
  frmeditMyProfiles.lblProfileDescription.text = kony.i18n.getLocalizedString("CTA_txtDesc");
  frmeditMyProfiles.lblAddress1.text = kony.i18n.getLocalizedString("CTA_phrAddress1");
  frmeditMyProfiles.lblAddress1Header.text = kony.i18n.getLocalizedString("CTA_phrAddress1");
  frmeditMyProfiles.lblAddress2.text = kony.i18n.getLocalizedString("CTA_phrAddress2");
  frmeditMyProfiles.lblAddress2Header.text = kony.i18n.getLocalizedString("CTA_phrAddress2");
  frmeditMyProfiles.lblDistrict.text = removeColonFromEnd(kony.i18n.getLocalizedString("District_label"));
  frmeditMyProfiles.lblDistrictHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("District_label"));
  frmeditMyProfiles.lblSubDistrict.text = removeColonFromEnd(kony.i18n.getLocalizedString("Sub_District_label"));
  frmeditMyProfiles.lblSubDistrictHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("Sub_District_label"));
  frmeditMyProfiles.lblProvince.text = removeColonFromEnd(kony.i18n.getLocalizedString("Province_label"));
  frmeditMyProfiles.lblProvinceHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("Province_label"));
  frmeditMyProfiles.lblZipCode.text = removeColonFromEnd(kony.i18n.getLocalizedString("Zipcode_label"));
  frmeditMyProfiles.lblZipCodeHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("Zipcode_label"));
  frmeditMyProfiles.btnSave.text = kony.i18n.getLocalizedString("CTA_btnSave");
  frmeditMyProfiles.flxSearchAddress.isVisible = false;
  frmeditMyProfiles.btnSave.onClick = disableBackButton;
  displayTextBoxAddress1(isNotBlank(frmeditMyProfiles.txtAddress1.text));
  displayTextBoxAddress2(isNotBlank(frmeditMyProfiles.txtAddress2.text));
  displayTextBoxSubDistrict(isNotBlank(frmeditMyProfiles.txtSubDistrict.text));
  displayTextBoxDistrict(isNotBlank(frmeditMyProfiles.lblDistrictVal.text));
  displayTextBoxProvince(isNotBlank(frmeditMyProfiles.lblProvinceVal.text));
  displayTextBoxZipCode(isNotBlank(frmeditMyProfiles.lblZipCodeVal.text));
  enableSaveAddressBtn();
  fillPreDataResponse();
}

function frmeditMyProfiles_PostShow_Action(){
  assignGlobalForMenuPostshow();
  addAccessPinKeypad(frmeditMyProfiles);
  dismissLoadingScreen();
}
function textBox_onDone_withNextFocus(obj){
  if(obj.id == "txtAddress1"){
    flxField_onClick_Action(frmeditMyProfiles.flxAddress2);
  }else if(obj.id == "txtAddress2"){
    flxField_onClick_Action(frmeditMyProfiles.flxSubDistrict);
  }else if(obj.id == "txtSubDistrict"){
    frmeditMyProfiles.flxFooter.setFocus(true);
  }
}
function textBox_onBeginCheckEmptyText(obj){
  if(obj.id == "txtAddress1"){
    setVisibilityAllAddressFieldBox("flxAddress1");
  }else if(obj.id == "txtAddress2"){
    setVisibilityAllAddressFieldBox("flxAddress2");
  }else if(obj.id == "txtSubDistrict"){
    setVisibilityAllAddressFieldBox("flxSubDistrict");
  }
}
function textBox_onDone_withOutFocus(obj){
  kony.print("OnDone Action !");
}
function loadSearchAddress(editAddressFlow){
  gblEditAddressFlow = editAddressFlow;
  frmeditMyProfiles.txtAddress1.text = gblAddress1Value;
  frmeditMyProfiles.txtAddress2.text = gblAddress2Value;
  frmeditMyProfiles.txtSubDistrict.text = gblsubdistrictValue;
  frmeditMyProfiles.lblDistrictVal.text = gbldistrictValue;
  frmeditMyProfiles.lblProvinceVal.text = gblStateValue;
  frmeditMyProfiles.lblZipCodeVal.text = gblzipcodeValue;
  StateValue = gblStateValue;
  districtValue = gbldistrictValue;
  subdistrictValue = gblsubdistrictValue;
  zipcodeValue = gblzipcodeValue;
  showLoadingScreen();
  inputParam["categoryCode"]="GET_ALL_LANG_ADDRESS_FROM_DB";
  if(gblLoanFullAddressSearchForEditAddress.length > 0){
    frmeditMyProfiles.show();
  }else{
    invokeServiceSecureAsync("getPersonalInfoFields", inputParam, callBackSearchAddress);
  }
}
function callBackSearchAddress(status, result){
  if (status == 400) {
    try{
      if (result.opstatus == "0") {
        var fullAddress = result["addressDetails"];
        fullAddress.sort(dynamicSortLoan("subDistProvince"));
        gblLoanFullAddressSearchForEditAddress = fullAddress;
        frmeditMyProfiles.show();
      }else {
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
      alert("Exception in callBackSearchAddress, e : "+e);
    }
  }
}
function fillPreDataResponse(){
  var locale = kony.i18n.getCurrentLocale();
  var searchSuggestedSubDistrictList;
  var searchText1 = frmeditMyProfiles.txtSubDistrict.text;
  var searchText2 = frmeditMyProfiles.lblDistrictVal.text;
  var searchText3 = frmeditMyProfiles.lblProvinceVal.text;
  var searchText4 = frmeditMyProfiles.lblZipCodeVal.text;
  for(var countVal=0; countVal<gblLoanFullAddressSearchForEditAddress.length; countVal++){
    keyToSearch = gblLoanFullAddressSearchForEditAddress[countVal].subDistProvince;
    keyToSearch = keyToSearch.toLowerCase();
    if(keyToSearch.indexOf(searchText1) >=0 && keyToSearch.indexOf(searchText2) >=0 && keyToSearch.indexOf(searchText3) >=0 && keyToSearch.indexOf(searchText4) >=0 ){
      searchSuggestedSubDistrictList = gblLoanFullAddressSearchForEditAddress[countVal];
      break;
    }                
  }
  var filtervalue = searchSuggestedSubDistrictList.subDistProvince.split(" >< ");
  var lblFullAddresstext;
  if (locale == "en_US") {
    gblAddrSearchLang = "EN";
    lblFullAddresstext = filtervalue[1];
  }else{
    gblAddrSearchLang = "TH";
    lblFullAddresstext = filtervalue[0];
  }
  var addressSplit = replaceAll(lblFullAddresstext,">>" , "~");
  frmeditMyProfiles.txtSubDistrict.text = addressSplit.split("~")[0] !== undefined ? addressSplit.split("~")[0].trim() : "";
  frmeditMyProfiles.lblDistrictVal.text = addressSplit.split("~")[1] !== undefined ? addressSplit.split("~")[1].trim() : "";
  frmeditMyProfiles.lblProvinceVal.text = addressSplit.split("~")[2] !== undefined ? addressSplit.split("~")[2].trim() : "";
  frmeditMyProfiles.lblZipCodeVal.text = addressSplit.split("~")[3] !== undefined ? addressSplit.split("~")[3].trim() : "";
  fillSavingValue(filtervalue[0]);
}
function onTextChangeSearchAddress(obj){
  var searchText = obj.text;
  frmeditMyProfiles.lblDistrictVal.text = "";
  frmeditMyProfiles.lblProvinceVal.text = "";
  frmeditMyProfiles.lblZipCodeVal.text = "";
  StateValue = "";
  districtValue = "";
  subdistrictValue = "";
  zipcodeValue = "";
  searchsubdistrictObj(searchText,true);
}
function searchsubdistrictObj(inputTxt,showAddress){
  var searchSubDistrictList = [];
  var searchSuggestedSubDistrictList = [];
  var searchText = inputTxt;
  var searchValue = "";
  gblAddrSearchLang = "";
  var currForm = kony.application.getCurrentForm();
  var locale = kony.i18n.getCurrentLocale();
  enableSaveAddressBtn();
  if (isNotBlank(searchText) && searchText.length >= 1) {
    searchText = searchText.toLowerCase();
    var countMatchedValue = 0;
    if(showAddress){
      currForm.flxSearchAddress.isVisible = true;
      currForm.flxSearchAddress.setFocus = true;
      currForm.flxMain.scrollToWidget(currForm.flxSearchAddress);
    }
    var searchEN = !isNullorEmpty(searchText.match(/[a-zA-Z\s]/g));
    var onlyEnSearch = false; 
    var searchTH = isNullorEmpty(searchText.match(/[a-zA-Z0-9\s]/g));
    var onlyThSearch = false;
    var searchNum = !isNullorEmpty(searchText.match(/[0-9\s]/g));
    var onlyNumSearch = false;
    var displayEN = false;
    var displayTH = false;
    if(searchEN){
      onlyEnSearch = (searchText.match(/[a-zA-Z\s]/g).length == searchText.length);
    }
    if(searchTH){
      onlyThSearch = true;
    }
    if(searchNum){
      onlyNumSearch = (searchText.match(/[0-9\s]/g).length == searchText.length);
    }
    if( onlyEnSearch || onlyThSearch || onlyNumSearch){
      if(onlyThSearch){
        displayTH = true;
        gblAddrSearchLang = "TH";
      }else if(onlyEnSearch){
        displayEN = true;
        gblAddrSearchLang = "EN";
      }else if(onlyNumSearch){
        if (locale == "en_US") {
          displayEN = true;
          gblAddrSearchLang = "EN";
        }else{
          displayTH = true;
          gblAddrSearchLang = "TH";
        }
      }
      kony.print("gblAddrSearchLang =" + gblAddrSearchLang);
      for(var countVal=0; countVal<gblLoanFullAddressSearchForEditAddress.length; countVal++){
        keyToSearch = gblLoanFullAddressSearchForEditAddress[countVal].subDistProvince;
        keyToSearch = keyToSearch.toLowerCase();
        if(keyToSearch.indexOf(searchText) >=0 ){
          searchSuggestedSubDistrictList[countMatchedValue] = gblLoanFullAddressSearchForEditAddress[countVal];
          countMatchedValue = countMatchedValue+1;
        }                
      }
      var dataset={};
      for(var count = 0; count<searchSuggestedSubDistrictList.length; count++){
        var filtervalue = searchSuggestedSubDistrictList[count].subDistProvince.split(" >< ");
        dataset={
          "lblTotalAddressEN": {"isVisible": displayEN,"text":filtervalue[1]},
          "lblTotalAddressTH": {"isVisible": displayTH,"text":filtervalue[0]},
        };
        searchSubDistrictList.push(dataset);
      }
    }
    currForm.segSubDistrictSearch.widgetDataMap = {lblTotalAddressEN: "lblTotalAddressEN", lblTotalAddressTH: "lblTotalAddressTH"};
    currForm.segSubDistrictSearch.setData(searchSubDistrictList);
  }else{
    if(showAddress){
      currForm.flxSearchAddress.isVisible = false;
    }
  }
  if(searchSuggestedSubDistrictList.length == 0){
    currForm.segSubDistrictSearch.setVisibility(false);
    currForm.lblAddressNotFound.setVisibility(true);
  }else{
    currForm.segSubDistrictSearch.setVisibility(true);
    currForm.lblAddressNotFound.setVisibility(false);
  }
}

function selectFullAddressEditProfile(obj){
  var lblFullAddresstext;
  kony.print("gblAddrSearchLang =" + gblAddrSearchLang);
  if(gblAddrSearchLang == "TH"){
    lblFullAddresstext = obj.selectedRowItems[0].lblTotalAddressTH.text;
  }else{
    lblFullAddresstext = obj.selectedRowItems[0].lblTotalAddressEN.text;
  }
  var currForm = kony.application.getCurrentForm();
  var addressSplit = replaceAll(lblFullAddresstext,">>" , "~");
  currForm.txtSubDistrict.text = addressSplit.split("~")[0] !== undefined ? addressSplit.split("~")[0].trim() : "";
  currForm.lblDistrictVal.text = addressSplit.split("~")[1] !== undefined ? addressSplit.split("~")[1].trim() : "";
  currForm.lblProvinceVal.text = addressSplit.split("~")[2] !== undefined ? addressSplit.split("~")[2].trim() : "";
  currForm.lblZipCodeVal.text = addressSplit.split("~")[3] !== undefined ? addressSplit.split("~")[3].trim() : "";
  fillSavingValue(obj.selectedRowItems[0].lblTotalAddressTH.text);
  displayTextBoxSubDistrict(true);
  displayTextBoxDistrict(true);
  displayTextBoxProvince(true);
  displayTextBoxZipCode(true);
  //Load All existing Value Loaded
  currForm.flxSearchAddress.isVisible = false;
  enableSaveAddressBtn();
  currForm.flxFooter.setFocus(true);
}
function fillSavingValue(lblFullAddresstext){
  var addressSplit = replaceAll(lblFullAddresstext,">>" , "~");
  subdistrictValue = addressSplit.split("~")[0] !== undefined ? addressSplit.split("~")[0].trim() : "";
  districtValue = addressSplit.split("~")[1] !== undefined ? addressSplit.split("~")[1].trim() : "";
  StateValue = addressSplit.split("~")[2] !== undefined ? addressSplit.split("~")[2].trim() : "";
  zipcodeValue = addressSplit.split("~")[3] !== undefined ? addressSplit.split("~")[3].trim() : "";
}
function displayTextBoxAddress1(display){
  frmeditMyProfiles.subflxAddress1.setVisibility(!display);
  frmeditMyProfiles.lblAddress1Header.setVisibility(display);
  frmeditMyProfiles.txtAddress1.setVisibility(display);
}
function displayTextBoxAddress2(display){
  frmeditMyProfiles.subflxAddress2.setVisibility(!display);
  frmeditMyProfiles.lblAddress2Header.setVisibility(display);
  frmeditMyProfiles.txtAddress2.setVisibility(display);
}
function displayTextBoxSubDistrict(display){
  frmeditMyProfiles.subflxSubDistrict.setVisibility(!display);
  frmeditMyProfiles.lblSubDistrictHeader.setVisibility(display);
  frmeditMyProfiles.txtSubDistrict.setVisibility(display);
}
function displayTextBoxDistrict(display){
  frmeditMyProfiles.subflxDistrict.setVisibility(!display);
  frmeditMyProfiles.lblDistrictHeader.setVisibility(display);
  frmeditMyProfiles.lblDistrictVal.setVisibility(display);
}
function displayTextBoxProvince(display){
  frmeditMyProfiles.subflxProvince.setVisibility(!display);
  frmeditMyProfiles.lblProvinceHeader.setVisibility(display);
  frmeditMyProfiles.lblProvinceVal.setVisibility(display);
}
function displayTextBoxZipCode(display){
  frmeditMyProfiles.subflxZipCode.setVisibility(!display);
  frmeditMyProfiles.lblZipCodeHeader.setVisibility(display);
  frmeditMyProfiles.lblZipCodeVal.setVisibility(display); 
}
function flxField_onClick_Action(obj){
  setVisibilityAllAddressFieldBox(obj.id);
  var fieldClick = obj.id;
  if(fieldClick == "flxAddress1"){
    displayTextBoxAddress1(true);
    frmeditMyProfiles.txtAddress1.setFocus(true);
  }else if(fieldClick == "flxAddress2"){
    displayTextBoxAddress2(true);
    frmeditMyProfiles.txtAddress2.setFocus(true);
  }else if(fieldClick == "flxSubDistrict"){
    displayTextBoxSubDistrict(true);
    frmeditMyProfiles.txtSubDistrict.setFocus(true);
  }
}
function setVisibilityAllAddressFieldBox(fieldClick){
  var currForm = frmeditMyProfiles;
  if(fieldClick != "flxAddress1" && !isNotBlank(currForm.txtAddress1.text)){
    displayTextBoxAddress1(false);
  }
  if(fieldClick != "flxAddress2" && !isNotBlank(currForm.txtAddress2.text)){
    displayTextBoxAddress2(false);
  }
  if(fieldClick != "flxSubDistrict" && !isNotBlank(currForm.txtSubDistrict.text)){
    displayTextBoxSubDistrict(false);
  }
  if(fieldClick != "flxDistrict" && !isNotBlank(currForm.lblDistrictVal.text)){
    displayTextBoxDistrict(false);
  }
  if(fieldClick != "flxProvince" && !isNotBlank(currForm.lblProvinceVal.text)){
    displayTextBoxProvince(false);
  }
  if(fieldClick != "flxZipcode" && !isNotBlank(currForm.lblZipCodeVal.text)){
    displayTextBoxZipCode(false);
  }
}

function enableSaveAddressBtn(){
  if(validateSaveProfile()){
    frmeditMyProfiles.btnSave.onClick = saveAddressInformation;
    frmeditMyProfiles.btnSave.skin = "btnBlueBGNoRound";
    frmeditMyProfiles.btnSave.focusSkin = "blueLightBlueDisbaled";
  }else{
    frmeditMyProfiles.btnSave.onClick = disableBackButton;
    frmeditMyProfiles.btnSave.skin = "blueLightBlueDisbaled";
    frmeditMyProfiles.btnSave.focusSkin = "blueLightBlueDisbaled";
  }
}
function validateSaveProfile(){
  return isNotBlank(frmeditMyProfiles.txtAddress1.text) && 
    isNotBlank(frmeditMyProfiles.txtAddress2.text) &&
    isNotBlank(frmeditMyProfiles.txtSubDistrict.text) &&
    isNotBlank(frmeditMyProfiles.lblDistrictVal.text) &&
    isNotBlank(frmeditMyProfiles.lblProvinceVal.text) &&
    isNotBlank(frmeditMyProfiles.lblZipCodeVal.text);
}
function saveAddressInformation(){
  profileedit = true;
  profileEmailFlag = "";
  addressedited = true;
  profileAddrFlag = "addr";
  caseTrans = "profile";
  profilePicFlag = "";
  profileDeviceNameFlag = "";
  var nodetails = kony.i18n.getLocalizedString("keyenterDetails");
  if(gblAuthAccessPin == true){
    showAccesspinPopup();
  }else{
    showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", verifyPWDMyAddressMB, 3);  
  }
} 
function isLetter(str) {
  return str.length >= 1 && str.match(/[a-zA-Z0-9]/i).length > 0;
}
function verifyPWDMyAddressMB() {
  var trap;
  var password = null;

  if(gblAuthAccessPin == true)
    password = gblNum;
  else
    password = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;

  if (password == "" || password == null) {
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
    return;
  }

  if(kony.application.getCurrentForm().id == "frmeditMyProfiles") {
    trap = true;
  } else {
    trap = trassactionPwdValidatn(password);
  }

  showLoadingScreen();
  if (trap == false) {
    dismissLoadingScreen();
    popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
    popupTractPwd.lblPopupTract7.setVisibility(true);
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
    popupTractPwd.show();
    if(popupTractPwd.hbxOTP.isVisible){
      popupTractPwd.txtOTP.setFocus(true)
    }else if(popupTractPwd.hbxPopupTranscPwd.isVisible){
      popupTractPwd.tbxPopupTractPwdtxtTranscPwd.setFocus(true)
    }else if(popupTractPwd.hbxPoupAccesspin.isVisible){
      popupTractPwd.tbxPopupTractPwdtxtAccPin.setFocus(true)
    }
  } else {
    if(gblEditAddressFlow == "myProfile"){
      doEditAddressForMyProfile(password);
    }else if(gblEditAddressFlow == "openAccount"){
      doEditAddressForOpenAccount(password);
    }else if(gblEditAddressFlow == "issueCard"){
      doEditAddressForOpenAccount(password);
    }else if(gblEditAddressFlow == "applyLoan"){
      doEditAddressForMyProfile(password);
    }else if(gblEditAddressFlow == "mutualFund"){
      alert("not implement!");
    }
  }
}

function doEditAddressForMyProfile(password){
  var verifyPWDMyProfileMB_InputParam = {}
  // sending the global params 
  txt1 = frmeditMyProfiles.txtAddress1.text;
  txt2 = frmeditMyProfiles.txtAddress2.text;
  province = StateValue;
  district = districtValue;
  subdistrict = subdistrictValue;
  zipcode = zipcodeValue;
  //sending addresschnage
  verifyPWDMyProfileMB_InputParam["globalvar_gblDeviceNickName"] = gblDeviceNickName;
  verifyPWDMyProfileMB_InputParam["globalvar_gblDeviceNickNameNew"] = "";
  verifyPWDMyProfileMB_InputParam["globalvar_GBLemailALOld"] = EmailHide(frmMyProfiles.lblEmailVal.text);
  verifyPWDMyProfileMB_InputParam["globalvar_GBLemailAL"] = ""; //if editited
  //email un masking for email notification
  verifyPWDMyProfileMB_InputParam["globalvar_UnMaskGBLemailALOld"] = frmMyProfiles.lblEmailVal.text;
  verifyPWDMyProfileMB_InputParam["globalvar_UnMaskGBLemailAL"] = ""; //if editited

  verifyPWDMyProfileMB_InputParam["globalvar_fbIdOld"] = gblFacebookIdOld;
  verifyPWDMyProfileMB_InputParam["globalvar_gblFacebookId"] = gblFacebookId; //new email
  var locale = kony.i18n.getCurrentLocale();
  if (locale == "en_US") {
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Language"] = "EN";
  } else {
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Language"] = "TH";
  }
  if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile"))
  {
    verifyPWDMyProfileMB_InputParam["globalvar_gblAddress"] = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district + " " + province + " " + zipcode; //editited
  }
  else{
    verifyPWDMyProfileMB_InputParam["globalvar_gblAddress"] = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district + " " + province + " " + zipcode; //editited
  } 
  verifyPWDMyProfileMB_InputParam["globalvar_gblAddressOld"] = frmMyProfiles.lblContactVal.text;
  verifyPWDMyProfileMB_InputParam["globalvar_gblTokenSwitchFlag"] = gblTokenSwitchFlag;
  verifyPWDMyProfileMB_InputParam["globalvar_base64ImageString"] = gblEditBase64List;
  verifyPWDMyProfileMB_InputParam["globalvar_fileType"] = gblEditPhotoSource;
  verifyPWDMyProfileMB_InputParam["globalvar_profilePicFlagIB"] = profilePicFlagSPA;
  verifyPWDMyProfileMB_InputParam["globalvar_flow"] = gblEditAddressFlow;
  //ending the global params
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_loginModuleId"] = "MB_TxPwd";
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_userStoreId"] = "DefaultStore";
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_password"] = password;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_caseTrans"] = caseTrans;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileEmailFlag"] = profileEmailFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileAddrFlag"] = profileAddrFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileDeviceNameFlag"] = profileDeviceNameFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profilePicFlag"] = profilePicFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileedit"] = profileedit;
  
  verifyPWDMyProfileMB_InputParam["channelId"] = "MB";
  //address flag
  var primaryaddrtype = "Primary";
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr1"] = txt1;
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr2"] = txt2;
  //verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = "testing";
  if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district;
  } else {
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district;
  }
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_City"] = province; //kony.i18n.getLocalizedString("BangkokThaiValue");
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_StateProv"] = gblnewStateValue;
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_PostalCode"] = zipcode;
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_AddrType"] = primaryaddrtype;
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_CountryCodeValue"] = kony.i18n.getLocalizedString("Thailand");
  //params for device update
  var deviceNickname = "";
  verifyPWDMyProfileMB_InputParam["deviceNameUpdate_deviceId"] = GBL_UNIQ_ID;
  verifyPWDMyProfileMB_InputParam["deviceNameUpdate_deviceNickName"] = deviceNickname;
  verifyPWDMyProfileMB_InputParam["crmProfileMod_emailAddr"] = "";
  popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
  invokeServiceSecureAsync("MyProfileModifyCompositeJavaService", verifyPWDMyProfileMB_InputParam, MyProfileModifyOperationcallBack)
}
function doEditAddressForOpenAccount(password){
  var verifyPWDMyProfileMB_InputParam = {};
  profilePicFlag = "";
  // sending the global params 
  txt1 = frmeditMyProfiles.txtAddress1.text;
  txt2 = frmeditMyProfiles.txtAddress2.text;
  province = StateValue;
  district = districtValue;
  subdistrict = subdistrictValue;
  zipcode = zipcodeValue;

  if (gblDeviceNickName == null || gblDeviceNickName == "") {
    gblDeviceNickName = "My Device";
  } 
  verifyPWDMyProfileMB_InputParam["globalvar_gblDeviceNickName"] = gblDeviceNickName;
  verifyPWDMyProfileMB_InputParam["globalvar_GBLemailALOld"] = EmailHide(gblEmailAddr);
  verifyPWDMyProfileMB_InputParam["globalvar_GBLemailAL"] = ""; //if editited
  //email un masking for email notification
  verifyPWDMyProfileMB_InputParam["globalvar_UnMaskGBLemailALOld"] = gblEmailAddr;
  verifyPWDMyProfileMB_InputParam["globalvar_UnMaskGBLemailAL"] = ""; //if editited
  verifyPWDMyProfileMB_InputParam["globalvar_flow"] = gblEditAddressFlow;

  if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) verifyPWDMyProfileMB_InputParam["globalvar_gblAddress"] = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district + " " + province + " " + zipcode; //editited
  else verifyPWDMyProfileMB_InputParam["globalvar_gblAddress"] = txt1 + " " + txt2 + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district + " " + province + " " + zipcode; //editited
  if(gblCCDBCardFlow == "DEBIT_CARD_REISSUE"){
    verifyPWDMyProfileMB_InputParam["globalvar_gblAddressOld"] = "";
  }else{
    verifyPWDMyProfileMB_InputParam["globalvar_gblAddressOld"] = frmCheckContactInfo.lblContactAddr1.text;
  }
  verifyPWDMyProfileMB_InputParam["globalvar_gblTokenSwitchFlag"] = gblTokenSwitchFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_loginModuleId"] = "MB_TxPwd";
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_userStoreId"] = "DefaultStore";
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_password"] = password;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_caseTrans"] = caseTrans;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileEmailFlag"] = profileEmailFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileAddrFlag"] = profileAddrFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileDeviceNameFlag"] = profileDeviceNameFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profilePicFlag"] = profilePicFlag;
  verifyPWDMyProfileMB_InputParam["verifyPswdMyProfile_profileedit"] = profileedit;
  //address flag
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr1"] = txt1;
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr2"] = txt2;
  //verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = "testing";
  if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district;
  } else {
    verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district;
  }
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_City"] = province; //kony.i18n.getLocalizedString("BangkokThaiValue");
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_StateProv"] = gblnewStateValue;
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_PostalCode"] = zipcode;
  verifyPWDMyProfileMB_InputParam["partyUpdateInputMap_CountryCodeValue"] = kony.i18n.getLocalizedString("Thailand");
  //params for device update
  //var deviceNickname = frmeditMyProfile.txtdevicenamevalue.text;
  verifyPWDMyProfileMB_InputParam["deviceNameUpdate_deviceId"] = GBL_UNIQ_ID;
  verifyPWDMyProfileMB_InputParam["deviceNameUpdate_deviceNickName"] = gblDeviceNickName;
  //editemail
  verifyPWDMyProfileMB_InputParam["crmProfileMod_emailAddr"] = gblEmailAddr;
  verifyPWDMyProfileMB_InputParam["password"] = password; //popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
  verifyPWDMyProfileMB_InputParam["notificationAdd_appID"] = appConfig.appId;
  verifyPWDMyProfileMB_InputParam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
  verifyPWDMyProfileMB_InputParam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
  verifyPWDMyProfileMB_InputParam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
  kony.print("verifyPWDMyProfileMB_InputParam before invoking OpenAcctContactDetailsCompositeJavaService -------->" + JSON.stringify(verifyPWDMyProfileMB_InputParam));
  invokeServiceSecureAsync("openAcctContactDetailsCompositeJavaService", verifyPWDMyProfileMB_InputParam, editContactsOpenAcOperationcallBackMB)
}
function frmMyProfiles_init_action(){
  frmMyProfiles.preShow = frmMyProfiles_preshow_action;
  frmMyProfiles.flxContactAddress.onClick = doEditAddress;
  frmMyProfiles.flxMobile.onClick = changeMobileNum;
  frmMyProfiles.flxEmail.onClick = changeEmailAddressAction;
  frmMyProfiles.flxAccessPin.onClick = doEditAccessPIN;
  frmMyProfiles.flxTransactionLimit.onClick = doEditTransactionLimit;
  frmMyProfiles.onDeviceBack = disableBackButton;
  frmMyProfiles.btnMenu.onClick = handleMenuBtn;
  frmMyProfiles.flxProilePic.onClick = chooseChangeProfileImageAction;
  frmMyProfiles.flxChangeImage.onClick = chooseChangeProfileImageAction;
  frmMyProfiles.flxProtectBackGround.onClick = dismissChangeImageMenu;
  frmMyProfiles.flxImport.onClick = openImageGallery;
  frmMyProfiles.flxUseDefault.onClick = useDefaultImage;
  frmMyProfiles.btnCancel.onClick = dismissChangeImageMenu;
  frmMyProfiles.btnCamera.onCapture = cameraPermissionCheck;
}
function frmMyProfiles_preshow_action(){
  confirmEdit = false;
  isMenuShown = false;
  imageDeleted = false;
  gblMyProfilepic = "";
  var randomnum = Math.floor((Math.random() * 10000) + 1);
  frmMyProfiles.flxProtectBackGround.setVisibility(false);
  gblMyProfilepic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=&modIdentifier=MyProfile&dummy=" + randomnum;
  if (gblDeviceNickName == null || gblDeviceNickName == "") {
    frmMyProfiles.lblMyDeviceVal.text = "My Device";
  } else {
    frmMyProfiles.lblMyDeviceVal.text = gblDeviceNickName;
  }
  if (!isNotBlank(kony.store.getItem("cachedProfilePic"))) {
    frmMyProfiles.imgProfile.src = "avatar.png";
  } else {
    frmMyProfiles.imgProfile.base64 = kony.store.getItem("cachedProfilePic");
  }

  if (kony.i18n.getCurrentLocale() == "th_TH") {
    frmMyProfiles.lblProfileDescription.text = gblCustomerNameTh;
  } else {
    frmMyProfiles.lblProfileDescription.text = gblCustomerName;
  }
  frmMyProfiles.lblMobileVal.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
  frmMyProfiles.lblMyProfileHeader.text = kony.i18n.getLocalizedString("keyMyProfile");
  frmMyProfiles.lblEmailHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyEmail"));
  frmMyProfiles.lblEmailVal.text = gblEmailAddr;
  frmMyProfiles.lblMobileHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyMyMobileNumber"));
  frmMyProfiles.lblContactAddressTxt.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyContactAddress"));
  frmMyProfiles.lblRegisterAddress.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyRegAddress"));
  frmMyProfiles.lblAccessPinHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyChangeAccessPin"));
  frmMyProfiles.lblTransactionLimitHeader.text = removeColonFromEnd(kony.i18n.getLocalizedString("keyMyDailyTransLimit"));
  frmMyProfiles.lblTransactionLimitDetail.text = kony.i18n.getLocalizedString("keyDesDailyLimit");
  frmMyProfiles.lblContactVal.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
  frmMyProfiles.lblRegisterAddressVal.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue + " " + gblregdistrictValue + " " + gblregStateValue + " " + gblregzipcodeValue + " " + gblregcountryCode;
  frmMyProfiles.lblfbidstudio1.text = gblFacebookId;//Modified by Studio Viz
  frmMyProfiles.lblTakeNew.text = kony.i18n.getLocalizedString("btnTakePhoto");
  frmMyProfiles.lblImport.text = kony.i18n.getLocalizedString("btnLibrary");
  frmMyProfiles.lblUseDefault.text = kony.i18n.getLocalizedString("btnDeleteProfileImg");
  frmMyProfiles.lblCancel.text = kony.i18n.getLocalizedString("btnPhotoCancel");
  var comcurr = ProfileCommFormat(gblEBMaxLimitAmtCurrent);
  frmMyProfiles.lblDailytransLimitvalue.text = comcurr + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  var commaamt = ProfileCommFormat(gblEBMaxLimitAmtHist);
  frmChangeMobNoTransLimitMB.lblMaxLimitDisplay.text = getDailyChangeLimtNote(ProfileCommFormat(GLOBAL_MAX_LIMIT_HIST), ProfileCommFormat(GLOBAL_CEILING_LIMIT));
}
function changeEmailAddressAction(){
  emailVerificationMB("myProfile"); // This is a common function from R82 for email verification
}
function doEditAddress(){
  editbuttonflag="profile";
  gblLoanNav = "";
  getIBMBEditProfileStatus();
}
function doEditAccessPIN(){
  TMBUtil.DestroyForm(frmCMChgAccessPin);
  gblChangePWDFlag = 0;
  editbuttonflag = "pin";
  getIBMBEditProfileStatus.call(this);
}
function doEditTransactionLimit(){
  TMBUtil.DestroyForm(frmChangeMobNoTransLimitMB);
  gblMobNoTransLimitFlag = false;
  editbuttonflag = "limit";
  getIBMBEditProfileStatus.call(this);
}
function chooseChangeProfileImageAction(){
  frmMyProfiles.flxProtectBackGround.setVisibility(true);
}
function dismissChangeImageMenu(){
  try{
    kony.print("@@@ In dismissChangeImageMenu() @@@");
    var currentForm = kony.application.getCurrentForm();
    kony.print("@@@CurrentForm::"+currentForm.id);
    if(isNotBlank(currentForm.id) && currentForm.id == "frmAccountSummary"){
      frmAccountSummary.flxTakeProfilePic.setVisibility(false);
    }else{
  frmMyProfiles.flxProtectBackGround.setVisibility(false);
}
  }catch(e){
    kony.print("@@@ In dismissChangeImageMenu() Exception::"+e);
  }
}
function openImageGallery(){
  dismissChangeImageMenu();
  getProfileImageFromSource();
}
function useDefaultImage(){
  dismissChangeImageMenu();
  deleteProfPic();
}
function cameraPermissionCheck(){
  dismissChangeImageMenu();
  getPermissionCheck();
}
function saveImageProfileService(MyProfileModifyOperationcallBack){
  var inputParam = {}
  showLoadingScreen();
  inputParam["globalvar_base64ImageString"] = gblEditBase64List;
  inputParam["globalvar_fileType"] = gblEditPhotoSource;
  inputParam["verifyPswdMyProfile_profilePicFlag"] = "picture";
  kony.store.removeItem("cachedProfilePic");
  kony.store.setItem("cachedProfilePic",gblEditBase64List);
  invokeServiceSecureAsync("myProfileChangeImage", inputParam, MyProfileModifyOperationcallBack);
}
function opencameraForChangeProfile(){
  try{
    kony.print("@@@ In opencameraForChangeProfile() @@@");
  gblEditPhotoSource = "png";
    var currentFrm = kony.application.getCurrentForm();
    var rawBytesString = currentFrm["btnCamera"].rawBytes;
  if (rawBytesString == null) {
    alert(kony.i18n.getLocalizedString("KeyImageNotSelected"));
    return;
  }
  var capturedImg = kony.image.createImage(rawBytesString);
    var highSize = capturedImg.getImageWidth()> capturedImg.getImageHeight() ? capturedImg.getImageWidth() : capturedImg.getImageHeight();
  if(highSize >=1024){ //Maximum should not greater than 2048px
    var scaleRatio = highSize/1024;
    scaleRatio = 1/scaleRatio;
    capturedImg.scale(scaleRatio);
  }
  gblEditBase64List = "";
  gblEditBase64List = kony.convertToBase64(capturedImg.getImageAsRawBytes());
  var curSize = (gblEditBase64List.length - 814)/1.37;		
  if (curSize > Gbl_Image_Size_Limit) {
    alert("" + kony.i18n.getLocalizedString("keyImageSizeTooLarge"));
    return;
  }
  gblEditBase64List = gblEditBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
    kony.print("@@@CurrentForm:::"+currentFrm.id);
    if(isNotBlank(currentFrm.id) && currentFrm.id == "frmAccountSummary"){
      saveImageProfileService(updateASProfileImageCallBack);
    }else{
      saveImageProfileService(MyProfileModifyOperationcallBack);
}
  }catch(e){
    kony.print("@@@ In opencameraForChangeProfile() Exception:::"+e);
  }
}
function getProfileImageFromSource(){
  gblEditPhotoSource = "png";
  try {
    function onselectioncallback(rawbytes) {
      if (rawbytes == null) {
        showAlertRcMB(kony.i18n.getLocalizedString("KeyImageNotSelected"), kony.i18n.getLocalizedString("info"), "info");
        return;
      }
      var capturedImg = kony.image.createImage(rawbytes);
      var highSize = capturedImg.getImageWidth()> capturedImg.getImageHeight()?
          capturedImg.getImageWidth() : capturedImg.getImageHeight();
      if(highSize >=1024){ //Maximum should not greater than 2048px
        var scaleRatio = highSize/1024;
        scaleRatio = 1/scaleRatio;
        capturedImg.scale(scaleRatio);
      }
      gblEditBase64List = "";
      gblEditBase64List = kony.convertToBase64(capturedImg.getImageAsRawBytes());

      var curSize = (gblEditBase64List.length - 814)/1.37;		

      if (curSize > Gbl_Image_Size_Limit) {
        alert("" + kony.i18n.getLocalizedString("keyImageSizeTooLarge"));
        return;
      }
      gblEditBase64List = gblEditBase64List.replace(/[\n\r\s\f\t\v]+/g, '');
      var currentForm = kony.application.getCurrentForm();
      kony.print("@@@currentForm:::"+currentForm.id)
      if(isNotBlank(currentForm.id) && currentForm.id == "frmAccountSummary"){
        saveImageProfileService(updateASProfileImageCallBack);
      }else{
        saveImageProfileService(MyProfileModifyOperationcallBack);
    }
    }
    var querycontext = {
      mimetype: "image/*"
    };
    returnStatus = kony.phone.openMediaGallery(onselectioncallback, querycontext);
  } catch (err) {
    showAlertRcMB(kony.i18n.getLocalizedString("KeyMediaGalleryError"), kony.i18n.getLocalizedString("info"), "info")
  }
}
function getPermissionCheck(){
  try{
    kony.print("@@@ In getPermissionCheck() @@@");
    var options = {};
    var result = kony.application.checkPermission(kony.os.RESOURCE_CAMERA, options);	
    kony.print("@@@checkCameraPermissionsForScan result ###########  "+result.status);
    if (result.status == kony.application.PERMISSION_DENIED){
      kony.print("@@@checkCameraPermissionsForScan Permission denied  ");
      if (result.canRequestPermission){
        kony.application.requestPermission(kony.os.RESOURCE_CAMERA, reqCameraPermissionCallback);
      }else{
        kony.print("checkCameraPermissionsForScan Permission denied. Cannot request  ");
      }
    } else if (result.status == kony.application.PERMISSION_GRANTED) {
      kony.print("@@@checkCameraPermissionsForScan Permission granted  ");
      kony.print("---Permission allowed--- ");
      opencameraForChangeProfile();    
    } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
      kony.print("@@@reqCameraPermissionCallback-PERMISSION_RESTRICTED: ");
    }

    function reqCameraPermissionCallback(response) {
      kony.print("@@@reqCameraPermissionCallback "+result.status);
      if (response.status == kony.application.PERMISSION_GRANTED) {
        kony.print("@@@reqCameraPermissionCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
        kony.print("---Permission allowed---");
        opencameraForChangeProfile();      
      } else if (response.status == kony.application.PERMISSION_DENIED) {
        kony.print("@@@reqCameraPermissionCallback-PERMISSION_DENIED: " + JSON.stringify(response));
      }
    }
  }catch(e){
    kony.print("@@@ In getPermissionCheck() Exception:::"+e);
  }
}