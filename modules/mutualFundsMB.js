gblFundClassPercentageData = [];
gblUnitHolderNoSelected = "";
gblMFPortfolioType = "PT";
gblUnitHolderNoList = [];

function setSortBtnSkinMBMF() {
  gblStmntSessionFlag= "2";
  noOfStmtServiceCalls=0;
  bindata="";
  binlength="";
  frmMFFullStatementMB.segcredit.removeAll();
  frmMFFullStatementMB.segOrdToPrced.removeAll();
}
//on click of segment, get the details
function getdescriptionMFMB(){
  frmMFFullStatementMB.segcredit.widgetDataMap={
    lblDate: "lblDate",
    lblAmount: "lblAmount",
    lblDescription: "lblDescription",
    ishidden: "yes",
    effectiveDatelbl:"effectiveDatelbl",
    effectiveDate: "effectiveDate",
    unitlbl:"unitlbl",
    unit: "unit",
    investmentlbl:"investmentlbl",
    investmentVal:"investmentVal",
    unitBalLbl:"unitBalLbl",
    unitBal: "unitBal",
    pricelbl:"pricelbl",
    price:"price",
    channel:"channel",
    channelexpval:"channelexpval"
  };
  var descstmt;
  var channelstmt; 
  var tab1=[];
  var indexOfSelectedIndex = frmMFFullStatementMB.segcredit.selectedItems[0];
  var indexOfSelectedRow = frmMFFullStatementMB.segcredit.selectedIndex[1];
  var indexOfSelectedRowexp = gblsegmentdataMB[indexOfSelectedRow];
  if(indexOfSelectedIndex.ishidden == "yes"){
    var selectedData = {
      lblDate: indexOfSelectedIndex.lblDate,
      lblAmount: indexOfSelectedIndex.lblAmount,
      lblDescription: indexOfSelectedIndex.lblDescription,
      ishidden: "no",
      effectiveDatelbl:kony.i18n.getLocalizedString("MF_thr_TransactionDate")+":",
      effectiveDate: indexOfSelectedRowexp.effectiveDate,
      unitlbl:kony.i18n.getLocalizedString("MF_lbl_Unit"),
      unit: indexOfSelectedRowexp.unit,
      investmentlbl:kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2"),
      investmentVal:indexOfSelectedRowexp.investmentVal,
      unitBalLbl:kony.i18n.getLocalizedString("MF_thr_Unit_balance")+":",
      unitBal: indexOfSelectedRowexp.unitBal,
      pricelbl:kony.i18n.getLocalizedString("MF_thr_Price")+":",
      price:indexOfSelectedRowexp.price,
      channel:kony.i18n.getLocalizedString("MF_lbl_Channel"),
      channelexpval:indexOfSelectedRowexp.channelexpval,
      template: hbcActStmtMF
    };
  }
  if(indexOfSelectedIndex.ishidden == "no")
  {
    var selectedData = {
      lblDate: indexOfSelectedIndex.lblDate,
      lblAmount: indexOfSelectedIndex.lblAmount,
      lblDescription: indexOfSelectedIndex.lblDescription,
      ishidden: "yes",
      template: hbxaccntstmt
    };
  }

  kony.table.insert(tab1,selectedData);
  var tab2 = []
  for (var i = 0; i < indexOfSelectedRow; i++) {

    kony.table.insert(tab2, gblsegmentdataMB[i])
  }
  kony.table.append(tab2, tab1);
  for (var j = indexOfSelectedRow + 1; j < gblsegmentdataMB.length; j++) {
    kony.table.insert(tab2, gblsegmentdataMB[j]);
  }
  frmMFFullStatementMB.segcredit.removeAll();
  frmMFFullStatementMB.segcredit.setData(tab2);
}



function MBcallMutualFundsSummary(){

  //#ifdef iphone
  TMBUtil.DestroyForm(frmMutualFundsSummaryLanding);		
  //#endif
  gblUnitHolderNoSelected = "";
  gblMFOrder = {};
  showLoadingScreen();
  var inputParam = {};
  invokeServiceSecureAsync("MFUHAccountSummaryInq", inputParam, MBcallMutualFundsSummaryCallBack);
}
function MBcallMutualFundsSummaryCallBack(status,resulttable){
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
    	
      	//if (resulttable["MF_TERMINOS_FLAG"] != null && resulttable["MF_TERMINOS_FLAG"] != undefined) {
		//	GBL_MF_TEMENOS_ENABLE_FLAG = resulttable["MF_TERMINOS_FLAG"];
		//}
		if(resulttable["FundClassDS"].length <= 0 ){ // MKI, MIB-10003
        	gblIsFundCodeNull =true;
      	}
      	kony.print("MBcallMutualFundsSummaryCallBack Nik Response+ "+JSON.stringify(resulttable));
      	gblMFSummaryData=resulttable;
      	gblUnitHolderNoList  = getUnitHolderNo(gblMFSummaryData);
      	kony.print("gblUnitHolderNoList Nik+ "+JSON.stringify(gblUnitHolderNoList));
      	if(gblMFPortfolioType == "AP"){
          gblUnitHolderNoSelected = formatUnitHolderNumer(gblUnitHolderNoList[0]);
          kony.print("gblUnitHolderNoSelected NIk "+JSON.stringify(gblUnitHolderNoSelected));
        }
      	if(isInvestFromAccSumm){
          //frmMFSelectPurchaseFundMB.show();
          onClickPurchaseMBMenu();
          isInvestFromAccSumm = false;
        }else{
      	frmMFSummary.show();
        }  
    }
    dismissLoadingScreen();
  }
}


function MBpopulateDataMFOverview(summaryDataTotal){
  try{
    kony.print("In MBpopulateDataMFOverview Nik")
      var summaryData=summaryDataTotal["portfolioWiseMarketVal"];
      var segmentData=[];
      var portfolioColor;
      var fundName;
      var unitHolderNumber;
      var unRealizedPL = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
      var unRealizedPLValue;
      var unRealizedPLValueSkin;
      var portfolioColor;
      var locale = kony.i18n.getCurrentLocale();
      var portfolioType;
    
 
    
    kony.print(" gblUnitHolderNoList njh Nik+ "+JSON.stringify(gblUnitHolderNoList));
      var unitHolderLength = gblUnitHolderNoList.length;
      var porfolioMarketValList = summaryData;
      kony.print("njh "+unitHolderLength);
      kony.print("njh Nik"+JSON.stringify(porfolioMarketValList));
    
      for(var i=0;i<unitHolderLength;i++)
      {
        
        //For Portfolio Number, POrtflio Value, Return Percentage
        var portfolioNo = gblUnitHolderNoList[i];
        kony.print("cgnh Nik1 "+portfolioNo);
         kony.print("In calculateOverviewportfolioWiseMarketVal Nik Value " +porfolioMarketValList[0][portfolioNo]);
        var porfolioMarketVal = parseFloat(porfolioMarketValList[0][portfolioNo]);
        var porfolioMarketValwithComma = numberWithCommas(numberFormat(porfolioMarketValList[0][portfolioNo], 2));
        var returnPerc = portfolioNo+":returnPerc";
        kony.print(" returnPerc NIk "+returnPerc);
        var returnPercVal = parseFloat(porfolioMarketValList[0][returnPerc]);
        
        if(returnPercVal === 0){
          unRealizedPLValueSkin = lblIB20pxBlack;
        }else if(returnPercVal>0){
          unRealizedPLValueSkin = lblIB24pxRegGreen;
        }else if(returnPercVal<0){
          unRealizedPLValueSkin = lblIB24pxRegRed;
        }
        
        //For Portfolio Type
        if(portfolioNo.substring(0, 2) == "PT"){
          portfolioType = kony.i18n.getLocalizedString("MF_Home_selfselection"); //"My Mutual Funds";
        }else if(portfolioNo.substring(0, 2) == "AP"){
          portfolioType = kony.i18n.getLocalizedString("MF_Home_AutoSmart");  //"Auto Smart Port";
        }
        var colorNo = i+1;
        switch(colorNo){
          case 1:
            portfolioColor = flxPortolioType1;
            break;
          case 2:
            portfolioColor = flxPortolioType2;
            break;
          case 3:
            portfolioColor = flxPortolioType3;
            break;
          case 4:
            portfolioColor = flxPortolioType4;
            break;
          case 5:
            portfolioColor = flxPortolioType5;
            break;
          case 6:
            portfolioColor = flxPortolioType6;
            break;
          case 7:
            portfolioColor = flxPortolioType7;
            break; 
          case 8:
            portfolioColor = flxPortolioType8;
            break;
          case 9:
            portfolioColor = flxPortolioType9;
            break;
           case 10:
            portfolioColor = flxPortolioType10;
            break;
          case 11:
            portfolioColor = flxPortolioType11;
            break;
          case 12:
            portfolioColor = flxPortolioType12;
            break;
          case 13:
            portfolioColor = flxPortolioType13;
            break;
          case 14:
            portfolioColor = flxPortolioType14;
            break;
          default:
            break;
            
        }
   //  	portfolioColor = "flxPortfolioType"+colorNo;//flxPortfolioType1;
        kony.print("portfolioColor Nik "+portfolioColor);
        var dataObject = {
            lblColor:{"skin":portfolioColor},
            lblfundName:portfolioType,
            lblunitHolderNumber: portfolioNo,
            lblinvestmentValue:porfolioMarketValwithComma + kony.i18n.getLocalizedString("currencyThaiBaht"),
            lblunRealizedPLValue:{"text":returnPercVal+"%","skin":unRealizedPLValueSkin},
            //template:MBMFContent,
            fundCode:portfolioNo,
            fundShortName: portfolioType,
          	template: flxMFOverview
          };
      
     		segmentData.push(dataObject);
      }// for loop ends
    
     return segmentData;

  }catch(e){
    kony.print("Exception in MBpopulateDataMFOverview "+e);
  }
}
function  MBpopulateDataMutualFundsSummary(summaryDataTotal){
  var summaryData=summaryDataTotal["FundClassDS"];
  var segmentDataTemp = [];
  var segmentHeaderTemp = [];
  var segmentData=[];
  var logo;
  var fundName;
  var unitHolderNumber;
  var unRealizedPL = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
  var unRealizedPLValue;
  var investment = kony.i18n.getLocalizedString("MF_lbl_Market_value");
  var investmentValue;
  var segmentTemplate;
  var locale = kony.i18n.getCurrentLocale();
  var header = "" ;
  var segmentZeroBalDataTemp = [];
  var zeroBalCount = 0;
  for(var i=0;i<summaryData.length;i++){

    segmentDataTemp = [];
    segmentHeaderTemp = [];

    var fundhouseDS = summaryData[i].FundHouseDS
    var fundHouseLength = fundhouseDS.length;
    var fundClass=summaryData[i].FundClassCode;

    var noOfAccounts = 0;

    for(var j=0;j<fundHouseLength;j++){

      var FundCodeDS = fundhouseDS[j].FundCodeDS;
      var FundCodeDSLength = FundCodeDS.length;

      var fundHouseCode = fundhouseDS[j]["FundHouseCode"]
      for(var k=0; k < FundCodeDSLength;k++){
        kony.print(" gbl list Nik-----"+gblUnitHolderNoList[k]);
	  if(FundCodeDS[k]["UnitHolderNo"].substring(0, 2) == gblMFPortfolioType ){  // for MB Checking the Prefix for Auto smart
        if(gblUnitHolderNoSelected == "" ||  (gblUnitHolderNoSelected == FundCodeDS[k]["UnitHolderNo"])){
          var randomnum = Math.floor((Math.random() * 10000) + 1);
          //logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext +"/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
		  logo = loadMFIcon(fundHouseCode);
          unRealizedPLValue = FundCodeDS[k]["UnrealizedProfit"];
          var unRealizedPLValueSkin;

          if(unRealizedPLValue == "0"){
            unRealizedPLValueSkin = lblIB20pxBlack;
            unRealizedPLValue = commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
          }else if(unRealizedPLValue.indexOf("-") < 0){
            unRealizedPLValueSkin = lblIB24pxRegGreen;
            unRealizedPLValue = "+" + commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
          }else{
            unRealizedPLValueSkin = lblIB24pxRegRed;
            unRealizedPLValue = commaFormatted(unRealizedPLValue) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
          }
          fundName = FundCodeDS[k]["FundNickNameEN"];
          if(locale != "en_US"){
            fundName = FundCodeDS[k]["FundNickNameTH"];
          }

          //=============Chagned by mayank========================Start
          var formattedAmount = amountFormat(FundCodeDS[k]["MarketValue"]);
          //=============Cahgned by mayank====================================End

          var dataObject = {
            imgLogo:logo,
            lblfundName:fundName,
            lblfundNickNameEN:FundCodeDS[k]["FundNickNameEN"],
            lblfundNickNameTH:FundCodeDS[k]["FundNickNameTH"],
            lblfundNameEN:FundCodeDS[k]["FundNameEN"],
            lblfundNameTH:FundCodeDS[k]["FundNameTH"],
            lblunitHolderNumber: formatUnitHolderNumer(FundCodeDS[k]["UnitHolderNo"]),
            lblunRealizedPL:unRealizedPL,
            lblunRealizedPLValue:{"text":unRealizedPLValue,"skin":unRealizedPLValueSkin},
            lblinvestment:investment,
            lblinvestmentValue:formattedAmount[0],
            lblinvestmentValueDecimal:formattedAmount[1],
            //template:MBMFContent,
            fundCode:FundCodeDS[k]["FundCode"],
            fundClass:fundClass,
            fundHouseCode:fundHouseCode,
            fundShortName: FundCodeDS[k]["FundShortName"],
            imageRightArrow:{"src":"navarrow.png"}
          };

		

          if(FundCodeDS[k]["MarketValue"] != 0) {
            noOfAccounts++;
            segmentDataTemp.push(dataObject);
          } else {
            zeroBalCount++;
            segmentZeroBalDataTemp.push(dataObject);
          }

        }
      }
        


      }//for loop end k
    }//for loop end j


    var moreThanOneAccnt = noOfAccounts > 1 ? "s" : "";
    if(locale == "en_US"){
      header = summaryData[i]["FundClassNameEN"] + " (" + noOfAccounts + " " + kony.i18n.getLocalizedString("MF_lbl_Account")+moreThanOneAccnt+")";
    }else{
      header = summaryData[i]["FundClassNameTH"] + " (" + noOfAccounts + " " +kony.i18n.getLocalizedString("MF_lbl_Account") +")";
    }
    var dataObjectHeader = {
      lblHead: header,
      lblHeadEN: summaryData[i]["FundClassNameEN"] + " (" + noOfAccounts + " ",
      lblHeadTH: summaryData[i]["FundClassNameTH"] + " (" + noOfAccounts + " ",
      template:MBMFHeaderFlex
    };
    if(noOfAccounts > 0){
      segmentHeaderTemp.push(dataObjectHeader);
      segmentData = segmentData.concat(segmentHeaderTemp.concat(segmentDataTemp));
    }


  }//for loop end i
  if(zeroBalCount > 0) {
    segmentHeaderTemp = [];
    var moreThanOneAccnt = zeroBalCount > 1 ? "s" : "";
    header = kony.i18n.getLocalizedString("keyMF0Balance") + " (" + zeroBalCount + " " + kony.i18n.getLocalizedString("MF_lbl_Account");
    if(locale == "en_US"){
      header = header + moreThanOneAccnt + ")";
    }else{
      header = header + ")";
    }
    var dataObjectHeader = {
      lblHead: header,
      lblHeadEN: "",
      lblHeadTH: "",
      template:MBMFHeaderFlex
    };
    segmentHeaderTemp.push(dataObjectHeader);

    segmentData = segmentData.concat(segmentHeaderTemp.concat(segmentZeroBalDataTemp));
  }
  kony.print("MBpopulateDataMutualFundsSummary Nik "+JSON.stringify(segmentData))
  return segmentData;
}

function frmMFSummaryLandingPostShow(){
  //MIB-15251
  //frmMFSummary.flxScrlBody.scrollToWidget(frmMFSummary.flexPortfolio); //mki, mib-11217
  assignGlobalForMenuPostshow();
}

function frmMFSummaryLandingPreShow(){
  try{
    gblChannel = "MB";
    showLoadingScreen();

    if(gblCallPrePost){
      var mfAmount;
      if(gblMFSummaryData != null){
        gblUnitHolderNoList  = getUnitHolderNo(gblMFSummaryData);
        kony.print("gblUnitHolderNoList  ----"+gblUnitHolderNoList);
        kony.print("gblMFPortfolioType ---"+gblMFPortfolioType);
        if(gblMFPortfolioType == "AP" && (gblUnitHolderNoList.length == null || gblUnitHolderNoList.length == 0)){
          frmMFSummary.lblMFAutoHeader.text = kony.i18n.getLocalizedString("MF_how_to_open_AS");
          frmMFSummary.lblMFAutoContent.text = kony.i18n.getLocalizedString("MF_How_To_AS_Content");
          frmMFSummary.btnGotit.text = kony.i18n.getLocalizedString("MF_I_Got_it");
          frmMFSummary.flxNoASPortfolioPopup.setVisibility(true);
          frmMFSummary.flxFreezePeriodPopup.setVisibility(false);    
          dismissLoadingScreen();
          return;
        }
        if(gblMFPortfolioType == "PT" && (gblUnitHolderNoList.length == null || gblUnitHolderNoList.length == 0)){
          frmMFSummary.lblMFAutoHeader.text = kony.i18n.getLocalizedString("MF_SS_Ftitle");  //"You have no holding fund and Portfoio Code" ;
          frmMFSummary.lblMFAutoContent.text = kony.i18n.getLocalizedString("MF_SS_MSG");  //"To start investment, press on +Open New portfolio";
          frmMFSummary.btnGotit.text = kony.i18n.getLocalizedString("MF_SS_Open"); //"+ Open New Portfolio";
          frmMFSummary.flxNoASPortfolioPopup.setVisibility(true);
          frmMFSummary.flxFreezePeriodPopup.setVisibility(false);  
          dismissLoadingScreen();
          return;
        }
        if(gblMFPortfolioType == "AP" && gblMFSummaryData.autoSmartFreezePeriod === "true"){
         
          
          frmMFSummary.lblFreezeHeader.text = kony.i18n.getLocalizedString("MF_how_to_open_AS"); //"Freeze Period for rebalance";
          frmMFSummary.lblMsg.text = kony.i18n.getLocalizedString("MF_How_To_AS_Content"); //"Auto Smart Portfolio are on rebalancing from dd/mm/yyyy to dd/mm/yyyy";
          frmMFSummary.btnFreezeCLose.text = kony.i18n.getLocalizedString("MF_I_Got_it");
          frmMFSummary.flxNoASPortfolioPopup.setVisibility(false);
          frmMFSummary.flxFreezePeriodPopup.setVisibility(true);
          dismissLoadingScreen();
          return;
        }
        kony.print("frmMFSummaryLandingPreShow Nik "+JSON.stringify(gblMFSummaryData));

        frmMFSummary.btnMFFundPortflio.text = kony.i18n.getLocalizedString("MF_Normal_Portfolio"); //"My Mutual Funds";
        frmMFSummary.btnMFAutoSmartPortfolio.text =  kony.i18n.getLocalizedString("MF_Auto_Smart_Portfolio"); //"Auto Smart Port";

        if(gblMFPortfolioType == "OW"){


          var segmentData = MBpopulateDataMFOverview(gblMFSummaryData);
          kony.print("frmMFSummaryLandingPreShow segmentData Nik+ "+JSON.stringify(segmentData));
          frmMFSummary.segAccountDetails.widgetDataMap ={
            lblColor: "lblColor",
            lblfundName:"lblfundName",
            lblinvestmentValue:"lblinvestmentValue",
            lblunitHolderNumber: "lblunitHolderNumber",
            lblunRealizedPL:"lblunRealizedPL",
            lblunRealizedPLValue:"lblunRealizedPLValue",
            fundCode:"fundCode",
            fundShortName:"fundShortName",
            flxMFOverview:flxMFOverview
          }; 
          
          frmMFSummary.segAccountDetails.removeAll();
          frmMFSummary.segAccountDetails.setData(segmentData);
          frmMFSummary.flxRiskLevel.setVisibility(true);
          frmMFSummary.flxReturn.setVisibility(true);
          frmMFSummary.flxFilter.setVisibility(false);
          frmMFSummary.flxOpenPortfolio.setVisibility(true);
          frmMFSummary.lblRiskLevelVal.text = gblMFSuitabilityScore;
          frmMFSummary.flxBody.height = "79%";

          frmMFSummary.LabelPort.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
          frmMFSummary.LabelPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
          frmMFSummary.LabelRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
          frmMFSummary.LabelMore.text = kony.i18n.getLocalizedString("MU_Ttl_More");
          frmMFSummary.LabelSwitch.text = kony.i18n.getLocalizedString("MF_MENU_Switch"); //"Switch";
          var gblDeviceInfo = kony.os.deviceInfo();
          var deviceModel = gblDeviceInfo["model"];
          if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
            frmMFSummary.LabelPort.skin = "lblWhiteNormal";
            frmMFSummary.LabelPurchase.skin = "lblWhiteNormal";
            frmMFSummary.LabelRedeem.skin = "lblWhiteNormal";
            frmMFSummary.LabelMore.skin = "lblWhiteNormal";
            frmMFSummary.LabelSwitch.skin = "lblWhiteNormal";
          }
          else
          {
            frmMFSummary.LabelPort.skin = "lbl20pxWhiteNormal";
            frmMFSummary.LabelPurchase.skin = "lbl20pxWhiteNormal";
            frmMFSummary.LabelRedeem.skin = "lbl20pxWhiteNormal";
            frmMFSummary.LabelMore.skin = "lbl20pxWhiteNormal";
            frmMFSummary.LabelSwitch.skin = "lblWhiteNormal";
          }

          frmMFSummary.lblTitle.text = kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");


          frmMFSummary.lblcurntportfolio.text = "Total Investment";
          frmMFSummary.lblCost.text = kony.i18n.getLocalizedString("MF_Home_Cost1"); //"Cost";
          frmMFSummary.lblProfit.text = kony.i18n.getLocalizedString("MU_SUM_PLB");  
          frmMFSummary.lblUnrelReturn = kony.i18n.getLocalizedString("MF_Home_Return1"); //"Unrealized Return";

          gblOWMarketValue = gblMFSummaryData["overviewMarketValue"];
          frmMFSummary.lblcurntportfolioVal.text = numberWithCommas(numberFormat(gblMFSummaryData["overviewMarketValue"], 2))+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
          if(gblMFSummaryData["overviewMarketValue"] >= 0.00){
            frmMFSummary.lblcurntportfolioVal.skin = lblGreenMFSummary;
          } else {
            frmMFSummary.lblcurntportfolioVal.skin = lblRedMFSummary;
          }
          frmMFSummary.lblCostVal.text = numberWithCommas(numberFormat(gblMFSummaryData["overviewCost"], 2))+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
          if(gblMFSummaryData["overviewCost"] >= 0.00){
            frmMFSummary.lblCostVal.skin = lblGreenMFSummary;
          } else {
            frmMFSummary.lblCostVal.skin = lblRedMFSummary;
          }

          frmMFSummary.lblProfitVal.text = numberWithCommas(numberFormat(gblMFSummaryData["overviewUnrealizedProfit"], 2))+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
          if(gblMFSummaryData["overviewUnrealizedProfit"] >= 0.00){
            frmMFSummary.lblProfitVal.skin = lblGreenMFSummary;
          } else {
            frmMFSummary.lblProfitVal.skin = lblRedMFSummary;
          }

          frmMFSummary.lblUnrelReturnVal.text = numberWithCommas(numberFormat(gblMFSummaryData["overviewunrealizedReturn"], 2))+"%";
          if(gblMFSummaryData["overviewunrealizedReturn"] >= 0.00){
            frmMFSummary.lblUnrelReturnVal.skin = lblGreenMFSummary;
          } else {
            frmMFSummary.lblUnrelReturnVal.skin = lblRedMFSummary;
          }  
          if(gblMFSummaryData["overviewMarketValue"] > 0.00){
            kony.print("Going to addgenerateChart Nik ")
            frmMFSummary.flexChart.setVisibility(true);
            addgenerateChart();
          } else {
            frmMFSummary.flexChart.setVisibility(false);
          }

          dismissLoadingScreen();

		//End of UI display for Overview MF
        }else{ 

          var segmentData= MBpopulateDataMutualFundsSummary(gblMFSummaryData);
          frmMFSummary.segAccountDetails.widgetDataMap ={
            lblHead: "lblHead",
            imgLogo:"imgLogo",
            lblfundName:"lblfundName",
            lblfundNickNameTH:"lblfundNickNameTH",
            lblfundNickNameEN:"lblfundNickNameEN",
            lblunitHolderNumber: "lblunitHolderNumber",
            lblunRealizedPL:"lblunRealizedPL",
            lblunRealizedPLValue:"lblunRealizedPLValue",
            lblinvestment:"lblinvestment",
            lblinvestmentValue:"lblinvestmentValue",
            lblinvestmentValueDecimal:"lblinvestmentValueDecimal",
            fundCode:"fundCode",
            fundHouseCode:"fundHouseCode",
            fundShortName:"fundShortName",
            imageRightArrow:"imageRightArrow"
          };
          frmMFSummary.segAccountDetails.removeAll();
          kony.print("MBpopulateDataMutualFundsSummary  "+JSON.stringify(segmentData))
          frmMFSummary.segAccountDetails.setData(segmentData);
 //       frmMFSummary.segAccountDetails.onRowClick = callMBMutualFundsDetails(frmMFSummary.segAccountDetails.selectedItems[0].lblunitHolderNumber,frmMFSummary.segAccountDetails.selectedItems[0].fundCode)
		  frmMFSummary.segAccountDetails.setEnabled(true);
          frmMFSummary.flxRiskLevel.setVisibility(false);
          frmMFSummary.flxReturn.setVisibility(false);
          frmMFSummary.flxFilter.setVisibility(true);
          frmMFSummary.flxOpenPortfolio.setVisibility(false);
          
          frmMFSummary.lblTitle.text = kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
          frmMFSummary.LabelFilterBy.text = kony.i18n.getLocalizedString("MU_SUM_Filter");
          frmMFSummary.lblcurntportfolio.text = kony.i18n.getLocalizedString("MU_SUM_Current");
          frmMFSummary.lblProfit.text = kony.i18n.getLocalizedString("MU_SUM_PLB");
          frmMFSummary.flxRiskLevel.setVisibility(false);
		  frmMFSummary.flxBody.height = "68%";


          frmMFSummary.LabelPort.text = kony.i18n.getLocalizedString("MU_ACC_InvestVal");
          frmMFSummary.LabelPurchase.text = kony.i18n.getLocalizedString("MU_Purchase");
          frmMFSummary.LabelRedeem.text = kony.i18n.getLocalizedString("MU_Redeem");
          frmMFSummary.LabelMore.text = kony.i18n.getLocalizedString("MU_Ttl_More");
          var gblDeviceInfo = kony.os.deviceInfo();
          var deviceModel = gblDeviceInfo["model"];
          if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
            frmMFSummary.LabelPort.skin = "lblWhiteNormal";
            frmMFSummary.LabelPurchase.skin = "lblWhiteNormal";
            frmMFSummary.LabelRedeem.skin = "lblWhiteNormal";
            frmMFSummary.LabelMore.skin = "lblWhiteNormal";
          }
          else
          {
            frmMFSummary.LabelPort.skin = "lbl20pxWhiteNormal";
            frmMFSummary.LabelPurchase.skin = "lbl20pxWhiteNormal";
            frmMFSummary.LabelRedeem.skin = "lbl20pxWhiteNormal";
            frmMFSummary.LabelMore.skin = "lbl20pxWhiteNormal";
          }
          if(gblUnitHolderNoSelected != "") {
            frmMFSummary.LabelUnitHolderNo.text = formatUnitHolderNumer(gblUnitHolderNoSelected);
          } else {
            if(gblMFPortfolioType == "AP"){
              gblUnitHolderNoSelected = formatUnitHolderNumer(gblUnitHolderNoList[0]);
              frmMFSummary.LabelUnitHolderNo.text = formatUnitHolderNumer(gblUnitHolderNoSelected);
            }else{
              frmMFSummary.LabelUnitHolderNo.text = kony.i18n.getLocalizedString("MU_SUM_Viewall");
            }
          }
          mfAmount = calculateTotalInvestmentValue(gblMFSummaryData, gblUnitHolderNoSelected);
          calculateFundClassPercentageMB(gblMFSummaryData, mfAmount["totalInvestmentValue"], gblUnitHolderNoSelected);

          if(mfAmount["totalInvestmentValue"] > 0.00){
            frmMFSummary.flexChart.setVisibility(true);
            addgenerateChart();
          } else {
            frmMFSummary.flexChart.setVisibility(false);
          }
          frmMFSummary.lblcurntportfolioVal.text = numberWithCommas(numberFormat(mfAmount["totalInvestmentValue"], 2));
          if(mfAmount["totalUnrealizeValue"] >= 0.00){
            frmMFSummary.lblProfitVal.skin = lblGreenMFSummary;
          } else {
            frmMFSummary.lblProfitVal.skin = lblRedMFSummary;
          }
          frmMFSummary.lblProfitVal.text = numberWithCommas(numberFormat(mfAmount["totalUnrealizeValue"], 2));
          dismissLoadingScreen();
        }
      }
    }else{
      dismissLoadingScreen();
      kony.print("no gblCallPrePost = "+gblCallPrePost);
    } 
    dismissLoadingScreen();
  }catch(e) {
    kony.print("Error :::" + e);
  }
}

function frmMutualFundsSummaryLandingPreShow(){
  changeStatusBarColor();
  if (kony.i18n.getCurrentLocale() == "en_US"){
    frmMFSummary.lblAccntHolderName.text = gblCustomerName;
  }else{
    frmMutualFundsSummaryLanding.lblAccntHolderName.text = gblCustomerNameTh;
  }
  frmMutualFundsSummaryLanding.label475124774164.text=kony.i18n.getLocalizedString("MF_Acc_Sumary_Title");
  frmMutualFundsSummaryLanding.lblBalanceValue.text=kony.i18n.getLocalizedString("MF_lbl_Investment_value_h1");
  ////MIB-1418  > MIB-1669
  frmMutualFundsSummaryLanding.btnBack1.text = kony.i18n.getLocalizedString("Back");
  var segmentData= MBpopulateDataMutualFundsSummary(gblMFSummaryData);
  frmMutualFundsSummaryLanding.segAccountDetails.widgetDataMap ={
    lblHead: "lblHead",
    imgLogo:"imgLogo",
    lblfundName:"lblfundName",
    lblfundNickNameTH:"lblfundNickNameTH",
    lblfundNickNameEN:"lblfundNickNameEN",
    lblunitHolderNumber: "lblunitHolderNumber",
    lblunRealizedPL:"lblunRealizedPL",
    lblunRealizedPLValue:"lblunRealizedPLValue",
    lblinvestment:"lblinvestment",
    lblinvestmentValue:"lblinvestmentValue",
    lblinvestmentValueDecimal:"lblinvestmentValueDecimal",
    fundCode:"fundCode",
    fundShortName:"fundShortName",
    imageRightArrow:"imageRightArrow"
  };
  frmMutualFundsSummaryLanding.segAccountDetails.removeAll();
  frmMutualFundsSummaryLanding.segAccountDetails.setData(segmentData);
}

//Mutual fund details
function callMBMutualFundsDetails(unitHolderNuber,fundcode){
  if(unitHolderNuber == null || unitHolderNuber == undefined) {
    return;
  }
  if(gblMFPortfolioType == "OW"){
    return;
  }
  
  gblUnitHolderNumber = unitHolderNuber;
  gblFundCode = fundcode;
  gblFundShort = frmMFSummary.segAccountDetails.selectedItems[0].fundShortName;

  var inputParam = {};
  inputParam["unitHolderNo"] = unitHolderNuber;
  inputParam["funCode"] = fundcode;
  inputParam["fundClassCode"] =frmMFSummary.segAccountDetails.selectedItems[0].fundClass;
  inputParam["serviceType"] = "1";
  showLoadingScreen();
  invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callMutualFundsDetailsCallBackMB);
}

function getSkinValueForUnrealPLMB(data){
  if(parseFloat(data) > 0){
    return lblIB18pxGreenNoMed;
  }else if(parseFloat(data) < 0){
    return lblIB18pxRedNoMed;
  }else{
    return lblGray;
  }
}
function displayUnitLTF5YMB(data){
  if(isNotBlank(data)){
    frmMBMutualFundDetails.hbxUseful10.setVisibility(true);
    frmMBMutualFundDetails.lblUseful10.text = kony.i18n.getLocalizedString("MF_lbl_LTF_Units");
    frmMBMutualFundDetails.lblUsefulValue10.text = verifyDisplayUnit(data);
  }else{
    frmMBMutualFundDetails.hbxUseful10.setVisibility(false);
    frmMBMutualFundDetails.lblUseful10.text = "";
    frmMBMutualFundDetails.lblUsefulValue10.text = "";
  }
} 

function frmMBMutualFundsSummaryPreShow(){
  changeStatusBarColor();
  var locale = kony.i18n.getCurrentLocale();

  // MF Details
  frmMBMutualFundDetails.lblHead.text = kony.i18n.getLocalizedString("MF_Acc_Detail_Title");
  if(isNotBlank(gblFundName)) {
    frmMBMutualFundDetails.lblUsefulValue1.text = gblFundName;
  }
  if (kony.i18n.getCurrentLocale() == "en_US"){
    frmMBMutualFundDetails.lblAccountNameHeader.text = gblSelFundNickNameEN;
  }else{
    frmMBMutualFundDetails.lblAccountNameHeader.text = gblSelFundNickNameTH;
  }
  frmMBMutualFundDetails.lblOrdToBProcess.text = kony.i18n.getLocalizedString("MF_lbl_Order_tobe_process");
  frmMBMutualFundDetails.lnkFullStatement.text = kony.i18n.getLocalizedString("MF_lbl_Full_statement");
  frmMBMutualFundDetails.lnkTaxDoc.text = kony.i18n.getLocalizedString("MF_lbl_Tax_doc");
  frmMBMutualFundDetails.btnBack1.text = kony.i18n.getLocalizedString("Back");	
  frmMBMutualFundDetails.lblUseful1.text = kony.i18n.getLocalizedString("MF_lbl_Fund_name");
  frmMBMutualFundDetails.lblUseful2.text = kony.i18n.getLocalizedString("MF_lbl_Unit_holder_no");
  frmMBMutualFundDetails.lblUseful3.text = kony.i18n.getLocalizedString("MF_lbl_Date_as_of");
  frmMBMutualFundDetails.lblUseful4.text = kony.i18n.getLocalizedString("MF_lbl_Units");
  frmMBMutualFundDetails.lblUseful5.text = kony.i18n.getLocalizedString("MF_lbl_NAV_unit");
  frmMBMutualFundDetails.lblUseful6.text = kony.i18n.getLocalizedString("MF_lbl_Cost");
  frmMBMutualFundDetails.lblUseful7.text = kony.i18n.getLocalizedString("MF_lbl_Investment_value_h2");
  frmMBMutualFundDetails.lblUseful8.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_baht");
  frmMBMutualFundDetails.lblUseful9.text = kony.i18n.getLocalizedString("MF_lbl_Unreal_profit_percent");
  frmMBMutualFundDetails.lblUseful10.text = kony.i18n.getLocalizedString("MF_lbl_LTF_Units");
  frmMBMutualFundDetails.lblUsefulValue1.text = locale == "th_TH" ? gblMBMFDetailsResulttable["FundNameTH"] : gblMBMFDetailsResulttable["FundNameEN"];

}

function viewMFFullStmt(startDate, endDate,pageNumber) {
  //showLoadingScreen();
  frmMFFullStatementNFundSummary.btnFundSummary1.setEnabled(false);
  // frmMFFullStatementNFundSummary.btnFullStatement.setEnabled(true); 
  kony.application.showLoadingScreen("frmLoading", "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
  showAnimationMFLoading1();
  showAnimationMFLoading2();
  //alert("in viewMFFullStmt");
  var inputParam = {};
  if(startDateStmt!=null && startDateStmt!="")
    inputParam["dateFrom"] = startDateStmt;
  else
    inputParam["dateFrom"] = startDate;

  if(endDateStmt!=null && endDateStmt!="")
    inputParam["dateTo"] = endDateStmt;
  else
    inputParam["dateTo"] = endDate;


  inputParam["pageNumber"] = pageNumber;
  inputParam["fundCode"] = gblFundCode;//"TB1";
  inputParam["unitHolderNumber"] = removeHyphenIB(gblUnitHolderNumber);//"3060008079";
  //invokeServiceSecureAsync("MFUHFullStmntInquiry", inputParam, startFullStmtMFServiceMBCallBack);

  invokeServiceSecureAsync("getMFFullStamentAllRecords", inputParam, startFullStmtMFServiceMBCallBack);
}

function clBackChangeMonth(status, result){
  if(status==400){
    if(result["opstatus"]==0){
      getMonthCycleTestMF(result["date"],frmMFFullStatementMB);
    }
  }
}


/** get date in the required format from button text dynamically
**/

function getDateFormatStmtMF(btntext,btnno){

  var monthMB;

  var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
                    kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
                    kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
                    kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
  var month = btntext.split(" ");
  for(i=0;i< montharray.length; i++){
    if(month[0] == montharray[i])
      monthMB = i;
  }
  var yr = month[1];
  var todayDate=gblDate.split("/");
  var fullYear = todayDate[0];
  if(yr==fullYear.substring(2, 4)){
    fullYear=fullYear;
  }else{
    fullYear= parseInt(fullYear)-1;
  }
  var lastDay = new Date(fullYear, monthMB + 1, 0);
  var startday= new Date(fullYear,monthMB,1);
  startDateStmt = dateFormatChangeMF(startday);
  endDateStmt = dateFormatChangeMF(lastDay);
  if(btnno == 0)
  {
    endDateStmt = dateFormatChangeMF(gblDate);//dateFormatChangeMF(todaysDate);//getTodaysDateStmt();
  }

  nextpageStmt = 1;
  viewMFFullStmt(startDateStmt,endDateStmt,nextpageStmt);
}   
/**
  *
  * format date in the required format which is returned fron slider
  */

function dateFormatChangeMF(dateIB) {
  var d = new Date(dateIB);
  var curr_date = d.getDate();
  var curr_month = d.getMonth() + 1; //Months are zero based
  var curr_year = d.getFullYear();
  if (curr_month < 10)
    curr_month = "0" + curr_month;
  if (curr_date < 10)
    curr_date = "0" + curr_date;
  var finalDate =curr_date + "/"+ curr_month + "/" +  curr_year;

  return finalDate;
}


function getMonthCycleTestMF(date,formname){
  var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
                    kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
                    kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
                    kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
  var tempRec = [];
  var resulttableRS = [];
  //todaysDate=date.replace("/", "-");
  todaysDate=date.split("/")[0]+"-"+date.split("/")[1]+"-"+date.split("/")[2];

  var mm=date.split("/")[1];
  var fullYear=date.split("/")[0];
  //yy = fullYear.toString();
  yy = fullYear.substring(2, 4);

  yeararray[0] =  fullYear;

  tempRec = ["0", montharray[mm-1]+" "+yy];
  resulttableRS.push(tempRec);


  mm = mm - 1;
  if(mm == 0){
    mm = 12;
    fullYear = fullYear - 1;
    yy = fullYear.toString();
    yy = yy.substring(2, 4);  		
  }
  yeararray[1] =  fullYear;
  tempRec = ["1", montharray[mm-1]+" "+yy];
  resulttableRS.push(tempRec);


  mm = mm - 1;
  if(mm == 0){
    mm = 12;
    fullYear = fullYear - 1;
    yy = fullYear.toString();
    yy = yy.substring(2, 4); 
  }
  yeararray[2] =  fullYear;
  tempRec = ["2", montharray[mm-1]+" "+yy];
  resulttableRS.push(tempRec);


  mm = mm - 1;
  if(mm == 0){
    mm = 12;
    fullYear = fullYear - 1;
    yy = fullYear.toString();
    yy = yy.substring(2, 4); 
  }
  yeararray[3] =  fullYear;
  tempRec = ["3", montharray[mm-1]+" "+yy];
  resulttableRS.push(tempRec);


  mm = mm - 1;
  if(mm == 0){
    mm = 12;
    fullYear = fullYear - 1;
    yy = fullYear.toString();
    yy = yy.substring(2, 4); 
  }
  yeararray[4] =  fullYear;
  tempRec = ["4", montharray[mm-1]+" "+yy];
  resulttableRS.push(tempRec);


  mm = mm - 1;
  if(mm == 0){
    mm = 12;
    fullYear = fullYear - 1;
    yy = fullYear.toString();
    yy = yy.substring(2, 4); 
  }
  yeararray[5] =  fullYear;
  tempRec = ["5", montharray[mm-1]+" "+yy];
  resulttableRS.push(tempRec);


  mm = mm - 1;
  if(mm == 0){
    mm = 12;
    fullYear = fullYear - 1;
    yy = fullYear.toString();
    yy = yy.substring(2, 4); 
  }
  yeararray[6] =  fullYear;
  tempRec = ["6", montharray[mm-1]+" "+yy];
  resulttableRS.push(tempRec);


  mm = mm - 1;
  if(mm == 0){
    mm = 12;
    fullYear = fullYear - 1;
    yy = fullYear.toString();
    yy = yy.substring(2, 4); 
  }
  yeararray[7] =  fullYear;
 
  frmMFFullStatementMB.cmbMFDates.masterData=resulttableRS;
  frmMFFullStatementMB.cmbMFDates.selectedKey=0;
}


function onRowSelectMFMonth() {
  //selectedMonth = popMFCmboBox.segordToBePrcd.selectedItems[0].lblMonth;
  //var selectedIndex = popMFCmboBox.segordToBePrcd.selectedIndex[1];
  selectedMonth=frmMFFullStatementMB["cmbMFDates"]["selectedKeyValue"][1];
  selectedIndex=frmMFFullStatementMB["cmbMFDates"]["selectedKeyValue"][0];
  getDateFormatStmtMF(selectedMonth,selectedIndex);
  //	gblDate=startDate;
  //	frmMFFullStatementMB.lblMonths.text=getLatestMonthMF();
  onRowSelect="Y";
  //popMFCmboBox.dismiss();
}

function getLatestMonthMF()
{
  var montharray = [kony.i18n.getLocalizedString("keyCalendarJan"), kony.i18n.getLocalizedString("keyCalendarFeb"), kony.i18n.getLocalizedString("keyCalendarMar"), 
                    kony.i18n.getLocalizedString("keyCalendarApr"),kony.i18n.getLocalizedString("keyCalendarMay"), kony.i18n.getLocalizedString("keyCalendarJun"),
                    kony.i18n.getLocalizedString("keyCalendarJul"), kony.i18n.getLocalizedString("keyCalendarAug"),kony.i18n.getLocalizedString("keyCalendarSep"),
                    kony.i18n.getLocalizedString("keyCalendarOct"), kony.i18n.getLocalizedString("keyCalendarNov"), kony.i18n.getLocalizedString("keyCalendarDec")];
  var selectedDate=gblDate;

  if(onRowSelect=="Y")
  {
    selectedDate=startDateStmt;
    onRowSelec="";
  }
  // gblDate=startDateStmt;
  var mm=selectedDate.split("/")[1];
  //alert("mm:"+mm);
  var fullYear=selectedDate.split("/")[2];
  yy = fullYear.substring(2, 4);
  gblCurrentMonth = montharray[mm-1]+" "+yy;
  return gblCurrentMonth;
}

function generateTaxDocPdfMB(){

  var inputParam = {};
  inputParam["unitHolderNo"] = removeHyphenIB(gblUnitHolderNumber);
  inputParam["funCode"] = gblFundCode;
  showLoadingScreenPopup();
  invokeServiceSecureAsync("MFTaxDocumentView", inputParam, mbRenderFileCallbackfunction);
}

function savePDFMutualFundFullStatementServiceMB(fileType, templateName) {
  var locale = kony.i18n.getCurrentLocale();
  var inputParam = {};     
  var totalData = {
    "localeId": kony.i18n.getCurrentLocale()         
  };

  if (gblDeviceInfo.name == "thinclient"){
    inputParam["channelId"] = GLOBAL_IB_CHANNEL;
  } else{
    inputParam["channelId"] = GLOBAL_MB_CHANNEL;
  }
  inputParam["filetype"] = fileType;
  inputParam["templatename"] = templateName;
  inputParam["unitHolderNo"] = gblUnitHolderNumber;


  if(locale == "en_US"){
    inputParam["fundName"] =  gblSelFundNickNameEN;
  }else{
    inputParam["fundName"] =  gblSelFundNickNameTH;
  }
  inputParam["investmentValue"] = commaFormatted(gblMBMFDetailsResulttable["InvestmentValue"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");    

  inputParam["datajson"] = JSON.stringify(totalData, "");
  //showLoadingScreenPopup();   
  showLoadingScreen();
  var url = "";
  invokeServiceSecureAsync("generatePdfImageForMFFullStmts", inputParam, mbRenderFileCallbackfunction);
}

/**
 * on CCMonth1 Click
 */
function onCCMonth1ClickMF() {
  setSortBtnSkinMBMF();
  /*clearCCValuesMF();
	frmMFFullStatementMB.btn1.skin = "btnTabLeftBlue";
	frmMFFullStatementMB.btn2.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn3.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn4.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn5.skin = "btnTab3MidNrml";
	frmMFFullStatementMB.btn6.skin = "btnTab3RightNrml";*/
  callOrderToBeProcessServiceMB();
}


function calculateTotalInvestmentValue(summaryDataTotal, unitHolderNoSelected) {
  var summaryData = summaryDataTotal["FundClassDS"];
  var totalInvestmentValue= 0;
  var totalUnrealizeValue=0;

  for(var i=0;i<summaryData.length;i++){
    var fundHouseDS = summaryData[i].FundHouseDS;
    var fundHouseLength = fundHouseDS.length;

    for(var j=0;j<fundHouseLength;j++){
      var fundCodeDS = fundHouseDS[j].FundCodeDS;
      var fundCodeDSLength = fundCodeDS.length;

      for(var k=0; k < fundCodeDSLength;k++){
        if (unitHolderNoSelected == "" || (unitHolderNoSelected != "" && unitHolderNoSelected == fundCodeDS[k]["UnitHolderNo"])) {
          if(fundCodeDS[k]["UnitHolderNo"].substring(0, 2) == gblMFPortfolioType){
            totalInvestmentValue = totalInvestmentValue +  parseFloat(fundCodeDS[k]["MarketValue"]);
            totalUnrealizeValue = totalUnrealizeValue +  parseFloat(fundCodeDS[k]["UnrealizedProfit"]);
          }
        }                
      } //for loop end k
    } //for loop end j
  } //for loop end i
  //alert ("totalInvestmentValue : " + totalInvestmentValue);
  var retVal = {
    "totalInvestmentValue": totalInvestmentValue,
    "totalUnrealizeValue": totalUnrealizeValue
  };
  return  retVal;
}

function calculateFundClassPercentageMB(summaryDataTotal, mfTotalAmount, unitHolderNoSelected) {
    var summaryData = summaryDataTotal["FundClassDS"];
    /*
      100 Local Fixed Income กองทุนตราสารหนี้ไทย
      200 Foreign Fixed Income กองทุนตราสารหนี้ต่างประเทศ
      300 Balanced กองทุนผสม
      400 Local Equity กองทุนหุ้นไทย
      500 Foreign Equity กองทุนหุ้นต่างประเทศ
      600 Others อื่นๆ
      700 Foreign Balanced กองทุนผสมต่างประเทศ
      800 LTF //MKI, MIB-11485
      900 RMF //MKI, MIB-11485
      */
    gblFundClassPercentageData[100] = 0;
    gblFundClassPercentageData[200] = 0;
    gblFundClassPercentageData[300] = 0;
    gblFundClassPercentageData[400] = 0;
    gblFundClassPercentageData[500] = 0;
    gblFundClassPercentageData[600] = 0;
    gblFundClassPercentageData[700] = 0;
  	gblFundClassPercentageData[800] = 0; //MKI, MIB-11485
    gblFundClassPercentageData[900] = 0; //MKI, MIB-11485
    var totalFundClassValue = 0;
    for (var i = 0; i < summaryData.length; i++) {
        var fundHouseDS = summaryData[i].FundHouseDS;
        var fundHouseLength = fundHouseDS.length;
        var fundValue = 0;
        for (var j = 0; j < fundHouseLength; j++) {
            var fundCodeDS = fundHouseDS[j].FundCodeDS;
            var fundCodeDSLength = fundCodeDS.length;
            var fundHouseCode = fundHouseDS[j]["FundHouseCode"]
            for (var k = 0; k < fundCodeDSLength; k++) {
                if (unitHolderNoSelected == "" || (unitHolderNoSelected != "" && unitHolderNoSelected == fundCodeDS[k]["UnitHolderNo"])) {
                  if(fundCodeDS[k]["UnitHolderNo"].substring(0, 2) == gblMFPortfolioType){
                    fundValue = fundValue + parseFloat(fundCodeDS[k]["MarketValue"]);
                    kony.print("fundValue  --"+fundValue);
                  }
                }
            } //for loop end k
        } //for loop end j
        var fundClassValue = (100 * fundValue) / mfTotalAmount;
      	
        fundClassValue = Math.round(fundClassValue); 
      kony.print("fundClassValue  --"+fundClassValue);
        if (fundClassValue == 0 && fundValue > 0) { 
           fundClassValue = 1; 
        } 
        if (totalFundClassValue + fundClassValue > 100) {
            if (fundClassValue > 0) {
                fundClassValue = 100 - totalFundClassValue;
            }
        }
        totalFundClassValue = totalFundClassValue + fundClassValue;
        var fundClassCode = summaryData[i]["FundClassCode"];
        //alert(fundClassCode + " : " + fundValue + " - " + fundClassValue + " -> " + totalFundClassValue + "%");
        gblFundClassPercentageData[fundClassCode] = fundClassValue;
    } //for loop end i
}
function addUnitHolderNoToList(unitHolderNoList, unitHolderNo) {
  var i = 0;
  var found = false;
  while (i<unitHolderNoList.length && !found){
    if (unitHolderNo == unitHolderNoList[i]) {
      found = true;
    }
    i++;
  }
  if (!found) {
    unitHolderNoList[unitHolderNoList.length] = unitHolderNo;    
  }
  return unitHolderNoList;
}

function getUnitHolderNo(summaryDataTotal) {
  kony.print("In getUnitHolderNo Nik")
  var summaryData = summaryDataTotal["FundClassDS"];
  var unitHolderNoList = [];

  for(var i=0;i<summaryData.length;i++){
    var fundHouseDS = summaryData[i].FundHouseDS;
    var fundHouseLength = fundHouseDS.length;

    for(var j=0;j<fundHouseLength;j++){
      var fundCodeDS = fundHouseDS[j].FundCodeDS;
      var fundCodeDSLength = fundCodeDS.length;
      var fundValue = 0;
      for(var k=0; k < fundCodeDSLength;k++){
        fundValue = parseFloat(fundCodeDS[k]["MarketValue"]);
        if(gblMFPortfolioType == "AP"){
          if(fundCodeDS[k]["UnitHolderNo"].substring(0, 2) == gblMFPortfolioType){
            unitHolderNoList = addUnitHolderNoToList(unitHolderNoList, fundCodeDS[k]["UnitHolderNo"]);
          	kony.print("getUnitHolderNo Nik 1")
          }else{
            //Do nothing
          }
        }else if(gblMFPortfolioType == "PT"){
          if(fundCodeDS[k]["UnitHolderNo"].substring(0, 2) == gblMFPortfolioType){
            unitHolderNoList = addUnitHolderNoToList(unitHolderNoList, fundCodeDS[k]["UnitHolderNo"]);
         	kony.print("getUnitHolderNo Nik 2")
          	}else{
              //do nothing
        }
        }else if(gblMFPortfolioType == "OW"){
           unitHolderNoList = addUnitHolderNoToList(unitHolderNoList, fundCodeDS[k]["UnitHolderNo"]); 
           kony.print("getUnitHolderNo Nik 3")
       
           kony.print("getUnitHolderNo Nik 4+ "+JSON.stringify(unitHolderNoList));

        }
      } //for loop end k
    } //for loop end j
  } //for loop end i
	kony.print("getUnitHolderNo Nik length? "+unitHolderNoList.length)
  return unitHolderNoList;
}


function addChartDescription() //MKI, MIB-11485 Start
{
   var FlexContainer0db5fbdfba85c43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "51%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "FlexContainer0db5fbdfba85c43",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_VERTICAL,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    FlexContainer0db5fbdfba85c43.setDefaultUnit(kony.flex.DP);
    var FlexClass100 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass100",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass100.setDefaultUnit(kony.flex.DP);
    var FlexContainer0e49aed2b2e4041 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35.00%",
        "clipBounds": true,
        "height": "10dp",
        "id": "FlexContainer0e49aed2b2e4041",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexBalanceBlueMagenta",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    FlexContainer0e49aed2b2e4041.setDefaultUnit(kony.flex.DP);
    FlexContainer0e49aed2b2e4041.add();
    var lblClass100 = new kony.ui.Label({
        "id": "lblClass100",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "Balanced",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass100.add(FlexContainer0e49aed2b2e4041, lblClass100);
    var FlexClass200 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass200",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass200.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0ab7b2316ac4340 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0ab7b2316ac4340",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexLocalEquityCyanBlue",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0ab7b2316ac4340.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0ab7b2316ac4340.add();
    var lblClass200 = new kony.ui.Label({
        "id": "lblClass200",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "Local Equity",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass200.add(CopyFlexContainer0ab7b2316ac4340, lblClass200);
    var FlexClass300 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass300",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass300.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0i85cbcd48fa54c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0i85cbcd48fa54c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexLocalFixedIncomeDarkGrey",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0i85cbcd48fa54c.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0i85cbcd48fa54c.add();
    var lblClass300 = new kony.ui.Label({
        "id": "lblClass300",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "Local Fixed Income",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass300.add(CopyFlexContainer0i85cbcd48fa54c, lblClass300);
    var FlexClass400 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass400",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass400.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0b1fbfd69e1014e = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0b1fbfd69e1014e",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexForeignFixedIncomeLightBlue",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0b1fbfd69e1014e.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0b1fbfd69e1014e.add();
    var lblClass400 = new kony.ui.Label({
        "id": "lblClass400",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "Foriegn Fixed Income",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass400.add(CopyFlexContainer0b1fbfd69e1014e, lblClass400);
    var FlexClass500 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass500",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass500.setDefaultUnit(kony.flex.DP);
    var FlexforeignBalanced = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "FlexforeignBalanced",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexForeignBalancedBlue",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    FlexforeignBalanced.setDefaultUnit(kony.flex.DP);
    FlexforeignBalanced.add();
    var lblClass500 = new kony.ui.Label({
        "id": "lblClass500",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "Foreign balanced",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass500.add(FlexforeignBalanced, lblClass500);
    var FlexClass600 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass600",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass600.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0e7aa1f1d13594c = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0e7aa1f1d13594c",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexForeignEquityBlue",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0e7aa1f1d13594c.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0e7aa1f1d13594c.add();
    var lblClass600 = new kony.ui.Label({
        "id": "lblClass600",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "Foriegn Equity",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass600.add(CopyFlexContainer0e7aa1f1d13594c, lblClass600);
    var FlexClass700 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass700",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass700.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0d5853f4a681042 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0d5853f4a681042",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexOthersLightBlue",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0d5853f4a681042.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0d5853f4a681042.add();
    var lblClass700 = new kony.ui.Label({
        "id": "lblClass700",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "Other",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass700.add(CopyFlexContainer0d5853f4a681042, lblClass700);
  	var FlexClass800 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass800",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass800.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0id6f536b844043 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0id6f536b844043",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexLTFDarkBlue",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0id6f536b844043.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0id6f536b844043.add();
    var lblClass800 = new kony.ui.Label({
        "id": "lblClass800",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "LTF",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass800.add(CopyFlexContainer0id6f536b844043, lblClass800);
    var FlexClass900 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "clipBounds": true,
        "id": "FlexClass900",
        "isVisible": true,
        "layoutType": kony.flex.FLOW_HORIZONTAL,
        "left": "3%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    FlexClass900.setDefaultUnit(kony.flex.DP);
    var CopyFlexContainer0bc8966c4290b41 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "35%",
        "clipBounds": true,
        "height": "10dp",
        "id": "CopyFlexContainer0bc8966c4290b41",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flexRMFBlue",
        "top": "4dp",
        "width": "10dp",
        "zIndex": 1
    }, {}, {});
    CopyFlexContainer0bc8966c4290b41.setDefaultUnit(kony.flex.DP);
    CopyFlexContainer0bc8966c4290b41.add();
    var lblClass900 = new kony.ui.Label({
        "id": "lblClass900",
        "isVisible": true,
        "left": "3dp",
        "skin": "lblBlackSmall",
        "text": "RMF",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    FlexClass900.add(CopyFlexContainer0bc8966c4290b41, lblClass900);
    FlexContainer0db5fbdfba85c43.add(FlexClass100, FlexClass200, FlexClass300, FlexClass400, FlexClass500, FlexClass600, FlexClass700, FlexClass800, FlexClass900);
    frmMFSummary.flexChart.add(FlexContainer0db5fbdfba85c43);
} //MKI, MIB-11485 End

function addgenerateChart()
{
  try{
    var locale = kony.i18n.getCurrentLocale();
    var chartObj;
	if(gblMFPortfolioType === "OW"){
      chartObj = kdv_createChartJSObjectOverview();
    }else{
      chartObj = kdv_createChartJSObject();
    }
    var chartWidget = new kony.ui.Chart2D3D({
      "centerX": "50%",
      "centerY": "50%",
      "width": "100%",
      "height":"100%",
      "zIndex": 1,
      "id": "chartWidget",
      "isVisible": true
    }, {
      "padding" : [2,2,2,2],
      "margin":[5,5,5,5],
      "hExpand":true,
      "vExpand":true,
      "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
      "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_CENTER,
      "containerWeight": 100
    }, chartObj);
	frmMFSummary.flexChart.removeAll();
    addChartDescription(); //mki, MIB-9014
    frmMFSummary.flexChart.add(chartWidget);
    clearChartDescription();
    for (var j = 0; j < gblMFSummaryData["FundClassDS"].length; j++) { 
      var fundClassCode = gblMFSummaryData["FundClassDS"][j]["FundClassCode"];
      var fundClassCodeVal = gblFundClassPercentageData[parseInt(fundClassCode)];
      var fundHouseDS = gblMFSummaryData["FundClassDS"][j].FundHouseDS;
	  var fundHouseLength = fundHouseDS.length;
      if( fundClassCodeVal > 0){
        if(locale == "th_TH") {
          lblHead = gblMFSummaryData["FundClassDS"][j]["FundClassNameTH"]
        } else {
          lblHead = gblMFSummaryData["FundClassDS"][j]["FundClassNameEN"]
        }
        kony.print("lblHead before ="+ lblHead);
        var deviceModel = getDeviceModel();
         kony.print("deviceModel1 ="+ deviceModel);
        if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5" || deviceModel == "iPhone SE" || deviceModel == "iPhone 5C") { //MKI, MIB-8974 ======Start
           lblHead = lblHead.substring(0, 15);
         }  //MKI, MIB-8974 ======End
        kony.print("lblHead after ="+ lblHead);
        
        switch(fundClassCode){
          case "100": 
            var unitholderListNo100 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo100.length; i++){
             if(unitholderListNo100[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass100.text = lblHead; 
              frmMFSummary.FlexClass100.setVisibility(true);
             } 
            }
            break;
          case "200": 
            var unitholderListNo200 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo200.length; i++){
             if(unitholderListNo200[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass200.text = lblHead; 
              frmMFSummary.FlexClass200.setVisibility(true);
             } 
            }
            break;
          case "300": 
            var unitholderListNo300 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo300.length; i++){
             if(unitholderListNo300[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass300.text = lblHead;
              frmMFSummary.FlexClass300.setVisibility(true);
             } 
            }
            break;
          case "400": 
            var unitholderListNo400 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo400.length; i++){
             if(unitholderListNo400[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass400.text = lblHead; 
              frmMFSummary.FlexClass400.setVisibility(true);
             } 
            }
            break;
          case "500": 
            var unitholderListNo500 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo500.length; i++){
             if(unitholderListNo500[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass500.text = lblHead; 
              frmMFSummary.FlexClass500.setVisibility(true);
             } 
            }
            break;
          case "600": 
            var unitholderListNo600 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo600.length; i++){
             if(unitholderListNo600[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass600.text = lblHead; 
              frmMFSummary.FlexClass600.setVisibility(true);
             } 
            }
            break;
          case "700":
            var unitholderListNo700 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo700.length; i++){
             if(unitholderListNo700[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass700.text = lblHead; 
              frmMFSummary.FlexClass700.setVisibility(true);
             } 
            }
            break;
          case "800":  //MKI, MIB-11485 start
            var unitholderListNo800 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo800.length; i++){
             if(unitholderListNo800[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass800.text = lblHead; 
              frmMFSummary.FlexClass800.setVisibility(true);
             } 
            }
            break;
          case "900": 
            var unitholderListNo900 = getPT_AP_UnitHolderNo(fundHouseDS,fundHouseLength);
            for(var i = 0; i < unitholderListNo900.length; i++){
             if(unitholderListNo900[i].substring(0, 2) == gblMFPortfolioType){
              frmMFSummary.lblClass900.text = lblHead; 
              frmMFSummary.FlexClass900.setVisibility(true);
             } 
            }
            break; //MKI, MIB-11485 End
        }
      }
    }
  }catch(e) {
    kony.print("Error :::" + e);
  }
}

function getPT_AP_UnitHolderNo(fundHouseDS , fundHouseLength){
  var unitHolderNoList = [];

    for(var j=0;j<fundHouseLength;j++){
      var fundCodeDS = fundHouseDS[j].FundCodeDS;
      var fundCodeDSLength = fundCodeDS.length;
      for(var k=0; k < fundCodeDSLength;k++){
        if(fundCodeDS[k]["UnitHolderNo"].substring(0, 2) == gblMFPortfolioType){
			unitHolderNoList = addUnitHolderNoToList(unitHolderNoList, fundCodeDS[k]["UnitHolderNo"]);
		}
      } //for loop end k
    } //for loop end j

  return unitHolderNoList;
}

function clearChartDescription(){
  frmMFSummary.FlexClass100.setVisibility(false);
  frmMFSummary.FlexClass200.setVisibility(false);
  frmMFSummary.FlexClass300.setVisibility(false);
  frmMFSummary.FlexClass400.setVisibility(false);
  frmMFSummary.FlexClass500.setVisibility(false);
  frmMFSummary.FlexClass600.setVisibility(false);
  frmMFSummary.FlexClass700.setVisibility(false);
  frmMFSummary.FlexClass800.setVisibility(false); //MKI, MIB-11485
  frmMFSummary.FlexClass900.setVisibility(false); //MKI, MIB-11485
}
//creating chart object with chart properties and chart data...

function kdv_createChartJSObject() {  //MKI, MIB-11485 start
  var fontSize = 22; 
  var deviceModel = getDeviceModel();
    kony.print("deviceModel2 ="+ deviceModel);
   if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
     fontSize = 17;
   }  
  var chartJSObj = {

    "chartProperties":{

      "drawEntities":["donutChart"],

      "chartHeight":125,

      "layerArea":{
        "background":{
          "color":["0xffffffff"]
        }
      },

      "donutChart":{
        "startAngle": 90,
        "pieSlice":{
          "color":["0xbfbfbfff","0x7f7f7fff", "0xb4c7e7ff", "0x0070c0ff",  "0xf19e65ff", "0xc55a11ff" , "0xffd966ff","0x000066ff","0x0f45bdff"]
        },

        "border":{
          "line":{
            "color":["0xffffffff"]
          }
        },
        "holeRadius": 70,
        "dataLabels":{
          "indicators":["rowName"],
          "placement" : "outside",

          "font":{
            "size":[fontSize], //MKI, MIB-8974
            "family": ["DBOzoneX"],
            "color" : ["0x00000000"],
          }
        }
      }

    },

    "chartData":
    {
      "columnNames":{
        "values":["Amount1"]
      },

      "data":{
        "Amount1":[gblFundClassPercentageData[100],gblFundClassPercentageData[200] ,gblFundClassPercentageData[300] ,gblFundClassPercentageData[400] ,gblFundClassPercentageData[500] ,gblFundClassPercentageData[600] ,gblFundClassPercentageData[700] ,gblFundClassPercentageData[800] ,gblFundClassPercentageData[900] ]
      },

      "rowNames":{
        "values":[gblFundClassPercentageData[100]+"%",gblFundClassPercentageData[200]+"%",gblFundClassPercentageData[300]+"%",gblFundClassPercentageData[400]+"%",gblFundClassPercentageData[500]+"%", gblFundClassPercentageData[600]+"%" , gblFundClassPercentageData[700]+"%", gblFundClassPercentageData[800]+"%" , gblFundClassPercentageData[900]+"%"]
      }
    }

  };
	kony.print("chartJSObj= "+ JSON.stringify(chartJSObj));
  return chartJSObj;
} //MKI, MIB-11485 End


function kdv_createChartJSObjectOverview() { 
  
  kony.print("In kdv_createChartJSObjectOverview ");
  var fontSize = 45; 
  var deviceModel = getDeviceModel();
  var Amount1= calculateOverviewportfolioWiseMarketVal(); 
  var chartDataValues = calculateOverviewportfolioWiseMarketValPercentage(); 
  var colorforOverview=[];
  var colorCodes = ["0x3c4a9bff","0x66ced6ff","0x3d5a80ff","0x271033ff","0xc49a36ff","0x967351ff","0xdaae9bff","0xd1376dff","0x9fc2d5ff","0x5c408fff","0xc1f7dcff","0xc3d2d5ff","0xbda0bcff","0xa2708aff"]
  for(i=0;i<gblUnitHolderNoList.length;i++){
    
    colorforOverview.push(colorCodes[i]);
    kony.print("colorforOverview Nik "+JSON.stringify(colorforOverview))
  }
  kony.print("Amount1 Nik "+JSON.stringify(Amount1));
  kony.print("char data values Nik "+JSON.stringify(chartDataValues));
  
    kony.print("deviceModel2 ="+ deviceModel);
   if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
     fontSize = 17;
   }  
  var chartJSObj = {

    "chartProperties":{

      "drawEntities":["donutChart"],

      "chartHeight":125,

      "layerArea":{
        "background":{
          "color":["0xffffffff"]
        }
      },

      "donutChart":{
        "startAngle": 90,
        "pieSlice":{
          "color":colorforOverview
        },

        "border":{
          "line":{
            "color":["0xffffffff"]
          }
        },
        "holeRadius": 70,
        "dataLabels":{
          "indicators":["rowName"],
          "placement" : "outside", //for %age values to appear outside

          "font":{
            "size":[fontSize], //MKI, MIB-8974
            "family": ["DBOzoneX"],
            "color" : ["0x000000ff"], //for %age values to appear in black
          }
        }
      }

    },

    "chartData":
    {
      "columnNames":{
        "values":["Amount1"]
      },

      "data":{
        "Amount1":Amount1
      },

      "rowNames":{
        "values":chartDataValues
       }
    }

  };
	kony.print("chartJSObj= Nik "+ JSON.stringify(chartJSObj));
  return chartJSObj;
}


function calculateOverviewportfolioWiseMarketVal(){
    try{
      kony.print("In calculateOverviewportfolioWiseMarketVal Nik");
      var portfolioVal =[];
      var unitHolderLength = gblUnitHolderNoList.length;
      var porfolioMarketValList = gblMFSummaryData["portfolioWiseMarketVal"];
      var overviewMarketValue = gblMFSummaryData["overviewMarketValue"];
      var portfolioValueFinal;
      var totalPortfolioValue = 0;
      kony.print("cgnh "+unitHolderLength);
      kony.print("cgnh Nik"+JSON.stringify(porfolioMarketValList));
      for(var i=0;i<unitHolderLength;i++)
      {
        var value = gblUnitHolderNoList[i];
        kony.print("cgnh Nik1 "+value)
         kony.print("In calculateOverviewportfolioWiseMarketVal Nik Value " +porfolioMarketValList[0][value]);
        var portfolioValueFinal = parseFloat(porfolioMarketValList[0][value])
        
        /* var fundClassValue = (100 * fundValue) / mfTotalAmount;    
		fundClassValue =fundClassValue.toFixed(2); 
        if (totalFundClassValue + fundClassValue > 100) {
            if (fundClassValue > 0) {
                fundClassValue = 100 - totalFundClassValue;
            }
        }
        totalFundClassValue = totalFundClassValue + fundClassValue;
        var fundClassCode = summaryData[i]["FundClassCode"];
        //alert(fundClassCode + " : " + fundValue + " - " + fundClassValue + " -> " + totalFundClassValue + "%");
        gblFundClassPercentageData[fundClassCode] = fundClassValue;
          fundClassValue = Math.round(fundClassValue); 
      kony.print("fundClassValue  --"+fundClassValue);
        if (fundClassValue == 0 && fundValue > 0) { 
           fundClassValue = 1; 
        }
        */
       
         var portfolioValue = (100 * portfolioValueFinal) / overviewMarketValue;    
//		portfolioValue =portfolioValue.toFixed(2); 
          portfolioValue = Math.round(portfolioValue); 
      kony.print("portfolioValue Nik  --"+portfolioValue);
        if (portfolioValue == 0 && portfolioValueFinal > 0) { 
           portfolioValue = 1; 
        }
        if (totalPortfolioValue + portfolioValue > 100) {
            if (portfolioValue > 0) {
                portfolioValue = 100 - totalPortfolioValue;
            }
        }
        totalPortfolioValue = totalPortfolioValue + portfolioValue;
   //     var fundClassCode = summaryData[i]["FundClassCode"];
        //alert(fundClassCode + " : " + fundValue + " - " + fundClassValue + " -> " + totalFundClassValue + "%");
   //     gblFundClassPercentageData[fundClassCode] = fundClassValue;
        
        portfolioVal.push(portfolioValue);

      }
      kony.print("calculateOverviewportfolioWiseMarketVal Nik "+JSON.stringify(portfolioVal));
      return portfolioVal;
      }catch(e){
        kony.print("Exception in calculateOverviewportfolioWiseMarketVal "+e);
          }
}


function calculateOverviewportfolioWiseMarketValPercentage(){
   kony.print("In  calculateOverviewportfolioWiseMarketValPercentage Nik");
  try{
   
       kony.print("calculateOverviewportfolioWiseMarketValPercentage getUnitHolderNo "+JSON.stringify(getUnitHolderNo));
    var portfolioValP =[];
    var unitHolderLength = gblUnitHolderNoList.length;
    var portfolioWiseMarketValwithP;
    var porfolioMarketValList = gblMFSummaryData["portfolioWiseMarketVal"]; 
    var overviewMarketValue = gblMFSummaryData["overviewMarketValue"];
    var portfolioValueFinal;
    var totalPortfolioValue = 0;

    kony.print("cgnh 1 "+unitHolderLength);
    for(var i=0;i<unitHolderLength;i++){
      var value = gblUnitHolderNoList[i];
      kony.print("cgnh Nik1 "+value)
      kony.print("In calculateOverviewportfolioWiseMarketValPercentage Nik Value " +porfolioMarketValList[0][value]);
      //    portfolioWiseMarketValwithP = porfolioMarketValList[0][value] +"%";

      var portfolioValueFinal = parseFloat(porfolioMarketValList[0][value])

      var portfolioValue = (100 * portfolioValueFinal) / overviewMarketValue;    
 //     portfolioValue =portfolioValue.toFixed(2); 
        portfolioValue = Math.round(portfolioValue); 
      kony.print("portfolioValue %age Nik  --"+portfolioValue);
        if (portfolioValue == 0 && portfolioValueFinal > 0) { 
           portfolioValue = 1; 
        }
      if (totalPortfolioValue + portfolioValue > 100) {
        if (portfolioValue > 0) {
          portfolioValue = 100 - totalPortfolioValue;
        }
      }
      totalPortfolioValue = totalPortfolioValue + portfolioValue;
      portfolioWiseMarketValwithP = portfolioValue+"%";
      kony.print("portfolioWiseMarketValwithP + "+portfolioWiseMarketValwithP);
      portfolioValP.push(portfolioWiseMarketValwithP);
 
  }

   kony.print("calculateOverviewportfolioWiseMarketVal Nik "+JSON.stringify(portfolioValP));
  return portfolioValP;
  }catch(e){
    kony.print("Exception in calculateOverviewportfolioWiseMarketVal "+e);
  } 
} 

