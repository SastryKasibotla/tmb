gbleKYCConfirmPIN="";
gbleKYCAccessPINFlow=""; 
function showMBeKYCAccessPin(){
  resetPINeKYC();
  frmMBeKYCAccessPin.flxMsgNotification.setVisibility(false);
  frmMBeKYCAccessPin.show();
}
function initMBeKYCAccessPin(){
  frmMBeKYCAccessPin.preShow=preShowMBeKYCAccessPin;
}
function preShowMBeKYCAccessPin(){
  kony.print("Inside Access Pin Preshow");
  
  if(gbleKYCVerifyAccessPin == "VERIFY"){
    kony.print("Inside VERIFY Preshow");
    gbleKYCAccessPINFlow="VERIFY_PIN";
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_titleEnterAccessPIN"),null,false,gbleKYCStep);
    frmMBeKYCAccessPin.btnAccessPin.onClick = onClickForgotPin;
    frmMBeKYCAccessPin.btnAccessPin.text = kony.i18n.getLocalizedString("eKYC_lkForgotAccessPIN");
    frmMBeKYCAccessPin.lblsubHeader.text = kony.i18n.getLocalizedString("eKYC_msgConfirmPINtoOpenAcct");
    frmMBeKYCAccessPin.btnAccessPin.setVisibility(true);
    frmMBeKYCAccessPin.lblsetUpPin.setVisibility(false);
  }else if(gbleKYCVerifyAccessPin == "EKYC_LOGIN"){
    //Shail : After EKYC flow completion user can login using accesspin. 
    kony.print("Inside EKYC_LOGIN Preshow");
    gbleKYCAccessPINFlow = "EKYC_LOGIN";
    setHeaderContentToForm(kony.i18n.getLocalizedString("login"),btnTnCBackeKYC,true,gbleKYCStep);
    frmMBeKYCAccessPin.lblsetUpPin.setVisibility(false);
     frmMBeKYCAccessPin.btnAccessPin.onClick = onclickOpenAccounteKYC;
    frmMBeKYCAccessPin.lblsubHeader.text=kony.i18n.getLocalizedString("eKYC_msgPlsLogin");
    frmMBeKYCAccessPin.btnAccessPin.text=kony.i18n.getLocalizedString("lblForgotPinLink");
  	frmMBeKYCAccessPin.btnTouchnBack.onClick = onClickPinBack;
  }else{
    gbleKYCAccessPINFlow="CREATE_PIN";
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleSetPIN"),showcreatePINIntro,true,gbleKYCStep);
    frmMBeKYCAccessPin.btnAccessPin.onClick=openMBActivationForm;
    frmMBeKYCAccessPin.lblsetUpPin.text= kony.i18n.getLocalizedString("eKYC_lbSetPIN");
    frmMBeKYCAccessPin.btnAccessPin.text = kony.i18n.getLocalizedString("eKYC_btnPINrules");
    frmMBeKYCAccessPin.lblsubHeader.text = kony.i18n.getLocalizedString("eKYC_msgCreatePINtoLogin");
    frmMBeKYCAccessPin.btnAccessPin.setVisibility(true);
    frmMBeKYCAccessPin.btnAccessPin.onClick=showCreatePinRules;
    frmMBeKYCAccessPin.btnTouchnBack.onClick=onClickPinBack;
    frmMBeKYCAccessPin.btnclose.onClick=onClickDismissPopup;
  }
  resetPINeKYC();
  frmMBeKYCAccessPin.btnOne.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnTwo.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnThree.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnFour.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnFive.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnSix.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnSeven.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnEight.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnNine.onClick=onClickeKYCKeyBoardButton;
  frmMBeKYCAccessPin.btnZero.onClick=onClickeKYCKeyBoardButton;
}

function showCreatePinRules(){
  frmMBeKYCAccessPin.FlexPasswordRules.setVisibility(true);
  setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleSetPIN"),null,false,gbleKYCStep);
}

function onClickDismissPopup(){
frmMBeKYCAccessPin.FlexPasswordRules.setVisibility(false);
setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleSetPIN"),showcreatePINIntro,true,gbleKYCStep);
}

function onClickeKYCKeyBoardButton(eventobject) {
  kony.print(" Event object  "+eventobject);
    onClickOfeKYCKeypad();
    getPineKYC(eventobject);
    //onClickChangeTouchIcon();
}


function onClickOfeKYCKeypad() {

    if (gblPinCount < 6) {
        gblPinCount++;
    }
	switch(gblPinCount) {
	    case 1:
	        frmMBeKYCAccessPin.imgone.src = "key_sel.png";
	        break;
	   case 2:
	        frmMBeKYCAccessPin.imgTwo.src = "key_sel.png";
	        break;
	   case 3:
	        frmMBeKYCAccessPin.imgThree.src = "key_sel.png";
	        break;
	    case 4:
	        frmMBeKYCAccessPin.imgFour.src = "key_sel.png";
	        break;
	    case 5:
	         frmMBeKYCAccessPin.imgFive.src = "key_sel.png";
	        break;
	    case 6:
	        frmMBeKYCAccessPin.imgSix.src = "key_sel.png";
	        break;            
	             
	} 
}

/*****************************************************************
 *     Name    : onClickPinBack
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to  unfill small circles when we click on back button
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function onClickPinBack() {
  if (gblPinCount > 0) {
    gblPinCount--;
    var pinLen = gblNum;
    var updatedPin = gblNum.substring(0, pinLen.length - 1)
    gblNum = updatedPin;
  }

  switch(gblPinCount) {
    case 0:
      frmMBeKYCAccessPin.imgone.src = "key_default.png";
      break;
    case 1:frmMBeKYCAccessPin.imgTwo.src = "key_default.png";
      break;
    case 2:frmMBeKYCAccessPin.imgThree.src = "key_default.png";
      break;
    case 3:frmMBeKYCAccessPin.imgFour.src = "key_default.png";
      break;
    case 4:frmMBeKYCAccessPin.imgFive.src = "key_default.png";
      break;
    case 5:frmMBeKYCAccessPin.imgSix.src = "key_default.png";
      break;

  }

}

/*****************************************************************
 *     Name    : getPin
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function to use get number from keypad
 *     Params  : yes
 * 	   Changes :
 ******************************************************************/
function getPineKYC(obj) {
    var temp;
    var caseVar = obj.id;
    
    switch(caseVar) {
	    case "btnOne":
	    	temp = "1";
	     break;
	    case "btnTwo":
	    	temp = "2";
    	break;
    	case "btnThree":
    		temp = "3";
    	break;
    	case "btnFour":
    		temp = "4";
    	break;
    	case "btnFive":
    		temp = "5";
    	break;
    	case "btnSix":
    		temp = "6";
    	break;
    	case "btnSeven":
    		 temp = "7";
    	break;
    	case "btnEight":
    		 temp = "8";
    	break;	
    	case "btnNine":
    		 temp = "9";
    	break;
    	case "btnZero":
    		 temp = "0";
    	break;			
    }
    

    if (gblNum.length < 6) {
        gblNum = gblNum + temp;

    }
    if (gblNum.length == 6) {
      	
    if(gbleKYCAccessPINFlow==="CREATE_PIN" || gbleKYCAccessPINFlow==="CONFIRM_PIN"){
      onDoneeKYCAccessPIN();
    }else if(gbleKYCAccessPINFlow==="VERIFY_PIN"){
      calleKYC_eKYCCreateRMOpenAcctCompositeService(gblNum.trim());
    }else if(gbleKYCAccessPINFlow==="EKYC_LOGIN"){
 		 glbAccessPin=gblNum;
         callloginekycService();
    }    
     //   accsPwdValidatnLogin(gblNum.trim());
    }
}

function onDoneeKYCAccessPIN(){
  kony.print("gbleKYCAccessPINFlow =="+gbleKYCAccessPINFlow);
  if(accessPinrule(gblNum)){
    frmMBeKYCAccessPin.flxMsgNotification.setVisibility(false);
        if(gbleKYCAccessPINFlow==="CONFIRM_PIN"){
        validateCreatePINeKYC();
        }else if(gbleKYCAccessPINFlow==="CREATE_PIN"){
            showeKYCConfirmPin();
        }
  }else{
    resetPINeKYC();
    frmMBeKYCAccessPin.lblMessage.text=kony.i18n.getLocalizedString("eKYC_msgPINnotSecured");
    frmMBeKYCAccessPin.flxMsgNotification.setVisibility(true);
    animateIdAndSelfieImages(frmMBeKYCAccessPin.flxArrow,45);
  }
}

function showeKYCConfirmPin(){
  gbleKYCConfirmPIN=gblNum;
  gblNum="";
  resetPINeKYC();
  frmMBeKYCAccessPin.lblsetUpPin.text= getLocalizedString("eKYC_lbConfirmPIN");
  frmMBeKYCAccessPin.lblsubHeader.text = getLocalizedString("eKYC_msgRepeatPIN");
  frmMBeKYCAccessPin.flxMsgNotification.setVisibility(false);
  frmMBeKYCAccessPin.btnAccessPin.setVisibility(false);
  gbleKYCAccessPINFlow="CONFIRM_PIN";
}

function resetPINeKYC(){
  gblNum="";
  gblPinCount=0;
  frmMBeKYCAccessPin.imgone.src = "key_default.png";
  frmMBeKYCAccessPin.imgTwo.src = "key_default.png";
  frmMBeKYCAccessPin.imgThree.src = "key_default.png";
  frmMBeKYCAccessPin.imgFour.src = "key_default.png";
  frmMBeKYCAccessPin.imgFive.src = "key_default.png";
  frmMBeKYCAccessPin.imgSix.src = "key_default.png";

}

function validateCreatePINeKYC(){
  if(gblNum===gbleKYCConfirmPIN){
     //&& gbleKYCAccessPINFlow==="CONFIRM_PIN"
      kony.print("gbleKYCConfirmPIN : "+gbleKYCConfirmPIN);
      var inputParam = {};
      kony.print("Calling service EKYCSetAccessPinJavaService");
      inputParam.mbAcPwd = encryptData(gbleKYCConfirmPIN);
      inputParam["deviceId"] = getDeviceID();
      inputParam["deviceNickName"] = "Test1";

      inputParam["deviceModel"] = getDeviceModel();
      inputParam["osType"] = getDeviceOS();


      showLoadingScreen();
      kony.print("PIN Matched");
      invokeServiceSecureAsync("EKYCSetAccessPinJavaService", inputParam, callBackEKYCSetAccessPinJavaService);
    
  }else{
    resetPINeKYC();
    kony.print("PIN Not Matched");
    frmMBeKYCAccessPin.lblMessage.text=kony.i18n.getLocalizedString("eKYC_msgPINnotMatchrd");
    frmMBeKYCAccessPin.flxMsgNotification.setVisibility(true);
    animateIdAndSelfieImages(frmMBeKYCAccessPin.flxArrow,45);
  }
}


function callBackEKYCSetAccessPinJavaService(status, result){
  if (status == 400) {
     kony.print("Result "+JSON.stringify(result));
   		if (result.opstatus === "0") {
          dismissLoadingScreen();
          gbleKYCStep="3";
          kony.store.removeItem("eKYCRegistrationFlag");
          kony.store.setItem("eKYCRegistrationFlag", "Y");
          var expiryDate=result["ExpiryDate"];
          gblEKYCFlowExpireDate = expiryDate;
         // encryptSecretKey(result["secretKey"],result["encryptKey"]);
          resetPINeKYC();
    	  navigateKYCIntroduction();
        } else {
          dismissLoadingScreen();
          showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
          return false;
        }
  }
}

function accessPinrule(num){
  var arr  = num.toString().split('') ;
  if(arr[0] !== arr[1] && arr[1] !== arr[2] && arr[2] !== arr[3] && arr[3] !== arr[4] && arr[4] !== arr[5] && arr[5] !== arr[0]){
    //(/([0-9]).*?\1/).test(num);
    if (!(/123456/.test(num))) {
      return true;
    }else{
      return false;
    }
  }else{
    return false;
  }
}
