function initfrmEkycIdInformation(){
  frmMBeKYCIDInfo.preShow = ekycidinfopreshow;
  //frmMBeKYCIDInfo.postShow = ekycidinfopostshow;
}

function ekycidinfopreshow(){
   var currentdatearray = EkycCurrentDate();
   var dateformatee = "dd MMM yyyy";
   setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_TitleInfoReadfromCID"),backCitizenID,true,gbleKYCStep);
//    setFooterTwobtnToForm(kony.i18n.getLocalizedString("Rescan"),kony.i18n.getLocalizedString("Correct"),backCitizenID,navToEnterLaserCode);
   setFooterTwobtnToForm(kony.i18n.getLocalizedString("Rescan"),kony.i18n.getLocalizedString("Correct"),"",ReScanCitizenID,validateCIDFields,null,true,false);
   frmMBeKYCIDInfo.lblTitle.text = kony.i18n.getLocalizedString("eKYC_msgCheckInfoCID");
   frmMBeKYCIDInfo.imgCheck.onTouchEnd = disableIDExpireDate;
   frmMBeKYCIDInfo.txtIDNo.onTextChange = formatCitizenIDekyc;
   frmMBeKYCIDInfo.calDOE.dateFormat = dateformatee;
   frmMBeKYCIDInfo.calDOI.dateFormat = dateformatee;
   frmMBeKYCIDInfo.calDOB.dateFormat = dateformatee;
   frmMBeKYCIDInfo.calDOB.validEndDate = currentdatearray;
   frmMBeKYCIDInfo.calDOI.validEndDate = currentdatearray;
   frmMBeKYCIDInfo.calDOE.validStartDate = currentdatearray;
//    frmMBeKYCIDInfo.calDOB.dateComponents = currentdatearray;
  if(kony.os.deviceInfo().name == "iphone" || kony.os.deviceInfo().name == "iPhone"){
   frmMBeKYCIDInfo.switchToggleIOS.onSlide = toggelIDExpireDateDisable;
  }
  frmMBeKYCIDInfo.lblIDNo.text = getLocalizedString("ID");
  frmMBeKYCIDInfo.lblTitleName.text = getLocalizedString("EngName");
  frmMBeKYCIDInfo.lblTitleLastname.text = getLocalizedString("EngSurname");
  frmMBeKYCIDInfo.lblThaiFirstName.text = getLocalizedString("ThaiName");
  frmMBeKYCIDInfo.lblThaiLastName.text = getLocalizedString("ThaiSurname");
  frmMBeKYCIDInfo.lblDOB1.text = getLocalizedString("DateofBirth");
  frmMBeKYCIDInfo.lblDOI1.text = getLocalizedString("IssueDate");
  frmMBeKYCIDInfo.lblDOE1.text = getLocalizedString("ExpiredDate");
  frmMBeKYCIDInfo.lblNoExpire.text = getLocalizedString("CID_NeverExpired");
  frmMBeKYCIDInfo.txtName.onTextChange = enFirstnamevalidate;
  frmMBeKYCIDInfo.txtLastName.onTextChange = enSurnamenamevalidate;
  frmMBeKYCIDInfo.txtThaiFirstName.onTextChange = thFirstnamevalidate;
  frmMBeKYCIDInfo.txtThaiLastName.onTextChange = thSurnamenamevalidate;
  frmMBeKYCIDInfo.txtName.onDone = enFirstnamevalidateondone;
  frmMBeKYCIDInfo.txtLastName.onDone = enSurnamenamevalidateondone;
  frmMBeKYCIDInfo.txtThaiFirstName.onDone = thFirstnamevalidateondone;
  frmMBeKYCIDInfo.txtThaiLastName.onDone = thSurnamenamevalidateondone;
}

function validateCIDFields() {
    var msg = "";
    var enFirstName = frmMBeKYCIDInfo.txtName.text;
    var enLastName = frmMBeKYCIDInfo.txtLastName.text;
    var thFirstName = frmMBeKYCIDInfo.txtThaiFirstName.text;
    var thLastName = frmMBeKYCIDInfo.txtThaiLastName.text;
    var citizenId = frmMBeKYCIDInfo.txtIDNo.text;
    if(!isNotBlank(citizenId)){
       msg = kony.i18n.getLocalizedString("eKYC_msgEmptyfield");
       msg = msg + "Citizen ID";
       showAlertWithCallBack(msg,kony.i18n.getLocalizedString("info"),CIDTxtFocus);
    }else if(citizenId.length !== 17){
       showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectCIDNo"),kony.i18n.getLocalizedString("info"),CIDTxtFocus);
    }else if(!isNotBlank(enFirstName)){
      showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectFirstName"),kony.i18n.getLocalizedString("info"),enFirstNameFocus);
    }else if(!isNotBlank(enLastName)){
      showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectLastName"),kony.i18n.getLocalizedString("info"),enSurNameFocus);
    }else if(!isNotBlank(thFirstName)){
      showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThFirstName"),kony.i18n.getLocalizedString("info"),thFirstNameFocus);
    }else if(!isNotBlank(thLastName)){
      showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThLastName"),kony.i18n.getLocalizedString("info"),thSurNameFocus);
    }else{
    var enFirstNameLength = enFirstName.length;
    var enSurNameLength = enLastName.length;
    var thFirstNameLength = thFirstName.length;
    var thSurNameLength = thLastName.length;
    var enFullNameLength =  enFirstNameLength + enSurNameLength;
    var thFullNameLength =  thFirstNameLength + thSurNameLength;
    if(enFullNameLength > 40){
             var msgenname = kony.i18n.getLocalizedString("eKYC_msgNameLength40Char");
             var title = kony.i18n.getLocalizedString("info");
             var okk = kony.i18n.getLocalizedString("Loan_Continue");
             var Noo = kony.i18n.getLocalizedString("WIFI_btnCancel");
             showAlertWithYesNoHandler(msgenname,title,okk,Noo,enNameTrim, doNothing);
    }
    else if(thFullNameLength > 40){
             var msgthname = kony.i18n.getLocalizedString("eKYC_msgNameLength40Char");
             var title = kony.i18n.getLocalizedString("info");
             var okk = kony.i18n.getLocalizedString("Loan_Continue");
             var Noo = kony.i18n.getLocalizedString("WIFI_btnCancel");
             showAlertWithYesNoHandler(msgthname,title,okk,Noo,thNameTrim, doNothing);
      
    }else if (frmMBeKYCIDInfo.calDOI.date === "" && frmMBeKYCIDInfo.calDOE.date === "") {
        var dateErrMsg = kony.i18n.getLocalizedString("eKYC_msgEmptyfield") + " " + kony.i18n.getLocalizedString("IssueDate");
        showAlert(dateErrMsg, kony.i18n.getLocalizedString("info"));
    }else if (frmMBeKYCIDInfo.calDOI.date === "") {
        var dateErrMsg = kony.i18n.getLocalizedString("eKYC_msgEmptyfield") + " " + kony.i18n.getLocalizedString("IssueDate");
        showAlert(dateErrMsg, kony.i18n.getLocalizedString("info"));
    }else if (frmMBeKYCIDInfo.calDOI.date === frmMBeKYCIDInfo.calDOE.date) {
      if(null !== frmMBeKYCIDInfo.imgCheck.src && frmMBeKYCIDInfo.imgCheck.src !== "chkbox_checked.png"){
			showAlert(kony.i18n.getLocalizedString("eKYC_msgIncorrectIssuedDate"), kony.i18n.getLocalizedString("info"));
		}else{
         saveCitizenIdDetailsInSession();
    }
    }else if (frmMBeKYCIDInfo.calDOE.date === "") {
      if(null !== frmMBeKYCIDInfo.imgCheck.src && frmMBeKYCIDInfo.imgCheck.src !== "chkbox_checked.png"){
         var dateErrMsg = kony.i18n.getLocalizedString("eKYC_msgEmptyfield") + " " + kony.i18n.getLocalizedString("ExpiredDate");
         showAlert(dateErrMsg, kony.i18n.getLocalizedString("info"));
        }else{
      saveCitizenIdDetailsInSession();
      }
    }else{
      saveCitizenIdDetailsInSession();
    }
      
    }
        
//     clearCizizenIdDetails();
    //TODO all the fields validation.
    //Call the validation service
    
}

function CIDTxtFocus(){
  frmMBeKYCIDInfo.txtIDNo.setFocus(true);
}

function enNameTrim(){
    var enFirstName = frmMBeKYCIDInfo.txtName.text;
    var enLastName = frmMBeKYCIDInfo.txtLastName.text;
    var enFirstNameLength = enFirstName.length;
    var enSurNameLength = enLastName.length;
  if(enFirstNameLength > 20 && enSurNameLength > 20){
    if(enFirstNameLength > 20){
    var fillteredName = enFirstName.substring(0, 20);
    frmMBeKYCIDInfo.txtName.text = fillteredName;
    }
    if(enSurNameLength > 20){
    var fillteredLastName = enLastName.substring(0, 20);
    frmMBeKYCIDInfo.txtLastName.text = fillteredLastName;
    }
  }else{
    if(enFirstNameLength < 20){
        var remaininglength = 40 - enFirstNameLength;
        var fillteredLastName = enLastName.substring(0, remaininglength);
        frmMBeKYCIDInfo.txtLastName.text = fillteredLastName;
    }
    if(enSurNameLength < 20){
        var remaininglength = 40 - enSurNameLength;
        var fillteredName = enFirstName.substring(0, remaininglength);
        frmMBeKYCIDInfo.txtName.text = fillteredName;
    }      
  }
}

function thNameTrim(){
    var thFirstName = frmMBeKYCIDInfo.txtThaiFirstName.text;
    var thLastName = frmMBeKYCIDInfo.txtThaiLastName.text;
    var thFirstNameLength = thFirstName.length;
    var thSurNameLength = thLastName.length;
  if(thFirstNameLength > 20 && thSurNameLength > 20){
    if(thFirstNameLength > 20){
    var fillteredName = thFirstName.substring(0, 20);
    frmMBeKYCIDInfo.txtThaiFirstName.text = fillteredName;
  }
    if(thSurNameLength > 20){
    var fillteredLastName = thLastName.substring(0, 20);
    frmMBeKYCIDInfo.txtThaiLastName.text = fillteredLastName;
   }
  }else{
    if(thFirstNameLength < 20){
        var remaininglength = 40 - thFirstNameLength;
        var fillteredLastName = thLastName.substring(0, remaininglength);
        frmMBeKYCIDInfo.txtThaiLastName.text = fillteredLastName;
    }
    if(thSurNameLength < 20){
        var remaininglength = 40 - thSurNameLength;
        var fillteredName = thFirstName.substring(0, remaininglength);
        frmMBeKYCIDInfo.txtThaiFirstName.text = fillteredName;
    }
  }
}

function clearCizizenIdDetails(){
  frmMBeKYCIDInfo.txtIDNo.text = "";
  frmMBeKYCIDInfo.txtThaiFirstName.text = "";
  frmMBeKYCIDInfo.txtThaiLastName.text = "";
  frmMBeKYCIDInfo.txtName.text = "";
  frmMBeKYCIDInfo.txtLastName.text = "";
}

function saveCitizenIdDetailsInSession(){
  showLoadingScreen();
  var inputParam = {};
  
  inputParam.nationality = "Thailand";
  inputParam.dateofBirth = frmMBeKYCIDInfo.calDOB.year+"-"+frmMBeKYCIDInfo.calDOB.month+"-"+frmMBeKYCIDInfo.calDOB.day; //yyyy-MM-dd
  inputParam.firstName = frmMBeKYCIDInfo.txtThaiFirstName.text;
  inputParam.lastName =  frmMBeKYCIDInfo.txtThaiLastName.text;
  inputParam.firstName_EN = frmMBeKYCIDInfo.txtName.text;
  inputParam.lastName_EN =  frmMBeKYCIDInfo.txtLastName.text;
  
  var citizenId = frmMBeKYCIDInfo.txtIDNo.text;
  inputParam.citizenId = replaceAll(citizenId,'-','');
  
  inputParam.IdDOI = frmMBeKYCIDInfo.calDOI.year+"-"+frmMBeKYCIDInfo.calDOI.month+"-"+frmMBeKYCIDInfo.calDOI.day; //yyyy-MM-dd
  inputParam.IdDOE = frmMBeKYCIDInfo.calDOE.year+"-"+frmMBeKYCIDInfo.calDOE.month+"-"+frmMBeKYCIDInfo.calDOE.day; //yyyy-MM-dd;
  inputParam.idType = "CI";
 
  invokeServiceSecureAsync("validateDOEDOBService", inputParam, callBackSaveCIDetailsInSession);
}

function callBackSaveCIDetailsInSession(status, result){
   if (status == 400) {
     var errorcode = result["errCode"];
      if (result["opstatus"] === 0 ||result["opstatus"] == "0") {
		kony.print(""+JSON.stringify(result));
		navToEnterLaserCode();
      }else {
        if(errorcode == "eKYCDOE01"){
          alert(getLocalizedString("eKYC_msgPassportExpiredDesc"));
        }else if(errorcode == "eKYCDOB01"){
          alert(getLocalizedString("eKYC_msgAgeLessthan18Y"));
        }else if(errorcode == "eKYCNATIONALITY01"){
          alert("Allowed Thai Passport only");
        }
//       showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        dismissLoadingScreen();
      return false;
    }
   }else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    } 
      dismissLoadingScreen();
}

function disableIDExpireDate(){
  if(null !== frmMBeKYCIDInfo.imgCheck.src && frmMBeKYCIDInfo.imgCheck.src == "chkbox_uncheck.png"){
    frmMBeKYCIDInfo.flxDOE1.setEnabled(false);
    frmMBeKYCIDInfo.flxDOE1.skin = "flexGreyBGLoan";
    frmMBeKYCIDInfo.imgCheck.src="chkbox_checked.png";
  }else{
    frmMBeKYCIDInfo.flxDOE1.setEnabled(true);
    frmMBeKYCIDInfo.flxDOE1.skin = "flexWhiteBG";
    frmMBeKYCIDInfo.imgCheck.src="chkbox_uncheck.png";
  }
}

function toggelIDExpireDateDisable(){
  if(frmMBeKYCIDInfo.switchToggleIOS.selectedIndex === 1){
    frmMBeKYCIDInfo.flxDOE1.setEnabled(false);
    frmMBeKYCIDInfo.flxDOE1.skin = "flexGreyBGLoan";
  }else{
    frmMBeKYCIDInfo.flxDOE1.setEnabled(true);
    frmMBeKYCIDInfo.flxDOE1.skin = "flexWhiteBG";
  }
}

function backCitizenID(){
  if(gblEKYCFlowCIDFlag){
    frmMBNewTnCLoan.show();
  }else{
  frmeKYCGetYourIdReady.show();
  resetGetYourIdReadyDetails();
  }
}

function ReScanCitizenID(){
   chkRunTimePermissionsForeKYC_CID();
}

function navToEnterLaserCode() {
  frmMBeKYCCitezenIDInfo.show();
  navEnterLaserCode();
}


function formatCitizenIDekyc() {

	if(frmMBeKYCIDInfo.txtIDNo.text.length == 0){
		frmMBeKYCIDInfo.txtIDNo.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
		frmMBeKYCIDInfo.txtIDNo.maxTextLength=17;
	}
	var temp = frmMBeKYCIDInfo.txtIDNo.text.substring(0,1);
// 	frmMBeKYCIDInfo.txtIDNo.skin = "txtNormalBG";
	
	if(kony.string.isNumeric(temp)) {
		//when ID no is used length is 13+4 hypens
		frmMBeKYCIDInfo.txtIDNo.maxTextLength=17;
		frmMBeKYCIDInfo.txtIDNo.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
		onEditEkycCitizenID(frmMBeKYCIDInfo.txtIDNo.text);
    }
}

function onEditEkycCitizenID(txt) {
	if (txt == null) return false;
	var noChars = txt.length;
	var temp = "";
	var i, txtLen = noChars;
	var currLen = noChars;
	
	if (gblPrevLen < currLen) {
		for (i = 0; i < noChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}		
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 0 || i == 4 || i == 9 || i == 11) {
				iphenText += '-';
			}
		}
			frmMBeKYCIDInfo.txtIDNo.text = iphenText;
	}
	gblPrevLen = currLen;
}

function enFirstnamevalidate(){ 
var letters = /^[a-zA-Z_@!#\$\^\-\ ]+$/;///^[0-9a-zA-Z]+$/;
// var englishAlphabetAndWhiteSpace = /[A-Za-z ]/g;
var textfirstname = frmMBeKYCIDInfo.txtName.text;
var txtlenth = textfirstname.length;
var prevtxtlenth = txtlenth - 1;
var prevprevtxtlenth = prevtxtlenth - 1;
var lastenteredchar = textfirstname.substring(prevtxtlenth, txtlenth);
var lastPreventeredchar = textfirstname.substring(prevprevtxtlenth, prevtxtlenth);
var Firstchar = textfirstname.substring(0, 1);
var Secondchar = textfirstname.substring(1, 2);
var txtvalidate = letters.test(lastenteredchar);
  if(lastenteredchar == " " || lastenteredchar == "-" || lastenteredchar == "" ){
    if(lastenteredchar == " "){
         if(textfirstname == " "){
         frmMBeKYCIDInfo.txtName.text = "";
        }else{
          if(lastPreventeredchar == "-"){
          var filteredtext = replaceAll(textfirstname, "- ", "-");
          frmMBeKYCIDInfo.txtName.text = filteredtext;
          }else if(lastPreventeredchar == " "){
           var filteredtext = replaceAll(textfirstname, "  ", " ");
          frmMBeKYCIDInfo.txtName.text = filteredtext;
          }
        }
    }else if(lastenteredchar == "-"){
         if(textfirstname == "-"){
         frmMBeKYCIDInfo.txtName.text = "";
        }else{
          if(lastPreventeredchar == "-"){
          var filteredtext = replaceAll(textfirstname, "--", "-");
          frmMBeKYCIDInfo.txtName.text = filteredtext;
          }else if(lastPreventeredchar == " "){
           var filteredtext = replaceAll(textfirstname, " -", "-");
          frmMBeKYCIDInfo.txtName.text = filteredtext;
          }
        }
    }
  }else{
        if(txtvalidate){ 
//       return true;
    }else{
       frmMBeKYCIDInfo.txtName.setFocus(false);
       showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectFirstName"),kony.i18n.getLocalizedString("info"),enFirstNameFocus);
//        return false;
    }
  }
   if(Firstchar == " "){
          frmMBeKYCIDInfo.txtName.text = textfirstname.trim();
        }else if(Firstchar == "-"){
          frmMBeKYCIDInfo.txtName.text = textfirstname.substring(1, txtlenth);
        }
    var Filteredfirstname = frmMBeKYCIDInfo.txtName.text;
    var stringcheck  = Filteredfirstname.indexOf("--");
    var stringcheck1 = Filteredfirstname.indexOf("  ");
    var stringcheck2 = Filteredfirstname.indexOf("- ");
    var stringcheck3 = Filteredfirstname.indexOf(" -");
  if(stringcheck !== -1 || stringcheck1 !== -1 || stringcheck2 !== -1 || stringcheck3 !== -1){
    showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectFirstName"),kony.i18n.getLocalizedString("info"),enFirstNameFocus);
  }
}

function enFirstnamevalidateondone(){
  var textfirstname = frmMBeKYCIDInfo.txtName.text;
  var txtlenth = textfirstname.length;
  var prevtxtlenth = txtlenth - 1;
  var prevprevtxtlenth = prevtxtlenth - 1;
  var lastenteredchar = textfirstname.substring(prevtxtlenth, txtlenth);
  var lastPreventeredchar = textfirstname.substring(prevprevtxtlenth, prevtxtlenth);
  var lasttwoletters = lastPreventeredchar + lastenteredchar;
  if(lastenteredchar == "-"){
    frmMBeKYCIDInfo.txtName.text = textfirstname.substring(0, prevtxtlenth);
    //showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectFirstName"),kony.i18n.getLocalizedString("info"),enFirstNameFocus);
  }else if(lastenteredchar == " "){
    frmMBeKYCIDInfo.txtName.text = textfirstname.trim();
  }
}

function enFirstNameFocus(){
  frmMBeKYCIDInfo.txtName.text = "";
  frmMBeKYCIDInfo.txtName.setFocus(true);
}

function enSurnamenamevalidate(){
  var letters = /^[a-zA-Z_@!#\$\^\-\ ]+$/;///^[0-9a-zA-Z]+$/;
// var englishAlphabetAndWhiteSpace = /[A-Za-z ]/g;
var textfirstname = frmMBeKYCIDInfo.txtLastName.text;
var txtlenth = textfirstname.length;
var prevtxtlenth = txtlenth - 1;
var prevprevtxtlenth = prevtxtlenth - 1;
var lastenteredchar = textfirstname.substring(prevtxtlenth, txtlenth);
var lastPreventeredchar = textfirstname.substring(prevprevtxtlenth, prevtxtlenth);
var Firstchar = textfirstname.substring(0, 1);
var txtvalidate = letters.test(lastenteredchar);
  if(lastenteredchar == " " || lastenteredchar == "-" || lastenteredchar == ""){
    if(lastenteredchar == " "){
         if(textfirstname == " "){
         frmMBeKYCIDInfo.txtLastName.text = "";
        }else{
          if(lastPreventeredchar == "-"){
          var filteredtext = replaceAll(textfirstname, "- ", "-");
          frmMBeKYCIDInfo.txtLastName.text = filteredtext;
          }
        }
    }else if(lastenteredchar == "-"){
         if(textfirstname == "-"){
         frmMBeKYCIDInfo.txtLastName.text = "";
        }else{
          if(lastPreventeredchar == " "){
           var filteredtext = replaceAll(textfirstname, " -", "-");
          frmMBeKYCIDInfo.txtLastName.text = filteredtext;
          }
        }
    }
  }else{
        if(txtvalidate){ 
//       return true;
    }else{
       frmMBeKYCIDInfo.txtLastName.setFocus(false);
       showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectLastName"),kony.i18n.getLocalizedString("info"),enSurNameFocus);
//        return false;
    }
  }
    if(Firstchar == " "){
          frmMBeKYCIDInfo.txtLastName.text = textfirstname.trim();
        }else if(Firstchar == "-"){
          frmMBeKYCIDInfo.txtLastName.text = textfirstname.substring(1, txtlenth);
        }
    var Filteredfirstname = frmMBeKYCIDInfo.txtLastName.text;
    var stringcheck =  Filteredfirstname.indexOf("--");
    var stringcheck1 = Filteredfirstname.indexOf("  ");
    var stringcheck2 = Filteredfirstname.indexOf("- ");
    var stringcheck3 = Filteredfirstname.indexOf(" -");
  if(stringcheck2 !== -1 || stringcheck3 !== -1){
    showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectLastName"),kony.i18n.getLocalizedString("info"),enSurNameFocus);
  }
}

function enSurnamenamevalidateondone(){
  var textfirstname = frmMBeKYCIDInfo.txtLastName.text;
  var txtlenth = textfirstname.length;
  var prevtxtlenth = txtlenth - 1;
  var prevprevtxtlenth = prevtxtlenth - 1;
  var lastenteredchar = textfirstname.substring(prevtxtlenth, txtlenth);
  var lastPreventeredchar = textfirstname.substring(prevprevtxtlenth, prevtxtlenth);
  var lasttwoletters = lastPreventeredchar + lastenteredchar;
  if(lastenteredchar == "-"){
    //showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectLastName"),kony.i18n.getLocalizedString("info"),enSurNameFocus);
    frmMBeKYCIDInfo.txtLastName.text = textfirstname.substring(0, prevtxtlenth);
  }else if(lastenteredchar == " "){
    frmMBeKYCIDInfo.txtLastName.text = textfirstname.trim();
  }
}

function enSurNameFocus(){
  frmMBeKYCIDInfo.txtLastName.text = "";
  frmMBeKYCIDInfo.txtLastName.setFocus(true);
}

function thFirstnamevalidate(){
  var letters = /^[a-zA-Z0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/;
  var textThaiFirstName = frmMBeKYCIDInfo.txtThaiFirstName.text;
  var txtlenth = textThaiFirstName.length;
  var prevtxtlenth = txtlenth - 1;
  var prevprevtxtlenth = prevtxtlenth - 1;
  var lastenteredchar = textThaiFirstName.substring(prevtxtlenth, txtlenth);
  var lastPreventeredchar = textThaiFirstName.substring(prevprevtxtlenth, prevtxtlenth);
  var Firstchar = textThaiFirstName.substring(0, 1);
  var txtvalidate = letters.test(lastenteredchar);
  if(lastenteredchar == " " || lastenteredchar == "-" || lastenteredchar == ""){
        if(lastenteredchar == " "){
         if(textThaiFirstName == " "){
         frmMBeKYCIDInfo.txtThaiFirstName.text = "";
        }else{
          if(lastPreventeredchar == " "){
           var filteredtext = replaceAll(textThaiFirstName, "  ", " ");
          frmMBeKYCIDInfo.txtThaiFirstName.text = filteredtext;
          }
        }
    }
  }else{
      if(!txtvalidate){
//     return true;
  }else{
    showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThFirstName"),kony.i18n.getLocalizedString("info"),thFirstNameFocus);
    frmMBeKYCIDInfo.txtThaiFirstName.text = "";
//     return false;
 }
  }
   if(Firstchar == " "){
          frmMBeKYCIDInfo.txtThaiFirstName.text = textThaiFirstName.trim();
   }
  var FilteredThaiFirstName = frmMBeKYCIDInfo.txtThaiFirstName.text;
  var stringcheck = FilteredThaiFirstName.indexOf("--");
  var stringcheck1 = FilteredThaiFirstName.indexOf("  ");
  var stringcheck2 = FilteredThaiFirstName.indexOf("- ");
  var stringcheck3 = FilteredThaiFirstName.indexOf(" -");
  if(stringcheck1 !== -1){
    showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThFirstName"),kony.i18n.getLocalizedString("info"),thFirstNameFocus);
  }

}

function thFirstnamevalidateondone(){
  var textThaiFirstName = frmMBeKYCIDInfo.txtThaiFirstName.text;
  var txtlenth = textThaiFirstName.length;
  var prevtxtlenth = txtlenth - 1;
  var prevprevtxtlenth = prevtxtlenth - 1;
  var lastenteredchar = textThaiFirstName.substring(prevtxtlenth, txtlenth);
  var lastPreventeredchar = textThaiFirstName.substring(prevprevtxtlenth, prevtxtlenth);
  if(lastenteredchar == " "){
    //showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThFirstName"),kony.i18n.getLocalizedString("info"),thFirstNameFocus);
    frmMBeKYCIDInfo.txtThaiFirstName.text = textThaiFirstName.trim();
  }
}

function thFirstNameFocus(){
  frmMBeKYCIDInfo.txtThaiFirstName.text = "";
  frmMBeKYCIDInfo.txtThaiFirstName.setFocus(true);
}

function thSurnamenamevalidate(){
  var letters =  /^[a-zA-Z0-9@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]+$/;
  var textThaiSurName = frmMBeKYCIDInfo.txtThaiLastName.text;
  var txtlenth = textThaiSurName.length;
  var prevtxtlenth = txtlenth - 1;
  var prevprevtxtlenth = prevtxtlenth - 1;
  var lastenteredchar = textThaiSurName.substring(prevtxtlenth, txtlenth);
  var lastPreventeredchar = textThaiSurName.substring(prevprevtxtlenth, prevtxtlenth);
  var Firstchar = textThaiSurName.substring(0, 1);
  var txtvalidate = letters.test(lastenteredchar);
  if(lastenteredchar == " " || lastenteredchar == "-" || lastenteredchar == ""){
        if(lastenteredchar == " "){
         if(textThaiSurName == " "){
         frmMBeKYCIDInfo.txtThaiLastName.text = "";
        }else{
          if(lastPreventeredchar == " "){
           var filteredtext = replaceAll(textThaiSurName, "  ", " ");
          frmMBeKYCIDInfo.txtThaiLastName.text = filteredtext;
          }
        }
    }    
  }else{
      if(!txtvalidate){
//      return true;
  }else{
   showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThLastName"),kony.i18n.getLocalizedString("info"),thSurNameFocus);
    frmMBeKYCIDInfo.txtThaiLastName.text = "";
//     return false;
 }
  }
   if(Firstchar == " "){
          frmMBeKYCIDInfo.txtThaiLastName.text = textThaiSurName.trim();
   }
  var FilteredThaiSurName = frmMBeKYCIDInfo.txtThaiLastName.text;
  var stringcheck = FilteredThaiSurName.indexOf("--");
  var stringcheck1 = FilteredThaiSurName.indexOf("  ");
  var stringcheck2 = FilteredThaiSurName.indexOf("- ");
  var stringcheck3 = FilteredThaiSurName.indexOf(" -");
  if(stringcheck1 !== -1){
    showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThLastName"),kony.i18n.getLocalizedString("info"),thSurNameFocus);
  }

}

function thSurnamenamevalidateondone(){
  var textThaiSurName = frmMBeKYCIDInfo.txtThaiLastName.text;
  var txtlenth = textThaiSurName.length;
  var prevtxtlenth = txtlenth - 1;
  var prevprevtxtlenth = prevtxtlenth - 1;
  var lastenteredchar = textThaiSurName.substring(prevtxtlenth, txtlenth);
  var lastPreventeredchar = textThaiSurName.substring(prevprevtxtlenth, prevtxtlenth);
  if(lastenteredchar == " "){
    //showAlertWithCallBack(getLocalizedString("eKYC_msgIncorrectThLastName"),kony.i18n.getLocalizedString("info"),thSurNameFocus);
    frmMBeKYCIDInfo.txtThaiLastName.text = textThaiSurName.trim();
  }
}

function thSurNameFocus(){
  frmMBeKYCIDInfo.txtThaiLastName.text = "";
  frmMBeKYCIDInfo.txtThaiLastName.setFocus(true);
}



