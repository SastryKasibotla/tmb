//Type your code here

function loadAccntProdImages(iconID)
{
  	var imageNameChnd="";
    var imageNameOrg=iconID;
	imageNameChnd=imageNameOrg.toLowerCase();
  	if(imageNameChnd=="ba_prod_logo"||imageNameChnd=="mf_product"){
        imageNameChnd=imageNameChnd+".png";
        //kony.print("^^^^^^The From Imagename mf utility is^^^^^^"+imageNameChnd);
	}else{
    	imageNameChnd=imageNameChnd.replace("-","_")+".png";
    	//kony.print("^^^^^^The From Imagename utility is^^^^^^"+imageNameChnd);
    }
	return imageNameChnd;  
}

function loadAccntProdImagesdetails(iconID)
{
  	var imageNameChnd="";
    var imageNameOrg=iconID;
	imageNameChnd=imageNameOrg.toLowerCase();
  	imageNameChnd=imageNameChnd.replace("-","_")+".png";
    kony.print("^^^^^^The Accnt details From Imagename utility is^^^^^^"+imageNameChnd);
    return imageNameChnd;  
}


function loadFromPalleteIcons(iconID)
{
  	var imageNameChnd="";
    var imageNameOrg=iconID;
	imageNameChnd=imageNameOrg.toLowerCase();
	imageNameChnd="new_"+imageNameChnd.replace("-","_")+".png";
  	kony.print("^^^^^^The Transfers Imagename utility is^^^^^^"+imageNameChnd);
    return imageNameChnd;  
}

function loadBillerIcons(iconID)
{
  	var imageNameChnd="";
    var imageNameOrg=iconID;
  	if(imageNameOrg=="CC01"||imageNameOrg=="AL02"||imageNameOrg=="AL01"||imageNameOrg=="AL03"){
      kony.print("$$$$$ Inside condition $$$$$$"+imageNameOrg);
      imageNameOrg="0699";
    }
	imageNameChnd="biller_"+imageNameOrg+".png";
    kony.print("$$$$$ Biller Utility is $$$$$$"+imageNameChnd);
    return imageNameChnd;
}

function loadBankIcon(iconID)
{
  	var imageNameChnd="";
    var imageNameOrg=iconID;
	imageNameChnd=imageNameOrg.toLowerCase();
	imageNameChnd="bank_logo_"+imageNameChnd+".png";
  	kony.print("@@@@@@The BANKICON Imagename utility is@@@@@@"+imageNameChnd);
    return imageNameChnd;  
}


function loadMFIcon(fundhouse)
{
  	var imageNameChnd="";
    var imageNameOrg=fundhouse;
	imageNameChnd=imageNameOrg.toLowerCase();
	imageNameChnd="mf_"+imageNameChnd+".png";
  	kony.print("@@@@@@The mutual Imagename utility is@@@@@@"+imageNameChnd);
    return imageNameChnd;  
}

//card icon loading
function getImageSRC(imageName) {
	//var randomnum = Math.floor((Math.random() * 10000) + 1);
	//var screendpiservice = "";
	//getDeviceDpi();
	//var card_image_url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext 
	//+ "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+imageName
	//+screendpiservice+"&modIdentifier=CARDPRODUCTIMG&dummy=" + randomnum;
  	var imageNameChnd="";
    var imageNameOrg=imageName;
	card_image_url=imageNameOrg.toLowerCase();
	card_image_url="cards_"+card_image_url+".png";
  	kony.print("@@@@@@The cards Imagename utility is@@@@@@"+card_image_url);
	return card_image_url;
}

//BG Image for Account Summary for Special Occasion
function getSpecialOccasionImageForAS() {
  try{
    var locale = "";
    if (kony.i18n.getCurrentLocale() == "th_TH"){
      locale = "_TH";
    }else{
      locale = "_EN";
    }      
    var as_special_occasion_bg_img_name = "occasion"+locale;
    var randomnum = Math.floor((Math.random()*10000)+1);
    var as_special_occasion_bg_img = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+as_special_occasion_bg_img_name+"&modIdentifier=PRODUCTPACKAGEIMG&rr="+randomnum;

    kony.print("@@@ Account Summary BG Image:::"+as_special_occasion_bg_img);
    return as_special_occasion_bg_img;
  }catch(e){
    kony.print("@@@ In getSpecialOccasionImageForAS() Exception:::"+e);
  }
}