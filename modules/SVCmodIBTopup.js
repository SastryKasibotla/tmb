function getFeeForTopUpIB() {
	var tranCode;
	if (frmIBTopUpLandingPage.lblAcctType.text == "DDA") {
		tranCode = "88" + "10";
	} else {
		tranCode = "88" + "20";
	}
	var fromAcctNo = removeHyphenIB(frmIBTopUpLandingPage.lblAccountNo.text);
	var fromAccType;
	if (fromAcctNo.length == 14) {
		fromAccType = "SDA";
	} else {
		fourthDigit = fromAcctNo.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAccType = "SDA";
			fromAcctNo = "0000" + fromAcctNo;
		} else {
			fromAccType = "DDA";
		}
	}
	var transferAmount;
	var fromAccount = fromAcctNo;
	if (frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.isVisible == true) {
		transferAmount = removeCommos(frmIBTopUpLandingPage.txtAmount.text);
	} else {
		transferAmount = removeCommos(frmIBTopUpLandingPage.comboAmount.selectedKeyValue[1]);
	}
	var text = frmIBTopUpLandingPage.lblCompCode.text;
	var compcode = gblCompCode;
	var date = changeDateFormatForService(frmIBTopUpLandingPage.lblPayBillOnValue.text);
	var inputParams = {
		tranCode: tranCode,
		fromAcctIdentValue: fromAccount,
		fromAcctTypeValue: fromAccType,
		transferAmount: transferAmount,
		pmtRefIdent: frmIBTopUpLandingPage.txtAddBillerRef1.text,
		postedDate: date,
		invoiceNumber: "",
		ePayCode: "",
		waiveCode: "I",
		compCode: compcode,
		fIIdent: frmIBTopUpLandingPage.lblDummy.text
	};
	invokeServiceSecureAsync("billPaymentInquiry", inputParams, getFeeForTopUpIBCallBack);
}

function getFeeForTopUpIBCallBack(status, resultable) {
	
	if (status == 400) {
		
		if (resultable["opstatus"] == "0") {
			if (resultable["StatusCode"] == "0") {
				var responseData = resultable["BillPmtInqRs"];
				if (responseData.length > 0) {
					for (var i = 0; i < resultable["BillPmtInqRs"].length; i++) {
						var FeeAmnt = resultable["BillPmtInqRs"][i]["FeeAmnt"];
						
						if (resultable["BillPmtInqRs"][i]["WaiveProductCode"] == "Y") {
							frmIBTopUpConfirmation.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
						} else {
							frmIBTopUpConfirmation.lblFeeVal.text = resultable["BillPmtInqRs"][i]["FeeAmnt"] + " " + kony.i18n.getLocalizedString(
								"currencyThaiBaht");
						}
						//frmIBTopUpConfirmation.lblTRNVal.text = resultable["BillPmtInqRs"][i]["TranCode"];
						// alert("FeeAmt is:" + FeeAmnt);
						if (gblBillerMethod == 1) {
							// onlinePaymentIBTopup(); Doubt? called earlier
						}
						checkTopUpCrmProfileInqIB();
						
					}
				}
			} else {
				dismissLoadingScreenPopup();
				alert(" " + resultable["errMsg"]);
			}
		}
	}
}

function populateTopUpConfirmation() {
 
   if(gblPaynow && gblBillpaymentNoFee){
		frmIBTopUpConfirmation.hbxFreeTrans.setVisibility(true);
		frmIBTopUpConfirmation.lblFreeTran.text = frmIBTopUpLandingPage.lblFreeTran.text
		frmIBTopUpConfirmation.lblFreeTransValue.text = frmIBTopUpLandingPage.lblFreeTransValue.text
	} else {
		frmIBTopUpConfirmation.hbxFreeTrans.setVisibility(false);
	}
	
	frmIBTopUpConfirmation.lblAccountNo.text = frmIBTopUpLandingPage.lblAccountNo.text;
	frmIBTopUpConfirmation.lblName.text = frmIBTopUpLandingPage.lblName.text;
	
	frmIBTopUpConfirmation.lblBeforBal.text = frmIBTopUpLandingPage.lblBalanceValue.text;
	frmIBTopUpConfirmation.lblNicknameTo.text = frmIBTopUpLandingPage.lblNickTopUp.text;
	frmIBTopUpConfirmation.lblCompCodeTo.text = frmIBTopUpLandingPage.lblCompCode.text;
	frmIBTopUpConfirmation.lblCompCodeTo.isVisible=false; // showing only 1 label for Nickname/Biller name compcode
	frmIBTopUpConfirmation.lblRef1.text = frmIBTopUpLandingPage.ref1.text;
	//frmIBTopUpConfirmation.lblRef1Value.text = frmIBTopUpLandingPage.ref1value.text;
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	frmIBTopUpConfirmation.lblMNVal.text = replaceHtmlTagChars(frmIBTopUpLandingPage.textarea210136690395040.text);
	//frmIBTopUpConfirmation.lblTransferVal.text = currentSystemDate();
	frmIBTopUpConfirmation.imgAcctLogo.src = frmIBTopUpLandingPage.imgFrom.src;
	frmIBTopUpConfirmation.imgbiller.src =frmIBTopUpLandingPage.imgTopUp.src;
	
	if(gblEasyPassCustomer){
	frmIBTopUpConfirmation.hbxEasyPassName.setVisibility(true);
	frmIBTopUpConfirmation.hbxTopUpRef.setVisibility(true);
	
	frmIBTopUpConfirmation.lblEasyPassName.text = easyPassNameEN;
	}
	else {
	frmIBTopUpConfirmation.hbxEasyPassName.setVisibility(false)
	frmIBTopUpConfirmation.hbxTopUpRef.setVisibility(false);
	}
	
	if (gblPaynow) {
		//make schedule fields invisible
		frmIBTopUpConfirmation.hbxScheduleDetails.setVisibility(false);
		frmIBTopUpConfirmation.hbxBalance.setVisibility(true);
	} else {
			frmIBTopUpConfirmation.hbxBalance.setVisibility(false);
			frmIBTopUpConfirmation.hbxScheduleDetails.setVisibility(true);
			var dates = frmIBTopUpLandingPage.lblDatesFuture.text.split(kony.i18n.getLocalizedString("keyTo"));
			var firstDate = dates[0].trim();
			var secondDate;// = dates[1].trim();
			if(dates[1] == null || dates[1] == "" || dates[1] == undefined) {
				secondDate = "-";
			}else {
				secondDate = dates[1].trim()
			}
			if(gblTimes=="-1"){
					gblTimes = "-";
			}		
			frmIBTopUpConfirmation.lblStartOnValue.text = firstDate;
			
			if (gblClicked == "Daily") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyDaily");
			} else if (gblClicked == "Weekly") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyWeekly");
			} else if (gblClicked == "Monthly") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyMonthly");
			} else if (gblClicked == "Yearly") {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyYearly");
			} else {
            frmIBTopUpConfirmation.lblRepeatValue.text = kony.i18n.getLocalizedString("keyOnce");
			}
			frmIBTopUpConfirmation.lblEnDONValue.text = secondDate;
			
			
			
			if(gblClicked=="once") {
				frmIBTopUpConfirmation.lblExecuteValue.text = "1";
			} else {

				frmIBTopUpConfirmation.lblExecuteValue.text = gblTimes;
			}
			 
	}
	
	if (frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.isVisible == true) {
		//frmIBTopUpConfirmation.lblAmtVal.text = commaFormatted(parseFloat(frmIBTopUpLandingPage.txtAmount.text).toFixed(2))+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	} else {
		//frmIBTopUpConfirmation.lblAmtVal.text = commaFormatted(frmIBTopUpLandingPage.comboAmount.selectedKeyValue[1]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	}
	var locale =kony.i18n.getCurrentLocale();
	/*if(locale == "en_US"){
		frmIBTopUpConfirmation.lblFromAccountName.text = gblCustomerName;	
	} else {
		frmIBTopUpConfirmation.lblFromAccountName.text = gblCustomerNameTh;
	}*/
	if(billPayTopup)
	{
	frmIBTopUpConfirmation.lblFromAccountName.text = gblAccountTable["custAcctRec"][gblIndex]["accountName"] ;
	}
	else
	{
	frmIBTopUpConfirmation.lblFromAccountName.text = accntTopupName;
	}
	gblEditTopUp=false;
	frmIBTopUpConfirmation.show();
	dismissLoadingScreenPopup();
}

function segBillerRowClickIB() {
	gblEasyPassCustomer = false;
	frmIBTopUpLandingPage.txtScheduleNickname.text="";
	frmIBTopUpLandingPage.txtAmount.text="";
	hideShowScheduleNicknameTopUpIB(false);
	var indexOfSelectedRow = frmIBTopUpLandingPage.segBiller.selectedIndex[1];
	indexOfSelectedIndex = frmIBTopUpLandingPage.segBiller.selectedItems[0];
	frmIBTopUpLandingPage.imgTopUp.isVisible = true;
	frmIBTopUpLandingPage.imgTopUp.setVisibility(true);
	frmIBTopUpLandingPage.vboxTo.isVisible = true;
	frmIBTopUpLandingPage.hbxRef1TopUpScreen.isVisible = true;
	var billerMethod = indexOfSelectedIndex.billerMethod;
	gblBillerMethod = billerMethod;
	gblCompCode = indexOfSelectedIndex.Compcode;
	gblToAccountKey = indexOfSelectedIndex.ToAccountKey;
	gblBillerID = indexOfSelectedIndex.CustomerBillID;
	gblbillerGroupType = indexOfSelectedIndex.billerGroupType;
	
	if(gblCompCode =="2151" ){
		gblEasyPassCustomer = true;
		
	}
	else {
			
			gblEasyPassCustomer = false;
	}
	
	var dataList = frmIBTopUpLandingPage.segBiller.data[indexOfSelectedRow];
	frmIBTopUpLandingPage.imgTopUp.src = indexOfSelectedIndex.imgBillerpic.src;
	frmIBTopUpLandingPage.lblNickTopUp.text = indexOfSelectedIndex.lblBillerName;
	frmIBTopUpLandingPage.lblCompCode.text = indexOfSelectedIndex.BillerCompCode;
	frmIBTopUpLandingPage.lblCompCode.isVisible=false;  //hiding this label for all case now as only one field having nickname or compcode biller name is shown
	frmIBTopUpLandingPage.ref1.text = indexOfSelectedIndex.lblRef1;
	frmIBTopUpLandingPage.ref1value.text = indexOfSelectedIndex.lblRef1Value;
	//below line is changed for CR - PCI-DSS masked Credit card no
	frmIBTopUpLandingPage.ref1valueMasked.text = indexOfSelectedIndex.lblRef1ValueMasked;
	frmIBTopUpLandingPage.txtAddBillerRef1.text= indexOfSelectedIndex.lblRef1Value;
	gblRef1LblEN = indexOfSelectedIndex.lblRef1EN;
	gblRef1LblTH = indexOfSelectedIndex.lblRef1TH;
	gblRef2LblEN = indexOfSelectedIndex.lblRef2;
	gblRef2LblTH = indexOfSelectedIndex.lblRef2TH;
	gblBillerCompCodeEN = indexOfSelectedIndex.BillerCompCodeEN;
	gblBillerCompCodeTH = indexOfSelectedIndex.BillerCompCodeTH;
	
	gblreccuringDisableAdd = indexOfSelectedIndex.IsRequiredRefNumber2Add
	gblreccuringDisablePay = indexOfSelectedIndex.IsRequiredRefNumber2Pay
	gblBillerMethod = parseInt(indexOfSelectedIndex.billerMethod);
	var groupType = indexOfSelectedIndex.billerGroupType;
	var ref1MaxTextLength = "0";
    for(var i = 0; i < MySelectBillSuggestListRs.length; i++) {
    	if(MySelectBillSuggestListRs[i]["BillerCompCode"].trim() == gblCompCode.trim()) {
    		ref1MaxTextLength = undefined != MySelectBillSuggestListRs[i]["Ref1Len"]?MySelectBillSuggestListRs[i]["Ref1Len"]:"0";
    		break;
    	}
    }
	frmIBTopUpLandingPage.txtAddBillerRef1.maxTextLength = parseInt(ref1MaxTextLength);
	if (billerMethod == 0) { //Offline Biller
		mapOfflineTopUp();
	} else if (billerMethod == 1) { //Online Biller
		mapOnlineTopUp();
	} else if (billerMethod == 2) { // TMB Credit Card/Ready Cash
		mapTMBCreditCardTopUp();
	} else if (billerMethod == 3) { //TMB Loan
		mapTMBLoanTopUp();
	} else if (billerMethod == 4) { //Ready Cash
		mapReadyCashTopUp();
	}
	frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
	frmIBTopUpLandingPage.hbxImage.setVisibility(true);
}

function segSuggestedTopUpBillerRowClickIB() {
	gblEasyPassCustomer = false;
	frmIBTopUpLandingPage.txtScheduleNickname.text="";
	frmIBTopUpLandingPage.txtAmount.text="";
	hideShowScheduleNicknameTopUpIB(false);
	var indexOfSelectedRow = frmIBTopUpLandingPage.segSuggestedBiller.selectedIndex[1];
	indexOfSelectedIndex = frmIBTopUpLandingPage.segSuggestedBiller.selectedItems[0];
	frmIBTopUpLandingPage.imgTopUp.isVisible = true;
	frmIBTopUpLandingPage.imgTopUp.setVisibility(true);
	frmIBTopUpLandingPage.vboxTo.isVisible = true;
	frmIBTopUpLandingPage.hbxRef1TopUpScreen.isVisible = true;
	var billerMethod = indexOfSelectedIndex.BillerMethod;
	gblBillerMethod = billerMethod;
	gblCompCode = indexOfSelectedIndex.BillerCompCode;
	gblToAccountKey = indexOfSelectedIndex.ToAccountKey;
	gblBillerID = "";//indexOfSelectedIndex.CustomerBillID;  --only for added billers
	gblbillerGroupType = indexOfSelectedIndex.BillerGroupType;
	
	if(gblCompCode =="2151" ){
		gblEasyPassCustomer = true;
		
	}
	else {
			
			gblEasyPassCustomer = false;
	}
	
	var dataList = frmIBTopUpLandingPage.segSuggestedBiller.data[indexOfSelectedRow];
	frmIBTopUpLandingPage.imgTopUp.src = indexOfSelectedIndex.imgSuggestedBiller.src;
	frmIBTopUpLandingPage.lblNickTopUp.text = indexOfSelectedIndex.lblSuggestedBiller;// only for Suggested Billers
	frmIBTopUpLandingPage.lblCompCode.text = indexOfSelectedIndex.lblSuggestedBiller;
	frmIBTopUpLandingPage.lblCompCode.isVisible=false; //hiding this label for all case now as only one field having nickname or compcode biller name is shown
	frmIBTopUpLandingPage.ref1.text = appendColon(indexOfSelectedIndex.Ref1Label);
	frmIBTopUpLandingPage.ref1value.text = "";
	//below line is changed for CR - PCI-DSS masked Credit card no
	frmIBTopUpLandingPage.ref1valueMasked.text = "";
	frmIBTopUpLandingPage.txtAddBillerRef1.text="";
	gblRef1LblEN = indexOfSelectedIndex.Ref1EN;
	gblRef1LblTH = indexOfSelectedIndex.Ref1TH;
	gblRef2LblEN = "";//indexOfSelectedIndex.lblRef2;  --not sure if we need ref2 for Top-Up
	gblRef2LblTH = "";//indexOfSelectedIndex.lblRef2TH;  --not sure if we need ref2 for Top-Up
	gblBillerCompCodeEN = indexOfSelectedIndex.lblSuggestedBillerEN;
	gblBillerCompCodeTH = indexOfSelectedIndex.lblSuggestedBillerTH;
	
	gblreccuringDisableAdd = indexOfSelectedIndex.IsRequiredRefNumber2Add
	gblreccuringDisablePay = indexOfSelectedIndex.IsRequiredRefNumber2Pay
	gblBillerMethod = parseInt(indexOfSelectedIndex.BillerMethod);
	var groupType = indexOfSelectedIndex.BillerGroupType;
	var ref1MaxTextLength = "0";
    for(var i = 0; i < MySelectBillSuggestListRs.length; i++) {
    	if(MySelectBillSuggestListRs[i]["BillerCompCode"].trim() == gblCompCode.trim()) {
    		ref1MaxTextLength = undefined != MySelectBillSuggestListRs[i]["Ref1Len"]?MySelectBillSuggestListRs[i]["Ref1Len"]:"0";
    		break;
    	}
    }
	frmIBTopUpLandingPage.txtAddBillerRef1.maxTextLength = parseInt(ref1MaxTextLength);
	if (billerMethod == 0) { //Offline Biller
		mapOfflineTopUp();
	} else if (billerMethod == 1) { //Online Biller
		mapOnlineTopUp();
	} else if (billerMethod == 2) { // TMB Credit Card/Ready Cash
		mapTMBCreditCardTopUp();
	} else if (billerMethod == 3) { //TMB Loan
		mapTMBLoanTopUp();
	} else if (billerMethod == 4) { //Ready Cash
		mapReadyCashTopUp();
	}
	frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
	frmIBTopUpLandingPage.hbxImage.setVisibility(true);
}

function IBverifyOTPTopUp() {


	var inputParam = {};
	if(gblPaynow){
		inputParam["gblPaynow"] = "true";
	}else{
		inputParam["gblPaynow"] = "false";
	}
	inputParam["channel"] = "IB";
	inputParam["topUpCompCode"] = gblCompCode;
	inputParam["addBill_BillerNickName"] = frmIBTopUpLandingPage.txtScheduleNickname.text;
	setTopUpServiceInputParams(inputParam); //JC
	setTopUpCrmProfileUpdateService(inputParam)//JC
	setNotificationInputParams(inputParam);//JC
	setActivitLoggingInputParams(inputParam); //JC
	setInputParamsFinancialActivityLogForTopup(frmIBTopUpConfirmation.lblTRNVal.text, frmIBTopUpConfirmation.lblAmtVal.text,frmIBTopUpConfirmation.lblFeeVal.text, "01", inputParam);

	
	if(gblTokenSwitchFlag == true ) {
	
		if(frmIBTopUpConfirmation.tbxToken.text == "" || frmIBTopUpConfirmation.tbxToken.text == " ") {
   				dismissLoadingScreenPopup();
   				alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
  	 			return false;
   		}else{
   			  inputParam["gblTokenSwitchFlag"] = "true";	
			  inputParam["verifyTokenEx_loginModuleId"] = "IB_HWTKN";
		      inputParam["verifyTokenEx_userStoreId"] = "DefaultStore";
		      inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
		      inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
		      inputParam["verifyTokenEx_userId"] = gblUserName;
		      
		      inputParam["verifyTokenEx_password"] = frmIBTopUpConfirmation.tbxToken.text;
		      
		      inputParam["verifyTokenEx_sessionVal"] = "";
		      inputParam["verifyTokenEx_segmentId"] = "segmentId";
		      inputParam["verifyTokenEx_segmentIdVal"] = "MIB";
		      inputParam["verifyTokenEx_channel"] = "InterNet Banking";
		      
				showLoadingScreenPopup();
				frmIBTopUpConfirmation.tbxToken.text="";
		      //invokeServiceSecureAsync("verifyTokenEx", inputParam, callBackIBverifyOTPTopUp); //JC
				invokeServiceSecureAsync("TopupBillPaymentCompositeService", inputParam,callBackIBverifyOTPTopUp); //JC
		      	
		}      
	}
   else{
   		if(frmIBTopUpConfirmation.txtOtp.text == "" || frmIBTopUpConfirmation.txtOtp.text == " ") {
   			dismissLoadingScreenPopup();
   			alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
  	 		return false;
   		}else{
   			inputParam["gblTokenSwitchFlag"] = "false";
			inputParam["verifyPasswordEx_serviceID"] = "verifyOTP";
			inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
			inputParam["verifyPasswordEx_password"] = frmIBTopUpConfirmation.txtOtp.text;
			//inputParam["otp"] = frmIBTopUpConfirmation.txtOtp.text;
			inputParam["verifyPasswordEx_userId"] = gblUserName;
		  	
		  	showLoadingScreenPopup();
		  	frmIBTopUpConfirmation.txtOtp.text="";
		  	//invokeServiceSecureAsync("verifyPasswordEx", inputParam, callBackIBverifyOTPTopUp);
			invokeServiceSecureAsync("TopupBillPaymentCompositeService", inputParam,callBackIBverifyOTPTopUp); //JC
		  	
  		}	  
	}
	
	//not working at present time
	// invokeServiceSecureAsync("verifyOTP", inputParam, callBackIBverifyOTPTopUp)
	
	
	
}


function callBackIBverifyOTPTopUp(status, resulttable) {
	if (status == 400) {
		dismissLoadingScreenPopup();
		if (resulttable["opstatus"] == 0) {
			frmIBTopUpComplete.hbox101450027629826.setVisibility(true);
			//  if (resulttable["tokenStatus"] == "Activated" && resulttable["showPinPwdCount"] != 0) {
			if (resulttable["showPinPwdCount"] != 0) {
				
				var billerMethod = gblBillerMethod;
				//if(billerMethod == "2") //JAI CHECK
					//frmIBTopUpComplete.lblCardBalance.text = commaFormatted(parseFloat(resulttable["OnlinePmtAddRs_Ref4"]).toFixed(2)) + "" + kony.i18n.getLocalizedString("currencyThaiBaht");
				frmIBTopUpComplete.lblBBPaymentValue.text = commaFormatted(resulttable["availBal"])+ kony.i18n.getLocalizedString("currencyThaiBaht");
				if (!gblPaynow)  
					gblCurrentBillPayTime = resulttable["currentTime"];
				else
					frmIBTopUpComplete.lblePayDate.text = resulttable["paymentServerDate"];
					
				gblRetryCountRequestOTP = 0;
				if(gblPaynow && gblBillpaymentNoFee){
					frmIBTopUpComplete.hbxFreeTrans.setVisibility(true);
					frmIBTopUpComplete.lblFreeTran.text = frmIBTopUpConfirmation.lblFreeTran.text;
					frmIBTopUpComplete.lblFreeTransValue.text = "";
					if(resulttable["RemainingFee"]!=undefined && resulttable["RemainingFee"]!=null)	
					frmIBTopUpComplete.lblFreeTransValue.text = resulttable["RemainingFee"];
				}
				else{
					frmIBTopUpComplete.hbxFreeTrans.setVisibility(false);
				}
				populateTopUpCompletescreen();
			}
			}
		 else if (resulttable["opstatus"] == 8005) {
		 frmIBTopUpConfirmation.txtOtp.text = "";
		 dismissLoadingScreenPopup();
			if (resulttable["errCode"] == "VrfyOTPErr00001") {
				gblRetryCountRequestOTP = resulttable["retryCounterVerifyOTP"];
				dismissLoadingScreenPopup();
				//alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commeted by swapna
				    frmIBTopUpConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBTopUpConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBTopUpConfirmation.hbxOTPincurrect.isVisible = true;
                    frmIBTopUpConfirmation.hbxRefNo.isVisible = false;
                    frmIBTopUpConfirmation.hbxOTPsnt.isVisible = false;
                    frmIBTopUpConfirmation.txtOtp.text = "";
                    frmIBTopUpConfirmation.tbxToken.text ="" ;
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
                      frmIBTopUpConfirmation.tbxToken.setFocus(true);
                    }else{
                       frmIBTopUpConfirmation.txtOtp.setFocus(true);
                    }
                    
                    
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00002") {
				dismissLoadingScreenPopup();
				//alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
				//startRcCrmUpdateProfilBPIB("04");
				handleOTPLockedIB(resulttable);
				return false;
			}else if (resulttable["errCode"] == "VrfyOTPErr00005") {
				dismissLoadingScreenPopup();
				//alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                //alert("" + kony.i18n.getLocalizedString("invalidOTP"));
				    frmIBTopUpConfirmation.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBTopUpConfirmation.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");; 
                    frmIBTopUpConfirmation.hbxOTPincurrect.isVisible = true;
                    frmIBTopUpConfirmation.hbxRefNo.isVisible = false;
                    frmIBTopUpConfirmation.hbxOTPsnt.isVisible = false;
                    frmIBTopUpConfirmation.txtOtp.text = "";
                    frmIBTopUpConfirmation.tbxToken.text =" " ;
                    if(gblTokenSwitchFlag == true && gblSwitchToken == false){
                      frmIBTopUpConfirmation.tbxToken.setFocus(true);
                    }else{
                      frmIBTopUpConfirmation.txtOtp.setFocus(true);
                    }
                      
                      
				return false;
			}else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	} else if (resulttable["errCode"] == "VrfyOTPErr00006") {
				gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
				alert("" + resulttable["errMsg"]);
				return false;
			
			}else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreenPopup();
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
                    frmIBTopUpConfirmation.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBTopUpConfirmation.lblPlsReEnter.text = " "; 
                    frmIBTopUpConfirmation.hbxOTPincurrect.isVisible = false;
                    frmIBTopUpConfirmation.hbxRefNo.isVisible = true;
                    frmIBTopUpConfirmation.hbxOTPsnt.isVisible = true;
                    
                    
             	dismissLoadingScreenPopup();
             	frmIBTopUpConfirmation.txtOtp.setFocus(true);
                    frmIBTopUpConfirmation.tbxToken.setFocus(true);
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
		} else if (resulttable["opstatus"] == 9000 || resulttable["opstatus"] == 1){
			dismissLoadingScreenPopup();
			frmIBTopUpComplete.hbox101450027629826.setVisibility(false);
			if (resulttable["errCode"] == "ERRDUPTR0001") {
            	showAlertWithHandler(kony.i18n.getLocalizedString("keyErrDuplicatemt"), kony.i18n.getLocalizedString("info"), topupExecutionErrCallBackIB);
            	return;
            }
			if(resulttable["opstatus"] == 1) {
				if (resulttable["errCode"] == "MAXBILLERS") {
            		alert(kony.i18n.getLocalizedString("MIB_BPErrMaxBiller"));
           		} else {  
					alert("" + resulttable["errMsg"]);
				}
			} else {
				alert("" + kony.i18n.getLocalizedString("keyErrResponseOne"));
			}
			
			if(resulttable["isServiceFailed"] == "true") {
				callTopUpPaymentCustomerAccountServiceIB();
			}
		} else {
			dismissLoadingScreenPopup();
			frmIBTopUpComplete.hbox101450027629826.setVisibility(false);
			alert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + resulttable["errMsg"]);
		}
	}
}
function topupExecutionErrCallBackIB(){
	callTopUpPaymentCustomerAccountServiceIB();
}


/*function callTopupAddServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		dismissLoadingScreenPopup();
		
		
		if (resulttable["opstatus"] == 0) {
			
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			dismissLoadingScreen();
			
			if (StatusCode != "0") {
				alert(" " + resulttable["errMsg"])
				return false;
			} else {
				callTopUpCrmProfileUpdateService();
			}
		}
	}
}*/


/*function topupOnlinePaymentAddServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		dismissLoadingScreenPopup();
		
		
		if (resulttable["opstatus"] == 0) {
			
			// fetching To Account Name for TMB Inq
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			
			
			if (StatusCode != "0") {
				alert(" " + resulttable["errMsg"])
				return false;
			} else {
				frmIBTopUpComplete.lblCardBalance.text = commaFormatted(parseFloat(resulttable["OnlinePmtAddRs"][0]["Ref4"]).toFixed(2)) + "" + kony.i18n.getLocalizedString("currencyThaiBaht");
				callTopUpCrmProfileUpdateService();
			}
		}
	}
}*/

/*
 * CallBack of BillPaymentAddService
 *
 */

/*function callTopUpTransferAddServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		dismissLoadingScreenPopup();
		
		
		if (resulttable["opstatus"] == 0) {
			
			// fetching To Account Name for TMB Inq
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			dismissLoadingScreen();
			
			if (StatusCode != "0") {
				alert(" " + resulttable["errMsg"])
				return false;
			} else {
				callTopUpCrmProfileUpdateService();
			}
		}
	}
}*/

/*function callTopUpCrmProfileUpdateService() {
	inputParam = {};
	inputParam["actionType"]="0";
	if(gblPaynow){
		
		var usageLimit = UsageLimit + parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)) + parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text));
		
		inputParam["ebAccuUsgAmtDaily"] = usageLimit;
	}
	else {
		var usageLimit = UsageLimit ;//- parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)) - parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text));
		inputParam["ebAccuUsgAmtDaily"] = usageLimit;
	}
	//var usageLimit = UsageLimit - parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)) - parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text));
	//inputParam["ebAccuUsgAmtDaily"] = usageLimit;
	showLoadingScreenPopup();
	invokeServiceSecureAsync("crmProfileMod", inputParam, callTopUpCrmProfileUpdateServiceCallBack);
}*/
/*
 * CallBack of callTopUpCrmProfileUpdateService
 *
 */

/*function callTopUpCrmProfileUpdateServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			dismissLoadingScreenPopup();
			gblUserLockStatusIB = resulttable["IBUserStatusID"];
			var activityTypeID = "";
			var errorCode = "";
			var activityStatus = "";
			var deviceNickName = "";
			var activityFlexValues5 = "";
			var billerName=frmIBTopUpConfirmation.lblCompCodeTo.text;
			billerName=billerName.substring(0, billerName.indexOf("("));
			var activityFlexValues1 = billerName;
			var activityFlexValues2 = frmIBTopUpConfirmation.lblAccountNo.text;
			var activityFlexValues3 = frmIBTopUpConfirmation.lblRef1Value.text;
			var amt = parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)).toFixed(2);
			var fee = parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text)).toFixed(2);
			var activityFlexValues4 = amt + "+" + fee;
			var logLinkageId = "";
			var flag = false;
			if (gblPaynow) {
				activityTypeID = "030";
			} else {
				activityTypeID = "031";
			}
			
			// fetching To Account Name for TMB Inq
			var StatusCode = resulttable["StatusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			activityStatus = "01";
			
			populateTopUpCompletescreen();
			callTopUpNotificationAddService(); //Not Working currently
			//TODO: remove populateTopUpCompletescreen and add callTopUpNotificationAddService when the latter is working
		} else {
			activityStatus = "02";
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1,
			activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
			if(gblPaynow){
				setFinancialActivityLogForTopup(frmIBTopUpConfirmation.lblTRNVal.text, frmIBTopUpConfirmation.lblAmtVal.text,
			frmIBTopUpConfirmation.lblFeeVal.text, activityStatus);			
			}
			
		
	}
}*/

/*function callTopUpNotificationAddService() {
	var inputParams = {};
    platformChannel =  gblDeviceInfo.name;
    if (platformChannel == "thinclient")
        inputParams["channelId"] = "Internet Banking";
    else
        inputParams["channelId"] = "Mobile Banking";

	if(gblPaynow){
	inputParams["source"]="billpaymentnow";
	}
	else{
	inputParams["source"]="FutureBillPaymentAndTopUp";
	
	inputParams["initiationDt"] = frmIBTopUpComplete.lblStartOnValue.text; 
	inputParams["todayTime"] = "11:59:59 PM"; 
	inputParams["recurring"] = frmIBTopUpComplete.lblRepeatValue.text 
	inputParams["endDt"] = frmIBTopUpComplete.lblEnDONValue.text 
	inputParams["mynote"] = frmIBTopUpComplete.lblMNVal.text
	}
	
    inputParams["customerName"] = gblCustomerName;
    inputParams["fromAccount"] =frmIBTopUpComplete.lblAccountNo.text;
    inputParams["fromAcctNick"] =  frmIBTopUpComplete.lblName.text;
    inputParams["billerNick"] = frmIBTopUpComplete.lblePayNickname.text;
    inputParams["billerName"] = frmIBTopUpComplete.lblCompCode.text;
    inputParams["ref1"] = frmIBTopUpComplete.lblRef1Value.text;
    inputParams["ref2"] = "";
   // inputParams["ref2"] = frmIBTopUpComplete.lblBPRef2Val.text;
	
	
    inputParams["amount"] = frmIBTopUpComplete.lblAmtVal.text;
    inputParams["fee"] = frmIBTopUpComplete.lblFeeVal.text;
    inputParams["date"] = frmIBTopUpComplete.lblePayDate.text;
    inputParams["refID"] = frmIBTopUpComplete.lblTRNVal.text;
    inputParams["memo"] = frmIBTopUpComplete.lblMNVal.text;
	
    inputParams["deliveryMethod"] = "Email";
    inputParams["noSendInd"] = "0";
   // inputParams["emailId"] = gblEmailIdBillerNotification;//gblEmailIdBillerNotification
    inputParams["notificationType"] = "Email";
    inputParams["Locale"] = kony.i18n.getCurrentLocale();
    inputParams["notificationSubject"] = "billpay";
    inputParams["notificationContent"] = "billPaymentDone";
    //alert("calling NotificationAdd ");
	//showLoadingScreen();
	invokeServiceSecureAsync("NotificationAdd", inputParams, callTopUpNotificationAddServiceCallBack);
}*/
/*
 * Method CallBack of BillPaymentNotificationAddServiceCallBack
 *
 */

/*function callTopUpNotificationAddServiceCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			
			var StatusCode = resulttable["statusCode"];
			var Severity = resulttable["severity"];
			var StatusDesc = resulttable["StatusDesc"];
			
			if (StatusCode != "0") {
				//alert("----callTopUpNotificationAddServiceCallBack status code:" + StatusCode)
				return false;
}
		}
	}
}*/

function populateTopUpCompletescreen() {
	if(gblEasyPassCustomer){
		frmIBTopUpComplete.hbxEasyPassName.setVisibility(true);
		frmIBTopUpComplete.hbxTopUpRef.setVisibility(true);
		frmIBTopUpComplete.hbxCardBal.setVisibility(true);
		frmIBTopUpComplete.lblEasyPassName.text = easyPassNameEN;
		frmIBTopUpComplete.lblTopUpRefVal.text=frmIBTopUpConfirmation.lblReferenceVal.text;//ref id Doubts
		gblEasyPassCustomer = false;
	}
	else {
			frmIBTopUpComplete.hbxEasyPassName.setVisibility(false)
			frmIBTopUpComplete.hbxTopUpRef.setVisibility(false);
			frmIBTopUpComplete.hbxCardBal.setVisibility(false);
	}
 
	var balanceBefore = removeCommos(frmIBTopUpConfirmation.lblBeforBal.text);
	var beforeAmount=removeCommos(frmIBTopUpConfirmation.lblAmtVal.text);
	var beforeFee=removeCommos(frmIBTopUpConfirmation.lblFeeVal.text);

	var balanceAfter = parseFloat(balanceBefore)-parseFloat(beforeAmount)-parseFloat(beforeFee)
	
	


	frmIBTopUpComplete.lblName.text = frmIBTopUpConfirmation.lblName.text; //from account type
	frmIBTopUpComplete.lblAccountNo.text = frmIBTopUpConfirmation.lblAccountNo.text; //from account no
	frmIBTopUpComplete.lblFromName.text = frmIBTopUpConfirmation.lblFromAccountName.text; //from name //
	//frmIBTopUpComplete.lblBBPaymentValue.text = commaFormatted(balanceAfter.toString())+ kony.i18n.getLocalizedString("currencyThaiBaht")//frmIBTopUpConfirmation.lblBeforBal.text; //balance
	frmIBTopUpComplete.lblePayNickname.text = frmIBTopUpConfirmation.lblNicknameTo.text; //nick
	frmIBTopUpComplete.lblCompCode.text = frmIBTopUpConfirmation.lblCompCodeTo.text; //compcode
	frmIBTopUpComplete.lblCompCode.isVisible=false; // showing only 1 label for Nickname/Biller name compcode
	frmIBTopUpComplete.lblref1.text = frmIBTopUpConfirmation.lblRef1.text; //ref1
	//below line is changed for CR - PCI-DSS masked Credit card no
	frmIBTopUpComplete.lblRef1Value.text = frmIBTopUpConfirmation.lblRef1ValueMasked.text; //frmIBTopUpConfirmation.lblRef1Value.text; //ref1 value
	// frmIBTopUpComplete.label588684466285184.text=frmIBTopUpConfirmation.//topup name Doubts
	//frmIBTopUpComplete.label588684466285186.text=frmIBTopUpConfirmation.//card balance Doubts
	frmIBTopUpComplete.lblAmtVal.text = frmIBTopUpConfirmation.lblAmtVal.text; //amt value
	frmIBTopUpComplete.lblFeeVal.text = frmIBTopUpConfirmation.lblFeeVal.text; //fee amount
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	frmIBTopUpComplete.lblMNVal.text = replaceHtmlTagChars(frmIBTopUpConfirmation.lblMNVal.text); //note
	frmIBTopUpComplete.lblTRNVal.text = frmIBTopUpConfirmation.lblTRNVal.text; //ref no
	frmIBTopUpComplete.imgFrom.src = frmIBTopUpConfirmation.imgAcctLogo.src;
	frmIBTopUpComplete.imgbiller.src = frmIBTopUpConfirmation.imgbiller.src;
	
	if (!gblPaynow) {
		frmIBTopUpComplete.hbxScheduleDetails.setVisibility(true);
		frmIBTopUpComplete.hbxBalance.setVisibility(false);
		frmIBTopUpComplete.lblStartOnValue.text = frmIBTopUpConfirmation.lblStartOnValue.text;
		frmIBTopUpComplete.lblRepeatValue.text = frmIBTopUpConfirmation.lblRepeatValue.text;
		frmIBTopUpComplete.lblEnDONValue.text = frmIBTopUpConfirmation.lblEnDONValue.text;
		frmIBTopUpComplete.lblExecuteValue.text = frmIBTopUpConfirmation.lblExecuteValue.text;
		frmIBTopUpComplete.lblePayDate.text = gblCurrentBillPayTime;
		frmIBTopUpComplete.lblTransfer.text = kony.i18n.getLocalizedString("keyBillPaymentPaymentOrderDate");
	} else {
		frmIBTopUpComplete.hbxBalance.setVisibility(true);
		frmIBTopUpComplete.hbxScheduleDetails.setVisibility(false);
		//frmIBTopUpComplete.lblePayDate.text = frmIBTopUpConfirmation.lblTransferVal.text;
		frmIBTopUpComplete.lblTransfer.text = kony.i18n.getLocalizedString("keyIBPaymentDate");
	}
	frmIBTopUpComplete.show();
}


/*function callCustomBillAddTopUpServiceIBCallback(status, resulttable) {
	
	
	if (status == 400) //success responce
	{
		dismissLoadingScreenPopup();
		
		
		
		if (resulttable["opstatus"] == 0) {
			var StatusCode = resulttable["statusCode"];
			var Severity = resulttable["severity"];
			var StatusDesc = resulttable["statusDesc"];
			
			if (StatusCode != "0") {
				alert(" " + StatusDesc)
				return false;
			} else {
				gblCurrentBillPayTime = resulttable["currentTime"];
				callTopUpCrmProfileUpdateService();
			}
			// callTopUpCrmProfileUpdateService();
		}
	}
}*/

function mapOfflineTopUp() {
	gblPenalty = false;
	//Penalty Hide
	//frmIBBillPaymentLP.hbxPenalty.setVisibility(false);
	frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.setVisibility(true); //set value from service
	frmIBTopUpLandingPage.txtAmount.setEnabled(true);
	frmIBTopUpLandingPage.hbxAmount.setVisibility(false);
}

function mapOnlineTopUp() {
	//for Online Biller
	// added for integration
	// Assign step amount for top up from master biller Inquiry here for fix of ticket #33768
	if (gblCompCode.trim() == "2704"){
      	frmIBTopUpLandingPage.txtAmount.text = "";
		frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.setVisibility(true);
		frmIBTopUpLandingPage.hbxAmount.setVisibility(false);
    }else{
      	var comboData = [
        [0, kony.i18n.getLocalizedString("keySelectAmt")]
	    ];
	    
	    var stepAmtTopupArray = [];
	    for(var i = 0; i < MySelectBillSuggestListRs.length; i++) {
	    	if(MySelectBillSuggestListRs[i]["BillerCompCode"].trim() == gblCompCode.trim()) {
	    		stepAmtTopupArray = MySelectBillSuggestListRs[i]["StepAmount"];
	    		break;
	    	}
	    }
	    if(stepAmtTopupArray.length != 0) {
			for (i = 0; i < stepAmtTopupArray.length; i++) {
				var temp = [(i + 1), stepAmtTopupArray[i]["Amt"]];
				comboData.push(temp);
			}
			
			frmIBTopUpLandingPage.comboAmount.masterData = comboData;
			frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.setVisibility(false);
			frmIBTopUpLandingPage.hbxAmount.setVisibility(true);
		}      
    }
}

function mapTMBCreditCardTopUp() {
	//for CreditCard and Ready cash
	var inputParam = {};
	// var toDayDate = getTodaysDate();
	
	var cardId = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	
	inputParam["cardId"] = "00000000" + cardId;
	//inputParam["waiverCode"] = WAIVERCODE;
	inputParam["tranCode"] = TRANSCODEMIN;
	inputParam["postedDt"] = getTodaysDate();
	frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.setVisibility(true);
	frmIBTopUpLandingPage.hbxAmount.setVisibility(false);
	//invokeServiceSecureAsync("creditcardDetailsInqNonSec", inputParam, topUpPaymentcreditcardDetailsInqServiceCallBackIB);
}

function mapReadyCashTopUp() {
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var ZERO_PAD = "0000";
	var BRANCH_CODE;
	var ref1AccountIDDeposit = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	var fiident;
	if (ref1AccountIDLoan.length == 10) {
		
		fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDDeposit[0] + ref1AccountIDDeposit[1] +
			ref1AccountIDDeposit[2] + ZERO_PAD;
		ref1AccountIDDeposit = ZERO_PAD + ref1AccountIDLoan
	}
	if (ref1AccountIDLoan.length == 13) {
		
		fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDDeposit[0] + ref1AccountIDDeposit[1] +
			ref1AccountIDDeposit[2] + ZERO_PAD;
		ref1AccountIDDeposit = "0" + ref1AccountIDLoan
	} else {
		fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDDeposit[1] + ref1AccountIDDeposit[2] +
			ref1AccountIDDeposit[3] + ZERO_PAD;
	}
	
	var inputParam = {};
	inputparam["acctId"] = ref1AccountIDDeposit;
	//inputparam["acctType"] = "CDA";
	//inputparam["spName"] = "com.fnf.xes.ST"
	invokeServiceSecureAsync("depositAccountInquiry", inputParam, topUpPaymentdepositAccountInquiryServiceCallBackIB);
}

function mapTMBLoanTopUp() {
	//for TMB Loan
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var ZERO_PAD = "0000";
	var BRANCH_CODE;
	var ref1AccountIDLoan = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	var fiident;
	if (ref1AccountIDLoan.length == 10) {
		
		fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] +
			ref1AccountIDLoan[2] + ZERO_PAD;
		ref1AccountIDLoan = ZERO_PAD + ref1AccountIDLoan
	}
	if (ref1AccountIDLoan.length == 13) {
		
		fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[0] + ref1AccountIDLoan[1] +
			ref1AccountIDLoan[2] + ZERO_PAD;
		ref1AccountIDLoan = "0" + ref1AccountIDLoan
	} else {
		fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + ref1AccountIDLoan[1] + ref1AccountIDLoan[2] +
			ref1AccountIDLoan[3] + ZERO_PAD;
	}
	
	var inputParam = {};
	inputParam["acctId"] = fiident + ref1AccountIDLoan;
	//inputParam["acctId"]="001100010330000003305702239001";
	//inputparam["acctId"] = gblAccountTable["custAcctRec"][gblIndex]["fiident"] + gblAccountTable["custAcctRec"][gblIndex]["accId"]
	//inputparam["acctType"] = gblAccountTable["custAcctRec"][gblIndex]["accType"];
	inputParam["acctType"] = "LOC";
	
	invokeServiceSecureAsync("doLoanAcctInq", inputParam, topUpPaymentdoLoanAcctInqServiceCallBackIB);
}

function topUpPaymentdoLoanAcctInqServiceCallBackIB(status, result) {
	
	
	frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.setVisibility(true);
	frmIBTopUpLandingPage.hbxAmount.setVisibility(false);
	
	if (status == 400) //success responce
	{
		
		
		
		if (result["opstatus"] == 0) {
			
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			/** checking for Soap status below  */
			//
			var balAmount;
			//
			for (var i = 0; i < result["acctBal"].length; i++) {
				if (result["acctBal"][i]["balType"] == "Outstanding") {
					balAmount = result["acctBal"][i]["balAmount"];
					
					alert("amount is>>>>" + balAmount);
				}
			}
			
			frmIBTopUpLandingPage.txtAmount.text = balAmount;
			dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			alert(" " + result["errMsg"]);
		}
	}
}

function topUpPaymentdepositAccountInquiryServiceCallBackIB(status, result) {
	
	
	frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.setVisibility(true);
	frmIBTopUpLandingPage.hbxAmount.setVisibility(false);
	
	if (status == 400) //success responce
	{
		
		
		
		if (result["opstatus"] == 0) {
			
			var BalType;
			var Amt;
			var CurCode;
			for (var i = 0; i < result["AcctBal"].length; i++) {
				BalType = result["AcctBal"][i]["BalType"];
				
				if (BalType == "Principle") {
					Amt = result["AcctBal"][i]["Amt"];
					CurCode = result["AcctBal"][i]["CurCode"];
				}
			}
			
			//alert("amount is>>>>" + Amt);
			
			// alert("amount is>>>>" + CurCode);
			frmIBTopUpLandingPage.txtAmount.text = Amt;
			dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			alert(" " + result["errMsg"]);
		}
	}
}



function checkTopUpCrmProfileInqIB() {
	var inputParam = {}
	inputParam["crmId"] = gblcrmId;
	//inputParam["transferFlag"] = "true"
	//showLoadingScreen()
	invokeServiceSecureAsync("crmProfileInq", inputParam, callBackTopUpCrmProfileInqIB)
}

function callBackTopUpCrmProfileInqIB(status, resulttable) {
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			/// check channel limit and available balance
			var StatusCode = resulttable["statusCode"];
			var Severity = resulttable["Severity"];
			var StatusDesc = resulttable["StatusDesc"];
			if (StatusCode == 0) {
			gblUserLockStatusIB=resulttable["ibUserStatusIdTr"]
				if (resulttable["ibUserStatusIdTr"] == gblFinancialTxnIBLock) {
					dismissLoadingScreenPopup();
					
					alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
					return ;
					}
				
				//setCrmProfileInqData(resulttable);  //caching profile information for profile Update; 
				var temp1 = [];
				var temp = {
					ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"]
				}
				kony.table.insert(temp1, temp);
				gblCRMProfileData = temp1;
				
				DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
				UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"].toString());
				// DailyLimit = 7000000;
				// UsageLimit = 500000;
				// gblTransferRefNo=resulttable["acctIdentValue"]
				// 
				var billAmount = "0.00";
				if(frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.isVisible){
					billAmount = parseFloat(removeCommos(frmIBTopUpLandingPage.txtAmount.text));
				} else {
					billAmount = parseFloat(removeCommos(frmIBTopUpLandingPage.comboAmount.selectedKeyValue[1]));
					
				}
				var billAmountFee = parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text));
				
				
				var totalBillPayAmt = billAmount ;//+ billAmountFee;
				
				gblchannelLimit = DailyLimit - UsageLimit;
				gblchannelLimit = fixedToTwoDecimal(gblchannelLimit);
				var BPdailylimitExceedMsg =  kony.i18n.getLocalizedString("MIB_BPExcDaily");
				BPdailylimitExceedMsg = BPdailylimitExceedMsg.replace("{remaining_limit}",  commaFormatted(gblchannelLimit+""));	
                
			if(gblPaynow) {	
				if (gblchannelLimit > 0) {
					
					if (gblchannelLimit < totalBillPayAmt) {
						dismissLoadingScreenPopup();
						showAlertIB(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
						//alert(kony.i18n.getLocalizedString("keyexceedsdailylimitTopUp"));
						// alert(" "+kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday"));
						return false;
					} else {
						//dismissLoadingScreenPopup();
						if(gblBillerMethod == "1"){
									OnlineTopUpInquiryAfterAmount();
								}else{
									
									generateTransferRefNoserviceIBTopUp();		
								}
						
						//dismissLoadingScreen() 
						return true;
					}
				}else {
						dismissLoadingScreenPopup();
						showAlertIB(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
						//alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
				}
			} else {
					if(DailyLimit > 0){
							if (DailyLimit < totalBillPayAmt) {
	                           	 dismissLoadingScreenPopup();
	                           	 showAlertIB(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
	                           	 //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
	                           	 return false;
                        	} else {
                        		if(gblBillerMethod == "1"){
									OnlineTopUpInquiryAfterAmount();
								}else{
									generateTransferRefNoserviceIBTopUp();
								}
                           	 	
                           	 	return true;
                        	}
					} else{
                        // alert("user channel limit exceeds for the day!!!");
                        dismissLoadingScreenPopup();
                        showAlertIB(BPdailylimitExceedMsg, kony.i18n.getLocalizedString("info"));
                        //alert(kony.i18n.getLocalizedString("keyexceedsdailylimit"));
                    }
			}	
			} else {
				dismissLoadingScreenPopup();//use in IB
				//dismissLoadingScreen();
				alert(" " + resulttable["errMsg"])
				return false;
			}
		} else {
			dismissLoadingScreenPopup();
			alert(" " + resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		}
	}
}

function generateTransferRefNoserviceIBTopUp() {
	inputParam = {};
	if (gblPaynow) {
		inputParam["transRefType"] = "NU";
	} else {
		inputParam["transRefType"] = "SU";
	}
	invokeServiceSecureAsync("generateTransferRefNo", inputParam, generateTransferRefNoserviceIBTopUpCallBack);
}

function generateTransferRefNoserviceIBTopUpCallBack(status, result) {
	
	
	
	if (status == 400) //success responce
	{
		
		
		
		if (result["opstatus"] == 0) {
			
			var refNum = result["transRefNum"];
			
			frmIBTopUpConfirmation.lblTRNVal.text = refNum + "00";
			saveToSessionTopUpIB()
			
		
			
		} else {
			dismissLoadingScreenPopup();
			alert(" " + result["errMsg"]);
		}
	}
}

function callTopUpPaymentCustomerAccountServiceIB() {
if(getCRMLockStatus())
	{
	curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}
	else{
	
	var inputParam = {}
	inputParam["billPayInd"] = "billPayInd";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("customerAccountInquiry", inputParam, topUpPaymentCustomerAccountCallBackIB);
	}
}

function topUpPaymentCustomerAccountCallBackIB(status, resulttable) {
	frmIBTopUpCW.txtAmount.setEnabled(false);
	
	
	if (status == 400) //success responce
	{
		
		
		
		if (resulttable["opstatus"] == 0) {
			/** checking for Soap status below  */
			var StatusCode = resulttable["statusCode"];
			
			//	if( resulttable["StatusCode"] !=null &&   resulttable["StatusCode"] == "0"){
			var fromData = []
			var j = 1
			gbltranFromSelIndex = [0, 0];
			
			
			if(resulttable.custAcctRec!=null)
			{gblNoOfFromAcs = resulttable.custAcctRec.length;
			var nonCASAAct = 0;
			for (var i = 0; i < resulttable.custAcctRec.length; i++) {
			var accountStatus = resulttable["custAcctRec"][i].acctStatus;
				if(accountStatus.indexOf("Active") == -1){
				nonCASAAct = nonCASAAct + 1;
				//showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
					//return false;
				}
					if(accountStatus.indexOf("Active") >=0){
					if(resulttable.custAcctRec[i].personalisedAcctStatusCode == "02" ) {
						continue;  //do not populate if account is deleted
					}
					
					var icon = "";
					/*if (resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("Saving")) {
						if (resulttable.custAcctRec[i].productID == "219" || resulttable.custAcctRec[i].productID == "220" || resulttable
							.custAcctRec[i].productID == "222") {
							icon = "prod_nofee.png"
						} else if (resulttable.custAcctRec[i].productID == "221") {
							icon = "prod_savingd.png"
						} else if (resulttable.custAcctRec[i].productID == "203" || resulttable.custAcctRec[i].productID == "206") {
							icon = "prod_savingd.png"
						} else {
							icon = "prod_currentac.png"
						}
					} else if (resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("Current")) {
						icon = "prod_currentac.png"
					} else if (resulttable.custAcctRec[i].accType == kony.i18n.getLocalizedString("termDeposit")) {
						icon = "prod_termdeposits.png"
					}*/
				//
				//fiident	
				icon="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+resulttable.custAcctRec[i]["ICON_ID"]+"&modIdentifier=PRODICON";	
				if (j == 1) {
					var temp = createSegmentRecordIB(resulttable.custAcctRec[i],icon)
				} else if (j == 2) {
					var temp = createSegmentRecordIB(resulttable.custAcctRec[i],icon)
				} else if (j == 3) {
					var temp = createSegmentRecordIB(resulttable.custAcctRec[i],icon)
					j = 0;
				}
				
				var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc
				
				if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {
					
				} else {
					
					kony.table.insert(fromData, temp[0])
				}
				}
				//
			} }//for
			
			
			
			//frmIBBillPaymentCW.customIBBPFromAccount.data = fromData;
			if(nonCASAAct==resulttable.custAcctRec.length)
			{
				showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
				return false;
			}
			if(fromData.length == 0){
			//alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"));
			showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
			dismissLoadingScreenPopup();
			return false;
			} else {
				frmIBTopUpCW.coverFlowTP.data = fromData;
			}
			dismissLoadingScreenPopup();
			frmIBTopUpCW.show();
			
			if(window.navigator.appVersion.match(/MSIE (\d+)/) != null && RegExp.$1 == "8")
				langspecificmenuIE8();
			else
				langspecificmenu();
			frmIBTopUpCW.segMenuOptions.removeAll();
			if(kony.i18n.getCurrentLocale() != "th_TH")
				frmIBTopUpCW.btnMenuTopUp.skin = "btnIBMenuTopUpFocus";
			else 
				frmIBTopUpCW.btnMenuTopUp.skin = "btnIBMenuTopUpFocusThai";
			//dismissLoadingScreen();
			frmIBTopUpCW.coverFlowTP.onSelect = customWidgetSelectEventTopUpPmntIB;
		}
		else if (resulttable["opstatus"] == 1) {
		showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
		} 
		else {
			dismissLoadingScreenPopup();
			alert(" " + resulttable["errMsg"]);
		}
		dismissLoadingScreenPopup();
	} //status 400
}
 
function customWidgetSelectEventTopUpPmntIB() {
	showLoadingScreenPopup();
	billPayTopup = false;
	frmIBTopUpLandingPage.hbxFrmSegmentDataContainer.setVisibility(true);
	var selectedItem = frmIBTopUpCW.coverFlowTP.selectedItem;
	var selectedData = frmIBTopUpCW.coverFlowTP.data[gblCWSelectedItem];
	frmIBTopUpLandingPage.lblName.text = selectedData.lblCustName;
    accntTopupName = selectedData.AccountName;
    gblProductNameEN = selectedData.lblAccountNickNameEN;
    gblProductNameTH = selectedData.lblAccountNickNameTH;
	 gblNickNameEN = selectedData.lblAccountNickNameEN;
    gblNickNameTH = selectedData.lblAccountNickNameTH;
    gblProductNameEN = selectedData.lblProductValEN;
    gblProductNameTH = selectedData.lblProductValTH;
    
	frmIBTopUpLandingPage.lblAcctType.text = selectedData.lblProductVal;
	frmIBTopUpLandingPage.lblAccountNo.text = selectedData.lblActNoval;
	
	//getAccountNameOfUser(selectedData.lblActNoval);
	
	frmIBTopUpLandingPage.lblBalanceValue.text = selectedData.lblBalance;
	frmIBTopUpLandingPage.imgFrom.src = selectedData.img1;
	frmIBTopUpLandingPage.lblDummy.text = selectedData.lblDummy;
	frmIBTopUpLandingPage.hbxBalance.isVisible = true;
	frmIBTopUpLandingPage.lblLimitValue.text = frmIBTopUpCW.lblLimitValue.text;
	hideShowScheduleNicknameTopUpIB(false);
	frmIBTopUpLandingPage.show();
	frmIBTopUpLandingPage.hbxImage.setVisibility(true);
	frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
	frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
	frmIBTopUpLandingPage.hbxwaterWheel.setVisibility(false);
	 if(selectedData.lblRemFee != "" && selectedData.lblRemFeeval != ""){
    frmIBTopUpLandingPage.lblFreeTran.text = selectedData.lblRemFee;
    frmIBTopUpLandingPage.lblFreeTransValue.text = selectedData.lblRemFeeval;
    frmIBTopUpLandingPage.hbxFreeTrans.setVisibility(true);
    gblBillpaymentNoFee=true;
    
    }
    else {
    gblBillpaymentNoFee=false;
    frmIBTopUpLandingPage.hbxFreeTrans.setVisibility(false);
    frmIBTopUpLandingPage.lblFreeTran.text ="";
    frmIBTopUpLandingPage.lblFreeTransValue.text="";
    
    }
}

function startTopUpPaymentOTPRequestService() {
frmIBTopUpConfirmation.txtOtp.text="";
	var locale = kony.i18n.getCurrentLocale();
	var eventNotificationPolicy;
	var SMSSubject;
	var Channel;
	Channel = "TopUpPayment";
	
    frmIBTopUpConfirmation.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBTopUpConfirmation.lblPlsReEnter.text = " "; 
    frmIBTopUpConfirmation.hbxOTPincurrect.isVisible = false;
    frmIBTopUpConfirmation.hbxRefNo.isVisible = true;
    frmIBTopUpConfirmation.hbxOTPsnt.isVisible = true;
  //  frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
  //  frmIBTopUpConfirmation.txtOtp.setFocus(true);
	
	var topUpAmountWithoutBaht = frmIBTopUpConfirmation.lblAmtVal.text.substring(0, frmIBTopUpConfirmation.lblAmtVal.text.length - 1);
	
	var inputParams = {
		retryCounterRequestOTP: gblRetryCountRequestOTP,
		Channel: Channel,
		locale: locale
	};
	showLoadingScreenPopup();
	try {
		// invokeServiceSecureAsync("generateOTP", inputParams, startOTPRequestTopUPServiceBPAsyncCallback);
		invokeServiceSecureAsync("generateOTPWithUser", inputParams, startOTPRequestTopUPServiceBPAsyncCallback);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}
/**
 * Callback method for startOTPRequestService
 * @param {} callBackResponse
 */

function startOTPRequestTopUPServiceBPAsyncCallback(status, callBackResponse) {
	if (status == 400) {
		if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
		dismissLoadingScreenPopup();
		showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"),
			kony.i18n.getLocalizedString("info"));
		return false;
	   }
	    if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
	   	dismissLoadingScreenPopup();
		showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
		return false;
	   }
		if (callBackResponse["opstatus"] == 0) {
			
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"),
					kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			gblOTPReqLimit=kony.os.toNumber(callBackResponse["otpRequestLimit"]);
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
		//	frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
		//	frmIBTopUpConfirmation.txtOtp.setFocus(true);
			kony.timer.schedule("OtpTimer", OTPTimercallbackTopUp, reqOtpTimer, false);
			dismissLoadingScreenPopup();
			frmIBTopUpConfirmation.hbxRefNo.setVisibility(true);
			var refVal="";
			frmIBTopUpConfirmation.hbxOTPsnt.setVisibility(true);
			frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
			frmIBTopUpConfirmation.txtOtp.setFocus(true);
   			for(var d=0;callBackResponse["Collection1"].length;d++){
    			if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
      				refVal=callBackResponse["Collection1"][d]["ValueString"];
      			break;
    		}
   		}
			//frmIBTopUpConfirmation.hbxOTPsnt.setVisibility(true);
            frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
			            frmIBTopUpConfirmation.txtOtp.setFocus(true);
			frmIBTopUpConfirmation.btnOTPReq.skin = btnIBREQotp;
			frmIBTopUpConfirmation.btnOTPReq.focusSkin = btnIBREQotp;
			frmIBTopUpConfirmation.btnOTPReq.hoverSkin = btnIBREQotp;
			frmIBTopUpConfirmation.btnOTPReq.onClick = "";
			frmIBTopUpConfirmation.lblBankRef.setVisibility(true);
			frmIBTopUpConfirmation.lblBankRef.text = kony.i18n.getLocalizedString("BankRef.No");
			frmIBTopUpConfirmation.refNoValue.text = refVal//callBackResponse["bankRefNo"];
			
			//frmIBTopUpConfirmation.label476047582127701.setVisibility(true);
			//            frmIBBillPaymentConfirm.label476047582115277.setVisibility(true);
			//            frmIBBillPaymentConfirm.label476047582115279.setVisibility(true);
			//            frmIBBillPaymentConfirm.label476047582127701.text = callBackResponse["bankRefNo"];
			//            frmIBBillPaymentConfirm.label476047582115277.text = kony.i18n.getLocalizedString("keyotpmsg");
			            frmIBTopUpConfirmation.lblPhoneNo.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
		//	dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			if(callBackResponse["errMsg"] != undefined)
        	{
        		alert(callBackResponse["errMsg"]);
        	}
        	else
        	{
        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
        	}
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
		}
	}
}

function validateChannelIBTopUp() {
	var ref1= frmIBTopUpLandingPage.txtAddBillerRef1.text;
	var newNickName = frmIBTopUpLandingPage.txtScheduleNickname.text;
	if (frmIBTopUpLandingPage.lblCompCode.text == null || kony.string.trim(frmIBTopUpLandingPage.lblCompCode.text) == "") {
		alert(kony.i18n.getLocalizedString("selectTopUpbiller"));
		return false;
	}else if(isNotBlank(ref1)){
			if(!kony.string.isNumeric(ref1)){
				var errmsg = kony.i18n.getLocalizedString("MIB_BPWrongRef"); // new key as per Re-design Top Up document.
				errmsg = errmsg.replace("{ref_label}",removeColonFromEnd(frmIBTopUpLandingPage.ref1.text));
				showAlert(errmsg,kony.i18n.getLocalizedString("info"));//for error in ref1
				return false;
			}else if(!gblBillerPresentInMyBills && !gblPaynow){
				if(isNotBlank(newNickName)){
					if(NickNameValid(newNickName)){
						if(checkBillerNicknameInMyBills(gblMyBillList,newNickName)){
							//nickname exists alert
							showAlert(kony.i18n.getLocalizedString("MIB_BPErr_alert_correctnickname"),kony.i18n.getLocalizedString("info"));
							return false;
						}
					}else{
						showAlert(kony.i18n.getLocalizedString("MIB_BPkeyInvalidNickName"),kony.i18n.getLocalizedString("info"));
						return false;
					}
				}else{
					showAlert(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"),kony.i18n.getLocalizedString("info"));
					return false;
				}	
			}else{
			  	//Case for already added biller
			  	frmIBTopUpLandingPage.txtScheduleNickname.text="";
			}
	}else{
		var errmsg = kony.i18n.getLocalizedString("MIB_BPNoRef"); // new key as per Re-design Top Up document.
		errmsg = errmsg.replace("{ref_label}",removeColonFromEnd(frmIBTopUpLandingPage.ref1.text));
		showAlert(errmsg,kony.i18n.getLocalizedString("info"));//error for blank ref1
		return false;
	}
		
	var inputParams = {
			billerCompcode: gblCompCode
		};
	showLoadingScreenPopup();	
	invokeServiceSecureAsync("masterBillerInquiry", inputParams, validateChannelIBTopUpCallBack);
}

function validateChannelIBTopUpCallBack(status, callBackResponse) {
	
	if (status == 400) {
		dismissLoadingScreenPopup();
		if (callBackResponse["opstatus"] == "0") {
			var responseData = callBackResponse["MasterBillerInqRs"];
			if (responseData.length > 0) {
				
				gblBillerCategoryID = responseData[0]["BillerCategoryID"];
				gblBillerId = responseData[0]["BillerID"];
				billerValidationTopUp();
			} else {
				
				alert(kony.i18n.getLocalizedString("keyNotValidForIB"));
			}
		} else {
			dismissLoadingScreenPopup();
			alert("No Suggested Biller found");
			return;
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("No Suggested Biller found");
			return;
		}
	}
}

function billerValidationTopUp() {
	//start of biller validation, validating ref1/ref2.amount/date 
	var billCompCode = frmIBTopUpLandingPage.lblCompCode.text;
	var topupBillerSelected = false;
	if (frmIBTopUpLandingPage.lblCompCode.text == null || kony.string.trim(frmIBTopUpLandingPage.lblCompCode.text) == "") {
		alert(kony.i18n.getLocalizedString("selectTopUpbiller"));
		topupBillerSelected = false;
		return false;
	} else {
		topupBillerSelected = true;
	}
	var compCodeBillPay = gblCompCode;
	
	var ref1ValBillPay = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	var ref2ValBillPay = "";
	var billAmountBillPay = "";
	if (frmIBTopUpLandingPage.hbxAmntExclBillerMethodOne.isVisible == true) {
		billAmountBillPay = removeCommos(frmIBTopUpLandingPage.txtAmount.text);
		
		if (gblCompCode == "2704"){
			if (billerValidationTopUpAmtMinMax(billAmountBillPay)){
				return;
			}
		}
		
		if (billAmountBillPay == null || billAmountBillPay == "" || billAmountBillPay == " " || billAmountBillPay == "0" || billAmountBillPay == "0.0" || billAmountBillPay == "0.00") {
			alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
			return;
		}
		
		var regex = /^(?:\d*\.\d{1,2}|\d+)$/
		if (!regex.test(billAmountBillPay)) {
			alert(kony.i18n.getLocalizedString("MIB_BPNoAmt"));
			return;
		}	
	} else {
		billAmountBillPay = frmIBTopUpLandingPage.comboAmount.selectedKeyValue[1];
		
		if (billAmountBillPay == null || billAmountBillPay == "" || billAmountBillPay == " " || billAmountBillPay == "0" || billAmountBillPay == "0.0" || billAmountBillPay == "0.00") {
			alert(kony.i18n.getLocalizedString("KeyEnterVldAmtPopUP"));
			return;
		}
		
		var regex = /^(?:\d*\.\d{1,2}|\d+)$/
		if (!regex.test(billAmountBillPay)) {
			alert(kony.i18n.getLocalizedString("KeyEnterVldAmtPopUP"));
			return;
		}else{
			
		}
	}
	
	var balAmountWithComma = frmIBTopUpLandingPage.lblBalanceValue.text
	
	var amount = balAmountWithComma.replace(/,/g,"");
	
	if (parseFloat(billAmountBillPay) > parseFloat(amount) && gblPaynow) {
		alert(kony.i18n.getLocalizedString("MIB_BPLessAvailBal"));
		return;
	}
	
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	/*
	if(isNotBlank(frmIBTopUpLandingPage.textarea210136690395040.text)){
		if(!MyNoteValid(frmIBTopUpLandingPage.textarea210136690395040.text)){
			alert(kony.i18n.getLocalizedString("MIB_TRkeyMynoteInvalid"));
			frmIBTopUpLandingPage.textarea210136690395040.setFocus(true);
			return false;
		}
	}
	*/
	if(isNotBlank(frmIBTopUpLandingPage.textarea210136690395040.text)){
		if(checkSpecialCharMyNote(frmIBTopUpLandingPage.textarea210136690395040.text)){
			alert(kony.i18n.getLocalizedString("MIB_MyNoteInvalidSpecialChar"));
			frmIBTopUpLandingPage.textarea210136690395040.setFocus(true);
			return false;
		}
	}
	
	if(isNotBlank(frmIBTopUpLandingPage.textarea210136690395040.text) && frmIBTopUpLandingPage.textarea210136690395040.text.length > 50){
		alert(kony.i18n.getLocalizedString("MaxlngthNoteError"));
		frmIBTopUpLandingPage.textarea210136690395040.setFocus(true);
		return;
	}
		
	if (topupBillerSelected) {
		var scheduleDtBillPay = frmIBTopUpLandingPage.lblPayBillOnValue.text;
		var billerType = "TopUp";
		callBillerValidationBillPayService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay,
			scheduleDtBillPay, billerType);
	}
	//getFeeForTopUpIB();
}

function billerValidationTopUpAmtMinMax(topupAmt){
	var j = 0;
	var tmp = "";
	var trueMoney = "2704";
	var amtMin = "";
	var amtMax = "";
	var errorText ="";
	var lengthBillersSuggestList = gblBillerLastSearchNoCat.length;

	for (j = 0, i = 0; i < lengthBillersSuggestList; i++) {			
			if (gblBillerLastSearchNoCat[i].BillerCompCode == trueMoney) {				
				amtMin = gblBillerLastSearchNoCat[i].TopupAmtMin; 
				amtMax = gblBillerLastSearchNoCat[i].TopupAmtMax;				
				break;
			}
	}
	
	if (topupAmt == null || topupAmt == "" || topupAmt == " " || topupAmt == "0" || topupAmt == "0.0" || topupAmt == "0.00") {
		var errorText = kony.i18n.getLocalizedString("MIB_BPMinimum");
		errorText = errorText.replace("{amt_value}", commaFormatted(amtMin));
		alert(errorText);
		return true;
	}else if (parseFloat(topupAmt) < parseFloat(amtMin)){
		var errorText = kony.i18n.getLocalizedString("MIB_BPMinimum");
		errorText = errorText.replace("{amt_value}", commaFormatted(amtMin));
		alert(errorText);
		return true;
	}else if (parseFloat(topupAmt) > parseFloat(amtMax)){
		var errorText = kony.i18n.getLocalizedString("MIB_BPMaximum");
		errorText = errorText.replace("{amt_value}", commaFormatted(amtMax));
		alert(errorText);
		return true;
	}
	return false;	
}

/*function setFinancialActivityLogForTopup(finTxnRefId, finTxnAmount, finTxnFee, finTxnStatus) {
	GBLFINANACIALACTIVITYLOG.finTxnRefId = finTxnRefId;
	var tranCode;
	var fromAccType = frmIBTopUpLandingPage.lblAcctType.text;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	if (gblPaynow) {
		GBLFINANACIALACTIVITYLOG.activityTypeId = "030";
	} else {
		GBLFINANACIALACTIVITYLOG.activityTypeId = "031";
	}
	//02 is for mobile banking
	GBLFINANACIALACTIVITYLOG.channelId = "01"
	var fromAccountId = frmIBTopUpConfirmation.lblAccountNo.text;
	GBLFINANACIALACTIVITYLOG.fromAcctId = fromAccountId.replace(/-/g, "");
	// GBLFINANACIALACTIVITYLOG.toAcctId = "";
	GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
	GBLFINANACIALACTIVITYLOG.finTxnAmount = parseFloat(removeCommos(finTxnAmount)).toFixed(2)
	GBLFINANACIALACTIVITYLOG.txnCd = tranCode;
	GBLFINANACIALACTIVITYLOG.finTxnFee = parseFloat(removeCommos(finTxnFee)).toFixed(2);
	var availableBal = frmIBTopUpComplete.lblBBPaymentValue.text;
	availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	availableBal = availableBal.replace(/,/g, "")
	availableBal = parseFloat(availableBal.toString());
	if (finTxnStatus == "01") {
		availableBal = availableBal - (parseFloat(finTxnAmount) + parseFloat(finTxnFee));
		//GBLFINANACIALACTIVITYLOG.clearingStatus = "01";
	} else {
		//GBLFINANACIALACTIVITYLOG.clearingStatus = "02";
	}
	GBLFINANACIALACTIVITYLOG.finTxnBalance = availableBal;
	//GBLFINANACIALACTIVITYLOG.noteToRecipient = ""
	//GBLFINANACIALACTIVITYLOG.recipientMobile = "";
	//GBLFINANACIALACTIVITYLOG.recipientEmail = ""
	// 01 for success 02 for fail
	GBLFINANACIALACTIVITYLOG.finTxnStatus = finTxnStatus;
	GBLFINANACIALACTIVITYLOG.smartFlag = "N";
	//GBLFINANACIALACTIVITYLOG.clearingStatus = "00"
	GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmIBTopUpConfirmation.lblName.text;
	GBLFINANACIALACTIVITYLOG.fromAcctName = frmIBTopUpConfirmation.lblFromAccountName.text;
	var billerNameCmpCode = frmIBTopUpConfirmation.lblCompCodeTo.text;
	var billerNAmeSplit = billerNameCmpCode.split("(");
	GBLFINANACIALACTIVITYLOG.toAcctName = billerNAmeSplit[0];
	GBLFINANACIALACTIVITYLOG.toAcctNickname = frmIBTopUpConfirmation.lblNicknameTo.text;
	GBLFINANACIALACTIVITYLOG.errorCd = "0000";
	var yearPadd = new Date()
		.getFullYear()
		.toString()
		.substring(2, 2);
	var refNumber = finTxnRefId.substring(2, finTxnRefId.length - 2)
	GBLFINANACIALACTIVITYLOG.eventId = yearPadd + refNumber;
	GBLFINANACIALACTIVITYLOG.billerBalance = "0";
	GBLFINANACIALACTIVITYLOG.txnType = "005";
	GBLFINANACIALACTIVITYLOG.billerRef1 = frmIBTopUpConfirmation.lblRef1Value.text;
	GBLFINANACIALACTIVITYLOG.billerRef2 = "";
	GBLFINANACIALACTIVITYLOG.billerCustomerName = frmIBTopUpConfirmation.lblNicknameTo.text;
	GBLFINANACIALACTIVITYLOG.finTxnMemo = frmIBTopUpConfirmation.lblMNVal.text;
	GBLFINANACIALACTIVITYLOG.tellerId = "4";
	//GBLFINANACIALACTIVITYLOG.txnDescription = "done";
	GBLFINANACIALACTIVITYLOG.finLinkageId = "1";
	GBLFINANACIALACTIVITYLOG.billerCommCode = billerNAmeSplit[1].replace(")", "");
	GBLFINANACIALACTIVITYLOG.finSchduleRefId = "SU" + kony.string.sub(finTxnRefId, 2, finTxnRefId.length);
	financialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG);
}*/

function removeHyphenIB(accno) {
	if(accno ==  null || accno == undefined ||accno == "" || accno  == "null" || accno  == "undefined")
	return "";
	var AcctID;
	for (var i = 0; i < accno.length; i++) {
		if (accno[i] != "-") {
			if (AcctID == null) {
				AcctID = accno[i];
			} else {
				AcctID = AcctID + accno[i];
			}
		}
	}
	return AcctID;
}


function editTopUpPayment() {
	gblEditTopUp = true;
	frmIBTopUpLandingPage.imgArrowBiller.setVisibility(false);
	frmIBTopUpLandingPage.lblAccountNo.text = frmIBTopUpConfirmation.lblAccountNo.text;
	frmIBTopUpLandingPage.lblBalanceValue.text = frmIBTopUpConfirmation.lblBeforBal.text;
	frmIBTopUpLandingPage.lblNickTopUp.text = frmIBTopUpConfirmation.lblNicknameTo.text;
	frmIBTopUpLandingPage.lblCompCode.text = frmIBTopUpConfirmation.lblCompCodeTo.text;
	frmIBTopUpLandingPage.ref1.text = frmIBTopUpConfirmation.lblRef1.text;
	frmIBTopUpLandingPage.ref1value.text = frmIBTopUpConfirmation.lblRef1Value.text;
	frmIBTopUpLandingPage.txtAddBillerRef1.text = frmIBTopUpConfirmation.lblRef1Value.text;
	//below line is changed for CR - PCI-DSS masked Credit card no
	frmIBTopUpLandingPage.ref1valueMasked.text = frmIBTopUpConfirmation.lblRef1ValueMasked.text;
	//MIB-4884-Allow special characters for My Note and Note to recipient field
	frmIBTopUpLandingPage.textarea210136690395040.text = decodeHtmlTagChars(frmIBTopUpConfirmation.lblMNVal.text);
	frmIBTopUpLandingPage.lblAccountNo.text = frmIBTopUpConfirmation.lblAccountNo.text;
	frmIBTopUpLandingPage.hbxBiller.setVisibility(false);
	frmIBTopUpLandingPage.hbxImage.setVisibility(true);
	frmIBTopUpLandingPage.hbxRef1TopUpScreen.setVisibility(true);
	frmIBTopUpLandingPage.imgTopUp.src = frmIBTopUpConfirmation.imgbiller.src;
	frmIBTopUpLandingPage.hbxFutureTopup.setVisibility(false);
	
	if(gblPaynow){
	frmIBTopUpLandingPage.lblPayBillOnValue.text = frmIBTopUpConfirmation.lblTransferVal.text;
	frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(true);
	frmIBTopUpLandingPage.lblDatesFuture.setVisibility(false);
	frmIBTopUpLandingPage.lblrepeats.setVisibility(false);
	}else{
	
		if(frmIBTopUpConfirmation.lblEnDONValue.text == "-") {
			frmIBTopUpLandingPage.lblDatesFuture.text = frmIBTopUpConfirmation.lblStartOnValue.text;
			frmIBTopUpLandingPage.lblrepeats.text = "Repeat " + frmIBTopUpConfirmation.lblRepeatValue.text;
		}else{
			frmIBTopUpLandingPage.lblDatesFuture.text = frmIBTopUpConfirmation.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBTopUpConfirmation.lblEnDONValue.text;
			if(frmIBTopUpConfirmation.lblRepeatValue.text=="once" || frmIBTopUpConfirmation.lblRepeatValue.text=="Once"){
				frmIBTopUpLandingPage.lblrepeats.text = "Repeat " + frmIBTopUpConfirmation.lblRepeatValue.text;
			}else{
			frmIBTopUpLandingPage.lblrepeats.text = "Repeat " + frmIBTopUpConfirmation.lblRepeatValue.text + " " + frmIBTopUpConfirmation.lblExecuteValue.text + " " + kony.i18n.getLocalizedString("keyTimesMB");
			}
		} 
	
	frmIBTopUpLandingPage.lblPayBillOnValue.setVisibility(false);
	frmIBTopUpLandingPage.lblDatesFuture.setVisibility(true);
	frmIBTopUpLandingPage.lblrepeats.setVisibility(true);
	}
	
	frmIBTopUpLandingPage.show();
}

function OTPTimercallbackTopUp() {
	if(gblRetryCountRequestOTP >= gblOTPReqLimit){
		frmIBTopUpConfirmation.btnOTPReq.skin = btnIBREQotp;
		frmIBTopUpConfirmation.btnOTPReq.focusSkin =  btnIBREQotp
		frmIBTopUpConfirmation.btnOTPReq.hoverSkin =  btnIBREQotp
		frmIBTopUpConfirmation.btnOTPReq.onClick = "";
	}
	else{
		frmIBTopUpConfirmation.btnOTPReq.skin = btnIBREQotpFocus;
		frmIBTopUpConfirmation.btnOTPReq.focusSkin = btnIBREQotpFocus
		frmIBTopUpConfirmation.btnOTPReq.hoverSkin = btnIBREQotpFocus
		frmIBTopUpConfirmation.btnOTPReq.onClick = startTopUpPaymentOTPRequestService;
	}
	
	try {
		kony.timer.cancel("OtpTimer");
	} catch (e) {
		
	}
}
/**
 * description
 * @returns {}
 */

function callcrmProfileInqForTopUP() {
	var inputParam = {}
	invokeServiceSecureAsync("crmProfileInq", inputParam, callBackCRMProfileInqForTopUpLimit)
}
/**
 * description
 * @returns {}
 */

function callBackCRMProfileInqForTopUpLimit(status, resultTable) {
	if (status == 400) {
		if (resultTable["opstatus"] == 0) {
			if (resultTable["statusCode"] != "0") {
				
			} else {
				var ebMaxLimitAmtCurrent = parseFloat(resultTable["ebMaxLimitAmtCurrent"]);
				var ebAccuUsgAmtDaily = parseFloat(resultTable["ebAccuUsgAmtDaily"]);
				var dailyLimit = (ebMaxLimitAmtCurrent - ebAccuUsgAmtDaily).toFixed(2);
				if (dailyLimit.toString()
					.indexOf(".") != -1) {
					var index = dailyLimit.toString()
						.indexOf(".");
					var limitAmt = dailyLimit.toString()
						.substring(0, index + 3);
					dailyLimit = limitAmt;
				}
				frmIBTopUpCW.lblLimitValue.text = commaFormatted(dailyLimit.toString())+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				frmIBTopUpLandingPage.lblLimitValue.text = commaFormatted(dailyLimit.toString())+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				frmIBBillPaymentCW.lblDaliyLimtValue.text = commaFormatted(dailyLimit.toString())+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				frmIBBillPaymentLP.lblDaliyLimtValue.text = commaFormatted(dailyLimit.toString())+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
		}
	}
}
function onTopUpPaymentNextIB() {
   var currForm = kony.application.getCurrentForm();
	

	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		checkTopUpPaymentTokenFlag();
	} 
	if(gblTokenSwitchFlag == true && gblSwitchToken == false){
	
			frmIBTopUpConfirmation.hbxTokenEntry.setVisibility(true);
			frmIBTopUpConfirmation.tbxToken.setFocus(true);
			frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(false);
			//frmIBTopUpConfirmation.label476047582115288.text="Please Enter Token";
			  frmIBTopUpConfirmation.label476047582115288.text=kony.i18n.getLocalizedString("keyPleaseEnterToken");	
		
	} else if(gblTokenSwitchFlag ==false && gblSwitchToken == true) {
		    frmIBTopUpConfirmation.hbxTokenEntry.setVisibility(false);
		    frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
		    frmIBTopUpConfirmation.txtOtp.setFocus(true);
		    startTopUpPaymentOTPRequestService();
		    frmIBTopUpConfirmation.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
	}

}
function checkTopUpPaymentTokenFlag() {
		var inputParam = [];
		inputParam["crmId"] = gblcrmId;
		showLoadingScreen();
		invokeServiceSecureAsync("tokenSwitching", inputParam, ibTopUpPaymentTokenFlagCallbackfunction);
}
function ibTopUpPaymentTokenFlagCallbackfunction(status,callbackResponse){
		var currForm = kony.application.getCurrentForm().id;
		
		if(status==400){
				if(callbackResponse["opstatus"] == 0){
					dismissLoadingScreen();
					dismissLoadingScreenPopup();
					if(callbackResponse["deviceFlag"].length==0){
						
						//return
					}
					if ( callbackResponse["deviceFlag"].length > 0 ){
							var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
							var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
							var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
							if ( tokenFlag=="Y" && ( mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02' ){
										frmIBTopUpConfirmation.hbxTokenEntry.setVisibility(true);
										frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(false);
										frmIBTopUpConfirmation.tbxToken.setFocus(true);
										frmIBTopUpConfirmation.label476047582115288.text=kony.i18n.getLocalizedString("emptyToken");
								gblTokenSwitchFlag = true;
							} else {
									    frmIBTopUpConfirmation.hbxTokenEntry.setVisibility(false);
									    frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
									    frmIBTopUpConfirmation.txtOtp.setFocus(true);
									    startTopUpPaymentOTPRequestService();
									    frmIBTopUpConfirmation.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
								gblTokenSwitchFlag = false;
							}
						}
					 else
					{
					 
						    frmIBTopUpConfirmation.hbxTokenEntry.setVisibility(false);
						    frmIBTopUpConfirmation.hbxOTPEntry.setVisibility(true);
						    frmIBTopUpConfirmation.txtOtp.setFocus(true);
						    startTopUpPaymentOTPRequestService();
						    frmIBTopUpConfirmation.label476047582115288.text=kony.i18n.getLocalizedString("keyotpmsgreq");
					 }
						
				}
		}
		
} 
function OnlineTopUpInquiryAfterAmount() {
	//for Online Biller
	// added for integration

	var inputParam = {};
	
	var compCodeBillPay = frmIBTopUpLandingPage.lblCompCode.text; //frmIBTopUpLandingPage.segBiller.selectedItems[0].BillerCompCode;    
	
	var compcode = compCodeBillPay.substring(compCodeBillPay.lastIndexOf("(") + 1, compCodeBillPay.lastIndexOf(")"));
	
	if(compcode=="2605" || compcode=="2704")
	{
		var amount = parseFloat(removeCommos(frmIBTopUpLandingPage.txtAmount.text)).toFixed(2);
	}
	else
	{
		var amount = parseFloat(removeCommos(frmIBTopUpLandingPage.comboAmount.selectedKeyValue[1])).toFixed(2);
	}
	inputParam["TrnId"] = "";
	inputParam["BankId"] = "011";
	inputParam["BranchId"] = "0001";
	inputParam["BankRefId"] = "";
	inputParam["Amt"] = amount;
	// inputParam["OnlinePmtType"] = "";
	inputParam["Ref1"] = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	inputParam["Ref2"] = ""
	inputParam["Ref3"] = "";
	inputParam["Ref4"] = "";
	inputParam["MobileNumber"] = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	inputParam["compCode"] = compcode; //frmIBTopUpLandingPage.segBiller.selectedItems[0].BillerCompCode; 
	inputParam["isChannel"] = "IB";
	inputParam["commandType"] = "Inquiry";
	inputParam["BillerGroupType"] = "1";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("onlinePaymentInq", inputParam, topUpPaymentOnlinePaymentInqServiceCallBackIBAfterAmount);
	
	//frmIBTopUpLandingPage.txtAmount.text = ""; //set value
}
function topUpPaymentOnlinePaymentInqServiceCallBackIBAfterAmount(status, result) {
	
	
	 easyPassNameEN = "";
	var easyPassNameTH ;
	var topUpRefID ;
	var customerName = "CustomerName";
	
	if (status == 400) //success responce
	{
		if (result["opstatus"] == 0) {
			BankRefId = "";
			TranId = "";
			gblRef1 = "";
			gblRef2 = "";
			gblRef3 = "";
			gblRef4 = "";
			if(result["StatusCode"]!=0)
			{
			dismissLoadingScreenPopup();
			alert(""+result["errMsg"]);
			return;
			}
			
			if (result["StatusCode"] == 0) {
				
				BankRefId = result["OnlinePmtInqRs"][0]["BankRefId"];
				TranId = result["OnlinePmtInqRs"][0]["TrnId"];
				gblRef1 = result["OnlinePmtInqRs"][0]["Ref1"];
				gblRef2 = result["OnlinePmtInqRs"][0]["Ref2"];
				gblRef3 = result["OnlinePmtInqRs"][0]["Ref3"];
				gblRef4 = result["OnlinePmtInqRs"][0]["Ref4"];
				frmIBTopUpConfirmation.lblReferenceVal.text = BankRefId;
				frmIBTopUpComplete.lblCardBalance.text = commaFormatted(parseFloat(gblRef4).toFixed(2)) + "" + kony.i18n.getLocalizedString("currencyThaiBaht");
				
				
				generateTransferRefNoserviceIBTopUp();
				if(gblEasyPassCustomer){
				var MiscDataLength = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"].length;
				
				for (var i = 0; i < MiscDataLength; i++){
				
				 if(kony.string.equalsIgnoreCase(result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscName"], customerName)){
				 easyPassNameEN = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"][i]["MiscText"];
						 }
					}
				}
			}
			else{
			dismissLoadingScreenPopup();
			alert(""+result["errMsg"]);
			}
		} else {
			dismissLoadingScreenPopup();
			alert("error");
		}
	}
}


//>>>>>>>>>>>Composite service calll code starts here>>>>>>>>>>>>>>>>>>>>>>>//

	function setTopupAddService(inputParam) {
		var toAccType;
		var toAcctNo = "";
		var billerMethod = "";
		var billerGroupType = "";
		toAcctNo = gblToAccountKey; 
		billerMethod = gblBillerMethod;
		billerGroupType = gblbillerGroupType;
		GBLFINANACIALACTIVITYLOG.toAcctId = toAcctNo;
		var transferFee = frmIBTopUpConfirmation.lblFeeVal.text;
		var transferAmount = parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)).toFixed(2); //+ parseFloat(transferFee);
		var fromAcctID;
		var frmAcct = frmIBTopUpConfirmation.lblAccountNo.text;
		var fromAcctType;
		for (var i = 0; i < frmAcct.length; i++) {
			if (frmAcct[i] != "-") {
				if (fromAcctID == null) {
					fromAcctID = frmAcct[i];
				} else {
					fromAcctID = fromAcctID + frmAcct[i];
				}
			}
		}
		if (fromAcctID.length == 14) {
			fromAcctType = "SDA";
		} else {
			fourthDigit = fromAcctID.charAt(3);
			if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
				fromAcctType = "SDA";
				fromAcctID = "0000" + fromAcctID;
			} else {
				fromAcctType = "DDA";
			}
		}
		if (toAcctNo.length == 14) {
			toAccType = "SDA";
		} else {
			fourthDigit = toAcctNo.charAt(3);
			if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
				toAccType = "SDA";
				//toAcctNo = "0000" + toAcctNo;
			} else {
				toAccType = "DDA";
			}
		}
		var tranCode;
		// var fromAccType = frmIBTopUpConfirmation.lblFromAccountName.text;
		if (fromAcctType == "DDA") {
			tranCode = "8810";
		} else {
			tranCode = "8820";
		}
		var invoice;
		if (gblreccuringDisableAdd == "Y") {
			invoice = ""; //no ref 2 for topup?
		} else {
			invoice = ""; //workaround
		}
		var accTypeCode;
		if (toAccType == "DDA") {
			accTypeCode = "0000";
		} else {
			accTypeCode = "0200";
		}
		var TMB_BANK_FIXED_CODE = "0001";
		var TMB_BANK_CODE_ADD = "0011";
		var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
		var text = frmIBTopUpConfirmation.lblCompCodeTo.text;
		//var compcode = text.substring(text.lastIndexOf("(") + 1, text.indexOf(")"));
		inputParam["billpaymentAdd_fromAcctNo"] = fromAcctID
		inputParam["billpaymentAdd_fromAcctTypeValue"] = fromAcctType; //frmIBTopUpConfirmation.lbl "";
		inputParam["billpaymentAdd_toAcctNo"] = toAcctNo;
		inputParam["billpaymentAdd_toAcctTypeValue"] = toAccType;
		inputParam["billpaymentAdd_frmFiident"] = frmIBTopUpLandingPage.lblDummy.text;
		inputParam["billpaymentAdd_toFIIdent"] = fiident;
		inputParam["billpaymentAdd_transferAmt"] = transferAmount; 
		inputParam["billpaymentAdd_tranCode"] = tranCode;
		inputParam["billpaymentAdd_billPmtFee"] = parseFloat(transferFee); //transferFee.substring(0, transferFee.indexOf(" "));
		inputParam["billpaymentAdd_pmtRefIdent"] = frmIBTopUpConfirmation.lblRef1Value.text;
		inputParam["billpaymentAdd_invoiceNum"] = invoice;
		inputParam["billpaymentAdd_PostedDt"] = getTodaysDate();
		inputParam["billpaymentAdd_EPAYCode"] = "";
		inputParam["billpaymentAdd_CompCode"] = gblCompCode;
		inputParam["billpaymentAdd_channelName"] = "IB";
		inputParam["billpaymentAdd_billPay"] = "billPay";
		//invokeServiceSecureAsync("billpaymentAdd", inputParam, callTopupAddServiceCallBack);
}

function setOnlinePaymentAddTopUpService(inputParam) {
	
	var toAccType;
	var toAcctNo = "";
	var billerMethod = "";
	var billerGroupType = "";
	toAcctNo = gblToAccountKey; 
	billerMethod = gblBillerMethod;
	billerGroupType = gblbillerGroupType;
		
	GBLFINANACIALACTIVITYLOG.toAcctId = toAcctNo;
	
	
	var transferFee = parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text));
	var transferAmount = parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)).toFixed(2); //+ parseFloat(transferFee);
	var fromAcctID;
	var frmAcct = frmIBTopUpConfirmation.lblAccountNo.text;
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	if (toAcctNo.length == 14) {
		toAccType = "SDA";
	} else {
		fourthDigit = toAcctNo.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			toAccType = "SDA";
			//toAcctNo = "0000" + toAcctNo;
		} else {
			toAccType = "DDA";
		}
	}
	var tranCode;
	// var fromAccType = frmIBTopUpConfirmation.lblFromAccountName.text;
	if (fromAcctType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}

	var accTypeCode;
	if (toAccType == "DDA") {
		accTypeCode = "0000";
	} else {
		accTypeCode = "0200";
	}
	var TMB_BANK_FIXED_CODE = "0001";
	var TMB_BANK_CODE_ADD = "0011";
	var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
	var text = frmIBTopUpConfirmation.lblCompCodeTo.text;
	//var compcode = text.substring(text.lastIndexOf("(") + 1, text.indexOf(")"));
	inputParam["onlinePaymentAdd_fromAcctNo"] = fromAcctID
	inputParam["onlinePaymentAdd_fromAcctTypeValue"] = fromAcctType; //frmIBTopUpConfirmation.lbl "";
	inputParam["onlinePaymentAdd_toAcctNo"] = toAcctNo;
	inputParam["onlinePaymentAdd_toAcctType"] = toAccType;
	inputParam["onlinePaymentAdd_frmFiident"] = frmIBTopUpLandingPage.lblDummy.text;
	inputParam["onlinePaymentAdd_toFIIdent"] = fiident;
	inputParam["onlinePaymentAdd_transferAmt"] = transferAmount; //need to know this value;//"amt+payment fee"
	inputParam["onlinePaymentAdd_Amt"] = transferAmount;
	inputParam["onlinePaymentAdd_TranCode"] = tranCode;
	inputParam["onlinePaymentAdd_billPmtFee"] = transferFee; 
	inputParam["onlinePaymentAdd_pmtRefIdent"] = frmIBTopUpConfirmation.lblRef1Value.text;
	var invoice;
	if(gblCompCode == "0835" )
	{
		invoice = "01"+TranId;
	} else if(gblCompCode == "2136" || gblCompCode == "0472" || gblCompCode == "2704"){
		invoice = TranId;
	} else if(gblCompCode == "0185" || gblCompCode == "0254"|| gblCompCode == "0328"|| gblCompCode == "2135"|| gblCompCode == "0131" ){
		invoice = gblRef2;
	} else if (gblCompCode == "2151"){
		invoice = BankRefId;
	} else {
		invoice = "";
	}
	inputParam["onlinePaymentAdd_invoiceNum"] = invoice;
	inputParam["onlinePaymentAdd_PostedDt"] = getTodaysDate();
	inputParam["onlinePaymentAdd_EPAYCode"] = "";//"EPYS";
	inputParam["onlinePaymentAdd_compCode"] = gblCompCode;
	inputParam["onlinePaymentAdd_channelName"] = "IB";
	inputParam["onlinePaymentAdd_billPay"] = "billPay";
	var ref1 = "";
	var ref2 = "";
	var ref3 = "";
	var ref4 = "";
	var bankRefId = "";
	//  var  compcode=frmIBTopUpLandingPage.segBiller.selectedItems[0].BillerCompcode;
	if(gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185")
	{
		bankRefId = BankRefId;
	} else if(gblCompCode == "0014" || gblCompCode == "0249" || gblCompCode == "2270" ) {
	
		bankRefId = TranId;
	} else {
		bankRefId = "";
	}
	if(gblCompCode == "2151" || gblCompCode == "0131" || gblCompCode == "2135" || gblCompCode == "0328" || gblCompCode == "0254" || gblCompCode == "0185")
	{
		ref1 = gblRef1;
		ref2 = gblRef2;
		ref3 = gblRef3;
		if(gblCompCode == "2151")
			ref4 = ""; 
		else
			ref4 = gblRef4;
			 
	} else {
		ref1 = "";
		ref2 = "";
		ref3 = "";
		ref4 = ""; 
	} 
	
	var TranID = TranId; //(frmIBTopUpConfirmation.lblTRNVal.text).substring(2);
	var mobileNoTopup = frmIBTopUpConfirmation.lblRef1Value.text;
	// inputParam["SvcProvider"] = "";
	//   inputParam["ChannelName"] = "";
	inputParam["onlinePaymentAdd_RqUID"] = "";
	inputParam["onlinePaymentAdd_TrnId"] = TranID;
	inputParam["onlinePaymentAdd_BankId"] = "011"; //hardcoded.this will not change
	inputParam["onlinePaymentAdd_BranchId"] = "0001"; //hardcoded This will not change
	//  inputParam["TellerId"] = "";
	inputParam["onlinePaymentAdd_BankRefId"] = bankRefId;
	inputParam["onlinePaymentAdd_Ref1"] = ref1;
	inputParam["onlinePaymentAdd_Ref2"] = ref2;
	inputParam["onlinePaymentAdd_Ref3"] = ref3;
	inputParam["onlinePaymentAdd_Ref4"] = ref4;
	inputParam["onlinePaymentAdd_InterRegionFee"] = "0.00";
	inputParam["onlinePaymentAdd_MobileNumber"] = mobileNoTopup;
	inputParam["onlinePaymentAdd_Amt"] = transferAmount;
	//invokeServiceSecureAsync("onlinePaymentAdd", inputParam, topupOnlinePaymentAddServiceCallBack);
}

function setTopUpTransferAddService(inputParam, billerMethod) {
	//var billerMethod = frmIBTopUpLandingPage.segBiller.selectedItems[0].billerMethod;
	var toAcctNo = frmIBTopUpLandingPage.txtAddBillerRef1.text; //frmIBTopUpLandingPage["segBiller"]["selectedItems"][0]["ToAccountKey"];
	GBLFINANACIALACTIVITYLOG.toAcctId = toAcctNo;
	var fromAcctID = removeHyphenIB(frmIBTopUpConfirmation.lblAccountNo.text);
	//inputParam = {};
	var toAcctTypeValue;
	var tValue;
	var fValue;
	if (billerMethod == 2) {
		toAcctTypeValue = kony.i18n.getLocalizedString("CCA");
		tValue = 9;
		//toAcctNo = "00000000" + toAcctNo;
	} else if (billerMethod == 3) {
		toAcctTypeValue = kony.i18n.getLocalizedString("Loan");
		tValue = 5;
		toAcctNo =  toAcctNo;
	} else {
		toAcctTypeValue = kony.i18n.getLocalizedString("CDA");
		tValue = 3;
	}
	//if (fromAcctTypeValue == kony.i18n.getLocalizedString("SDA")) {
//		fValue = 2;
//		if (fromAcctID.length != 14) {
//			fromAcctID = "0000" + fromAcctID;
//		}
//	} else {
//		fValue = 1;
//	}
	var amount = parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)).toFixed(2);
	inputParam["TransferAdd_fromAcctNo"] = fromAcctID; //frmIBTopUpConfirmation.lblAccountNo.text;
	inputParam["TransferAdd_toAcctNo"] = toAcctNo; //frmIBTopUpConfirmation.lbl;
	inputParam["TransferAdd_transferAmt"] = amount;
	inputParam["TransferAdd_transferDate"] = getTodaysDate();;
	//invokeServiceSecureAsync("TransferAdd", inputParam, callTopUpTransferAddServiceCallBack);
}

function setPaymentAddTopUpServiceIB(inputParam, billerMethod) {
	//var indexOfSelectedRow = frmIBTopUpLandingPage.segBiller.selectedIndex[1];
	var toAccID = "";
	var billerID = "";
	toAccID = gblToAccountKey;
	billerID = gblBillerID;
	GBLFINANACIALACTIVITYLOG.toAcctId = toAccID;
	
	//var inputParam = {}
	
	//inputParam["crmId"]="001100000000000000000009925939";
	var toAcct = toAccID;

	var frmAcct = frmIBTopUpConfirmation.lblAccountNo.text;
	var fromAcctID;
	var fromAcctType;
	for (var i = 0; i < frmAcct.length; i++) {
		if (frmAcct[i] != "-") {
			if (fromAcctID == null) {
				fromAcctID = frmAcct[i];
			} else {
				fromAcctID = fromAcctID + frmAcct[i];
			}
		}
	}
	if (fromAcctID.length == 14) {
		fromAcctType = "SDA";
	} else {
		fourthDigit = fromAcctID.charAt(3);
		if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			fromAcctType = "SDA";
			fromAcctID = "0000" + fromAcctID;
		} else {
			fromAcctType = "DDA";
		}
	}
	var acctIdlen = toAcct.length;
	var toAccType;
	if (acctIdlen == 14) {
		fourthDigit = toAcct.charAt(7);
	} else {
		fourthDigit = toAcct.charAt(3);
	}
	if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9")
		toAccType = "SDA";
	else if (fourthDigit == "1")
		toAccType = "DDA";
	else if (fourthDigit == "3")
		toAccType = "CDA";
	else if (fourthDigit == "0" || fourthDigit == "5" || fourthDigit == "6")
		toAccType = "LOC";
	else
		toAccType = "CCA";
		
		if(billerMethod == "2")
			toAccType = "CCA";
		else if(billerMethod == "3")
			toAccType = "LOC";
		else if(billerMethod == "4")
			toAccType = "CDA";
			
	var tranCode;
	var f;
	var t;
	if (fromAcctType == "DDA") {
		f = "1";
	} else if (fromAcctType == "SDA") {
		f = "2";
	} else {
		f = "3";
	}
	if (toAccType == "DDA") {
		t = "1";
	} else if (toAccType == "SDA") {
		t = "2";
	} else if (toAccType == "CDA") {
		t = "3";
	} else if (toAccType == "LOC") {
		t = "5";
	} else if(toAccType == "CCA") {
		t = "9";
	}
	if(billerMethod == 0 || billerMethod == 1){
		tranCode = "88"+ f + "0";
	}
	else{
		tranCode = "88" + f + t;
	}
	//tranCode = "88" + f + t;
	if (gblrepeat == "1") {
		inputParam["doPmtAdd_NumInsts"] = frmIBTopUpConfirmation.lblExecuteValue.text;
	} else if (gblrepeat == "2") {
		inputParam["doPmtAdd_FinalDueDt"] = changeDateFormatForService(frmIBTopUpConfirmation.lblEnDONValue.text);
	}
	var pmtMethod = "BILL_PAYMENT"
    if(billerMethod == 1){
    	pmtMethod = "ONLINE_PAYMENT";
    	inputParam["doPmtAdd_PmtMiscType"] = "MOBILE" ;
    	inputParam["doPmtAdd_MiscText"] = frmIBTopUpConfirmation.lblRef1Value.text;
    }
	else if(billerMethod == 2 || billerMethod == 3 || billerMethod == 4){
		pmtMethod = "INTERNAL_PAYMENT"
	}
	var amount;
	var tranId = frmIBTopUpConfirmation.lblTRNVal.text;
	//amount = parseFloat(frmIBTopUpConfirmation.lblAmtVal.text) + parseFloat(frmIBTopUpConfirmation.lblFeeVal.text)
	amount = parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)).toFixed(2) ;//+ parseFloat(frmIBTopUpConfirmation.lblFeeVal.text)
	inputParam["doPmtAdd_rqUID"] = "";
	inputParam["doPmtAdd_toBankId"] = "011";
	inputParam["crmId"] = gblcrmId;
	inputParam["doPmtAdd_amt"] = amount;
	inputParam["doPmtAdd_fromAcct"] = fromAcctID;
	inputParam["doPmtAdd_fromAcctType"] = fromAcctType;
	inputParam["doPmtAdd_toAcct"] = toAccID; //frmIBTopUpConfirmation.lblFromAccName.text;
	inputParam["doPmtAdd_toAccTType"] = toAccType;
	inputParam["doPmtAdd_dueDate"] = changeDateFormatForService(frmIBTopUpConfirmation.lblStartOnValue.text.trim());
	inputParam["doPmtAdd_pmtRefNo1"] = frmIBTopUpConfirmation.lblRef1Value.text;
	inputParam["doPmtAdd_custPayeeId"] = billerID;
	inputParam["doPmtAdd_transCode"] = tranCode;
	inputParam["doPmtAdd_curCode"] = "THB";
	inputParam["doPmtAdd_channelId"]="IB";
	inputParam["doPmtAdd_memo"] = frmIBTopUpConfirmation.lblMNVal.text;
	inputParam["doPmtAdd_pmtMethod"] = pmtMethod; //"BILL_PAYMENT";
	inputParam["doPmtAdd_extTrnRefId"] = "SU" + kony.string.sub(tranId, 2, tranId.length);
	if(billPayFromSummary){
        	inputParam["doPmtAdd_fromAcctNickName"] = frmIBTopUpConfirmation.lblName.text;
        	billPayFromSummary =false;
        }
	//
	inputParam["doPmtAdd_toNickName"] = frmIBTopUpConfirmation.lblNicknameTo.text;
	
	var frequency = frmIBTopUpConfirmation.lblRepeatValue.text;
	inputParam["doPmtAdd_freq"] = frequency;
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
		inputParam["doPmtAdd_freq"] = "Daily";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
		inputParam["doPmtAdd_freq"] = "Weekly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
		inputParam["doPmtAdd_freq"] = "Monthly";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly")) || kony.string.equalsIgnoreCase(frequency, "Yearly")) {
		inputParam["doPmtAdd_freq"] = "Annually";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce")) || kony.string.equalsIgnoreCase(frequency, "Once")) {
        inputParam["doPmtAdd_freq"] = "once";
    }
	inputParam["doPmtAdd_desc"] = "";
	//invokeServiceSecureAsync("doPmtAdd", inputParam, callCustomBillAddTopUpServiceIBCallback);
}


function setTopUpServiceInputParams(inputParam) {
	var billerMethod = gblBillerMethod;
	inputParam["billerMethod"] = billerMethod;
	if (gblPaynow) {
		
		if (billerMethod == 0) {
			inputParam["billpaymentAdd_billerMethod"] = billerMethod;
			setTopupAddService(inputParam);
		}
		if (billerMethod == 1) {
			inputParam["onlinePaymentAdd_billerMethod"] = billerMethod;
			setOnlinePaymentAddTopUpService(inputParam);
		}
		if (billerMethod == 2 || billerMethod == 3 || billerMethod == 4) {
			inputParam["TransferAdd_billerMethod"] = billerMethod;
			setTopUpTransferAddService(inputParam, billerMethod);
		}
	} else {
		setPaymentAddTopUpServiceIB(inputParam, billerMethod);
	}
}

function setTopUpCrmProfileUpdateService(inputParam) {
	//inputParam = {};
	if(gblPaynow){
		
		var usageLimit = UsageLimit + parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)) ;//+ parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text));
		
		inputParam["crmProfileMod_ebAccuUsgAmtDaily"] = usageLimit;
	}
	else {
		var usageLimit = UsageLimit ;//- parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)) - parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text));
		inputParam["crmProfileMod_ebAccuUsgAmtDaily"] = usageLimit;
	}
	inputParam["crmProfileMod_actionType"] = "0"; //JAI CHECK: IT IS NOT THERE IN EXISTING CODE. I HAVE ADDED TO MAKE IT WORKING
	//inputParam["crmProfileMod_ibUserStatusId"] = "02"; //JAI CHECK: IT IS NOT THERE IN EXISTING CODE. I HAVE ADDED TO MAKE IT WORKING
	//showLoadingScreenPopup();
	//invokeServiceSecureAsync("crmProfileMod", inputParam, callTopUpCrmProfileUpdateServiceCallBack);
}

function setNotificationInputParams(inputParam){
	var platformChannel = gblDeviceInfo.name;
	//Start:NotificationAdd service input params

	inputParam["NotificationAdd_endDt"] =  frmIBTopUpConfirmation.lblEnDONValue.text;
	if (platformChannel == "thinclient"){
		inputParam["NotificationAdd_channelId"] = "Internet Banking";
	}else{
		inputParam["NotificationAdd_channelId"] = "Mobile Banking";
	}
	if(gblPaynow){
		inputParam["NotificationAdd_source"]="billpaymentnow";
	}else{
		inputParam["NotificationAdd_source"]="FutureBillPaymentAndTopUp";
		
			if(frmIBTopUpConfirmation.lblEnDONValue.text == "-") {
				inputParam["NotificationAdd_PaymentSchedule"] = frmIBTopUpConfirmation.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBTopUpConfirmation.lblRepeatValue.text;
			}else{
				inputParam["NotificationAdd_PaymentSchedule"] = frmIBTopUpConfirmation.lblStartOnValue.text + " " + kony.i18n.getLocalizedString("keyTo") + " " +  frmIBTopUpConfirmation.lblEnDONValue.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBTopUpConfirmation.lblRepeatValue.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmIBTopUpConfirmation.lblExecuteValue.text +" "+kony.i18n.getLocalizedString("keyTimesIB");
				inputParam["NotificationAdd_endDt"] = inputParam["NotificationAdd_endDt"] + " - " + frmIBTopUpConfirmation.lblExecuteValue.text +" "+kony.i18n.getLocalizedString("keyTimesIB");
			}
	}
	inputParam["NotificationAdd_startDt"] = frmIBTopUpConfirmation.lblStartOnValue.text
	inputParam["NotificationAdd_initiationDt"] = frmIBTopUpConfirmation.lblTransferVal.text
	inputParam["NotificationAdd_todayTime"] = "11:59:59 PM";	
	
	//Fixed defect MIB-1396 
	if(!gblPaynow) {
		if (gblClicked == "Daily") {
	            inputParam["NotificationAdd_recurring"] = "keyDaily";
		} else if (gblClicked == "Weekly") {
	            inputParam["NotificationAdd_recurring"] = "keyWeekly";
		} else if (gblClicked == "Monthly") {
	            inputParam["NotificationAdd_recurring"] = "keyMonthly";
		} else if (gblClicked == "Yearly") {
	            inputParam["NotificationAdd_recurring"] = "keyYearly";
		} else {
	            inputParam["NotificationAdd_recurring"] = "keyOnce";
		}
	}
	

	//inputParam["NotificationAdd_recurring"] =  "keyOnce";frmIBTopUpConfirmation.lblRepeatValue.text

	inputParam["NotificationAdd_mynote"]  = frmIBTopUpConfirmation.lblMNVal.text;
	inputParam["NotificationAdd_customerName"] = gblCustomerName;
	inputParam["NotificationAdd_fromAccount"]  = frmIBTopUpConfirmation.lblAccountNo.text; //from account no
	inputParam["NotificationAdd_fromAcctNick"]  = frmIBTopUpConfirmation.lblName.text; 
	inputParam["NotificationAdd_fromAcctName"]  = frmIBTopUpConfirmation.lblFromAccountName.text; 
	inputParam["NotificationAdd_billerNick"]  = frmIBTopUpConfirmation.lblNicknameTo.text; //nick
	//inputParam["NotificationAdd_billerName"] = frmIBTopUpConfirmation.lblCompCodeTo.text; //compcode
	inputParam["NotificationAdd_billerName"] = gblBillerCompCodeEN;
	inputParam["NotificationAdd_billerNameTH"] = gblBillerCompCodeTH;
 	inputParam["NotificationAdd_ref1EN"] = gblRef1LblEN+" : " + frmIBTopUpConfirmation.lblRef1Value.text;
    inputParam["NotificationAdd_ref1TH"] = gblRef1LblTH+" : " + frmIBTopUpConfirmation.lblRef1Value.text;

    inputParam["NotificationAdd_ref2EN"] = "";
    inputParam["NotificationAdd_ref2TH"] = "";
    
	inputParam["NotificationAdd_amount"] =  frmIBTopUpConfirmation.lblAmtVal.text; //amt value
	inputParam["NotificationAdd_fee"]  = frmIBTopUpConfirmation.lblFeeVal.text; //fee amount
	inputParam["NotificationAdd_date"]  = frmIBTopUpConfirmation.lblTransferVal.text //date
	inputParam["NotificationAdd_refID"]  = frmIBTopUpConfirmation.lblTRNVal.text; //ref no
	inputParam["NotificationAdd_memo"]  = frmIBTopUpConfirmation.lblMNVal.text; //note
	inputParam["NotificationAdd_deliveryMethod"] = "Email";
	inputParam["NotificationAdd_noSendInd"] = "0";
	inputParam["NotificationAdd_notificationType"] = "Email";
	inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
	inputParam["NotificationAdd_notificationSubject"] = "billpay";
	inputParam["NotificationAdd_notificationContent"] = "billPaymentDone";

}

function setActivitLoggingInputParams(inputParam){
	var platformChannel = gblDeviceInfo.name;
	//Start:Activity log input params
	//gblUserLockStatusIB = resulttable["IBUserStatusID"]; //JC
	var activityTypeID = "";
	var errorCode = "";
	var activityStatus = "";
	var deviceNickName = "";
	var activityFlexValues5 = "";
	var billerName=frmIBTopUpConfirmation.lblCompCodeTo.text;
	billerName=billerName.substring(0, billerName.indexOf("("));
	var activityFlexValues1 = billerName;
	var activityFlexValues2 = frmIBTopUpConfirmation.lblAccountNo.text;
	var activityFlexValues3 = frmIBTopUpConfirmation.lblRef1Value.text;
	var amt = parseFloat(removeCommos(frmIBTopUpConfirmation.lblAmtVal.text)).toFixed(2);
	var fee = parseFloat(removeCommos(frmIBTopUpConfirmation.lblFeeVal.text)).toFixed(2);
	var activityFlexValues4 = amt + "+" + fee;
	var logLinkageId = "";
	var flag = false;
	if (gblPaynow) {
		activityTypeID = "030";
	} else {
		activityTypeID = "031";
	}
	activityStatus = "01";
	
	inputParam["activationActivityLog_gblPaynow"] = gblPaynow;
	
	inputParam["activationActivityLog_activityTypeID"] = activityTypeID;
	//For failures the error code received from ECAS/XPRESS/Other systems. For Success case it should be "0000".
	inputParam["activationActivityLog_errorCd"] = errorCode; 
	//This status willl be "01" for success and "02" for failure of the actual event/instance
	inputParam["activationActivityLog_activityStatus"] = activityStatus;
	inputParam["activationActivityLog_deviceNickName"] = deviceNickName;
	inputParam["activationActivityLog_activityFlexValues1"] = activityFlexValues1;
	inputParam["activationActivityLog_activityFlexValues2"] = activityFlexValues2;
	inputParam["activationActivityLog_activityFlexValues3"] = activityFlexValues3;
	inputParam["activationActivityLog_activityFlexValues4"] = activityFlexValues4;
	inputParam["activationActivityLog_activityFlexValues5"] = activityFlexValues5;
	if (platformChannel == "thinclient"){
		inputParam["activationActivityLog_channelId"] = GLOBAL_IB_CHANNEL;
	}else{
		inputParam["activationActivityLog_channelId"] = GLOBAL_MB_CHANNEL;
	}
	inputParam["activationActivityLog_logLinkageId"] = logLinkageId;

}

function setInputParamsFinancialActivityLogForTopup(finTxnRefId, finTxnAmount, finTxnFee, finTxnStatus, inputParam) {

	GBLFINANACIALACTIVITYLOG.finTxnRefId = finTxnRefId;
	var tranCode;
	var fromAccType = frmIBTopUpLandingPage.lblAcctType.text;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	if (gblPaynow) {
		GBLFINANACIALACTIVITYLOG.activityTypeId = "030";
	} else {
		GBLFINANACIALACTIVITYLOG.activityTypeId = "031";
	}
	//02 is for mobile banking
	GBLFINANACIALACTIVITYLOG.channelId = "01"
	var fromAccountId = frmIBTopUpConfirmation.lblAccountNo.text;
	GBLFINANACIALACTIVITYLOG.fromAcctId = fromAccountId.replace(/-/g, "");
	// GBLFINANACIALACTIVITYLOG.toAcctId = "";
	GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";
	GBLFINANACIALACTIVITYLOG.finTxnAmount = parseFloat(removeCommos(finTxnAmount)).toFixed(2)
	GBLFINANACIALACTIVITYLOG.txnCd = tranCode;
	GBLFINANACIALACTIVITYLOG.finTxnFee = parseFloat(removeCommos(finTxnFee)).toFixed(2);
	//var availableBal = frmIBTopUpComplete.lblBBPaymentValue.text;
	//availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
	//availableBal = availableBal.replace(/,/g, "")
	//availableBal = parseFloat(availableBal.toString());
	var balanceBefore =  removeCommos(frmIBTopUpConfirmation.lblBeforBal.text);
	var availableBal = parseFloat(balanceBefore);//-parseFloat(frmIBTopUpConfirmation.lblAmtVal.text)-parseFloat(frmIBTopUpConfirmation.lblFeeVal.text) 
	if (finTxnStatus == "01") {
		availableBal = availableBal - (parseFloat(finTxnAmount) + parseFloat(finTxnFee));
		//GBLFINANACIALACTIVITYLOG.clearingStatus = "01";
	} else {
		//GBLFINANACIALACTIVITYLOG.clearingStatus = "02";
	}
	GBLFINANACIALACTIVITYLOG.finTxnBalance = availableBal;
	//GBLFINANACIALACTIVITYLOG.noteToRecipient = ""
	//GBLFINANACIALACTIVITYLOG.recipientMobile = "";
	//GBLFINANACIALACTIVITYLOG.recipientEmail = ""
	// 01 for success 02 for fail
	GBLFINANACIALACTIVITYLOG.finTxnStatus = finTxnStatus;
	GBLFINANACIALACTIVITYLOG.smartFlag = "N";
	//GBLFINANACIALACTIVITYLOG.clearingStatus = "00"
	GBLFINANACIALACTIVITYLOG.fromAcctNickname = frmIBTopUpConfirmation.lblName.text;
	GBLFINANACIALACTIVITYLOG.fromAcctName = frmIBTopUpConfirmation.lblFromAccountName.text;
	var billerNameCmpCode = frmIBTopUpConfirmation.lblCompCodeTo.text;
	var billerNAmeSplit = billerNameCmpCode.substring(0, billerNameCmpCode.lastIndexOf("("));;
	GBLFINANACIALACTIVITYLOG.toAcctName = billerNAmeSplit;
	GBLFINANACIALACTIVITYLOG.toAcctNickname = frmIBTopUpConfirmation.lblNicknameTo.text;
	GBLFINANACIALACTIVITYLOG.errorCd = "0000";
	var yearPadd = new Date()
		.getFullYear()
		.toString()
		.substring(2, 2);
	var refNumber = kony.string.sub(finTxnRefId, 2, finTxnRefId.length)
	GBLFINANACIALACTIVITYLOG.eventId = refNumber;
	GBLFINANACIALACTIVITYLOG.billerBalance = "0";
	GBLFINANACIALACTIVITYLOG.txnType = "005";
	GBLFINANACIALACTIVITYLOG.billerRef1 = frmIBTopUpConfirmation.lblRef1Value.text;
	GBLFINANACIALACTIVITYLOG.billerRef2 = "";
	
	if(gblCompCode == '2151'){
		GBLFINANACIALACTIVITYLOG.billerCustomerName = frmIBTopUpConfirmation.lblEasyPassName.text;
	}else {
		GBLFINANACIALACTIVITYLOG.billerCustomerName = frmIBTopUpConfirmation.lblNicknameTo.text;
	}
	GBLFINANACIALACTIVITYLOG.finTxnMemo = frmIBTopUpConfirmation.lblMNVal.text;
	GBLFINANACIALACTIVITYLOG.tellerId = "4";
	//GBLFINANACIALACTIVITYLOG.txnDescription = "done";
	GBLFINANACIALACTIVITYLOG.finLinkageId = "1";
	GBLFINANACIALACTIVITYLOG.billerCommCode = gblCompCode;//billerNAmeSplit[1].replace(")", "");
	GBLFINANACIALACTIVITYLOG.finSchduleRefId = "SU" + kony.string.sub(finTxnRefId, 2, finTxnRefId.length);

	setInputParamsForFinancialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG,inputParam);
}

function setInputParamsForFinancialActivityLogServiceCall(CRMFinancialActivityLogAddRequest, inputParam) {
	//inputParam = {};
	//inputParam["ipAddress"] = ""; 
	//inputParam["financialActivityLog_crmId"] = CRMFinancialActivityLogAddRequest.crmId;
	inputParam["financialActivityLog_finTxnRefId"] = CRMFinancialActivityLogAddRequest.finTxnRefId;
	inputParam["financialActivityLog_finTxnDate"] = CRMFinancialActivityLogAddRequest.finTxnDate;
	inputParam["financialActivityLog_activityTypeId"] = CRMFinancialActivityLogAddRequest.activityTypeId;
	inputParam["financialActivityLog_txnCd"] = CRMFinancialActivityLogAddRequest.txnCd;
	inputParam["financialActivityLog_tellerId"] = CRMFinancialActivityLogAddRequest.tellerId;
	inputParam["financialActivityLog_txnDescription"] = CRMFinancialActivityLogAddRequest.txnDescription;
	inputParam["financialActivityLog_finLinkageId"] = CRMFinancialActivityLogAddRequest.finLinkageId;
	inputParam["financialActivityLog_channelId"] = CRMFinancialActivityLogAddRequest.channelId;
	inputParam["financialActivityLog_fromAcctId"] = CRMFinancialActivityLogAddRequest.fromAcctId;
	inputParam["financialActivityLog_toAcctId"] = CRMFinancialActivityLogAddRequest.toAcctId;
	inputParam["financialActivityLog_toBankAcctCd"] = CRMFinancialActivityLogAddRequest.toBankAcctCd;
	inputParam["financialActivityLog_finTxnAmount"] = CRMFinancialActivityLogAddRequest.finTxnAmount;
	inputParam["financialActivityLog_finTxnFee"] = CRMFinancialActivityLogAddRequest.finTxnFee;
	inputParam["financialActivityLog_finTxnBalance"] = CRMFinancialActivityLogAddRequest.finTxnBalance;
	inputParam["financialActivityLog_finTxnMemo"] = CRMFinancialActivityLogAddRequest.finTxnMemo;
	inputParam["financialActivityLog_billerRef1"] = CRMFinancialActivityLogAddRequest.billerRef1;
	inputParam["financialActivityLog_billerRef2"] = CRMFinancialActivityLogAddRequest.billerRef2;
	inputParam["financialActivityLog_noteToRecipient"] = CRMFinancialActivityLogAddRequest.noteToRecipient;
	inputParam["financialActivityLog_recipientMobile"] = CRMFinancialActivityLogAddRequest.recipientMobile;
	inputParam["financialActivityLog_recipientEmail"] = CRMFinancialActivityLogAddRequest.recipientEmail;
	inputParam["financialActivityLog_finTxnStatus"] = CRMFinancialActivityLogAddRequest.finTxnStatus;
	inputParam["financialActivityLog_smartFlag"] = CRMFinancialActivityLogAddRequest.smartFlag;
	inputParam["financialActivityLog_clearingStatus"] = CRMFinancialActivityLogAddRequest.clearingStatus;
	inputParam["financialActivityLog_finSchduleRefId"] = CRMFinancialActivityLogAddRequest.finSchduleRefId;
	inputParam["financialActivityLog_billerCommCode"] = CRMFinancialActivityLogAddRequest.billerCommCode;
	inputParam["financialActivityLog_fromAcctName"] = CRMFinancialActivityLogAddRequest.fromAcctName;
	inputParam["financialActivityLog_fromAcctNickname"] = CRMFinancialActivityLogAddRequest.fromAcctNickname;
	inputParam["financialActivityLog_toAcctName"] = CRMFinancialActivityLogAddRequest.toAcctName;
	inputParam["financialActivityLog_toAcctNickname"] = CRMFinancialActivityLogAddRequest.toAcctNickname;
	inputParam["financialActivityLog_billerCustomerName"] = CRMFinancialActivityLogAddRequest.billerCustomerName;
	inputParam["financialActivityLog_billerBalance"] = CRMFinancialActivityLogAddRequest.billerBalance;
	inputParam["financialActivityLog_activityRefId"] = CRMFinancialActivityLogAddRequest.activityRefId;
	inputParam["financialActivityLog_eventId"] = CRMFinancialActivityLogAddRequest.eventId;
	inputParam["financialActivityLog_errorCd"] = CRMFinancialActivityLogAddRequest.errorCd;
	inputParam["financialActivityLog_txnType"] = CRMFinancialActivityLogAddRequest.txnType;
	inputParam["financialActivityLog_tdInterestAmount"] = CRMFinancialActivityLogAddRequest.tdInterestAmount;
	inputParam["financialActivityLog_tdTaxAmount"] = CRMFinancialActivityLogAddRequest.tdTaxAmount;
	inputParam["financialActivityLog_tdPenaltyAmount"] = CRMFinancialActivityLogAddRequest.tdPenaltyAmount;
	inputParam["financialActivityLog_tdNetAmount"] = CRMFinancialActivityLogAddRequest.tdNetAmount;
	inputParam["financialActivityLog_tdMaturityDate"] = CRMFinancialActivityLogAddRequest.tdMaturityDate;
	inputParam["financialActivityLog_remainingFreeTxn"] = CRMFinancialActivityLogAddRequest.remainingFreeTxn;
	inputParam["financialActivityLog_toAccountBalance"] = CRMFinancialActivityLogAddRequest.toAccountBalance;
	inputParam["financialActivityLog_sendToSavePoint"] = CRMFinancialActivityLogAddRequest.sendToSavePoint;
	inputParam["financialActivityLog_openTdTerm"] = CRMFinancialActivityLogAddRequest.openTdTerm;
	inputParam["financialActivityLog_openTdInterestRate"] = CRMFinancialActivityLogAddRequest.openTdInterestRate;
	inputParam["financialActivityLog_openTdMaturityDate"] = CRMFinancialActivityLogAddRequest.openTdMaturityDate;
	inputParam["financialActivityLog_affiliatedAcctNickname"] = CRMFinancialActivityLogAddRequest.affiliatedAcctNickname;
	inputParam["financialActivityLog_affiliatedAcctId"] = CRMFinancialActivityLogAddRequest.affiliatedAcctId;
	inputParam["financialActivityLog_beneficialFirstname"] = CRMFinancialActivityLogAddRequest.beneficialFirstname;
	inputParam["financialActivityLog_beneficialLastname"] = CRMFinancialActivityLogAddRequest.beneficialLastname;
	inputParam["financialActivityLog_relationship"] = CRMFinancialActivityLogAddRequest.relationship;
	inputParam["financialActivityLog_percentage"] = CRMFinancialActivityLogAddRequest.percentage;
	inputParam["financialActivityLog_finFlexValues1"] = CRMFinancialActivityLogAddRequest.finFlexValues1;
	inputParam["financialActivityLog_finFlexValues2"] = CRMFinancialActivityLogAddRequest.finFlexValues2;
	inputParam["financialActivityLog_finFlexValues3"] = CRMFinancialActivityLogAddRequest.finFlexValues3;
	inputParam["financialActivityLog_finFlexValues4"] = CRMFinancialActivityLogAddRequest.finFlexValues4;
	inputParam["financialActivityLog_finFlexValues5"] = CRMFinancialActivityLogAddRequest.finFlexValues5;
	inputParam["financialActivityLog_dueDate"] = CRMFinancialActivityLogAddRequest.dueDate;
	inputParam["financialActivityLog_beepAndBillTxnId"] = CRMFinancialActivityLogAddRequest.beepAndBillTxnId;
	
}
//>>>>>>>>>>>Composite service calll code ends here>>>>>>>>>>>>>>>>>>>>>>>//

function clearAllTopUpGlobalsIB(){
	gblBillerMethod=""; 
	gblCompCode="";
	gblToAccountKey="";
	gblBillerID="";
	gblbillerGroupType="";
	gblRef1LblEN="";
	gblRef1LblTH="";
	gblRef2LblEN="";
	gblRef2LblTH="";
	gblBillerCompCodeEN="";
	gblBillerCompCodeTH="";
	gblreccuringDisableAdd="";
	gblreccuringDisablePay="";
	gblNickNameTH="";
	gblNickNameEN="";
	hideShowScheduleNicknameTopUpIB(false);
}

function getTopUpBillerNickName(){
	var topUpNickname="";
	var ref1 = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	topUpNickname = getBillerNickNameFromMyBills(gblMyBillList,gblCompCode,ref1,"")
	if("" == topUpNickname || undefined == topUpNickname){
		topUpNickname = frmIBTopUpLandingPage.lblCompCode.text; //sending biller name compcode here
	}
	frmIBTopUpLandingPage.lblNickTopUp.text = topUpNickname;
	
	//Hiding or Showing Biller Nickname related based on flag - gblBillerPresentInMyBills being set in function getTopUpBillerNickName();
	hideShowScheduleNicknameTopUpIB(!gblBillerPresentInMyBills && !gblPaynow);
}

function validateFleetCardTopUpIB() {
	var inputparam = {};
	inputparam["cardId"] = frmIBTopUpLandingPage.txtAddBillerRef1.text;
	//inputparam["waiverCode"] = WAIVERCODE;
	inputparam["tranCode"] = TRANSCODEUN;
	invokeServiceSecureAsync("creditcardDetailsInqNonSec", inputparam, validateFleetCardCallBackIB);
}
/*
*************************************************************************************
		Module	: validateCreditCardCallBack
		Author  : Kony
		Purpose : Validation Biller Method for credit card
****************************************************************************************
*/

function validateFleetCardCallBackIB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			if(resulttable["StatusCode"]!=0)
			{	
				dismissLoadingScreenPopup();	
				alert(resulttable["errMsg"]);
				return false;
			} 
			else{
				//calling next service
				frmIBTopUpConfirmation.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                checkTopUpCrmProfileInqIB();
			}
		}	
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}

function validateFleetCardTopUpMB() {
	showLoadingScreen();
	var inputparam = {};
	inputparam["cardId"] = frmTopUp.lblRef1Value.text;
	//inputparam["waiverCode"] = WAIVERCODE;
	inputparam["tranCode"] = TRANSCODEUN;
	invokeServiceSecureAsync("creditcardDetailsInqNonSec", inputparam, validateFleetCardCallBackMB);
}
/*
*************************************************************************************
		Module	: validateCreditCardCallBack
		Author  : Kony
		Purpose : Validation Biller Method for credit card
****************************************************************************************
*/

function validateFleetCardCallBackMB(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			if(resulttable["StatusCode"]!=0)
			{	
				dismissLoadingScreen();	
				alert(resulttable["errMsg"]);
				return false;
			} 
			else{
				//calling next service
				frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text = "0.00" + kony.i18n.getLocalizedString("currencyThaiBaht");
                checkBillPaymentCrmProfileInqMB();
			}
		}	
	} else {
		if (status == 300) {
			dismissLoadingScreen();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}