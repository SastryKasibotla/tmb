gblQRPayData = {};
gblQRPayDefaultAcct = "";
gblTotalPinAttemptsTransfers = 3;
function preShowFrmQRSelectAccount() {
  
  if(gblPlatformName == "iPhone"  || gblDeviceInfo["name"] == "iPad"|| gblPlatformName == "iPhone Simulator" ){
    frmQRSelectAccount.segTransFrm.viewConfig = {coverflowConfig: {isCircular: true}};
  }

  frmQRSelectAccount.lblHdrTxt.text = kony.i18n.getLocalizedString("lb_QRselectAccount");
  frmQRSelectAccount.LabelShowDefaultAcct.text = kony.i18n.getLocalizedString("msgQRDefaultAccount");
  frmQRSelectAccount.LinkSetDefaultAcct.text = kony.i18n.getLocalizedString("msgSetQRDefaultAccount");
  frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("msg_QRInsufficientFund");
  
  frmQRSelectAccount.flexRefNumber.setVisibility(false);
  frmQRSelectAccount.flexAccountNum.setVisibility(true);
  
  frmQRSelectAccount.LabelAccountName.text = gblQRPayData["toAccountName"];
  if(gblQRPayData["mobileNo"] !== undefined && gblQRPayData["mobileNo"] !== ""){
    frmQRSelectAccount.LabelAccountNo.text = formatMobileNumber(gblQRPayData["mobileNo"]);
  } else if(gblQRPayData["cId"] !== undefined && gblQRPayData["cId"] !==""){
    frmQRSelectAccount.LabelAccountNo.text = formatCitizenID(gblQRPayData["cId"]);
  } else if(gblQRPayData["eWallet"] !== undefined && gblQRPayData["eWallet"] !==""){
    frmQRSelectAccount.LabelAccountNo.text = formatEWalletNumber(gblQRPayData["eWallet"]);
  }else if(gblQRPayData["billerID"] !== undefined && gblQRPayData["billerID"] !==""){
    kony.print("Inside Bill Payment");
    frmQRSelectAccount.flexRefNumber.setVisibility(true);
    frmQRSelectAccount.flexAccountNum.setVisibility(false);
    frmQRSelectAccount.LabelAccountName.text = gblQRDefaultAccountResponse["pp_toAcctName"];
    var lblRef1 = gblQRDefaultAccountResponse["labelRef1EN"];
    lblRef1 = lblRef1.search(":");
    if(lblRef1 == -1){
     frmQRSelectAccount.lblReferenceNum1.text = gblQRDefaultAccountResponse["labelRef1EN"] + ": " + gblQRPayData["ref1"];
    }else{
     frmQRSelectAccount.lblReferenceNum1.text = gblQRDefaultAccountResponse["labelRef1EN"] + " " + gblQRPayData["ref1"];
    }
    if(gblQRDefaultAccountResponse["labelRef2EN"]!= "" && gblQRDefaultAccountResponse["labelRef2EN"]!= undefined){
      var lblRef2 = gblQRDefaultAccountResponse["labelRef2EN"];
      lblRef2 = lblRef2.search(":");
      if(lblRef2 == -1){
        frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+": "+gblQRPayData["ref2"];
      }else{
        frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+" "+gblQRPayData["ref2"];
      }
    }else{
       frmQRSelectAccount.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
     }
     if(gblref2Val){
       frmQRSelectAccount.lblReferenceNum2.setVisibility(true);
     }else{
       frmQRSelectAccount.lblReferenceNum2.setVisibility(false);
     }
  }

  //=============Chagned by mayank========================Start
  var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
  frmQRSelectAccount.LabelAmount.text = formattedAmount[0];
  frmQRSelectAccount.LabelAmountDigits.text = formattedAmount[1];
  //frmQRSelectAccount.LabelAmount.text = commaFormatted(gblQRPayData["transferAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  //=============Cahgned by mayank====================================End
  if(gblQRPayData["fee"] !== undefined && gblQRPayData["fee"] != "" && gblQRPayData["fee"] != "0.00"){
    frmQRSelectAccount.LabelFee.setVisibility(true);
    frmQRSelectAccount.LabelFee.text = kony.i18n.getLocalizedString("keyFee") + gblQRPayData["fee"] + " " + kony.i18n.getLocalizedString("keyBaht");
  } else {
    frmQRSuccess.LabelFee.setVisibility(false);
  }

  frmQRSelectAccount.ButtonPayNow.setVisibility(true);
  frmQRSelectAccount.HBoxError.setVisibility(false);

  frmQRSelectAccount.LabelShowDefaultAcct.setVisibility(true);
  frmQRSelectAccount.LinkSetDefaultAcct.setVisibility(false);
  frmQRSelectAccount.ButtonPayNow.text = kony.i18n.getLocalizedString("btnPayQR");
  checkAmountQRPay();
}

function checkAmountQRPay() {
  enteredAmountF = parseFloat(gblQRPayData["transferAmt"]);
  avaiLength = frmQRSelectAccount.segTransFrm.selectedItems[0].lblBalance.length;
  avaiAmount = frmQRSelectAccount.segTransFrm.selectedItems[0].lblBalance;
  avaiAmount = avaiAmount.replace(/,/g, "");
  avaiAmount = avaiAmount.substring(0, avaiLength - 2);
  avaiAmountF = parseFloat(avaiAmount);
  if (avaiAmountF >= enteredAmountF) {
    frmQRSelectAccount.ButtonPayNow.setVisibility(true);
    frmQRSelectAccount.HBoxError.setVisibility(false);
    if(gblQRPayData["fee"] !== undefined && gblQRPayData["fee"] != "" && gblQRPayData["fee"] != "0.00"){
      frmQRSelectAccount.LabelFee.setVisibility(true);
    } else {
      frmQRSelectAccount.LabelFee.setVisibility(false);
    }
    if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
      promptpayETEInquiryModule();
    }else{
      checkOnUsPromptPayinqServiceQRPay();
    } 
    
  } else {
    frmQRSelectAccount.HBoxError.setVisibility(true);
    frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("msg_QRInsufficientFund");
    frmQRSelectAccount.LabelFee.setVisibility(false);
    frmQRSelectAccount.ButtonPayNow.setVisibility(false);
  }
}


function loadMapforQrPay() {
  checkPermissionsInQR();
  /*
  kony.print("loadMapforQrPay Post Login");
  var inputParam = {}
  //gblSelTransferMode = 2;
  showLoadingScreen();
  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
    inputParam["billPayInd"] = "billPayInd";
  }else{
    inputParam["transferFlag"] = "true";
  } 
  invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackloadMapforQrPay);
  */
}



function callBackloadMapforQrPay(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      kony.print("Success");
      var initDPParam = resulttable["initDPParam"];
      if(initDPParam!=undefined&&initDPParam!=null&&initDPParam!=""){
        gblDPPk = resulttable["initDPParam"][0]["pk"];
		gblDPRandNumber = resulttable["initDPParam"][0]["randomNumber"];
      }
      dismissLoadingScreen();
      checkPermissionsInQR();
    } else {
      dismissLoadingScreen();
      if (resulttable["errMsg"] == "No Valid Accounts Available for Transfer") {
        alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"));
      } else {
        alert(" " + resulttable["errMsg"]);
      }
    }
  }
}


function onSelectFromAccntQRPay() {
  var inputParam = {}
  //gblSelTransferMode = 2;
  showLoadingScreen();
  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
    inputParam["billPayInd"] = "billPayInd";
  }else if(gblQRPayData["QR_TransType"]=="QR_Transfer"){
    inputParam["transferFlag"] = "true";
  }
  inputParam["QRFlag"] = "Y";
  invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackSelectFromAcctForQRPayCallBack);
}

function callBackSelectFromAcctForQRPayCallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      frmQRSelectAccount.segTransFrm.removeAll();
      var setyourIDTransfer = resulttable["setyourIDTransfer"];
      GLOBAL_TODAY_DATE = resulttable["TODAY_DATE"];
      var initDPParam = resulttable["initDPParam"];
      if(initDPParam!=undefined&&initDPParam!=null&&initDPParam!=""){
        gblDPPk = resulttable["initDPParam"][0]["pk"];
		gblDPRandNumber = resulttable["initDPParam"][0]["randomNumber"];
      }
      var fromData = []
      if (resulttable.custAcctRec.length == undefined) {
        dismissLoadingScreen();
        alert("Accounts are not eligible for Transfer or either Hidden");
        return;
      }
      gblSelectedRecipentName = gblCustomerName;
      var allowed = false;
      var j = 1 /*** below are configurable params driving from Backend and get Cached. **/
      gblMaxTransferORFT = resulttable.ORFTTransLimit;
      gblMaxTransferSMART = resulttable.SMARTTransLimit;
      gblTransORFTSplitAmnt = resulttable.ORFTTransSplitAmnt;
      gblTransSMARTSplitAmnt = resulttable.SMARTTransAmnt;
      gblLimitORFTPerTransaction = resulttable.ORFTTransSplitAmnt;
      gblLimitSMARTPerTransaction = resulttable.SMARTTransAmnt; /*** till hereee  ***/
      var ORFTRange1Lower = resulttable.ORFTRange1Lower
      var ORFTRange1Higher = resulttable.ORFTRange1Higher
      var SMARTRange1Higher = resulttable.SMARTRange1Higher
      var SMARTRange1Lower = resulttable.SMARTRange1Lower
      var ORFTRange2Lower = resulttable.ORFTRange2Lower
      var ORFTRange2Higher = resulttable.ORFTRange2Higher
      var SMARTRange2Higher = resulttable.SMARTRange2Higher
      var SMARTRange2Lower = resulttable.SMARTRange2Lower
      var ORFTSPlitFeeAmnt1 = resulttable.ORFTSPlitFeeAmnt1
      var ORFTSPlitFeeAmnt2 = resulttable.ORFTSPlitFeeAmnt2
      var SMARTSPlitFeeAmnt1 = resulttable.SMARTSPlitFeeAmnt1
      var SMARTSPlitFeeAmnt2 = resulttable.SMARTSPlitFeeAmnt2
      var WithinBankOwnAccLimit = resulttable.WithinBankOwnAccLimit
      var WithinBankOwnAccLimitTransaction = resulttable.WithinBankOwnAccLimitTransaction
      gblXerSplitData = [];
      var temp1 = [];
      var temp = {
        ORFTRange1Lower: resulttable.ORFTRange1Lower,
        ORFTRange1Higher: resulttable.ORFTRange1Higher,
        SMARTRange1Higher: resulttable.SMARTRange1Higher,
        SMARTRange1Lower: resulttable.SMARTRange1Lower,
        ORFTRange2Lower: resulttable.ORFTRange2Lower,
        ORFTRange2Higher: resulttable.ORFTRange2Higher,
        SMARTRange2Higher: resulttable.SMARTRange2Higher,
        SMARTRange2Lower: resulttable.SMARTRange2Lower,
        ORFTSPlitFeeAmnt1: resulttable.ORFTSPlitFeeAmnt1,
        ORFTSPlitFeeAmnt2: resulttable.ORFTSPlitFeeAmnt2,
        SMARTSPlitFeeAmnt1: resulttable.SMARTSPlitFeeAmnt1,
        SMARTSPlitFeeAmnt2: resulttable.SMARTSPlitFeeAmnt2,
        WithinBankOwnAccLimit: resulttable.WithinBankOwnAccLimit,
        WithinBankOwnAccLimitTransaction: resulttable.WithinBankOwnAccLimitTransaction
      }
      kony.table.insert(temp1, temp)
      gblXerSplitData = temp1;
      var nonCASAAct = 0;
      gblTransfersOtherAccounts = [];
      for (var i = 0; i < resulttable.custAcctRec.length; i++) {
        var accountStatus = resulttable["custAcctRec"][i].acctStatus;
        var anyIDEligible = true;
        if (gblfromCalender && frmTransfersAckCalendar.lblMobileNumber.isVisible || (gblSelTransferMode != 1)) { // if p2 p show only anyID eligible
          anyIDEligible = resulttable.custAcctRec[i].anyIdAllowed == "Y";
        }
        if (accountStatus.indexOf("Active") == -1) {
          nonCASAAct = nonCASAAct + 1;
        }
        var joinType = resulttable.custAcctRec[i].partyAcctRelDesc;
        var AccStatCde = resulttable.custAcctRec[i].personalisedAcctStatusCode;
        if (AccStatCde == undefined || AccStatCde == "01" || AccStatCde == "") {
          if (joinType == "PRIJNT" || joinType == "SECJAN" || joinType == "OTHJNT" || joinType == "SECJNT") {
            //do nothing
          } else {
            var icon = "";
            var iconcategory = "";
            var randomnum = Math.floor((Math.random() * 10000) + 1);
            icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + "NEW_" + resulttable.custAcctRec[i]["ICON_ID"] + "&modIdentifier=PRODICON&rr="+randomnum;
            iconcategory = resulttable.custAcctRec[i]["ICON_ID"];
            // added new From account widget
            if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
              var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew1, icon)
              } else if (iconcategory == "ICON-03") {
                var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew2, icon)
                } else if (iconcategory == "ICON-04") {
                  var temp = createSegmentRecord(resulttable.custAcctRec[i], hbxSliderNew3, icon)
                  }
            if (anyIDEligible && accountStatus.indexOf("Active") >= 0) kony.table.insert(fromData, temp[0]);
          }
        } // if end
      } //for
      // for other accounts
      if (nonCASAAct == resulttable.custAcctRec.length) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onclickActivationCompleteStart);
        return false;
      }
      //Adding this condition for BillPayment 
      if(gblQRPayData["QR_TransType"]!="QR_Bill_Payment"){
            for (var i = 0; i < resulttable.OtherAccounts.length; i++) {
            var prdCode = resulttable.OtherAccounts[i].productID;
            if (prdCode == 290) {
              var accountId = kony.string.replace(resulttable.OtherAccounts[i].accId, "-", "");
              if (accountId.length == 14) {
                resulttable.OtherAccounts[i].accId = accountId.substring(4, 14);
              }
              var temp = [{
                custName: resulttable.OtherAccounts[i].accountName,
                acctNo: addHyphenMB(resulttable.OtherAccounts[i].accId),
                nickName: resulttable.OtherAccounts[i].acctNickName,
                productName: productionNameTrun(resulttable.OtherAccounts[i].productNmeEN),
                productNameTH: productionNameTrun(resulttable.OtherAccounts[i].productNmeTH)
              }]
              kony.table.insert(gblTransfersOtherAccounts, temp[0])
            }
          }
      }
      
      if (gblDeviceInfo["name"] == "android") {
        if (fromData.length == 1) {
          frmQRSelectAccount.segTransFrm.viewConfig = {
            "coverflowConfig": {
              "rowItemRotationAngle": 0,
              "isCircular": false,
              "spaceBetweenRowItems": 10,
              "projectionAngle": 90,
              "rowItemWidth": 80
            }
          };
        } else {
          frmQRSelectAccount.segTransFrm.viewConfig = {
            "coverflowConfig": {
              "rowItemRotationAngle": 0,
              "isCircular": true,
              "spaceBetweenRowItems": 10,
              "projectionAngle": 90,
              "rowItemWidth": 80
            }
          };
        }
      }
      frmQRSelectAccount.segTransFrm.widgetDataMap = {
        lblACno: "lblACno",
        lblAcntType: "lblAcntType",
        img1: "img1",
        lblCustName: "lblCustName",
        lblBalance: "lblBalance",
        lblActNoval: "lblActNoval",
        lblDummy: "lblDummy",
        lblSliderAccN1: "lblSliderAccN1",
        lblSliderAccN2: "lblSliderAccN2",
        lblRemainFee: "lblRemainFee",
        lblRemainFeeValue: "lblRemainFeeValue"
      }
      gblNoOfFromAcs = fromData.length;
      if (gblNoOfFromAcs == 0) {
        showAlert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"));
        dismissLoadingScreen();
        return false;
      }
      var selectedIndex = 0;

      if (gblQRPayDefaultAcct.length == 14) {
        gblQRPayDefaultAcct = gblQRPayDefaultAcct.substring(4);
      }
      if (isNotBlank(gblQRPayDefaultAcct)) {
        gblSelTransferFromAcctNo = gblQRPayDefaultAcct;
      }
      for (var i = 0; i < gblNoOfFromAcs; i++) {
        var acctNo = fromData[i]["lblActNoval"]
        acctNo = kony.string.replace(acctNo, "-", "");
        if (gblQRPayDefaultAcct == acctNo) {
          selectedIndex = i;
          allowed = true;
          break;
        }
      }
      if (!allowed && !gblfromCalender && isNotBlank(gblQRPayDefaultAcct)) {
        dismissLoadingScreen();
        alert(kony.i18n.getLocalizedString("keyNotAllloedTransactions"));
        return false;
      }
      if (selectedIndex) gbltranFromSelIndex = [0, selectedIndex];
      else gbltranFromSelIndex = [0, 0];
      frmQRSelectAccount.segTransFrm.data = fromData;
      frmQRSelectAccount.segTransFrm.selectedIndex = gbltranFromSelIndex;
      frmQRSelectAccount.show();
      dismissLoadingScreen();
    } else {
      dismissLoadingScreen();
      if (resulttable["errMsg"] == "No Valid Accounts Available for Transfer") {
        alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"));
      } else {
        alert(" " + resulttable["errMsg"]);
      }
    }
  }
}

function getAccountIndexForQRPay() {}
getAccountIndexForQRPay.prototype.paginationSwitch = function(sectionIndex, rowIndex) {
  var segdata = frmQRSelectAccount.segTransFrm.data;

  gbltdFlag = segdata[rowIndex]["tdFlag"];
  rowIndex = parseFloat(rowIndex);
  frmQRSelectAccount.segTransFrm.selectedIndex = [0, rowIndex];
  gbltranFromSelIndex = frmQRSelectAccount.segTransFrm.selectedIndex;
  frmQRSelectAccount.ButtonPayNow.setVisibility(true);
  frmQRSelectAccount.HBoxError.setVisibility(false);
  frmQRSelectAccount.LabelFee.setVisibility(true);
  var fromAcctID = frmQRSelectAccount.segTransFrm.selectedItems[0].lblActNoval;
  gblQRPayData["accountName"] = frmQRSelectAccount.segTransFrm.selectedItems[0].custAcctName;//mki, MIB-9963
  gblQRPayData["fromAccountNo"] = frmQRSelectAccount.segTransFrm.selectedItems[0].custAcctName;
  var frmAcct = fromAcctID.replace(/-/g, "");

  if (frmAcct != gblQRPayDefaultAcct) {
    frmQRSelectAccount.LabelShowDefaultAcct.setVisibility(false);
    frmQRSelectAccount.LinkSetDefaultAcct.setVisibility(true);
  } else {
    frmQRSelectAccount.LabelShowDefaultAcct.setVisibility(true);
    frmQRSelectAccount.LinkSetDefaultAcct.setVisibility(false);
  }

  checkAmountQRPay();
}

function checkOnUsPromptPayinqServiceQRPay() {
  var inputParam = {};
  var fromData = "";
  var i = gbltranFromSelIndex[1];
  fromData = frmQRSelectAccount.segTransFrm.data;
  var enteredAmount = gblQRPayData["transferAmt"];
  var frmID = fromData[i].lblActNoval;
  var prodCode = fromData[i].prodCode;
  var fromAcctID = kony.string.replace(frmID, "-", "");
  var toAcctID = "";
  var mobileOrCI = "";
  if (gblSelTransferMode == 2) {
    toAcctID = gblQRPayData["mobileNo"];
    mobileOrCI = "02";
  } else if (gblSelTransferMode == 3) {
    toAcctID = gblQRPayData["cId"];
    mobileOrCI = "01";
  } else if (gblSelTransferMode == 5) {
    toAcctID = gblQRPayData["eWallet"];
    mobileOrCI = "04";
  }
  var locale = kony.i18n.getCurrentLocale();
  //frmTransferConfirm.lblHiddenToAccount.text = toAcctID;
  inputParam["fromAcctNo"] = fromAcctID;
  inputParam["toAcctNo"] = toAcctID;
  inputParam["toFIIdent"] = gblisTMB;
  inputParam["transferAmt"] = enteredAmount;
  inputParam["prodCode"] = prodCode;
  inputParam["mobileOrCI"] = mobileOrCI;
  inputParam["locale"] = locale;
  inputParam["QRFlag"] = "Y";  
  showLoadingScreen();
  //alert(inputParam);
  invokeServiceSecureAsync("checkOnUsPromptPayinq", inputParam, callBackCheckOnUsPromptPayinqServiceQRPay);
}

function callBackCheckOnUsPromptPayinqServiceQRPay(status, resulttable) {
  var ToAccountName = "";
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      gblisTMB = resulttable["destBankCode"];
      gblBANKREF = getBankNameCurrentLocaleTransfer(gblisTMB);
      if (!isNotBlank(gblisTMB)) {
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        dismissLoadingScreen();
      }
      if (gblisTMB != gblTMBBankCD) {
        ToAccountName = resulttable["toAcctName"];
      } else {
        ToAccountName = resulttable["toAccTitle"];
      }
      if (!isNotBlank(ToAccountName) && gblisTMB != gblTMBBankCD) {
        if (gblSelTransferMode == 2) {
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PNullAcctName");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else if (gblSelTransferMode == 3){
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PNullAcctName2");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        }
        dismissLoadingScreen();
        return false;
      }
      gblQRPayData["toAccount"] = resulttable["toAcctNo"];
      gblp2pAccountNumber = resulttable["toAcctNo"];
      var isOtherBank = frmQRSelectAccount.segTransFrm.selectedItems[0].isOtherBankAllowed;
      var isOtherTmb = frmQRSelectAccount.segTransFrm.selectedItems[0].isOtherTMBAllowed;
      var isAllowedSA = frmQRSelectAccount.segTransFrm.selectedItems[0].isAllowedSA;
      var isAllowedCA = frmQRSelectAccount.segTransFrm.selectedItems[0].isAllowedCA;
      var isAllowedTD = frmQRSelectAccount.segTransFrm.selectedItems[0].isAllowedTD;
      var fromAccNumber = frmQRSelectAccount.segTransFrm.selectedItems[0].accountNum;
      var prodCode = frmQRSelectAccount.segTransFrm.selectedItems[0].prodCode;
      if (isOtherBank != "Y" && gblisTMB != gblTMBBankCD) {
        frmQRSelectAccount.HBoxError.setVisibility(true);
        frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PErrFromToNotMatch");
        frmQRSelectAccount.LabelFee.setVisibility(false);
        frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        dismissLoadingScreen();
      }
      var formAcctNum = removeHyphenIB(fromAccNumber);
      if (formAcctNum == gblp2pAccountNumber && gblisTMB == gblTMBBankCD) {
        frmQRSelectAccount.HBoxError.setVisibility(true);
        frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("msgErrOwnAcctQR");
        frmQRSelectAccount.LabelFee.setVisibility(false);
        frmQRSelectAccount.ButtonPayNow.setVisibility(false);

        dismissLoadingScreen();
      }

      isOwnAccountP2P = false;
      if (resulttable["isOwn"] == "Y") {
        gblpp_isOwnAccount = true; // MKI, MIB-9972
        isOwnAccountP2P = true;
      }
      gblQRPayData["fee"] = resulttable["itmxFee"];
      if (resulttable["itmxFee"] != "0.00") {
        frmQRSelectAccount.LabelFee.text =  kony.i18n.getLocalizedString("keyFee") + resulttable["itmxFee"] + " " + kony.i18n.getLocalizedString("keyBaht");
      }
      gblPaynow = true;
      gblBANKREF = getBankNameCurrentLocaleTransfer(gblisTMB);
      gblSelectedRecipentName = ToAccountName;
      if (kony.application.getCurrentForm().id != "frmQRSelectAccount") {
        frmQRSelectAccount.show();
      }
      dismissLoadingScreen();
    } else {
      dismissLoadingScreen();
      if (resulttable["promptPayFlag"] == "1") {
        var errCode = resulttable["errCode"];
        var errorText = "";
        if (!isNotBlank(errCode)) {
          //showAlertWithCallBack(kony.i18n.getLocalizedString("ECGenericError"), 
        } else if (errCode == 'XB240063') {
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PRecBankRejectAmt");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else if (errCode == 'XB240048') {
          var errorText = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = errorText;
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else if (errCode == 'XB240066') {
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else if (errCode == 'XB240088') {
          if (gblSelTransferMode == 2) {
            frmQRSelectAccount.HBoxError.setVisibility(true);
            frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
            frmQRSelectAccount.LabelFee.setVisibility(false);
            frmQRSelectAccount.ButtonPayNow.setVisibility(false);
          } else if(gblSelTransferMode == 3 || gblSelTransferMode == 4 || gblSelTransferMode == 5){
            frmQRSelectAccount.HBoxError.setVisibility(true);
            frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PAccInActive");
            frmQRSelectAccount.LabelFee.setVisibility(false);
            frmQRSelectAccount.ButtonPayNow.setVisibility(false);
          }	
        } else if (errCode == 'XB240098') {
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PExceedeWal");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else if (errCode == 'XB240067') {
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else if (errCode == 'XB240072') {
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PDestTimeout");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else if (errCode == 'X8899') {
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = kony.i18n.getLocalizedString("MIB_P2PCloseBranchErr");
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        } else {
          var errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode + ")";

          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = errorText;
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);

        }
      } else {
        var errorText = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
        frmQRSelectAccount.HBoxError.setVisibility(true);
        frmQRSelectAccount.LabelCaution.text = errorText;
        frmQRSelectAccount.LabelFee.setVisibility(false);
        frmQRSelectAccount.ButtonPayNow.setVisibility(false);
      }
    }
  }
}

function displayNotEligibleNumberQRPay() {
  var errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrMobtNoInvalid");
  errorText = errorText.replace("{mobile_no}", frmQRSelectAccount.LabelAccountNo.text);
  frmQRSelectAccount.HBoxError.setVisibility(true);
  frmQRSelectAccount.LabelCaution.text = errorText;
  frmQRSelectAccount.LabelFee.setVisibility(false);
  frmQRSelectAccount.ButtonPayNow.setVisibility(false);
}
function displayNotEligibleCitizenIDQRPay(){
  var errorText = "";
  if(ITMX_TRANSFER_ENABLE == "true"){ 
    errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrCINotRegisTurnOn");
  } else {
    errorText = kony.i18n.getLocalizedString("MIB_P2PkeyErrCINotRegisTurnOff");
  }
  errorText = errorText.replace("{citizenID}", frmQRSelectAccount.LabelAccountNo.text);
  frmQRSelectAccount.HBoxError.setVisibility(true);
  frmQRSelectAccount.LabelCaution.text = errorText;
  frmQRSelectAccount.LabelFee.setVisibility(false);
  frmQRSelectAccount.ButtonPayNow.setVisibility(false);
}

function frmQRSelectAccountOnClickPay() {
  gblQRPayData["fromAcctID"] = frmQRSelectAccount.segTransFrm.selectedItems[0].lblActNoval;
  gblQRPayData["custAcctName"] = frmQRSelectAccount.segTransFrm.selectedItems[0].custAcctName;
  gblQRPayData["custName"] = frmQRSelectAccount.segTransFrm.selectedItems[0].lblCustName;
  var availableBal = frmQRSelectAccount.segTransFrm.selectedItems[0].lblBalance;
  availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
  gblqrToAccntBal = kony.string.replace(availableBal, ",", "");
  if(isSignedUser)
  {
    gblqrflow="postLogin";
  }else{
    gblqrflow="preLogin";
  }
  var bankList = globalSelectBankData.length;
  if (bankList == 0 || bankList == "0") {
    srvGetBankListforQR();
  } else {
    checkCrmProfileInqForQRPay();
  }
}

function frmQRSuccessOnClickPay(){
  //loading map to fix the default transfers logic on click qr
  loadMapforQrPayNew(); 
}


function checkCrmProfileInqForQRPay() {
  var inputParam = {}
  gblTransEmail = 0;
  gblTrasSMS = 0;
 
  var transferAmt = gblQRPayData["transferAmt"];
  inputParam["transferAmt"] = transferAmt; //future
  inputParam["gblisTMB"] = gblisTMB;
  if (gblisTMB == gblTMBBankCD) {
    inputParam["gblTMBBankCD"] = gblTMBBankCD;
  }
  inputParam["gblPaynow"] = gblPaynow;

  var i = gbltranFromSelIndex[1];

  //fromData = gblQRPayData["fromAcctID"];
  var fee = "";
  activityTypeID = "024";
  activityFlexValues5 = "";
  fee = 0;

  var frmID = gblQRPayData["fromAcctID"];
  var frmAccnt = replaceCommon(frmID, "-", "");
  inputParam["frmAccnt"] = frmAccnt;
  var toNum = gblQRPayData["toAcctIDNoFormat"];
  inputParam["toNum"] = toNum;
  var activityFlexValues3 = getBankShortName(gblisTMB);
  inputParam["bankRef"] = activityFlexValues3;
  inputParam["FuturetransferDate"] = changeDateFormatForService(gblStartBPDate);
  inputParam["fee"] = fee;
  inputParam["remainingFree"] = gblQRPayData["fee"];

  var transferData = "";
  var mobileOrCI = "";
  if(gblQRPayData["QR_TransType"]=="QR_Transfer"){
      if (gblSelTransferMode == 2) {
        mobileOrCI = "02";
        inputParam["P2P"] = gblQRPayData["mobileNo"];
        transferData = frmAccnt + "|" + toNum + "|" + transferAmt + "|" + gblQRPayData["mobileNo"];
      } else if (gblSelTransferMode == 3) {
        mobileOrCI = "01";
        inputParam["P2P"] = gblQRPayData["cId"];
        transferData = frmAccnt + "|" + toNum + "|" + transferAmt + "|" + gblQRPayData["cId"];
      } else if (gblSelTransferMode == 5) {
        mobileOrCI = "04";
        inputParam["P2P"] = gblQRPayData["eWallet"];
        transferData = frmAccnt + "|" + toNum + "|" + transferAmt + "|" + gblQRPayData["eWallet"];
      } 
      inputParam["transferFlag"] = "true";
  }
  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment")
  {
    mobileOrCI = "05";
    inputParam["P2P"] = "";
    transferData = frmAccnt + "|" + toNum + "|" + transferAmt + "|" + "";
  }
  inputParam["mobileOrCI"] = mobileOrCI;
  inputParam["encryptedData"] = encryptDPUsingE2EE(transferData);
  inputParam["bankShortName"] = getBankShortName(gblisTMB);
  if(gbleDonationType == "QR_Type" || gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
    inputParam["QRFlag"] = "Y";
  }
  kony.print("QR Payment Crm Profile Inq" + inputParam)
  //TODO EDonation
  if(gbleDonationType == "Barcode_Type"){
    inputParam = [];
  }
  showLoadingScreen();
  if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackCrmProfileInqForQRPay_donation)
  }else{
    invokeServiceSecureAsync("crmProfileInq", inputParam, callBackCrmProfileInqForQRPay)
  }
  
}


function callBackCrmProfileInqForQRPay(status, resulttable) {
  if (status == 400) {
    gblDPPk = undefined != resulttable["pk"] ? resulttable["pk"] : "";
    gblDPRandNumber = undefined != resulttable["randomNumber"] ? resulttable["randomNumber"] : "";
    var reload = undefined != resulttable["reload"] ? resulttable["reload"] : "";
    if (resulttable["opstatus"] == 0) {

      if ("true" == reload) {
        checkCrmProfileInqForQRPay();
        return false;
      }

      var StatusCode = resulttable["statusCode"];
      var Severity = resulttable["Severity"];
      var StatusDesc = resulttable["StatusDesc"];
      gblEmailId = resulttable["emailAddr"];
      gblQRPayData["transferDate"] = resulttable["serverDate"];
      if (StatusCode == 0) {

        if (resulttable["mbFlowStatusIdRs"] == "03") {
          dismissLoadingScreen();
          alert("" + kony.i18n.getLocalizedString("keyErrTxnPasslock"));
          return false;
        }

        gblTransferRefNo = resulttable["acctIdentValue"]
        gblXferLnkdAccnts = resulttable["p2pLinkedAcct"]
        var temp1 = [];
        var temp = {
          ebAccuUsgAmtDaily: resulttable["ebAccuUsgAmtDaily"],
          mbFlowStatusIdRs: resulttable["mbFlowStatusIdRs"]
        }

        kony.table.insert(temp1, temp);
        gblCRMProfileData = temp1;
        dismissLoadingScreen()
        kony.print("here1" + gblTransEmail + gblTrasSMS + gblisTMB);
        if (gblTransEmail == 1 && gblTrasSMS == 1 && gblisTMB == gblTMBBankCD) {
          //alert("here1 inside1");
          if (!verifyExceedAvalibleBalanceQRPay()) {
            return false;
          }
          checkDepositAccountInqQRPay();

        } else {
          if (!verifyExceedAvalibleBalanceQRPay()) {
            return false;
          }
          var UsageLimit;
          var DailyLimit = resulttable["ebMaxLimitAmtCurrent"];
          DailyLimit = parseFloat(DailyLimit)
          if (resulttable["ebAccuUsgAmtDaily"] == "" && resulttable["ebAccuUsgAmtDaily"].length == 0) {
            UsageLimit = 0;
          } else {
            UsageLimit = parseFloat(resulttable["ebAccuUsgAmtDaily"]);
          }

          var transferAmt = gblQRPayData["transferAmt"];
          var channelLimit = DailyLimit - UsageLimit;
          if (channelLimit < 0) {
            channelLimit = 0;
          }
          var dailylimitExceedMsg = kony.i18n.getLocalizedString("keyUserchannellimitexceedsfortheday")
          dailylimitExceedMsg = dailylimitExceedMsg.replace("{1}", commaFormatted(fixedToTwoDecimal(channelLimit) + ""));
          
          if (gblQRPayData["QR_TransType"]=="QR_Bill_Payment") {
				//Display DailyLimit
              if (transferAmt > channelLimit) {
                dismissLoadingScreen();
                errorText = dailylimitExceedMsg;
                if(kony.application.getCurrentForm().id == "frmQRSelectAccount"){
                  frmQRSelectAccount.HBoxError.setVisibility(true);
                  frmQRSelectAccount.LabelCaution.text = errorText;
                  frmQRSelectAccount.LabelFee.setVisibility(false);
                  frmQRSelectAccount.ButtonPayNow.setVisibility(false);
                }else{
                  frmQRSuccess.lblerrormsg1.text = errorText;
                  frmQRSuccess.flxErrorBody.height = "94%"
                  frmQRSuccess.flxErrorBody.setVisibility(true);
                  frmQRSuccess.flxPayFooter.setVisibility(false);
                  frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
                  frmQRSuccess.flxLoginFooter.setVisibility(false);
                  frmQRSuccess.FlexFooter.setVisibility(false);
                  closeApprovalKeypad();
                  return false;
                }

                return false;
              }

          } else if (gblQRPayData["QR_TransType"]=="QR_Transfer") {         
          
	          if ((gblSelTransferMode != 1 && gblSelTransferMode != 0 ) && !isOwnAccountP2P && gblisTMB != gblTMBBankCD && gblPaynow == true) { //Case Transfer to other bank by Prompt Pay                       
	            var p2pTranLimit = 0;
	            if (isNotBlank(resulttable["P2P_TRANSACTION_LIMIT"])) {
	              p2pTranLimit = parseFloat(resulttable["P2P_TRANSACTION_LIMIT"]);
	            }
	
	            var p2pTranlimitExceedMsg = kony.i18n.getLocalizedString("MIB_P2PTRErr_TxnLimit")
	            p2pTranlimitExceedMsg = p2pTranlimitExceedMsg.replace("{transaction limit}", commaFormatted(p2pTranLimit + ""));
	
	
	            //Compare base on minimum value                                                                                
	            if (channelLimit < p2pTranLimit) {
	              //Display DailyLimit
	              if (transferAmt > channelLimit) {
	                dismissLoadingScreen();
	                errorText = dailylimitExceedMsg;
	                if(kony.application.getCurrentForm().id == "frmQRSelectAccount"){
	                  frmQRSelectAccount.HBoxError.setVisibility(true);
	                  frmQRSelectAccount.LabelCaution.text = errorText;
	                  frmQRSelectAccount.LabelFee.setVisibility(false);
	                  frmQRSelectAccount.ButtonPayNow.setVisibility(false);
	                }else{
	                  frmQRSuccess.lblerrormsg1.text = errorText;
	                  frmQRSuccess.flxErrorBody.height = "94%"
	                  frmQRSuccess.flxErrorBody.setVisibility(true);
	                  frmQRSuccess.flxPayFooter.setVisibility(false);
	                  frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
	                  frmQRSuccess.flxLoginFooter.setVisibility(false);
	                  frmQRSuccess.FlexFooter.setVisibility(false);
	                  closeApprovalKeypad();
	                  return false;
	                }
	
	                return false;
	              }
	            }
	
	            if (p2pTranLimit < channelLimit) {
	              //Display p2pTranLimit
	              if (transferAmt > p2pTranLimit) {
	                dismissLoadingScreen();
	                errorText = p2pTranlimitExceedMsg;
	                if(kony.application.getCurrentForm().id == "frmQRSelectAccount"){
	                  frmQRSelectAccount.HBoxError.setVisibility(true);
	                  frmQRSelectAccount.LabelCaution.text = errorText;
	                  frmQRSelectAccount.LabelFee.setVisibility(false);
	                  frmQRSelectAccount.ButtonPayNow.setVisibility(false);
	                  return false;
	                }else{
	                  frmQRSuccess.lblerrormsg1.text = errorText;
	                  frmQRSuccess.flxErrorBody.height = "94%"
	                  frmQRSuccess.flxErrorBody.setVisibility(true);
	                  frmQRSuccess.flxPayFooter.setVisibility(false);
	                  frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
	                  frmQRSuccess.flxLoginFooter.setVisibility(false);
	                  frmQRSuccess.FlexFooter.setVisibility(false);
	                  closeApprovalKeypad();
	                  return false;
	                }                  
	
	              }
	            }
	
	            if (p2pTranLimit == channelLimit) {
	              //Display p2pTranLimit
	              if (transferAmt > p2pTranLimit) {
	                dismissLoadingScreen();
	                errorText = p2pTranlimitExceedMsg;
	                if(kony.application.getCurrentForm().id == "frmQRSelectAccount"){
	                  frmQRSelectAccount.HBoxError.setVisibility(true);
	                  frmQRSelectAccount.LabelCaution.text = errorText;
	                  frmQRSelectAccount.LabelFee.setVisibility(false);
	                  frmQRSelectAccount.ButtonPayNow.setVisibility(false);
	                  return false;  
	                }else{
	                  frmQRSuccess.lblerrormsg1.text = errorText;
	                  frmQRSuccess.flxErrorBody.height = "94%"
	                  frmQRSuccess.flxErrorBody.setVisibility(true);
	                  frmQRSuccess.flxPayFooter.setVisibility(false);
	                  frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
	                  frmQRSuccess.flxLoginFooter.setVisibility(false);
	                  frmQRSuccess.FlexFooter.setVisibility(false);
	                  closeApprovalKeypad();
	                  return false;
	                }  
	
	              }
	            }
	
	          } else if (gblSelTransferMode != 1 && isOwnAccountP2P) { //Transfer Own by Prompt Pay
	
	          } else if (gblSelTransferMode != 1 && gblisTMB == gblTMBBankCD && gblPaynow == true) { //Transfer to other TMB by Prompt Pay            
	            //Check only daily limit
	            if (channelLimit < transferAmt) {
	              dismissLoadingScreen();
	              errorText = dailylimitExceedMsg;
	              if(kony.application.getCurrentForm().id == "frmQRSelectAccount"){
	                frmQRSelectAccount.HBoxError.setVisibility(true);
	                frmQRSelectAccount.HBoxError.setVisibility(true);
	                frmQRSelectAccount.LabelCaution.text = errorText;
	                frmQRSelectAccount.LabelFee.setVisibility(false);
	                frmQRSelectAccount.ButtonPayNow.setVisibility(false);
	                return false;
	              }else{
	                frmQRSuccess.lblerrormsg1.text = errorText;
	                frmQRSuccess.flxErrorBody.height = "94%"
	                frmQRSuccess.flxErrorBody.setVisibility(true);
	                frmQRSuccess.flxPayFooter.setVisibility(false);
	                frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
	                frmQRSuccess.flxLoginFooter.setVisibility(false);
	                frmQRSuccess.FlexFooter.setVisibility(false);
	                closeApprovalKeypad();
	                return false;                
	              }
	
	            }
	          } else { //end P2P MIB-521
	            //Check To Account TAB //
	            //checkMaxLimitForTransfer//
	            // Transfer to other TMB                                                                                      
	            if (!(gblTransEmail == 1 && gblTrasSMS == 1) && gblisTMB == gblTMBBankCD) {
	              if (channelLimit < transferAmt && gblPaynow == true) {
	                dismissLoadingScreen();
	                errorText = dailylimitExceedMsg;
	                if(kony.application.getCurrentForm().id == "frmQRSelectAccount"){
	                  frmQRSelectAccount.HBoxError.setVisibility(true);
	                  frmQRSelectAccount.LabelCaution.text = errorText;
	                  frmQRSelectAccount.LabelFee.setVisibility(false);
	                  frmQRSelectAccount.ButtonPayNow.setVisibility(false);
	                  return false;  
	                }else{
	                  frmQRSuccess.lblerrormsg1.text = errorText;
	                  frmQRSuccess.flxErrorBody.height = "94%"
	                  frmQRSuccess.flxErrorBody.setVisibility(true);
	                  frmQRSuccess.flxPayFooter.setVisibility(false);
	                  frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
	                  frmQRSuccess.flxLoginFooter.setVisibility(false);
	                  frmQRSuccess.FlexFooter.setVisibility(false);
	                  closeApprovalKeypad();
	                  return false;
	                }
	
	              }
	            }
	
	          } //end To Account TAB        
	      }                                                                                                                                                                     
          dismissLoadingScreen();

          if (gblisTMB == null && gblisTMB == "" && gblisTMB == undefined) {
            alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            //setEnabledTransferLandingPage(true);
            return false;
          }

          transferAmt = gblQRPayData["transferAmt"];
          if (gblSelTransferMode != 1 && gblisTMB != gblTMBBankCD) {
            //checkTransferTypeMBQRPay();
            //showQRPayPwdPopupForProcess();
            /*if(gblqrflow=="postLogin"){
              showQRPayPwdPopupForProcess();
            }else{
              verifyTransferQRPay("")
            }*/
            if (kony.application.getCurrentForm().id != "frmQRSelectAccount")
            {
              if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                //alert("call billpayment service");
                callGenerateTransferRefNoserviceqrBillPay();
              }else{
                verifyTransferQRPay(gblNumApproval);
              }
              
            }else{
              
              //showAccessPinScreenKeypad(); //q1q2
              if(gblpp_isOwnAccount)// MKI, MIB-9972 start
              {
                //write code for access pin skip
                 if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                //alert("call billpayment service");
                callGenerateTransferRefNoserviceqrBillPay();
                }else{
                  verifyTransferQRPay(gblNumApproval.trim());
                }
              }
              else{
                  showAccessPinScreenKeypad();  
              } // MKI, MIB-9972 End
            }

            //
          } else if (gblisTMB == gblTMBBankCD) {
            
            if (kony.application.getCurrentForm().id != "frmQRSelectAccount")
            {
              if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                //alert("call billpayment service");
                callGenerateTransferRefNoserviceqrBillPay();
              }else{
                checkDepositAccountInqQRPay();
              }              
            }else{
              //showAccessPinScreenKeypad();
              if(gblpp_isOwnAccount)// MKI, MIB-9972 start
              {
                //write code for access pin skip
                if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                  //alert("call billpayment service");
                  callGenerateTransferRefNoserviceqrBillPay();
                }else{
                  verifyTransferQRPay(gblNumApproval.trim());
                }
              }
              else{
                  showAccessPinScreenKeypad();  
              } // MKI, MIB-9972 End
            }            


          } else {
            //checkTransferTypeMBQRPay();
            //showQRPayPwdPopupForProcess();
            //showAccessPinScreenKeypad();
            if (kony.application.getCurrentForm().id != "frmQRSelectAccount")
            {
              if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                //alert("call billpayment service");
                callGenerateTransferRefNoserviceqrBillPay();
              }else{
                verifyTransferQRPay(gblNumApproval);
              }              
            }else{
              //showAccessPinScreenKeypad();
              if(gblpp_isOwnAccount)// MKI, MIB-9972 start
              {
                //write code for access pin skip
                if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                  //alert("call billpayment service");
                  callGenerateTransferRefNoserviceqrBillPay();
                }else{
                  verifyTransferQRPay(gblNumApproval.trim());
                }
              }
              else{
                  showAccessPinScreenKeypad();  
              } // MKI, MIB-9972 End
            }
            //verifyTransferQRPay(gblNumApproval);
            /*if(gblqrflow=="postLogin"){
              showQRPayPwdPopupForProcess();
            }else{
              verifyTransferQRPay("")
            }*/

          }

        }
      } else {
        dismissLoadingScreen();
        showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
        //setEnabledTransferLandingPage(true);
        return false;
      }
    } else {
      dismissLoadingScreen();
      if (resulttable["opstatus"] == "8005") {
       showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info")); //MKI, 8005 error
      } else {
        showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
        //setEnabledTransferLandingPage(true);
      }
    }
  }
}

function checkDepositAccountInqQRPay() {
  var inputParam = {}
  //var toAcctID = frmTransferConfirm.lblHiddenToAccount.text;
  //toAcctID = kony.string.replace(toAcctID, "-", "")
  toAcctID = gblQRPayData["toAcctIDNoFormat"];
  inputParam["acctId"] = toAcctID;
  //inputParam["acctType"] = accountType;
  showLoadingScreen();
  if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
    invokeServiceSecureAsync("depositAccountInquiryNonSec", inputParam, callBackDepositAccountInqQRPay_donation)
  }else{
    invokeServiceSecureAsync("depositAccountInquiryNonSec", inputParam, callBackDepositAccountInqQRPay)
  }
  
}

function callBackDepositAccountInqQRPay(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      // fetching To Account Name for TMB Inq
      var StatusCode = resulttable["StatusCode"];
      var Severity = resulttable["Severity"];
      var StatusDesc = resulttable["StatusDesc"];
      dismissLoadingScreen();

      if (StatusCode != "0") {
        errorText = kony.i18n.getLocalizedString("Receipent_alert_correctdetails");
        frmQRSelectAccount.HBoxError.setVisibility(true);
        frmQRSelectAccount.LabelCaution.text = errorText;
        frmQRSelectAccount.LabelFee.setVisibility(false);
        frmQRSelectAccount.ButtonPayNow.setVisibility(false);
        return false;
      } else {
        var ToAccountName = resulttable["accountTitle"]
        kony.print("mki8"+resulttable["accountTitle"]);
        //gblQRPayData["custName"] = resulttable["accountTitle"]; // MKI, MIB-0000, Incorrect From account in TMB bank QR trasfer completion.screen shot screen
        if (ToAccountName != null && ToAccountName.length > 0) {} else {
          errorText = kony.i18n.getLocalizedString("Receipent_alert_correctdetails");
          frmQRSelectAccount.HBoxError.setVisibility(true);
          frmQRSelectAccount.LabelCaution.text = errorText;
          frmQRSelectAccount.LabelFee.setVisibility(false);
          frmQRSelectAccount.ButtonPayNow.setVisibility(false);
          return false;
        } /** checking if transfer is happeninf from TD account(i.e AccountPreWithDrawInq  */
        if (gbltdFlag != kony.i18n.getLocalizedString("termDeposit")) {
          checkFundTransferInqMBQRPay();
        }
      }
    } else {
      dismissLoadingScreen();
      showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
    }
  } else {
    //setEnabledTransferLandingPage(true);
  }
}

function checkFundTransferInqMBQRPay() {
  //alert("Final");
  var inputParam = {}
  var fromData;
  var depositeNo;
  var fromAcctID;
  var toAcctID;
  var k = 3;
  var i = gbltranFromSelIndex[1]

  fromAcctID = gblqrFromAccntNum;
  //fromAcctID = kony.string.replace(fromAcctID, "-", "");
  if (fromAcctID.length == 14) k = 7;
  if (fromAcctID[k] == "3") {
    depositeNo = gblTDDeposit;
  }
  //inputParam["fromAcctTypeValue"] = gbltdFlag;
  //inputParam["toAcctTypeValue"] = toaccountType;
  //toAcctID = frmTransferConfirm.lblHiddenToAccount.text;
  //toAcctID = kony.string.replace(toAcctID, "-", "");
  toAcctID = gblQRPayData["toAcctIDNoFormat"];
  //var amt = frmTransferLanding.txtTranLandAmt.text + "";
  //amt = parseFloat(removeCommos(amt)).toFixed(2);
  inputParam["transferAmt"] = gblQRPayData["transferAmt"];
  inputParam["transferDate"] = gblTransferDate
  inputParam["depositeNo"] = depositeNo;
  inputParam["fromAcctNo"] = fromAcctID
  inputParam["toAcctNo"] = toAcctID
  showLoadingScreen();
  invokeServiceSecureAsync("fundTransferInq", inputParam, callBackFundTransferInqMBQRPay)
}

function callBackFundTransferInqMBQRPay(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      // fetching To Account Name for TMB Inq
      var StatusCode = resulttable["StatusCode"];
      var Severity = resulttable["Severity"];
      var StatusDesc = resulttable["StatusDesc"];
      var fee = resulttable["transferFee"];
      var FlagFeeReg = resulttable["FlagFeeReg"];
      var tranCode = resulttable["TranCode"]
      var temp1 = [];
      var temp = {
        FlagFeeReg: resulttable["FlagFeeReg"],
        tranCode: resulttable["TranCode"]
      }
      kony.table.insert(temp1, temp)
      gblFundXferData = temp1;
      if (fee == "" && fee.length == 0) fee = "0.00"
      dismissLoadingScreen();
      if (StatusCode != "0") {
        showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
        //setEnabledTransferLandingPage(true);
        return false;
      } else {
        //if (FlagFeeReg == "I")
        //frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + parseFloat(fee).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
        //else
        //frmTransferConfirm.lblTransCnfmTotFeeVal.text = "(" + "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht") + ")";
        //checkTransferTypeMB()
        //alert("Show Tran Pass");
        //showQRPayPwdPopupForProcess();
        if(gblqrflow=="postLogin"){
          if(kony.application.getCurrentForm().id == "frmQRSelectAccount"){
            showAccessPinScreenKeypad();
          }
          else{
            if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                //alert("call billpayment service");
              	callGenerateTransferRefNoserviceqrBillPay();
              }else{
                verifyTransferQRPay(gblNumApproval.trim());
              }
            
          }
          //showQRPayPwdPopupForProcess();
        }else{
         //alert("Pre login");
         if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                //alert("call billpayment service");
           		callGenerateTransferRefNoserviceqrBillPay();
          }else{
                verifyTransferQRPay(gblNumApproval.trim());
          }
        }

      }
    } else {
      dismissLoadingScreen();
      showCommonAlert(resulttable["errMsg"], resulttable["XPServerStatCode"]);
      //setEnabledTransferLandingPage(true);
    }
  } else {
    //setEnabledTransferLandingPage(true);
  }
}

function verifyExceedAvalibleBalanceQRPay() {

  var availableBal;
  var i = gbltranFromSelIndex[1];
  kony.print("Inside Check balance")
  //var fromData = frmQRSelectAccount.segTransFrm.data;
  availableBal = gblqrToAccntBal;
  //availableBal = frmQRSelectAccount.segTransFrm.selectedItems[0].lblBalance;
  //for product code 225 and 226
  //gblNofeeVar = fromData[i].prodCode;
  //availableBal = fromData[i].lblBalance;
  //availableBal = availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
  //availableBal = kony.string.replace(availableBal, ",", "");
  //availableBal = parseFloat(availableBal.trim());
  var findDot;
  var isCrtFormt;
  //enteredAmount = frmTransferLanding.txtTranLandAmt.text;
  enteredAmount = gblQRPayData["transferAmt"];

  var fee = parseFloat(gblQRPayData["fee"]);
  kony.print("Inside Check balance" + enteredAmount + availableBal + " Flags" + gblPaynow + gblSelTransferMode);
  if (enteredAmount != null && enteredAmount != undefined && (availableBal < (parseFloat(enteredAmount) + fee)) && gblPaynow) {
    if (gblSelTransferMode != 1) {
      errorText = kony.i18n.getLocalizedString("msg_QRInsufficientFund");
      frmQRSelectAccount.HBoxError.setVisibility(true);
      frmQRSelectAccount.LabelCaution.text = errorText;
      frmQRSelectAccount.LabelFee.setVisibility(false);
      frmQRSelectAccount.ButtonPayNow.setVisibility(false);
      return false;
    }
  }
  return true;
}

function verifyTransferQRPay(tranPassword) {
  var frmAcct;
  var fromAcctID;
  var inputParam = {};
  var fromData;
  showLoadingScreen();
  var toNickname = "";
  //var toAcctid = frmTransferConfirm.lblHiddenToAccount.text;
  //var transferAmtTmp = frmTransferLanding.txtTranLandAmt.text;  
  //transferAmtTmp = kony.string.replace(transferAmtTmp, kony.i18n.getLocalizedString("currencyThaiBaht"), "")
  //var transferAmt = parseFloat(removeCommos(transferAmtTmp)).toFixed(2);
  var transferAmt = gblQRPayData["transferAmt"];

  fromAcctID = gblQRPayData["fromAcctID"];

  frmAcct = fromAcctID.replace(/-/g, "");
  //var toAcct = toAcctid.replace(/-/g, "");
  var toAcct = gblQRPayData["toAcctIDNoFormat"];

  inputParam["loginModuleId"] = "MB_TxPwd";
  inputParam["password"] = tranPassword;
  //popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";


  inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
  inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;

  //gblBalAfterXfer = gblBalAfterXfer.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "")
  //gblBalAfterXfer = kony.string.replace(gblBalAfterXfer, "?", "");
  gblBalAfterXfer = "";

  if (gblSelTransferMode == 2) { // Always send Mobile Number
    toNickname = kony.i18n.getLocalizedString("MIB_P2PMob");
  } else if(gblSelTransferMode == 3){
    toNickname = kony.i18n.getLocalizedString("MIB_P2PCiti");
  } else if (gblSelTransferMode == 5) {
    toNickname = kony.i18n.getLocalizedString("MIB_P2PToeWalletLab");
  }

  inputParam["fromAcctNo"] = frmAcct; //k                                       
  inputParam["toAcctNo"] = toAcct; //kk
  inputParam["dueDateForEmail"] = "";
  inputParam["memo"] = ""; //k
  inputParam["transferAmt"] = transferAmt; //k
  inputParam["toAcctBank"] = gblBANKREF

  var fourthDigit;
  if (frmAcct.length == 14) {
    fourthDigit = frmAcct.charAt(7);
  } else {
    fourthDigit = frmAcct.charAt(3);
  }
  if (fourthDigit == "3") {
    var depositeNo = gblTDDeposit;
    inputParam["depositeNo"] = depositeNo;
  }

  var date = gblQRPayData["transferDate"];
  date = date.substr(0, 10);
  kony.print("Values are checking !!@@@@@@@@@"+gblQRPayData["custAcctName"]+gblQRPayData["custName"]+gblQRPayData["toAccountName"])
  //for date format should be dd/mm/yy DEF7481 
  var currentDate = date.toString();
  var dateTD = currentDate.substr(0, 2);
  var monthTD = currentDate.substr(3, 2);
  var yearTD = currentDate.substr(8, 2);
  date = dateTD + "/" + monthTD + "/" + yearTD;
  inputParam["toFIIdent"] = gblisTMB; //orft                     
  inputParam["transferDate"] = date;
  inputParam["finTxnMemo"] = "";
  inputParam["frmAcctName"] = gblQRPayData["custAcctName"];
  inputParam["fromAcctNickname"] = gblQRPayData["custName"];
  inputParam["toAcctName"] = gblQRPayData["toAccountName"];
  inputParam["toAcctNickname"] = toNickname; //financial       
  inputParam["toBankId"] = gblisTMB; //k
  inputParam["custName"] = gblCustomerName;
  inputParam["toAcctBank"] = gblBANKREF;
  inputParam["RecipentName"] = gblSelectedRecipentName;
  inputParam["Recp_category"] = Recp_category;
  inputParam["gblBalAfterXfer"] = gblBalAfterXfer; //financial                   
  inputParam["gblPaynow"] = gblPaynow; //k
  inputParam["gblisTMB"] = gblisTMB;
  inputParam["gblTrasORFT"] = gblTrasORFT; //k
  inputParam["gblTransSMART"] = gblTransSMART; //k
  inputParam["gblBANKREF"] = gblBANKREF; //future
  inputParam["bankShortName"] = getBankShortName(gblisTMB);
  inputParam["gblTrasSMS"] = gblTrasSMS; //future
  inputParam["gblTransEmail"] = gblTransEmail; //future
  inputParam["oneTimeTransfer"] = "AccountNumber";
  inputParam["QRFlag"] = 'Y';
  kony.print("Inside excute transfer promptpay");
  invokeServiceSecureAsync("executeTransfer", inputParam, callbackVerifyTransferQRPay)
}

function callbackVerifyTransferQRPay(status, resultTable) {
  dismissLoadingScreen();
  try{
  if (status == 400) {
		if (resultTable["opstatus"] == 0) {
				var transferData=resultTable["Transfer"];
				if(transferData!=null||refID!=""||refID!="null"||refID!=undefined||refID!="undefined"){
				
					var isFail = false , fee = 0, refId =0;
                  	
					if(transferData == "" || transferData.length == 0 || transferData[0]["image"] == "wrong.png") {
						isFail = true;
                  	 }
                  	else
                    {
                      fee = transferData[0]["fee"],
                        refId = transferData[0]["refId"];
                    }
                  	
					
					if(isFail)
					{
                      	  closeApprovalKeypad();
                      	  frmQRPaymentSuccess.show();
                      	  //failed case
                      	  frmQRPaymentSuccess.imagestatusicon.src="iconnotcomplete.png";
                      	  frmQRPaymentSuccess.LabelRefId.setVisibility(false);
                      	  frmQRPaymentSuccess.hbxShareOption.setVisibility(false);
                      	  frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("keyServiceUnavailable");
                      		//end failed case
                      	kony.print("transferAmount   "+resultTable["transferAmount"]);
                      		 var formattedAmount = amountFormat(resultTable["transferAmount"]);
                          frmQRPaymentSuccess.lblTransNPbAckTotVal.text=formattedAmount[0]+formattedAmount[1];
                          //frmQRPaymentSuccess.LabelRefId.text = kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + resultTable["Transfer"][0]["refId"];

                          //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custAcctName"];
                      	  var deviceInfo = kony.os.deviceInfo();
                          var deviceModel = deviceInfo.model;
                          var frmName = gblQRPayData["custAcctName"]; // MKI, MIB-9931
                          if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                            frmName = frmName.substring(0,11);// MKI, MIB-9931
                          }//mki, MIB-9931 ====End
                          frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                          frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                      	  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            gblQRPayData["toAccountName"] = gblQRDefaultAccountResponse["pp_toAcctName"];
                          }
                          if (gblSelTransferMode == 2) { // Always send Mobile Number
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PMob");
                          } else if(gblSelTransferMode == 3){
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PCiti");
                          } else if (gblSelTransferMode == 5) {
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PToeWalletLab");
                          }
                                  if(gblQRPayData["toAccountName"]== "" || gblQRPayData["toAccountName"] == undefined ){
                            var tonameSub = gblqrToUsrName;
                          }else{
                            var tonameSub = gblQRPayData["toAccountName"];
                          }
                          frmQRPaymentSuccess.lblTransNPbAckToNum.text= tonameSub;
                          frmQRPaymentSuccess.lblTransNPbAckToName.text= toNickname;

                      	var formattedAmount = amountFormat(resultTable["transferAmount"]);
                      	var formattedAmount1 = amountFormat(fee);
                     
	 					frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=formattedAmount[0]+formattedAmount[1]+" ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=resultTable["transferAmount"]+kony.i18n.getLocalizedString("currencyThaiBaht")+" ("+resultTable["Transfer"][0]["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text= " ("+resultTable["Transfer"][0]["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                      		//var formattedAmount = amountFormat(resultTable["Transfer"][0]["fee"]);
    					  frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          frmQRPaymentSuccess.lblTransNPbAckFrmNum.text="xxx-x-" + gblQRPayData["fromAcctID"].substring(6,11) + "-x";
                          //frmQRPaymentSuccess.lblMobileNumber.text=maskMobileNumber(gblQRPayData["mobileNo"]);
                      	  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                          }else{
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=refId;
                          } 
                          
                          frmQRPaymentSuccess.lblTransNPbAckDateVal.text=resultTable["serverDate"];
                          frmQRPaymentSuccess.lblSmartDateVal.text=resultTable["serverDate"];
                          if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                          } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                          } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                          }
                      	  
						  //closeApprovalKeypad();
						  //alert(kony.i18n.getLocalizedString("ECGenericError"));              
					}else{
                      	gblTotalPinAttemptsTransfers = 3; 
						if(isSignedUser){
                          closeApprovalKeypad();
                          
                          //Start success case
                          frmQRPaymentSuccess.imagestatusicon.src="greencomplete.png";
                      	  frmQRPaymentSuccess.LabelRefId.setVisibility(true);
                          frmQRPaymentSuccess.hbxShareOption.setVisibility(true);
                          frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("msgQRPaymentSuccess");
                          //Start success case
                          frmQRPaymentSuccess.show();
                          var formattedAmount = amountFormat(resultTable["transferAmount"]);
     					  frmQRPaymentSuccess.lblTransNPbAckTotVal.text=formattedAmount[0]+formattedAmount[1];
                          frmQRPaymentSuccess.LabelRefId.text = kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + resultTable["Transfer"][0]["refId"];
							gblQRPayData["refId"] = resultTable["Transfer"][0]["refId"];
                          //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custName"];//gblQRPayData["custAcctName"];
                          var deviceInfo = kony.os.deviceInfo();
                          var deviceModel = deviceInfo.model;
                          var frmName = gblQRPayData["custName"]; // MKI, MIB-9931
                          if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                            frmName = frmName.substring(0,11);// MKI, MIB-9931
                          }//mki, MIB-9931 ====End
                          frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                          frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                          if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            gblQRPayData["toAccountName"] = gblQRDefaultAccountResponse["pp_toAcctName"];
                          }
                          
                          if (gblSelTransferMode == 2) { // Always send Mobile Number
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PMob");
                          } else if(gblSelTransferMode == 3){
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PCiti");
                          } else if (gblSelTransferMode == 5) {
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PToeWalletLab");
                          }
                                  if(gblQRPayData["toAccountName"]== "" || gblQRPayData["toAccountName"] == undefined ){
                            var tonameSub = gblqrToUsrName;
                          }else{
                            var tonameSub = gblQRPayData["toAccountName"];
                          }
                          frmQRPaymentSuccess.lblTransNPbAckToNum.text= tonameSub;
                          frmQRPaymentSuccess.lblTransNPbAckToName.text= toNickname;
                          //var formattedAmount = amountFormat(resultTable["transferAmount"]);
                          var formattedAmount1 = amountFormat(resultTable["Transfer"][0]["fee"]);
	 						frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=formattedAmount[0]+formattedAmount[1]+" ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=resultTable["transferAmount"]+kony.i18n.getLocalizedString("currencyThaiBaht")+" ("+resultTable["Transfer"][0]["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text= " ("+resultTable["Transfer"][0]["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                          //var formattedAmount = amountFormat(resultTable["Transfer"][0]["fee"]);
    					  frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          frmQRPaymentSuccess.lblTransNPbAckFrmNum.text="xxx-x-" + gblQRPayData["fromAcctID"].substring(6,11) + "-x";
                          //frmQRPaymentSuccess.lblMobileNumber.text=maskMobileNumber(gblQRPayData["mobileNo"]);
                          if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            //alert(gblbilPayRefNum);
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                          }else{
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                          }
                          kony.timer.schedule("billpay", function(){onclickQRPaymentSaveImage();kony.timer.cancel("billpay");}, 1, false);
                          frmQRPaymentSuccess.lblTransNPbAckDateVal.text=resultTable["serverDate"];
                          frmQRPaymentSuccess.lblSmartDateVal.text=resultTable["serverDate"];
                          if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                          } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                          } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                          }
						}
                        else{
                          closeApprovalKeypad();
                          frmQRPaymentSuccess.show();
                          //Start success case
                          frmQRPaymentSuccess.imagestatusicon.src="greencomplete.png";
                      	  frmQRPaymentSuccess.LabelRefId.setVisibility(true);
                          frmQRPaymentSuccess.hbxShareOption.setVisibility(true);
                          frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("msgQRPaymentSuccess");
                          //Start success case
                          frmQRPaymentSuccess.LabelRefId.text = kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + resultTable["Transfer"][0]["refId"];
                          var formattedAmount = amountFormat(resultTable["transferAmount"]);
     					  frmQRPaymentSuccess.lblTransNPbAckTotVal.text=formattedAmount[0]+formattedAmount[1];
                          //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custName"];//gblQRPayData["custAcctName"];
                          var deviceInfo = kony.os.deviceInfo();
                          var deviceModel = deviceInfo.model;
                          var frmName = gblQRPayData["custName"]; // MKI, MIB-9931
                          if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                            frmName = frmName.substring(0,11);// MKI, MIB-9931
                          }//mki, MIB-9931 ====End
                          frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                          frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                          if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            gblQRPayData["toAccountName"] = gblQRDefaultAccountResponse["pp_toAcctName"];
                          }
                          if (gblSelTransferMode == 2) { // Always send Mobile Number
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PMob");
                          } else if(gblSelTransferMode == 3){
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PCiti");
                          } else if (gblSelTransferMode == 5) {
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PToeWalletLab");
                          }
                                  if(gblQRPayData["toAccountName"]== "" || gblQRPayData["toAccountName"] == undefined ){
                            var tonameSub = gblqrToUsrName;
                          }else{
                            var tonameSub = gblQRPayData["toAccountName"];
                          }
                          frmQRPaymentSuccess.lblTransNPbAckToNum.text= tonameSub;
                          frmQRPaymentSuccess.lblTransNPbAckToName.text= toNickname;
                           //var formattedAmount = amountFormat(resultTable["transferAmount"]);
                          var formattedAmount1 = amountFormat(resultTable["Transfer"][0]["fee"]);
	 						frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=formattedAmount[0]+formattedAmount[1]+" ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=resultTable["transferAmount"]+kony.i18n.getLocalizedString("currencyThaiBaht")+" ("+resultTable["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text= " ("+resultTable["Transfer"][0]["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                          //var formattedAmount = amountFormat(resultTable["Transfer"][0]["fee"]);
    					  frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          frmQRPaymentSuccess.lblTransNPbAckFrmNum.text="xxx-x-" + gblQRPayData["fromAcctID"].substring(6,11) + "-x";
                          //frmQRPaymentSuccess.lblMobileNumber.text=maskMobileNumber(gblQRPayData["mobileNo"]);
                          if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                          }else{
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                          }
                          frmQRPaymentSuccess.lblTransNPbAckDateVal.text=resultTable["serverDate"];
                          frmQRPaymentSuccess.lblSmartDateVal.text=resultTable["serverDate"];
                          //loadvaluesforScreenshotqr(status, resultTable);
                          kony.timer.schedule("billpay", function(){onclickQRPaymentSaveImage();kony.timer.cancel("billpay");}, 1, false);
                          if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                            frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                          } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                            frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                          } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                            frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                          }
                      }
					}
					
				}
				else{
						  closeApprovalKeypad();
                  		  frmQRPaymentSuccess.show();
                  		  //failed case
                      	  frmQRPaymentSuccess.imagestatusicon.src="iconnotcomplete.png";
                      	  frmQRPaymentSuccess.LabelRefId.setVisibility(false);
                      	  frmQRPaymentSuccess.hbxShareOption.setVisibility(false);
                      	  frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("keyServiceUnavailable");
                      	  //end failed case
                  			var formattedAmount = amountFormat(resultTable["transferAmount"]);
     						frmQRPaymentSuccess.lblTransNPbAckTotVal.text=formattedAmount[0]+formattedAmount[1];
						  //frmQRPaymentSuccess.lblTransNPbAckTotVal.text=resultTable["transferAmount"]+kony.i18n.getLocalizedString("currencyThaiBaht");
                          //frmQRPaymentSuccess.LabelRefId.text = kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + resultTable["Transfer"][0]["refId"];

                          //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custAcctName"];
                  		  var deviceInfo = kony.os.deviceInfo();
                          var deviceModel = deviceInfo.model;
                          var frmName = gblQRPayData["custAcctName"]; // MKI, MIB-9931
                          if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                            frmName = frmName.substring(0,11);// MKI, MIB-9931
                          }//mki, MIB-9931 ====End
                          frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                          frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                  		  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            gblQRPayData["toAccountName"] = gblQRDefaultAccountResponse["pp_toAcctName"];
                          }
                          if (gblSelTransferMode == 2) { // Always send Mobile Number
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PMob");
                          } else if(gblSelTransferMode == 3){
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PCiti");
                          } else if (gblSelTransferMode == 5) {
                            var toNickname = kony.i18n.getLocalizedString("MIB_P2PToeWalletLab");
                          }
                                  if(gblQRPayData["toAccountName"]== "" || gblQRPayData["toAccountName"] == undefined ){
                            var tonameSub = gblqrToUsrName;
                          }else{
                            var tonameSub = gblQRPayData["toAccountName"];
                          }
                          frmQRPaymentSuccess.lblTransNPbAckToNum.text= tonameSub;
                          frmQRPaymentSuccess.lblTransNPbAckToName.text= toNickname;
                  			 //var formattedAmount = amountFormat(resultTable["transferAmount"]);
                  			 var formattedAmount1 = amountFormat(resultTable["Transfer"][0]["fee"]);
	 						frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=formattedAmount[0]+formattedAmount[1]+" ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotVal2.text=resultTable["transferAmount"]+kony.i18n.getLocalizedString("currencyThaiBaht")+" ("+resultTable["Transfer"][0]["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                  		//var formattedAmount = amountFormat(resultTable["Transfer"][0]["fee"]);
    					frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " ("+formattedAmount1[0]+formattedAmount1[1]+")";
                          //frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text= " ("+resultTable["Transfer"][0]["fee"]+kony.i18n.getLocalizedString("currencyThaiBaht")+")";
                          frmQRPaymentSuccess.lblTransNPbAckFrmNum.text="xxx-x-" + gblQRPayData["fromAcctID"].substring(6,11) + "-x";
                          //frmQRPaymentSuccess.lblMobileNumber.text=maskMobileNumber(gblQRPayData["mobileNo"]);
                  		  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                          }else{
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                          }
                          frmQRPaymentSuccess.lblTransNPbAckDateVal.text=resultTable["serverDate"];
                          frmQRPaymentSuccess.lblSmartDateVal.text=resultTable["serverDate"];
                          if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                          } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                          } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                          frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                          }
                      	  //frmQRPaymentSuccess.show();
				}
		 }
		 else {
          
		  if(resultTable["errCode"] == "VrfyAcPWDErr00001" || resultTable["errCode"] == "VrfyAcPWDErr00002"){
			resetKeypadApproval();           
			var badLoginCount = resultTable["badLoginCount"];
			var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
      incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttemptsTransfers - badLoginCount); 

			frmQRSuccess.lblForgotPin.text = incorrectPinText; //kony.i18n.getLocalizedString("invalidCurrPIN"); //MKI, MIB-9892
			frmQRSuccess.lblForgotPin.skin = "lblBlackMed150NewRed";
			frmQRSuccess.lblForgotPin.onTouchEnd = doNothing;
		  } else if (resultTable["errCode"] == "VrfyAcPWDErr00003") {
			closeApprovalKeypad();
			//showTranPwdLockedPopup();
            gotoUVPINLockedScreenPopUp();
		  } else {
			closeApprovalKeypad();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		  }
          
		}	
      
    }else{
		closeApprovalKeypad();
		alert(kony.i18n.getLocalizedString("ECGenericError"));
	}
  }
  catch(ex)
    {
      kony.print("callbackVerifyTransferQRPay ex="+ex.message);
    }
    
}

function showQRPayPwdPopupForProcess() {
  var lblText = kony.i18n.getLocalizedString("transPasswordSub");
  var refNo = "";
  var mobNO = "";
  showLoadingScreen();
  showOTPPopup(lblText, refNo, mobNO, onClickTransferQRPayConfirmPop, 3);
}

function onClickTransferQRPayConfirmPop(tranPassword) {
  if (isNotBlank(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text)) {
    showLoadingScreen();
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
    popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;

    confirmTransferQRPay(tranPassword);


  } else {
    setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
    return false;
  }
}

function confirmTransferQRPay(tranPassword) {

  if (tranPassword == null || tranPassword == '') {
    setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"));
    return false;
  } else {
    //showLoadingScreen();
    popupTractPwd.show();
    popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("keyTransPwdMsg");
    popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = tbxPopupBlue;
    popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = tbxPopupBlue;
    popupTractPwd.hbxPopupTranscPwd.skin = tbxPopupBlue;

    verifyTransferQRPay(tranPassword);
  }
}

function frmQRSelectAccountOnClickSelAcctLink() {
  var inputParam = {};

  var fromAcctID = frmQRSelectAccount.segTransFrm.selectedItems[0].lblActNoval;

  var frmAcct = fromAcctID.replace(/-/g, "");

  inputParam["accountNumber"] = frmAcct;
  inputParam["transType"] = "TRANSFER_PROMPTPAY";

  showLoadingScreen();
  invokeServiceSecureAsync("QRPaymentDefaultAccountSettings", inputParam, onClickSelAcctLinkCallBack)

}

function onClickSelAcctLinkCallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      var fromAcctID = frmQRSelectAccount.segTransFrm.selectedItems[0].lblActNoval;

      var frmAcct = fromAcctID.replace(/-/g, "");
      frmQRSelectAccount.LabelShowDefaultAcct.setVisibility(true);
      frmQRSelectAccount.LinkSetDefaultAcct.setVisibility(false);
      gblQRPayDefaultAcct = frmAcct;
       if(resulttable["pp_isOwn"] == 'Y'){ // MKI, MIB-9972 ---start
       		gblpp_isOwnAccount = true; 
       } // MKI, MIB-9972 ---End
      dismissLoadingScreen();
    } else {
      dismissLoadingScreen();
      alert("Cannot Set Default Account");
    }
  }
}

function srvGetBankListforQR() {
  showLoadingScreen();
  var getBankList_inputparam = {};
  invokeServiceSecureAsync("getBankList", getBankList_inputparam, getBankListServiceCallBackQR);
}


function getBankListServiceCallBackQR(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      bankListArray = [];
      globalSelectBankData = [];
      // This locale is used to change bank name in list to current locale
      var locale = kony.i18n.getCurrentLocale();
      var collectionData = resulttable["Results"];
      for (var i = 0; i < resulttable.Results.length; i++) {
        var obj = {};
        //
        if ((resulttable.Results[i].bankStatus == "01" || resulttable.Results[i].bankStatus.toUpperCase() == "ACTIVE") && (resulttable.Results[i].smartFlag.toUpperCase() == "Y" || resulttable.Results[i].orftFlag.toUpperCase() == "Y")) {
          if (kony.string.startsWith(locale, "en", true) == true) obj.lblBanklist = resulttable.Results[i].bankNameEng;
          else
            obj.lblBanklist = resulttable.Results[i].bankNameThai;
          obj.bankCode = resulttable.Results[i].bankCode;
          obj.orftFlag = resulttable.Results[i].orftFlag;
          //
          bankListArray.push(obj);
        }
        if (locale == "en_US") {
          tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameEng"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"], collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]];
        } else {
          tempRecord = [collectionData[i]["bankCode"], collectionData[i]["bankNameThai"], collectionData[i]["bankShortName"], collectionData[i]["orftFlag"], collectionData[i]["smartFlag"], collectionData[i]["bankAccntLen"], collectionData[i]["bankAccntLengths"], collectionData[i]["bankNameEng"], collectionData[i]["bankNameThai"]];
        }
        globalSelectBankData.push(tempRecord);
      }
      gblBANKREF = getBankNameCurrentLocaleTransfer(gblisTMB);
      checkCrmProfileInqForQRPay();
      //popBankList.segBanklist.setData(bankListArray);
      //popBankList.lblFlag.text = "Bank";
      //dismissLoadingScreen();
    } else {
      dismissLoadingScreen();
      if (resulttable["errMsg"] != undefined) {
        alert(resulttable["errMsg"]);
      } else {
        alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
      }
    }
  }
}
function frmQRPaymentSuccessPreShow(){

  frmQRPaymentSuccess.LabelHeader.text = kony.i18n.getLocalizedString("TRComplete_Title");
  frmQRPaymentSuccess.LabelSave.text = kony.i18n.getLocalizedString("TRShare_Image");
  frmQRPaymentSuccess.LabelMessenger.text = kony.i18n.getLocalizedString("TRShare_Messenger");
  frmQRPaymentSuccess.LabelLine.text = kony.i18n.getLocalizedString("TRShare_Line");
  frmQRPaymentSuccess.LabelMore.text = kony.i18n.getLocalizedString("More");

  //frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("msgQRPaymentSuccess");
  //frmQRPaymentSuccess.LabelToAccountName.text = gblQRPayData["toAccountName"];
  if(gblQRPayData["banckCode"]== "11"){
    kony.print("Inside 11");
    frmQRPaymentSuccess.LabelToAccountName.text = gblqrToUsrName;
  }else{
    frmQRPaymentSuccess.LabelToAccountName.text = gblQRPayData["toAccountName"];
  }
  if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
    frmQRPaymentSuccess.LabelToAccountPhone.text = maskMobileNumber(gblQRPayData["mobileNo"]);
  } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
    frmQRPaymentSuccess.LabelToAccountPhone.text = maskCitizenID(gblQRPayData["cId"]);
  } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
    frmQRPaymentSuccess.LabelToAccountPhone.text = maskeWalletID(gblQRPayData["eWallet"]);
  } else if(gblQRPayData["billerID"] !== undefined && gblQRPayData["billerID"] !==""){
    frmQRPaymentSuccess.LabelToAccountName.text = gblQRDefaultAccountResponse["pp_toAcctName"];
  }


  //=============Cahgned by mayank========================Start
  var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
  frmQRPaymentSuccess.LabelAmount.text = formattedAmount[0];
  frmQRPaymentSuccess.LabelAmountDigits.text = formattedAmount[1];
  //frmQRPaymentSuccess.LabelAmount.text = commaFormatted(gblQRPayData["transferAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); // Changed by: mayan
  //frmQRPaymentSuccess.LabelAmount.text = gblQRPayData["transferAmt"] +" "+ kony.i18n.getLocalizedString("currencyThaiBaht");
  //=============Cahgned by mayank====================================End

  if(gblQRPayData["fee"] !== undefined && gblQRPayData["fee"] != "" && gblQRPayData["fee"] != "0.00"){
    frmQRPaymentSuccess.LabelFee.text = kony.i18n.getLocalizedString("keyFee") + gblQRPayData["fee"] + " " + kony.i18n.getLocalizedString("keyBaht");
    frmQRPaymentSuccess.LabelFee.setVisibility(true);    } else {
      frmQRPaymentSuccess.LabelFee.setVisibility(false);
    }

  frmQRPaymentSuccess.ButtonScanMore.text = kony.i18n.getLocalizedString("btnScanMore");
  frmQRPaymentSuccess.ButtonDone.text = kony.i18n.getLocalizedString("key_ScanQR_Home");
}

function frmQRPaymentSuccessOnClickDone(){
  onClickBackFromQRSuccess();
}

function loadMapforQrPreLoginPay() {
  kony.print("loadMapforQrPay");
  var inputParam = {}
  //gblSelTransferMode = 2;
  gblQRPayData["mobileNo"] = gblqrmobileno;
  gblQRPayData["transferAmt"] = gblqrTransferAmnt;
  showLoadingScreen();
  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
    inputParam["billPayInd"] = "billPayInd";
  }else{
    inputParam["transferFlag"] = "true";
  }invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackloadMapforPreloginQrPay);
}



function callBackloadMapforPreloginQrPay(status, resulttable) {
  kony.print("inside callBackloadMapforPreloginQrPay");
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      kony.print("Success");
      var initDPParam = resulttable["initDPParam"];
      if(initDPParam!=undefined&&initDPParam!=null&&initDPParam!=""){
        gblDPPk = resulttable["initDPParam"][0]["pk"];
		gblDPRandNumber = resulttable["initDPParam"][0]["randomNumber"];
      }
      dismissLoadingScreen();
      //frmQRSuccessOnClickPay();
      var bankList = globalSelectBankData.length;
	  if (bankList == 0 || bankList == "0") {
		srvGetBankListforQR();
	  } else {
		checkCrmProfileInqForQRPay()();
	  }
    } else {
      dismissLoadingScreen();
      if (resulttable["errMsg"] == "No Valid Accounts Available for Transfer") {
        alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"));
      } else {
        alert(" " + resulttable["errMsg"]);
      }
    }
  }
}


function loadvaluesforScreenshotqr(status, resultTable)
{
  alert("CHecking"+resultTable[transferAmount]);

}
function frmQRSuccessOnDoneTextAmount(){

  var amount =  frmQRSuccess.tbxEnterAmount.text;
  amount = onDoneEditingAmountValue(amount);

  if (kony.string.containsChars(amount, ["."]))
    amount = amount;
  else
    amount = amount + ".00";


  if(amount != ""){
    amount = amount + " " +kony.i18n.getLocalizedString("currencyThaiBaht");
  }
  frmQRSuccess.tbxEnterAmount.text = amount;
  frmQRSuccess.flxFullBody.setFocus(true);
  if(amount == "" || amount == "0" || amount == "0.00"){
    showAlertWithCallBack(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), 
                          kony.i18n.getLocalizedString("info"),callBackfrmQRSuccessOnDoneTextAmount); 
  } else {
    gblQRPayData["transferAmt"] = removeCommos(amount);
    invokeQRPaymentDefaultAccountServiceCalFee();
  }
}
function callBackfrmQRSuccessOnDoneTextAmount(){
  if(frmQRSuccess.tbxEnterAmount.text.length > 0){
    frmQRSuccess.tbxEnterAmount.skin = "txtBlue300";
    frmQRSuccess.tbxEnterAmount.focusSkin = "txtBlue300";
  } else{
    frmQRSuccess.tbxEnterAmount.skin = "txtBlue48px";
    frmQRSuccess.tbxEnterAmount.focusSkin = "txtBlue48px";
  }
  frmQRSuccess.flxPayFooter.setVisibility(false);
  gblQRPayData["transferAmt"]="";
  //frmQRSuccess.tbxEnterAmount.setFocus(true);
}
function frmQRSuccessOnTextChangeAmount(){
  var enteredAmount = frmQRSuccess.tbxEnterAmount.text;
  if(isNotBlank(enteredAmount)) {
    enteredAmount = kony.string.replace(enteredAmount, ",", "");
    if(!(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0)){
      frmQRSuccess.tbxEnterAmount.text = commaFormattedTransfer(enteredAmount);
    }
  }
  if(frmQRSuccess.tbxEnterAmount.text.length > 0){
    frmQRSuccess.tbxEnterAmount.skin = "txtBlue300";
    frmQRSuccess.tbxEnterAmount.focusSkin = "txtBlue300";
  }else{
    frmQRSuccess.tbxEnterAmount.skin = "txtBlue48px";
    frmQRSuccess.tbxEnterAmount.skin = "txtBlue48px";
  }
}
function frmQRSuccessOnClickSelectAccount(){
  if (gblQRPayData["transferAmt"] != undefined && gblQRPayData["transferAmt"] != "" && gblQRPayData["transferAmt"] != null) {
    if(isSignedUser){
      onSelectFromAccntQRPay();
    } else {
      gblQrSetDefaultAccnt = true;
	  
      if(frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left == "0%"){
        frmMBPreLoginAccessesPin.FlexQuickBalanceContainer.left="-100%";
        frmMBPreLoginAccessesPin.FlexLoginContainer.left="0%";
      }
      frmMBPreLoginAccessesPin.flexCrossmark.setVisibility(true);
      frmMBPreLoginAccessesPin.flexHeader.setVisibility(false);
      frmMBPreLoginAccessesPin.flexSwipeContainer.top = "20%"
      frmMBPreLoginAccessesPin.FlexContainer0b16eb3557e0043.setVisibility(false);
      frmMBPreLoginAccessesPin.imgQRHeaderLogo.setVisibility(true);
      frmMBPreLoginAccessesPin.LabelLoginForQR.setVisibility(true);
      frmMBPreLoginAccessesPin.LabelLoginForQR.text = kony.i18n.getLocalizedString("msgeLoginforQRPayment");
      frmMBPreLoginAccessesPin.FlexLine.setVisibility(true);
      frmMBPreLoginAccessesPin.FlexLoginContainer.top = "9%"; 
	  showAccessPinScreenQR();
    }
  } else {
    alert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"));
  }
}
function frmQRSelectAccountOnClickBack(){
  showQRScanningLanding();
}


function frmQRPaymentSuccesPostShow(){
  //generateQRforTransRefId();
}

function generateQRforTransRefId(){
  
  if(gblShowQRPayslip == "ON"){
    showLoadingScreen();
    //gblShowQRPayslip = false;
    frmQRPaymentSuccess.flexTransactionDetailsQR.isVisible = true;
    frmQRPaymentSuccess.flexTransactionDetails.isVisible = false;
    var inputParams = {};
    inputParams["qrCodeType"] = "qrBillPayment";
    //frmQRPaymentSuccess.QRImage.isVisible = false;
    if(frmQRPaymentSuccess.lblTransNPbAckRefDes.text != ""){
     var originalAmountVal = frmQRPaymentSuccess.lblTransNPbAckTotVal.text; 
     originalAmountVal = originalAmountVal.substring(0,originalAmountVal.indexOf('.'))+ "<a href=''>"+ originalAmountVal.substring(originalAmountVal.indexOf('.'),originalAmountVal.length)+"</a>"
     frmQRPaymentSuccess.lblTransNPbAckTotVal.text = originalAmountVal;
     inputParams["transactionRef"] = frmQRPaymentSuccess.lblTransNPbAckRefDes.text; 
     frmQRPaymentSuccess.lblTransNPbAckRefDesQR.text = frmQRPaymentSuccess.lblTransNPbAckRefDes.text;
     frmQRPaymentSuccess.lblSmartDateValQR.text = frmQRPaymentSuccess.lblSmartDateVal.text; //mki, MIB-13325
       
     invokeServiceSecureAsync("BOTQRCodeGenerator", inputParams, generateQRforTransRefIdCallBack);
  }
  }else{
     frmQRPaymentSuccess.flexTransactionDetailsQR.isVisible = false;
    frmQRPaymentSuccess.flexTransactionDetails.isVisible = true;
    
  }
  
  
}

function generateQRforTransRefIdCallBack(status,resulttable){
 dismissLoadingScreen();
 if(status == 400){
   if(resulttable["opstatus"] == 0){
   			if(resulttable["qrDataSet"].length > 0){
                frmQRPaymentSuccess.QRImage.isVisible = true;
  	 			frmQRPaymentSuccess.QRImage.base64 = resulttable["qrDataSet"][0]["base64QR"];
                  if(gblQRPaymentCompleteShareFlow == "saveimage"){
                  onclickQRPaymentSaveImageGenCode();
                }else if(gblQRPaymentCompleteShareFlow == "messenger"){
                  onClickQRPaymentMesGenCode();
                }else if(gblQRPaymentCompleteShareFlow == "line"){
                  onClickQRPaymentLineGenCode();
                }else if(gblQRPaymentCompleteShareFlow == "others"){
                  onClickQRPaymentOthersGenCode();
                }
      }
   }
 }
 
}

function onclickQRPaymentSaveImage(){
  gblQRPaymentCompleteShareFlow = "saveimage"
  generateQRforTransRefId();
}

function onclickQRPaymentMessenger(){
  gblQRPaymentCompleteShareFlow = "messenger"
  generateQRforTransRefId();
}

function onclickQRPaymentLine(){
  gblQRPaymentCompleteShareFlow = "line"
  generateQRforTransRefId();
}

function onclickQRPaymentOthers(){
  gblQRPaymentCompleteShareFlow = "others"
  generateQRforTransRefId();
}


function onclickQRPaymentSaveImageGenCode(){
   //#ifdef iphone
    //#define CHANNEL_CONDITION__g1a635da356d4a3e8348670095350556_iphone
    //#endif
    //#ifdef CHANNEL_CONDITION__g1a635da356d4a3e8348670095350556_iphone
    screenShotCall.call(this,"screenshot");
    //#endif
    //#ifdef android
    //#define CHANNEL_CONDITION__iddc7f61b0f64cca9a28f315edf9d0b1_android
    //#endif
    //#ifdef CHANNEL_CONDITION__iddc7f61b0f64cca9a28f315edf9d0b1_android
    shareIntentmoreCallandroid.call(this,"screenshot", "Transfer");
    //#endif
}

function onClickQRPaymentMesGenCode(){
  shareIntentCall.call(this,"facebook", "Transfer");
}

function onClickQRPaymentLineGenCode(){
   shareIntentCall.call(this, "line", "Transfer");
}

function onClickQRPaymentOthersGenCode(){
    //#ifdef iphone
    //#define CHANNEL_CONDITION__f0acbf4811ae42e8bbfbdc7d6accdb75_iphone
    //#endif
    //#ifdef CHANNEL_CONDITION__f0acbf4811ae42e8bbfbdc7d6accdb75_iphone
    shareIntentmoreCall.call(this,"more", "Transfer");
    //#endif
    //#ifdef android
    //#define CHANNEL_CONDITION__i49a987015bf47baaa7ef609972919c9_android
    //#endif
    //#ifdef CHANNEL_CONDITION__i49a987015bf47baaa7ef609972919c9_android
    shareIntentmoreCallandroid.call(this,"more", "Transfer");
    //#endif
}




function loadMapforQrPayNew() {
  kony.print("loadMapforQrPay Post Login New");
  var inputParam = {}
  //gblSelTransferMode = 2;
  showLoadingScreen();
  if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
    inputParam["billPayInd"] = "billPayInd";
  }else{
    inputParam["transferFlag"] = "true";
  } 
  invokeServiceSecureAsync("customerAccountInquiry", inputParam, callBackloadMapforQrPayNew);
}



function callBackloadMapforQrPayNew(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      kony.print("Success");
      var initDPParam = resulttable["initDPParam"];
      if(initDPParam!=undefined&&initDPParam!=null&&initDPParam!=""){
        gblDPPk = resulttable["initDPParam"][0]["pk"];
		gblDPRandNumber = resulttable["initDPParam"][0]["randomNumber"];
      }
	  var bankList = globalSelectBankData.length;
      //TODO: EDonation
      if(gbleDonationType == "Barcode_Type"){
        checkCrmProfileInqForQRPay();
      }else{
        if (bankList == 0 || bankList == "0") {
          srvGetBankListforQR();
        } else {
          checkCrmProfileInqForQRPay();
        }
      }

    } else {
      dismissLoadingScreen();
      if (resulttable["errMsg"] == "No Valid Accounts Available for Transfer") {
        alert(kony.i18n.getLocalizedString("MB_CommonError_NoSA"));
      } else {
        alert(" " + resulttable["errMsg"]);
      }
    }
  }
}

function generateQRforTransRefIdTransCal() {
    if (gblShowQRPayslip == "ON") {
        //  var currForm = kony.application.getCurrentForm();
        //showLoadingScreen();
        var inputParams = {};
        inputParams["qrCodeType"] = "qrBillPayment";
        if (frmTransfersAckCalendar.lblTransNPbAckRefDes.text != "") {
            inputParams["transactionRef"] = frmTransfersAckCalendar.lblTransNPbAckRefDes.text;
            invokeServiceSecureAsync("BOTQRCodeGenerator", inputParams, generateQRforTransRefIdTransCalCallback);
        }
    }
}

function generateQRforTransRefIdTransCalCallback(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            if (resulttable["qrDataSet"].length > 0) {
                //  var currForm = kony.application.getCurrentForm();
                frmTransfersAckCalendar.imgQR.base64 = resulttable["qrDataSet"][0]["base64QR"];
              if(gblTransferCompleteShareFlow == "saveimage"){
                 onClickSaveimageTransCalGenCode();
              }  else if(gblTransferCompleteShareFlow == "messenger"){
                onClickMesTransCalGenCode();
              } else if(gblTransferCompleteShareFlow == "line"){
                onClickLineTransCalGenCode();
              } else if(gblTransferCompleteShareFlow == "others"){
                onClickOthersTransCalGenCode();
              }
            }
        } else {
            dismissLoadingScreen();
        }
    } else {
        dismissLoadingScreen();
    }
}

