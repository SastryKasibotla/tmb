var mobileNumberCheckTimerId = "";
function showAlertMobileNotMatched(keyMsg, KeyTitle) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handle2
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);

	function handle2(response) {
      frmActivationAttention.show();
    }
	return;
}

function activationAlertForSettings(keyMsg, KeyTitle) {
    var keyCancelBtn = kony.i18n.getLocalizedString("keyCancelButton");
    var keySettingsBtn = "";
    //#ifdef iphone
  	    keySettingsBtn = kony.i18n.getLocalizedString("keyOK");
   //#endifW
  
   //#ifdef android
 	   keySettingsBtn = kony.i18n.getLocalizedString("keySetting");
  //#endif
	var tittleMessage = kony.i18n.getLocalizedString("WIFI_Topic");
  var keyMsg = kony.i18n.getLocalizedString("WIFI_Desc");
   if(gbleKYCflow){
     keyMsg= kony.i18n.getLocalizedString("eKYC_WIFI_Desc");
   }
   //Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_CONFIRMATION,
		alertTitle: tittleMessage,
		yesLabel: keySettingsBtn,
		noLabel: keyCancelBtn,
		alertHandler: callBackActivationAlertForSettings
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}


	
function callBackActivationAlertForSettings(response){
kony.print("In side call back callBackShowAlertTouchMessage");
		//#ifdef android
 	 	if(response == true){
          kony.print("Navigate to device settings screen");	
          navigatetoWIFISettings(); 
		}
  	//#endif
}

function checkUserPushNotificationEnable(callbackFunction) {
	if (arePushNotificationsEnabled() == true) {
		callbackFunction.call();
	} else {
		 var title = "";
        var yeslbl = kony.i18n.getLocalizedString("keyOK");
        var nolbl = kony.i18n.getLocalizedString("keyLater");
        var alertPushMessage=kony.i18n.getLocalizedString("keyCheckDeviceNotific");
		var basicConf = {
			message: alertPushMessage,
			alertType: constants.ALERT_TYPE_CONFIRMATION,
			alertTitle: title,
			yesLabel: yeslbl,
			noLabel: nolbl,
			alertHandler: function (response) {
				if (response == true) {
					navigatetoPushNotificationSettings();
				} else {
					callbackFunction.call();
				}
			}
		};
		var pspConf = {};
		var infoAlert = kony.ui.Alert(basicConf, pspConf);
	}
}

function checkUserPushNotification(alertPushMessage, callbackFunction, needPushNotification) {
  
  if(GLOBAL_UV_STATUS_FLAG == "ON"){

  if (arePushNotificationsEnabled() == true) {
        //showLoadingScreen();
        callbackFunction.call();
    } else {
        var title = "";
        var yeslbl = kony.i18n.getLocalizedString("notificationAllow");
        var nolbl = kony.i18n.getLocalizedString("notificationNotAllow");
        var basicConf = {
            message: alertPushMessage,
            alertType: constants.ALERT_TYPE_CONFIRMATION,
            alertTitle: title,
            yesLabel: yeslbl,
            noLabel: nolbl,
            alertHandler: function(response) {
                if (response == true) {
                    navigatetoPushNotificationSettings();
                } else {
                    //showLoadingScreen();
                  	if (needPushNotification) {
						return false;                    	
                    }else{
                    	callbackFunction.call();
                    }
                }
            }
        };
        var pspConf = {};
        var infoAlert = kony.ui.Alert(basicConf, pspConf);
    }
	
	}else{
    
    callbackFunction.call();
    
  }
    
}


function callServiceStartMB() {
    onClickOfStartBtnForMB();
}

function initFrmRegAttention(){

  frmActivationAttention.btncancel.onClick = showMBakingscreenNew;
  frmActivationAttention.btnnext.onClick = onClickAttentionNext;
 // frmActivationAttention.btncancel.onClick = showMBakingscreenNew;
  frmActivationAttention.btnSetings.onClick = navigatetoWIFISettings;
  frmActivationAttention.btnSettingCancel.onClick = onClickSettingsCancel;
  frmActivationAttention.flexSettingsPopup.onTouchEnd=blanckFunction;
  
  frmActivationAttention.lblRegAttentionTitle.text = kony.i18n.getLocalizedString("CKM_txtTittle");
  frmActivationAttention.richRegAttentiontext.text = kony.i18n.getLocalizedString("CKM_txtTopic");
  frmActivationAttention.lblAttentionTitleText.text = kony.i18n.getLocalizedString("CKM_txtTitle");
 // frmActivationAttention.CKM_btnCancel.text = kony.i18n.getLocalizedString("CKM_btnCancel");
 // frmActivationAttention.CKM_btnNext.text = kony.i18n.getLocalizedString("CKM_btnNext");
  frmActivationAttention.richTextSettingsHeadder.text = kony.i18n.getLocalizedString("WIFI_Topic");
  frmActivationAttention.lblwifiinformation.text = kony.i18n.getLocalizedString("WIFI_Desc");
  frmActivationAttention.btnSetings.text = kony.i18n.getLocalizedString("WIFI_btnSetting");
  frmActivationAttention.btnSettingCancel.text = kony.i18n.getLocalizedString("WIFI_btnCancel");
  frmActivationAttention.btncancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmActivationAttention.btnnext.text = kony.i18n.getLocalizedString("Next");
  frmActivationAttention.preShow =preshowActivationAttention; 
}

function preshowActivationAttention(){
  changeStatusBarColor();
  //checkMobileNumberServlet();
    //#ifdef android
              frmActivationAttention.onDeviceBack=blanckFunction;
  //#endif
  frmActivationAttention.btncancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  frmActivationAttention.btnnext.text = kony.i18n.getLocalizedString("Next");
  
  frmActivationAttention.lblRegAttentionTitle.text = kony.i18n.getLocalizedString("CKM_txtTittle");
  frmActivationAttention.richRegAttentiontext.text = kony.i18n.getLocalizedString("CKM_txtTopic");
  frmActivationAttention.lblAttentionTitleText.text = kony.i18n.getLocalizedString("CKM_txtTitle");
 // frmActivationAttention.CKM_btnCancel.text = kony.i18n.getLocalizedString("CKM_btnCancel");
 // frmActivationAttention.CKM_btnNext.text = kony.i18n.getLocalizedString("CKM_btnNext");
  frmActivationAttention.richTextSettingsHeadder.text = kony.i18n.getLocalizedString("WIFI_Topic");
  frmActivationAttention.lblwifiinformation.text = kony.i18n.getLocalizedString("WIFI_Desc");
  frmActivationAttention.btnSetings.text = kony.i18n.getLocalizedString("WIFI_btnSetting");
  frmActivationAttention.btnSettingCancel.text = kony.i18n.getLocalizedString("WIFI_btnCancel");
  frmActivationAttention.btnnext.onClick = onClickAttentionNext;
  if(gblActivationCurrentForm == "TMBConfirm"){
    frmActivationAttention.lblRegAttentionTitle.text = kony.i18n.getLocalizedString("WMB_txtTitle");
  	frmActivationAttention.richRegAttentiontext.text = kony.i18n.getLocalizedString("WMB_txtTopic");
  	frmActivationAttention.lblAttentionTitleText.text = kony.i18n.getLocalizedString("WMB_txtDesc");
    frmActivationAttention.btncancel.onClick=onClickUVApprovalGoToAccountSummary;
  }else  if(gblActivationCurrentForm == "cardlessWithdraw"){
    frmActivationAttention.lblRegAttentionTitle.text = kony.i18n.getLocalizedString("WMB_txtTitle");
  	frmActivationAttention.richRegAttentiontext.text = kony.i18n.getLocalizedString("WMB_txtTopic");
  	frmActivationAttention.lblAttentionTitleText.text = kony.i18n.getLocalizedString("WMB_txtDesc");
    frmActivationAttention.btncancel.onClick = showAccountSummaryFromMenu;	
  }else{
    frmActivationAttention.btncancel.onClick = showMBakingscreenNew;
    
  }

}


function settingGestureForRegSucess(widgetSwipe) {

	var setSwipe = {
		fingers: 1,
		swipedistance: 2,
		swipevelocity: 100
	};
	
	swipeGesture = widgetSwipe.setGestureRecognizer(2, setSwipe, onSwipeRegSuccess);
	
}

function blanckFunction(){
  
}

function onClickAttentionNext(){
 
  showLoadingScreen();
  kony.print("Printing the current time "+kony.os.time());
  mobilenumberCehckDelay();
  
}

function checkUserConnectivity() {
  if(gblActivationWithWifi) {
    onClickUVAttentionNext();  
    return;
  }
  if(isActiveNetworkAvailable() == false){

   retryInActivationNetworkCheck();

  }else if(isNetworkType3G() == true){

    gblRetryCountRequestOTP = "0";
    
    if(gblActivationCurrentForm == "cardlessWithdraw" || gblActivationCurrentForm == "TMBConfirm"){
       //verifyMobileNumberForCardlessActivation(); 
		checkMobileNumberServletCardLess();
      kony.print("checkUserConnectivity - mobileNumberFromUserDevice "+mobileNumberFromUserDevice);     
    }else{
      showLoadingScreen();
       checkMobileNumberServlet();  
    }
  }else{

    //frmActivationAttention.flexSettingsPopup.setVisibility(true);
	activationAlertForSettings();
  }
}

function callBackNetworkcheck(response){
 if (response == true)
	{
    	onClickAttentionNext();
	}
    
 }

function onClickSettingsCancel(){
  
  frmActivationAttention.flexSettingsPopup.setVisibility(false);
  
}

function onClickCancelRegAttention(){
  
  var previousForm = kony.application.getPreviousForm();
 	previousForm.show(); 
  
}
  
function ActvatnCodeValidatn(text) {
	if (text == null) return false;
	var txtLen = text.length;
	/*According to common validatev10.0*/
	/*
	var charArray = ["|", "|", "o", "O", "0"];
	var isIPCharchek = kony.string.containsNoGivenChars(text, charArray);
	var pat1 = /[A-Z]/g;
	var pat2 = /[a-z]/g;
	var pat3 = /[0-9]/g;
	var pat4 = /[-!$%^&*()_+|~=`{}\[\]:\";\s'<>?,.@#\/]/g;
	var isUpperCasechk = pat1.test(text);
	var isLowerCaseChk = pat2.test(text);
	var isNumCheck = pat3.test(text);
	var isSpecialCharCheck = pat4.test(text);
	//
	//if (txtLen != 8 || isIPCharchek == false || isSpecialCharCheck == true) {AS PER ANURAG'S COMFIRMATION MAIL REMOVED THE CLIENT SIDE VALIDATION
	*/
	if (txtLen > 8) {
		//alert("Incorrect ActivationCode, please re-enter");
		return false;
	}
	return true;
}

function trassactionPwdValidatn(text) {
	if (text == null) return false;
	var txtLen = text.length;
	var pat1 = /[A-Za-z]/g;
	var pat2 = /[0-9]/g
	var isAlpha = pat1.test(text);
	var isNum = pat2.test(text);
	var i = 0;
	for (i = 0; i < txtLen; i++) {
		if (text[i] == ' ') return false;
	}
	if (txtLen < 8 || txtLen > 20 || isAlpha == false || isNum == false) {
		//alert("Not a valid Transaction Password");
		return false;
	}
	return true;
}

function onEditAccNum(txt) {
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 3 || i == 8) {
				iphenText += '-';
			}
		}
		if (flowSpa) {
			frmMBActivation.txtAccountNumberspa.text = iphenText;
		} else {
			frmMBActivation.txtAccountNumber.text = iphenText;
		}
	}
	
	gblPrevLen = currLen;
}
/*
 * This method is used to validate the entered account number by length and
 * by checking account digits via double modulus 10 method
 *
 * Logic of Double modulus method logic:
 * ----------------------------------------
 * Take the account number and leave the last digit.
 * Write "2" and "1" under the account digits till last digit -1
 * Multiply the account digit with "2"/"1" accordingly
 *  if the multiplication results in two digit number and both numbers
 * Add all the digits after multiplication and do a mod operation by 10.
 * The result of mod operation should be equal to the last digit of the
 * account number that was ignored in beginning. If not, throw an error
 * message saying invalid account number.
 *
 * Eg:
 *   AccNum : 0 4 9 2 1 8 4 5 2 8
 *   Logic of Double add double modulus 10:
 *      0 4 9  2 1 8 4 5 2 | 8
 *      2 1 2  1 2 1 2 1 2 | (Ignore last digit)
 *   Multiply 0 4 18 2 2 8 8 5 4   (If multiply results in double digit, add both )
 *      0 4 9  2 2 8 8 5 4
 *   Add    42
 *   Mod 10  2
 *      Subtract 10-2 = 8 (This is same as the last digit of the account number)
 */

function accDoubleAddValidtn(text) {
	if (text == null) return false;
	var temp = "" + text;
	
	var txtLen = temp.length;
	
	var weight = [];
	var digit_account = [];
	var accountNum = [];
	for (i = 0; i < txtLen; i++) {
		accountNum[i] = 0;
		
	}
	i = 0;
	while (text) {
		accountNum[txtLen - 1 - i] = text % 10;
		text = (text - (text % 10)) / 10;
		i++;
		
	}
	if (txtLen != 10) {
		//alert("INVALID ACCOUNT NUMBER");
		return false;
	} else {
		weight[0] = [2]
		weight[1] = [1]
		weight[2] = [2]
		weight[3] = [1]
		weight[4] = [2]
		weight[5] = [1]
		weight[6] = [2]
		weight[7] = [1]
		weight[8] = [2]
		var length = 1
		var digit_multiply_result = 0
		var summary_multiply_result = 0
		var mod_result = 0
		var subtract_result = 0
		var position;
		for (position = 0; position < txtLen - 1; position++) {
			digit_account[position] = accountNum[position]
			digit_multiply_result = digit_account[position] * weight[position];
			if (digit_multiply_result > 9 && digit_multiply_result < 100) {
				var temp1 = digit_multiply_result % 10;
				var temp2 = (digit_multiply_result - digit_multiply_result % 10) / 10;
				digit_multiply_result = temp1 + temp2;
			}
			summary_multiply_result = summary_multiply_result + digit_multiply_result;
		}
		mod_result = summary_multiply_result % 10;
		if (mod_result != "0") {
			subtract_result = 10 - mod_result;
		} else {
			subtract_result = mod_result;
		}
		if (subtract_result != accountNum[9]) {
			return false;
		}
	}
	
	return true;
}

//Working as expected and this is taken from ME Team
function checkCitizenID(text){
	if(isNaN(text)) {
		return false;
	}	
	if(text.length!=13) { 
		return false;
	}	
	for(i=0, sum=0; i < 12; i++) {
		sum += parseFloat(text.charAt(i))*(13-i); 
	}
	
	if((11-sum%11)%10!=parseFloat(text.charAt(12))) { 
		return false; 
	}	
	return true;
}	


function onEditMobileNumber(txt) {
	if(flowSpa){
		frmChangeMobNoTransLimitMB.txtChangeMobileNumber.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
	}
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 5) {
				iphenText += '-';
			}
		}
		frmChangeMobNoTransLimitMB.txtChangeMobileNumber.text = iphenText;
		frmMBActiAtmIdMobile.tbxMobileNo.text = iphenText;
	}
	
	gblPrevLen = currLen;
}

function onEditCitiZenID(txt) {
	if (txt == null) return false;
	var noChars = txt.length;
	var temp = "";
	var i, txtLen = noChars;
	var currLen = noChars;
	
	if (gblPrevLen < currLen) {
		for (i = 0; i < noChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
			
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 0 || i == 4 || i == 9 || i == 11) {
				iphenText += '-';
			}
		}
		
		//if (flowSpa) {
			//frmMBActivation.txtIDPass.text = iphenText;
			//
		//} else {
			frmMBActivation.txtIDCitizen.text = iphenText;
			frmMBActiAtmIdMobile.tbxIdCitizen.text = iphenText;
		//}
	}
	gblPrevLen = currLen;
}
function onEditCitiZenIDLoanKYC(txt) {
	if (txt == null) return false;
	var noChars = txt.length;
	var temp = "";
	var i, txtLen = noChars;
	var currLen = noChars;
	
	if (gblPrevLen < currLen) {
		for (i = 0; i < noChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
			
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 0 || i == 4 || i == 9 || i == 11) {
				iphenText += '-';
			}
		}
		
		//if (flowSpa) {
			//frmMBActivation.txtIDPass.text = iphenText;
			//
		//} else {
			frmMBLoanKYC.tbxCustID.text = iphenText;
		//}
	}
	gblPrevLen = currLen;
}
function PasprtValidatn(text) {
	if (text == null) return false;
	var txtLen = text.length;
	/*According to common validatev10.0*/
	//var pat = /[^./()A-Z0-9]/g;
	//var isValidPassport =pat.test(text);
	//var isAlpNum = kony.string.isAsciiAlphaNumeric(text);
	if (txtLen > 25) {
		//alert("Passport Number is Not Valid");  
		return false;
	}
	return true;
}

function emailValidatn(txt) {
	if (txt == null) return false;
	if(isNotBlank(txt))
		txt = txt.trim();
	var txtLen = txt.length;
	var i, j;
	var pat1 = /[=<>()"',:;\s]/g;
	/*According to common validatev10.0*/
	var noChars = pat1.test(txt);
	if (noChars == true) {
		return false;
	}
	var atIndex, count = 0;
	for (i = 0; i < txtLen; i++) {
		if (txt[i] == '@') {
			count++;
			atIndex = i;
		}
	}
	if ((atIndex < 1) || (atIndex == txtLen - 1)) return false;
	//
	if (count != 1) {
		//alert("INVALID EMAIL , PLEASE RE-ENTER");
		return false;
	} else {
		if ((atIndex + 13) <= txtLen) {
			temp = txt.substring(atIndex + 1, atIndex + 13);
			if (temp == "facebook.com") {
				//alert("INVALID EMAIL , PLEASE RE-ENTER");
				return false;
			}
			
		}
		count = 0;
		for (i = atIndex; i < txtLen; i++) {
			if (txt[i] == '.') {
				//
				count++;
				var temp = "";
				if ((i + 3) > txtLen) {
					//alert("INVALID EMAIL , PLEASE RE-ENTER");
					return false;
				} else {
					temp = txt[i + 1] + txt[i + 2];
					var flag = kony.string.isAsciiAlphaNumeric(temp);
					var flag1 = kony.string.isAsciiAlpha(temp);
					var flag2 = kony.string.isNumeric(temp);
					
					if (!(flag || flag1 || flag2)) {
						//alert("");
						return false;
					}
				}
			}
		}
		if (count == 0) {
			//alert("INVALID EMAIL , PLEASE RE-ENTER");
			return false;
		}
	}
	return true;
}

function MblNickName(text) {
	if ((text == null) || (text == "")||text.trim().length=="") return false;
	var txtLen = text.length;
	//var patt1 = /^[A-Za-z0-9._@-]+$/;def 709
	//var patt1=/^[A-Za-z0-9\s._@-]+$/;
	//var isAlphNum = patt1.test(text);
	
	
	//var pat2 = /[\u0E00-\u0E7F]+/;
	var pat2 = /^[A-Za-z0-9\u0E00-\u0E7F\s.-]+$/
	var isAlphNum = pat2.test(text);
	//Below fix has been done for UAT DEF2596 
	if (isAlphNum) {
		if( txtLen <=20&&txtLen>=3){
			return true;
		}
		else
		{
		return false;
		}
	}
	return false;
	/*}
	 else {		
		if(isAlphNum == true&& txtLen <=40&&txtLen>=3 )
		{
		return true;
		}
		else
		{
		return false;
		}
	}*/
}

function NickNameValid(text) {
	if ((text == null) || (text == "")||text.trim().length=="") return false;
	var txtLen = text.length;
	var pat2 = /^[A-Za-z0-9\u0E00-\u0E7F\s._@-]+$/
	var isAlphNum = pat2.test(text);
	//Below fix has been done for UAT DEF2596 
	if (isAlphNum) {
		if( txtLen <=20&&txtLen>=3){
			return true;
		}
		else
		{
		return false;
		}
	}
	return false;
}

function MyNoteValid(text) {
	if ((text == null) || (text == "")||text.trim().length=="") return true;
	var txtLen = text.length;
	var pat2 = /^[A-Za-z0-9\u0E00-\u0E7F\s._@-]+$/
	var isAlphNum = pat2.test(text);	
	if (isAlphNum) {
		return true;
	}else{
		return false;
	}	
	return false;
}

function AccountNameValid(text) {
	if ((text == null) || (text == "")||text.trim().length=="") return false;
	var txtLen = text.length;
	
	var pat2 = /^[A-Za-z0-9\u0E00-\u0E7F\s._@-]+$/
	var isAlphNum = pat2.test(text);
	//Below fix has been done for UAT DEF2596 
	if (isAlphNum) {
		if( txtLen <=40&&txtLen>=3){
			return true;
		}
		else
		{
		return false;
		}
	}
	return false;
}
function OTPValdatn(text) {
	if (text == null) return false;
	var txtLen = text.length;
	
	var isNum = kony.string.isNumeric(text);
	
	if (!isNum) {
		alert(kony.i18n.getLocalizedString("keyInvalidOTP"));
		return false;
	}
	if(flowSpa)
	{		
		if(gblTokenSwitchFlag == true)
		{
			gblOTPLENGTH=6;
			otpValidationspa(text);
		}
	}
	if (txtLen > gblOTPLENGTH) {
		alert("OTP code should be maximum " + gblOTPLENGTH + " characters");
		return false;
	}	
	return true;
}

function accsPwdValidatn(text) {
	if (text == null) return false;
	var txtLen = text.length;
	var isNum = kony.string.isNumeric(text);
	if (txtLen != 6 || isNum == false) {
		//alert("Access Password is Not Valid");
		return false;
	}
	return true;
}

function accValidatn(text) {
	if (text == null) return false;
	var txtLen = text.length;
	var isNum = kony.string.isNumeric(text);
	if (txtLen != 10 || isNum == false) {
		//alert("Entered AccountNumber is not valid");
		return false;
	}
	
	return true;
}

function mbActivationValidatn() {
	var actCodeFlag = ActvatnCodeValidatn(frmMBActivation.txtActivationCode.text);
	// changed the i18n key from invalidActiCode to invalidAccNo because when the Activation code is blank or wrong we need to throw "Incorrect information. Your transaction cannot be proceeded"
	var blankActioncode = kony.i18n.getLocalizedString("keyMBEnterActivationCode");
	var invalidActiCode1 = kony.i18n.getLocalizedString("invalidActicodes");
	var blankAccountNumber = kony.i18n.getLocalizedString("keyMBEnterDepositAccountNo");
	var blankIdPass = kony.i18n.getLocalizedString("keyMBPleaseEnterIDorPassportNo");
	var info1 = kony.i18n.getLocalizedString("info");
	var okk = kony.i18n.getLocalizedString("keyOK");
  	
	if(flowSpa && !gblSetPwdSpa){
		if (frmMBActivation.txtActivationCode.text == "" || frmMBActivation.txtActivationCode.text == null) {
			showAlert(blankActioncode, info1);
			//	alert("INVALID ACTIVATION CODE"); 
			//frmMBActivation.txtActivationCode.skin = txtErrorBG;
			return false;
		}
		if (actCodeFlag == false) {
				showAlert(invalidActiCode1, info1);
				//	alert("INVALID ACTIVATION CODE");
				//frmMBActivation.txtActivationCode.skin = txtErrorBG;
				return false;
		}
	}else if (!flowSpa){
		if (frmMBActivation.txtActivationCode.text == "" || frmMBActivation.txtActivationCode.text == null) {
			showAlert(blankActioncode, info1);
			//	alert("INVALID ACTIVATION CODE"); 
			//frmMBActivation.txtActivationCode.skin = txtErrorBG;
			return false;
		}
		if (actCodeFlag == false) {
				showAlert(invalidActiCode1, info1);
				//	alert("INVALID ACTIVATION CODE");
				//frmMBActivation.txtActivationCode.skin = txtErrorBG;
				return false;
		}
	}
	if (flowSpa) {
		var temp = frmMBActivation.txtAccountNumberspa.text;
	} else {
		var temp = frmMBActivation.txtAccountNumber.text;
	}
	var invalidAccNo1 = kony.i18n.getLocalizedString("invalidActicodes");
	if (temp == null || temp == "") {
		showAlert(blankAccountNumber, info1);
		//	alert("INVALID ACCOUNT NUMBER");
		//frmMBActivation.txtAccountNumber.skin = txtErrorBG;
		return false
	}
	
	var tempLen = temp.length;
	for (i = 0; i < tempLen; i++) {
		if (temp[i] == '-') {
			temp = temp.replace("-", "");
		}
	}
	
	/*if (flowSpa) {
        if ((/[^a-z0-9\.]/gi).test(temp)) {
            showAlert(invalidAccNo1, info1);
			frmMBActivation.txtAccountNumberspa.skin = txtErrorBG;
            return false;
        }
    }*/
    if(flowSpa){
		if(!(kony.string.isNumeric(temp))){
			showAlert(invalidAccNo1, info1);
			//frmMBActivation.txtAccountNumberspa.skin = txtErrorBG;
			//showAlertRcMB(kony.i18n.getLocalizedString("keyErrAccntNoInvalid"), kony.i18n.getLocalizedString("info"), "info")
			return false;
		}
	}
    
	var accNumFlag1 = accDoubleAddValidtn(temp);
	
	var accNumFlag2 = accValidatn(temp);
	var accNumType = getAccType(temp);
	var temp = frmMBActivation.txtIDPass.text;
  	var tempCitizen = frmMBActivation.txtIDCitizen.text;
	// changed the i18n key from invalidIDorPassport to invalidAccNo because when the citizenID/passport.no is blank or wrong we need to throw "Incorrect information. Your transaction cannot be proceeded"
	var invalidID = kony.i18n.getLocalizedString("invalidActicodes");
  	if(frmMBActivation.txtIDPass.isVisible){
        if (temp == null || temp == "") {
          showAlert(blankIdPass, info1);
          //	 alert("INVALID ID/Pass Number");
          //frmMBActivation.txtIDPass.skin = txtErrorBG;
          return false;
      }
      
      
    }
  	if(frmMBActivation.txtIDCitizen.isVisible)
    {
      if (tempCitizen == null || tempCitizen == "") {
          showAlert(blankIdPass, info1);
          //	 alert("INVALID ID/Pass Number");
          //frmMBActivation.txtIDPass.skin = txtErrorBG;
          return false;
      }
      
    }
	
	/*
	if (flowSpa) {
        if ((/[^a-z0-9\.]/gi).test(temp)) {
            showAlert(invalidAccNo1, info1);
			frmMBActivation.txtIDPass.skin = txtErrorBG;
            return false;
        }
    }
    */
  	temp = temp.replace(/\-/g, "");
	tempCitizen = tempCitizen.replace(/\-/g, "");
	var citizezIDFlag = checkCitizenID(tempCitizen);
	var passFlag = PasprtValidatn(temp);
	var isCitizen = false;
	var isPassport = false;
  	if(frmMBActivation.txtIDCitizen.isVisible)
    {
      	isCitizen = true;
		isPassport = false;
    }else if(frmMBActivation.txtIDPass.isVisible){
      	isCitizen = false;
		isPassport = true;			      	
    }
	//alert("values of validation are isCitizen"+isCitizen+"isPassport "+isPassport+"passFlag "+passFlag+"citizezIDFlag "+citizezIDFlag);
  	  	//Block2
  	if(frmMBActivation.txtIDCitizen.isVisible){
      	if ((isCitizen && !citizezIDFlag)) {
			showAlert(kony.i18n.getLocalizedString("invalidActicodes"), info1);
			//	alert("INVALID ID/Pass Number");
			//frmMBActivation.txtIDPass.skin = txtErrorBG;
			return false;
        }
      	
    }
	else if(frmMBActivation.txtIDPass.isVisible){
      	if ((isPassport && !passFlag)) {
          showAlert(kony.i18n.getLocalizedString("invalidActicodes"), info1);
          //	alert("INVALID ID/Pass Number");
          //frmMBActivation.txtIDPass.skin = txtErrorBG;
          return false;
      }
    }
  	//Block2 End
  	//Block1
  	if (gblSetPwdSpa && !accNumFlag2) {
	    // remove validate account check digit on client site, 
		showAlert(invalidAccNo1, info1);
		return false;
	}
  	
  	else if (!gblSetPwdSpa && (accNumFlag1 == false || accNumFlag2 == false)) {
		showAlert(invalidAccNo1, info1);
		return false;
		//	alert("INVALID ACCOUNT NUMBER");
	} else if (accNumType == "NA for Activation") {
		showAlert(kony.i18n.getLocalizedString("keyECAcctValErr00001"), info1);
		return false;
	}
  	//Block1 End

     else {
		if (flowSpa) {
			if (gblSetPwdSpa == true) {
				mbActivationConfirm("12", temp, isCitizen, isPassport); //Reset Password flow
			} else {
				mbActivationConfirm("11", temp, isCitizen, isPassport); //For activation or add device
			}
		} else {
				//always for MB  when we use acitvation code send action code 09
				// else crm inquiry will not work 
				mbActivationConfirm("09", temp, isCitizen, isPassport); 
		}
		//var deviceInfo = kony.os.deviceInfo();
		//if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			TrusteerDeviceIdActivation();
		//}else if (gblDeviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				getUniqueIDActivation();
			//#endif
		//#endif
		//}
		//frmMBActiConfirm.show();
		//Test Coding for add device flow comment below lines of code during real time integration
		if (frmMBActivation.txtActivationCode.text == "123456Aa") {
			//Delete this variable from gbl variables while integration.
			gblTestAddDevice = true;
		} else {
			gblTestAddDevice = false;
		}
	}
}
function TrusteerDeviceIdActivation()
{
	/**
	 * The below code is commented, MIB-6454 Analysis of Trusteer code flow
	 * 
	 * We are going to send the KonydeviceId isnted of Trusteer unique id
	 ***/
		
	/*
	var lObject = {};
	lObject["deviceId"] = deviceIDActivation;
	lObject["message"] = "yes";
	
	ffinamespace.getDeviceId(lObject);
	*/
	deviceIDActivation(getDeviceID());
	
}
function deviceIDActivation(str) {
	GBL_UNIQ_ID = str;
}
function getUniqueIDActivation(){

	/**
	 * The below code is commented, MIB-6454 Analysis of Trusteer code flow
	 * 
	 * We are going to send the KonydeviceId isnted of Trusteer unique id
	 ***/
		
	/**
	 * 
	 * 
    if(TmbTrusteerFFIObject == null)
        TmbTrusteerFFIObject = new trust.TmbTrusteerFFI();
    else
        TmbTrusteerFFIObject.getUniqueID(callbackUniqueActivation);
	**/
	
		deviceIDActivation(getDeviceID());
}
function callbackUniqueActivation(idresult){
	GBL_UNIQ_ID = idresult;
}
/*
*************************************************************************************
		Module	: mbActivationConfirm
		Author  : Kony
		Purpose : Invoke crmProfileEnquiry service
****************************************************************************************
*/

function mbActivationConfirm(actionCodeFlow, nationID, isCitizen, isPassport) {
	//kony.application.showLoadingScreen(frmLoading, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, null);
  	frmMBActivation.txtActivationCode.skin = txtNormalBG;
	if(!flowSpa)
	{
		frmMBActivation.txtAccountNumber.skin = txtNormalBG;
	}
	frmMBActivation.txtIDPass.skin = txtNormalBG;
  	frmMBActivation.txtIDCitizen.skin = txtNormalBG;
	showLoadingScreen();
	inputParam = {};
	gActivationCode = frmMBActivation.txtActivationCode.text;
	if (flowSpa) {
		var acctNum = frmMBActivation.txtAccountNumberspa.text.replace(/\-/g, "");
	} else {
		var acctNum = frmMBActivation.txtAccountNumber.text.replace(/\-/g, "");
	}
  	var idNum="";
  	if(frmMBActivation.txtIDPass.isVisible)
    {
		idNum = frmMBActivation.txtIDPass.text.replace(/\-/g, "");
      	frmMBActiConfirm.lblIDNoVal.text = frmMBActivation.txtIDPass.text;
    }else if(frmMBActivation.txtIDCitizen.isVisible)
    {
      	idNum = frmMBActivation.txtIDCitizen.text.replace(/\-/g, "");
      	frmMBActiConfirm.lblIDNoVal.text = frmMBActivation.txtIDCitizen.text;
    }
	inputParam["actionCode"] = actionCodeFlow;
	inputParam["deviceId"] = getDeviceID();
	inputParam["activationCode"] = frmMBActivation.txtActivationCode.text;
	inputParam["accountNumber"] = acctNum;
	if (flowSpa) {
		inputParam["channelId"] = "01";
	} else {
		if (GLOBAL_MB_CHANNEL != null || GLOBAL_MB_CHANNEL != "") {
			inputParam["channelId"] = GLOBAL_MB_CHANNEL;
		} else {
			inputParam["channelId"] = "02";
		}
	}
	inputParam["activationCounter"] = gblActivationCounter;
	inputParam["idCounter"] = gblIdCounter;
	//inputParam["nationalId"] = idNum ;
	if (isCitizen) {
		inputParam["idType"] = "CI";
	} else if (isPassport) {
		inputParam["idType"] = "PP";
	}
		inputParam['accountStatusCode'] = '';
		inputParam['acctId'] = '';
		inputParam['acctType'] = '';
		//inputParam['activationCounter'] = '0'
		// inputParam['channelId'] = '02'
		inputParam['currencyCode'] = '';
		// inputParam['deviceId'] = "000000000000000"//"1234567890"
		inputParam['fiIdent'] = "";
		inputParam['rmId'] = "";
		inputParam["idNumber"] = idNum;
  		inputParam["mobileNumberFromUserDevice"] = mobileNumberFromUserDevice;
       inputParam["networkProvider"] = networkProvider;
		invokeServiceSecureAsync("KonyCRMProfileInquiry", inputParam, callBackOnClickActivation)
}
/*
*************************************************************************************
		Module	: callBackOnClickActivation
		Author  : Kony
		Purpose : Capturing the result and showing Confirmation screen
****************************************************************************************
*/
 successFlag = false;
function callBackOnClickActivation(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			kony.application.dismissLoadingScreen();
			gblPHONENUMBER = resulttable["mobileNo"];
			if (isNotBlank(gblPHONENUMBER)) {
				frmMBActiConfirm.lblPhNoVal.text="xxx-xxx-"+gblPHONENUMBER.substring(6, 10);
			}else{
				frmMBActiConfirm.lblPhNoVal.text="xxx-xxx-0000";
			}
			gblEmailId = resulttable["emailId"];
			gblActionCode = resulttable["actnCode"];
			if(resulttable["mbFlowStatusId"]!=null){
				gblUserLockStatusMB = resulttable["mbFlowStatusId"];
				gblMBFlowStatus = resulttable["mbFlowStatusId"];
			}	
			if(resulttable["ibFlowStatusIdRs"]!=null){
				gblIBFlowStatus = resulttable["ibFlowStatusIdRs"]
			}
			
			successFlag = true;
			frmMBActiConfirm.show();
		} else {
          if (resulttable["errCode"] == "UV_msgMobileNotMatched") {
				kony.application.dismissLoadingScreen();
				showAlertMobileNotMatched(kony.i18n.getLocalizedString("UV_msgMobileNotMatched"), kony.i18n.getLocalizedString("UV_msgMobileNotMatchedTitle"));
				return false;
			}else if (resulttable["errCode"] == "KonyDvMgmtErr00001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "AppSrvErr00001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECAppSrvErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "AcctValErr00001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("keyECAcctValErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "AcctValErr00002") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("invalidAccNo"), kony.i18n.getLocalizedString("info"));
				frmMBActivation.txtAccountNumber.skin = txtNormalBG;
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECMIBInqErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00002") {
				kony.application.dismissLoadingScreen();
				//activityLogServiceCall("005","MIBInqErr00002","02");
				showAlert(kony.i18n.getLocalizedString("ECMIBInqErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqIDErr00001" || resulttable["errCode"] == "MIBInqIDErr00002" ||
				resulttable["errCode"] == "MIBInqIDErr00003") {
				if (resulttable["errCode"] == "MIBInqIDErr00001") {
					gblIdCounter = "1";
				} else if (resulttable["errCode"] == "MIBInqIDErr00002") {
					gblIdCounter = "2";
				} else if (resulttable["errCode"] == "MIBInqIDErr00003") {
					gblIdCounter = "3";
				}
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECMIBInqErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00004") {
				gblIdCounter = "0";
				kony.application.dismissLoadingScreen();			
				if (flowSpa == true && gblSetPwdSpa == true) {
					popupSpaForgotPassword.lblText.text=kony.i18n.getLocalizedString("keyIncorectReActInfoSPA");
					popupSpaForgotPassword.show();
				} else {
					showAlertWithCallBack(kony.i18n.getLocalizedString("ECMIBInqErr00004"), kony.i18n.getLocalizedString("info"),
					calBackLockAlert);
				}
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00005") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECMIBInqErr00005"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "MIBInqErr00006") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("KeyECMIBInqErr00006"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (resulttable["errCode"] == "MIBInqErr00007") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECMIBInqErr00007"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "CRMPrfInqErr00001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECCRMPrfInqErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "CRMPrfInqErr00002") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECCRMPrfInqErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPCtrErr00001" || resulttable["errCode"] == "VrfyOTPCtrErr00002" ||
				resulttable["errCode"] == "VrfyOTPCtrErr00003") {
				if (resulttable["errCode"] == "VrfyOTPCtrErr00001") {
					gblActivationCounter = "1";
				} else if (resulttable["errCode"] == "VrfyOTPCtrErr00002") {
					gblActivationCounter = "2";
				} else if (resulttable["errCode"] == "VrfyOTPCtrErr00003") {
					gblActivationCounter = "3";
				}
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("invalidAccNo"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00002") {
				gblActivationCounter = "0";
				kony.application.dismissLoadingScreen();
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				showAlertWithCallBack(kony.i18n.getLocalizedString("ECVrfyOTPErr00002"), kony.i18n.getLocalizedString("info"),
					calBackLockAlert);
				//showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00003") {
				kony.application.dismissLoadingScreen();
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00004") {
				kony.application.dismissLoadingScreen();
				//activityLogServiceCall("005","VrfyOTPErr00002","02");
				showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00004"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00005") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyOTPErr00005"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "KonyDvMgmtErr00002") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECKonyDvMgmtErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "JavaErr00001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyTknErr00001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyTknErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyTknErr00002") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyTknErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "VrfyTknErr00003") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECVrfyTknErr00003"), kony.i18n.getLocalizedString("info"));
				return false;
			} else {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
				return false;
			}
		}
		kony.application.dismissLoadingScreen();
	}
}

function mbSetPwdValidatn() {
	var invalidAccPIN = kony.i18n.getLocalizedString("keyIncorrectAccPIN");
	var blankAccess = kony.i18n.getLocalizedString("keyMBEnterAccessPin");
	var blankDevicename = kony.i18n.getLocalizedString("keyMBEnterDevice");
	var invalidTranPwd = kony.i18n.getLocalizedString("keyIncorrectTransPass");
	var blankTransPass =kony.i18n.getLocalizedString("keyblankTransPass");
	var invalidDivName = kony.i18n.getLocalizedString("invalidDeviceName");
	var info1 = kony.i18n.getLocalizedString("info");
	var okk = kony.i18n.getLocalizedString("keyOK");
	if (gblflag) {
		if (frmMBsetPasswd.txtTempAccess.text == "" || frmMBsetPasswd.txtTempAccess.text == null){
		showAlert(blankAccess, info1);
		frmMBsetPasswd.txtTempAccess.skin = txtErrorBG;
		return false;
		} 
		var accessPINFlag = accsPwdValidatn(frmMBsetPasswd.txtTempAccess.text);
		glbAccessPin = frmMBsetPasswd.txtTempAccess.text;
	} else {
		if (frmMBsetPasswd.txtAccessPwd.text == null || frmMBsetPasswd.txtAccessPwd.text == ""){
		showAlert(blankAccess, info1);
		frmMBsetPasswd.txtAccessPwd.skin = txtErrorBG;
		return false;
		}		
		var accessPINFlag = accsPwdValidatn(frmMBsetPasswd.txtAccessPwd.text);
		glbAccessPin = frmMBsetPasswd.txtAccessPwd.text;
	}
	if (accessPINFlag == false) {
		frmMBsetPasswd.txtTempAccess.skin = txtErrorBG;
		frmMBsetPasswd.txtAccessPwd.skin = txtErrorBG;
		showAlert(invalidAccPIN, info1);
		return false;
	} 
	if (gblflag) {
		if (frmMBsetPasswd.txtTemp.text == "" || frmMBsetPasswd.txtTemp.text == null) {
		showAlert(blankTransPass, info1);
		frmMBsetPasswd.txtTemp.skin = txtErrorBG
		return false;
		}
		var trasPwdFlag = trassactionPwdValidatn(frmMBsetPasswd.txtTemp.text);
		glbTrasactionPwd = frmMBsetPasswd.txtTemp.text
	} else {
		if (frmMBsetPasswd.txtTransPass.text == null ||frmMBsetPasswd.txtTransPass.text == ""){ 
		showAlert(blankTransPass, info1);
		frmMBsetPasswd.txtTransPass.skin = txtErrorBG
		return false;
		}
		var trasPwdFlag = trassactionPwdValidatn(frmMBsetPasswd.txtTransPass.text);
		glbTrasactionPwd = frmMBsetPasswd.txtTransPass.text //Defined global variable to use them in reset password service
	}
	if (trasPwdFlag == false && gblActionCode != "23") {
		frmMBsetPasswd.txtTransPass.skin = txtErrorBG;
		frmMBsetPasswd.txtTemp.skin = txtErrorBG;
		showAlert(invalidTranPwd, info1);
		return false;
	}
	
	if (gblSetPwd == false) {

		gFromConnectAccount = true
		if (gblActionCode == "21") {
			onClickSetPasswordNext();
		} else if (gblActionCode == "23") {
			var devNameFlag = MblNickName(frmMBsetPasswd.txtDeviceName.text);
			if(frmMBsetPasswd.txtDeviceName.text == null || frmMBsetPasswd.txtDeviceName.text == "")
			{
				showAlert(blankDevicename, info1);
				return false;
			}
			if (devNameFlag == false) {
				showAlert(invalidDivName, info1);
				return false;
			}
			addNewDeviceFlow(frmMBsetPasswd.txtAccessPwd.text);
		}
	} else {
		resetPassword();
	}
}
/*
*************************************************************************************
		Module	: onClickSetPasswordNext
		Author  : Kony
		Purpose : Invoking create user ECAS service
****************************************************************************************
*/

function onClickSetPasswordNext() {
	showLoadingScreen();
	inputParam = {};
	inputParam["userStoreId"] = "";
	inputParam["loginId"] = "";
	inputParam["userName"] = "";
	inputParam["segmentId"] = "MIB";
	inputParam["mbAcPwd"] = glbAccessPin;
	inputParam["mbTxPwd"] = glbTrasactionPwd;
	inputParam["session"] = "";
	invokeServiceSecureAsync("createUser", inputParam, callBackCustAccForEcas)
}

/*
*************************************************************************************
		Module	: callBackCustAccForEcas
		Author  : Kony
		Purpose : Invoking customerAccountEnquiry service
****************************************************************************************
*/

function callBackCustAccForEcas(status, resulttable) {
	if (status == 400) {
		if (resulttable["errCode"] == "AccessPwdErr") {
			kony.application.dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("invalidAxPin"), kony.i18n.getLocalizedString("info"));
			frmMBsetPasswd.txtAccessPwd.skin = txtErrorBG;
			frmMBsetPasswd.txtTempAccess.text = txtErrorBG;
			return false;
		} else if (resulttable["errCode"] == "TxPwdError") {
			kony.application.dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("invalidTxnPwd"), kony.i18n.getLocalizedString("info"));
			frmMBsetPasswd.txtTransPass.skin = txtErrorBG
			frmMBsetPasswd.txtTemp.skin = txtErrorBG;
			return false;
		}
		if (resulttable["opstatus"] == 0) {
			//inputparam = {};
			kony.application.dismissLoadingScreen();
			if( gblEmailId != null && gblEmailId != undefined && gblEmailId != "" )
			frmConnectAccMB.tbxPopupTractPwdtxt.text = gblEmailId
			frmConnectAccMB.show();
			//invokeServiceSecureAsync("customerAccountInquiry", inputparam, createUserOnEcas);
		} else {
			if (resulttable["errCode"] == "1000") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECCreateUserExits"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (resulttable["errCode"] == "40001") {
				kony.application.dismissLoadingScreen();
				showAlert(kony.i18n.getLocalizedString("ECCreateUserError"), kony.i18n.getLocalizedString("info"));
				return false;
			} else {
				kony.application.dismissLoadingScreen();
			if(resulttable["errMsg"] != undefined)
	        	{
	        		alert(resulttable["errMsg"]);
	        	}
	        	else
	        	{
	        		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	        	}
			}
			/*else {
			
				
				inputParam = {};
				inputParam["segmentId"] = "MIB";
				
				invokeServiceSecureAsync("deleteUser", inputParam, deleteUserFromECASCallBack);
			
			}*/
		}
	}
}

function deleteUserFromECASCallBack(status, resulttable) {
	
	if (status == 400) {
		
		
		
		
		if (resulttable["opstatus"] == 0) {
			
			
			kony.application.dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		kony.application.dismissLoadingScreen();
	}
}

function deviceID(str) {
	kony.print("CRMMATCH FPRINT GBL_FLOW_ID_CA="+GBL_FLOW_ID_CA);
	
	GBL_UNIQ_ID = str;
	if(str != null && str != undefined && str != "" ) {
	    switch(GBL_FLOW_ID_CA) {
		    case 1:
		            updateDeviceServiceCall(str);
		            break;
		    case 2:
		            updateDevicePartOfActivation(str);
		            break;
		    case 3:
		            showAccountSummLndnd(str);
		            break;
		    case 4:
		            invokeVerifyOTPKonyService(str);
		            break;
		    case 5:
		            forDeviceID(str);
		            break;
		    case 6:
		   			 sessionCheckAddResetPwdMB(str);
		   			 break; 
		   	case 7:
		   			 callTouchIdStatusService(str);
		   			 break; 
		   	case 8:	
					 MBSpaLoginService(str);	//Added Case 8 for ENH_207 to send uniqueid in IB login for MB Activation
					 break;			 
		    case 9:	
					 validateAtmCardDetails(str);	//Added Case 9 for ENH_207_6 to send uniqueid in ATM verify details for MB Activation
					 break;	
			case 10:	
					 onClickOTPRequest(str);	//Added Case 10 for ENH_207 to send uniqueid in Activation Code flow for MB Activation
					 break;
			case 11:	
					 invokeQuickBalanceService(str);	//Added Case 11 for calling quickbalance service before login
					 break;
			case 12:
					 validateCardNumberDetails(str);
					 break;			 
		    default:
		            break;
		}
	} else {
	    if(GBL_FLOW_ID_CA == 1 || GBL_FLOW_ID_CA == 2){
	        showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keyFailedUniqueKeyGenAct"));
	    } else {
	        showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keyFailedUniqueGenLogin"));
	    }
	}
} 

function callbackUnique(idresult){
	kony.print("FPRINT: IN callbackUnique");
	kony.print("FPRINT: Unique Id Generated Before Login");
	GBL_UNIQ_ID = idresult;
	kony.print("FPRINT: GBL_UNIQ_ID="+GBL_UNIQ_ID);
	
	if(idresult !=null && idresult != undefined && idresult != "" ){
		//HARDWARE_ID = idresult;
	    switch(GBL_FLOW_ID_CA) {
		    case 1:
		            updateDeviceServiceCall(idresult);
		            break;
		    case 2:
		            updateDevicePartOfActivation(idresult);
		            break;
		    case 3:
		            showAccountSummLndnd(idresult);
		            break;
		    case 4:
		            invokeVerifyOTPKonyService(idresult);
		            break;
		    case 5:  
		            forDeviceID(idresult);
		            break;
		    case 6:
		   			 sessionCheckAddResetPwdMB(idresult);
		   			 break;
		   	case 7:
		   			 kony.print("FPRINT: calling callTouchIdStatusService for ANDROID");
		   			 callTouchIdStatusService(idresult);
		   			 break; 		   			 
		   	case 8:	
					 MBSpaLoginService(idresult);	//Added Case 8 for ENH_207 to send uniqueid in IB login for MB Activation
					 break;	
			case 9:	
					 validateAtmCardDetails(idresult);	//Added Case 9 for ENH_207_6 to send uniqueid in ATM verify details for MB Activation
					 break;
			case 10:	
					 onClickOTPRequest(idresult);	//Added Case 10 for ENH_207 to send uniqueid in Activation Code flow for MB Activation
					 break;	
			case 11:	
					 invokeQuickBalanceService(idresult);	//Added Case 11 for calling quickbalance service before login
					 break;	
			case 12:
					validateCardNumberDetails(idresult);
					break;	
			case 13:
					chkUserStatus(idresult);	
					break;						 		
		    default:
		            break;
		}
	} else {
		if(GBL_FLOW_ID_CA == 1 || GBL_FLOW_ID_CA == 2) {
		    showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keyFailedUniqueKeyGenAct"));
		} else {
		    showAlertForUniqueGenFail(kony.i18n.getLocalizedString("keyFailedUniqueGenLogin"));
		}  
	}
}

function accsPwdValidatnLogin(text) {
	if (text == null) return false;
	
	var txtLen = text.length;
	var isNum = kony.string.isNumeric(text);
	var invalidAccPwd = kony.i18n.getLocalizedString("invalidAxPin1");
	var info1 = kony.i18n.getLocalizedString("info");
	var okk = kony.i18n.getLocalizedString("keyOK");
// below part commented aspart of ENH205 and ENH051	
//	if (txtLen != 6 || isNum == false) {
//		showAlert(invalidAccPwd, info1);
//		return false;
//		gblNum = ""; 
//		//frmAfterLogoutMB.tbxAccessPIN.text = "";
//	} else {
		//if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			//frmAfterLogoutMB.tbxAccessPIN.setFocus(false);
		//#else
			//#ifdef android
			
			//#endif
		//#endif
		//}else if (gblDeviceInfo["name"] == "android"){
		
		//}
  		idealTimeout = true;
		showLoadingScreen();
		gblIndex = -1;
		isMenuShown = false;
		gFromConnectAccount = false
		gblAccountTable = "";
		//frmAccountSummaryLanding = null;
//		frmAccountSummaryLandingGlobals();
		//TMBUtil.DestroyForm(frmAccountSummaryLanding);
		serviceCheckFlag = true;
		GBL_FLOW_ID_CA = 3;
  		//getKonyDeviceVerify(getDeviceID());
  
		//if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPhone Simulator") {
		//#ifdef iphone
			TrusteerDeviceId();
		//}else if (gblDeviceInfo["name"] == "android"){
		//#else
			//#ifdef android
				getUniqueID();
			//#endif
		//#endif
		//}
	//}
}

function getKonyDeviceVerify(unqId) {
    //alert("in getDeviceNameForProfile");
    inputParam = {};
    GBL_UNIQ_ID = unqId;
    //inputParam["associationId"] = gblcrmId;
    inputParam["deviceId"] = unqId;
    inputParam["profileFlag"] = "true";
    invokeServiceSecureAsync("deviceEnquiry", inputParam, getKonyDeviceVerifycallBack)
}

function getKonyDeviceVerifycallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["Results"][0]["deviceId"] != null) {
                gblDeviceNickName = resulttable["Results"][0]["deviceNickName"];
                dismissLoadingScreen();
				//#ifdef iphone
					TrusteerDeviceId();
				//#endif
				//#ifdef android
					getUniqueID();
				//#endif
            } else {
                dismissLoadingScreen();
              showAlertDeviceIdTouchMessage(kony.i18n.getLocalizedString("deviceIdEnhancementUnlock"),kony.i18n.getLocalizedString("deviceIDUnlock"));
				//showAlertWithCallBack("Device ID not Found, Please reactivate your TMB Touch mobile app", kony.i18n.getLocalizedString("info"),reactivationDeviceIdFlow);
                return false;
            }
        }
    }
}

/*****************************************************************
 *     Name    : showAlertTouchMessageFPAndroid
 *     Module  : Touch ID
 *     Author  : Kony IT Services
 *     Purpose : Function Touch message
 *     Params  : None
 * 	   Changes :
 ******************************************************************/
function showAlertDeviceIdTouchMessage(keyMsg, KeyTitle) {
	//var KeyTitle = "";//"Sorry" kony.i18n.getLocalizedString("keyOK");
  //  var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage"); //ErrAndNoFinPrint
   // var okk = kony.i18n.getLocalizedString("keyOK");
    var keyCloseAppBtn = kony.i18n.getLocalizedString("deviceIDCloseApp");
    var keyUnlockBtn = kony.i18n.getLocalizedString("deviceIDUnlockNow");
   //Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_CONFIRMATION,
		alertTitle: KeyTitle,
		yesLabel: keyUnlockBtn,
		noLabel: keyCloseAppBtn,
		alertHandler: callBackShowAlertDeviceIdTouchMessage
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}
	
function callBackShowAlertDeviceIdTouchMessage(response){
	kony.print("In side call back showAlertDeviceIdTouchMessage");

	if(response == true){
		frmMBanking.show();
	}else{
      kony.application.exit();
    }
}

function reactivationDeviceIdFlow(){
    frmMBanking.show();
}

function chngAccepin() {
	var worngEntry = kony.i18n.getLocalizedString("invalidPwd3Entry");
	var invalidCurPin = kony.i18n.getLocalizedString("invalidCurrPIN");
	var invalidNwPIN = kony.i18n.getLocalizedString("invalidNewPin");
	var blankAccePIN = kony.i18n.getLocalizedString("blankNewAccPIN");
    // added the i18 key for blank Current access Pin as "blankCurrAccPIN" and blank New Access pin as "blankNewAccPIN1" for frmCMChgTransPwd	
	var blankAccePIN1 = kony.i18n.getLocalizedString("blankNewAccPIN1")
	var blankCurrAccePIN = kony.i18n.getLocalizedString("blankCurrAccPIN")
	var info1 = kony.i18n.getLocalizedString("info");
	var okk = kony.i18n.getLocalizedString("keyOK");
	var isBlockList = true;
	var oldPwd;  
	var newPwd;  
	
	if(frmCMChgAccessPin.tbxCurAccPin.isVisible == true){
		oldPwd = frmCMChgAccessPin.tbxCurAccPin.text;
		newPwd = frmCMChgAccessPin.txtAccessPwd.text;
	} else {
		oldPwd = frmCMChgAccessPin.tbxCurAccPinUnMask.text;
		newPwd = frmCMChgAccessPin.txtAccessPwdUnMask.text;
	}
	
	
	if(oldPwd.trim() == "" || oldPwd == null){
    	frmCMChgAccessPin.tbxCurAccPin.skin = txtErrorBG;
    	frmCMChgAccessPin.tbxCurAccPinUnMask.skin = txtErrorBG;
    	showAlert(blankCurrAccePIN, info1);
		return false;
	}
	
	if(newPwd.trim() == "" || newPwd == null){
    	frmCMChgAccessPin.txtAccessPwd.skin = txtErrorBG;
    	frmCMChgAccessPin.txtAccessPwdUnMask.skin = txtErrorBG;
    	showAlert(blankAccePIN1, info1);	
		return false;
	}
	var oldFlag;
	var newFlag;
	
	if(frmCMChgAccessPin.tbxCurAccPin.isVisible == true){
		oldFlag = accsPwdValidatn(frmCMChgAccessPin.tbxCurAccPin.text);
		newFlag = accsPwdValidatn(frmCMChgAccessPin.txtAccessPwd.text);
		isBlockList = validateBlockedAXPIN(frmCMChgAccessPin.txtAccessPwd.text);
	} else {
		oldFlag = accsPwdValidatn(frmCMChgAccessPin.tbxCurAccPinUnMask.text);
		newFlag = accsPwdValidatn(frmCMChgAccessPin.txtAccessPwdUnMask.text);
		isBlockList = validateBlockedAXPIN(frmCMChgAccessPin.txtAccessPwdUnMask.text);
	}
	
	//isBlockList = validateBlockedAXPIN(frmCMChgAccessPin.txtAccessPwd.text);
	
	if (oldFlag == false) {
		gblShowPwd++;
		frmCMChgAccessPin.tbxCurAccPin.skin = txtErrorBG;
		frmCMChgAccessPin.tbxCurAccPinUnMask.skin = txtErrorBG;
		showAlert(invalidCurPin, info1);
		return false;
	} else if (newFlag == false) {
		frmCMChgAccessPin.txtAccessPwd.skin = txtErrorBG;
		frmCMChgAccessPin.txtAccessPwdUnMask.skin = txtErrorBG;
		showAlert(invalidNwPIN, info1);
		return false;
	} else if (isBlockList == false) {
		showAlert(kony.i18n.getLocalizedString("keyInsecureAxPin"), info1);
		return false;
	} else {
        //Commented to by pass txn pwd authentication
		/*popupTractPwd.lblPopupTract7.setVisibility(true);
		popupTractPwd.lblPopupTract7.text = "Please enter your transaction password";
		popupTractPwd.lblPopupTract7.skin = lblPopupLabelTxt;
		showOTPPopup(kony.i18n.getLocalizedString("transPasswordSub"), "", "", onClickConfirmTranscPwd, 3);*/
        changeTxnPasswordcompositeService();
	}
}

function validateBlockedAXPIN(axPin) {
	if (GLOBAL_BLOCKED_ACCESS.length != 0) {
		for (i = 0; i < (GLOBAL_BLOCKED_ACCESS.length); i++) {
			var blcAxList = GLOBAL_BLOCKED_ACCESS[i]["BLOCKED_ACESS"];
			if (blcAxList == axPin) {
				return false;
			}
		}
	}
}

function recipientNameVal(text) {
	var temp = text;
	temp = temp.trim();
	if (temp == null || temp == undefined || temp == "") {
		return false;
	}
	else {
		var recipientName = /^[a-zA-Z0-9'\s]{3,40}$/ig;
		var resultRecipientName = recipientName.test(text);
		return resultRecipientName;
	}
}

function recipientMobileVal(text) {
	var resultRecipientMobile;
	//var recipientMobile = /^[0][8,9][0-9]{8}$/g;
	//var resultRecipientMobile = recipientMobile.test(text);
	
	var res = text.substring(0, 2);
	// changes as per DEF 2  --surender Appam
	if(text.length<10){
	resultRecipientMobile = false;
	}else{
	if (Gbl_StartDigsMobileNum.indexOf(res) < 0) 
		resultRecipientMobile = false;
	else resultRecipientMobile = true;
	}
	
	/*if (Gbl_StartDigsMobileNum.indexOf(res) < 0) 
		resultRecipientMobile = false;
	else resultRecipientMobile = true;*/
	
	return resultRecipientMobile;
}
// function to show Alert, that will accept two parameters Title and Message of Alert 

//function shoAlert(keyMsg, KeyTitle) {
//	var okk = kony.i18n.getLocalizedString("keyOK");
//	//Defining basicConf parameter for alert
//	var basicConf = {
//		message: keyMsg,
//		alertType: constants.ALERT_TYPE_INFO,
//		alertTitle: KeyTitle,
//		yesLabel: okk,
//		noLabel: "",
//		alertHandler: handle2
//	};
//	//Defining pspConf parameter for alert
//	var pspConf = {};
//	//Alert definition
//	var infoAlert = kony.ui.Alert(basicConf, pspConf);
//
//	function handle2(response) {}
//	return;
//}

//function saveTranPwd() {
//	if (gblShowPwd == 3) {
//		alert("Entered wrong Current Password 3 times")
//		isSignedUser = false;
//		invokeLogoutService();
//		//frmAfterLogoutMB.show();
//		return false;
//	}
//	var curPin = frmCMChgTransPwd.tbxTranscCrntPwd.text
//	var pat1 = /[A-Za-z]/g
//	var pat2 = /[0-9]/g
//	var isAlpha = pat1.test(curPin)
//	var isNum = pat2.test(curPin)
//	
//	if (isAlpha == false || isNum == false) {
//		frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtErrorBG;
//		gblShowPwd++;
//		alert("Please Enter atleast 1 alphabet and 1 numeric character for Current Password");
//		return false;
//	}
//	if (curPin == null || curPin.length < 8) {
//		gblShowPwd++;
//		frmCMChgTransPwd.tbxTranscCrntPwd.skin = txtErrorBG;
//		var msg = "Please enter atleast 8 Characters for Current Password";
//		var okk = kony.i18n.getLocalizedString("keyOK");
//		showAlert(msg, okk);
//		alert("Please enter atleast 8 Characters for Current Password");
//		return false;
//	}
//	var newPinVisibility = frmCMChgTransPwd.txtTransPass.isVisible
//	if (newPinVisibility) {
//		var newPin = frmCMChgTransPwd.txtTransPass.text
//		var pat1 = /[A-Za-z]/g
//		var pat2 = /[0-9]/g
//		var isAlpha = pat1.test(newPin)
//		var isNum = pat2.test(newPin)
//		if (isAlpha == false || isNum == false) {
//			frmCMChgTransPwd.txtTransPass.skin = txtErrorBG;
//			gblShowPwd++;
//			alert("Please Enter atleast 1 alphabet and 1 numeric character for New Password");
//			return false;
//		}
//		if (newPin == null || newPin.length < 8) {
//			frmCMChgTransPwd.txtTransPass.skin = txtErrorBG;
//			alert("Please enter atleast 8 Characters for New Password")
//			return false;
//		}
//	} else {
//		var newPin = frmCMChgTransPwd.txtTemp.text
//		var pat1 = /[A-Za-z]/g
//		var pat2 = /[0-9]/g
//		var isAlpha = pat1.test(newPin)
//		var isNum = pat2.test(newPin)
//		if (newPin == null || newPin.length < 8) {
//			frmCMChgTransPwd.txtTemp.skin = txtErrorBG;
//			alert("Please enter atleast 8 Characters for New Password")
//			return false;
//		}
//	}
//}
// function to show Alert with message and title

function showAlert(keyMsg, KeyTitle) {
	var okk = kony.i18n.getLocalizedString("keyOK");
	//Defining basicConf parameter for alert
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: handle2
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);

	function handle2(response) {}
	return;
}
/*
 * This method is used to return the type of account via account number
 */

function getAccType(accntNum) {
	var acctLength = accntNum.length;
	var fourthDigit;
	var accType = "";
	if (acctLength == 10) {
		fourthDigit = accntNum.charAt((GLOBAL_ACCT_NUM_POSITION - 1));
	}
	
	var numSA = GLOBAL_NUM_AT_POS_SA.split(",");
	
	if (fourthDigit == numSA[0] || fourthDigit == numSA[1] || fourthDigit == numSA[2]) {
		accType = "SA"; // "SA";
	} else if (fourthDigit == GLOBAL_NUM_AT_POS_CA) {
		accType = "CA"; // "CA";
	} else if (fourthDigit == GLOBAL_NUM_AT_POS_TD) {
		accType = "TD"; // "TD";
	} else {
		accType = "NA for Activation"
	}
	return accType;
}

function validationNickName(text) {
	if(!NickNameValid(text)){
		showAlert(kony.i18n.getLocalizedString("Valid_BillerNicknameMandatory"), kony.i18n.getLocalizedString("info"))
		return false;
	}
		var txtLen = text.length;
		var isNumeric = kony.string.isNumeric(text);
		//
		if (txtLen > 20 || isNumeric) {
			//alert("INVALID MOBILE NICK NAME, PLEASE RE-ENTER");
			return false;
		}
	else {
		return true;
	}
}

function validateRef2ValueMB(mytext) {
	//var mytext = frmAddTopUpToMB.txtRef2.text;
	//
	if (mytext.length>0) {
		var mypattern = /^[a-zA-Z0-9. \u0E00-\u0E7F\/-]*$/;
		var isValid = mypattern.test(mytext);
			return isValid;
	} else {
			return false;
	}
}

function validateRef2ValueAll(text) {
	if (text != null && text!= "") {
		var mypattern = /^[a-zA-Z0-9. \u0E00-\u0E7F\/-]*$/;
		var isValid = mypattern.test(text);
	} else {
		isValid = false;
	}
	return isValid;
}

function validateRef1ValueMB(mytext) {
	
	if (mytext.length>0) {
		var mypattern = /^[0-9]*$/;
		var isValid = mypattern.test(mytext);
		return isValid;
	} else {
		return false;
	}
}

function captchaValidation(text) {
	var pat1 = /^[a-z0-9]+$/;
	var isMatching = pat1.test(text);
	if ((!isMatching) || (text.length != 6)) {
		alert(kony.i18n.getLocalizedString("keyInvalidCaptcha"));
		return false;
	} else {
		IBLoginService();
		return true;
	}
}

function AccountNameCheck(text) {
	if ((text == null) || (text == "")||text.trim().length=="") return false;
	var txtLen = text.length;
	//var patt1 = /^[A-Za-z0-9._@-]+$/;def 709
	//var patt1=/^[A-Za-z0-9\s._@-]+$/;
	//var isAlphNum = patt1.test(text);
	
	
	//var pat2 = /[\u0E00-\u0E7F]+/;
	var pat2 = /^[A-Za-z0-9\u0E00-\u0E7F\s._@-]+$/
	var isAlphNum = pat2.test(text);
	//Below fix has been done for UAT DEF2596 
	if (isAlphNum) {
		if( txtLen <=40&&txtLen>=3){
			return true;
		}
		else
		{
		return false;
		}
	}
	return false;
}
function validateRef2Value() {
	var mytext = frmIBMyBillersHome.txtAddBillerRef2.text;
	if (mytext.length > 0) {
		var mypattern = /^[a-zA-Z0-9. \u0E00-\u0E7F\/-]*$/;
		var isValid = mypattern.test(mytext);
		return isValid;
	} else {
		return false;
	}
}

function amountValidation(text) {
	if ((text == null) || (text == "")) return false;
	var txtLen = text.length;
	var pat1 = /^\s*-?(\d+(\.\d{1,10})?|\.\d{1,10})\s*$/; //to test for number and decimal
	if ((!pat1.test(text)) || (text < 0)) return false;
	if (text[0] == '.') {
		text = '0' + text;
	}
	if ((/[.0]/g.test(text)) && (text[txtLen - 2] == '.')) {
		text += '0';
	}
	var parts = text.split(".");
	parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","); //to format the number into commas
	text = parts.join(".");
	return true;
}
/**
 * Method to validate the passwords for IB
 * @returns {}
 */

function validatePasswordProfileIB() {
    
    if(frmIBCMChgPwd.txtPassword.text.indexOf(" ") > 0 || frmIBCMChgPwd.txtRetypePwd.text.indexOf(" ") > 0){
		alert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"));
		return;
 	}
	if(frmIBCMChgPwd.txtPassword.text.match(/\s/g) || frmIBCMChgPwd.txtRetypePwd.text.match(/\s/g) > 0){
		alert("inside space");
		alert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"));
		return;
 	}
	if (frmIBCMChgPwd.tbxTranscCrntPwd.text == null || frmIBCMChgPwd.tbxTranscCrntPwd.text == ""){
		
		alert(kony.i18n.getLocalizedString("keyEmptyCurrentPassIB"));
		return;
	}
	if (frmIBCMChgPwd.txtPassword.text == null || frmIBCMChgPwd.txtPassword.text == "") {
		
		alert(kony.i18n.getLocalizedString("keyEmptyNewPasswordIB"));
		return;
	}
	if (frmIBCMChgPwd.txtRetypePwd.text == null || frmIBCMChgPwd.txtRetypePwd.text == ""){
		
		alert(kony.i18n.getLocalizedString("keyEmptyReTypePassIB"));
		return;
	}
	if(kony.string.containsChars(frmIBCMChgPwd.txtPassword.text, ["<"]) && kony.string.containsChars(frmIBCMChgPwd.txtPassword.text, [">"])){
		kony.print("inside my code")
		alert(kony.i18n.getLocalizedString("keyPasswordMinRequirement"));
		return;
 	}
	if (frmIBCMChgPwd.tbxTranscCrntPwd.text == frmIBCMChgPwd.txtPassword.text || frmIBCMChgPwd.tbxTranscCrntPwd.text ==
		frmIBCMChgPwd.txtRetypePwd.text) {
		
		alert(kony.i18n.getLocalizedString("keycurrentPassEqualsNewPassIB"));
		return;
	}
	if (frmIBCMChgPwd.txtPassword.text == kony.string.trim(gblUserName)) {
		
		alert(kony.i18n.getLocalizedString("keyUseridEqualsNewPassIB"));
		return;
	}
	if (frmIBCMChgPwd.txtPassword.text.match(/(.)\1{3,}/) == null && frmIBCMChgPwd.txtPassword.text.length > 7 && frmIBCMChgPwd.txtPassword.text.match(/[a-z]/gi) != null && frmIBCMChgPwd.txtPassword.text.match(/[0-9]/g) != null){
	   if (frmIBCMChgPwd.txtPassword.text == frmIBCMChgPwd.txtRetypePwd.text) {
		  
		  gblOTPFlag = true;
		  //invokeRequestOtpChngPWD();
		 // onMyProfilePwdNextIB();
		/*  var oldPwdIBChange = frmIBCMChgPwd.tbxTranscCrntPwd.text;
   		  var newPwdIBChange = frmIBCMChgPwd.txtPassword.text;
		  invokeSaveparamInSessionForPwdUserChange(oldPwdIBChange,newPwdIBChange,"2");*/
		  checkMyProfileChangePwdTokenIB();
	      }
	   else {
		
		alert(kony.i18n.getLocalizedString("KeyPassMismatchIB"));
		}
	}else {
		
		alert(kony.i18n.getLocalizedString("invalidPasswordIB"));
	}
}


function onEditMobileNumberRecp(txt) {
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if(numChars <= 10)
		gblprenum = txt;
	else
	{
		var form = kony.application.getCurrentForm();
		 form.tbxMobileNo.text = gblprenum;
	}
	kony.print("Text length" + numChars);
}

//ENH_207 New Activation Flow

function mbSetAccPinPwdValidatn() {
	showLoadingScreen();

	var invalidAccPIN = kony.i18n.getLocalizedString("keyIncorrectAccPIN");
	var blankAccess = kony.i18n.getLocalizedString("keyMBEnterAccessPin");
	var invalidTranPwd = kony.i18n.getLocalizedString("keyIncorrectTransPass");
	var blankTransPass =kony.i18n.getLocalizedString("keyblankTransPass");
	var info1 = kony.i18n.getLocalizedString("info");
	var okk = kony.i18n.getLocalizedString("keyOK");
	
	var accPin = frmMBSetAccPinTxnPwd.txtAccessPin.text;
	//var txnPwd = frmMBSetAccPinTxnPwd.txtTransPass.text;  //MIB-13232 - Upadte set Accesspin screen
	if (gblflag == 1) {
		txnPwd = frmMBSetAccPinTxnPwd.txtTransPassShow.text;
	} 
	if (!isNotBlank(accPin)){
		dismissLoadingScreen();
		showAlert(blankAccess, info1);
		clearAccessPin();
		return false;
	} 
	var accessPINFlag = accsPwdValidatn(accPin);
	glbAccessPin = accPin;
		

	if (accessPINFlag == false) {
		dismissLoadingScreen();
		showAlert(invalidAccPIN, info1);
		clearAccessPin();
		return false;
	} 
	//MIB-13232 - Upadte set Accesspin screen
  /*
	if (!isNotBlank(txnPwd)) {
		dismissLoadingScreen();
		showAlert(blankTransPass, info1);
		clearTxnPwd();
		return false;
	}
	var trasPwdFlag = trassactionPwdValidatn(txnPwd);
	glbTrasactionPwd = txnPwd;
		
	
	if (trasPwdFlag == false) {
		dismissLoadingScreen();
		clearTxnPwd();
		showAlert(invalidTranPwd, info1);
		return false;
	} */
	
	inputParam = {};
	inputParam["createUser_userStoreId"] = "";
	inputParam["createUser_loginId"] = "";
	inputParam["createUser_userName"] = "";
	inputParam["createUser_segmentId"] = "MIB";
	inputParam["createUser_mbAcPwd"] = encryptData(glbAccessPin);
	inputParam["createUser_mbTxPwd"] = ""; //encryptData(glbTrasactionPwd); //MIB-13232 - Upadte set Accesspin screen
	inputParam["createUser_session"] = "";
	
	invokeServiceSecureAsync("setPasswordComposite", inputParam, setPasswordCompositeCallBack)
	
}

function setPasswordCompositeCallBack(status, resulttable){
	if(status == 400){
		if (resulttable["opstatus"] == 0) {
          	kony.store.setItem("isUserStatusActive", true);
          kony.print("FPRINT 4 setting isUserStatusActive to true");
          if(null != resulttable["trusteerIdFromClient"] && undefined != resulttable["trusteerIdFromClient"] ){
             	 tmbSaveOnDevice("trusteerIdFromClient",resulttable["trusteerIdFromClient"] )
            }
          		gblStartClickFromActivationMB=true;
          
              if(GLOBAL_UV_STATUS_FLAG == "ON"){
                  
                  		 try {
                                registerAppWithPushNotificationsInfrastructure();
                          }catch(e){
                                kpns_log("registerAppWithPushNotificationsInfrastructure: Exception : "+e.message);
                                showUVActivationFail("Push Registration Failed");
                          }
                        //Cancelling Timer
                          cancelScheduledTimer("accPinPwdShowTimer");

                          notificationAddServiceForAddDeviceFlow();
                          encryptSecretKey(resulttable["secretKey"], resulttable["encryptKey"]);

                  }else{
                   		  //removing the KSID for reactivation.
                    	  kony.store.removeItem("ksid");
                		  kony.store.removeItem("ksidUpdated");
                    	  kony.store.removeItem("ksidsavedDate");
                    	  //
                          dismissLoadingScreen();
                    	  cancelScheduledTimer("accPinPwdShowTimer");
	
                          frmMBActiComplete.image250285458166.src = "iconcomplete.png";
                          frmMBActiComplete.label50285458167.text = kony.i18n.getLocalizedString("appReadytoUse");
                          frmMBActiComplete.btnStart.setVisibility(true);
                          notificationAddServiceForAddDeviceFlow();
                          encryptSecretKey(resulttable["secretKey"], resulttable["encryptKey"]);
                          frmMBActiComplete.show();
                          campaginService("image2447443295186","frmMBActiComplete","M");
                  }
          
          
		} else if(resulttable["opstatus"] == "-1") {
			//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			dismissLoadingScreen();	
          	frmMBanking.show();
		} else if (resulttable["errCode"] == "1000") {
          	dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECCreateUserExits"), kony.i18n.getLocalizedString("info"));
			clearAccessPin();
			clearTxnPwd();
			return false;
		} else if (resulttable["errCode"] == "40001") {
          	dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECCreateUserError"), kony.i18n.getLocalizedString("info"));
			clearAccessPin();
			clearTxnPwd();
			return false;
		} else if (resulttable["errCode"] == "AccessPwdErr") {
          	dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("invalidAxPin"), kony.i18n.getLocalizedString("info"));
			clearAccessPin();
			return false;
		} else if (resulttable["errCode"] == "TxPwdError") {
          	dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("invalidTransPwd"), kony.i18n.getLocalizedString("info"));
			clearTxnPwd();
			return false;
		} else if(resulttable["errMsg"] != undefined) {
          	dismissLoadingScreen();
    		alert(resulttable["errMsg"]);
			clearAccessPin();
			clearTxnPwd();
    		return false;
        } else {
          	dismissLoadingScreen();
    		alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
    		clearAccessPin();
			clearTxnPwd();
    		return false;
        }
	}
}

function clearTxnPwd() {
	frmMBSetAccPinTxnPwd.txtTransPass.text="";
	frmMBSetAccPinTxnPwd.txtTransPassShow.text="";
	frmMBSetAccPinTxnPwd.txtTransPass.skin = txtErrorBG;
	frmMBSetAccPinTxnPwd.txtTransPassShow.skin = txtErrorBG;
}

function mbActivationEmailDeviceNameValidatn() {
	//var deviceChk = frmMBActiEmailDeviceName.tbxDeviceName.text
    var deviceChk = frmMBSetEmail.lblDeviceName.text
    kony.print("deviceChk---->"+deviceChk);
	//var devNameFlag = MblNickName(frmMBActiEmailDeviceName.tbxDeviceName.text);
  	var devNameFlag = MblNickName(frmMBSetEmail.lblDeviceName.text);
  	var devNameFlag = true;
	var blankEmail = kony.i18n.getLocalizedString("keyMBEnteremailID");
	var blankDevicename = kony.i18n.getLocalizedString("keyMBEnterDevice");
	var invalidEmail1 = kony.i18n.getLocalizedString("invalidEmail");
	var invalidDivName = kony.i18n.getLocalizedString("invalidDeviceName");
	var info1 = kony.i18n.getLocalizedString("info");
	var okk = kony.i18n.getLocalizedString("keyOK");
	//gblActionCode = "21";
	//gActivationCode = "erT12345";
	//if(frmMBActiEmailDeviceName.tbxEmail.isVisible)
  	if(frmMBSetEmail.txtEmail.isVisible)
	{
		//var emailChk=frmMBActiEmailDeviceName.tbxEmail.text;
      	var emailChk=frmMBSetEmail.txtEmail.text;
		//var emailFlag = validateEmail(frmMBActiEmailDeviceName.tbxEmail.text);
      	var emailFlag = validateEmail(frmMBSetEmail.txtEmail.text);
		if (emailChk == null || emailChk == "")
		{
			//frmMBActiEmailDeviceName.tbxEmail.skin = txtErrorBG;
          	frmMBSetEmail.lblInvalidEmail.isVisible = true;
			//showAlert(blankEmail, info1);
			return false;
		}if (emailFlag == false) {
			//frmMBActiEmailDeviceName.tbxEmail.skin = txtErrorBG;
          	frmMBSetEmail.lblInvalidEmail.isVisible = true;
			//showAlert(invalidEmail1, info1);
			return false;
			//	alert("INVALID EMAIL ID");
		}
	}
	if (deviceChk == null || deviceChk == ""){
		frmMBActiEmailDeviceName.tbxDeviceName.skin = txtErrorBG;
		showAlert(blankDevicename, info1);
		return false;
	}if (devNameFlag == false) {
		frmMBActiEmailDeviceName.tbxDeviceName.skin = txtErrorBG;
		showAlert(invalidDivName, info1);
		return false;
		//	alert("INVALID DEVICE NAME");
	} else {
		GBL_FLOW_ID_CA = 1;
		//#ifdef iphone
			TrusteerDeviceId();
		//#else
			//#ifdef android
				getUniqueID();
			//#endif
		//#endif
	}
}

function onClickTnCConfirm(){

 
  if(gblActivationWithWifi != true)
  {
    frmActivationAttention.show();
  }
  else{
    
  
  
	gblRetryCountRequestOTP = "0";
	if(gblMBActivationVia == "2"){
		if(kony.i18n.getCurrentLocale() == "en_US"){
			setLocaleEng();
			frmMBActivationIBLogin.show();
		}else{
			setLocaleTH();	
			frmMBActivationIBLogin.show();
		}	
	}else if(gblMBActivationVia == "3"){
		
		atmCitizenIdVisibilityForConfirmation(false)
	}else{
		frmMBActivation.show();
	}
    
  }
}

//ENH_207_6 - Activate by using TMB ATM Authentication, modified to use E2EE(MIB-17 Jira Ticket)
function mbValidateAtmPin() {
	showLoadingScreen();
  		kony.print(" glbPin  --- > "+glbPin);
	var atmPin = decryptPinDigit(glbPin);
  	kony.print(" atmPin  --- > "+atmPin);
	encryptVerifyPinUsingE2EE(atmPin);
}

function mbValidateCVV() {
	showLoadingScreen();
	var creditCardCVV = decryptPinDigit(glbPin);
	validateCVV(creditCardCVV.substring(0, 3)); // temporary solution to make flow work
	//encryptVerifyPinUsingE2EE(creditCardCVV);  FFI function call to encrypt the CVV
}

function atmCitizenIdVisibilityForConfirmation(isConfirm){
	if(isConfirm){
      	
      	if(frmMBActiAtmIdMobile.tbxIdPassport.text!="" && frmMBActiAtmIdMobile.tbxIdPassport.isVisible)
		{
          frmMBActiAtmIdMobile.lblIdpasportConfirm.text = frmMBActiAtmIdMobile.tbxIdPassport.text;
        }        
        if(frmMBActiAtmIdMobile.tbxIdCitizen.text!="" && frmMBActiAtmIdMobile.tbxIdCitizen.isVisible)
        {
           frmMBActiAtmIdMobile.lblIdpasportConfirm.text = frmMBActiAtmIdMobile.tbxIdCitizen.text;
        }
      
		frmMBActiAtmIdMobile.lblInfo.text=kony.i18n.getLocalizedString("nextFollowInfoCrt");
		frmMBActiAtmIdMobile.tbxAtmNumber.setVisibility(false);
		frmMBActiAtmIdMobile.tbxIdPassport.setVisibility(false);
		frmMBActiAtmIdMobile.tbxIdCitizen.setVisibility(false);
      	frmMBActiAtmIdMobile.HBox0b02b9f46127d49.setVisibility(false);
      	frmMBActiAtmIdMobile.tbxMobileNo.setVisibility(false);
		frmMBActiAtmIdMobile.lineAtm.setVisibility(false);
		frmMBActiAtmIdMobile.lineIdPassport.setVisibility(false);
		frmMBActiAtmIdMobile.lineMobileNo.setVisibility(false);
		frmMBActiAtmIdMobile.lblIdpasportConfirm.setVisibility(true);
		frmMBActiAtmIdMobile.lblMobileNoConfirm.setVisibility(true);
		var cardNumber = frmMBActiAtmIdMobile.txtCreditCardNum.text;
		if(frmMBActiAtmIdMobile.hbxATM.skin == hbxATMcardfocus){
			cardNumber=frmMBActiAtmIdMobile.tbxAtmNumber.text;
			frmMBActiAtmIdMobile.lblAtmNumberConfirm.text = cardNumber;
			frmMBActiAtmIdMobile.lblAtmNumberConfirm.setVisibility(true);
			frmMBActiAtmIdMobile.lblCreditCardNumConfirm.setVisibility(false);
		}else{
			frmMBActiAtmIdMobile.lblCreditCardNumConfirm.text = cardNumber;
			frmMBActiAtmIdMobile.lblCreditCardNumConfirm.setVisibility(true);
			frmMBActiAtmIdMobile.lblAtmNumberConfirm.setVisibility(false);
		}	
		//cardNumber = formatCardNumber(cardNumber);
      
       	
		frmMBActiAtmIdMobile.lblMobileNoConfirm.text = maskingMB(frmMBActiAtmIdMobile.tbxMobileNo.text.replace(/\-/g, ""));
		frmMBActiAtmIdMobile.lblOption.setVisibility(false);
		frmMBActiAtmIdMobile.hbxChoiceDebitCredit.setVisibility(false);
		frmMBActiAtmIdMobile.hbxLightBlueTopArrow.setVisibility(false);
		frmMBActiAtmIdMobile.hbxCardNote.setVisibility(false);
		frmMBActiAtmIdMobile.txtCreditCardNum.setVisibility(false);
		frmMBActiAtmIdMobile.lineCreditCard.setVisibility(false);
	}else{
		frmMBActiAtmIdMobile.lblInfo.text=kony.i18n.getLocalizedString("keyInputInfolbl");
		frmMBActiAtmIdMobile.tbxAtmNumber.setVisibility(true);
		frmMBActiAtmIdMobile.tbxIdPassport.setVisibility(false);
		frmMBActiAtmIdMobile.tbxIdCitizen.setVisibility(true);
      	frmMBActiAtmIdMobile.HBox0b02b9f46127d49.setVisibility(true);
		frmMBActiAtmIdMobile.tbxMobileNo.setVisibility(true);
		frmMBActiAtmIdMobile.lineAtm.setVisibility(true);
		frmMBActiAtmIdMobile.lineIdPassport.setVisibility(true);
		frmMBActiAtmIdMobile.lineMobileNo.setVisibility(true);
		frmMBActiAtmIdMobile.lblAtmNumberConfirm.setVisibility(false);
		frmMBActiAtmIdMobile.lblIdpasportConfirm.setVisibility(false);
		frmMBActiAtmIdMobile.lblMobileNoConfirm.setVisibility(false);
		frmMBActiAtmIdMobile.lblAtmNumberConfirm.text = frmMBActiAtmIdMobile.tbxAtmNumber.text = "";
		frmMBActiAtmIdMobile.lblIdpasportConfirm.text = frmMBActiAtmIdMobile.tbxIdPassport.text = "" ;
		frmMBActiAtmIdMobile.lblMobileNoConfirm.text = frmMBActiAtmIdMobile.tbxMobileNo.text ="";
		frmMBActiAtmIdMobile.lblOption.setVisibility(true);
		frmMBActiAtmIdMobile.hbxChoiceDebitCredit.setVisibility(true);
		frmMBActiAtmIdMobile.hbxLightBlueTopArrow.setVisibility(true);
		frmMBActiAtmIdMobile.hbxCardNote.setVisibility(true);
		frmMBActiAtmIdMobile.txtCreditCardNum.setVisibility(true);
		frmMBActiAtmIdMobile.lineCreditCard.setVisibility(true);
		onClickATMCardImg();	
	}
	
	frmMBActiAtmIdMobile.show();
}

function validateAtmCardDetails(uniqueId){
	var inputParam = {};
  	var issueIdentVal="";
  	if(frmMBActiAtmIdMobile.tbxIdPassport.isVisible)
    {
      issueIdentVal=frmMBActiAtmIdMobile.tbxIdPassport.text.replace(/\-/g, "");
    }
  	else if(frmMBActiAtmIdMobile.tbxIdCitizen.isVisible)
    {
      issueIdentVal=frmMBActiAtmIdMobile.tbxIdCitizen.text.replace(/\-/g, "");
    }
	inputParam["issueIdentValue"] = issueIdentVal;
	inputParam["mobileNumber"] = frmMBActiAtmIdMobile.tbxMobileNo.text.replace(/\-/g, "");
	inputParam["deviceId"] = uniqueId;
	inputParam["captchaID"] = frmMBActiAtmIdMobile.txtCaptchaText.text;
	var cardType="Credit";
	var cardNumber = frmMBActiAtmIdMobile.txtCreditCardNum.text.replace(/\-/g, "");
	if(frmMBActiAtmIdMobile.hbxATM.skin == hbxATMcardfocus){
		cardType = "Debit";
		cardNumber = frmMBActiAtmIdMobile.tbxAtmNumber.text.replace(/\-/g, "");
	}
	inputParam["cardType"] = cardType;
	inputParam["cardNumber"] = encryptDPUsingE2EE(cardNumber);
    inputParam["mobileNumberFromUserDevice"] = mobileNumberFromUserDevice;
    inputParam["networkProvider"] = networkProvider;
	showLoadingScreen();
	invokeServiceSecureAsync("cardVerifyInq", inputParam, validateAtmCardDetailsCallBack);
}

gblATMPinLengthActivation = 4;
function validateAtmCardDetailsCallBack(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == 0) {
			dismissLoadingScreen();
	        if(frmMBActiAtmIdMobile.hbxATM.skin == hbxATMcardfocus){
	        	gblPk = resulttable["pubKey"];
		        gblRand = resulttable["serverRandom"];
		        gblAlgorithm = resulttable["hashAlgorithm"];
              	 kony.print("resulttable::::::"+resulttable)
               //  alert("Pingleght from the the server"+resulttable["pinLength"])
                 if("undefined" != typeof resulttable["pinLength"] ){
      				if("" != resulttable["pinLength"].trim() && 0 != resulttable["pinLength"]){
        			  gblATMPinLengthActivation = resulttable["pinLength"];
                      maxLenPin =  gblATMPinLengthActivation;
                       kony.print("maxLenPin::::::"+maxLenPin);
                      gblPinDigits = gblATMPinLengthActivation;
      			  }
                 }
	        	initializeAndNavigatetoVerifyPin();
	        }else{
	        	gblDPPk=resulttable["pk"];
                gblDPRandNumber=resulttable["randomNumber"];
	        	if(resulttable["ccVerifyActivateFlag"]=="yes"){
	        		//initializeAndNavigatetoVerifyCVV();
                    init_functionCVVDetailActivation();
                    
	        	}	
	        }	
		}else if(resulttable["opstatus"] == -1) {
			dismissLoadingScreen();
			//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			gblDPPk="";
			gblDPRandNumber="";
          
            if (resulttable["errCode"] == "UV_msgMobileNotMatched") {
					kony.application.dismissLoadingScreen();
					showAlertMobileNotMatched(kony.i18n.getLocalizedString("UV_msgMobileNotMatched"), kony.i18n.getLocalizedString("UV_msgMobileNotMatchedTitle"));
					return false;
			}else{
				frmMBanking.show();
          }
          
          
		} else {
			dismissLoadingScreen();
			if(resulttable["opstatus"] == "1"){
				if(resulttable["errMsg"]=="invalidVerifyCASA"){
					showAlert(kony.i18n.getLocalizedString("MB_ATErr_NotEligible"), kony.i18n.getLocalizedString("info"));	
				}   else {
					alert(kony.i18n.getLocalizedString("ECMIBInqErr00003"));
					frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
				}	
			}
			if(resulttable["opstatus"] == "2"){//CUST TYPE NOT ALLOWED
				if(resulttable["XPServerStatCode"] == "TS8884"){
					showAlert(kony.i18n.getLocalizedString("keyInvalidCus"), kony.i18n.getLocalizedString("info"));
				}else if(resulttable["XPServerStatCode"] == "TS8886"){
					showAlert(kony.i18n.getLocalizedString("MB_ATErr_CardStatus"), kony.i18n.getLocalizedString("info"));
				}else if(resulttable["XPServerStatCode"] == "TS8887"){
					showAlert(kony.i18n.getLocalizedString("MB_ATErr_CardStatus"), kony.i18n.getLocalizedString("info"));
				}else{ 
					showAlert(kony.i18n.getLocalizedString("invalidAccNo"), kony.i18n.getLocalizedString("info"));
					frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
				}
			}
			if(resulttable["opstatus"] == "0" && resulttable["StatusCode"] != "0"){
				showAlert(kony.i18n.getLocalizedString("invalidAccNo"), kony.i18n.getLocalizedString("info"));
			}			
			if(resulttable["captchaCode"]!= undefined && resulttable["captchaCode"] == "1"){
				if(resulttable["opstatus"] == 8005){
					//handles the case where pre processor captcha validation failed
					alert(kony.i18n.getLocalizedString("msgIBCaptchaWrong"));
				}	
				frmMBActiAtmIdMobile.hboxCaptcha.isVisible=true;
				frmMBActiAtmIdMobile.hboxCaptchaText.isVisible=true;
				frmMBActiAtmIdMobile.imgcaptcha.base64 = resulttable["captchaImage"];
				frmMBActiAtmIdMobile.txtCaptchaText.text="";
			}else{
				if(resulttable["opstatus"] == "8005" && resulttable["StatusCode"] == undefined){					
					showAlert(kony.i18n.getLocalizedString("invalidAccNo"), kony.i18n.getLocalizedString("info"));	            
				}
				frmMBActiAtmIdMobile.hboxCaptcha.isVisible=false;
				frmMBActiAtmIdMobile.hboxCaptchaText.isVisible=false;
				frmMBActiAtmIdMobile.txtCaptchaText.text="";
			}			
			setGblvalidateAtmCardDetails(resulttable);
		}
	}
}
function setGblvalidateAtmCardDetails(resulttable){
	gblDPPk=resulttable["pk"];
    gblDPRandNumber=resulttable["randomNumber"];
}

function validateTextfieldsAtmDetails(){
	var idpassport = frmMBActiAtmIdMobile.tbxIdPassport.text;
  	var idcitizen  = frmMBActiAtmIdMobile.tbxIdCitizen.text;
	var cardNumber = frmMBActiAtmIdMobile.txtCreditCardNum.text;
	var mobileNumber = frmMBActiAtmIdMobile.tbxMobileNo.text;
	var captchaText	= frmMBActiAtmIdMobile.txtCaptchaText.text;
	var info1 = kony.i18n.getLocalizedString("info");
	var isDebitCard = false;
	if(frmMBActiAtmIdMobile.hbxATM.skin == hbxATMcardfocus){
		isDebitCard = true;
		cardNumber = frmMBActiAtmIdMobile.tbxAtmNumber.text;
	}
  	if(frmMBActiAtmIdMobile.tbxIdPassport.isVisible)
    {
       if(!isNotBlank(idpassport)){
		showAlert(kony.i18n.getLocalizedString("keyMBPleaseEnterIDorPassportNo"), info1);
		frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
		return false;
		}
    }
	if(frmMBActiAtmIdMobile.tbxIdCitizen.isVisible)
    {
      if(!isNotBlank(idcitizen)){
		showAlert(kony.i18n.getLocalizedString("keyMBPleaseEnterIDorPassportNo"), info1);
		frmMBActiAtmIdMobile.tbxIdCitizen.setFocus(true);
		return false;
		}
    }
	if(!isNotBlank(mobileNumber)){
		showAlert(kony.i18n.getLocalizedString("plzEnterRegMobileNum"), info1);
		frmMBActiAtmIdMobile.tbxMobileNo.setFocus(true);
		return false;
	}
	if(idcitizen!=null||idcitizen!=""||idcitizen!=undefined||idcitizen!="undefined"){
    	idcitizen = idcitizen.replace(/\-/g, "");  
    }
  	if(idpassport!=null||idpassport!=""||idpassport!=undefined||idpassport!="undefined"){
    	idpassport = idpassport.replace(/\-/g, "");
    }
	
	mobileNumber = mobileNumber.replace(/\-/g, "");
	
	var citizezIDFlag = checkCitizenID(idcitizen);
	var passFlag = PasprtValidatn(idpassport);
	
	var isCitizen = false;
	var isPassport = false;
	if (frmMBActiAtmIdMobile.tbxIdCitizen.isVisible) {
		isCitizen = true;
		isPassport = false;
	} else if(frmMBActiAtmIdMobile.tbxIdPassport.isVisible) {
		isCitizen = false;
		isPassport = true;
	}
  	if (frmMBActiAtmIdMobile.tbxIdCitizen.isVisible) {
      if ((isCitizen && !citizezIDFlag))  {
          showAlert(kony.i18n.getLocalizedString("MB_ATErr_InvalidInfo"), info1);
          frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
          return false;
      }
    }else if(frmMBActiAtmIdMobile.tbxIdPassport.isVisible) {
		if((isPassport && !passFlag)) {
          showAlert(kony.i18n.getLocalizedString("MB_ATErr_InvalidInfo"), info1);
          frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
          return false;
      }	
    }
    mobileNumber = mobileNumber.toString().replace(/-/g, "");
    var res = mobileNumber.substring(0, 2);
    var isNum = kony.string.isNumeric(mobileNumber);
    if (Gbl_StartDigsMobileNum.indexOf(res) < 0) {
        var number1 = kony.i18n.getLocalizedString("keyEnterMobileNumInvalid")+" "+Gbl_StartDigsMobileNum;
        showAlert(number1, "Info");
		frmMBActiAtmIdMobile.tbxMobileNo.setFocus(true);
        return false;
    } else if (!isNum || mobileNumber.length < 10 || mobileNumber.length > 10) {
        showAlert(kony.i18n.getLocalizedString("keyIncorrectMobileNo"), "Info");
		frmMBActiAtmIdMobile.tbxMobileNo.setFocus(true);
        return false;
    }
    gblPHONENUMBER = mobileNumber;
	if(!isNotBlank(cardNumber)){
		if(isDebitCard){
			showAlert(kony.i18n.getLocalizedString("plzEnterAtmNumber"), info1);
			frmMBActiAtmIdMobile.tbxAtmNumber.setFocus(true);
		}else{
			showAlert(kony.i18n.getLocalizedString("MB_ATErr_CardNo"), info1);
			frmMBActiAtmIdMobile.txtCreditCardNum.setFocus(true);
		}	
		return false;
	}
	
	cardNumber = cardNumber.replace(/\-/g, "");
	if(cardNumber.length != 16){
		if(isDebitCard){
			showAlert(kony.i18n.getLocalizedString("plzEnterAtmNumber"), info1);
			frmMBActiAtmIdMobile.tbxAtmNumber.setFocus(true);
		}else{
			showAlert(kony.i18n.getLocalizedString("MB_ATErr_CardNo"), info1);
			frmMBActiAtmIdMobile.txtCreditCardNum.setFocus(true);
		}
		return false;
	}
	if(isDebitCard){
		if(cardNumber.charAt(0) != '0' && cardNumber.charAt(0) != '4' && cardNumber.charAt(0) != '5'){
			showAlert(kony.i18n.getLocalizedString("ECMIBInqErr00003"), info1);
			frmMBActiAtmIdMobile.tbxAtmNumber.setFocus(true);
			return false;
		}
	}else{
		if(cardNumber.charAt(0) != '4' && cardNumber.charAt(0) != '5'){
			showAlert(kony.i18n.getLocalizedString("ECMIBInqErr00003"), info1);
			frmMBActiAtmIdMobile.txtCreditCardNum.setFocus(true);
			return false;
		}
	}
    if(frmMBActiAtmIdMobile.hboxCaptcha.isVisible){
    	if(!isNotBlank(captchaText)){
    		alert(kony.i18n.getLocalizedString("keyCaptchaRequired"));
    		return false;
    	}
    	var pat1 = /^[a-z0-9]+$/;
		var isMatching = pat1.test(captchaText);
		if ((!isMatching) || (captchaText.length != 6)) {
			alert(kony.i18n.getLocalizedString("msgIBCaptchaWrong"));
			return false;
		}
    }
	return true;
	//onEditMobileNumberIB(txt)
}
//form changes for the ATMBID Mobile
function onDoneValidateCIorPPInActivationUsingCard_Passport() {
	gblPrevLen=0;
	var temp = frmMBActiAtmIdMobile.tbxIdPassport.text;
	var numChars = temp.length;
	
	for(var i=0;i<numChars;i++){
		if(temp[i] == '-'){
			temp = temp.replace("-","");
		}
	}

	if(!PasprtValidatn(temp)) {
			showAlert(kony.i18n.getLocalizedString("MB_ATErr_InvalidInfo"), kony.i18n.getLocalizedString("info"));
			frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
			return false;
	}
	return true;
}

function onDoneValidateCIorPPInActivationUsingCard_CitiZen() {
	gblPrevLen=0;
	var temp = frmMBActiAtmIdMobile.tbxIdCitizen.text;
	var numChars = temp.length;
	
	for(var i=0;i<numChars;i++){
		if(temp[i] == '-'){
			temp = temp.replace("-","");
		}
	}

	if(!checkCitizenID(temp)) {
			showAlert(kony.i18n.getLocalizedString("MB_ATErr_InvalidInfo"), kony.i18n.getLocalizedString("info"));
			frmMBActiAtmIdMobile.tbxIdCitizen.setFocus(true);
			return false;
	}
	return true;
}

function onTextChangeOfCIOrPPInActivationUsingCard_Passport() {

	if(frmMBActiAtmIdMobile.tbxIdPassport.text.length==0){
		frmMBActiAtmIdMobile.tbxIdPassport.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		frmMBActiAtmIdMobile.tbxIdPassport.maxTextLength=25;
	}
	var temp = frmMBActiAtmIdMobile.tbxIdPassport.text.substring(0,1);
	frmMBActiAtmIdMobile.tbxIdPassport.skin = "txtNormalBG";
	
	//when ID no is used length is 13+4 hypens
	var len1=frmMBActiAtmIdMobile.tbxIdPassport.text;
	var lenthOfText=len1.length;
	var charToUp=len1.charAt(lenthOfText-1);
	if(kony.string.isAsciiAlpha(charToUp))
	{
		len1.toUpperCase();
		frmMBActiAtmIdMobile.tbxIdPassport.text=len1.toUpperCase();
	}
	
}

function onTextChangeOfCIOrPPInActivationUsingCard_Citizen() {

	if(frmMBActiAtmIdMobile.tbxIdCitizen.text.length==0){
		frmMBActiAtmIdMobile.tbxIdCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
		frmMBActiAtmIdMobile.tbxIdCitizen.maxTextLength=17;
	}
	var temp = frmMBActiAtmIdMobile.tbxIdCitizen.text.substring(0,1);
	frmMBActiAtmIdMobile.tbxIdCitizen.skin = "txtNormalBG";
	
	//when ID no is used length is 13+4 hypens
	frmMBActiAtmIdMobile.tbxIdCitizen.maxTextLength=17;
	frmMBActiAtmIdMobile.tbxIdCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
	onEditCitiZenID(frmMBActiAtmIdMobile.tbxIdCitizen.text);
	
}

function onBeginEditOfCIorPPInActivationUsingCard_Citizen() {
	gblTxtFocusFlag = 0;
	frmMBActiAtmIdMobile.tbxIdCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
}

function onBeginEditOfCIorPPInActivationUsingCard_Passport() {
	gblTxtFocusFlag = 0;
	frmMBActiAtmIdMobile.tbxIdPassport.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
}

function onClickCitizenIDButton()
{
    frmMBActiAtmIdMobile.btnCitizenIDsel.skin="btnBlack20pxBold";
  	frmMBActiAtmIdMobile.lineCitizen.skin="lineBlack";
    frmMBActiAtmIdMobile.lineCitizen.thickness=4;
    frmMBActiAtmIdMobile.btnPassportID.skin="btnBlue160";
  	frmMBActiAtmIdMobile.linePassport.skin="lineBlue";
  	frmMBActiAtmIdMobile.linePassport.thickness=1;
  	frmMBActiAtmIdMobile.tbxIdPassport.setVisibility(false);
  	frmMBActiAtmIdMobile.tbxIdCitizen.setVisibility(true);
  	frmMBActiAtmIdMobile.tbxIdCitizen.text="";
  	frmMBActiAtmIdMobile.tbxIdCitizen.setFocus(true);
}

function onClickPassportIDButton()
{
	frmMBActiAtmIdMobile.btnCitizenIDsel.skin="btnBlue160";
  	frmMBActiAtmIdMobile.lineCitizen.skin="lineBlue";
    frmMBActiAtmIdMobile.lineCitizen.thickness=1;
    frmMBActiAtmIdMobile.btnPassportID.skin="btnBlack20pxBold";
  	frmMBActiAtmIdMobile.linePassport.skin="lineBlack";
  	frmMBActiAtmIdMobile.linePassport.thickness=4;
  	frmMBActiAtmIdMobile.tbxIdPassport.setVisibility(true);
  	frmMBActiAtmIdMobile.tbxIdCitizen.setVisibility(false);
  	frmMBActiAtmIdMobile.tbxIdPassport.text="";
  	frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
}

//
function onDoneValidateCIorPPInActivationUsingCard() {
	gblPrevLen=0;

	var temp = frmMBActiAtmIdMobile.tbxIdPassport.text;
	var numChars = temp.length;
	
	for(var i=0;i<numChars;i++){
		if(temp[i] == '-'){
			temp = temp.replace("-","");
		}
	}

	if(kony.string.isNumeric(temp)) {
		if(!checkCitizenID(temp)) {
			showAlert(kony.i18n.getLocalizedString("MB_ATErr_InvalidInfo"), kony.i18n.getLocalizedString("info"));
			frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
			return false;
		}
	} else {
		if(!PasprtValidatn(temp)) {
			showAlert(kony.i18n.getLocalizedString("MB_ATErr_InvalidInfo"), kony.i18n.getLocalizedString("info"));
			frmMBActiAtmIdMobile.tbxIdPassport.setFocus(true);
			return false;
		}
	}
	
	return true;
}

function onDoneValidateCIorPPInActivationUsingActiCode() {
		
	gblPrevLen=0;
	
	var temp = frmMBActivation.txtIDPass.text;
	var numChars = temp.length;
	
	for(var i=0;i<numChars;i++){
		if(temp[i] == '-'){
			temp = temp.replace("-","");
		}
	}

	if(kony.string.isNumeric(temp)) {
		if(!checkCitizenID(temp)) {
			showAlert(kony.i18n.getLocalizedString("invalidActicodes"), kony.i18n.getLocalizedString("info"));
			frmMBActivation.txtIDPass.setFocus(true);
			return false;
		}
	} else {
		if(!PasprtValidatn(temp)) {
			showAlert(kony.i18n.getLocalizedString("invalidActicodes"), kony.i18n.getLocalizedString("info"));
			frmMBActivation.txtIDPass.setFocus(true);
			return false;
		}
	}
	
	return true;
}
//Adding new code for citizenid and Passfor fields
function onClickCitizenIDButton_NormalActivation()
{
    frmMBActivation.btnCitizenIDsel.skin="btnBlack20pxBold";
  	frmMBActivation.lineCitizen.skin="lineBlack";
    frmMBActivation.lineCitizen.thickness=4;
    frmMBActivation.btnPassportID.skin="btnBlue160";
  	frmMBActivation.linePassport.skin="lineBlue";
  	frmMBActivation.linePassport.thickness=1;
  	frmMBActivation.txtIDPass.setVisibility(false);
  	frmMBActivation.txtIDCitizen.setVisibility(true);
  	frmMBActivation.txtIDCitizen.text="";
  	frmMBActivation.txtIDCitizen.setFocus(true);
}

function onClickPassportIDButton_NormalActivation()
{
	frmMBActivation.btnCitizenIDsel.skin="btnBlue160";
  	frmMBActivation.lineCitizen.skin="lineBlue";
    frmMBActivation.lineCitizen.thickness=1;
    frmMBActivation.btnPassportID.skin="btnBlack20pxBold";
  	frmMBActivation.linePassport.skin="lineBlack";
  	frmMBActivation.linePassport.thickness=4;
  	frmMBActivation.txtIDPass.setVisibility(true);
  	frmMBActivation.txtIDCitizen.setVisibility(false);
  	frmMBActivation.txtIDPass.text="";
  	frmMBActivation.txtIDPass.setFocus(true);
}

function onTextChangeOfCIOrPPInActivationUsingActiCode_passport() {

	if(frmMBActivation.txtIDPass.text.length==0){
		frmMBActivation.txtIDPass.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		frmMBActivation.txtIDPass.maxTextLength=25;
	}
	var temp = frmMBActivation.txtIDPass.text.substring(0,1);
	frmMBActivation.txtIDPass.skin = "txtNormalBG";
	//frmMBActivation.txtIDPass.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
	//Done by KH1197
	var len1=frmMBActivation.txtIDPass.text;
	var lenthOfText=len1.length;
	var charToUp=len1.charAt(lenthOfText-1);
	if(kony.string.isAsciiAlpha(charToUp))
	{
		len1.toUpperCase();
		frmMBActivation.txtIDPass.text=len1.toUpperCase();
	}
}

function onBeginEditOfCIorPPInActivationUsingActiCode_passport() {
	if(isMenuShown){
		frmMBActivation.scrollboxMain.scrollToEnd();
		isMenuShown = false;
	}
	frmMBActivation.txtIDPass.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
}

function onBeginEditOfCIorPPInActivationUsingActiCode_citizen() {
	if(isMenuShown){
		frmMBActivation.scrollboxMain.scrollToEnd();
		isMenuShown = false;
	}
	frmMBActivation.txtIDCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
}

function onTextChangeOfCIOrPPInActivationUsingActiCode_citizen() {

	if(frmMBActivation.txtIDCitizen.text.length==0){
		frmMBActivation.txtIDCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
		frmMBActivation.txtIDCitizen.maxTextLength=17;
	}
	var temp = frmMBActivation.txtIDCitizen.text.substring(0,1);
	frmMBActivation.txtIDCitizen.skin = "txtNormalBG";
	
	if(kony.string.isNumeric(temp)) {
		//when ID no is used length is 13+4 hypens
		frmMBActivation.txtIDCitizen.maxTextLength=17;
		frmMBActivation.txtIDCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
		onEditCitiZenID(frmMBActivation.txtIDCitizen.text);
    }
}

function onDoneValidateCIorPPInActivationUsingActiCode_passport() {
		
	gblPrevLen=0;
	
	var temp = frmMBActivation.txtIDPass.text;
	var numChars = temp.length;
	
	for(var i=0;i<numChars;i++){
		if(temp[i] == '-'){
			temp = temp.replace("-","");
		}
	}
	
  	if(!PasprtValidatn(temp)) {
			showAlert(kony.i18n.getLocalizedString("invalidActicodes"), kony.i18n.getLocalizedString("info"));
			frmMBActivation.txtIDPass.setFocus(true);
			return false;
	}
	
  	return true;
}

function onDoneValidateCIorPPInActivationUsingActiCode_citizen() {
		
	gblPrevLen=0;
	
	var temp = frmMBActivation.txtIDCitizen.text;
	var numChars = temp.length;
	
	for(var i=0;i<numChars;i++){
		if(temp[i] == '-'){
			temp = temp.replace("-","");
		}
	}

	if(kony.string.isNumeric(temp)) {
		if(!checkCitizenID(temp)) {
			showAlert(kony.i18n.getLocalizedString("invalidActicodes"), kony.i18n.getLocalizedString("info"));
			frmMBActivation.txtIDCitizen.setFocus(true);
			return false;
		}
    }
	
	return true;
}


function verifyATMCardForActivation(){
	showLoadingScreen();
	if(frmMBActiAtmIdMobile.lblAtmNumberConfirm.isVisible || frmMBActiAtmIdMobile.lblCreditCardNumConfirm.isVisible){
		onClickActiConfirm();
	}else{
			if(!validateTextfieldsAtmDetails()){
				dismissLoadingScreen();
				return false;
			}
//Added for ENH_207_6 to send uniqueid in ATM verify details for MB Activation - function validateAtmCardDetails()
		
			GBL_FLOW_ID_CA=9;
			//#ifdef iphone
				TrusteerDeviceId();
			//#else
				//#ifdef android
					getUniqueID();
				//#endif
			//#endif 
	}
}

function validateAtmPin(atmPinEncrypted){
	var inputParam = {};
	inputParam["rPin"]=atmPinEncrypted;
	invokeServiceSecureAsync("verifyPin", inputParam, validateAtmPinCallBack);
}

function validateAtmPinCallBack(status, resulttable) {
	if (status == 400) {
		dismissLoadingScreen();
		if (resulttable["opstatus"] == 0) {
			//PIN Verify Success
			gblPk="";
    		gblRand="";
    		gblAlgorithm="";
			atmCitizenIdVisibilityForConfirmation(true);
		} else if(resulttable["opstatus"] == "-1") {
			//showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			frmMBanking.show();
		} else {
			//clearVerifyPinMB();  // method to clear the virtual keyboard
            btnPinClearActivation();
			if(resulttable["errCode"] == "B243053") {
	 			alert(kony.i18n.getLocalizedString("keyInvalidAtmPin"));
	 		} else if(resulttable["errCode"] == "B243062") {
	 			alert(kony.i18n.getLocalizedString("keyAtmPinLocked"));
	 			frmMBanking.show();
	 		} else {
				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
	 		}
	 		gblPk = resulttable["pubKey"];
	       	gblRand = resulttable["serverRandom"];
	        gblAlgorithm = resulttable["hashAlgorithm"];	
			return false;
		}
	}
}


function formatCardNumber(cardNumber){
//	if(cardNumber == "" || cardNumber == undefined || cardNumber == null){
	//	frmMBActiAtmIdMobile.tbxAtmNumber.text = cardNumber;
//	}else{
		cardNumber = cardNumber.replace(/\-/g, "");
		if(cardNumber.length == 16){
			cardNumber = cardNumber.substring(0, 4) + "-" + cardNumber.substring(4, 8) + "-" + cardNumber.substring(8, 12) + "-" + cardNumber.substring(12, 16);
			if(frmMBActiAtmIdMobile.hbxATM.skin == hbxATMcardfocus)
				frmMBActiAtmIdMobile.tbxAtmNumber.text = cardNumber;
			else
				frmMBActiAtmIdMobile.txtCreditCardNum.text = cardNumber;
			
		}
}

function clearAccessPin() {
	frmMBSetAccPinTxnPwd.txtAccessPin.text="";
 	accessPinRender("");
 	frmMBSetAccPinTxnPwd.txtAccessPin.setFocus(false);
}

function onClickOfStartBtnForMBSnippet(){
  gblTCEmailTriggerFlag = true;
  //gblDeviceInfo is assigning deviceInfo at app preApp init. no need assign again.
  //gblDeviceInfo = kony.os.deviceInfo();
  gblSpaTokenServFalg = true;

  if(gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"]=="spa"){
    if(isMenuShown == false){
      //createNoOfRowsDynamically();
      //addButton2();
      gblIndex = -1
      isMenuRendered = false;
      isMenuShown = false;
      gFromConnectAccount=true
      //showAccountSummLndnd();
      //showAccuntSummaryScreen();
      SpaLoginServiceonComplete();
      //frmAccountSummaryLanding.show();
      //TMBUtil.DestroyForm(frmAfterLogoutMB);
    }else {
      frmMBActiComplete.scrollboxMain.scrollToEnd();
    }
  } else {
    if(isMenuShown == false){
      onClickOfStartBtnForMB();
    }else {
      frmMBActiComplete.scrollboxMain.scrollToEnd();
    }
  }
}

function onClickOfStartBtnForMBSnippetForCmp(){
	isCmpFlowFrmActivation = true;
	gblTCEmailTriggerFlag = true;
	//gblDeviceInfo is assigning deviceInfo at app preApp init. no need assign again.
	//gblDeviceInfo = kony.os.deviceInfo();
	gblSpaTokenServFalg = true;

	if(gblDeviceInfo["name"] == "thinclient" & gblDeviceInfo["type"]=="spa"){
		if(isMenuShown == false){
			//createNoOfRowsDynamically();
			//addButton2();
			gblIndex = -1
			isMenuRendered = false;
			isMenuShown = false;
			gFromConnectAccount=true
			//showAccountSummLndnd();
			//showAccuntSummaryScreen();
			SpaLoginServiceonComplete();
			//frmAccountSummaryLanding.show();
			//TMBUtil.DestroyForm(frmAfterLogoutMB);
		}else {
			frmMBActiComplete.scrollboxMain.scrollToEnd();
		}
	} else {
		if(isMenuShown == false){
			onClickOfStartBtnForMB();
		}else {
			frmMBActiComplete.scrollboxMain.scrollToEnd();
		}
	}
}

function onClickOfStartBtnForMB(){
	gblIndex = -1
	isMenuShown = false;
	gFromConnectAccount=true
	gblTouchStatus = "N";
	gblSuccess  = false; 
    kony.store.removeItem("cachedResulttableASLoginFlow");
	kony.store.setItem("usesTouchId", "N");
  	kony.store.setItem("SaveAlertIntro", "False");
	if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "android"){
	  onClickCheckTouchIdAvailableUsingKonyAPI();
	  gblActivationFlow = true;
	}
	gblNum=glbAccessPin;
	accsPwdValidatnLogin(glbAccessPin);
	
	if(flowSpa){
	}
	else{
		//gblStartClickFromActivationMB=true;
	}
}
function validateLength(text,min,max){
	if ((text == null) || (text == "")||text.trim().length=="") return false;
	var txtLen = text.length;
	if( txtLen <=max && txtLen>=min){
		return true;
	}else{
		return false;
	}
}

function onTextChangeOfCIOrPPInActivationUsingCard() {

	if(frmMBActiAtmIdMobile.tbxIdPassport.text.length==0){
		frmMBActiAtmIdMobile.tbxIdPassport.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		frmMBActiAtmIdMobile.tbxIdPassport.maxTextLength=17;
	}
	var temp = frmMBActiAtmIdMobile.tbxIdPassport.text.substring(0,1);
	frmMBActiAtmIdMobile.tbxIdPassport.skin = "txtNormalBG";
	
	if(kony.string.isNumeric(temp)) {
		//when ID no is used length is 13+4 hypens
		frmMBActiAtmIdMobile.tbxIdPassport.maxTextLength=17;
		frmMBActiAtmIdMobile.tbxIdPassport.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		onEditCitiZenID(frmMBActiAtmIdMobile.tbxIdPassport.text);
	} else {
		//frmMBActiAtmIdMobile.tbxIdPassport.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		//Done by KH1197
		var len1=frmMBActiAtmIdMobile.tbxIdPassport.text;
		var lenthOfText=len1.length;
		var charToUp=len1.charAt(lenthOfText-1);
		if(kony.string.isAsciiAlpha(charToUp))
		{
			len1.toUpperCase();
			frmMBActiAtmIdMobile.tbxIdPassport.text=len1.toUpperCase();
		}
	}
}

function onBeginEditOfCIorPPInActivationUsingCard() {
	gblTxtFocusFlag = 0;
	frmMBActiAtmIdMobile.tbxIdPassport.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
  	frmMBActiAtmIdMobile.tbxIdCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
}



function onTextChangeOfCIOrPPInActivationUsingActiCode() {

	if(frmMBActivation.txtIDPass.text.length==0){
		frmMBActivation.txtIDPass.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		frmMBActivation.txtIDPass.maxTextLength=17;
	}
	var temp = frmMBActivation.txtIDPass.text.substring(0,1);
	frmMBActivation.txtIDPass.skin = "txtNormalBG";
	
	if(kony.string.isNumeric(temp)) {
		//when ID no is used length is 13+4 hypens
		frmMBActivation.txtIDPass.maxTextLength=17;
		frmMBActivation.txtIDPass.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		onEditCitiZenID(frmMBActivation.txtIDPass.text);
	} else {
		//frmMBActivation.txtIDPass.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
		//Done by KH1197
		var len1=frmMBActivation.txtIDPass.text;
		var lenthOfText=len1.length;
		var charToUp=len1.charAt(lenthOfText-1);
		if(kony.string.isAsciiAlpha(charToUp))
		{
			len1.toUpperCase();
			frmMBActivation.txtIDPass.text=len1.toUpperCase();
		}
	}
}

function onBeginEditOfCIorPPInActivationUsingActiCode() {
	if(isMenuShown){
		frmMBActivation.scrollboxMain.scrollToEnd();
		isMenuShown = false;
	}
	frmMBActivation.txtIDPass.textInputMode=constants.TEXTBOX_INPUT_MODE_ANY;
  	frmMBActivation.txtIDCitizen.textInputMode=constants.TEXTBOX_INPUT_MODE_NUMERIC;
}

function validateEmail(email) {
    var allowChar = new RegExp("^[A-Za-z0-9#@._-]+$","g");
    var emailPattern = "^[\\w#-]+(?:\\.[\\w#-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
    var emailRegExp = new RegExp(emailPattern, "g");
    if (GBL_EMAIL_REG_PATTERN != undefined && GBL_EMAIL_REG_PATTERN != "" && GBL_EMAIL_REG_PATTERN != null) {
        emailRegExp = new RegExp(GBL_EMAIL_REG_PATTERN, "g");
    }

    if (allowChar.test(email) && emailRegExp.test(email)) {
        return true;
    }
    return false;
}

function verifyRefData(eventobject){
    var curData = ""+eventobject.text;
    kony.print("Bill Payment Ref1 & Ref2 Input Data"+curData+"gblITMXFlag"+gblITMXFlag);
    if (gblITMXFlag == "N" && curData.length > 0) { 
         var isValid = false;
         var regex = /^[0-9a-zA-Z\s]*$/;
         isValid = regex.test(curData);
         if(isValid){
          var data = curData.toUpperCase();
          eventobject.text = data
         }else{
          var data = curData.replace(/[^0-9a-zA-Z\s]+/ig, '');
          eventobject.text = data;
        }
   }else{
     data = curData;
     eventobject.text = curData;
   }
    kony.print("Bill Payment Ref1 & Ref2 Modified Data"+data);
}

function verifyRefDataMB(curData){
    kony.print("Bill Payment Ref1 & Ref2 Input Data"+curData+"gblITMXFlag"+gblITMXFlag);
    if (gblITMXFlag == "N") { 
         var isValid = false;
         var regex = /^[0-9a-zA-Z\s]*$/;
         isValid = regex.test(curData);
         if(isValid){
          var data = curData.toUpperCase();
          return data;
         }else{
            var res = curData.charAt(curData.length-1);
            var regex = /^[0-9a-zA-Z\s]*$/;
             isValid = regex.test(res);
            if(isValid){
                data = curData;
            }else{
                var data = curData.replace(/[^0-9a-zA-Z\s]+/ig, '');
            }
            return data;
         }
   }else{
     return curData;
   }
}

function validateRefValueAll(text) {
  var isValid = false;
	if (text != null && text!= "") {
		var mypattern = /^[0-9a-zA-Z\s]*$/;
		isValid = mypattern.test(text);
	} else {
		isValid = false;
	}
  kony.print("isValid"+isValid);
	return isValid;
}

function onTextChangeRef1Ref2BillpaymentIB(){
		var ref1ValBillPay = frmIBMyBillersHome.txtAddBillerRef1.text;
		if(gblITMXFlag == "N" && ref1ValBillPay.length >0 && !validateRefValueAll(ref1ValBillPay)){
			var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef1Val");
			wrongRefMsg = wrongRefMsg.replace("{ref_label}",  removeColonFromEnd(frmIBMyBillersHome.txtAddBillerRef1.text+""));	
			dismissLoadingScreen();						
			alert(kony.i18n.getLocalizedString("keyBilerValidationFail"));
          	frmIBMyBillersHome.txtAddBillerRef1.text = "";
			frmIBMyBillersHome.txtAddBillerRef1.setFocus(true);
			return;
		}else if(gblITMXFlag == "N" && ref1ValBillPay.length >0){
            frmIBMyBillersHome.txtAddBillerRef1.text = ref1ValBillPay.toUpperCase();
		}	
		var ref2ValBillPay = frmIBMyBillersHome.txtAddBillerRef2.text;
		if(gblITMXFlag == "N" && ref2ValBillPay.length >0 && !validateRefValueAll(ref2ValBillPay)){
			var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef1Val");
			wrongRefMsg = wrongRefMsg.replace("{ref_label}",  removeColonFromEnd(frmIBMyBillersHome.txtAddBillerRef2.text+""));	
			dismissLoadingScreen();						
			alert(kony.i18n.getLocalizedString("keyBilerValidationFail"));
            frmIBMyBillersHome.txtAddBillerRef2.text = "";
			frmIBMyBillersHome.txtAddBillerRef2.setFocus(true);
			return;
		}
        else if(gblITMXFlag == "N" && ref2ValBillPay.length >0){
              frmIBMyBillersHome.txtAddBillerRef2.text = ref2ValBillPay.toUpperCase();
          }
}

function addBillersBillpayment() {
	
    frmIBMyBillersHome.txtAddBillerRef1.onKeyUp = verifyRefData;
    frmIBMyBillersHome.txtAddBillerRef2.onKeyUp = verifyRefData;
	
	frmIBMyBillersHome.txtAddBillerRef1.onTextChange = onTextChangeRef1Ref2BillpaymentIB;
    frmIBMyBillersHome.txtAddBillerRef2.onTextChange = onTextChangeRef1Ref2BillpaymentIB;
}

function retryInActivationNetworkCheck(){
  
   var title = "";
    var message = kony.i18n.getLocalizedString("CKM_msgNoService");
    var yeslbl = kony.i18n.getLocalizedString("Btn_Retry");
    var nolbl = kony.i18n.getLocalizedString("WIFI_btnCancel");
    var basicConf = {
      message: message,
      alertType: constants.ALERT_TYPE_CONFIRMATION,
      alertTitle: title,
      yesLabel: yeslbl,
      noLabel: nolbl,
      alertHandler: callBackNetworkcheck
    };

    var pspConf = {};
    var infoAlert = kony.ui.Alert(basicConf, pspConf);

    return false;
}

function retryInActivationTelcoMobileNumber(){
  
   var title = "";
    var message = kony.i18n.getLocalizedString("CKM_msgNoMobile");
    var yeslbl = kony.i18n.getLocalizedString("Btn_Retry");
    var nolbl = kony.i18n.getLocalizedString("WIFI_btnCancel");
    var basicConf = {
      message: message,
      alertType: constants.ALERT_TYPE_CONFIRMATION,
      alertTitle: title,
      yesLabel: yeslbl,
      noLabel: nolbl,
      alertHandler: callBackNetworkcheck
    };

    var pspConf = {};
    var infoAlert = kony.ui.Alert(basicConf, pspConf);

    return false;
}

function mobilenumberCehckDelay(){
	kony.print("before the timer")
	mobileNumberCheckTimerId = "MobileNumberCehck"+parseInt((Math.random()*1000000)+"") + "";
	
	kony.print("before the timer mobileNumberCheckTimerId : "+mobileNumberCheckTimerId);
	kony.timer.schedule(mobileNumberCheckTimerId,timerCallBackMobileNumberCheck,2 , false);
}


function timerCallBackMobileNumberCheck(){
  
  kony.print("Printing the current time in call back : "+kony.os.time());
  dismissLoadingScreen();
  try{
     kony.timer.cancel(mobileNumberCheckTimerId);
  }catch(timerError){
    	kony.print("Error Occuered while canceling the time "+mobileNumberCheckTimerId);
    	
  }
  mobileNumberCheckTimerId = "";
   var needPushNotification = false;
  var alertPushMessage = kony.i18n.getLocalizedString("msgPushNoti");
  if (isNotBlank(isSignedUser) && isSignedUser == true) {
    //for TMB confirm & cardless withdraw need to enable push notification
    needPushNotification = true;
    if (gblActivationCurrentForm == "TMBConfirm") {
      alertPushMessage = kony.i18n.getLocalizedString("msgPushNoti");
    }else if (gblActivationCurrentForm == "cardlessWithdraw") {
      alertPushMessage =  kony.i18n.getLocalizedString("msgPushNotiCardless");
    }
  }
  
 
  
  if(GLOBAL_UV_STATUS_FLAG == "ON"){
  	checkUserPushNotification(alertPushMessage, checkUserConnectivity, needPushNotification);
  }
    
}