function openCreditCardDeeplink(serviceType){
  showLoadingScreen();
  gblDeepLinkFlow = true;
  gblServiceType = serviceType;
  frmMBCreditCardListDeeplink.destroy();
  getCustomerCardListForDeeplink();
}
function frmMBCreditCardListDeeplink_init_Action(){
  frmMBCreditCardListDeeplink.preShow = frmMBCreditCardListDeeplink_preShow_Action;
  frmMBCreditCardListDeeplink.postShow = frmMBCreditCardListDeeplink_postShow_Action;
  frmMBCreditCardListDeeplink.btnMainMenu.onClick = handleMenuBtn;
  frmMBCreditCardListDeeplink.onDeviceBack = disableBackButton;
  frmMBCreditCardListDeeplink.btnCCOpenAC.onClick = openCreditCardLink;
  frmMBCreditCardListDeeplink.btnCCReadMore.onClick = moreDtailClicks;
}
function frmMBCreditCardListDeeplink_preShow_Action(){
  frmMBCreditCardListDeeplink.lblCardList.text = kony.i18n.getLocalizedString("MB_CCList_Title");
  frmMBCreditCardListDeeplink.lblHeaderNoCard.text = kony.i18n.getLocalizedString("MB_ErrNoEligibleCC");
  frmMBCreditCardListDeeplink.lblCreditCard1.text = kony.i18n.getLocalizedString("GetCreditCard");
  frmMBCreditCardListDeeplink.btnCCOpenAC.text = kony.i18n.getLocalizedString("MB_ApplyCC");
  frmMBCreditCardListDeeplink.lblCreditCard2.text = kony.i18n.getLocalizedString("CCA02_OrText");
  frmMBCreditCardListDeeplink.btnCCReadMore.text = kony.i18n.getLocalizedString("MB_SeeMoreDetails");
  frmMBCreditCardListDeeplink.imgBoxCard.src = getImageSRC("blankcard");
}
function frmMBCreditCardListDeeplink_postShow_Action(){
  dismissLoadingScreen();
}

function moreDtailClicks(){
  var url = "";
  showLoadingScreen();
  gblManageCardFlow = "frmMBCreditCardListDeeplink";
  url = getLinkCardURL("Credit Card");
  frmMBCardMoreDetail.browserCard.requestURLConfig = {
    URL: url,
    requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
  }
  frmMBCardMoreDetail.buttonBack.text = kony.i18n.getLocalizedString("Back");
  frmMBCardMoreDetail.lblCardTitle.text = kony.i18n.getLocalizedString("MB_CCList_Title");
  frmMBCardMoreDetail.browserCard.onSuccess = dismissLoadingScreen;
  frmMBCardMoreDetail.show(); 

}

function openCreditCardLink(){
  gblDeepLinkFlow = true;
  showLoadingScreen();
  kony.modules.loadFunctionalModule("loanModule");
  onClickCreditCards();  
}

function getCustomerCardListForDeeplink() {
  gblManageCardFlow = "frmMBCreditCardListDeeplink";
  frmMBCreditCardListDeeplink.FlexMain.removeAll();
  showLoadingScreen();
  inputparam = {};
  inputparam["cardFlag"] = "3";
  var labelTextHeader = "";
  if(gblServiceType == "soGood"){
    labelTextHeader = kony.i18n.getLocalizedString("MB_lbSelectCreditCardforSoGoood"); 
  }else if(gblServiceType == "cashAdvance"){
    labelTextHeader = kony.i18n.getLocalizedString("MB_lbSelectCreditCardforCashAdvance"); 
  }
  setDeeplinkCardHeader(labelTextHeader);
  invokeServiceSecureAsync("customerCardList", inputparam, getCallbackShowCreditCardList);

}
function getCallbackShowCreditCardList(status, resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    gblCardRef = "";
    if (resulttable["opstatus"] == 0) {
      if(resulttable["errCode"] == undefined) {
        if(resulttable["creditCardRec"].length > 0) {
          gblCardList = resulttable;
          frmMBCreditCardListDeeplink.flxNoCard.setVisibility(false);
          frmMBCreditCardListDeeplink.FlexMain.setVisibility(true);
          var haveCardList = dynamicListCardForDeeplink(resulttable["creditCardRec"]);
          if(haveCardList >= 1){
            if(haveCardList == 1 && gblDeepLinkFlow){
              gblDeepLinkFlow = false;
              objEvent = {};
              objEvent["id"] = gblCardRef;
              prepareCreditCardData(objEvent);
            }else{
              gblDeepLinkFlow = false;
              frmMBCreditCardListDeeplink.flxNoCard.setVisibility(false);
              frmMBCreditCardListDeeplink.FlexMain.setVisibility(true);
              frmMBCreditCardListDeeplink.show();
            }
          }else{
            gblDeepLinkFlow = false;
            gblCardRef = "";
            frmMBCreditCardListDeeplink.flxNoCard.setVisibility(true);
            frmMBCreditCardListDeeplink.FlexMain.setVisibility(false);
            frmMBCreditCardListDeeplink.show();
          }
        }else {
          gblDeepLinkFlow = false;
          frmMBCreditCardListDeeplink.flxNoCard.setVisibility(true);
          frmMBCreditCardListDeeplink.FlexMain.setVisibility(false);
          frmMBCreditCardListDeeplink.show();
        }
      } else {
        gblDeepLinkFlow = false;
        showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        return false;
      }
    } else{
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  } else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}
function setDeeplinkCardHeader(txtLabel){

  var lblSectionHeader = "";
  var flexSectionHeader = "";

  lblSectionHeader = new kony.ui.Label({
    "id": "lblSectionHeader" + txtLabel,
    "isVisible": true,
    "left": "10%",
    "skin": "lblblackbig",
    "top": "0dp",
    "width": "80%",
    "text": txtLabel,
    "zIndex": 1
  }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
    "textCopyable": false
  });
  flexSectionHeader = new kony.ui.FlexContainer({
    "id": "flexSectionHeader" +  txtLabel,
    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
    "clipBounds": true,
    "isVisible": true,
    "layoutType": kony.flex.FREE_FORM,
    "left": "0dp",
    "skin": "slFbox",
    "top": "5%",
    "width": "100%",
    "zIndex": 1
  }, {}, {});
  flexSectionHeader.setDefaultUnit(kony.flex.DP);
  flexSectionHeader.add(
    lblSectionHeader);

  frmMBCreditCardListDeeplink.FlexMain.setDefaultUnit(kony.flex.DP);
  frmMBCreditCardListDeeplink.FlexMain.add(flexSectionHeader);

}
function dynamicListCardForDeeplink(cardArray){


  var imgCard = "";
  var lblRibbon = "";
  var flexCard = "";
  var lblLinkText = "";
  var flexLink = "";
  var flexContent = "";
  var lblCardNo = "";
  var lblCardNickName = "";
  var btnCard = "";

  var imgSrc = "";
  var ribbonText = "";
  var linkText = "";
  var cardEvent = "";
  var cardNickName = "";
  var cardNo = "";

  var ribbonSkin = "";

  var cardType = "";
  var cardStatus = "";
  var cardFrmID = "";

  var allowManageCard = "";
  var imageName = "";

  var cardheight = "40%";
  var cardNoSkin = "lblWhiteCard24px";
  var cardNameSkin = "lblBlackCard22px"; //MKI, MIB-12137
  var imgSrcFailed = "";
  var gblDeviceInfo = kony.os.deviceInfo();
  var deviceName = gblDeviceInfo["name"];
  var screenwidth = gblDeviceInfo["deviceWidth"];
  var screenheight = gblDeviceInfo["deviceHeight"];
  var haveCardList = 0;
  if (deviceName == "android") {
    if((screenwidth == 1080 && screenheight < 1920) || screenwidth == 480 || (screenwidth == 720 && screenheight < 1280) || (screenwidth == 1440 && screenheight < 2560)) {
      cardheight = "43%";
    } else if (screenwidth == 768 && screenheight < 1280) {
      cardheight = "46%";
    } else {
      cardheight = "40%";
    }
  } else {
    cardheight = "40%";
  }


  if(cardArray.length < 1) {
    return;
  }

  for(var index = 0; index < cardArray.length; index++) {
    cardType = cardArray[index]["cardType"];
    cardFrmID = setCardId(cardType) + index;
    cardStatus = cardArray[index]["cardStatus"];
    //applySoGoodFlag
    if (cardStatus != "Active") {
      continue;
    }else if(cardArray[index]["applySoGoodFlag"] != "Y" && gblServiceType == "soGood"){
      continue;
    }
    if(cardArray[index]["IMG_CARD_ID"] == "DebitTMBWave"){ //MKI, MIB-12137 start
      cardNameSkin = "lblBlackCard22px";
    }
    else{         
      cardNameSkin = "lblWhiteCard22px";
    }
    cardNoSkin = getCardNoSkin(cardArray[index]["IMG_CARD_ID"]); //MKI, MIB-12137 end
    if(cardType == "Debit Card") {
      allowManageCard = cardArray[index]["allowManageCard"];
      imgSrcFailed = "debitdefault.png";
    } else {
      if(cardType == "Credit Card"){
        if(cardArray[index]["accountNoFomatted"][0] == '5') {;
                                                             imgSrcFailed = "masterdefault.png";
                                                            } else if (cardArray[index]["accountNoFomatted"][0] == '4'){
                                                              imgSrcFailed = "visadefault.png";
                                                            }
      } else if (cardType == "Ready Cash"){
        imgSrcFailed = "readycard.png";
      }

    }
    imageName = cardArray[index]["IMG_CARD_ID"];
    imgSrc = getImageSRC(imageName);

    kony.print(" imageName : " + imageName + " imgSrc : " + imgSrc);
    if(cardArray[index]["acctNickName"] != undefined ) {
      cardNickName = cardArray[index]["acctNickName"];
      if(cardNickName == "") {
        if(kony.i18n.getCurrentLocale() == "en_US") {
          cardNickName = cardArray[index]["defaultCurrentNickNameEN"];
        } else {
          cardNickName = cardArray[index]["defaultCurrentNickNameTH"];
        }
      }
    } else {
      if(kony.i18n.getCurrentLocale() == "en_US") {
        cardNickName = cardArray[index]["defaultCurrentNickNameEN"];
      } else {
        cardNickName = cardArray[index]["defaultCurrentNickNameTH"];
      }
    }


    cardNo = cardArray[index]["accountNoFomatted"].split("-").join("  ");
    linkText = getLinkTextCard(cardType, cardStatus, allowManageCard);
    cardEvent = getLinkEventCardForDeepLink(cardType, cardStatus, allowManageCard);
	gblCardRef = "btnCard" + cardFrmID;
    btnCard = new kony.ui.Button({
      "id": "btnCard" + cardFrmID,
      "top": "0%",
      "left": "0",
      "width": "100%",
      "height": "100%",
      "zIndex": 5,
      "isVisible": true,
      "text": null,
      "skin": "noSkinButtonNormal",
      "focusSkin" : "noSkinButtonNormal",
      "onClick" : cardEvent
    }, {
      "padding": [0, 0, 0, 0],
      "contentAlignment": constants.CONTENT_ALIGN_CENTER,
      "displayText": true,
      "marginInPixel": false,
      "paddingInPixel": false,
      "containerWeight": 0
    }, {
      "glowEffect": false,
      "showProgressIndicator": true
    });
    imgCard = new kony.ui.Image2({
      "id": "imgCard" + cardFrmID,
      "top": "-8.3%", //-4% good for nexus , -8% good for sam
      "width": "100%",
      "height": "100.0%",
      "zIndex": 3,
      "isVisible": true,
      "src": imgSrc,
      "imageWhenFailed": imgSrcFailed,
      "imageWhileDownloading": "empty.png"
    }, {
      "padding": [0, 0, 0, 0],
      "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
      "marginInPixel": false,
      "paddingInPixel": false,
      "containerWeight": 0
    }, {});

    lblLinkText = new kony.ui.Label({
      "id": "lblLinkText" + cardFrmID,
      "top": "0dp",
      "centerX": "50%",
      "zIndex": 3,
      "isVisible": true,
      "text": linkText,
      "skin": "lblbluemedium200"
    }, {
      "padding": [0, 0, 0, 0],
      "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
      "marginInPixel": false,
      "paddingInPixel": false,
      "containerWeight": 0
    }, {
      "textCopyable": false
    });

    lblCardNo = new kony.ui.Label({
      "id": "lblCardNo" + cardFrmID,
      "top": "0%",
      "left": "10%",
      "width": "90%",
      "zIndex": 3,
      "isVisible": true,
      "text": cardNo,
      "skin": cardNoSkin
    }, {
      "padding": [0, 0, 0, 0],
      "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
      "marginInPixel": false,
      "paddingInPixel": false,
      "containerWeight": 0
    }, {
      "textCopyable": false
    });
    lblCardNickName = new kony.ui.Label({
      "id": "lblCardNickName" + cardFrmID,
      "top": "45%",
      "left": "10%",
      "width": "90%",
      "height": "40%",
      "zIndex": 3,
      "isVisible": true,
      "text": cardNickName,
      "skin": cardNameSkin //MKI, MIB-12137
    }, {
      "padding": [0, 0, 0, 0],
      "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
      "marginInPixel": false,
      "paddingInPixel": false,
      "containerWeight": 0
    }, {
      "textCopyable": false
    });

    flexContent = new kony.ui.FlexContainer({
      "id": "flexContent" + cardFrmID,
      "top": "0",
      "left": "0",
      "width": "100%",
      "height": "30%",
      "centerX": "50%",
      "centerY": "62%",
      "zIndex": 3,
      "isVisible": true,
      "clipBounds": true,
      "Location": "[0,107]",
      "skin": "slFbox",
      "layoutType": kony.flex.FREE_FORM
    }, {
      "padding": [0, 0, 0, 0]
    }, {});;
    flexContent.setDefaultUnit(kony.flex.DP)
    flexContent.add(lblCardNo, lblCardNickName);

    flexCard = new kony.ui.FlexContainer({
      "id": "flexCard" + cardFrmID,
      "top": "5%",
      "left": "10.89%",
      "width": "83%",
      "height": cardheight,
      "centerX": "50.0%",
      "zIndex": 2,
      "isVisible": true,
      "clipBounds": true,
      "Location": "[39,202]",
      "skin": "noSkinFlexContainer",
      "layoutType": kony.flex.FREE_FORM
    }, {
      "padding": [0, 0, 0, 0]
    }, {});
    flexCard.setDefaultUnit(kony.flex.DP)
    flexCard.add(btnCard, imgCard, flexContent);

    frmMBCreditCardListDeeplink.FlexMain.add(flexCard);
    haveCardList +=1;
  }
  var flexSession = new kony.ui.FlexContainer({
    "id": "flexSession" + cardType,
    "top": "0dp",
    "left": "0dp",
    "width": "100%",
    "height": "30dp",
    "zIndex": 1,
    "isVisible": true,
    "clipBounds": true,
    "Location": "[0,200]",
    "skin": "noSkinFlexContainer",
    "layoutType": kony.flex.FREE_FORM
  }, {
    "padding": [0, 0, 0, 0]
  }, {});

  frmMBCreditCardListDeeplink.FlexMain.add(flexSession);
  return haveCardList;
}

function getLinkEventCardForDeepLink(cardType, cardStatus, allowManageCard) {
  try{
    var performEventCard = null; 

    if(cardStatus == "Active") {
      performEventCard = prepareCreditCardData;
    } else if (cardStatus == "Not Activated") {
      performEventCard = performShowActivateCard;
    }
    return performEventCard;
  }
  catch(e){
    kony.print("ENTERED ERROR in getLinkEventCard:"+e);
  }
}
function prepareCreditCardData(eventobject, x, y) {
  showLoadingScreen();
  gblSelectedCard = getCardInfo(eventobject);
  if (isNotBlank(gblAccountTable["custAcctRec"]) && gblAccountTable["custAcctRec"].length > 0 && isNotBlank(gblSelectedCard) && isNotBlank(gblSelectedCard["acctsCardRefId"])) {
    //set gblIndex
    for (var i = 0; i < gblAccountTable["custAcctRec"].length; i++) {
      if (gblAccountTable["custAcctRec"][i]["cardRefId"] == gblSelectedCard["acctsCardRefId"]) {
        gblIndex = i;
        gblCACardRefNumber = gblSelectedCard["cardRefId"];
        gblManageCardFlow = "frmMBCreditCardListDeeplink";
        frmMBManageCard.lblCardNumber.text = gblSelectedCard["cardNo"];
        frmMBManageCard.lblCardAccountName.text = gblSelectedCard["cardName"];
        frmMBManageCard.imgCard.src = gblSelectedCard["imgSrc"];
        break;
      }
    }
    if (isNotBlank(gblIndex)) {
      if(gblServiceType == "soGood"){
        applySoGoodWithDeepLink(gblAccountTable["custAcctRec"][gblIndex]["accId"]);
      }else if(gblServiceType == "cashAdvance"){
        onClickOfCashAdvanceMenu();
        gblDeepLinkFlow = false;
        gblContentDeeplinkData = "";
      }
    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  } else {
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
  }
}

function applySoGoodWithDeepLink(cardId) {
  var inputparam = {};
  inputparam["cardId"] = cardId;
  inputparam["tranCode"] = "1";

  var status = invokeServiceSecureAsync("creditcardDetailsInq", inputparam, creditCardDeeplinkCallBack);
}

function creditCardDeeplinkCallBack(status, resulttable) {
  dismissLoadingScreen();
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      var allowApplySoGoooddet = gblAccountTable["custAcctRec"][gblIndex]["allowApplySoGoood"];
      if (allowApplySoGoooddet == "1" && validCicsTranCode(resulttable["cicsTranCode"])) {
        onClickApplySoGooODLink();
        gblDeepLinkFlow = false;
        gblContentDeeplinkData = "";
      } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  }else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}