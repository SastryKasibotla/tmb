function onClickEditBeneficiaryFromAccountDetailsIB(){
	gblEditClickFromAccountDetails=true;
	frmIBMyAccnts.init(); // prevents global variables not initialized error
	myAccountListServiceIB(); // Service called on My accounts
	//populateMyAccountsListScreenIB();
}

function getBeneficaryDetailsMyAccountsIB(){
	showLoadingScreenPopup();
	var inputparam = {};
	var index = frmIBMyAccnts.segTMBAccntsList.selectedIndex[1];
	inputparam["acctId"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0]["hiddenAccountNo"];
	invokeServiceSecureAsync("beneficiaryInq", inputparam, getBeneficaryDetailsMyAccountsIBCallback);
}

function getBeneficaryDetailsMyAccountsIBCallback(status,resulttable){
	if(status == 400){
		dismissLoadingScreenPopup();
		if(resulttable["opstatus"]==0){
			if(resulttable["StatusCode"] == "0"){
				if (resulttable["neverSetBeneficiary"] != undefined){
					gblNeverSetBeneficiary=resulttable["neverSetBeneficiary"];
				}else{
					gblNeverSetBeneficiary="";
				}
				//inq service is success
				if(resulttable["beneficiaryDS"].length >0){
					gblBeneficiaryData = resulttable["beneficiaryDS"];
					handleSavingCareIBMyAccount(true);
				}else{
					handleSavingCareIBMyAccount(false);
					gblBeneficiaryData="";
				}
			}else{
				//error case when service fail
			}
		}
	}
}

function handleSavingCareIBMyAccount(hasData){
	frmIBEditMyAccounts.hbxViewBeneficiary.isVisible=true;	
	if (frmIBMyAccnts.segTMBAccntsList.selectedItems[0]["hiddenOpeningMethod"] != undefined) {
		gblOpeningMethod=frmIBMyAccnts.segTMBAccntsList.selectedItems[0]["hiddenOpeningMethod"];
	}else{		
		gblOpeningMethod="";
	}
	if(hasData){
		frmIBEditMyAccounts.hbxBeneficiaryText.isVisible=true;
		frmIBEditMyAccounts.hbxBeneficiaryNotSpecified.isVisible=false;
		frmIBEditMyAccounts.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesSpecify");
		frmIBEditMyAccounts.lblSegBeneficiaryName.text=kony.i18n.getLocalizedString("keyBeneficiaryName");
		frmIBEditMyAccounts.lblSegBeneficiaryRelation.text=kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
		frmIBEditMyAccounts.lblSegBeneficiaryBenefit.text=kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
		var locale = kony.i18n.getCurrentLocale();
		var relationValue="";
		
		var segBeneficiarydata=[];
		for (var i=0;i<gblBeneficiaryData.length;i++){
			if("th_TH" == locale){
				relationValue = gblBeneficiaryData[i].relationTH;
			}else{
				relationValue = gblBeneficiaryData[i].relationEN;
			}
			var tempSegmentRecord = {
				"lblSegBeneficiaryNameValue":gblBeneficiaryData[i].fullName,
				"lblSegBeneficiaryRelationValue":relationValue,
				"lblSegBeneficiaryBenefitValue":gblBeneficiaryData[i].percentValue,
				"hdnSegBeneficiaryRelationCode":gblBeneficiaryData[i].relationCode
			};
			segBeneficiarydata.push(tempSegmentRecord);
		}
		frmIBEditMyAccounts.segBeneficiaryData.setData(segBeneficiarydata);
	}else{
		frmIBEditMyAccounts.hbxBeneficiaryText.isVisible=false;
		frmIBEditMyAccounts.hbxBeneficiaryNotSpecified.isVisible=true;
		frmIBEditMyAccounts.segBeneficiaryData.setData([]);		
		if(kony.string.equalsIgnoreCase(frmIBMyAccnts.segTMBAccntsList.selectedItems[0]["hiddenOpeningMethod"],"BRN") && kony.string.equalsIgnoreCase(gblNeverSetBeneficiary, "Y")){
			frmIBEditMyAccounts.lblBeneficiaryNotSpecified.text=kony.i18n.getLocalizedString("keyBeneficiaryNoDataBranchEdit");
			frmIBEditMyAccounts.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"));
		}else{
			frmIBEditMyAccounts.lblBeneficiaryNotSpecified.text=kony.i18n.getLocalizedString("keyBeneficiariesNotSpecifyDetail");
			frmIBEditMyAccounts.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify");
		}
	}	
	//if(gblEditClickFromAccountDetails)onClickEditSavingsCare(); // Directly show T n C
}

function callTermsAndConditionForEditBeneficiaryIB(showPdf){
	showLoadingScreenPopup();
	var input_param={}; 
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US") {
		input_param["localeCd"]="en_US";
	}else{
		input_param["localeCd"]="th_TH";
	}
	input_param["moduleKey"]= "TMBSavingCare";
	
	if(showPdf)
		invokeServiceSecureAsync("readUTFFile", input_param, loadTNCOpenActCallBackIB);
	else
		invokeServiceSecureAsync("readUTFFile", input_param, loadTnCEditBeneficiaryCallBack);	
}

function loadTnCEditBeneficiaryCallBack(status,result){
	if(status == 400){
		if(result["opstatus"] == 0){
			frmIBEditMyAccounts.hbxTermsAndConditions.isVisible=true;
			frmIBEditMyAccounts.hbxViewAccnt.isVisible=false;
			frmIBEditMyAccounts.lblOpenActDesc.text =  result["fileContent"];
			frmIBEditMyAccounts.lblOpenActDescSubTitle.setFocus(true);
			if(kony.application.getCurrentForm().id == "frmIBEditMyAccounts")
				dismissLoadingScreenPopup();
		}else{
			dismissLoadingScreenPopup();
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function onClickEditSavingsCare(){
	getRelationShipsFromService();
	if(frmIBMyAccnts.segTMBAccntsList.selectedItems != null && undefined != frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID && gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID) >= 0){
		onMyAccountEditIB();
	}else{
		onClickEditMyaccount();
		frmIBEditMyAccounts.hbxEditBeneficiary.isVisible=false;
	}	
}

function onMyAccountEditIB(){
    if(gblOpeningMethod=="BRN" && gblNeverSetBeneficiary=="Y"){
		popupIBCommonConfirmCancel.lblPopUpHeader.text=kony.i18n.getLocalizedString("edit_benefi");
		popupIBCommonConfirmCancel.lblPopUpMessage.text=kony.i18n.getLocalizedString("keyeditBeneficSure");
		popupIBCommonConfirmCancel.btnPopUpConfirm.text=kony.i18n.getLocalizedString("keyYes");
		popupIBCommonConfirmCancel.btnPopUpCancel.text=kony.i18n.getLocalizedString("keyNo");
		popupIBCommonConfirmCancel.btnPopUpConfirm.onClick = onpopupEditSavingsCareYES;		
		popupIBCommonConfirmCancel.btnPopUpCancel.onClick=onpopupEditSavingsCareNO;	
		popupIBCommonConfirmCancel.show();
	}else{
		callTermsAndConditionForEditBeneficiaryIB(false);
		frmIBEditMyAccounts.hbxEditBeneficiary.isVisible=false;
	}
}

function onpopupEditSavingsCareYES(){
	popupIBCommonConfirmCancel.dismiss();
	callTermsAndConditionForEditBeneficiaryIB(false);
	frmIBEditMyAccounts.hbxEditBeneficiary.isVisible=false;
}

function onpopupEditSavingsCareNO(){
	popupIBCommonConfirmCancel.dismiss();
}

function onClickEmailTnCIBEditSavingCare() {
	//call notification add service for email with the tnc keyword
	showLoadingScreen()
	var inputparam = {};
	inputparam["channelName"] = "Internet Banking";
	inputparam["channelID"] = "01";
	inputparam["notificationType"] = "Email"; // always email
	inputparam["phoneNumber"] = gblPHONENUMBER;
	inputparam["mail"] = gblEmailId;
	inputparam["customerName"] = gblCustomerName;
	inputparam["localeCd"] = kony.i18n.getCurrentLocale();
    inputparam["moduleKey"] = "OpenAccount";
    inputparam["fileNameOpen"] = "TMBSavingCare";
	inputparam["productName"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameEng;
	inputparam["productNameTH"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameThai;
	inputparam["prodCode"] = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID;
	invokeServiceSecureAsync("TCEMailService", inputparam, callBackNotificationOpenAccntAddServiceMB);
}

function onClickCancelDuringEditSavingCare(){
	if(gblEditClickFromAccountDetails){
		accountsummaryLangToggleIB();
	}else{
		frmIBEditMyAccounts.hbxViewAccnt.isVisible=true;
		frmIBEditMyAccounts.hbxTermsAndConditions.isVisible=false;
		frmIBEditMyAccounts.hbxEditAccntNN.isVisible=false;
		frmIBEditMyAccounts.hbxTMBImg.isVisible=true;
	}
}

function onClickAgreeTnCEditSavingCareIB(){
	if(undefined != gblAccountTable["SAVING_CARE_MAX_BENEFICIARIES"]){
		//configurable beneficiary count.
		gblMaxBeneficiaryConfig = kony.os.toNumber(gblAccountTable["SAVING_CARE_MAX_BENEFICIARIES"]);
	}	
	onClickEditMyaccount();
	frmIBEditMyAccounts.hbxEditBeneficiary.isVisible=true;
	frmIBEditMyAccounts.hbxViewAccnt.isVisible=false;
	frmIBEditMyAccounts.hbxTermsAndConditions.isVisible=false;
	frmIBEditMyAccounts.hbxEditAccntNN.isVisible=true;
	frmIBEditMyAccounts.hbxTMBImg.isVisible=true;
	for(var i=0;i<gblBeneficiaryCount;i++){
		if(frmIBEditMyAccounts["txtBefFirstName"+i] != undefined){
			deleteBeneficiaryEntry({"id":""+i})
		}
	}
	gblBeneficiaryCount=0;
	gblBeneficaryIndexArray=[];
	//getRelationShipsFromService();
	if(gblBeneficiaryData==""){
		onClickNonSpecifyEditSavingCareIB();
	}else{
		onClickSpecifyEditSavingCareIB();
		populateBeneficiaryDataForEditIB();
	}
}

function onClickSpecifyEditSavingCareIB(){
	frmIBEditMyAccounts.btnSpecified.skin=btnIBTab4LeftFocus;
	frmIBEditMyAccounts.btnNotSpecified.skin=btnIbTab4RightNrml;
	frmIBEditMyAccounts.hbxEditBeneficiaryNotSpecified.isVisible=false;
	frmIBEditMyAccounts.hbxBeneficiaryData.isVisible=true;
	frmIBEditMyAccounts.hbxLink3.isVisible=true;
}

function onClickNonSpecifyShowPopupIB(){
	if(gblBeneficaryIndexArray.length >0){
		popupIBCommonConfirmCancel.lblPopUpHeader.text=kony.i18n.getLocalizedString("Confirmation");
		popupIBCommonConfirmCancel.btnPopUpConfirm.onClick=onClickNonSpecifyEditSavingCareIB;
		popupIBCommonConfirmCancel.lblPopUpMessage.text=kony.i18n.getLocalizedString("keyBeneficiaryCnfrmDelDetail");
		popupIBCommonConfirmCancel.show();
	}else{
		onClickNonSpecifyEditSavingCareIB();
	}
}

function onClickNonSpecifyEditSavingCareIB(){
	for(var i=0;i<gblBeneficiaryCount;i++){
		if(frmIBEditMyAccounts["txtBefFirstName"+i] != undefined){
			deleteBeneficiaryEntry({"id":""+i})
		}
	}
	gblBeneficiaryCount=0;
	gblBeneficaryIndexArray=[];
	frmIBEditMyAccounts.btnSpecified.skin=btnIBTab4LeftNrml;
	frmIBEditMyAccounts.btnNotSpecified.skin=btnIBTab4RightFocus;
	frmIBEditMyAccounts.hbxEditBeneficiaryNotSpecified.isVisible=true;
	frmIBEditMyAccounts.hbxBeneficiaryData.isVisible=false;
	frmIBEditMyAccounts.hbxLink3.isVisible=false;
}

function createDynamicBeneficiaryEntry(){
	 var lblFiller = new kony.ui.Label({
        "id": "lblFiller"+gblBeneficiaryCount,
        "isVisible": true,
        "text": null,
        "skin": "lblNormal"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 91
    }, {
        "toolTip": null
    });
    var btnDeleteBeneficiary = new kony.ui.Button({
        "id": "btnDeleteBeneficiary"+gblBeneficiaryCount,
        "isVisible": true,
        "text": null,
        "skin": "btnIBdelicon",
        "focusSkin": "btnIBdeliconHover",
        "onClick": showDeleteBeneficiaryConfirmPopupIB
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_MIDDLE_RIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 8, 0],
        "padding": [0, 0, 0, 0],
        "displayText": true,
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 9
    }, {
        "hoverskin": "btnIBdeliconHover",
        "toolTip": null
    });
    hbxDeleteButton = new kony.ui.Box({
        "id": "hbxDeleteButton"+gblBeneficiaryCount,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 26,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 3, 0, 3],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxDeleteButton.add(lblFiller, btnDeleteBeneficiary);
    var lblBefFirstName = new kony.ui.Label({
        "id": "lblBefFirstName"+gblBeneficiaryCount,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyFirstName"),
        "skin": "lblIB20pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyFirstName')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 30
    }, {
        "toolTip": null
    });
    var txtBefFirstName = new kony.ui.TextBox2({
        "id": "txtBefFirstName"+gblBeneficiaryCount,
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "maxTextLength": 19,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtIB20pxGreyBackground",
        "focusSkin": "txtIB20pxGreyFocus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_DEFAULT_PLATFORM_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 70
    }, {
        "onBeginEditing": changeSkinBeneficiaryTextBox_OnBeginEditingIB,
        "onEndEditing": "",
        "autoCorrect": false,
        "autoComplete": false
    });
    var hbxBefFirstName = new kony.ui.Box({
        "id": "hbxBefFirstName"+gblBeneficiaryCount,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxProper",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 21,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 2, 0, 1],
        "padding": [3, 0, 3, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefFirstName.add(lblBefFirstName, txtBefFirstName);
    var lineBefFirstName = new kony.ui.Line({
        "id": "lineBefFirstName"+gblBeneficiaryCount,
        "isVisible": true,
        "skin": "line300px"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var lblBefLastName = new kony.ui.Label({
        "id": "lblBefLastName"+gblBeneficiaryCount,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyLastName"),
        "skin": "lblIB20pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyLastName')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 30
    }, {
        "toolTip": null
    });
    var txtBefLastName = new kony.ui.TextBox2({
        "id": "txtBefLastName"+gblBeneficiaryCount,
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "maxTextLength": 20,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtIB20pxGreyBackground",
        "focusSkin": "txtIB20pxGreyFocus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_DEFAULT_PLATFORM_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 70
    }, {
        "onBeginEditing": changeSkinBeneficiaryTextBox_OnBeginEditingIB,
        "onEndEditing": "",
        "autoCorrect": false,
        "autoComplete": false
    });
    var hbxBefLastName = new kony.ui.Box({
        "id": "hbxBefLastName"+gblBeneficiaryCount,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxProper",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 21,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 1, 0, 1],
        "padding": [3, 0, 3, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefLastName.add(lblBefLastName, txtBefLastName);
    var lineBefLastName = new kony.ui.Line({
        "id": "lineBefLastName"+gblBeneficiaryCount,
        "isVisible": true,
        "skin": "line300px"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var cbxData = gblRelationshipData[0];
    if(kony.i18n.getCurrentLocale() == "th_TH")
    	cbxData = gblRelationshipData[1];
    var cbxBefRelation = new kony.ui.ComboBox({
        "id": "cbxBefRelation"+gblBeneficiaryCount,
        "isVisible": true,
        "masterData": cbxData,
        "selectedKey": "P",
        "skin": "cbxIBDropDownArrowGreyNoLine",
        "focusSkin": "cbxIBDropDownArrowGreyNoLineFocus",
        "onSelection": ""
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 1, 0, 1],
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 100
    }, {
        "viewType": constants.COMBOBOX_VIEW_TYPE_LISTVIEW,
        "hoverSkin": "cbxIBDropDownArrowGreyNoLineFocus"
    });
    var hbxBefRelation = new kony.ui.Box({
        "id": "hbxBefRelation"+gblBeneficiaryCount,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 21,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [3, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefRelation.add(cbxBefRelation);
    var lineBefRelation = new kony.ui.Line({
        "id": "lineBefRelation"+gblBeneficiaryCount,
        "isVisible": true,
        "skin": "line300px"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var lblBefBenefit = new kony.ui.Label({
        "id": "lblBefBenefit"+gblBeneficiaryCount,
        "isVisible": true,
        "text": kony.i18n.getLocalizedString("keyBenfit"),
        "skin": "lblIB20pxBlack",
        "i18n_text": "kony.i18n.getLocalizedString('keyBenfit')"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 33
    }, {
        "toolTip": null
    });
    var txtBefBenefit = new kony.ui.TextBox2({
        "id": "txtBefBenefit"+gblBeneficiaryCount,
        "isVisible": true,
        "text": null,
        "secureTextEntry": false,
        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
        "maxTextLength": 3,
        "placeholder": null,
        "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
        "skin": "txtIB20pxGreyBackground",
        "focusSkin": "txtIB20pxGreyFocus"
    }, {
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
        "vExpand": false,
        "hExpand": true,
        "margin": [0, 0, 0, 0],
        "padding": [1, 1, 1, 1],
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "containerHeightMode": constants.TEXTBOX_DEFAULT_PLATFORM_HEIGHT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 67
    }, {
        "onBeginEditing": changeSkinBeneficiaryTextBox_OnBeginEditingIB,
        "onEndEditing": "",
        "autoCorrect": false,
        "autoComplete": false
    });
    var hbxBefBenefit = new kony.ui.Box({
        "id": "hbxBefBenefit"+gblBeneficiaryCount,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hbxProper",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 26,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 1, 0, 1],
        "padding": [4, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxBefBenefit.add(lblBefBenefit, txtBefBenefit);
    var lineBefBenefit = new kony.ui.Line({
        "id": "lineBefBenefit"+gblBeneficiaryCount,
        "isVisible": true,
        "skin": "line300pxBottom"
    }, {
        "thickness": 1,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "paddingInPixel": false
    }, {});
    var vbxEachBeneficiaryDetail = new kony.ui.Box({
        "id": "vbxEachBeneficiaryDetail"+gblBeneficiaryCount,
        "isVisible": true,
        "orientation": constants.BOX_LAYOUT_VERTICAL
    }, {
        "containerWeight": 100,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "marginInPixel": false,
        "paddingInPixel": false,
        "vExpand": false,
        "hExpand": true,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    vbxEachBeneficiaryDetail.add(hbxBefFirstName, lineBefFirstName, hbxBefLastName, lineBefLastName, hbxBefRelation, lineBefRelation, hbxBefBenefit, lineBefBenefit);
    hbxEachBeneficiaryDetail = new kony.ui.Box({
        "id": "hbxEachBeneficiaryDetail"+gblBeneficiaryCount,
        "isVisible": true,
        "position": constants.BOX_POSITION_AS_NORMAL,
        "skin": "hboxLightGrey",
        "orientation": constants.BOX_LAYOUT_HORIZONTAL
    }, {
        "containerWeight": 74,
        "percent": true,
        "widgetAlignment": constants.WIDGET_ALIGN_TOP_LEFT,
        "margin": [0, 0, 0, 0],
        "padding": [0, 0, 0, 0],
        "vExpand": false,
        "marginInPixel": false,
        "paddingInPixel": false,
        "layoutType": constants.CONTAINER_LAYOUT_BOX
    }, {});
    hbxEachBeneficiaryDetail.add(vbxEachBeneficiaryDetail);
}

function changeSkinBeneficiaryTextBox_OnBeginEditingIB(eventobject){
	var widgetid = eventobject["id"]
    frmIBEditMyAccounts[widgetid].skin = "txtIB20pxGreyBackground";
}

function addBeneficaryEntry(){
	if(validateAllBeneficiaryDataIB()){
		if(gblMaxBeneficiaryConfig > gblBeneficaryIndexArray.length){
			gblBeneficaryIndexArray.push(gblBeneficiaryCount);
			createDynamicBeneficiaryEntry();
			frmIBEditMyAccounts.vbxBeneficiaryData.add(hbxDeleteButton,hbxEachBeneficiaryDetail);
			addNumberCheckListnerZeroToNine("txtBefBenefit"+gblBeneficiaryCount);// Restricting to only numbers for benefit percent fields.
			gblBeneficiaryCount++;
			if(gblMaxBeneficiaryConfig == gblBeneficaryIndexArray.length){
				frmIBEditMyAccounts.hbxLink3.isVisible=false;
			}
		}else{
			frmIBEditMyAccounts.hbxLink3.isVisible=false;
		}
	}	
}

function showDeleteBeneficiaryConfirmPopupIB(eventobject){
	popupIBCommonConfirmCancel.lblPopUpHeader.text=kony.i18n.getLocalizedString("Confirmation");
	popupIBCommonConfirmCancel.lblPopUpMessage.text=kony.i18n.getLocalizedString("keyBeneficiaryConfirmDelete");
	popupIBCommonConfirmCancel.show();
	popupIBCommonConfirmCancel.btnPopUpConfirm.onClick=function(){deleteBeneficiaryEntry(eventobject)};
}
function deleteBeneficiaryEntry(eventobject){
	popupIBCommonConfirmCancel.dismiss();
	var indexDeleted = eventobject.id.match(/[0-9]+/)[0];
	var delButtons = frmIBEditMyAccounts["hbxDeleteButton"+indexDeleted].removeFromParent();
	var delDetails = frmIBEditMyAccounts["hbxEachBeneficiaryDetail"+indexDeleted].removeFromParent();
	gblBeneficaryIndexArray.remove(indexDeleted);
	if(gblMaxBeneficiaryConfig > gblBeneficaryIndexArray.length){
		frmIBEditMyAccounts.hbxLink3.isVisible=true;
	}
	if(gblBeneficaryIndexArray.length == 0){
		onClickNonSpecifyEditSavingCareIB();
	}
}

function populateBeneficiaryDataForEditIB(){
	for (var i=0;i<gblBeneficiaryData.length;i++){
		addBeneficaryEntry();
		var addBeneficiaryIndex = gblBeneficiaryCount-1;
		var relationCode = gblBeneficiaryData[i].relationCode;
		var fullName = gblBeneficiaryData[i].fullName.trim();
		if("th_TH" == locale){
			relationValue = gblBeneficiaryData[i].relationTH;
		}else{
			relationValue = gblBeneficiaryData[i].relationEN;
		}
		frmIBEditMyAccounts["cbxBefRelation"+addBeneficiaryIndex].selectedKey = relationCode;
		
		var firstName = fullName.substring(0,fullName.indexOf(" "));
		var lastName = fullName.substring(fullName.indexOf(" ")+1);
		
		frmIBEditMyAccounts["txtBefFirstName"+addBeneficiaryIndex].text = firstName; // first name
		frmIBEditMyAccounts["txtBefLastName"+addBeneficiaryIndex].text = lastName; //last name
		frmIBEditMyAccounts["txtBefBenefit"+addBeneficiaryIndex].text = gblBeneficiaryData[i].percentValue; //percentage
	}
}

function validateAllBeneficiaryDataIB(){
	gblHaveErrorFlag = false;
	for(var i in gblBeneficaryIndexArray){
		var indexWidget = gblBeneficaryIndexArray[i];
		
		var firstName = frmIBEditMyAccounts["txtBefFirstName"+indexWidget];
		var lastName = frmIBEditMyAccounts["txtBefLastName"+indexWidget];
		var relation = frmIBEditMyAccounts["cbxBefRelation"+indexWidget].selectedKey;
		var benefitPercent = frmIBEditMyAccounts["txtBefBenefit"+indexWidget];
		
		if (firstName.text.trim() == "" || firstName.text == null) {
            showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail"), kony.i18n.getLocalizedString("info"));
            firstName.skin = "txtErrorBG";
            return false;
        } else if (lastName.text.trim() == "" || lastName.text == null) {
            showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail"), kony.i18n.getLocalizedString("info"));
            lastName.skin = "txtErrorBG";
            return false;
        } else if (benefitPercent.text.trim() == "" || benefitPercent.text == null) {
            showAlert(kony.i18n.getLocalizedString("keyBeneficiaryAddDetail"), kony.i18n.getLocalizedString("info"));
            benefitPercent.skin = "txtErrorBG";
            return false;
        } else if (relation == "P" || relation == null) {
            showAlert(kony.i18n.getLocalizedString("keyBeneficiarySelRelationship"), kony.i18n.getLocalizedString("info"));
            return false;
        } else if (!(benNameAlpNumValidation(firstName.text))) {
            showAlert(kony.i18n.getLocalizedString("keyBenFirstNameAlphaNum"), kony.i18n.getLocalizedString("info"));
            firstName.skin = txtErrorBG;
            return false;
        } else if (!(benNameAlpNumValidation(lastName.text))) {
            showAlert(kony.i18n.getLocalizedString("keyBenSecondNameAlphNum"), kony.i18n.getLocalizedString("info"));
            lastName.skin = txtErrorBG;
            return false;
        } else if ( kony.os.toNumber(benefitPercent.text) > 100 ) {
            showAlert(kony.i18n.getLocalizedString("keyBenpercentage"), kony.i18n.getLocalizedString("info"));
            benefitPercent.skin = "txtErrorBG";
            return false;
        } else if ( kony.os.toNumber(benefitPercent.text) <= 0 ) {
            gblHaveErrorFlag = true;
        } 
	}
	return true;
}

function duplicateBeneficiaryNameIB(){
	uniqArr = [];
    for (var i in gblBeneficaryIndexArray) {
    	var indexWidget = gblBeneficaryIndexArray[i];
        var usrName = frmIBEditMyAccounts["txtBefFirstName"+indexWidget].text.trim() + frmIBEditMyAccounts["txtBefLastName"+indexWidget].text.trim();
        uniqArr.push(usrName);
    }
    for (i = 0; i < uniqArr.length; i++) {
        for (j = 0; j < uniqArr.length; j++) {
            if (i != j) {
                if (uniqArr[i] == uniqArr[j]) {
                    return false;
                }
            }
        }
    }
    return true;
}

function totalPercentBeneficiaryIB(){
	var totalBenPercent = 0;
    for (var i in gblBeneficaryIndexArray) {
    	var indexWidget = gblBeneficaryIndexArray[i];
        var amount = frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim();
        if(amount == "")amount = 0;
        totalBenPercent = totalBenPercent + kony.os.toNumber(amount)
    }
    if(frmIBEditMyAccounts.btnNotSpecified.skin == "btnIBTab4RightFocus"){
    	totalBenPercent=100;
    }
    return totalBenPercent;
}

function noChangeInBeneficiaryData(){
	//Comparing user entered data and Service data
	var serviceDataString="";
	for (var i in frmIBEditMyAccounts.segBeneficiaryData.data){
		var name = frmIBEditMyAccounts.segBeneficiaryData.data[i]["lblSegBeneficiaryNameValue"];
		var relation = frmIBEditMyAccounts.segBeneficiaryData.data[i]["hdnSegBeneficiaryRelationCode"];
		var benefit = frmIBEditMyAccounts.segBeneficiaryData.data[i]["lblSegBeneficiaryBenefitValue"];
		if(serviceDataString != "")
			serviceDataString = serviceDataString +","+name+relation+benefit;
		else
			serviceDataString = name+relation+benefit;
	}
	serviceDataString = returnSortedStringOfCommaSeparatedText(serviceDataString);
	
	var inputDataString="";
	for(var i in gblBeneficaryIndexArray){
		var indexWidget = gblBeneficaryIndexArray[i];
		var name = frmIBEditMyAccounts["txtBefFirstName"+indexWidget].text.trim()+" "+frmIBEditMyAccounts["txtBefLastName"+indexWidget].text.trim();
		var relation = frmIBEditMyAccounts["cbxBefRelation"+indexWidget].selectedKeyValue[0];
		var benefit = frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim();
		if(inputDataString != "")
			inputDataString = inputDataString+","+name+relation+benefit;
		else
			inputDataString = name+relation+benefit;
	}
	inputDataString = returnSortedStringOfCommaSeparatedText(inputDataString);
	
	if(serviceDataString.toUpperCase() == inputDataString.toUpperCase() && (gblNeverSetBeneficiary=="N"))	
		return true;
	return false;	
}

function returnSortedStringOfCommaSeparatedText(inputString){
	var temp = inputString.split(",");
	temp.sort();
	return temp.toString();
}
function checkLogicForEditBeneficiary(){
	if((frmIBMyAccnts.segTMBAccntsList.selectedItems != null && undefined != frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID && gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(frmIBMyAccnts.segTMBAccntsList.selectedItems[0].productID) < 0 ) || frmIBMyAccnts.segOtherBankAccntsList.selectedItems != null){
		showfrmViewAccntIB();
	}else if(noChangeInBeneficiaryData()){
		showfrmViewAccntIB();
	}else{
		var newNickName = frmIBEditMyAccounts.txtbxEditAccntNN.text;
		var oldNickName = frmIBEditMyAccounts.lblViewNickNameHdr.text;
		if(!NickNameValid(newNickName)){
			showAlert(kony.i18n.getLocalizedString("keyInvalidNickName"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		if ((newNickName == null) || (newNickName == "")) {
			showAlert(kony.i18n.getLocalizedString("keyEmptyNickName"), kony.i18n.getLocalizedString("info"));
			return false;
		}
		if (newNickName != oldNickName) {
			for (var i = 0; i < gblAccntData.length; i++) {
			    if (gblAccntData[i].accntStatus != "02") 
				{
					if (gblAccntData[i].nickName == newNickName) 
				    {
			            showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctnickname"), kony.i18n.getLocalizedString("info"));
			            return false;
			        }
			    }
			}
		}
		if(validateAllBeneficiaryDataIB()){
		
			if(gblHaveErrorFlag == true) {
				confirmValidateZeroValueEditBenefit();
			} else {
				var chkDupAndTot = validateDupNameAndTotPercent();
				return chkDupAndTot;
			}
		}	
	}
}

function confirmValidateZeroValueEditBenefit() {
	popupIBCommonConfirmCancel.lblPopUpHeader.text = kony.i18n.getLocalizedString("Confirmation");
    popupIBCommonConfirmCancel.lblPopUpMessage.text = kony.i18n.getLocalizedString("benefit_notAllocatedBenfi");
    popupIBCommonConfirmCancel.show();
    popupIBCommonConfirmCancel.btnPopUpConfirm.onClick = function() {
		validateZeroValueEditBenefit();
    };
	popupIBCommonConfirmCancel.btnPopUpCancel.onClick = function() {
   		unsuccessValidateBeneficiaryEditBenefit();
    };
}

function unsuccessValidateBeneficiaryEditBenefit() {
	popupIBCommonConfirmCancel.dismiss();
}

function validateZeroValueEditBenefit() {
	popupIBCommonConfirmCancel.dismiss();
	validateDupNameAndTotPercent();
}

function validateDupNameAndTotPercent() {
    if (!duplicateBeneficiaryNameIB()) {
        showAlert(kony.i18n.getLocalizedString("keyBeneficiaryDupName"), kony.i18n.getLocalizedString("info"));
        return false;
    }

    var totalPercent = totalPercentBeneficiaryIB();

    if (totalPercent != 100) {
        showAlert(kony.i18n.getLocalizedString("keyBeneficiaryTotalPercent"), kony.i18n.getLocalizedString("info"));
         return false;
    }
	saveEditBeneficiarySessionIB();
	return true;
}

function onClickNextEditBeneficiaryIB(){
	frmIBEditMyAccountsConfirmComplete.hbxOtpBox.isVisible=false;
	frmIBEditMyAccountsConfirmComplete.btnEditEStatement.isVisible=true;
	frmIBEditMyAccountsConfirmComplete.imgViewAccntDetails.src = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].imgAccountPicture;
	if(kony.i18n.getCurrentLocale() == "en_US")
		frmIBEditMyAccountsConfirmComplete.lblProductName.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameEng;
	else
		frmIBEditMyAccountsConfirmComplete.lblProductName.text = frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenProductNameThai;
	frmIBEditMyAccountsConfirmComplete.lblAccountNicknameValue.text =  frmIBEditMyAccounts.txtbxEditAccntNN.text;
	if(frmIBEditMyAccounts.btnSpecified.skin == "btnIBTab4LeftFocus"){
		//Specified is clicked
		frmIBEditMyAccountsConfirmComplete.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesSpecify");
		frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryName.text=kony.i18n.getLocalizedString("keyBeneficiaryName");
		frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryRelation.text=kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
		frmIBEditMyAccountsConfirmComplete.lblSegBeneficiaryBenefit.text=kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
		frmIBEditMyAccountsConfirmComplete.hbxBeneficiaryText.isVisible=true;
		frmIBEditMyAccountsConfirmComplete.hbxBeneficiaryNotSpecified.isVisible=false;
		//Populating segment here
		var segBeneficiarydata=[];
		for(var i in gblBeneficaryIndexArray){
			if(frmIBEditMyAccounts["txtBefBenefit"+i] != undefined && frmIBEditMyAccounts["txtBefBenefit"+i].text.trim() != "0") {
				var indexWidget = gblBeneficaryIndexArray[i];
				var tempSegmentRecord = {
					"lblSegBeneficiaryNameValue":frmIBEditMyAccounts["txtBefFirstName"+indexWidget].text.trim()+" "+frmIBEditMyAccounts["txtBefLastName"+indexWidget].text.trim(),
					"lblSegBeneficiaryRelationValue":frmIBEditMyAccounts["cbxBefRelation"+indexWidget].selectedKeyValue[1],
					"lblSegBeneficiaryBenefitValue":frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim()
				};
			segBeneficiarydata.push(tempSegmentRecord);
			}
		}
		frmIBEditMyAccountsConfirmComplete.segBeneficiaryData.setData(segBeneficiarydata);
	}else{
		frmIBEditMyAccountsConfirmComplete.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify");
		frmIBEditMyAccountsConfirmComplete.hbxBeneficiaryText.isVisible=false;
		frmIBEditMyAccountsConfirmComplete.hbxBeneficiaryNotSpecified.isVisible=true;
		frmIBEditMyAccountsConfirmComplete.segBeneficiaryData.setData([]);
	}
	frmIBEditMyAccountsConfirmComplete.show();
}

function saveEditBeneficiarySessionIB(){
	showLoadingScreenPopup();
	var fullNameList = "";
	var relationList = "";
	var benefitPercentList = "";
	var inputParams = {};
	for(var i in gblBeneficaryIndexArray){
		var indexWidget = gblBeneficaryIndexArray[i];
		var benefit = frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim();
		if(benefit != "0") {
			var fullname = frmIBEditMyAccounts["txtBefFirstName"+indexWidget].text.trim()+" "+frmIBEditMyAccounts["txtBefLastName"+indexWidget].text.trim();
			if(fullNameList == "")
				fullNameList = fullname;
			else
				fullNameList = fullNameList +"," + fullname;
			var relation = frmIBEditMyAccounts["cbxBefRelation"+indexWidget].selectedKey;
			if(relationList == "")
				relationList = relation;
			else
				relationList = relationList +","+ relation;
			var benefit = frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim();
			if(benefitPercentList == "")
				benefitPercentList = benefit;
			else	
				benefitPercentList = benefitPercentList +","+ benefit
		}
	}
	inputParams["beneficiaryfullNames"]=fullNameList;
	inputParams["beneficiarypercentage"]=benefitPercentList;
	inputParams["beneficiaryrelationship"]=relationList;
	
	invokeServiceSecureAsync("saveEditBeneficiarySession", inputParams, callBackSaveEditBeneficiarySessionIB);
}
function callBackSaveEditBeneficiarySessionIB(status,resulttable){
	if(status == 400){
		dismissLoadingScreenPopup();
		if(resulttable["opstatus"]==0){
			onClickNextEditBeneficiaryIB();
		}
	}
}

function tokenCheckEditBeneficiaryIB()
{	
	var locale = kony.i18n.getCurrentLocale();
	frmIBEditMyAccountsConfirmComplete.hbxToken.setVisibility(false);
	//frmIBEditMyAccountsConfirmComplete.lblOTPinCurr.setVisibility(false);
	//frmIBEditMyAccountsConfirmComplete.lblPlsReEnter.setVisibility(false);
	frmIBEditMyAccountsConfirmComplete.hbxOTPEntry.setVisibility(true);
	frmIBEditMyAccountsConfirmComplete.hbxOtpBox.setVisibility(true);
	frmIBEditMyAccountsConfirmComplete.txtBxOTP.setFocus(true);
	frmIBEditMyAccountsConfirmComplete.hbxOTPRef.setVisibility(true);
	frmIBEditMyAccountsConfirmComplete.hbxOTPsnt.setVisibility(true);
	frmIBEditMyAccountsConfirmComplete.txtBxOTP.setFocus(true);
	frmIBEditMyAccountsConfirmComplete.btnEditEStatement.setVisibility(false);
	var inputParam = [];
	inputParam["crmId"] = gblcrmId;
	showLoadingScreenPopup();
	invokeServiceSecureAsync("tokenSwitching", inputParam, tokenCheckEditBeneficiaryIBCallback);
}

function tokenCheckEditBeneficiaryIBCallback(status,callbackResponse){
	if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            if (callbackResponse["deviceFlag"].length == 0) {
                generateOTPForEditSavingCareIB();
            }
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];

                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN" ) && tokenStatus=='02') {
                    gblTokenSwitchFlag = true;
                    IBTokenForEditBeneficiary();
                } else {
                    gblTokenSwitchFlag = false;
                    generateOTPForEditSavingCareIB();
                    gblTokenSwitchFlag = false;
                }
            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

function IBTokenForEditBeneficiary() {
    frmIBEditMyAccountsConfirmComplete.hbxOTPEntry.setVisibility(false);
    frmIBEditMyAccountsConfirmComplete.hbxOTPsnt.setVisibility(false);
    frmIBEditMyAccountsConfirmComplete.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
    frmIBEditMyAccountsConfirmComplete.hbxToken.setVisibility(true);
    frmIBEditMyAccountsConfirmComplete.hbxOTPRef.isVisible=false;
    dismissLoadingScreenPopup();
    frmIBEditMyAccountsConfirmComplete.tbxToken.text ="";
    frmIBEditMyAccountsConfirmComplete.tbxToken.setFocus(true);
}

function generateOTPForEditSavingCareIB()
{
		showLoadingScreenPopup();
		frmIBEditMyAccountsConfirmComplete.lblOTPinCurr.text = "";
        frmIBEditMyAccountsConfirmComplete.lblPlsReEnter.text = "";
		var inputParams = {};
		inputParams["Channel"] = "EditBeneficiary";
		inputParams["retryCounterRequestOTP"]=gblRetryCountRequestOTP;
	 	inputParams["locale"]=kony.i18n.getCurrentLocale();
	 
	 	//InputParms for activity logging
		inputParams["channelId"] = GLOBAL_IB_CHANNEL;
		inputParams["logLinkageId"] = "";
		inputParams["deviceNickName"] = "";
		inputParams["activityFlexValues2"] = "";
		
		var index = frmIBMyAccnts.segTMBAccntsList.selectedIndex[1];
		
		inputParams["accountNo"] = frmIBMyAccnts.segTMBAccntsList.data[index]["hiddenAccountNo"];
    	inputParams["productNameEN"] = frmIBMyAccnts.segTMBAccntsList.data[index]["hiddenProductNameEng"];
    	inputParams["productNameTH"] = frmIBMyAccnts.segTMBAccntsList.data[index]["hiddenProductNameThai"];
    	
		invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackgenerateOTPForEditSavingCareIB);
}

function callBackgenerateOTPForEditSavingCareIB(status,callBackResponse){
	if (status == 400) {
			if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
				//alert("Test1");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				//alert("Test2");
				dismissLoadingScreenPopup();
				showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
              }
			
			else if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}
			if (callBackResponse["opstatus"] == 0) {
			
			//Resetting the OTP screen
			frmIBEditMyAccountsConfirmComplete.hbxOtpBox.isVisible=true;
			frmIBEditMyAccountsConfirmComplete.hbxOTPEntry.setVisibility(true);
		    frmIBEditMyAccountsConfirmComplete.hbxOTPsnt.setVisibility(true);
		    frmIBEditMyAccountsConfirmComplete.lblkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		    frmIBEditMyAccountsConfirmComplete.hbxOTPRef.isVisible=true;
		    frmIBEditMyAccountsConfirmComplete.hbxToken.setVisibility(false);
		    gblTokenSwitchFlag=false;
		    dismissLoadingScreenPopup();
		    //frmIBEditMyAccountsConfirmComplete.tbxToken.text ="";
		    //frmIBEditMyAccountsConfirmComplete.tbxToken.setFocus(true);
    		//Finish reset here

			//gblOTPdisabletime = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]) * 100;
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			kony.timer.schedule("OtpTimerEditBeneficiary", IBOTPTimercallbackEditBeneficiary, reqOtpTimer, false);
			   
        	    	
          	gblOTPReqLimit=gblOTPReqLimit+1;
          //	setTimeout('OTPTimercallbackOpenAccount()',reqOtpTimer);

			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
						refVal=callBackResponse["Collection1"][d]["ValueString"];
						break;
					}
				}
					
				gblestmt="verOTP";	 			
		   		frmIBEditMyAccountsConfirmComplete.btnOTPReq.skin = btnIBREQotp;
            	frmIBEditMyAccountsConfirmComplete.btnOTPReq.focusSkin = btnIBREQotp;
            	frmIBEditMyAccountsConfirmComplete.btnOTPReq.onClick = ""; 
            	frmIBEditMyAccountsConfirmComplete.lblBankRefVal.text = refVal;			
				//curr_form = kony.application.getCurrentForm();
				frmIBEditMyAccountsConfirmComplete.txtBxOTP.maxTextLength = gblOTPLENGTH;
				frmIBEditMyAccountsConfirmComplete.txtBxOTP.text = "";
				frmIBEditMyAccountsConfirmComplete.lblOTPMblNum.text =" " + maskingIB(gblPHONENUMBER);
				dismissLoadingScreenPopup();
		} else {
			dismissLoadingScreenPopup();
			alert("Error " + callBackResponse["errMsg"]);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			alert("Error");
		}
	}
}

function IBOTPTimercallbackEditBeneficiary(){
	frmIBEditMyAccountsConfirmComplete.btnOTPReq.skin = btnIBREQotpFocus;
	frmIBEditMyAccountsConfirmComplete.btnOTPReq.focusSkin = btnIBREQotpFocus;
	frmIBEditMyAccountsConfirmComplete.btnOTPReq.onClick = generateOTPForEditSavingCareIB;
	
	try {
		kony.timer.cancel("OtpTimerEditBeneficiary");
	} catch (e) {
		
	}
}

function invokeEditBeneficiaryCompositeIB() {
	var inputparam = {};
	var otpPwd = "";
	if(gblTokenSwitchFlag){
		otpPwd = frmIBEditMyAccountsConfirmComplete.tbxToken.text;
	}else{
		otpPwd = frmIBEditMyAccountsConfirmComplete.txtBxOTP.text;
	}
	if("" == otpPwd || kony.os.toNumber(otpPwd)==null || otpPwd.length != 6){
		showAlert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"), kony.i18n.getLocalizedString("info"));
		return false;
	}
	showLoadingScreenPopup();
	inputparam["password"] = otpPwd
	inputparam["TokenSwitchFlag"] = gblTokenSwitchFlag;
    inputparam["verifyToken_loginModuleId"] = "IB_HWTKN";
    inputparam["verifyToken_userStoreId"] = "DefaultStore";
    inputparam["verifyToken_retryCounterVerifyAccessPin"] = "0";
    inputparam["verifyToken_retryCounterVerifyTransPwd"] = "0";
    inputparam["verifyToken_userId"] = gblUserName;
    
    //inputparam["verifyToken_password"] = Pwd;
    inputparam["verifyToken_sessionVal"] = "";
    inputparam["verifyToken_segmentId"] = "segmentId";
    inputparam["verifyToken_segmentIdVal"] = "MIB";
    inputparam["verifyToken_channel"] = "Internet Banking";
    
    //OTP
    //inputparam["verifyPwd_TokenSwitchFlag"] = gblTokenSwitchFlag;
    inputparam["verifyPwd_retryCounterVerifyOTP"] = "0",
    inputparam["verifyPwd_userId"] = gblUserName,
    inputparam["verifyPwd_userStoreId"] = "DefaultStore",
    inputparam["verifyPwd_loginModuleId"] = "IBSMSOTP",
    //inputparam["verifyPwd_password"] = Pwd,
    inputparam["verifyPwd_segmentId"] = "MIB"
    //Mobile Pwd 
    inputparam["verifyPwdMB_loginModuleId"] = "MB_TxPwd";
    inputparam["verifyPwdMB_retryCounterVerifyAccessPin"] = "0";
    inputparam["verifyPwdMB_retryCounterVerifyTransPwd"] = "0";
	
	inputparam["openActModule"]="EditBeneficiary";
	
	var fullNameList = "";
	var relationList = "";
	var benefitPercentList = "";
	if(frmIBEditMyAccounts.btnSpecified.skin == "btnIBTab4LeftFocus"){
		for(var i in gblBeneficaryIndexArray){
			var indexWidget = gblBeneficaryIndexArray[i];
			if(frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim() != "0") {
				
				var fullname = frmIBEditMyAccounts["txtBefFirstName"+indexWidget].text.trim()+" "+frmIBEditMyAccounts["txtBefLastName"+indexWidget].text.trim();
				if(fullNameList == "")
					fullNameList = fullname;
				else
					fullNameList = fullNameList +"," + fullname;
				var relation = frmIBEditMyAccounts["cbxBefRelation"+indexWidget].selectedKey;
				if(relationList == "")
					relationList = relation;
				else
					relationList = relationList +","+ relation;
				var benefit = frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim();
				if(benefitPercentList == "")
					benefitPercentList = benefit;
				else	
					benefitPercentList = benefitPercentList +","+ benefit
			}
		}
	}
	inputparam["beneficiaryfullNames"]=fullNameList;
	inputparam["beneficiarypercentage"]=benefitPercentList;
	inputparam["beneficiaryrelationship"]=relationList;
	
	inputparam["inqToActId"]=frmIBMyAccnts.segTMBAccntsList.selectedItems[0].hiddenAccountNo; //14 digit account Id
	
	inputparam["notificationAdd_channel"] = "IB";
    inputparam["notificationAdd_channelId"] = "Internet Banking";
    inputparam["notificationAdd_deliveryMethod"] = "Email";
    inputparam["notificationAdd_notificationType"] = "Email";
    inputparam["notificationAdd_noSendInd"] = "0";
    inputparam["notificationAdd_Locale"] = kony.i18n.getCurrentLocale();
	inputparam["notificationAdd_notificationSubject"] = "";
	inputparam["notificationAdd_notificationContent"] = "";
	inputparam["notificationAdd_custName"] = gblCustomerName;
	inputparam["notificationAdd_custNameTH"] = gblCustomerName;
	inputparam["notificationAdd_customerName"] = gblCustomerName;
    inputparam["notificationAdd_appID"] = appConfig.appId;
    inputparam["notificationAdd_nickname"] = frmIBEditMyAccounts.txtbxEditAccntNN.text;
    var newNickName = frmIBEditMyAccounts.txtbxEditAccntNN.text;
	var oldNickName = frmIBEditMyAccounts.lblViewNickNameHdr.text;
	if(newNickName != oldNickName ){
			inputparam["gblIBEditNickNameValue"] =   oldNickName + newNickName ;
		}
	invokeServiceSecureAsync("openActConfirmCompositeService", inputparam, callBackEditBeneficiaryCompositeIB);
}

function callBackEditBeneficiaryCompositeIB(status,resulttable){
	if(status==400){
		dismissLoadingScreenPopup();
		if(resulttable["opstatus"]==0){
			if(frmIBEditMyAccounts.txtbxEditAccntNN.text != frmIBEditMyAccounts.lblViewNickNameHdr.text){
				showfrmViewAccntIB();
			}else{
				if(gblEditClickFromAccountDetails){
					accountsummaryLangToggleIB();
				}else{
					onClickSegmentMyAccountsTMBIB();
				}
			}
				
		}else if (resulttable["opstatus"] == 8005) {
            if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                frmIBEditMyAccountsConfirmComplete.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBEditMyAccountsConfirmComplete.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                handleOTPLockedIB(resulttable);
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                alert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                alert("" + resulttable["errMsg"]);
                return false;
            }
            else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
              }
            else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                alert("" + resulttable["errMsg"]);
                return false;
            }else{
              	alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
                return false;
            }
        }else{
        	alert(kony.i18n.getLocalizedString("keyBeneficiaryEditServiceFailed"));
        	showfrmViewAccntIB();
        	return false;
        }
	}
}

function changeMasterdatainRelationIBEditMyAccounts(){
	var currLocale = kony.i18n.getCurrentLocale();
	if(gblRelationshipData != null && gblRelationshipData.length > 0)
	{
		if (currLocale == "en_US") {
			gblRelationshipData[0].splice(0, 1, ["P",kony.i18n.getLocalizedString("keyBeneficiaryPlsSel")])
	   		masterData = gblRelationshipData[0];
	   	}else{
	   		gblRelationshipData[1].splice(0, 1, ["P",kony.i18n.getLocalizedString("keyBeneficiaryPlsSel")])
	   		masterData = gblRelationshipData[1];
	   	}
	   	for(var i=0;i < gblBeneficiaryCount ; i++){
			if( frmIBEditMyAccounts["cbxBefRelation" + i] != undefined){
				var currentVal = frmIBEditMyAccounts["cbxBefRelation" + i].selectedKey;
				frmIBEditMyAccounts["cbxBefRelation" + i].masterData = masterData;
				frmIBEditMyAccounts["cbxBefRelation" + i].selectedKey = currentVal;
			}
		}
	}
}

function setEditBenificiariesConfirmCompleteSeg(){
	var relationValue="";
	
	var segBeneficiarydata=[];
	for(var i in gblBeneficaryIndexArray){
		var indexWidget = gblBeneficaryIndexArray[i];
		relationValue = frmIBEditMyAccountsConfirmComplete.segBeneficiaryData.data[i].lblSegBeneficiaryRelationValue;
		
		var tempSegmentRecord = {
			"lblSegBeneficiaryNameValue":frmIBEditMyAccounts["txtBefFirstName"+indexWidget].text.trim()+" "+frmIBEditMyAccounts["txtBefLastName"+indexWidget].text.trim(),
			"lblSegBeneficiaryRelationValue":changeEditRelationTab(relationValue),
			"lblSegBeneficiaryBenefitValue":frmIBEditMyAccounts["txtBefBenefit"+indexWidget].text.trim()
		};
		segBeneficiarydata.push(tempSegmentRecord);
	}
	frmIBEditMyAccountsConfirmComplete.segBeneficiaryData.setData(segBeneficiarydata);
}

function getRelationIndex(tempData,currentRelValue){
	var result = -1;
	for( var j = 0 ; j < tempData.length; j++ ) {
	    if( tempData[j][1] == currentRelValue ) {
	        result = j;
	    }
	}
	return result;
}

function changeEditRelationTab(relationValue){
	var locale = kony.i18n.getCurrentLocale();
	kony.print(relationValue);
		var curIdx = getRelationIndex(gblRelationshipData[0],relationValue);
		kony.print(curIdx);
		var curIdx2 = getRelationIndex(gblRelationshipData[1],relationValue);
		kony.print(curIdx2);
		if( curIdx > 0 && locale == "th_TH") {
			newRelValue = gblRelationshipData[1][curIdx];
			return newRelValue[1];
		} else if( curIdx2 > 0 && locale == "en_US"){
			newRelValue = gblRelationshipData[0][curIdx2];
			return newRelValue[1];
		}
		return relationValue;
}

function handleEditSavingCareIBMyAccount(hasData){
	frmIBEditMyAccounts.hbxViewBeneficiary.isVisible=true;
	if(hasData){
		frmIBEditMyAccounts.hbxBeneficiaryText.isVisible=true;
		frmIBEditMyAccounts.hbxBeneficiaryNotSpecified.isVisible=false;
		frmIBEditMyAccounts.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesSpecify");
		frmIBEditMyAccounts.lblSegBeneficiaryName.text=kony.i18n.getLocalizedString("keyBeneficiaryName");
		frmIBEditMyAccounts.lblSegBeneficiaryRelation.text=kony.i18n.getLocalizedString("keyBeneficiaryRelationship");
		frmIBEditMyAccounts.lblSegBeneficiaryBenefit.text=kony.i18n.getLocalizedString("keyBeneficiaryBenefit");
		var locale = kony.i18n.getCurrentLocale();
		var relationValue="";
		
		var segBeneficiarydata=[];
		for (var i=0;i<gblBeneficiaryData.length;i++){
			if("th_TH" == locale){
				relationValue = gblBeneficiaryData[i].relationTH;
			}else{
				relationValue = gblBeneficiaryData[i].relationEN;
			}
			var tempSegmentRecord = {
				"lblSegBeneficiaryNameValue":gblBeneficiaryData[i].fullName,
				"lblSegBeneficiaryRelationValue":relationValue,
				"lblSegBeneficiaryBenefitValue":gblBeneficiaryData[i].percentValue,
				"hdnSegBeneficiaryRelationCode":gblBeneficiaryData[i].relationCode
			};
			segBeneficiarydata.push(tempSegmentRecord);
		}
		frmIBEditMyAccounts.segBeneficiaryData.setData(segBeneficiarydata);
	}else{
		frmIBEditMyAccounts.hbxBeneficiaryText.isVisible=false;
		frmIBEditMyAccounts.hbxBeneficiaryNotSpecified.isVisible=true;
		frmIBEditMyAccounts.segBeneficiaryData.setData([]);		
		if(kony.string.equalsIgnoreCase(frmIBMyAccnts.segTMBAccntsList.selectedItems[0]["hiddenOpeningMethod"],"BRN") && kony.string.equalsIgnoreCase(gblNeverSetBeneficiary, "Y")){
			frmIBEditMyAccounts.lblBeneficiaryNotSpecified.text=kony.i18n.getLocalizedString("keyBeneficiaryNoDataBranchEdit");
			frmIBEditMyAccounts.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"));
		}else{			
			frmIBEditMyAccounts.lblBeneficiaryNotSpecified.text=kony.i18n.getLocalizedString("keyBeneficiariesNotSpecifyDetail");
			frmIBEditMyAccounts.lblBeneficiarySpecify.text=appendColon(kony.i18n.getLocalizedString("keyBeneficiaries"))+" "+kony.i18n.getLocalizedString("keyBeneficiariesNotSpecify");
		}
	}	
}