function onClickAddToMyBills() {
    
    //Purging BillerFlowCache
    removeCache("BillerFlowCache");
  
	if(GblBillTopFlag){
		popupAddToMyBills.lblHdrAddToMyRecipient.text = kony.i18n.getLocalizedString("MIB_BPAddBill");
	}else{
		popupAddToMyBills.lblHdrAddToMyRecipient.text = kony.i18n.getLocalizedString("MIB_TPAddBill");
	}
	var billerNameCompCode = frmBillPaymentComplete.lblBillerNameCompCode.text;
	var ref1Val = frmBillPaymentComplete.lblRef1ValueMasked.text;
	var imgBillerLogo = frmBillPaymentComplete.imgBillerPic.src;
	var ref2Val = frmBillPaymentComplete.lblRef2Value.text;
	var ref1TitleVal = frmBillPaymentComplete.lblRef1.text;
	var ref2TitleVal = frmBillPaymentComplete.lblRef2.text;
	
	// Biller name and CompCode
	popupAddToMyBills.lblBillerNameCompCode.text = billerNameCompCode;
    popupAddToMyBills.imgBillerLogo.src = imgBillerLogo;
    
    // Nickname
    popupAddToMyBills.txtBillerNickName.text = "";
    popupAddToMyBills.txtBillerNickName.setFocus(true);
    popupAddToMyBills.lineNickname.skin = "linePopupBlack";
    popupAddToMyBills.lblBillerNicknameTitle.text = kony.i18n.getLocalizedString("KeyNickNameBiller");
    
    // Ref 1.
    popupAddToMyBills.lblBillerRef1.text = ref1Val;
    popupAddToMyBills.lblBillerRef1Title.text = removeColonFromEnd(ref1TitleVal);
    popupAddToMyBills.lineRef1.skin  = "linePopupBlack";
    if(isNotBlank(ref1Val)){
    	popupAddToMyBills.lineRef1.skin  = "lineLightBlue";
    }

    // Ref 2.
	displayBillerRef2(false);
	gblIsRef2RequiredMB = "N";
	if(gblreccuringDisableAdd == "Y"){
		displayBillerRef2(true);
		popupAddToMyBills.lblBillerRef2.text = ref2Val;
	    popupAddToMyBills.lblBillerRef2Title.text = removeColonFromEnd(ref2TitleVal);
	    popupAddToMyBills.lineRef2.skin  = "linePopupBlack";
		if(isNotBlank(ref2Val)){
	    	popupAddToMyBills.lineRef2.skin  = "lineLightBlue";
	    }
	    gblIsRef2RequiredMB = "Y";
	    if(gblDeviceInfo["name"] == "iPhone"){
			popupAddToMyBills.hbxAddToMyBills.margin = [0,42,0,5];
		}else{
			popupAddToMyBills.hbxAddToMyBills.margin = [0,19,0,4];
		}
	}else{
		if(gblDeviceInfo["name"] == "iPhone"){
			popupAddToMyBills.hbxAddToMyBills.margin = [0,62,0,5];
		}else{
			popupAddToMyBills.hbxAddToMyBills.margin = [0,39,0,4];
		}
		if(isNotBlank(ref1Val)){
			popupAddToMyBills.lineRef2.skin  = "lineLightBlue";
		}else{
			popupAddToMyBills.lineRef2.skin  = "linePopupBlack";
		}
	}
	popupAddToMyBills.show();
}

function displayBillerRef2(isShow){
	popupAddToMyBills.hbxRef2.setVisibility(isShow);
	popupAddToMyBills.lineRef1.setVisibility(isShow);
}

function onDoneAddToMyBillsInputBillerNickName() {
	if(!isNotBlank(popupAddToMyBills.txtBillerNickName.text)
		|| popupAddToMyBills.txtBillerNickName.text.length < 3){
		popupAddToMyBills.lineNickname.skin  = "linePopupBlack";
	}else{
		popupAddToMyBills.lineNickname.skin  = "lineLightBlue";
		popupAddToMyBills.txtBillerNickName.skin =  "txtBlueText171";
	}
}


function onClickSaveAddToMyBills() {
	showLoadingScreen();
	if(!validateAddToMyBillsFields()){
		dismissLoadingScreen();
		return false;
	}
	// add to my bill service
	var module = "";
	if(GblBillTopFlag)
		module = "AddBillersMB";
	else
		module = "AddTopupsMB";	
	callConfirmBillerAddService(module, false);
	//dismissLoadingScreen();
}

function callConfirmBillerAddService(module, spaflag) {
    var inputParam = {};
    spaFlag = spaflag;
    moduleName = module;
  	var Ref2="";
	if(popupAddToMyBills.lblBillerRef2.text!="Label"){
		Ref2=popupAddToMyBills.lblBillerRef2.text;
	}
	var Ref2Title="";
	if(popupAddToMyBills.lblBillerRef2Title.text!="Label"){
		Ref2Title=popupAddToMyBills.lblBillerRef2Title.text;
	}
    //Add Billers
    var billerDetails = "";
    billerDetails = popupAddToMyBills.txtBillerNickName.text + "##" + 
    				gblBillerId + "##" + 
    				gblCompCode + "##" + 
    				popupAddToMyBills.lblBillerRef1.text + "##" + 
    				Ref2+ "##" + 
			    	popupAddToMyBills.lblBillerNameCompCode.text + "##" + 
			    	popupAddToMyBills.lblBillerRef1Title.text + "##" + 
    				Ref2Title + "##" + 
			    	gblRef1LblEN + "##" + 
			    	gblRef1LblTH + "##" + 
			    	gblRef2LblEN + "##" + 
			    	gblRef2LblTH + "##" + 
			    	gblBillerCompCodeEN + "##" + 
			    	gblBillerCompCodeTH + "~";
    inputParam["billAdd_billerAddDetails"] = billerDetails;
    //Notification ADD
    var platformChannel = "";
    platformChannel = gblDeviceInfo.name;

    if (platformChannel == "thinclient") {
        inputParam["notificationAdd_channel"] = "IB";
        inputParam["notificationAdd_channelId"] = "Internet Banking";
    } else {
        inputParam["notificationAdd_channel"] = "MB";
        inputParam["notificationAdd_channelId"] = "Mobile Banking";
    }

    inputParam["notificationAdd_deliveryMethod"] = "Email";
    inputParam["notificationAdd_noSendInd"] = "0";

    inputParam["notificationAdd_notificationType"] = "Email";
    inputParam["notificationAdd_Locale"] = kony.i18n.getCurrentLocale();


    inputParam["notificationAdd_customerName"] = gblCustomerName;
    inputParam["notificationAdd_appID"] = appConfig.appId;

    inputParam["notificationAdd_source"] = "addBiller";


    if (moduleName == "AddBillersIB" || moduleName == "AddBillersMB")
        inputParam["activityLog_activityTypeID"] = "061";
    else if (moduleName == "AddTopupsIB" || moduleName == "AddTopupsMB"){
        inputParam["activityLog_activityTypeID"] = "062";
        inputParam["notificationAdd_source"] = "addTopup";
    }    

    inputParam["activityLog_errorCd"] = "";
    inputParam["activityLog_activityFlexValues1"] = "Add";
    if (platformChannel == "thinclient"){
		inputParam["activityLog_channelId"] = GLOBAL_IB_CHANNEL;
    }else{
    	inputParam["activityLog_channelId"] = GLOBAL_MB_CHANNEL;
    }
    inputParam["activityLog_logLinkageId"] = "";
    //showLoadingScreen();
       
    invokeServiceSecureAsync("ConfirmBillAddService", inputParam, callBackConfirmBillAddService);
}

function callBackConfirmBillAddService(status, callBackResponse) {
	dismissLoadingScreen();
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
        	gblBillerID = callBackResponse["custPayID"];
           	if (moduleName == "AddBillersMB" || moduleName == "AddTopupsMB") {
                displayAddToMyBillsDoneMsg();
                gblFlagConfirmDataAddedMB = 0;
          	}
        }else {
            if (callBackResponse["errCode"] == "MAXBILLERS") {
            	showAlert(kony.i18n.getLocalizedString("MIB_BPErrMaxBiller"), kony.i18n.getLocalizedString("info"));
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
        }
    } else if (status == 300) {
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}

function validateAddToMyBillsFields(){
	var billerNickName = popupAddToMyBills.txtBillerNickName.text;
	if(!isNotBlank(billerNickName) || billerNickName.trim() == kony.i18n.getLocalizedString("keylblNickname")){
		TextBoxErrorSkin(popupAddToMyBills.txtBillerNickName, txtErrorBG);
		showAlertRcMB(kony.i18n.getLocalizedString("MIB_BPErr_NoAccNickname"), kony.i18n.getLocalizedString("info"), "info");
		return false;
	}
	if(billerNickName.length < 3 || billerNickName.length > 20 || !NickNameValid(billerNickName)){
		TextBoxErrorSkin(popupAddToMyBills.txtBillerNickName, txtErrorBG);
		showAlertRcMB(kony.i18n.getLocalizedString("MIB_BPkeyInvalidNickName"), kony.i18n.getLocalizedString("info"), "info");
		return false;
	}
	if(!checkDuplicateBillerNickCheck(billerNickName)){
		TextBoxErrorSkin(popupAddToMyBills.txtBillerNickName, txtErrorBG);
		showAlertRcMB(kony.i18n.getLocalizedString("MIB_BPErr_alert_correctnickname"), kony.i18n.getLocalizedString("info"), "info");
		return false;
	}
	if(!checkMaxBillerCountAddToMyBills()){
		showAlertRcMB(kony.i18n.getLocalizedString("MIB_BPErrMaxBiller"), kony.i18n.getLocalizedString("info"), "info");
		return false;
	}
	return true;
}

function displayAddToMyBillsDoneMsg(){
	popupAddToMyBills.dismiss();
	popGeneralMsg.lblMsg.text = kony.i18n.getLocalizedString("MIB_BPAdd_Complete");
	popGeneralMsg.btnClose.text = kony.i18n.getLocalizedString("keyOK");
	popGeneralMsg.btnClose.onClick = closePopGeneralMsg;
	frmBillPaymentComplete.hbxAddToMyBills.setVisibility(false);
	popGeneralMsg.show();
}

function closePopGeneralMsg(){
	popGeneralMsg.dismiss();
}


function checkMaxBillerCountAddToMyBills() {
	var maxCount = kony.os.toNumber(GLOBAL_MAX_BILL_COUNT);
	if (gblMyBillList.length == 0) {
		return true;
	} else if (gblMyBillList.length < maxCount){
		return true
	} else if (gblMyBillList.length >= maxCount){
		return false;
	}
}

function checkDuplicateBillerNickCheck(nickname) {
    if (gblMyBillList != null) {
        for (var i = 0; i < gblMyBillList.length; i++) {
            val = gblMyBillList[i]["BillerNickName"];
            if (kony.string.equals(nickname.trim(), val)) {
                return false;
            }
        }
    }
    return true;
}

function onClickVbxBillerNickName(){
	popupAddToMyBills.txtBillerNickName.setFocus(true);
}