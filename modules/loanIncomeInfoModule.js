function frmLoanIncomeInfo_init(){
  frmLoanIncomeInfo.preShow = frmLoanIncomeInfo_preShow;
  frmLoanIncomeInfo.postShow = frmLoanIncomeInfo_postShow;
  frmLoanIncomeInfo.btnBackPersonalInformation.onClick = frmLoanIncomeInfo_btnBackPersonalInformation_onClick;
  frmLoanIncomeInfo.onDeviceBack=disableBackButton;
  //Loan Income fields - Employee
  frmLoanIncomeInfo.flxBankAccount.onClick = frmLoanIncomeInfo_flxBankAccount_onClick;
  frmLoanIncomeInfo.flxAccountNumber.onClick = frmLoanIncomeInfo_flxAccountNumber_onClick;
  frmLoanIncomeInfo.flxFixedIncome.onClick = frmLoanIncomeInfo_flxFixedIncome_onClick;
  frmLoanIncomeInfo.flxCOLAIncome.onClick = frmLoanIncomeInfo_flxCOLAIncome_onClick;
  frmLoanIncomeInfo.flxBonusYearly.onClick = frmLoanIncomeInfo_flxBonusYearly_onClick;
  frmLoanIncomeInfo.flxOtherIncome.onClick = frmLoanIncomeInfo_flxOtherIncome_onClick;
  frmLoanIncomeInfo.btnSelfEmployed.onClick = frmLoanIncomeInfo_btnSelfEmployed_onClick;
  //Account Number
  frmLoanIncomeInfo.txtFieldAccountNumber.onDone = frmLoanIncomeInfo_AccountNumber_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldAccountNumber.onTextChange = onTextChangeBankAccountNumber;
  frmLoanIncomeInfo.txtFieldAccountNumber.onEndEditing = frmLoanIncomeInfo_AccountNumber_onDone;
  //Fixed Income
  frmLoanIncomeInfo.txtFieldFixedIncome.onDone = frmLoanIncomeInfo_txtNumberField_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldFixedIncome.onTextChange = frmLoanIncomeInfo_txtNumberField_onTextChange;
  frmLoanIncomeInfo.txtFieldFixedIncome.onEndEditing = frmLoanIncomeInfo_txtNumberField_onDone;
  //COLA Income
  frmLoanIncomeInfo.txtFieldCOLAIncome.onDone = frmLoanIncomeInfo_txtNumberField_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldCOLAIncome.onTextChange = frmLoanIncomeInfo_txtNumberField_onTextChange;
  frmLoanIncomeInfo.txtFieldCOLAIncome.onEndEditing = frmLoanIncomeInfo_txtNumberField_onDone;
  //Bonus Yearly
  frmLoanIncomeInfo.txtFieldBonusYearly.onDone = frmLoanIncomeInfo_txtNumberField_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldBonusYearly.onTextChange = frmLoanIncomeInfo_txtNumberField_onTextChange;
  frmLoanIncomeInfo.txtFieldBonusYearly.onEndEditing = frmLoanIncomeInfo_txtNumberField_onDone;
  //Other Income
  frmLoanIncomeInfo.txtFieldOtherIncome.onDone = frmLoanIncomeInfo_txtNumberField_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldOtherIncome.onTextChange = frmLoanIncomeInfo_txtNumberField_onTextChange;
  frmLoanIncomeInfo.txtFieldOtherIncome.onEndEditing = frmLoanIncomeInfo_txtNumberField_onDone;
  //NCP Model
  frmLoanIncomeInfo.imgChkAccept.onTouchEnd = acceptNCP;
  frmLoanIncomeInfo.imgchkNotAccept.onTouchEnd = notAcceptNCP;
  frmLoanIncomeInfo.btnFacilityInfoScrore.onClick = showHideNCPHelpGuide;
  
  //Loan Income fields - Self Employee
  frmLoanIncomeInfo.flxBankAccountSelf.onClick = frmLoanIncomeInfo_flxBankAccountSelf_onClick;
  frmLoanIncomeInfo.flxAccountNumberSelf.onClick = frmLoanIncomeInfo_flxAccountNumberSelf_onClick;
  frmLoanIncomeInfo.flxDeclaredIncomeSelf.onClick = frmLoanIncomeInfo_flxDeclaredIncomeSelf_onClick;
  frmLoanIncomeInfo.flxCashInflowSelf.onClick = frmLoanIncomeInfo_flxCashInflowSelf_onClick;
  frmLoanIncomeInfo.flxShareHolderSelf.onClick = frmLoanIncomeInfo_flxShareHolderSelf_onClick;
  frmLoanIncomeInfo.btnNext.onClick = frmLoanIncomeInfo_btnNext_onClick;
  frmLoanIncomeInfo.btnEmployeeDeselect.onClick = frmLoanIncomeInfo_btnEmployeeDeselect_onClick;
  
  //Account Number
  frmLoanIncomeInfo.txtFieldAccountNumberSelf.onDone = frmLoanIncomeInfo_AccountNumber_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldAccountNumberSelf.onTextChange = onTextChangeBankAccountNumber;
  frmLoanIncomeInfo.txtFieldAccountNumberSelf.onEndEditing = frmLoanIncomeInfo_AccountNumber_onDone;
  //Declared Income
  frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.onDone = frmLoanIncomeInfo_txtNumberField_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.onTextChange = frmLoanIncomeInfo_txtNumberField_onTextChange;
  frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.onEndEditing = frmLoanIncomeInfo_txtNumberField_onDone;
  //CashFlow
  frmLoanIncomeInfo.txtFieldCashFlow.onDone = frmLoanIncomeInfo_txtNumberField_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldCashFlow.onTextChange = frmLoanIncomeInfo_txtNumberField_onTextChange;
  frmLoanIncomeInfo.txtFieldCashFlow.onEndEditing = frmLoanIncomeInfo_txtNumberField_onDone;
  //Share Holder
  frmLoanIncomeInfo.txtFieldShareHolderSelf.onDone = frmLoanIncomeInfo_txtNumberField_onDone_withFocus;   
  frmLoanIncomeInfo.txtFieldShareHolderSelf.onTextChange = frmLoanIncomeInfo_txtNumberField_onTextChange;
  frmLoanIncomeInfo.txtFieldShareHolderSelf.onEndEditing = frmLoanIncomeInfo_txtNumberField_onDone;
  //NCP Model
  frmLoanIncomeInfo.imgChkAcceptSelf.onTouchEnd = acceptNCP;
  frmLoanIncomeInfo.imgchkNotAcceptSelf.onTouchEnd = notAcceptNCP;
  frmLoanIncomeInfo.btnFacilityInfoScroreSelf.onClick = showHideNCPHelpGuide;
  //Loan Income PopupClose Button
  frmLoanIncomeInfo.BtnPopUpClosePopUp.onClick=frmLoanIncomeInfo_BtnPopUpClosePopUp_onClick;
  frmLoanIncomeInfo.btnCloseNCB.onClick = showHideNCPHelpGuide;
}
function frmLoanIncomeInfo_preShow(){
  showLoadingScreen();
  changeStatusBarColor();
  frmLoanIncomeInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("Income_Info_title");
  frmLoanIncomeInfo.lblPersonalInfo.text = kony.i18n.getLocalizedString("Income_Info_title");
  frmLoanIncomeInfo.btnEmployee.text = kony.i18n.getLocalizedString("Employee_label");  
  frmLoanIncomeInfo.btnSelfEmployed.text = kony.i18n.getLocalizedString("Self_Employed_label");
  frmLoanIncomeInfo.lblBankAccount.text = kony.i18n.getLocalizedString("Bank_Account_textbox");
  frmLoanIncomeInfo.lblBankAccountHeader.text = kony.i18n.getLocalizedString("Bank_Account_textbox");
  frmLoanIncomeInfo.lblAccountNumber.text = kony.i18n.getLocalizedString("Account_No_textbox");
  frmLoanIncomeInfo.lblAccountNumberHeader.text = kony.i18n.getLocalizedString("Account_No_textbox");
  frmLoanIncomeInfo.lblFixedIncome.text = kony.i18n.getLocalizedString("Fixed_Income_textbox");
  frmLoanIncomeInfo.lblFixedIncomeHeader.text = kony.i18n.getLocalizedString("Fixed_Income_textbox");
  frmLoanIncomeInfo.lblCOLAIncome.text = kony.i18n.getLocalizedString("COLA_textbox");
  frmLoanIncomeInfo.lblCOLAIncomeHeader.text = kony.i18n.getLocalizedString("COLA_textbox");
  frmLoanIncomeInfo.lblBonusYearly.text = kony.i18n.getLocalizedString("Bonus_Yearly_textbox");
  frmLoanIncomeInfo.lblBonusYearlyHeader.text = kony.i18n.getLocalizedString("Bonus_Yearly_textbox");
  frmLoanIncomeInfo.lblOtherIncome.text = kony.i18n.getLocalizedString("Other_Income_textbox");
  frmLoanIncomeInfo.lblOtherIncomeHeader.text = kony.i18n.getLocalizedString("Other_Income_textbox");
  frmLoanIncomeInfo.btnEmployeeDeselect.text = kony.i18n.getLocalizedString("Employee_label");
  frmLoanIncomeInfo.btnSelfEmployeeSelect.text = kony.i18n.getLocalizedString("Self_Employed_label");
  
  frmLoanIncomeInfo.lblBankAccountSelfEmploy.text = kony.i18n.getLocalizedString("Bank_Account_textbox");
  frmLoanIncomeInfo.lblBankAccountHeaderSelfEmploy.text = kony.i18n.getLocalizedString("Bank_Account_textbox");
  frmLoanIncomeInfo.lblAccountNumberSelf.text = kony.i18n.getLocalizedString("Account_No_textbox");
  frmLoanIncomeInfo.lblAccountNumberHeaderSelf.text = kony.i18n.getLocalizedString("Account_No_textbox");
  frmLoanIncomeInfo.lblDeclaredIncomeSelf.text = kony.i18n.getLocalizedString("Declared_Income_textbox");
  frmLoanIncomeInfo.lblDeclaredIncomeHeaderSelf.text = kony.i18n.getLocalizedString("Declared_Income_textbox");
  frmLoanIncomeInfo.lblCashFlowSelf.text = kony.i18n.getLocalizedString("Average_Cash_Inflow_textbox");
  frmLoanIncomeInfo.lblCashFlowHeaderSelf.text = kony.i18n.getLocalizedString("Average_Cash_Inflow_textbox");
  frmLoanIncomeInfo.lblShareHolderSelf.text = kony.i18n.getLocalizedString("Shareholder_Percentage_textbox");
  frmLoanIncomeInfo.lblSahreHolderHeaderSelf.text = kony.i18n.getLocalizedString("Shareholder_Percentage_textbox");
  frmLoanIncomeInfo.btnNext.text = kony.i18n.getLocalizedString("SaveandContinue");
  frmLoanIncomeInfo.lblBankPopup.text = kony.i18n.getLocalizedString("Select_Bank_Title");
  
  frmLoanIncomeInfo.lblCcScoring.text = kony.i18n.getLocalizedString("NCB_Model_label");
  frmLoanIncomeInfo.LblScore1.text = kony.i18n.getLocalizedString("NCB_Model_Accept");
  frmLoanIncomeInfo.lblSc2.text = kony.i18n.getLocalizedString("NCB_Model_NotAccept");
  frmLoanIncomeInfo.lblCcScoringSelf.text = kony.i18n.getLocalizedString("NCB_Model_label");
  frmLoanIncomeInfo.LblScoreSelf.text = kony.i18n.getLocalizedString("NCB_Model_Accept");
  frmLoanIncomeInfo.lblScSelf.text = kony.i18n.getLocalizedString("NCB_Model_NotAccept");
  frmLoanIncomeInfo.txtAreaNCBDesc.text = kony.i18n.getLocalizedString("NCBModelHelp_Content");
  frmLoanIncomeInfo.lblHeadingNCB.text = kony.i18n.getLocalizedString("NCBModelHelp_Title");
  frmLoanIncomeInfo.lblCcScoreHelp.text = kony.i18n.getLocalizedString("NCB_Model_help");
  frmLoanIncomeInfo.lblCcScoreHelpSelf.text = kony.i18n.getLocalizedString("NCB_Model_help");
  setupButtonAndBullet();
}
function frmLoanIncomeInfo_postShow(){
  dismissLoadingScreen();
}
function frmLoanIncomeInfo_btnBackPersonalInformation_onClick(){
  showLoadingScreen();
  if(typeof(gblLoanIncomeInformation["txtFieldAccountNumber"]) != "undefined"
    || typeof(gblLoanIncomeInformation["txtFieldAccountNumberSelf"]) != "undefined"){
    gblPersonalInfo.saveIncome = true;
  }
  if(typeof(gblIncomeInvalidField.skin) != "undefined"){
      gblIncomeInvalidField.parent.skin = "lFboxLoan";
  }
  showfrmLoanApplication();
}
function frmLoanIncomeInfo_btnEmployeeDeselect_onClick(){
  frmLoanIncomeInfo.lblEmploymentStatus.text = kony.i18n.getLocalizedString("Employee_label");
  if(gblChosenTab == "" || gblChosenTab == "01"){
    if(frmLoanIncomeInfo.flxMainSelfEmployed.isVisible){
      if(typeof(gblIncomeInvalidField.skin) != "undefined"){
        gblIncomeInvalidField.parent.skin = "lFboxLoan";
      }
      clearDataSelected();
      gblLoanIncomeInformation["incomeBankNameTmp"] = "";
      gblPersonalInfo.saveIncome = false;
      setupButtonAndBullet();
      frmLoanIncomeInfo.flxMainSelfEmployed.setVisibility(false);
      frmLoanIncomeInfo.flxMain.setVisibility(true);
    }
  }
}
function frmLoanIncomeInfo_btnSelfEmployed_onClick(){
    frmLoanIncomeInfo.lblSelfEmploymentStatus.text = kony.i18n.getLocalizedString("Self_Employed_label");
  if(gblChosenTab == "" || gblChosenTab == "02"){
    if(frmLoanIncomeInfo.flxMain.isVisible){
      if(typeof(gblIncomeInvalidField.skin) != "undefined"){
        gblIncomeInvalidField.parent.skin = "lFboxLoan";
      }
      clearDataSelected();
      gblLoanIncomeInformation["incomeBankNameTmp"] = "";
      gblPersonalInfo.saveIncome = false;
      setupButtonAndBullet();
      frmLoanIncomeInfo.flxMain.setVisibility(false);
      frmLoanIncomeInfo.flxMainSelfEmployed.setVisibility(true);
    }
  }
}
function frmLoanIncomeInfo_flxBankAccount_onClick(){
  frmLoanIncomeInfo.flxBankPopUp.setVisibility(true);
  loadSegmentBankList();
}
function displayTextBoxBankAccount(display){
  frmLoanIncomeInfo.flxSubBankAccount.setVisibility(!display);
  frmLoanIncomeInfo.lblBankAccountDesc.setVisibility(display);
  frmLoanIncomeInfo.lblBankAccountHeader.setVisibility(display);
}
function displayTextBoxAccountNumber(display){
  frmLoanIncomeInfo.subflxAccountNumber.setVisibility(!display);
  frmLoanIncomeInfo.lblAccountNumberHeader.setVisibility(display);
  frmLoanIncomeInfo.txtFieldAccountNumber.setVisibility(display);
}
function displayTextBoxFixedIncome(display){
  frmLoanIncomeInfo.subflxFixedIncome.setVisibility(!display);
  frmLoanIncomeInfo.lblFixedIncomeHeader.setVisibility(display);
  frmLoanIncomeInfo.txtFieldFixedIncome.setVisibility(display);
}
function displayTextBoxCOLAIncome(display){
  frmLoanIncomeInfo.subflxCOLAIncome.setVisibility(!display);
  frmLoanIncomeInfo.lblCOLAIncomeHeader.setVisibility(display);
  frmLoanIncomeInfo.txtFieldCOLAIncome.setVisibility(display);
}
function displayTextBoxBonusYearly(display){
  frmLoanIncomeInfo.subflxBonusYearly.setVisibility(!display);
  frmLoanIncomeInfo.lblBonusYearlyHeader.setVisibility(display);
  frmLoanIncomeInfo.txtFieldBonusYearly.setVisibility(display);
}
function displayTextBoxOtherIncome(display){
  frmLoanIncomeInfo.subflxOtherIncome.setVisibility(!display);
  frmLoanIncomeInfo.lblOtherIncomeHeader.setVisibility(display);
  frmLoanIncomeInfo.txtFieldOtherIncome.setVisibility(display);
}
function setVisibilityAllEmployeeFieldBox(fieldClick){
  if(fieldClick != "AccountNumber" && !isNotBlank(frmLoanIncomeInfo.txtFieldAccountNumber.text)){
	displayTextBoxAccountNumber(false);
  }
  if(fieldClick != "FixedIncome" && !isNotBlank(frmLoanIncomeInfo.txtFieldFixedIncome.text)){
	displayTextBoxFixedIncome(false);
  }
  if(fieldClick != "COLAIncome" && !isNotBlank(frmLoanIncomeInfo.txtFieldCOLAIncome.text)){
	displayTextBoxCOLAIncome(false);
  }
  if(fieldClick != "BonusYearly" && !isNotBlank(frmLoanIncomeInfo.txtFieldBonusYearly.text)){
	displayTextBoxBonusYearly(false);
  }
  if(fieldClick != "OtherIncome" && !isNotBlank(frmLoanIncomeInfo.txtFieldOtherIncome.text)){
	displayTextBoxOtherIncome(false);
  }
}
function clearDataSelected(){
  gblPrevLen = 0;
  MAX_ACC_LEN_LIST = "";
  gblIncomeInvalidField = "";
  //Clear Employee data
  frmLoanIncomeInfo.lblBankAccountDesc.text = "";
  frmLoanIncomeInfo.txtFieldAccountNumber.text = "";
  frmLoanIncomeInfo.txtFieldFixedIncome.text = "";
  frmLoanIncomeInfo.txtFieldCOLAIncome.text = "";
  frmLoanIncomeInfo.txtFieldBonusYearly.text = "";
  frmLoanIncomeInfo.txtFieldOtherIncome.text = "";
  frmLoanIncomeInfo.lblConsentValue.text = "";
  frmLoanIncomeInfo.imgChkAccept.skin = "lblskinpigrayLineLone";
  frmLoanIncomeInfo.imgchkNotAccept.skin = "lblskinpigrayLineLone";
  frmLoanIncomeInfo.flxcircleNotAccept.skin = "slFboxGreaycircle";
  frmLoanIncomeInfo.flxcircleAccept.skin = "slFboxGreaycircle";
  
  //CLear Self Employee Data
  frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text = "";
  frmLoanIncomeInfo.txtFieldAccountNumberSelf.text = "";
  frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.text = "";
  frmLoanIncomeInfo.txtFieldCashFlow.text = "";
  frmLoanIncomeInfo.txtFieldShareHolderSelf.text = "";
  frmLoanIncomeInfo.lblConsentValueSelf.text = "";
  frmLoanIncomeInfo.imgChkAcceptSelf.skin = "lblskinpigrayLineLone";
  frmLoanIncomeInfo.imgchkNotAcceptSelf.skin = "lblskinpigrayLineLone";
  frmLoanIncomeInfo.flxcircleNotAcceptSelf.skin = "slFboxGreaycircle";
  frmLoanIncomeInfo.flxcircleAcceptSelf.skin = "slFboxGreaycircle";
  
  setVisibilityAllSelfEmployeeFieldBox("");
  setVisibilityAllEmployeeFieldBox("");
  displayTextBoxBankAccount(false);
  displayTextBoxSelfBankAccount(false);
}
function setupButtonAndBullet(){
  if(gblPersonalInfo.saveIncome !== undefined && gblPersonalInfo.saveIncome === true){
    frmLoanIncomeInfo.lblcircle.skin = "skinpiGreenLineLone";
//     frmLoanIncomeInfo.flxcircle.skin="flxGreenCircleLoan";
    frmLoanIncomeInfo.btnNext.skin = "btnBlue28pxLoan2";
    frmLoanIncomeInfo.btnNext.focusSkin="btnBlue200GreyBG";
    frmLoanIncomeInfo.btnNext.onClick = frmLoanIncomeInfo_btnNext_onClick;
  }else{
//     frmLoanIncomeInfo.flxcircle.skin="flexGreyBGLoan";
    frmLoanIncomeInfo.lblcircle.skin = "skinpiorangeLineLone";

    frmLoanIncomeInfo.btnNext.onClick = disableBackButton;
    frmLoanIncomeInfo.btnNext.skin = "btnGreyBGNoRoundLoan";
    frmLoanIncomeInfo.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
  }
}
function frmLoanIncomeInfo_flxAccountNumber_onClick(){
  setVisibilityAllEmployeeFieldBox("AccountNumber");
  if(frmLoanIncomeInfo.subflxAccountNumber.isVisible){
    displayTextBoxAccountNumber(true);
  }
  frmLoanIncomeInfo.txtFieldAccountNumber.setFocus(true);
}
function frmLoanIncomeInfo_flxFixedIncome_onClick(){
  setVisibilityAllEmployeeFieldBox("FixedIncome");
  if(frmLoanIncomeInfo.subflxFixedIncome.isVisible){
    displayTextBoxFixedIncome(true);
  }
  frmLoanIncomeInfo.txtFieldFixedIncome.setFocus(true);
}
function frmLoanIncomeInfo_flxCOLAIncome_onClick(){
  setVisibilityAllEmployeeFieldBox("COLAIncome");
  if(frmLoanIncomeInfo.subflxCOLAIncome.isVisible){
    displayTextBoxCOLAIncome(true);
  }
  frmLoanIncomeInfo.txtFieldCOLAIncome.setFocus(true);
}
function frmLoanIncomeInfo_flxBonusYearly_onClick(){
  setVisibilityAllEmployeeFieldBox("BonusYearly");
  if(frmLoanIncomeInfo.subflxBonusYearly.isVisible){
    displayTextBoxBonusYearly(true);
  }
  frmLoanIncomeInfo.txtFieldBonusYearly.setFocus(true);
}
function frmLoanIncomeInfo_flxOtherIncome_onClick(){
  setVisibilityAllEmployeeFieldBox("OtherIncome");
  if(frmLoanIncomeInfo.subflxOtherIncome.isVisible){
    displayTextBoxOtherIncome(true);
  }
  frmLoanIncomeInfo.txtFieldOtherIncome.setFocus(true);
}
function displayTextBoxSelfBankAccount(display){
  frmLoanIncomeInfo.subflxBankAccountSelf.setVisibility(!display);
  frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.setVisibility(display);
  frmLoanIncomeInfo.lblBankAccountHeaderSelfEmploy.setVisibility(display);
}
function displayTextBoxSelfAccountNumber(display){
  	frmLoanIncomeInfo.subflxAccountNumberSelf.setVisibility(!display);
    frmLoanIncomeInfo.lblAccountNumberHeaderSelf.setVisibility(display);
    frmLoanIncomeInfo.txtFieldAccountNumberSelf.setVisibility(display);
}
function displayTextBoxSelfDeclaredIncome(display){
    frmLoanIncomeInfo.subflxDeclaredIncomeSelf.setVisibility(!display);
    frmLoanIncomeInfo.lblDeclaredIncomeHeaderSelf.setVisibility(display);
    frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.setVisibility(display);
}
function displayTextBoxSelfCashInflow(display){
    frmLoanIncomeInfo.subflxCashInflowSelf.setVisibility(!display);
    frmLoanIncomeInfo.lblCashFlowHeaderSelf.setVisibility(display);
    frmLoanIncomeInfo.txtFieldCashFlow.setVisibility(display);
}
function displayTextBoxSelfShareHolder(display){
    frmLoanIncomeInfo.subflxShareHolderSelf.setVisibility(!display);
    frmLoanIncomeInfo.lblSahreHolderHeaderSelf.setVisibility(display);
    frmLoanIncomeInfo.txtFieldShareHolderSelf.setVisibility(display);
}

function setVisibilityAllSelfEmployeeFieldBox(fieldClick){
  if(fieldClick != "AccountNumber" && !isNotBlank(frmLoanIncomeInfo.txtFieldAccountNumberSelf.text)){
    displayTextBoxSelfAccountNumber(false);
  }
  if(fieldClick != "DeclaredIncome" && !isNotBlank(frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.text)){
    displayTextBoxSelfDeclaredIncome(false);
  }
  if(fieldClick != "CashInflow" && !isNotBlank(frmLoanIncomeInfo.txtFieldCashFlow.text)){
    displayTextBoxSelfCashInflow(false);
  }
  if(fieldClick != "ShareHolder" && !isNotBlank(frmLoanIncomeInfo.txtFieldShareHolderSelf.text)){
    displayTextBoxSelfShareHolder(false);
  }
}
function frmLoanIncomeInfo_flxBankAccountSelf_onClick(){
  frmLoanIncomeInfo.flxBankPopUp.setVisibility(true);
  loadSegmentBankList();
}
function frmLoanIncomeInfo_flxAccountNumberSelf_onClick(){
  setVisibilityAllSelfEmployeeFieldBox("AccountNumber");
  if(frmLoanIncomeInfo.subflxAccountNumberSelf.isVisible){
    displayTextBoxSelfAccountNumber(true);
  }
  frmLoanIncomeInfo.txtFieldAccountNumberSelf.setFocus(true);
}
function frmLoanIncomeInfo_flxDeclaredIncomeSelf_onClick(){
  setVisibilityAllSelfEmployeeFieldBox("DeclaredIncome");
  if(frmLoanIncomeInfo.subflxDeclaredIncomeSelf.isVisible){
    displayTextBoxSelfDeclaredIncome(true);
  }
  frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.setFocus(true);
}
function frmLoanIncomeInfo_flxCashInflowSelf_onClick(){
  setVisibilityAllSelfEmployeeFieldBox("CashInflow");
  if(frmLoanIncomeInfo.subflxCashInflowSelf.isVisible){
    displayTextBoxSelfCashInflow(true);
  }
  frmLoanIncomeInfo.txtFieldCashFlow.setFocus(true);
}
function frmLoanIncomeInfo_flxShareHolderSelf_onClick(){
  setVisibilityAllSelfEmployeeFieldBox("ShareHolder");
  if(frmLoanIncomeInfo.subflxShareHolderSelf.isVisible){
    displayTextBoxSelfShareHolder(true);
  }
  frmLoanIncomeInfo.txtFieldShareHolderSelf.setFocus(true);
}
function frmLoanIncomeInfo_btnNext_onClick(){
  gblLoanIncomeInformation = [];
  gblIncomeInvalidField = "";
  var validInput = false;
  var messageInvalid = kony.i18n.getLocalizedString("Err_IncorrectInfo");
  if(frmLoanIncomeInfo.flxMain.isVisible){
    validInput = employeeInformationValid();
  }else{
    validInput = selfEmployeeInformationValid();
  }
  if(validInput){
    //call updateFacilityInfo
    var inputParams ={};
    inputParams["incomeInfoSavedFlag"] = "Y";
    inputParams["incomeType"] = "1";
    inputParams["caId"] = gblCaId;
    if(frmLoanIncomeInfo.flxMain.isVisible){
      inputParams["incomeBankAccoutNumber"] = removeHyphenIB(gblLoanIncomeInformation["txtFieldAccountNumber"]);
      inputParams["bankNameEN"] = gblLoanIncomeInformation["bankNameEN"];
      inputParams["bankNameTH"] = gblLoanIncomeInformation["bankNameTH"];
      inputParams["bankCode"] = gblLoanIncomeInformation["incomeBankName"];
      inputParams["ncbConsentFlag"] = gblLoanIncomeInformation["lblConsentValue"];
      inputParams["incomeBasicSalary"] = removeNumberFormat(gblLoanIncomeInformation["txtFieldFixedIncome"]);
      if(isNotBlank(gblLoanIncomeInformation["txtFieldOtherIncome"])){
        inputParams["incomeExtraOther"] = removeNumberFormat(gblLoanIncomeInformation["txtFieldOtherIncome"]);
      }else{
        inputParams["incomeExtraOther"] = "";
      }
      if(isNotBlank(gblLoanIncomeInformation["txtFieldCOLAIncome"])){
        inputParams["incomeOtherIncome"] = removeNumberFormat(gblLoanIncomeInformation["txtFieldCOLAIncome"]);
      }else{
        inputParams["incomeOtherIncome"] = "";
      }
      if(isNotBlank(gblLoanIncomeInformation["txtFieldBonusYearly"])){
        inputParams["bonusYearly"] = removeNumberFormat(gblLoanIncomeInformation["txtFieldBonusYearly"]);
      }else{
        inputParams["bonusYearly"] = "";
      }
      inputParams["incomeDeclared"] = "";
      inputParams["incometotalLastMthCreditAcct1"] = "";
      inputParams["incomeSharedHolderPercent"] = "";
    }else{
      inputParams["incomeBankAccoutNumber"] = removeHyphenIB(gblLoanIncomeInformation["txtFieldAccountNumberSelf"]);
      inputParams["bankNameEN"] = gblLoanIncomeInformation["bankNameEN"];
      inputParams["bankNameTH"] = gblLoanIncomeInformation["bankNameTH"];
      inputParams["bankCode"] = gblLoanIncomeInformation["incomeBankName"];
      inputParams["ncbConsentFlag"] = gblLoanIncomeInformation["lblConsentValueSelf"];
      inputParams["incomeBasicSalary"] = "";
      inputParams["incomeOtherIncome"] = "";
      inputParams["bonusYearly"] = "";
      inputParams["incomeExtraOther"] = "";
      inputParams["incomeDeclared"] = removeNumberFormat(gblLoanIncomeInformation["txtFieldlDeclaredIncomeSelf"]);
      inputParams["incometotalLastMthCreditAcct1"] = removeNumberFormat(gblLoanIncomeInformation["txtFieldCashFlow"]);
      inputParams["incomeSharedHolderPercent"] = removeNumberFormat(gblLoanIncomeInformation["txtFieldShareHolderSelf"]);
    }
    showLoadingScreen();
//     updateIncomeInformationCallBack(null,null);
    invokeServiceSecureAsync("updateCustInfoJavaService", inputParams, updateIncomeInformationCallBack);
  }else{
    if(gblIncomeInvalidField.id == "txtFieldShareHolderSelf"){
      messageInvalid = kony.i18n.getLocalizedString("Err_ShareHolder_Exceed");
    }else if(gblIncomeInvalidField.id != "lblBankAccountDesc" && gblIncomeInvalidField.id != "lblBankAccountDescSelfEmploy"
             && gblIncomeInvalidField.id != "txtFieldAccountNumber" && gblIncomeInvalidField.id != "txtFieldAccountNumberSelf"){
      messageInvalid = kony.i18n.getLocalizedString("Err_Maximum_Amount");
    }
      showAlertWithCallBack(messageInvalid, kony.i18n.getLocalizedString("info"),onFocusFieldInvalid);
  }
}
function removeNumberFormat(inputText){
  var txt = inputText;
  var tempAmt = txt;
  var tempAmt1 = 0.00;
  tempAmt = replaceCommon(tempAmt, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
  tempAmt = kony.string.replace(tempAmt, ",", "");
  tempAmt1 = parseFloat(tempAmt.toString());
  return tempAmt1;
}
function updateIncomeInformationCallBack(status, callBackResponse){
  try{
    if (callBackResponse.opstatus == "0") {
      dismissLoadingScreen();
      gblLoanIncomeInformation = [];
      gblIncomeInvalidField = "";
      gblPersonalInfo.saveIncome = true;
      callbackOfgetPersonalInfoFields(status, callBackResponse);
    }else {
      dismissLoadingScreen();
      gblLoanIncomeInformation = [];
      gblIncomeInvalidField = "";
//       gblPersonalInfo.saveIncome = false;
      var errorResponse = callBackResponse.responseCode;
      if(typeof(errorResponse) != "string"){
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      }else{
        showAlert(replaceAll(kony.i18n.getLocalizedString("Err_LoanApp"),"{errorCode}",errorResponse), kony.i18n.getLocalizedString("info"));
      }
      return false;
    }
  }catch(e){
    alert("Exception in callbackOfSavePersonalInfoFields, e : "+e);
    dismissLoadingScreen();
  }
  dismissLoadingScreen();
}
function onFocusFieldInvalid(){
  gblIncomeInvalidField.parent.skin = "flexRedBorder1px";
  if(gblIncomeInvalidField.id != "lblBankAccountDesc" && gblIncomeInvalidField.id != "lblBankAccountDescSelfEmploy" ){
    gblIncomeInvalidField.setFocus(true);
  }
}

function employeeInformationValid(){
  var valid = true;
  if(!commonValidateInput(frmLoanIncomeInfo.lblBankAccountDesc, "painText", true)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldAccountNumber, "accountNumber", true)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldFixedIncome, "number", true)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldCOLAIncome, "number", false)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldBonusYearly, "number", false)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldOtherIncome, "number", false)){
    valid = false;
  }else{
    gblLoanIncomeInformation["incomeBankName"] = gblbankInfo.bankCode;
    gblLoanIncomeInformation["MAX_ACC_LEN_LIST"] = gblbankInfo.bankAccntLengths;
    gblLoanIncomeInformation["bankNameEN"] = gblbankInfo.bankNameEN;
    gblLoanIncomeInformation["bankNameTH"] = gblbankInfo.bankNameTH;
    gblLoanIncomeInformation["bankAccountName"] = frmLoanIncomeInfo.lblBankAccountDesc.text;
    gblLoanIncomeInformation["txtFieldAccountNumber"] = frmLoanIncomeInfo.txtFieldAccountNumber.text;
    gblLoanIncomeInformation["txtFieldFixedIncome"] = frmLoanIncomeInfo.txtFieldFixedIncome.text;
    gblLoanIncomeInformation["txtFieldCOLAIncome"] = frmLoanIncomeInfo.txtFieldCOLAIncome.text;
    gblLoanIncomeInformation["txtFieldBonusYearly"] = frmLoanIncomeInfo.txtFieldBonusYearly.text;
    gblLoanIncomeInformation["txtFieldOtherIncome"] = frmLoanIncomeInfo.txtFieldOtherIncome.text;
    gblLoanIncomeInformation["lblConsentValue"] = frmLoanIncomeInfo.lblConsentValue.text;
  }
  return valid;
}
function selfEmployeeInformationValid(){
  var valid = true;
  if(!commonValidateInput(frmLoanIncomeInfo.lblBankAccountDescSelfEmploy, "painText", true)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldAccountNumberSelf, "accountNumber", true)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf, "number", true)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldCashFlow, "number", true)){
    valid = false;
  } else if(!commonValidateInput(frmLoanIncomeInfo.txtFieldShareHolderSelf, "number", true)){
    valid = false;
  } else{
    gblLoanIncomeInformation["incomeBankName"] = gblbankInfo.bankCode;
    gblLoanIncomeInformation["MAX_ACC_LEN_LIST"] = gblbankInfo.bankAccntLengths;
    gblLoanIncomeInformation["bankNameEN"] = gblbankInfo.bankNameEN;
    gblLoanIncomeInformation["bankNameTH"] = gblbankInfo.bankNameTH;
    gblLoanIncomeInformation["bankAccountName"] = frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text;
    gblLoanIncomeInformation["txtFieldAccountNumberSelf"] = frmLoanIncomeInfo.txtFieldAccountNumberSelf.text;
    gblLoanIncomeInformation["txtFieldlDeclaredIncomeSelf"] = frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.text;
    gblLoanIncomeInformation["txtFieldCashFlow"] = frmLoanIncomeInfo.txtFieldCashFlow.text;
    gblLoanIncomeInformation["txtFieldShareHolderSelf"] = frmLoanIncomeInfo.txtFieldShareHolderSelf.text;
    gblLoanIncomeInformation["lblConsentValueSelf"] = frmLoanIncomeInfo.lblConsentValueSelf.text;
  }
  return valid;
}

function commonValidateInput(fieldObject, inputType, isRequired){
  var valid = true;
  if(!isRequired && !isNotBlank(fieldObject.text)){
    return valid;
  }
  else{
    if(inputType == "number"){
      if(!validAmountInput(fieldObject)){
        gblIncomeInvalidField = fieldObject;
        valid = false;
      }
    }else if(inputType == "accountNumber"){
      if(!validAccountNumber(fieldObject)){
        gblIncomeInvalidField = fieldObject;
        valid = false;
      }
    }else if(inputType == "painText"){
      if(!isNotBlank(fieldObject.text)){
        gblIncomeInvalidField = fieldObject;
        valid = false;
      }
    }
  }
  return valid;
}
function validAccountNumber(fieldObject){
  gblPrevLen=0;
  if(isNotBlank(fieldObject.text) && fieldObject.text.length > 0){
      var  lenghtsList= MAX_ACC_LEN_LIST.split(",");
      var minimumLength = parseInt(lenghtsList[0]);
      var enteredAccountLength = removeHyphenIB(fieldObject.text).length;
      if( MAX_ACC_LEN_LIST.indexOf(enteredAccountLength) < 0 || enteredAccountLength < minimumLength){
        return false;
      }else{
		return true;
	  }
  }else{
	return false;
  }
}

function frmLoanIncomeInfo_BtnPopUpClosePopUp_onClick(){
  frmLoanIncomeInfo.flxBankPopUp.setVisibility(false);
}
function frmLoanIncomeInfo_fxbankPopupseg_incomeBankList_selectedRowItems(){
  gblbankInfo = frmLoanIncomeInfo.incomeBankList.selectedRowItems[0];
  frmLoanIncomeInfo.flxBankAccount.skin = "lFboxLoan";
  var lbltext = gblbankInfo.lblBankName;
  MAX_ACC_LEN_LIST = gblbankInfo.bankAccntLengths;
  gblLoanIncomeInformation["incomeBankNameTmp"] = gblbankInfo.bankCode;
  if(frmLoanIncomeInfo.flxMain.isVisible){
    frmLoanIncomeInfo.lblBankAccountDesc.text = lbltext;
    frmLoanIncomeInfo.txtFieldAccountNumber.text = "";
    displayTextBoxAccountNumber(false);
    displayTextBoxBankAccount(true);
  }else{
    frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text = lbltext;
    frmLoanIncomeInfo.txtFieldAccountNumberSelf.text = "";
    displayTextBoxSelfAccountNumber(false);
    displayTextBoxSelfBankAccount(true);
  }
  frmLoanIncomeInfo.flxBankPopUp.setVisibility(false);
  handleSaveBtnIncomeInfo();
  if(frmLoanIncomeInfo.flxMain.isVisible){
    frmLoanIncomeInfo_flxAccountNumber_onClick();
    frmLoanIncomeInfo.txtFieldAccountNumber.setFocus(true);
  }else{
    frmLoanIncomeInfo_flxAccountNumberSelf_onClick();
    frmLoanIncomeInfo.txtFieldAccountNumberSelf.setFocus(true);
  }
}
function validAmountInput(eventObject){
  	var txt = eventObject.text;
  	var tempAmt = txt;
  	var tempAmt1 = 0.00;
	if(isNotBlank(tempAmt)){
      tempAmt = replaceCommon(tempAmt, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
      tempAmt = kony.string.replace(tempAmt, ",", "");
      tempAmt1 = parseFloat(tempAmt.toString());
      if(tempAmt1.length == 0 || tempAmt1 == "." || tempAmt1 == ","){
        return false;
      }else{
        if(eventObject.id == "txtFieldShareHolderSelf"){
          if(tempAmt1 > 100){
            return false;
          }
        }else if(tempAmt1 > 999999999.99){
          return false;
        }
        return true;
      }
    }else{
      return false;
    }
}

function handleSaveBtnIncomeInfo() {
  var filledInput = true;
  var txtArry = [];
  var notRequired = ["txtFieldCOLAIncome","txtFieldBonusYearly","txtFieldOtherIncome"];
  if(frmLoanIncomeInfo.flxMain.isVisible){
    txtArry = ["lblBankAccountDesc","txtFieldAccountNumber","txtFieldFixedIncome","txtFieldCOLAIncome","txtFieldBonusYearly","txtFieldOtherIncome","lblConsentValue"];
  }else{
    txtArry = ["lblBankAccountDescSelfEmploy","txtFieldAccountNumberSelf","txtFieldlDeclaredIncomeSelf","txtFieldCashFlow","txtFieldShareHolderSelf","lblConsentValueSelf"];
  }
  for(var i = 0; i < txtArry.length; i++){
    var val = frmLoanIncomeInfo[txtArry[i]].text;
    if(!isNotBlank(val) && notRequired.indexOf(txtArry[i]) == -1) {
      filledInput = false;
      break;
    }
  }  

  if(!filledInput) { 
    frmLoanIncomeInfo.btnNext.onClick = disableBackButton;
    frmLoanIncomeInfo.lblcircle.skin = "skinpiorangeLineLone";
    frmLoanIncomeInfo.btnNext.skin = "btnGreyBGNoRoundLoan";
    frmLoanIncomeInfo.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
  } else {
    frmLoanIncomeInfo.lblcircle.skin = "skinpiGreenLineLone";
    frmLoanIncomeInfo.btnNext.skin = "btnBlue28pxLoan2";
    frmLoanIncomeInfo.btnNext.focusSkin="btnBlue200GreyBG";
    frmLoanIncomeInfo.btnNext.onClick = frmLoanIncomeInfo_btnNext_onClick;
  }
}

function loadSegmentBankList (){
  try{
    frmLoanIncomeInfo.incomeBankList.removeAll();
    //entryCode 011
    //entryID 94332
    //activeStatus = 1
    //entryName2 = thainame
    //entryName = eng
    //refEntryCode = A01
    var bankSelected = getBankSelected();
    var incomeBankListData = [];
   for(var j = 0; j < gblPersonalInfo.DIS_BANK.length ; j++){
     var obj = {};
     if(gblPersonalInfo.DIS_BANK[j].activeStatus == "1"){
       var productCode = gblPersonalInfo.DIS_BANK[j].entryCode;
       var imageURL = loadBankIcon(productCode.substr(1,productCode.length));
       var bankAccntLengths = gblPersonalInfo.DIS_BANK[j].BANK_ACCT_LENGTH;
       var bankName = "";
       var imageTick = "empty.png";
       var flxSkin = "lFboxSegBrd0pxLoan";
       if(bankSelected == productCode && isNotBlank(gblbankInfo)){
         imageTick = "tick_icon.png";
         flxSkin = "flexGreyBGLoan";
       }
       if("th_TH" == kony.i18n.getCurrentLocale()){
         bankName = gblPersonalInfo.DIS_BANK[j].entryName2;
       }else{
         bankName = gblPersonalInfo.DIS_BANK[j].entryName2;
       }
       obj = {
         "lblBankName": bankName,
         "imgBankLogo":imageURL,
         "bankAccntLengths":bankAccntLengths,
         "bankCode":productCode,
         "bankNameEN":gblPersonalInfo.DIS_BANK[j].entryName2,
         "bankNameTH":gblPersonalInfo.DIS_BANK[j].entryName2,
         "flxBaseField":{
           "skin":flxSkin
         },
         "lblSegRowLine":"------", 
         "imgTick":imageTick, 
       };
       incomeBankListData.push(obj);
     }
   }
        frmLoanIncomeInfo.incomeBankList.widgetDataMap = {lblBankName: "lblBankName", imgBankLogo: "imgBankLogo", 
                                                  bankAccntLengths: "bankAccntLengths", bankCode: "bankCode",
                                                  bankNameEN:"bankNameEN",bankNameTH:"bankNameTH",
                                                  flxBaseField:"flxBaseField",lblSegRowLine:"lblSegRowLine",
                                                  imgTick:"imgTick"};
    frmLoanIncomeInfo.incomeBankList.setData(incomeBankListData);
    frmLoanIncomeInfo.incomeBankList.onRowClick = frmLoanIncomeInfo_fxbankPopupseg_incomeBankList_selectedRowItems;
  }catch(e){
    kony.print("Exception : "+e);
  }
}

function frmLoanIncomeInfo_txtNumberField_onDone(eventObject, changedText)
{
  eventObject.parent.skin = "lFboxLoan";
  if(eventObject.text.indexOf(".")>0){
    eventObject.text = formatAmountValue(eventObject.text);
  }
  handleSaveBtnIncomeInfo();
}
function frmLoanIncomeInfo_txtNumberField_onDone_withFocus(eventObject, changedText)
{
  eventObject.parent.skin = "lFboxLoan";
  if(eventObject.text.indexOf(".")>0){
    eventObject.text = formatAmountValue(eventObject.text);
  }
  handleSaveBtnIncomeInfo();
  handleTextNavigation(eventObject);
}

function handleTextNavigation(eventObject){
  if(frmLoanIncomeInfo.flxMainSelfEmployed.isVisible){
    if(eventObject.id == "txtFieldAccountNumberSelf"){
      frmLoanIncomeInfo_flxDeclaredIncomeSelf_onClick();
      frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.setFocus(true);
    }else if(eventObject.id == "txtFieldlDeclaredIncomeSelf"){
      frmLoanIncomeInfo_flxCashInflowSelf_onClick();
      frmLoanIncomeInfo.txtFieldCashFlow.setFocus(true);
    }else if(eventObject.id == "txtFieldCashFlow"){
      frmLoanIncomeInfo_flxShareHolderSelf_onClick();
      frmLoanIncomeInfo.txtFieldShareHolderSelf.setFocus(true);
    }else if(eventObject.id == "txtFieldShareHolderSelf"){
      setVisibilityAllEmployeeFieldBox();
      frmLoanIncomeInfo.flexCcScoringSelf.setFocus(true);
    }
  }else{
    if(eventObject.id == "txtFieldAccountNumber"){
      frmLoanIncomeInfo_flxFixedIncome_onClick();
      frmLoanIncomeInfo.txtFieldFixedIncome.setFocus(true);
    }else if(eventObject.id == "txtFieldFixedIncome"){
      frmLoanIncomeInfo_flxCOLAIncome_onClick();
      frmLoanIncomeInfo.txtFieldCOLAIncome.setFocus(true);
    }else if(eventObject.id == "txtFieldCOLAIncome"){
      frmLoanIncomeInfo_flxBonusYearly_onClick();
      frmLoanIncomeInfo.txtFieldBonusYearly.setFocus(true);
    }else if(eventObject.id == "txtFieldBonusYearly"){
      frmLoanIncomeInfo_flxOtherIncome_onClick();
      frmLoanIncomeInfo.txtFieldOtherIncome.setFocus(true);
    }else if(eventObject.id == "txtFieldOtherIncome"){
      setVisibilityAllEmployeeFieldBox();
      frmLoanIncomeInfo.flexCcScoring.setFocus(true);
    }
  }
}
function frmLoanIncomeInfo_AccountNumber_onDone(eventObject, changedText){
  handleSaveBtnIncomeInfo();
}
function frmLoanIncomeInfo_AccountNumber_onDone_withFocus(eventObject, changedText){
  handleSaveBtnIncomeInfo();
  handleTextNavigation(eventObject);
}
function frmLoanIncomeInfo_txtNumberField_onTextChange(eventObject, changedText){
  	eventObject.parent.skin = "lFboxLoan";
	var enteredAmount = eventObject.text;
	if(isNotBlank(enteredAmount)) {
		enteredAmount = kony.string.replace(enteredAmount, ",", "");
		if(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) == 0){
			
		}else{
			eventObject.text = commaFormattedTransfer(enteredAmount);
		}
	}
  	handleSaveBtnIncomeInfo();
}

function onTextChangeBankAccountNumber(eventObject, textChanged){
  	eventObject.parent.skin = "lFboxLoan";
	if(eventObject.text.length > 0){
      var minLengh = MAX_ACC_LEN_LIST.split(",");
		var minLengh = parseInt(minLengh[0]);
		if(minLengh == 10 && MAX_ACC_LEN_LIST.indexOf(",") < 0){
			onEditAccNumber(eventObject);
		}
	}
  	handleSaveBtnIncomeInfo();
}
function onEditAccNumber(eventObject) {
  	var txt = eventObject.text;
	if (txt == null) return false;
	var numChars = txt.length;
	var temp = "";
	var i, txtLen = numChars;
	var currLen = numChars;
	if (gblPrevLen < currLen) {
		for (i = 0; i < numChars; ++i) {
			if (txt[i] != '-') {
				temp = temp + txt[i];
			} else {
				txtLen--;
			}
		}
		var iphenText = "";
		for (i = 0; i < txtLen; i++) {
			iphenText += temp[i];
			if (i == 2 || i == 3 || i == 8) {
				iphenText += '-';
			}
		}
			eventObject.text = iphenText;
	}
	
	gblPrevLen = currLen;
}
function formatAmountValue(amount){
	if(!isNotBlank(amount)){
		return "";
	}
    tmpamount = removeCommos(amount) + "";
	var amountFormat = tmpamount.match(/^-?\d+(?:\.\d{0,2})?/)[0];
    if(amountFormat.indexOf(".")>0){
      amountFormat = commaFormatted(parseFloat(amountFormat).toFixed(2));
    }else{
      amountFormat = commaFormatted(amountFormat);
    }
	return amountFormat;
}

function navigateToLoanInformationInfo(){
  clearDataSelected();
//   if(gblChosenTab == "01"){
//     //Active employee
//     frmLoanIncomeInfo_btnEmployeeDeselect_onClick();
//   }else if(gblChosenTab == "02"){
//     //Active self employee
//     frmLoanIncomeInfo_btnSelfEmployed_onClick();
//   }
  if(typeof(gblLoanIncomeInformation["txtFieldAccountNumber"]) != "undefined"){
    frmLoanIncomeInfo.flxMainSelfEmployed.setVisibility(false);
    frmLoanIncomeInfo.flxMain.setVisibility(true);
    if("th_TH" == kony.i18n.getCurrentLocale()){
      frmLoanIncomeInfo.lblBankAccountDesc.text = gblLoanIncomeInformation["bankNameTH"];
    }else{
      frmLoanIncomeInfo.lblBankAccountDesc.text = gblLoanIncomeInformation["bankNameEN"];
    }
    MAX_ACC_LEN_LIST = gblLoanIncomeInformation["MAX_ACC_LEN_LIST"];
    gblLoanIncomeInformation["incomeBankNameTmp"] = gblLoanIncomeInformation["incomeBankName"];
    frmLoanIncomeInfo.txtFieldAccountNumber.text = gblLoanIncomeInformation["txtFieldAccountNumber"];
    frmLoanIncomeInfo.txtFieldFixedIncome.text = gblLoanIncomeInformation["txtFieldFixedIncome"];
    frmLoanIncomeInfo.txtFieldCOLAIncome.text = gblLoanIncomeInformation["txtFieldCOLAIncome"];
    frmLoanIncomeInfo.txtFieldBonusYearly.text = gblLoanIncomeInformation["txtFieldBonusYearly"];
    frmLoanIncomeInfo.txtFieldOtherIncome.text = gblLoanIncomeInformation["txtFieldOtherIncome"];
    frmLoanIncomeInfo.lblConsentValue.text = gblLoanIncomeInformation["lblConsentValue"];
    displayTextBoxBankAccount(isNotBlank(frmLoanIncomeInfo.lblBankAccountDesc.text));
	displayTextBoxAccountNumber(isNotBlank(frmLoanIncomeInfo.txtFieldAccountNumber.text));
	displayTextBoxFixedIncome(isNotBlank(frmLoanIncomeInfo.txtFieldFixedIncome.text));
	displayTextBoxCOLAIncome(isNotBlank(frmLoanIncomeInfo.txtFieldCOLAIncome.text));
	displayTextBoxBonusYearly(isNotBlank(frmLoanIncomeInfo.txtFieldBonusYearly.text));
	displayTextBoxOtherIncome(isNotBlank(frmLoanIncomeInfo.txtFieldOtherIncome.text));
    if(frmLoanIncomeInfo.lblConsentValue.text == "Y"){
      acceptNCP();
    }else if(frmLoanIncomeInfo.lblConsentValue.text == "N"){
      notAcceptNCP();
    }
    setupButtonAndBullet();
  }else if(typeof(gblLoanIncomeInformation["txtFieldAccountNumberSelf"]) != "undefined"){
    frmLoanIncomeInfo.flxMainSelfEmployed.setVisibility(true);
    frmLoanIncomeInfo.flxMain.setVisibility(false);
    if("th_TH" == kony.i18n.getCurrentLocale()){
      frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text = gblLoanIncomeInformation["bankNameTH"];
    }else{
      frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text = gblLoanIncomeInformation["bankNameEN"];
    }
    MAX_ACC_LEN_LIST = gblLoanIncomeInformation["MAX_ACC_LEN_LIST"];
    gblLoanIncomeInformation["incomeBankNameTmp"] = gblLoanIncomeInformation["incomeBankName"];
    frmLoanIncomeInfo.txtFieldAccountNumberSelf.text = gblLoanIncomeInformation["txtFieldAccountNumberSelf"];
    frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.text = gblLoanIncomeInformation["txtFieldlDeclaredIncomeSelf"];
    frmLoanIncomeInfo.txtFieldCashFlow.text = gblLoanIncomeInformation["txtFieldCashFlow"];
    frmLoanIncomeInfo.txtFieldShareHolderSelf.text = gblLoanIncomeInformation["txtFieldShareHolderSelf"];
    frmLoanIncomeInfo.lblConsentValueSelf.text = gblLoanIncomeInformation["lblConsentValueSelf"];
    displayTextBoxSelfBankAccount(isNotBlank(frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text));
    displayTextBoxSelfAccountNumber(isNotBlank(frmLoanIncomeInfo.txtFieldAccountNumberSelf.text));
    displayTextBoxSelfDeclaredIncome(isNotBlank(frmLoanIncomeInfo.txtFieldlDeclaredIncomeSelf.text));
    displayTextBoxSelfCashInflow(isNotBlank(frmLoanIncomeInfo.txtFieldCashFlow.text));
    displayTextBoxSelfShareHolder(isNotBlank(frmLoanIncomeInfo.txtFieldShareHolderSelf.text));
    if(frmLoanIncomeInfo.lblConsentValueSelf.text == "Y"){
      acceptNCP();
    }else if(frmLoanIncomeInfo.lblConsentValueSelf.text == "N"){
      notAcceptNCP();
    }
    setupButtonAndBullet();
  }
  frmLoanIncomeInfo.show();
}

function getBankSelected(){
  var bankSelected ="";
  if(typeof(gblLoanIncomeInformation["incomeBankNameTmp"]) != "undefined"){
    bankSelected = gblLoanIncomeInformation["incomeBankNameTmp"];
    if(frmLoanIncomeInfo.flxMain.isVisible){
      if(typeof(frmLoanIncomeInfo.lblBankAccountDesc.text) != "undefined" && isNotBlank(frmLoanIncomeInfo.lblBankAccountDesc.text)){
        bankSelected = gblLoanIncomeInformation["incomeBankNameTmp"];
      }
      if(typeof(bankSelected) != "undefined"){
        return bankSelected;
      }
    }else{
      if(typeof(frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text) != "undefined" && isNotBlank(frmLoanIncomeInfo.lblBankAccountDescSelfEmploy.text)){
        bankSelected = gblLoanIncomeInformation["incomeBankNameTmp"];
      }
      if(typeof(bankSelected) != "undefined"){
        return bankSelected;
      }
    }
  }else{
    return "";
  }
}

function getBankNameByID(){
     for(var j = 0; j < gblPersonalInfo.DIS_BANK.length ; j++){
       var obj = {};
       gblbankInfo = {};
       if(gblPersonalInfo.DIS_BANK[j].activeStatus == "1"){
         var productCode = gblPersonalInfo.DIS_BANK[j].entryCode;
         if(productCode == gblLoanIncomeInformation["incomeBankName"]){
           gblLoanIncomeInformation["MAX_ACC_LEN_LIST"] = gblPersonalInfo.DIS_BANK[j].BANK_ACCT_LENGTH;
           gblLoanIncomeInformation["bankNameEN"] = gblPersonalInfo.DIS_BANK[j].entryName2;
           gblLoanIncomeInformation["bankNameTH"] = gblPersonalInfo.DIS_BANK[j].entryName2;
           gblbankInfo.lblBankName = gblPersonalInfo.DIS_BANK[j].entryName2;
           gblbankInfo.bankAccntLengths = gblPersonalInfo.DIS_BANK[j].BANK_ACCT_LENGTH;
           gblbankInfo.bankCode = productCode;
           break;
         }
       }
   }
}

function acceptNCP() {
  setVisibilityAllEmployeeFieldBox();
  if(frmLoanIncomeInfo.flxMain.isVisible){
    frmLoanIncomeInfo.flxcircleAccept.skin = "slFboxgreenBGcircle";
    frmLoanIncomeInfo.flxcircleNotAccept.skin = "slFboxGreaycircle";
    frmLoanIncomeInfo.imgChkAccept.skin = "lblskinpiGreencheckLineLone";
    frmLoanIncomeInfo.imgchkNotAccept.skin = "lblskinpigrayLineLone";
    frmLoanIncomeInfo.lblConsentValue.text = "Y";
  }else{
    frmLoanIncomeInfo.flxcircleAcceptSelf.skin = "slFboxgreenBGcircle";
    frmLoanIncomeInfo.flxcircleNotAcceptSelf.skin = "slFboxGreaycircle";
    frmLoanIncomeInfo.imgChkAcceptSelf.skin = "lblskinpiGreencheckLineLone";
    frmLoanIncomeInfo.imgchkNotAcceptSelf.skin = "lblskinpigrayLineLone";
    frmLoanIncomeInfo.lblConsentValueSelf.text = "Y";
  }
  handleSaveBtnIncomeInfo();
}

function notAcceptNCP() {
  setVisibilityAllEmployeeFieldBox();
  if(frmLoanIncomeInfo.flxMain.isVisible){
    frmLoanIncomeInfo.imgChkAccept.skin = "lblskinpigrayLineLone";
    frmLoanIncomeInfo.imgchkNotAccept.skin = "lblskinpiGreencheckLineLone";
    frmLoanIncomeInfo.flxcircleNotAccept.skin = "slFboxgreenBGcircle";
    frmLoanIncomeInfo.flxcircleAccept.skin = "slFboxGreaycircle";
    frmLoanIncomeInfo.lblConsentValue.text = "N";
  }else{
    frmLoanIncomeInfo.imgChkAcceptSelf.skin = "lblskinpigrayLineLone";
    frmLoanIncomeInfo.imgchkNotAcceptSelf.skin = "lblskinpiGreencheckLineLone";
    frmLoanIncomeInfo.flxcircleNotAcceptSelf.skin = "slFboxgreenBGcircle";
    frmLoanIncomeInfo.flxcircleAcceptSelf.skin = "slFboxGreaycircle";
    frmLoanIncomeInfo.lblConsentValueSelf.text = "N";
  }

  handleSaveBtnIncomeInfo();
}

function showHideNCPHelpGuide(){
  if(frmLoanIncomeInfo.flxNCBPopUp.isVisible){
    frmLoanIncomeInfo.flxNCBPopUp.setVisibility(false);
  }else{
    frmLoanIncomeInfo.flxNCBPopUp.setVisibility(true);
  }
  
}

function convertAccontNumberFormat(accountNumber){
            gblPrevLen = 0;
          var minLengh = gblLoanIncomeInformation["MAX_ACC_LEN_LIST"].split(",");
          var minLengh = parseInt(minLengh[0]);
          if(minLengh == 10 && gblLoanIncomeInformation["MAX_ACC_LEN_LIST"].indexOf(",") < 0){
            var numChars = accountNumber.length;
            var temp = "";
            var i, txtLen = numChars;
            var currLen = numChars;
              for (i = 0; i < numChars; ++i) {
                if (accountNumber[i] != '-') {
                  temp = temp + accountNumber[i];
                } else {
                  txtLen--;
                }
              }
              var iphenText = "";
              for (i = 0; i < txtLen; i++) {
                iphenText += temp[i];
                if (i == 2 || i == 3 || i == 8) {
                  iphenText += '-';
                }
              }
              accountNumber = iphenText;
          }
  return accountNumber;
}