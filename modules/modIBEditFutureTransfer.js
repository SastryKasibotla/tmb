







function gblVarEditFTIB() {
    gblFrmaccntProdCode = "" //to use for getTransferFee
    Accnts_RelTable = []; //To save Accnt_Rel Data  globally
    TMBAccntData = []; // To save CustmerAccntInq Data globally
    gblFrmAccntID = "" // Chek the format of the Acnt Num which one used in all cases
    gblFrmAccntType = ""
    gblToAccntID = ""
    gblToAccntType = ""
    gblTransfrAmt = "" // Saving to use for getTranferFee
    //gblBankType = ""; // Used to get TO account Name b4 navigationg to Edit Confirmation page
    gblToBankCdFT = ""; // Used to get TO account Name b4 navigationg to Edit Confirmation page
  	gblToBankCdFTTmp = "";

    gblEditFTSchdule = false; // Used for schedule changes
    gblFtDel = false; // Used to handle delete service from edit flow n delete flow
    gblTransCodeFT = ""; //To use for functranferInq i/p

    gblpmtMethodFT = ""; //for doPmtAdd 
    gblOrderDateFT = "";
    gblOTPLENGTH_FT = "";
    gblAccntName_ORFT = "";
    //gblFreeTransactions = ""     //to use for getTransferFee (nt using ri8 now)
    gblCRMProfileInqCal = false;
    gblOTPLock = false; //TO show same popup based on gblOTPLock value
    gbltokenFlag = false;

    gblFrmAccntfIIdentFT = "";
    gblToAccntfIIdentFT = "";
    gblFrmAccntdepositeNo = "";

    //Adding to save old data 	
    gblFTViewInitiateDate = "";
    gblFTViewStartOnDate = "";
    gblFTViewRepeatAsVal = "";
    gblFTViewEndVal = "";
    gblFTViewEndDate = "";
    gblFTViewExcuteVal = "";
    gblFTViewExcuteremaing = "";
    gblEndValTemp = "";


    gblSMARTTransAmnt = "";
    gblORFTPerTransAmntLimt = "";

    //gblSchduleRefIDFT = "";
    gblisEditFTFlow = false;
    srvPaymentInq();

}

// JDBC which gives all the list of accounts present in AccountRelation Table 

function getAllAccountsInfo() {

    var temp = gblToAccntID;
  
   kony.print("gblToBankCdFT  "+gblToBankCdFT);
  	if(isNotBlank(gblToBankCdFT) && gblToBankCdFT == "11"){
    	if(temp.length > 10){
		  if (temp.length > 10) temp = temp.substring(temp.length - 10, temp.length);
		}
    }
  
   
   
    /* New
    if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
        var len = temp.length;
        if (gblToAccntType == "CDA") {
        	if(len == "13")temp = gblToAccntID.substring(0, len-3);
		}else
		   if (len > 10) temp = temp.substring(len - 10, len);
	}else{
		var len = temp.length;
		if(len > 10)
			temp = temp.substring(len - 10, len);
	}	
    */
    var inputParam = {}
    inputParam["crmId"] = gblcrmId;
    inputParam["personalizedAcctId"] = temp;

    invokeServiceSecureAsync("crmAllAccountsInquiryKony", inputParam, getAllAccountsInfocallBack)
}

function getAllAccountsInfocallBack(status, resulttable) {
    //dismissLoadingScreenPopup();
    if (status == 400) {
        gblPersoanlizedId = "";
        gblpersonalizedPicId = "";
        gblpersonalizedName = "";
        gblToAccntNickUpdated = "";
        if (resulttable["opstatus"] == 0) {
            if (resulttable.Results.length != 0) {
                Accnts_RelTable = []; // global array to search from/to account status n nickname and accnut name if TMB

                for (var i = 0; i < resulttable.Results.length; i++) {
                    var accntNickName = "";
                    var accntStatus = "";
                    var accntName = "";
                    var bankcd = "";
                    var accntNum = "";
                    var personalizedId = "";
                    var personalizedPicId = "";
                    var personalizedName = "";

                    if (resulttable.Results[i].acctNickName != null && resulttable.Results[i].acctNickName != "")
                        accntNickName = resulttable.Results[i].acctNickName;
                    if (resulttable.Results[i].accntStatus != null && resulttable.Results[i].accntStatus != "")
                        accntStatus = resulttable.Results[i].accntStatus;
                    if (resulttable.Results[i].accntName != null && resulttable.Results[i].accntName != "")
                        accntName = resulttable.Results[i].accntName;
                    if (resulttable.Results[i].bankCD != null && resulttable.Results[i].bankCD != "")
                        bankcd = resulttable.Results[i].bankCD;
                    if (resulttable.Results[i].personalizedAcctId != null && resulttable.Results[i].personalizedAcctId != "")
                        accntNum = resulttable.Results[i].personalizedAcctId;

                    if (resulttable.Results[i].personalizedId != null && resulttable.Results[i].personalizedId != "")
                        personalizedId = resulttable.Results[i].personalizedId;

                    if (resulttable.Results[i].personalizedPicId != undefined)
                        personalizedPicId = resulttable.Results[i].personalizedPicId;

                    if (resulttable.Results[i].personalizedName != undefined && resulttable.Results[i].personalizedName != null)
                        personalizedName = resulttable.Results[i].personalizedName;

                    /*
			    if(bankcd != "11" && accntNum.length >= 10){
			    	accntNum = accntNum.substring(accntNum.length-10, accntNum.length);
			    }
				*/
                    Accnts_RelTable.push({
                        "accntNickName": accntNickName,
                        "accntStatus": accntStatus,
                        "accntName": accntName,
                        "bankcd": bankcd,
                        "accntNum": accntNum,
                        "personalizedId": personalizedId,
                        "personalizedPicId": personalizedPicId,
                        "personalizedName": personalizedName

                    });
                }

                //Checking existenace of "TO Account" 

                var accntFound = false;
                isTMB = false;
                var isOwnAccnt = false;
                var accIndex = "";
                var tmpAccnt = gblToAccntID;
                gblPersoanlizedId = "";
                if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
                    if (gblToAccntType == "SDA" || gblToAccntType == "CDA") {
                        if (gblToAccntID.length == 10)
                            tmpAccnt = "0000" + tmpAccnt;
                    }
                }

                // Searching TO-Accnt in response of crmAllAccntInq   
                for (var j = 0; j < Accnts_RelTable.length; j++) {
                    if (tmpAccnt == Accnts_RelTable[j].accntNum && Accnts_RelTable[j].bankcd == gblToBankCdFTTmp) {
                        accIndex = j;
                        gblAccntName_ORFT = Accnts_RelTable[j].accntName; //accntNickName Accnts_RelTable[j].accntName
                        gblToBankCdFT = Accnts_RelTable[accIndex].bankcd;
                        gblPersoanlizedId = Accnts_RelTable[accIndex].personalizedId;
                        gblpersonalizedPicId = Accnts_RelTable[accIndex].personalizedPicId;
                        gblpersonalizedName = Accnts_RelTable[accIndex].personalizedName;
                        
                        if(isNotBlank(Accnts_RelTable[j].accntNickName)) {
                       	 	gblToAccntNickUpdated = Accnts_RelTable[j].accntNickName;
                        	frmIBFTrnsrView.lblToAccntNickName.text = Accnts_RelTable[j].accntNickName;
                        } else {
                        	gblToAccntNickUpdated = frmIBFTrnsrView.lblToAccntNickName.text;
                        }
                        if(Accnts_RelTable[accIndex].accntName!=null && Accnts_RelTable[accIndex].accntName != undefined && Accnts_RelTable[accIndex].accntName != "NONE" && Accnts_RelTable[accIndex].accntName != "NA" && Accnts_RelTable[accIndex].accntName != "null")
                        {
							frmIBFTrnsrView.lblToAccntName.text = Accnts_RelTable[accIndex].accntName;
						}else{
							frmIBFTrnsrView.lblToAccntName.text = "";
						}
                        if (Accnts_RelTable[j].bankcd == "11") {
                            if (gblOwnPersonlisedIdFT == gblPersoanlizedId) {
                                isTMB = true;
                            } else
                                isTMB = false;


                            if (Accnts_RelTable[j].accntStatus == "02") {
                                accntFound = false;
                            } else {
                                accntFound = true;
                                frmIBFTrnsrView.lblToAccntName.text = Accnts_RelTable[accIndex].accntName;
                                //frmIBFTrnsrView.lblToAccntNickName.text = Accnts_RelTable[accIndex].accntNickName;
                            }
                        } else
                            accntFound = true;
                    }
                }

                // Searching TO-Accnt in response of customerAccntInq  

                if (accntFound == false) {
                    for (var k = 0; k < TMBAccntData.length; k++) {
                        if (tmpAccnt == TMBAccntData[k].accntNum && TMBAccntData[k].bankcd == gblToBankCdFTTmp) {
                            //  
                            accIndex = k;
                            gblAccntName_ORFT = TMBAccntData[k].accntName;
                            gblToBankCdFT = "11"
                            gblToAccntfIIdentFT = TMBAccntData[k].fiident;

                            //  isTMB = true;

                            if (TMBAccntData[k].bankcd != "") {
                                gblToBankCdFT = TMBAccntData[k].bankcd;
                                isTMB = false;
                                isOwnAccnt = true;
                            } else {
                                gblToBankCdFT = "11";
                                isTMB = true;
                                isOwnAccnt = true;
                            }

                            accntFound = true;
                            if (TMBAccntData[k].accntStatus == "02") {
                                accntFound = false;
                            } else {
                                frmIBFTrnsrView.lblToAccntName.text = TMBAccntData[k].accntName;
                                //frmIBFTrnsrView.lblToAccntNickName.text = TMBAccntData[k].nickName;
                            }
                        }
                    }
                }

                if (gblPersoanlizedId != "") {
                	var randomnum = Math.floor((Math.random()*10000)+1); 
                    var recepentImageURL = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/ImageRender?billerId=&crmId=Y&personalizedId=" + gblPersoanlizedId + "&rr=" + randomnum ;
                        //var recepentImageURL = "https://vit.tau2904.com:443/tmb/ImageRender?billerId=&crmId=" + gblcrmIdFT +"&personalizedId="+gblPersoanlizedId	
                } else
                    var recepentImageURL = "";

                var ownProfilePic = "";
                ownProfilePic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";
                //ownProfilePic = "https://vit.tau2904.com:443/tmb/ImageRender?crmId=" + gblcrmIdFT + "&personalizedId=&billerId=";

                
                if (accntFound == true) {
                    // gblToBankCdFT = Accnts_RelTable[accIndex].bankcd; 
                    frmIBFTrnsrView.btnFTEdit.setEnabled(true);
                    frmIBFTrnsrView.btnNxt.setEnabled(true);
                    frmIBFTrnsrView.btnCancel.setEnabled(true);

                    frmIBFTrnsrView.hbxTrnsfrTo.setVisibility(true);
                    frmIBFTrnsrView.hbxToAccntHidenError.setVisibility(false);
                    frmIBFTrnsrView.rchtxtErrorToAccnt.setVisibility(false);

                    if (frmIBFTrnsrView.hbxFrmAccntHidenError.isVisible == false) {
                        frmIBFTrnsrView.btnFTEditFlow.setEnabled(true);
                        frmIBFTrnsrView.btnFTEditFlow.skin = "btnIBediticonsmall";
                        frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnIbeditsmalliconHover";
                        frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnIbeditsmalliconHover";
                    } else {
                        frmIBFTrnsrView.btnFTEditFlow.setEnabled(false);
                        frmIBFTrnsrView.btnFTEditFlow.skin = "btnEditDisabled";
                        frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnEditDisabled";
                        frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnEditDisabled";
                    }

                    if (isTMB == true) {
                        frmIBFTrnsrView.lblToAccntName.setVisibility(true); //Making visible as accnt name is not shown for other banks
                        frmIBFTrnsrView.imgToAccntFT.src = ownProfilePic //"avatar_dis.jpg";     //recepentImageURL + ".jpg" //"icon.png"; 
                        frmIBFTrnsrEditCnfmtn.imgToAccntFT.src = ownProfilePic //"avatar_dis.jpg";	//recepentImageURL + ".jpg" //"icon.png";      

                        frmIBFTrnsrView.hbxNotifyReceipent.setVisibility(false);
                        frmIBFTrnsrView.hbxNoteToReceipent.setVisibility(false);
                        frmIBFTrnsrEditCnfmtn.hbxNotifyReceipent.setVisibility(false);
                        frmIBFTrnsrEditCnfmtn.hbxNoteToReceipent.setVisibility(false);

                        //Search in CustmerAccntInq Response if Nickname is not found in Account_RelTable 
                        /*
		          if (frmIBFTrnsrView.lblToAccntNickName.text == undefined || frmIBFTrnsrView.lblToAccntNickName.text == "" || frmIBFTrnsrView.lblToAccntNickName.text == null) {
		              for (var k = 0; k < TMBAccntData.length; k++) {
		                  if (gblToAccntID == TMBAccntData[k].accntNum) {
		                         frmIBFTrnsrView.lblToAccntNickName.text = TMBAccntData[k].nickName;
		                  }
		              }
		          }
		       */
                    } else {
                        
                        if (isOwnAccnt == true) {
                            

                            frmIBFTrnsrView.lblToAccntName.setVisibility(true);
                            frmIBFTrnsrView.imgToAccntFT.src = ownProfilePic; //"avatar_dis.jpg";     //recepentImageURL + ".jpg" //"icon.png"; 
                            frmIBFTrnsrEditCnfmtn.imgToAccntFT.src = ownProfilePic; //"avatar_dis.jpg";	//recepentImageURL + ".jpg" //"icon.png";  

                            frmIBFTrnsrView.hbxNotifyReceipent.setVisibility(false);
                            frmIBFTrnsrView.hbxNoteToReceipent.setVisibility(false);
                            frmIBFTrnsrEditCnfmtn.hbxNotifyReceipent.setVisibility(false);
                            frmIBFTrnsrEditCnfmtn.hbxNoteToReceipent.setVisibility(false);

                        } else {

                            if (gblOwnPersonlisedIdFT == gblPersoanlizedId) {

                                
                                frmIBFTrnsrView.hbxNotifyReceipent.setVisibility(false);
                                frmIBFTrnsrView.hbxNoteToReceipent.setVisibility(false);
                                frmIBFTrnsrEditCnfmtn.hbxNotifyReceipent.setVisibility(false);
                                frmIBFTrnsrEditCnfmtn.hbxNoteToReceipent.setVisibility(false);

                                frmIBFTrnsrView.imgToAccntFT.src = ownProfilePic;
                                frmIBFTrnsrEditCnfmtn.imgToAccntFT.src = ownProfilePic;


                            } else {
                                
                                frmIBFTrnsrView.hbxNotifyReceipent.setVisibility(true);
                                frmIBFTrnsrView.hbxNoteToReceipent.setVisibility(true);
                                frmIBFTrnsrEditCnfmtn.hbxNotifyReceipent.setVisibility(true);
                                frmIBFTrnsrEditCnfmtn.hbxNoteToReceipent.setVisibility(true);

                                if (gblpersonalizedPicId != null && gblpersonalizedPicId != "") {
                                    if (gblpersonalizedPicId.indexOf("fbcdn") >= 0) {
                                        frmIBFTrnsrView.imgToAccntFT.src = gblpersonalizedPicId;
                                        frmIBFTrnsrEditCnfmtn.imgToAccntFT.src = gblpersonalizedPicId;
                                    } else {
                                        frmIBFTrnsrView.imgToAccntFT.src = recepentImageURL;
                                        frmIBFTrnsrEditCnfmtn.imgToAccntFT.src = recepentImageURL;
                                    }
                                } else {
                                    frmIBFTrnsrView.imgToAccntFT.src = recepentImageURL;
                                    frmIBFTrnsrEditCnfmtn.imgToAccntFT.src = recepentImageURL;
                                }
                            }

                            //if (Accnts_RelTable[accIndex].accntNickName != null || Accnts_RelTable[accIndex].accntNickName != "")  frmIBFTrnsrView.lblToAccntNickName.text = Accnts_RelTable[accIndex].accntNickName;

                            frmIBFTrnsrView.lblToAccntName.setVisibility(true);
                        }
                    }

                } else {

                    /*
		        frmIBFTrnsrView.hbxTrnsfrTo.setVisibility(false);
				frmIBFTrnsrView.hbxToAccntHidenError.setVisibility(true);
				frmIBFTrnsrView.rchtxtErrorToAccnt.setVisibility(true);
				frmIBFTrnsrView.rchtxtErrorToAccnt.text =  kony.i18n.getLocalizedString("keyErrorMsgToaccntDeletnFT") +"<a onclick= \"return addReceipentFT();\" href = \"#\" ; style= 'color:blue' > "+kony.i18n.getLocalizedString("keyMyReceipentList")+" </a>" +kony.i18n.getLocalizedString("keyErrorMsgFT2");
				frmIBFTrnsrView.rchtxtErrorToAccnt.skin = lblIB18pxBlack
				*/

                    //frmIBFTrnsrView.lblToAccntNickName.text = "";
                    frmIBFTrnsrView.imgToAccntFT.src = "avatar_dis.png";
					frmIBFTrnsrView.lblToAccntName.text = "";
					frmIBFTrnsrView.lblToAccntNickName.text = "";
                    //Disabling all btn clicks	
                    //frmIBFTrnsrView.btnFTEditFlow.setEnabled(false);
                    //frmIBFTrnsrView.btnFTEdit.setEnabled(false);
                    //frmIBFTrnsrView.btnNxt.setEnabled(false);
                    //frmIBFTrnsrView.btnCancel.setEnabled(false);
                   // frmIBFTrnsrView.btnFTEditFlow.skin = "btnEditDisabled";
                    //frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnEditDisabled";
                    //frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnEditDisabled";
                }

                if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
                    if (gblFrmAccntType == "CDA") {
                    	var length = gblFrmAccntID.length;
                    	if(length == "13"){
                    		var accuntTemp = gblFrmAccntID.substring(0, length-3);
                    		getTDAccountDetailsFT(accuntTemp, gblFrmAccntfIIdentFT);
                    	}else
                            getTDAccountDetailsFT(gblFrmAccntID, gblFrmAccntfIIdentFT);

                    } else if (gblToAccntType == "CDA") {
                    	var len = gblToAccntID.length;
                    	if(len == "13"){
                    		var accTemp = gblToAccntID.substring(0, len-3);
                    		getTDAccountDetailsFT(accTemp, gblToAccntfIIdentFT);
                    	}else	
                           getTDAccountDetailsFT(gblToAccntID, gblToAccntfIIdentFT);

                    } else
                        feeTrnsfrInq();

                } else
                    srvGetBankTypeFT(gblToBankCdFT); //calling getBankDetails Service to get the Bank ShortName

            } else {
                dismissLoadingScreenPopup();
                alert("No records in Account Relation Table");
                return false;
            }

        } else {

            //New changes 24/12
            
            var accntFound = false;
            isTMB = false;
            var accIndex = "";
            var tmpAccnt = gblToAccntID;
            if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
                if (gblToAccntType == "SDA" || gblToAccntType == "CDA") {
                    if (gblToAccntID.length == 10)
                        tmpAccnt = "0000" + tmpAccnt;
                }
            }

            // Searching TO-Accnt in response of customerAccntInq  

            for (var k = 0; k < TMBAccntData.length; k++) {
                if (tmpAccnt == TMBAccntData[k].accntNum) {
                    accIndex = k;
                    gblAccntName_ORFT = TMBAccntData[k].accntName;
                    if (TMBAccntData[k].bankcd != "") {
                        gblToBankCdFT = TMBAccntData[k].bankcd;
                        isTMB = false;
                    } else {
                        gblToBankCdFT = "11";
                        isTMB = true;
                    }

                    gblToAccntfIIdentFT = TMBAccntData[k].fiident;
                    //isTMB = true;
                    accntFound = true;
                    if (TMBAccntData[k].accntStatus == "02") {
                        accntFound = false;
                    } else {
                        frmIBFTrnsrView.lblToAccntName.text = TMBAccntData[k].accntName;
                        // frmIBFTrnsrView.lblToAccntNickName.text = TMBAccntData[k].nickName;
                    }
                }
            }

            if (accntFound == true) {
                // gblToBankCdFT = Accnts_RelTable[accIndex].bankcd; 

                frmIBFTrnsrView.btnFTEdit.setEnabled(true);
                frmIBFTrnsrView.btnNxt.setEnabled(true);
                frmIBFTrnsrView.btnCancel.setEnabled(true);

                frmIBFTrnsrView.hbxTrnsfrTo.setVisibility(true);
                frmIBFTrnsrView.hbxToAccntHidenError.setVisibility(false);
                frmIBFTrnsrView.rchtxtErrorToAccnt.setVisibility(false);

                frmIBFTrnsrView.hbxNotifyReceipent.setVisibility(false);
                frmIBFTrnsrView.hbxNoteToReceipent.setVisibility(false);
                frmIBFTrnsrEditCnfmtn.hbxNotifyReceipent.setVisibility(false);
                frmIBFTrnsrEditCnfmtn.hbxNoteToReceipent.setVisibility(false);

                if (frmIBFTrnsrView.hbxFrmAccntHidenError.isVisible == false) {
                    frmIBFTrnsrView.btnFTEditFlow.setEnabled(true);
                    frmIBFTrnsrView.btnFTEditFlow.skin = "btnIBediticonsmall";
                    frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnIbeditsmalliconHover";
                    frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnIbeditsmalliconHover";
                } else {
                    frmIBFTrnsrView.btnFTEditFlow.setEnabled(false);
                    frmIBFTrnsrView.btnFTEditFlow.skin = "btnEditDisabled";
                    frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnEditDisabled";
                    frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnEditDisabled";
                }

                var ownProfilePic = "";
                ownProfilePic = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=Y&personalizedId=&billerId=";

                frmIBFTrnsrView.imgToAccntFT.src = ownProfilePic; //"avatar_dis.jpg";  //"icon.png"; 
                frmIBFTrnsrEditCnfmtn.imgToAccntFT.src = ownProfilePic; //"avatar_dis.jpg"; //"icon.png"; 


                if (isTMB == true) {
                    frmIBFTrnsrView.lblToAccntName.setVisibility(true); //Making visible as accnt name is not shown for other banks
                } else {
                    frmIBFTrnsrView.lblToAccntName.setVisibility(true);
                }

            } else {
                //Changes as per DEF15787
                /*
		        frmIBFTrnsrView.hbxTrnsfrTo.setVisibility(false);
				frmIBFTrnsrView.hbxToAccntHidenError.setVisibility(true);
				frmIBFTrnsrView.rchtxtErrorToAccnt.setVisibility(true);
				frmIBFTrnsrView.rchtxtErrorToAccnt.text =  kony.i18n.getLocalizedString("keyErrorMsgToaccntDeletnFT") +"<a onclick= \"return addReceipentFT();\" href = \"#\" ; style= 'color:blue' > "+kony.i18n.getLocalizedString("keyMyReceipentList")+" </a>" +kony.i18n.getLocalizedString("keyErrorMsgFT2");
				frmIBFTrnsrView.rchtxtErrorToAccnt.skin = lblIB18pxBlack
				*/

                //frmIBFTrnsrView.lblToAccntNickName.text = "";
                frmIBFTrnsrView.imgToAccntFT.src = "avatar_dis.png";

                //Disabling all btn clicks	
                //frmIBFTrnsrView.btnFTEditFlow.setEnabled(false);
                //frmIBFTrnsrView.btnFTEdit.setEnabled(false);
                //frmIBFTrnsrView.btnNxt.setEnabled(false);
                //frmIBFTrnsrView.btnCancel.setEnabled(false);
               // frmIBFTrnsrView.btnFTEditFlow.skin = "btnEditDisabled";
                //frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnEditDisabled";
                //frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnEditDisabled";
            }

            if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
                if (gblFrmAccntType == "CDA") {
                	//getTDAccountDetailsFT(gblFrmAccntID, gblFrmAccntfIIdentFT);
                	var len = gblFrmAccntID.length;
                	if(len == "13"){
                		var tempAcc = gblFrmAccntID.substring(0, len-3);
                		getTDAccountDetailsFT(tempAcc, gblFrmAccntfIIdentFT);
                	}else
	                    getTDAccountDetailsFT(gblFrmAccntID, gblFrmAccntfIIdentFT);
	                   
                } else if (gblToAccntType == "CDA") {
                	//getTDAccountDetailsFT(gblFrmAccntID, gblFrmAccntfIIdentFT);	
                		var len = gblToAccntID.length;
                		if(len == "13"){
                			var tempAcc = gblToAccntID.substring(0, len-3);
                			getTDAccountDetailsFT(tempAcc, gblToAccntfIIdentFT);
                		}else
		                    getTDAccountDetailsFT(gblToAccntID, gblToAccntfIIdentFT);
		                    
                } else
                    feeTrnsfrInq();

            } else
                srvGetBankTypeFT(gblToBankCdFT); //calling getBankDetails Service to get the Bank ShortName
        }
    }
}


//Service to check from account status , account nickname and account name

function getAccountListService() {
    var inputparam = {};
    inputparam["crmId"] = gblcrmId;
    inputparam["transferFlag"] = "true";
    inputparam["bankCd"] = "11"; // TMB_BANK_CD;
    invokeServiceSecureAsync("customerAccountInquiry", inputparam, getAccountListServiceCallBack);

}

function getAccountListServiceCallBack(status, resulttable) {

    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            gblOwnPersonlisedIdFT = "";
            gblFrmAccntNickEN_IB = "";
            var gblFrmAccntNickTH_IB = "";
            TMBAccntData = [];
            if (resulttable.custAcctRec != null) {
                for (var i = 0; i < resulttable.custAcctRec.length; i++) {
                    var nickName = "";
                    var accntStatus = "";
                    var bankcd = "";
                    var accntName = "";
                    var accntNum = "";
                    var ICON_ID = "";
                    var nickNameTH = "";
                    if (resulttable.custAcctRec[i].acctNickName != undefined && resulttable.custAcctRec[i].acctNickName != null && resulttable.custAcctRec[i].acctNickName != "") {
                        nickName = resulttable.custAcctRec[i].acctNickName;
                    } else {
                        var len = resulttable.custAcctRec[i].accId.length;
                        if (kony.i18n.getCurrentLocale() == "en_US")
                            nickName = resulttable.custAcctRec[i].productNmeEN + resulttable.custAcctRec[i].accId.substring(len - 4, len);
                    }
                    var len = resulttable.custAcctRec[i].accId.length;
                    if (resulttable.custAcctRec[i].acctNickNameTh != undefined)
                        nickNameTH = resulttable.custAcctRec[i].acctNickNameTh;
                    else
                        nickNameTH = "";


                    if (resulttable.custAcctRec[i].personalizedAcctStatus != undefined)
                        accntStatus = resulttable.custAcctRec[i].personalizedAcctStatus;
                    else
                        accntStatus = "01";
                    if (resulttable.custAcctRec[i].bankCD != null)
                        bankcd = resulttable.custAcctRec[i].bankCD;

                    if (resulttable.custAcctRec[i].accountName != null && resulttable.custAcctRec[i].accountName != "")
                        accntName = resulttable.custAcctRec[i].accountName;
                    if (resulttable.custAcctRec[i].accId != null && resulttable.custAcctRec[i].accId != "")
                        accntNum = resulttable.custAcctRec[i].accId;
                    if (resulttable.custAcctRec[i].ICON_ID != null && resulttable.custAcctRec[i].ICON_ID != "")
                        ICON_ID = resulttable.custAcctRec[i].ICON_ID;

                    var availBal = resulttable.custAcctRec[i].availableBal;
                    var numOfFreeTrnsctns = resulttable.custAcctRec[i].remainingFee;
                    var prodCode = resulttable.custAcctRec[i].productID;
                    var fiident = resulttable.custAcctRec[i].fiident;

                    if (resulttable.custAcctRec[i].persionlizedId != null && resulttable.custAcctRec[i].persionlizedId != "")
                        gblOwnPersonlisedIdFT = resulttable.custAcctRec[i].persionlizedId;


                    TMBAccntData.push({
                        "nickName": nickName,
                        "accntStatus": accntStatus,
                        "bankcd": bankcd,
                        "accntName": accntName,
                        "accntNum": accntNum,
                        "availBal": availBal,
                        "numOfFreeTrnsctns": numOfFreeTrnsctns,
                        "prodCode": prodCode,
                        "fiident": fiident,
                        "ICON_ID": ICON_ID,
                        "nickNameTH": nickNameTH

                    });

                }

                if (resulttable.OtherAccounts.length > 0) {
                    for (var i = 0; i < resulttable.OtherAccounts.length; i++) {
                        var nickName = "";
                        var accntStatus = "";
                        var bankcd = "";
                        var accntName = "";
                        var accntNum = "";
                        if (resulttable.OtherAccounts[i].acctNickName != undefined && resulttable.OtherAccounts[i].acctNickName != null && resulttable.OtherAccounts[i].acctNickName != "") {
                            nickName = resulttable.OtherAccounts[i].acctNickName
                        } else {
                            var len = resulttable.OtherAccounts[i].accId.length;
                            if (kony.i18n.getCurrentLocale() == "en_US") {
                                nickName = resulttable.OtherAccounts[i].productNmeEN + resulttable.OtherAccounts[i].accId.substring(len - 4, len);

                            } else if (kony.i18n.getCurrentLocale() == "th_TH") {
                                nickName = resulttable.OtherAccounts[i].productNmeTH + resulttable.OtherAccounts[i].accId.substring(len - 4, len);
                            }

                        }
                        if (resulttable.OtherAccounts[i].personalizedAcctStatus != undefined)
                            accntStatus = resulttable.OtherAccounts[i].personalizedAcctStatus;
                        else
                            accntStatus = "01";
                        if (resulttable.OtherAccounts[i].bankCD != null)
                            bankcd = resulttable.OtherAccounts[i].bankCD;


                        if (resulttable.OtherAccounts[i].accountName != null && resulttable.OtherAccounts[i].accountName != "")
                            accntName = resulttable.OtherAccounts[i].accountName;
                        if (resulttable.OtherAccounts[i].accId != null && resulttable.OtherAccounts[i].accId != "")
                            accntNum = resulttable.OtherAccounts[i].accId;


                        var availBal = resulttable.OtherAccounts[i].availableBal;
                        var numOfFreeTrnsctns = resulttable.OtherAccounts[i].remainingFee;
                        var prodCode = resulttable.OtherAccounts[i].productID;
                        var fiident = resulttable.OtherAccounts[i].fiident;

                        TMBAccntData.push({
                            "nickName": nickName,
                            "accntStatus": accntStatus,
                            "bankcd": bankcd,
                            "accntName": accntName,
                            "accntNum": accntNum,
                            "availBal": availBal,
                            "numOfFreeTrnsctns": numOfFreeTrnsctns,
                            "prodCode": prodCode,
                            "fiident": fiident

                        });
                    }
                }

                // Checking whether the "From account" is existing or not in CustmerAccntInq Response  
                for (var k = 0; k < TMBAccntData.length; k++) {
                    var tempAccnt = gblFrmAccntID;

                    if (gblFrmAccntType == "SDA" && gblFrmAccntID.length == 10)
                        tempAccnt = "0000" + tempAccnt;
                    if (gblFrmAccntType == "CDA" && gblFrmAccntID.length == 10)
                        tempAccnt = "0000" + tempAccnt;


                    if (tempAccnt == TMBAccntData[k].accntNum) {

                        gblFreeTransactions = TMBAccntData[k].numOfFreeTrnsctns; // Saving to use for getTransferFee
                        gblFrmaccntProdCode = TMBAccntData[k].prodCode; // Saving to use for getTransferFee & TMB logo assignmnt
                        gblFrmAccntfIIdentFT = TMBAccntData[k].fiident;

                        //if( TMBAccntData[k].bankcd == "11" ){

                        gblFrmAccntNickEN_IB = TMBAccntData[k].nickName;
                        if (TMBAccntData[k].nickNameTH != "") {
                            gblFrmAccntNickTH_IB = TMBAccntData[k].nickNameTH;
                        } else
                            gblFrmAccntNickTH_IB = "";

                        if (TMBAccntData[k].accntStatus == "02") {
                            //Show link with add myaccount link;
                            //frmIBFTrnsrView.rchtxtError.setVisibility(true);

                            frmIBFTrnsrView.hbxFrmAccntHidenError.setVisibility(true);
                            frmIBFTrnsrView.hbxFromAccnt.setVisibility(false);
                            frmIBFTrnsrView.rchtxtErrorFrmAccnt.setVisibility(true);
                            frmIBFTrnsrView.rchtxtErrorFrmAccnt.text = kony.i18n.getLocalizedString("keyErrorMsgFT1") + "<a onclick= \"return addMyAccntFT();\" href = \"#\" ; style= 'color:blue' > " + kony.i18n.getLocalizedString("S2S_MyAcctList") + " </a>" + kony.i18n.getLocalizedString("keyErrorMsgFT2");

                            frmIBFTrnsrView.rchtxtErrorFrmAccnt.skin = lblIB18pxBlack // Have to give red font color skin as per FS
                            frmIBFTrnsrView.btnFTEditFlow.setEnabled(false);
                            frmIBFTrnsrView.btnFTEditFlow.skin = "btnEditDisabled";
                            frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnEditDisabled";
                            frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnEditDisabled";
                            //Disabling all btn clicks 
                            frmIBFTrnsrView.btnFTEdit.setEnabled(true);
                            frmIBFTrnsrView.btnNxt.setEnabled(false);
                            frmIBFTrnsrView.btnCancel.setEnabled(false);

                        } else {
                            frmIBFTrnsrView.btnFTEditFlow.skin = "btnIBediticonsmall";
                            frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnIbeditsmalliconHover";
                            frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnIbeditsmalliconHover";
                            frmIBFTrnsrView.btnFTEditFlow.setEnabled(true);
                            frmIBFTrnsrView.hbxFrmAccntHidenError.setVisibility(false);
                            frmIBFTrnsrView.hbxFromAccnt.setVisibility(true);
                            frmIBFTrnsrView.rchtxtErrorFrmAccnt.setVisibility(false);
                            frmIBFTrnsrView.btnFTEdit.setEnabled(true);
                            frmIBFTrnsrView.btnNxt.setEnabled(true);
                            frmIBFTrnsrView.btnCancel.setEnabled(true);

                            frmIBFTrnsrView.lblFrmAccntName.setVisibility(true);
                            if (frmIBFTrnsrView.lblFrmAccntName.text != null || frmIBFTrnsrView.lblFrmAccntName.text != "")
                                frmIBFTrnsrView.lblFrmAccntName.text = TMBAccntData[k].accntName;
                            if (frmIBFTrnsrView.lblFrmAccntNickName.text != null || frmIBFTrnsrView.lblFrmAccntNickName.text != "")
                                frmIBFTrnsrView.lblFrmAccntNickName.text = TMBAccntData[k].nickName;

                            var amt = commaFormatted(TMBAccntData[k].availBal);
                            frmIBFTrnsrView.lblFTEditAftrAvailBalVal.text = amt + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

                            var tmp_ICON_ID = TMBAccntData[k].ICON_ID;
                            var frmProdIcon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + tmp_ICON_ID + "&modIdentifier=PRODICON";
                            //var frmProdIcon = "https://vit.tau2904.com:443/tmb/ImageRender?crmId=&&personalizedId=&billerId="+tmp_ICON_ID+"&modIdentifier=PRODICON";                                                                                                                                                    
                            
                            frmIBFTrnsrView.imgFrmAccntFT.src = frmProdIcon + ".png";
                            frmIBFTrnsrEditCnfmtn.imgFrmAccntFT.src = frmProdIcon + ".png";

                        }

                        //}
                    }

                }

                gblSMARTTransAmnt = resulttable["SMARTTransAmnt"];
                gblORFTPerTransAmntLimt = resulttable["ORFTTransSplitAmnt"];
                gblIBStatus = resulttable["IBStatus"];
                                                                
                //Fee caluculation Logic for ORFT
                var trnferType = gblpmtMethodFT;

                //ORFT var
                var feeORFTAmt = 0.00;
                var ORFT_FREE_TRANS_CODES = resulttable["ORFT_FREE_TRANS_CODES"]; //"219"
                var freeTransactions = "";
                var ORFTRange1Lower = resulttable["ORFTRange1Lower"]; //"0"
                ORFTRange1Lower = parseFloat(ORFTRange1Lower.toString());
                var ORFTRange1Higher = resulttable["ORFTRange1Higher"] //"20000"
                ORFTRange1Higher = parseFloat(ORFTRange1Higher.toString());

                var ORFT_FEE_AMNT1 = resulttable["ORFTSPlitFeeAmnt1"] //"25"
                var ORFT_FEE_AMNT2 = resulttable["ORFTSPlitFeeAmnt2"] //"35"

                var ORFT_FEE_RANGE1_Lower2 = resulttable["ORFTRange2Lower"]; //"20001"
                ORFT_FEE_RANGE1_Lower2 = parseFloat(ORFT_FEE_RANGE1_Lower2.toString());

                gblORFTRANGE2 = resulttable["ORFT_FEE_RANGE1"]; //"20001-50000"
                gblORFT_FEE_AMNT2 = resulttable["ORFTSPlitFeeAmnt2"] //"35"
                gblORFT_FEE_AMNT1 = resulttable["ORFTSPlitFeeAmnt1"] //"25"

                var temp = resulttable["ORFT_FEE_RANGE1"]; //"20001-50000"
                temp = temp.split("-", 2);

                var ORFT_FEE_RANGE1_Higher2 = temp[1]; //"50000"
                ORFT_FEE_RANGE1_Higher2 = parseFloat(ORFT_FEE_RANGE1_Higher2.toString());

                //SMART var
                var SMART_FREE_TRANS_CODES = resulttable["SMART_FREE_TRANS_CODES"]; //"219,220,222"
                var tempSmart = resulttable["SMART_FEE_RANGE1"]; //"0-100000"
                tempSmart = tempSmart.split("-", 2);

                var SMART_FEE_RANGE1_Lower1 = tempSmart[0];
                SMART_FEE_RANGE1_Lower1 = parseFloat(SMART_FEE_RANGE1_Lower1.toString());

                var SMART_FEE_RANGE1_Higher1 = tempSmart[1];
                SMART_FEE_RANGE1_Higher1 = parseFloat(SMART_FEE_RANGE1_Higher1.toString());
                var SMART_FEE_AMNT1 = resulttable["SMART_FEE_AMNT_RANGE1"];
                var feeSMARTAmt = 0.00;

                var amtTransr = frmIBFTrnsrView.lblFTViewAmountVal.text
                amtTransr = amtTransr.substring("0", amtTransr.length - 1);

                if (amtTransr.indexOf(",") != -1) {
                    amtTransr = replaceCommon(amtTransr, ",", "");
                }
                amtTransr = parseFloat(amtTransr.toString());
                if (trnferType == "ORFT") {
                    freeTransactions = 0;

					if (gblORFT_ALL_FREE_TRANS_CODES.indexOf(gblFrmaccntProdCode) >= 0) {
                    	freeTransactions = 1;
                    }
                    if (freeTransactions == 0) {
                        if (amtTransr > ORFTRange1Lower && amtTransr <= ORFTRange1Higher) {
                            feeORFTAmt = ORFT_FEE_AMNT1;
                        } else if (amtTransr > ORFT_FEE_RANGE1_Lower2 && amtTransr <= ORFT_FEE_RANGE1_Higher2) {
                            feeORFTAmt = ORFT_FEE_AMNT2;
                        }
                        frmIBFTrnsrView.lblFeeVal.text = eval(feeORFTAmt).toFixed(2) + kony.i18n.getLocalizedString("currencyThaiBaht");
                    } else
                        frmIBFTrnsrView.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht");

                } else if (trnferType == "SMART") {
                    freeTransactions = 0;

                    if (gblALL_SMART_FREE_TRANS_CODES.indexOf(gblFrmaccntProdCode) >= 0) {
                                freeTransactions = 1;
                    }
                    if (freeTransactions == 0) {
                        if (amtTransr > SMART_FEE_RANGE1_Lower1 && amtTransr <= SMART_FEE_RANGE1_Higher1) {
                            feeSMARTAmt = SMART_FEE_AMNT1;
                            frmIBFTrnsrView.lblFeeVal.text = eval(feeSMARTAmt).toFixed(2) + kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                    } else
                        frmIBFTrnsrView.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");

                }else if (trnferType == "PROMPTPAY_ACCOUNT") {
				
                  setToAccPromptPayGlobalVars(resulttable); 
                  freeTransactions = 0;
                                                                   if (gblTOACC_PP_FREE_PROD_CODES.indexOf(gblFrmaccntProdCode) >= 0) {
                                freeTransactions = 1;
                    }
                    if (freeTransactions == 0) {
                      if (amtTransr > gbltoAccPromptPayRange1Lower && amtTransr <= gbltoAccPromptPayRange1Higher) {
                            feeORFTAmt = gbltoAccPromptPaySPlitFeeAmnt1;
                                                                                                                frmIBFTrnsrView.lblFeeVal.text = eval(feeORFTAmt).toFixed(2) + kony.i18n.getLocalizedString("currencyThaiBaht");
                        } else if (amtTransr > gbltoAccPromptPayRange2Lower && amtTransr <= gbltoAccPromptPayRange2Higher) {
                            feeORFTAmt = gbltoAccPromptPaySPlitFeeAmnt2;
                            frmIBFTrnsrView.lblFeeVal.text = eval(feeORFTAmt).toFixed(2)+ kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                    } else frmIBFTrnsrView.lblFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                                
                gblFTFeeView = frmIBFTrnsrView.lblFeeVal.text;
				
                getAllAccountsInfo(); //Call personalisedInq service

            } else {
                dismissLoadingScreenPopup();
                alert("No Accounts");
                return false;
            }

        } else {
            dismissLoadingScreenPopup();
            alert(resulttable["errMsg"]);
            return false;
        }
    }

}

// PaymentInquiry service to get payment details

function srvPaymentInq() {
    showLoadingScreenPopup();
    //Handling the visibility of schedule details hboxes
    frmIBFTrnsrView.hbxTrnsfrDetails2.setVisibility(false);
    frmIBFTrnsrView.hbxTrnsfrDetails2ForView.setVisibility(true);
    frmIBFTrnsrView.hbxEditFT.setVisibility(false);

    if (frmIBFTrnsrView.btnFTEditFlow.isVisible == false) {
        frmIBFTrnsrView.btnFTEditFlow.setVisibility(true);
    }
    if (frmIBFTrnsrView.btnFTDel.isVisible == false) {
        frmIBFTrnsrView.btnFTDel.setVisibility(true);
    }

    var inputparam = {};
    inputparam["scheRefID"] = gblSchduleRefIDFT;
    //inputparam["scheRefID"] = "ST0013000000023457";   //13000000011409 //0013000000012296 -with to accnt hidden
    inputparam["rqUID"] = "";
    invokeServiceSecureAsync("doPmtInq", inputparam, srvPaymentInqCallBack);

}

function srvPaymentInqCallBack(status, resulttable) {

    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
                if (resulttable["PmtInqRs"].length == 0) {
                    dismissLoadingScreenPopup();
                    alert("" + resulttable["AdditionalStatusDesc"]);
                    return false;
                }

                gblIsEditAftrFrmService = false;
                gblIsEditOnDateFrmService = false;
                gblIsEditAftr = false;
                gblIsEditOnDate = false;
                gblScheduleEndFT = "none"

                gblFrmAccntID = resulttable["PmtInqRs"][0]["FromAccId"]
                gblFrmAccntType = resulttable["PmtInqRs"][0]["FromAccType"]
                gblToAccntID = resulttable["PmtInqRs"][0]["ToAccId"]
                gblToAccntType = resulttable["PmtInqRs"][0]["ToAccType"]

                if (resulttable["PmtInqRs"][0]["BankId"] != undefined){
                	gblToBankCdFTTmp = resulttable["PmtInqRs"][0]["BankId"]; 
                    gblToBankCdFT = resulttable["PmtInqRs"][0]["BankId"]; //gblToBankIdFT
                }
                  	
                gblTransfrAmt = resulttable["PmtInqRs"][0]["Amt"]
                gblTransCodeFT = resulttable["PmtInqRs"][0]["TransCode"];
                gblOrderDateFT = resulttable["PmtInqRs"][0]["InitiatedDt"];
                gblpmtMethodFT = resulttable["PmtInqRs"][0]["PmtMethod"];
                gblToAccntNick = resulttable["PmtInqRs"][0]["ToAcctNickname"];
                
                 if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
			        var len = gblToAccntID.length;
			        if (gblToAccntType == "CDA") 
			        	if(len == "13")gblToAccntID = gblToAccntID.substring(0, len-3);
				    if(!isNotBlank(gblToBankCdFTTmp) || !isNotBlank(gblToBankCdFT)){
						if(gblToBankCdFTTmp == "" || gblToBankCdFT == "")
						{
							gblToBankCdFTTmp = gblTMBBankCD;
							gblToBankCdFT = gblTMBBankCD;
						}
					}
                 }
				
                frmIBFTrnsrView.lblToAccntNickName.text = resulttable["PmtInqRs"][0]["ToAcctNickname"];
                frmIBFTrnsrView.lblFTViewAmountVal.text = commaFormatted(resulttable["PmtInqRs"][0]["Amt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                //var tmp = gblFrmAccntID;
				//masking CR code
				var tmp = resulttable["PmtInqRs"][0]["maskedFromAccId"];
                tmp = tmp.substring(tmp.length - 10, tmp.length); // Last 10 digit
                frmIBFTrnsrView.lblFrmAccntNumber.text = accntNumFormat(tmp); //gblFrmAccntID;  //Formatting the AccntNum

                //var tmpToAcnt = gblToAccntID;
				//masking CR code
				var tmpToAcnt = resulttable["PmtInqRs"][0]["maskedToAccId"];
                if (tmpToAcnt.length >= 10) {
                	if (gblpmtMethodFT != "SMART" && gblpmtMethodFT != "ORFT" && gblpmtMethodFT !="PROMPTPAY_ACCOUNT"){
                    	if(resulttable["PmtInqRs"][0]["ToAccType"]=="CDA"){
	                    	if(tmpToAcnt.length == 13){
	                    	 	tmpToAcnt = tmpToAcnt.substring(tmpToAcnt.length - 13, tmpToAcnt.length-3);//first 10 digit
	                    	}
	                    	frmIBFTrnsrView.lblToAccntNumber.text = accntNumFormat(tmpToAcnt);//for term deposit acc
	                    }else{
	                        tmpToAcnt = tmpToAcnt.substring(tmpToAcnt.length - 10, tmpToAcnt.length); // Last 10 digit
	                        frmIBFTrnsrView.lblToAccntNumber.text = accntNumFormat(tmpToAcnt);//for other acc
                        }
                    }else {
						if(resulttable["PmtInqRs"][0]["ToAccType"]=="CDA"){
	                    	if(tmpToAcnt.length == 13){
	                    		tmpToAcnt = tmpToAcnt.substring(tmpToAcnt.length - 13, tmpToAcnt.length-3);//first 10 digit
	                    	}
                    		frmIBFTrnsrView.lblToAccntNumber.text = accntNumFormat(tmpToAcnt);
						}else{
	                        if(tmpToAcnt.length == 10){
								frmIBFTrnsrView.lblToAccntNumber.text = accntNumFormat(tmpToAcnt);
							}else{
								frmIBFTrnsrView.lblToAccntNumber.text = tmpToAcnt;
							}
	                    }
                    }
                } else frmIBFTrnsrView.lblToAccntNumber.text = tmpToAcnt; //gblToAccntID;  //Formatting the AccntNum

                frmIBFTrnsrView.lblOrderDateVal.text = dateFormatForDisplayWithTimeStamp(resulttable["PmtInqRs"][0]["InitiatedDt"]);
                frmIBFTrnsrView.lblViewStartOnDateVal.text = formatDateFT(resulttable["PmtInqRs"][0]["Duedt"])
                frmIBFTrnsrView.lblViewStartOnDateValView.text = formatDateFT(resulttable["PmtInqRs"][0]["Duedt"]); //New
                gblNotificationType = "";
                if (resulttable["PmtInqRs"][0]["NotificationType"] != undefined) {
                    gblNotificationType = resulttable["PmtInqRs"][0]["NotificationType"];
                    if (resulttable["PmtInqRs"][0]["RecipientEmailAddr"] != undefined && (resulttable["PmtInqRs"][0]["NotificationType"] == "Email" || resulttable["PmtInqRs"][0]["NotificationType"] == "1") ) {
                        frmIBFTrnsrView.lblNotifyRecipientVal.text = resulttable["PmtInqRs"][0]["RecipientEmailAddr"];
                    } else if (resulttable["PmtInqRs"][0]["RecipientMobileNbr"] != undefined && ( resulttable["PmtInqRs"][0]["NotificationType"] == "SMS" || resulttable["PmtInqRs"][0]["NotificationType"] == "0") ) {
                    	var phoneNo = resulttable["PmtInqRs"][0]["RecipientMobileNbr"];
                        frmIBFTrnsrView.lblNotifyRecipientVal.text = phoneNo;
                    } else {
                        frmIBFTrnsrView.lblNotifyRecipientVal.text = "-";
                    }
                } else {
                    frmIBFTrnsrView.lblNotifyRecipientVal.text = "-";
                }

                if (resulttable["PmtInqRs"][0]["RecipientNote"] != undefined) {
                    frmIBFTrnsrView.lblNoteToRecipientVal.text = replaceHtmlTagChars(resulttable["PmtInqRs"][0]["RecipientNote"]);

                } else
                    frmIBFTrnsrView.lblNoteToRecipientVal.text = "-";

                if (resulttable["PmtInqRs"][0]["MyNote"] != undefined) {
                    frmIBFTrnsrView.lblMyNoteVal.text = replaceHtmlTagChars(resulttable["PmtInqRs"][0]["MyNote"]);
                } else
                    frmIBFTrnsrView.lblMyNoteVal.text = "-";

                var scheRefNo = resulttable["PmtInqRs"][0]["scheRefNo"];
                //scheRefNo = scheRefNo.replace("S", "F");
                frmIBFTrnsrView.lblScheduleRefNoVal.text = scheRefNo;

                if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Daily" || resulttable["PmtInqRs"][0]["RepeatAs"] == "Weekly" || resulttable["PmtInqRs"][0]["RepeatAs"] == "Monthly" || resulttable["PmtInqRs"][0]["RepeatAs"] == "Annually") {
                    gblViewRepeatAsLS = gblRepeatAsforLS = resulttable["PmtInqRs"][0]["RepeatAs"];
                    frmIBFTrnsrView.lblRepeatAsVal.text = resulttable["PmtInqRs"][0]["RepeatAs"];
                    frmIBFTrnsrView.lblRepeatAsValView.text = resulttable["PmtInqRs"][0]["RepeatAs"]; //New

                    gblFTViewRepeatAsVal = resulttable["PmtInqRs"][0]["RepeatAs"];

                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Daily")
                        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Daily");
                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Weekly")
                        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Weekly");
                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Monthly")
                        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Monthly");
                    if (resulttable["PmtInqRs"][0]["RepeatAs"] == "Annually") {
                        gblViewRepeatAsLS = gblRepeatAsforLS = "Yearly";
                        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Yearly");
                        gblFTViewRepeatAsVal = kony.i18n.getLocalizedString("Transfer_Yearly")
                        frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Yearly");
                        frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("Transfer_Yearly"); //New
                    }
                    frmIBFTrnsrView.lblRepeatAsValView.text = gblScheduleRepeatFT //New

                } else {

                    frmIBFTrnsrView.lblRepeatAsVal.text = "Once";
                    frmIBFTrnsrView.lblRepeatAsValView.text = "Once";
                    gblScheduleRepeatFT = "Once";
                    gblFTViewRepeatAsVal = "Once";
                    gblViewRepeatAsLS = gblRepeatAsforLS = "Once";
                    //gblScheduleEndFT = "none";
                }

                //Storing globally to map the old data after schedule edit in edit page and then cancel click		
                gblFTViewInitiateDate = resulttable["PmtInqRs"][0]["InitiatedDt"];
                gblFTViewStartOnDate = resulttable["PmtInqRs"][0]["Duedt"];


                if (gblScheduleRepeatFT != "Once") {
                    if (resulttable["PmtInqRs"][0]["EndDate"] != undefined) {
                        frmIBFTrnsrView.lblEndOnDateVal.text = formatDateFT(resulttable["PmtInqRs"][0]["EndDate"]);
                        frmIBFTrnsrView.lblEndOnDateValView.text = formatDateFT(resulttable["PmtInqRs"][0]["EndDate"]); //New
                        gblFTViewEndDate = resulttable["PmtInqRs"][0]["EndDate"];

                        //Adding these to differentiate between After and OnDate to show with focus on click of schedule button	
                        gblIsEditAftrFrmService = false;
                        gblIsEditOnDateFrmService = true;
                        gblIsEditAftr = false;
                        gblIsEditOnDate = true;

                        gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_OnDate")
                        gblFTViewEndVal = kony.i18n.getLocalizedString("Transfer_OnDate");


                        frmIBFTrnsrView.lblExcuteVal.text = numberOfExecutionFT(frmIBFTrnsrView.lblViewStartOnDateVal.text, frmIBFTrnsrView.lblEndOnDateVal.text, gblScheduleRepeatFT);

                        frmIBFTrnsrView.lblExcuteValView.text = numberOfExecutionFT(frmIBFTrnsrView.lblViewStartOnDateVal.text, frmIBFTrnsrView.lblEndOnDateValView.text, gblScheduleRepeatFT); //New  
                        gblFTViewExcuteVal = numberOfExecutionFT(frmIBFTrnsrView.lblViewStartOnDateVal.text, frmIBFTrnsrView.lblEndOnDateVal.text, gblScheduleRepeatFT);
                        frmIBFTrnsrView.lblRemainingVal.text = "-";
                        frmIBFTrnsrView.lblRemainingValView.text = "-"; //New 							
                        gblFTViewExcuteremaing = "-";

                        gblFTViewExcuteremaing = parseFloat(gblFTViewExcuteVal.toString()) - parseFloat(resulttable["PmtInqRs"][0]["TotalProcessesTransactions"]); //have to chek					       
                        frmIBFTrnsrView.lblRemainingVal.text = gblFTViewExcuteremaing;
                        frmIBFTrnsrView.lblRemainingValView.text = gblFTViewExcuteremaing;


                    } else if (resulttable["PmtInqRs"][0]["ExecutionTimes"] != undefined) {
                        frmIBFTrnsrView.lblExcuteVal.text = resulttable["PmtInqRs"][0]["ExecutionTimes"]
                        frmIBFTrnsrView.lblExcuteValView.text = resulttable["PmtInqRs"][0]["ExecutionTimes"]; //New

                        gblIsEditAftrFrmService = true;
                        gblIsEditOnDateFrmService = false;
                        gblIsEditAftr = true;
                        gblIsEditOnDate = false;

                        var tempDate = formatDateFT(resulttable["PmtInqRs"][0]["Duedt"])
                        var times = resulttable["PmtInqRs"][0]["ExecutionTimes"]
                        gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_After");
                        gblFTViewEndVal = kony.i18n.getLocalizedString("Transfer_After");
                        frmIBFTrnsrView.lblEndOnDateVal.text = endDateCal(tempDate, times);
                        frmIBFTrnsrView.lblEndOnDateValView.text = endDateCal(tempDate, times); //New

                        gblFTViewExcuteVal = resulttable["PmtInqRs"][0]["ExecutionTimes"];
                        gblFTViewExcuteremaing = parseFloat(resulttable["PmtInqRs"][0]["ExecutionTimes"]) - parseFloat(resulttable["PmtInqRs"][0]["TotalProcessesTransactions"]); //have to chek
                        
                        // gblFTViewExcuteremaing = frmIBFTrnsrView.lblRemainingVal.text;
                        frmIBFTrnsrView.lblRemainingVal.text = gblFTViewExcuteremaing;
                        frmIBFTrnsrView.lblRemainingValView.text = gblFTViewExcuteremaing;


                        gblFTViewEndDate = frmIBFTrnsrView.lblEndOnDateVal.text;
                    } else if ((resulttable["PmtInqRs"][0]["EndDate"] == undefined) && (resulttable["PmtInqRs"][0]["ExecutionTimes"] == undefined)) {

                        frmIBFTrnsrView.lblExcuteVal.text = "-";
                        frmIBFTrnsrView.lblRemainingVal.text = "-";
                        frmIBFTrnsrView.lblEndOnDateVal.text = "-"

                        //New
                        frmIBFTrnsrView.lblExcuteValView.text = "-";
                        frmIBFTrnsrView.lblRemainingValView.text = "-";
                        frmIBFTrnsrView.lblEndOnDateValView.text = "-";

                        gblFTViewExcuteVal = "-";
                        gblFTViewExcuteremaing = "-";
                        gblFTViewEndDate = "-";
                        gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_Never");
                        gblFTViewEndVal = kony.i18n.getLocalizedString("Transfer_Never");

                    }

                } else {
                    gblScheduleEndFT = "none";
                    gblFTViewEndVal = "none";
                    frmIBFTrnsrView.lblEndOnDateVal.text = formatDateFT(resulttable["PmtInqRs"][0]["Duedt"]);
                    frmIBFTrnsrView.lblExcuteVal.text = "1";
                    frmIBFTrnsrView.lblRemainingVal.text = "1";

                    //New
                    frmIBFTrnsrView.lblExcuteValView.text = "1";
                    frmIBFTrnsrView.lblRemainingValView.text = "1";
                    frmIBFTrnsrView.lblEndOnDateValView.text = formatDateFT(resulttable["PmtInqRs"][0]["Duedt"]);

                    frmIBFTrnsrView.txtNumOfTimes.text = "";
                    gblFTViewExcuteVal = "1";
                    gblFTViewEndDate = formatDateFT(resulttable["PmtInqRs"][0]["Duedt"]);
                    gblFTViewExcuteremaing = "1";

                }

                if (resulttable["PmtInqRs"][0]["PmtMethod"] == "INTERNAL_TRANSFER") {
                    frmIBFTrnsrView.lblMethodName.text = "";
                    frmIBFTrnsrView.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrEditCnfmtn.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrView.btnDaily.setEnabled(true);

                } else if (resulttable["PmtInqRs"][0]["PmtMethod"] == "ORFT") {
                    frmIBFTrnsrView.lblMethodName.text = resulttable["XferExcutionTime"]; //DEF15194 changes 
                    frmIBFTrnsrView.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrEditCnfmtn.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrView.btnDaily.setEnabled(true);
                    // call ORFT trnfr fee cal service (feeEngine) 
                    //Caluculating the fee for ORFT // Logic without feeEngine service call in the response of cusomerAccountInq

                } else if (resulttable["PmtInqRs"][0]["PmtMethod"] == "SMART") {
                    frmIBFTrnsrView.lblMethodName.text = resulttable["XferExcutionTime"]; //DEF15194 changes
                    frmIBFTrnsrView.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrEditCnfmtn.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrView.btnDaily.setEnabled(false); //Diabling the "daily" button on schedule
                    // call SMART trnfr fee cal service (feeEngine) 
                    // Caluculating Fee for SMART	//Logic without feeEngine service call in the response of cusomerAccountInq
                }else if (resulttable["PmtInqRs"][0]["PmtMethod"] == "PROMPTPAY_ACCOUNT") {
                    frmIBFTrnsrView.lblMethodName.text = resulttable["XferExcutionTime"]; //DEF15194 changes
                    frmIBFTrnsrView.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrEditCnfmtn.lblFee.text = kony.i18n.getLocalizedString("keyXferFee");
                    frmIBFTrnsrView.btnDaily.setEnabled(true); //Diabling the "daily" button on schedule
                    // call SMART trnfr fee cal service (feeEngine) 
                    // Caluculating Fee for SMART	//Logic without feeEngine service call in the response of cusomerAccountInq
                }

                getAccountListService(); //Calling custmerAccntInq service

            } else {
                dismissLoadingScreenPopup();
                alert(resulttable["AdditionalStatusDesc"]);
                return false;
            }

        } else {
            dismissLoadingScreenPopup();
            //alert(resulttable["errmsg"]);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}


// Service to check TMB Internal Transfer is 0 or not 
function feeTrnsfrInq() {
    var inputParam = {};
    inputParam["fromAcctNo"] = gblFrmAccntID;
    inputParam["fromAcctTypeValue"] = gblFrmAccntType;
    inputParam["toAcctNo"] = gblToAccntID;
    inputParam["toAcctTypeValue"] = gblToAccntType;
    var tempAmt = frmIBFTrnsrView.lblFTViewAmountVal.text;
        
    tempAmt = parseFloat(removeCommos(tempAmt)).toFixed(2);

    inputParam["transferAmt"] = tempAmt;
    inputParam["transferDate"] = hypnFormatDateFT(frmIBFTrnsrView.lblViewStartOnDateVal.text) //yyyy-mm-dd
    inputParam["waiverCode"] = "I";
    inputParam["tranCode"] = gblTransCodeFT;

    invokeServiceSecureAsync("fundTransferInq", inputParam, feeTrnsfrInqcallBack);

}

function feeTrnsfrInqcallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
                if (resulttable["transferFee"] > 0) {
                    frmIBFTrnsrView.lblMethodName.text = resulttable["FlagFeeReg"];
                    frmIBFTrnsrView.lblFee.text = "Fee Cross Region:";
                    frmIBFTrnsrEditCnfmtn.lblFee.text = "Fee Cross Region:";
                    frmIBFTrnsrView.lblFeeVal.text = resulttable["transferFee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                } else {
                    frmIBFTrnsrView.lblMethodName.text = "";
                    frmIBFTrnsrView.lblFeeVal.text = resulttable["transferFee"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                }
                srvGetBankTypeFT(gblToBankCdFT);
            } else {
                dismissLoadingScreenPopup();
                alert(resulttable["errMsg"]);
                return false;
            }

        } else {
            dismissLoadingScreenPopup();
            alert(resulttable["StatusDesc"]);
            return false;
        }
    }
}

// to get TD details
function getTDAccountDetailsFT(acctID, fIIdent) {
    var inputParam = {}
    var fromAcctID = "";
    if (acctID.indexOf("-", 0) != -1) {
        //acctID = kony.string.replace(acctID, "-", "");
        acctID = replaceCommon(acctID, "-", "");
    }

    if (acctID.length == 14) {
        acctID = acctID.substring(acctID.length - 10, acctID.length);
    }

    inputParam["acctIdentValue"] = acctID;
    inputParam["fIIdent"] = fIIdent;
    //showLoadingScreenPopup();
    invokeServiceSecureAsync("tDDetailinq", inputParam, getTDAccountDetailsFTCallBack);

}

function getTDAccountDetailsFTCallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //resulttable.tdDetailsRec[0].depositNo  //&& gblToBankCdFT == "11"
			if(resulttable.tdDetailsRec.length == 0){
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			}

            var inputParam = {};
            inputParam["fromAcctTypeValue"] = gblFrmAccntType;
            inputParam["toAcctTypeValue"] = gblToAccntType;
            var tempAmt = frmIBFTrnsrView.lblFTViewAmountVal.text;
            
			tempAmt = parseFloat(removeCommos(tempAmt)).toFixed(2);
			
            inputParam["transferAmt"] = tempAmt;
            inputParam["transferDate"] = hypnFormatDateFT(frmIBFTrnsrView.lblViewStartOnDateVal.text) //yyyy-mm-dd
            inputParam["waiverCode"] = "I";
            inputParam["tranCode"] = gblTransCodeFT;

            if (gblFrmAccntType == "CDA" && gblToAccntType == "CDA" && gblFrmAccntdepositeNo == "") {
                if (gblFrmAccntdepositeNo == "") {
                    gblFrmAccntdepositeNo = resulttable.tdDetailsRec[0].depositNo;
                    getTDAccountDetailsFT(gblToAccntID, gblToAccntfIIdentFT);
                }
            } else {
                if (gblFrmAccntType == "CDA" && gblToAccntType == "CDA" && gblFrmAccntdepositeNo != "") {
                    var depositeNo = resulttable.tdDetailsRec[0].depositNo;
                    if (depositeNo.length == 1) {
                        depositeNo = "00" + depositeNo;
                    } else if (depositeNo.length == 2)
                        depositeNo = "0" + depositeNo;

                    if (gblFrmAccntdepositeNo.length == 1) {
                        gblFrmAccntdepositeNo = "00" + gblFrmAccntdepositeNo;
                    } else if (gblFrmAccntdepositeNo.length == 2)
                        gblFrmAccntdepositeNo = "0" + gblFrmAccntdepositeNo;

                    inputParam["fromAcctNo"] = gblFrmAccntID + gblFrmAccntdepositeNo;
                    inputParam["toAcctNo"] = gblToAccntID + depositeNo;
                    invokeServiceSecureAsync("fundTransferInq", inputParam, feeTrnsfrInqcallBack);
                } else {
                    var depositeNo = resulttable.tdDetailsRec[0].depositNo;
                    if (depositeNo.length == 1) {
                        depositeNo = "00" + depositeNo;
                    } else if (depositeNo.length == 2)
                        depositeNo = "0" + depositeNo;

                    if (gblFrmAccntType == "CDA")
                        inputParam["fromAcctNo"] = gblFrmAccntID + depositeNo;
                    else
                        inputParam["fromAcctNo"] = gblFrmAccntID;

                    if (gblToAccntType == "CDA")
                        inputParam["toAcctNo"] = gblToAccntID + depositeNo;
                    else
                        inputParam["toAcctNo"] = gblToAccntID;

                    invokeServiceSecureAsync("fundTransferInq", inputParam, feeTrnsfrInqcallBack);

                }

            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + resulttable["errMsg"]);
        }

    }
}

// Validation for Amount
function validateAmt() {
    var tempAmt = frmIBFTrnsrView.lblFTViewAmountVal.text;
    tempAmt = tempAmt.substring(0, tempAmt.length - 1)
    if (tempAmt.indexOf(",") != -1) {
        //tempAmt = kony.string.replace(tempAmt, ",", "");
        tempAmt = replaceCommon(tempAmt, ",", "");
    }

    tempAmt = parseFloat(tempAmt.toString())
    var tempAmt1 = frmIBFTrnsrView.txtFTEditAmountval.text;
    
    if (tempAmt1.length == 0 || tempAmt1 == "." || tempAmt1 == "," || tempAmt1.trim().length==1) {
        dismissLoadingScreenPopup();
        //alert("Enter Valid Amount!");
      	if(tempAmt1.length == 0 || tempAmt1 == "" || tempAmt1.trim().length == 0){
         	alert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"));
        }else{
         	alert(kony.i18n.getLocalizedString("Error_MinXfrAmountAllowed"));
        }
        return false;
    }

    if (tempAmt1.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) {
        tempAmt1 = replaceCommon(tempAmt1, kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    }

    tempAmt1 = tempAmt1.trim();
    if (tempAmt1.indexOf(",") != -1) {
        tempAmt1 = replaceCommon(tempAmt1, ",", "").trim();
    }

    var pat1 = /[^0-9.,]/g
    var pat2 = /[\s]/g
    var isAlpha = pat1.test(tempAmt1);
    var containsSpace = pat2.test(tempAmt1);
    if (isAlpha == true || containsSpace == true) {
        dismissLoadingScreenPopup();
        //alert("Enter Valid Amount!");
        alert(kony.i18n.getLocalizedString("Error_MinXfrAmountAllowed"));
        return false;
    }

    tempAmt1 = parseFloat(tempAmt1.toString());
    if (tempAmt1 < 0.01) {
        dismissLoadingScreenPopup();
        //alert("Min. Tranfer Amount Should be 0.01"+kony.i18n.getLocalizedString("currencyThaiBaht"));
        alert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"));
        return false;
    }
    var compare = (tempAmt == tempAmt1);
    if (compare == true && gblEditFTSchdule == false) {
        dismissLoadingScreenPopup();
        //alert("Change Either Amount or Schedule");
        alert(kony.i18n.getLocalizedString("Error_NoEditingDone"));
        return false;
    }

    var maxTransactnLimt = "";
    if (gblpmtMethodFT == "ORFT")
        maxTransactnLimt = gblORFTPerTransAmntLimt; //"50000"; //Getting it from gblORFTPerTransAmntLimt  ORFTTransSplitAmnt
    if (gblpmtMethodFT == "SMART")
        maxTransactnLimt = gblSMARTTransAmnt; //"100000"; //Getting it From gblSMARTTransAmnt - SMARTTransAmnt
	if(gblpmtMethodFT == "PROMPTPAY_ACCOUNT")
		maxTransactnLimt = gbltoAccPromptPayRange2Higher; //50000 configurable transaction limit for promptpay
		
    maxTransactnLimt = parseFloat(maxTransactnLimt.toString());

    frmIBFTrnsrEditCnfmtn.lblFeeVal.text = frmIBFTrnsrView.lblFeeVal.text;
    if (tempAmt1 > maxTransactnLimt) {
        dismissLoadingScreenPopup();
       	var amounttext = kony.i18n.getLocalizedString("TRErr_MaxLimitExceedLimitType");
       	amounttext = amounttext.replace("{MaxLimitperTypeAmount}", maxTransactnLimt);
      	alert(amounttext);
        return false;
    } else {
        if (gblpmtMethodFT == "ORFT") {
        	if (gblORFT_ALL_FREE_TRANS_CODES.indexOf(gblFrmaccntProdCode) >= 0) {
				frmIBFTrnsrEditCnfmtn.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht"); //"0"
            }else{
            	var tempRange = gblORFTRANGE2;
	            tempRange = tempRange.split("-"); //"20001-50000"
	            tempRange[0] = parseFloat(tempRange[0].toString());
	            if (tempAmt1 >= tempRange[0]) {
	                frmIBFTrnsrEditCnfmtn.lblFeeVal.text = eval(gblORFT_FEE_AMNT2).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); //"35"
	            } else
	                frmIBFTrnsrEditCnfmtn.lblFeeVal.text = eval(gblORFT_FEE_AMNT1).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); //"25"
            }
        }else if (gblpmtMethodFT == "PROMPTPAY_ACCOUNT") {
                    if (isNotBlank(gblTOACC_PP_FREE_PROD_CODES) && gblTOACC_PP_FREE_PROD_CODES.indexOf(gblFrmaccntProdCode) >= 0) {
                     	frmIBFTrnsrEditCnfmtn.lblFeeVal.text = "0.00 " + kony.i18n.getLocalizedString("currencyThaiBaht"); //"0"
                    }else{
                        if( tempAmt1 > gbltoAccPromptPayRange1Lower && tempAmt1 <= gbltoAccPromptPayRange1Higher){
                           frmIBFTrnsrEditCnfmtn.lblFeeVal.text = eval(gbltoAccPromptPaySPlitFeeAmnt1).toFixed(2) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                        }else if (tempAmt1 > gbltoAccPromptPayRange2Lower && tempAmt1 <= gbltoAccPromptPayRange2Higher) {
                          frmIBFTrnsrEditCnfmtn.lblFeeVal.text =eval(gbltoAccPromptPaySPlitFeeAmnt2).toFixed(2)+ kony.i18n.getLocalizedString("currencyThaiBaht");
                        }
                }
    	}	
    }
	srvTokenSwitchingDummy();
    //getFeeCrmProfileInq();

}



// Service to get daily channel n Transactn limit for ORFT n SMART
function getFeeCrmProfileInq() {
    
    showLoadingScreenPopup();
    var inputParam = {}
        //inputParam["crmId"] = gblcrmId;
    if (gblCRMProfileInqCal == false) {
        var amt = frmIBFTrnsrView.txtFTEditAmountval.text;
        var Fee = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
        var startOnDate = frmIBFTrnsrView.lblViewStartOnDateVal.text;
        startOnDate = changeDateFormatForService(startOnDate);
        if (amt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) {
            amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
        }
        
        if (amt.indexOf(",") != -1) {
       	    amt = parseFloat(replaceCommon(amt, ",", "")).toFixed(2);
   		}else
    	    amt = parseFloat(amt).toFixed(2);
    	
        if (Fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) {
            Fee = Fee.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
        }

        inputParam["module"] = "schedule"
        if (gblFrmAccntType == "CDA") {
            inputParam["module"] = "withdraw"
        }
        var pmtMethod = frmIBFTrnsrView.lblMethodName.text;
        if (gblpmtMethodFT == "SMART" || gblpmtMethodFT == "ORFT") {
            inputParam["module"] = "execute";
        }

		
        inputParam["AmtEdited"] = amt;				//amt;
        inputParam["toAccName"] = frmIBFTrnsrView.lblToAccntNickName.text;
        inputParam["toAccNum"] = frmIBFTrnsrView.lblToAccntNumber.text;
        inputParam["bankName"] = frmIBFTrnsrView.lblBankNameVal.text;
        gblisEditFTFlow = true;
        inputParam["isEditFTFlow"] = gblisEditFTFlow;
        inputParam["FeeFT"] = Fee;
        inputParam["TransferDate"] = startOnDate.trim();	//startOnDate.trim();

    }
    
    //inputParam["transferFlag"] = "true";
    invokeServiceSecureAsync("crmProfileInq", inputParam, getFeeCrmProfileInqcallBack)
}


function getFeeCrmProfileInqcallBack(status, resulttable) {

    if (status == 400) {

        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = "" //frmIBTransferNowConfirmation.lblAccountNo.text;
        var activityFlexValues3 = "";
        var activityFlexValues4 = "";
        var activityFlexValues5 = gblpmtMethodFT;
        if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
            if(isTMB)
            {
             activityFlexValues5 = "My TMB";
            }
            else
            {
             activityFlexValues5 = "Other TMB";
            }
        }
        
        var logLinkageId = "";

        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                activityStatus = "00"
                if (gblCRMProfileInqCal == true) {
                    var ibStatus = resulttable["ibUserStatusIdTr"];
                    if (ibStatus == gblIBStatus) {
                        dismissLoadingScreenPopup(); //gblFinancialTxnIBLock  
                        //alert("User Channel Status is Locked");
                        //alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
                        popIBBPOTPLocked.show();
                        return false;
                    }
                    dismissLoadingScreenPopup();

                    activityFlexValues2 = formatDateFT(gblFTViewStartOnDate); //frmIBTransferNowConfirmation.lblAccountNo.text;
                    activityFlexValues3 = gblFTViewRepeatAsVal;
                    activityFlexValues4 = gblTransfrAmt;


                    frmIBFTrnsrView.hbxFTEditAmnt.setVisibility(true);
                    frmIBFTrnsrView.hbxFTVeiwAmnt.setVisibility(false);
                    frmIBFTrnsrView.lblFTEditHdr.setVisibility(true);
                    frmIBFTrnsrView.btnFTEdit.setVisibility(true);
                    frmIBFTrnsrView.hbxFTViewHdr.setVisibility(false);
                    frmIBFTrnsrView.btnReturnView.setVisibility(false)
                    frmIBFTrnsrView.hbxEditBtns.setVisibility(true);

                    frmIBFTrnsrView.btnFTEditFlow.setVisibility(false);
                    frmIBFTrnsrView.btnFTDel.setVisibility(false);

                    var tmpAmt = frmIBFTrnsrView.lblFTViewAmountVal.text;
                    tmpAmt = tmpAmt.substring(0, tmpAmt.length - 1)
                    tmpAmt = tmpAmt.trim();
                    frmIBFTrnsrView.txtFTEditAmountval.text = frmIBFTrnsrView.lblFTViewAmountVal.text; //tmpAmt;
                    frmIBFTrnsrView.txtFTEditAmountval.setFocus(true);
                    frmIBFTrnsrView.hbxFTAfterEditAvilBal.setVisibility(true);

                    if (gblToAccntNickUpdated != "") {
                        frmIBFTrnsrView.lblToAccntNickName.text = gblToAccntNickUpdated;
                    }

                    gblCRMProfileInqCal = false;

                } else {

                    activityFlexValues2 = formatDateFT(gblFTViewStartOnDate) + "+" + frmIBFTrnsrView.lblViewStartOnDateVal.text;
                    activityFlexValues3 = gblFTViewRepeatAsVal + "+" + frmIBFTrnsrView.lblRepeatAsVal.text;
                    var tmpAmt = frmIBFTrnsrView.lblFTViewAmountVal.text;
                    var len = tmpAmt.length;
                    tmpAmt = tmpAmt.substring(0, len - 1);
                    activityFlexValues4 = gblTransfrAmt + "+" + tmpAmt;


                    var DailyLimit = parseFloat(resulttable["ebMaxLimitAmtCurrent"].toString());
                    var transferAmt = frmIBFTrnsrView.txtFTEditAmountval.text;

                    if (transferAmt.indexOf(",") != -1) {
                        //transferAmt = kony.string.replace(transferAmt, ",", "");
                        transferAmt = replaceCommon(transferAmt, ",", "");
                    }

                    transferAmt = parseFloat(transferAmt.toString());

                    if (transferAmt > DailyLimit && frmIBFTrnsrView.hbxNotifyReceipent.isVisible) {
                        dismissLoadingScreenPopup();
                        var amounttext = kony.i18n.getLocalizedString("TRErr_MaxLimitExceedLimitType");
                        amounttext = amounttext.replace("{MaxLimitperTypeAmount}", DailyLimit);
                        alert(amounttext);
                        return false;
                    } else {
                        frmIBFTrnsrEditCnfmtn.lblToAccntName.text = frmIBFTrnsrView.lblToAccntName.text;

                        if (resulttable["tokenDeviceFlag"] == "Y")
                            gbltokenFlag = true;
                        frmIBFTrnsrEditCnfmtn.lblFrmAccntName.text = frmIBFTrnsrView.lblFrmAccntName.text;
                        frmIBFTrnsrEditCnfmtn.lblFrmAccntNum.text = frmIBFTrnsrView.lblFrmAccntNumber.text;
                        frmIBFTrnsrEditCnfmtn.lblFrmAccntNickName.text = frmIBFTrnsrView.lblFrmAccntNickName.text;
                        frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text = frmIBFTrnsrView.lblToAccntNickName.text;
                        frmIBFTrnsrEditCnfmtn.lblToAccntNum.text = frmIBFTrnsrView.lblToAccntNumber.text;
                        frmIBFTrnsrEditCnfmtn.lblBankNameVal.text = frmIBFTrnsrView.lblBankNameVal.text;
                        /*
							    var amt = frmIBFTrnsrView.txtFTEditAmountval.text;
							    if(amt.indexOf(",") != -1){
							    	amt = replaceCommon(amt, ",", "");
							    }
													    
						    if(amt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
						    	frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text = frmIBFTrnsrView.txtFTEditAmountval.text;
						    }else
						        frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text = frmIBFTrnsrView.txtFTEditAmountval.text+ " "+kony.i18n.getLocalizedString("currencyThaiBaht");
						   	
						   	*/
                        frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text = frmIBFTrnsrView.txtFTEditAmountval.text;
                        //frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text = commaFormatted(amt)+ " "+kony.i18n.getLocalizedString("currencyThaiBaht") ;
                        frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text = frmIBFTrnsrView.lblOrderDateVal.text;
                        frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text = frmIBFTrnsrView.lblViewStartOnDateVal.text;
                        frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text = frmIBFTrnsrView.lblRepeatAsVal.text;
                        frmIBFTrnsrEditCnfmtn.lblEndOnDateVal.text = frmIBFTrnsrView.lblEndOnDateVal.text;
                        frmIBFTrnsrEditCnfmtn.lblExcuteVal.text = frmIBFTrnsrView.lblExcuteVal.text;
                        frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text = frmIBFTrnsrView.lblNotifyRecipientVal.text;
                        frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text = replaceHtmlTagChars(frmIBFTrnsrView.lblNoteToRecipientVal.text);
                        frmIBFTrnsrEditCnfmtn.lblMyNoteVal.text = replaceHtmlTagChars(frmIBFTrnsrView.lblMyNoteVal.text);
                        frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text = frmIBFTrnsrView.lblScheduleRefNoVal.text;
                        frmIBFTrnsrEditCnfmtn.lblRemainingVal.text = frmIBFTrnsrView.lblRemainingVal.text;
                        frmIBFTrnsrEditCnfmtn.lblMethodName.text = frmIBFTrnsrView.lblMethodName.text;
                        frmIBFTrnsrEditCnfmtn.show();
                        dismissLoadingScreenPopup();
                        //reqOTPFT();

                    }
                }

            } else {
                dismissLoadingScreenPopup();
                activityStatus = "02";
                alert(resulttable["StatusDesc"]);
            }

        } else {
            dismissLoadingScreenPopup();
            activityStatus = "02";
            alert(resulttable["errmsg"]);
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);

    }
}


//TokenSwitch changes
function tokenHnadlerEditFT() {
    if (gblSwitchToken == false && gblTokenSwitchFlag == false) {
        srvTokenSwitchingFT();
    } else if (gblTokenSwitchFlag == false && gblSwitchToken == true) {
        frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        reqOTPFT();
    } else if (gblTokenSwitchFlag == true && gblSwitchToken == false) {
        frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP"); //Receipent_Token
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyPleaseEnterToken"); //Receipent_tokenId
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keySwitchToSMS"); //Receipent_SwitchtoOTP
        dismissLoadingScreenPopup();

        frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.txtBxOTPFT.setFocus(true);
        frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(false);

    } else if (gblTokenSwitchFlag == true && gblSwitchToken == true) {
        frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        reqOTPFT();
    }

}

function srvTokenSwitchingFT() {
    var inputParam = [];
    inputParam["crmId"] = gblcrmId;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingFTCallBack);
}

function srvTokenSwitchingFTCallBack(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            
            if (callbackResponse["deviceFlag"].length > 0) {
                var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
                var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
                var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];

                if (tokenFlag == "Y" && (mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus == '02') {
                    frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(false);
                    frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(false);
                    frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP"); //Receipent_Token
                    frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyPleaseEnterToken"); //Receipent_tokenId
                    frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keySwitchToSMS");
                    gblTokenSwitchFlag = true;

                    frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(true);
                    frmIBFTrnsrEditCnfmtn.txtBxOTPFT.setFocus(true);
                    frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(true);
                    frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(false);
                    dismissLoadingScreenPopup();

                    //frmIBFTrnsrEditCnfmtn.show();

                } else {
                    frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(true);
                    frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(true);
                    frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
                    frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                    frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
                    gblTokenSwitchFlag = false;
                    //frmIBFTrnsrEditCnfmtn.show();
                    reqOTPFT()
                        //dismissLoadingScreenPopup();
                }
            } else {
                frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(true);
                frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(true);
                frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
                frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
                frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
                gblTokenSwitchFlag = false;
                reqOTPFT()

            }
        } else {
            dismissLoadingScreenPopup();
        }
    }
}

//End date caluculation,if the After is clicked
function endDateMap() {
    var times = frmIBFTrnsrView.txtNumOfTimes.text;
    var startDate = frmIBFTrnsrView.calStartDate.formattedDate;
    var startDate = parseDateIB(startDate);
    frmIBFTrnsrView.lblEndOnDateVal.text = endDateCalculatorFT(startDate, times)
}

function endDateCal(date, times) {
    var times = times;
    var startDate = date; //date should be formatteddate
    var startDate = parseDateIB(startDate);
    //frmIBFTrnsrView.lblEndOnDateVal.text = endDateCalculatorFT(startDate, times);
    var endDate = endDateCalculatorFT(startDate, times);
    return endDate;
}



function parseDateIB(str) {
    var mdy = str.split('/')
    return new Date(mdy[2], mdy[1] - 1, mdy[0]);
}


//New caluculation Changes
function endDateCalculatorFT(staringDateFromCalendar, repeatTimesTextBoxValue) {
    var endingDate = staringDateFromCalendar;
    var numberOfDaysToAdd = 0;
    

    if (kony.string.equalsIgnoreCase(gblScheduleRepeatFT, kony.i18n.getLocalizedString("keyDaily"))) {
        numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd - 1);

    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatFT, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
        numberOfDaysToAdd = ((repeatTimesTextBoxValue - 1) * 7);
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);

    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatFT, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
        endingDate.setDate(endingDate.getDate());
        var dd = endingDate.getDate();
        
        var mm = endingDate.getMonth() + 1;
        
        var newmm = parseFloat(mm.toString()) + parseFloat(repeatTimesTextBoxValue - 1);
        
        var newmmadd = newmm % 12;
        if (newmmadd == 0) {
            newmmadd = 12;
        }
        
        var yearAdd = Math.floor((newmm / 12));
        
        var y = endingDate.getFullYear();
        if (newmmadd == 12) {
            y = parseFloat(y) + parseFloat(yearAdd) - 1;
        } else
            y = parseFloat(y) + parseFloat(yearAdd);

        mm = parseFloat(mm.toString()) + newmmadd;
        
        if (newmmadd == 2) {
            if (dd > 28) {
                dd = 28;
            }
        }
        if (newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11) {
            if (dd > 30) {
                dd = 30;
            }
        }

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (newmmadd < 10) {
            newmmadd = '0' + newmmadd;
        }

        var someFormattedDate = dd + '/' + newmmadd + '/' + y;
        return someFormattedDate;

    } else if (kony.string.equalsIgnoreCase(gblScheduleRepeatFT, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
        var dd = endingDate.getDate();
        var mm = endingDate.getMonth() + 1;
        var y = endingDate.getFullYear();
        var newYear = parseFloat(y) + parseFloat(repeatTimesTextBoxValue - 1);
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var someFormattedDate = dd + '/' + mm + '/' + newYear;
        return someFormattedDate;
    }
    var dd = endingDate.getDate();
    var mm = endingDate.getMonth() + 1;
    var y = endingDate.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}

// to get transfer fee for ORFT n SMART
/*
function getTransferFeeFTIB() {
	//if(gblRC_QA_TEST_VAL==0){
			
		//}else{ }
		var inputParam = {}
		
		inputParam["Amount"] = gblTransfrAmt;   // gblTransfrAmt getting frm paymentInq
		inputParam["freeTransCnt"]=gblFreeTransactions;
        inputParam["prodCode"]=gblFrmaccntProdCode;
		showLoadingScreenPopup();
		invokeServiceSecureAsync("geEdittFTferFee", inputParam, getTransferFeeFTIBcallBack)
}

function getTransferFeeFTIBcallBack(status, resulttable) {

	if(status==400){
		
		
		if(resulttable["opstatus"] == 0)
		 {
		 	var orftFee=resulttable["orftFee"];
		 	var smartFee=resulttable["smartFee"];
		 	var currentDate=resulttable["orftFeeDate"]
		 	
		 	frmIBTranferLP.btnXferORFT.text = orftFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			frmIBTranferLP.btnXferSMART.text = smartFee + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			dismissLoadingScreenPopup();
			var currentDate = currentDate.toString();
			  var dateTD = currentDate.substr(0, 2);
			  var yearTD = currentDate.substr(6, 4);
			  var monthTD = currentDate.substr(3, 2);
			  
			  
			  var completeDate = yearTD + "-" + monthTD + "-" + dateTD;
			  
		 	
		 	//gblTransferDate = completeDate;

		 }
		 else
		 {
		 	dismissLoadingScreenPopup();
			alert(" "+resulttable["errMsg"]);
		 } 
	 }
}
*/

//Service to get To-Bank name

function srvGetBankTypeFT(bankCd) {
    //showLoadingScreenPopup();
    var inputparam = {};
    inputparam["bankCode"] = bankCd;
    invokeServiceSecureAsync("getBankDetails", inputparam, srvGetBankTypeFTCallBack);
}

function srvGetBankTypeFTCallBack(status, resulttable) {
    
    if (status == 400) {
        var gblBankType = "";
		var promptPayFlag = ""; 
        if (resulttable["opstatus"] == 0) {
			gblbankInfo = {};
            gblBankType = resulttable.Results[0].orftFlag;
			 promptPayFlag =  resulttable.Results[0].promptPayFlag;	
            if (resulttable.Results[0].errMsg == undefined) {
                if (kony.i18n.getCurrentLocale() == "en_US") { 
              	 frmIBFTrnsrView.lblBankNameVal.text = resulttable.Results[0].bankNameEng;
              	}else{
              	 frmIBFTrnsrView.lblBankNameVal.text = resulttable.Results[0].bankNameThai;
               	}
            } else
                frmIBFTrnsrView.lblBankNameVal.text = "-";
                if(frmIBFTrnsrView.lblToAccntNickName.text == "")
               	 frmIBFTrnsrView.lblToAccntNickName.text = resulttable.Results[0].bankShortName;

          	gblbankInfo.bankShortName = resulttable.Results[0].bankShortName;
          
            if (frmIBFTrnsrView.rchtxtErrorFrmAccnt.isVisible == true || frmIBFTrnsrView.lblToAccntNickName.text == "") {
                //frmIBFTrnsrView.lblBankNameVal.text = "-";
                frmIBFTrnsrView.btnFTEditFlow.setEnabled(false);
            } else {
                frmIBFTrnsrView.btnFTEditFlow.setEnabled(true);
                frmIBFTrnsrView.btnFTEditFlow.skin = "btnIBediticonsmall";
                frmIBFTrnsrView.btnFTEditFlow.focusSkin = "btnIbeditsmalliconHover";
                frmIBFTrnsrView.btnFTEditFlow.hoverSkin = "btnIbeditsmalliconHover";
            }

            if (gblToBankCdFT == "11") {
                frmIBFTrnsrView.lblToAccntName.setVisibility(true);
                //frmIBFTrnsrView.lblToAccntName.text = gblAccntName_ORFT;
                srvDepositeInqFT();
            } else if (gblBankType == "Y" && promptPayFlag != "Y") {
                frmIBFTrnsrView.lblToAccntName.setVisibility(true);
                srvOrftAccountInqFT(gblToAccntID);
            } else if (gblpmtMethodFT =="PROMPTPAY_ACCOUNT") {
          	   editSchedulePromptPayAccNameCheck();
          	    frmIBFTrnsrView.lblToAccntName.setVisibility(true);
            }else {
               // frmIBFTrnsrView.lblToAccntName.text = "";
			   
                frmIBFTrnsrView.show();
                dismissLoadingScreenPopup();

            }

            //frmIBFTrnsrView.show();
            //dismissLoadingScreenPopup();

        } else {
            dismissLoadingScreenPopup();
            
            alert(" " + resulttable["errMsg"]);
        }
    } else if (status == 300) {
        dismissLoadingScreenPopup();
        

    }
}

//If To bank is TMB 
function srvDepositeInqFT() {
    var inputparam = {};
    inputparam["acctId"] = gblToAccntID;
    invokeServiceSecureAsync("depositAccountInquiryNonSec", inputparam, srvDepositeInqFTCallBackIB);
}

function srvDepositeInqFTCallBackIB(status, resulttable) {
    
    if (status == 400) {
        
        if (resulttable["opstatus"] == 0) {
            
            
            //

            //frmIBFTrnsrEditCnfmtn.lblToAccntName.text =resulttable["accountTitle"]; 
            frmIBFTrnsrView.lblToAccntName.text = resulttable["accountTitle"];
            frmIBFTrnsrView.show();
            dismissLoadingScreenPopup();
            //srvAllReceipents()

        } else {
            dismissLoadingScreenPopup();
            
            alert(" " + resulttable["errMsg"]);
        }
    } else if (status == 300) {
        dismissLoadingScreenPopup();
        

    }
}

function srvOrftAccountInqFT(acctNo) {
    //showLoadingScreen();
    var inputParam = {}
    inputParam["fromAcctNo"] = gblFrmAccntID
    inputParam["toAcctNo"] = acctNo;
    inputParam["toFIIdent"] = gblToBankCdFT;
    inputParam["transferAmt"] = "500";
   	inputParam["AccountName"] = frmIBFTrnsrView.lblToAccntName.text
    invokeServiceSecureAsync("ORFTInq", inputParam, srvOrftAccountInqFTCallBack)
        //callBackOrftAccountInq(400,{opstatus:3})
}

function srvOrftAccountInqFTCallBack(status, resulttable) {
    
    if (status == 400) {
        
        
        if (resulttable["opstatus"] == 0) {
            if (resulttable["StatusCode"] == 0) {
                ///fetching To Account Name from ORFT Inq
                // frmIBFTrnsrEditCnfmtn.lblToAccntName.text = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];
                frmIBFTrnsrView.lblToAccntName.text = resulttable["ORFTTrnferInqRs"][0]["toAcctName"];

                frmIBFTrnsrView.show();
                dismissLoadingScreenPopup();
                //srvAllReceipents()

            } else {
                //dismissLoadingScreenPopup()
                
                dismissLoadingScreenPopup();
                alert(" " + resulttable["errMsg"]);
                return false;
            }
            //frmTransferConfirm.lblTransCnfmToName.text=ToAccountName;
        } else {
            dismissLoadingScreenPopup()
            alert(" " + resulttable["errMsg"]);
            return false;

        }

    } else if (status == 300) {
        dismissLoadingScreenPopup()
        

    }

}


function OTPTimerCallBackFT() {

    if (gblRetryCountRequestOTP > gblOTPReqLimit) {
        frmIBFTrnsrEditCnfmtn.btnOTPReq.skin = btnIBREQotp;
        frmIBFTrnsrEditCnfmtn.btnOTPReq.focusSkin = btnIBREQotp;
        frmIBFTrnsrEditCnfmtn.btnOTPReq.setEnabled(false);
        frmIBFTrnsrEditCnfmtn.btnOTPReq.hoverSkin = btnIBREQotp;
    } else {
        frmIBFTrnsrEditCnfmtn.btnOTPReq.skin = btnIBREQotpFocus;
        frmIBFTrnsrEditCnfmtn.btnOTPReq.focusSkin = btnIBREQotpFocus;
        frmIBFTrnsrEditCnfmtn.btnOTPReq.setEnabled(true);
        frmIBFTrnsrEditCnfmtn.btnOTPReq.hoverSkin = btnIBREQotpFocus;
    }

    //  frmIBFTrnsrEditCnfmtn.btnOTPReq.skin =btnIBREQotpFocus;
    //  frmIBFTrnsrEditCnfmtn.btnOTPReq.setEnabled(true);
    try {
        kony.timer.cancel("OTPTimerReqFT")
    } catch (e) {
        
    }
}


function srvFutureTransferDelete() {
    var inputparam = {};
    //showLoadingScreenPopup();
    inputparam["rqUID"] = "12345654-2341-9876-9283-000000000011";
    inputparam["clientDate"] = gblOrderDateFT;
    //inputparam["PmtId"] = ""; 
    if (gblFtDel == false) {
        var scheID = frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text;
        inputparam["scheID"] = scheID.replace("F", "S");
        invokeServiceSecureAsync("doPmtCan", inputparam, srvFutureTransferDeletecallBack);
    } else {
        var scheID = frmIBFTrnsrView.lblScheduleRefNoVal.text;
        inputparam["scheID"] = scheID.replace("F", "S");
        var activityFlexValues5 = gblpmtMethodFT;
        if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
            if(isTMB)
            {
            activityFlexValues5 = "My TMB";
            }
            else
            {
            activityFlexValues5 = "Other TMB";
            }
        }

        var amt = frmIBFTrnsrView.lblFTViewAmountVal.text;
        amt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");

        
        var logLinkageId = "";
        inputparam["isCancelFlow"] = "true";
        inputparam["EditFTFlow"] = "EditFT";
        inputparam["activityFlexValues1"] = "Cancel";
        inputparam["activityFlexValues2"] = formatDateFT(gblFTViewStartOnDate);
        inputparam["activityFlexValues3"] = gblFTViewRepeatAsVal;
        inputparam["activityFlexValues4"] = amt;
        inputparam["activityFlexValues5"] = activityFlexValues5;

        invokeServiceSecureAsync("doPmtCan", inputparam, srvFutureTransferDeleteOnlycallBack);
    }
}


function srvFutureTransferDeletecallBack(status, result) {

    if (status == 400) {

        if (result["opstatus"] == 0) {

            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];

            if (StatusCode == 0) {

                var finSchduleRefId = frmIBFTrnsrView.lblScheduleRefNoVal.text;
                finSchduleRefId = finSchduleRefId.replace("F", "S");
                // setFinancialActivityLogFTDelete(finSchduleRefId);  

                srvGenerateNewTransRefIDFT();
            } else {
                dismissLoadingScreenPopup();
                alert("Service failed with Status code : " + StatusCode + " and Status description as " + StatusDesc);
                return false;
            }

        } else {
            dismissLoadingScreenPopup();
            
        }
    }
}

function srvGenerateNewTransRefIDFT() {
    var inputParam = {};
    inputParam["transRefType"] = "";
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, srvGenerateNewTransRefIDFTcallBack);
}

/*
   **************************************************************************************
		Module	: callBackGenerateNewTransRefIDService
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function srvGenerateNewTransRefIDFTcallBack(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var refNum = result["transRefNum"];
            frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text = "ST" + refNum + "00";
            srvPmtAddServiceFT();
        } else {
            dismissLoadingScreenPopup();
            alert(" " + result["errMsg"]);
        }
    }
}


/*
   **************************************************************************************
		Module	: callPmtAddServiceForEditBPIB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
function srvPmtAddServiceFT() {
    var inputParam = {};
    inputParam["rqUID"] = "";
    inputParam["crmId"] = gblcrmId;


    var tmpFee = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
    tmpFee = tmpFee.substring("0", tmpFee.length - 1);

    var tmpAmt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
    var len = tmpAmt.length;
    tmpAmt = tmpAmt.substring("0", len - 1);
    tmpAmt = tmpAmt.trim();
    if (tmpAmt.indexOf(",") != -1) {
        tmpAmt = replaceCommon(tmpAmt, ",", "");
    }

    tmpAmt = parseFloat(tmpAmt.toString())
    tmpAmt = "" + tmpAmt;
    if (tmpAmt.indexOf(".") != -1) {
        var tmp = tmpAmt.split(".", 2);
        if (tmp[1].length == 1) {
            tmpAmt = tmpAmt + "0";
        } else if (tmp[1].length > 2) {
            tmp[1] = tmp[1].substring("0", "2")
            tmpAmt = ""
            tmpAmt = tmp[0] + "." + tmp[1];
        }
    }

    inputParam["amt"] = tmpAmt;

    inputParam["fromAcct"] = gblFrmAccntID; // from Account ID (from PaymentInq)
    inputParam["fromAcctType"] = gblFrmAccntType; // from Account Type (from PaymentInq)
    inputParam["toAcct"] = gblToAccntID; // TO Acc number (from PaymentInq)
    inputParam["toAccTType"] = gblToAccntType; // To Account type ((from PaymentInq)
    inputParam["dueDate"] = changeDateFormatForService(frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text); // Start On Date
    inputParam["pmtRefNo1"] = ""; // Ref 1 value  // Have to chek for FT
    inputParam["custPayeeId"] = ""; // check this 
    inputParam["transCode"] = gblTransCodeFT; //TransCode (from PaymentInq) 
    inputParam["memo"] = frmIBFTrnsrEditCnfmtn.lblMyNoteVal.text; // My Note 
    inputParam["pmtMethod"] = gblpmtMethodFT; // get this value from paymentInq
    var extTrnRefId = frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text;
    extTrnRefId = extTrnRefId.replace("F", "S");
    inputParam["extTrnRefId"] = extTrnRefId; // Updated Schedule Ref No, shud come frm service call
    if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_After")) {
        inputParam["NumInsts"] = frmIBFTrnsrEditCnfmtn.lblExcuteVal.text; // Execution times 
    }
    /*
    if(gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_Never")){
        inputParam["NumInsts"] = "99";			// Execution times should be 99 incase of never
    }*/
    if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_OnDate")) {
        inputParam["FinalDueDt"] = changeDateFormatForService(frmIBFTrnsrEditCnfmtn.lblEndOnDateVal.text); // End On Date
    }
    var frequency = frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;

    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
        inputParam["dayOfWeek"] = "";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
        inputParam["dayOfMonth"] = "";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
        frequency = "Annually";
        inputParam["dayofMonth"] = "";
        inputParam["monthofYear"] = "";
    }
    inputParam["freq"] = frequency;

    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
        inputParam["freq"] = "Daily";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
        inputParam["freq"] = "Weekly";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
        inputParam["freq"] = "Monthly";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
        inputParam["freq"] = "Annually";
    }
    if (kony.string.equalsIgnoreCase(frequency, "Once")) {
        inputParam["freq"] = "once";
    }

    inputParam["desc"] = "";

    inputParam["curCode"] = "THB";
    inputParam["pmtRefNo1"] = "XXX";
    inputParam["channelId"] = "IB";

    inputParam["toBankId"] = gblToBankCdFT;

    var pmtMethod = frmIBFTrnsrEditCnfmtn.lblMethodName.text;

    if (pmtMethod == "SMART" || pmtMethod == "ORFT") {
        if (gblNotificationType != "" && gblNotificationType == "Email" && frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text != "-") {
            inputParam["notificationType"] = "1";
            inputParam["recipientEmailAddr"] = frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text;
            inputParam["receipientNote"] = frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text;
        } else if (gblNotificationType != "" && gblNotificationType == "SMS" && frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text != "-") {
            inputParam["notificationType"] = "0";
            inputParam["recipientMobileNbr"] = frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text;
            inputParam["receipientNote"] = frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text;
        }

    }
    if (pmtMethod == "ORFT") {
        inputParam["PmtMiscType"] = "MOBILE";
        inputParam["MiscText"] = gblPHONENUMBER;
    }

    invokeServiceSecureAsync("doPmtAdd", inputParam, srvPmtAddServiceFTcallBack);

}


function srvFutureTransferUpdate() {

    var inputparam = {};

    var tmpFee = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
    tmpFee = tmpFee.substring("0", tmpFee.length - 1);
    var transferAmt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text //Add Fee value as wel 
    var len = transferAmt.length;
    transferAmt = transferAmt.substring("0", len - 1);
    transferAmt = transferAmt.trim();

    if (transferAmt.indexOf(",") != -1) {
        transferAmt = replaceCommon(transferAmt, ",", "");
    }
    transferAmt = parseFloat(transferAmt.toString())
    transferAmt = "" + transferAmt;
    if (transferAmt.indexOf(".") != -1) {
        var tmp = transferAmt.split(".", 2);
        if (tmp[1].length == 1) {
            transferAmt = transferAmt + "0";
        } else if (tmp[1].length > 2) {
            tmp[1] = tmp[1].substring("0", "2")
            transferAmt = ""
            transferAmt = tmp[0] + "." + tmp[1];
        }
    }

    inputparam["rqUID"] = "12345654-2341-9876-9283-000000000011";
    inputparam["clientDate"] = gblOrderDateFT;
    var extTrnRefID = frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text;
    extTrnRefID = extTrnRefID.replace("F", "S");
    inputparam["extTrnRefID"] = extTrnRefID;
    inputparam["Amt"] = transferAmt; // HAve to get after fee cal
    inputparam["CurCode"] = "THB"; // Hav to Chek

    var tempDate = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text;
    tempDate = hypnFormatDateFT(tempDate)
    inputparam["Starton"] = tempDate;
    inputparam["channelId"] = "IB";
    invokeServiceSecureAsync("doPmtMod", inputparam, srvFutureTransferUpdatecallBack)
}

//Call before saving the schdule details

function validateSchdule() {

    //validating edited start date
    var today = new Date();
    var date = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();
    var d1 = date + "/" + month + "/" + year;
    var d2 = frmIBFTrnsrView.calStartDate.formattedDate;
    frmIBFTrnsrView.imgTMB.setVisibility(false);
    //Should handle when tomorrows date is selectd,today's time is b4 11pm or not
    if (parseDateIB(d1) >= parseDateIB(d2)) {
        alert(kony.i18n.getLocalizedString("Error_InvalidStartDate")); //alert("Start On Date should be greater than todays date");
        return false;
    }

    frmIBFTrnsrView.lblViewStartOnDateVal.text = frmIBFTrnsrView.calStartDate.formattedDate;

    //if (frmIBFTrnsrView.lblRepeatAsVal.text == "Once") {
        if (gblScheduleRepeatFT != "Once") {
            if (gblScheduleEndFT == "none") {
                alert("Please select Ending Value");
                return false;
            } else if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_After")) {
                if (frmIBFTrnsrView.txtNumOfTimes.text == "") {
                    alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes")); //alert("Enter No. of times");
                    return false;
                } else {
                    var tempAmt = frmIBFTrnsrView.txtNumOfTimes.text;
                    var pat1 = /[^0-9]/g
                    var isNumeric = pat1.test(tempAmt);
                    if (isNumeric == true) {
                        //alert("Enter Valid Input");
                        alert(kony.i18n.getLocalizedString("keyIBOtpNumeric")); //Only Numeric input allowed
                        return false;
                    }

                }
            }
        } else if (gblScheduleRepeatFT == "Once" && gblScheduleEndFT != "none") {
            alert("Please Select Repeat As Value");
            return false;

        } else if (gblScheduleRepeatFT == "Once" && gblScheduleEndFT == "none") {
            frmIBFTrnsrView.lblEndOnDateVal.text = frmIBFTrnsrView.lblViewStartOnDateVal.text;
            frmIBFTrnsrView.lblExcuteVal.text = "1";
            frmIBFTrnsrView.lblRepeatAsVal.text = "Once";
            frmIBFTrnsrView.lblRemainingVal.text = "1";

        }
    //}

    if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_OnDate")) {
        // Date validations
        //dateChek(frmIBFTrnsrView.calStartDate.dateComponents , frmIBFTrnsrView.calEndDate.dateComponents);
        if (!isNotBlank(frmIBFTrnsrView.calEndDate.formattedDate) || parseDateIB(frmIBFTrnsrView.calStartDate.formattedDate) >= parseDateIB(frmIBFTrnsrView.calEndDate.formattedDate)) {
            //alert("End Date should be greater than the start Date");
            alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));
            return false;
        }

       // var date = frmIBFTrnsrView.calStartDate.dateComponents;
		//Begin : MIB-554 
        frmIBFTrnsrView.lblViewStartOnDateVal.text = frmIBFTrnsrView.calStartDate.formattedDate;
		//End : MIB-554
        frmIBFTrnsrView.lblEndOnDateVal.text = frmIBFTrnsrView.calEndDate.formattedDate;
        var tempNumTimes = numberOfExecutionFT(frmIBFTrnsrView.calStartDate.formattedDate, frmIBFTrnsrView.calEndDate.formattedDate, gblRepeatAsforLS);
        if (tempNumTimes > 99) {
            alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
            return false;
        }

        frmIBFTrnsrView.lblExcuteVal.text = numberOfExecutionFT(frmIBFTrnsrView.calStartDate.formattedDate, frmIBFTrnsrView.calEndDate.formattedDate, gblRepeatAsforLS);
        gblIsEditOnDate = true;
        gblIsEditAftr = false;
        //frmIBFTrnsrView.lblRemainingVal.text = frmIBFTrnsrView.lblExcuteVal.text;  

    }
    //if after is changed, have to show the end date in view page
    if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_After")) {
        //frmIBFTrnsrView.txtNumOfTimes.setFocus(true);
        if (frmIBFTrnsrView.txtNumOfTimes.text != "" && frmIBFTrnsrView.txtNumOfTimes.text != "0") {
            var tempAmt = frmIBFTrnsrView.txtNumOfTimes.text;
            var pat1 = /[^0-9]/g
            var isNumeric = pat1.test(tempAmt);
            if (isNumeric == true) {
                alert(kony.i18n.getLocalizedString("keyIBOtpNumeric")); //Only Numeric input allowed
                return false;
            } else {
                if (tempAmt > 99) {
                    alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
                    return false;
                }
                frmIBFTrnsrView.lblExcuteVal.text = frmIBFTrnsrView.txtNumOfTimes.text;
            }

        } else {
            alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes")); //alert("Enter No. of Times");
            return false;
        }
        endDateMap();
        gblIsEditOnDate = false;
        gblIsEditAftr = true;
        //frmIBFTrnsrView.lblRemainingVal.text = frmIBFTrnsrView.lblExcuteVal.text; 
    }

    if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_Never")) {

        frmIBFTrnsrView.lblEndOnDateVal.text = "-";
        frmIBFTrnsrView.lblExcuteVal.text = "-";
        frmIBFTrnsrView.lblRemainingVal.text = "-"

        gblIsEditOnDate = false;
        gblIsEditAftr = false;

    }

    if (gblScheduleRepeatFT == kony.i18n.getLocalizedString("Transfer_Daily"))
        frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Daily");
    if (gblScheduleRepeatFT == kony.i18n.getLocalizedString("Transfer_Weekly"))
        frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Weekly");
    if (gblScheduleRepeatFT == kony.i18n.getLocalizedString("Transfer_Monthly"))
        frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Monthly");
    if (gblScheduleRepeatFT == kony.i18n.getLocalizedString("Transfer_Yearly"))
        frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Yearly");

    //Logic if only date or num of times is edited without clicking repeatas btn and ending btn

    if (gblIsEditOnDate == true && gblIsEditAftr == false) {

        if (parseDateIB(frmIBFTrnsrView.calStartDate.formattedDate) >= parseDateIB(frmIBFTrnsrView.calEndDate.formattedDate)) {
            //alert("End Date should be greater than the start Date");
            alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));
            return false;
        }

        var d2 = frmIBFTrnsrView.lblEndOnDateVal.text;
        var d3 = frmIBFTrnsrView.calEndDate.formattedDate;

        if (parseDateIB(d2) != parseDateIB(d3)) {
            gblIsEditAftr = false;
            gblIsEditOnDate = true;

        }

        frmIBFTrnsrView.lblExcuteVal.text = numberOfExecutionFT(frmIBFTrnsrView.calStartDate.formattedDate, frmIBFTrnsrView.calEndDate.formattedDate, gblScheduleRepeatFT);

    }

    if (gblIsEditAftr == true && gblIsEditOnDate == false) {

        var n1 = frmIBFTrnsrView.txtNumOfTimes.text;
        var n2 = frmIBFTrnsrView.lblExcuteVal.text;

        //Only if repeatas and ending value are not changed	
        if (gblScheduleRepeatFT == frmIBFTrnsrView.lblRepeatAsVal.text) {
            if (n1 != n2) {
                endDateMap();
                //frmIBFTrnsrView.lblRemainingVal.text = frmIBFTrnsrView.lblExcuteVal.text;
                gblIsEditAftr = true;
                gblIsEditOnDate = false;
            } else {
                endDateMap();
            }
        }

    }

    //starton date holyday chek
    if (gblpmtMethodFT == "SMART") {
        var d = frmIBFTrnsrView.calStartDate.dateComponents;
		var monthTemp = parseInt(d[1],10);  
        var d2 = new Date(d[2], monthTemp - 1, d[0]);		//new Date(d[2], d[1] - 1, d[0])
        
        if (d2.getDay() == 6 || d2.getDay() == 0) {
            alert(kony.i18n.getLocalizedString("keyFTSMARTOnWeekend"));
            return;
        }
        d[2] = d[2].toString();
        var date = d[2] + "/" + d[1] + "/" + d[0]; //"2013/11/11"
        srvHolydayChk(date);

    } else {
        frmIBFTrnsrView.hbxEditFT.setVisibility(false);
        frmIBFTrnsrView.imgTMB.setVisibility(true);
        frmIBFTrnsrView.lblRemainingVal.text = frmIBFTrnsrView.lblExcuteVal.text;
    }

    // frmIBFTrnsrView.hbxEditFT.setVisibility(false);
	// frmIBFTrnsrView.imgTMB.setVisibility(true);    
   // frmIBFTrnsrView.lblRemainingVal.text = frmIBFTrnsrView.lblExcuteVal.text;

}


//New changes to handle token switch

function verifyPasswordFT() {
    var text = frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text;
    var isNumOTP = kony.string.isNumeric(text);
    var txtLen1 = text.length;
if(isTMB){

}else{
    if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keyRequest")) {
        
        if (txtLen1 != gblOTPLENGTH_FT || isNumOTP == false || text == "") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP")); //OTP is incorrect. Please reinput
            return false;
        }

    } else if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
        
        if (isNumOTP == false || text == "") {
            dismissLoadingScreenPopup();
            alert(kony.i18n.getLocalizedString("emptyToken"));
            return false;
        }
    }
    }

    // gblVerifyOTP = gblVerifyOTP + 1
    //

    //srvVerifyPasswordExFT();
    srvVerifyPasswordExFT_CS();

}

/*function callBackVerifyPasswordFT(status, callBackResponse) {
    if (status == 400) {
        frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text = "";
        if (callBackResponse["opstatus"] == 0) {
            gblVerifyOTPCounter = "0";

            if (gblEditFTSchdule == true) {
                srvFutureTransferDelete();
            } else {
                srvFutureTransferUpdate();
            }

        } else if (callBackResponse["opstatus"] == 8005) {
            if (callBackResponse["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = callBackResponse["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
                    alert("" + callBackResponse["errMsg"]);
                    return false;
                } else {
                    alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                    return false;
                }

            } else if (callBackResponse["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                //alert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"));
                //startRcCrmUpdateProIB("3");
                handleOTPLockedIB(callBackResponse);
                return false;
            }
        } else {
            dismissLoadingScreenPopup();
            alert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + callBackResponse["errMsg"]);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("" + kony.i18n.getLocalizedString("Receipent_alert_Error"));
        }
    }

}*/

//--
// Verify OTP service and Edit Future Transfer complete
function verifyOTPFT() {
    showLoadingScreenPopup();

    var text = frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text;
    var isNumOTP = kony.string.isNumeric(text);
    var txtLen1 = text.length;
    if (txtLen1 != gblOTPLENGTH_FT || isNumOTP == false || text == "") {
        dismissLoadingScreenPopup();
        //alert("OTP Should be Numaric and of length " + gblOTPLENGTH_FT);
        alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP")); //OTP is incorrect. Please reinput
        return false;
    }

    var inputParam = {};
    inputParam["serviceID"] = "verifyOTP";
    inputParam["retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
    inputParam["otp"] = frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text;
    invokeServiceSecureAsync("verifyOTP", inputParam, verifyOTPFTcallBack)
}

function verifyOTPFTcallBack(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (resulttable["tokenStatus"] == "Activated" && resulttable["showPinPwdCount"] != 0) {
                
                gblVerifyOTPCounter = "0";

                if (gblEditFTSchdule == true) {
                    srvFutureTransferDelete();
                } else {
                    srvFutureTransferUpdate();
                }
            }
        } else {

            if (resulttable["opstatus"] == 8005) {
                if (resulttable["errCode"] == "VrfyOTPErr00001") {
                    gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                    dismissLoadingScreenPopup();
                    showCommonAlertMYA("" + kony.i18n.getLocalizedString("invalidOTP"), null);
                    return false;
                } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                    dismissLoadingScreenPopup();
                    showCommonAlertMYA("" + kony.i18n.getLocalizedString("invalidOTP"), null);
                    return false;
                } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                    dismissLoadingScreenPopup();
                    showCommonAlertMYA("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"), null);
                    //startCrmUpdateProIBMYA("3");
                    startCrmUpdateProFTEdit("3");
                    return false;
                }

            } else {
                dismissLoadingScreenPopup();
                showCommonAlertMYA(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + resulttable["errMsg"], null);
            }

        }
    } else {
        //dismissLoadingScreenPopup();
        //
        //return false;
        if (status == 300) {
            dismissLoadingScreenPopup();
            showCommonAlertMYA("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
        }


    }
}

//Service for Holyday check

function srvHolydayChk(date) {
    showLoadingScreenPopup();
    var inputParam = {}
    inputParam["date"] = date; //date formate dd-mm-yyyy;   //"29-AUG-13";
    invokeServiceSecureAsync("getHolydayFlag", inputParam, srvHolydayChkcallBack)

}

function srvHolydayChkcallBack(status, resulttable) {
	dismissLoadingScreenPopup();
    
    
    if (status == 400) {
        
        
        var opstatus = resulttable["opstatus"];
        opstatus = opstatus.toString();
        if (opstatus == 0) {
            if (resulttable["statusCode"].trim() == "1") {
                
                alert(kony.i18n.getLocalizedString("keyFTSMARTOnHoliday"));
                //alert("Your selected transfer date is on Bank Holiday. Money will be transferred on next working day. If it 's not your preferred date,please reset");
                return false;
            } else {
                
                frmIBFTrnsrView.hbxEditFT.setVisibility(false);
                frmIBFTrnsrView.imgTMB.setVisibility(true);
                frmIBFTrnsrView.lblRemainingVal.text = frmIBFTrnsrView.lblExcuteVal.text;
            }

        } else {
            dismissLoadingScreenPopup();
            alert("getHolydayFlag service Not returning opstatus 0 ");

        }

    } else if (status == 300) {
        dismissLoadingScreenPopup();
        

    }

}

function showEditSchedule() {
    frmIBFTrnsrView.calStartDate.dateEditable = false;
    frmIBFTrnsrView.calEndDate.dateEditable = false;

    // var currentDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var currentDate = new Date(new Date(GLOBAL_TODAY_DATE).getTime() + 24 * 60 * 60 * 1000);
    var day = currentDate.getDate()
    var month = currentDate.getMonth() + 1
    var year = currentDate.getFullYear()
    frmIBFTrnsrView.calStartDate.validStartDate = [day, month, year];
    frmIBFTrnsrView.calEndDate.validStartDate = [day, month, year];

    if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Monthly")) {
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        frmIBFTrnsrView.hbxSCancelNSaveBtns.setVisibility(true);

        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Monthly");

        frmIBFTrnsrView.btnMonthly.skin = btnIBTab4MidFocus;
        frmIBFTrnsrView.btnWeekly.skin = btnIBTab4MidNrml;
        frmIBFTrnsrView.btnDaily.skin = btnIBTab4LeftNrml;
        frmIBFTrnsrView.btnYearly.skin = btnIbTab4RightNrml;
        if (gblIsEditOnDate == true && gblIsEditAftr == false) {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_OnDate");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(true);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);

            var dateSet = frmIBFTrnsrView.lblEndOnDateVal.text;
            dateSet = dateSet.split("/", 3);
            var d0 = dateSet[0];
            var d1 = dateSet[1];
            var d2 = dateSet[2];

            frmIBFTrnsrView.calEndDate.dateComponents = [d0, d1, d2];
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightFocus
        } else if (gblIsEditAftr == true && gblIsEditOnDate == false) {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_After");
            //gblScheduleEndFT = kony.i18n.getLocalizedString("keyAfter");	  
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(true);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(true);


            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidFocus;
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml;
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml;
            frmIBFTrnsrView.txtNumOfTimes.text = frmIBFTrnsrView.lblExcuteVal.text;
            frmIBFTrnsrView.txtNumOfTimes.setFocus(true);
        } else {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_Never");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftFocus
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml

        }

    } else if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Daily")) {
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        frmIBFTrnsrView.hbxSCancelNSaveBtns.setVisibility(true);

        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Daily");
        frmIBFTrnsrView.btnMonthly.skin = btnIBTab4MidNrml
        frmIBFTrnsrView.btnWeekly.skin = btnIBTab4MidNrml
        frmIBFTrnsrView.btnDaily.skin = btnIBTab4LeftFocus
        frmIBFTrnsrView.btnYearly.skin = btnIbTab4RightNrml;
        if (gblIsEditOnDate == true && gblIsEditAftr == false) {

            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_OnDate");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);

            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(true);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
            var dateSet = frmIBFTrnsrView.lblEndOnDateVal.text;
            dateSet = dateSet.split("/", 3);
            var d0 = dateSet[0];
            var d1 = dateSet[1];
            var d2 = dateSet[2];

            frmIBFTrnsrView.calEndDate.dateComponents = [d0, d1, d2];
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightFocus
        } else if (gblIsEditAftr == true && gblIsEditOnDate == false) {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_After");
            gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_After");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(true);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(true);

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidFocus
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml
            frmIBFTrnsrView.txtNumOfTimes.text = frmIBFTrnsrView.lblExcuteVal.text
        } else {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_Never");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftFocus
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml

        }

    } else if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Weekly")) {
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        frmIBFTrnsrView.hbxSCancelNSaveBtns.setVisibility(true);
        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Weekly");

        frmIBFTrnsrView.btnMonthly.skin = btnIBTab4MidNrml;
        frmIBFTrnsrView.btnWeekly.skin = btnIBTab4MidFocus;
        frmIBFTrnsrView.btnDaily.skin = btnIBTab4LeftNrml;
        frmIBFTrnsrView.btnYearly.skin = btnIbTab4RightNrml;
        if (gblIsEditOnDate == true && gblIsEditAftr == false) {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_OnDate");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(true);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
            var dateSet = frmIBFTrnsrView.lblEndOnDateVal.text;
            dateSet = dateSet.split("/", 3);
            var d0 = dateSet[0];
            var d1 = dateSet[1];
            var d2 = dateSet[2];

            frmIBFTrnsrView.calEndDate.dateComponents = [d0, d1, d2];
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightFocus
        } else if (gblIsEditAftr == true && gblIsEditOnDate == false) {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_After");
            gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_After");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(true);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(true);

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidFocus
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml
            frmIBFTrnsrView.txtNumOfTimes.text = frmIBFTrnsrView.lblExcuteVal.text
        } else {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_Never");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftFocus
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml

        }

    } else if (frmIBFTrnsrView.lblRepeatAsVal.text == kony.i18n.getLocalizedString("Transfer_Yearly")) {
        frmIBFTrnsrView.lblEnding.setVisibility(true);
        frmIBFTrnsrView.hbxSCancelNSaveBtns.setVisibility(true);

        gblScheduleRepeatFT = kony.i18n.getLocalizedString("Transfer_Yearly");
        frmIBFTrnsrView.btnMonthly.skin = btnIBTab4MidNrml;
        frmIBFTrnsrView.btnWeekly.skin = btnIBTab4MidNrml;
        frmIBFTrnsrView.btnDaily.skin = btnIBTab4LeftNrml;
        frmIBFTrnsrView.btnYearly.skin = btnIBTab4RightFocus;

        if (gblIsEditOnDate == true && gblIsEditAftr == false) {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_OnDate");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(true);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
            var dateSet = frmIBFTrnsrView.lblEndOnDateVal.text;
            dateSet = dateSet.split("/", 3);
            var d0 = dateSet[0];
            var d1 = dateSet[1];
            var d2 = dateSet[2];

            frmIBFTrnsrView.calEndDate.dateComponents = [d0, d1, d2];
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightFocus
        } else if (gblIsEditAftr == true && gblIsEditOnDate == false) {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_After");
            gblScheduleEndFT = kony.i18n.getLocalizedString("Transfer_After");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(true);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(true);

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidFocus
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftNrml
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml
            frmIBFTrnsrView.txtNumOfTimes.text = frmIBFTrnsrView.lblExcuteVal.text
        } else {
            gblEndValTemp = kony.i18n.getLocalizedString("Transfer_Never");
            frmIBFTrnsrView.hbxEndingBtns.setVisibility(true);
            frmIBFTrnsrView.hbxEditFtEndDate.setVisibility(false);
            frmIBFTrnsrView.hbxNumOfTimes.setVisibility(false);
            frmIBFTrnsrView.lblAfterIncludeMsg.setVisibility(false);
            frmIBFTrnsrView.txtNumOfTimes.text = "";

            frmIBFTrnsrView.btnAftr.skin = btnIBTab3MidNrml
            frmIBFTrnsrView.btnNever.skin = btnIBTab3LeftFocus
            frmIBFTrnsrView.btnOnDate.skin = btnIBTab3RightNrml

        }

    }

}


/*
   **************************************************************************************
		Module	: numberOfExecution
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
// for Execution times 
function numberOfExecutionFT(date1, date2, repeatAs) {

    var startDate = parseDate(date1);
    var endDate = parseDate(date2);
    var executionTimes = "";

    if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("Transfer_Daily")) || kony.string.equalsIgnoreCase(repeatAs, "Daily")) {
        executionTimes = daydiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("Transfer_Weekly")) || kony.string.equalsIgnoreCase(repeatAs, "Weekly")) {
        executionTimes = weekdiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("Transfer_Monthly")) || kony.string.equalsIgnoreCase(repeatAs, "Monthly")) {
        executionTimes = monthdiff(startDate, endDate);
    } else if (kony.string.equalsIgnoreCase(repeatAs, kony.i18n.getLocalizedString("Transfer_Yearly")) || kony.string.equalsIgnoreCase(repeatAs, "Yearly")) {
        executionTimes = yeardiff(startDate, endDate);
    }
    return executionTimes + "";
}


function formatDateFT(date) {
    var d = [];
    d = date.split("-");
    return d[2] + "/" + d[1] + "/" + d[0];
}

function hypnFormatDateFT(date) {
    var d = [];
    d = date.split("/");
    return d[2] + "-" + d[1] + "-" + d[0];

}


function completeProcess() {
    //checkNotificationAddIBFT();
    // frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text = currentDateWithTimeStamp();
    frmIBFTrnsrEditCnfmtn.hbxFTCmpletngHdr.setVisibility(true);
    frmIBFTrnsrEditCnfmtn.imgComplete.setVisibility(true);

    frmIBFTrnsrEditCnfmtn.btnReturnUpdated.setVisibility(true)
    frmIBFTrnsrEditCnfmtn.hbxFTCmpleAvilBal.setVisibility(false);

    frmIBFTrnsrEditCnfmtn.hbxFTSave.setVisibility(true)

    frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(false);


    frmIBFTrnsrEditCnfmtn.hbxFTViewHdr.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.lblFTAvailBalVal.text = frmIBFTrnsrView.lblFTEditAftrAvailBalVal.text;
    frmIBFTrnsrEditCnfmtn.lblFTHide.text = kony.i18n.getLocalizedString("keyUnhide");

    campaginService("imgAd1", "frmIBFTrnsrEditCnfmtn", "I");

    //frmIBFTrnsrEditCnfmtn.hbxAd.setVisibility(true);

    //frmIBFTrnsrEditCnfmtn.hbxAdv.setVisibility(true);
    dismissLoadingScreenPopup();
    //if nonTMB send notification


}

function fbShareFutureTransfersIB() {
    //window.open('https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.tmbbank.com&t');
    window.open('https://www.facebook.com/sharer/sharer.php?u=www.tmbbank.com', 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
    return false;

    /*
         var inputparam = {};
    	 inputparam["custNME"]=frmIBFTrnsrEditCnfmtn.lblFrmAccntNickName.text;
    	 inputparam["transXN"]="Edit Scheduled Future Transfer";
    	 inputparam["channel"]="IB";
    	 inputparam["fbUser"]="";
    	 invokeServiceSecureAsync("fbpostmessageonwall", inputparam, callbackFBFT);
    */

}

function accntNumFormat(accNo) {
    var len = accNo.length;
    var formattedAccNum = "";
    if (len == 10) {
        formattedAccNum = accNo.substring(0, 3) + "-" + accNo.charAt(3) + "-" + accNo.substring(4, 9) + "-" + accNo.charAt(9);
    } else {
        formattedAccNum = formattedAccNum;//accNo.substring(0, 3) + "-" + accNo.charAt(3) + "-" + accNo.substring(4, 9) + "-" + accNo.substring(9, len);
    }
    //var formattedAccNum = accNo.substring(0, 3) + "-" + accNo.charAt(3)+ "-" + accNo.substring(4, 9)+"-"+ accNo.charAt(9);
    return formattedAccNum;
}

//Handling the OTP lock case when 3 times wrong OTP is entered

function startCrmUpdateProFTEdit(iblock) {
        var inputParams = {
            ibUserStatusId: iblock.toString()
        };
        showLoadingScreenPopup();
        try {
            invokeServiceSecureAsync("crmProfileMod", inputParams, startCrmUpdateProFTEditcallback);
        } catch (e) {
            // todo: handle exception
            invokeCommonIBLogger("Exception in invoking service");
        }
    }
    /**
     * Callback method for startCrmUpdateProIBApplyServices()
     * @returns {}
     */

function startCrmUpdateProFTEditcallback(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            
            gblOTPLock = true;
            gblUserLockStatusIB = resulttable["IBUserStatusID"];
            dismissLoadingScreenPopup();
            popupIBFTDelete.lblConfoMsg.text = kony.i18n.getLocalizedString("Receipent_OTPLocked")
            popupIBFTDelete.show();

        } else {
            dismissLoadingScreenPopup();
            showCommonAlert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + callBackResponse["errMsg"], null);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            
        }
    }
}

function isEditSchedule() {
    var tmpDate = formatDateFT(gblFTViewStartOnDate);
    if (tmpDate != frmIBFTrnsrView.lblViewStartOnDateVal.text) {
        gblEditFTSchdule = true;
    } else {

        gblEditFTSchdule = false;

        if (gblFTViewRepeatAsVal != gblScheduleRepeatFT) {
            gblEditFTSchdule = true;

        } else {
            gblEditFTSchdule = false;
            if (gblFTViewEndVal != gblScheduleEndFT) {
                gblEditFTSchdule = true;
                /*
		  if(frmIBFTrnsrView.lblEndOnDateVal.text != "-"){
		   var temp = gblFTViewEndDate;
		     if(temp.indexOf("-",0) != -1){
		      temp = formatDateFT(gblFTViewEndDate);
		     }
		     if(temp == frmIBFTrnsrView.lblEndOnDateVal.text){
		       gblEditFTSchdule = false;
		     }
		  }
		 */

            } else {
                gblEditFTSchdule = false;
                if (gblFTViewEndVal == kony.i18n.getLocalizedString("Transfer_OnDate")) {
                    var temp = gblFTViewEndDate;
                    if (temp.indexOf("-", 0) != -1) {
                        temp = formatDateFT(gblFTViewEndDate);
                    }
                    if (temp != frmIBFTrnsrView.lblEndOnDateVal.text)
                        gblEditFTSchdule = true;

                } else if (gblFTViewEndVal == kony.i18n.getLocalizedString("Transfer_After")) {
                    if (parseFloat(gblFTViewExcuteVal) != parseFloat(frmIBFTrnsrView.lblExcuteVal.text))
                        gblEditFTSchdule = true;
                }

            }
        }
    }

    validateAmt();

}

function tokenFlagHandler() {
    if (gbltokenFlag == true) {
        //gblSwitchToken == true
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        gbltokenFlag = false;

    } else {
        if (gbltokenFlag == false) {
            //gblSwitchToken == false
            reqOTPFT();
        }
    } // end of else
}



function srvPmtAddServiceFTcallBack(status, result) {
    if (status == 400) {

        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";

        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDate) + "+" + frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text //frmIBTransferNowConfirmation.lblAccountNo.text;
        var activityFlexValues3 = gblFTViewRepeatAsVal + "+" + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;

        var tmpAmt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
        var len = tmpAmt.length;
        tmpAmt = tmpAmt.substring("0", len - 1);
        tmpAmt = parseFloat(tmpAmt.toString());

        var feeAmt = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0", lenFee - 1);
        feeAmt = parseFloat(feeAmt.toString());

        var activityFlexValues4 = gblTransfrAmt + "+" + tmpAmt;
        var activityFlexValues5 = gblpmtMethodFT;

        if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
           if(isTMB)
           {
            activityFlexValues5 = "My TMB";
            }
            else
            {
            activityFlexValues5 = "Other TMB";
            }
        }

        


        var logLinkageId = "";

        if (result["opstatus"] == 0) {
            if (result["statusCode"] == 0) {
                activityStatus = "01";
                dismissLoadingScreenPopup();
                frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text = result["currentTime"];
                completeProcess();

            } else {
                dismissLoadingScreenPopup();
                activityStatus = "02";
                
                return false;
            }

        } else {
            dismissLoadingScreenPopup();
            alert("Service failed with opstatus code : " + result["opstatus"]);
            activityStatus = "02";
        }

        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)

    }
}

function srvFutureTransferUpdatecallBack(status, result) {
    if (status == 400) {

        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";

        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDate) + "+" + frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text //frmIBTransferNowConfirmation.lblAccountNo.text;
            //var activityFlexValues3 =  frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text;  //frmIBTransferNowConfirmation.lblAccountNo.text;
        var activityFlexValues3 = gblFTViewRepeatAsVal + "+" + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;

        var tmpAmt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
        var len = tmpAmt.length;
        tmpAmt = tmpAmt.substring("0", len - 1);
        tmpAmt = parseFloat(tmpAmt.toString());

        var feeAmt = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0", lenFee - 1);
        feeAmt = parseFloat(feeAmt.toString());

        var activityFlexValues4 = gblTransfrAmt + "+" + tmpAmt;
        var activityFlexValues5 = gblpmtMethodFT;

        if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
             if(isTMB)
            {
             activityFlexValues5 = "My TMB";
            }
            else
            {
             activityFlexValues5 = "Other TMB";
            }
        }
        

        var logLinkageId = "";

        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                activityStatus = "01";
                //srvNotificationAddFT("Future Tranfer Is Updated");
                var initiatedDt = result["InitiatedDt"];
                frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text = dateFormatForDisplayWithTimeStamp(initiatedDt);
                completeProcess();
                var availBal = frmIBFTrnsrView.lblFTEditAftrAvailBalVal.text;
                var len = availBal.length;
                availBal = availBal.substring(0, len - 1);
                //availBal = kony.string.replace(availBal,",","");
                availBal = replaceCommon(availBal, ",", "");
                var finSchduleRefId = frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text;
                finSchduleRefId = finSchduleRefId.replace("F", "S");
                //setFinancialActivityLogFTUpdate(finSchduleRefId,tmpAmt,feeAmt,availBal);
                /*
        	 	 if(gblBankType == "Y")
					  setFinancialActivityLogFT(tmpAmt,feeAmt,availBal);
				   else
					  setFinancialActivityLogFT("00",tmpAmt,feeAmt,"01","4","1");
        	 	*/
            } else {
                dismissLoadingScreenPopup();
                alert("Service failed with Status code : " + StatusCode + " and Status description as " + StatusDesc);
                activityStatus = "02";
                return false;
            }

        } else {
            dismissLoadingScreenPopup();
            alert("Service failed with opstatus code : " + result["opstatus"]);
            activityStatus = "02";
            return false;
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);

    }
}


function srvFutureTransferDeleteOnlycallBack(status, result) {

    if (status == 400) {
        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Cancel";
        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDate); //frmIBTransferNowConfirmation.lblAccountNo.text;
        var activityFlexValues3 = gblFTViewRepeatAsVal;

        var tmpAmt = frmIBFTrnsrView.lblFTViewAmountVal.text;
        var len = tmpAmt.length;
        tmpAmt = tmpAmt.substring("0", len - 1);
        tmpAmt = parseFloat(tmpAmt.toString());

        var feeAmt = frmIBFTrnsrView.lblFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0", lenFee - 1);
        feeAmt = parseFloat(feeAmt.toString());

        var activityFlexValues4 = gblTransfrAmt;
        var activityFlexValues5 = gblpmtMethodFT;

        if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
            if(isTMB)
            {
            activityFlexValues5 = "My TMB";
            }
            else
            {
            activityFlexValues5 = "Other TMB";
            }
        }

        
        var logLinkageId = "";

        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                if (gblFtDel == true) {
                    dismissLoadingScreenPopup();
                    activityStatus = "01";
                    //srvNotificationAddFT("Future Tranfer Is Deleted");

                    var finSchduleRefId = frmIBFTrnsrView.lblScheduleRefNoVal.text;
                    finSchduleRefId = finSchduleRefId.replace("F", "S");

                    // setFinancialActivityLogFTDelete(finSchduleRefId);   

                    if (gblActivitiesNvgn == "F") {
                        invokeFutureInstructionService();
                        /*
					frmIBMyActivities.show();
					if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
				    showCalendar(gsSelectedDate,frmIBMyActivities,1);
				    */
                        IBMyActivitiesReloadAndShowCalendar();
                    } else if (gblActivitiesNvgn == "C") {
                        /*
				  frmIBMyActivities.show();
				  if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
				    showCalendar(gsSelectedDate,frmIBMyActivities,1);
				*/
                        IBMyActivitiesReloadAndShowCalendar();
                    }
                    
                    // Have to navigate to where the View FT is started            	

                }
            } else {
                dismissLoadingScreenPopup();
                activityStatus = "02";
                alert("Service failed with Status code : " + StatusCode + " and Status description as " + StatusDesc);
                return false;
            }

        } else {
            dismissLoadingScreenPopup();
            activityStatus = "02";
            
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);

    }
}


/*function addReceipentFT() {
    gblAdd_Receipent_State = gblNEW_RC_ADDITION;
    clearDataOnRcNewRecipientAdditionForm();
    frmIBMyReceipentsAddContactManually.show();

}*/

function addMyAccntFT() {
    frmIBMyAccnts.hbxTMBImg.setVisibility(true);
    frmIBMyAccnts.hboxAddNewAccnt.setVisibility(false);
    //frmIBMyAccnts.imgArrow.setVisibility(false);
    frmIBMyAccnts.hbxViewAccnt.setVisibility(false);
    //frmIBMyAccnts.arrwImgSeg1.setVisibility(false);
    //frmIBMyAccnts.arrwImgSeg2.setVisibility(false);
    frmIBMyAccnts.cmbobxAccntType.setVisibility(false);
    frmIBMyAccnts.lineComboAccntype.setVisibility(false);
    frmIBMyAccnts.hbxEditAccntNN.setVisibility(false);
    frmIBMyAccnts.show();

}

//Notification Add Service

/*function callBackNotificationAddFT(status, resulttable) {
    
    if (status == 400) {
        
        dismissLoadingScreenPopup();
        
        if (resulttable["opstatus"] == 0) {
            

        }
    }
}*/


function activityLogCancelFT() {
    var activityTypeID = "060";
    var errorCode = "";
    var activityStatus = "";
    var deviceNickName = "";
    var activityFlexValues1 = "Cancel";

    var activityFlexValues2 = formatDateFT(gblFTViewStartOnDate) //frmIBTransferNowConfirmation.lblAccountNo.text;
        //var activityFlexValues3 =  frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text;  //frmIBTransferNowConfirmation.lblAccountNo.text;
    var activityFlexValues3 = gblFTViewRepeatAsVal;
    var activityFlexValues4 = gblTransfrAmt;
    var activityFlexValues5 = gblpmtMethodFT;
    if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
         if(isTMB)
           {
            activityFlexValues5 = "My TMB";
            }
            else
            {
            activityFlexValues5 = "Other TMB";
            }
    }
    
    var logLinkageId = "";
    activityStatus = "00";
    activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);

}


function reqOTPFT() {

    var locale = kony.i18n.getCurrentLocale();
    var eventNotificationPolicy;
    var SMSSubject;
    var Channel;
    Channel = "FutureThirdPartyTransfer";
    var currFrm = kony.application.getCurrentForm().id;
    if(currFrm == "frmIBFTrnsrEditCnfmtn"){
        //alert("in ifff frmIBFTrnsrEditCnfmtn");
	    frmIBFTrnsrEditCnfmtn.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
	    frmIBFTrnsrEditCnfmtn.lblPlsReEnter.text = " "; 
	    frmIBFTrnsrEditCnfmtn.hbxOTPincurrect.isVisible = false;
	    frmIBFTrnsrEditCnfmtn.hbxBankRef.isVisible = true;
	    frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.isVisible = true;   
    }
    if (gblFrmAccntType == "CDA") {
        Channel = "WithdrawTD";
    }
    var pmtMethod = frmIBFTrnsrView.lblMethodName.text;
    if (gblpmtMethodFT == "SMART" || gblpmtMethodFT == "ORFT") {
        Channel = "ExecuteTransfer";
    }

    /*
    if (locale == "en_US") {
        eventNotificationPolicy = "MIB_ScheThirdPartyTransfer_EN";   //Updated with Schedule Transfer data from MApping_ServiceMIB_ECAS_V1.12
        SMSSubject = "MIB_ScheThirdPartyTransfer_EN";     
        
         if(gblFrmAccntType =="CDA")
        {
          SMSSubject = "MIB_WithdrawTD_EN" 
          eventNotificationPolicy = "MIB_WithdrawTD_EN"
        }
        var pmtMethod = frmIBFTrnsrView.lblMethodName.text;
        if(pmtMethod == "SMART" || pmtMethod == "ORFT")
        {
        	 SMSSubject = "MIB_iORFT_EN" 
          eventNotificationPolicy = "MIB_iORFT_EN"
        }
                   
    } else {
        eventNotificationPolicy = "MIB_ScheThirdPartyTransfer_TH";   
        SMSSubject = "MIB_ScheThirdPartyTransfer_TH";   
        
        if(gblFrmAccntType =="CDA")
        {
          SMSSubject = "MIB_WithdrawTD_TH" 
          eventNotificationPolicy = "MIB_WithdrawTD_TH"
        }
        
        if(pmtMethod == "SMART" || pmtMethod == "ORFT")
        {
        	 SMSSubject = "MIB_iORFT_TH" 
          eventNotificationPolicy = "MIB_iORFT_TH"
        
        }
                      
    }*/



    var amount = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text + "";
    if (amount.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) {
        amount = replaceCommon(amount, kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
    }
    var userName = frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text + "";
    var userNum = frmIBFTrnsrEditCnfmtn.lblToAccntNum.text + "";
    if (userNum.length > 0) {
        userNum = removeHyphenIB(userNum);
        userNum = "xxx-x-" + userNum.substring(userNum.length - 6, userNum.length - 1) + "-x";
    }
    var BANKREF = frmIBFTrnsrEditCnfmtn.lblBankNameVal.text;
    var inputParams = {
        retryCounterRequestOTP: gblRetryCountRequestOTP,
        Channel: Channel,
        locale: locale
    };
    showLoadingScreenPopup();
    invokeServiceSecureAsync("generateOTPWithUser", inputParams, reqOTPFTcallBack);

}

function reqOTPFTcallBack(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
            dismissLoadingScreenPopup();
            showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["opstatus"] == 0) {
            

            dismissLoadingScreenPopup();
            if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (callBackResponse["errCode"] == "JavaErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }

            frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(true);
            frmIBFTrnsrEditCnfmtn.txtBxOTPFT.setFocus(true);
            frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(true);
            frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(false);


            var reqOtpTimerFT = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            kony.timer.schedule("OTPTimerReqFT", OTPTimerCallBackFT, reqOtpTimerFT, false);

            gblOTPReqLimit = kony.os.toNumber(callBackResponse["otpRequestLimit"]);

            gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
            gblOTPLENGTH_FT = kony.os.toNumber(callBackResponse["otpLength"]);
            

            frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(true);
            frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(true);
            frmIBFTrnsrEditCnfmtn.txtBxOTPFT.maxTextLength = gblOTPLENGTH_FT;
            var refVal = "";
            for (var d = 0; callBackResponse["Collection1"].length; d++) {
                if (callBackResponse["Collection1"][d]["keyName"] == "pac") {
                    refVal = callBackResponse["Collection1"][d]["ValueString"];
                    break;
                }
            }
            frmIBFTrnsrEditCnfmtn.lblBankRefFT.text = refVal;
            frmIBFTrnsrEditCnfmtn.lblOTPMobileNumFT.text = maskingIB(gblPHONENUMBER);
            frmIBFTrnsrEditCnfmtn.btnOTPReq.skin = btnIBREQotp;
            frmIBFTrnsrEditCnfmtn.btnOTPReq.setEnabled(false);
            dismissLoadingScreenPopup();

        } else {
            dismissLoadingScreenPopup();
            alert("Error " + callBackResponse["errMsg"]);
        }
    } else {
        if (status == 300) {
            dismissLoadingScreenPopup();
            alert("Error");
        }
    }
}


function srvCrmProfileInqforFTCancel() {
    showLoadingScreenPopup();
    var inputParam = {};
    inputParam["transferFlag"] = "true";
    invokeServiceSecureAsync("crmProfileInq", inputParam, srvCrmProfileInqforFTCancelCallBack)

}

function srvCrmProfileInqforFTCancelCallBack(status, resulttable) {
    if (status == 400) {

        var activityTypeID = "060";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Cancel";

        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDate) //frmIBTransferNowConfirmation.lblAccountNo.text;
            //var activityFlexValues3 =  frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text;  //frmIBTransferNowConfirmation.lblAccountNo.text;
        var activityFlexValues3 = gblFTViewRepeatAsVal;
        var activityFlexValues4 = gblTransfrAmt;
        var activityFlexValues5 = gblpmtMethodFT;

        if (gblpmtMethodFT == "INTERNAL_TRANSFER") {
             if(isTMB)
            {
             activityFlexValues5 = "My TMB";
            }
            else
            {
             activityFlexValues5 = "Other TMB";
            }
        }
        

        var logLinkageId = "";
        activityStatus = "00";

        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                activityStatus = "00"

                var ibStatus = resulttable["ibUserStatusIdTr"];
                if (ibStatus == gblIBStatus) {
                    dismissLoadingScreenPopup(); //gblFinancialTxnIBLock  
                    //alert("User Channel Status is Locked");
                    alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
                    return false;
                }
                dismissLoadingScreenPopup();
                popupIBFTDelete.lblConfoMsg.text = kony.i18n.getLocalizedString("FTDeletesureMessage");
                // popupIBFTDelete.lblConfoMsg.text = "Are you sure to delete this Future Transaction?";
                popupIBFTDelete.show();

            } else {
                dismissLoadingScreenPopup();
                activityStatus = "02";
                alert(resulttable["StatusDesc"]);
            }

        } else {
            dismissLoadingScreenPopup();
            activityStatus = "02";
            alert(resulttable["errMsg"]);
        }
        activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);

    }
}



function saveAsPDFFutureTransferIB(filetype) {
    var inputParam = {}
    inputParam["templatename"] = "FutureTransferComplete";
    inputParam["filetype"] = filetype;
    var amt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
    var len = amt.length;
    amt = amt.substring("0", len - 1);
    var fee = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;
    var lenfee = fee.length;
    fee = fee.substring("0", lenfee - 1);

    var notifyVia = "";
    var notifyThru = frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text;
    if (notifyThru == "-") {
        notifyVia = "";
    } else {
        if (notifyThru.indexOf("@", 0) != -1) {
            notifyVia = "Email: " + notifyThru;
        } else
            notifyVia = "SMS: " + notifyThru;
    }

    var transferOrderDate = frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text;
    transferOrderDate = transferOrderDate.split(" ")[0];

    var transferSchedule = "";
    if (frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text == "Once") {
        transferSchedule = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text + " " + " Repeat " + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;
    } else if (frmIBFTrnsrEditCnfmtn.lblExcuteVal.text == "-") {
        transferSchedule = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text + " " + " Repeat " + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;
    } else {
        transferSchedule = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text + " to " + frmIBFTrnsrEditCnfmtn.lblEndOnDateVal.text + " " + " Repeat " + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text + " for " + frmIBFTrnsrEditCnfmtn.lblExcuteVal.text + " times ";
    }

    var toAcctName = "";
    if (frmIBFTrnsrView.lblToAccntName.isVisible) {
        toAcctName = frmIBFTrnsrEditCnfmtn.lblToAccntName.text;
    } else
        toAcctName = frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text;

    var fromAcctNo = frmIBFTrnsrEditCnfmtn.lblFrmAccntNum.text;
    if (fromAcctNo.indexOf("-") != -1) fromAcctNo = replaceCommon(fromAcctNo, "-", "");
    var length = fromAcctNo.length;
    if (length == 10) {
        fromAcctNo = "XXX-X-" + fromAcctNo.substring(length - 6, length - 1) + "-X";
    } else
        fromAcctNo = frmIBFTrnsrEditCnfmtn.lblFrmAccntNum.text;

    var toAcctNo = frmIBFTrnsrEditCnfmtn.lblToAccntNum.text;
    if (toAcctNo.indexOf("-") != -1) toAcctNo = replaceCommon(toAcctNo, "-", "");
    var len = toAcctNo.length;
    if (len == 10) {
        toAcctNo = "XXX-X-" + toAcctNo.substring(len - 6, len - 1) + "-X";
    } else
        toAcctNo = frmIBFTrnsrEditCnfmtn.lblToAccntNum.text;

    var pdfImagedata = {
        "localeId": kony.i18n.getCurrentLocale(),
        "fromAcctNo": fromAcctNo,
        "fromAcctName": frmIBFTrnsrEditCnfmtn.lblFrmAccntName.text,
        "toAcctNo": toAcctNo,
        "toAcctName": toAcctName,
        "bankName": frmIBFTrnsrEditCnfmtn.lblBankNameVal.text,
        "transferAmount": amt,
        "transferFee": fee,
        "myNote": frmIBFTrnsrEditCnfmtn.lblMyNoteVal.text,
        "noteToRecipient": frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text,
        "notifyVia": notifyVia,
        "transactionRefNo": frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text,
        "transferOrderDate": transferOrderDate,
        "transferSchedule": transferSchedule

    };
    inputParam["outputtemplatename"] = "Future_Transfer_Set_Details_" + kony.os.date("ddmmyyyy");


    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
	saveFuturePDF(filetype, "060", frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text);
}

function onclickOfNext() {

    if (gbltokenFlag == true) {
        frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keySwitchToSMS");

        frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.txtBxOTPFT.setFocus(true);
        frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(false);

    } else {
        frmIBFTrnsrEditCnfmtn.hbxBankRef.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.lblOTP.text = kony.i18n.getLocalizedString("keyOTP");
        frmIBFTrnsrEditCnfmtn.lblMsgRequset.text = kony.i18n.getLocalizedString("keyotpmsgreq");
        frmIBFTrnsrEditCnfmtn.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
        reqOTPFT()
    }

}

function srvVerifyPasswordExFT_CS() {
	showLoadingScreenPopup();
    var inputParam = {};
    inputParam["flowspa"] = false
    inputParam["gblEditFTSchdule"] = gblEditFTSchdule;
    inputParam["ClientDt"] = gblOrderDateFT;
    inputParam["channel"] = "IB";
	var transferDate = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text;
	transferDate = changeDateFormatForService(transferDate);
    var transferAmt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
    var len = transferAmt.length;
    transferAmt = transferAmt.substring("0", len - 1);
    transferAmt = transferAmt.trim();

    if (transferAmt.indexOf(",") != -1) {
        transferAmt = parseFloat(replaceCommon(transferAmt, ",", "")).toFixed(2);
    }else
    	transferAmt = parseFloat(transferAmt).toFixed(2);
    	
   /*
    transferAmt = parseFloat(transferAmt.toString())
    transferAmt = "" + transferAmt;
    if (transferAmt.indexOf(".") != -1) {
        var tmp = transferAmt.split(".", 2);
        if (tmp[1].length == 1) {
            transferAmt = transferAmt + "0";
        } else if (tmp[1].length > 2) {
            tmp[1] = tmp[1].substring("0", "2")
            transferAmt = ""
            transferAmt = tmp[0] + "." + tmp[1];
        }
    }
	*/
	
    inputParam["Amt"] = transferAmt;

    if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
        inputParam["gblTokenSwitchFlag"] = true;
        inputParam["verifyTokenEx_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        inputParam["verifyTokenEx_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        inputParam["verifyTokenEx_userId"] = gblUserName;
        inputParam["verifyTokenEx_password"] = frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text;
        inputParam["verifyTokenEx_channel"] = "InterNet Banking";
        
    } else {
        inputParam["gblTokenSwitchFlag"] = false;
        inputParam["verifyPasswordEx_retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
        inputParam["verifyPasswordEx_password"] = frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text;
        inputParam["verifyPasswordEx_userId"] = gblUserName;
    }

    //doPmtMod
    inputParam["doPmtMod_rqUID"] = "";
    var tempDate = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text;
    inputParam["doPmtMod_Starton"] = hypnFormatDateFT(tempDate);
    inputParam["doPmtMod_channelId"] = "IB";
    // doPmtCan
    //doPmtAdd	

    inputParam["doPmtAdd_channelId"] = "IB";
    inputParam["doPmtAdd_toAccTType"] = gblToAccntType;
    inputParam["doPmtAdd_dueDate"] = changeDateFormatForService(frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text);
    inputParam["doPmtAdd_memo"] = frmIBFTrnsrEditCnfmtn.lblMyNoteVal.text;

    inputParam["doPmtAdd_toNickName"] = gblToAccntNick; //frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text;		// CR Change

    if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_After") || gblScheduleEndFT == "After") {
        inputParam["doPmtAdd_NumInsts"] = frmIBFTrnsrEditCnfmtn.lblExcuteVal.text;
      	inputParam["NotificationAdd_NumInsts"] = frmIBFTrnsrEditCnfmtn.lblExcuteVal.text;
    }

    if (gblScheduleEndFT == kony.i18n.getLocalizedString("Transfer_OnDate") || gblScheduleEndFT == "On Date") {
        inputParam["doPmtAdd_FinalDueDt"] = changeDateFormatForService(frmIBFTrnsrEditCnfmtn.lblEndOnDateVal.text);
		inputParam["NotificationAdd_NumInsts"] = frmIBFTrnsrEditCnfmtn.lblExcuteVal.text;
    }
    var frequency = frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;
	inputParam["doPmtAdd_freq"] = frequency;
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyDailyNE"))) {
        inputParam["doPmtAdd_freq"] = "Daily";
        inputParam["NotificationAdd_recurring"] = "keyDaily";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyWeeklyNE"))) {
        inputParam["dayOfWeek"] = "";
      	inputParam["doPmtAdd_freq"] = "Weekly";
        inputParam["NotificationAdd_recurring"] = "keyWeekly";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyMonthlyNE"))) {
        inputParam["doPmtAdd_freq"] = "Monthly";
        inputParam["NotificationAdd_recurring"] = "keyMonthly";
      	inputParam["dayOfMonth"] = "";
    }
    if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyYearlyNE"))) {
        frequency = "Annually";
        inputParam["dayofMonth"] = "";
        inputParam["monthofYear"] = "";
      	inputParam["doPmtAdd_freq"] = "Annually";
        inputParam["NotificationAdd_recurring"] = "keyYearly";
    }
    if(kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnceNT")) || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnceNE"))){
        inputParam["doPmtAdd_freq"] = "once";
        inputParam["NotificationAdd_recurring"] = "keyOnce";
      	inputParam["NotificationAdd_NumInsts"] = "1";
    }
  	frequency = inputParam["doPmtAdd_freq"];
    inputParam["doPmtAdd_toBankId"] = gblToBankCdFT;

    var pmtMethod = frmIBFTrnsrEditCnfmtn.lblMethodName.text;

    if (gblpmtMethodFT == "SMART" || gblpmtMethodFT == "ORFT") {
        if (gblNotificationType == "Email" && frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text != "-") {
            inputParam["doPmtAdd_notificationType"] = "1";
            inputParam["doPmtAdd_recipientEmailAddr"] = frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text;
            inputParam["doPmtAdd_receipientNote"] = frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text;
        } else if (gblNotificationType == "SMS" && frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text != "-") {
            inputParam["doPmtAdd_notificationType"] = "0";
            inputParam["doPmtAdd_recipientMobileNbr"] = frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text;
            inputParam["doPmtAdd_receipientNote"] = frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text;
        }

    }
    if (gblpmtMethodFT == "ORFT") {
        inputParam["doPmtAdd_PmtMiscType"] = "MOBILE";
        inputParam["doPmtAdd_MiscText"] = gblPHONENUMBER;
    }



    //NotificationAdd

    var deliveryMethod;
    var phoneNo = gblPHONENUMBER

    var emailID = gblEmailId;
    var BANKREF = frmIBFTrnsrEditCnfmtn.lblBankNameVal.text;
    var notifyThru = frmIBFTrnsrEditCnfmtn.lblNotifyRecipientVal.text;
    inputParam["NotificationAdd_bank"] = gblbankInfo.bankShortName;
    
    if (frmIBFTrnsrEditCnfmtn.hbxNotifyReceipent.isVisible) {
        if (notifyThru.indexOf("@") != -1) {
            inputParam["NotificationAdd_recipientMobile"] = "";
            inputParam["NotificationAdd_recipientEmail"] = notifyThru;
        } else {
            inputParam["NotificationAdd_recipientMobile"] = notifyThru;
            inputParam["NotificationAdd_recipientEmail"] = "";
        }
    } else {
        inputParam["NotificationAdd_recipientMobile"] = "";
        inputParam["NotificationAdd_recipientEmail"] = "";
    }


    if (frmIBFTrnsrEditCnfmtn.hbxNoteToReceipent.isVisible) {
        inputParam["NotificationAdd_recipientMessage"] = frmIBFTrnsrEditCnfmtn.lblNoteToRecipientVal.text;
    } else
        inputParam["NotificationAdd_recipientMessage"] = "";



    inputParam["NotificationAdd_phoneNumber"] = phoneNo;
    inputParam["NotificationAdd_phoneNo"] = phoneNo;
    inputParam["NotificationAdd_custName"] = gblCustomerName;
    inputParam["NotificationAdd_frmAcctName"] = frmIBFTrnsrEditCnfmtn.lblFrmAccntName.text;
    inputParam["NotificationAdd_toAcctName"] = frmIBFTrnsrEditCnfmtn.lblToAccntName.text;
    inputParam["NotificationAdd_toAcctBank"] = BANKREF;
    inputParam["NotificationAdd_channelName"] = "IB";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    var transAmt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
    if (transAmt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1)
        transAmt = transAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
    inputParam["NotificationAdd_transferAmt"] = transAmt; //frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
    inputParam["NotificationAdd_memo"] = frmIBFTrnsrEditCnfmtn.lblMyNoteVal.text;
    inputParam["NotificationAdd_channelID"] = "IB";
    inputParam["NotificationAdd_emailId"] = emailID;

    var dateTr = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text;
    var txnRefNo = frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text;
    var fee = frmIBFTrnsrEditCnfmtn.lblFeeVal.text;

    if (fee.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1) {
        fee = fee.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
    }

    inputParam["NotificationAdd_transferDate"] = dateTr;
    inputParam["NotificationAdd_transferFee"] = fee;
    inputParam["NotificationAdd_transferOrderDate"] = frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text;
    inputParam["NotificationAdd_endDate"] = frmIBFTrnsrEditCnfmtn.lblEndOnDateVal.text;

    if (gblpersonalizedName != "") {
        inputParam["NotificationAdd_RecipentName"] = gblpersonalizedName;
    } else {
        inputParam["NotificationAdd_RecipentName"] = frmIBFTrnsrEditCnfmtn.lblToAccntNickName.text;
    }

    if (gblPersoanlizedId == "" || gblpersonalizedName == "_MyTMB_" || gblOwnPersonlisedIdFT == gblPersoanlizedId) {
        inputParam["NotificationAdd_RecipentName"] = gblCustomerName;
    }

    if (gblEditFTSchdule) {
        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDate) + "+" + frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text;
        var activityFlexValues3 = gblFTViewRepeatAsVal + "+" + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;
    } else {
        var activityFlexValues2 = formatDateFT(gblFTViewStartOnDate);
        var activityFlexValues3 = gblFTViewRepeatAsVal;
    }

    var tmpAmt = frmIBFTrnsrEditCnfmtn.lblFTViewAmountVal.text;
    var len = tmpAmt.length;
    tmpAmt = tmpAmt.substring("0", len - 1);
    tmpAmt = parseFloat(tmpAmt.toString());

    var activityFlexValues4 = gblTransfrAmt + "+" + tmpAmt;
    var activityFlexValues5 = gblpmtMethodFT; //frmIBFTrnsrEditCnfmtn.lblMethodName.text;
    if (gblpmtMethodFT == "INTERNAL_TRANSFER")
    {
           if(isTMB)
           {
             activityFlexValues5 = "My TMB";
            }
            else
            {
             activityFlexValues5 = "Other TMB";
            }
    }
        

    inputParam["activityLog_channelId"] = "01";
    inputParam["activityLog_activityFlexValues2"] = activityFlexValues2;
    inputParam["activityLog_activityFlexValues3"] = activityFlexValues3;
    inputParam["activityLog_activityFlexValues4"] = activityFlexValues4;
    inputParam["activityLog_activityFlexValues5"] = activityFlexValues5;

	inputParam["TransferDate"] = transferDate;
	
	var transferSchedule = "";
    if (frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text == "Once") {
        transferSchedule = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;
    } else if (frmIBFTrnsrEditCnfmtn.lblExcuteVal.text == "-") {
        transferSchedule = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text;
    } else {
        transferSchedule = frmIBFTrnsrEditCnfmtn.lblViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBFTrnsrEditCnfmtn.lblEndOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text + " " + kony.i18n.getLocalizedString("keyFor") + " " + frmIBFTrnsrEditCnfmtn.lblExcuteVal.text + " " + kony.i18n.getLocalizedString("keyTimesIB");
    }
	
	inputParam["PaymentSchedule"] = transferSchedule;
	
    invokeServiceSecureAsync("EditFutureTransferCompositeService", inputParam, callBackExcuteFTComposite);

}

function callBackExcuteFTComposite(status, result) {
    
    dismissLoadingScreenPopup();
    if (result["opstatus"] == "1") {
        alert("Transaction Can Not Be Preocessed");
        return false;
    }
    if (result["opstatusVPX"] == "0") {
        gblVerifyOTPCounter = "0";
        gblRetryCountRequestOTP = "0";
        if (result["opstatus"] == "0") {
            if (gblEditFTSchdule) {
                var transRefNum = result["transRefNum"];
                //transRefNum = transRefNum.replace("S", "F");
                frmIBFTrnsrEditCnfmtn.lblScheduleRefNoVal.text = transRefNum;
                frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text = result["currentTime"];
            } else {
                var initiatedDt = result["InitiatedDt"];
                frmIBFTrnsrEditCnfmtn.lblOrderDateVal.text = dateFormatForDisplayWithTimeStamp(initiatedDt);
            }
            completeProcess();
        } else {
            dismissLoadingScreenPopup();
            alert(result["errMsg"]);
            return false;
        }
    } else {
        
        frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text = "";
        if (result["opstatusVPX"] == "8005") {
            if (result["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
                dismissLoadingScreenPopup();
                if (frmIBFTrnsrEditCnfmtn.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
                    //alert(""+result["errMsg"]);
                   // alert("" + kony.i18n.getLocalizedString("invalidOTP")); 
                    frmIBFTrnsrEditCnfmtn.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBFTrnsrEditCnfmtn.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo"); 
                    frmIBFTrnsrEditCnfmtn.hbxOTPincurrect.isVisible = true;
                    frmIBFTrnsrEditCnfmtn.hbxBankRef.isVisible = false;
                    frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.isVisible = false;
                    frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text = "";
                    frmIBFTrnsrEditCnfmtn.txtBxOTPFT.setFocus(true);

                   //alert("in iffff");
                    return false;
                } else {
                   // alert("in elseee");
                   // alert("" + kony.i18n.getLocalizedString("invalidOTP"));//commented by swapna.
                    frmIBFTrnsrEditCnfmtn.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                    frmIBFTrnsrEditCnfmtn.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo"); 
                    frmIBFTrnsrEditCnfmtn.hbxOTPincurrect.isVisible = true;
                    frmIBFTrnsrEditCnfmtn.hbxBankRef.isVisible = false;
                    frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.isVisible = false;
                    frmIBFTrnsrEditCnfmtn.txtBxOTPFT.text = "";
                    frmIBFTrnsrEditCnfmtn.txtBxOTPFT.setFocus(true);
                    return false;
                }
            } else if (result["errCode"] == "VrfyOTPErr00002") {
                dismissLoadingScreenPopup();
                handleOTPLockedIB(result);
                return false;
            } else {
                frmIBFTrnsrEditCnfmtn.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBFTrnsrEditCnfmtn.lblPlsReEnter.text = " "; 
                frmIBFTrnsrEditCnfmtn.hbxOTPincurrect.isVisible = false;
                frmIBFTrnsrEditCnfmtn.hbxBankRef.isVisible = true;
                frmIBFTrnsrEditCnfmtn.hbxOTPMsgNphnNum.isVisible = true;
                dismissLoadingScreenPopup();
                //alert(" "+result["errMsg"]);
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
}


function frmIBFTrnsrEditCnfmtnPreShow() {
    frmIBFTrnsrEditCnfmtn.hbxFTCmpletngHdr.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.imgComplete.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.btnReturnUpdated.setVisibility(false)
    frmIBFTrnsrEditCnfmtn.hbxFTCmpleAvilBal.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.hbxFTSave.setVisibility(false)
    frmIBFTrnsrEditCnfmtn.hbxFTViewHdr.setVisibility(true);
    frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(false);
    frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(true);
    frmIBFTrnsrEditCnfmtn.hbxAdv.setVisibility(false);

    var prevFrm = kony.application.getCurrentForm().id;
    
    
    if (prevFrm == "frmIBFBLogin") {
        
        frmIBFTrnsrEditCnfmtn.hbxFTCmpletngHdr.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.imgComplete.setVisibility(true);
        frmIBFTrnsrEditCnfmtn.btnReturnUpdated.setVisibility(true)
        frmIBFTrnsrEditCnfmtn.hbxFTCmpleAvilBal.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.hbxFTSave.setVisibility(true)
        frmIBFTrnsrEditCnfmtn.hbxFTViewHdr.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.hbxOTP.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.hbxEditBtns.setVisibility(false);
        frmIBFTrnsrEditCnfmtn.btnNext.setVisibility(false);
        //frmIBFTrnsrEditCnfmtn.hbxAdv.setVisibility(true);
    }

}

function syncIBEditFT() {
    
    var curFrm = kony.application.getCurrentForm().id;
    if ((curFrm == "frmIBFTrnsrView") || (curFrm == "frmIBFTrnsrEditCnfmtn")) {
        if (gblRepeatAsforLS == "Daily") {
            frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyDaily");
            frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyDaily");
            frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyDaily");
        } else if (gblRepeatAsforLS == "Weekly") {
            frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
            frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyWeekly");
            frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
        } else if (gblRepeatAsforLS == "Monthly") {
            frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
            frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyMonthly");
            frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
        } else if (gblRepeatAsforLS == "Yearly") {
            frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
            frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyYearly");
            frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");
        } else if (gblRepeatAsforLS == "Once") {
            frmIBFTrnsrView.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce");
            frmIBFTrnsrView.lblRepeatAsValView.text = kony.i18n.getLocalizedString("keyOnce");
            frmIBFTrnsrEditCnfmtn.lblRepeatAsVal.text = kony.i18n.getLocalizedString("keyOnce");
        }

    }

}




function srvTokenSwitchingDummy() {
    var inputParam = [];
    //showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingDummyCallBack);
}

function srvTokenSwitchingDummyCallBack(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
        	getFeeCrmProfileInq()
        }
	}
}



function editSchedulePromptPayAccNameCheck(){
	
	
	var inputParam = {}
 	inputParam["fromAcctNo"] = gblFrmAccntID; // Need to update 
	inputParam["toAcctNo"] = gblToAccntID; // Need to update 
	inputParam["toFIIdent"] = gblToBankCdFT;	// Need to check 
	inputParam["transferAmt"] = gblTransfrAmt; // Need to Update 
	inputParam["mobileOrCI"] = "00";
  	
	showLoadingScreen();
	invokeServiceSecureAsync("checkOnUsPromptPayinq", inputParam, editSchedTransferToAccntPromptPay);
}

function editSchedTransferToAccntPromptPay(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0) {
			var ToAccountName = "";
			if(gblisTMB != gblTMBBankCD){
				ToAccountName = resulttable["toAcctName"];
			}else{
				ToAccountName = resulttable["toAccTitle"];
			}
			frmIBFTrnsrView.lblToAccntName.text= ToAccountName;
			 frmIBFTrnsrView.show();
             dismissLoadingScreenPopup();
		}else {
			frmIBFTrnsrView.show();
			dismissLoadingScreenPopup();
		}
	}
}