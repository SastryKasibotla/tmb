var gblloanCollectionData = [];
var gblLoanSearchFlow = "";
var gblLoanFullAddressSearch = [];

function initWorkingInformationfrm(){
  frmLoanWorkInfo.onDeviceBack = disableBackButton;
  frmLoanWorkInfo.preShow = workingInformationPreShow;
  frmLoanWorkInfo.postShow = workingInformationPostShow;
}

function linkWorkInfoEvents(){
  try{
    kony.print("linkWorkInfoEvents");
    frmLoanWorkInfo.btnBackWorkInfo.onClick = settingSavedValueInWF;
    frmLoanWorkInfo.FlexEmploymentStatus.onClick = getEmployeeStatus;

    frmLoanWorkInfo.flxOccupation.onClick = getOccupation;
    frmLoanWorkInfo.flxProfessional.onClick = getProfessions;
    kony.print("linkWorkInfoEvents @@ -2");

    frmLoanWorkInfo.flxDepartmentName.onClick = departmentSettings;
    frmLoanWorkInfo.tbxDept.onTextChange = workingInformationPostShow;
    frmLoanWorkInfo.tbxDept.onDone = onDoneDeptWI;
    frmLoanWorkInfo.tbxDept.onTouchEnd = touchEndDeptWI;
    frmLoanWorkInfo.tbxDept.onEndEditing = endEditDeptWI;
    frmLoanWorkInfo.flxContractEmployed.onClick = contractEmp;
    frmLoanWorkInfo.flxLenthOfEmployment.onClick = lengthCurrentEmp;
    frmLoanWorkInfo.flxWorkingAddress.onClick = getfulladdress;
    frmLoanWorkInfo.flxWorkingAddressFilled.onClick = getfulladdress;
    //office
    frmLoanWorkInfo.flxOfficeNo.onClick = onClickContactNo;
    frmLoanWorkInfo.flxExtNo.onClick = onClickExtNo;
    frmLoanWorkInfo.tbxOfficeNo.onDone = endEditOfcNo;
    frmLoanWorkInfo.tbxOfficeNo.onTouchEnd = touchEndOfcNo;
    frmLoanWorkInfo.tbxOfficeNo.onEndEditing = endEditOfcNo;

    frmLoanWorkInfo.tbxExtNo.onDone = endEditExtNo;
    frmLoanWorkInfo.tbxExtNo.onTouchEnd = touchEndExtNo;
    frmLoanWorkInfo.tbxExtNo.onEndEditing = endEditExtNo;

    frmLoanWorkInfo.btnNext.onClick = saveAndContinue;
    frmLoanWorkInfo.btnContractPopUpClose.onClick = closeContractPopUp;
    kony.print("linkWorkInfoEvents @@ -1");
    frmLoanWorkInfo.btnEmploy.onClick = cancelContractEmp;
    frmLoanWorkInfo.btnContracted.onClick = nextContractEmp;
    frmLoanWorkInfo.calStartDate.onSelection = selectStartDate;
    frmLoanWorkInfo.calEndDate.onSelection = selectEndDate;
    frmLoanWorkInfo.btnMonthlystmt.onClick = monthly;
    frmLoanWorkInfo.btnYearlystmt.onClick = yearly;
    kony.print("linkWorkInfoEvents @@ 3");
    frmLoanWorkInfo.btnPopUpClosePopUp.onClick = closeDynamicPopup;
    kony.print("linkWorkInfoEvents @@ 4");

    kony.print("linkWorkInfoEvents @@ 5");
    frmLoanWorkInfo.tbxOfficeNo.onTextChange = formatContactNo;
    frmLoanWorkInfo.tbxDept.onTextChange = handleSaveBtnWorkInfo;
    //frmLoanWorkInfo.tbxOfficeNo.onTextChange = handleSaveBtnWorkInfo;
    //frmLoanWorkInfo.tbxExtNo.onTextChange = handleSaveBtnWorkInfo;

    frmLoanWorkInfo.tbxSearch.onTextChange = searchloanworkinginfo;
    frmLoanWorkInfo.segDyna.onRowClick = assignValue; 
    
     //  frmLoanWorkInfo.FlexAddNo.onTouchEnd = loanAddNoOnClick;
   
    frmLoanWorkInfo.FlexAddNo.onClick = loanAddNoOnClick;
    frmLoanWorkInfo.flxSearchAddress.setVisibility(false);
    frmLoanWorkInfo.tbxAddrVal.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxAddrVal.onEndEditing = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxAddrVal.onDone = onFocusBuildingno;
    
//  frmLoanWorkInfo.FlexBuildingNo.onTouchEnd = loanBuildingNoOnClick;
    frmLoanWorkInfo.FlexBuildingNo.onClick = loanBuildingNoOnClick;
  
  
//  frmLoanWorkInfo.tbxBuildingNo.onTextChange = onTextChangeBuildNo;
  
    frmLoanWorkInfo.tbxBuildingNo.onDone = onFocusFloor;
    frmLoanWorkInfo.tbxBuildingNo.onEndEditing = onTextChangeBuildNo;
     
    frmLoanWorkInfo.FlexFloorNo.onClick = loanFloorNoOnClick;
  
//  frmLoanWorkInfo.tbxFloorNo.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxFloorNo.onDone = onFocusMoo;
    frmLoanWorkInfo.tbxFloorNo.onEndEditing = onTextChangeBuildNo;
  
	frmLoanWorkInfo.FlexSoi.onClick = loanSoiOnClick;
  
 // frmLoanWorkInfo.tbxSoi.onTextChange = onTextChangeBuildNo;
  
    frmLoanWorkInfo.tbxSoi.onDone = onFocusRoad;
    frmLoanWorkInfo.tbxSoi.onEndEditing = onTextChangeBuildNo;
    
    frmLoanWorkInfo.FlexRoad.onClick = loanRoadOnClick;
  
//  frmLoanWorkInfo.tbxRoad.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxRoad.onDone = onFocusSubDistrict;
    frmLoanWorkInfo.tbxRoad.onEndEditing = onTextChangeBuildNo;
  
    frmLoanWorkInfo.FlexMoo.onClick = loanMooOnClick;
  
//  frmLoanWorkInfo.tbxMoo.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxMoo.onDone = onFocusSoi;
    frmLoanWorkInfo.tbxMoo.onEndEditing = onTextChangeBuildNo;
    
    frmLoanWorkInfo.flxSubDistrict.onClick = loanSubDistrictOnClick;
//  frmLoanWorkInfo.tbxSubDistrict.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxSubDistrict.onDone = onFocusDistrict;
    frmLoanWorkInfo.tbxSubDistrict.onTextChange = searchsubdistrictloan;
  
    frmLoanWorkInfo.tbxSubDistrict.onEndEditing = onTextChangeBuildNo;
    frmLoanWorkInfo.flxDistrict.onClick = loanDistrictOnClick;
    frmLoanWorkInfo.tbxDistrict.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxDistrict.onDone = onFocusProvince;
    frmLoanWorkInfo.tbxDistrict.onEndEditing = onTextChangeBuildNo;
    frmLoanWorkInfo.flxProvince.onClick = loanProvinceOnClick;
    frmLoanWorkInfo.tbxProvince.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxProvince.onDone = onFocusZipCode;
    frmLoanWorkInfo.tbxProvince.onEndEditing = onTextChangeBuildNo;
    frmLoanWorkInfo.flxZipCode.onClick = loanZipCodeOnClick;
    frmLoanWorkInfo.tbxZipCode.onTextChange = onTextChangeBuildNo;
    frmLoanWorkInfo.tbxZipCode.onEndEditing = onTextChangeBuildNo;
  }
  catch(e){
    kony.print("exception in @@ linking events: "+e);
    kony.print("exception in @@ linking events message: "+e.message);
  }
}

function onFocusBuildingno(){
     frmLoanWorkInfo.lblMainBuildingNo.isVisible=false;
     frmLoanWorkInfo.lblBuildingNo.isVisible=true;
     frmLoanWorkInfo.tbxBuildingNo.isVisible=true;
     frmLoanWorkInfo.tbxBuildingNo.setFocus(true);
}

function onFocusFloor(){
     frmLoanWorkInfo.lblMainFloorNo.isVisible=false;
     frmLoanWorkInfo.lblFloorNo.isVisible=true;
     frmLoanWorkInfo.tbxFloorNo.isVisible=true;
     frmLoanWorkInfo.tbxFloorNo.setFocus(true);  
}

function onFocusMoo(){
     frmLoanWorkInfo.lblMainMoo.isVisible=false;
     frmLoanWorkInfo.lblMoo.isVisible=true;
     frmLoanWorkInfo.tbxMoo.isVisible=true;
     frmLoanWorkInfo.tbxMoo.setFocus(true);
}

function onFocusSoi(){
     frmLoanWorkInfo.lblMainSoi.isVisible=false;
     frmLoanWorkInfo.lblSoi.isVisible=true;
     frmLoanWorkInfo.tbxSoi.isVisible=true;
     frmLoanWorkInfo.tbxSoi.setFocus(true);
}

function onFocusRoad(){
     frmLoanWorkInfo.LblMainRoadNo.isVisible=false;
     frmLoanWorkInfo.lblRoad.isVisible=true;
     frmLoanWorkInfo.tbxRoad.isVisible=true;
     frmLoanWorkInfo.tbxRoad.setFocus(true);
}

function onFocusSubDistrict(){
     frmLoanWorkInfo.lblMainSubDistrict.isVisible=false;
     frmLoanWorkInfo.lblSubDistrict.isVisible=true;
     frmLoanWorkInfo.tbxSubDistrict.isVisible=true;
     frmLoanWorkInfo.tbxSubDistrict.setFocus(true);
}

function onFocusDistrict(){
     frmLoanWorkInfo.lblMainDistrict.isVisible=false;
     frmLoanWorkInfo.lblDistrict.isVisible=true;
     frmLoanWorkInfo.tbxDistrict.isVisible=true;
     frmLoanWorkInfo.tbxDistrict.setFocus(true);
}

function onFocusProvince(){
    frmLoanWorkInfo.lblMainProvince.isVisible=false;
    frmLoanWorkInfo.lblProvince.isVisible=true;
    frmLoanWorkInfo.tbxProvince.isVisible=true;
    frmLoanWorkInfo.tbxProvince.setFocus(true);
}

function onFocusZipCode(){
    frmLoanWorkInfo.lblMainZipCode.isVisible=false;
    frmLoanWorkInfo.lblZipCode.isVisible=true;
    frmLoanWorkInfo.tbxZipCode.isVisible=true;
    frmLoanWorkInfo.tbxZipCode.setFocus(true);
}

function workingInfoPhrases(){
  try{
    frmLoanWorkInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("Working_Info_Title");
    frmLoanWorkInfo.lblPersonalInfo.text = kony.i18n.getLocalizedString("Working_Info_Title");
    frmLoanWorkInfo.lblEmploymentStatus.text = kony.i18n.getLocalizedString("Employment_Status_textbox");
    kony.print("workingInformationPreShow @@ 2");
    frmLoanWorkInfo.lblEmploymentStatusHeader.text = kony.i18n.getLocalizedString("Employment_Status_textbox");

    frmLoanWorkInfo.lblOccupation.text = kony.i18n.getLocalizedString("Occupation_textbox");
    frmLoanWorkInfo.lblOccupationHeader.text = kony.i18n.getLocalizedString("Occupation_textbox");

    frmLoanWorkInfo.lblProfessional.text = kony.i18n.getLocalizedString("Professional_textbox");
    frmLoanWorkInfo.lblProfessionalHeader.text = kony.i18n.getLocalizedString("Professional_textbox");

    frmLoanWorkInfo.lblDateOfCitID.text = kony.i18n.getLocalizedString("Length_of_Current_Employment_textbox");

    frmLoanWorkInfo.lblDepartmentName.text = kony.i18n.getLocalizedString("Company_Name_textbox");
    frmLoanWorkInfo.lblDepartmentNameHeader.text = kony.i18n.getLocalizedString("Company_Name_textbox");
  
    frmLoanWorkInfo.lblContractEmployed.text = kony.i18n.getLocalizedString("Contract_Employed_textbox");
    frmLoanWorkInfo.lblContractEmployedHeader.text = kony.i18n.getLocalizedString("Contract_Employed_textbox");
    kony.print("workingInformationPreShow @@ 3");
    frmLoanWorkInfo.lblWorkingAddress.text = kony.i18n.getLocalizedString("Working_Address_textbox");
  
    frmLoanWorkInfo.btnNext.text = kony.i18n.getLocalizedString("SaveandContinue");
    //Contract popup

    frmLoanWorkInfo.lblAddress.text = kony.i18n.getLocalizedString("Contract_Employed_title");
    frmLoanWorkInfo.btnEmploy.text = kony.i18n.getLocalizedString("Employ_label");

    frmLoanWorkInfo.btnContracted.text = kony.i18n.getLocalizedString("Contract_Employed_label");
    frmLoanWorkInfo.lblDOB.text = kony.i18n.getLocalizedString("Contract_Start_Date");

    frmLoanWorkInfo.lblContractEndDate.text = kony.i18n.getLocalizedString("Contract_End_Date");
    kony.print("workingInformationPreShow @@ 4");
    frmLoanWorkInfo.lblContractPeriod.text = kony.i18n.getLocalizedString("Contract_Period");
    frmLoanWorkInfo.btnMonthlystmt.text = kony.i18n.getLocalizedString("Monthly_tab");

    frmLoanWorkInfo.btnYearlystmt.text = kony.i18n.getLocalizedString("Yearly_tab");
    frmLoanWorkInfo.btnSaveContractEmployed.text = kony.i18n.getLocalizedString("MF_RDM_30");
    frmLoanWorkInfo.lblContractEmp.text = kony.i18n.getLocalizedString("Contract_Employed_title");
    frmLoanWorkInfo.lblContractStartDate.text = kony.i18n.getLocalizedString("Contract_Start_Date");
    frmLoanWorkInfo.lblContractEndDt.text = kony.i18n.getLocalizedString("Contract_End_Date");
    frmLoanWorkInfo.lblContrtPeriod.text = kony.i18n.getLocalizedString("Contract_Period");

    kony.print("workingInformationPreShow @@ 5");

    frmLoanWorkInfo.lblMainAddressNo.text = kony.i18n.getLocalizedString("Address_label");
    //frmLoanWorkInfo.lblAddressTitle.text = kony.i18n.getLocalizedString("Address_label");
    frmLoanWorkInfo.lblAddressNo.text = kony.i18n.getLocalizedString("Address_label");
    frmLoanWorkInfo.lblBuildingNo.text = kony.i18n.getLocalizedString("Building_label");
    frmLoanWorkInfo.lblMainBuildingNo.text = kony.i18n.getLocalizedString("Building_label");
    frmLoanWorkInfo.lblFloorNo.text = kony.i18n.getLocalizedString("Floor_label");  
    frmLoanWorkInfo.lblMainFloorNo.text = kony.i18n.getLocalizedString("Floor_label");
    frmLoanWorkInfo.lblSoi.text = kony.i18n.getLocalizedString("Soi_label");
    frmLoanWorkInfo.lblMainSoi.text = kony.i18n.getLocalizedString("Soi_label");
    frmLoanWorkInfo.lblRoad.text = kony.i18n.getLocalizedString("Road_label");
    frmLoanWorkInfo.LblMainRoadNo.text = kony.i18n.getLocalizedString("Road_label");
    frmLoanWorkInfo.lblMoo.text = kony.i18n.getLocalizedString("Moo_label");  
    frmLoanWorkInfo.lblMainMoo.text = kony.i18n.getLocalizedString("Moo_label");
    frmLoanWorkInfo.lblSubDistrict.text = kony.i18n.getLocalizedString("Sub_District_label");
    frmLoanWorkInfo.lblMainSubDistrict.text = kony.i18n.getLocalizedString("Sub_District_label");
    frmLoanWorkInfo.lblDistrict.text = kony.i18n.getLocalizedString("District_label");
    frmLoanWorkInfo.lblMainDistrict.text = kony.i18n.getLocalizedString("District_label");
    frmLoanWorkInfo.lblProvince.text = kony.i18n.getLocalizedString("Province_label");
    frmLoanWorkInfo.lblMainProvince.text = kony.i18n.getLocalizedString("Province_label");
    frmLoanWorkInfo.lblZipCode.text = kony.i18n.getLocalizedString("Zipcode_label");
    frmLoanWorkInfo.lblMainZipCode.text = kony.i18n.getLocalizedString("Zipcode_label");
    frmLoanWorkInfo.btnSaveAddr.text = kony.i18n.getLocalizedString("MF_RDM_30");
    frmLoanWorkInfo.lblYears.text = kony.i18n.getLocalizedString("Years_textbox");
    frmLoanWorkInfo.lblMonth.text = kony.i18n.getLocalizedString("Months_textbox");
    frmLoanWorkInfo.btnSaveCurrentEmploye.text = kony.i18n.getLocalizedString("keysave");
    frmLoanWorkInfo.lblInformationMsg.text = kony.i18n.getLocalizedString("keybillernotfound");
    frmLoanWorkInfo.tbxSearch.placeholder = kony.i18n.getLocalizedString("KeySearch");
    frmLoanWorkInfo.lblMainOfficeNo.text = kony.i18n.getLocalizedString("OfficeNo_textbox");
     frmLoanWorkInfo.lblMainExtNo.text = kony.i18n.getLocalizedString("Ext_textbox");
    frmLoanWorkInfo.lblOfficeNo.text = kony.i18n.getLocalizedString("OfficeNo_textbox");
    frmLoanWorkInfo.lblExtNo.text = kony.i18n.getLocalizedString("Ext_textbox");
    frmLoanWorkInfo.lblWorkingAddressFilled.text = kony.i18n.getLocalizedString("Working_Address_textbox");
  }catch(e){
    alert("Exception in workingInfoPhrases : "+e);
  }  
}


function workingInformationPreShow(){     

  period = "Monthly";

  try{
    kony.print("workingInformationPreShow ----");
    frmLoanWorkInfo.lblcircle.skin = "skinpiorangeLineLone";
    frmLoanWorkInfo.flxcircle.skin="slFbox";
    frmLoanWorkInfo.btnNext.skin = "btnGreyBGNoRound";
    frmLoanWorkInfo.btnNext.focusSkin = "btnGreyBGNoRound";
    frmLoanWorkInfo.flxContractEmployed.setVisibility(true);
    preLoadWInfo();
    linkWorkInfoEvents();
    onOpenWInfo();
    kony.print("workingInformationPreShow @@ 1");

    kony.print("workingInformationPreShow @@ 6");
    if(isNotBlank(frmLoanWorkInfo.lblWorkingAddressInfor.text)){
      frmLoanWorkInfo.flxWorkingAddress.setVisibility(false);
      frmLoanWorkInfo.flxWorkingAddressFilled.setVisibility(true);
    }else{
    frmLoanWorkInfo.flxWorkingAddress.setVisibility(true);
    frmLoanWorkInfo.flxWorkingAddressFilled.setVisibility(false);
    }
    frmLoanWorkInfo.lblDateOfCitID.skin = "lblGrey20px333333";
   
    //frmLoanWorkInfo.flxExtNo.setEnabled(false);
    //frmLoanWorkInfo.tbxExtNo.text = "";
    frmLoanWorkInfo.flxLengthCurrentEmp.setVisibility(false);
    frmLoanWorkInfo.flxContractPopUp.setVisibility(false);
    frmLoanWorkInfo.flxAddressPopUp.setVisibility(false);
    frmLoanWorkInfo.flxWorkingInfo.setVisibility(true);
    frmLoanWorkInfo.lbltempOfficeAddr.setVisibility(false);
    frmLoanWorkInfo.tbxFloorNo.maxTextLength = 3;
    frmLoanWorkInfo.tbxAddrVal.maxTextLength = 20;
    workingInfoPhrases();
    kony.print("workingInformationPreShow 8");
  }catch(e){
    kony.print("Exception at workingInformationPreShow "+e);
  }
}


function formatContactNo() {
    kony.print("formatContactNo");
    var contactNo = "";
    var landline = false;
    contactNo = frmLoanWorkInfo.tbxOfficeNo.text;
    kony.print("formatContactNo number: " + contactNo);
    var firstDigs = contactNo.substring(0, 2);
    kony.print("formatContactNo firstdigs: " + firstDigs);
    frmLoanWorkInfo.tbxOfficeNo.maxTextLength = 12;
    if (contactNo.length == 11) {
        kony.print("formatContactNo landline ");
        frmLoanWorkInfo.flxExtNo.setEnabled(true);
        frmLoanWorkInfo.lblMainExtNo.skin = "lblblue24px007abc";
        frmLoanWorkInfo.tbxExtNo.skin = "txtBlue24px007abc";
        formatLoanLandlineNumber(contactNo);
    } else {
        kony.print("formatContactNo mobile ");
        frmLoanWorkInfo.flxExtNo.setEnabled(false);
        frmLoanWorkInfo.tbxExtNo.text = "";
        frmLoanWorkInfo.tbxExtNo.setVisibility(false);
        frmLoanWorkInfo.lblExtNo.setVisibility(false);
        frmLoanWorkInfo.lblMainExtNo.setVisibility(true);
        frmLoanWorkInfo.lblMainExtNo.skin = "lblGrey171";
        //frmLoanWorkInfo.tbxExtNo.skin = "txtGrey24px333333";
        formatLoanMobileNumber(contactNo);
    }
    handleSaveBtnWorkInfo();
}

function onClickContactNo(){
  frmLoanWorkInfo.lblMainOfficeNo.setVisibility(false);
  frmLoanWorkInfo.lblOfficeNo.setVisibility(true);
  frmLoanWorkInfo.tbxOfficeNo.setVisibility(true);
  frmLoanWorkInfo.tbxOfficeNo.text = isNotBlank(frmLoanWorkInfo.tbxOfficeNo.text) ? "" : frmLoanWorkInfo.tbxOfficeNo.text;
  frmLoanWorkInfo.tbxOfficeNo.setFocus(true);
  frmLoanWorkInfo.tbxOfficeNo.onTextChange = formatContactNo;
}

function onClickExtNo(){
  frmLoanWorkInfo.lblMainExtNo.setVisibility(false);
  frmLoanWorkInfo.lblExtNo.setVisibility(true);
  frmLoanWorkInfo.tbxExtNo.setVisibility(true);
  frmLoanWorkInfo.tbxExtNo.text = isNotBlank(frmLoanWorkInfo.tbxExtNo.text) ? "" : frmLoanWorkInfo.tbxExtNo.text;
  frmLoanWorkInfo.tbxExtNo.setFocus(true);
}

function endEditOfcNo(){
  if(isNullorEmpty(frmLoanWorkInfo.tbxOfficeNo.text)){
    frmLoanWorkInfo.lblMainOfficeNo.setVisibility(true);
    frmLoanWorkInfo.tbxOfficeNo.setVisibility(false);
    frmLoanWorkInfo.lblOfficeNo.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblMainOfficeNo.setVisibility(false);
    frmLoanWorkInfo.tbxOfficeNo.setVisibility(true);
    frmLoanWorkInfo.lblOfficeNo.setVisibility(true);
    handleSaveBtnWorkInfo();
  }
}

function endEditExtNo(){
  if(isNullorEmpty(frmLoanWorkInfo.tbxExtNo.text)){
    frmLoanWorkInfo.lblMainExtNo.setVisibility(true);
    frmLoanWorkInfo.tbxExtNo.setVisibility(false);
    frmLoanWorkInfo.lblExtNo.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblMainExtNo.setVisibility(false);
    frmLoanWorkInfo.tbxExtNo.setVisibility(true);
    frmLoanWorkInfo.lblExtNo.setVisibility(true);
    handleSaveBtnWorkInfo();
  }
}

function touchEndOfcNo(){
  if(isNullorEmpty(frmLoanWorkInfo.tbxOfficeNo.text)){
    frmLoanWorkInfo.lblMainOfficeNo.setVisibility(true);
    frmLoanWorkInfo.tbxOfficeNo.setVisibility(false);
    frmLoanWorkInfo.lblOfficeNo.setVisibility(false);
  }
}

function touchEndExtNo(){
  if(isNullorEmpty(frmLoanWorkInfo.tbxExtNo.text)){
    frmLoanWorkInfo.lblMainExtNo.setVisibility(true);
    frmLoanWorkInfo.tbxExtNo.setVisibility(false);
    frmLoanWorkInfo.lblExtNo.setVisibility(false);
  }
}

function initLoanAddress(){
  kony.print("in initLoanAddress");
  try{
   frmLoanWorkInfo.preShow = loanaddresspreshow;
  }catch(e){
    kony.print("exception in @@ initLoanAddress: "+e);
    kony.print("exception in @@ initLoanAddress message: "+e.message);
  }
}

function loanaddresspreshow(){
  
  kony.print("in initLoanAddress");
  try{
  
  } catch(e){
    kony.print("exception in @@ initLoanAddress: "+e);
    kony.print("exception in @@ initLoanAddress message: "+e.message);
  }
}

function onTextChangeBuildNo(obj){
  handleAddrSaveBtn();
  addressEmptyCheck(obj);
  frmLoanWorkInfo.flxSearchAddress.isVisible = false;
}

function searchsubdistrictloan(){
  var searchSubDistrictList = [];
  var searchSuggestedSubDistrictList = [];
  var searchText = frmLoanWorkInfo.tbxSubDistrict.text;
  var searchValue = "";
  
  if (isNotBlank(searchText) && searchText.length >= 1) {
         searchText = searchText.toLowerCase();
         var countMatchedValue = 0;
         frmLoanWorkInfo.flxSearchAddress.isVisible = true;
         frmLoanWorkInfo.flxScrollAddress.scrollToWidget(frmLoanWorkInfo.flxSearchAddress);
         frmLoanWorkInfo.flxSearchAddress.setFocus = true;
    
         for(var countVal=0; countVal<gblLoanFullAddressSearch.length; countVal++){
             keyToSearch = gblLoanFullAddressSearch[countVal].subDistProvince;
             keyToSearch = keyToSearch.toLowerCase();
             if(keyToSearch.indexOf(searchText) >=0 ){
               searchSuggestedSubDistrictList[countMatchedValue] = gblLoanFullAddressSearch[countVal];
			   countMatchedValue = countMatchedValue+1;
             }                
      }

        
    }else{
    frmLoanWorkInfo.flxSearchAddress.isVisible = false;
  }
  var dataset={};
  for(var count = 0; count<searchSuggestedSubDistrictList.length; count++){
    var filtervalue = searchSuggestedSubDistrictList[count].subDistProvince;
             dataset={
               "lblTotalAddress": filtervalue,
             };
             searchSubDistrictList.push(dataset);
        }
       
        frmLoanWorkInfo.segSubDistrictSearch.setData(searchSubDistrictList);
  if(searchSuggestedSubDistrictList.length == 0){
    frmLoanWorkInfo.segSubDistrictSearch.setVisibility(false);
    frmLoanWorkInfo.lblAddressNotFound.setVisibility(true);
  }else{
    frmLoanWorkInfo.segSubDistrictSearch.setVisibility(true);
    frmLoanWorkInfo.lblAddressNotFound.setVisibility(false);
  }
  handleAddrSaveBtn();
  addressEmptyCheck(obj);
}

// function openAddressPopUpWithEnteredData() {
//   //Added for Pop Clicks
//   frmLoanWorkInfo.flxMain.isVisible = false;
//   if(gblPersonalInfo.ContactAdress !== undefined && gblPersonalInfo.ContactAdress !== null) {
//     frmLoanWorkInfo.flxContractPopUp.isVisible=true;
//     if(Object.keys(gblPersonalInfo.ContactAdress).length > 0) {
//       frmLoanWorkInfo.tbxAddrVal.text = frmLoanWorkInfo.lblAddWithData.text;
//       frmLoanWorkInfo.tbxBuildingNo.text = frmLoanWorkInfo.lblBuildingWithData.text;
//       frmLoanWorkInfo.tbxFloorNo.text = gblPersonalInfo.ContactAdress.tbxFloorNo;
//       frmLoanWorkInfo.tbxMoo.text = gblPersonalInfo.ContactAdress.tbxMoo;
//       frmLoanWorkInfo.tbxSoi.text = gblPersonalInfo.ContactAdress.tbxSoi;
//       frmLoanWorkInfo.tbxRoad.text = frmLoanWorkInfo.lblRoadNoWithdata.text;
//     }
//   }
// }

function addressDetailsOnClick(dataEntered){
  if(undefined !== dataEntered && "N" === dataEntered){
    frmLoanWorkInfo.lblMainAddressNo.isVisible=true;
    frmLoanWorkInfo.lblMainBuildingNo.isVisible=true;
    frmLoanWorkInfo.lblMainFloorNo.isVisible=true;
    frmLoanWorkInfo.lblMainMoo.isVisible=true;
    frmLoanWorkInfo.lblMainSoi.isVisible=true;
    frmLoanWorkInfo.LblMainRoadNo.isVisible=true;
    frmLoanWorkInfo.lblMainSubDistrict.isVisible=true;
    frmLoanWorkInfo.lblMainDistrict.isVisible=true;
    frmLoanWorkInfo.lblMainProvince.isVisible=true;
    frmLoanWorkInfo.lblMainZipCode.isVisible=true;
    frmLoanWorkInfo.btnSaveAddr.onClick  = null;
    frmLoanWorkInfo.btnSaveAddr.skin ="btnGreyBGNoRound";
    frmLoanWorkInfo.btnSaveAddr.focusSkin ="btnGreyBGNoRound";
    frmLoanWorkInfo.lblAddressNo.isVisible=false;
    frmLoanWorkInfo.tbxAddrVal.isVisible=false;
    frmLoanWorkInfo.lblBuildingNo.isVisible=false;
    frmLoanWorkInfo.tbxBuildingNo.isVisible=false;
    frmLoanWorkInfo.lblFloorNo.isVisible=false;
    frmLoanWorkInfo.tbxFloorNo.isVisible=false;
    frmLoanWorkInfo.lblMoo.isVisible=false;
    frmLoanWorkInfo.tbxMoo.isVisible=false;
    frmLoanWorkInfo.lblSoi.isVisible=false;
    frmLoanWorkInfo.tbxSoi.isVisible=false;
    frmLoanWorkInfo.lblRoad.isVisible=false;
    frmLoanWorkInfo.tbxRoad.isVisible=false;
    frmLoanWorkInfo.lblSubDistrict.isVisible=false;
    frmLoanWorkInfo.tbxSubDistrict.isVisible=false;
    frmLoanWorkInfo.lblDistrict.isVisible=false;
    frmLoanWorkInfo.tbxDistrict.isVisible=false;
    frmLoanWorkInfo.lblProvince.isVisible=false;
    frmLoanWorkInfo.tbxProvince.isVisible=false;
    frmLoanWorkInfo.lblZipCode.isVisible=false;
    frmLoanWorkInfo.tbxZipCode.isVisible=false;
    frmLoanWorkInfo.tbxBuildingNo.text= "";
    frmLoanWorkInfo.tbxAddrVal.text="";
    frmLoanWorkInfo.tbxFloorNo.text="";
    frmLoanWorkInfo.tbxMoo.text="";
    frmLoanWorkInfo.tbxRoad.text="";
    frmLoanWorkInfo.tbxSoi.text="";
    frmLoanWorkInfo.tbxSubDistrict.text="";
    frmLoanWorkInfo.tbxDistrict.text="";
    frmLoanWorkInfo.tbxProvince.text="";
    frmLoanWorkInfo.tbxZipCode.text="";
  }else{
    if(isNotBlank(frmLoanWorkInfo.tbxAddrVal.text)){
      frmLoanWorkInfo.lblMainAddressNo.setVisibility(false);
      frmLoanWorkInfo.lblAddressNo.setVisibility(true);
      frmLoanWorkInfo.tbxAddrVal.setVisibility(true);
    }else{
      frmLoanWorkInfo.lblMainAddressNo.setVisibility(true);
      frmLoanWorkInfo.lblAddressNo.setVisibility(false);
      frmLoanWorkInfo.tbxAddrVal.setVisibility(false);      
    }
   if(isNotBlank(frmLoanWorkInfo.tbxBuildingNo.text)){
      frmLoanWorkInfo.lblMainBuildingNo.setVisibility(false);
      frmLoanWorkInfo.lblBuildingNo.setVisibility(true);
      frmLoanWorkInfo.tbxBuildingNo.setVisibility(true);
    }else{
      frmLoanWorkInfo.lblMainBuildingNo.setVisibility(true);
      frmLoanWorkInfo.lblBuildingNo.setVisibility(false);
      frmLoanWorkInfo.tbxBuildingNo.setVisibility(false);     
    }
   if(isNotBlank(frmLoanWorkInfo.tbxFloorNo.text)){
      frmLoanWorkInfo.lblMainFloorNo.setVisibility(false);
      frmLoanWorkInfo.lblFloorNo.setVisibility(true);
      frmLoanWorkInfo.tbxFloorNo.setVisibility(true);
    }else{
      frmLoanWorkInfo.lblMainFloorNo.setVisibility(true);
      frmLoanWorkInfo.lblFloorNo.setVisibility(false);
      frmLoanWorkInfo.tbxFloorNo.setVisibility(false);
    }
   if(isNotBlank(frmLoanWorkInfo.tbxMoo.text)){
      frmLoanWorkInfo.lblMainMoo.setVisibility(false);
      frmLoanWorkInfo.lblMoo.setVisibility(true);
      frmLoanWorkInfo.tbxMoo.setVisibility(true);
    }else{
      frmLoanWorkInfo.lblMainMoo.setVisibility(true);
      frmLoanWorkInfo.lblMoo.setVisibility(false);
      frmLoanWorkInfo.tbxMoo.setVisibility(false);
    }
   if(isNotBlank(frmLoanWorkInfo.tbxSoi.text)){
      frmLoanWorkInfo.lblMainSoi.setVisibility(false);
      frmLoanWorkInfo.lblSoi.setVisibility(true);
      frmLoanWorkInfo.tbxSoi.setVisibility(true);
    }else{
      frmLoanWorkInfo.lblMainSoi.setVisibility(true);
      frmLoanWorkInfo.lblSoi.setVisibility(false);
      frmLoanWorkInfo.tbxSoi.setVisibility(false);
    }
   if(isNotBlank(frmLoanWorkInfo.tbxRoad.text)){
     frmLoanWorkInfo.LblMainRoadNo.setVisibility(false);
     frmLoanWorkInfo.lblRoad.setVisibility(true);
     frmLoanWorkInfo.tbxRoad.setVisibility(true);
    }else{
     frmLoanWorkInfo.LblMainRoadNo.setVisibility(true);
     frmLoanWorkInfo.lblRoad.setVisibility(false);
     frmLoanWorkInfo.tbxRoad.setVisibility(false);
    }
    frmLoanWorkInfo.lblMainSubDistrict.isVisible=false;
    frmLoanWorkInfo.lblSubDistrict.isVisible=true;
    frmLoanWorkInfo.tbxSubDistrict.isVisible=true;
    frmLoanWorkInfo.lblMainDistrict.isVisible=false;
    frmLoanWorkInfo.lblDistrict.isVisible=true;
    frmLoanWorkInfo.tbxDistrict.isVisible=true;
    frmLoanWorkInfo.lblMainProvince.isVisible=false;
    frmLoanWorkInfo.lblProvince.isVisible=true;
    frmLoanWorkInfo.tbxProvince.isVisible=true;
    frmLoanWorkInfo.lblMainZipCode.isVisible=false;
    frmLoanWorkInfo.lblZipCode.isVisible=true;
    frmLoanWorkInfo.tbxZipCode.isVisible=true;    
  }

}

function flexAddNoOnClick(){
  var arrayLabel = ["tbxAddrVal","tbxBuildingNo","tbxFloorNo","tbxSoi","tbxMoo","tbxRoad","tbxSubDistrict"];
  addressEmptyCheck(arrayLabel);
  frmLoanWorkInfo.lblMainAddressNo.isVisible=false;
  frmLoanWorkInfo.lblAddressNo.isVisible=true;
  frmLoanWorkInfo.tbxAddrVal.isVisible=true;
  frmLoanWorkInfo.tbxAddrVal.text = "";
  frmLoanWorkInfo.tbxAddrVal.setFocus(true);

}

function addressEmptyCheck(arry) {
  kony.print("addressEmptyCheck normal"+arry["id"]);
  kony.print("addressEmptyCheck json"+arry["text"]);
  try{

    var objId = arry["id"];
    var objText  = arry["text"];
    
      if(objId === "tbxAddrVal") {
        kony.print("addressEmptyCheck inside tbxAddrVal");
        if(!isNotBlank(objText)) {
          kony.print("addressEmptyCheck inside tbxAddrVal null");
          frmLoanWorkInfo.lblMainAddressNo.isVisible=true;
          frmLoanWorkInfo.lblAddressNo.isVisible=false;
          frmLoanWorkInfo.tbxAddrVal.isVisible=false;
          //frmLoanWorkInfo.tbxAddrVal.text = "";
//           frmLoanWorkInfo.tbxAddrVal.setFocus(false);
        }

      } else if (objId === "tbxBuildingNo") {
        kony.print("addressEmptyCheck inside tbxBuildingNo ");
        if(!isNotBlank(objText)) {
          kony.print("addressEmptyCheck inside tbxBuildingNo null");
          frmLoanWorkInfo.lblMainBuildingNo.isVisible=true;
          frmLoanWorkInfo.lblBuildingNo.isVisible=false;
          frmLoanWorkInfo.tbxBuildingNo.isVisible=false;
          //frmLoanWorkInfo.tbxBuildingNo.text = "";
//           frmLoanWorkInfo.tbxBuildingNo.setFocus(false);
        }

      } else if (objId === "tbxFloorNo") {
        kony.print("addressEmptyCheck inside tbxFloorNo ");
        if(!isNotBlank(objText)) {
          kony.print("addressEmptyCheck inside tbxFloorNo null");
          frmLoanWorkInfo.lblMainFloorNo.isVisible=true;
          frmLoanWorkInfo.lblFloorNo.isVisible=false;
          frmLoanWorkInfo.tbxFloorNo.isVisible=false;
          //frmLoanWorkInfo.tbxFloorNo.text = "";
//           frmLoanWorkInfo.tbxFloorNo.setFocus(false);
        }

      } else if (objId === "tbxSoi") {
        if(!isNotBlank(objText)) {
          frmLoanWorkInfo.lblMainSoi.isVisible=true;
          frmLoanWorkInfo.lblSoi.isVisible=false;
          frmLoanWorkInfo.tbxSoi.isVisible=false;
          //frmLoanWorkInfo.tbxSoi.text = "";
//           frmLoanWorkInfo.tbxSoi.setFocus(false);
        }

      } else if (objId === "tbxRoad") {
        if(!isNotBlank(objText)) {
          frmLoanWorkInfo.LblMainRoadNo.isVisible=true;
          frmLoanWorkInfo.lblRoad.isVisible=false;
          frmLoanWorkInfo.tbxRoad.isVisible=false;
          //frmLoanWorkInfo.tbxRoad.text = "";
//           frmLoanWorkInfo.tbxRoad.setFocus(false);
        }

      } else if (objId === "tbxMoo") {
        if(!isNotBlank(objText)) {
          frmLoanWorkInfo.lblMainMoo.isVisible=true;
          frmLoanWorkInfo.lblMoo.isVisible=false;
          frmLoanWorkInfo.tbxMoo.isVisible=false;
          //frmLoanWorkInfo.tbxMoo.text = "";
//           frmLoanWorkInfo.tbxMoo.setFocus(false);
        }

      } else if (objId === "tbxSubDistrict") {
        if(!isNotBlank(objText)) {
          frmLoanWorkInfo.lblMainSubDistrict.isVisible=true;
          frmLoanWorkInfo.lblSubDistrict.isVisible=false;
          frmLoanWorkInfo.tbxSubDistrict.isVisible=false;
//           frmLoanWorkInfo.tbxSubDistrict.setFocus(false);
        }

      } else if (objId === "tbxDistrict") {
        if(!isNotBlank(objText)) {
         frmLoanWorkInfo.lblMainDistrict.isVisible=true;
          frmLoanWorkInfo.lblDistrict.isVisible=false;
          frmLoanWorkInfo.tbxDistrict.isVisible=false;
          //frmLoanWorkInfo.tbxMoo.text = "";
//           frmLoanWorkInfo.tbxDistrict.setFocus(false);

        }
      } else if (objId === "tbxProvince") {
        if(!isNotBlank(objText)) {
          frmLoanWorkInfo.lblMainProvince.isVisible=true;
          frmLoanWorkInfo.lblProvince.isVisible=false;
          frmLoanWorkInfo.tbxProvince.isVisible=false;
          //frmLoanWorkInfo.tbxMoo.text = "";
//           frmLoanWorkInfo.tbxProvince.setFocus(false);
        }

      } else if (objId === "tbxZipCode") {
        if(!isNotBlank(objText)) {
          frmLoanWorkInfo.lblMainZipCode.isVisible=true;
          frmLoanWorkInfo.lblZipCode.isVisible=false;
          frmLoanWorkInfo.tbxZipCode.isVisible=false;
          //frmLoanWorkInfo.tbxMoo.text = "";
          frmLoanWorkInfo.tbxZipCode.setFocus(false);
        }
      }
  }catch(e){
        kony.print("exception in @@ addressEmptyCheck events: "+e);
    kony.print("exception in @@ addressEmptyCheck events message: "+e.message);
  }
}

function handleAddrSaveBtn(){
  var buildingNo = frmLoanWorkInfo.tbxBuildingNo.text;
  var tbxAddrVal = frmLoanWorkInfo.tbxAddrVal.text;
  var tbxFloorNo = frmLoanWorkInfo.tbxFloorNo.text;
  var tbxMoo = frmLoanWorkInfo.tbxMoo.text;
  var tbxRoad = frmLoanWorkInfo.tbxRoad.text;
  var tbxSoi = frmLoanWorkInfo.tbxSoi.text;
  var tbxSubDistrict = frmLoanWorkInfo.tbxSubDistrict.text;
  var tbxDistrict = frmLoanWorkInfo.tbxDistrict.text;
  var tbxProvince = frmLoanWorkInfo.tbxProvince.text;
  var tbxZipCode = frmLoanWorkInfo.tbxZipCode.text;
  kony.print("after save buildingNo "+buildingNo);
  kony.print("after save tbxAddrVal "+tbxAddrVal);
  kony.print("after save tbxFloorNo "+tbxFloorNo);
  kony.print("after save tbxMoo "+tbxMoo);
  kony.print("after save tbxRoad "+tbxRoad);
  kony.print("after save tbxSoi "+tbxSoi);
  kony.print("after save tbxSubDistrict "+tbxSubDistrict);
  kony.print("after save tbxDistrict "+tbxDistrict);
  kony.print("after save tbxProvince "+tbxProvince);
  kony.print("after save tbxZipCode "+tbxZipCode);
  
  if(isNotBlank(tbxAddrVal) && isNotBlank(tbxSubDistrict) && isNotBlank(tbxDistrict) && isNotBlank(tbxProvince) &&
     isNotBlank(tbxZipCode) && tbxZipCode.length == 5)
  {
    frmLoanWorkInfo.btnSaveAddr.setEnabled(true);
    frmLoanWorkInfo.btnSaveAddr.skin ="btnBlue28pxLoan2";
    frmLoanWorkInfo.btnSaveAddr.focusSkin ="btnBlueBGNoRound150pxLoan";
    frmLoanWorkInfo.btnSaveAddr.onClick  = popUpSaveAddress;
//     frmLoanWorkInfo.flxWorkingAddressFilled.setVisibility(true);
//     frmLoanWorkInfo.flxWorkingAddress.setVisibility(false);
  } else{
    frmLoanWorkInfo.btnSaveAddr.setEnabled(false);
    frmLoanWorkInfo.btnSaveAddr.skin ="btnGreyBGNoRound";
    frmLoanWorkInfo.btnSaveAddr.focusSkin ="btnGreyBGNoRound";
//     frmLoanWorkInfo.flxWorkingAddressFilled.setVisibility(false);
//     frmLoanWorkInfo.flxWorkingAddress.setVisibility(true);
  }
}

function enableAddressAutoFocus () {
  var arrayLabel = ["tbxAddrVal","tbxBuildingNo","tbxFloorNo","tbxSoi","tbxMoo","tbxRoad"];
  addressEmptyCheck(arrayLabel);
}


function loanAddNoOnClick(){
  frmLoanWorkInfo.lblMainAddressNo.setVisibility(false);
  frmLoanWorkInfo.lblAddressNo.setVisibility(true);
  frmLoanWorkInfo.tbxAddrVal.setVisibility(true);
  frmLoanWorkInfo.tbxAddrVal.text = isNotBlank(frmLoanWorkInfo.tbxAddrVal.text) ? "" : frmLoanWorkInfo.tbxAddrVal.text;
  frmLoanWorkInfo.tbxAddrVal.setFocus(true);

}

function loanBuildingNoOnClick(){
  frmLoanWorkInfo.lblMainBuildingNo.setVisibility(false);
  frmLoanWorkInfo.lblBuildingNo.setVisibility(true);
  frmLoanWorkInfo.tbxBuildingNo.setVisibility(true);
  frmLoanWorkInfo.tbxBuildingNo.text = isNotBlank(frmLoanWorkInfo.tbxBuildingNo.text) ? "" : frmLoanWorkInfo.tbxBuildingNo.text;
  frmLoanWorkInfo.tbxBuildingNo.setFocus(true);
}

function loanFloorNoOnClick(){
  frmLoanWorkInfo.lblMainFloorNo.setVisibility(false);
  frmLoanWorkInfo.lblFloorNo.setVisibility(true);
  frmLoanWorkInfo.tbxFloorNo.setVisibility(true);
  frmLoanWorkInfo.tbxFloorNo.text = isNotBlank(frmLoanWorkInfo.tbxFloorNo.text) ? "" : frmLoanWorkInfo.tbxFloorNo.text;
  frmLoanWorkInfo.tbxFloorNo.setFocus(true);
}

function loanSoiOnClick(){
  frmLoanWorkInfo.lblMainSoi.setVisibility(false);
  frmLoanWorkInfo.lblSoi.setVisibility(true);
  frmLoanWorkInfo.tbxSoi.setVisibility(true);
  frmLoanWorkInfo.tbxSoi.text = isNotBlank(frmLoanWorkInfo.tbxSoi.text) ? "" : frmLoanWorkInfo.tbxSoi.text;
  frmLoanWorkInfo.tbxSoi.setFocus(true);
}

function loanRoadOnClick(){
  frmLoanWorkInfo.LblMainRoadNo.setVisibility(false);
  frmLoanWorkInfo.lblRoad.setVisibility(true);
  frmLoanWorkInfo.tbxRoad.setVisibility(true);
  frmLoanWorkInfo.tbxRoad.text = isNotBlank(frmLoanWorkInfo.tbxRoad.text) ? "" : frmLoanWorkInfo.tbxRoad.text;
  frmLoanWorkInfo.tbxRoad.setFocus(true);
}

function loanMooOnClick(){
  frmLoanWorkInfo.lblMainMoo.setVisibility(false);
  frmLoanWorkInfo.lblMoo.setVisibility(true);
  frmLoanWorkInfo.tbxMoo.setVisibility(true);
  frmLoanWorkInfo.tbxMoo.text = isNotBlank(frmLoanWorkInfo.tbxMoo.text) ? "" : frmLoanWorkInfo.tbxMoo.text;
  frmLoanWorkInfo.tbxMoo.setFocus(true);
}



function loanSubDistrictOnClick(){
  frmLoanWorkInfo.lblMainSubDistrict.setVisibility(false);
  frmLoanWorkInfo.lblSubDistrict.setVisibility(true);
  frmLoanWorkInfo.tbxSubDistrict.setVisibility(true);
  frmLoanWorkInfo.tbxSubDistrict.text = isNotBlank(frmLoanWorkInfo.tbxSubDistrict.text) ? "" : frmLoanWorkInfo.tbxSubDistrict.text;
  frmLoanWorkInfo.tbxSubDistrict.setFocus(true);
}
function loanDistrictOnClick(){
  frmLoanWorkInfo.lblMainDistrict.setVisibility(false);
  frmLoanWorkInfo.lblDistrict.setVisibility(true);
  frmLoanWorkInfo.tbxDistrict.setVisibility(true);
  frmLoanWorkInfo.tbxDistrict.text = isNotBlank(frmLoanWorkInfo.tbxDistrict.text) ? "" : frmLoanWorkInfo.tbxDistrict.text;
  frmLoanWorkInfo.tbxDistrict.setFocus(true);
}

function loanProvinceOnClick(){
  frmLoanWorkInfo.lblMainProvince.setVisibility(false);
  frmLoanWorkInfo.lblProvince.setVisibility(true);
  frmLoanWorkInfo.tbxProvince.setVisibility(true);
  frmLoanWorkInfo.tbxProvince.text = isNotBlank(frmLoanWorkInfo.tbxProvince.text) ? "" : frmLoanWorkInfo.tbxProvince.text;
  frmLoanWorkInfo.tbxProvince.setFocus(true);
}
function loanZipCodeOnClick(){
  frmLoanWorkInfo.lblMainZipCode.setVisibility(false);
  frmLoanWorkInfo.lblZipCode.setVisibility(true);
  frmLoanWorkInfo.tbxZipCode.setVisibility(true);
  frmLoanWorkInfo.tbxZipCode.text = isNotBlank(frmLoanWorkInfo.tbxZipCode.text) ? "" : frmLoanWorkInfo.tbxZipCode.text;
  frmLoanWorkInfo.tbxZipCode.setFocus(true);
}

function popUpSaveAddress(){
 try{
  var locale = kony.i18n.getCurrentLocale();
  var buildingNo = frmLoanWorkInfo.tbxBuildingNo.text;
  var tbxAddrVal = frmLoanWorkInfo.tbxAddrVal.text;
  var tbxFloorNo = frmLoanWorkInfo.tbxFloorNo.text;
  var tbxMoo = frmLoanWorkInfo.tbxMoo.text;
  var tbxRoad = frmLoanWorkInfo.tbxRoad.text;
  var tbxSoi = frmLoanWorkInfo.tbxSoi.text;

  var tbxSubDistrict = frmLoanWorkInfo.tbxSubDistrict.text;
  var tbxDistrict = frmLoanWorkInfo.tbxDistrict.text;
  var tbxProvince = frmLoanWorkInfo.tbxProvince.text;
  var tbxZipCode = frmLoanWorkInfo.tbxZipCode.text;
  if(isNotBlank(tbxFloorNo)){
    if (locale == "th_TH") {
    var tbxFloorNoDesc = getLocalizedString("Floor_label")+" "+tbxFloorNo;
  } else {
    var tbxFloorNoDesc = tbxFloorNo+" "+getLocalizedString("Floor_label");
  }
 }else{
    var tbxFloorNoDesc = "";
 }
   if(isNotBlank(tbxMoo)){
    if (locale == "th_TH") {
    var tbxMooDesc = getLocalizedString("Moo_label")+" "+tbxMoo;
  } else {
    var tbxMooDesc = getLocalizedString("Moo_label")+" "+tbxMoo;
  }
 }else{
    var tbxMooDesc = "";
 }
   if(isNotBlank(tbxSoi)){
    if (locale == "th_TH") {
    var tbxSoiDesc = getLocalizedString("Soi_label")+" "+tbxSoi;
  } else {
    var tbxSoiDesc = getLocalizedString("Soi_label")+" "+tbxSoi;
  }
 }else{
    var tbxSoiDesc = "";
 }
   if(isNotBlank(tbxRoad)){
    if (locale == "th_TH") {
    var tbxRoadDesc = getLocalizedString("Road_label")+" "+tbxRoad;
  } else {
    var tbxRoadDesc = tbxRoad+" "+getLocalizedString("Road_label");
  }
 }else{
    var tbxRoadDesc = "";
 }
   
  var workingAddressInfo =  tbxAddrVal+" "+buildingNo+" "+tbxFloorNoDesc+" "+tbxMooDesc+" "+tbxSoiDesc+" "+tbxRoadDesc+" "+tbxSubDistrict+" "+tbxDistrict+" "+tbxProvince+" "+tbxZipCode;
  frmLoanWorkInfo.lblWorkingAddressInfor.text = workingAddressInfo;
   
   frmLoanWorkInfo.lbltempOfficeAddr.text =  tbxAddrVal+"~"+buildingNo+"~"+tbxFloorNo+"~"+tbxMoo+"~"+tbxSoi+"~"+tbxRoad+"~"+tbxSubDistrict+"~"+tbxDistrict+"~"+tbxProvince+"~"+tbxZipCode;
//   frmLoanWorkInfo.lblWorkAddHidden.text = tempWorkinfo;
   
//   frmLoanWorkInfo.lblAddWithData.text =tbxAddrVal;
//   frmLoanWorkInfo.lblBuildingWithData.text =buildingNo;
//   frmLoanWorkInfo.lblRoadNoWithdata.text = tbxRoad;
  
  
//   frmLoanWorkInfo.lblFloorVal.text = tbxFloorNo;
//   frmLoanWorkInfo.lblSubDistrictMainVal.text =tbxSubDistrict;
//   frmLoanWorkInfo.lblDistrictMainVal.text = tbxDistrict;
//   frmLoanWorkInfo.lblProvinceMainVal.text =tbxProvince;
//   frmLoanWorkInfo.lblZipCodeMainVal.text =tbxZipCode;
 
  gblPersonalInfo.WorkingAdress = {"lblAddNoVal" : tbxAddrVal,"lblBuildingVal" : buildingNo, "lblRoadNoVal" : tbxRoad,
                      "tbxSoi" : tbxSoi, "tbxMoo" : tbxMoo, "tbxFloorNo" : tbxFloorNo,
                     "tbxSubDistrict" : tbxSubDistrict, "tbxDistrict" : tbxDistrict,
                     "tbxProvince" : tbxProvince, "tbxZipCode" : tbxZipCode,};
   
  frmLoanWorkInfo.flxWorkingAddress.setVisibility(false);
  frmLoanWorkInfo.flxWorkingAddressFilled.setVisibility(true);
  frmLoanWorkInfo.flxMain.setVisibility(true);
   frmLoanWorkInfo.flxAddressPopUp.setVisibility(false);
   frmLoanWorkInfo.flxWorkingInfo.setVisibility(true);
  frmLoanWorkInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("Working_Info_Title");
  frmLoanWorkInfo.btnBackWorkInfo.onClick = gotoLoanApplicationForm;
  handleSaveBtnWorkInfo();
   //frmLoanWorkInfo.show();
   frmLoanWorkInfo.flxMain.scrollToEnd(true);
 }catch(e){
   alert("Exception in popUpSaveAddress : "+e);
 }
}


function retrivechangeWorkAddress(){
    
//   var tempWorkingAddr = frmLoanWorkInfo.lblWorkAddHidden.text;
  var tempWorkingAddr = frmLoanWorkInfo.lbltempOfficeAddr.text;
  var locale = kony.i18n.getCurrentLocale();
  kony.print("### tempWorkingAddr : "+tempWorkingAddr);
  if(null !==  tempWorkingAddr && "" !== tempWorkingAddr && tempWorkingAddr !== undefined && tempWorkingAddr.split("~").length > 1){
    var tbxAddrVal = tempWorkingAddr.split("~")[0] !== undefined ? tempWorkingAddr.split("~")[0] : "";
    var buildingNo = tempWorkingAddr.split("~")[1] !== undefined ? tempWorkingAddr.split("~")[1] : "";
    var tbxFloorNo = tempWorkingAddr.split("~")[2] !== undefined ? tempWorkingAddr.split("~")[2] : "";
    var tbxMoo = tempWorkingAddr.split("~")[3] !== undefined ? tempWorkingAddr.split("~")[3] : "";
    var tbxSoi = tempWorkingAddr.split("~")[4] !== undefined ? tempWorkingAddr.split("~")[4] : "";
    var tbxRoad = tempWorkingAddr.split("~")[5] !== undefined ? tempWorkingAddr.split("~")[5] : "";
    var tbxSubDistrict = tempWorkingAddr.split("~")[6] !== undefined ? tempWorkingAddr.split("~")[6] : "";
    var tbxDistrict = tempWorkingAddr.split("~")[7] !== undefined ? tempWorkingAddr.split("~")[7] : "";
    
    var tbxProvince = tempWorkingAddr.split("~")[8] !== undefined ? tempWorkingAddr.split("~")[8] : "";
    var tbxZipCode = tempWorkingAddr.split("~")[9] !== undefined ? tempWorkingAddr.split("~")[9] : "";
    
   if(isNotBlank(tbxFloorNo)){
    if (locale == "th_TH") {
    var tbxFloorNoDesc = getLocalizedString("Floor_label")+" "+tbxFloorNo;
  } else {
    var tbxFloorNoDesc = tbxFloorNo+" "+getLocalizedString("Floor_label");
  }
 }else{
    var tbxFloorNoDesc = "";
 }
   if(isNotBlank(tbxMoo)){
    if (locale == "th_TH") {
    var tbxMooDesc = getLocalizedString("Moo_label")+" "+tbxMoo;
  } else {
    var tbxMooDesc = getLocalizedString("Moo_label")+" "+tbxMoo;
  }
 }else{
    var tbxMooDesc = "";
 }
   if(isNotBlank(tbxSoi)){
    if (locale == "th_TH") {
    var tbxSoiDesc = getLocalizedString("Soi_label")+" "+tbxSoi;
  } else {
    var tbxSoiDesc = getLocalizedString("Soi_label")+" "+tbxSoi;
  }
 }else{
    var tbxSoiDesc = "";
 }
   if(isNotBlank(tbxRoad)){
    if (locale == "th_TH") {
    var tbxRoadDesc = getLocalizedString("Road_label")+" "+tbxRoad;
  } else {
    var tbxRoadDesc = tbxRoad+" "+getLocalizedString("Road_label");
  }
 }else{
    var tbxRoadDesc = "";
 }
    
    var RetriveworkAddressDesc =  tbxAddrVal+" "+buildingNo+" "+tbxFloorNoDesc+" "+tbxMooDesc+" "+tbxSoiDesc+" "+tbxRoadDesc+" "+tbxSubDistrict+" "+tbxDistrict+" "+tbxProvince+" "+tbxZipCode;
    
    return RetriveworkAddressDesc;
  }
}


function handleSaveBtnWorkInfo() {
  //start
  var confirmSkin = "";
    if(isNotBlank(frmLoanWorkInfo.lblEmploymentStatusDesc.text) && isNotBlank(frmLoanWorkInfo.lblOccupationDesc.text)&&
    isNotBlank(frmLoanWorkInfo.lblProfessionalDesc.text) && isNotBlank(frmLoanWorkInfo.lblLengthOfCurrentEmployee.text) &&
    isNotBlank(frmLoanWorkInfo.tbxDept.text) && isNotBlank(frmLoanWorkInfo.tbxOfficeNo.text) && frmLoanWorkInfo.tbxOfficeNo.text.length >= 11 && frmLoanWorkInfo.flxWorkingAddressFilled.isVisible){                   
    if(frmLoanWorkInfo.lblEmpCode.text == "01"){
        if((frmLoanWorkInfo.lblContractEmployedDesc.isVisible||frmLoanWorkInfo.lblContractEmp.isVisible)){
          confirmSkin = "btnBlue28pxLoan2";
        }else{
          confirmSkin = "btnGreyBGNoRound";
         }
      }else {
        confirmSkin = "btnBlue28pxLoan2";
      }
            
   } else {
        confirmSkin = "btnGreyBGNoRound";
         }

  if(confirmSkin === "btnBlue28pxLoan2") { 
    frmLoanWorkInfo.flxcircle.skin="flxGreenCircleLoan";
    frmLoanWorkInfo.lblcircle.skin = "skinpiGreenLineLone";
    frmLoanWorkInfo.btnNext.skin = confirmSkin;
    frmLoanWorkInfo.btnNext.focusSkin = "btnBlueBGNoRound150pxLoan";
   // nextJSONPreparationWI();
    frmLoanWorkInfo.btnNext.onClick = saveAndContinue;
  } else {
      frmLoanWorkInfo.btnNext.onClick = null;
    frmLoanWorkInfo.btnNext.skin = "btnGreyBGNoRound";
    frmLoanWorkInfo.btnNext.focusSkin="btnGreyBGNoRound";
    frmLoanWorkInfo.lblcircle.skin = "skinpiorangeLineLone";
    frmLoanWorkInfo.flxcircle.skin="slFbox";
  }
  //end
}

/* function nextJSONPreparationWI() {
  try {
    frmLoanApplicationForm.lblPersonalCircle.skin = "skinpiGreenLineLone";    
    frmLoanApplicationForm.lblcircle.skin="flxGreenCircleLoan";
    gblPersonalInfo = {"lblSalVal" : frmLoanWorkInfo.lblSalVal.text,
                       "lblTitleTypeVal" : frmLoanWorkInfo.lblTitleTypeVal.text,
                       "lblThaiNameVal" : frmLoanWorkInfo.lblThaiNameVal.text,
                       "lblThaiSurNameVal" : frmLoanWorkInfo.lblThaiSurNameVal.text,
                       "lblEngNameVal" : frmLoanWorkInfo.lblEngNameVal.text,
                       "lblEngSurNameVal" : frmLoanWorkInfo.lblEngSurNameVal.text,
                       "lblSourceCountry" : frmLoanWorkInfo.lblSourceCountry.text,
                       "lblEmailVal" : frmLoanWorkInfo.lblEmailVal.text,
                       "lblEStatementVal" : frmLoanWorkInfo.lblEStatementVal.text,
                       "lblResidentStatusVal" : frmLoanWorkInfo.lblResidentStatusVal.text,
                       "lblMobileNo" : frmLoanWorkInfo.lblMobileNo.text
                      };
    //alert("nextJSONPreparation : "+JSON.stringify(gblPersonalInfo));
  } catch(e) {
    alert("Exception in nextJSONPreparation : "+e);
  }
}*/

function touchImgContractEmp(){
  frmLoanWorkInfo.flxContractPopUp.left=0+"%";
  frmLoanWorkInfo.flxContractPopUp.zIndex=20;
  addContractData();
}
function endEditDeptWI(){
  if(isNullorEmpty(frmLoanWorkInfo.tbxDept.text)){
    frmLoanWorkInfo.lblDepartmentName.setVisibility(true);
    frmLoanWorkInfo.tbxDept.setVisibility(false);
    frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblDepartmentName.setVisibility(false);
    frmLoanWorkInfo.tbxDept.setVisibility(true);
    frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(true);
  }
}
function touchEndDeptWI(){
  if(isNullorEmpty(frmLoanWorkInfo.tbxDept.text)){
    frmLoanWorkInfo.lblDepartmentName.setVisibility(true);
    frmLoanWorkInfo.tbxDept.setVisibility(false);
    frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(false);
  }
}
function onDoneDeptWI(){
  workingInformationPostShow();
  saveDepartmentName();
}

// function closeMobilePopup(){
//   frmLoanWorkInfo.flxMobilePopUp.setVisibility(false);
//   frmLoanWorkInfo.flxMain.isVisible = true;
// }
function tambolPopup(){

  frmContactAddressWithoutData.lblTambolPopulate.setVisibility(false);
  frmContactAddressWithoutData.lstTamBol.setVisibility(true);

}
function saveTambol(){
  tumbolPopUpSaveWI();
  workingInformationPostShow();
}
function closeTumbolPopup(){

  frmLoanWorkInfo.flxMain.isVisible = true;

}
function onRowClicksegDyna(){
  assignValue();
  workingInformationPostShow();
}


function closeDynamicPopup(){
  kony.print("closeDynamicPopup");
  frmLoanWorkInfo.flxDynamicPopUp.setVisibility(false);
  //frmLoanWorkInfo.flxDynamicPopUp.left = 100+"%";
  frmLoanWorkInfo.flxDynamicPopUp.zIndex=20;
}
function onDoneAddress(){
 // onDoneWsave();
  touchStartflxAddressNo();
}
function onDoneBuildingNo(){
  onDoneWsave();
  touchStartFlexBuildingNo();
}

function onDoneSoi(){
  onDoneWsave();
  touchStartFlexSoi();
}
function onDoneRoad(){
  onDoneWsave();
  touchStartFlexRoad();
}
function onDoneMoo(){
  onDoneWsave();
  touchStartFlexMoo();
}



function saveWorkAddr(){
  //frmLoanWorkInfo.FlexWorkingAddressPop.left = 100 +"%";
  //frmLoanWorkInfo.FlexWorkingAddressPop.setVisibility(false);
  addDynamicWAddress();
  workingInformationPostShow();
}

function touchEndFlexBuildingNo(){

  frmLoanWorkInfo.lblBuildingNo.setVisibility(true);
  frmLoanWorkInfo.tbcBuildingNo.setVisibility(true);
  frmLoanWorkInfo.lblbuildingMoo.setVisibility(false);
  frmLoanWorkInfo.tbcBuildingNo.setFocus(true);

}


function touchStartFlexBuildingNo(){
  if(isNullorEmpty(frmLoanWorkInfo.tbcBuildingNo.text)){

    frmLoanWorkInfo.lblbuildingMoo.setVisibility(true);
    frmLoanWorkInfo.tbcBuildingNo.setVisibility(false);
    frmLoanWorkInfo.lblBuildingNo.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblbuildingMoo.setVisibility(false);
    frmLoanWorkInfo.tbcBuildingNo.setVisibility(true);
    frmLoanWorkInfo.lblBuildingNo.setVisibility(true);
  }

}

function touchEndFlexSoi(){

  frmLoanWorkInfo.lblSoi.setVisibility(true);
  frmLoanWorkInfo.tbxSoi.setVisibility(true);
  frmLoanWorkInfo.lblSoiNo.setVisibility(false);
  frmLoanWorkInfo.tbxSoi.setFocus(true);

}


function touchStartFlexSoi(){

  if(isNullorEmpty(frmLoanWorkInfo.tbxSoi.text)){

    frmLoanWorkInfo.lblSoiNo.setVisibility(true);
    frmLoanWorkInfo.tbxSoi.setVisibility(false);
    frmLoanWorkInfo.lblSoi.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblSoiNo.setVisibility(false);
    frmLoanWorkInfo.tbxSoi.setVisibility(true);
    frmLoanWorkInfo.lblSoi.setVisibility(true);
  }

}
function touchEndFlexRoad(){

  frmLoanWorkInfo.lblRoad.setVisibility(true);
  frmLoanWorkInfo.tbxRoad.setVisibility(true);
  frmLoanWorkInfo.lblRoadNo.setVisibility(false);
  frmLoanWorkInfo.tbxRoad.setFocus(true);

}


function touchStartFlexRoad(){

  if(isNullorEmpty(frmLoanWorkInfo.tbxRoad.text)){

    frmLoanWorkInfo.lblRoadNo.setVisibility(true);
    frmLoanWorkInfo.tbxRoad.setVisibility(false);
    frmLoanWorkInfo.lblRoad.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblRoadNo.setVisibility(false);
    frmLoanWorkInfo.tbxRoad.setVisibility(true);
    frmLoanWorkInfo.lblRoad.setVisibility(true);
  }

}
function touchEndFlexMoo(){

  frmLoanWorkInfo.lblMoo.setVisibility(true);
  frmLoanWorkInfo.tbxMoo.setVisibility(true);
  frmLoanWorkInfo.lblMooNo.setVisibility(false);
  frmLoanWorkInfo.tbxMoo.setFocus(true);


}


function touchStartFlexMoo(){

  if(isNullorEmpty(frmLoanWorkInfo.tbxMoo.text)){

    frmLoanWorkInfo.lblMooNo.setVisibility(true);
    frmLoanWorkInfo.tbxMoo.setVisibility(false);
    frmLoanWorkInfo.lblMoo.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblMooNo.setVisibility(false);
    frmLoanWorkInfo.tbxMoo.setVisibility(true);
    frmLoanWorkInfo.lblMoo.setVisibility(true);
  }

}


function touchEndflxAddressNo(){

  frmLoanWorkInfo.lblAddressNo.setVisibility(true);
  frmLoanWorkInfo.tbxAddrVal.setVisibility(true);
  frmLoanWorkInfo.lblAddressTitle.setVisibility(false);
  frmLoanWorkInfo.tbxAddrVal.setFocus(true);

}


function touchStartflxAddressNo(){

  if(isNullorEmpty(frmLoanWorkInfo.tbxAddrVal.text)){

    frmLoanWorkInfo.lblAddressTitle.setVisibility(true);
    frmLoanWorkInfo.tbxAddrVal.setVisibility(false);
    frmLoanWorkInfo.lblAddressNo.setVisibility(false);
  }else{
    frmLoanWorkInfo.lblAddressTitle.setVisibility(false);
    frmLoanWorkInfo.tbxAddrVal.setVisibility(true);
    frmLoanWorkInfo.lblAddressNo.setVisibility(true);
  }

}
function closeContractPopUp(){
//   frmLoanWorkInfo.flxContractPopUp.left = 100+"%";
//   frmLoanWorkInfo.flxContractPopUp.zIndex=1;
  frmLoanWorkInfo.flxMain.setVisibility(true);
  frmLoanWorkInfo.flxContractPopUp.setVisibility(false);
  frmLoanWorkInfo.flxMain.scrollToEnd(true);
}

function cancelContractEmp(){
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnEmploy.skin = "btnBlueTabBGNoRound";
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnContracted.skin = "btnTabWhiteRightRound";
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnEmploy.focusSkin = "btnBlueTabBGNoRound";
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnContracted.focusSkin = "btnTabWhiteRightRound";
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.skin = slFboxBrd0pxLoan;
  frmLoanWorkInfo.flxContractPopUp.FlexStartDate.setVisibility(false);
  frmLoanWorkInfo.flxContractPopUp.FlexEndDate.setVisibility(false);
  frmLoanWorkInfo.flxContractPopUp.FlexContractPeriod.setVisibility(false);
  frmLoanWorkInfo.flxContractPopUp.btnSaveContractEmployed.top = 48+"%";
}

function nextContractEmp(){

  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnEmploy.skin = "btnWhiteTabBGNoRound";
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnContracted.skin = "btnTabBlueRightRound";
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnEmploy.focusSkin = "btnWhiteTabBGNoRound";
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.btnContracted.focusSkin = "btnTabBlueRightRound";	
  frmLoanWorkInfo.flxContractPopUp.flxbuttons.skin = slFboxBrd0pxLoan;
  frmLoanWorkInfo.flxContractPopUp.FlexStartDate.setVisibility(true);
  frmLoanWorkInfo.flxContractPopUp.FlexEndDate.setVisibility(true);
  frmLoanWorkInfo.flxContractPopUp.FlexContractPeriod.setVisibility(true);
  frmLoanWorkInfo.flxContractPopUp.btnSaveContractEmployed.top = 6+"%";

  var flag = checkIsSaveEnable();

  if(flag){
    frmLoanWorkInfo.btnSaveContractEmployed.setEnabled(true);
    frmLoanWorkInfo.btnSaveContractEmployed.skin = "btnBlueBGNoRound150pxLoan";
  }
}

function selectStartDate(){
  var flag = checkIsSaveEnable();

  if(flag){
    frmLoanWorkInfo.btnSaveContractEmployed.setEnabled(true);
    frmLoanWorkInfo.btnSaveContractEmployed.skin = "btnBlueBGNoRound150pxLoan";
  }
}
function selectEndDate(){
  var flag = checkIsSaveEnable();

  if(flag){
    frmLoanWorkInfo.btnSaveContractEmployed.setEnabled(true);
    frmLoanWorkInfo.btnSaveContractEmployed.skin = "btnBlueBGNoRound150pxLoan";
  }
}

// function wrkAdressPopupClose(){
//   //frmLoanWorkInfo.FlexWorkingAddressPop.left = 100+"%";
//   frmLoanWorkInfo.FlexWorkingAddressPop.setVisibility(false);
//   frmLoanWorkInfo.flxMain.isVisible = true;

// }

function departmentSettings(){
  frmLoanWorkInfo.lblDepartmentNameHeader.setVisibility(true);
  frmLoanWorkInfo.tbxDept.setVisibility(true);
  frmLoanWorkInfo.tbxDept.setFocus(true);
  frmLoanWorkInfo.lblDepartmentName.setVisibility(false);
  handleSaveBtnWorkInfo();
}

function loanCurrentDate() {
	var day = new Date();
	var dd = day.getDate();
	var mm = day.getMonth() + 1;
	var yyyy = day.getFullYear();
	var datearray = [dd, mm, yyyy, "0", "0", "0"];
	return datearray;
}

function loanAssignedDate() {
	var day = new Date();
	var dd = day.getDate();
	var mm = day.getMonth() + 1;
	var yyyy = day.getFullYear();
	var datearray = [dd, mm, yyyy, "0", "0", "0"];
	return datearray;
}

function loanAssignedDate(dateAssigned) {
    var day = dateAssigned;
    var dd = day.split("/")[0];
    var mm = day.split("/")[1];
    var yyyy = day.split("/")[2];
    var dateAssigned = [dd, mm, yyyy, "0", "0", "0"];
    return dateAssigned;
}

function contractEmp() {
    //   frmLoanWorkInfo.flxContractPopUp.left=0+"%";
    //   frmLoanWorkInfo.flxContractPopUp.zIndex=2;
    //   frmLoanWorkInfo.flxMain.setVisibility(false);
    var StartDate = "";
    var EndDate = "";
    var currentdatearray = "";
    if(frmLoanWorkInfo.lblContractFlag.text == "Y"){
          nextContractEmp();
    } else {
        cancelContractEmp();
    }
    if (frmLoanWorkInfo.lblContractStartDateVal.isVisible) {
        StartDate = loanAssignedDate(frmLoanWorkInfo.lblContractStartDateVal.text);
        frmLoanWorkInfo.calStartDate.dateComponents = StartDate;
    } else if (!isNotBlank(gblPersonalInfo.contractEmpStartDate)) {
        currentdatearray = loanCurrentDate();
        frmLoanWorkInfo.calStartDate.dateComponents = currentdatearray;
    } else {
        contractStartDate = gblPersonalInfo.contractEmpStartDate;
        StartDate = loanAssignedDate(contractStartDate);
        frmLoanWorkInfo.calStartDate.dateComponents = StartDate;
    }
    if (frmLoanWorkInfo.lblContractEndDtVal.isVisible) {
        EndDate = loanAssignedDate(frmLoanWorkInfo.lblContractEndDtVal.text);
        frmLoanWorkInfo.calEndDate.dateComponents = EndDate;
    } else if (!isNotBlank(gblPersonalInfo.contractEmpEndDate)) {
        currentdatearray = loanCurrentDate();
        frmLoanWorkInfo.calEndDate.dateComponents = currentdatearray;
    } else {
        contractEndDate = gblPersonalInfo.contractEmpEndDate;
        EndDate = loanAssignedDate(contractEndDate);
        frmLoanWorkInfo.calEndDate.dateComponents = EndDate;
    }
    if(frmLoanWorkInfo.lblContrtPeriodVal.text == kony.i18n.getLocalizedString("Monthly_tab")){
       frmLoanWorkInfo.btnMonthlystmt.skin = "btnBlueTabBGNoRound";
       frmLoanWorkInfo.btnYearlystmt.skin = "btnTabWhiteRightRound";
       frmLoanWorkInfo.btnMonthlystmt.focusSkin = "btnBlueTabBGNoRound";
       frmLoanWorkInfo.btnYearlystmt.focusSkin = "btnTabWhiteRightRound";
    } else if(frmLoanWorkInfo.lblContrtPeriodVal.text ==  kony.i18n.getLocalizedString("Yearly_tab")){
       frmLoanWorkInfo.btnYearlystmt.skin = "btnTabBlueRightRound";
       frmLoanWorkInfo.btnMonthlystmt.skin = "btnWhiteTabBGNoRound";
       frmLoanWorkInfo.btnYearlystmt.focusSkin = "btnTabBlueRightRound";
       frmLoanWorkInfo.btnMonthlystmt.focusSkin = "btnWhiteTabBGNoRound";
    }
    frmLoanWorkInfo.flxContractPopUp.setVisibility(true);
    addContractData();
    frmLoanWorkInfo.btnSaveContractEmployed.onClick = saveContractEmp;
}

function saveContractEmp(){
//   frmLoanWorkInfo.flxContractPopUp.left = 100+"%";
//   frmLoanWorkInfo.flxContractPopUp.zIndex=1;
  
  if(frmLoanWorkInfo.btnEmploy.skin === "btnBlueTabBGNoRound"){
    frmLoanWorkInfo.lblContractFlag.text = "N";
  }else{
    frmLoanWorkInfo.lblContractFlag.text = "Y";
    var flag = validateLoanContractDates();
    if (false === flag) {
      alert("Contract End date should be greater than Contract Start Date");
      return false;
    }
  }
  if(frmLoanWorkInfo.btnMonthlystmt.skin === "btnBlueTabBGNoRound"){
    frmLoanWorkInfo.lblContrtPeriodVal.text = kony.i18n.getLocalizedString("Monthly_tab");
  }else if(frmLoanWorkInfo.btnYearlystmt.skin === "btnTabBlueRightRound"){
    frmLoanWorkInfo.lblContrtPeriodVal.text =  kony.i18n.getLocalizedString("Yearly_tab");
  }
  frmLoanWorkInfo.flxContractPopUp.setVisibility(false);
  frmLoanWorkInfo.flxMain.setVisibility(true);
  addDynamicContract();
  handleSaveBtnWorkInfo();
  //workingInformationPostShow();
  frmLoanWorkInfo.flxMain.scrollToEnd(true);
}

function validateLoanContractDates(){
  try{
  	var startDate = new Date(frmLoanWorkInfo.calStartDate.month + "/" + frmLoanWorkInfo.calStartDate.day + "/" + frmLoanWorkInfo.calStartDate.year);
	var endDate = new Date(frmLoanWorkInfo.calEndDate.month + "/" + frmLoanWorkInfo.calEndDate.day + "/" + frmLoanWorkInfo.calEndDate.year);
  kony.print("startDate : "+startDate+" , endDate : "+endDate);
    var second = 1000,
          minute = second * 60,
          hour = minute * 60,
          day = hour * 24;
    
    var timediff = endDate - startDate;
    kony.print(timediff);
    var daysDiff = Math.floor(timediff / day);
    kony.print("daysDiff : "+daysDiff);
    if (daysDiff > 0) {
      kony.print("Greater");
      return true;
    }else{
      kony.print("lesser or equal");
      return false;
    }
  }catch(e){
    return false;
  } 
}

function lengthCurrentEmp(){
  var employmentLength = frmLoanWorkInfo.lblLengthOfCurrentEmployee.text;  
  if(employmentLength !== "" && employmentLength !== undefined){
     var employmentLength = replaceAll(employmentLength, " ", "~");
     frmLoanWorkInfo.tbxYear.text = employmentLength.split("~")[0] !== undefined ? employmentLength.split("~")[0] : "";
     frmLoanWorkInfo.tbxMonth.text = employmentLength.split("~")[2] !== undefined ? employmentLength.split("~")[2] : "";
  }
  frmLoanWorkInfo.flxLengthCurrentEmp.setVisibility(true);
//   frmLoanWorkInfo.flxMain.setVisibility(false);
  //frmLoanWorkInfo.flxLengthCurrentEmp.isVisible = true;
  frmLoanWorkInfo.tbxYear.maxTextLength = 2;
  frmLoanWorkInfo.tbxMonth.maxTextLength = 2;
  frmLoanWorkInfo.tbxYear.setFocus(true);
  frmLoanWorkInfo.tbxYear.onTextChange = yearsCheck;
  frmLoanWorkInfo.tbxYear.onDone = yearsCheck;
  frmLoanWorkInfo.tbxMonth.onDone = monthsCheck;
  frmLoanWorkInfo.tbxMonth.onTextChange = monthsCheck;
//   frmLoanWorkInfo.flxLengthCurrentEmp.zIndex = 2;
  frmLoanWorkInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("LengthOfCurrent_title");
  frmLoanWorkInfo.btnBackWorkInfo.onClick = gotoLoanWorkInfo;
  var yearsLength = frmLoanWorkInfo.tbxYear.text;
  var monthsLength = frmLoanWorkInfo.tbxMonth.text;
  if((frmLoanWorkInfo.tbxYear.text == "0" && frmLoanWorkInfo.tbxMonth.text == "0") || (frmLoanWorkInfo.tbxYear.text == "00" && frmLoanWorkInfo.tbxMonth.text == "00")){
    frmLoanWorkInfo.tbxYear.text = ""
    frmLoanWorkInfo.tbxMonth.text = ""
  }
  monthsCheck();
  yearsCheck();
  frmLoanWorkInfo.btnSaveCurrentEmploye.onClick = saveYearMonths;
  
}

function yearsCheck(){
  var yearsTxt = "";
  var currentDate = kony.os.date("dd/mm/yyyy");
  var employeDob = gblCustomerDob;
  var yearcurretdate = currentDate.substring(6, 10);
  var yearemployeDob = employeDob.substring(6, 10);
  var yearcurretdateInt = parseFloat(yearcurretdate);
  var yearemployeDobInt = parseFloat(yearemployeDob);
  var expyears = yearcurretdateInt - yearemployeDobInt;
  yearsTxt = frmLoanWorkInfo.tbxYear.text;
  var years = parseInt(yearsTxt); 
  if(years > expyears){
    //frmLoanWorkInfo.tbxYear.text = 0;
//     showAlert("Please enter value less than 60.", "Info");
    frmLoanWorkInfo.tbxYear.setEnabled(false);
    showAlertWithCallBack("Please enter value less than your age.", "Info",yearsetfocus);
  }else{
    if(yearsTxt.length == 2 ){
    	frmLoanWorkInfo.tbxMonth.setFocus(true);
    }
  }
  if(isNotBlank(frmLoanWorkInfo.tbxYear.text) && isNotBlank(frmLoanWorkInfo.tbxMonth.text)){
  frmLoanWorkInfo.btnSaveCurrentEmploye.skin = "btnBlue28pxLoan";
  frmLoanWorkInfo.btnSaveCurrentEmploye.focusSkin = "btnBlueBGNoRound150pxLoan";
  }else{
    frmLoanWorkInfo.btnSaveCurrentEmploye.skin = "btnGreyBGNoRound";
    frmLoanWorkInfo.btnSaveCurrentEmploye.focusSkin = "btnGreyBGNoRound";
  }
}

function yearsetfocus(){
  frmLoanWorkInfo.tbxYear.setEnabled(true);
  frmLoanWorkInfo.tbxYear.text = "";
  frmLoanWorkInfo.tbxYear.setFocus(true);
}

function monthsCheck(){
  var tbxMonthvar = parseInt(frmLoanWorkInfo.tbxMonth.text); 
  if(tbxMonthvar > 11){
    //frmLoanWorkInfo.tbxMonth.text = 0;
//     showAlert("Please enter value less than 12.", "Info");
    frmLoanWorkInfo.tbxMonth.setEnabled(false);
    showAlertWithCallBack(kony.i18n.getLocalizedString("InputMonthError"), kony.i18n.getLocalizedString("info"),mnthsetfocus);
  }
  if(isNotBlank(frmLoanWorkInfo.tbxYear.text) && isNotBlank(frmLoanWorkInfo.tbxMonth.text)){
  frmLoanWorkInfo.btnSaveCurrentEmploye.skin = "btnBlue28pxLoan";
  frmLoanWorkInfo.btnSaveCurrentEmploye.focusSkin = "btnBlueBGNoRound150pxLoan";
  }else{
    frmLoanWorkInfo.btnSaveCurrentEmploye.skin = "btnGreyBGNoRound";
    frmLoanWorkInfo.btnSaveCurrentEmploye.focusSkin = "btnGreyBGNoRound";
  }
}

function mnthsetfocus(){
  frmLoanWorkInfo.tbxMonth.setEnabled(true);
  frmLoanWorkInfo.tbxMonth.text = "";
  frmLoanWorkInfo.tbxMonth.setFocus(true);
}

function saveYearMonths(){
  var yearsLength = frmLoanWorkInfo.tbxYear.text;
  var monthsLength = frmLoanWorkInfo.tbxMonth.text;
  var empLengthLable = yearsLength+" "+getLocalizedString("Years_textbox")+" "+monthsLength+" "+getLocalizedString("Months_textbox");
  frmLoanWorkInfo.lblEmpMonth.text = frmLoanWorkInfo.tbxMonth.text;
  frmLoanWorkInfo.lblEmpYear.text = frmLoanWorkInfo.tbxYear.text;
  
  if(isNotBlank(yearsLength) && isNotBlank(monthsLength)){
  frmLoanWorkInfo.lblLengthOfCurrentEmployee.text = empLengthLable;
  handleSaveBtnWorkInfo();
  gotoLoanWorkInfo();
  frmLoanWorkInfo.flxMain.setVisibility(true);
  frmLoanWorkInfo.flxLengthCurrentEmp.setVisibility(false);
  departmentSettings();
  }
}

function gotoLoanWorkInfo(){
 // frmLoanWorkInfo.flxLengthCurrentEmp.isVisible = false;
//   frmLoanWorkInfo.flxLengthCurrentEmp.zIndex = 1;
 //   frmLoanWorkInfo.show();
  frmLoanWorkInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("Working_Info");
   frmLoanWorkInfo.flxLengthCurrentEmp.setVisibility(false);
  frmLoanWorkInfo.flxMain.setVisibility(true);
  frmLoanWorkInfo.btnBackWorkInfo.onClick = gotoLoanApplicationForm;
}

function workingAddr(){
    
//   var tempWorkingAddr = frmLoanWorkInfo.lblWorkAddHidden.text;
  var tempWorkingAddr = frmLoanWorkInfo.lbltempOfficeAddr.text;
  kony.print("### tempWorkingAddr : "+tempWorkingAddr);
  if(null !==  tempWorkingAddr && "" !== tempWorkingAddr && tempWorkingAddr !== undefined && tempWorkingAddr.split("~").length > 1){
    frmLoanWorkInfo.tbxAddrVal.text = tempWorkingAddr.split("~")[0] !== undefined ? tempWorkingAddr.split("~")[0] : "";
    frmLoanWorkInfo.tbxBuildingNo.text = tempWorkingAddr.split("~")[1] !== undefined ? tempWorkingAddr.split("~")[1] : "";
    frmLoanWorkInfo.tbxFloorNo.text = tempWorkingAddr.split("~")[2] !== undefined ? tempWorkingAddr.split("~")[2] : "";
    frmLoanWorkInfo.tbxMoo.text = tempWorkingAddr.split("~")[3] !== undefined ? tempWorkingAddr.split("~")[3] : "";
    frmLoanWorkInfo.tbxSoi.text = tempWorkingAddr.split("~")[4] !== undefined ? tempWorkingAddr.split("~")[4] : "";
    frmLoanWorkInfo.tbxRoad.text = tempWorkingAddr.split("~")[5] !== undefined ? tempWorkingAddr.split("~")[5] : "";
    frmLoanWorkInfo.tbxSubDistrict.text = tempWorkingAddr.split("~")[6] !== undefined ? tempWorkingAddr.split("~")[6] : "";
    frmLoanWorkInfo.tbxDistrict.text = tempWorkingAddr.split("~")[7] !== undefined ? tempWorkingAddr.split("~")[7] : "";
    
    frmLoanWorkInfo.tbxProvince.text = tempWorkingAddr.split("~")[8] !== undefined ? tempWorkingAddr.split("~")[8] : "";
    frmLoanWorkInfo.tbxZipCode.text = tempWorkingAddr.split("~")[9] !== undefined ? tempWorkingAddr.split("~")[9] : "";
    addressDetailsOnClick("Y");
  }else{
    addressDetailsOnClick("N");
  }
  
  frmLoanWorkInfo.btnBackWorkInfo.onClick = gotoWorkInfo;

  frmLoanWorkInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("WorkingAddress");
  
  frmLoanWorkInfo.flxAddressPopUp.setVisibility(true);
//   frmLoanWorkInfo.flxMain.setVisibility(false);
  frmLoanWorkInfo.flxWorkingInfo.setVisibility(false);
  handleAddrSaveBtn();
}


function saveAndContinue(){
  ////frmLoanApplicationForm.CopyimgCompletionCircle0a2d39a85897b49.setVisibility(true);
  ////frmLoanApplicationForm.CopyImage0f8929401bfe549.setVisibility(false);
  // //frmLoanApplicationForm.lblCircle2.skin = "skinpiGreenLineLone";
  //   //frmLoanApplicationForm.flxlblcircle2.skin="flxGreenCircleLoan";
  //   onClickNextWInfo();
  if(frmLoanWorkInfo.btnNext.skin === "btnBlue28pxLoan2") {
    frmLoanApplicationForm.lblWorkingCircle.skin = "skinpiGreenLineLone";
     frmLoanApplicationForm.flxWorkingCirle.skin = "flxGreenCircleLoan";
//     alert("inside working filled");
  }
  //gotoLoanApplicationForm();
   var tempWorkingAddr = frmLoanWorkInfo.lbltempOfficeAddr.text;
    if (null !== tempWorkingAddr && "" !== tempWorkingAddr && tempWorkingAddr !== undefined && tempWorkingAddr.split("~").length > 1) {
        frmLoanWorkInfo.tbxAddrVal.text = tempWorkingAddr.split("~")[0] !== undefined ? tempWorkingAddr.split("~")[0] : "";
        frmLoanWorkInfo.tbxBuildingNo.text = tempWorkingAddr.split("~")[1] !== undefined ? tempWorkingAddr.split("~")[1] : "";
        frmLoanWorkInfo.tbxFloorNo.text = tempWorkingAddr.split("~")[2] !== undefined ? tempWorkingAddr.split("~")[2] : "";
        frmLoanWorkInfo.tbxMoo.text = tempWorkingAddr.split("~")[3] !== undefined ? tempWorkingAddr.split("~")[3] : "";
        frmLoanWorkInfo.tbxSoi.text = tempWorkingAddr.split("~")[4] !== undefined ? tempWorkingAddr.split("~")[4] : "";
        frmLoanWorkInfo.tbxRoad.text = tempWorkingAddr.split("~")[5] !== undefined ? tempWorkingAddr.split("~")[5] : "";
        frmLoanWorkInfo.tbxSubDistrict.text = tempWorkingAddr.split("~")[6] !== undefined ? tempWorkingAddr.split("~")[6] : "";
        frmLoanWorkInfo.tbxDistrict.text = tempWorkingAddr.split("~")[7] !== undefined ? tempWorkingAddr.split("~")[7] : "";
        frmLoanWorkInfo.tbxProvince.text = tempWorkingAddr.split("~")[8] !== undefined ? tempWorkingAddr.split("~")[8] : "";
        frmLoanWorkInfo.tbxZipCode.text = tempWorkingAddr.split("~")[9] !== undefined ? tempWorkingAddr.split("~")[9] : "";
    }
  onClickOfDoneBtnWorking();
  handleSaveBtnWorkInfo();
}

function onClickOfDoneBtnWorking(){
  try{	
  	// need to pass below values to updateCustomerInfo service
  	//empStatusCode , occupationCode, professionCode, lengthOfCurrentEmp, departmentName
     // contractEmp, workingInfo, officeNumber, extNumber
    if(!isNotBlank(frmLoanWorkInfo.lblEmpYear.text) || frmLoanWorkInfo.lblEmpYear.text == "00"){
            frmLoanWorkInfo.lblEmpYear.text = "0";
        }
        if(!isNotBlank(frmLoanWorkInfo.lblEmpMonth.text) || frmLoanWorkInfo.lblEmpMonth.text == "00"){
            frmLoanWorkInfo.lblEmpMonth.text = "0";
        }
    var contractEmpStartDate = "", contractEmpEndDate = "";
    if("Y" === frmLoanWorkInfo.lblContractFlag.text){
      contractEmpStartDate = frmLoanWorkInfo.lblContractStartDateVal.isVisible ? frmLoanWorkInfo.lblContractStartDateVal.text : "";
      contractEmpEndDate = frmLoanWorkInfo.lblContractEndDtVal.isVisible ? frmLoanWorkInfo.lblContractEndDtVal.text : "";
    }
    var inputParams = {
 		empStatusCode : frmLoanWorkInfo.lblEmpCode.text,
 		occupationCode : frmLoanWorkInfo.lblOccupationCode.text,
 		professionCode : frmLoanWorkInfo.lblProfessionalCode.text,
 		empMonth : frmLoanWorkInfo.lblEmpMonth.text,
        empYear : frmLoanWorkInfo.lblEmpYear.text,
      	departmentName : frmLoanWorkInfo.tbxDept.text,
        contractEmpStartDate : contractEmpStartDate,
        contractEmpEndDate : contractEmpEndDate,
      	workingInfo : frmLoanWorkInfo.lblWorkAddHidden.text,
      	officeNumber : frmLoanWorkInfo.tbxOfficeNo.text,
      	extNumber : frmLoanWorkInfo.tbxExtNo.text,
		caId : gblCaId,
        employmentInfoSavedFlag : "Y"
      	
	};
     if(null === frmLoanWorkInfo.lblContractFlag.text || undefined === frmLoanWorkInfo.lblContractFlag.text){
    	inputParams["contractEmp"] ="";
     }else{
       inputParams["contractEmp"] =frmLoanWorkInfo.lblContractFlag.text;
     }
    var addressworking = {
        addrTypCode : "O",
        address : frmLoanWorkInfo.tbxAddrVal.text,
 		amphur : frmLoanWorkInfo.tbxDistrict.text,
 		buildingName : frmLoanWorkInfo.tbxBuildingNo.text,
//  		cifId : frmLoanWorkInfo.lblEmpMonth.text,
//       country : gblcountryCode,
//       	id : frmLoanWorkInfo.tbxDept.text,
        floor : frmLoanWorkInfo.tbxFloorNo.text,
      	moo : frmLoanWorkInfo.tbxMoo.text,
      	postalCode : frmLoanWorkInfo.tbxZipCode.text,
      	province : frmLoanWorkInfo.tbxProvince.text,
        road : frmLoanWorkInfo.tbxRoad.text,
      	streetName : frmLoanWorkInfo.tbxSoi.text,
      	tumbol : frmLoanWorkInfo.tbxSubDistrict.text
    };
    
    inputParams.addresses = addressworking;
    kony.print("inputParams : "+JSON.stringify(inputParams));
	showLoadingScreen();
	invokeServiceSecureAsync("updateCustInfoJavaService", inputParams, callbackOfSaveWorkingInfoFields);
    gblPersonalInfo.empMonth = frmLoanWorkInfo.tbxMonth.text;
    gblPersonalInfo.empYear = frmLoanWorkInfo.tbxYear.text;
    gblPersonalInfo.empDept = frmLoanWorkInfo.tbxDept.text;
  }catch(e){
    alert("Exception in onClickOfDoneBtnWorking function , "+e);
  }
}

function callbackOfSaveWorkingInfoFields(status, result){
    try{
//       alert("result : "+JSON.stringify(result));
      if (result.opstatus == "0") {
        dismissLoadingScreen();
        kony.print("result : "+JSON.stringify(result));
        
        gblPersonalInfo.saveWI = true;
        var tempAddresses = frmLoanWorkInfo.tbxAddrVal.text+"~"+frmLoanWorkInfo.tbxBuildingNo.text+"~"+frmLoanWorkInfo.tbxFloorNo.text+"~"+frmLoanWorkInfo.tbxMoo.text+"~"+frmLoanWorkInfo.tbxRoad.text
        +"~"+frmLoanWorkInfo.tbxSoi.text+"~"+frmLoanWorkInfo.tbxSubDistrict.text+"~"+frmLoanWorkInfo.tbxDistrict.text+"~"+frmLoanWorkInfo.tbxProvince.text+"~"+frmLoanWorkInfo.tbxZipCode.text;
        gblPersonalInfo.totalOfficeAddr = tempAddresses;
        gblPersonalInfo.saveIncome = false;
        callbackOfgetPersonalInfoFields(status, result);
        //gotoLoanApplicationForm();
      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    }catch(e){
    alert("Exception in callbackOfSaveWorkingInfoFields, e : "+e);
      dismissLoadingScreen();
  }
  dismissLoadingScreen();
}


function settingSavedValueInWF(){
  try{
    gotoLoanApplicationForm();
    
    //setting actual empCode and occupationCode and prof Code
    frmLoanWorkInfo.lblEmpCode.text = gblPersonalInfo.OCCUPATION.refEntryCode;
	frmLoanWorkInfo.lblOccupationCode.text = gblPersonalInfo.OCCUPATION.entryCode;
	frmLoanWorkInfo.lblProfessionalCode.text = gblPersonalInfo.profCode;
  }catch(e){
    kony.print("Error occcured in settingSavedValueInWF: , "+e);
  }
}

function gotoLoanApplicationForm(){
  kony.print("in gotoLoanApplicationForm");
  frmLoanApplicationForm.show();  
}

function gotoWorkInfo(){
  kony.print("in gotoWorkInfo");
//   handleAddrSaveBtn();
//   handleSaveBtnWorkInfo();
//   frmLoanWorkInfo.show();
  frmLoanWorkInfo.lblApplicationForm.text = kony.i18n.getLocalizedString("Working_Info");
  frmLoanWorkInfo.flxWorkingInfo.setVisibility(true);
  frmLoanWorkInfo.flxMain.setVisibility(true);
  frmLoanWorkInfo.flxMain.scrollToEnd(true);
  frmLoanWorkInfo.flxAddressPopUp.setVisibility(false);
  frmLoanWorkInfo.btnBackWorkInfo.onClick = gotoLoanApplicationForm;
}

function workingInformationPostShow(){     
  kony.print("workingInformationPostShow entered");
  try{
    frmLoanWorkInfo.flxMain.isVisible = true;
    var flag = checkEnable();
    frmLoanWorkInfo.btnNext.skin = "btnGreyBGNoRound";   
    frmLoanWorkInfo.btnNext.focusSkin = "btnGreyBGNoRound"; 
    frmLoanWorkInfo.flxFooter.skin = "flexWhiteBG";
    kony.print("workingInformationPostShow in try ");
//     if(flag){
//       //frmLoanWorkInfo.btnNext.setEnabled(true);
//       //frmLoanWorkInfo.flxWorkingInfo.imgInCompletionCircle.setVisibility(false);
//       // frmLoanWorkInfo.flxWorkingInfo.imgCompletionCircle.setVisibility(true);
//       frmLoanWorkInfo.flxcircle.skin="flxGreenCircleLoan";
//       frmLoanWorkInfo.lblcircle.skin = "skinpiGreenLineLone";
//       frmLoanWorkInfo.btnNext.skin = "btnBlue28pxLoan2";
//       frmLoanWorkInfo.flxFooter.skin = "flexBlueBG";
//       frmLoanWorkInfo.btnNext.focusSkin = "btnBlueBGNoRound150pxLoan";
//       //frmLoanWorkInfo.btnNext.onClick = onClickNextWInfo;
//       kony.print("workingInformationPostShow if");
//     }else{    
//       //frmLoanWorkInfo.btnNext.setEnabled(false);
//       // frmLoanWorkInfo.flxWorkingInfo.imgInCompletionCircle.setVisibility(true);
//       // frmLoanWorkInfo.flxWorkingInfo.imgCompletionCircle.setVisibility(false);
//       frmLoanWorkInfo.lblcircle.skin = "skinpiorangeLineLone";
//       frmLoanWorkInfo.flxcircle.skin="slFbox";
//       frmLoanWorkInfo.btnNext.skin = "btnGreyBGNoRound";
//       frmLoanWorkInfo.flxFooter.skin = "flexWhiteBG";
//       frmLoanWorkInfo.btnNext.focusSkin = "btnGreyBGNoRound";
//       kony.print("workingInformationPostShow else");
//     }
    formatContactNo();
    kony.print("workingInformationPostShow out");
  } catch(e){
    kony.print("workingInformationPostShow@@@ "+e);
  }
  kony.print("workingInformationPostShow total out");
}


function searchloanworkinginfo(){
  
 var currForm = kony.application.getCurrentForm();
  var userInput = currForm.tbxSearch.text;
  kony.print("Search String ::: "+userInput);
  kony.print("gblLoanSearchFlow  "+gblLoanSearchFlow);
  
  var tempDataToPass = [];
  
  var legthOfuserInput = userInput.length;
  
  if(legthOfuserInput == 0){
 	kony.print("Reset All Values and legthOfSearchString is "+legthOfuserInput);
     setDropDownDatainSeg(gblloanCollectionData, true, "", gblLoanSearchFlow);
    if (currForm == frmLoanPersonalInfo) {
            frmLoanPersonalInfo.flxInformationMsg.setVisibility(true);
            frmLoanPersonalInfo.flxScrllSourceOfIncome.setVisibility(false);
        }else if(currForm == frmLoanWorkInfo){
            frmLoanWorkInfo.flxBankPopupSeg.setVisibility(true);
            frmLoanWorkInfo.flxInformationMsg.setVisibility(false);
        }
  }else if(legthOfuserInput >= 1){
    kony.print("Reset All Values and legthOfSearchString is "+legthOfuserInput);
    
    userInput = userInput.toLowerCase();
    
  		var countMatchedKey = 0;
		for (var countVal = 0; countVal < gblloanCollectionData.length; countVal++){
			var keyToSearch = "";
				if (locale == "th_TH") {
				keyToSearch = gblloanCollectionData[countVal].entryName2;
			} else {
				
              keyToSearch  = gblloanCollectionData[countVal].entryName;
			}
          keyToSearch = keyToSearch.toLowerCase();
			kony.print("userInput "+userInput +" keyToSearch "+keyToSearch);
			if(keyToSearch.indexOf(userInput) >=0 ){
				tempDataToPass[countMatchedKey] = gblloanCollectionData[countVal];
				countMatchedKey = countMatchedKey+1;
			}
		}
    kony.print("tempDataToPass "+tempDataToPass.length);
    setDropDownDatainSeg(tempDataToPass, true, "", gblLoanSearchFlow);
  }else{
    return;
  }
  if (countMatchedKey == 0) {
    if(currForm == frmLoanWorkInfo){
        frmLoanWorkInfo.flxInformationMsg.setVisibility(true);
        frmLoanWorkInfo.flxBankPopupSeg.setVisibility(false);
    }else if(currForm == frmLoanPersonalInfo){
          frmLoanPersonalInfo.flxInformationMsg.setVisibility(true);
          frmLoanPersonalInfo.flxScrllSourceOfIncome.setVisibility(false);
       }
  } else {
       if(currForm == frmLoanWorkInfo){
         frmLoanWorkInfo.flxBankPopupSeg.setVisibility(true);
         frmLoanWorkInfo.flxInformationMsg.setVisibility(false);
       }else if(currForm == frmLoanPersonalInfo){
         frmLoanPersonalInfo.flxScrllSourceOfIncome.setVisibility(true);
         frmLoanPersonalInfo.flxInformationMsg.setVisibility(false);
       }
   }
}