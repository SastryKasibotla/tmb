
function MBcallGetFundList(){
  var inputParam = {};
  inputParam["channel"] = "rc"; 
  showLoadingScreen();
  invokeServiceSecureAsync("MFFundListInq", inputParam, getFundListMBServiceCallBack)
}

function getFundListMBServiceCallBack(status, resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    if (resulttable["opstatus"] == 0) {
      gblFundList = resulttable["fundListDS"]; 
      if(gblMFEventFlag == MF_EVENT_PURCHASE) {
        frmMFSelectPurchaseFundMB.show();
      } else if(gblMFEventFlag == MF_EVENT_REDEEM) {
        frmMFSelectRedeemFundMB.show();
      }else if(gblMFEventFlag == MF_EVENT_SWO) {
        
        kony.print("In getFundListMBServiceCallBack  result is  "+ JSON.stringify(resulttable));
        setFundListForRedeem();
        leftAnimation("flxFundList", "0%", 0.3, 0);
      }
    } else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  } else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}
function checkIsAllowAllotType(fundCode){
  for(var i = 0 ; i < gblFundList.length ; i++){
    if(gblFundList[i]["fundCode"] == fundCode){
      if( (gblFundList[i]["allotType"] == "2") || (gblFundList[i]["allotType"] == "3")) {
        return false;
      } else {
        return true;
      }
    } 
  }
}
function setFundListForRedeem(){
  var summaryData = gblMFSummaryData["FundClassDS"];
  var segmentData=[];
  var locale = kony.i18n.getCurrentLocale();
  var redeemFundListData = [];

  for(var i=0;i<summaryData.length;i++){

    var fundhouseDS = summaryData[i].FundHouseDS
    var fundHouseLength = fundhouseDS.length;
    var fundClassCode = summaryData[i].FundClassCode

    for(var j=0;j<fundHouseLength;j++){

      var FundCodeDS = fundhouseDS[j].FundCodeDS;
      var FundCodeDSLength = FundCodeDS.length;
      var fundHouseCode = fundhouseDS[j]["FundHouseCode"]
      for(var k=0; k < FundCodeDSLength;k++){

        if(FundCodeDS[k]["MarketValue"] != "0"){
          //if(checkIsAllowAllotType(FundCodeDS[k]["FundCode"])) {

          var tempRecord = { 
            "LabelUnitHolderNo": formatUnitHolderNumer(FundCodeDS[k]["UnitHolderNo"]),
            "LabelFundName": FundCodeDS[k]["FundShortName"],
            "LabelFundHouseCode": fundHouseCode,
            "LabelFundClaseCode": fundClassCode,
            "FundNameTH" : FundCodeDS[k]["FundNameTH"],
            "FundNameEN" : FundCodeDS[k]["FundNameEN"],
			"FundCode": FundCodeDS[k]["FundCode"]
          };
          redeemFundListData.push(tempRecord);
          //}
        }
      }//for loop end k
    }//for loop end j
  }//for loop end i
  frmMFSelectFundRedeemMB.SegmentFundList.setData(redeemFundListData);
  
  frmMFSwitchLanding.SegmentTargetFundList.isVisible = false;
  frmMFSwitchLanding.SegmentFundList.isVisible = true;
  frmMFSwitchLanding.SegmentFundList.setData(redeemFundListData);
  
}

function getFundRuleRedeemMBonSelectedIndex(){
  var inputParam = {};
  var selectedIndex = frmMFSelectFundRedeemMB.SegmentFundList.selectedIndex[1];
  var fundDataObject = frmMFSelectFundRedeemMB.SegmentFundList.data[selectedIndex];
  var unitholder = "";
  var fundCode = "";
  var fundHouseCode = "";
  var fundShortName = "";

  gblMFEventFlag = MF_EVENT_REDEEM;

  fundCode = fundDataObject.FundCode;
  unitholder = fundDataObject.LabelUnitHolderNo.replace(/-/gi, "");
  fundHouseCode = fundDataObject.LabelFundHouseCode;
  fundShortName = fundDataObject.LabelFundName;

  gblMFOrder["fundHouseCode"] = fundHouseCode; 
  gblMFOrder["fundCode"] = fundCode;
  gblMFOrder["unitHolderNo"] = unitholder;
  //gblMFOrder["fundNameTH"] = fundDataObject.FundNameTH; //mki, mib-11102
  //gblMFOrder["fundNameEN"] = fundDataObject.FundNameEN; //mki, mib-11102
  gblMFOrder["fundNameTH"] = gblMBMFDetailsResulttable["FundNameTH"]; //mki, mib-11102
  gblMFOrder["fundNameEN"] = gblMBMFDetailsResulttable["FundNameEN"]; //mki, mib-11102

  gblMFOrder["fundShortName"] = fundShortName;

  showLoadingScreen();

  inputParam["unitHolderNo"] = unitholder;
  inputParam["orderType"] = gblMFEventFlag;
  inputParam["fundHouseCode"] = fundHouseCode; 
  inputParam["fundCode"] = fundCode; 
  inputParam["orderDate"] = "";

  invokeServiceSecureAsync("MFFundValidation", inputParam, getFundRuleSummaryFlowCallBack);
}

function getFundRuleSummaryFlowCallBack(status,resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    if ( (resulttable["opstatus"] == 0) ) {
      gblFundRulesData = resulttable;
      if(gblChannel == "MB"){
        MBorderMutualFund();
      }else if(gblChannel == "IB"){
        IBorderMutualFund();
      }

    } else {
      if ( (resulttable["errCode"] == 21) ) { //MKI, MIB - 10144 start
        dismissLoadingScreen();
      	showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01004"), kony.i18n.getLocalizedString("info"));
      }
      else if ( (resulttable["errCode"] == 22) ) { //MKI,MIB-11357 start
        dismissLoadingScreen();
      	showAlert(kony.i18n.getLocalizedString("MF_ERR_UHNO_NOTALLOW"), kony.i18n.getLocalizedString("info"));
      }//MKI,MIB-11357 End
      else if ( (resulttable["errCode"] == 1) ) {
        dismissLoadingScreen();
      	showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
      }
      else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      } //MKI, MIB - 10144 End
    }
  } else {
    dismissLoadingScreen();
  }	

}

function frmMFSelectFundRedeemMBOnClose(){
  frmMFSelectRedeemFundMB.show();
}

function callMBMutualFundsDetailsForSelectFund(){

  var inputParam = {};
  var selectedIndex = frmMFSelectFundRedeemMB.SegmentFundList.selectedIndex[1];
  var fundDataObject = frmMFSelectFundRedeemMB.SegmentFundList.data[selectedIndex];
  var unitholder = "";
  var fundCode = "";
  var funClassCode = "";

  fundCode = fundDataObject.FundCode;
  unitholder = fundDataObject.LabelUnitHolderNo.replace(/-/gi, "");
  
  var APchar = unitholder.substring(0, 2);
  kony.print("APchar  "+APchar);
    kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
	alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  
  funClassCode = fundDataObject.LabelFundClaseCode;
  gblMFOrder["fundClassCode"] = "";//funClassCode;
  inputParam["unitHolderNo"] = unitholder;
  inputParam["funCode"] = fundCode;
  inputParam["fundClassCode"] = funClassCode;
  inputParam["serviceType"] = "1";
  gblMFOrder["redeemUnit"] = ORD_UNIT_ALL;
  showLoadingScreen();
  invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callMutualFundsDetailsSelectFundCallBackMB);
}

function callMutualFundsDetailsSelectFundCallBackMB(status,resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    if ( (resulttable["opstatus"] == 0) ) {
      gblMBMFDetailsResulttable = resulttable;
      if(gblMFEventFlag == MF_EVENT_PURCHASE){
        getFundRulePurchaseOnSelectedIndex();
      } else if(gblMFEventFlag == MF_EVENT_REDEEM){
        getFundRuleRedeemMBonSelectedIndex();
      }

    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  } else {
    dismissLoadingScreen();
  }	

}
//=== Changed by: Mayank, References: MIB-9160
function setFundListForPurchase(fundHouseCode, searchFundCode) {
    var locale = kony.i18n.getCurrentLocale();
    var purchaseFundListData = [];
    var fundCode = "";
  	var fundShortname =  "";
  	var imgWow ="imgselectfund.png"; //MKI,MIB-11522
    for (var i = 0; i < gblFundList.length; i++) {
		if (locale == "th_TH") {
          
            fundNickName = gblFundList[i]["fundNameTH"]
        } else {       
            fundNickName = gblFundList[i]["fundNameEN"]
        }
      	kony.print("MKI gblFundList"+ JSON.stringify(gblFundList));
      	if(gblFundList[i]["fundTypeB"] != undefined && gblFundList[i]["fundTypeB"] != "" && gblFundList[i]["fundTypeB"] != null) //MKI,MIB-11522 start
        {
          kony.print("mki13551 inside if "+ gblFundList[i]["fundCode"].toLowerCase());
        	imgWow ="imgselectfund.png";
		} else{
           kony.print("mki13551 inside else");
            imgWow ="";
        } //MKI,MIB-11522 end
        if (fundHouseCode != "") {
            if (gblFundList[i]["fundHouseCode"] == fundHouseCode) {
                fundCode = gblFundList[i]["fundCode"].toLowerCase();
                fundShortname =  gblFundList[i]["fundShortName"];
                if (searchFundCode == "" || fundCode.includes(searchFundCode.toLowerCase()) || fundNickName.toLowerCase().includes(searchFundCode.toLowerCase()) 
                    || fundShortname.toLowerCase().includes(searchFundCode.toLowerCase())) {
                    
                    var tempRecord = {
                        "LabelFundFullName": fundNickName,
                        "FundCode": gblFundList[i]["fundCode"],
                        "LabelFundName": gblFundList[i]["fundShortName"],
                        "AllotType": gblFundList[i]["allotType"],
                        "FundHouseCode": gblFundList[i]["fundHouseCode"],
                        "FundShortName": gblFundList[i]["fundShortName"],
                        "FundNameTH": gblFundList[i]["fundNameTH"],
                        "FundNameEN": gblFundList[i]["fundNameEN"],
                      	"imgWowPoint":imgWow //MKI,MIB-11522
                    };
                    purchaseFundListData.push(tempRecord);
                }
            }
        } else {
            fundCode = gblFundList[i]["fundCode"].toLowerCase();
            if (searchFundCode == "" || fundCode.includes(searchFundCode.toLowerCase()) || fundNickName.toLowerCase().includes(searchFundCode.toLowerCase())
               			|| fundShortname.toLowerCase().includes(searchFundCode.toLowerCase())) {
                var tempRecord = {
                    "LabelFundFullName": fundNickName,
                    "FundCode": gblFundList[i]["fundCode"],
                    "LabelFundName": gblFundList[i]["fundShortName"],
                    "AllotType": gblFundList[i]["allotType"],
                    "FundHouseCode": gblFundList[i]["fundHouseCode"],
                    "FundShortName": gblFundList[i]["fundShortName"],
                    "FundNameTH": gblFundList[i]["fundNameTH"],
                    "FundNameEN": gblFundList[i]["fundNameEN"],
                  	"imgWowPoint":imgWow //MKI,MIB-11522
                };
                purchaseFundListData.push(tempRecord);
            }
        }
    }
    purchaseFundListData.sort(dynamicSortOther("FundCode"));
    if (gblChannel == "MB") {
        frmMFSeachFundPurchaseMB.SegmentFundList.setData(purchaseFundListData);
    } else if (gblChannel == "IB") {
        frmIBMFSelPurchaseFundsEntry.SegmentFundList.removeAll();
        frmIBMFSelPurchaseFundsEntry.SegmentFundList.setData(purchaseFundListData);
    }

}
function getUnitHolderNoPrefix(unitHolderNo){
  var prefixCode = "";
  prefixCode = unitHolderNo.substring(0, 3);
  return prefixCode;
}
function setUnitHolderNumberListPurchase(fundHouseCodeSearch, fundCode, allotType){
  var summaryData = gblMFSummaryData["FundClassDS"];
  var segmentData=[];
  var locale = kony.i18n.getCurrentLocale();
  var unitHolderNoData = [];

  var sas_prefix_ltfrmf = gblMFSummaryData["SAS_UHNO_LRMF_PREFIX"];
  var sas_prefix_other = gblMFSummaryData["SAS_UHNO_OTHER_PREFIX"];
  
  for(var i=0;i<summaryData.length;i++){

    var fundhouseDS = summaryData[i].FundHouseDS
    var fundHouseLength = fundhouseDS.length;
    var fundClassCode = summaryData[i].FundClassCode

    for(var j=0;j<fundHouseLength;j++){

      var FundCodeDS = fundhouseDS[j].FundCodeDS;
      var FundCodeDSLength = FundCodeDS.length;
      var fundHouseCode = fundhouseDS[j]["FundHouseCode"];

      for(var k=0; k < FundCodeDSLength;k++){
        var unitHolderNo = FundCodeDS[k]["UnitHolderNo"];
		var isValidFundType = false;
        if(fundHouseCodeSearch != "TMBAM"){
          var prefix = getUnitHolderNoPrefix(unitHolderNo);
          if(allotType == '2' || allotType == '3'){
			if(GBL_MF_TEMENOS_ENABLE_FLAG =="ON" && FundCodeDS[k]["FundNameTH"] == allotType){	 //MKI, MIB-Charuphat Onsamlee mail start			
					isValidFundType = true;				
			}else if(GBL_MF_TEMENOS_ENABLE_FLAG =="OFF" && (sas_prefix_ltfrmf.indexOf(prefix) > 0)){				
					isValidFundType = true;				
			}else{
              	isValidFundType = false;				
            }
            if(isValidFundType) { //MKI, MIB-Charuphat Onsamlee mail End
              if(fundHouseCode == fundHouseCodeSearch) {
					var tempRecord = { 
					  "LabelUnitHolderNo": formatUnitHolderNumer(unitHolderNo)
					};
					if ((JSON.stringify(unitHolderNoData).indexOf(JSON.stringify(tempRecord))) > 0){
					  // duplicate fundhouse
					} else {
					  unitHolderNoData.push(tempRecord);
					}
              }
            }
          } else {
             //MKI, MIB-Charuphat Onsamlee mail start
            if(GBL_MF_TEMENOS_ENABLE_FLAG  =="ON"  &&  FundCodeDS[k]["FundNameTH"] == '1'){				
					isValidFundType = true;				
			}else if(GBL_MF_TEMENOS_ENABLE_FLAG  =="OFF"  && (sas_prefix_other.indexOf(prefix) > 0)){				
					isValidFundType = true;				
			}else{
              	isValidFundType = false;				
            }
            if(isValidFundType) { //MKI, MIB-Charuphat Onsamlee mail End
				if(fundHouseCode == fundHouseCodeSearch) {
					var tempRecord = { 
					"LabelUnitHolderNo": formatUnitHolderNumer(unitHolderNo)
				  };
				  if ((JSON.stringify(unitHolderNoData).indexOf(JSON.stringify(tempRecord))) > 0){
					// duplicate fundhouse
				  } else {
					unitHolderNoData.push(tempRecord);
				  }
				}
            }
          }
        } else {
          if(fundHouseCode == fundHouseCodeSearch) {
            var tempRecord = { 
              "LabelUnitHolderNo": formatUnitHolderNumer(unitHolderNo)
            };
            if ((JSON.stringify(unitHolderNoData).indexOf(JSON.stringify(tempRecord))) > 0){
              // duplicate fundhouse
            } else {
              unitHolderNoData.push(tempRecord);
            }
          }
        }
        //}

      }//for loop end k

    }//for loop end j
  }//for loop end i
   
  if(gblChannel == "MB"){
    frmMFFilterUnitHolderNoMB.SegmentList.removeAll();
    frmMFFilterUnitHolderNoMB.SegmentList.setData(unitHolderNoData);
     
	if(frmMFFilterUnitHolderNoMB.SegmentList.data == null || frmMFFilterUnitHolderNoMB.SegmentList.data == "") //MKI,MIB-11900
      return 0;
	else 
      return frmMFFilterUnitHolderNoMB.SegmentList.data.length;
  } else if(gblChannel == "IB"){
    frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.removeAll();
    frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.setData(unitHolderNoData);
	if(frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.data == null || frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.data == "") //MKI,MIB-11900
      return 0;
	else 
      return frmIBMFSelPurchaseFundsEntry.SegmentUnitHolderList.data.length;
  } 
  return 0;
  
}
function getFundRulePurchaseOnSelectedIndex(){
  var inputParam = {};

  gblMFEventFlag = MF_EVENT_PURCHASE;

  showLoadingScreen();

  inputParam["unitHolderNo"] = gblMFOrder["unitHolderNo"];
  inputParam["orderType"] = gblMFEventFlag;
  inputParam["fundHouseCode"] = gblMFOrder["fundHouseCode"]; 
  inputParam["fundCode"] = gblMFOrder["fundCode"]; 
  inputParam["orderDate"] = "";

  invokeServiceSecureAsync("MFFundValidation", inputParam, getFundRuleSummaryFlowCallBack);
}

function setFundHouseListForPurchase(){

  var locale = kony.i18n.getCurrentLocale();
  var fundHouseListDataTemp = [];
  var fundHouseListData = [];
  var logo = "";
  var fundHouseCode = "";
  var randomnum;
  var fundHouseList = gblMFSummaryData["FundHouseList"];
  var tempTMBRecord = "";
  var fundHouseName = "";
  for(var i = 0 ; i < fundHouseList.length ; i++){

    randomnum = Math.floor((Math.random() * 10000) + 1);
    fundHouseCode = fundHouseList[i]["fundHouseCode"];
    if(gblChannel == "MB"){
      logo = loadMFIcon(fundHouseCode);
    }else{
      logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+fundHouseCode+"&modIdentifier=MFLOGOS&rr="+randomnum;
    }
    
    if(locale == "th_TH") {
      fundHouseName = fundHouseList[i]["fundHouseNameTh"]
    } else {
      fundHouseName = fundHouseList[i]["fundHouseNameEn"]
    }

    if(fundHouseCode == "TMBAM"){
      tempTMBRecord = { 
        "LabelFundHouse" : fundHouseName,
        "ImageFund" : logo,
        "fundHouseCode" : fundHouseCode,
        "fundHouseNameEn" : fundHouseList[i]["fundHouseNameEn"],
        "fundHouseNameTh" : fundHouseList[i]["fundHouseNameTh"]
      };
    } else {
      var tempRecord = { 
        "LabelFundHouse" : fundHouseName,
        "ImageFund" : logo,
        "fundHouseCode" : fundHouseCode,
        "fundHouseNameEn" : fundHouseList[i]["fundHouseNameEn"],
        "fundHouseNameTh" : fundHouseList[i]["fundHouseNameTh"]
      };
      fundHouseListData.push(tempRecord);
    }
  }
  fundHouseListData.sort(dynamicSortOther("fundHouseCode"));
  var tempAllRecord = { 
    "LabelFundHouse" : kony.i18n.getLocalizedString("MF_RDM_20"),
    "ImageFund" : "",
    "fundHouseCode" : kony.i18n.getLocalizedString("MF_RDM_20"),
    "fundHouseNameEn" : "",
    "fundHouseNameTh" : ""
  };
  //fundHouseListData.push(tempAllRecord);
  fundHouseListData.splice(0, 0, tempAllRecord);
  fundHouseListData.splice(1, 0, tempTMBRecord);
  if(gblChannel == "MB"){
    frmMFSeachFundPurchaseMB.SegmentFundHouseList.removeAll();
    frmMFSeachFundPurchaseMB.SegmentFundHouseList.setData(fundHouseListData);
  } else if(gblChannel == "IB"){
    popupIBSelectFundHouseMF.SegmentFundHouse.removeAll();
    popupIBSelectFundHouseMF.SegmentFundHouse.setData(fundHouseListData);
  }

}
function IBcallGetFundList(){
  var inputParam = {};
  inputParam["channel"] = "rc"; 
  showLoadingScreen();
  invokeServiceSecureAsync("MFFundListInq", inputParam, getFundListIBServiceCallBack)
}
function getFundListIBServiceCallBack(status, resulttable){
  if (status == 400) {
	dismissLoadingScreen();
    if (resulttable["opstatus"] == 0) {
      gblFundList = resulttable["fundListDS"]; 
      if(gblMFEventFlag == MF_EVENT_PURCHASE) {
        frmIBMFSelPurchaseFundsEntry.show();
      } else if(gblMFEventFlag == MF_EVENT_REDEEM) {
        frmIBMFSelRedeemFund.show();
      }
    } else{
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  } else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}

function setFundListForRedeemIB(){
  var summaryData = gblMFSummaryData["FundClassDS"];
  var segmentData=[];
  var locale = kony.i18n.getCurrentLocale();
  var redeemFundListData = [];

  for(var i=0;i<summaryData.length;i++){

    var fundhouseDS = summaryData[i].FundHouseDS
    var fundHouseLength = fundhouseDS.length;
    var fundClassCode = summaryData[i].FundClassCode

    for(var j=0;j<fundHouseLength;j++){

      var FundCodeDS = fundhouseDS[j].FundCodeDS;
      var FundCodeDSLength = FundCodeDS.length;
      var fundHouseCode = fundhouseDS[j]["FundHouseCode"]
      for(var k=0; k < FundCodeDSLength;k++){

        if(FundCodeDS[k]["MarketValue"] != "0"){

          var tempRecord = { 
            "lblunitholdernoval": formatUnitHolderNumer(FundCodeDS[k]["UnitHolderNo"]),
            "lblfundcodeval": FundCodeDS[k]["FundShortName"],
            "LabelFundHouseCode": fundHouseCode,
            "LabelFundClaseCode": fundClassCode,
            "FundNameTH" : FundCodeDS[k]["FundNameTH"],
            "FundNameEN" : FundCodeDS[k]["FundNameEN"],
			"FundCode": FundCodeDS[k]["FundCode"]

          };
          redeemFundListData.push(tempRecord);
        }
      }//for loop end k
    }//for loop end j
  }//for loop end i
  frmIBMFSelRedeemFund.SegmentSelectFund.setData(redeemFundListData);
}

function callIBMutualFundsDetailsForSelectFund(){

  var inputParam = {};
  var selectedIndex = frmIBMFSelRedeemFund.SegmentSelectFund.selectedIndex[1];
  var fundDataObject = frmIBMFSelRedeemFund.SegmentSelectFund.data[selectedIndex];
  var unitholder = "";
  var fundCode = "";
  var funClassCode = "";

  fundCode = fundDataObject.FundCode;
  unitholder = fundDataObject.lblunitholdernoval.replace(/-/gi, "");
  
  var APchar = unitholder.substring(0, 2);
  kony.print("APchar  "+APchar);
  kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
	alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  
  funClassCode = fundDataObject.LabelFundClaseCode;
  gblMFOrder["fundClassCode"] = "";//funClassCode;
  inputParam["unitHolderNo"] = unitholder;
  inputParam["funCode"] = fundCode;
  inputParam["fundClassCode"] = funClassCode;
  inputParam["serviceType"] = "1";
  showLoadingScreen();
  invokeServiceSecureAsync("MFUHAccountDetailInq", inputParam, callMutualFundsDetailsSelectFundCallBackIB);
}

function callMutualFundsDetailsSelectFundCallBackIB(status,resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    if ( (resulttable["opstatus"] == 0) ) {
      gblMFDetailsResulttable = resulttable;
      if(gblMFEventFlag == MF_EVENT_PURCHASE){
        //getFundRulePurchaseMBonSelectedIndex();
      } else if(gblMFEventFlag == MF_EVENT_REDEEM){
        getFundRuleRedeemIBonSelectedIndex(resulttable.FundNameTH,resulttable.FundNameEN); //mki, mib-11102
      }

    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  } else {
    dismissLoadingScreen();
  }	

}
function getFundRuleRedeemIBonSelectedIndex(varFundNameTH,varFundNameEN){ //mki, mib-11102
  var inputParam = {};
  var selectedIndex = frmIBMFSelRedeemFund.SegmentSelectFund.selectedIndex[1];
  var fundDataObject = frmIBMFSelRedeemFund.SegmentSelectFund.data[selectedIndex];
  var unitholder = "";
  var fundCode = "";
  var fundHouseCode = "";
  var fundShortName = "";

  gblMFEventFlag = MF_EVENT_REDEEM;

  fundCode = fundDataObject.FundCode;
  unitholder = fundDataObject.lblunitholdernoval.replace(/-/gi, "");
  fundHouseCode = fundDataObject.LabelFundHouseCode;
  fundShortName = fundDataObject.lblfundcodeval;

  gblMFOrder["fundHouseCode"] = fundHouseCode; 
  gblMFOrder["fundCode"] = fundCode;
  gblMFOrder["unitHolderNo"] = unitholder;
  gblMFOrder["fundShortName"] = fundShortName;
  gblMFOrder["fundNameTH"] = varFundNameTH; //mki, mib-11102
  gblMFOrder["fundNameEN"] = varFundNameEN; //mki, mib-11102

  showLoadingScreen();

  inputParam["unitHolderNo"] = unitholder;
  inputParam["orderType"] = gblMFEventFlag;
  inputParam["fundHouseCode"] = fundHouseCode; 
  inputParam["fundCode"] = fundCode; 
  inputParam["orderDate"] = "";
  

  invokeServiceSecureAsync("MFFundValidation", inputParam, getFundRuleSummaryFlowCallBack);
}

