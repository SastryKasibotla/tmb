







var retailLiqTrnsAdd;
 


// to show account list page, if no fixed account is hidden.
function onClickContinuePopForS2S() {
	
	gblS2SHiddenStatus = "dummyhds"; 
	onClickMyAccountsFlow();
	popTransactionPwd.dismiss();
}


// Function to take Decision of deleting Send to Save Service




/*
 * function to remove commas from amounts to send it as service input parameter
 */
function removeCommas(tempVal){
	if (tempVal == null) return ;

	
	var cleanedVal = tempVal.replace(/,/g, "");
	
	return cleanedVal;

}

/*
 * function to remove unwanted symbols from account nos to send it as service input parameter
 */
function removeUnwatedSymbols(tempValue){
	if (tempValue == null) return;
	
	
	var cleanedValue = tempValue.replace(/-/g, "");
	
	return cleanedValue;

}


//Function to validate and verify transaction passwd

function onClickSSConfrm(trnsPwd){
	var enteredTxt = trnsPwd;
	/*var pat1 = /[A-Za-z]/g
	var pat2 = /[0-9]/g
	var isAlpha = pat1.test(enteredTxt)
	var isNum = pat2.test(enteredTxt)
	
	if (enteredTxt != null && enteredTxt.length < 8) {
		
		gblShowPwd++;		
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
		
		showAlert("Please enter atleast 8 Characters for Transaction Password", "")
		
		return false;
	}
	else if (isAlpha == false || isNum == false) {
		gblShowPwd++;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
		showAlert("Please Enter atleast 1 alphabet and 1 numeric character for Transaction Password","");
		return false;
	}
	// to verify the pwd
*/	
	if(gblSSServieHours == "applyServiceHours")
	 s2sVerifyTxnPwdApply(trnsPwd);
	else if(gblSSServieHours == "dummyABX")
	 s2sVerifyTxnPwdEdit(trnsPwd);
	else if (gblSSServieHours == "dummyS2SExecuteTransfer")
	 s2sVerifyTxnPwdExecute(trnsPwd);
	 
}
 
/*
 *Function to retina display of iPhone 5
 */ 
function iPhoneDisplay(currForm, scrlR8Hght, scrlR8HghtiPh){
	// iPhone 5 ratina display
	//scrlR8Hght: container height for iPhone 5 n above, scrlR8HghtiPh: continer height for iPhone Size < 567
	/*var deviceInfo = kony.os.deviceInfo();
	var deviceHght = deviceInfo["deviceHeight"];
	if(deviceHght > 567){
		currForm.scrollbox474135225108084.containerHeight = scrlR8Hght;
		if(isSignedUser)
			currForm.scrollbox47417063023487.containerHeight = 91.85;
		else
			currForm.scrollbox47417063023487.containerHeight = 93.30;
	}
	else{
		currForm.scrollbox474135225108084.containerHeight = scrlR8HghtiPh; 
		if(isSignedUser)
			currForm.scrollbox47417063023487.containerHeight = 90.2;
		else
			currForm.scrollbox47417063023487.containerHeight = 92;
	}*/
}

// function to validate minimum, maximum and pivotal amount for Send to save service


/*
 * function to delete S2S service
 */
function retailLiquidityDelete(){
	var patryId = removeUnwatedSymbols(frmSSServiceED.lblToAccNo.text);
	inputParam = {};
	 
	//inputParam["actionFlag"] = "D";
    //inputParam["rmNum"] = "";//we need to save this value from LiquidityMod service in a gbl in integer form/string.
    inputParam["PartyId"] = patryId;//Main-account or No fixed Account
    //inputParam["channelName"] = "MB";
    inputParam["activityFlexValues1"] = gblPHONENUMBER;
    inputParam["activityFlexValues2"] = removeUnwatedSymbols(frmSSServiceED.lblFromAccNo.text);
    inputParam["activityFlexValues3"] = removeUnwatedSymbols(frmSSServiceED.lblToAccNo.text);
    inputParam["activityFlexValues4"] = removeCommas(frmSSServiceED.lblAmntLmtMax.text);
    inputParam["activityFlexValues5"] = removeCommas(frmSSServiceED.lblAmntLmtMin.text);

	invokeServiceSecureAsync("RetailLiquidityDelete", inputParam, retailLiquidityDeleteCB)
}
function retailLiquidityDeleteCB(status, resultTbl)
{
	
	
//	alert("Delete Status Code is:"+status);
	if (status == 400) {
		if (resultTbl["opstatus"] == 0){
			if(resultTbl["statusCode"] == 0){
				
				s2s_activityStatus = "01";
		
				
				onClickReturnS2S();	//to show account summary
				popupConfirmation.dismiss();
			}
			else {
				
				s2s_activityStatus = "02";
				showAlert(resultTbl["errMsg"], kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
				return false;
			}	
		}
		else{
    		showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info"));
	 	  	dismissLoadingScreen();
			return false;  
		}	
	}		
      
}
/*
 * function to update or edit Max and Min limit to use S2S service
 */
function invokeRetailLiquidityMod(){
	showLoadingScreen();
	
	
    var txtBalMaxVal = frmSSService.txtBalMax.text
     txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmSSService.txtBalMin.text
     txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	inputParam = {};
	// mobileNumber will be picked from session in preprocessor
	var inputParam = {
		acctIdentValue:linkdAcct,
		partyId:noFixedAcct,
		maxAmt:removeCommas(txtBalMaxVal),
		minAmt:removeCommas(txtBalMinVal)
	}
	invokeServiceSecureAsync("RetailLiquidityMod", inputParam, invokeRetailLiquidityModCallBack);
}

function invokeRetailLiquidityModCallBack(status, resultTbl){

	if (status == 400) //success responce
    {
    	
        
        
        if (resultTbl["opstatus"] == 0) {
        	
        	
			
			frmSSServiceED.lblAmntLmtMax.text = commaFormatted(resultTbl["amntMax"]);
			frmSSServiceED.lblAmntLmtMin.text = commaFormatted(resultTbl["amntMin"]);
	
			gblSSApply = true;
			frmSSServiceED.show();
			
            s2s_activityStatus = "01";
 			callApplyS2SActivityLogForEdit(s2s_activityStatus);
			
        }else {
            s2s_activityStatus = "02";
            callApplyS2SActivityLogForEdit(s2s_activityStatus);
            showAlert(resultTbl["errMsg"], kony.i18n.getLocalizedString("info"));
			return false;
        }        
    }
}

/*
* This function is used to check the business hours for S2S Edit flow 
*/

function checkBusinessHoursForS2SEditDel() {
	//Loading Screen function
	var inputParams = {		
		myProfileFlag:"false"
    }; 
    invokeServiceSecureAsync("checkS2SBusinessHours", inputParams, checkBusinessHoursForS2SEditDelCB);
}

function checkBusinessHoursForS2SEditDelCB(status, collectionData) {
	
	if (status == 400) {
		
        if (collectionData["opstatus"] == 0) {
        	
            if(collectionData["s2sBusinessHrsFlag"] == "true") {
		    	
		    	
				
				if(gblSSServieHours == "deleteS2S"){
					gblSSServieHours = "dummyXYZ"; //assigning this value to avoid any conflict 
					retailLiquidityDelete(); // this is for delete S2S flow, onClick of delete icon, we r giving value of gblSSApply = "delete"
				}
				else if(gblSSServieHours == "modifyS2S"){
					 gblSSServieHours = "dummyABX"; //assigning this value to avoid any conflict
					 if(flowSpa)
					 {
					 	var inputParams = {}
						spaChnage = "SendtoSaveEdit"
						gblOTPFlag = true;
						gblOnClickReq = false;
						try {
							kony.timer.cancel("otpTimer")
						} catch (e) {
							
						}
						//input params for generateotpwithuser
						gblSpaChannel="EditS2S"
						onClickOTPRequestSpa();
					 }
					 else
					 {
					 showOTPPopup(kony.i18n.getLocalizedString("TransactionPass")+":", "", "",onClickSSConfrm , 3);
					 }
					 
					
				}
				else if(gblSSServieHours == "S2SEdit"){
					frmSSService.imgLinkAcc.src = frmSSServiceED.imgLinkAcc.src;
					frmSSService.txtBalMax.text = frmSSServiceED.lblAmntLmtMax.text+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
					frmSSService.txtBalMin.text = frmSSServiceED.lblAmntLmtMin.text+" "+kony.i18n.getLocalizedString("currencyThaiBaht"); 
					frmSSService.lblFromAccBal.text = frmSSServiceED.lblFromAccBal.text;
					frmSSService.lblFromAccName.text = frmSSServiceED.lblFromAccName.text;
					frmSSService.lblFromAccNo.text = frmSSServiceED.lblFromAccNo.text ;
					frmSSService.lblFromAccType.text = frmSSServiceED.lblFromAccType.text;
					frmSSService.lblNFAccBal.text =	frmSSServiceED.lblToAccBal.text;
					frmSSService.lblNFAccName.text = frmSSServiceED.lblToAccName.text; 
					frmSSService.lblNFAccNo.text = frmSSServiceED.lblToAccNo.text; 
					frmSSService.lblNFAccType.text = frmSSServiceED.lblToAccType.text; 
					gblSSServieHours = "S2SAuthenticated";
					frmSSService.show();
					dismissLoadingScreen();
				}
				else if(gblSSServieHours == "s2sExecuteTransfer"){
				    s2sActivityStatus="00";
	                callApplyS2SActivityLogForS2SExecutionInitial(s2sActivityStatus);
					gblSSServieHours = "dummyS2SExecuteTransfer";
					if(flowSpa)
					 {
					 	var inputParams = {}
					 	onClickExecutePreConfirmMB();
						spaChnage = "SendtoSaveExecute"
						gblOTPFlag = true;
						gblOnClickReq = false;
						try {
							kony.timer.cancel("otpTimer")
						} catch (e) {
							
						}
						
						gblSpaChannel="ExecuteTransfer"
						//onClickOTPRequestSpa();
					 }
					else
					{
						showOTPPopup(kony.i18n.getLocalizedString("TransactionPass")+":", "", "",onClickSSConfrm , 3);				
					}
				}
				
		    }else{
	    		s2s_activityStatus = "02";
	    	//	activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
            	dismissLoadingScreen();
            	showAlert("Operation can be executed successfully only within service hours","info");
            	return false;
            }
        }else{
    		showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info"));
	 	  	dismissLoadingScreen();
			return false;  
		}	
    }
	
}



/*
 * Function to execute S2S trasfer amount, [screen: SSSExecute]
 */
function invokeRetailLiquidityTransferInq(){
    showLoadingScreen();
	invokeServiceSecureAsync("RetailLiquidityTransferInquiry", {}, retailLiquidityTransferInquiryCB)
}

var retailLiqTranInqRslt;
function retailLiquidityTransferInquiryCB(status, resultTbl){

	
	
	if (status == 400) //success responce
    {
    	
        
        
        dismissLoadingScreen();
        if (resultTbl["opstatus"] == 0){
            var StatusCode = resultTbl["StatusCode"];
            if (StatusCode != "0") {
				dismissLoadingScreen();
 				showCommonAlert(resultTbl["errMsg"],kony.i18n.getLocalizedString("info"));
				return false;
			}
            else{
        	retailLiqTranInqRslt = resultTbl;	// storing the data-table in global variable 
			// to fetch account nickname and account type
			linkdAcct = resultTbl["FromAcctNo"];
			noFixedAcct = resultTbl["ToAcctNo"];						
			getCustomerAccountInfo();
			}
		}else {
    		showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info"));
	 	  	dismissLoadingScreen();
	 	  	if(gblExeS2S == "true"){
        		onClickReturnS2S();
        		gblExeS2S = "dummyJpB";// to control flow
        	}
			return false;
		}
	}	
    
}

function s2sExeFormMapping(transRefNumForS2S){
	var resultTbl = retailLiqTranInqRslt;
	
	
	if(resultTbl["FromAcctNo"].length==0 ||resultTbl["ToAcctNo"].length==0){
	dismissLoadingScreen();
	alert("Service is returning null Values Please try after some time")
	return false;
	}
//Date format 2013-07-30 to 30/07/13	
	var date = resultTbl["TransferDate"];
	
	var dateSplit = date.split("-");
	var formattedDate = dateSplit[2] + "/" + dateSplit[1] + "/" + dateSplit[0].substring(dateSplit[0].length - 2, dateSplit[0].length);
	
	
	
	
	
	
	
	
	
	


	
	frmSSSExecute.lblSendFromAccName.text = exeFromAcctName;
	frmSSSExecute.lblSendFromAccType.text = exeFromAcctType;
	frmSSSExecute.lblSendFromAccNo.text = formatAccountNo(resultTbl["FromAcctNo"]);
	frmSSSExecute.lblSendFromAccBal.text = commaFormatted(resultTbl["FromAmt"]);
	
	frmSSSExecute.lblSendToAccName.text = exeToAcctName;
	frmSSSExecute.lblSendToAccBal.text = commaFormatted(resultTbl["ToAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
	frmSSSExecute.lblSendToAccType.text = exeToAcctType;
	frmSSSExecute.lblSendToAccNo.text = formatAccountNo(resultTbl["ToAcctNo"]);
	
	frmSSSExecute.lblAmntLmtMax.text = commaFormatted(resultTbl["MaxCurAmount"]);
	frmSSSExecute.lblAmntLmtMin.text = commaFormatted(resultTbl["MinCurAmt"]);
	frmSSSExecute.lblTrnsAmnt.text = commaFormatted(resultTbl["TranAmt"]);
	frmSSSExecute.lblTransDate.text = gblCurrentDateS2SMB;
	frmSSSExecute.lblTrnsRefNo.text = transRefNumForS2S+"00";

	
	gblSSExcuteCnfrm = "dummyJP"; // to control UI of frmSSSExecute screen
	gblExeS2S = "randonXjp";// to control flow
	frmSSSExecute.show();
	dismissLoadingScreen();
	
}

/*
 * Function to complete execution trasfer amount by calling RetailLiquidityTransferAdd service, [screen: SSSExecute]
 */
function invokeRetailLiquidityTransferAdd(){
	showLoadingScreen();
	inputParam = {};
	invokeServiceSecureAsync("RetailLiquidityTransferAdd", inputParam, invokeRetailLiquidityTransferAddCB)
}

function invokeRetailLiquidityTransferAddCB(status, resultTbl){
	
	retailLiqTrnsAdd = resultTbl;
	if (status == 400) //success responce
    {
    	
        
        
        dismissLoadingScreen();
        if (resultTbl["opstatus"] == 0){
			var date1 = resultTbl["TransferDate"];
			var dateSplit1 = date1.split("-");
			var formattedDate = dateSplit1[2] + "/" + dateSplit1[1] + "/" + dateSplit1[0].substring(dateSplit1[0].length - 2, dateSplit1[0].length);
			
			frmSSSExecute.lblFromAccBalAftrMsg.text = kony.i18n.getLocalizedString("S2S_BalAfterTrans") + ": ";
			frmSSSExecute.lblFromAccBalAftr.text = commaFormatted(resultTbl["FromAmt"]);// + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			frmSSSExecute.lblSendToAccBal.text = commaFormatted(resultTbl["ToAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			frmSSSExecute.lblTransDate.text = formattedDate;//13/09/13, 2013-09-13
			frmSSSExecute.lblTrnsRefNo.text = resultTbl["TrnId"];
						

			if(resultTbl["TransferStatus"] == "S"){
			
				popupTractPwd.dismiss();	//to dismiss popup

				s2s_ActivityStatus = "01";
				callApplyS2SActivityLogForS2SExecutionComplete(s2s_ActivityStatus);
				
				
				gblSSExcuteCnfrm = "transferS2Ssuccess"; //to control UI only
				
				
				frmSSSExecute.lblHdrTxt.text = kony.i18n.getLocalizedString("Complete");
				frmSSSExecute.btnRight.setVisibility(true);
				frmSSSExecute.hbxFromAccBalAftr.setVisibility(true);
				frmSSSExecute.hbxConfrmCancl.setVisibility(false);
				frmSSSExecute.hbxCmptReturn.setVisibility(true);
				frmSSSExecute.hboxSaveCamEmail.setVisibility(false);
				
				frmSSSExecute.show();
				
							
				createFinActivityLogObj(); //Financial activity logging
			}
			else {
						
				s2s_ActivityStatus = "02";
				callApplyS2SActivityLogForS2SExecutionComplete(s2s_ActivityStatus);
				showAlert("Transaction not successful", kony.i18n.getLocalizedString("info"));
				dismissLoadingScreen();
				
				return false;
			}	
		}else{  
    		 showAlert(kony.i18n.getLocalizedString("ECGenericError"),kony.i18n.getLocalizedString("info"));
	 	  	 dismissLoadingScreen();
			 return false;  
		}	      
	}
    
}
	
	
function onClickCallBackFunctionMB() {
	frmMyAccountList.show();
}

/*
 * function to save screen as pdf
 */

/*
 * function to save S2S transfer execution complete screen
 */



/*
 * activity log for s2s apply complete
 */

function callApplyS2SActivityLogForS2SActiComplete(s2sActivityStatus) {
var txtBalMaxVal =frmSSConfirmation.lblAmntMax.text
     txtBalMaxVal = txtBalMaxVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
    var txtBalMinVal = frmSSConfirmation.lblAmntMin.text
     txtBalMinVal = txtBalMinVal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
	
	s2s_activityTypeID = "035";
    s2s_errorCode = "";
    s2s_activityStatus = s2sActivityStatus;
    s2s_deviceNickName = "";
    s2s_activityFlexValues1 = gblPHONENUMBER;
    s2s_activityFlexValues2 = removeUnwatedSymbols(frmSSConfirmation.lblLinkAccNo.text);
	s2s_activityFlexValues3 = removeUnwatedSymbols(frmSSConfirmation.lblNFAccNo.text);
	s2s_activityFlexValues4 = removeCommas(txtBalMaxVal);
	s2s_activityFlexValues5 = removeCommas(txtBalMinVal);		
    s2s_logLinkageId = "";
    
    activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
}



/*
 * activity log for s2s edit flow
 */

function callApplyS2SActivityLogForEdit(s2sActivityStatus) {
	
	s2s_activityTypeID = "037";
    s2s_errorCode = "";
    s2s_activityStatus = s2sActivityStatus;
    s2s_deviceNickName = "";
    s2s_activityFlexValues1 = gblPHONENUMBER;
    s2s_activityFlexValues2 = removeUnwatedSymbols(frmSSService.lblFromAccNo.text);
	s2s_activityFlexValues3 = removeUnwatedSymbols(frmSSService.lblNFAccNo.text);
	s2s_activityFlexValues4 = removeCommas(frmSSService.txtBalMax.text);
	s2s_activityFlexValues5 = removeCommas(frmSSService.txtBalMin.text);		
    s2s_logLinkageId = "";
    
    activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
}




/*
 * activity log for s2s transfer execution complete
 */
function callApplyS2SActivityLogForS2SExecutionInitial(s2sActivityStatus) {

	
	s2s_activityTypeID = "038";
    s2s_errorCode = "";
    s2s_activityStatus = s2sActivityStatus;
    s2s_deviceNickName = "";
    s2s_activityFlexValues1 = gblPHONENUMBER;
    s2s_activityFlexValues2 = removeUnwatedSymbols(frmSSSExecute.lblSendFromAccNo.text);
	s2s_activityFlexValues3 = removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
	s2s_activityFlexValues4 = removeCommas(frmSSSExecute.lblAmntLmtMax.text);
	s2s_activityFlexValues5 = removeCommas(frmSSSExecute.lblAmntLmtMin.text);		
    s2s_logLinkageId = "";
    
    activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
}

function callApplyS2SActivityLogForS2SExecutionComplete(s2sActivityStatus) {

	
	s2s_activityTypeID = "038";
    s2s_errorCode = "";
    s2s_activityStatus = s2sActivityStatus;
    s2s_deviceNickName = "";
    s2s_activityFlexValues1 = gblPHONENUMBER;
    s2s_activityFlexValues2 = removeUnwatedSymbols(frmSSSExecute.lblSendFromAccNo.text);
	s2s_activityFlexValues3 = removeUnwatedSymbols(frmSSSExecute.lblSendToAccNo.text);
	s2s_activityFlexValues4 = removeCommas(frmSSSExecute.lblAmntLmtMax.text);
	s2s_activityFlexValues5 = removeCommas(frmSSSExecute.lblAmntLmtMin.text);		
    s2s_logLinkageId = "";
    
    activityLogServiceCall(s2s_activityTypeID, s2s_errorCode, s2s_activityStatus, s2s_deviceNickName, s2s_activityFlexValues1, s2s_activityFlexValues2, s2s_activityFlexValues3, s2s_activityFlexValues4, s2s_activityFlexValues5, s2s_logLinkageId);
}
function changeLocaleforExecuteDetail(){
 if(collectionDataExecute !=null || collectionDataExecute !='')
 var locale = kony.i18n.getCurrentLocale();
 if(locale=="en_US"){
     
	 if(collectionDataExecute["lblExeFromAccNameEN"]==null|| collectionDataExecute["lblExeFromAccNameEN"]=='' )
	 frmSSSExecute.lblSendFromAccName.text = collectionDataExecute["lblExeFromAccName"];
	 else
	 frmSSSExecute.lblSendFromAccName.text = collectionDataExecute["lblExeFromAccNameEN"];
	 if(collectionDataExecute["lblExeFromAccTypeEN"]==null|| collectionDataExecute["lblExeFromAccTypeEN"]=='' )
	 frmSSSExecute.lblSendFromAccType.text = collectionDataExecute["lblExeFromAccType"];
	 else
	 frmSSSExecute.lblSendFromAccType.text = collectionDataExecute["lblExeFromAccTypeEN"];
	 if(collectionDataExecute["lblExeToAccNameEN"]==null|| collectionDataExecute["lblExeToAccNameEN"]=='' )
	 frmSSSExecute.lblSendToAccName.text=collectionDataExecute["lblExeToAccName"];
	 else
	 frmSSSExecute.lblSendToAccName.text=collectionDataExecute["lblExeToAccNameEN"]
	 if(collectionDataExecute["lblFromAccTypeEN"]==null|| collectionDataExecute["lblFromAccTypeEN"]=='' )
	 frmSSSExecute.lblSendToAccType.text=collectionDataExecute["lblFromAccType"]
	 else
	 frmSSSExecute.lblSendToAccType.text=collectionDataExecute["lblFromAccTypeEN"];
	 
 }
 else{
	 if(collectionDataExecute["lblExeFromAccNameTH"]==null|| collectionDataExecute["lblExeFromAccNameTH"]=='' )
	 frmSSSExecute.lblSendFromAccName.text = collectionDataExecute["lblExeFromAccName"];
	 else
	 frmSSSExecute.lblSendFromAccName.text= collectionDataExecute["lblExeFromAccNameTH"];
	 if(collectionDataExecute["lblExeFromAccTypeTH"]==null|| collectionDataExecute["lblExeFromAccTypeTH"]=='' )
	 frmSSSExecute.lblSendFromAccType.text = collectionDataExecute["lblExeFromAccType"];
	 else
	 frmSSSExecute.lblSendFromAccType.text = collectionDataExecute["lblExeFromAccTypeTH"];
	 if(collectionDataExecute["lblExeToAccNameTH"]==null|| collectionDataExecute["lblExeToAccNameTH"]=='' )
	 frmSSSExecute.lblSendToAccName.text=collectionDataExecute["lblExeToAccName"];
	 else
	 frmSSSExecute.lblSendToAccName.text=collectionDataExecute["lblExeToAccNameTH"]
	 if(collectionDataExecute["lblFromAccTypeTH"]==null|| collectionDataExecute["lblFromAccTypeTH"]=='' )
	  frmSSSExecute.lblSendToAccType.text=collectionDataExecute["lblFromAccType"]
	 else
	 frmSSSExecute.lblSendToAccType.text=collectionDataExecute["lblFromAccTypeTH"];
 
 }
}


