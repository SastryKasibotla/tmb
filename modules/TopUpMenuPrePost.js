function frmMyTopUpCompleteMenuPreshow() {
	if(gblCallPrePost)
	{
		frmMyTopUpComplete.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    ehFrmMyTopUpComplete_frmMyTopUpComplete_preshow.call(this);
	    DisableFadingEdges.call(this, frmMyTopUpComplete);
	}
}

function frmMyTopUpCompleteMenuPostshow() {
	if(gblCallPrePost)
	{
		campaginService.call(this, "image2447443295186", "frmMyTopUpComplete", "M");
	    if (gblMyBillerTopUpBB == 0) {
	        campaginService.call(this, "image2447443295186", "frmMyBillerAddComplete", "M");
	    } else {
	        campaginService.call(this, "imgTwo", "frmMyTopUpAddComplete", "M");
	    }
	}
	assignGlobalForMenuPostshow();
}

function frmMyTopUpCompleteCalendarMenuPreshow() {
	if(gblCallPrePost)
	{
		DisableFadingEdges.call(this, frmMyTopUpCompleteCalendar);
	}
}

function frmMyTopUpCompleteCalendarMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMyTopUpEditScreensMenuPreshow() {
	if(gblCallPrePost)
	{
		frmMyTopUpEditScreens.txtEditName.skin = txtNormalBG;
	    frmMyTopUpEditScreens.txtEditName.focusSkin = txtNormalBG;
	    frmMyTopUpEditScreens.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    if (gblMyBillerTopUpBB == 0) {
	        frmMyTopUpEditScreens.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyMyBillMB')
	    } else {
	        frmMyTopUpEditScreens.lblHdrTxt.text = kony.i18n.getLocalizedString('myTopUpMB')
	    }
	    frmMyTopUpEditScreensPreshow.call(this);
	    DisableFadingEdges.call(this, frmMyTopUpEditScreens);
	}
}

function frmMyTopUpEditScreensMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmMyTopUpListMenuPreshow() {
	if(gblCallPrePost)
	{
		frmMyTopUpList.txbSearch.text = "";
	    frmMyTopUpList.lblNoBill.setVisibility(false);
	    frmMyTopUpList.lblErrorMsg.setVisibility(false);
	    if (gblMyBillerTopUpBB == 2) {
	        frmBBListPreshow.call(this);
	        frmMyTopUpList.scrollboxMain.scrollToEnd();
	        frmMyTopUpList.txbSearch.text = "";
	        gblIndex = -1;
	        isMenuShown = false;
	        isSignedUser = true;
	        //frmMyTopUpList.lblSuggestedBillersText.setVisibility(false);
	        frmMyTopUpList.lblMyBillers.setVisibility(true);
	        frmMyTopUpList.lblSuggestedBillersText.setVisibility(true)
	        frmMyTopUpList.hbxMore.setVisibility(false);
	        frmMyTopUpList.hbxMoreBB.setVisibility(true);
	        frmMyTopUpList.hbxSugMoreBB.setVisibility(true);
	        gblTopUpMore = 0;
	        gblsearchtxt = "";
	    } else {
	        if (gblMyBillerTopUpBB == 0) {
	            frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyMyBillsMB')
	            frmMyTopUpList.lblMyBillers.text = kony.i18n.getLocalizedString('KeyMyBillsMB')
	            frmMyTopUpList.lblNoBill.text = kony.i18n.getLocalizedString('keyaddbillerstolist');
	        } else if (gblMyBillerTopUpBB == 1) {
	            frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('myTopUpsMB')
	            frmMyTopUpList.lblMyBillers.text = kony.i18n.getLocalizedString('myTopUpsMB')
	            frmMyTopUpList.lblNoBill.text = kony.i18n.getLocalizedString('keyNoTopUps');
	        } else if (gblMyBillerTopUpBB == 2) {
	            frmMyTopUpList.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBB')
	            frmMyTopUpList.lblMyBillers.text = kony.i18n.getLocalizedString('KeyMyBillsMB')
	        }
	        frmMyTopUpList.hbxMoreBB.setVisibility(false);
	        frmMyTopUpList.hbxSugMoreBB.setVisibility(false);
	        frmMyTopUpListPreShow.call(this);
	        frmMyTopUpList.scrollboxMain.scrollToEnd();
	        frmMyTopUpList.txbSearch.text = "";
	        gblIndex = -1;
	        isMenuShown = false;
	        isSignedUser = true;
	        isCatSelected = false;
			searchFlagForLoadMore = false;
	        frmMyTopUpList.segSuggestedBillers.setVisibility(false);
	        frmMyTopUpList.hbxMore.setVisibility(false);
	        frmMyTopUpList.lblSuggestedBillersText.setVisibility(false);
	        gblTopUpMore = 0;
	        gblsearchtxt = "";
	        currentCatSelected = 0;
	        /* 
	setDataOnsegOnSuggestedBiller.call(this);
	
	 */
	    }
	    DisableFadingEdges.call(this, frmMyTopUpList);
	}
}

function frmMyTopUpListMenuPostshow() {
  	if(gblCallPrePost)
	{
		   	if (gblMyBillerTopUpBB == 2) {
	        
	        if (gblBBorBillers == true) {
	            if (gblCustomerBBMBInqRs.length == 0) {
	                frmMyTopUpList.lblMyBillers.setVisibility(false);
	                frmMyTopUpList.hbxMoreBB.setVisibility(false);
	                frmMyTopUpList.lblNoBill.setVisibility(true);
	            } else {
	                frmMyTopUpList.lblMyBillers.setVisibility(true);
	                frmMyTopUpList.hbxMoreBB.setVisibility(true);
	                frmMyTopUpList.lblNoBill.setVisibility(false);
	            }
	        } else {
	            if (gblCustomerBillListBB.length == 0) {
	                frmMyTopUpList.lblMyBillers.setVisibility(false);
	                frmMyTopUpList.hbxMoreBB.setVisibility(false);
	                frmMyTopUpList.lblNoBill.setVisibility(true);
	            } else if (gblCustomerBillListBB.length > GLOBAL_LOAD_MORE) {
	                frmMyTopUpList.lblMyBillers.setVisibility(true);
	                frmMyTopUpList.hbxMoreBB.setVisibility(true);
	                frmMyTopUpList.lblNoBill.setVisibility(false);
	            } else {
	                frmMyTopUpList.lblMyBillers.setVisibility(true);
	                frmMyTopUpList.hbxMoreBB.setVisibility(false);
	                frmMyTopUpList.lblNoBill.setVisibility(false);
	            }
	        }
	        gblSugBBMoreMB = 0;
	        onMyBBSelectMoreDynamicMB();
	    } else {
	        addAnotherValidValue = 0;
	        startMBBillTopUpProfileInqService();
	        if (gblMyBillerTopUpBB == 2) {
	            inquireCustomerBeepAndBillList();
	        } else {
	            //getMyTopUpListMB();
	            getMyTopUpSuggestListMB();
	            if (!isfirstCallToMaster) {
	                getMyTopUpListMB();
	            }
	        }
	    }
	}
	assignGlobalForMenuPostshow();
}

function frmMyTopUpSelectMenuPreshow() {
	if(gblCallPrePost)
	{
		 frmMyTopUpSelect.txbSearch.text = "";
	    frmMyTopUpSelect.lblErrorMsg.setVisibility(false);
	    frmMyTopUpSelect.txbSearch.placeholder = kony.i18n.getLocalizedString("keySearch");
	    var tmpBillerCatGroupType = "";
      	kony.print("gblMyBillerTopUpBB >> "+gblMyBillerTopUpBB);
	    if (gblMyBillerTopUpBB == 0) {
	        tmpBillerCatGroupType = gblBillerCategoryGroupType;
	    } else if (gblMyBillerTopUpBB == 1) {
	        tmpBillerCatGroupType = gblTopupCategoryGroupType;
	    } else if (gblMyBillerTopUpBB == 2) {
	        tmpBillerCatGroupType = gblBillerAndTopUpCategoryGroupType;
	    }
	    kony.print("@@@Calling Biller Inq Service with tmpBillerCatGroupType = " + tmpBillerCatGroupType);
	    var inputParams = {
	        BillerCategoryGroupType: tmpBillerCatGroupType
	    };
	    invokeServiceSecureAsync("billerCategoryInquiry", inputParams, displayAllBillerCategoryServiceCallback);
	    if (gblMyBillerTopUpBB == 2) {
	        frmMyTopUpSelect.scrollboxMain.scrollToEnd();
	        gblTopUpMore = 0;
	        gblsearchtxt = "";
	        gblIndex = -1;
	        isMenuShown = false;
	        isSignedUser = true;
	        frmMyTopUpSelect.txbSearch.text = "";
	        frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBB')
	        frmBBSelectBillPreshow.call(this);
	        /* 
	setDataOnSegSelectBB.call(this);
	
	 */
	    } else {
	        frmMyTopUpSelect.scrollboxMain.scrollToEnd();
	        gblTopUpMore = 0;
	        gblsearchtxt = "";
	        gblTopUpfirstTime = true;
	        isCatSelected = false;
	        gblIndex = -1;
	        isMenuShown = false;
	        isSignedUser = true;
	        frmMyTopUpSelect.txbSearch.text = "";
	        if (gblMyBillerTopUpBB == 0) {
	            frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('keyBillPaymentSelectBill')
	        } else if (gblMyBillerTopUpBB == 1) {
	            frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('SelectTopUp')
	        } else {
	            frmMyTopUpSelect.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBB')
	        }
	        frmMyTopUpSelectPreshow.call(this);
	        /* 
	setDataOnsegSelectTopUp.call(this);
	
	 */
	    }
	    DisableFadingEdges.call(this, frmMyTopUpSelect);
	}
}

function frmMyTopUpSelectMenuPostshow() {
	if(gblCallPrePost)
	{
		if (gblMyBillerTopUpBB == 2) {
	        setDataOnsegSelectTopUpFromService.call(this);
	    } else {
	        setDataOnsegSelectTopUpFromService.call(this);
	    }
	}
	assignGlobalForMenuPostshow();
}

function frmAddTopUpBillerMenuPreshow() {
	if(gblCallPrePost)
	{
	   	frmAddTopUpBiller.textbox21011640310171357.text = "";
	    frmAddTopUpBiller.textbox21011640310171358.text = "";
	    frmAddTopUpBiller.textbox244741353089006.text = "";
	    if (gblLocale == true) {
	        frmAddTopUpBillerPreShow.call(this);
	    } else {
	        frmAddTopUpBiller.scrollboxMain.scrollToEnd();
	        gblIndex = -1;
	        isMenuRendered = false;
	        isMenuShown = false;
	        isSignedUser = true;
	        if (gblMyBillerTopUpBB == 0) {
	            frmAddTopUpBiller.hbxRef2.isVisible = true;
	            frmAddTopUpBiller.lineref2.isVisible = true;
	            frmAddTopUpBiller.hbxSegMain.isVisible = false;
	            frmAddTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('keyAddBillMB')
	        } else if (gblMyBillerTopUpBB == 1) {
	            frmAddTopUpBiller.hbxRef2.isVisible = false;
	            frmAddTopUpBiller.lineref2.isVisible = false;
	            frmAddTopUpBiller.hbxSegMain.isVisible = false;
	            frmAddTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('addtopUpMB')
	        } else {
	            frmAddTopUpBiller.hbxRef2.isVisible = true;
	            frmAddTopUpBiller.lineref2.isVisible = true;
	            frmAddTopUpBiller.hbxSegMain.isVisible = true;
	            frmAddTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBBPayment')
	        }
	    }
	    DisableFadingEdges.call(this, frmAddTopUpBiller);
	}
}

function frmAddTopUpBillerMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmAddTopUpBillerconfrmtnMenuPreshow() {
	if(gblCallPrePost)
	{
		frmAddTopUpBillerconfrmtn.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmAddTopUpBillerconfrmtnPreshow.call(this);
	    DisableFadingEdges.call(this, frmAddTopUpBillerconfrmtn);
	}
}

function frmAddTopUpBillerconfrmtnMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmAddTopUpToMBMenuPreshow() {
	if(gblCallPrePost)
	{
			frmAddTopUpToMB.txbNickName.skin = txtNormalBG;
		    frmAddTopUpToMB.txbNickName.focusSkin = txtFocusBG;
		    frmAddTopUpToMB.txtRef1.skin = txtNormalBG;
		    frmAddTopUpToMB.txtRef1.focusSkin = txtFocusBG;
		    if (gblMyBillerTopUpBB == 2) {
		       
		        frmBBPaymentApplyPreshow.call(this);
		        frmAddTopUpToMB.scrollboxMain.scrollToEnd();
		        gblIndex = -1;
		        isMenuRendered = false;
		        isMenuShown = false;
		        isSignedUser = true;
		        //frmAddTopUpToMB.hbxref2.isVisible = true;
		        //frmAddTopUpToMB.lineRef2.isVisible = true;
		         frmAddTopUpToMB.hbxSegMain.isVisible = true;
		        //frmAddTopUpToMB.btnShow.skin = btnSearch;
		        frmAddTopUpToMB.lblChargeTo.setVisibility(true);
		        frmAddTopUpToMB.btnShow.skin = btnFindSearch;
		        frmAddTopUpToMB.btnShow.focusskin = btnFindSearch;
		        //callBillPaymentCustomerBBAccountService();
		        //setRowTemplate(frmBBPaymentApply);
		    } else {
		        frmAddTopUpToMB.txbNickName.text = "";
		        frmAddTopUpToMB.txtRef1.text = "";
		        frmAddTopUpToMB.txtRef2.text = "";
		        frmAddTopUpToMB.btnRef2Dropdown.setVisibility(false);
		        frmAddTopUpToMB.btnShow.skin = btnSelectBiller;
		        frmAddTopUpToMB.btnShow.focusSkin = btnSelectBiller;
		        if (gblLocale == true) {
		            frmAddTopUpToMBPreShow.call(this);
		        } else {
		            frmAddTopUpToMB.scrollboxMain.scrollToEnd();
		            gblIndex = -1;
		            isMenuRendered = false;
		            isMenuShown = false;
		            isSignedUser = true;
		            if (gblMyBillerTopUpBB == 0) {
		                // frmAddTopUpToMB.hbxref2.isVisible = true;
		                // frmAddTopUpToMB.lineRef2.isVisible = true;
		                frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyAddBillMB')
		           
                        setRowTemplate(frmAddTopUpToMB);
                        frmAddTopUpToMB.hbxSegMain.isVisible = false;
		                frmAddTopUpToMB.lblChargeTo.setVisibility(false);
		                frmAddTopUpToMB.btnShow.skin = btnSelectBiller;
		            } else if (gblMyBillerTopUpBB == 1) {
		                frmAddTopUpToMB.hbxref2.isVisible = false;
		                frmAddTopUpToMB.lineRef2.isVisible = false;
		                frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('addtopUpMB')
		              
		                    setRowTemplate(frmAddTopUpToMB);
		                    frmAddTopUpToMB.hbxSegMain.isVisible = false;
		                frmAddTopUpToMB.lblChargeTo.setVisibility(false);
		                frmAddTopUpToMB.btnShow.skin = btnSelectBiller;
		            } else if (gblMyBillerTopUpBB == 2) {
		                frmAddTopUpToMB.hbxref2.isVisible = true;
		                frmAddTopUpToMB.lineRef2.isVisible = true;
		                frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyMyBBPayment')
		                frmAddTopUpToMB.hbxSegMain.isVisible = true;
		                callBillPaymentCustomerBBAccountService();
		                //setRowTemplate(frmAddTopUpToMB);
		            }
		        }
		    }
		    if (gblMyBillerTopUpBB == 0) {
		        frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyAddBillMB');
		    } else if (gblMyBillerTopUpBB == 1) {
		        frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('addtopUpMB');
		    } else if (gblMyBillerTopUpBB == 2) {
		        frmAddTopUpToMB.lblHdrTxt.text = kony.i18n.getLocalizedString('keyBBApplyPayment');
		    }
		    DisableFadingEdges.call(this, frmAddTopUpToMB);
	}
}

function frmAddTopUpToMBMenuPostshow() {
	if(gblCallPrePost)
	{
		if (gblMyBillerTopUpBB == 2) {
	        populateOnFrmIBBeepAndBillListMB.call(this);
	    }
	}
	assignGlobalForMenuPostshow();
}

function frmTopUpMenuPreshow() {
  	if (gblPlatformName == "iPhone"  || gblDeviceInfo["name"] == "iPad"|| gblPlatformName == "iPhone Simulator" ){
      frmTopUp.segSlider.viewConfig = {coverflowConfig: {isCircular: true}};
    }
	if(gblCallPrePost)
	{
		frmTopUp.tbxMyNoteValue.numberOfVisibleLines = 1
	    frmTopUp.scrollboxMain.scrollToEnd();
	    //frmTopUpPreShow.call(this);
	    isMenuShown = false;
	    isSignedUser = true;
	    gblMyBillerTopUpBB = 1;
	    GblBillTopFlag = false;
	    if (gblFirstTimeTopUp) {
          	frmTopUpPreShow.call(this);
	        launchTopUpFirstTime.call(this);
	    }
	    if (gblPaynow) {
	        currentSystemDate.call(this);
	        frmTopUp.lblPayBillOnValue.text = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
	    }
	    DisableFadingEdges.call(this, frmTopUp);
	}
}

function frmTopUpMenuPostshow() {
	if(gblCallPrePost)
	{
		 autoFocusAfterSelectTopUp.call(this);
	}
	assignGlobalForMenuPostshow();
}

function frmTopUpAmountMenuPreshow() {
	if(gblCallPrePost)
	{
		frmTopUpAmount.lblHdrTxt.text = kony.i18n.getLocalizedString("MIB_TUAmount");
	}
}

function frmTopUpAmountMenuPostshow() {
	assignGlobalForMenuPostshow();
}

function frmViewTopUpBillerMenuPreshow() {
	if(gblCallPrePost)
	{
		frmViewTopUpBiller.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    if (gblMyBillerTopUpBB == 0) {
	        
	        frmViewTopUpBiller.hbxLinkBeepandBill.isVisible = false;
	        frmViewTopUpBiller.lineLast.isVisible = true;
	        frmViewTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('KeyMyBillMB')
	    } else {
	        frmViewTopUpBiller.hbxLinkBeepandBill.isVisible = false;
	        frmViewTopUpBiller.lineLast.isVisible = false;
	        frmViewTopUpBiller.lblHdrTxt.text = kony.i18n.getLocalizedString('myTopUpMB')
	    }
	  
	    DisableFadingEdges.call(this, frmViewTopUpBiller);
	}
}

function frmViewTopUpBillerMenuPostshow() {
	assignGlobalForMenuPostshow();
}
