var maskedAccntNum = "";
var unmaskedAccntNum = "";
var shareMessage = "";
var settingVisibility = false;
var hbxadv = false;
var isAddToMyBillsDisplay = false;
var iphonemodule = "";
formSkinShare = "";
function hideHeaderFooter() {
    var frmName = kony.application.getCurrentForm();
    try {
        if (frmName.id != "frmQRPaymentSuccess") {
            kony.application.getCurrentForm().statusBarColor = "FFFFFF00";
        }
    } catch (e) {
        // todo: handle exception
    }
    var frmName = kony.application.getCurrentForm();
    if (gblDeviceInfo["name"] == "android") {
        //frmBlank.show();
        //frmName.show();
    }
	 formSkinShare = frmName.skin;
	 frmName.skin = "frmFullWhite"
    if (frmName.id == "frmTransfersAck") {
        frmName.lblSucessScreenshot.setVisibility(true);
        unmaskedAccntNum = frmName.lblTransNPbAckFrmNum.text;
        maskedAccuntNum = "xxx-x-" + frmName.lblTransNPbAckFrmNum.text.substring(6, 11) + "-x";
        frmName.lblTransNPbAckFrmNum.text = maskedAccuntNum;
        frmName.hbxHeaderConfirmation.setVisibility(false);
        frmName.hbxConfirmCancel.setVisibility(false);
        frmName.LineSavePopup.setVisibility(false);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxFromToAcct2.setVisibility(false);
      	kony.print(">>>>>>here in true sender note");
      	//frmTransfersAck.hbxSenderNotes.setVisibility(true);
      	if(isNotBlank(frmTransferLanding.textRecNoteEmail.text)){
              kony.print(">>>>>>inside data in sender notes999>>>>");
      		frmTransfersAck.lblConfirmSendersNotes.text=frmTransferLanding.textRecNoteEmail.text;//vvvv
              frmTransfersAck.hbxSenderNotes.setVisibility(true);
            }else{
              kony.print(">>>>>>no data in sender notes>>>>");
             frmTransfersAck.hbxSenderNotes.setVisibility(false);
            }
        if (frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length >= 0) {
            if (frmName.lblAllSuccess.text == "0") {
                frmName.segTransAckSplit.setVisibility(false);
                frmName.lblHideMore.skin = "lblGreyLightmask";
                frmName.imgMore.setVisibility(false);
            } else {
                frmName.segTransAckSplit.setVisibility(false);
                if (frmName.id == "frmTransfersAck") {
                    frmTransfersAck.lblCnfmToRefTitle2.skin = "lblGreyLightmask";
                } else {
                    frmTransfersAckCalendar.lblCnfmToRefTitle1.skin = "lblGreyLightmask";
                }
                frmName.lblHideMore.skin = "lblGreyLightmask";
                frmName.imgMore.setVisibility(false);
            }
        }
        frmName.hbxAdv.setVisibility(false);
    } else if (frmName.id == "frmTransfersAckCalendar") {
        frmName.lblSucessScreenshot.setVisibility(true);
        unmaskedAccntNum = frmName.lblTransNPbAckFrmNum.text;
        maskedAccuntNum = "xxx-x-" + frmName.lblTransNPbAckFrmNum.text.substring(6, 11) + "-x";
        frmName.lblTransNPbAckFrmNum.text = maskedAccuntNum;
        frmName.hbxHeaderConfirmation.setVisibility(false);
      	if(isNotBlank(frmTransfersAckCalendar.lblConfirmSendersNotes.text)){
              kony.print(">>>>>>inside data in sender notes123>>>>"+frmTransfersAckCalendar.lblConfirmSendersNotes.text);
      		//frmTransfersAckCalendar.lblConfirmSendersNotes.text=frmTransfersAckCalendar.lblConfirmSendersNotes.text;//vvvv
              frmTransfersAckCalendar.hbxSenderNotes.setVisibility(true);
            }else{
              kony.print(">>>>>>no data in sender notes>>>>");
             frmTransfersAckCalendar.hbxSenderNotes.setVisibility(false);
            }
        if (frmName.lblhidden.text == "QR") {
            frmName.hbxConfirmCancel.setVisibility(false);
            frmName.hbxqrback.setVisibility(false);
            // This code is to show QR transaction image for QR payments in calendar share
            frmTransfersAckCalendar.hboxQRTransaction.isVisible = true;
            frmTransfersAckCalendar.hbxPart3.isVisible = false;
          
            frmTransfersAckCalendar.lblTransNPbAckRefDesQR.text = frmTransfersAckCalendar.lblTransNPbAckRefDes.text;
            frmTransfersAckCalendar.lblSmartDateValQR.text = frmTransfersAckCalendar.lblSmartDateVal.text;
        } else {
            frmTransfersAckCalendar.hboxQRTransaction.isVisible = false;
            frmTransfersAckCalendar.hbxPart3.isVisible = true;
           
            frmName.hbxConfirmCancel.setVisibility(false);
            frmName.hbxqrback.setVisibility(false);
        }
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxFromToAcct2.setVisibility(false);
        if (frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length >= 0) {
            if (frmName.lblAllSuccess.text == "0") {
                frmName.segTransAckSplit.setVisibility(false);
                frmName.lblHideMore.skin = "lblGreyLightmask";
                frmName.imgMore.setVisibility(false);
            } else {
                frmName.segTransAckSplit.setVisibility(false);
                if (frmName.id == "frmTransfersAck") {
                    frmTransfersAck.lblCnfmToRefTitle2.skin = "lblGreyLightmask";
                } else {
                    frmTransfersAckCalendar.lblCnfmToRefTitle1.skin = "lblGreyLightmask";
                }
                frmName.lblHideMore.skin = "lblGreyLightmask";
                frmName.imgMore.setVisibility(false);
            }
        }
        frmName.hbxAdv.setVisibility(false);
    } else if (frmName.id == "frmMyQRCode") {
        frmName.FlexScrollContainer0bdfd74500f3340.isVisible = false
        frmName.flexScrollboxShare.isVisible = true
        /*	frmName.flxHeader.isVisible = false;
          frmName.flexFooter.isVisible = false;
        	//frmName.flexShare.isVisible = false;
          frmName.lblBodyHeader.isVisible = false;
          //frmName.flexQRImages.top = "20%" */
    } else if (frmName.id == "frmMBFTEditCmplete") {

        frmName.lblSucessScreenshot.setVisibility(true);
        unmaskedAccntNum = frmName.lblFrmAccntNum.text;
        maskedAccuntNum = "xxx-x-" + frmName.lblFrmAccntNum.text.substring(6, 11) + "-x";
        frmName.lblFrmAccntNum.text = maskedAccuntNum;
        frmName.hbxHeaderConfirmation.setVisibility(false);
        frmName.hbxConfirmCancel.setVisibility(false);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxFromToAcct2.setVisibility(false);
        frmName.hbxShareOption.setVisibility(false);
        /*if(frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length>=0)
        {
        	if(frmName.lblAllSuccess.text=="0")
        	{
        		frmName.segTransAckSplit.setVisibility(false);
        		frmName.lblHideMore.skin="lblGreyLightmask";
        		frmName.imgMore.setVisibility(false);
        	}
        	else
        	{
        		frmName.segTransAckSplit.setVisibility(false);
        		if(frmName.id=="frmTransfersAck")
        		{
        			frmTransfersAck.lblCnfmToRefTitle2.skin="lblGreyLightmask";
        		}
        		else
        		{
        			frmTransfersAckCalendar.lblCnfmToRefTitle1.skin="lblGreyLightmask";
        		}
        		frmName.lblHideMore.skin="lblGreyLightmask";
        		frmName.imgMore.setVisibility(false);
        	}
        }*/
        //frmName.hbxAdv.setVisibility(false);

    } else if (frmName.id == "frmBillPaymentComplete" || frmName.id == "frmBillPaymentEditFutureComplete") {
        unmaskedAccntNum = frmName.lblAccountNum.text;
        maskedAccuntNum = "xxx-x-" + frmName.lblAccountNum.text.substring(6, 11) + "-x";
        frmName.lblAccountNum.text = maskedAccuntNum;
        frmName.hbxHeaderConfirmation.setVisibility(false);
        //frmName.line968116430627578.setVisibility(false);
        frmName.lblSucessScreenshot.setVisibility(true);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxConfirmCancel.setVisibility(false);

        frmName.hbxOAFrmDetAck.setVisibility(false);
        if (frmName.hbxAddToMyBills.isVisible) {
            isAddToMyBillsDisplay = true;
        } else {
            isAddToMyBillsDisplay = false;
        }
        frmName.hbxAddToMyBills.setVisibility(false);
        if (frmName.hbxAdv.isVisible) {
            hbxadv = true;
            frmName.hbxAdv.setVisibility(false);
            frmName.hbxCommon.setVisibility(false);
        } else {
            hbxadv = false;
            frmName.hbxAdv.setVisibility(false);
            frmName.hbxCommon.setVisibility(false);
        }
    } else if (frmName.id == "frmBillPaymentCompleteCalendar") {
        unmaskedAccntNum = frmName.lblAccountNum.text;
        maskedAccuntNum = "xxx-x-" + frmName.lblAccountNum.text.substring(6, 11) + "-x";
        frmName.lblAccountNum.text = maskedAccuntNum;
        frmName.hbxHeaderConfirmation.setVisibility(false);
        //frmName.line968116430627578.setVisibility(false);
        frmName.lblSucessScreenshot.setVisibility(true);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxbpconfcancel.setVisibility(false);

        frmName.hbxOAFrmDetAck.setVisibility(false);

        if (frmName.hbox50285458168.isVisible) {
            hbxadv = true;
            frmName.hbox50285458168.setVisibility(false);
        } else {
            hbxadv = false;
            frmName.hbox50285458168.setVisibility(false);
        }
          if(frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text == "QR"){
          frmBillPaymentCompleteCalendar.vboxNewCompleteDesign.isVisible = true;
            frmBillPaymentCompleteCalendar.vbox4751247744173.isVisible = false;
        }else{
           frmBillPaymentCompleteCalendar.vboxNewCompleteDesign.isVisible = false;
            frmBillPaymentCompleteCalendar.vbox4751247744173.isVisible = true;
        }
    } else if (frmName.id == "frmQRPaymentSuccess") {
        frmQRPaymentSuccess.hbxShareOption.setVisibility(false);
     	frmQRPaymentSuccess.flxHeader.setVisibility(false);  
        frmQRPaymentSuccess.flxFullBody.height = "100%"
        frmQRPaymentSuccess.FlexMain.setVisibility(false);
        frmQRPaymentSuccess.flxFooter.setVisibility(false);
        frmQRPaymentSuccess.FlexscreenShot.setVisibility(true);
        frmQRPaymentSuccess.lblSucessScreenshot.isVisible = true;
    } else if (frmName.id == "frmEDonationPaymentComplete"){
      frmEDonationPaymentComplete.flxSuccessSaveShare.setVisibility(true);
      frmEDonationPaymentComplete.hbxShareOption.setVisibility(false);
      frmEDonationPaymentComplete.flxHeader.setVisibility(false); 
      frmEDonationPaymentComplete.flxFooter.setVisibility(false);
      frmEDonationPaymentComplete.flxBody.height = "100%";
      frmEDonationPaymentComplete.lblAccountNumberValue.setVisibility(false);
      frmEDonationPaymentComplete.lblAccountNumberValueMasked.setVisibility(true);
//       frmEDonationPaymentComplete.lblCitizenIDValue.setVisibility(false);
//       frmEDonationPaymentComplete.lblCitizenIDValueMasked.setVisibility(true);
      frmEDonationPaymentComplete.flxFooterHistoryView.setVisibility(false);  
    }

    //frmName.hbxImageAddRecipient.setVisibility(false);
    frmName.hbxShareOption.setVisibility(false);
}

function displayHeaderFooter() {
    kony.print("displayHeaderFooter above changeStatusBarColor");
    changeStatusBarColor();
    kony.print("displayHeaderFooter after changeStatusBarColor");
    //alert(megaTransfer)
    var frmName = kony.application.getCurrentForm();
	if(formSkinShare != ""){
		frmName.skin = formSkinShare;
        formSkinShare = "";
	}
	
    if (frmName.id == "frmTransfersAck") {
        frmName.lblSucessScreenshot.setVisibility(false);
        frmName.lblTransNPbAckFrmNum.text = unmaskedAccntNum;
        frmName.hbxHeaderConfirmation.setVisibility(true);
        frmName.LineSavePopup.setVisibility(true);
        frmName.hbxConfirmCancel.setVisibility(true);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxFromToAcct2.setVisibility(true);
		kony.print(">>>>>>here in dont display sender note");
      	frmTransfersAck.hbxSenderNotes.setVisibility(false);
        if (frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length >= 0) {
            if (frmName.lblAllSuccess.text == "0") {
                frmName.segTransAckSplit.setVisibility(true);
                //frmName.hbxMoreHide.setVisibility(true);
                //frmName.lblCnfmToRefTitle2.skin="lblGreySmall114";
                frmName.lblHideMore.skin = "lblbluemedium142";
                frmName.imgMore.setVisibility(true);
            } else {
                frmName.segTransAckSplit.setVisibility(true);
                if (frmName.id == "frmTransfersAck") {
                    frmTransfersAck.lblCnfmToRefTitle2.skin = "lblGreySmall114";
                } else {
                    frmTransfersAckCalendar.lblCnfmToRefTitle1.skin = "lblGreySmall114";
                }
                frmName.lblHideMore.skin = "lblbluemedium142";
                frmName.imgMore.setVisibility(true);
            }
        }
        //frmName.hbxAdv.setVisibility(true);
      	var bannerImgs = frmName.imgTransAdv.src;
        if(bannerImgs != null && bannerImgs != "" && bannerImgs != undefined){
          frmName.hbxAdv.setVisibility(true);
        }else{
          frmName.hbxAdv.setVisibility(false);
        }
    } else if (frmName.id == "frmTransfersAckCalendar") {
        frmName.lblSucessScreenshot.setVisibility(false);
        frmName.lblTransNPbAckFrmNum.text = unmaskedAccntNum;
        frmName.hbxHeaderConfirmation.setVisibility(true);
        frmTransfersAckCalendar.hbxSenderNotes.setVisibility(false);
        if (frmName.lblhidden.text == "QR") {
            // This code is to show QR transaction image for QR payments in calendar share
            frmName.hboxQRTransaction.isVisible = false;
            frmName.hbxPart3.isVisible = true;
          
             frmTransfersAckCalendar.lblTransNPbAckRefDesQR.text = ""
            frmTransfersAckCalendar.lblSmartDateValQR.text = ""
          
            frmName.hbxConfirmCancel.setVisibility(false);
            frmName.hbxqrback.setVisibility(true);
        } else {
            frmName.hbxConfirmCancel.setVisibility(true);
            frmName.hbxqrback.setVisibility(false);
          
            // This code is to show QR transaction image for QR payments in calendar share
            frmName.hboxQRTransaction.isVisible = false;
            frmName.hbxPart3.isVisible = true;
        }

        frmName.hbxArrow.setVisibility(false);
        frmName.hbxFromToAcct2.setVisibility(true);

        if (frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length >= 0) {
            if (frmName.lblAllSuccess.text == "0") {
                frmName.segTransAckSplit.setVisibility(true);
                //frmName.hbxMoreHide.setVisibility(true);
                //frmName.lblCnfmToRefTitle2.skin="lblGreySmall114";
                frmName.lblHideMore.skin = "lblbluemedium142";
                frmName.imgMore.setVisibility(true);
            } else {
                frmName.segTransAckSplit.setVisibility(true);
                if (frmName.id == "frmTransfersAck") {
                    frmTransfersAck.lblCnfmToRefTitle2.skin = "lblGreySmall114";
                } else {
                    frmTransfersAckCalendar.lblCnfmToRefTitle1.skin = "lblGreySmall114";
                }
                frmName.lblHideMore.skin = "lblbluemedium142";
                frmName.imgMore.setVisibility(true);
            }
        }
        //frmName.hbxAdv.setVisibility(true);
      	var bannerImgs = frmName.imgTransAdv.src;
        if(bannerImgs != null && bannerImgs != "" && bannerImgs != undefined){
          frmName.hbxAdv.setVisibility(true);
        }else{
          frmName.hbxAdv.setVisibility(false);
        }
    } else if (frmName.id == "frmMyQRCode") {
        frmName.FlexScrollContainer0bdfd74500f3340.isVisible = true
        frmName.flexScrollboxShare.isVisible = false
        /* 	frmName.flxHeader.isVisible = true;
        frmName.flexFooter.isVisible = true;
      	//frmName.flexShare.isVisible = true;
        frmName.lblBodyHeader.isVisible = true;
     // frmName.flexQRImages.top = "0%" */
    } else if (frmName.id == "frmMBFTEditCmplete") {

        frmName.lblSucessScreenshot.setVisibility(false);
        //	unmaskedAccntNum=frmName.lblFrmAccntNum.text;
        //maskedAccuntNum="xxx-x-" + frmName.lblFrmAccntNum.text.substring(6,11) + "-x";
        frmName.lblFrmAccntNum.text = unmaskedAccntNum;
        frmName.hbxHeaderConfirmation.setVisibility(true);
        frmName.hbxConfirmCancel.setVisibility(true);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxFromToAcct2.setVisibility(true);
        /*if(frmName.segTransAckSplit.data != null && frmName.segTransAckSplit.data.length>=0)
        {
        	if(frmName.lblAllSuccess.text=="0")
        	{
        		frmName.segTransAckSplit.setVisibility(false);
        		frmName.lblHideMore.skin="lblGreyLightmask";
        		frmName.imgMore.setVisibility(false);
        	}
        	else
        	{
        		frmName.segTransAckSplit.setVisibility(false);
        		if(frmName.id=="frmTransfersAck")
        		{
        			frmTransfersAck.lblCnfmToRefTitle2.skin="lblGreyLightmask";
        		}
        		else
        		{
        			frmTransfersAckCalendar.lblCnfmToRefTitle1.skin="lblGreyLightmask";
        		}
        		frmName.lblHideMore.skin="lblGreyLightmask";
        		frmName.imgMore.setVisibility(false);
        	}
        }*/
        //frmName.hbxAdv.setVisibility(false);

    } else if (frmName.id == "frmBillPaymentComplete" || frmName.id == "frmBillPaymentEditFutureComplete") {
        frmName.lblAccountNum.text = unmaskedAccntNum;
        frmName.hbxHeaderConfirmation.setVisibility(true);
        //frmName.line968116430627578.setVisibility(true);
        frmName.lblSucessScreenshot.setVisibility(false);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxConfirmCancel.setVisibility(true);
        
		// Hide Balance after when from account is credit card
		if (frmName.lblAccountNum.text.length == 19) {
			frmName.hbxOAFrmDetAck.setVisibility(false);
		} else {
			frmName.hbxOAFrmDetAck.setVisibility(true);
		}
        if (isAddToMyBillsDisplay) {
            frmName.hbxAddToMyBills.setVisibility(true);
        } else {
            frmName.hbxAddToMyBills.setVisibility(false);
        }
        if (hbxadv) {
            frmName.hbxAdv.setVisibility(true);
            frmName.hbxCommon.setVisibility(false);
        } else {
            frmName.hbxCommon.setVisibility(true);
            frmName.hbxAdv.setVisibility(false);
        }
        hbxAdv = false;
      
        if (frmName.id == "frmBillPaymentComplete") {
           	//MIB-15629
          	showPEAMessageForOverDue();
        }
      
    } else if (frmName.id == "frmBillPaymentCompleteCalendar") {

        frmName.lblAccountNum.text = unmaskedAccntNum;
        frmName.hbxHeaderConfirmation.setVisibility(true);
        //frmName.line968116430627578.setVisibility(true);
        frmName.lblSucessScreenshot.setVisibility(false);
        frmName.hbxArrow.setVisibility(false);
        frmName.hbxbpconfcancel.setVisibility(true);
        // Hide Balance after when from account is credit card
		if (frmName.lblAccountNum.text.length == 19) {
			frmName.hbxOAFrmDetAck.setVisibility(false);
		} else {
			frmName.hbxOAFrmDetAck.setVisibility(true);
		}
        if (hbxadv) {
            frmName.hbox50285458168.setVisibility(true);
        } else {
            frmName.hbox50285458168.setVisibility(false);
        }
        if(frmBillPaymentCompleteCalendar.lblHiddenQRFlowCheck.text == "QR"){
          frmBillPaymentCompleteCalendar.vboxNewCompleteDesign.isVisible = false;
        }else{
          frmBillPaymentCompleteCalendar.vbox4751247744173.isVisible = true;
        }
	  frmBillPaymentCompleteCalendar.vboxNewCompleteDesign.isVisible = false;
      frmBillPaymentCompleteCalendar.vbox4751247744173.isVisible = true;
    } else if (frmName.id == "frmQRPaymentSuccess") {
         frmQRPaymentSuccess.lblSucessScreenshot.isVisible = false;
        frmQRPaymentSuccess.flxFullBody.height = "81%"
        frmQRPaymentSuccess.flxHeader.setVisibility(true);
        //frmQRPaymentSuccess.hbxShareOption.setVisibility(false);
        frmQRPaymentSuccess.flxFooter.setVisibility(true);
        frmQRPaymentSuccess.FlexMain.setVisibility(true);
        frmQRPaymentSuccess.FlexscreenShot.setVisibility(false);
    } else if (frmName.id == "frmEDonationPaymentComplete"){
      frmEDonationPaymentComplete.hbxShareOption.setVisibility(true);
      frmEDonationPaymentComplete.flxHeader.setVisibility(true); 
      frmEDonationPaymentComplete.flxSuccessSaveShare.setVisibility(false);
      frmEDonationPaymentComplete.flxBody.height = "70%";
//       frmEDonationPaymentComplete.lblCitizenIDValue.setVisibility(true);
//       frmEDonationPaymentComplete.lblCitizenIDValueMasked.setVisibility(false);
      frmEDonationPaymentComplete.lblAccountNumberValue.setVisibility(true);
      frmEDonationPaymentComplete.lblAccountNumberValueMasked.setVisibility(false);
      if(kony.application.getPreviousForm().id == "frmEDonationPaymentConfirm"){
        frmEDonationPaymentComplete.flxFooter.setVisibility(true);
        frmEDonationPaymentComplete.flxFooterHistoryView.setVisibility(false);
      }else{
        frmEDonationPaymentComplete.flxFooter.setVisibility(false);
        frmEDonationPaymentComplete.flxFooterHistoryView.setVisibility(true);
      }
    }
    kony.print("frmName>>"+frmName);
    kony.print("Image>>>"+frmTransfersAck.imgTransNBpAckComplete.src);
	if(frmName.id == "frmTransfersAck"){
        kony.print("Inside frmTransfersAck form ");
		if(frmTransfersAck.imgTransNBpAckComplete.src == "iconnotcomplete.png" || frmTransfersAck.imgTransNBpAckComplete.src == "mes2.png"){
			 kony.print("Inside if of iconnotcomplete");
			 frmTransfersAck.hbxShareOption.setVisibility(false);
		}else{
			  kony.print("Inside ELSE of iconnotcomplete");
			   frmTransfersAck.hbxShareOption.setVisibility(true);
		}
	}else{
       kony.print("ELSE frmTransfersAck form ");
	   frmName.hbxShareOption.setVisibility(true);
	}
     settingVisibility = false;
}
/*****************************************************
function to handle share click in ios and Android
*****************************************************/

function shareToSocialMedia(url, module) {
    if (gblDeviceInfo["name"] == "android") {
        //kony.print("if android share");
        androidshare(url, module);

    } else if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPad Simulator") {
        iosshare(url, module);

    }
}

/*****************************************************
Sharing the data in iOS devices
*****************************************************/
function iosshare(url, module) {
    var emailsubject = "";
    var module = module;
    if (kony.i18n.getCurrentLocale() == "en_US") {
        if (module == "Transfer") {
            emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailTransferSlip");
        } else if (module == "BillPayment") {
            emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailBillPaySlip");
        } else if (module == "TopupPayment") {
            emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailTopUpSlip");
        } else if (module == "eDonation") {
            emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailEDonationSlip");
        }

    } else {
        if (module == "Transfer") {
            emailsubject = kony.i18n.getLocalizedString("keyEmailTransferSlip") + " " + gblCustomerNameTh;
        } else if (module == "BillPayment") {
            emailsubject = kony.i18n.getLocalizedString("keyEmailBillPaySlip") + " " + gblCustomerNameTh;
        } else if (module == "TopupPayment") {
            emailsubject = kony.i18n.getLocalizedString("keyEmailTopUpSlip") + " " + gblCustomerNameTh;
        } else if (module == "eDonation") {
            emailsubject = kony.i18n.getLocalizedString("keyEmailEDonationSlip") + " " + gblCustomerNameTh;
        }
    }
    try {
        var socialmsg = url
        var emailmsg = url
        var smsmsg = url
        var language = kony.i18n.getCurrentLocale();
        var sharecontent = [];
        sharecontent[0] = emailsubject;
        sharecontent[2] = socialmsg;
        sharecontent[1] = emailmsg;
        sharecontent[3] = language;
        // sharecontent[4] = isSave; 
         // kony.print("popup showed isSave1"+isSave );
        //Creates an object of class 'sharescript'
        var sharescriptObject = new iossharemedia.sharescript();
        //Invokes method 'shareonios' on the object
        sharescriptObject.shareonios(
            /**Array*/
            sharecontent);
      // following code is for AutoSavepopup
        
      if(socialmsg=="screenshot"){
      var options = {};
      var result = kony.application.checkPermission(kony.os.RESOURCE_PHOTO_GALLERY, options);
       if (result.status == kony.application.PERMISSION_GRANTED){
         showScreenSavePopUp();
          kony.print("popup showed " );
       }
      }
    } catch (err) {
        kony.print("the value of err.message is:" + err);
    }
}

/*****************************************************
Sharing the data in Android devices
*****************************************************/
function androidshare(url, module) {
    try {
        //alert("in anroid share"+url);
        var module = module
        var emailsubject = "";
        if (kony.i18n.getCurrentLocale() == "en_US") {
            if (module == "Transfer") {
                emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailTransferSlip");
            } else if (module == "BillPayment") {
                emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailBillPaySlip");
            } else if (module == "TopupPayment") {
                emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailTopUpSlip");
            } else if (module == "eDonation") {
              	emailsubject = gblCustomerName + " " + kony.i18n.getLocalizedString("keyEmailEDonationSlip");
            }

        } else {
            if (module == "Transfer") {
                emailsubject = kony.i18n.getLocalizedString("keyEmailTransferSlip") + " " + gblCustomerNameTh;
            } else if (module == "BillPayment") {
                emailsubject = kony.i18n.getLocalizedString("keyEmailBillPaySlip") + " " + gblCustomerNameTh;
            } else if (module == "TopupPayment") {
                emailsubject = kony.i18n.getLocalizedString("keyEmailTopUpSlip") + " " + gblCustomerNameTh;
            } else if (module == "eDonation") {
              	emailsubject = kony.i18n.getLocalizedString("keyEmailEDonationSlip") + " " + gblCustomerNameTh;
            }
        }
        var socialmsg = url
        var emailmsg = url
        var smsmsg = url

        var language = kony.i18n.getCurrentLocale();

        //Creates an object of class 'ShareOnAndroid'
        var ShareOnAndroidObject = new androidshareactivity.ShareOnAndroid(
            /**String*/
            emailsubject,
            /**String*/
            emailmsg,
            /**String*/
            socialmsg,
            /**String*/
            smsmsg,
            /**String*/
            language,
            /**String*/
            module);
        //kony.print("in android share 1");
        if (socialmsg == "line") {
            ShareOnAndroidObject.onJSClickActivityline();
        } else if (socialmsg == "facebook") {
            ShareOnAndroidObject.onJSClickActivityfbmsngr();
        } else if (socialmsg == "more") {
            ShareOnAndroidObject.onJSClickActivity();
        } else if (socialmsg == "screenshot") {
            ShareOnAndroidObject.onJSClickActivityScreenShot();
        }
        // kony.print("in android share 21");
    } catch (err) {
        kony.print("the value of err.message is:" + err);
    }
}


function shareIntentCall(inputmessage, flow) {
    if (gblDeviceInfo["name"] == "android") {} else {
        hideHeaderFooter();
    }
    shareMessage = inputmessage;
    module = flow;
    var message = "";
    if (inputmessage == "facebook") {
        message = kony.i18n.getLocalizedString("keyNavigateMsngr");
    } else {
        message = kony.i18n.getLocalizedString("keyNavigateLine");
    }
    var title = "";
    var yeslbl = kony.i18n.getLocalizedString("keyOK");
    var nolbl = kony.i18n.getLocalizedString("keyCancelButton");
    var basicConf = {
        message: message,
        alertType: constants.ALERT_TYPE_CONFIRMATION,
        alertTitle: title,
        yesLabel: yeslbl,
        noLabel: nolbl,
        alertHandler: callbackShareFFI
    };
    //Defining pspConf parameter for alert
    var pspConf = {};
    //Alert definition
    var infoAlert = kony.ui.Alert(basicConf, pspConf);
}


function callbackShareFFI(response) {
    if (gblDeviceInfo["name"] == "android") {
        hideHeaderFooter();
    } else {}
    hideHeaderFooter();
    if (response == true) {
        shareToSocialMedia(shareMessage, module);
        try {
            kony.timer.cancel("displayTimer")
        } catch (e) {}
        kony.timer.schedule("displayTimer", displayHeaderFooter, 2, false);
    } else {
        displayHeaderFooter();
    }

}


function screenShotCall(message) {
    hideHeaderFooter();
    try {
        kony.timer.cancel("takescreenshot")
    } catch (e) {}
    shareMessage = message;
    kony.timer.schedule("takescreenshot", shareToSocialMediascreenShot, 2, false);

}

function shareToSocialMediascreenShot() {
    shareToSocialMedia(shareMessage);
    displayHeaderFooter();
    //alert(kony.i18n.getLocalizedString("keySaveImageMessage"));
}


function shareIntentmoreCall(message, module) {
    hideHeaderFooter();
    try {
        kony.timer.cancel("shareIntent")
    } catch (e) {}
    shareMessage = message;
    iphonemodule = module;
    kony.timer.schedule("shareIntent", shareToSocialMediaIntent, 2, false);

}

function shareToSocialMediaIntent() {
    shareToSocialMedia(shareMessage, iphonemodule)
    displayHeaderFooter();
}

function shareIntentmoreCallandroid(message, module) {
    hideHeaderFooter();
    shareToSocialMedia(message, module)
    try {
        kony.timer.cancel("shareIntentdis")
    } catch (e) {}
    shareMessage = message;
    kony.timer.schedule("shareIntentdis", shareToSocialMediaIntentand, 2, false);

}

function shareToSocialMediaIntentand() {
    displayHeaderFooter();
    if (shareMessage == "screenshot") {
            // following code is commented for AutoSavepopup
//         var message = kony.i18n.getLocalizedString("keySaveImageMessage");
//         var title = "";
//         var yeslbl = kony.i18n.getLocalizedString("keyOK");
//         var nolbl = "";
//         var basicConf = {
//             message: message,
//             alertType: constants.ALERT_TYPE_INFO,
//             alertTitle: title,
//             yesLabel: yeslbl,
//             noLabel: nolbl
//         };
//         //Defining pspConf parameter for alert
//         var pspConf = {};
//         //Alert definition
//         var infoAlert = kony.ui.Alert(basicConf, pspConf);
      showScreenSavePopUp();
      
      
    }

}

// following function is for to show AutoSavepopup

function showScreenSavePopUp(){
  var context="";
  if(kony.application.getCurrentForm().id == "frmBillPaymentCompleteCalendar"){
   context={"widget":frmBillPaymentCompleteCalendar.lblHdrTxt,"anchor":"left","sizetoanchorwidth":false};
  }else if(kony.application.getCurrentForm().id == "frmTransfersAck"){
    context={"widget":frmTransfersAck.lblHdrTxt,"anchor":"left","sizetoanchorwidth":false};
  }else  if(kony.application.getCurrentForm().id == "frmTransfersAckCalendar"){
     context={"widget":frmTransfersAckCalendar.lblHdrTxt,"anchor":"left","sizetoanchorwidth":false};
  }else  if(kony.application.getCurrentForm().id == "frmBillPaymentComplete"){
     context={"widget":frmBillPaymentComplete.lblHdrTxt,"anchor":"left","sizetoanchorwidth":false};
  }else{
    context={"widget":frmTransfersAck.lblHdrTxt,"anchor":"left","sizetoanchorwidth":false};
  } 
  screenSavePopup.setContext(context);
  screenSavePopup.show();
  kony.timer.schedule("screenSavePopuptimer", dismissSaveAckpopup, 4,false);	
}

// following function is for to dismiss AutoSavepopup
function dismissSaveAckpopup(){
  screenSavePopup.dismiss();
  kony.timer.cancel("screenSavePopuptimer");
}


function onClickShareBillpayCalendar() {

    if (frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src == "icon_notcomplete.png" || frmBillPaymentCompleteCalendar.imgTransNBpAckComplete.src == "mes2.png") {
        alert(kony.i18n.getLocalizedString("Err_ShareFailed"));
        return;
    } else {
        if (frmBillPaymentCompleteCalendar.hbxShareOption.isVisible) {
            frmBillPaymentCompleteCalendar.hbxShareOption.isVisible = true;
            frmBillPaymentCompleteCalendar.imgHeaderMiddle.src = "arrowtop.png";
        } else {
            frmBillPaymentCompleteCalendar.hbxShareOption.isVisible = true;
            frmBillPaymentCompleteCalendar.imgHeaderMiddle.src = "empty.png";
        }
    }
}