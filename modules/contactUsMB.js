







function contactFAQUrl(){
	var locale = kony.i18n.getCurrentLocale();
	
	
	var url = "";
	//if(locale == "en_US") {
//		//url ="https://www.tmbbank.com/howto/en/e-banking/faq.html";
//		url ="https://www.tmbbank.com/en/howto/e-banking/faq.html";
//	} else {
//		url ="https://www.tmbbank.com/howto/e-banking/faq.html";
//	}
				url = kony.i18n.getLocalizedString("contact_Faq");
				
	frmContactusFAQMB.browser506459299404741.requestURLConfig = {
		     URL: url,
		     requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
   		 }
   	frmContactusFAQMB.hbox475124774143.setVisibility(true);
    frmContactusFAQMB.hbox476018633224052.setVisibility(false);
    frmContactusFAQMB.button506459299404751.setVisibility(true);
    frmContactusFAQMB.show();
   		 
}
function contactUsOptions(){
	var selectIndex = frmContactUsMB.cmbOption.selectedKey;
	if ( selectIndex == 5) {
			frmContactUsMB.hbxContactMe.setVisibility(true);
			frmContactUsMB.hbxRadioButton.setVisibility(true);
			frmContactUsMB.radioGroup.selectedKey="2";
	} else {
			frmContactUsMB.hbxContactMe.setVisibility(false);
			frmContactUsMB.hbxRadioButton.setVisibility(false);	
	}
	if(flowSpa)
	frmContactUsMB.tbxCNTBoxSPA.text=""; 
	else
	frmContactUsMB.tbxCNTBox.text=""; 
}
var contactMode ="";
var cmbValue = "";
var emailId="";
var subject = "";
var sourceType = "";
var cSource = "contactUsEmail";
var message = null;
var locale="";
var gblContactComplete = "xxx-xxx-0000";
function contactUsEmainNotification(){
var msgTosend = null;
if(flowSpa)
msgTosend = frmContactUsMB.tbxCNTBoxSPA.text;
else
msgTosend = frmContactUsMB.tbxCNTBox.text;
	if ( frmContactUsMB.cmbOption.selectedKey == 1 ){
			alert(kony.i18n.getLocalizedString("cntoption"));
 			return false;	
	}
	else if(msgTosend == null || msgTosend.trim() == "" ){
			alert(kony.i18n.getLocalizedString("cntText"));
			return false;
	}
	
	var key = frmContactUsMB.cmbOption.selectedKey;
	if ( key == 2 ) {
		subject = kony.i18n.getLocalizedString("Cnt_IB");
		sourceType = "IB";	
	} else if ( key == 3 ){
		subject = kony.i18n.getLocalizedString("Cnt_DA");
		sourceType = "deposit";
	} else if ( key == 4 ) {
		subject = kony.i18n.getLocalizedString("Cnt_LP");
		sourceType = "lending"
	} else if( key == 5) {
		subject = kony.i18n.getLocalizedString("Cnt_Compliants");
		sourceType = "complaint"
	}
	if(key == 5){
		contactMode = frmContactUsMB.radioGroup.selectedKeyValue[1];
	} else {
		contactMode = kony.i18n.getLocalizedString("email");
	}
	
	cmbValue = frmContactUsMB.cmbOption.selectedKeyValue;
	
	locale=kony.i18n.getCurrentLocale();
	 if(flowSpa)
	 	message = frmContactUsMB.tbxCNTBoxSPA.text;
	 else
	message = frmContactUsMB.tbxCNTBox.text;
	var inputParams = {
 		notificationType:"Email",
 		notificationSubject:subject,
		recipientNickName:gblCustomerName,
		//source:cSource,
		Locale:locale,
		//channel:"MB",
 		message:message,
		type:"User",
 		custNAME:gblCustomerName
 		
	};
	showLoadingScreen();
    invokeServiceSecureAsync("ContactUsNotification", inputParams, sendContactUsNotificationCallBack);
    	//var inputParam={};
//    	inputParam["crmId"] = "";
//		inputParam["rqUUId"]="";
//		invokeServiceSecureAsync("crmProfileInq", inputParam, crmProfileEmailCallBack);
}
//function crmProfileEmailCallBack(status, callBackResponse){
//	
//	
//
//	if (status == 400) {
//		if (callBackResponse["opstatus"] == 0) {
//				
//				
//				emailId=callBackResponse["emailAddr"];
//		}
//	}
//
//
//}
function sendContactUsNotificationCallBack(status,callbackResponse){
	gblContactComplete = "xxx-xxx-0000";
	var locale = kony.i18n.getCurrentLocale();
    
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
            dismissLoadingScreen();
             //Code changed for IOS9 upgrade
            TMBUtil.DestroyForm(frmContactUsMB);
            
            
            if (cmbValue[1] == kony.i18n.getLocalizedString("Cnt_Compliants")) {
                if (contactMode.trim() == kony.i18n.getLocalizedString("mobilePhone")) {
                    if (gblPHONENUMBER != null && gblPHONENUMBER.length >= 10) {
						gblContactComplete = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
                    } else {
						gblContactComplete = gblPHONENUMBER;
                    }
                    frmContactUsCompleteScreenMB.lblmessage.text = kony.i18n.getLocalizedString("keyTMBCompleteData") + " " + gblContactComplete + " " + kony.i18n.getLocalizedString("keyTMBComplete");
                } else {
					gblContactComplete = callbackResponse["emailId"];
					frmContactUsCompleteScreenMB.lblmessage.text = kony.i18n.getLocalizedString("keyTMBCompleteData") + " " + gblContactComplete + " " + kony.i18n.getLocalizedString("keyTMBComplete");
                }
            } else {
				gblContactComplete = callbackResponse["emailId"];
				if(locale=='th_TH'){
					frmContactUsCompleteScreenMB.lblmessage.text = kony.i18n.getLocalizedString("keyTMBCompleteData") + " " + gblContactComplete ;
				} else {
					frmContactUsCompleteScreenMB.lblmessage.text = kony.i18n.getLocalizedString("keyTMBCompleteData") + " " + gblContactComplete + " " + kony.i18n.getLocalizedString("keyShortly");
				}
				
            }
			
		}
        contactUsTMBEmailNotification(sourceType, message, gblCustomerName, gblPHONENUMBER, locale, contactMode, subject, cSource);
        frmContactUsCompleteScreenMB.show();
    }
  }
function contactUsTMBEmailNotification(sourceType,message,gblCustomerName,gblPHONENUMBER,locale,contactMode,subject,cSource) {
		
			var inputParams = {
				notificationType:"Email",
				notificationSubject:subject,
				//source:cSource,
				Locale:"th_TH",
				//channel:"MB",
				message:message,
				sourcetype:sourceType,
				contactType:contactMode,
				custNAME:gblCustomerName,//"Revanth"
				type:"Admin",
				phoneNumber:gblPHONENUMBER
			};
			 invokeServiceSecureAsync("ContactUsNotification", inputParams, sendContactUsAdminNotificationCallBack);
}

function sendContactUsAdminNotificationCallBack(status,callbackResponse){
		
}
	function contactUsDisplayForm(){
			//Code changed for IOS9 upgrade
			frmContactUsMB.hbxPostLogin.setVisibility(true);
			frmContactUsMB.hboxContactSend.setVisibility(true);
			frmContactUsMB.hbxFeedback.setVisibility(false);
			frmContactUsMB.hboxFeedBackSend.setVisibility(false);
			frmContactUsMB.hbxContact.setVisibility(false);
			frmContactUsMB.line47592361418663.setVisibility(false);
			frmContactUsMB.line47592361418559.setVisibility(false);
			frmContactUsMB.hbxFAQ.setVisibility(false);
			frmContactUsMB.hbxFB.setVisibility(false);
			frmContactUsMB.hbxFindTMB.setVisibility(false);
			frmContactUsMB.lblheader.text =kony.i18n.getLocalizedString("keyContactUs");
			frmContactUsMB.button477746511286959.text = kony.i18n.getLocalizedString("keyFeedback");
			frmContactUsMB.button506459299445012.text = kony.i18n.getLocalizedString("keyContactUs");
			frmContactUsMB.button477746511286957.text = kony.i18n.getLocalizedString("keyContactUs");
			frmContactUsMB.button506459299445013.text = kony.i18n.getLocalizedString("keyFeedback");
			frmContactUsMB.link506459299445023.text   = kony.i18n.getLocalizedString("SeeFAQ");
			frmContactUsMB.label506459299445025.text  = kony.i18n.getLocalizedString("keyWriteUs");
			frmContactUsMB.label506459299445035.text = kony.i18n.getLocalizedString("keyContactMe");
			frmContactUsMB.label506459299445030.text = kony.i18n.getLocalizedString("cnt_message");
			if(flowSpa)
			frmContactUsMB.tbxCNTBoxSPA.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
			else
			frmContactUsMB.tbxCNTBox.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
			var temp=[];
			temp.push(["1",kony.i18n.getLocalizedString("cnt_topic")]);
			temp.push(["2",kony.i18n.getLocalizedString("Cnt_IB")]);
			temp.push(["3",kony.i18n.getLocalizedString("Cnt_DA")]);
			temp.push(["4",kony.i18n.getLocalizedString("Cnt_LP")]);
			temp.push(["5",kony.i18n.getLocalizedString("Cnt_Compliants")]);
			frmContactUsMB.cmbOption.masterData=temp;
			
			 if(flowSpa){
			 	frmContactUsMB.tbxCNTBoxSPA.text="";
			 } else {
			 	frmContactUsMB.tbxCNTBox.text="";
			 }
			frmContactUsMB.hbxContactMe.setVisibility(false);
			frmContactUsMB.hbxRadioButton.setVisibility(false);
			 
			 var data=[];
		     data.push(["1",kony.i18n.getLocalizedString("mobilePhone")]);
			 data.push(["2",kony.i18n.getLocalizedString("email")]);
			 frmContactUsMB.radioGroup.masterData = data;
			 frmContactUsMB.radioGroup.selectedKey="2";
			 
			 if(kony.string.startsWith(gblDeviceInfo.name, "iPod", true) || kony.string.startsWith(gblDeviceInfo.name, "iPhone Simulator", true)){
			 	//frmContactUsMB.hbxContact.setVisibility(false);
			 	frmContactUsMB.imgCall.setVisibility(false);
			 	frmContactUsMB.lnkCall.setVisibility(false);
			 } else {
			 	//frmContactUsMB.hbxContact.setVisibility(true);
			 	frmContactUsMB.imgCall.setVisibility(true);
			 	frmContactUsMB.lnkCall.setVisibility(true);
			 }
			frmContactUsMB.show();
	}
