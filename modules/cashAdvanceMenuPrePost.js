function frmMBCashAdvanceCardInfoMenuPreshow(){
  if (gblCallPrePost) {
    frmMBCashAdvanceCardInfo.lblCardName.text = "";
    frmMBCashAdvanceCardInfo.lblCardNumber.text = frmMBManageCard.lblCardNumber.text;
    frmMBCashAdvanceCardInfo.lblCardAcctName.text = frmMBManageCard.lblCardAccountName.text;
    frmMBCashAdvanceCardInfo.imgCard.src = frmMBManageCard.imgCard.src;
    frmMBCashAdvanceCardInfo.lblCardNumber.skin = getCardNoSkin(frmMBCashAdvanceCardInfo.imgCard.src); //MKI, MIB-12137 start
    if(frmMBCashAdvanceCardInfo.imgCard.src.toLowerCase().includes("debittmbwave")){      
      frmMBCashAdvanceCardInfo.lblCardAcctName.skin = "lblBlackCard22px";
    }else{         
      frmMBCashAdvanceCardInfo.lblCardAcctName.skin = "lblWhiteCard22px";
    } //MKI, MIB-12137 end
    frmMBCashAdvanceCardInfoLocalePreshow();
  }
}

function frmMBCashAdvanceCardInfoLocalePreshow(){
  changeStatusBarColor();
  frmMBCashAdvanceCardInfo.lblCashAdvanceTitle.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
  frmMBCashAdvanceCardInfo.lblRemDailyLimit.text = kony.i18n.getLocalizedString("CAV02_keyRCreditLimit");
  frmMBCashAdvanceCardInfo.lblAvailSpend.text = kony.i18n.getLocalizedString("CAV02_keyAvailToSpend");
  frmMBCashAdvanceCardInfo.lblAvailCashAdv.text = kony.i18n.getLocalizedString("CAV02_keyAvailCA");
  frmMBCashAdvanceCardInfo.btnBack.text = kony.i18n.getLocalizedString("CAV02_btnBack");
  frmMBCashAdvanceCardInfo.btnNext.text = kony.i18n.getLocalizedString("CAV02_btnNext");
}

function frmMBCashAdvanceCardInfoMenuPostshow(){
  if (gblCallPrePost) {
    // Add the new code in the post show please add here
  }
  assignGlobalForMenuPostshow();
}

function frmMBCashAdvAcctSelectMenuPreshow(){
  if (gblCallPrePost){
    frmMBCashAdvAcctSelectLocalePreshow();
  }
}

function frmMBCashAdvAcctSelectLocalePreshow(){
  changeStatusBarColor();
  frmMBCashAdvAcctSelect.lblCashAdvanceTitle.text = kony.i18n.getLocalizedString("CAV06_keyTitle");
  frmMBCashAdvAcctSelect.lblCancel.text = kony.i18n.getLocalizedString("CAV06_btnCancel");
}

function frmMBCashAdvAcctSelectMenuPostshow(){
  if (gblCallPrePost) {
    // Add the new code in the post show please add here
  }
  assignGlobalForMenuPostshow();
}

function frmMBCashAdvanceSelectPlanMenuPostshow(){
  if (gblCallPrePost) {
    // Add the new code in the post show please add here
  }
  assignGlobalForMenuPostshow();
}

function frmMBCashAdvanceTnCMenuPreshow(){
  if (gblCallPrePost){
    //#ifdef android    
    //mki, mib-10789 Start
    // The below code snippet is to fix the whilte label on some screens
    frmMBCashAdvanceTnC.flexBodyScroll.showFadingEdges  = false;
    //#endif  
    //mki, mib-10789 End

    frmMBCashAdvanceTnCLocalePreshow();
  }
}

function frmMBCashAdvanceTnCLocalePreshow(){
  changeStatusBarColor();
  frmMBCashAdvanceTnC.lblCashAdvanceHdrTxt.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
  frmMBCashAdvanceTnC.btnCashAdvanceBack.text = kony.i18n.getLocalizedString("CAV02_btnBack");
  frmMBCashAdvanceTnC.btnCashAdvanceAgree.text = kony.i18n.getLocalizedString("CAV03_btnAgree");
  frmMBCashAdvanceTnC.flxSaveCamEmail.isVisible = false;
  frmMBCashAdvanceTnC.btnRight.skin = "btnRightShare";
  loadTermsNConditionCashAdvanceMB();
}

function frmMBCashAdvanceTnCMenuPostshow(){
  if (gblCallPrePost) {
    // Add the new code in the post show please add here
    //loadTermsNConditionCashAdvanceMB();
  }
  assignGlobalForMenuPostshow();
}

function frmCashAdvanceInstallmentPlanDetailsMenuPreshow(){
  //#ifdef android    
  //mki, mib-10789 Start
  // The below code snippet is to fix the whilte label on some screens
  frmCashAdvanceInstallmentPlanDetails.flexBodyScroll.showFadingEdges  = false;
  //#endif  
  //mki, mib-10789 End

  if (gblCallPrePost){
    frmCashAdvanceInstallmentPlanDetails.lblCardNum.text = frmMBManageCard.lblCardNumber.text;

    frmCashAdvanceInstallmentPlanDetails.lblCustName.text = frmMBManageCard.lblCardAccountName.text;
    frmCashAdvanceInstallmentPlanDetails.imgMainCard.src = frmMBManageCard.imgCard.src;
    frmCashAdvanceInstallmentPlanDetails.lblCardNum.skin = getCardNoSkin(frmCashAdvanceInstallmentPlanDetails.imgMainCard.src); //MKI, MIB-12137 start
    if(frmCashAdvanceInstallmentPlanDetails.imgMainCard.src.toLowerCase().includes("debittmbwave")){      
      frmCashAdvanceInstallmentPlanDetails.lblCustName.skin = "lblBlackCard22px";
    }else{         
      frmCashAdvanceInstallmentPlanDetails.lblCustName.skin = "lblWhiteCard22px";
    } //MKI, MIB-12137 end
    frmCashAdvanceInstallmentPlanDetailsLocalePreshow();
  }
}

function frmCashAdvanceInstallmentPlanDetailsLocalePreshow(){
  changeStatusBarColor();
  frmCashAdvanceInstallmentPlanDetails.flexBodyScroll.scrollToWidget(frmCashAdvanceInstallmentPlanDetails.flxTop);
  frmCashAdvanceInstallmentPlanDetails.lblHeader.text = kony.i18n.getLocalizedString("CAV02_keyTitle");
  frmCashAdvanceInstallmentPlanDetails.lblAvailableCashDesc.text = kony.i18n.getLocalizedString("CAV02_keyAvailToSpend");
  frmCashAdvanceInstallmentPlanDetails.lblRemainDailyLimit.text = kony.i18n.getLocalizedString("CAV02_keyRCreditLimit");
  frmCashAdvanceInstallmentPlanDetails.lblAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");
  frmCashAdvanceInstallmentPlanDetails.lblEnterAmountDesc.text = kony.i18n.getLocalizedString("CAV04_keyAmount");

  if(frmCashAdvanceInstallmentPlanDetails.flxFullPayment.isVisible){
    frmCashAdvanceInstallmentPlanDetails.lblFullPayment.text = kony.i18n.getLocalizedString("CAV04_plFullPayment");
    frmCashAdvanceInstallmentPlanDetails.lblPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
    frmCashAdvanceInstallmentPlanDetails.lblFeeDesc.text = kony.i18n.getLocalizedString("CAV04_keyFee");
    frmCashAdvanceInstallmentPlanDetails.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
    frmCashAdvanceInstallmentPlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime01");
    frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
  }
  if(frmCashAdvanceInstallmentPlanDetails.rchTxtNote.isVisible){
    var intRate = kony.string.replace(frmCashAdvanceInstallmentPlanDetails.lblAnnualRateVal.text, "%", "");
    var msgPro = kony.i18n.getLocalizedString("msgPromotionRateWarning");
    msgPro = msgPro.replace("{Annual_Rate}", intRate);
    frmCashAdvanceInstallmentPlanDetails.rchTxtNote.text = msgPro;
  }
  if(frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.isVisible){
    var intRate = kony.string.replace(frmCashAdvanceInstallmentPlanDetails.lblTenorVal.text, "%", "");
    var msgPro = kony.i18n.getLocalizedString("msgPromotionRateWarning");
    msgPro = msgPro.replace("{Annual_Rate}", intRate);
    frmCashAdvanceInstallmentPlanDetails.rchTxtNote2.text = msgPro;
  }
  if(frmCashAdvanceInstallmentPlanDetails.flxSelectPaymentPlan.isVisible){
    frmCashAdvanceInstallmentPlanDetails.lblSelectPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_plPaymentPlan");
    frmCashAdvanceInstallmentPlanDetails.lblPaymentPlanTitle.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
  }
  if(frmCashAdvanceInstallmentPlanDetails.flxInstallmentPlan.isVisible){
    frmCashAdvanceInstallmentPlanDetails.lblInsPaymentPlan.text = kony.i18n.getLocalizedString("CAV04_keyPaymentPlan");
    frmCashAdvanceInstallmentPlanDetails.lblInsFeeDesc.text = kony.i18n.getLocalizedString("CAV04_keyFee");
    if(gblCAFullPaymentFlag == true){
      frmCashAdvanceInstallmentPlanDetails.lblInstallmentTitle.text = kony.i18n.getLocalizedString("CAV04_plFullPayment");
      frmCashAdvanceInstallmentPlanDetails.lblTenorDesc.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
      frmCashAdvanceInstallmentPlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime01");
      frmCashAdvanceInstallmentPlanDetails.lblNowDesc.text = kony.i18n.getLocalizedString("keyNOW");
    }else{
      frmCashAdvanceInstallmentPlanDetails.lblInstallmentTitle.text = kony.i18n.getLocalizedString("CAV04_plInstallment");
      frmCashAdvanceInstallmentPlanDetails.lblTenorDesc.text = kony.i18n.getLocalizedString("Loan_NoofMonth");
      frmCashAdvanceInstallmentPlanDetails.lblRatePerMonth.text = kony.i18n.getLocalizedString("CAV05_keyRate");
      frmCashAdvanceInstallmentPlanDetails.lblPaymentAmt.text = kony.i18n.getLocalizedString("Loan_FirstPayment");
      frmCashAdvanceInstallmentPlanDetails.lblTime.text = kony.i18n.getLocalizedString("CAV04_keyTime02");
    }
  }
  if(frmCashAdvanceInstallmentPlanDetails.flxDebitCardDateDetails.isVisible){
    frmCashAdvanceInstallmentPlanDetails.lblDebitCardDesc.text = kony.i18n.getLocalizedString("CAV04_keyDebitOn");
    frmCashAdvanceInstallmentPlanDetails.lblReveivedByDesc.text = kony.i18n.getLocalizedString("CAV04_keyReceivedBy");
  }

  if(frmCashAdvanceInstallmentPlanDetails.flxEnterFullAmountDtls.isVisible){
    veriflyEnterAmountValueCA("frmCashAdvanceInstallmentPlanDetails");
  }else{
    frmCashAdvanceInstallmentPlanDetails.flxAmount.setVisibility(true);
    frmCashAdvanceInstallmentPlanDetails.flxEnterFullAmountDtls.setVisibility(false);
  }

  frmCashAdvanceInstallmentPlanDetails.lblAccount.text = kony.i18n.getLocalizedString("CAV04_keyToAct");
  if(isNotBlank(gblCASelectedToAcctNum)){
    if(kony.i18n.getCurrentLocale() == "en_US"){
      frmCashAdvanceInstallmentPlanDetails.lblSelectedAccount.text = gblCASelectedToAcctNameEN;
    }else{
      frmCashAdvanceInstallmentPlanDetails.lblSelectedAccount.text = gblCASelectedToAcctNameTH;
    }
  }else{
    frmCashAdvanceInstallmentPlanDetails.lblSelectedAccount.text = kony.i18n.getLocalizedString("CAV04_plToAct");
  }

  frmCashAdvanceInstallmentPlanDetails.btnBack.text = kony.i18n.getLocalizedString("CAV04_btnBack");
  frmCashAdvanceInstallmentPlanDetails.btnNext.text = kony.i18n.getLocalizedString("CAV04_btnNext");
}

function frmCashAdvanceInstallmentPlanDetailsMenuPostshow(){
  if (gblCallPrePost) {
    // Add the new code in the post show please add here
  }
  assignGlobalForMenuPostshow();
}

function frmCAPaymentPlanConfirmationPreshow(){
  if (gblCallPrePost) {
    frmCAPaymentPlanConfirmation.imgCard.src = frmCashAdvanceInstallmentPlanDetails.imgMainCard.src;
    frmCAPaymentPlanConfirmation.lblCardNumber.text = frmCashAdvanceInstallmentPlanDetails.lblCardNum.text;
    frmCAPaymentPlanConfirmation.lblCardNumber.skin = getCardNoSkin(frmCAPaymentPlanConfirmation.imgCard.src); //MKI, MIB-12137 start
    if(frmCAPaymentPlanConfirmation.imgCard.src.toLowerCase().includes("debittmbwave")){      
      frmCAPaymentPlanConfirmation.lblCardName.skin = "lblBlackCard22px";
    }
    else{         
      frmCAPaymentPlanConfirmation.lblCardName.skin = "lblWhiteCard22px";
    } //MKI, MIB-12137 end
    frmCAPaymentPlanConfirmation.lblCardName.text = frmCashAdvanceInstallmentPlanDetails.lblCustName.text;

    frmCAPaymentPlanConfirmationLocalePreshow();
  }
}

function frmCAPaymentPlanConfirmationLocalePreshow(){
  changeStatusBarColor();
  frmCAPaymentPlanConfirmation.lblHde.text = kony.i18n.getLocalizedString("CAV07_keyTitle");
  frmCAPaymentPlanConfirmation.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle") 
    + " " + frmCashAdvanceInstallmentPlanDetails.lblEnterAmount.text
    + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
  frmCAPaymentPlanConfirmation.lblFrom.text = kony.i18n.getLocalizedString("CAV07_keyFrom");
  frmCAPaymentPlanConfirmation.lblTo.text = kony.i18n.getLocalizedString("CAV07_keyTo");
  frmCAPaymentPlanConfirmation.lblReference.text = kony.i18n.getLocalizedString("CAV07_keyRefNo");
  frmCAPaymentPlanConfirmation.lblAmount.text = kony.i18n.getLocalizedString("CAV07_keyAmt");
  frmCAPaymentPlanConfirmation.lblFee.text = kony.i18n.getLocalizedString("CAV04_keyFee");
  if(gblCAFullPaymentFlag){
    frmCAPaymentPlanConfirmation.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
  }else{	
    frmCAPaymentPlanConfirmation.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV05_keyRate");
  }
  frmCAPaymentPlanConfirmation.lblTenor.text = kony.i18n.getLocalizedString("Loan_NoofMonth");
  frmCAPaymentPlanConfirmation.lblPaymentAmt.text = kony.i18n.getLocalizedString("Loan_FirstPayment");
  frmCAPaymentPlanConfirmation.lblDebitCardOn.text = kony.i18n.getLocalizedString("CAV04_keyDebitOn");
  frmCAPaymentPlanConfirmation.lblRecieveDate.text = kony.i18n.getLocalizedString("CAV04_keyReceivedBy");
  frmCAPaymentPlanConfirmation.btnBack.text = kony.i18n.getLocalizedString("CAV07_btnCancel");
  frmCAPaymentPlanConfirmation.btnNext.text = kony.i18n.getLocalizedString("CAV07_btnConfirm");
}

function frmCAPaymentPlanConfirmationPostshow(){
  if (gblCallPrePost) {
    // Add the new code in the post show please add here
  }
  assignGlobalForMenuPostshow();
  addAccessPinKeypad(frmCAPaymentPlanConfirmation); 
}

function frmCAPaymentPlanCompletePreshow(){
  if (gblCallPrePost) {
    frmCAPaymentPlanCompleteLocalePreshow();
  }
}

function frmCAPaymentPlanCompleteLocalePreshow(){
  changeStatusBarColor();
  frmCAPaymentPlanComplete.lblHde.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
  frmCAPaymentPlanComplete.lblCashAdvanceVal.text = kony.i18n.getLocalizedString("CAV02_keyTitle") 
    + " " + frmCAPaymentPlanComplete.lblAmountVal.text;
  frmCAPaymentPlanComplete.lblFrom.text = kony.i18n.getLocalizedString("CAV07_keyFrom");
  frmCAPaymentPlanComplete.lblTo.text = kony.i18n.getLocalizedString("CAV07_keyTo");
  frmCAPaymentPlanComplete.lblReference.text = kony.i18n.getLocalizedString("CAV07_keyRefNo");
  frmCAPaymentPlanComplete.lblAmount.text = kony.i18n.getLocalizedString("CAV07_keyAmt");
  frmCAPaymentPlanComplete.lblFee.text = kony.i18n.getLocalizedString("CAV04_keyFee");
  frmCAPaymentPlanComplete.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
  frmCAPaymentPlanComplete.lblDebitCardOn.text = kony.i18n.getLocalizedString("CAV04_keyDebitOn");
  frmCAPaymentPlanComplete.lblRecieveDate.text = kony.i18n.getLocalizedString("CAV04_keyReceivedBy");
  frmCAPaymentPlanComplete.lblManageOtherCards.text = kony.i18n.getLocalizedString("CAV08_btnManageOther");
  frmCAPaymentPlanComplete.lblTenor.text = kony.i18n.getLocalizedString("Loan_NoofMonth");
  frmCAPaymentPlanComplete.lblPaymentAmt.text = kony.i18n.getLocalizedString("Loan_FirstPayment");
  if(gblCAFullPaymentFlag){
    frmCAPaymentPlanComplete.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV04_keyRate02");
  }else{
    frmCAPaymentPlanComplete.lblAnnualRate.text = kony.i18n.getLocalizedString("CAV05_keyRate");
  }
}

function frmCAPaymentPlanCompletePostshow(){
  if (gblCallPrePost) {
    // Add the new code in the post show please add here
  }
  assignGlobalForMenuPostshow();
}

