







/*
 * Method callOnlinePaymentAddService() 
 * 
 */
function callOnlinePaymentAddServiceMB()
{

	var toAccType;
    var toAcctNo = frmSelectBiller.segMyBills["selectedItems"][0]["ToAccountKey"];
    GBLFINANACIALACTIVITYLOG.toAcctId=  frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
    var billerMethod = frmSelectBiller.segMyBills["selectedItems"][0]["billerMethod"];
    var billerGroupType = frmSelectBiller.segMyBills["selectedItems"][0]["billerGroupType"];
    
    
    
    var transferFee = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text)).toFixed(2);
    var transferAmount = parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text)).toFixed(2); //+ parseFloat(transferFee.substring(0, transferFee.indexOf(" "))) ;
	 
    if (toAcctNo.length == 14) {
        toAccType = "SDA";
    } else {
        fourthDigit = toAcctNo.charAt(3);
        
        if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
            toAccType = "SDA";
            toAcctNo="0000"+toAcctNo;
        } else {
            toAccType = "DDA";
        }
          
    }
    
    var frmAcct = frmBillPaymentConfirmationFuture.lblAccountNum.text; //fromAcctID;  
    var fromAcctID = "";    
    var fromAcctType;
    for (var i = 0; i < frmAcct.length; i++) {
        if (frmAcct[i] != "-") {
            if (fromAcctID == null) {
                fromAcctID = frmAcct[i];
            } else {
                fromAcctID = fromAcctID + frmAcct[i];
            }
        }
    }
    if (fromAcctID.length == 14) {
        fromAcctType = "SDA";
    } else {
        fourthDigit = fromAcctID.charAt(3);
        if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
            fromAcctType = "SDA";
            fromAcctID="0000"+fromAcctID;
        } else {
            fromAcctType = "DDA";
        }
    }

    var tranCode;
    if (fromAcctType == "DDA") {
        tranCode = "8810";
    } else {
        tranCode = "8820";
    }
    var invoice="XXX";
    //invoice=frmBillPayment.tbxRef2Value.text
    var accTypeCode;
    if (toAccType == "DDA") {
        accTypeCode = "0000";
    } else {
        accTypeCode = "0200";
    }
    var TMB_BANK_FIXED_CODE = "0001";
    var TMB_BANK_CODE_ADD = "0011";
    var fiident = TMB_BANK_CODE_ADD + TMB_BANK_FIXED_CODE + "0" + toAcctNo[0] + toAcctNo[1] + toAcctNo[2] + accTypeCode;
    var ref1 = "";
    var ref2 = "";
    var ref3 = "";
    var ref4 = "";
    
     var gblCompCode = frmSelectBiller["segMyBills"]["selectedItems"][0]["BillerCompCode"].text;
    	if(gblCompCode == "2151" || gblCompCode == "2347" || gblCompCode == "2348" || gblCompCode == "0333" || gblCompCode == "0594" || gblCompCode == "0803")
	{
		bankrefID = BankRefId;
	} else if(gblCompCode == "0014" || gblCompCode == "0249" || gblCompCode == "2270" ) {
	
		bankrefID = TranId;
	} else {
		bankrefID = "";
	}
	if(gblCompCode == "2151" || gblCompCode == "2347" || gblCompCode == "2348" || gblCompCode == "0333" || gblCompCode == "0254" || gblCompCode == "0185")
	{
		ref1 = gblRef1;
		ref2 = gblRef2;
		ref3 = gblRef3;
		ref4 = gblRef4; 
	} else {
		ref1 = "";
		ref2 = "";
		ref3 = "";
		ref4 = ""; 
	} 
		
    inputParam = {};
    var TranID =  TranId;//frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
    	inputParam["TrnId"] = TranID;
    	inputParam["BankId"] = "011";
    	inputParam["BranchId"] = "0001";
    	inputParam["BankRefId"] =bankrefID;
    	inputParam["Amt"] =transferAmount;
   		inputParam["Ref1"] = ref1;//frmIBBillPaymentLP.lblBPRef1Val.text;
    	inputParam["Ref2"] = ref2;//frmIBBillPaymentLP.lblRefVal2.text;
    	inputParam["Ref3"] = ref3;
    	inputParam["Ref4"] = ref4;
    	inputParam["MobileNumber"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
        inputParam["fromAcctNo"] =fromAcctID;// removeHyphenIB(frmIBBillPaymentConfirm.lblFromAccountNo.text); //
        inputParam["fromAcctTypeValue"] = fromAcctType;
        inputParam["toAcctNo"] = toAcctNo;
        inputParam["toAcctType"] = toAccType;
        inputParam["toFIIdent"] = fiident;
	    inputParam["frmFiident"] = frmBillPayment.segSlider.selectedItems[0].fromFIIdent;
        inputParam["transferAmt"] = transferAmount;
        inputParam["TranCode"] = tranCode;
        inputParam["channelName"] = "IB";
        inputParam["billPmtFee"] = transferFee;
        inputParam["pmtRefIdent"] = frmBillPayment.lblRef1Value.text;
         var invoice="XXX";
//         invoice=frmBillPaymentConfirmationFuture.lblRef2Value.text;
        inputParam["invoiceNum"] = invoice;
        inputParam["PostedDt"] = getTodaysDate();
        inputParam["EPAYCode"] = "EPYS";
        inputParam["compCode"] = gblCompCode;
        inputParam["InterRegionFee"] = "0.00";
        inputParam["billPay"] = "billPay";
   			
   			
			invokeServiceSecureAsync("onlinePaymentAdd", inputParam, BillPaymentOnlinePaymentAddServiceCallBack);

}
function BillPaymentOnlinePaymentAddServiceCallBack(status,resulttable){

	if(status==400){
		
		
		if(resulttable["opstatus"] == 0)
		 {
		 	
		 	
		 	// fetching To Account Name for TMB Inq
			
		 	var StatusCode=resulttable["StatusCode"];
		 	var Severity=resulttable["Severity"];
		 	var StatusDesc=resulttable["StatusDesc"];
		 	dismissLoadingScreen();
		 	
		 	if(StatusCode!="0"){
		 		alert(" "+resulttable["errMsg"]);
		 		return false;
		 	}
		 	else{
		 		callBillPaymentCrmProfileUpdateService();
		 	}
		 }
		else
		 {
		 	dismissLoadingScreen();
			alert(" "+resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		 } 
	}

}
/*
 * Method CallBillPaymentTransferService
 * 
 */

function callBillPaymentTransferServiceMB(){
			var billerMethod = frmSelectBiller.segMyBills.selectedItems[0].billerMethod;
			var billerGroupType = frmSelectBiller.segMyBills.selectedItems[0].billerGroupType;
			inputParam ={};
			GBLFINANACIALACTIVITYLOG.toAcctId=  frmSelectBiller.segMyBills.selectedItems[0]["ToAccountKey"];
			var i=gbltranFromSelIndex[1];
			var toAcctTypeValue;
			var toAcctNo=removeHyphenIB(frmBillPaymentConfirmationFuture.lblRef1Value.text);
			var tValue;
			var fValue;
			var ref2 = frmBillPaymentConfirmationFuture.lblRef2Value.text;
			var fromData=frmBillPayment.segSlider.data;
			var	frmID=fromData[i].lblActNoval;
			//var fromFIIdent=fromData[i].fromFIIdent;
			var	fromAcctID;
			for(var i = 0; i < frmID.length; i++){
				if(frmID[i] != "-"){
					if(fromAcctID == null){
						fromAcctID = frmID[i];
					}else{
						fromAcctID = fromAcctID + frmID[i];
					}
				}
			}
			if(billerMethod==2){
			 toAcctTypeValue = "CCA";
			 tValue =9;
			  if(toAcctNo.length == 10){
       				 toAcctNo="00000000000000"+toAcctNo;
       		 }
       			 if(toAcctNo.length == 16){
       				 toAcctNo="00000000"+toAcctNo;
       			 }     
			} else if(billerMethod==3) {
				toAcctTypeValue = "LOC";
				tValue =5;
				if(toAcctNo.length == 10){
      			  toAcctNo=toAcctNo+ref2;
       			 }
        		 
			} else {
				toAcctTypeValue = "CDA";
				tValue =3;
				 
        if(toAcctNo.length == 10){
        toAcctNo="00000000000000"+toAcctNo;
        }
        if(toAcctNo.length == 16){
        toAcctNo="00000000"+toAcctNo;
        } 
			}
			var fromAcctNo=frmBillPaymentConfirmationFuture.lblAccountNum.text;
			fromAcctNo = removeHyphenIB(fromAcctNo);
			var fromAccType;
			if (fromAcctNo.length == 14) {
			        fromAccType = "SDA";
			    } else {
			        fourthDigit = fromAcctNo.charAt(3);
			        if (fourthDigit == "2" || fourthDigit == "7" || fourthDigit == "9") {
			            fromAccType = "SDA";
			            fromAcctNo="0000"+fromAcctNo;
			        } else {
			            fromAccType = "DDA";
			        }
			    }
			if(fromAccType == "SDA"){
				 fValue =2;
			} else{
				 	fValue =1;
				 }
			
		if(billerMethod==2	|| billerMethod==3	|| billerMethod==4){
		var amount=parseFloat(removeCommos(frmBillPaymentConfirmationFuture.lblAmountValue.text))
			
			inputParam["fromAcctNo"]=fromAcctNo;
			inputParam["fromAcctTypeValue"]=fromAccType;
			inputParam["toAcctNo"]=toAcctNo;
			inputParam["toAcctTypeValue"]=toAcctTypeValue;
			inputParam["transferAmt"]=amount.toFixed(2);
			inputParam["tranCode"]="88"+fValue+tValue;
			inputParam["transferDate"]=getTodaysDate();	
					
			invokeServiceSecureAsync("TransferAdd", inputParam, BillPaymentTransferAddServiceCallBack);
		}

}
/*
 * CallBack of BillPaymentAddService
 * 
 */
function BillPaymentAddServiceCallBack(status,resulttable){

	if(status==400){
		
		
		if(resulttable["opstatus"] == 0)
		 {
		 	
		 	// fetching To Account Name for TMB Inq
		 	var StatusCode=resulttable["StatusCode"];
		 	var Severity=resulttable["Severity"];
		 	var StatusDesc=resulttable["StatusDesc"];
		 	
		 	if(StatusCode!="0"){
		 	dismissLoadingScreen();
		 	var statusErr= "";
		 	if(resulttable["additionalDS"] != undefined && resulttable["additionalDS"].length>3){
		 		statusErr = resulttable["additionalDS"][2]["StatusDesc"];
		 	}
		 		alert(resulttable["StatusDesc"]+" "+statusErr);
		 		//alert("----callBackcheckBillPaymentAddMB status code:"+StatusCode)
		 		return false;
		 	}
		 	else{
		 		callBillPaymentCrmProfileUpdateService();
		 	}
		 }
		else
		 {
		 	dismissLoadingScreen();
			alert(" "+resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		 } 
	}

}
/*
 * CallBack of BillPaymentTransferAddServiceCallBack
 * 
 * 
 */
function BillPaymentTransferAddServiceCallBack(status,resulttable){

	if(status==400){
		
		
		if(resulttable["opstatus"] == 0)
		 {
		 	
		 	// fetching To Account Name for TMB Inq
		 	var StatusCode=resulttable["StatusCode"];
		 	var Severity=resulttable["Severity"];
		 	var StatusDesc=resulttable["StatusDesc"];
		 	
		 	if(StatusCode!="0"){
				dismissLoadingScreen();		 		
		 		alert(" "+resulttable["errMsg"]);
		 		return false;
		 	}
		 	else{
		 		callBillPaymentCrmProfileUpdateService();
		 	}
		 }
		else
		 {	dismissLoadingScreen();
			alert(" "+resulttable["errMsg"]);
		 } 
	}

}
/*
 * Method BillPaymentCRMProfileUpdateService
 * 
 */
function callBillPaymentCrmProfileUpdateService(){
	inputParam = {};
	//var daliyLimit=DailyLimit
	//gblEmailIdBillerNotification="";
	inputParam["actionType"]="0";
	
    
	if(gblPaynow){
	inputParam["ebAccuUsgAmtDaily"]=(UsageLimit+totalBillPayAmt).toFixed(2);
	} else {
	
	inputParam["ebAccuUsgAmtDaily"]=UsageLimit.toFixed(2);
	
	}
    
   
    
    showLoadingScreen();
	invokeServiceSecureAsync("crmProfileMod", inputParam, callBillPaymentCrmProfileUpdateServiceCallBack);

}
/*
 * CallBack of BillPaymentCRMProfileUpdateService
 * 
 */
function callBillPaymentCrmProfileUpdateServiceCallBack(status,resulttable){
//
 				
		if(status==400){
			 var activityTypeID = "";
             var errorCode = "";
             var activityStatus = "";
             var deviceNickName = "";
             var activityFlexValues5 = "";
             var activityFlexValues1 = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.substring(0, frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text.indexOf("("));
             var activityFlexValues2 = frmBillPaymentConfirmationFuture.lblAccountNum.text; 
             var activityFlexValues3 = frmBillPaymentConfirmationFuture.lblRef1Value.text;
             
             var amt = parseFloat(frmBillPaymentConfirmationFuture.lblAmountValue.text); 
             var fee = parseFloat(frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text);
             var activityFlexValues4 = amt.toString()+ "+" + fee.toString();
             var logLinkageId = "";
             if(gblPaynow){
             activityTypeID = "027";
             }
             else {
             activityTypeID = "028";
             }
		
		
		if(resulttable["opstatus"] == 0)
		 {
			 
		 	
		 	// fetching To Account Name for TMB Inq
		 	var StatusCode=resulttable["StatusCode"];
		 	var Severity=resulttable["Severity"];
		 	var StatusDesc=resulttable["StatusDesc"];
		 //	gblEmailIdBillerNotification = resulttable["emailAddr"]
		 	
		 	activityStatus = "01";
		 	gotoBillPaymentCompleteMB(resulttable);
		 	callBillPaymentNotificationAddService(); //not working at present
		 	
		 }
		else
		 {
		 	activityStatus = "02";
		 	dismissLoadingScreen();
			alert(" "+resulttable["errMsg"]);
			/**dismissLoadingScreen();**/
		 } 
	activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId)
	if(gblPaynow) {
	setFinancialActivityLogForBillPaymentMB(frmBillPaymentConfirmationFuture.lblTxnNumValue.text,frmBillPaymentConfirmationFuture.lblAmountValue.text,frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text,activityStatus);
	}
	else{
	//gblPaynow=true;
	}
    
	}
}
/*
 * Method CallBillPaymentNotificationAddService
 * 
 */
function callBillPaymentNotificationAddService() {
    var inputParams = {};
    platformChannel =  gblDeviceInfo.name;
    if (platformChannel == "thinclient")
        inputParams["channelId"] = "Internet Banking";
    else
        inputParams["channelId"] ="Mobile Banking";

	if(gblPaynow){
	inputParams["source"]="billpaymentnow";
	}
	else{
	inputParams["source"]="FutureBillPaymentAndTopUp";
	inputParams["initiationDt"] = frmBillPaymentConfirmationFuture.lblStartOnValue.text; 
	inputParams["todayTime"] = "11:59:59 PM"; 
	inputParams["recurring"] = frmBillPaymentConfirmationFuture.lblEveryMonth.text 
	inputParams["endDt"] = frmBillPaymentConfirmationFuture.lblEndOnValue.text 
	//inputParams["mynote"] = frmIBBillPaymentConfirm.lblMyNoteVal.text
	}
	
    inputParams["customerName"] = gblCustomerName;
    inputParams["fromAccount"] = frmBillPaymentConfirmationFuture.lblAccountNum.text;
    inputParams["fromAcctNick"] = frmBillPaymentConfirmationFuture.lblAccountName.text;
    inputParams["billerNick"] = frmBillPaymentConfirmationFuture.lblBillerNickname.text;
    inputParams["billerName"] = frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text;
    inputParams["ref1"] = frmBillPaymentConfirmationFuture.lblRef1Value.text;
    if(frmBillPaymentConfirmationFuture.hbxRef2.isVisible){
    
    	 ref2 = frmBillPaymentConfirmationFuture.lblRef2Value.text;
    	 if(kony.i18n.getCurrentLocale() == "en_US"){
       		inputParams["ref2"] = "<br/>Ref.2 : "+ref2;
    		 }else{
      	 inputParams["ref2"] = "<br/>รหัสอ้างอิงที่ 2 : "+ref2;
    	 }
     }
     else{
       inputParams["ref2"] = "";
     }
 
    //inputParams["ref2"] = frmBillPaymentConfirmationFuture.lblRef2Value.text;
    inputParams["amount"] = frmBillPaymentConfirmationFuture.lblAmountValue.text;
    inputParams["fee"] = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
    inputParams["date"] = frmBillPaymentConfirmationFuture.lblPaymentDateValue.text;
    inputParams["refID"] = frmBillPaymentConfirmationFuture.lblTxnNumValue.text;
    inputParams["memo"] = frmBillPaymentConfirmationFuture.lblMyNoteValue.text;
	
    inputParams["deliveryMethod"] = "Email";
    inputParams["noSendInd"] = "0";
   // inputParams["emailId"] = gblEmailIdBillerNotification;//
    inputParams["notificationType"] = "Email";
    inputParams["Locale"] = kony.i18n.getCurrentLocale();
    inputParams["notificationSubject"] = "billpay";
    inputParams["notificationContent"] = "billPaymentDone";
    //alert("calling NotificationAdd ");
    //showLoadingScreen();
    
    invokeServiceSecureAsync("NotificationAdd", inputParams, callBillPaymentNotificationAddServiceCallBack);


}
/*
 * Method CallBack of BillPaymentNotificationAddServiceCallBack
 * 
 */

function callBillPaymentNotificationAddServiceCallBack(status,resutlttable){

	if(status==400){
		
		
		if(resutlttable["opstatus"] == 0)
		 {
		 	
		 	
		 	
		 	var StatusCode=resutlttable["StatusCode"];
		 	var Severity=resutlttable["Severity"];
		 	var StatusDesc=resutlttable["StatusDesc"];
		 	
		 	if(StatusCode!="0"){
		 		//alert("----callBackFundTransferInqMB status code:"+StatusCode)
		 		return false;
		 	}else{
		 //	gotoBillPaymentCompleteMB();
		 	}
		 }
		 else
		 {
			//alert(" "+resutlttable["errMsg"]);
			/**dismissLoadingScreen();**/
		 } 
	}



}

function onClickBillPayConfirmPop(){
	if (popTransactionPwd.tbxPopTransactionPwd.text==""){
		popTransactionPwd.lblPopTranscationMsg.text="Please input transaction password"
		return false;
	}
	else 
		checkBillPayVerifyPWDMB();
		//transferFlowAckMB()
}
/*
************************************************************************

	Module	: checkVerifyPWDMB
	Author  : Kony
	Purpose : updating channel limit of Crmprofile

****/
function checkBillPayVerifyPWDMB(){
	/**if(popTransactionPwd.tbxPopTransactionPwd.text!="12345"){
		if(gblVerifyOTP <= 1){
			popTransactionPwd.tbxPopTransactionPwd.text="";
			showAlertForSplitTransfers("Please Enter Valid Password",constants.ALERT_TYPE_CONFIRMATION,"",kony.i18n.getLocalizedString("keyYes"),"No",verifyOTP)
		}
		else{ 
			checkCrmProfileUpadateMB();
			popTransferConfirmOTPLock.show();
			popTransactionPwd.dismiss();
		}
	}
	else{
		popTransactionPwd.dismiss();
		transferFlowAckMB()
	}**/
	alert("hi in password verify");
	if(gblRC_QA_TEST_VAL==0){
		
	}
	else{
		
		var inputParam ={}
		inputParam["loginModuleId"] = "MB_TxPwd";
		inputParam["retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
		inputParam["retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
		inputParam["userStoreId"] = "DefaultStore";
		inputParam["userId"] = "";
		inputParam["password"] = popTransactionPwd.tbxPopTransactionPwd.text;
		inputParam["sessionVal"] = "";
		inputParam["segmentId"] = "segmentId";
		//var entries = JSON.stringify([{"segmentId":"MIB"}]);
	    inputParam["segmentIdVal"] = "MIB";
	    popTransactionPwd.tbxPopTransactionPwd.text="";
	    invokeServiceSecureAsync("verifyPassword", inputParam, callBackBillPayVerifyPWDMB)
	    
		//invokeServiceSecureAsync("verifyOTP", inputParam, callBackVerifyPWDMB)
	}
} 

/*
*************************************************************************************
		Module	: callBackNotificationAddMB
		Author  : Kony
		Date    : May 05, 2013
		Purpose : callback function for callBackNotificationAddMB
****************************************************************************************
*/
function callBackBillPayVerifyPWDMB(status, resulttable)
{
alert("----callBackBillPayVerifyPWDMB status code:");
}		


function setFinancialActivityLogForBillPaymentMB(finTxnRefId,finTxnAmount,finTxnFee,finTxnStatus){
	
	GBLFINANACIALACTIVITYLOG.finTxnRefId=finTxnRefId;
	

	 var tranCode;
    var fromAccType = frmBillPaymentConfirmationFuture.lblAccountName.text;
    if (fromAccType == "DDA") {
        tranCode = "8810";
    } else {
        tranCode = "8820";
    }
	
    if(gblPaynow){
		GBLFINANACIALACTIVITYLOG.activityTypeId="027";
	}else{
		GBLFINANACIALACTIVITYLOG.activityTypeId="028";
	}
	//02 is for mobile banking
	GBLFINANACIALACTIVITYLOG.channelId="02"
	var fromAcctId = frmBillPaymentConfirmationFuture.lblAccountNum.text
	fromAcctId = removeHyphenIB(fromAcctId);
	GBLFINANACIALACTIVITYLOG.fromAcctId = fromAcctId;
	//GBLFINANACIALACTIVITYLOG.toAcctId= "";
	GBLFINANACIALACTIVITYLOG.toBankAcctCd="11";
	GBLFINANACIALACTIVITYLOG.finTxnAmount=parseFloat(removeCommos(finTxnAmount)).toFixed(2);
	GBLFINANACIALACTIVITYLOG.txnCd=tranCode;
	GBLFINANACIALACTIVITYLOG.finTxnFee= parseFloat(removeCommos(finTxnFee)).toFixed(2);
	var availableBal=frmBillPaymentConfirmationFuture.lblBalBeforePayValue.text;
	availableBal=availableBal.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"")
	availableBal=availableBal.replace(",","")
	availableBal=parseFloat(availableBal.trim());
	if(finTxnStatus =="01"){
		availableBal=availableBal-(parseFloat(finTxnAmount)+parseFloat(finTxnFee));
		GBLFINANACIALACTIVITYLOG.clearingStatus="01"
	}
	else {
	GBLFINANACIALACTIVITYLOG.clearingStatus="02"
	}
	GBLFINANACIALACTIVITYLOG.finTxnBalance=availableBal.toFixed(2);
	GBLFINANACIALACTIVITYLOG.noteToRecipient=""
	GBLFINANACIALACTIVITYLOG.recipientMobile="";
	GBLFINANACIALACTIVITYLOG.recipientEmail=""
	// 01 for success 02 for fail
	GBLFINANACIALACTIVITYLOG.finTxnStatus=finTxnStatus
	GBLFINANACIALACTIVITYLOG.smartFlag="0"
	GBLFINANACIALACTIVITYLOG.clearingStatus="00"
	GBLFINANACIALACTIVITYLOG.fromAcctName=frmBillPaymentConfirmationFuture.lblAccountName.text+""
	
	GBLFINANACIALACTIVITYLOG.toAcctNickname=frmBillPaymentConfirmationFuture.lblBillerNickname.text;
	var billerNameCmpCode =frmBillPaymentConfirmationFuture.lblBillerNameCompCode.text;
	var billerNAmeSplit = billerNameCmpCode.split("(");
	GBLFINANACIALACTIVITYLOG.toAcctName=billerNAmeSplit[0];
	GBLFINANACIALACTIVITYLOG.errorCd="0000000000000";
	var yearPadd = new Date().getFullYear().toString().substring(2,2);
	var refNumber = finTxnRefId.substring(2, finTxnRefId.length-2)
	GBLFINANACIALACTIVITYLOG.eventId=yearPadd+refNumber;
	GBLFINANACIALACTIVITYLOG.billerBalance="0";
	GBLFINANACIALACTIVITYLOG.txnType="002";
	GBLFINANACIALACTIVITYLOG.billerRef1=frmBillPaymentConfirmationFuture.lblRef1Value.text;
	GBLFINANACIALACTIVITYLOG.billerRef2=frmBillPaymentConfirmationFuture.lblRef2Value.text;
	GBLFINANACIALACTIVITYLOG.billerCustomerName=frmBillPaymentConfirmationFuture.lblBillerNickname.text;
	GBLFINANACIALACTIVITYLOG.fromAcctNickname="";//frmBillPaymentConfirmationFuture.lblAccountName.text+"";
	GBLFINANACIALACTIVITYLOG.finTxnMemo =frmBillPaymentConfirmationFuture.lblMyNoteValue.text; 
	GBLFINANACIALACTIVITYLOG.tellerId  ="4";
	//GBLFINANACIALACTIVITYLOG.txnDescription ="done"; 
	GBLFINANACIALACTIVITYLOG.finLinkageId  = "1";
	GBLFINANACIALACTIVITYLOG.billerCommCode  =frmSelectBiller["segMyBills"]["selectedItems"][0]["BillerCompCode"].text;
	GBLFINANACIALACTIVITYLOG.finSchduleRefId  ="SB" + kony.string.sub(finTxnRefId, 2, finTxnRefId.length);// "SB"+finTxnRefId;
	
	financialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG);
	
	}
