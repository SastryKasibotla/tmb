function init_eKycErrorScreen(){
  frmMBeKycSuccessNerror.preShow = frmMBeKycSuccessNerror_Preshow_Action;
}

function frmMBeKycSuccessNerror_Preshow_Action(){
  if(gbleKYCStatus == "SUCCESS"){
    frmMBeKycSuccessNerror.imagestatusicon.src = "greencomplete.png"
	frmMBeKycSuccessNerror.lblMessage1.setVisibility(true);
    frmMBeKycSuccessNerror.lblMessage3.setVisibility(true);
    frmMBeKycSuccessNerror.lblMessage2.setVisibility(false);
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_titleCompleteAcctOpening"),null,false,null);
  	setFooterSinglebtnToForm(kony.i18n.getLocalizedString("eKYC_btnStart"),onClickStartNewAccBtn);
  }else if(gbleKYCStatus == "ERROR"){
    frmMBeKycSuccessNerror.imagestatusicon.src = "icon_failed.png"
    frmMBeKycSuccessNerror.lblMessage1.text = kony.i18n.getLocalizedString("eKYC_msgOpenAcctFailed");
    frmMBeKycSuccessNerror.lblMessage3.setVisibility(false);
    frmMBeKycSuccessNerror.lblMessage2.setVisibility(true);
    setHeaderContentToForm(kony.i18n.getLocalizedString("eKYC_titleError"),null,false,null);
  	setFooterSinglebtnToForm(kony.i18n.getLocalizedString("Home"),onClickHomeBtn);
  }
  
}


function onClickHomeBtn(){
  eKYCLogoutService();
}

function onClickStartNewAccBtn(){
  onClickOfStartBtnForMB();
  //alert("Start Page");
}
function calleKYC_eKYCCreateRMOpenAcctCompositeService(text){
  try{
    showLoadingScreen();
    var inputParam = {};
    if(text != ""){
          gblAccessPineKYC = text;
      }
      var enCryptAccessPin = encryptData(gblAccessPineKYC);
      glbAccessPin = gblAccessPineKYC;
      kony.print("enCryptAccessPin :: " + enCryptAccessPin)
      inputParam["marketConsent"] = gbleKYCConsentflag;
      inputParam["password"] = enCryptAccessPin;
      inputParam["deviceId"] = getDeviceID();
    invokeServiceSecureAsync("eKYCCreateRMOpenAcctCompositeService", inputParam, callBack_eKYCCreateRMOpenAcctCompositeService);
  }catch(e){
    kony.print("@@@ readeConsentServiceeKYC() Exeption:::"+e);
  }
}
function callBack_eKYCCreateRMOpenAcctCompositeService(status, result){
  try{
    kony.print("Result:::::::::"+JSON.stringify(result));
     if (status == 400) {
       kony.print("Inside callBack_eKYCCreateRMOpenAcctCompositeService :: " + result["opstatus"])
        if (result["opstatus"] == 0 && isNotBlank(result["flow_status"]) && result["flow_status"] == "OPEN_ACCT_COMPLETE") {
          gblRMOpenAcct = true;
          gblStartClickFromActivationMB=true;
          encryptSecretKey(result["secretKey"], result["encryptKey"]);
          gblRMOpenAcctresult = result;
          if(GLOBAL_UV_STATUS_FLAG == "ON"){
               try {
                  registerAppWithPushNotificationsInfrastructure();
                }catch(e){
                  kpns_log("registerAppWithPushNotificationsInfrastructure: Exception : "+e.message);
                  showUVActivationFail("Push Registration Failed");
                }
              
      		}else{
              //removing the KSID for reactivation.
              kony.store.removeItem("ksid");
              kony.store.removeItem("ksidUpdated");
              kony.store.removeItem("ksidsavedDate");
              //
              dismissLoadingScreen();
              SuccessPineKYC(gblRMOpenAcctresult);
      		}
        }else if (result["opstatus"] == "1") {
          dismissLoadingScreen();
          kony.print("Inside opstatus 1");
              if(result["errCode"] == "E10020" || result["errMsg"] == "Wrong Password" ){
                kony.print("Inside Else 1");
                //Wrong password Entered 
                  resetPINeKYC();
                  var badLoginCount = result["badLoginCount"];
                  var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
                  incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
                  if(badLoginCount == 1 || badLoginCount == 2){
                    frmMBeKYCAccessPin.lblErrormsg.text = kony.i18n.getLocalizedString("eKYC_msgIncorrectAccessPIN");
                    frmMBeKYCAccessPin.flexError.setVisibility(true);
                    frmMBeKYCAccessPin.CopyflxHeader0ad9fbe4694f14f.setVisibility(false);
                  }else if(badLoginCount >= 3){
                    gbleKYCStatus = "ERROR";
                    frmMBeKYCAccessPin.flexError.setVisibility(false);
                    frmMBeKYCAccessPin.CopyflxHeader0ad9fbe4694f14f.setVisibility(true);
                    frmMBeKycSuccessNerror.lblMessage2.text = kony.i18n.getLocalizedString("eKYC_msgAccessPINLockedDetails");
                    frmMBeKycSuccessNerror.show();
                  }
              }else if(result["errCode"] == "E10403" || result["errMsg"] == "Password Locked" ){
                kony.print("Inside Else 2");
                  gbleKYCStatus = "ERROR";
                  frmMBeKYCAccessPin.flexError.setVisibility(false);
                  frmMBeKYCAccessPin.CopyflxHeader0ad9fbe4694f14f.setVisibility(true);
                  frmMBeKycSuccessNerror.lblMessage2.text = kony.i18n.getLocalizedString("eKYC_msgAccessPINLockedDetails");
                  frmMBeKycSuccessNerror.show();
              }else{
                kony.print("Inside Else 3");
                  gbleKYCStatus = "ERROR";
                  frmMBeKYCAccessPin.flexError.setVisibility(false);
                  frmMBeKYCAccessPin.CopyflxHeader0ad9fbe4694f14f.setVisibility(true);
                  frmMBeKycSuccessNerror.lblMessage2.text = kony.i18n.getLocalizedString("eKYC_msgServiceDownDetails");
                  frmMBeKycSuccessNerror.show();
              } 
		}else{
          kony.print("Inside Else 4");
          dismissLoadingScreen();
          gbleKYCStatus = "ERROR";
          frmMBeKYCAccessPin.flexError.setVisibility(false);
          frmMBeKYCAccessPin.CopyflxHeader0ad9fbe4694f14f.setVisibility(true);
          frmMBeKycSuccessNerror.lblMessage2.text = kony.i18n.getLocalizedString("eKYC_msgServiceDownDetails");
          frmMBeKycSuccessNerror.show();
        } 
     } 
   }catch(e){
    kony.print("@@@ readeConsentServiceeKYC() Exeption:::"+e);
  }
}
function SuccessPineKYC(result){
  gbleKYCStatus = "SUCCESS"
  var cardFee = kony.i18n.getLocalizedString("eKYC_msgDebitCardRemark");
  cardFee = cardFee.replace("{debitCardFee}",result["debitCardFee"]);
  cardFee = cardFee.replace("{debitCardAnnualFee}",result["debitCardAnnualFee"]);
  frmMBeKycSuccessNerror.lblMessage1.text = productName;
  
  var productName = kony.i18n.getLocalizedString("eKYC_msgCompleteAcctOpening");
  var locale = kony.i18n.getCurrentLocale();
  if (locale == "en_US") {
    productName = productName.replace("{productName}",result["productNameEN"]);
  }else{
    productName = productName.replace("{productName}",result["productNameTH"]);
  }
  frmMBeKycSuccessNerror.lblMessage3.text = cardFee;
  frmMBeKYCAccessPin.flexError.setVisibility(false);
  frmMBeKYCAccessPin.CopyflxHeader0ad9fbe4694f14f.setVisibility(false);
  frmMBeKycSuccessNerror.show();
}
function eKYCLogoutService() {
	showLoadingScreen();
	inputParam = {};
	inputParam["channelId"] = "02";
	inputParam["timeOut"] = GBL_Time_Out;
	inputParam["deviceId"] = getDeviceID();	

	invokeServiceSecureAsync("logOutTMB", inputParam, callBackeKYCLogoutService);
}

function callBackeKYCLogoutService(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      dismissLoadingScreen();
     	frmeKYCStartUp.show();
    }
  }
}