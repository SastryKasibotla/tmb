function partyInqforKYCOpenAccount(){
    var inputParams = {};
    showLoadingScreenPopup();
    inputParams["viewFlag"] = "OpenAccount";    
    invokeServiceSecureAsync("MyProfileViewCompositeService", inputParams, partyInqforKYCOpenAccountCallBack);
}

function partyInqforKYCOpenAccountCallBack(status,resulttable){
	if (status == 400) //success response
    {
      	dismissLoadingScreenPopup();
      	hideKYCAllArrows();
      	if(resulttable["opstatus"] == 0) {
      		gblMyProfileDataOpenAccount=resulttable;
      		if (resulttable["customerNameTH"] != null) {
      			frmIBOpenAccountContactKYC.lblThaiNameValue.text = resulttable["customerNameTH"];
      		}
      		if (resulttable["customerName"] != null) {
      			frmIBOpenAccountContactKYC.lblEngNameValue.text = resulttable["customerName"];
      		}
      		if (resulttable["emailAddr"] != null) {
                    gblEmailAddr = resulttable["emailAddr"];
                    gblEmailAddrOld = gblEmailAddr;
                    frmIBOpenAccountContactKYC.lblEmailValue.text = gblEmailAddr;
            }
			for (var i = 0; i < resulttable["ContactNums"].length; i++) {
				var PhnType = resulttable["ContactNums"][i]["PhnType"];
							   
				if (PhnType != null && PhnType != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined) {
					if (PhnType == "Mobile") {
						if ((resulttable["ContactNums"][i]["PhnNum"] != null) && (resulttable["ContactNums"][i]["PhnNum"] != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined)) {
							gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
							gblPHONENUMBEROld = gblPHONENUMBER;
							GblMobileAL = HidePhnNum(gblPHONENUMBER);
							
							frmIBOpenAccountContactKYC.lblMobileNoValue.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
						}
					}
				}
            }
			if(resulttable["Persondata"]!= null && resulttable["Persondata"]!= undefined){
				for (var i = 0; i < resulttable["Persondata"].length; i++) {
					var addressType = resulttable["Persondata"][i]["AddrType"];
					if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                        var adr3 = resulttable["Persondata"][i]["addr3"];
                        var reg = / {1,}/;
                        var tempArr = [];
                        tempArr = adr3.split(reg);
                        
                        var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
						var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
						var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
						var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";
						
						if (addressType == "Primary") {
							if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                                    if(tempArr[0].indexOf(subDistBan, 0) >= 0)
										gblsubdistrictValue = tempArr[0].substring(4);
									else if(tempArr[0].indexOf(subDistNotBan, 0) >= 0) 
											gblsubdistrictValue = tempArr[0].substring(2);
										 else gblsubdistrictValue = "";	
									if(tempArr[1].indexOf(distBan, 0) >= 0)  	 
										gbldistrictValue = tempArr[1].substring(3);
									else if(tempArr[1].indexOf(distNotBan, 0) >= 0) 
											gbldistrictValue = tempArr[1].substring(2); 
										 else 	gbldistrictValue = "";
                                } 
                            } else {
                                gblsubdistrictValue = kony.i18n.getLocalizedString('keyIBPleaseSelect');
                                gbldistrictValue = kony.i18n.getLocalizedString('keyIBPleaseSelect');
                            }
                            if (tempArr[0] == undefined || tempArr[0] == "" || tempArr[0] == null || tempArr[0] == "undefined") gblViewsubdistrictValue = "";
                            else gblViewsubdistrictValue = tempArr[0];
                            if (tempArr[1] == undefined || tempArr[1] == "" || tempArr[1] == null || tempArr[1] == "undefined") gblViewdistrictValue = "";
                            else gblViewdistrictValue = tempArr[1];
                            if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined){
								gblStateValue = resulttable["Persondata"][i]["City"];
                            }else{
                            	gblStateValue = "";
                            }
							if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
								gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
							else 	
								gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
							if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
								gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
							else	gblcountryCode = "";
							if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                gblnotcountry = true;
                            } else {
                                gblnotcountry = false;
                            } 
							gblAddress1Value = resulttable["Persondata"][i]["addr1"];
							gblAddress2Value = resulttable["Persondata"][i]["addr2"];
							frmIBOpenAccountContactKYC.lblContactAddressLine1.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue;
                            frmIBOpenAccountContactKYC.lblContactAddressLine2.text = gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
							if(gblStateValue == ""){
								gblStateValue = kony.i18n.getLocalizedString('keyIBPleaseSelect');
							}
						}else if (addressType == "Registered") {
                        
                        //alert("registered address..."); 
                        if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                var adr3 = resulttable["Persondata"][i]["addr3"];
                                //alert(adr3);
                                var reg = / {1,}/;
                                var tempArr = [];
                                tempArr = adr3.split(reg);
                                //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 
								{
                                    if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                        if (resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != undefined && resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
                                            gblregsubdistrictValue = tempArr[0] //.substring(3);
                                            gblregdistrictValue = tempArr[1] //.substring(3);
                                        } else {
                                            gblregsubdistrictValue = tempArr[0] //.substring(2);
                                            gblregdistrictValue = tempArr[1] //.substring(2);
                                        }
                                    }
                                }
                                gblregAddress1Value = resulttable["Persondata"][i]["addr1"];
                                gblregAddress2Value = resulttable["Persondata"][i]["addr2"];
                                if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "" || resulttable["Persondata"][i]["City"] != undefined) {
                                    gblregStateValue = resulttable["Persondata"][i]["City"];
                                } else {
                                    gblregStateValue = "";
                                }
                                //gblregStateValue = resulttable["Persondata"][i]["City"];
                                if(resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != undefined)
                                	gblregzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                                else   gblregzipcodeValue = "";	
                                if(resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)
                                	gblregcountryCodeIB = resulttable["Persondata"][i]["CountryCodeValue"];
                                else 	gblregcountryCodeIB = "";
                                frmIBOpenAccountContactKYC.lblRegisterAddressLine1.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue;
                                frmIBOpenAccountContactKYC.lblRegisterAddressLine2.text = gblregdistrictValue + " " + " " + gblregStateValue + " " + " " + gblregzipcodeValue + " " + " " + gblregcountryCodeIB;
                                //
                            }
                        }
                    }
					}
					frmIBOpenAccountContactKYC.show();	
				}
			
			}
		}
	}
}

function languageToggleForOpenAccountContactKYC(){
	frmIBOpenAccountContactKYC.txtChangeMobileNumber.placeholder=kony.i18n.getLocalizedString("keyNewMobileNum");
	if(frmIBOpenAccountContactKYC.hbxcnf2.isVisible)
		frmIBOpenAccountContactKYC.btnConfirm.text=kony.i18n.getLocalizedString("keyConfirm");
	else
		frmIBOpenAccountContactKYC.btnConfirm.text=kony.i18n.getLocalizedString("keysave");
}

function onClickKYCbtnEditContactAddress(){
	hideKYCAllArrows();
	if(getCRMLockStatus())
	{
		curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}else{
		frmIBOpenAccountContactKYC.imgEditAddressArrow.setVisibility(true);
		frmIBOpenAccountContactKYC.hbximage.setVisibility(false);
		editAddressForOpenAccount();
		frmIBOpenAccountContactKYC.txtemailvalue.text=gblEmailAddr;
	}	
}

function onClickKYCbtnEditEmail(){
	hideKYCAllArrows();
	if(getCRMLockStatus())
	{
		curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}else{
		frmIBOpenAccountContactKYC.imgEditEmailArrow.setVisibility(true);
		frmIBOpenAccountContactKYC.hbximage.setVisibility(false);
		editAddressForOpenAccount();
		//frmIBOpenAccountContactKYC.hboxEdit.setVisibility(true);		-- done in data population
		//frmIBOpenAccountContactKYC.hbxEditEmailAddressCancelConfirm.setVisibility(true);
		frmIBOpenAccountContactKYC.txtemailvalue.text=gblEmailAddr;
	}	
}

function onClickKYCbtnEditMobileNo(){
	hideKYCAllArrows();
	if(getCRMLockStatus())
	{
		curr_form=kony.application.getCurrentForm().id;
		popIBBPOTPLocked.show();
	}else{
		frmIBOpenAccountContactKYC.imgEditMobileNoArrow.setVisibility(true);
		frmIBOpenAccountContactKYC.hbxChangeMobileNumber.setVisibility(true);
		frmIBOpenAccountContactKYC.hbxChangeMobileCancelNext.setVisibility(true);
		frmIBOpenAccountContactKYC.hbximage.setVisibility(false);
		frmIBOpenAccountContactKYC.txtChangeMobileNumber.text="";
		frmIBOpenAccountContactKYC.txtChangeMobileNumber.setEnabled(true);
	}	
}

function hideKYCAllArrows(){
	frmIBOpenAccountContactKYC.imgEditAddressArrow.setVisibility(false);
	frmIBOpenAccountContactKYC.imgEditEmailArrow.setVisibility(false);
	frmIBOpenAccountContactKYC.imgEditMobileNoArrow.setVisibility(false);
	frmIBOpenAccountContactKYC.hbximage.setVisibility(true);
	//Mobile change related reset
	frmIBOpenAccountContactKYC.hbxChangeMobileNumber.setVisibility(false);
	frmIBOpenAccountContactKYC.hbxOtpBox.setVisibility(false);
	frmIBOpenAccountContactKYC.hbxChangeMobileCancelNext.setVisibility(false);
	//Email Address related reset
	frmIBOpenAccountContactKYC.hboxEdit.setVisibility(false);
	frmIBOpenAccountContactKYC.hbxcnf2.setVisibility(false);
	frmIBOpenAccountContactKYC.hbxEditEmailAddressCancelConfirm.setVisibility(false);
}

function isOpenAccountKYCRelated(){
	if(kony.application.getCurrentForm().id == "frmIBOpenAccountContactKYC"){
		return true;
	}	
	return false;
}

function populateDataKYCOccupationInfo(){
	if(undefined != gblMyProfileDataOpenAccount && gblMyProfileDataOpenAccount !=""){
		var curr_locale=kony.i18n.getCurrentLocale();
		frmIBOpenAccountOccupationKYC.lblOfficeNameValue.text = gblMyProfileDataOpenAccount["CompanyName"]!=undefined?gblMyProfileDataOpenAccount["CompanyName"]:"-";
		if(curr_locale == "en_US"){
			frmIBOpenAccountOccupationKYC.lblOccupationValue.text = gblMyProfileDataOpenAccount["OccupationEN"]!=undefined?gblMyProfileDataOpenAccount["OccupationEN"]:"-";
			frmIBOpenAccountOccupationKYC.lblSourceOfIncomeValue.text = gblMyProfileDataOpenAccount["SourceOfIncomeEN"]!=undefined?gblMyProfileDataOpenAccount["SourceOfIncomeEN"]:"-";
			frmIBOpenAccountOccupationKYC.lblCountryOfIncomeValue.text = gblMyProfileDataOpenAccount["CountryOfIncomeEN"]!=undefined?gblMyProfileDataOpenAccount["CountryOfIncomeEN"]:"-";
		}else{
			frmIBOpenAccountOccupationKYC.lblOccupationValue.text = gblMyProfileDataOpenAccount["OccupationTH"]!=undefined?gblMyProfileDataOpenAccount["OccupationTH"]:"-";
			frmIBOpenAccountOccupationKYC.lblSourceOfIncomeValue.text = gblMyProfileDataOpenAccount["SourceOfIncomeTH"]!=undefined?gblMyProfileDataOpenAccount["SourceOfIncomeTH"]:"-";
			frmIBOpenAccountOccupationKYC.lblCountryOfIncomeValue.text = gblMyProfileDataOpenAccount["CountryOfIncomeTH"]!=undefined?gblMyProfileDataOpenAccount["CountryOfIncomeTH"]:"-";
		}
		
		if(gblMyProfileDataOpenAccount["Persondata"]!= null && gblMyProfileDataOpenAccount["Persondata"]!= undefined){
			for(var i = 0; i < gblMyProfileDataOpenAccount["Persondata"].length; i++){
				var addressType = gblMyProfileDataOpenAccount["Persondata"][i]["AddrType"];
				if(addressType == "Office"){
					var officeAddrLine1=gblMyProfileDataOpenAccount["Persondata"][i]["addr1"]!=undefined?gblMyProfileDataOpenAccount["Persondata"][i]["addr1"]:"";
					var officeAddrLine2=gblMyProfileDataOpenAccount["Persondata"][i]["addr2"]!=undefined?gblMyProfileDataOpenAccount["Persondata"][i]["addr2"]:"";
					var officeAddrLine3=gblMyProfileDataOpenAccount["Persondata"][i]["addr3"]!=undefined?gblMyProfileDataOpenAccount["Persondata"][i]["addr3"]:"";
					var officeCity=gblMyProfileDataOpenAccount["Persondata"][i]["City"]!=undefined?gblMyProfileDataOpenAccount["Persondata"][i]["City"]:"";
					var officePostalCode=gblMyProfileDataOpenAccount["Persondata"][i]["PostalCode"]!=undefined?gblMyProfileDataOpenAccount["Persondata"][i]["PostalCode"]:"";
					var officeCountry=gblMyProfileDataOpenAccount["Persondata"][i]["CountryCodeValue"]!=undefined?gblMyProfileDataOpenAccount["Persondata"][i]["CountryCodeValue"]:"";
					
					frmIBOpenAccountOccupationKYC.lblOfficeAddressLine1.text=officeAddrLine1+" "+officeAddrLine2+" "+officeAddrLine3;
                	frmIBOpenAccountOccupationKYC.lblOfficeAddressLine2.text=officeCity+" "+officePostalCode+" "+officeCountry;
                	frmIBOpenAccountOccupationKYC.hbox474136158370823.setVisibility(true);
                	break;
				}else{
					frmIBOpenAccountOccupationKYC.lblOfficeAddressLine1.text="-";
                	frmIBOpenAccountOccupationKYC.lblOfficeAddressLine2.text="-";
                	frmIBOpenAccountOccupationKYC.hbox474136158370823.setVisibility(false);
				}
			}
		}
	}
	frmIBOpenNewSavingsAcc.lblFromAccount.text ="";
	frmIBOpenNewSavingsAcc.lblAccNo.text="";
	frmIBOpenNewSavingsAcc.imageAcc.src="";
	frmIBOpenNewSavingsAcc.imageAcc.setVisibility(false);	
	frmIBOpenNewSavingsAcc.vbox866794452610619.setVisibility(true);
}

function onClickBackKYCOccupationInfo(){
	hideKYCAllArrows();
	frmIBOpenAccountContactKYC.show();
}

function onClickKYCConfirm(){
	var inputParam={};
	invokeServiceSecureAsync("MyProfileViewCompositeServiceKYCOpenAcc", inputParam, onClickKYCConfirmCallBack);
}
function onClickKYCConfirmCallBack(){}

function validateOTPKYCAddressEmailChange() {

    var currForm = kony.application.getCurrentForm();
    if (gblTokenSwitchFlag == true) {
        var otpToken = frmIBOpenAccountContactKYC.tbxToken.text;
        
        if (otpToken != null) otpToken = otpToken.trim();
        else {
              frmIBOpenAccountContactKYC.tbxToken.text = "";
              alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
              return false;
        }
        if (otpToken == "") {
            frmIBOpenAccountContactKYC.tbxToken.text = "";
            alert(kony.i18n.getLocalizedString("Receipent_tokenId"));
            return false;
        }
			verifyOTPOpenAccountKYCAddressEmailChange(otpToken); 
    } else {
        var otpText = frmIBOpenAccountContactKYC.txtBxOTP.text;
        if (otpText != null) otpText = otpText.trim();
        else {
        		frmIBOpenAccountContactKYC.txtBxOTP.text = "";
                alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
                return false;
        }
        if (otpText == "") {
        		frmIBOpenAccountContactKYC.txtBxOTP.text = "";
                alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
                return false;
        }
			verifyOTPOpenAccountKYCAddressEmailChange(otpText); 
    }
}

function verifyOTPOpenAccountKYCAddressEmailChange(otptext) {
    var locale = kony.i18n.getCurrentLocale();
    showLoadingScreenPopup();
    var compositeEditAddressEmailIB_inputParam = {}
    // sending the global params 
    var addressLine1 = frmIBOpenAccountContactKYC.txtAddress1.text.trim();
    var addressLine2 = frmIBOpenAccountContactKYC.txtAddress2.text.trim();
    
   	province = StateValue;
    district = districtValue;
    subdistrict = subdistrictValue;
    zipcode = zipcodeValue;
    
    
    //Sending old values for Activity logging
    compositeEditAddressEmailIB_inputParam["globalvar_gblAddressOld"] = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " "  + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue+ " " + gblcountryCode;
    compositeEditAddressEmailIB_inputParam["globalvar_UnMaskGBLemailALOld"] = gblEmailAddr;
    //End Activity Logging params
    
    //OTP or Token related
    if (gblTokenSwitchFlag == true){
     	// TOKEN
     	compositeEditAddressEmailIB_inputParam["verifyToken_loginModuleId"] = "IB_HWTKN";
     	compositeEditAddressEmailIB_inputParam["verifyToken_userStoreId"] = "DefaultStore";
        compositeEditAddressEmailIB_inputParam["verifyToken_retryCounterVerifyAccessPin"] = "0";
        compositeEditAddressEmailIB_inputParam["verifyToken_retryCounterVerifyTransPwd"] = "0";
        compositeEditAddressEmailIB_inputParam["verifyToken_userId"] = gblUserName;
        
        compositeEditAddressEmailIB_inputParam["verifyToken_sessionVal"] = "";
        compositeEditAddressEmailIB_inputParam["verifyToken_segmentId"] = "segmentId";
        compositeEditAddressEmailIB_inputParam["verifyToken_segmentIdVal"] = "MIB";
        compositeEditAddressEmailIB_inputParam["verifyToken_channel"] = "Internet Banking";
     	 
    } else {
    	compositeEditAddressEmailIB_inputParam["verifyPwd_loginModuleId"] = "IBSMSOTP",
    	 //OTP
        compositeEditAddressEmailIB_inputParam["verifyPwd_retryCounterVerifyOTP"] = "0",
        compositeEditAddressEmailIB_inputParam["verifyPwd_userId"] = gblUserName,
        compositeEditAddressEmailIB_inputParam["verifyPwd_userStoreId"] = "DefaultStore",
        compositeEditAddressEmailIB_inputParam["verifyPwd_segmentId"] = "MIB"
    }
    compositeEditAddressEmailIB_inputParam["TokenSwitchFlag"] = gblTokenSwitchFlag;
    compositeEditAddressEmailIB_inputParam["notificationAdd_appID"] = appConfig.appId;
    compositeEditAddressEmailIB_inputParam["password"] = otptext;
    compositeEditAddressEmailIB_inputParam["locale"] = locale;
    //End OTP
    
    compositeEditAddressEmailIB_inputParam["crmProfileMod_emailAddr"] = frmIBOpenAccountContactKYC.clblEmailVal.text.trim();
    //address flag
    //sending addresschnage
    var locale = kony.i18n.getCurrentLocale();
    
    var primaryaddrtype = "Primary";
    compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_Addr1"] = addressLine1;
    compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_Addr2"] = addressLine2;
    if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
        compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district;
    } else {
        compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district;
    }
    compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_City"] = province;
    compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_StateProv"] = gblnewStateValue;
    compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_PostalCode"] = zipcode;
    compositeEditAddressEmailIB_inputParam["partyUpdateInputMap_CountryCodeValue"] = kony.i18n.getLocalizedString("Thailand");
    invokeServiceSecureAsync("openAcctContactDetailsCompositeJavaService", compositeEditAddressEmailIB_inputParam, openAcctContactDetailsCompositeJavaServicecallBackIB)
}

function openAcctContactDetailsCompositeJavaServicecallBackIB(status,resulttable){
	if(status == 400){
		var curr_form = frmIBOpenAccountContactKYC;
		curr_form.tbxToken.text = "";
        curr_form.txtBxOTP.text = "";
		dismissLoadingScreenPopup();
		if(resulttable["opstatus"]==0){
			partyInqforKYCOpenAccount();
		}else if (resulttable["opstatus"] == 8005) {
			flagMobNum = "old";
			curr_form.txtBxOTP.text = "";
			curr_form.tbxToken.text = "";
			if (resulttable["errCode"] == "VrfyOTPErr00001") {
				dismissLoadingScreenPopup();
				curr_form.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone"); 
				curr_form.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
				curr_form.hbxOTPincurrect.isVisible = true;
				curr_form.hbox476047582127699.isVisible = false;
				curr_form.hbxOTPsnt.isVisible = false;
				curr_form.txtBxOTP.text = "";
				curr_form.tbxToken.text = "";
				if (gblTokenSwitchFlag == true) curr_form.tbxToken.setFocus(true);
				else curr_form.txtBxOTP.setFocus(true);
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00002") {
				dismissLoadingScreenPopup();
				handleOTPLockedIB(resulttable);
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00005") {
				dismissLoadingScreenPopup();
				alert(kony.i18n.getLocalizedString("invalidOTP"));
				return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00006") {
				gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
				if (resulttable["errMsg"] != undefined) {
					alert("" + resulttable["errMsg"]);
				}
				return false;
			} else if (resulttable["errCode"] == "GenOTPRtyErr00001") {
                dismissLoadingScreenPopup();
                showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
			} else if (resulttable["errCode"] == "VrfyOTPErr00004") {
				alert("" + resulttable["errMsg"]);
				return false;
			} else {
				dismissLoadingScreenPopup();
				alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
				return false;
			}
		} else{
            if (resulttable["errMsg"] != undefined) {
                alert("" + resulttable["errMsg"]);
            } else {
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
	}
}