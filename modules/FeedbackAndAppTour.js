







var formOnClickBackAppTour = null;
var FeedbackOriginal = null;
/*
 * 1 = show
 * 2 = remind later
 * 3 = no thanks
 */
var FeedbackLater = "2";

//MB FUNCTIONS==================================================================================


function showFeedbackPage(eo){
  	kony.print("Inside showFeedbackPage start");
  // Added undefined check for deeplink feedback flow as no eventObj in this flow
	if(eo == undefined || eo.id == "button506459299445013"){ 
		frmContactUsMB.lblFeel.text = kony.i18n.getLocalizedString("keyApplicationFeel") + "?";
	}
	else if(eo.id == "hbox50285458168"){
		frmContactUsMB.lblFeel.text = kony.i18n.getLocalizedString("keyServiceFeel") + "?";
		frmContactUsMB.show();
	}
	frmContactUsMB.hbxContact.isVisible = false;
	frmContactUsMB.line47592361418663.isVisible = false;
	frmContactUsMB.line47592361418559.isVisible = false;
	frmContactUsMB.hbxFAQ.isVisible = false;
	frmContactUsMB.hbxFB.isVisible = false;
	frmContactUsMB.hbxFindTMB.isVisible = false;
	frmContactUsMB.hbxPostLogin.isVisible = false;
	frmContactUsMB.hboxContactSend.isVisible = false;
	frmContactUsMB.hbxFeedback.isVisible = true;
	frmContactUsMB.hboxFeedBackSend.isVisible = true;
	frmContactUsMB.hbxFB1.skin = "hboxFBBlue";
	frmContactUsMB.hbxFB2.skin = "hboxFBGrey";
	frmContactUsMB.hbxFB3.skin = "hboxFBGrey";
	frmContactUsMB.btn1.skin = "btnDBOzoneXMedWhite142";//def152
    frmContactUsMB.btn2.skin = "btnDBOzoneXMedBlack142";
    frmContactUsMB.btn3.skin = "btnDBOzoneXMedBlack142";
	frmContactUsMB.btn1.focusSkin = "btnDBOzoneXMedWhite142";
    //frmContactUsMB.btn2.focusSkin = "btnDBOzoneXMedWhite142";
    //frmContactUsMB.btn3.focusSkin = "btnDBOzoneXMedWhite142";
	frmContactUsMB.btn2.focusSkin = "btnDBOzoneXMedBlack142";
    frmContactUsMB.btn3.focusSkin = "btnDBOzoneXMedBlack142";
	
	
	//frmContactUsMB.lblheader.text = kony.i18n.getLocalizedString("keyFeedback");
	FeedbackOriginal = "-1";
  kony.print("Inside showFeedbackPage end");
}

function onFeedbackSelection(eventobject){
	var form = kony.application.getCurrentForm();
	var blue="hboxFBBlue",grey="hboxFBGrey";
	var black="btnDBOzoneXMedBlack142",white="btnDBOzoneXMedWhite142";
	if(form.id == "frmIBContactUs"){
		blue="hboxFBBlueIB";
		grey="hboxFBGreyIB";
		black="btnIB20pxBlack";
		white="btnIB20pxWhite";
	}
	if(eventobject.id == "hbxFB1" || eventobject.id == "btn1"){
		form.hbxFB1.skin = blue;//"hboxFBBlue";
		form.hbxFB1.focusSkin = blue;
		form.hbxFB2.skin = grey;//"hboxFBGrey";
		form.hbxFB3.skin = grey;//"hboxFBGrey";
		form.btn1.skin = white;
		form.btn1.focusSkin = white;
		form.btn2.skin = black;
		form.btn2.focusSkin = black;
		form.btn3.skin = black;
		form.btn3.focusSkin = black;
		FeedbackOriginal = "1";
	}
	if(eventobject.id == "hbxFB2" || eventobject.id == "btn2"){
		form.hbxFB2.skin = blue;//"hboxFBBlue";
		form.hbxFB2.focusSkin = blue;
		form.hbxFB1.skin = grey;//"hboxFBGrey";
		form.hbxFB3.skin = grey;//"hboxFBGrey";
		form.btn2.skin = white;
		form.btn2.focusSkin = white;
		form.btn1.skin = black;
		form.btn1.focusSkin = black;
		form.btn3.skin = black;
		form.btn3.focusSkin = black;
		FeedbackOriginal = "2";
	}
	if(eventobject.id == "hbxFB3" || eventobject.id == "btn3"){
		form.hbxFB3.skin = blue;//"hboxFBBlue";
		form.hbxFB3.focusSkin = blue;
		form.hbxFB1.skin = grey;//"hboxFBGrey";
		form.hbxFB2.skin = grey;//"hboxFBGrey";
		form.btn3.skin = white;
		form.btn3.focusSkin = white;
		form.btn2.skin = black;
		form.btn2.focusSkin = black;
		form.btn1.skin = black;
		form.btn1.focusSkin = black;
		FeedbackOriginal = "3";
	}
}

function updateFeedbackInDBMB(){
	var form = kony.application.getCurrentForm();
	var OS = "1";
	var osVersion = gblDeviceInfo["version"];
	var msgAdditional = "NULL";
	var channel = "02";
	if(form.id == "frmIBContactUs"){
		OS = "3";
		osVersion = "NULL";
		channel = "01";
	}
	else if(gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad"){
		OS = "2";
	}
	var feedbackmsgTosend = null;
	if(flowSpa)
		feedbackmsgTosend = form.tbxMessageSPA.text;
	else
		feedbackmsgTosend = form.tbxMessage.text;

	if(feedbackmsgTosend != null && feedbackmsgTosend.trim() != ""){
		
		msgAdditional = feedbackmsgTosend;
	}
//	else{
//		alert(kony.i18n.getLocalizedString("cntText"));
//        return false;
//	}
	if(FeedbackOriginal == "-1"){
		FeedbackOriginal = "1";
	}
	var appVersion = "NULL";
	if(appConfig.appVersion != null){
		appVersion = appConfig.appVersion.substring(0, 10);
	}
	var inputParams = {
						deviceId 			: "NULL",
						customerType 		: "1",
						scoreLevel 			: FeedbackOriginal,
						satisfactionNotes 	: msgAdditional,
						osType				: OS,
						osVersion			: osVersion,
						appVersion			: appVersion,
						source				: "NULL",
						mychannel			: channel,
						deviceBrowserName	: "NULL",
						functionId			: "NULL"
					  };
	if(form.id == "frmIBContactUs"){
		showLoadingScreenPopup();
		invokeServiceSecureAsync("saveCustomerSatisfactionScore", inputParams, updateFeedbackInDBIBCB);
	}
	else{
		showLoadingScreen();
		invokeServiceSecureAsync("saveCustomerSatisfactionScore", inputParams, updateFeedbackInDBMBCB);
	}
}

function updateFeedbackInDBMBCB(status, resultset){
	if(status == 400){
		if(resultset["opstatus"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
		}
		else if(resultset["opstatus"] == "0"){
			if(resultset["feedback"] != null){
				if(resultset["feedback"] == "1" || resultset["feedback"] == "2"){
					if(FeedbackLater == "2"){
						popFeedBack.label476108006293987.text = kony.i18n.getLocalizedString("keyThankYou");
						popFeedBack.label476108006293988.text = kony.i18n.getLocalizedString("keyRateTMB");
						popFeedBack.button476108006293989.text = kony.i18n.getLocalizedString("keyRateTMB1");
						popFeedBack.button476108006294001.text = kony.i18n.getLocalizedString("keyRemindLater");
						popFeedBack.button476108006294007.text = kony.i18n.getLocalizedString("keyNoThanks");
						
						popFeedBack.show();
					}
					else{
						frmFeedbackComplete.show();
					}
				}
				else if(resultset["feedback"] == "3"){
					frmFeedbackComplete.show();
				}
			}
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
		}
		kony.application.dismissLoadingScreen();
	}
}

function showAppTourForm() {
	if (isSignedUser == false) {

		formOnClickBackAppTour = "frmMBPreLoginAccessesPin"; //frmAfterLogoutMB
		var getEncrKeyFromDevice = kony.store.getItem("encrytedText");
		if (getEncrKeyFromDevice != null) {
			formOnClickBackAppTour = "frmMBPreLoginAccessesPin" //frmAfterLogoutMB
		} else {
			formOnClickBackAppTour = "frmMBanking"
		}

	} else {
		formOnClickBackAppTour = "";
	}
	if (gblDeviceInfo.name == "android" || gblDeviceInfo.name == "iphone" || flowSpa) {
		if (gblTourFlag) {
			frmAppTour.show();
		} else if (kony.i18n.getCurrentLocale() == "en_US"){
			kony.application.openURL(kony.i18n.getLocalizedString("keyAppTourEN"))
		} else if (kony.i18n.getCurrentLocale() == "th_TH"){
			kony.application.openURL(kony.i18n.getLocalizedString("keyAppTourTH"))
		}				
	} else {
		frmAppTour.show();
	}
}

function AppTourOnClickBack(){
	//
//	if (formOnClickBackAppTour == "frmSPALogin") {
//        formOnClickBackAppTour = "";
//        frmSPALogin.show();
//    }else if(formOnClickBackAppTour == "frmAfterLogoutMB"){
//		formOnClickBackAppTour="";
//		frmAfterLogoutMB.show();
//	}else if(formOnClickBackAppTour == "frmMBanking"){
//		formOnClickBackAppTour="";
//		frmMBanking.show();
//	}
//	
//	else{
//		//Need to be navigate to AccountSummary
//		//if (isSignedUser == true) {
////        	showAccountSummaryFromMenu.call(this);
////    	} else {}
//
//		//var form = kony.application.getPreviousForm();
//		//form.show();
//		frmAppTour.show();
//	}
if(isMenuShown == false){
	var previousForm = kony.application.getPreviousForm();
	previousForm.show();
}
}

function frmAppTour_PreShow(){
	frmAppTour.scrollboxMain.scrollToEnd();
	
	gblIndex = -1;
	isMenuShown = false;
	//var CurrentformId = "frmAppTour";
	//DisableFadingEdges(CurrentformId);

	frmAppTourPreShow();
}
//IB FUNCTIONS==================================================================================

function showFeedbackPageIB(eo){
	if(eo.id == "btnFeedBack"){
		frmIBContactUs.lblFeel.text = kony.i18n.getLocalizedString("keyApplicationFeel") + "?";
	}
	else if(eo.id == "hbox50285458168"){
		frmIBContactUs.lblFeel.text = kony.i18n.getLocalizedString("keyServiceFeel") + "?";
		frmIBContactUs.show();
	}
	frmIBContactUs.btnContactUs.skin = "btnTab2LeftNormal";
  	frmIBContactUs.btnFeedBack.skin = "btnTab2RightFocus";
	frmIBContactUs.hbxContactIB.setVisibility(false);
	//frmIBContactUs.label506459299513312.isVisible = false;
	//frmIBContactUs.hbxRadioBtn.isVisible = false;
	//frmIBContactUs.button506459299513301.isVisible = false;
	frmIBContactUs.hbox476108006294054.isVisible = true;
	frmIBContactUs.hbxFB1.isVisible = true;
	frmIBContactUs.hbxFB2.isVisible = true;
	frmIBContactUs.hbxFB3.isVisible = true;
	//frmIBContactUs.hbxFB1.skin = "hboxFBBlue";
	//frmIBContactUs.hbxFB2.skin = "hboxFBGrey";
	//frmIBContactUs.hbxFB3.skin = "hboxFBGrey";
	frmIBContactUs.hbxFB1.skin = "hboxFBBlueIB";
	frmIBContactUs.hbxFB2.skin = "hboxFBGreyIB";
	frmIBContactUs.hbxFB3.skin = "hboxFBGreyIB";
	frmIBContactUs.btn1.skin = "btnIB20pxWhite";
	frmIBContactUs.btn1.focusSkin = "btnIB20pxWhite";
	frmIBContactUs.btn2.skin = "btnIB20pxBlack";
	frmIBContactUs.btn2.focusSkin = "btnIB20pxWhite";
	frmIBContactUs.btn3.skin = "btnIB20pxBlack";
	frmIBContactUs.btn3.focusSkin = "btnIB20pxWhite";
	frmIBContactUs.label477746511291445.isVisible = true;
	frmIBContactUs.tbxMessage.text = null;
	frmIBContactUs.tbxMessage.isVisible = true;
	frmIBContactUs.tbxMessage.placeholder = kony.i18n.getLocalizedString("cnt_textMessage");
	frmIBContactUs.hbox98089341265246.isVisible = false;
	frmIBContactUs.hbxCntImage.isVisible = true;
	frmIBContactUs.btnSend.setVisibility(true);
	frmIBContactUs.line473367234297752.isVisible = false;
	//frmIBContactUs.label506459299571357.text = kony.i18n.getLocalizedString("keyFeedback"); - DEF14641
	FeedbackOriginal = "-1";
}

function updateFeedbackInDBIBCB(status, resultset){
	if(status == 400){
		if(resultset["opstatus"] == "1"){
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
		}
		else if(resultset["opstatus"] == "0"){
			//hbox4768527816204

			frmIBContactUs.hbox98089341265246.isVisible = false;
			frmIBContactUs.hbxCntImage.isVisible = true;
			frmIBContactUs.hbox98089341292096.isVisible = true;
			frmIBContactUs.hbxCntCmplt.isVisible = false;
			frmIBContactUs.hbxFeedComplt.isVisible = false;
			frmIBContactUs.hbxContactIB.isVisible = false;
			frmIBContactUs.hbox98089341292088.isVisible = true;
			frmIBContactUs.hbox506459299512370.isVisible = false;
			frmIBContactUs.hbox476108006294054.isVisible = false;
			frmIBContactUs.hbxFB1.isVisible = false;
			frmIBContactUs.hbxFB2.isVisible = false;
			frmIBContactUs.hbxFB3.isVisible = false;
			frmIBContactUs.btnSend.isVisible = false;
			frmIBContactUs.label477746511291445.isVisible = false;
			frmIBContactUs.tbxMessage.isVisible = false;
			
			/*frmIBContactUs.hbxCntImage.setVisibility(true);
				frmIBContactUs.hbxCntCmplt.setVisibility(false);
				frmIBContactUs.hbxFeedComplt.setVisibility(false);
				
				frmIBContactUs.hbox98089341265246.setVisibility(false);
			    frmIBContactUs.hbox98089341292096.setVisibility(false);
			    frmIBContactUs.hbxContactIB.setVisibility(false);
			    frmIBContactUs.hbox506459299512370.setVisibility(false);
			    frmIBContactUs.label506459299571357.setVisibility(false);*/
		}
		else{
			showAlertRcMB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("Receipent_alert_Error"), "error");
		}
		dismissLoadingScreenPopup();
	}
}

function showAppTourFormIB(eo){
		kony.application.openURL(kony.i18n.getLocalizedString("linkSiteTour"));
}
