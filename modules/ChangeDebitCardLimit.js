var isSuccessDebitLimitChange = false;
gblPaywaveUsage = "Off";
gblEcommerceUsage = "Off";
gblOverseaUsage = "Off";


function initfrmMBChangeDebitCardLimit(){
  try{
  	   	frmMBManageChangeDebitCardLimit.preShow = preshowfrmMBChangeDebitCardLimit;
 	    frmMBManageChangeDebitCardLimit.onDeviceBack = disableBackButton;  
	}catch(e){
    kony.print("@@@ In initfrmMBChangeDebitCardLimit() Exception:::"+e);
	}
}

function initfrmMBManageCardLimit(){
  try{
    kony.print("@@@ In initfrmMBManageCardLimit() @@@");
    frmMBManageCardLimit.preshow = preShowfrmMBManageCardLimit;
    frmMBManageCardLimit.sliderCardLimit.onSlide = onSlideManageCardLimt;
    frmMBManageCardLimit.btnBack.onClick = onCancelManageCardLimt;
    frmMBManageCardLimit.btnSave.onClick = onSaveManageCardLimt;
    frmMBManageCardLimit.btnCancel.onClick = onCancelManageCardLimt;
    frmMBManageCardLimit.onDeviceBack = disableBackButton;
    frmMBManageCardLimit.btnMinus.onClick = onClickSliderMinus;
    frmMBManageCardLimit.btnPlus.onClick = onClickSliderPlus;
  }catch(e){
    kony.print("@@@ In initfrmMBManageCardLimit() Exception:::"+e);
  }
}

function showfrmMBManageChangeDebitCardLimit(){
  try{
  invokeCardOperInq();
  }catch(e){
    kony.print("@@@ In frmMBManageChangeDebitCardLimit() Exception:::"+e);
  }
}
// Access pin logic for Debit card limit

function invokeDebitcardCahngeApproval(pinValue){
    var inputParam = {};
	showLoadingScreen();
	kony.print("invokeAccessPinVerification >>"+pinValue);
  	kony.print("@@@gblCACardRefNumber:::"+gblCACardRefNumber);
    var atmAmount = frmMBManageDebitCardConfirmation.lblAMtCashWithLimit.text;
    var edcAmount =frmMBManageDebitCardConfirmation.lblAmtEDCOnlineSpendLimit.text;
  
  	inputParam["cardRefId"] = gblCACardRefNumber;
  
    if(isNotBlank(pinValue)){
      inputParam["password"] =  encryptData(pinValue);  
    }
    if(isNotBlank(atmAmount)){
	    inputParam["atmWithdrawlimit"] = parseFloat(removeCommos(atmAmount));
	}
	if(isNotBlank(edcAmount)){
	  	inputParam["posWithdrawlimit"] = parseFloat(removeCommos(edcAmount));
	} 
    if(gblOverseaUsage == "Off"){
      	inputParam["overseaFlag"] = "N";
    }else{
        inputParam["overseaFlag"] ="Y"
    }
  	if(gblPaywaveUsage == "Off"){
      	inputParam["payWaiveFlag"] ="N"
    }else{
        inputParam["payWaiveFlag"] ="Y"
    }
  	if(gblEcommerceUsage == "Off"){
      	inputParam["ecommFlag"] = "N"
    }else{
        inputParam["ecommFlag"] ="Y"
    }
  
  	invokeServiceSecureAsync("cardLimitSettingCompositeService", inputParam, callBackinvokeDebitCardLimitAccessPinVerification);
}


function callBackinvokeDebitCardLimitAccessPinVerification(status, resulttable){
  try{
  	if (status == 400) {
        kony.print("Printing the result for callBackinvokeDebitCardLimitAccessPinVerification "+JSON.stringify(resulttable));
        if (resulttable["opstatus"] == 0) {
            dismissLoadingScreen();
            kony.print("Form id>>"+kony.application.getCurrentForm().id);
          	if(kony.application.getCurrentForm().id == "frmMBManageDebitCardConfirmation"){
             	kony.print("if the current form is frmMBManageDebitCardConfirmation ");
 				closeApprovalKeypad();
                 kony.print("After closing keyPadApprol fun ");
            }
            isSuccessDebitLimitChange = true;
            frmMBManageDebitCardConfirmation.show();
        } else if (resulttable["opstatus"] == 1) {
            if(resulttable["errCode"] == "E10020" || resulttable["errMsg"] == "Wrong Password" ){
              //Wrong password Entered 
            	resetKeypadApproval();
            	var badLoginCount = resulttable["badLoginCount"];
            	var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
            	incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
            	frmMBManageDebitCardConfirmation.lblForgotPin.text = incorrectPinText;
            	frmMBManageDebitCardConfirmation.lblForgotPin.skin = "lblBlackMed150NewRed";
            	frmMBManageDebitCardConfirmation.lblForgotPin.onTouchEnd = doNothing;
                dismissLoadingScreen();
              
            }else if(resulttable["errCode"] == "E10403" || resulttable["errMsg"] == "Password Locked" ){
              // password Locked 
              closeApprovalKeypad();
              dismissLoadingScreen();
              //frmMBManageDebitCardFailure.rchAddress.text = kony.i18n.getLocalizedString("ChangeDBL_Failed");
              gotoUVPINLockedScreenPopUp();
              
            }else if(resulttable["isCardOpModFailed"] == "Y" ){
                  dismissLoadingScreen(); 
              	  if(resulttable["showTryAgain"] == "Y"){
                      closeApprovalKeypad()
                      frmMBManageDebitCardFailure.lblTryAgain.setVisibility(true);
                      frmMBManageDebitCardFailure.lblTryAgain.text =kony.i18n.getLocalizedString("btnTryAgain");
                      frmMBManageDebitCardFailure.lblTryAgain.onClick =invokeDebitcardCahngeApprovalTryAgain;
                   }else{
                      frmMBManageDebitCardFailure.lblTryAgain.setVisibility(false);
                   }
                   preshowfrmMBManageDebitCardFailure(); 
            }else if(resulttable["errCode"] == "MaxAttemptsReached"){
                dismissLoadingScreen();
              	closeApprovalKeypad();
              	var maxAttempts = resulttable["maxAttempts"];
            	var changeLimitRepeat = kony.i18n.getLocalizedString("ChangeLimit_Repeat");
            	changeLimitRepeat = changeLimitRepeat.replace("{maxAttempt}", maxAttempts);
              	showAlert(changeLimitRepeat, kony.i18n.getLocalizedString("info"));
            }else{
              closeApprovalKeypad();
              dismissLoadingScreen();
              showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
          
        }else{
          	dismissLoadingScreen();
        	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }

  }catch(e){
    kony.print("Exception in callBackinvokeDebitCardLimitAccessPinVerification:"+e);
  }
}

function invokeDebitcardCahngeApprovalTryAgain(){
    var inputParam = {};
	showLoadingScreen();
	
  	kony.print("@@@gblCACardRefNumber:::"+gblCACardRefNumber);
    var atmAmount = frmMBManageDebitCardConfirmation.lblAMtCashWithLimit.text;
    var edcAmount =frmMBManageDebitCardConfirmation.lblAmtEDCOnlineSpendLimit.text;
    var overseaFlag = frmMBManageDebitCardConfirmation.lblStatusOverseaUsage.text; 
   	var payWaiveFlag = frmMBManageDebitCardConfirmation.lblStatusPaywaveUsage.text;
    var ecommFlag = frmMBManageDebitCardConfirmation.lblStatusECommerceUsage.text;
    kony.print("@@@atmAmount:"+atmAmount);
  	kony.print("@@@edcAmount::"+edcAmount);
  	inputParam["cardRefId"] = gblCACardRefNumber;
    if(isNotBlank(atmAmount)){
	    atmAmount = parseFloat(removeCommos(atmAmount));
	}
	if(isNotBlank(edcAmount)){
	  	edcAmount = parseFloat(removeCommos(edcAmount));
	} 
    if(overseaFlag == "Off"){
      	overseaFlag ="N"
    }else{
        overseaFlag ="Y"
    }
  	if(payWaiveFlag == "Off"){
      	payWaiveFlag ="N"
    }else{
        payWaiveFlag ="Y"
    }
  	if(ecommFlag == "Off"){
      	ecommFlag ="N"
    }else{
        ecommFlag ="Y"
    }
  	kony.print("overseaFlag>>"+overseaFlag+">>payWaiveFlag>>"+payWaiveFlag+">>ecommFlag>>"+ecommFlag);
   	inputParam["atmWithdrawlimit"] = atmAmount;
    inputParam["posWithdrawlimit"] = edcAmount;
    inputParam["overseaFlag"] = overseaFlag;
    inputParam["payWaiveFlag"] = payWaiveFlag;
    inputParam["ecommFlag"] = ecommFlag;
  	invokeServiceSecureAsync("cardLimitSettingCompositeService", inputParam, callBackinvokeDebitCardLimitAccessPinVerification);
}

function invokeCardOperInq() {
  var inputParam = {};
  showLoadingScreen();
  kony.print("@@@gblCACardRefNumber:::"+gblCACardRefNumber);
  inputParam["cardRefId"] = gblCACardRefNumber;
  invokeServiceSecureAsync("cardOperInqComposite", inputParam, successCardOperInqService);
}
 
function successCardOperInqService(status, resulttable){
  try{
    kony.print("@@@ In successCardOperInqService() @@@");
    if(status == 400){
      if(resulttable["opstatus"] == 0){
        var cashLimit = resulttable["ATMWithdrawLimit"];
        var eocLimit = resulttable["POSWithdrawLimit"];
        var overseaUsage = resulttable["OverseaFlag"];
        var paywageUsage = resulttable["PayWaiveFlag"];
        var ecomUsage = resulttable["EcommFlag"];
        gblCardOperData = null;
        
        kony.print("@@@CashLimit:::"+cashLimit+"||EocLimit:::"+eocLimit);
        if(isNotBlank(cashLimit)){
          if(parseFloat(cashLimit) == 1){
            frmMBManageChangeDebitCardLimit.lblAmtCashWithLimit.text = "0 " +  kony.i18n.getLocalizedString("currencyThaiBaht");
          }else{
            frmMBManageChangeDebitCardLimit.lblAmtCashWithLimit.text = commaFormattedTransfer(parseFloat(cashLimit)+"") + " " +  kony.i18n.getLocalizedString("currencyThaiBaht");
          }
        }
        if(isNotBlank(eocLimit)){
          if(parseFloat(eocLimit) == 1){
            frmMBManageChangeDebitCardLimit.lblAmtEdcOnlineSpendLimit.text = "0 " +  kony.i18n.getLocalizedString("currencyThaiBaht");
          }else{
            frmMBManageChangeDebitCardLimit.lblAmtEdcOnlineSpendLimit.text = commaFormattedTransfer(parseFloat(eocLimit)+"") + " " +  kony.i18n.getLocalizedString("currencyThaiBaht");
          }
        }
        if(isNotBlank(overseaUsage) && overseaUsage == "Y"){
          frmMBManageChangeDebitCardLimit.imgOverseaUsage.src = "icon_on.png";
          gblOverseaUsage = "On";
        }else{
          frmMBManageChangeDebitCardLimit.imgOverseaUsage.src = "icon_off.png";
          gblOverseaUsage = "Off";
        }
        if(isNotBlank(paywageUsage) && paywageUsage == "Y"){
          frmMBManageChangeDebitCardLimit.imgPaywaveUsage.src = "icon_on.png";
          gblPaywaveUsage = "On";
        }else{
          frmMBManageChangeDebitCardLimit.imgPaywaveUsage.src = "icon_off.png";
          gblPaywaveUsage = "Off";
        }
        if(isNotBlank(ecomUsage) && ecomUsage == "Y"){
          frmMBManageChangeDebitCardLimit.imgEcommerceUsage.src = "icon_on.png";
          gblEcommerceUsage = "On";
        }else{
          frmMBManageChangeDebitCardLimit.imgEcommerceUsage.src = "icon_off.png";
          gblEcommerceUsage = "Off";
        }
        showChangeDebitCardOptions(resulttable);
        frmMBManageChangeDebitCardLimit.show();
        gblCardOperData = { "cashLimit": frmMBManageChangeDebitCardLimit.lblAmtCashWithLimit.text,
                          	"eocLimit": frmMBManageChangeDebitCardLimit.lblAmtEdcOnlineSpendLimit.text,
                            "overseaUsage": gblOverseaUsage,
                            "paywageUsage": gblPaywaveUsage,
                            "ecomUsage": gblEcommerceUsage };
        dismissLoadingScreen();
      }else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      }
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  }catch(e){
    dismissLoadingScreen();
    kony.print("@@@ In successCardOperInqService() Exception:::"+e);
  }
}

function showChangeDebitCardOptions(resulttable){
  try{
    kony.print("@@@ In showChangeDebitCardOptions() @@@");
    kony.print("@@@allowSetLimit:::"+gblSelectedCard["allowSetLimit"]);
    kony.print("@@@microCardFlag:::"+gblSelectedCard["microCardFlag"]);
    if(gblSelectedCard["allowSetLimit"] == "Y" && gblSelectedCard["microCardFlag"] == "Y"){
      frmMBManageChangeDebitCardLimit.flxCashWithLimit.setVisibility(false);
      frmMBManageChangeDebitCardLimit.flxEdcOnlineSpendLimit.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxOverseaUsage.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxPaywaveUsage.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxEcommerceUsage.setVisibility(false);
      frmMBManageChangeDebitCardLimit.flxEdcOnlineSpendLimit.top = "0%";
    }else if(gblSelectedCard["allowSetLimit"] == "Y" && gblSelectedCard["microCardFlag"] == "N"){
      frmMBManageChangeDebitCardLimit.flxCashWithLimit.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxEdcOnlineSpendLimit.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxOverseaUsage.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxPaywaveUsage.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxEcommerceUsage.setVisibility(true);
      frmMBManageChangeDebitCardLimit.flxEdcOnlineSpendLimit.top = "-1%";
    }
    kony.print("@@@cashWithDrawalLimit:"+resulttable["ATMWithdrawMaxLimit"]+"||edcLimit:::"+resulttable["POSWithdrawMaxLimit"]);
    if(isNotBlank(gblSelectedCard["cashWithDrawalLimit"]) && isNotBlank(resulttable["ATMWithdrawMaxLimit"])){
      var cashLimit = gblSelectedCard["cashWithDrawalLimit"];
      cashLimit = cashLimit.split("-");
      frmMBManageChangeDebitCardLimit.lblCashMinMax.text = cashLimit[0]+"-"+parseInt(resulttable["ATMWithdrawMaxLimit"]);
    }
    if(isNotBlank(gblSelectedCard["edcLimit"]) && isNotBlank(resulttable["POSWithdrawMaxLimit"])){
      var edcLimit = gblSelectedCard["edcLimit"];
      edcLimit = edcLimit.split("-");
      frmMBManageChangeDebitCardLimit.lblEDCMinMax.text = edcLimit[0]+"-"+parseInt(resulttable["POSWithdrawMaxLimit"]);
    }
  }catch(e){
    kony.print("@@@ In showChangeDebitCardOptions() Exception:::"+e);
  }
}

function preshowfrmMBChangeDebitCardLimit(){
    try{
    frmMBManageChangeDebitCardLimit.lblCashWithLimit.text= kony.i18n.getLocalizedString("DBL_CashWithdrawal");
    frmMBManageChangeDebitCardLimit.lblEdcOnlineSpendLimit.text= kony.i18n.getLocalizedString("DBL_EDCSpending");
    frmMBManageChangeDebitCardLimit.lblOverseaUsage.text= kony.i18n.getLocalizedString("DBL_OverseaUsage");
    frmMBManageChangeDebitCardLimit.lblPaywaveUsage.text= kony.i18n.getLocalizedString("DBL_PaywaveUsage");
    frmMBManageChangeDebitCardLimit.lblEcommerceUsage.text= kony.i18n.getLocalizedString("DBL_ECommerceUsage");
    frmMBManageChangeDebitCardLimit.lblChangeDebitCardLimitTitle.text= kony.i18n.getLocalizedString("DBL_ChangeDebitLimitTitle");
    frmMBManageChangeDebitCardLimit.btnBack.text= kony.i18n.getLocalizedString("CAV02_btnBack");
    frmMBManageChangeDebitCardLimit.btnNext.text= kony.i18n.getLocalizedString("CAV02_btnNext");
	//kony.print("@@@@@Image for card"+frmMBManageChangeDebitCardLimit.imgCard.src);
    frmMBManageChangeDebitCardLimit.imgCard.src= frmMBManageCard.imgCard.src;
    frmMBManageChangeDebitCardLimit.lblCardNumber.text=frmMBManageCard.lblCardNumber.text;
    frmMBManageChangeDebitCardLimit.lblCardAccountName.text=frmMBManageCard.lblCardAccountName.text;
	frmMBManageChangeDebitCardLimit.lblCardNumber.skin = getCardNoSkin(frmMBManageChangeDebitCardLimit.imgCard.src); //MKI, MIB-12137 start
    if(frmMBManageChangeDebitCardLimit.imgCard.src.toLowerCase().includes("debittmbwave")){      
		frmMBManageChangeDebitCardLimit.lblCardAccountName.skin = "lblBlackCard22px";
    }
    else{         
        frmMBManageChangeDebitCardLimit.lblCardAccountName.skin = "lblWhiteCard22px";
    } //MKI, MIB-12137 End
    frmMBManageChangeDebitCardLimit.imgCashWithLimit.src= "icon_limit_cash.png";
    frmMBManageChangeDebitCardLimit.imgEdcOnlineSpendLimit.src = "icon_limit_edc.png";
    frmMBManageChangeDebitCardLimit.imgOverseaUsage.src = "icon_limit_global.png";
    frmMBManageChangeDebitCardLimit.imgPaywaveUsage.src = "icon_limit_paywave.png";
    frmMBManageChangeDebitCardLimit.imgEcommerceUsage.src = "icon_limit_ecommerce.png";
    frmMBManageChangeDebitCardLimit.imgSelectPlanArrow.src = "icon_arrow_right.png";
    frmMBManageChangeDebitCardLimit.imgArrowBlue.src = "icon_arrow_right.png";
	
    if(gblOverseaUsage=="On"){
    frmMBManageChangeDebitCardLimit.imgRadioOverseaUsage.src = "icon_on.png";
 	}else{
    frmMBManageChangeDebitCardLimit.imgRadioOverseaUsage.src = "icon_off.png";
 	}

    if(gblPaywaveUsage=="On"){
    frmMBManageChangeDebitCardLimit.imgRadioPaywaveUsage.src = "icon_on.png";
 	}else{
    frmMBManageChangeDebitCardLimit.imgRadioPaywaveUsage.src = "icon_off.png";
 	}

    if(gblEcommerceUsage=="On"){
    frmMBManageChangeDebitCardLimit.imgRadioEcommerceUsage.src = "icon_on.png";
 	}else{
    frmMBManageChangeDebitCardLimit.imgRadioEcommerceUsage.src = "icon_off.png";
 	}

    frmMBManageChangeDebitCardLimit.imgRadioOverseaUsage.onTouchEnd = onClickRadioOverseaUsage;
    frmMBManageChangeDebitCardLimit.imgRadioPaywaveUsage.onTouchEnd = onClickRadioPaywaveUsage;
    frmMBManageChangeDebitCardLimit.imgRadioEcommerceUsage.onTouchEnd = onClickRadioEcommerceUsage;
    
    frmMBManageChangeDebitCardLimit.btnBack.onClick = onClickBackfrmMBManageChangeDebitCardLimit;
   // frmMBManageChangeDebitCardLimit.flxCashWithLimit.onClick = onClickSetCardLimt;
    //frmMBManageChangeDebitCardLimit.flxEdcOnlineSpendLimit.onClick = onClickSetCardLimt;

    frmMBManageChangeDebitCardLimit.btnNext.onClick = showfrmMBManageDebitCardConfirmation ;
   
	}catch(e){
    kony.print("@@@ In preshowfrmMBChangeDebitCardLimit() Exception:::"+e);
	}
}

function onClickRadioEcommerceUsage(){
 try
 {//code here
   //frmMBManageChangeDebitCardLimit.imgRadioEcommerceUsage.src = "icon_off.png";
   if(frmMBManageChangeDebitCardLimit.imgRadioEcommerceUsage.src == "icon_off.png"){
    frmMBManageChangeDebitCardLimit.imgRadioEcommerceUsage.src = "icon_on.png";
    gblEcommerceUsage = "On";
     
  }else{
    frmMBManageChangeDebitCardLimit.imgRadioEcommerceUsage.src = "icon_off.png";
    gblEcommerceUsage = "Off";
  }
  }catch(e){
    kony.print("@@@ In onClickRadioEcommerceUsage() Exception:::"+e);
	}
}

function onClickRadioOverseaUsage(){
	try{
  //frmMBManageChangeDebitCardLimit.imgRadioOverseaUsage.src = "icon_off.png";
    if(frmMBManageChangeDebitCardLimit.imgRadioOverseaUsage.src == "icon_off.png"){
    frmMBManageChangeDebitCardLimit.imgRadioOverseaUsage.src = "icon_on.png";
    gblOverseaUsage = "On";
  }else{
    frmMBManageChangeDebitCardLimit.imgRadioOverseaUsage.src = "icon_off.png";
    gblOverseaUsage = "Off";
  }
  }catch(e){
    kony.print("@@@ In onClickRadioOverseaUsage() Exception:::"+e);
	}
  
}

function onClickRadioPaywaveUsage(){
	try{
 // frmMBManageChangeDebitCardLimit.imgRadioPaywaveUsage.src = "icon_on.png";
    if(frmMBManageChangeDebitCardLimit.imgRadioPaywaveUsage.src == "icon_off.png"){
    frmMBManageChangeDebitCardLimit.imgRadioPaywaveUsage.src = "icon_on.png";
    gblPaywaveUsage = "On";
  }else{
    frmMBManageChangeDebitCardLimit.imgRadioPaywaveUsage.src = "icon_off.png";
    gblPaywaveUsage = "Off";
  }
  }catch(e){
    kony.print("@@@ In onClickRadioPaywaveUsage() Exception:::"+e);
	}
  
}

function onClickBackfrmMBManageChangeDebitCardLimit(){
  frmMBManageCard.show();
  
}

//Setting Debit Card Limit
function preShowfrmMBManageCardLimit(){
  try{
    kony.print("@@@ In preShowfrmMBManageCardLimit() @@@");
    frmMBManageCardLimit.lblManageCardTitle.text = kony.i18n.getLocalizedString("DBL_ChangeDebitLimitTitle");
    //frmMBManageCardLimit.lblCashWDLimit.text = kony.i18n.getLocalizedString("DBL_CashWithdrawal"); 
    frmMBManageCardLimit.lblMinText.text = kony.i18n.getLocalizedString("DBL_Minimum");
    frmMBManageCardLimit.lblMaxText.text = kony.i18n.getLocalizedString("DBL_Maximum");
    frmMBManageCardLimit.btnCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmMBManageCardLimit.btnSave.text = kony.i18n.getLocalizedString("keysave");
    
    if(gblDeviceInfo["name"] == "android"){
      frmMBManageCardLimit.sliderCardLimit.thumbOffset = 0;
	  frmMBManageCardLimit.sliderCardLimit.top = "8%";
	  frmMBManageCardLimit.sliderCardLimit.width = "112%";
    }else{
      //frmMBManageCardLimit.sliderCardLimit.top = "15%";
      //frmMBManageCardLimit.sliderCardLimit.width = "80%";
    }
  }catch(e){
    kony.print("@@@ In preShowfrmMBManageCardLimit() Exception:::"+e);
  }
}

function onClickSetCardLimt(flag){
  try{
    kony.print("@@@ In onClickSetCardLimt() @@@");
    gblEditAmountFlow = flag;
    kony.print("@@@gblEditAmountFlow:::"+gblEditAmountFlow);
    var amount = null;
    var minMaxAmount = null;
    var minAmount = null, maxAmount = null;
    if(isNotBlank(gblEditAmountFlow) && gblEditAmountFlow == "CASH"){
      frmMBManageCardLimit.lblCashWDLimit.text = kony.i18n.getLocalizedString("DBL_CashWithdrawal");
      amount = frmMBManageChangeDebitCardLimit.lblAmtCashWithLimit.text;
      minMaxAmount = frmMBManageChangeDebitCardLimit.lblCashMinMax.text;
    }
    if(isNotBlank(gblEditAmountFlow) && gblEditAmountFlow == "EDC"){
      frmMBManageCardLimit.lblCashWDLimit.text = kony.i18n.getLocalizedString("DBL_EDCSpending");
      amount = frmMBManageChangeDebitCardLimit.lblAmtEdcOnlineSpendLimit.text
      minMaxAmount = frmMBManageChangeDebitCardLimit.lblEDCMinMax.text;
    }
    kony.print("@@@Amount:::"+amount+"||Min-Max::"+minMaxAmount);
    
    if(isNotBlank(minMaxAmount) && minMaxAmount.indexOf("-") > -1){
      minMaxAmount = minMaxAmount.split("-");
      minAmount = minMaxAmount[0];
      maxAmount = minMaxAmount[1];
      kony.print("@@@MinVal::"+minAmount+"||MaxVal::"+maxAmount);
      if(isNotBlank(minAmount) && parseInt(minAmount) == 1){
        frmMBManageCardLimit.lblMinVal.text = 0;
      }else{
        frmMBManageCardLimit.lblMinVal.text = minAmount;
      }
      frmMBManageCardLimit.sliderCardLimit.min = 0;
      frmMBManageCardLimit.sliderCardLimit.max = parseInt(maxAmount);
      
      frmMBManageCardLimit.lblMaxVal.text = commaFormattedTransfer(maxAmount);
    }
    if(isNotBlank(amount)){
      amount = amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim(); //parseFloat(removeCommos(amount));
      kony.print("@@@Current Amount:::"+amount);
      frmMBManageCardLimit.sliderCardLimit.selectedValue = removeCommos(amount);
      frmMBManageCardLimit.lblLimitVal.text = amount;
      frmMBManageCardLimit.sliderCardLimit.step = 1000;
    }
    frmMBManageCardLimit.imgCard.src = frmMBManageChangeDebitCardLimit.imgCard.src;
    frmMBManageCardLimit.lblCardNumber.text = frmMBManageChangeDebitCardLimit.lblCardNumber.text;
    frmMBManageCardLimit.lblCardAccountName.text = frmMBManageChangeDebitCardLimit.lblCardAccountName.text;
    frmMBManageCardLimit.lblCardNumber.skin =getCardNoSkin(frmMBManageCardLimit.imgCard.src); //MKI, MIB-12137 start
    if(frmMBManageCardLimit.imgCard.src.toLowerCase().includes("debittmbwave")){      
		frmMBManageCardLimit.lblCardAccountName.skin = "lblBlackCard22px";
     }
     else{         
        frmMBManageCardLimit.lblCardAccountName.skin = "lblWhiteCard22px";
     }
    frmMBManageCardLimit.lblPrevAmount.text = amount; //Hidden variable to get the previous limit amount value.
    
    preShowfrmMBManageCardLimit();
    setMinusPlusButtons(minAmount, maxAmount);
    frmMBManageCardLimit.show();
  }catch(e){
    kony.print("@@@ In onClickSetCardLimt() Exception:::"+e);
  }
}

function setMinusPlusButtons(minAmount, maxAmount){
  try{
    kony.print("@@@ In setMinusPlusButtons() @@@");
    var currentAmt = frmMBManageCardLimit.lblLimitVal.text;
    if(isNotBlank(currentAmt)){
      currentAmt = removeCommos(currentAmt)
     
      kony.print("@@@currentAmt:::"+currentAmt+"||MinAmount:::"+minAmount+"|||MaxAmount:::"+maxAmount);
      //if(minAmount == "1000" || minAmount == 1000 ){
      if(currentAmt == "0" || currentAmt == 0 ){
        frmMBManageCardLimit.btnMinus.skin = "sknBtnMinusWhite";
        frmMBManageCardLimit.btnMinus.setEnabled(false);
      }else{
        frmMBManageCardLimit.btnMinus.skin = "sknBtnMinusBlue";
        frmMBManageCardLimit.btnMinus.setEnabled(true);
      }
      
      if(parseInt(currentAmt) >= parseInt(maxAmount)){
        frmMBManageCardLimit.btnPlus.skin = "sknBtnPlusWhite";
        frmMBManageCardLimit.btnPlus.setEnabled(false);
      }else{
        frmMBManageCardLimit.btnPlus.skin = "sknBtnPlusBlue";
        frmMBManageCardLimit.btnPlus.setEnabled(true);
      }
    }
  }catch(e){
    kony.print("@@@ In setMinusPlusButtons() Exception:::"+e);
  }
}


function onClickSliderMinus(){
  try{
    kony.print("@@@ In onClickSliderMinus() @@@");
    var currentAmt = frmMBManageCardLimit.lblLimitVal.text;
    if(isNotBlank(currentAmt)){
      currentAmt = parseInt(removeCommos(currentAmt));
      currentAmt = currentAmt - 1000;
      frmMBManageCardLimit.sliderCardLimit.selectedValue = currentAmt;
      kony.print("@@@currentAmt:::"+currentAmt);
      if(isNotBlank(currentAmt) && currentAmt <= 0 ){
        frmMBManageCardLimit.lblLimitVal.text = "0"; //commaFormattedTransfer(selVal+"");
        frmMBManageCardLimit.btnMinus.skin = "sknBtnMinusWhite";
        frmMBManageCardLimit.btnMinus.setEnabled(false);
      }else{
        frmMBManageCardLimit.lblLimitVal.text = commaFormattedTransfer(currentAmt+"");
        frmMBManageCardLimit.btnMinus.skin = "sknBtnMinusBlue";
        frmMBManageCardLimit.btnMinus.setEnabled(true);
      }
      frmMBManageCardLimit.btnPlus.skin = "sknBtnPlusBlue";
      frmMBManageCardLimit.btnPlus.setEnabled(true);
      
    }
  }catch(e){
    kony.print("@@@ In onClickSliderMinus() Exception:::"+e);
  }
}

function onClickSliderPlus(){
  try{
    kony.print("@@@ In onClickSliderPlus() @@@");
    var currentAmt = frmMBManageCardLimit.lblLimitVal.text;
    var maxAmount = frmMBManageCardLimit.lblMaxVal.text;
    kony.print("@@CurrentAmt:::"+currentAmt+"||@@MaxAmt:::"+maxAmount);
    if(isNotBlank(currentAmt)){
      currentAmt = parseInt(removeCommos(currentAmt));
      //if(currentAmt == 1) currentAmt = 0;
      currentAmt = currentAmt + 1000;
      frmMBManageCardLimit.sliderCardLimit.selectedValue = currentAmt;
      frmMBManageCardLimit.lblLimitVal.text = commaFormattedTransfer(currentAmt+"");
      
      kony.print("@@CurrentAmt:::"+currentAmt+"||@@MaxAmt:::"+maxAmount);
      if(isNotBlank(maxAmount) && parseInt(currentAmt) < parseInt(removeCommos(maxAmount))){
        frmMBManageCardLimit.btnPlus.skin = "sknBtnPlusBlue";
        frmMBManageCardLimit.btnPlus.setEnabled(true);
      }else{
        frmMBManageCardLimit.btnPlus.skin = "sknBtnPlusWhite";
        frmMBManageCardLimit.btnPlus.setEnabled(false);
      }
      frmMBManageCardLimit.btnMinus.skin = "sknBtnMinusBlue";
      frmMBManageCardLimit.btnMinus.setEnabled(true);
    }
  }catch(e){
   kony.print("@@@ In onClickSliderPlus() Exception:::"+e);
  }
}


function onSlideManageCardLimt(){
  try{
    var selVal = frmMBManageCardLimit.sliderCardLimit.selectedValue;
	/*
    if(isNotBlank(selVal) && selVal == 0){
      frmMBManageCardLimit.lblLimitVal.text = "0"; //commaFormattedTransfer(selVal+"");
    }else{
      frmMBManageCardLimit.lblLimitVal.text = commaFormattedTransfer(selVal+"");
    }*/
	frmMBManageCardLimit.lblLimitVal.text = commaFormattedTransfer(selVal+"");
    kony.print("@@@selVal:::"+selVal);
    if(parseInt(selVal) > 0){
      frmMBManageCardLimit.btnMinus.skin = "sknBtnMinusBlue";
      frmMBManageCardLimit.btnMinus.setEnabled(true);
    }else{
      frmMBManageCardLimit.btnMinus.skin = "sknBtnMinusWhite";
      frmMBManageCardLimit.btnMinus.setEnabled(false);
    }
    
    var maxAmount = frmMBManageCardLimit.lblMaxVal.text;
    kony.print("@@@maxAmount:::"+selVal+"|||selVal:::"+selVal);
    if(isNotBlank(maxAmount) && parseInt(selVal) < parseInt(removeCommos(maxAmount))){
      frmMBManageCardLimit.btnPlus.skin = "sknBtnPlusBlue";
      frmMBManageCardLimit.btnPlus.setEnabled(true);
    }else{
      frmMBManageCardLimit.btnPlus.skin = "sknBtnPlusWhite";
      frmMBManageCardLimit.btnPlus.setEnabled(false);
    }
  }catch(e){
    kony.print("@@@ In onSlideManageCardLimt() Exception:::"+e);
  }
}

function onSaveManageCardLimt(){
  try{
    kony.print("@@@ In onSaveManageCardLimt() @@@");
    var prevAmount = frmMBManageCardLimit.lblPrevAmount.text;
    var limitAmount = frmMBManageCardLimit.lblLimitVal.text+"";
    kony.print("@@@PrevLimit:::"+prevAmount+"||CurrentLimit::"+limitAmount);
    if(isNotBlank(prevAmount) && isNotBlank(limitAmount)){
      if(prevAmount == limitAmount){
        showAlert(kony.i18n.getLocalizedString("DBL_Limit_NoChangeLimit"), kony.i18n.getLocalizedString("info"));
      }else{
        kony.print("@@@ gblEditAmountFlow:::"+gblEditAmountFlow+"||limitAmount::"+limitAmount);
        if(isNotBlank(gblEditAmountFlow) && gblEditAmountFlow == "CASH" && isNotBlank(limitAmount)){
          frmMBManageChangeDebitCardLimit.lblAmtCashWithLimit.text = limitAmount + " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        if(isNotBlank(gblEditAmountFlow) && gblEditAmountFlow == "EDC" && isNotBlank(limitAmount)){
          frmMBManageChangeDebitCardLimit.lblAmtEdcOnlineSpendLimit.text = limitAmount + " "+ kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        frmMBManageChangeDebitCardLimit.show();
      }
    }
  }catch(e){
    kony.print("@@@ In onSaveManageCardLimt() Exception:::"+e);
  }
}

function onCancelManageCardLimt(){
  try{
    kony.print("@@@ In onCancelManageCardLimt() @@@");
    frmMBManageChangeDebitCardLimit.show();
  }catch(e){
    kony.print("@@@ In onCancelManageCardLimt() Exception:::"+e);
  }
}

//Debit Card Confirmation screen

function showfrmMBManageDebitCardConfirmation(){
  try{
    kony.print("@@@ In showfrmMBManageDebitCardConfirmation() @@@");
    var cashAmt = "";
   	var validFlag = false;
    kony.print("@@PreviousData::"+JSON.stringify(gblCardOperData));
    if(gblCardOperData != null){
      if(frmMBManageChangeDebitCardLimit.lblAmtCashWithLimit.text != gblCardOperData.cashLimit){
        validFlag = true;
      }else if(frmMBManageChangeDebitCardLimit.lblAmtEdcOnlineSpendLimit.text != gblCardOperData.eocLimit){
        validFlag = true;
      }else if(gblOverseaUsage != gblCardOperData.overseaUsage){
        validFlag = true;
      }else if(gblPaywaveUsage != gblCardOperData.paywageUsage){
        validFlag = true;
      }else if(gblEcommerceUsage != gblCardOperData.ecomUsage){
        validFlag = true;
      }else{
        validFlag = false;
      }
      
      if(validFlag){
        var cashAmt = frmMBManageChangeDebitCardLimit.lblAmtCashWithLimit.text;
        var edcAmt = frmMBManageChangeDebitCardLimit.lblAmtEdcOnlineSpendLimit.text;
        var overUsage = "", payWaive = "", ecomm = "";
        if(isNotBlank(cashAmt)){
          cashAmt = parseFloat(removeCommos(cashAmt));
        }
        if(isNotBlank(edcAmt)){
          edcAmt = parseFloat(removeCommos(edcAmt));
        }
        if(gblOverseaUsage == "On"){
          overUsage = "Y";
        }else if(gblOverseaUsage == "Off"){
          overUsage = "N";
        }
        if(gblPaywaveUsage == "On"){
          payWaive = "Y";
        }else if(gblPaywaveUsage == "Off"){
          payWaive = "N";
        }
        if(gblEcommerceUsage == "On"){
          ecomm = "Y";
        }else if(gblEcommerceUsage == "Off"){
          ecomm = "N";
        }
        kony.print("cashAmt>>"+cashAmt+"<<edcAmt>>"+edcAmt+"<<overUsage>>"+overUsage+"<<payWaive>>"+ payWaive+"<<ecomm>>"+ecomm);
        callCardLimitSettingSaveToSession(cashAmt, edcAmt, overUsage, payWaive, ecomm);
      }else{
        showAlert(kony.i18n.getLocalizedString("DBL_Limit_NoChange"), kony.i18n.getLocalizedString("info"));
      }
    }
  }catch(e){
    kony.print("@@@ In showfrmMBManageDebitCardConfirmation() Exception:::"+e);
  }
}


function callCardLimitSettingSaveToSession(atmAmount, edcAmount, overUsageVal, payWaiveVal, ecommVal){
  try{
    kony.print("@@@ In callCardLimitSettingSaveToSession() @@@");
    showLoadingScreen();
    var inputParams = {};
    inputParams["cardRefId"] = gblCACardRefNumber;
    inputParams["atmWithdrawlimit"] = atmAmount;
    inputParams["posWithdrawlimit"] = edcAmount;
    inputParams["overseaFlag"] = overUsageVal;
    inputParams["payWaiveFlag"] = payWaiveVal;
    inputParams["ecommFlag"] = ecommVal;
    invokeServiceSecureAsync("cardLimitSettingSaveToSession", inputParams, onSuccessCardLimitSettingSaveToSession);
  }catch(e){
    kony.print("@@@ In callCardLimitSettingSaveToSession() Exception:::"+e);
  }
}

function onSuccessCardLimitSettingSaveToSession(status, resulttable){
  try{
    kony.print("@@@ In onSuccessCardLimitSettingSaveToSession() @@@");
    if(status == 400){
      if(resulttable["opstatus"] == 0){
        if(isNotBlank(resulttable["atmWithdrawlimit"])){
          frmMBManageDebitCardConfirmation.lblAMtCashWithLimit.text = commaFormattedTransfer(parseFloat(resulttable["atmWithdrawlimit"])+"") + " " +  kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        if(isNotBlank(resulttable["posWithdrawlimit"])){
          frmMBManageDebitCardConfirmation.lblAmtEDCOnlineSpendLimit.text = commaFormattedTransfer(parseFloat(resulttable["posWithdrawlimit"])+"") + " " +  kony.i18n.getLocalizedString("currencyThaiBaht");
        }
        if(isNotBlank(resulttable["overseaFlag"]) && resulttable["overseaFlag"] == "Y"){
          frmMBManageDebitCardConfirmation.lblStatusOverseaUsage.text = kony.i18n.getLocalizedString("DBL_OnUSage");
          gblOverseaUsage = "On";
        }else if(isNotBlank(resulttable["overseaFlag"]) && resulttable["overseaFlag"] == "N"){
          frmMBManageDebitCardConfirmation.lblStatusOverseaUsage.text = kony.i18n.getLocalizedString("DBL_OffUSage");
          gblOverseaUsage = "Off";
        }
        if(isNotBlank(resulttable["payWaiveFlag"]) && resulttable["payWaiveFlag"] == "Y"){
          frmMBManageDebitCardConfirmation.lblStatusPaywaveUsage.text = kony.i18n.getLocalizedString("DBL_OnUSage");
          gblPaywaveUsage = "On";
        }else if(isNotBlank(resulttable["payWaiveFlag"]) && resulttable["payWaiveFlag"] == "N"){
          frmMBManageDebitCardConfirmation.lblStatusPaywaveUsage.text = kony.i18n.getLocalizedString("DBL_OffUSage");
          gblPaywaveUsage = "Off";
        }
        if(isNotBlank(resulttable["ecommFlag"]) && resulttable["ecommFlag"] == "Y"){
          frmMBManageDebitCardConfirmation.lblStatusECommerceUsage.text = kony.i18n.getLocalizedString("DBL_OnUSage");
          gblEcommerceUsage = "On";
        }else if(isNotBlank(resulttable["ecommFlag"]) && resulttable["ecommFlag"] == "N"){
          frmMBManageDebitCardConfirmation.lblStatusECommerceUsage.text = kony.i18n.getLocalizedString("DBL_OffUSage");
          gblEcommerceUsage = "Off";
        }
		if(gblSelectedCard["allowSetLimit"] == "Y" && gblSelectedCard["microCardFlag"] == "Y"){
          frmMBManageDebitCardConfirmation.flxCashWithLimit.setVisibility(false);
          frmMBManageDebitCardConfirmation.flxEDCOnlineSpendLimit.setVisibility(true);
          frmMBManageDebitCardConfirmation.flxOverseaUsage.setVisibility(true);
          frmMBManageDebitCardConfirmation.flxPaywaveUsage.setVisibility(true);
          frmMBManageDebitCardConfirmation.flxECommerceUsage.setVisibility(false);
        }else if(gblSelectedCard["allowSetLimit"] == "Y" && gblSelectedCard["microCardFlag"] == "N"){
          frmMBManageDebitCardConfirmation.flxCashWithLimit.setVisibility(true);
          frmMBManageDebitCardConfirmation.flxEDCOnlineSpendLimit.setVisibility(true);
          frmMBManageDebitCardConfirmation.flxOverseaUsage.setVisibility(true);
          frmMBManageDebitCardConfirmation.flxPaywaveUsage.setVisibility(true);
          frmMBManageDebitCardConfirmation.flxECommerceUsage.setVisibility(true);
        }
        isSuccessDebitLimitChange = false;
        frmMBManageDebitCardConfirmation.show();
	  	dismissLoadingScreen();
      }else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      }
    }else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  }catch(e){
    dismissLoadingScreen();
    kony.print("@@@ In onSuccessCardLimitSettingSaveToSession() Exception:::"+e);
  }
}


function initfrmMBManageDebitCardConfirmationt(){
  try{  
	frmMBManageDebitCardConfirmation.preShow = preShowfrmMBManageDebitCardConfirmation;
    
    frmMBManageDebitCardConfirmation.onDeviceBack = disableBackButton;  
  }catch(e){
    kony.print("@@@ In onCancelManageCardLimt() Exception:::"+e);
  }
}


function preShowfrmMBManageDebitCardConfirmation(){
  try{
    //frmMBManageDebitCardConfirmation.lblsubheader.text= kony.i18n.getLocalizedString("DBL_ChangeDebitLimitTitle");                                                   
    frmMBManageDebitCardConfirmation.lblCashWithLimit.text= kony.i18n.getLocalizedString("DBL_CashWithdrawal");
    frmMBManageDebitCardConfirmation.lblEDCOnlineSpendLimit.text= kony.i18n.getLocalizedString("DBL_EDCSpending");
    frmMBManageDebitCardConfirmation.lblOverseaUsage.text= kony.i18n.getLocalizedString("DBL_OverseaUsage");
    frmMBManageDebitCardConfirmation.lblPaywaveUsage.text= kony.i18n.getLocalizedString("DBL_PaywaveUsage");
    frmMBManageDebitCardConfirmation.lblECommerceUsage.text= kony.i18n.getLocalizedString("DBL_ECommerceUsage");
    frmMBManageDebitCardConfirmation.imgCard.src= frmMBManageCard.imgCard.src;
    frmMBManageDebitCardConfirmation.lblCardNumber.text=frmMBManageCard.lblCardNumber.text;
    frmMBManageDebitCardConfirmation.lblCardAccountName.text=frmMBManageCard.lblCardAccountName.text;
    frmMBManageDebitCardConfirmation.lblCardNumber.skin = getCardNoSkin(frmMBManageDebitCardConfirmation.imgCard.src); //MKI, MIB-12137 start
    if(frmMBManageDebitCardConfirmation.imgCard.src.toLowerCase().includes("debittmbwave")){      
    	frmMBManageDebitCardConfirmation.lblCardAccountName.skin = "lblBlackCard22px";
    }
    else{         
        frmMBManageDebitCardConfirmation.lblCardAccountName.skin = "lblWhiteCard22px";
    } //MKI, MIB-12137 end
    if(gblOverseaUsage == "On"){
      frmMBManageDebitCardConfirmation.lblStatusOverseaUsage.text = kony.i18n.getLocalizedString("DBL_OnUSage");
    }else{
      frmMBManageDebitCardConfirmation.lblStatusOverseaUsage.text = kony.i18n.getLocalizedString("DBL_OffUSage");
    }
    if(gblPaywaveUsage == "On"){
      frmMBManageDebitCardConfirmation.lblStatusPaywaveUsage.text = kony.i18n.getLocalizedString("DBL_OnUSage");
    }else{
      frmMBManageDebitCardConfirmation.lblStatusPaywaveUsage.text = kony.i18n.getLocalizedString("DBL_OffUSage");
    }
    if(gblEcommerceUsage == "On"){
      frmMBManageDebitCardConfirmation.lblStatusECommerceUsage.text = kony.i18n.getLocalizedString("DBL_OnUSage");
    }else{
      frmMBManageDebitCardConfirmation.lblStatusECommerceUsage.text = kony.i18n.getLocalizedString("DBL_OffUSage");
    }
    kony.print("@@@isSuccessDebitLimitChange:::"+isSuccessDebitLimitChange);
    frmMBManageDebitCardConfirmation.onDeviceBack = disableBackButton; 
    if(isSuccessDebitLimitChange == false){
      	kony.print("@@@ In in isSuccessDebitLimitChange false ");
      	frmMBManageDebitCardConfirmation.flxFooter.setVisibility(true);
        frmMBManageDebitCardConfirmation.flxFooterManageOthercard.setVisibility(false);
      	frmMBManageDebitCardConfirmation.imgSuccessTick.setVisibility(false);//  imgCard
        frmMBManageDebitCardConfirmation.lblHdrTxt.text= kony.i18n.getLocalizedString("Confirmation");
        frmMBManageDebitCardConfirmation.lblsubheader.text= kony.i18n.getLocalizedString("DBL_ChangeDebitLimitTitle");                                                   
        frmMBManageDebitCardConfirmation.btnback.text= kony.i18n.getLocalizedString("Back");
        frmMBManageDebitCardConfirmation.btnnext.text= kony.i18n.getLocalizedString("CAV07_btnConfirm");
        frmMBManageDebitCardConfirmation.btnback.onClick = onCancelManageCardLimt;//PO comments	showfrmMBManageChangeDebitCardLimit;
     	//frmMBManageDebitCardConfirmation.btnnext.onClick= checkIsSuccessLimitChange;
        //frmMBManageDebitCardConfirmation.flexLine.setVisibility(false);
    }else if (isSuccessDebitLimitChange == true){
      	kony.print("@@@ In in isSuccessDebitLimitChange true ");
        //frmMBManageDebitCardConfirmation.flexLine.setVisibility(true);
      	frmMBManageDebitCardConfirmation.imgSuccessTick.setVisibility(true);
        frmMBManageDebitCardConfirmation.flxFooter.setVisibility(false);
        frmMBManageDebitCardConfirmation.flxFooterManageOthercard.setVisibility(true);
        frmMBManageDebitCardConfirmation.btnManageOtherCard.text= kony.i18n.getLocalizedString("btnManageOtherCard");
        frmMBManageDebitCardConfirmation.lblHdrTxt.text= kony.i18n.getLocalizedString("CAV08_keyTitle");
        frmMBManageDebitCardConfirmation.lblsubheader.text= kony.i18n.getLocalizedString("DBL_ResultDesc"); 
      	frmMBManageDebitCardConfirmation.btnManageOtherCard.onClick = preShowMBCardList;
        //isSuccessDebitLimitChange = false;
    }
  }catch(e){
    kony.print("@@@ In onCancelManageCardLimt() Exception:::"+e);
  }  
}

//failure screen
function btnManageOtherCardFailure(){
    try{
  //showLoadingScreen();
  kony.print("@@@ In in btnManageOtherCardFailure ");  
  frmMBManageDebitCardFailure.show();
    }catch(e){
    kony.print("@@@ In btnManageOtherCardFailure() Exception:::"+e);
  }
}

function initfrmMBManageDebitCardFailure(){
  try{  
    kony.print("@@@ In in initfrmMBManageDebitCardFailure ");
  frmMBManageDebitCardFailure.preShow = preshowfrmMBManageDebitCardFailure;
  frmMBManageDebitCardFailure.onDeviceBack = disableBackButton;  
  }catch(e){
    kony.print("@@@ In initfrmMBManageDebitCardFailure() Exception:::"+e);
  }
}


function preshowfrmMBManageDebitCardFailure(){
  try{
	//changeStatusBarColor();
    kony.print("@@@ In in preshowfrmMBManageDebitCardFailure ");
	frmMBManageDebitCardFailure.lblHeader.text = kony.i18n.getLocalizedString("CAV08_keyTitle");
	frmMBManageDebitCardFailure.lblCallcenternum.text = kony.i18n.getLocalizedString("DBI03_Call1558");
	frmMBManageDebitCardFailure.btnManageOtherCard.text = kony.i18n.getLocalizedString("CCA05_BtnActOtherCard");
	frmMBManageDebitCardFailure.rchAddress.text = kony.i18n.getLocalizedString("ChangeDBL_Failed");
    frmMBManageDebitCardFailure.lblTryAgain.text =kony.i18n.getLocalizedString("btnTryAgain");
     frmMBManageDebitCardFailure.onDeviceBack = disableBackButton; 
    frmMBManageDebitCardFailure.btnManageOtherCard.onClick = preShowMBCardList;
    frmMBManageDebitCardFailure.show();
      }catch(e){
    kony.print("@@@ In preshowfrmMBManageDebitCardFailure() Exception:::"+e);
  }
}

function frmMBManageDebitCardConfirmationPostShow(frmName) {
  kony.print("Previous Form>>"+kony.application.getPreviousForm().id);
  kony.print("Current Form>>"+kony.application.getCurrentForm().id);
  if(kony.application.getPreviousForm().id != "frmMBManageDebitCardFailure"){
    addAccessPinKeypad(frmName);
  }
}
  
