var serviceDocObject = {
 "appId": "d78b224c-1ac2-45bd-b662-75bfa4f5b0ea",
 "baseId": "588d4557-46f3-43ba-892b-8521d99eb93c",
 "name": "TMBMIB",
 "selflink": "https://mibdev1.tau2904.com/authService/100000002/appconfig",
 "integsvc": {
  "RTPServices": "https://mibdev1.tau2904.com/services/RTPServices",
  "TMBMIBService50": "https://mibdev1.tau2904.com/services/TMBMIBService50",
  "TMBMIBService51": "https://mibdev1.tau2904.com/services/TMBMIBService51",
  "TMBMIBService52": "https://mibdev1.tau2904.com/services/TMBMIBService52",
  "MFSuitabilityServices": "https://mibdev1.tau2904.com/services/MFSuitabilityServices",
  "TMBMIBService53": "https://mibdev1.tau2904.com/services/TMBMIBService53",
  "TMBMIBService10": "https://mibdev1.tau2904.com/services/TMBMIBService10",
  "LoanSubmissionServices": "https://mibdev1.tau2904.com/services/LoanSubmissionServices",
  "TMBMIBService11": "https://mibdev1.tau2904.com/services/TMBMIBService11",
  "LoanUpdateIncompleteDocApp": "https://mibdev1.tau2904.com/services/LoanUpdateIncompleteDocApp",
  "TMBMIBService12": "https://mibdev1.tau2904.com/services/TMBMIBService12",
  "TMBMIBService8": "https://mibdev1.tau2904.com/services/TMBMIBService8",
  "TMBMIBService13": "https://mibdev1.tau2904.com/services/TMBMIBService13",
  "JSONSample": "https://mibdev1.tau2904.com/services/JSONSample",
  "TMBMIBService9": "https://mibdev1.tau2904.com/services/TMBMIBService9",
  "TMBMIBService14": "https://mibdev1.tau2904.com/services/TMBMIBService14",
  "LoanSubGetCustomerInfo": "https://mibdev1.tau2904.com/services/LoanSubGetCustomerInfo",
  "TMBMIBService15": "https://mibdev1.tau2904.com/services/TMBMIBService15",
  "TMBMIBService16": "https://mibdev1.tau2904.com/services/TMBMIBService16",
  "TMBMIBService17": "https://mibdev1.tau2904.com/services/TMBMIBService17",
  "SaveAlertList": "https://mibdev1.tau2904.com/services/SaveAlertList",
  "TMBMIBService18": "https://mibdev1.tau2904.com/services/TMBMIBService18",
  "MFCalcSuitabilityInqService": "https://mibdev1.tau2904.com/services/MFCalcSuitabilityInqService",
  "GetLoanConfirmDetailsComposite": "https://mibdev1.tau2904.com/services/GetLoanConfirmDetailsComposite",
  "ProspectService": "https://mibdev1.tau2904.com/services/ProspectService",
  "LoanSubmissionGetChecklistInfo": "https://mibdev1.tau2904.com/services/LoanSubmissionGetChecklistInfo",
  "LoanUpdateCreditCardInfo": "https://mibdev1.tau2904.com/services/LoanUpdateCreditCardInfo",
  "TMBMIBService40": "https://mibdev1.tau2904.com/services/TMBMIBService40",
  "TMBMIBService41": "https://mibdev1.tau2904.com/services/TMBMIBService41",
  "TMBMIBService42": "https://mibdev1.tau2904.com/services/TMBMIBService42",
  "TMBMIBService43": "https://mibdev1.tau2904.com/services/TMBMIBService43",
  "TMBMIBService44": "https://mibdev1.tau2904.com/services/TMBMIBService44",
  "TMBMIBService45": "https://mibdev1.tau2904.com/services/TMBMIBService45",
  "TMBMIBService46": "https://mibdev1.tau2904.com/services/TMBMIBService46",
  "TMBMIBService47": "https://mibdev1.tau2904.com/services/TMBMIBService47",
  "TMBMIBService48": "https://mibdev1.tau2904.com/services/TMBMIBService48",
  "TMBMIBService49": "https://mibdev1.tau2904.com/services/TMBMIBService49",
  "CalRiskPersonal": "https://mibdev1.tau2904.com/services/CalRiskPersonal",
  "TMBMIBService2": "https://mibdev1.tau2904.com/services/TMBMIBService2",
  "TMBMIBService3": "https://mibdev1.tau2904.com/services/TMBMIBService3",
  "DOPACheckCardBank": "https://mibdev1.tau2904.com/services/DOPACheckCardBank",
  "CRMProfilePartyiInq": "https://mibdev1.tau2904.com/services/CRMProfilePartyiInq",
  "TMBMIBService0": "https://mibdev1.tau2904.com/services/TMBMIBService0",
  "TMBMIBService1": "https://mibdev1.tau2904.com/services/TMBMIBService1",
  "TMBMIBService6": "https://mibdev1.tau2904.com/services/TMBMIBService6",
  "TMBMIBService7": "https://mibdev1.tau2904.com/services/TMBMIBService7",
  "TMBMIBService4": "https://mibdev1.tau2904.com/services/TMBMIBService4",
  "TMBMIBService5": "https://mibdev1.tau2904.com/services/TMBMIBService5",
  "TMBMIBService30": "https://mibdev1.tau2904.com/services/TMBMIBService30",
  "TMBMIBService31": "https://mibdev1.tau2904.com/services/TMBMIBService31",
  "TMBMIBService32": "https://mibdev1.tau2904.com/services/TMBMIBService32",
  "TMBMIBService33": "https://mibdev1.tau2904.com/services/TMBMIBService33",
  "TMBMIBService34": "https://mibdev1.tau2904.com/services/TMBMIBService34",
  "TMBMIBService35": "https://mibdev1.tau2904.com/services/TMBMIBService35",
  "TMBMIBService39": "https://mibdev1.tau2904.com/services/TMBMIBService39",
  "LoanSubGetApplicationInfo": "https://mibdev1.tau2904.com/services/LoanSubGetApplicationInfo",
  "LoanSubGetCreditCardInfo": "https://mibdev1.tau2904.com/services/LoanSubGetCreditCardInfo",
  "LoanSubGetFacilityInfo": "https://mibdev1.tau2904.com/services/LoanSubGetFacilityInfo",
  "TMBMIBService20": "https://mibdev1.tau2904.com/services/TMBMIBService20",
  "TMBMIBService21": "https://mibdev1.tau2904.com/services/TMBMIBService21",
  "TMBMIBService22": "https://mibdev1.tau2904.com/services/TMBMIBService22",
  "LoanSubSubmitApplication": "https://mibdev1.tau2904.com/services/LoanSubSubmitApplication",
  "TMBMIBService23": "https://mibdev1.tau2904.com/services/TMBMIBService23",
  "TMBMIBService24": "https://mibdev1.tau2904.com/services/TMBMIBService24",
  "TMBMIBService25": "https://mibdev1.tau2904.com/services/TMBMIBService25",
  "TMBMIBService26": "https://mibdev1.tau2904.com/services/TMBMIBService26",
  "TMBMIBService27": "https://mibdev1.tau2904.com/services/TMBMIBService27",
  "TMBMIBService28": "https://mibdev1.tau2904.com/services/TMBMIBService28",
  "TMBMIBService29": "https://mibdev1.tau2904.com/services/TMBMIBService29",
  "TMBMIBService19": "https://mibdev1.tau2904.com/services/TMBMIBService19",
  "LoanUpdateFacilityInfo": "https://mibdev1.tau2904.com/services/LoanUpdateFacilityInfo",
  "LoanSubUpdateCustInfo": "https://mibdev1.tau2904.com/services/LoanSubUpdateCustInfo",
  "EKYCOne2OneVerification": "https://mibdev1.tau2904.com/services/EKYCOne2OneVerification",
  "SaveAlert": "https://mibdev1.tau2904.com/services/SaveAlert",
  "VerifyPromptPayReceipt": "https://mibdev1.tau2904.com/services/VerifyPromptPayReceipt",
  "IdPService": "https://mibdev1.tau2904.com/services/IdPService"  
 },
 "service_doc_etag": "0000016589AAE9B3",
 "appId": "7c47dad9-0b15-4d6d-b7ac-c8507e071ad5",
 "identity_features": {
  "reporting_params_header_allowed": true
 },
 "name": "TMBMIB",
 "reportingsvc": {
  "session": "https://mibdev1.tau2904.com/services/IST",
  "custom": "https://mibdev1.tau2904.com/services/CMS"
 },
 "baseId": "36082ac7-3f5d-4fb8-8ee6-5e1bb85bd61d",
 "services_meta": {
  "RTPServices": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/RTPServices"
  },
  "TMBMIBService50": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService50"
  },
  "TMBMIBService51": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService51"
  },
  "TMBMIBService52": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService52"
  },
  "MFSuitabilityServices": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/MFSuitabilityServices"
  },
  "TMBMIBService53": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService53"
  },
  "TMBMIBService10": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService10"
  },
  "LoanSubmissionServices": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubmissionServices"
  },
  "TMBMIBService11": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService11"
  },
  "LoanUpdateIncompleteDocApp": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanUpdateIncompleteDocApp"
  },
  "TMBMIBService12": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService12"
  },
  "TMBMIBService8": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService8"
  },
  "TMBMIBService13": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService13"
  },
  "JSONSample": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/JSONSample"
  },
  "TMBMIBService9": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService9"
  },
  "TMBMIBService14": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService14"
  },
  "LoanSubGetCustomerInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubGetCustomerInfo"
  },
  "TMBMIBService15": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService15"
  },
  "TMBMIBService16": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService16"
  },
  "TMBMIBService17": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService17"
  },
  "SaveAlertList": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/SaveAlertList"
  },
  "TMBMIBService18": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService18"
  },
  "MFCalcSuitabilityInqService": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/MFCalcSuitabilityInqService"
  },
  "GetLoanConfirmDetailsComposite": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/GetLoanConfirmDetailsComposite"
  },
  "ProspectService": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/ProspectService"
  },
  "LoanSubmissionGetChecklistInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubmissionGetChecklistInfo"
  },
  "LoanUpdateCreditCardInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanUpdateCreditCardInfo"
  },
  "TMBMIBService40": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService40"
  },
  "TMBMIBService41": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService41"
  },
  "TMBMIBService42": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService42"
  },
  "TMBMIBService43": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService43"
  },
  "TMBMIBService44": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService44"
  },
  "TMBMIBService45": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService45"
  },
  "TMBMIBService46": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService46"
  },
  "TMBMIBService47": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService47"
  },
  "TMBMIBService48": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService48"
  },
  "TMBMIBService49": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService49"
  },
  "CalRiskPersonal": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/CalRiskPersonal"
  },
  "TMBMIBService2": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService2"
  },
  "TMBMIBService3": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService3"
  },
  "DOPACheckCardBank": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/DOPACheckCardBank"
  },
  "CRMProfilePartyiInq": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/CRMProfilePartyiInq"
  },
  "TMBMIBService0": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService0"
  },
  "TMBMIBService1": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService1"
  },
  "TMBMIBService6": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService6"
  },
  "TMBMIBService7": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService7"
  },
  "TMBMIBService4": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService4"
  },
  "TMBMIBService5": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService5"
  },
  "TMBMIBService30": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService30"
  },
  "TMBMIBService31": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService31"
  },
  "TMBMIBService32": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService32"
  },
  "TMBMIBService33": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService33"
  },
  "TMBMIBService34": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService34"
  },
  "TMBMIBService35": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService35"
  },
  "TMBMIBService39": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService39"
  },
  "LoanSubGetApplicationInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubGetApplicationInfo"
  },
  "LoanSubGetCreditCardInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubGetCreditCardInfo"
  },
  "LoanSubGetFacilityInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubGetFacilityInfo"
  },
  "TMBMIBService20": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService20"
  },
  "TMBMIBService21": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService21"
  },
  "TMBMIBService22": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService22"
  },
  "LoanSubSubmitApplication": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubSubmitApplication"
  },
  "TMBMIBService23": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService23"
  },
  "TMBMIBService24": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService24"
  },
  "TMBMIBService25": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService25"
  },
  "TMBMIBService26": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService26"
  },
  "TMBMIBService27": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService27"
  },
  "TMBMIBService28": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService28"
  },
  "TMBMIBService29": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService29"
  },
  "TMBMIBService19": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/TMBMIBService19"
  },
  "LoanUpdateFacilityInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanUpdateFacilityInfo"
  },
  "LoanSubUpdateCustInfo": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/LoanSubUpdateCustInfo"
  },
  "EKYCOne2OneVerification": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/EKYCOne2OneVerification"
  },
  "SaveAlert": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/SaveAlert"
  },
  "VerifyPromptPayReceipt": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/VerifyPromptPayReceipt"
  },
  "IdPService": {
   "type": "integsvc",
   "version": "1.0",
   "url": "https://mibdev1.tau2904.com/services/IdPService"
  }  
 }
};

