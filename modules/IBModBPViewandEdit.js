/*
   **************************************************************************************
		Module	: repeatScheduleFrequency
		Author  : Kony
		Date    : 
		Purpose : To know which frequency button is clicked (Daily,Weekly,Monthly or Yearly)
	***************************************************************************************
*/
var onClickRepeatAs = "";
var onClickEndFreq = "";

function repeatScheduleFrequency(eventObject) {
	frmIBBillPaymentView.btnBillpayDaily.skin = "btnIBTab4LeftNrml";
	frmIBBillPaymentView.btnBillpayWeekly.skin = "btnIBTab4MidNrml";
	frmIBBillPaymentView.btnBillpayMonthly.skin = "btnIBTab4MidNrml";
	frmIBBillPaymentView.btnBillpayYearly.skin = "btnIbTab4RightNrml";
	var btnEventId = eventObject.id;
	if (kony.string.equalsIgnoreCase(btnEventId, "btnBillpayDaily")) {
		eventObject.skin = "btnIBTab4LeftFocus";
		onClickRepeatAs = "Daily";
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnBillpayWeekly")) {
		eventObject.skin = "btnIBTab4MidFocus";
		onClickRepeatAs = "Weekly";
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnBillpayMonthly")) {
		eventObject.skin = "btnIBTab4MidFocus";
		onClickRepeatAs = "Monthly";
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnBillpayYearly")) {
		eventObject.skin = "btnIBTab4RightFocus";
		onClickRepeatAs = "Yearly";
	}
}
/*
   **************************************************************************************************
		Module	: endingSchedule
		Author  : Kony
		Date    : 
		Purpose : To know which button is clicked and changing the skin accordingly(Never,After,EndOn)
	**************************************************************************************************
*/

function endingSchedule(eventObject) {
	frmIBBillPaymentView.btnBPNever.skin = "btnIBTab3LeftNrml";
	frmIBBillPaymentView.btnBPAfter.skin = "btnIBTab3MidNrml";
	frmIBBillPaymentView.btnBPOnDate.skin = "btnIBTab3RightNrml";
	var btnEventId = eventObject.id;
	if (kony.string.equalsIgnoreCase(btnEventId, "btnBPNever")) {
		eventObject.skin = "btnIBTab3LeftFocus";
		onClickEndFreq = "Never";
		if (!frmIBBillPaymentView.lineSchBPClose.isVisible) {
			frmIBBillPaymentView.lineSchBPClose.setVisibility(true);
		}
		if (frmIBBillPaymentView.hbxBPexecutiontimes.isVisible) {
			frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(false);
		}
		if (frmIBBillPaymentView.hbxBPUntil.isVisible) {
			frmIBBillPaymentView.hbxBPUntil.setVisibility(false);
		}
		//frmIBBillPaymentView.calendarEndOn.date = currentDateForcalender();
		// frmIBBillPaymentView.txtBPExecutiontimesval.text = "";
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnBPAfter")) {
		eventObject.skin = "btnIBTab3MidFocus";
		onClickEndFreq = "After";
		if (!frmIBBillPaymentView.hbxBPexecutiontimes.isVisible) {
			frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(true);
		}
		if (frmIBBillPaymentView.hbxBPUntil.isVisible) {
			frmIBBillPaymentView.hbxBPUntil.setVisibility(false);
		}
		var afterValue = frmIBBillPaymentView.lblBPExcuteVal.text;
		if (afterValue != "-") {
			frmIBBillPaymentView.txtBPExecutiontimesval.text = frmIBBillPaymentView.lblBPExcuteVal.text;
		} else {
			frmIBBillPaymentView.txtBPExecutiontimesval.text = "";
		}
	} else if (kony.string.equalsIgnoreCase(btnEventId, "btnBPOnDate")) {
		eventObject.skin = "btnIBTab3RightFocus";
		onClickEndFreq = "OnDate";
		if (frmIBBillPaymentView.hbxBPexecutiontimes.isVisible) {
			frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(false);
		}
		if (!frmIBBillPaymentView.hbxBPUntil.isVisible) {
			frmIBBillPaymentView.hbxBPUntil.setVisibility(true);
		}
		var onDateValue = frmIBBillPaymentView.lblEndOnDateVal.text;
		if (onDateValue != "-") {
			frmIBBillPaymentView.calendarEndOn.date = frmIBBillPaymentView.lblEndOnDateVal.text;
		} else {
			frmIBBillPaymentView.calendarEndOn.date = currentDateForcalender();
		}
	}
}
//global variables :
gblEndDateOnLoad = "";
gblExecutionOnLoad = "";
gblEndingFreqOnLoadIB = "";
gblOnLoadRepeatAsIB = "";
gblBillerStatus = "0";
gblToAccNo = "";
gblFromAccNo = "";
gblBillerCompcode = "";
gblTransCode = "";
// Testing Cache data 
/*
function getMasterBillerData(scheID){
	var inputParam = {};
    //inputParam["IsActive"] = "1";
    inputParam["BillerGroupType"] = "0";
    showLoadingScreenPopup();
    invokeServiceSecureAsync("masterBillerInquiryForEditBP", inputParam, callBackMasterBillerInqForEditBPService)
}

function callBackMasterBillerInqForEditBPService(status,result){
if (status == 400) {
        if (result["opstatus"] == 0) {
			invokeCustomerBillInqForEditBPService();
        }else {
        	dismissLoadingScreenPopup();
            
        }
    }
}

function invokeCustomerBillInqForEditBPService(){

	var inputParam = {};
    invokeServiceSecureAsync("customerBillInquiry", inputParam, callBackCustomerBillInqForEditBPService)
}


function callBackCustomerBillInqForEditBPService(status,result){
if (status == 400) {
        if (result["opstatus"] == 0) {
			callPaymentInqServiceForEditBP();
        }else {
        	dismissLoadingScreenPopup();
            
        }
    }

}
*/
function resetGolbalVariablesForEditBP(){
	gblEndDateOnLoad = "";
	gblExecutionOnLoad = "";
	gblEndingFreqOnLoadIB = "";
	gblOnLoadRepeatAsIB = "";
	gblBillerStatus = 0;
	gblToAccNo = "";
	gblFromAccNo = "";
	gblBillerCompcode = "";
	gblBillerTaxID = "";
	gblTransCode = "";
	onClickRepeatAs = "";
	onClickEndFreq = "";
	gblScheduleButtonClicked = false;
	gblScheduleFreqChanged = false;
	gblAmountSelected = false;
	repeatAsIB = "";
	endFreqSave = "";
	gblVerifyOTPEditBP = 0;
	viewEditBPFrmReset();
}

function viewEditBPFrmReset(){
	if (frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
        frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
    }
    if(frmIBBillPaymentView.hbxBPEditAmnt.isVisible){
	//frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
		frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
	}
	if(!frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible){
		frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true);
	}
	if(frmIBBillPaymentView.lblBPeditHrd.isVisible){
		frmIBBillPaymentView.lblBPeditHrd.setVisibility(false);
	}
	
	//NEW changes
	if(frmIBBillPaymentView.buttonEdit.isVisible == false){
		frmIBBillPaymentView.buttonEdit.setVisibility(true);
	}
	
	if(frmIBBillPaymentView.buttonDel.isVisible == false){
		frmIBBillPaymentView.buttonDel.setVisibility(true);
	}
	//--	
	
	if(!frmIBBillPaymentView.lblBPViewHrd.isVisible){
		frmIBBillPaymentView.lblBPViewHrd.setVisibility(true);
	}
	if(!frmIBBillPaymentView.hbxBPViewHdr.isVisible){
		frmIBBillPaymentView.hbxBPViewHdr.setVisibility(true);
	}
	if(frmIBBillPaymentView.btnBPSchedule.isVisible){
		frmIBBillPaymentView.btnBPSchedule.setVisibility(false);
	}
	if(!frmIBBillPaymentView.hbxbtnReturn.isVisible){
		frmIBBillPaymentView.hbxbtnReturn.setVisibility(true);
	}
	if(frmIBBillPaymentView.hbxEditBtns.isVisible){
		frmIBBillPaymentView.hbxEditBtns.setVisibility(false);
	}
	if(frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible){
		 frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
	}
	if(frmIBBillPaymentView.hbxOnlinePayment.isVisible){
		 frmIBBillPaymentView.hbxOnlinePayment.setVisibility(false);
	}
	if(frmIBBillPaymentView.hboxOnlinePayPenalty.isVisible){
		 frmIBBillPaymentView.hboxOnlinePayPenalty.setVisibility(false);
	}
	
	//schedule 
	if (frmIBBillPaymentView.lblScheduleBillPayment.isVisible) {
		frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxEditBPFutureSchedule.isVisible) {
		frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
	}
	frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
	//menuResetForEdit();
}

/*
function menuResetForEdit(){
	nameofform = kony.application.getCurrentForm();
	nameofform.hbxMenuAboutMe.skin = "hbxIBAboutMeMenu";
	nameofform.hbxMenuBillPayment.skin = "hbxIBBillPayMenu";
	nameofform.hbxMenuCardlessWithdrawal.skin = "hbxIBCardlessMenu";
	nameofform.hbxMenuConvenientServices.skin = "hbxIBConvServMenu"
	nameofform.hbxMenuMyAccountSummary.skin = "hbxIBMyAcctSummaryMenu"
	nameofform.hbxMenuMyActivities.skin = "hbxIBMyActivitiesMenuFocus"
	nameofform.hbxMenuMyInbox.skin = "hbxIBMyInboxMenu"
	nameofform.hbxMenuTopUp.skin = "hbxIBTopUpMenu";
	nameofform.hbxMenuTransfer.skin = "hbxIBTransferMenu";
}
*/

function callPaymentInqServiceForEditBP(scheID) {
	resetGolbalVariablesForEditBP();
	var inputparam = [];
	inputparam["scheRefID"] = scheID; // change this value when integrated with calender / future transactions page
	inputparam["rqUID"] = "";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("doPmtInqForEditBP", inputparam, callBackPaymenInqServiceForEditBP);
}

function callBackPaymenInqServiceForEditBP(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            /*
            var statusCode = result["StatusCode"];
            var severity = result["Severity"];
            var statusDesc = result["StatusDesc"];
            if (statusCode != 0) {
                
            } else { */
            if (result["PmtInqRs"].length > 0 && result["PmtInqRs"] != null) {
                var amount = result["PmtInqRs"][0]["Amt"];
                gblAmtFromService = amount;
                gblFromAccNo = result["PmtInqRs"][0]["FromAccId"];
                gblFromAccType = result["PmtInqRs"][0]["FromAccType"];
                gblToAccNo = result["PmtInqRs"][0]["ToAccId"];
                gblToAccType = result["PmtInqRs"][0]["ToAccType"];
                var executionOnLoad = result["PmtInqRs"][0]["ExecutionTimes"];
                var endDateOnLoad = result["PmtInqRs"][0]["EndDate"];
                var onLoadRepeatAsIB = result["PmtInqRs"][0]["RepeatAs"];
                var pmtOrderdt = result["PmtInqRs"][0]["InitiatedDt"]; // check this with TL
                var scheduleRefNo = result["PmtInqRs"][0]["scheRefNo"];
                var startOn = result["PmtInqRs"][0]["Duedt"];
                var myNote = result["PmtInqRs"][0]["MyNote"];
                var bankId = result["PmtInqRs"][0]["BankId"];
                gblTransCode = result["PmtInqRs"][0]["TransCode"];
       //         gblMobileNumber = result["PmtInqRs"][0]["RecipientMobileNbr"]; // check this with TL
                gblCustPayID = result["PmtInqRs"][0]["InternalBillerID"];
                gblBillerCategoryID = result["PmtInqRs"][0]["billerCategoryID"]; //BillerCategoryID for creditcard mask
                gblRef2EditBP = "";
                if (result["PmtInqRs"][0]["Ref2"] != undefined && result["PmtInqRs"][0]["Ref2"] != "undefined") {
                    gblRef2EditBP = result["PmtInqRs"][0]["Ref2"];
                    frmIBBillPaymentView.labelBPRef2Val.text = result["PmtInqRs"][0]["Ref2"];
                } else {
                    gblRef2EditBP = "";
                    frmIBBillPaymentView.labelBPRef2Val.text = "";
                }
                frmIBBillPaymentView.lblBPAmtValue.text = commaFormatted(amount) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"); // Amount 
                //frmIBBillPaymentView.lblBPScheduleRefNoVal.text = scheduleRefNo.replace("S", "F"); // schedule ref Num	
                frmIBBillPaymentView.lblBPScheduleRefNoVal.text = "SB" + scheduleRefNo.substring(2, scheduleRefNo.length);
                frmIBBillPaymentView.lblBPStartOnDateVal.text = dateFormatForDisplay(startOn);
                gblStartOn = dateFormatForDisplay(startOn);
                if (myNote != undefined && myNote != null) {
                    frmIBBillPaymentView.lblMyNoteVal.text = replaceHtmlTagChars(myNote);
                } else {
                    frmIBBillPaymentView.lblMyNoteVal.text = "-";
                }
                frmIBBillPaymentView.lblBPPaymentOrderValue.text = dateFormatForDisplayWithTimeStamp(pmtOrderdt);
                if (onLoadRepeatAsIB != "" && onLoadRepeatAsIB != undefined && onLoadRepeatAsIB != null) {
                    if (onLoadRepeatAsIB == "Annually") {
                        gblOnLoadRepeatAsIB = "Yearly";
                        frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");;
                    } 
                    else if (onLoadRepeatAsIB == "Monthly") {
                        gblOnLoadRepeatAsIB = onLoadRepeatAsIB;
                        frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
                    } 
                    else if (onLoadRepeatAsIB == "Weekly") {
                        gblOnLoadRepeatAsIB = onLoadRepeatAsIB;
                        frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
                    } 
                    else if (onLoadRepeatAsIB == "Daily") {
                        gblOnLoadRepeatAsIB = onLoadRepeatAsIB;
                        frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Daily");
                    } 
                    else {
                        gblOnLoadRepeatAsIB = onLoadRepeatAsIB;
                        frmIBBillPaymentView.lblBPRepeatAsVal.text = onLoadRepeatAsIB;
                    }
                    if (endDateOnLoad != "" && endDateOnLoad != undefined && endDateOnLoad != null) {
                        gblEndingFreqOnLoadIB = "OnDate";
                        gblEndDateOnLoad = dateFormatForDisplay(endDateOnLoad);
                        frmIBBillPaymentView.lblEndOnDateVal.text = dateFormatForDisplay(endDateOnLoad);
                        var executiontimes = numberOfExecution(frmIBBillPaymentView.lblBPStartOnDateVal.text, frmIBBillPaymentView.lblEndOnDateVal.text, onLoadRepeatAsIB);
                        gblExecutionOnLoad = executiontimes;
                        frmIBBillPaymentView.lblBPExcuteVal.text = executiontimes;
                        frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB");
                        frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
                    } else if (executionOnLoad != "" && executionOnLoad != undefined && executionOnLoad != null) {
                        gblEndingFreqOnLoadIB = "After";
                        gblExecutionOnLoad = executionOnLoad;
                        frmIBBillPaymentView.lblBPExcuteVal.text = executionOnLoad;
                        frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesIB");
                        frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
                        var endOnDate = endOnDateCalculator(frmIBBillPaymentView.lblBPStartOnDateVal.text, frmIBBillPaymentView.lblBPExcuteVal.text, onLoadRepeatAsIB);
                        gblEndDateOnLoad = endOnDate;
                        frmIBBillPaymentView.lblEndOnDateVal.text = endOnDate;
                    } else {
                        gblEndingFreqOnLoadIB = "Never";
                        // frmIBBillPaymentView.hboxEndOn.setVisibility(false);
                        // frmIBBillPaymentView.hboxExecute.setVisibility(false);
                        frmIBBillPaymentView.lblEndOnDateVal.text = "-";
                        frmIBBillPaymentView.lblBPExcuteVal.text = "-";
                        frmIBBillPaymentView.lblBPExcuteVal.setVisibility(false);
                        frmIBBillPaymentView.lblBPExcuteValTimes.contentAlignment = "CONTENT_ALIGN_MIDDLE_RIGHT";
                        frmIBBillPaymentView.lblBPExcuteValTimes.text = "-";
                        frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
                        gblEndDateOnLoad = "";
                        gblExecutionOnLoad = "";
                    }
                } else {
                    onLoadRepeatAsIB = "Once";
                    gblOnLoadRepeatAsIB = onLoadRepeatAsIB;
                    frmIBBillPaymentView.lblEndOnDateVal.text = dateFormatForDisplay(startOn);
                    frmIBBillPaymentView.lblBPExcuteVal.text = "1";
                    frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB");
                    frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
                    frmIBBillPaymentView.lblBPRepeatAsVal.text = onLoadRepeatAsIB;
                    gblEndingFreqOnLoadIB = "";
                    gblEndDateOnLoad = dateFormatForDisplay(startOn);
                    gblExecutionOnLoad = "1";
                    //frmIBBillPaymentView.hboxEndOn.setVisibility(false);
                    //frmIBBillPaymentView.hboxRepeatAs.setVisibility(false);
                    // frmIBBillPaymentView.hboxExecute.setVisibility(false);
                }
                if (result["PmtInqRs"][0]["ref1ValueB"] != null && result["PmtInqRs"][0]["ref1ValueB"] != undefined) {
                    gblRef1ValueBP = result["PmtInqRs"][0]["ref1ValueB"];
                    frmIBBillPaymentView.lblBPRef1Val.text = result["PmtInqRs"][0]["ref1ValueB"];
                    //below line is added for CR - PCI-DSS masked Credit card no
                    frmIBBillPaymentView.lblBPRef1ValMasked.text = result["PmtInqRs"][0]["maskedRef1ValueB"];
                    /*
					var ref1Length = result["PmtInqRs"][0]["ref1ValueB"].length;
					if(ref1Length >= "10"){
						frmIBBillPaymentView.lblBPRef1Val.text = result["PmtInqRs"][0]["ref1ValueB"];  //addHyphenIB(result["PmtInqRs"][0]["ref1ValueB"]);
					}else{
						frmIBBillPaymentView.lblBPRef1Val.text = result["PmtInqRs"][0]["ref1ValueB"];
					}
					*/
                } else {
                    frmIBBillPaymentView.lblBPRef1Val.text = "";
                    //below line is added for CR - PCI-DSS masked Credit card no
                    frmIBBillPaymentView.lblBPRef1ValMasked.text = "";
                }
                gblBillerStatus = result["PmtInqRs"][0]["activeStatus"];
                if (result["PmtInqRs"][0]["ref2ValueB"] != null && result["PmtInqRs"][0]["ref2ValueB"] != undefined && result["PmtInqRs"][0]["ref2ValueB"] != "") {
                    var ref2Length = result["PmtInqRs"][0]["ref2ValueB"].length;
                    frmIBBillPaymentView.labelBPRef2Val.text = result["PmtInqRs"][0]["ref2ValueB"];
                    /*
					if(ref2Length >= "10"){
						frmIBBillPaymentView.labelBPRef2Val.text = result["PmtInqRs"][0]["ref2ValueB"];
					}else{
						frmIBBillPaymentView.labelBPRef2Val.text = result["PmtInqRs"][0]["ref2ValueB"];
					}
					*/
                }
                if (result["PmtInqRs"][0]["billerNickName"] != null && result["PmtInqRs"][0]["billerNickName"] != "") {
                    if (gblBillerStatus == 0) {
                        frmIBBillPaymentView.lblBPBillerNickName.setVisibility(false);
                    } else {
                        frmIBBillPaymentView.lblBPBillerNickName.text = result["PmtInqRs"][0]["billerNickName"];
                    }
                } else {
                    //If Not avialble , show blank
                    frmIBBillPaymentView.lblBPBillerNickName.text = "";
                }
                // biller logo and short name from masterbillerInquiry (T.S)If My Biller/Top-Up is not found in CustomerBillerInquiry response
                //have to discuss about this condition 
                /*
								frmIBBillPaymentView.lblBPBillNameAndCompCode.text = billerShortName;
								frmIBBillPaymentView.lblBPBillerNickName.setVisibility(false);
								frmIBBillPaymentView.lblBPRef1.text = labelReferenceNumber1; /// check this 
								frmIBBillPaymentView.buttonEdit.setEnabled(false);
								frmIBBillPaymentView.buttonEdit.skin = "btnIBediticonsmall";
								frmIBBillPaymentView.hbox58788011366391.setVisibility(false);
					*/
                gblEditPaymentMethod = result["PmtInqRs"][0]["PmtMethod"]; //used for doPmtAdd service
                gblEditBillMethod = result["PmtInqRs"][0]["billerMethod"];
                gblEditBillerGroupType = result["PmtInqRs"][0]["billerGroupType"];
                billerShortName = result["PmtInqRs"][0]["billerShortName"];
                gblBillerCompcode = result["PmtInqRs"][0]["billerCompcode"];
                var local = kony.i18n.getCurrentLocale();
                gblBPEditEffDtMB = result["PmtInqRs"][0]["effDt"];
                //  gblBPEditEffDt  get this param also as we need in onlinepaymentinquiry
                var billerName = "";
                billerNameThai=result["PmtInqRs"][0]["billerNameTH"];
                labelReferenceNumber1 = "";
                labelReferenceNumber2 = "";
                if (local == "en_US") {
                    billerName = result["PmtInqRs"][0]["billerNameEN"];
                    labelReferenceNumber1 = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
                    labelReferenceNumber2 = result["PmtInqRs"][0]["labelReferenceNumber2EN"];
                } else if (local == "th_TH") {
                    billerName = result["PmtInqRs"][0]["billerNameTH"];
                    labelReferenceNumber1 = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                    labelReferenceNumber2 = result["PmtInqRs"][0]["labelReferenceNumber2TH"];
                }
                gblRef1LblEN = result["PmtInqRs"][0]["labelReferenceNumber1EN"];
                gblRef1LblTH = result["PmtInqRs"][0]["labelReferenceNumber1TH"];
                gblRef2LblEN = result["PmtInqRs"][0]["labelReferenceNumber2EN"];
                gblRef2LblTH = result["PmtInqRs"][0]["labelReferenceNumber2TH"];
                gblBillerTaxID = result["PmtInqRs"][0]["billerTaxID"];
                if (gblBillerStatus == "0") {
                    frmIBBillPaymentView.lblBPBillNameAndCompCode.text = billerShortName;
                    if (labelReferenceNumber1 != "") {
                        frmIBBillPaymentView.lblBPRef1.text = appendColon(labelReferenceNumber1);
                    }
                    frmIBBillPaymentView.buttonEdit.setEnabled(false);
                    frmIBBillPaymentView.buttonEdit.skin = "btnIBediticonsmall";
                    frmIBBillPaymentView.labelBPRef2.text = "";
                    frmIBBillPaymentView.hbox58788011366391.setVisibility(false);
                } else {
                    frmIBBillPaymentView.lblBPBillNameAndCompCode.text = billerName + " (" + gblBillerCompcode + ") ";
                    if (labelReferenceNumber2 != null && labelReferenceNumber2 != undefined && labelReferenceNumber2 != "" && (result["PmtInqRs"][0]["ref2ValueB"] != "" && result["PmtInqRs"][0]["ref2ValueB"] != "undefined")) {
                        frmIBBillPaymentView.labelBPRef2.text = appendColon(labelReferenceNumber2);
                        frmIBBillPaymentView.hbox58788011366391.setVisibility(true);
                    } else {
                        frmIBBillPaymentView.labelBPRef2.text = "";
                        frmIBBillPaymentView.hbox58788011366391.setVisibility(false);
                    }
                    if (labelReferenceNumber1 != "") {
                        frmIBBillPaymentView.lblBPRef1.text = appendColon(labelReferenceNumber1);
                    }
                }
                //BILLER_LOGO_URL = "https://vit.tau2904.com/tmb/ImageRender";
                var imagesUrl = BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcode + "&modIdentifier=MyBillers";
                //BILLER_LOGO_URL + "?" + "crmId=&personalizedId=&billerId=" + gblBillerCompcode;
                frmIBBillPaymentView.imageBillerPic.src = imagesUrl;
                frmIBEditFutureBillPaymentPrecnf.image2448366816169765.src = imagesUrl;
                //As per mail discussion don't call billpaymentInq service for TMB credit card and TMB Loan
                if (gblEditBillMethod == 0 || gblEditBillMethod == 1) {
                    forBPBillPmtInq(gblEditBillMethod);
                } else {
                    frmIBBillPaymentView.lblBPPaymentFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
                    forBPCallCustomerAccountInq();
                    //frmIBBillPaymentView.show();
                    //dismissLoadingScreenPopup();
                }
            } else {
                dismissLoadingScreenPopup();
                //alert(result["StatusDesc"]);
                alert(result["AdditionalStatusDesc"]);
            }
            // }
        } else {
            dismissLoadingScreenPopup();
            //alert(result["errmsg"]);
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function dateFormatForDisplay(date) {
	//Day format 2013-07-30	
	var dateSplit = date.split("-");
	var formattedDate = dateSplit[2] + "/" + dateSplit[1] + "/" + dateSplit[0];
	return formattedDate;
}

function dateFormatForDisplayWithTimeStamp(datetimestamp) {
	var year = datetimestamp.substring(0, 4);
	var month = datetimestamp.substring(5, 7);
	var date = datetimestamp.substring(8, 10);
	var TimeofActivation = datetimestamp.substring(11, 19);
	var formatedDatewithTimeStamp = date + "/" + month + "/" + year + " " + TimeofActivation;
	NormalDate=date + "/" + month + "/" + year;
	return formatedDatewithTimeStamp;
}
/*
   **************************************************************************************
		Module	: forBPCallCustomerAccountInq
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forBPCallCustomerAccountInq() {
	var inputparam = [];
	inputparam["billPayInd"] = "billPayInd";
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, forBPCusAccInqCallBackFunction);
}
/*
   **************************************************************************************
		Module	: forBPcusAccInqcallBackFunction
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forBPCusAccInqCallBackFunction(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			/*	var statusCode = result["statusCode"];
            var severity = result["severity"];
            var statusDesc = result["statusDesc"];
            if (statusCode != 0) {
                
            } else { */
			if (result["custAcctRec"].length > 0 && result["custAcctRec"] != null) {
			var accFound = false;
			var ICON_ID = "";
				for (var i = 0; i < result["custAcctRec"].length; i++) {
					var cusAccNo = result["custAcctRec"][i]["accId"];
					var pmtFromAcctNo = gblFromAccNo;
					/*
					if(cusAccNo.length == "14"){
						cusAccNo = cusAccNo.substring(4, cusAccNo.length);
					}*/
					/*
					if (gblFromAccType == "SDA") {
						pmtFromAcctNo = "0000" + gblFromAccNo;
					} else {
						pmtFromAcctNo = gblFromAccNo;
					}
					*/
					// Below code added due to  DEF734 defect for IB billpayment
					if(gblFromAccType == "SDA" || gblFromAccType == "CDA" ){
                        if(pmtFromAcctNo.length == "10")
                              pmtFromAcctNo = "0000" + gblFromAccNo;
                    } else {
                             pmtFromAcctNo = gblFromAccNo;
                    }
					if (cusAccNo == pmtFromAcctNo) {
						accFound = true;
						var fromAccNickName = result["custAcctRec"][i]["acctNickName"];
						var fromAcctStatus = result["custAcctRec"][i]["personalisedAcctStatusCode"];
						var fromAccName = result["custAcctRec"][i]["accountName"];
						gblAvailBal = result["custAcctRec"][i]["availableBal"];
						var productID =result["custAcctRec"][i]["productID"];
					     ICON_ID = result["custAcctRec"][i]["ICON_ID"];
						if (fromAcctStatus == "02") {
							if (frmIBBillPaymentView.hbxFromAccnt.isVisible) {
								frmIBBillPaymentView.hbxFromAccnt.setVisibility(false);
							}
							if (!frmIBBillPaymentView.richtextFromAccHidden.isVisible) {
								frmIBBillPaymentView.hbox587850322128235.setVisibility(true);
								frmIBBillPaymentView.richtextFromAccHidden.setVisibility(true);
							}
							// var delAccMessage = kony.i18n.getLocalizedString("FromAccDelMessage"); //get this from i18n //"You already deleted this account. Please add the account under <a href=link>My Account List</a> before proceeding with this future bill payment!";
							// "You already deleted this account. Please add the account under <a onclick= \"return onClickCallBackFunction();\" href = \"#\"  >My Account List</a> before proceeding with this future bill payment!";
							/// hide the formAccount details
							var delAccMessage = kony.i18n.getLocalizedString("S2S_DelAcctMsg") +
								"<a onclick= \"return onClickCallBackFunction();\" href = \"#\"  >" + kony.i18n.getLocalizedString(
									"S2S_MyAcctList") + "</a>" + kony.i18n.getLocalizedString("S2S_BeforeProcTrans");
							frmIBBillPaymentView.richtextFromAccHidden.text = delAccMessage;
							frmIBBillPaymentView.richtextFromAccHidden.skin = lblIB18pxBlack;
							// frmIBBillPaymentView.richtextFromAccHidden.onClick = onClickCallBackFunction();
							frmIBBillPaymentView.buttonEdit.setEnabled(false);
							frmIBBillPaymentView.buttonEdit.skin = "btnIBediticonsmall"; // check for the skin - To Do 
						} else {
							//enable buttons and hbox 
							if (!frmIBBillPaymentView.hbxFromAccnt.isVisible) {
   									frmIBBillPaymentView.hbxFromAccnt.setVisibility(true);
							}
							if (frmIBBillPaymentView.richtextFromAccHidden.isVisible) {
    								 frmIBBillPaymentView.hbox587850322128235.setVisibility(false);
     								 frmIBBillPaymentView.richtextFromAccHidden.setVisibility(false);
							}
							frmIBBillPaymentView.buttonEdit.setEnabled(true);
							frmIBBillPaymentView.buttonEdit.skin = "btnIBediticonsmall"; 
							
							//If nickname not found, then we need to show product name +last 4-digits of the account number
							//concatenated (default nickname). Ex: TMB No Fixed 1234 // In Tech Spec
							if (fromAccNickName != null && fromAccNickName != "") {
								frmIBBillPaymentView.lblBPFromAccNickName.text = fromAccNickName; // from Acc Nick Name
							} else {
								//for eng  :ProductNameEng
								//for TH : ProductNameThai
								var local = kony.i18n.getCurrentLocale();
								var length = cusAccNo.length;
								var accNum = cusAccNo.substring(length - 4, length); // last four digits of the account number
								if (local == "en_US") {
									var productNameEng = result["custAcctRec"][i]["productNmeEN"];
									frmIBBillPaymentView.lblBPFromAccNickName.text = productNameEng + " " + accNum;
								} else if (local == "th_TH") {
									var productNameThai = result["custAcctRec"][i]["productNmeTH"];
									frmIBBillPaymentView.lblBPFromAccNickName.text = productNameThai + " " + accNum;
								}
							}
							frmIBBillPaymentView.lblBPFromAccName.text = fromAccName;
							frmIBBillPaymentView.lblBPFromAccNumber.text = addHyphenIB(cusAccNo); //from Acc Number

				
		   var frmProdIcon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+ICON_ID+"&modIdentifier=PRODICON";
  		  //var frmProdIcon = "https://vit.tau2904.com:443/tmb/ImageRender?crmId=&&personalizedId=&billerId="+ICON_ID+"&modIdentifier=PRODICON";		
			frmIBBillPaymentView.imageFromAccPic.src = frmProdIcon+".png";
            frmIBEditFutureBillPaymentPrecnf.image247502979411375.src = frmProdIcon+".png";




							//Assigning from Account images
						/*
							if(gblFromAccType == "SDA"){
									if(productID == "222"){
										    	frmIBBillPaymentView.imageFromAccPic.src = "prod_nofee.png";
										    	frmIBEditFutureBillPaymentPrecnf.image247502979411375.src = "prod_nofee.png";
									 }else if(productID == "221"){
										    	frmIBBillPaymentView.imageFromAccPic.src = "piggyblueico.png";
										    	frmIBEditFutureBillPaymentPrecnf.image247502979411375.src = "piggyblueico.png";
									 }else if(productID == "203" || productID == "206"){
											    frmIBBillPaymentView.imageFromAccPic.src = "piggyblueico.png";
											    frmIBEditFutureBillPaymentPrecnf.image247502979411375.src = "piggyblueico.png";
									 }else{
										        frmIBBillPaymentView.imageFromAccPic.src = "prod_currentac.png";
										        frmIBEditFutureBillPaymentPrecnf.image247502979411375.src = "prod_currentac.png";
									 }
							}else if(gblFromAccType == "DDA"){
										 	frmIBBillPaymentView.imageFromAccPic.src = "prod_currentac.png";
										 	frmIBEditFutureBillPaymentPrecnf.image247502979411375.src = "prod_currentac.png";  
							}else if(gblFromAccType == "CDA"){
										    frmIBBillPaymentView.imageFromAccPic.src = "prod_termdeposits.png";
										    frmIBEditFutureBillPaymentPrecnf.image247502979411375.src = "prod_termdeposits.png";
							}
							*/
						}
						//forBPMasterBillInq();
						//forBPCustomerBillInquiry();
						frmIBBillPaymentView.show();
						dismissLoadingScreenPopup();
					} else {
					 	if(result["custAcctRec"].length == i+1){ 
								if( !accFound){
									dismissLoadingScreenPopup();
									//alert("No Records Found for the given Account num");
								}
							}
					 }
				}
			} else {
				dismissLoadingScreenPopup();
				
			}
			// }
		} else {
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
		}
	}
}
/*
   **************************************************************************************
		Module	: onClickCallBackFunction
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function onClickCallBackFunction() {
	frmIBMyAccnts.show();
}
/*
   **************************************************************************************
		Module	: forBPBillPmtInq
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forBPBillPmtInq(EditBillMethodIB) {
	var inputParam = [];
	var tranCode;
	if (gblFromAccType == "DDA") {
		tranCode = "88" + "10";
	} else {
		tranCode = "88" + "20";
	}
	inputParam["tranCode"] = tranCode; // from paymentInq //Transaction Code
	inputParam["fromAcctTypeValue"] = gblFromAccType; //From Account Type from paymentInq (commented for testing) 
	inputParam["postedDate"] = changeDateFormatForService(gblStartOn); //Payment Date
	inputParam["ePayCode"] = "EPYS"; //Fixed value if ePayment	"value""EPYS"" - ePayment"
	inputParam["waiveCode"] = "I";
	inputParam["fIIdent"] = ""; //Financial Identity Information of To Account

	if (EditBillMethodIB == 0){ // Offline Bill Payment
		//MIB-6010 P2P Bill Payment	
		var mobileOrCI = "05";
		inputParam["mobileOrCI"] = mobileOrCI;
		if(gblBillerCompcode.length > 4){
			inputParam["compCode"] = "";		
		}else{
			inputParam["compCode"] = gblBillerCompcode;		
		}
		inputParam["toAcctNo"] = gblBillerTaxID;
		inputParam["fromAcctNo"] = gblFromAccNo; //From Account Number from paymentInq 
		inputParam["transferAmt"] = gblAmtFromService; //BillPayment Amount from paymentInq 
		inputParam["ref1"] = removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text); // Refernece 1 
		inputParam["ref2"] = removeHyphenIB(frmIBBillPaymentView.labelBPRef2Val.text); //Refernece 2 check this 
		inputParam["ref3"] = "";
		inputParam["ref4"] = "";		
		invokeServiceSecureAsync("promptPayInq", inputParam, forBPpromptPayInqServiceCallBack);
	}else{ // Online Bill Payment
		inputParam["compCode"] = gblBillerCompcode; //get from masterbillInq		
		inputParam["fromAcctIdentValue"] = gblFromAccNo; //From Account Number from paymentInq 		
		inputParam["transferAmount"] = gblAmtFromService; //BillPayment Amount from paymentInq 
		inputParam["pmtRefIdent"] = removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text); // Refernece 1 
		inputParam["invoiceNumber"] = ""; //frmIBBillPaymentView.labelBPRef2Val.text; //Refernece 2 check this		
		invokeServiceSecureAsync("billPaymentInquiry", inputParam, forBPBillPaymentInquiryServiceCallBack);
	}
}

function forBPpromptPayInqServiceCallBack(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["WaiveFlag"] != null) {
				if (result["WaiveFlag"] == "Y") {
					frmIBBillPaymentView.lblBPPaymentFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				} else {
					frmIBBillPaymentView.lblBPPaymentFeeVal.text = commaFormatted(result["FeeAmt"]) + " " + kony
						.i18n.getLocalizedString("currencyThaiBaht");
				}
				forBPCallCustomerAccountInq();
				//frmIBBillPaymentView.show();
				//dismissLoadingScreenPopup();
			} else {
				dismissLoadingScreenPopup();
				alert(result["errMsg"]);
			}
		} else {
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
			//
		}
	}
}
/*
   **************************************************************************************
		Module	: forBPbillPaymentInquiryServiceCallBack
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forBPBillPaymentInquiryServiceCallBack(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["BillPmtInqRs"].length > 0 && result["BillPmtInqRs"] != null) {
				if (result["BillPmtInqRs"][0]["WaiveFlag"] == "Y") {
					frmIBBillPaymentView.lblBPPaymentFeeVal.text = "0.00" + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				} else {
					frmIBBillPaymentView.lblBPPaymentFeeVal.text = commaFormatted(result["BillPmtInqRs"][0]["FeeAmount"]) + " " + kony
						.i18n.getLocalizedString("currencyThaiBaht");
				}
				forBPCallCustomerAccountInq();
				//frmIBBillPaymentView.show();
				//dismissLoadingScreenPopup();
			} else {
				dismissLoadingScreenPopup();
				alert(result["errMsg"]);
			}
		} else {
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
			//
		}
	}
}
/*
   **************************************************************************************
		Module	: checkMBIBStatus
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function checkMBIBStatus() {
	var inputparam = [];
	inputparam["crmId"] = "";
	inputparam["rqUUId"] = "";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForEditBP);
}
/*
   **************************************************************************************
		Module	: callBackCrmProfileInqForEditBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCrmProfileInqForEditBP(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var statusCode = result["statusCode"];
			var severity = result["Severity"];
			var statusDesc = result["StatusDesc"];
			if (statusCode != 0) {
				alert(statusDesc);
			} else {
				var ibStat = result["ibUserStatusIdTr"];
				if (ibStat != null) {
					var inputParam = [];
					inputParam["ibStatus"] = ibStat;
					invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckStatus);
				}
			}
		} else {
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
		}
	} else {
		//dismissLoadingScreenPopup();
		if (status == 300) {
			
		}
	}
}
/*
   **************************************************************************************
		Module	: callBackCheckStatus
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCheckStatus(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["ibStatusFlag"] == "true") {
				//alert("Your transaction has been locked, so you can't Edit Bill Payment");
				//alert(kony.i18n.getLocalizedString("Error_UserStatusLocked"));
				dismissLoadingScreenPopup();
				popIBBPOTPLocked.show();
				
				return false;
			} else {
				editBillpayment();
			}
		} else {
			dismissLoadingScreenPopup();
			
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
	
}
///Edit
/*
   **************************************************************************************
		Module	: editBillpayment
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function editBillpayment() {
//
	//if (!frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
	//    frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(true);
	//}
	if (!frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
		//frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
		frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(true);
	}
	if (frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible) {
		frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(false);
	}
	if (!frmIBBillPaymentView.lblBPeditHrd.isVisible) {
		frmIBBillPaymentView.lblBPeditHrd.setVisibility(true);
	}
	if (frmIBBillPaymentView.lblBPViewHrd.isVisible) {
		frmIBBillPaymentView.lblBPViewHrd.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxBPViewHdr.isVisible) {
		frmIBBillPaymentView.hbxBPViewHdr.setVisibility(false);
	}
	//NEw Header changes
	if(frmIBBillPaymentView.buttonDel.isVisible){
		frmIBBillPaymentView.buttonDel.setVisibility(false);
	}
	if(frmIBBillPaymentView.buttonEdit.isVisible){
		frmIBBillPaymentView.buttonEdit.setVisibility(false);
	}	
	// --	
	
	if (!frmIBBillPaymentView.btnBPSchedule.isVisible) {
		frmIBBillPaymentView.btnBPSchedule.setVisibility(true);
	}
	if (frmIBBillPaymentView.hbxbtnReturn.isVisible) {
		frmIBBillPaymentView.hbxbtnReturn.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hbxEditBtns.isVisible) {
		frmIBBillPaymentView.hbxEditBtns.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
		frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(true);
	}
	 frmIBBillPaymentView.lblBPEditAvailableBalanceVal.text = commaFormatted(gblAvailBal) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
    if (gblEditBillMethod == 0 && gblEditBillerGroupType == 0) {
    	var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBBillPaymentView.lblBPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = gblStartOn; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsIB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmtFromService; //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
        frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
        dismissLoadingScreenPopup();
    } else if (gblEditBillMethod == 1 && gblEditBillerGroupType == 0) {
        var inputparam = [];
        inputparam["rqUID"] = "";
        inputparam["EffDt"] = "";
        inputparam["TrnId"] = ""; // change this value when integrated with calender / future transactions page
        inputparam["Amt"] = "0.00"; /// for billpayment it is 0.00
        inputparam["compCode"] = gblBillerCompcode;
        //inputparam["TranCode"] = gblTransCode;
        //inputparam["AcctId"] = gblFromAccNo;
        //inputparam["AcctTypeValue"] = gblFromAccType;
        inputparam["MobileNumber"] = removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);
		inputparam["BankId"]="011";
		inputparam["BranchId"]= "0001";
        invokeServiceSecureAsync("onlinePaymentInqForEditBP", inputparam, forBPOnlinePaymentInquiryServiceCallBack);
    } else if (gblEditBillMethod == 2 && gblEditBillerGroupType == 0) {
        var inputparam = [];
       // inputparam["cardId"] = "00000000"+removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);
	    inputparam["cardId"] = gblRef1ValueBP; //removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);
        inputparam["waiverCode"] = "I";
        inputparam["tranCode"] = TRANSCODEMIN;
        inputparam["postedDt"] = getTodaysDate(); // current date you inq for smt
        inputparam["rqUUId"] = "";
        //creditcardDetailsInq  creditcardDetailsInqNonSec
        var cardId = gblRef1ValueBP;
        gblownCardBP = false;
	    
	    var accountLength =gblAccountTable["custAcctRec"].length;
	    
	    for(var i=0;i<accountLength;i++){
	    	
	    	if(gblAccountTable["custAcctRec"][i].accType=="CCA"){
	    		
	  			var accountNo = gblAccountTable["custAcctRec"][i].accId;
	    		accountNo = accountNo.substring(accountNo.length-16 , accountNo.length);
	    		
	    		if(accountNo == cardId){
	    			gblownCardBP =true;
	    			break;
	    		}
		   	}
	   }
        if(gblownCardBP){
        	invokeServiceSecureAsync("creditcardDetailsInq", inputparam, forBPCreditCardDetailsInqServiceCallBack);
        }else{
        	if (frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
				frmIBBillPaymentView.hbxOnlinePayment.setVisibility(false);
			}
			if (!frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
				frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(true);
			}
			
			frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
			dismissLoadingScreenPopup();
        
        }
    } else if (gblEditBillMethod == 3) {
        var inputparam = [];
        var accNumLon = removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);
        var fiident = "";
        var ref2 = frmIBBillPaymentView.labelBPRef2Val.text;
        /*
            if (accNumLon.length == 10) {
                
                fiident = "0011" + "0001" + "0" + accNumLon[0] + accNumLon[1] + accNumLon[2] + "0000";
                accNumLon = "0000" + accNumLon;;
            }
            if (accNumLon.length == 13) {
                
                fiident = "0011" + "0001" + "0" + accNumLon[0] + accNumLon[1] + accNumLon[2] + "0000";
                accNumLon = "0" + accNumLon;
            } else {
                fiident = "0011" + "0001" + "0" + accNumLon[1] + accNumLon[2] + accNumLon[3] + "0000";
            }
            */
        var acctId = "0" + accNumLon+ref2;
        inputparam["acctId"] =  "0" + accNumLon+ref2;//frmIBBillPaymentView.lblBPRef1Val.text;
        inputparam["acctType"] = "LOC";	//kony.i18n.getLocalizedString("Loan");
        inputparam["rqUUId"] = "";
        
        var ownAcnt = false;
        var accountLength =gblAccountTable["custAcctRec"].length;
	    
	    for(var i=0;i<accountLength;i++){
	    	
	    	if(gblAccountTable["custAcctRec"][i].accType=="LOC"){
	  			var accountNo = gblAccountTable["custAcctRec"][i].accId;
	    		if(accountNo == acctId){
	    			ownAcnt =true;
	    			break;
	    		}
		   	}
	    }
        
        if(ownAcnt){
        	invokeServiceSecureAsync("doLoanAcctInq", inputparam, forBPLoanAccountInquiryServiceCallBack);
        }else{
			
			if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
				frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
			}
			
			if (frmIBBillPaymentView.hboxPenaltyAmt.isVisible) {
				frmIBBillPaymentView.hboxPenaltyAmt.setVisibility(false);
			}
			if (frmIBBillPaymentView.hboxOnlineLoan.isVisible) {
				frmIBBillPaymentView.hboxOnlineLoan.setVisibility(false);
			}
			if (!frmIBBillPaymentView.hboxOnlinePayPenalty.isVisible) {
				frmIBBillPaymentView.hboxOnlinePayPenalty.setVisibility(true);
			}
			if (!frmIBBillPaymentView.hboxTotalAmount.isVisible) {
				frmIBBillPaymentView.hboxTotalAmount.setVisibility(true);
			}
			
			
			//frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
			frmIBBillPaymentView.textboxOnlineTotalAmt.text = frmIBBillPaymentView.lblBPAmtValue.text;
			dismissLoadingScreenPopup();
        	
        }
        
    }else{
    	
    	if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
		//frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
			frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
		}
		if (!frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible) {
			frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true);
		}
		if (frmIBBillPaymentView.lblBPeditHrd.isVisible) {
			frmIBBillPaymentView.lblBPeditHrd.setVisibility(false);
		}
		if (!frmIBBillPaymentView.lblBPViewHrd.isVisible) {
			frmIBBillPaymentView.lblBPViewHrd.setVisibility(true);
		}
		if (!frmIBBillPaymentView.hbxBPViewHdr.isVisible) {
			frmIBBillPaymentView.hbxBPViewHdr.setVisibility(true);
		}
		
		//New Header changes
		if(!frmIBBillPaymentView.buttonEdit.isVisible){
			frmIBBillPaymentView.buttonEdit.setVisibility(true);
		}
		
		if(!frmIBBillPaymentView.buttonDel.isVisible){
			frmIBBillPaymentView.buttonDel.setVisibility(true);
		}
		//--
		
		if (frmIBBillPaymentView.btnBPSchedule.isVisible) {
			frmIBBillPaymentView.btnBPSchedule.setVisibility(false);
		}
		if (!frmIBBillPaymentView.hbxbtnReturn.isVisible) {
			frmIBBillPaymentView.hbxbtnReturn.setVisibility(true);
		}
		if (frmIBBillPaymentView.hbxEditBtns.isVisible) {
			frmIBBillPaymentView.hbxEditBtns.setVisibility(false);
		}
		if (frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
			frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
		}
		dismissLoadingScreenPopup();
    	alert("you can't edit this Bill Payment ");
    }
    frmIBBillPaymentView.txtBPEditAmtValue.setFocus(true)
    
}
/*
   **************************************************************************************
		Module	: forBPOnlinePaymentInquiryServiceCallBack
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forBPOnlinePaymentInquiryServiceCallBack(status, result) {
	gblMinAmount = "";
	gblMaxAmount = "";
	gblPayFull = "";
	if (status == 400) {
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBBillPaymentView.lblBPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = gblStartOn; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsIB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmtFromService; //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
		if (result["opstatus"] == 0) {
			if (result["OnlinePmtInqRs"] != undefined && result["OnlinePmtInqRs"].length > 0 && result["OnlinePmtInqRs"] != null) {
				activityStatus = "00";
				fullAmtValue = ""; 
				//var miscName = result["OnlinePmtInqRs"][0]["MiscName"];
				//var miscText = result["OnlinePmtInqRs"][0]["MiscText"];
				if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
						//frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
					frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
					}
				if (!frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible) {
					frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true); // to display amount
				}
				if(!frmIBBillPaymentView.hboxPenaltyAmt.isVisible){
					frmIBBillPaymentView.hboxPenaltyAmt.setVisibility(true);	// to display penalty amount
				}	
				if(!frmIBBillPaymentView.hboxOnlineLoan.isVisible){
						frmIBBillPaymentView.hboxOnlineLoan.setVisibility(true); // to display buttons
				}
				if(!frmIBBillPaymentView.hboxTotalAmount.isVisible){
						frmIBBillPaymentView.hboxTotalAmount.setVisibility(true); // to display total amount
				}
				if (!frmIBBillPaymentView.hboxOnlinePayPenalty.isVisible) {
				frmIBBillPaymentView.hboxOnlinePayPenalty.setVisibility(true);
			}
				
				if(result["OnlinePmtMiscDataDisp"].length >0 && result["OnlinePmtMiscDataDisp"] != null){
					for(i=0; i < result["OnlinePmtMiscDataDisp"].length ;i++ ){
						if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "BilledAmt"){
							var billedAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
							var billedAmtLabelTH = result["OnlinePmtMiscDataDisp"][i]["MiscName_TH"];
							var billedAmtValue = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
							frmIBBillPaymentView.lblBPAmt.text = billedAmtLabelEN;
							frmIBBillPaymentView.lblBPAmtValue.text = commaFormatted(billedAmtValue)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");;
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "FullAmt"){
							//var fullAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
							//var fullAmtLabelTH = result["OnlinePmtMiscDataDisp"][i]["MiscName_TH"];
							fullAmtValue = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
							//frmIBBillPaymentView.labelPenalty.text = fullAmtLabelEN;
							//frmIBBillPaymentView.textboxOnlineTotalAmt.text = fullAmtValue;
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "PenaltyAmt"){
							var penaltyAmtLabelEN = result["OnlinePmtMiscDataDisp"][i]["MiscName_EN"];
							var penaltyAmtLabelTH = result["OnlinePmtMiscDataDisp"][i]["MiscName_TH"];
							var penaltyAmtValue = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
							frmIBBillPaymentView.labelPenalty.text = penaltyAmtLabelEN;
							frmIBBillPaymentView.label587850322124849.text = commaFormatted(penaltyAmtValue)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");;
						}   
						//Getting Min Max amount from the service 
						else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "Minimum"){
							gblMinAmount = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
						}else if(result["OnlinePmtMiscDataDisp"][i]["MiscName"] == "Maximum"){
							gblMaxAmount = result["OnlinePmtMiscDataDisp"][i]["MiscText"];
						}
					}
				
				}else{
					//New 
						
					var arrayTemp = result["OnlinePmtInqRs"][0]["OnlinePmtMiscData"];
					if(arrayTemp != undefined && arrayTemp.length > 0){
						for(var i=0; i < arrayTemp.length ;i++ ){
							if(arrayTemp[i]["MiscName"] == "BilledAmt"){
								var billedAmtValue = arrayTemp[i]["MiscText"];
								frmIBBillPaymentView.lblBPAmtValue.text = commaFormatted(billedAmtValue)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");;
							}else if(arrayTemp[i]["MiscName"] == "FullAmt"){
								fullAmtValue = arrayTemp[i]["MiscText"];
							}else if(arrayTemp[i]["MiscName"] == "PenaltyAmt"){
								var penaltyAmtValue = arrayTemp[i]["MiscText"];
								frmIBBillPaymentView.label587850322124849.text = commaFormatted(penaltyAmtValue)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");;
							}   
							//Get the min max value here
							else if(arrayTemp[i]["MiscName"] == "Minimum"){
								gblMinAmount = arrayTemp[i]["MiscText"];
							}else if(arrayTemp[i]["MiscName"] == "Maximum"){
								gblMaxAmount = arrayTemp[i]["MiscText"];
							}
						}
					}	
					//	
				}
				gblPayFull = result["isFullPayment"];
				if(gblPayFull == "Y") {
					frmIBBillPaymentView.hboxOnlineLoan.setVisibility(false);
					frmIBBillPaymentView.textboxOnlineTotalAmt.setEnabled(false);
				}
				frmIBBillPaymentView.textboxOnlineTotalAmt.text = commaFormatted(gblAmtFromService)+ " " + kony.i18n.getLocalizedString("currencyThaiBaht");;;
				frmIBBillPaymentView.buttononlineFull.skin = "btnIBrndleftGreyNoWidth";
				frmIBBillPaymentView.buttononlineSpecified.skin = "btnIBrndRightBlueNoWidth";
				frmIBBillPaymentView.show();
				dismissLoadingScreenPopup();
			} else {
				activityStatus = "02"; 
				dismissLoadingScreenPopup();
				
				resetToView();
				alert(result["errMsg"]);
				//return false;
			}
			
		} else {
			activityStatus = "02"; 
			dismissLoadingScreenPopup();
			resetToView();
			alert(result["errMsg"]);
			//return false;
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}
/*
   **************************************************************************************
		Module	: forBPCreditCardDetailsInqServiceCallBack
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forBPCreditCardDetailsInqServiceCallBack(status, result) {
	if (status == 400) {
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBBillPaymentView.lblBPBillNameAndCompCode.text +"+"+removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = gblStartOn; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsIB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmtFromService; //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
		if (result["opstatus"] == 0) {
			activityStatus = "00";
			//if(result["OnlinePmtInqRs"].length > 0 && result["OnlinePmtInqRs"] != null){
			if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
				frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
			}
			fullAmount = "";
			minAmount = "";
			
			
			if (result["fullPmtAmt"] != "0.00" && result["fullPmtAmt"] != undefined) {
				fullAmount = commaFormatted(result["fullPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				if(parseInt(fullAmount)<0)gblPaymentOverpaidFlag=true;
			} else {
				fullAmount = result["fullPmtAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			if (result["minPmtAmt"] != "0.00" && result["minPmtAmt"] != undefined) {
				minAmount = commaFormatted(result["minPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				minAmount = result["minPmtAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			
			
			if (!frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
				frmIBBillPaymentView.hbxOnlinePayment.setVisibility(true);
			}
			frmIBBillPaymentView.btnOnlineSpecified.skin = btnIBgreyActive;
			frmIBBillPaymentView.btnOnlineFull.skin = btnIBgreyInactive;
			frmIBBillPaymentView.btnOnlineMin.skin = btnIBgreyInactive;
			//enable specified button and amt text
			frmIBBillPaymentView.textboxOnlineAmtvalue.setEnabled(true);
			frmIBBillPaymentView.textboxOnlineAmtvalue.text = frmIBBillPaymentView.lblBPAmtValue.text;
			paymentOverpaidCheck();
			/*   
            if (result["ledgerBal"] != null) {
            
                if (!frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
                    frmIBBillPaymentView.hbxOnlinePayment.setVisibility(true);
                }
                frmIBBillPaymentView.btnOnlineFull.skin = btnIBgreyActive;
                frmIBBillPaymentView.btnOnlineMin.skin = btnIBgreyInactive;
                frmIBBillPaymentView.btnOnlineSpecified.skin = btnIBgreyInactive;
                frmIBBillPaymentView.textboxOnlineAmtvalue.setEnabled(false);
                // enable full button and amt text 
                frmIBBillPaymentView.textboxOnlineAmtvalue.text = commaFormatted(result["ledgerBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
           
           		var fullAmount = commaFormatted(result["ledgerBal"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            } else if (result["lastPmtAmt"] != null) {
            
                if (!frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
                    frmIBBillPaymentView.hbxOnlinePayment.setVisibility(true);
                }
                frmIBBillPaymentView.btnOnlineMin.skin = btnIBgreyActive;
                frmIBBillPaymentView.btnOnlineSpecified.skin = btnIBgreyInactive;
                frmIBBillPaymentView.btnOnlineFull.skin = btnIBgreyInactive;
                frmIBBillPaymentView.textboxOnlineAmtvalue.setEnabled(false);
                // enable min button and amt text 
                frmIBBillPaymentView.textboxOnlineAmtvalue.text = commaFormatted(result["lastPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
           	
           		var minAmount = commaFormatted(result["lastPmtAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            } else {
                if (!frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
                    frmIBBillPaymentView.hbxOnlinePayment.setVisibility(true);
                }
                frmIBBillPaymentView.btnOnlineSpecified.skin = btnIBgreyActive;
                frmIBBillPaymentView.btnOnlineFull.skin = btnIBgreyInactive;
                frmIBBillPaymentView.btnOnlineMin.skin = btnIBgreyInactive;
                //enable specified button and amt text
                frmIBBillPaymentView.textboxOnlineAmtvalue.setEnabled(true);
                frmIBBillPaymentView.textboxOnlineAmtvalue.text = frmIBBillPaymentView.lblBPAmtValue.text;
            } */
			//}else{
			dismissLoadingScreenPopup();
			//	
			//}
		} else {
			activityStatus = "02";
			dismissLoadingScreenPopup();
			resetToView();
			alert(result["errMsg"]);
			//return false;
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	
	}
}
/*
   **************************************************************************************
		Module	: forBPLoanAccountInquiryServiceCallBack
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forBPLoanAccountInquiryServiceCallBack(status, result) {
	if (status == 400) {
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBBillPaymentView.lblBPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = gblStartOn; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsIB;  //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmtFromService;  //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
		if (result["opstatus"] == 0) {
			activityStatus = "00";
			//  if (result["acctBal"].length > 0 && result["acctBal"] != null) {
			if (frmIBBillPaymentView.hboxPenaltyAmt.isVisible) {
				frmIBBillPaymentView.hboxPenaltyAmt.setVisibility(false);
			}
			if (!frmIBBillPaymentView.hboxOnlineLoan.isVisible) {
				frmIBBillPaymentView.hboxOnlineLoan.setVisibility(true);
			}
			if (!frmIBBillPaymentView.hboxOnlinePayPenalty.isVisible) {
				frmIBBillPaymentView.hboxOnlinePayPenalty.setVisibility(true);
			}
			if (!frmIBBillPaymentView.hboxTotalAmount.isVisible) {
				frmIBBillPaymentView.hboxTotalAmount.setVisibility(true);
			}
			// display full and specified amt
			fullAmtLoan = "";
			if (result["regPmtCurAmt"] != "0.00") {
				fullAmtLoan = commaFormatted(result["regPmtCurAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				//	frmIBBillPaymentView.textboxOnlineTotalAmt.text = commaFormatted(result["regPmtCurAmt"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			} else {
				fullAmtLoan = result["regPmtCurAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
				//	frmIBBillPaymentView.textboxOnlineTotalAmt.text = result["regPmtCurAmt"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
			}
			frmIBBillPaymentView.textboxOnlineTotalAmt.text = frmIBBillPaymentView.lblBPAmtValue.text;
			if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
				frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
			}
			frmIBBillPaymentView.buttononlineFull.skin = "btnIBrndleftGreyNoWidth";
			frmIBBillPaymentView.buttononlineSpecified.skin = "btnIBrndRightBlueNoWidth";
			dismissLoadingScreenPopup();
			
			var fullAmtLoanTemp = fullAmtLoan;
			fullAmtLoanTemp = fullAmtLoanTemp.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(fullAmtLoanTemp.indexOf(",") != -1)fullAmtLoanTemp = replaceCommon(fullAmtLoanTemp, ",", "");
			var amtTemp = frmIBBillPaymentView.lblBPAmtValue.text;
			amtTemp = amtTemp.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(amtTemp.indexOf(",") != -1)amtTemp = replaceCommon(amtTemp, ",", "");
			
			if(parseFloat(fullAmtLoanTemp) == parseFloat(amtTemp)){
				frmIBBillPaymentView.buttononlineSpecified.skin = "btnIBrndrightGreyNowidth";
				frmIBBillPaymentView.buttononlineFull.skin="btnIBrndleftblueNoWidth";
			}else{
				frmIBBillPaymentView.buttononlineSpecified.skin = "btnIBrndRightBlueNoWidth";
				frmIBBillPaymentView.buttononlineFull.skin="btnIBrndleftGreyNoWidth";
			}
			
			
			//regPmtCurAmt
			//   } else {
			//       dismissLoadingScreenPopup();
			//       
			//   }
		} else {
			activityStatus = "02";
			dismissLoadingScreenPopup();
			resetToView();
			alert(result["errMsg"]);
			//return false;
		}
			activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}
/*
   **************************************************************************************
		Module	: numberOfExecution
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
// for Execution times 

function numberOfExecution(date1, date2, repeatAs) {
	var startDate = parseDate(date1);
	var endDate = parseDate(date2);
	var executionTimes = "";
	if (kony.string.equalsIgnoreCase(repeatAs, "Daily")) {
		executionTimes = daydiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, "Weekly")) {
		executionTimes = weekdiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, "Monthly")) {
		executionTimes = monthdiff(startDate, endDate);
	} else if (kony.string.equalsIgnoreCase(repeatAs, "Yearly")) {
		executionTimes = yeardiff(startDate, endDate);
	}
	return executionTimes + "";
}
/*
   **************************************************************************************
		Module	: endOnDateCalculator
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/


//New caluculation Changes
function endOnDateCalculator(staringDateFromCalendar, repeatTimesTextBoxValue,repeatAs) {
    var endingDate = parseDate(staringDateFromCalendar);
    var numberOfDaysToAdd = 0;
    
	
    if (kony.string.equalsIgnoreCase(repeatAs, "Daily")) {
        numberOfDaysToAdd = repeatTimesTextBoxValue * 1;
            endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd - 1);
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Weekly")) {
        numberOfDaysToAdd = ((repeatTimesTextBoxValue-1) * 7);
        endingDate.setDate(endingDate.getDate() + numberOfDaysToAdd);
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Monthly")) {
            	endingDate.setDate(endingDate.getDate());
                var dd = endingDate.getDate();
                
    			var mm = endingDate.getMonth() + 1;
    			
    			var newmm = parseFloat(mm.toString())+ parseFloat(repeatTimesTextBoxValue - 1);
    			
    			var newmmadd = newmm % 12;
    			if(newmmadd == 0){
    				newmmadd = 12;
    			}
    			
    			var yearAdd = Math.floor((newmm / 12));
    			
   				var y = endingDate.getFullYear();
   				if(newmmadd == 12){
   					y = parseFloat(y )+ parseFloat(yearAdd)-1;
   				}else
   					y = parseFloat(y )+ parseFloat(yearAdd);
   				
   				mm = parseFloat(mm.toString())+ newmmadd;
   				
   				if(newmmadd == 2){
   				if(dd > 28){
   				dd = 28;
   				}
   				}
   				if(newmmadd == 4 || newmmadd == 6 || newmmadd == 9 || newmmadd == 11){
   				if(dd > 30){
   				dd = 30;
   				}
   				}
   				if (dd < 10) {
        			dd = '0' + dd;
   		 		}
   		 		
   		 		if (newmmadd < 10) {
        			newmmadd = '0' + newmmadd
   				 }
   		 		
   				var someFormattedDate = dd + '/' + newmmadd + '/' + y;
   				return someFormattedDate;
        
    } else if (kony.string.equalsIgnoreCase(repeatAs, "Yearly")) {
        var dd = endingDate.getDate();
   		var mm = endingDate.getMonth() + 1;
    	var y = endingDate.getFullYear();
    	var newYear = parseFloat(y)+ parseFloat(repeatTimesTextBoxValue - 1) ;
    	if (dd < 10) {
        	dd = '0' + dd
   		 }
   		 if (mm < 10) {
      	  mm = '0' + mm
   		 }
    var someFormattedDate = dd + '/' + mm + '/' + newYear;
    return someFormattedDate;
    }
    var dd = endingDate.getDate();
    var mm = endingDate.getMonth() + 1;
    var y = endingDate.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var someFormattedDate = dd + '/' + mm + '/' + y;
    return someFormattedDate;
}


/*
   **************************************************************************************
		Module	: onclickScheduleButton
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
gblScheduleButtonClicked = false

function onclickScheduleButton() {
	if (!gblScheduleButtonClicked) {
		gblScheduleButtonClicked = true;
		frmIBBillPaymentView.btnBPSchedule.setEnabled(false);
	}
	frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(true);
	frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(true);
	frmIBBillPaymentView.arrowBPScheduleField.setVisibility(true);
	frmIBBillPaymentView.imgTMB.setVisibility(false);
	
	
	frmIBBillPaymentView.calendarStartOn.validStartDate = currentDateForcalender();
	frmIBBillPaymentView.calendarEndOn.validStartDate = currentDateForcalender();
	if (repeatAsIB == "" && endFreqSave == "") {
		//frmIBBillPaymentView.calendarStartOn.date = frmIBBillPaymentView.lblBPStartOnDateVal.text;
		var d = frmIBBillPaymentView.lblBPStartOnDateVal.text;
		d = d.split("/");
		frmIBBillPaymentView.calendarStartOn.dateComponents = [d[0], d[1], d[2]];
		onLoadShowRepeatFreqIB(gblOnLoadRepeatAsIB);
		onLoadShowEndFreqIB(gblEndingFreqOnLoadIB);
	} else {
		var d1 = frmIBBillPaymentView.lblBPStartOnDateVal.text;
		d1 = d1.split("/");
		//frmIBBillPaymentView.calendarStartOn.date = frmIBBillPaymentView.lblBPStartOnDateVal.text;
		frmIBBillPaymentView.calendarStartOn.dateComponents = [d1[0], d1[1], d1[2]];
		onLoadShowRepeatFreqIB(repeatAsIB);
		onLoadShowEndFreqIB(endFreqSave);
	}
	
    //New
	gblTemprepeatAsBP = repeatAsIB;
	gblTempendFreqSaveBP = endFreqSave;
}

function onLoadShowEndFreqIB(endingFreq) {
	frmIBBillPaymentView.btnBPNever.skin = "btnIBTab3LeftNrml";
	frmIBBillPaymentView.btnBPAfter.skin = "btnIBTab3MidNrml";
	frmIBBillPaymentView.btnBPOnDate.skin = "btnIBTab3RightNrml";
	if (endingFreq == "OnDate") {
		frmIBBillPaymentView.hbxBPUntil.setVisibility(true);
		frmIBBillPaymentView.btnBPOnDate.skin = "btnIBTab3RightFocus";
		// get endOn date and display
		var d2 = frmIBBillPaymentView.lblEndOnDateVal.text;
		d2 = d2.split("/");
		frmIBBillPaymentView.calendarEndOn.dateComponents = [d2[0], d2[1], d2[2]];   //frmIBBillPaymentView.lblEndOnDateVal.text;
		if (frmIBBillPaymentView.hbxBPexecutiontimes.isVisible) {
			frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(false);
		}
	} else if (endingFreq == "After") {
		frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(true);
		frmIBBillPaymentView.lineSchBPClose.setVisibility(false);
		frmIBBillPaymentView.btnBPAfter.skin = "btnIBTab3MidFocus";
		// get execution times and display
		frmIBBillPaymentView.txtBPExecutiontimesval.text = frmIBBillPaymentView.lblBPExcuteVal.text;
		if (frmIBBillPaymentView.hbxBPUntil.isVisible) {
			frmIBBillPaymentView.hbxBPUntil.setVisibility(false);
		}
	} else if (endingFreq == "Never") {
		frmIBBillPaymentView.btnBPNever.skin = "btnIBTab3LeftFocus";
		if (frmIBBillPaymentView.hbxBPexecutiontimes.isVisible) {
			frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(false);
		}
		if (frmIBBillPaymentView.hbxBPUntil.isVisible) {
			frmIBBillPaymentView.hbxBPUntil.setVisibility(false);
		}
	} else {
		//if (frmIBBillPaymentView.hbxBPUntil.isVisible) {
			frmIBBillPaymentView.hbxBPUntil.setVisibility(false);
		//}
		//if (frmIBBillPaymentView.hbxBPexecutiontimes.isVisible) {
			frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(false);
		//}
	}
}

function onLoadShowRepeatFreqIB(repeatFreq) {
	frmIBBillPaymentView.btnBillpayDaily.skin = "btnIBTab4LeftNrml";
	frmIBBillPaymentView.btnBillpayWeekly.skin = "btnIBTab4MidNrml";
	frmIBBillPaymentView.btnBillpayMonthly.skin = "btnIBTab4MidNrml";
	frmIBBillPaymentView.btnBillpayYearly.skin = "btnIbTab4RightNrml";
	if (kony.string.equalsIgnoreCase(repeatFreq, "Daily")) {
		frmIBBillPaymentView.lblBPEnding.setVisibility(true);
		frmIBBillPaymentView.hbxBPEndingBtns.setVisibility(true);
		frmIBBillPaymentView.lineSchBPClose.setVisibility(true);
		frmIBBillPaymentView.btnBillpayDaily.skin = "btnIBTab4LeftFocus";
	} else if (kony.string.equalsIgnoreCase(repeatFreq, "Weekly")) {
		frmIBBillPaymentView.lblBPEnding.setVisibility(true);
		frmIBBillPaymentView.hbxBPEndingBtns.setVisibility(true);
		frmIBBillPaymentView.lineSchBPClose.setVisibility(true);
		frmIBBillPaymentView.btnBillpayWeekly.skin = "btnIBTab4MidFocus";
	} else if (kony.string.equalsIgnoreCase(repeatFreq, "Monthly")) {
		frmIBBillPaymentView.lblBPEnding.setVisibility(true);
		frmIBBillPaymentView.hbxBPEndingBtns.setVisibility(true);
		frmIBBillPaymentView.lineSchBPClose.setVisibility(true);
		frmIBBillPaymentView.btnBillpayMonthly.skin = "btnIBTab4MidFocus";
	} else if (kony.string.equalsIgnoreCase(repeatFreq, "Yearly")) {
		frmIBBillPaymentView.lblBPEnding.setVisibility(true);
		frmIBBillPaymentView.hbxBPEndingBtns.setVisibility(true);
		frmIBBillPaymentView.lineSchBPClose.setVisibility(true);
		frmIBBillPaymentView.btnBillpayYearly.skin = "btnIBTab4RightFocus";
	} else {
		frmIBBillPaymentView.lblBPEnding.setVisibility(false);
		frmIBBillPaymentView.hbxBPEndingBtns.setVisibility(false);
		frmIBBillPaymentView.lineSchBPClose.setVisibility(true);
		frmIBBillPaymentView.hbxBPUntil.setVisibility(false);
		frmIBBillPaymentView.hbxBPexecutiontimes.setVisibility(false);
		
	}
}
/*
   **************************************************************************************
		Module	: editBPscheduleSaveValidations
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
/*
	gblscheduleRepeatasEditFlag = false ;
	gblscheduleEndingEditFlag = false ;	
	gblscheduleStartOnEditFlag = false;
	*/
gblScheduleFreqChanged = false;
var repeatAsIB = "";
var endFreqSave = "";


function editBPscheduleSaveValidations() {
	//gblscheduleRepeatasEditFlag = true ;
	//gblscheduleEndingEditFlag = true ;	
	var scheduleRepeatasEditFlag = false;
	var scheduleEndingEditFlag = false;
	var scheduleStartOnEditFlag = false;
	var d = frmIBBillPaymentView.calendarStartOn.dateComponents;
	
	
	
	  d[0] = d[0]+"";
	  d[0] = (d[0].length == "1")? "0"+d[0]:d[0];
	  d[1] = d[1]+"";
	  d[1] = (d[1].length == "1")? "0"+d[1]:d[1];
	
	var startOnDate = d[0]+"/"+d[1]+"/"+d[2];    //frmIBBillPaymentView.calendarStartOn.formattedDate;
	var executionNumberOftimes = "";
	var endOnDate = "";
	frmIBBillPaymentView.imgTMB.setVisibility(false);
	if (gblScheduleButtonClicked) {
		gblScheduleButtonClicked = false;
		frmIBBillPaymentView.btnBPSchedule.setEnabled(true);
	}
	//check if local variable is updated with the latest value else work with global variable
	//OnClickRepeatAs - repeat as (daily,weekly,monthly,yearly) -local variable
	//OnClickEndFreq - end freq (never,after and ondate) - local variable
	//gblOnLoadRepeatAsIB - repeat As - global variable
	//gblEndingFreqOnLoadIB - end freq - global variable
	if (onClickRepeatAs != gblOnLoadRepeatAsIB && onClickRepeatAs != "") {
		scheduleRepeatasEditFlag = true;
	} else {
		scheduleRepeatasEditFlag = false;
	}
	if (onClickEndFreq != gblEndingFreqOnLoadIB && onClickEndFreq != "") {
		scheduleEndingEditFlag = true;
	} else {
		scheduleEndingEditFlag = false;
	}
	if (onClickRepeatAs != "") {
		repeatAsIB = onClickRepeatAs;
	} else {
		repeatAsIB = gblOnLoadRepeatAsIB;
	}
	if (onClickEndFreq != "") {
		endFreqSave = onClickEndFreq;
	} else {
		endFreqSave = gblEndingFreqOnLoadIB;
	}
	if (repeatAsIB != "Once") {
		if (endFreqSave == "") {
			//alert(" Please select the ending frequency");
			alert(kony.i18n.getLocalizedString("Error_NoEndingValueSelected"));
			repeatAsIB = "";
			scheduleRepeatasEditFlag = false;
			scheduleEndingEditFlag = false;
			return false;
		}
	}
	frmIBBillPaymentView.lblBPStartOnDateVal.text = startOnDate;
	
	//New changes to handle 'Once' display
	if(repeatAsIB == "Once" && endFreqSave == ""){
		frmIBBillPaymentView.lblEndOnDateVal.text = frmIBBillPaymentView.lblBPStartOnDateVal.text;
		frmIBBillPaymentView.lblBPRepeatAsVal.text = "Once";
		frmIBBillPaymentView.lblBPExcuteVal.setVisibility(true);
		frmIBBillPaymentView.lblBPExcuteVal.text = "1"; 
		frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
		frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB"); 
	}
	//--
	if (startOnDate == null || startOnDate == "") {
		//alert("Please select valid start date");
	
		alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
		return false;
	}else{
		var curDt = currentDate();
		if( parseDate(curDt) >= parseDate(startOnDate)){ 
			
			alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));
			return false;
		}
	}
	if (startOnDate == gblStartOn) {
		scheduleStartOnEditFlag = false;
	} else {
		var curDt = currentDate();
		
		
		
		if (parseDate(startOnDate) > parseDate(curDt)) {
			scheduleStartOnEditFlag = true;
		} else {
			//alert("Start Date Should be Greater than Current Date");
			
			alert(kony.i18n.getLocalizedString("Error_InvalidStartDate"));	
			return false;
		}
	}
	if (kony.string.equalsIgnoreCase(endFreqSave, "Never")) {
		//frmIBBillPaymentView.lblBPStartOnDateVal.text = startOnDate;
		if (!frmIBBillPaymentView.hboxRepeatAs.isVisible) {
			frmIBBillPaymentView.hboxRepeatAs.setVisibility(true);
		}
		if (!frmIBBillPaymentView.hboxExecute.isVisible) {
			frmIBBillPaymentView.hboxExecute.setVisibility(true);
		}
		if (!frmIBBillPaymentView.hboxEndOn.isVisible) {
			frmIBBillPaymentView.hboxEndOn.setVisibility(true);
		}
		frmIBBillPaymentView.lblEndOnDateVal.text = "-";
		frmIBBillPaymentView.lblBPExcuteVal.text = "-";
		frmIBBillPaymentView.lblBPExcuteVal.setVisibility(false);
		frmIBBillPaymentView.lblBPExcuteValTimes.text = "-";
		frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
		gblEndingFreqOnLoadIB = "Never";
	} else if (kony.string.equalsIgnoreCase(endFreqSave, "After")) {
		executionNumberOftimes = frmIBBillPaymentView.txtBPExecutiontimesval.text;
		if (executionNumberOftimes == "" || executionNumberOftimes == "0" || executionNumberOftimes == "0.00") {
			scheduleRepeatasEditFlag = false;
			scheduleEndingEditFlag = false;
			//alert("please enter valid number of times");
			alert(kony.i18n.getLocalizedString("Error_EnterNumOfTimes"));
			endFreqSave = "";
			repeatAsIB = "";
			return false;
		}
		if(executionNumberOftimes > 99){
		   scheduleRepeatasEditFlag = false;
			scheduleEndingEditFlag = false;
			//alert("please enter valid number of times");
			alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
			endFreqSave = "";
			repeatAsIB = "";
			return false;
		
		}
		
		
		if (kony.string.isNumeric(executionNumberOftimes) == true) {
			if (executionNumberOftimes > gblExecutionOnLoad || executionNumberOftimes < gblExecutionOnLoad) { //if selected execution times is greater than or less than the execution times from service then set the flag to true
				gblEndingFreqOnLoadIB = "After";
				scheduleEndingEditFlag = true;
			}
		} else {
			//alert("Please enter numeric value");
			alert(kony.i18n.getLocalizedString("keyIBOtpNumeric"));
			return false;
		}
		gblEndingFreqOnLoadIB = "After";
		endOnDate = endOnDateCalculator(startOnDate, executionNumberOftimes, repeatAsIB);
		frmIBBillPaymentView.lblEndOnDateVal.text = endOnDate;
		//frmIBBillPaymentView.lblBPStartOnDateVal.text = startOnDate;
		frmIBBillPaymentView.lblBPExcuteVal.setVisibility(true);
		frmIBBillPaymentView.lblBPExcuteVal.text = executionNumberOftimes;
		frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
		frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB"); 
		if (!frmIBBillPaymentView.hboxRepeatAs.isVisible) {
			frmIBBillPaymentView.hboxRepeatAs.setVisibility(true);
		}
		if (!frmIBBillPaymentView.hboxExecute.isVisible) {
			frmIBBillPaymentView.hboxExecute.setVisibility(true);
		}
		if (!frmIBBillPaymentView.hboxEndOn.isVisible) {
			frmIBBillPaymentView.hboxEndOn.setVisibility(true);
		}
	} else if (kony.string.equalsIgnoreCase(endFreqSave, "OnDate")) {
		endOnDate = frmIBBillPaymentView.calendarEndOn.formattedDate;
		if (endOnDate == null || endOnDate == "") {
			//alert("Please select valid end date");
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));
			return false;
		}
		if (parseDate(endOnDate) <= parseDate(startOnDate)) {
			scheduleRepeatasEditFlag = false;
			scheduleEndingEditFlag = false;
			//alert("End date should be greater than Start Date");
			alert(kony.i18n.getLocalizedString("Error_InvalidaEndDate"));
			return false;
		}
		if (parseDate(endOnDate) > parseDate(gblEndDateOnLoad) || parseDate(endOnDate) < parseDate(gblEndDateOnLoad)) { //if selected end date is greater than or less than the end date from service then set the flag to true
			gblEndingFreqOnLoadIB = "OnDate";
			scheduleEndingEditFlag = true;
		}
		executionNumberOftimes = numberOfExecution(startOnDate, frmIBBillPaymentView.calendarEndOn.formattedDate, repeatAsIB);
		if(executionNumberOftimes > 99){
		    scheduleRepeatasEditFlag = false;
			scheduleEndingEditFlag = false;
			//alert("End date should be greater than Start Date");
			alert(kony.i18n.getLocalizedString("Valid_ErrorRecLessthan99"));
			return false;
		}
		gblEndingFreqOnLoadIB = "OnDate";
		frmIBBillPaymentView.lblEndOnDateVal.text = endOnDate;
		//frmIBBillPaymentView.lblBPStartOnDateVal.text = startOnDate;
		frmIBBillPaymentView.lblBPExcuteVal.setVisibility(true);
		frmIBBillPaymentView.lblBPExcuteVal.text = executionNumberOftimes;
		frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true);
		frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB"); 
		if (!frmIBBillPaymentView.hboxRepeatAs.isVisible) {
			frmIBBillPaymentView.hboxRepeatAs.setVisibility(true);
		}
		if (!frmIBBillPaymentView.hboxExecute.isVisible) {
			frmIBBillPaymentView.hboxExecute.setVisibility(true);
		}
		if (!frmIBBillPaymentView.hboxEndOn.isVisible) {
			frmIBBillPaymentView.hboxEndOn.setVisibility(true);
		}
	}
	if (frmIBBillPaymentView.lblScheduleBillPayment.isVisible) {
		frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxEditBPFutureSchedule.isVisible) {
		frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
	}
	if (frmIBBillPaymentView.arrowBPScheduleField.isVisible) {
		frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
	}
	if (repeatAsIB == "Annually") {
    	frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyYearly");;
    } 
	else if (repeatAsIB == "Monthly") {
       	frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyMonthly");
    }
    else if (repeatAsIB == "Weekly") {
      	frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("keyWeekly");
   	}
	else if (repeatAsIB == "Daily") {
    	frmIBBillPaymentView.lblBPRepeatAsVal.text = kony.i18n.getLocalizedString("Transfer_Daily");
    }
    else {
   		//gblOnLoadRepeatAsIB = repeatAsIB;
      	frmIBBillPaymentView.lblBPRepeatAsVal.text = repeatAsIB;
  	}	
	
	if (scheduleRepeatasEditFlag == true || scheduleEndingEditFlag == true || scheduleStartOnEditFlag == true) {
		gblScheduleFreqChanged = true;
	} else {
		gblScheduleFreqChanged = false;
	}
	frmIBBillPaymentView.imgTMB.setVisibility(true);
}

/*
   **************************************************************************************
		Module	: editBillPaymentonClickNext
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
//gblAmountSelected = false ;

function editBillPaymentOnClickNext() {
	var userEnteredAmt = "";
	var amt = 0;
	if (gblEditBillMethod == 0) {
		amt = frmIBBillPaymentView.txtBPEditAmtValue.text;
		/*	
       	var currencyPosition = 	amt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"));
		if(currencyPosition != -1){
			 userEnteredAmt = amt.substring(0, amt.length -1).trim();
		}else{
			 userEnteredAmt = amt.trim();
		}
		*/
		userEnteredAmt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	} else if (gblEditBillMethod == 2) {
		if(gblownCardBP){
			amt = frmIBBillPaymentView.textboxOnlineAmtvalue.text;
			userEnteredAmt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
		}else{
			amt = frmIBBillPaymentView.txtBPEditAmtValue.text;
			userEnteredAmt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
		}
	} else if (gblEditBillMethod == 1 || gblEditBillMethod == 3) {
		amt = frmIBBillPaymentView.textboxOnlineTotalAmt.text;
		userEnteredAmt = amt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();;
	}
	
	if (userEnteredAmt == "" || userEnteredAmt == "0" || userEnteredAmt == "0.00") {
		//alert("Please enter a valid amount");
		alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));
		gblAmountSelected = false;
		return false;
	} else {
		//if userEnteredAmt is not equal to gblamt then set the flag to true
			var amtvalidation = amtValidationIB(userEnteredAmt);
			if (amtvalidation == false) {
				//alert("Please enter a valid amount");
				alert(kony.i18n.getLocalizedString("Error_Invalid_Amount"));
				gblAmountSelected = false;
				return false;
			} else {
				var amount = "";
				if(gblEditBillMethod == 1){
				 	amount = gblAmtFromService
				}else {
					amount = frmIBBillPaymentView.lblBPAmtValue.text;
					amount = amount.substring(0, amount.length - 1).trim();
				}
				
				amount = amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
				if(amount.indexOf(",") != -1)amount = replaceCommon(amount, ",", "");
				
			//var amount = gblAmtFromService;//frmIBBillPaymentView.lblBPAmtValue.text;
			//amount = amount.substring(0, amount.length - 1)
			//	.trim();
			
			var amtTemp = userEnteredAmt;
			amtTemp = amtTemp.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
			if(amtTemp.indexOf(",") != -1)amtTemp = replaceCommon(amtTemp, ",", "");
			if (parseFloat(amtTemp) != parseFloat(amount)) { // add online amt also
				gblAmountSelected = true;
			} else {
				gblAmountSelected = false;
			}
			gblUserEnteredAmt = userEnteredAmt;
		}
		// gblAmountSelected = true;
	}
	//Validation for min max amount
	if(gblPayFull != "Y") {
		var minAmount = parseFloat(gblMinAmount);
		var maxAmount = parseFloat(gblMaxAmount);
		if((parseFloat(userEnteredAmt) < minAmount) || (parseFloat(userEnteredAmt) > maxAmount)) {
			var message = kony.i18n.getLocalizedString("PlzEnterValidMinMaxAmount");
			message = message.replace("[min]", minAmount);
			message = message.replace("[max]", maxAmount);
			alert(message);
			return false;
		}
	}
	if (gblScheduleButtonClicked) {
		gblScheduleButtonClicked = false;
		frmIBBillPaymentView.btnBPSchedule.setEnabled(true);
	}
	if (frmIBBillPaymentView.arrowBPScheduleField.isVisible) {
		frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
	}
	if (gblAmountSelected == true || gblScheduleFreqChanged == true) {
		    //srvTokenSwitchingDummyEditBP(); //callCrmProfileInqServiceForEditBPAmt();
			return callBillerValidationEdit();
	} else {
		//alert("Please Edit Amount or Schedule Frequency to proceed Further")
			alert(kony.i18n.getLocalizedString("Error_NoEditingDoneBP"));
		return false
	}
	return true;
}




function callBillerValidationEdit(){
    billAmountBillPay = "";
    //lblBillerName - lblBPBillerName

    		//Payment Details
		if (gblEditBillMethod == 0) {
			var changeAmt = frmIBBillPaymentView.txtBPEditAmtValue.text;
			changeAmt = replaceCommon(changeAmt, ",", "");
		
			//frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = frmIBBillPaymentView.txtBPEditAmtValue.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		} else if (gblEditBillMethod == 2) {
			var changeAmt = "";
			if(gblownCardBP){
				 changeAmt = frmIBBillPaymentView.textboxOnlineAmtvalue.text ;
			}else
				 changeAmt = frmIBBillPaymentView.txtBPEditAmtValue.text;
				
			changeAmt = replaceCommon(changeAmt, ",", "");
		
			//frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = frmIBBillPaymentView.textboxOnlineAmtvalue.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		} else {
			var changeAmt = frmIBBillPaymentView.textboxOnlineTotalAmt.text ;
			changeAmt = changeAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"");
			if(changeAmt.indexOf(",") != -1)changeAmt = replaceCommon(changeAmt, ",", "");
			changeAmt = commaFormatted(changeAmt);
			
		}
		changeAmt = changeAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"");
   	    billAmountBillPay=changeAmt;
     
        var compCodeBillPay = gblBillerCompcode;
        var ref1ValBillPay = frmIBBillPaymentView.lblBPRef1Val.text;
        var ref2ValBillPay=frmIBBillPaymentView.labelBPRef2Val.text;
        var scheduleDtBillPay = frmIBBillPaymentView.lblBPStartOnDateVal.text;
        var billerType = "Biller";
        var billerMethod = gblEditBillMethod;
        callBillerValidationEditService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay, scheduleDtBillPay, billerType,billerMethod);
        return true;
	
}

function callBillerValidationEditService(compCodeBillPay, ref1ValBillPay, ref2ValBillPay, billAmountBillPay, scheduleDtBillPay, billerType,billerMethod){
    inputParam = {};

    inputParam["BillerCompcode"] = compCodeBillPay;
    inputParam["ReferenceNumber1"] = ref1ValBillPay;
    inputParam["ReferenceNumber2"] = ref2ValBillPay;
    inputParam["Amount"] = billAmountBillPay;
    amountpaid = billAmountBillPay;
    inputParam["ScheduleDate"] = scheduleDtBillPay;
    inputParam["billerCategoryID"] = gblBillerCategoryID;
    inputParam["billerMethod"] = billerMethod;
    inputParam["ModuleName"] = "BillPay";
    billerTypeCheck = billerType;
    showLoadingScreenPopup();
    invokeServiceSecureAsync("billerValidation", inputParam, callBillerValidationBillPayEditServiceCallBack);
}

function callBillerValidationBillPayEditServiceCallBack(status, result) {

    
    if (status == 400) //success responce
    {
        
        
        
        var validationBillPayFlag = "";
        var isValidOnlineBiller = result["isValidOnlineBiller"];
        if (result["opstatus"] == 0) {
            
            validationBillPayFlag = result["validationResult"];
            
        } else {
            dismissLoadingScreenPopup();
            
            validationBillPayFlag = result["validationResult"];
            
        }
        
        if (billerTypeCheck == "Biller") {
            if (validationBillPayFlag == "true") {
                srvTokenSwitchingDummyEditBP();
               
            } else {
                dismissLoadingScreenPopup();
                if(isValidOnlineBiller!=undefined && isValidOnlineBiller == "true"){
                	alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
                }else{
                	alert(kony.i18n.getLocalizedString("keyBillerValidationFailed"));
                }
                return false;
            }
            
        }

        if (billerTypeCheck == "TopUp") {
        	if (gblBillerCompcodeTP == "2704"){
        		if (billerValidationIBTopUpAmtMinMax(gblUserEnteredAmtTP)){
        			dismissLoadingScreenPopup();
                	return false;
            	}
        	} 
        	
            if (validationBillPayFlag == "true") {
                 srvTokenSwitchingDummyEditTP();
                
            } else {
            	dismissLoadingScreenPopup();
            	if(isValidOnlineBiller!=undefined && isValidOnlineBiller == "true"){
                	alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
                }else{
                	alert(kony.i18n.getLocalizedString("keyTopUpValidationFailed"));
                }	
                return false;
            }
        }
    }
}
/*
   **************************************************************************************
		Module	: callCrmProfileInqServiceforEditBPAmt
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callCrmProfileInqServiceForEditBPAmt() {
	var inputparam = [];
	inputparam["crmId"] = ""; // check this value as that service is not having preprocess
	inputparam["rqUUId"] = "";
	showLoadingScreenPopup();
		var edAmt = gblUserEnteredAmt;  //removeCommaIB(gblUserEnteredAmt);
		edAmt = edAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		
		if (edAmt.indexOf(",") != -1) {
       	    edAmt = parseFloat(replaceCommon(edAmt, ",", "")).toFixed(2);
   		}else
    	    edAmt = parseFloat(edAmt).toFixed(2);
		
		/*
		edAmt = parseFloat(edAmt.toString());
		edAmt = "" + edAmt;
		if (edAmt.indexOf(".") != -1) {
			var tmp = edAmt.split(".", 2);
			if (tmp[1].length == 1) {
				edAmt = edAmt + "0";
			} else if (tmp[1].length > 2) {
				tmp[1] = tmp[1].substring("0", "2")
				edAmt = ""
				edAmt = tmp[0] + "." + tmp[1];
			}
		}
		*/
		
		inputparam["amtEdited"] = edAmt;
		var billerName = frmIBBillPaymentView.lblBPBillNameAndCompCode.text;
		billerName = billerName.split("(")[0];
		billerName = billerName.toString().trim();
		inputparam["billerName"] = billerName; //frmIBBillPaymentView.lblBPBillerNickName.text;  
		
		inputparam["billerRef1"] = frmIBBillPaymentView.lblBPRef1Val.text;
		
		var ref2V = frmIBBillPaymentView.labelBPRef2Val.text;
		if(ref2V != ""){
			inputparam["billerRef2"] = frmIBBillPaymentView.labelBPRef2Val.text;
		}
		
	if ((gblAmountSelected == true && gblScheduleFreqChanged == true) || (gblAmountSelected == false && gblScheduleFreqChanged == true)) {
			inputparam["editEditBillPayFlag"] = "freqChangeEdit";
			var scheIDDele = frmIBBillPaymentView.lblBPScheduleRefNoVal.text;
			scheIDDele = "SB" + scheIDDele.substring(2, scheIDDele.length);
			
			inputparam["scheID"] = scheIDDele;
	}else if (gblAmountSelected == true && gblScheduleFreqChanged == false) {
		inputparam["editEditBillPayFlag"] = "amtChangeEdit";
		inputparam["startOnDate"] = changeDateFormatForService(frmIBBillPaymentView.lblBPStartOnDateVal.text);
     	inputparam["extTrnRefID"] = "SB" + frmIBBillPaymentView.lblBPScheduleRefNoVal.text.substring(2, frmIBBillPaymentView.lblBPScheduleRefNoVal.text.length);
	}
	invokeServiceSecureAsync("crmProfileInq", inputparam, forEditBPCrmProfileInqServiceCallBack);
}
/*
   **************************************************************************************
		Module	: forEditBPCrmProfileInqServiceCallBack
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function forEditBPCrmProfileInqServiceCallBack(status, result) {
	//tokenFlag = false;
	if (status == 400) {
		var feeAmt = frmIBBillPaymentView.lblBPPaymentFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0",lenFee-1);
        feeAmt = parseFloat(feeAmt.toString());
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBBillPaymentView.lblBPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = "";
		var activityFlexValues4 = "";
		var activityFlexValues5 = "";
		if(gblScheduleFreqChanged == true){
			activityFlexValues3 = gblStartOn +"+"+ frmIBBillPaymentView.lblBPStartOnDateVal.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIB +"+"+ frmIBBillPaymentView.lblBPRepeatAsVal.text; //Old Frequency + New Frequency (Edit case)
		}else {
			activityFlexValues3 = gblStartOn; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIB; //Old Frequency + New Frequency (Edit case)
		}
		if(gblAmountSelected == true){
			activityFlexValues5 = gblAmtFromService +"+"+ (gblUserEnteredAmt + feeAmt); //Old Amount + New Amount (Edit case)
		}else{
			activityFlexValues5 = gblAmtFromService; //Old Amount + New Amount (Edit case)
		}
		var logLinkageId = "";
		if (result["opstatus"] == 0) { 
			activityStatus = "00";
			var StatusCode = result["statusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				var dailyChannelLimit = result["ebMaxLimitAmtCurrent"];
				
				var totalamt = parseFloat(removeCommaIB(gblUserEnteredAmt)) + feeAmt;
				//       var totalamt = (gblUserEnteredAmt*1) + (fee.substring(0, feegth -1).trim()*1); // check this its not working with comma
				if (dailyChannelLimit < totalamt) {
					activityStatus = "02";
					dismissLoadingScreenPopup();
					//alert("Entered Amount is exceeding the Daily Channel Limit");
					alert(kony.i18n.getLocalizedString("Error_Amount_Daily_Limit"));
					return false;
				} else {
						frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIBREQotpFocus;
						frmIBEditFutureBillPaymentPrecnf.btnOTPReq.setEnabled(true);
						frmIBEditFutureBillPaymentPrecnf.btnOTPReq.onClick = onclickRequestBtnIBForOTP;
						try {
							kony.timer.cancel("OTPTimerReq")
						} catch (e) {
							
						}
						
						
					/*	
						
					if (result["tokenDeviceFlag"] == "N") {
						tokenFlag = false;
						//gblOTPFlag = true;
						//	
						frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(true);
						frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(true);
						frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
						frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
						frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
						frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
						//callGenRefNoServiceForEditBP(); 
					} else {
						tokenFlag = true;
						//gblOTPFlag = false;
						//
						frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(false);
						frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(false);
						frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("Receipent_tokenId");
						frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("Receipent_Token");
						frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("Receipent_SwitchtoOTP");
						 frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
					}
					
					*/
					frmIBEditFutureBillPaymentPrecnf.show()
				}
			} else {
				activityStatus = "02";
				dismissLoadingScreenPopup();
				
			}
		} else {
			activityStatus = "02";
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);	
	}
}
/*
   **************************************************************************************
		Module	: populateEditBillPaymentConfirmScreen
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function populateEditBillPaymentConfirmScreen() {
	var status = editBillPaymentOnClickNext();
	if (status == true) {
		//From Account 
		frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNickName.text = frmIBBillPaymentView.lblBPFromAccNickName.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text = frmIBBillPaymentView.lblBPFromAccName.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text = frmIBBillPaymentView.lblBPFromAccNumber.text;
		//To Biller
		if (frmIBBillPaymentView.lblBPBillerNickName.isVisible) {
			frmIBEditFutureBillPaymentPrecnf.lblBPBillerNickName.text = frmIBBillPaymentView.lblBPBillerNickName.text;
		} else {
			frmIBEditFutureBillPaymentPrecnf.lblBPBillerNickName.setVisibility(false);
		}
		frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text = frmIBBillPaymentView.lblBPBillNameAndCompCode.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPRef1.text = frmIBBillPaymentView.lblBPRef1.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text = frmIBBillPaymentView.lblBPRef1Val.text;
		//below line is added for CR - PCI-DSS masked Credit card no
		frmIBEditFutureBillPaymentPrecnf.lblBPRef1ValMasked.text = frmIBBillPaymentView.lblBPRef1ValMasked.text;
		
		if (frmIBBillPaymentView.hbox58788011366391.isVisible) {
			frmIBEditFutureBillPaymentPrecnf.labelBPRef2.text = frmIBBillPaymentView.labelBPRef2.text;
			frmIBEditFutureBillPaymentPrecnf.labelBPRef2Val.text = frmIBBillPaymentView.labelBPRef2Val.text;
		} else {
			frmIBEditFutureBillPaymentPrecnf.hbox58788011366391.setVisibility(false);
		}
		//Payment Details
		if (gblEditBillMethod == 0) {
			var changeAmt = frmIBBillPaymentView.txtBPEditAmtValue.text;
			changeAmt = replaceCommon(changeAmt, ",", "");
			var currencyPosition = 	changeAmt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"));
			if(currencyPosition == -1){
			 	frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = commaFormatted(changeAmt + " " + kony.i18n.getLocalizedString("currencyThaiBaht"));
			}else{
			 	frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = commaFormatted(changeAmt);
			}
			//frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = frmIBBillPaymentView.txtBPEditAmtValue.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		} else if (gblEditBillMethod == 2) {
			var changeAmt = "";
			if(gblownCardBP){
				 changeAmt = frmIBBillPaymentView.textboxOnlineAmtvalue.text ;
			}else
				 changeAmt = frmIBBillPaymentView.txtBPEditAmtValue.text;
				
			changeAmt = replaceCommon(changeAmt, ",", "");
			var currencyPosition = 	changeAmt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"));
			if(currencyPosition == -1){
			 	frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = commaFormatted(changeAmt + " " + kony.i18n.getLocalizedString("currencyThaiBaht"));
			}else{
			 	frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = commaFormatted(changeAmt);
			}
			//frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = frmIBBillPaymentView.textboxOnlineAmtvalue.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		} else {
			var changeAmt = frmIBBillPaymentView.textboxOnlineTotalAmt.text ;
			changeAmt = changeAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"");
			if(changeAmt.indexOf(",") != -1)changeAmt = replaceCommon(changeAmt, ",", "");
			changeAmt = commaFormatted(changeAmt);
			frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = changeAmt+" "+kony.i18n.getLocalizedString("currencyThaiBaht");
			
			/*
			var currencyPosition = 	changeAmt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht"));
			if(currencyPosition == -1){
			 	frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = commaFormatted(changeAmt+ " " + kony.i18n.getLocalizedString("currencyThaiBaht"));
			}else{
			 	frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = commaFormatted(changeAmt);
			}
			*/
			//frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text = frmIBBillPaymentView.textboxOnlineTotalAmt.text + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
		}
		frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text = frmIBBillPaymentView.lblBPPaymentFeeVal.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text = frmIBBillPaymentView.lblBPPaymentOrderValue.text;
		//schedule details 
		frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text = frmIBBillPaymentView.lblBPStartOnDateVal.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text = frmIBBillPaymentView.lblBPRepeatAsVal.text;
		frmIBEditFutureBillPaymentPrecnf.lblEndOnDateVal.text = frmIBBillPaymentView.lblEndOnDateVal.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text = frmIBBillPaymentView.lblBPExcuteVal.text;
		frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.setVisibility(true);
		
		if(kony.string.equals("-", "" + frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text)){
			frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.setVisibility(false);
			frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.setVisibility(true);
			frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text=frmIBBillPaymentView.lblBPExcuteVal.text;
		}else{
			frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.text = frmIBBillPaymentView.lblBPExcuteValTimes.text;
			frmIBEditFutureBillPaymentPrecnf.lblBPExcuteValTimes.setVisibility(true);
		}
				
		//MyNote
		frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text = replaceHtmlTagChars(frmIBBillPaymentView.lblMyNoteVal.text);
		//Schedule Ref. No.
		frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text = frmIBBillPaymentView.lblBPScheduleRefNoVal.text;
		if (frmIBBillPaymentView.lblScheduleBillPayment.isVisible) {
			frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
		}
		if (frmIBBillPaymentView.hbxEditBPFutureSchedule.isVisible) {
			frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
		}
		//frmIBEditFutureBillPaymentPrecnf.show();
	} else if (status == false) {
		frmIBBillPaymentView.show();
	}
}
/*
   **************************************************************************************
		Module	: callGenRefNoServiceForEditBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callGenRefNoServiceForEditBP() {
	var locale = kony.i18n.getCurrentLocale();
	var eventNotificationPolicy;
	var SMSSubject;
	var Channel;
	var Amount = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text;
	
	frmIBEditFutureBillPaymentPrecnf.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    frmIBEditFutureBillPaymentPrecnf.lblPlsReEnter.text = " "; 
    frmIBEditFutureBillPaymentPrecnf.hbxOTPincurrect.isVisible = false;
    frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.isVisible = true;
    frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.isVisible = true;
	
	if(Amount.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
		Amount = Amount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").trim();
	}
	
	/*
	if (locale == "en_US") {
		eventNotificationPolicy = "MIB_BillPayment_EN";
		SMSSubject = "MIB_BillPayment_EN";
	} else {
		eventNotificationPolicy = "MIB_BillPayment_TH";
		SMSSubject = "MIB_BillPayment_TH";
	}*/
	Channel = "BillPayment";
	var inputParam = [];
	inputParam["retryCounterRequestOTP"] = gblRetryCountRequestOTP; //check this count value
	inputParam["Channel"] = Channel;
	inputParam["locale"] = locale;
	inputParam["billerCategoryID"] = gblBillerCategoryID;
	showLoadingScreenPopup();
	invokeServiceSecureAsync("generateOTPWithUser", inputParam, callbackGenRefNoServiceForEditBP);
}
/*
   **************************************************************************************
		Module	: callbackGenRefNoServiceForEditBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
//gblOTPLENGTHEdit

function callbackGenRefNoServiceForEditBP(status, result) {
	if (status == 400) {
	     if (result["errCode"] == "GenOTPRtyErr00001") {
			dismissLoadingScreenPopup();
			showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
			return false;
		  }
		if (result["errCode"] == "GenOTPRtyErr00002") {
			dismissLoadingScreenPopup();
			showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
			return false;
		  }
	
		
	
	
		if (result["opstatus"] == 0) {
			if (result["errCode"] == "GenOTPRtyErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (result["errCode"] == "JavaErr00001") {
				dismissLoadingScreenPopup();
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			
			frmIBEditFutureBillPaymentPrecnf.btnNext.setVisibility(false);
			frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(true);
			frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
			frmIBEditFutureBillPaymentPrecnf.hbxbtnUpdatedReturn.setVisibility(false);
			frmIBEditFutureBillPaymentPrecnf.hbxEditBtns.setVisibility(true);
			
			var reqOtpTimer = kony.os.toNumber(result["requestOTPEnableTime"]);
			gblRetryCountRequestOTP = kony.os.toNumber(result["retryCounterRequestOTP"]);
			gblOTPLENGTHEdit = kony.os.toNumber(result["otpLength"]);
			
			kony.timer.schedule("OTPTimerReq", OTPTimerCallBackEditBP, reqOtpTimer, false);
			frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(true);
			frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(true);
			frmIBEditFutureBillPaymentPrecnf.labelBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
			var refVal="";
			for(var d=0;result["Collection1"].length;d++){
				if(result["Collection1"][d]["keyName"] == "pac"){
						refVal=result["Collection1"][d]["ValueString"];
						break;
				}
			}
			frmIBEditFutureBillPaymentPrecnf.labelBankRefVal.text = refVal;
			frmIBEditFutureBillPaymentPrecnf.labelOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsg");
			frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
			frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
			frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
			frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
			
			frmIBEditFutureBillPaymentPrecnf.labelMobileNum.text = "xx-xxx-" + gblPHONENUMBER.substring(6, 10);
			frmIBEditFutureBillPaymentPrecnf.txtBxOTP.maxTextLength = gblOTPLENGTHEdit;
			//frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIB125Disabled;
			frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIBREQotp; 
			frmIBEditFutureBillPaymentPrecnf.btnOTPReq.setEnabled(false);
			dismissLoadingScreenPopup();
			frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
			
		} else {
			dismissLoadingScreenPopup();
			alert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + result["errMsg"]);
		}
	}
}
/*
   **************************************************************************************
		Module	: onclickRequestBtnIBForOTP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function onclickRequestBtnIBForOTP() {
	if (tokenFlag == true) {
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBEditFutureBillPaymentPrecnf.txtBxOTP.text = "";
		frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
		tokenFlag = false;
	} else {
		callGenRefNoServiceForEditBP();
	}
}
/*
   **************************************************************************************
		Module	: OTPTimerCallBackEditBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function OTPTimerCallBackEditBP() {
	//frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIB125active;
	frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIBREQotpFocus;
	frmIBEditFutureBillPaymentPrecnf.btnOTPReq.setEnabled(true);
	frmIBEditFutureBillPaymentPrecnf.btnOTPReq.onClick = callGenRefNoServiceForEditBP;
	try {
		kony.timer.cancel("OTPTimerReq")
	} catch (e) {
		
	}
}

gblVerifyOTPEditBP = 0;

/*
   **************************************************************************************
		Module	: callBackVerifyOTPForEditBPIB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/
/*
function callBackVerifyOTPForEditBPIB(status, result) {
    if (status == 400) {
        if (result["opstatus"] == 0) {
            var StatusCode = result["StatusCode"];
            var Severity = result["Severity"];
            var StatusDesc = result["StatusDesc"];
            if (StatusCode == 0) {
                if (gblVerifyOTPEditBP <= 3) // read this value from property 
                {
                    editBillPaymentFlow();
                } else {
                    popIBBPOTPLocked.show();
                    callCRMProfileModEditBP();
                }
            } else {
                
            }
        } else if (result["opstatus"] == 8005) {
            if (gblVerifyOTP >= 4) {
                popIBBPOTPLocked.show();
            } else {
                alert("Please enter valid OTP");
            }
        } else {
            
        }
    }
}*/

function callBackVerifyOTPForEditBPIB(status, result) {
	dismissLoadingScreenPopup();
	if (status == 400) {
		if (result["opstatusVPX"] == 0) {
			//if (result["tokenStatus"] == "Activated" && result["showPinPwdCount"] != 0) {
				
				gblVerifyOTPEditBP = "0";
				frmIBEditFutureBillPaymentPrecnf.txtBxOTP.text = "";
				frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIBREQotpFocus;
				frmIBEditFutureBillPaymentPrecnf.btnOTPReq.setEnabled(true);
				try {
					kony.timer.cancel("OTPTimerReq")
				} catch (e) {
					
				}
				editBillPaymentFlow(result);
			//}
		} else {
			frmIBEditFutureBillPaymentPrecnf.txtBxOTP.text = "";
			if (result["opstatusVPX"] == 8005) {
				if (result["errCode"] == "VrfyOTPErr00001") {
					gblVerifyOTPCounter = result["retryCounterVerifyOTP"];
					dismissLoadingScreenPopup();
					//showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);//commented by swapna
                    frmIBEditFutureBillPaymentPrecnf.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); 
                    frmIBEditFutureBillPaymentPrecnf.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                    frmIBEditFutureBillPaymentPrecnf.hbxOTPincurrect.isVisible = true;
                    frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.isVisible = false;
                    frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.isVisible = false;
                    frmIBEditFutureBillPaymentPrecnf.txtBxOTP.text = "";
                    frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
					return false;
				} else if (result["errCode"] == "VrfyOTPErr00004") {
					dismissLoadingScreenPopup();
					showAlert("" + kony.i18n.getLocalizedString("invalidOTP"), null);
					return false;
				} else if (result["errCode"] == "VrfyOTPErr00002") {
					dismissLoadingScreenPopup();
					//showAlert("" + kony.i18n.getLocalizedString("ECVrfyOTPErr"), null);
					//callCRMProfileModEditBP("04");
					handleOTPLockedIB(result);
					return false;
				}else if (result["errCode"] == "EditPaymentErr001") {
					dismissLoadingScreenPopup();
					showAlert("Transaction Can Not Be Preocessed", null);
					return false;
				} 
			} else {
		        frmIBEditFutureBillPaymentPrecnf.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
                frmIBEditFutureBillPaymentPrecnf.lblPlsReEnter.text = " "; 
                frmIBEditFutureBillPaymentPrecnf.hbxOTPincurrect.isVisible = false;
                frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.isVisible = true;
                frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.isVisible = true;
				dismissLoadingScreenPopup();
				//showAlert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + result["errMsg"], null);
				showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			}
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			showCommonAlertMYA("" + kony.i18n.getLocalizedString("Receipent_alert_Error"), null);
		}
	}
}

/*function callBackVerifyTokenForEditBPIB(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			alert("To be implemented");
		}
	}
}*/


/*function callBackCRMProfileModEditBP(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			
			popIBBPOTPLocked.show();
			gblRcOpLockStatus = true;
			gblUserLockStatusIB=resulttable["IBUserStatusID"];		
		}else {
			dismissLoadingScreenPopup();
			showAlert(" " + kony.i18n.getLocalizedString("Receipent_alert_Error") + ":" + result["errMsg"], null);
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
}*/
/*
   **************************************************************************************
		Module	: editBillPaymentFlow
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function editBillPaymentFlow(result) {
	if (gblAmountSelected == true && gblScheduleFreqChanged == true) {
		//callPaymentDeleteService();
		callBackPmtCanServiceForEditBPIB(result);
	} else if (gblAmountSelected == false && gblScheduleFreqChanged == true) {
		//callPaymentDeleteService();
		callBackPmtCanServiceForEditBPIB(result);
	} else if (gblAmountSelected == true && gblScheduleFreqChanged == false) {
		gblAmountSelected = false;
		gblScheduleFreqChanged = false;
		callBackPmtModServiceForEditBPIB(result);
	}
}

/*
   **************************************************************************************
		Module	: callBackPmtModServiceForEditBPIB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackPmtModServiceForEditBPIB(result) {
		if (result["opstatusBP"] == 0) {
			var StatusCode = result["StatusCode"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				var initiatedDt = result["InitiatedDt"];
				frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text = dateFormatForDisplayWithTimeStamp(initiatedDt);
				populateEditBillPaymentCompleteScreen();
				//callNotificationAddService();
			} else {
				dismissLoadingScreenPopup();
        	 	alert(" " +StatusDesc);
        	 	return false;
			}
		} else {
			 	dismissLoadingScreenPopup();
				alert(" " +result["opstatusBP"]);
				return false;
		}
}

/*
   **************************************************************************************
		Module	: callBackNotificationAddService
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackNotificationAddService(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				//populateEditBillPaymentCompleteScreen();
				
			} else {
				
				return false;
			}
		} else {
			
		}
	}
}
/*
   **************************************************************************************
		Module	: populateEditBillPaymentCompleteScreen
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function populateEditBillPaymentCompleteScreen() {
	//frmIBEditFutureBillPaymentPrecnf.lblBPBalAfterPayVal.text = commaFormatted(gblAvailBal) + " " + kony.i18n.getLocalizedString(
	//	"currencyThaiBaht");
	frmIBEditFutureBillPaymentPrecnf.hbxBPCmpletngHdr.setVisibility(true);
	frmIBEditFutureBillPaymentPrecnf.imgComplete.setVisibility(true);
	frmIBEditFutureBillPaymentPrecnf.hbxbtnUpdatedReturn.setVisibility(true);
	//frmIBEditFutureBillPaymentPrecnf.hbxBPCmpleAvilBal.setVisibility(true);
	frmIBEditFutureBillPaymentPrecnf.hbxBPSave.setVisibility(true);
	frmIBEditFutureBillPaymentPrecnf.hbxBPViewHdr.setVisibility(false);
	frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(false);
	frmIBEditFutureBillPaymentPrecnf.hbxEditBtns.setVisibility(false);
	
	//frmIBEditFutureBillPaymentPrecnf.hbxAd.setVisibility(true);
	frmIBEditFutureBillPaymentPrecnf.hbxAdv.setVisibility(true);
	campaginService("imgAd1","frmIBEditFutureBillPaymentPrecnf","I");
	
	frmIBEditFutureBillPaymentPrecnf.btnNext.setVisibility(false);
}

/*
   **************************************************************************************
		Module	: callBackPmtCanServiceForEditBPIB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackPmtCanServiceForEditBPIB(result) {
		if (result["opstatusCancel"] == 0) {
			var StatusCode = result["statusCodeCancel"];
			var StatusDesc = result["statusDescCancel"];
			if (StatusCode == 0) {
				//var refNum = result["transRefNum"];
				//frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text = refNum;
				callBackPmtAddServiceForEditBPIB(result);
			} else {
				//activityStatus = "02";
				gblAmountSelected = false;
				gblScheduleFreqChanged = false;
				dismissLoadingScreenPopup();
				alert(" " + StatusDesc);
				return false;
			}
		} else {
			gblAmountSelected = false;
			gblScheduleFreqChanged = false;
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
		}
	
}


/*
   **************************************************************************************
		Module	: callPmtAddServiceForEditBPIB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

/*function callPmtAddServiceForEditBPIB() {
	var inputparam = [];
	inputparam["rqUID"] = "";
	inputparam["crmId"] = gblcrmId;
	inputparam["curCode"] = "THB";
	var edAmt = removeCommaIB(frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text);
	edAmt = edAmt.substring("0", edAmt.length - 1);
	edAmt = parseFloat(edAmt.toString());
	edAmt = "" + edAmt;
	if (edAmt.indexOf(".") != -1) {
		var tmp = edAmt.split(".", 2);
		if (tmp[1].length == 1) {
			edAmt = edAmt + "0";
		} else if (tmp[1].length > 2) {
			tmp[1] = tmp[1].substring("0", "2")
			edAmt = ""
			edAmt = tmp[0] + "." + tmp[1];
		}
	}
	
	var ref2 = "";
    if(frmIBBillPaymentView.hbox58788011366391.isVisible) {
	    ref2 = frmIBEditFutureBillPaymentPrecnf.labelBPRef2Val.text;
	    inputparam["pmtRefNo2"] = ref2;
     }
	if(gblRef2EditBP != ""){
		inputparam["pmtRefNo2"] = ref2;
	}
	
	inputparam["amt"] = edAmt;
	inputparam["fromAcct"] = gblFromAccNo; // from Account ID (from PaymentInq)
	inputparam["fromAcctType"] = gblFromAccType; // from Account Type (from PaymentInq)
	inputparam["toAcct"] = gblToAccNo; // TO Biller No (from PaymentInq)
	inputparam["toAccTType"] = gblToAccType; // To Biller Type ((from PaymentInq)
	inputparam["dueDate"] = changeDateFormatForService(frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text); // Start On Date
	inputparam["pmtRefNo1"] = removeHyphenIB(frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text); // Ref 1 value
	inputparam["custPayeeId"] = gblCustPayID; // check this with TL
	inputparam["transCode"] = gblTransCode; //TransCode (from PaymentInq)
	inputparam["memo"] = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text; // My Note 
    var refBP = frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text;
	refBP = removeHyphenIB(refBP);
	//var pmtMethod = "BILL_PAYMENT";
   if(gblEditBillMethod == "1"){
     inputparam["PmtMiscType"] = "MOBILE";
     inputparam["MiscText"] = refBP;
   }
	inputparam["pmtMethod"] = gblEditPaymentMethod ; // get this value from paymentInq

	inputparam["extTrnRefId"] = "SB" + frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.substring(2, frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.length); // Updated Schedule Ref No	
	if (endFreqSave == "After") {
		inputparam["NumInsts"] = frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text; // Execution times 
	}
	if (endFreqSave == "OnDate") {
		inputparam["FinalDueDt"] = changeDateFormatForService(frmIBEditFutureBillPaymentPrecnf.lblEndOnDateVal.text); // End On Date
	}
	var frequency = frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text;
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
		inputparam["dayOfWeek"] = "";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
		inputparam["dayOfMonth"] = "";
	}
	if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
		frequency = "Annually";
		inputparam["dayofMonth"] = "";
		inputparam["monthofYear"] = "";
	}
	if(kony.string.equalsIgnoreCase(frequency, "Once")){
		inputparam["freq"] = "once";
	}else{
		inputparam["freq"] = frequency;
	}
	inputparam["desc"] = "";
	inputparam["channelId"]="IB";
	invokeServiceSecureAsync("doPmtAdd", inputparam, callBackPmtAddServiceForEditBPIB);
}*/
/*
   **************************************************************************************
		Module	: callBackPmtAddServiceForEditBPIB
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackPmtAddServiceForEditBPIB(result) {
        gblAmountSelected = false;
		gblScheduleFreqChanged = false;
		if (result["opstatusPayAdd"] == 0) {
			var StatusCode = result["statusCodeAdd"];
			var StatusDesc = result["statusDescAdd"];
			if (StatusCode == 0) {
				var refNum = result["transRefNum"];
				frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text = refNum;
				frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text = result["currentTime"];
				populateEditBillPaymentCompleteScreen();
				//callNotificationAddService();
			} else {
				dismissLoadingScreenPopup();
				alert(" " + StatusDesc);
				return false;
			}
		} else {
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
		}
	
}
/*
   **************************************************************************************
		Module	: resetEditFutureBP
		Author  : Kony
		Date    : 
		Purpose : To reset the editted future bill payment
	***************************************************************************************
*/

function resetEditFutureBP() {
	frmIBBillPaymentView.imgTMB.setVisibility(false);
	if (gblScheduleButtonClicked) {
		gblScheduleButtonClicked = false;
		frmIBBillPaymentView.btnBPSchedule.setEnabled(true);
	}
	if (frmIBBillPaymentView.lblScheduleBillPayment.isVisible) {
		frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxEditBPFutureSchedule.isVisible) {
		frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hboxExecute.isVisible) {
		frmIBBillPaymentView.hboxExecute.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hboxEndOn.isVisible) {
		frmIBBillPaymentView.hboxEndOn.setVisibility(true);
	}
	if (frmIBBillPaymentView.arrowBPScheduleField.isVisible) {
		frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
	}
	
	if (repeatAsIB == "" || endFreqSave == "") {
		//frmIBBillPaymentView.lblBPStartOnDateVal.text = gblStartOn;
		if (gblEndingFreqOnLoadIB == "Never") {
			frmIBBillPaymentView.lblBPStartOnDateVal.text = gblStartOn;
			frmIBBillPaymentView.lblEndOnDateVal.text = "-";
			frmIBBillPaymentView.lblBPExcuteVal.text = "-";
			frmIBBillPaymentView.lblBPRepeatAsVal.text = gblOnLoadRepeatAsIB;
			onClickRepeatAs = "";
			onClickEndFreq = "";
		}else if(gblOnLoadRepeatAsIB == "Once"){
			onClickRepeatAs = repeatAsIB;
			onClickEndFreq = endFreqSave;
		}else {
			frmIBBillPaymentView.lblBPStartOnDateVal.text = gblStartOn;
			frmIBBillPaymentView.lblEndOnDateVal.text = gblEndDateOnLoad;
			frmIBBillPaymentView.lblBPExcuteVal.text = gblExecutionOnLoad;
			frmIBBillPaymentView.lblBPRepeatAsVal.text = gblOnLoadRepeatAsIB;
			onClickRepeatAs = "";
			onClickEndFreq = "";
		}
		/*
		frmIBBillPaymentView.lblBPRepeatAsVal.text = gblOnLoadRepeatAsIB;
		//frmIBBillPaymentView.calendarEndOn.date = gblEndDateOnLoad;
		//frmIBBillPaymentView.txtBPExecutiontimesval.text = gblExecutionOnLoad;
		onClickRepeatAs = "";
		onClickEndFreq = "";
		*/
	} else {
		onClickRepeatAs = repeatAsIB;
		onClickEndFreq = endFreqSave;
		//frmIBBillPaymentView.lblBPStartOnDateVal.text= dateFormatForDisplay(gblStartOn);
		//frmIBBillPaymentView.calendarEndOn.dateComponents = currentDateForcalender();
		//frmIBBillPaymentView.txtBPExecutiontimesval.text = "5 times";
		//frmIBBillPaymentView.lblBPRepeatAsVal.text=gblrepeatAs1;
	}
	
	
	
}

function currentDateForcalender() {
	var day = new Date(GLOBAL_TODAY_DATE);
	var dd = day.getDate() * 1 + 1;
	var mm = day.getMonth() + 1;
	var yyyy = day.getFullYear();
	var hr = day.getHours();
	var min = day.getMinutes();
	var sec = day.getSeconds();
	if (dd < 10) {
		dd = '0' + dd
	}
	if (mm < 10) {
		mm = '0' + mm
	}
	//var deviceInfo = kony.os.deviceInfo();
    if (gblDeviceInfo["name"] == "iPhone" || gblDeviceInfo["name"] == "iPad" || gblDeviceInfo["name"] == "iPhone Simulator") {
     	var returnedValue = iPhoneCalendar.getDeviceDateLocale();
     	var deviceDate = returnedValue.split("|")[0];
     	yyyy = deviceDate.substr(0,4)
     }
     
	return [dd, mm, yyyy, hr, min, sec];
}


function currentDate() {
	var day = new Date(GLOBAL_TODAY_DATE);
	var dd = day.getDate();
	var mm = day.getMonth() + 1;
	var yyyy = day.getFullYear();
	return dd + "/" + mm + "/" + yyyy;
}
/*
   **************************************************************************************
		Module	: generate PDF for Bill payment
		Author  : Kony
		Date    : 
		Purpose : To display billpayment in PDF format
	***************************************************************************************
*/
/*
function generatePDFForEditFutureBP() {
	//alert("To be implemented");
	var service = "";
	var fileName = "";
	var inputParam = [];
	var paymentOrderDate = ""
	fileName = "FutureBillPaymentTemplate";
	service = "BillTopUpPaymentPdfImage";
	inputParam["paymentOrderDate"] = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text;
	inputParam["PaymentSchedule"] = frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text + " " +
		frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text;
	inputParam["filename"] = fileName;
	inputParam["filetype"] = "pdf";
	inputParam["AccountNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text;
	inputParam["AccountName"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text;
	inputParam["BillerName"] = frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text;
	inputParam["MobileNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text;
	inputParam["amount"] = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text;
	inputParam["Fee"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text;
	inputParam["TopUpDate"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text;
	inputParam["MyNote"] = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text;
	inputParam["TransactionRefNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text;
	inputParam["MeterNO"] = "";
	
	//"&localID=en_US"
	var url = "";
	url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" +
		service + "?filename=" + inputParam["filename"] + "&filetype=" + inputParam["filetype"];
	var url1 = "&AccountNo=" + inputParam["AccountNo"] + "&AccountName=" + inputParam["AccountName"] + "&BillerName=" +
		inputParam["BillerName"] + "&MobileNo=" + inputParam["MobileNo"] + "&amount=" + inputParam["amount"] + "&Fee=" +
		inputParam["Fee"] + "&TopUpDate=" + inputParam["TopUpDate"] + "&MyNote=" + inputParam["MyNote"] +
		"&TransactionRefNo=" + inputParam["TransactionRefNo"];
	url = url + url1;
	var url2 = "&paymentOrderDate=" + inputParam["paymentOrderDate"] + "&paymentOrderDate=" + inputParam[
		"paymentOrderDate"]
	url = url + url2;
	
	frmIBTransferGeneratePDF.browPdfTransfer.requestURLConfig = {
		URL: url,
		requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
	}
	frmIBTransferGeneratePDF.show();
	return false;
}
*/
function onClickGeneratePDFImageForEditFutureBP(filetype){
	var inputParam = {};
	var scheduleDetails = "";
	var pdfImagedata = {};
	var ref2Flag = false;
	//fileType = filetype;
	if(frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text == "Once"){
	 	scheduleDetails = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text + " "+ " Repeat " +frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text;
	 }else if(frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text == "-"){
	 	scheduleDetails = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text + " "+ " Repeat " +frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text;
	 } else{
	 	scheduleDetails = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text +" to " +frmIBEditFutureBillPaymentPrecnf.lblEndOnDateVal.text + " "+ " Repeat " +frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text +" for "+frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text+" times " ;
	 }
	
	if(frmIBEditFutureBillPaymentPrecnf.hbox58788011366391.isVisible){
		 ref2Flag = true;	
	 }
	 
	 var AccountNo = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text;
	 if(AccountNo.indexOf("-") != -1) AccountNo = replaceCommon(AccountNo, "-", "");
	 var len = AccountNo.length;
	 if(len == 10){
	 	AccountNo = "XXX-X-"+AccountNo.substring(len-6, len-1)+"-X";
	 }else
	 	AccountNo = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text;
	 
	 
	 
	if(ref2Flag == true){
   		 pdfImagedata = {
     		"AccountNo" : AccountNo,
     		"AccountName" : frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text,
     		"BillerName" : frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text,
     		"Ref1Label" : frmIBEditFutureBillPaymentPrecnf.lblBPRef1.text,
	 		"Ref1Value" : frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text,
	 		"Ref2Label": frmIBEditFutureBillPaymentPrecnf.labelBPRef2.text,
	 		"Ref2Value": frmIBEditFutureBillPaymentPrecnf.labelBPRef2Val.text,
	 		"Amount" : frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text,
	 		"Fee" : frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text,	
	 		"PaymentOrderDate"	: frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text,
	 		"PaymentSchedule":scheduleDetails,
	 		"MyNote": frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text,
	 		"TransactionRefNo" : frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text,
	 		"localeId":kony.i18n.getCurrentLocale()
    	 }
    }else{
    	 pdfImagedata = {
     		"AccountNo" : frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text,
     		"AccountName" : frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text,
     		"BillerName" : frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text,
     		"Ref1Label" : frmIBEditFutureBillPaymentPrecnf.lblBPRef1.text,
	 		"Ref1Value" : frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text,
	 		"Amount" : frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text,
	 		"Fee" : frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text,	
	 		"PaymentOrderDate"	: frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text,
	 		"PaymentSchedule":scheduleDetails,
	 		"MyNote": frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text,
	 		"TransactionRefNo" : frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text,
	 		"localeId":kony.i18n.getCurrentLocale()
    	 }
    }
    inputParam["Ref2Falg"] = ref2Flag;
    inputParam["filetype"] = filetype;
    inputParam["templatename"] = "FutureBillPaymentTemplate";
   // inputParam["localeId"]= kony.i18n.getCurrentLocale();
    inputParam["datajson"] = JSON.stringify(pdfImagedata, "", "");
    inputParam["outputtemplatename"]= "Future_Bill_Payment_Set_Details_"+kony.os.date("ddmmyyyy");
   // var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext +"/GenericPdfImageServlet";
 //	kony.net.invokeServiceAsync(url, inputParam, callbackfunctionForPDFImageBP);
	
	//invokeServiceSecureAsync("generatePdfImage", inputParam, ibRenderFileCallbackfunction);
	
	saveFuturePDF(filetype, "067", frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text);
}
/*
function callbackfunctionForPDFImageBP(status,result){
	if (status == 400) {
        if (result["opstatus"] == 0) {
            //alert("opstatus is zero");
   			var newurl = result["ouputfilepath"]; //check this ouputfilepath
    		var url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext+"/RenderGenericPdfImageServlet";
    		uriquery = "filename="+encodeURIComponent(newurl)+"&filetype="+encodeURIComponent(fileType);
    		//alert(url+"?"+uriquery);
    		frmIBTransferGeneratePDF.browPdfTransfer.requestURLConfig = {URL: url+"?"+uriquery,requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"}
    		var XferUrl=frmIBTransferGeneratePDF.browPdfTransfer.requestURLConfig;
   		}
    }
}
*/
/*
   **************************************************************************************
		Module	: printEditFutureBP
		Author  : Kony
		Date    : 
		Purpose : To print bill payment statement
	***************************************************************************************


function printEditFutureBP() {
	alert("To be implemented");
	var service = "";
	var fileName = "";
	var inputParam = [];
	var paymentOrderDate = ""
	fileName = "FutureBillPaymentTemplate";
	service = "BillTopUpPaymentPdfImage";
	inputParam["paymentOrderDate"] = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text;
	inputParam["PaymentSchedule"] = frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text + " " +
		frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text;
	inputParam["filename"] = fileName;
	inputParam["filetype"] = "png";
	inputParam["AccountNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text;
	inputParam["AccountName"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text;
	inputParam["BillerName"] = frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text;
	inputParam["MobileNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text;
	inputParam["amount"] = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text
	inputParam["Fee"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text
	inputParam["TopUpDate"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text
	inputParam["MyNote"] = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text
	inputParam["TransactionRefNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text;
	inputParam["MeterNO"] = "";
	
	var url = "";
	url = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" +
		service + "?filename=" + inputParam["filename"] + "&filetype=" + inputParam["filetype"];
	var url1 = "&AccountNo=" + inputParam["AccountNo"] + "&AccountName=" + inputParam["AccountName"] + "&BillerName=" +
		inputParam["BillerName"] + "&MobileNo=" + inputParam["MobileNo"] + "&amount=" + inputParam["amount"] + "&Fee=" +
		inputParam["Fee"] + "&TopUpDate=" + inputParam["TopUpDate"] + "&MyNote=" + inputParam["MyNote"] +
		"&TransactionRefNo=" + inputParam["TransactionRefNo"];
	url = url + url1;
	var url2 = "&paymentOrderDate=" + inputParam["paymentOrderDate"] + "&paymentOrderDate=" + inputParam[
		"paymentOrderDate"]
	url = url + url2;
	
	frmIBTransferGeneratePDF.browPdfTransfer.requestURLConfig = {
		URL: url,
		requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
	}
	//frmIBTransferGeneratePDF.show();
	return false;
}
/*
   **************************************************************************************
		Module	: shareonFBEditFutureBP
		Author  : Kony
		Date    : 
		Purpose : share on FB
	***************************************************************************************
*/

function shareonFBEditFutureBP() {
	//alert("To be implemented");
/*
	var service = "";
	var fileName = "";
	var inputParam = [];
	var paymentOrderDate = ""
	fileName = "FutureBillPaymentTemplate";
	service = "BillTopUpPaymentPdfImage";
	inputParam["paymentOrderDate"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text;
	inputParam["PaymentSchedule"] = frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text + " " +
		frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text;
	inputParam["filename"] = fileName;
	inputParam["filetype"] = "pdf";
	inputParam["AccountNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text;
	inputParam["AccountName"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text;
	inputParam["BillerName"] = frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text;
	inputParam["MobileNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text;
	inputParam["amount"] = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text
	inputParam["Fee"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text
	inputParam["TopUpDate"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text
	inputParam["MyNote"] = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text
	inputParam["TransactionRefNo"] = frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text;
	inputParam["MeterNO"] = "";
	
	var url = "";
	url = "https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.tmbbank.com&t";
	//kony.application.openURL(url);
	frmIBTransferGeneratePDF.browPdfTransfer.requestURLConfig = {
		URL: url,
		requestMethod: "constants.BROWSER_REQUEST_METHOD_GET"
	}
	//frmIBTransferGeneratePDF.show();
	*/
	window.open('https://www.facebook.com/sharer/sharer.php?u=www.tmbbank.com', 'facebook_share', 'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
	return false;
}
/*
   **************************************************************************************
		Module	: cancelEditFutureBP
		Author  : Kony
		Date    : 
		Purpose : to cancel the editted future Bill payment on confirm page
	***************************************************************************************
*/

function cancelEditFutureBP() {
	if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
		frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible) {
		frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true);
	}
	if (frmIBBillPaymentView.lblBPeditHrd.isVisible) {
		frmIBBillPaymentView.lblBPeditHrd.setVisibility(false);
	}
	if (!frmIBBillPaymentView.lblBPViewHrd.isVisible) {
		frmIBBillPaymentView.lblBPViewHrd.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hbxBPViewHdr.isVisible) {
		frmIBBillPaymentView.hbxBPViewHdr.setVisibility(true);
	}
	
	//NEw header changs
	if (!frmIBBillPaymentView.buttonEdit.isVisible) {
		frmIBBillPaymentView.buttonEdit.setVisibility(true);
	}
	
	if (!frmIBBillPaymentView.buttonDel.isVisible) {
		frmIBBillPaymentView.buttonDel.setVisibility(true);
	}
	//--
	
	if (frmIBBillPaymentView.btnBPSchedule.isVisible) {
		frmIBBillPaymentView.btnBPSchedule.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hbxbtnReturn.isVisible) {
		frmIBBillPaymentView.hbxbtnReturn.setVisibility(true);
	}
	if (frmIBBillPaymentView.hbxEditBtns.isVisible) {
		frmIBBillPaymentView.hbxEditBtns.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
		frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hboxRepeatAs.isVisible) {
		frmIBBillPaymentView.hboxRepeatAs.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hboxEndOn.isVisible) {
		frmIBBillPaymentView.hboxEndOn.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hboxExecute.isVisible) {
		frmIBBillPaymentView.hboxExecute.setVisibility(true);
	}
	if (frmIBBillPaymentView.lblScheduleBillPayment.isVisible) {
		frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxEditBPFutureSchedule.isVisible) {
		frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
	}
	if (frmIBBillPaymentView.arrowBPScheduleField.isVisible) {
		frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
		frmIBBillPaymentView.hbxOnlinePayment.setVisibility(false);
	}
	frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = btnIBREQotpFocus;
	frmIBEditFutureBillPaymentPrecnf.btnOTPReq.setEnabled(true);
	//frmIBEditFutureBillPaymentPrecnf.btnOTPReq.onClick = callGenRefNoServiceForEditBP;
	try {
		kony.timer.cancel("OTPTimerReq")
	} catch (e) {
		
	}
	gblAmountSelected = false;
	gblScheduleFreqChanged = false;
	
	
	
	//// New changes
	if (frmIBBillPaymentView.hboxTotalAmount.isVisible) {
		frmIBBillPaymentView.hboxTotalAmount.setVisibility(false);
	}
	if (gblScheduleButtonClicked) {
		gblScheduleButtonClicked = false;
		frmIBBillPaymentView.btnBPSchedule.setEnabled(true);
	}
	frmIBBillPaymentView.lblBPStartOnDateVal.text = gblStartOn;
	if (gblEndingFreqOnLoadIB == "Never") {
		frmIBBillPaymentView.lblEndOnDateVal.text = "-";
		frmIBBillPaymentView.lblBPExcuteVal.text = "-";
		frmIBBillPaymentView.lblBPExcuteVal.setVisibility(false);
		frmIBBillPaymentView.lblBPExcuteValTimes.text = "-";
		frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true); 
	} else {
		frmIBBillPaymentView.lblEndOnDateVal.text = gblEndDateOnLoad;
		frmIBBillPaymentView.lblBPExcuteVal.text = gblExecutionOnLoad;
		frmIBBillPaymentView.lblBPExcuteVal.setVisibility(true);
		frmIBBillPaymentView.lblBPExcuteValTimes.text = kony.i18n.getLocalizedString("keyTimesMB");
		frmIBBillPaymentView.lblBPExcuteValTimes.setVisibility(true); 
	}
	frmIBBillPaymentView.lblBPRepeatAsVal.text = gblOnLoadRepeatAsIB;
	frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text; //for amt display
	if (frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
		frmIBBillPaymentView.hbxOnlinePayment.setVisibility(false);
	}
	onClickRepeatAs = "";
	onClickEndFreq = "";
	endFreqSave = "";
	repeatAsIB = "";
	gblScheduleFreqChanged = false;
	
	
}
/*
   **************************************************************************************
		Module	: cancelEditFutureBPonEditPage
		Author  : Kony
		Date    : 
		Purpose : to cancel the editted future Bill payment on edit page
	***************************************************************************************
*/

function cancelEditFutureBPonEditPage() {
	if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
		frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible) {
		frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true);
	}
	if (frmIBBillPaymentView.lblBPeditHrd.isVisible) {
		frmIBBillPaymentView.lblBPeditHrd.setVisibility(false);
	}
	if (!frmIBBillPaymentView.lblBPViewHrd.isVisible) {
		frmIBBillPaymentView.lblBPViewHrd.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hbxBPViewHdr.isVisible) {
		frmIBBillPaymentView.hbxBPViewHdr.setVisibility(true);
	} 
	
	//NEw header changes
	if(!frmIBBillPaymentView.buttonEdit.isVisible){
		frmIBBillPaymentView.buttonEdit.setVisibility(true);
	}
	if(!frmIBBillPaymentView.buttonDel.isVisible){
		frmIBBillPaymentView.buttonDel.setVisibility(true);
	}
	//--
	
	if (frmIBBillPaymentView.btnBPSchedule.isVisible) {
		frmIBBillPaymentView.btnBPSchedule.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hbxbtnReturn.isVisible) {
		frmIBBillPaymentView.hbxbtnReturn.setVisibility(true);
	}
	if (frmIBBillPaymentView.hbxEditBtns.isVisible) {
		frmIBBillPaymentView.hbxEditBtns.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
		frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hboxRepeatAs.isVisible) {
		frmIBBillPaymentView.hboxRepeatAs.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hboxEndOn.isVisible) {
		frmIBBillPaymentView.hboxEndOn.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hboxExecute.isVisible) {
		frmIBBillPaymentView.hboxExecute.setVisibility(true);
	}
	if (frmIBBillPaymentView.lblScheduleBillPayment.isVisible) {
		frmIBBillPaymentView.lblScheduleBillPayment.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxEditBPFutureSchedule.isVisible) {
		frmIBBillPaymentView.hbxEditBPFutureSchedule.setVisibility(false);
	}
	if (frmIBBillPaymentView.arrowBPScheduleField.isVisible) {
		frmIBBillPaymentView.arrowBPScheduleField.setVisibility(false);
	}
	if (frmIBBillPaymentView.hboxPenaltyAmt.isVisible) {
		frmIBBillPaymentView.hboxPenaltyAmt.setVisibility(false);
	}
	if (frmIBBillPaymentView.hboxOnlineLoan.isVisible) {
		frmIBBillPaymentView.hboxOnlineLoan.setVisibility(false);
	}
	if (frmIBBillPaymentView.hboxOnlinePayPenalty.isVisible) {
		frmIBBillPaymentView.hboxOnlinePayPenalty.setVisibility(false);
	}
	if (frmIBBillPaymentView.hboxTotalAmount.isVisible) {
		frmIBBillPaymentView.hboxTotalAmount.setVisibility(false);
	}
	if (gblScheduleButtonClicked) {
		gblScheduleButtonClicked = false;
		frmIBBillPaymentView.btnBPSchedule.setEnabled(true);
	}
	frmIBBillPaymentView.lblBPStartOnDateVal.text = gblStartOn;
	if (gblEndingFreqOnLoadIB == "Never") {
		frmIBBillPaymentView.lblEndOnDateVal.text = "-";
		frmIBBillPaymentView.lblBPExcuteVal.text = "-";
	} else {
		frmIBBillPaymentView.lblEndOnDateVal.text = gblEndDateOnLoad;
		frmIBBillPaymentView.lblBPExcuteVal.text = gblExecutionOnLoad;
	}
	frmIBBillPaymentView.lblBPRepeatAsVal.text = gblOnLoadRepeatAsIB;
	frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text; //for amt display
	if (frmIBBillPaymentView.hbxOnlinePayment.isVisible) {
		frmIBBillPaymentView.hbxOnlinePayment.setVisibility(false);
	}
	onClickRepeatAs = "";
	onClickEndFreq = "";
	endFreqSave = "";
	repeatAsIB = "";
	gblScheduleFreqChanged = false;
	
	frmIBBillPaymentView.imgTMB.setVisibility(true);
	frmIBBillPaymentView.show();
}
/*
   **************************************************************************************
		Module	: Delete Future Bill payment
		Author  : Kony
		Date    : 
		Purpose : To delete the scheduled bill payment
	***************************************************************************************
*/

function deleteFutureBillPaymentViewPage() {
	var inputparam = [];
	inputparam["RqUID"] = "";
	inputparam["PmtId"] = "";
	var scheIDDe = frmIBBillPaymentView.lblBPScheduleRefNoVal.text;
	scheIDDe = "SB" + scheIDDe.substring(2, frmIBBillPaymentView.lblBPScheduleRefNoVal.text.lenght);
	inputparam["scheID"] = scheIDDe; // get this value from calendar or future transactions;

	
	inputparam["activityFlexValues1"] = "Cancel";
	inputparam["activityFlexValues2"] = frmIBBillPaymentView.lblBPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);
	inputparam["activityFlexValues3"] = gblStartOn;
	inputparam["activityFlexValues4"] = gblOnLoadRepeatAsIB;
	inputparam["activityFlexValues5"] = gblAmtFromService;
	inputparam["isCancelFlow"] = "true";
	inputparam["EditFTFlow"] = "EditBP";

	invokeServiceSecureAsync("doPmtCan", inputparam, callBackPmtCanServiceForEditBPIBViewPage)
}

function callBackPmtCanServiceForEditBPIBViewPage(status, result) {
	if (status == 400) {
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Cancel";
       	var activityFlexValues2 =  frmIBBillPaymentView.lblBPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBBillPaymentView.lblBPRef1Val.text);	
		var activityFlexValues3 = gblStartOn ; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsIB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmtFromService; //Old Amount + New Amount (Edit case)		
        var logLinkageId = "";
		if (result["opstatus"] == 0) {
			var StatusCode = result["StatusCode"];
			var ServerStatusCode = result["ServerStatusCode"]
			var Severity = result["Severity"];
			var StatusDesc = result["StatusDesc"];
			if (StatusCode == 0) {
				activityStatus = "01";
				//Commented as  financial Activity is not required 
				/*
				var finSchdRefId = "SB" + frmIBBillPaymentView.lblBPScheduleRefNoVal.text.substring(2, frmIBBillPaymentView.lblBPScheduleRefNoVal.text.lenght);;
				setFinancialActivityLogFTDelete(finSchdRefId);
				*/
				//alert("Successfully Deleted Bill payment");
				alert(kony.i18n.getLocalizedString("ScheduledPayment_Delete"));
				if(gblActivitiesNvgn=="F"){
					invokeFutureInstructionService();
					/*
					frmIBMyActivities.show();
					if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
				    showCalendar(gsSelectedDate,frmIBMyActivities,1);*/
				    IBMyActivitiesReloadAndShowCalendar();
					
				}else if(gblActivitiesNvgn=="C"){
					
					/*
					frmIBMyActivities.show();
					if(frmIBMyActivities.btncalender.focusSkin == btnIBTabBlue)
				    showCalendar(gsSelectedDate,frmIBMyActivities,1);
				    */
				    IBMyActivitiesReloadAndShowCalendar();
					
				}
				
			} else {
				activityStatus = "02";
				//alert("Couldn't delete bill payment");
				alert(" " + StatusDesc);
			}
		}else{
			alert(result["errMsg"]);
		
		}
		activityLogServiceCall(activityTypeID, errorCode, activityStatus, deviceNickName, activityFlexValues1, activityFlexValues2, activityFlexValues3, activityFlexValues4, activityFlexValues5, logLinkageId);
	}
}

function removeCommaIB(amt) {
	var amountForService;
	for (var i = 0; i < amt.length; i++) {
		if (amt[i] != ",") {
			if (amountForService == null) {
				amountForService = amt[i];
			} else {
				amountForService = amountForService + amt[i];
			}
		}
	}
	return amountForService;
}

/*
function setFinancialActivityLogForEditBPAdd(finTxnRefId,finTxnAmount,finTxnFee,finTxnBalance){
	
	/*
	var fromAccType = frmIBBillPaymentConfirm.lblAcctType.text;
	if (fromAccType == "DDA") {
		tranCode = "8810";
	} else {
		tranCode = "8820";
	}
	
	//GBLFINANACIALACTIVITYLOG.crmId = crmId;
	GBLFINANACIALACTIVITYLOG.finTxnRefId = finTxnRefId;
	//GBLFINANACIALACTIVITYLOG.finTxnDate="";
	GBLFINANACIALACTIVITYLOG.activityTypeId = "027";
	GBLFINANACIALACTIVITYLOG.txnCd = gblTransCode;
	//GBLFINANACIALACTIVITYLOG.tellerId ="";
	GBLFINANACIALACTIVITYLOG.txnDescription ="done";
	GBLFINANACIALACTIVITYLOG.finLinkageId = "1";
	GBLFINANACIALACTIVITYLOG.channelId = "01";
	GBLFINANACIALACTIVITYLOG.fromAcctId = removeHyphenIB(frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text);
	//GBLFINANACIALACTIVITYLOG.toAcctId = "";
	GBLFINANACIALACTIVITYLOG.toBankAcctCd = "11";//check this - TO DO 
	GBLFINANACIALACTIVITYLOG.finTxnAmount = finTxnAmount;
	GBLFINANACIALACTIVITYLOG.finTxnFee = finTxnFee;
	GBLFINANACIALACTIVITYLOG.finTxnBalance = finTxnBalance;
	GBLFINANACIALACTIVITYLOG.finTxnMemo = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text;
	GBLFINANACIALACTIVITYLOG.billerRef1 = removeHyphenIB(frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text);
	GBLFINANACIALACTIVITYLOG.billerRef2 = removeHyphenIB(frmIBEditFutureBillPaymentPrecnf.labelBPRef2Val.text);
	GBLFINANACIALACTIVITYLOG.noteToRecipient ="";
	GBLFINANACIALACTIVITYLOG.recipientMobile ="";
	GBLFINANACIALACTIVITYLOG.recipientEmail = "";
	GBLFINANACIALACTIVITYLOG.finTxnStatus = "";//check this - TO DO 
	//GBLFINANACIALACTIVITYLOG.smartFlag
	//GBLFINANACIALACTIVITYLOG.clearingStatus
	GBLFINANACIALACTIVITYLOG.finSchduleRefId = "SB" + frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.substring(2, frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.length);
	GBLFINANACIALACTIVITYLOG.billerCommCode = gblBillerCompcode;
	GBLFINANACIALACTIVITYLOG.fromAcctName = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text;
	var fromAccNickName =  frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNickName.text+"";
	fromAccNickName = fromAccNickName.substring(0, 20);
	GBLFINANACIALACTIVITYLOG.fromAcctNickname = fromAccNickName;
	GBLFINANACIALACTIVITYLOG.toAcctName = frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text;
	var billerNickName =  frmIBEditFutureBillPaymentPrecnf.lblBPBillerNickName.text+"";
	billerNickName = billerNickName.substring(0, 20);
	GBLFINANACIALACTIVITYLOG.toAcctNickname = billerNickName;
	//GBLFINANACIALACTIVITYLOG.billerCustomerName = ""; //check this To Do 
	GBLFINANACIALACTIVITYLOG.billerBalance = "0"; //check this To Do 
	GBLFINANACIALACTIVITYLOG.activityRefId = ""; //check this To Do
	var eventId = frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text;
	eventId = eventId.substring(2, eventId.length-2);
	GBLFINANACIALACTIVITYLOG.eventId = eventId; //check this To Do
	GBLFINANACIALACTIVITYLOG.errorCd = "0000000000000";//check this To Do
	GBLFINANACIALACTIVITYLOG.txnType = "002"; //005 for TU
	//GBLFINANACIALACTIVITYLOG.tdInterestAmount = "";
	//GBLFINANACIALACTIVITYLOG.tdTaxAmount = "";
	//GBLFINANACIALACTIVITYLOG.tdPenaltyAmount  = "";
	//GBLFINANACIALACTIVITYLOG.tdNetAmount  = "";
	//GBLFINANACIALACTIVITYLOG.tdMaturityDate  = "";
	//GBLFINANACIALACTIVITYLOG.remainingFreeTxn  = "";
	//GBLFINANACIALACTIVITYLOG.toAccountBalance = "";
	//GBLFINANACIALACTIVITYLOG.sendToSavePoint = "";
	//GBLFINANACIALACTIVITYLOG.openTdTerm = "";
	//GBLFINANACIALACTIVITYLOG.openTdInterestRate = "";
	//GBLFINANACIALACTIVITYLOG.openTdMaturityDate = "";
	//GBLFINANACIALACTIVITYLOG.affiliatedAcctNickname = "";
	//GBLFINANACIALACTIVITYLOG.affiliatedAcctId = "";
	//GBLFINANACIALACTIVITYLOG.beneficialFirstname = "";
	//GBLFINANACIALACTIVITYLOG.beneficialLastname = "";
	//GBLFINANACIALACTIVITYLOG.relationship = "";
	//GBLFINANACIALACTIVITYLOG.percentage = "";
	//GBLFINANACIALACTIVITYLOG.finFlexValues1 ="";
	//GBLFINANACIALACTIVITYLOG.finFlexValues2 ="";
	//GBLFINANACIALACTIVITYLOG.finFlexValues3 ="";
	//GBLFINANACIALACTIVITYLOG.finFlexValues4 ="";
	//GBLFINANACIALACTIVITYLOG.finFlexValues5 ="";
	GBLFINANACIALACTIVITYLOG.dueDate = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text;
	//GBLFINANACIALACTIVITYLOG.beepAndBillTxnId ="";

	financialActivityLogServiceCall(GBLFINANACIALACTIVITYLOG);
}

*/


function checkIBStatusForDel() {
	var inputparam = [];
	inputparam["crmId"] = "";
	inputparam["rqUUId"] = "";
	showLoadingScreenPopup();
	invokeServiceSecureAsync("crmProfileInq", inputparam, callBackCrmProfileInqForDelBP);
}
/*
   **************************************************************************************
		Module	: callBackCrmProfileInqForDelBP
		Author  : Kony
		Date    : 
		Purpose : 
	***************************************************************************************
*/

function callBackCrmProfileInqForDelBP(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			var statusCode = result["statusCode"];
			var severity = result["Severity"];
			var statusDesc = result["StatusDesc"];
			if (statusCode != 0) {
				
			} else {
				var ibStat = result["ibUserStatusIdTr"];
				if (ibStat != null) {
					var inputParam = [];
					inputParam["ibStatus"] = ibStat;
					invokeServiceSecureAsync("checkIBMBStatus", inputParam, callBackCheckIBStatus);
				}
			}
		} else {
			dismissLoadingScreenPopup();
			alert(result["errMsg"]);
		}
	} else {
		//dismissLoadingScreenPopup();
		if (status == 300) {
			
		}
	}
}


function callBackCheckIBStatus(status, result) {
	if (status == 400) {
		if (result["opstatus"] == 0) {
			if (result["ibStatusFlag"] == "true") {
				//alert("Your transaction has been locked, so you can't Delete Bill Payment");
				alert(kony.i18n.getLocalizedString["Error_UserStatusLocked"]);
				dismissLoadingScreenPopup();
				return false;
			} else {
				dismissLoadingScreenPopup();
				popIBFutureBPCancel.show();
			}
		} else {
			dismissLoadingScreenPopup();
			
		}
	} else {
		if (status == 300) {
			dismissLoadingScreenPopup();
			
		}
	}
	
}


//Token switching changes
function tokenHandlerEditBP(){
	if(gblSwitchToken == false && gblTokenSwitchFlag == false){
		srvTokenSwitchingEditBP();
	}else if(gblTokenSwitchFlag ==false && gblSwitchToken == true){
		frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(true);
		frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(true);
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
	    dismissLoadingScreenPopup();
	    callGenRefNoServiceForEditBP()
	}else if(gblTokenSwitchFlag ==true && gblSwitchToken == false){
		frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(false);
		frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(false);
		frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
		
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keySwitchToSMS");
		frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
		frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
	    frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(true);
	    dismissLoadingScreenPopup();
	    
	    frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(true);
	    frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
		frmIBEditFutureBillPaymentPrecnf.hbxEditBtns.setVisibility(true);
		frmIBEditFutureBillPaymentPrecnf.btnNext.setVisibility(false);
	    
	}else if(gblTokenSwitchFlag ==true && gblSwitchToken == true){
	
		frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(true);
		frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(true);
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
		frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
		frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
		frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
	    dismissLoadingScreenPopup();
	    callGenRefNoServiceForEditBP()
	
	}
}	
	
function srvTokenSwitchingEditBP(){
	var inputParam = [];
	inputParam["crmId"] = gblcrmId;
	showLoadingScreenPopup();
	invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingEditBPCallBack);
}

function srvTokenSwitchingEditBPCallBack(status,callbackResponse){
   if(status  == 400){
   	 if(callbackResponse["opstatus"] == 0){
		//dismissLoadingScreen();
		if(callbackResponse["deviceFlag"].length==0){
			
			//return
		}
		    if(callbackResponse["deviceFlag"].length > 0 ){
				var tokenFlag = callbackResponse["deviceFlag"][0]["TOKEN_DEVICE_FLAG"];
				var mediaPreference = callbackResponse["deviceFlag"][0]["MEDIA_PREFERENCE"];
				var tokenStatus = callbackResponse["deviceFlag"][0]["TOKEN_STATUS_ID"];
				
				if (tokenFlag=="Y" && ( mediaPreference == "Token" || mediaPreference == "TOKEN") && tokenStatus=='02' ){
					//tokenFlag = true;
					frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(false);
					frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(false);
					frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyPleaseEnterToken");
					frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
					frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keySwitchToSMS");
					frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
					
					frmIBEditFutureBillPaymentPrecnf.hbxOTP.setVisibility(true);
					frmIBEditFutureBillPaymentPrecnf.hbxEditBtns.setVisibility(true);
					frmIBEditFutureBillPaymentPrecnf.btnNext.setVisibility(false);
					
					dismissLoadingScreenPopup();
					frmIBEditFutureBillPaymentPrecnf.txtBxOTP.setFocus(true);
					gblTokenSwitchFlag = true;
					
				} else {
					//tokenFlag = false; // same flag is being used in the flow alredy
					frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(true);
					frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(true);
					frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
					frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
					frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
					frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
					
					gblTokenSwitchFlag = false;
					callGenRefNoServiceForEditBP()
					//dismissLoadingScreenPopup();
					
				}
			}else{
			
				frmIBEditFutureBillPaymentPrecnf.hboxBankRefNo.setVisibility(true);
				frmIBEditFutureBillPaymentPrecnf.hboxOTPSent.setVisibility(true);
				frmIBEditFutureBillPaymentPrecnf.labelkeyOTPMsg.text = kony.i18n.getLocalizedString("keyotpmsgreq");
				frmIBEditFutureBillPaymentPrecnf.labelkeyOTP.text = kony.i18n.getLocalizedString("keyOTP");
				frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text = kony.i18n.getLocalizedString("keyRequest");
				frmIBEditFutureBillPaymentPrecnf.btnOTPReq.skin = "btnIBREQotpFocus";
				
				gblTokenSwitchFlag = false;
				callGenRefNoServiceForEditBP()
			}
		}else {
		 	dismissLoadingScreenPopup();
		 }
    }	
}

//verify token changes
gblVerifyOTPEditBP = 0;
function onClickConfirmEditBPIB(){
	var otpText = frmIBEditFutureBillPaymentPrecnf.txtBxOTP.text;
	var OTPLen = otpText.length;
	var isNumOTP = kony.string.isNumeric(otpText);
	
	if(frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text == kony.i18n.getLocalizedString("keyRequest")){
		if(otpText == "" || isNumOTP == false || OTPLen != gblOTPLENGTHEdit){
	  		dismissLoadingScreenPopup();
	  		alert(kony.i18n.getLocalizedString("Receipent_alert_correctOTP"));
            return false;	
	  	}
	
	}else{
		if(otpText == "" || isNumOTP == false){
	 		dismissLoadingScreenPopup();
	 		alert(kony.i18n.getLocalizedString("emptyToken"));
		 	return false;
	 	}
	
	}

	gblVerifyOTPEditBP = gblVerifyOTPEditBP + 1
	srvVerifyPasswordExEditBP();

}

function srvVerifyPasswordExEditBP(){
	
	showLoadingScreenPopup()
	var inputParam = {};
	//gblTokenSwitchFlag == false
	if(frmIBEditFutureBillPaymentPrecnf.btnOTPReq.text == kony.i18n.getLocalizedString("keySwitchToSMS")) {
		inputParam["OTP_TOKEN_FLAG"] = "TOKEN";
	}
	else{
		  inputParam["OTP_TOKEN_FLAG"] = "OTP";
	}
		inputParam["retryCounterVerifyOTP"] = gblRetryCountRequestOTP;
		inputParam["password"] = frmIBEditFutureBillPaymentPrecnf.txtBxOTP.text;
	if ((gblAmountSelected == true && gblScheduleFreqChanged == true) || (gblAmountSelected == false && gblScheduleFreqChanged == true)) {
			var scheIDDele = frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text;
			scheIDDele = "SB" + scheIDDele.substring(2, scheIDDele.length);
			
			inputParam["scheID"] = scheIDDele;
			inputParam["selectedEditOption"] = "Frequency";
			inputParam["transRefType"] = "SB";
		var edAmt = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text;  //removeCommaIB(frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text);
		
		if(edAmt.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1)
			edAmt = edAmt.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		
		if (edAmt.indexOf(",") != -1) {
       	    edAmt = parseFloat(replaceCommon(edAmt, ",", "")).toFixed(2);
   		}else
    	    edAmt = parseFloat(edAmt).toFixed(2);
		
		var ref2 = "";
	    if(frmIBBillPaymentView.hbox58788011366391.isVisible) {
		    ref2 = frmIBEditFutureBillPaymentPrecnf.labelBPRef2Val.text;
		    inputParam["pmtRefNo2"] = ref2;
	     }
		if(gblRef2EditBP != ""){
			inputParam["pmtRefNo2"] = ref2;
		}
		inputParam["amt"] = edAmt;
		inputParam["fromAcct"] = gblFromAccNo; // from Account ID (from PaymentInq)
		inputParam["fromAcctType"] = gblFromAccType; // from Account Type (from PaymentInq)
		inputParam["toAcct"] = gblToAccNo; // TO Biller No (from PaymentInq)
		inputParam["toAccTType"] = gblToAccType; // To Biller Type ((from PaymentInq)
		inputParam["dueDate"] = changeDateFormatForService(frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text); // Start On Date
		inputParam["pmtRefNo1"] = removeHyphenIB(frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text); // Ref 1 value
		inputParam["custPayeeId"] = gblCustPayID; // check this with TL
		inputParam["transCode"] = gblTransCode; //TransCode (from PaymentInq)
		inputParam["memo"] = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text; // My Note 
	    var refBP = frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text;
		refBP = removeHyphenIB(refBP);
		   if(gblEditBillMethod == "1"){
		     inputParam["PmtMiscType"] = "MOBILE";
		     inputParam["MiscText"] = refBP;
		   }
		   inputParam["billMethod"] = gblEditBillMethod;
		inputParam["pmtMethod"] = gblEditPaymentMethod ; // get this value from paymentInq
	
		inputParam["extTrnRefId"] = "SB" + frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.substring(2, frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.length); // Updated Schedule Ref No
		inputParam["endFreqSave"] = endFreqSave;	
		if (endFreqSave == "After") {
			inputParam["NumInsts"] = frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text; // Execution times 
		}
		if (endFreqSave == "OnDate") {
			inputParam["FinalDueDt"] = changeDateFormatForService(frmIBEditFutureBillPaymentPrecnf.lblEndOnDateVal.text); // End On Date
		}
		inputParam["frequencyCode"] = "0";
		var frequency = frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text;
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Weekly"))) {
			inputParam["dayOfWeek"] = "";
			inputParam["frequencyCode"] = "1";
			frequency = "Weekly";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Monthly"))) {
			inputParam["dayOfMonth"] = "";
			inputParam["frequencyCode"] = "2";
			frequency = "Monthly";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Yearly"))) {
			frequency = "Annually";
			inputParam["dayofMonth"] = "";
			inputParam["monthofYear"] = "";
			inputParam["frequencyCode"] = "3";
		}
		if (kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("Transfer_Daily"))) {
			inputParam["dayOfWeek"] = "";
			inputParam["frequencyCode"] = "4";
			frequency = "Daily";
		}
		if(kony.string.equalsIgnoreCase(frequency, "Once") || kony.string.equalsIgnoreCase(frequency, kony.i18n.getLocalizedString("keyOnce"))   ){
			inputParam["freq"] = "once";
		}else{
			inputParam["freq"] = frequency;
		}
		inputParam["desc"] = "";
		var feeAmt = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0",lenFee-1);
        feeAmt = parseFloat(feeAmt.toString());
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text+"+"+ removeHyphenIB(frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = "";
		var activityFlexValues4 = "";
		var activityFlexValues5 = "";
		if(gblScheduleFreqChanged == true){
			activityFlexValues3 = gblStartOn +"+"+ frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIB +"+"+  frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text; //Old Frequency + New Frequency (Edit case)
		}else{
			activityFlexValues3 = gblStartOn; //Old Future Date + New Future Date (Edit case)
			activityFlexValues4 = gblOnLoadRepeatAsIB; //Old Frequency + New Frequency (Edit case)
		}
		
		activityFlexValues5 = gblAmtFromService +"+"+(gblUserEnteredAmt)
		/*
		if(gblAmountSelected == true){
			activityFlexValues5 = gblAmtFromService +"+"+(gblUserEnteredAmt); //Old Amount + New Amount (Edit case)		
 		}else{
			activityFlexValues5 = gblAmtFromService; //Old Amount + New Amount (Edit case)		
 		}
		*/
	} else if (gblAmountSelected == true && gblScheduleFreqChanged == false) {
	//	gblAmountSelected = false;
		//gblScheduleFreqChanged = false;
		//callPaymentUpdateService();
		inputParam["selectedEditOption"] = "Amount";
		var editedAmount = "";
		editedAmount = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text;	 //removeCommaIB(frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text);
		editedAmount = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text;
		if(editedAmount.indexOf(kony.i18n.getLocalizedString("currencyThaiBaht")) != -1){
			editedAmount = editedAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "");
		}
		
		if (editedAmount.indexOf(",") != -1) {
       	    editedAmount = parseFloat(replaceCommon(editedAmount, ",", "")).toFixed(2);
   		}else
    	    editedAmount = parseFloat(editedAmount).toFixed(2);
		
		inputParam["extTrnRefID"] ="SB" + frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.substring(2, frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text.length);
		
		inputParam["Amt"] = editedAmount;
		inputParam["Starton"] = changeDateFormatForService(frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text);
		var feeAmt = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text;
        var lenFee = feeAmt.length;
        feeAmt = feeAmt.substring("0",lenFee-1);
        feeAmt = parseFloat(feeAmt.toString());
		var activityTypeID = "067";
        var errorCode = "";
        var activityStatus = "";
        var deviceNickName = "";
        var activityFlexValues1 = "Edit";
        var activityFlexValues2 = frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text +"+"+ removeHyphenIB(frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text);    //Biller Name + Ref1
		var activityFlexValues3 = gblStartOn; //Old Future Date + New Future Date (Edit case)
		var activityFlexValues4 = gblOnLoadRepeatAsIB; //Old Frequency + New Frequency (Edit case)
		var activityFlexValues5 = gblAmtFromService +"+"+ (gblUserEnteredAmt); //Old Amount + New Amount (Edit case)
		
	}
		inputParam["activityTypeID"] = activityTypeID;
		inputParam["activityFlexValues1"] = activityFlexValues1;
		inputParam["activityFlexValues2"] = activityFlexValues2;
		inputParam["activityFlexValues3"] = activityFlexValues3;
		inputParam["activityFlexValues4"] = activityFlexValues4;
		inputParam["activityFlexValues5"] = activityFlexValues5;
	if (gblTrasSMS == 1) // check this gblTranSMS 
		notificationType = "SMS";
	else notificationType = "Email";
	inputParam["notificationType"] = notificationType; // check this : which value we have to pass
	inputParam["customerName"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text; 
	var fromAccount = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNumber.text;
	if(fromAccount.indexOf("-") != -1){
	    fromAccount = replaceCommon(fromAccount, "-", "");
	}
	inputParam["fromAccount"] = fromAccount;
	inputParam["fromAcctNick"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccNickName.text;
	inputParam["fromAcctName"] = frmIBEditFutureBillPaymentPrecnf.lblBPFromAccName.text; 
	inputParam["billerNick"] = frmIBEditFutureBillPaymentPrecnf.lblBPBillerNickName.text; 
	inputParam["billerName"] = frmIBEditFutureBillPaymentPrecnf.lblBPBillNameAndCompCode.text; 
	inputParam["billerNameTH"] = billerNameThai;
	inputParam["ref1EN"] = gblRef1LblEN+" : " + frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text;
    inputParam["ref1TH"] = gblRef1LblTH+" : " + frmIBEditFutureBillPaymentPrecnf.lblBPRef1Val.text;
	var ref2 = "";
    if(frmIBBillPaymentView.hbox58788011366391.isVisible) {
	    ref2 = frmIBEditFutureBillPaymentPrecnf.labelBPRef2Val.text;
	    inputParam["ref2EN"] = gblRef2LblEN+" : " + ref2;
        inputParam["ref2TH"] = gblRef2LblTH+" : " + ref2;
     }else{
       inputParam["ref2EN"] = "";
       inputParam["ref2TH"] = "";
     }  
  	inputParam["paymentOrderDateEmailPDF"] =frmIBEditFutureBillPaymentPrecnf.lblBPPaymentOrderValue.text;
	inputParam["amount"] = frmIBEditFutureBillPaymentPrecnf.lblBPAmtValue.text; 
	inputParam["fee"] = frmIBEditFutureBillPaymentPrecnf.lblBPPaymentFeeVal.text; 
	inputParam["initiationDt"] = NormalDate;
	inputParam["recurring"] = frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text; 
	inputParam["startDt"] = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text;
	inputParam["endDt"] = frmIBEditFutureBillPaymentPrecnf.lblEndOnDateVal.text; 
	inputParam["refID"] = frmIBEditFutureBillPaymentPrecnf.lblBPScheduleRefNoVal.text;
	inputParam["mynote"] = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text;
	inputParam["memo"] = frmIBEditFutureBillPaymentPrecnf.lblMyNoteVal.text;
	inputParam["source"] = "FutureBillPaymentAndTopUp";
	inputParam["Locale"] = kony.i18n.getCurrentLocale();
	
	var scheduleDetails = "";
	if(frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text == "Once"){
	 	scheduleDetails = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text;
	 }else if(frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text == "-"){
	 	scheduleDetails = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text;
	 } else{
	 	scheduleDetails = frmIBEditFutureBillPaymentPrecnf.lblBPViewStartOnDateVal.text + " " + kony.i18n.getLocalizedString("keyTo") + " " + frmIBEditFutureBillPaymentPrecnf.lblEndOnDateVal.text + " " + kony.i18n.getLocalizedString("keyRepeat") + " " + frmIBEditFutureBillPaymentPrecnf.lblBPRepeatAsVal.text +" for "+frmIBEditFutureBillPaymentPrecnf.lblBPExcuteVal.text + " " + kony.i18n.getLocalizedString("keyTimesIB");
	 }
	
	inputParam["PaymentSchedule"] = scheduleDetails;
	
	invokeServiceSecureAsync("FutureBillPaymentEditExecute", inputParam, callBackVerifyOTPForEditBPIB);
}








function resetToView(){
	if (frmIBBillPaymentView.hbxBPEditAmnt.isVisible) {
	//frmIBBillPaymentView.txtBPEditAmtValue.text = frmIBBillPaymentView.lblBPAmtValue.text;
		frmIBBillPaymentView.hbxBPEditAmnt.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hbxBPVeiwAmnt.isVisible) {
		frmIBBillPaymentView.hbxBPVeiwAmnt.setVisibility(true);
	}
	if (frmIBBillPaymentView.lblBPeditHrd.isVisible) {
		frmIBBillPaymentView.lblBPeditHrd.setVisibility(false);
	}
	if (!frmIBBillPaymentView.lblBPViewHrd.isVisible) {
		frmIBBillPaymentView.lblBPViewHrd.setVisibility(true);
	}
	if (!frmIBBillPaymentView.hbxBPViewHdr.isVisible) {
		frmIBBillPaymentView.hbxBPViewHdr.setVisibility(true);
	}
	
	//New Header changes
	if(!frmIBBillPaymentView.buttonEdit.isVisible){
		frmIBBillPaymentView.buttonEdit.setVisibility(true);
	}
	
	if(!frmIBBillPaymentView.buttonDel.isVisible){
		frmIBBillPaymentView.buttonDel.setVisibility(true);
	}
	//--
	
	if (frmIBBillPaymentView.btnBPSchedule.isVisible) {
		frmIBBillPaymentView.btnBPSchedule.setVisibility(false);
	}
	if (!frmIBBillPaymentView.hbxbtnReturn.isVisible) {
		frmIBBillPaymentView.hbxbtnReturn.setVisibility(true);
	}
	if (frmIBBillPaymentView.hbxEditBtns.isVisible) {
		frmIBBillPaymentView.hbxEditBtns.setVisibility(false);
	}
	if (frmIBBillPaymentView.hbxBPAfterEditAvilBal.isVisible) {
		frmIBBillPaymentView.hbxBPAfterEditAvilBal.setVisibility(false);
	}

}

/*
function syncIBEditBP(){
	var curfrm = kony.application.getCurrentForm().id;
	if(curfrm == "frmIBBillPaymentView" || curfrm == "frmIBEditFutureBillPaymentPrecnf"){
		var locale = kony.i18n.getCurrentLocale();
		if(locale == "en_US"){
			frmIBBillPaymentView.lblBPRef1.text = gblRef1LblEN+":";
			if(gblRef2LblEN != undefined){
				frmIBBillPaymentView.labelBPRef2.text = gblRef2LblEN + ":";		
			}else
				frmIBBillPaymentView.labelBPRef2.text = "";
		}else{
			frmIBBillPaymentView.lblBPRef1.text = gblRef1LblTH+":";
			if(gblRef2LblTH != undefined){
				frmIBBillPaymentView.labelBPRef2.text = gblRef2LblTH + ":";
			}else
			    frmIBBillPaymentView.labelBPRef2.text = "";
		
		}
	}

}


*/

function srvTokenSwitchingDummyEditBP() {
    var inputParam = [];
    //showLoadingScreenPopup();
    invokeServiceSecureAsync("tokenSwitching", inputParam, srvTokenSwitchingDummyCallBackEditBP);
}

function srvTokenSwitchingDummyCallBackEditBP(status, callbackResponse) {
    if (status == 400) {
        if (callbackResponse["opstatus"] == 0) {
        	callCrmProfileInqServiceForEditBPAmt();
        }
	}
}




