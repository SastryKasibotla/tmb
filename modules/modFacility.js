function defineFacilityGlobals(){
  debitSaveFlag = false;
  cashSaveFlag = false;
  saveFI = false;
  gblFacilityInfo = {};
  gblGetPaymentMethodResponse = "";
  gblRecievePaymentMethodResponse = "";
  gblGetFacilityServiceResponse = "";
  gblSelectedFacilityItem = "";

}

function setDataFromGetFacilityService(result) {
    try {
       var locale_app = kony.i18n.getCurrentLocale();
      	gblFacilityInfo = {};
       if(result["facilitySavedFlag"] == "Y"){
          if (result["limitApplied"] != "") {
            frmFacilityInformation.txtReqCreditcardAmt.text = commaFormatted(result["limitApplied"]);
            frmFacilityInformation.lblRequestCreditLimit.isVisible = false;
            frmFacilityInformation.lblRequestCreditLimitNameHeader.isVisible = true;
            frmFacilityInformation.txtReqCreditcardAmt.isVisible = true;
            // frmFacilityInformation_txtNumberField_onTextChange(frmFacilityInformation.txtReqCreditcardAmt)
        }else{
          	frmFacilityInformation.txtReqCreditcardAmt.text = "";
            frmFacilityInformation.lblRequestCreditLimit.isVisible = true;
            frmFacilityInformation.lblRequestCreditLimitNameHeader.isVisible = false;
            frmFacilityInformation.txtReqCreditcardAmt.isVisible = false;
        }

        if (result["tenure"] != "") {
            for (var j = 0; j < gblPersonalInfo.TENURE.length; j++) {
                if (result["tenure"] == gblPersonalInfo.TENURE[j].entryCode) {
                  	if ("th_TH" == locale_app) {
                        frmFacilityInformation.lblTenureDesc.text = gblPersonalInfo.TENURE[j].entryName2;
                    } else {
                        frmFacilityInformation.lblTenureDesc.text = gblPersonalInfo.TENURE[j].entryName;
                    }
                    gblFacilityInfo["tenureCode"]= result["tenure"]; 
                }
            }
            frmFacilityInformation.lblTenure.isVisible = false;
            frmFacilityInformation.lblTenureHeader.isVisible = true;
            frmFacilityInformation.lblTenureDesc.isVisible = true;

        }else{
          	frmFacilityInformation.lblTenureDesc.text = "";
          	frmFacilityInformation.lblTenure.isVisible = true;
            frmFacilityInformation.lblTenureHeader.isVisible = false;
            frmFacilityInformation.lblTenureDesc.isVisible = false;
        }
          if (result["cardDelivery"] != "") {
            for (var j = 0; j < gblPersonalInfo.CARD_SEND_PREFERENCE.length; j++) {
                if (result["cardDelivery"] == gblPersonalInfo.CARD_SEND_PREFERENCE[j].entryCode) {
                  	if ("th_TH" == locale_app) {
                        frmFacilityInformation.lblCardDeliveryDesc.text = gblPersonalInfo.CARD_SEND_PREFERENCE[j].entryName2;
                    } else {
                        frmFacilityInformation.lblCardDeliveryDesc.text = gblPersonalInfo.CARD_SEND_PREFERENCE[j].entryName;
                    }
                   gblFacilityInfo["cardDeliveryCode"] = result["cardDelivery"];
                }
            }
            frmFacilityInformation.lblCardDelivery.isVisible = false;
            frmFacilityInformation.lblCardDeliveryDesc.isVisible = true;
            frmFacilityInformation.lblCardDeliveryHeader.isVisible = true;
            // frmFacilityInformation.lblCardDeliveryDesc.text = result["cardDelivery"];
        }else{
          frmFacilityInformation.lblCardDeliveryDesc.text = "";
          frmFacilityInformation.lblCardDelivery.isVisible = true;
          frmFacilityInformation.lblCardDeliveryDesc.isVisible = false;
          frmFacilityInformation.lblCardDeliveryHeader.isVisible = false;
        }
        if (result["mailingPreference"] != "") {
            for (var j = 0; j < gblPersonalInfo.MAIL_PREFERENCE.length; j++) {
                if (result["mailingPreference"] == gblPersonalInfo.MAIL_PREFERENCE[j].entryCode) {
                  	if ("th_TH" == locale_app) {
                        frmFacilityInformation.lblMailingPreDesc.text = gblPersonalInfo.MAIL_PREFERENCE[j].entryName2;
                    } else {
                        frmFacilityInformation.lblMailingPreDesc.text = gblPersonalInfo.MAIL_PREFERENCE[j].entryName;
                    }  
                  gblFacilityInfo["mailPrefCode"] = result["mailingPreference"];
                }
            }
            frmFacilityInformation.lblMailingPref.isVisible = false;
            frmFacilityInformation.lblMailingPreDesc.isVisible = true;
            frmFacilityInformation.lblMailingPreHeader.isVisible = true;
            //   frmFacilityInformation.lblMailingPreDesc.text = result["mailingPreference"];
        }else{
          frmFacilityInformation.lblMailingPreDesc.text = "";
          frmFacilityInformation.lblMailingPref.isVisible = true;
          frmFacilityInformation.lblMailingPreDesc.isVisible = false;
          frmFacilityInformation.lblMailingPreHeader.isVisible = false;
        }
        

        if (result["paymentMethod"] == "0" || result["paymentMethod"] == "1") {

            if (result["paymentMethod"] == "0") {
                frmFacilityInformation.lblPaymentMethodAccountName.isVisible = false;
                frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = false;
                frmFacilityInformation.lblCashCheque.isVisible = true;
                frmFacilityInformation.flexPaymentMethodPopup.isVisible = false;
                frmFacilityInformation.lblPaymentMethodType.isVisible = true;
                cashSaveFlag = true;
                debitSaveFlag = false;
            } else if (result["paymentMethod"] == "1") {
                if (result["paymentAccountNo"] != "") {
                    frmFacilityInformation.lblPaymentMethodAcctNo.text = formatAccount(result["paymentAccountNo"]);
                    frmFacilityInformation.lblPaymentMethodAccountName.text = result["paymentAccountName"];
                    frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = true;
                    frmFacilityInformation.lblPaymentMethodBankName.isVisible = false;
                    frmFacilityInformation.lblPaymentMethodAccountName.isVisible = true;
                    frmFacilityInformation.lblCashCheque.isVisible = false;
                    frmFacilityInformation.lblPaymentMethodType.isVisible = true;
                    debitSaveFlag = true;
                    cashSaveFlag = false;
                }
            }
            frmFacilityInformation.lblPmtMethod.isVisible = false;
            frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
            frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
            frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";
            //setDatatoPaymentMethod();
        }else{
          		frmFacilityInformation.lblPaymentMethodAcctNo.text = "";
                frmFacilityInformation.lblPaymentMethodAccountName.text = "";
                frmFacilityInformation.lblPaymentMethodAccountName.isVisible = false;
                frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = false;
                frmFacilityInformation.lblCashCheque.isVisible = false;
                frmFacilityInformation.lblPmtMethod.isVisible = true;
                frmFacilityInformation.lblPaymentMethodType.isVisible = false;
          		frmFacilityInformation.btnSavePaymentMethod.setEnabled(false);
                frmFacilityInformation.btnSavePaymentMethod.skin = "btnGreyBGNoRound";
                frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnGreyBGNoRound";
          		
        }
         if (result["payMethodCriteria"] != "") {
            for (var j = 0; j < gblPersonalInfo.PYMT_CRITERIA.length; j++) {
                if (result["payMethodCriteria"] == gblPersonalInfo.PYMT_CRITERIA[j].entryCode) {
                  	if ("th_TH" == locale_app) {
                        frmFacilityInformation.lblcriteriavalue.text = gblPersonalInfo.PYMT_CRITERIA[j].entryName2;
                    } else {
                        frmFacilityInformation.lblcriteriavalue.text = gblPersonalInfo.PYMT_CRITERIA[j].entryName;
                    }  
                    gblFacilityInfo["paymentCriteriaCode"] = result["payMethodCriteria"];
                }
            }

            frmFacilityInformation.lblPmtCreteria.isVisible = false;
            frmFacilityInformation.lblcriteria.isVisible = true;
            frmFacilityInformation.lblcriteriavalue.isVisible = true;
            //frmFacilityInformation.lblcriteriavalue.text = result["payMethodCriteria"];
        }else{
          	frmFacilityInformation.lblcriteriavalue.text = "";
            frmFacilityInformation.lblPmtCreteria.isVisible = true;
            frmFacilityInformation.lblcriteria.isVisible = false;
            frmFacilityInformation.lblcriteriavalue.isVisible = false;
            if(cashSaveFlag){
              frmFacilityInformation.flexPmtCriteria.onClick = doNothing;
              frmFacilityInformation.flexPmtCriteria.isVisible = false;
              frmFacilityInformation.lblPmtCreteria.skin = "lblGrey171"
            }else{
              frmFacilityInformation.flexPmtCriteria.onClick = onClickFacilityFlexItems
              frmFacilityInformation.flexPmtCriteria.isVisible = true;
              frmFacilityInformation.lblPmtCreteria.skin = "lblblue24px007abc"
            }
        }
        if (result["disburstAccountNo"] != "") {
            frmFacilityInformation.lblLoanRecevie.isVisible = false;
            frmFacilityInformation.lblLoanRecevieHeader.isVisible = true;
            frmFacilityInformation.lblAccNo.isVisible = true;
            frmFacilityInformation.lblAccName.isVisible = true;
            frmFacilityInformation.lblAccName.text = result["disburstAccountName"];
            frmFacilityInformation.lblAccNo.text = formatAccount(result["disburstAccountNo"]);
        }else{
          	frmFacilityInformation.lblAccNo.text = "";
          	frmFacilityInformation.lblAccName.text = "";
          	frmFacilityInformation.lblLoanRecevie.isVisible = true;
            frmFacilityInformation.lblLoanRecevieHeader.isVisible = false;
            frmFacilityInformation.lblAccNo.isVisible = false;
            frmFacilityInformation.lblAccName.isVisible = false;
        }


         if (result["considerLoanWithOtherBank"] != "" && result["considerLoanWithOtherBank"] != undefined)  {
             var considerLoanData = [{
                "lblName": kony.i18n.getLocalizedString("Select_None"),
                "lblSegRowLine": ".",
                "lblCode": "1"
            }, {
                "lblName": kony.i18n.getLocalizedString("Select_One_label"),
                "lblSegRowLine": ".",
                "lblCode": "2"
            }, {
                "lblName": kony.i18n.getLocalizedString("Select_More_than_three_label"),
                "lblSegRowLine": ".",
                "lblCode": "3"
            }];
     		for (var j = 0; j < considerLoanData.length; j++) {
           	 if(result["considerLoanWithOtherBank"] == considerLoanData[j].lblCode){
               frmFacilityInformation.lblConsiderLoanValue.text = considerLoanData[j].lblName;	
               	gblFacilityInfo["considerLoanCode"] = result["considerLoanWithOtherBank"];
               }
              }
         	  frmFacilityInformation.lblConsiderLoanHeader.isVisible = false;
              frmFacilityInformation.lblConsiderLoanHeader1.isVisible = true;
              frmFacilityInformation.lblConsiderLoanValue.isVisible = true;
              //frmFacilityInformation.lblConsiderLoanValue.text = result["considerLoanWithOtherBank"];
        }else{
          	frmFacilityInformation.lblConsiderLoanValue.text = "";
          	frmFacilityInformation.lblConsiderLoanHeader.isVisible = true;
            frmFacilityInformation.lblConsiderLoanHeader1.isVisible = false;
            frmFacilityInformation.lblConsiderLoanValue.isVisible = false;
        }
  
   if (result["loanWithOtherBank"] != "" && result["loanWithOtherBank"] != undefined) {
              var otherLoanData = [{
                  "lblName": kony.i18n.getLocalizedString("Select_None"),
                   "lblCode": "1",
                  "lblSegRowLine": "."
              }, {
                  "lblName": kony.i18n.getLocalizedString("Select_One_label"),
                  "lblSegRowLine": ".",
                   "lblCode": "2",
              }, {
                  "lblName": kony.i18n.getLocalizedString("Select_More_than_three_label"),
                  "lblSegRowLine": ".",
                  "lblCode": "3",
              }];
     		  for (var j = 0; j < otherLoanData.length; j++) {
            	 if(result["loanWithOtherBank"] == otherLoanData[j].lblCode){
               frmFacilityInformation.lblOtherLoanValue.text = otherLoanData[j].lblName;
                   	gblFacilityInfo["otherLoanCode"] = result["loanWithOtherBank"];
               }
              }
              frmFacilityInformation.lblOtherLoanHeader.isVisible = false;
              frmFacilityInformation.lblOtherLoanHeader1.isVisible = true;
              frmFacilityInformation.lblOtherLoanValue.isVisible = true;
            //  frmFacilityInformation.lblOtherLoanValue.text = result["loanWithOtherBank"];
        } else{
          	frmFacilityInformation.lblOtherLoanValue.text = "";
          	frmFacilityInformation.lblOtherLoanHeader.isVisible = true;
            frmFacilityInformation.lblOtherLoanHeader1.isVisible = false;
            frmFacilityInformation.lblOtherLoanValue.isVisible = false;
        }
         enableDisableFacilitySaveBtn();
       }else if(result["creditcardSavedFlag"] == "Y"){

        if (result["cardDeliveryAddress"] != "") {
            for (var j = 0; j < gblPersonalInfo.CARD_SEND_PREFERENCE.length; j++) {
                if (result["cardDeliveryAddress"] == gblPersonalInfo.CARD_SEND_PREFERENCE[j].entryCode) {
                  	if ("th_TH" == locale_app) {
                        frmFacilityInformation.lblCardDeliveryDesc.text = gblPersonalInfo.CARD_SEND_PREFERENCE[j].entryName2;
                    } else {
                        frmFacilityInformation.lblCardDeliveryDesc.text = gblPersonalInfo.CARD_SEND_PREFERENCE[j].entryName;
                    }    
                  gblFacilityInfo["cardDeliveryCode"] = result["cardDeliveryAddress"];
                }
            }
            frmFacilityInformation.lblCardDelivery.isVisible = false;
            frmFacilityInformation.lblCardDeliveryDesc.isVisible = true;
            frmFacilityInformation.lblCardDeliveryHeader.isVisible = true;
            // frmFacilityInformation.lblCardDeliveryDesc.text = result["cardDelivery"];
        }else{
          	frmFacilityInformation.lblCardDeliveryDesc.text = "";
            frmFacilityInformation.lblCardDelivery.isVisible = true;
            frmFacilityInformation.lblCardDeliveryDesc.isVisible = false;
            frmFacilityInformation.lblCardDeliveryHeader.isVisible = false;
        }
        if (result["mailPreference"] != "") {
            for (var j = 0; j < gblPersonalInfo.MAIL_PREFERENCE.length; j++) {
                if (result["mailPreference"] == gblPersonalInfo.MAIL_PREFERENCE[j].entryCode) {
                  if ("th_TH" == locale_app) {
                        frmFacilityInformation.lblMailingPreDesc.text = gblPersonalInfo.MAIL_PREFERENCE[j].entryName2;
                    } else {
                        frmFacilityInformation.lblMailingPreDesc.text = gblPersonalInfo.MAIL_PREFERENCE[j].entryName;
                    }   
                  gblFacilityInfo["mailPrefCode"] = result["mailPreference"];
                }
            }
            frmFacilityInformation.lblMailingPref.isVisible = false;
            frmFacilityInformation.lblMailingPreDesc.isVisible = true;
            frmFacilityInformation.lblMailingPreHeader.isVisible = true;
            //   frmFacilityInformation.lblMailingPreDesc.text = result["mailingPreference"];
        }else{
          	frmFacilityInformation.lblMailingPreDesc.text = "";
            frmFacilityInformation.lblMailingPref.isVisible = true;
            frmFacilityInformation.lblMailingPreDesc.isVisible = false;
            frmFacilityInformation.lblMailingPreHeader.isVisible = false;
        }
         
        

      	kony.print("22222-->"+result["paymentMethod"]);
           if (result["paymentMethod"] == "0" || result["paymentMethod"] == "1") {

            if (result["paymentMethod"] == "0") {
                frmFacilityInformation.lblPaymentMethodAccountName.isVisible = false;
                frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = false;
                frmFacilityInformation.lblCashCheque.isVisible = true;
                frmFacilityInformation.flexPaymentMethodPopup.isVisible = false;
                frmFacilityInformation.lblPaymentMethodType.isVisible = true;
                cashSaveFlag = true;
                debitSaveFlag = false;
            } else if (result["paymentMethod"] == "1") {
              	kony.print("22222-->"+result["debitAccountNo"]);
                if (result["debitAccountNo"] != "") {
                    frmFacilityInformation.lblPaymentMethodAcctNo.text = formatAccount(result["debitAccountNo"]);	
                    frmFacilityInformation.lblPaymentMethodAccountName.text = result["debitAccountName"];
                    frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = true;
                    frmFacilityInformation.lblPaymentMethodBankName.isVisible = false;
                    frmFacilityInformation.lblPaymentMethodAccountName.isVisible = true;
                    frmFacilityInformation.lblCashCheque.isVisible = false;
                    frmFacilityInformation.lblPaymentMethodType.isVisible = true;
                    debitSaveFlag = true;
                    cashSaveFlag = false;
                }
            }
            frmFacilityInformation.lblPmtMethod.isVisible = false;
            frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
            frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
            frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";
            //setDatatoPaymentMethod();
        }else{
          		frmFacilityInformation.lblPaymentMethodAcctNo.text = "";
                frmFacilityInformation.lblPaymentMethodAccountName.text = "";
                frmFacilityInformation.lblPaymentMethodAccountName.isVisible = false;
                frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = false;
                frmFacilityInformation.lblCashCheque.isVisible = false;
                frmFacilityInformation.lblPmtMethod.isVisible = true;
                frmFacilityInformation.lblPaymentMethodType.isVisible = false;
          		frmFacilityInformation.btnSavePaymentMethod.setEnabled(false);
                frmFacilityInformation.btnSavePaymentMethod.skin = "btnGreyBGNoRound";
                frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnGreyBGNoRound";
        }
          kony.print("4444-->"+result["paymentCriteria"]);
        if (result["paymentCriteria"] != "") {
            for (var j = 0; j < gblPersonalInfo.PYMT_CRITERIA.length; j++) {
                if (result["paymentCriteria"] == gblPersonalInfo.PYMT_CRITERIA[j].entryCode) {
                  	 if ("th_TH" == locale_app) {
                        frmFacilityInformation.lblcriteriavalue.text = gblPersonalInfo.PYMT_CRITERIA[j].entryName2;
                    } else {
                        frmFacilityInformation.lblcriteriavalue.text = gblPersonalInfo.PYMT_CRITERIA[j].entryName;
                    }  
                    gblFacilityInfo["paymentCriteriaCode"] = result["paymentCriteria"];
                }
            }

            frmFacilityInformation.lblPmtCreteria.isVisible = false;
            frmFacilityInformation.lblcriteria.isVisible = true;
            frmFacilityInformation.lblcriteriavalue.isVisible = true;
            //frmFacilityInformation.lblcriteriavalue.text = result["payMethodCriteria"];
        }else{
          	frmFacilityInformation.lblcriteriavalue.text = "";
            frmFacilityInformation.lblPmtCreteria.isVisible = true;
            frmFacilityInformation.lblcriteria.isVisible = false;
            frmFacilityInformation.lblcriteriavalue.isVisible = false;
             if(cashSaveFlag){
              frmFacilityInformation.flexPmtCriteria.onClick = doNothing;
              frmFacilityInformation.flexPmtCriteria.isVisible = false;
              frmFacilityInformation.lblPmtCreteria.skin = "lblGrey171"
            }else{
              frmFacilityInformation.flexPmtCriteria.onClick = onClickFacilityFlexItems
              frmFacilityInformation.flexPmtCriteria.isVisible = true;
              frmFacilityInformation.lblPmtCreteria.skin = "lblblue24px007abc"
            }
        }
		enableDisableFacilitySaveBtn();
       }
    } catch (e) {
        kony.print("e--->" + e)
    }
}

function getFacilityInfoCallback(status, result) {
    try {
        kony.print("-->" + status)
        dismissLoadingScreen();
        //frmFacilityInformation;
        debitSaveFlag = false;
        cashSaveFlag = false;
        if (status == 400) {
          	gblFacilityInfo = {};
          	if (result["opstatus"] == "0") {
            if (result["LoanInfoDS"][0]["facilitySavedFlag"] == "Y" || result["LoanInfoDS"][0]["creditcardSavedFlag"] == "Y") {
                frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
                frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
                frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";
                kony.print("------------------------>" + result["LoanInfoDS"][0])
                setDataFromGetFacilityService(result["LoanInfoDS"][0]);
            } else {
                gblFacilityInfo = {};
                // Tenue
              	frmFacilityInformation.btnNext.skin = "btnGreyBGNoRoundLoan";
                frmFacilityInformation.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
                frmFacilityInformation.flxcircle.skin = "slFbox";
                frmFacilityInformation.lblcircle.skin = "skinpiorangeLineLone";
                frmFacilityInformation.btnNext.onClick = "";
                frmFacilityInformation.lblTenure.isVisible = true;
                frmFacilityInformation.lblTenureHeader.isVisible = false;
                frmFacilityInformation.lblTenureDesc.isVisible = false;
                frmFacilityInformation.lblTenureDesc.text = "";
                frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;

                frmFacilityInformation.lblCardDelivery.isVisible = true;
                frmFacilityInformation.lblCardDeliveryDesc.isVisible = false;
                frmFacilityInformation.lblCardDeliveryHeader.isVisible = false;
                frmFacilityInformation.lblCardDeliveryDesc.text = "";

                frmFacilityInformation.lblMailingPref.isVisible = true;
                frmFacilityInformation.lblMailingPreDesc.isVisible = false;
                frmFacilityInformation.lblMailingPreHeader.isVisible = false;
                frmFacilityInformation.lblMailingPreDesc.text = "";
                frmFacilityInformation.lblPmtCreteria.isVisible = true;
                frmFacilityInformation.lblcriteria.isVisible = false;
                frmFacilityInformation.lblcriteriavalue.isVisible = false;
                frmFacilityInformation.lblcriteriavalue.text = "";

                frmFacilityInformation.lblPaymentMethodAcctNo.text = "";
                frmFacilityInformation.lblPaymentMethodAccountName.text = "";
                frmFacilityInformation.lblPaymentMethodAccountName.isVisible = false;
                frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = false;
                frmFacilityInformation.lblCashCheque.isVisible = false;
                frmFacilityInformation.lblPmtMethod.isVisible = true;
                frmFacilityInformation.flexPaymentMethodPopup.isVisible = false;
                frmFacilityInformation.lblPaymentMethodType.isVisible = false;

                frmFacilityInformation.lblLoanRecevie.isVisible = true;
                frmFacilityInformation.lblLoanRecevieHeader.isVisible = false;
                frmFacilityInformation.lblAccNo.isVisible = false;
                frmFacilityInformation.lblAccName.isVisible = false;
                frmFacilityInformation.lblAccName.text = "";
                frmFacilityInformation.lblAccNo.text = "";
                frmFacilityInformation.flxLoanRecievePopUp.isVisible = false;

                frmFacilityInformation.txtReqCreditcardAmt.text = "";
                frmFacilityInformation.lblRequestCreditLimit.isVisible = true;
                frmFacilityInformation.lblRequestCreditLimitNameHeader.isVisible = false;
                frmFacilityInformation.txtReqCreditcardAmt.isVisible = false;

                frmFacilityInformation.lblConsiderLoanHeader.isVisible = true;
                frmFacilityInformation.lblConsiderLoanHeader1.isVisible = false;
                frmFacilityInformation.lblConsiderLoanValue.isVisible = false;
                frmFacilityInformation.lblConsiderLoanValue.text = "";

                frmFacilityInformation.lblOtherLoanHeader.isVisible = true;
                frmFacilityInformation.lblOtherLoanHeader1.isVisible = false;
                frmFacilityInformation.lblOtherLoanValue.isVisible = false;
                frmFacilityInformation.lblOtherLoanValue.text = "";
              	debitSaveFlag = false;
            	cashSaveFlag = false;
            }

            // frmFacilityInformation_txtNumberField_onTextChange(frmFacilityInformation.txtReqCreditcardAmt)
            
            frmFacilityInformation.flxRequestCreditLimit.isVisible = false;
            frmFacilityInformation.flxTenure.isVisible = false;
            frmFacilityInformation.flxLoanReceive.isVisible = false;
            frmFacilityInformation.flexPaymentMethod.isVisible = false;
            frmFacilityInformation.flexPmtCriteria.isVisible = false;
            frmFacilityInformation.flexOtherloan.isVisible = false;
            frmFacilityInformation.flexConsiderLoan.isVisible = false;
            frmFacilityInformation.flexMailPref.isVisible = false;
            frmFacilityInformation.flexCardDelivery.isVisible = false;
            if (gblSelectedLoanProduct == "VP" || gblSelectedLoanProduct == "MS" || gblSelectedLoanProduct == "VM" || gblSelectedLoanProduct == "VT") { //  credit card
                frmFacilityInformation.flexPaymentMethod.isVisible = true;
              	if(cashSaveFlag){// New CR for facility
                  frmFacilityInformation.flexPmtCriteria.isVisible = false;
                }else{
                  frmFacilityInformation.flexPmtCriteria.isVisible = true;
                }
                frmFacilityInformation.flexMailPref.isVisible = true;
                frmFacilityInformation.flexCardDelivery.isVisible = true;
            } else if (gblSelectedLoanProduct == "RC01") { // TMB TMB Ready Cash
                frmFacilityInformation.flexPaymentMethod.isVisible = true;
                if(cashSaveFlag){ // New CR for facility
                  frmFacilityInformation.flexPmtCriteria.isVisible = false;
                }else{
                  frmFacilityInformation.flexPmtCriteria.isVisible = true;
                }
                frmFacilityInformation.flexOtherloan.isVisible = true;
                frmFacilityInformation.flexConsiderLoan.isVisible = true;
                frmFacilityInformation.flexMailPref.isVisible = true;
                frmFacilityInformation.flexCardDelivery.isVisible = true;
            } else if (gblSelectedLoanProduct == "C2G01") { // TMB cash to go
                frmFacilityInformation.flxRequestCreditLimit.isVisible = true;
                frmFacilityInformation.flxTenure.isVisible = true;
                frmFacilityInformation.flxLoanReceive.isVisible = true;
                frmFacilityInformation.flexPaymentMethod.isVisible = true;
                //frmFacilityInformation.flexPmtCriteria.isVisible = true;
                frmFacilityInformation.flexOtherloan.isVisible = true;
                frmFacilityInformation.flexConsiderLoan.isVisible = true;
                frmFacilityInformation.flexMailPref.isVisible = true;
            }
            frmFacilityInformation.show();
        }else{
          	showAlert(replaceAll(kony.i18n.getLocalizedString("Err_LoanApp"),"{errorCode}",result['LoanInfoDS'][0]["responseCode"]), kony.i18n.getLocalizedString("info"));
        }
    }
    } catch (e) {
        kony.print("e--->" + e)
    }
}

function onClickOfImgArrow() {
    try {
      	showLoadingScreen();
      	kony.print("Coming to onClickOfImgArrow-->" )
      	debitSaveFlag = false; // MIB-14384
        cashSaveFlag = false;  // MIB-14384
        var inputParam = {};
        inputParam["caID"] = gblCaId;
        inputParam["applicationProduct"] = gblSelectedLoanProduct;
        invokeServiceSecureAsync("LoanFacilityCreditCardInfoJavaService", inputParam, getFacilityInfoCallback);
        //invokeServiceSecureAsync("getFacilityInfo", inputParam, getFacilityInfoCallback);

    } catch (e) {}
}

function onClickFacilityFlexItems(eventobject) {
    try {
        if (gblSelectedLoanProduct == "C2G01") {
            checkAmountValue(); // This medhod will rest the amount field if the amount is empty
        }
        var selectedFlexId = eventobject["id"];
        kony.print("selectedFlexId-->" + selectedFlexId)
        if (selectedFlexId == "flxRequestCreditLimit") {
            frmFacilityInformation.lblRequestCreditLimit.isVisible = false;
            frmFacilityInformation.lblRequestCreditLimitNameHeader.isVisible = true;
            frmFacilityInformation.txtReqCreditcardAmt.isVisible = true;
            frmFacilityInformation.txtReqCreditcardAmt.setFocus(true)
            // frmFacilityInformation.flexFacitlyDataPopup.isVisible = true;
        } else if (selectedFlexId == "flxTenure") {
            frmFacilityInformation.flexFacitlyDataPopup.isVisible = true;
            gblSelectedFacilityItem = "tenure"
            setDatatoFacilityPopup();
        } else if (selectedFlexId == "flexPmtCriteria") {
            gblSelectedFacilityItem = "paymentCriteria"
            frmFacilityInformation.flexFacitlyDataPopup.isVisible = true;
            setDatatoFacilityPopup();
        } else if (selectedFlexId == "flexCardDelivery") {
            gblSelectedFacilityItem = "cardDelivery"
            setDatatoFacilityPopup();
            frmFacilityInformation.flexFacitlyDataPopup.isVisible = true;
        } else if (selectedFlexId == "flexMailPref") {
            frmFacilityInformation.flexFacitlyDataPopup.isVisible = true;
            gblSelectedFacilityItem = "mailPref"
            // setDatatoMailPrefPopup();
            setDatatoFacilityPopup();
        } else if (selectedFlexId == "flexOtherloan") {
            frmFacilityInformation.flexFacitlyDataPopup.isVisible = true;
            gblSelectedFacilityItem = "otherLoan"
            setDatatoFacilityPopup();
        } else if (selectedFlexId == "flexConsiderLoan") {
            gblSelectedFacilityItem = "considerLoan"
            setDatatoFacilityPopup();
            frmFacilityInformation.flexFacitlyDataPopup.isVisible = true;
        } else if (selectedFlexId == "flxLoanReceive") {
            setDatatoRecieveLoan();
            frmFacilityInformation.flxLoanRecievePopUp.isVisible = true;
        } else if (selectedFlexId == "flexPaymentMethod") {
           kony.print("debitSaveFlag--->"+debitSaveFlag)
           kony.print("cashSaveFlag--->"+cashSaveFlag)
            if (debitSaveFlag) {
                onClickFacilityInfoDirectDebit();
            } else if (cashSaveFlag) {
                onClickFacilityInfoCashCheque();
                frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
                frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
                frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";
            } else if (!debitSaveFlag && !cashSaveFlag) {
                debitSaveFlag = true;
                frmFacilityInformation.btnDebit.skin = "btnBlueTabBGNoRound";
                frmFacilityInformation.btnDebit.focusSkin = "btnBlueTabBGNoRound";
                frmFacilityInformation.btnCash.skin = "btnTabWhiteRightRound";
                frmFacilityInformation.btnCash.focusSkin = "btnTabWhiteRightRound";
                frmFacilityInformation.flexPaymentMethodPopupData.isVisible = true;
                frmFacilityInformation.flexPaymentMethodDirectDebit.isVisible = false;
                frmFacilityInformation.btnSavePaymentMethod.skin = "btnGreyBGNoRound";
                frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnGreyBGNoRound";
                frmFacilityInformation.btnSavePaymentMethod.setEnabled(false);
            }
            setDatatoPaymentMethod();
            frmFacilityInformation.flexPaymentMethodPopup.isVisible = true;
        }
    } catch (e) {
        kony.print("e--->" + e)
    }

}

function setActionstoLoanFacilityInfoForm() {
    try {
        debitSaveFlag = false;
        cashSaveFlag = false;
        // These for onclicks of close button on the popups
        frmFacilityInformation.btnClosePopup.onClick = btnCloseCPopUpOnClick
        frmFacilityInformation.btnClosePaymentMethodPopup.onClick = btnCloseCPopUpOnClick
        frmFacilityInformation.btnCloseLoanRecievePopup.onClick = btnCloseCPopUpOnClick
        // These for on row clicks of the data on the popup segment
        frmFacilityInformation.segFacilityDataNew.onRowClick = onRowClickFacilityPopupData
        frmFacilityInformation.segLoanRecieve.onRowClick = onRowClickFacilityPopupData
        frmFacilityInformation.segPaymentMethod.onRowClick = onRowClickFacilityPopupData
        frmFacilityInformation.btnDebit.onClick = onClickFacilityInfoDirectDebit;
        frmFacilityInformation.btnCash.onClick = onClickFacilityInfoCashCheque;
        // These for onclicks of rows on the facility info screen
        frmFacilityInformation.flxTenure.onClick = onClickFacilityFlexItems;
        frmFacilityInformation.flxRequestCreditLimit.onClick = onClickFacilityFlexItems;
        frmFacilityInformation.flexPmtCriteria.onClick = onClickFacilityFlexItems
        frmFacilityInformation.flexCardDelivery.onClick = onClickFacilityFlexItems
        frmFacilityInformation.flexMailPref.onClick = onClickFacilityFlexItems
        frmFacilityInformation.flexOtherloan.onClick = onClickFacilityFlexItems
        frmFacilityInformation.flexConsiderLoan.onClick = onClickFacilityFlexItems
        //frmFacilityInformation.btnFacilityInfoScrore.onClick = onClickFacilityFlexItems
        frmFacilityInformation.flxLoanReceive.onClick = onClickFacilityFlexItems
        frmFacilityInformation.flexPaymentMethod.onClick = onClickFacilityFlexItems
        //Fixed Income
        frmFacilityInformation.txtReqCreditcardAmt.onDone = frmFacilityInformation_txtNumberField_onDone;
        frmFacilityInformation.txtReqCreditcardAmt.onTextChange = frmFacilityInformation_txtNumberField_onTextChange;
        frmFacilityInformation.txtReqCreditcardAmt.onEndEditing = frmFacilityInformation_txtNumberField_onDone;
        frmFacilityInformation.btnBack.onClick = onCickBackFacilityInfo;
        frmFacilityInformation.btnSavePaymentMethod.onClick = btnSavePaymentMethodOnClick;
        //gblLoanType = "PL";
    } catch (e) {
        alert("ERROR " + e);
    }
}

function btnSavePaymentMethodOnClick() {
    //frmFacilityInformation.flxLoanRecievePopUp.isVisible = false;
    kony.print("skin--->" + frmFacilityInformation.btnDebit.skin)
    if (frmFacilityInformation.btnDebit.skin == "btnBlueTabBGNoRound") {
        frmFacilityInformation.lblPmtMethod.isVisible = false;
        frmFacilityInformation.lblPaymentMethodType.isVisible = true;
        frmFacilityInformation.lblPaymentMethodBankName.isVisible = false;
        frmFacilityInformation.lblPaymentMethodAccountName.isVisible = true;
        frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = true;
      	 var segData = frmFacilityInformation.segPaymentMethod.data;
            for (var i = 0; i < segData.length; i++) {
                var alreadyselectedPaymentData = frmFacilityInformation.segPaymentMethod.data[i];
                if (alreadyselectedPaymentData["imgTick"]["isVisible"] == true) {
                  	frmFacilityInformation.lblPaymentMethodAccountName.text = alreadyselectedPaymentData["lblName"];
                    frmFacilityInformation.lblPaymentMethodAcctNo.text = alreadyselectedPaymentData["lblAccountNo"];
                    break;
                }
            }
        //frmFacilityInformation.lblPaymentMethodAccountName.text = frmFacilityInformation.segPaymentMethod.selectedRowItems[0]["lblName"];
        //frmFacilityInformation.lblPaymentMethodAcctNo.text = frmFacilityInformation.segPaymentMethod.selectedRowItems[0]["lblAccountNo"];
        frmFacilityInformation.flexPaymentMethodPopup.isVisible = false;
        frmFacilityInformation.lblCashCheque.isVisible = false;
        debitSaveFlag = true;
        cashSaveFlag = false;
    } else {
        frmFacilityInformation.lblPmtMethod.isVisible = false;
        frmFacilityInformation.lblPaymentMethodType.isVisible = true;
        frmFacilityInformation.lblPaymentMethodAccountName.isVisible = false;
        frmFacilityInformation.lblPaymentMethodAcctNo.isVisible = false;
        frmFacilityInformation.lblCashCheque.isVisible = true;
        frmFacilityInformation.flexPaymentMethodPopup.isVisible = false;
        cashSaveFlag = true;
        debitSaveFlag = false;
    }
  	enableDisablePaymentCriteria();
    enableDisableFacilitySaveBtn();
    
}

function onCickBackFacilityInfo() {
    var previousForm = kony.application.getPreviousForm();
    previousForm.show();
}
var gblSelectedIndex = "";
var gblPreviousIndex = "";



function frmFacilityInformation_txtNumberField_onDone(eventObject, changedText) {
   frmFacilityInformation.flxRequestCreditLimit.skin = "lFboxLoan"
    if (eventObject.text == "") {
        frmFacilityInformation.txtReqCreditcardAmt.isVisible = false;
        frmFacilityInformation.lblRequestCreditLimit.isVisible = true;
        frmFacilityInformation.lblRequestCreditLimitNameHeader.isVisible = false;
        frmFacilityInformation.txtReqCreditcardAmt.setFocus(false);
    }
    eventObject.text = formatAmountValue(eventObject.text);
    enableDisableFacilitySaveBtn()
    // handleSaveBtnIncomeInfo();
}

function frmFacilityInformation_txtNumberField_onTextChange(eventObject, changedText) {
  	frmFacilityInformation.flxRequestCreditLimit.skin = "lFboxLoan"
    var enteredAmount = eventObject.text;
    if (isNotBlank(enteredAmount)) {
        enteredAmount = kony.string.replace(enteredAmount, ",", "");
        if (isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseFloat(enteredAmount, 10) == 0) {} else {
            eventObject.text = commaFormattedTransfer(enteredAmount);
        }
    }
    //	handleSaveBtnIncomeInfo();
}




function btnCloseCPopUpOnClick() {

    if (frmFacilityInformation.flexFacitlyDataPopup.isVisible) {
        gblSelectedFacilityItem = "";
        frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
    }
    else if (frmFacilityInformation.flxLoanRecievePopUp.isVisible) {
        frmFacilityInformation.flxLoanRecievePopUp.isVisible = false;
    } else if (frmFacilityInformation.flexPaymentMethodPopup.isVisible) {
        frmFacilityInformation.flexPaymentMethodPopup.isVisible = false;
        if (frmFacilityInformation.lblPaymentMethodAcctNo.isVisible && frmFacilityInformation.btnDebit.skin == "btnWhiteTabBGNoRound") {
            debitSaveFlag = true;
            cashSaveFlag = false;
        }
        if (frmFacilityInformation.lblCashCheque.isVisible && frmFacilityInformation.btnCash.skin == "btnTabWhiteRightRound") {
            frmFacilityInformation.lblPaymentMethodAcctNo.text = "";
            cashSaveFlag = true;
            debitSaveFlag = false;
        }
    }
}

function onRowClickFacilityPopupData(eventObject) {
    kony.print("eventObject--->" + eventObject)

    try {
        if (frmFacilityInformation.flexFacitlyDataPopup.isVisible) {
            if (gblSelectedFacilityItem == "tenure") {
                frmFacilityInformation.lblTenure.isVisible = false;
                frmFacilityInformation.lblTenureHeader.isVisible = true;
                frmFacilityInformation.lblTenureDesc.isVisible = true;
                frmFacilityInformation.lblTenureDesc.text = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblName"];
                frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
                gblFacilityInfo["tenureCode"] = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"];
                gblSelectedFacilityItem = "";
            } else if (gblSelectedFacilityItem == "mailPref") {
                frmFacilityInformation.lblMailingPref.isVisible = false;
                frmFacilityInformation.lblMailingPreDesc.isVisible = true;
                frmFacilityInformation.lblMailingPreHeader.isVisible = true;
                frmFacilityInformation.lblMailingPreDesc.text = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblName"];
                frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
                gblFacilityInfo["mailPrefCode"] = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"]
                gblSelectedFacilityItem = "";
                kony.print("entry code mail prefer --->" + frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"])
            } else if (gblSelectedFacilityItem == "otherLoan") {
                frmFacilityInformation.lblOtherLoanHeader.isVisible = false;
                frmFacilityInformation.lblOtherLoanHeader1.isVisible = true;
                frmFacilityInformation.lblOtherLoanValue.isVisible = true;
                frmFacilityInformation.lblOtherLoanValue.text = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblName"];
                frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
                gblFacilityInfo["otherLoanCode"] = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"];
                gblSelectedFacilityItem = "";
            } else if (gblSelectedFacilityItem == "considerLoan") {
                frmFacilityInformation.lblConsiderLoanHeader.isVisible = false;
                frmFacilityInformation.lblConsiderLoanHeader1.isVisible = true;
                frmFacilityInformation.lblConsiderLoanValue.isVisible = true;
                frmFacilityInformation.lblConsiderLoanValue.text = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblName"];
                frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
                gblFacilityInfo["considerLoanCode"] = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"]
                gblSelectedFacilityItem = "";
            } else if (gblSelectedFacilityItem == "paymentCriteria") {
                frmFacilityInformation.lblPmtCreteria.isVisible = false;
                frmFacilityInformation.lblcriteria.isVisible = true;
                frmFacilityInformation.lblcriteriavalue.isVisible = true;
                frmFacilityInformation.lblcriteriavalue.text = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblName"];
                frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
                gblFacilityInfo["paymentCriteriaCode"] = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"]
            } else if (gblSelectedFacilityItem == "paymentCriteria") {
                frmFacilityInformation.lblCardDelivery.isVisible = false;
                frmFacilityInformation.lblCardDeliveryDesc.isVisible = true;
                frmFacilityInformation.lblCardDeliveryHeader.isVisible = true;
                frmFacilityInformation.lblCardDeliveryDesc.text = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblName"];
                kony.print("entry code card delivery--->" + frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"])
                frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
                gblFacilityInfo["cardDeliveryCode"] = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"]
            }else if (gblSelectedFacilityItem == "cardDelivery") {
                   frmFacilityInformation.lblCardDelivery.isVisible = false;
                   frmFacilityInformation.lblCardDeliveryDesc.isVisible = true;
                   frmFacilityInformation.lblCardDeliveryHeader.isVisible = true;
                   frmFacilityInformation.lblCardDeliveryDesc.text = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblName"];
                   kony.print("entry code card delivery--->"+frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"])
                   frmFacilityInformation.flexFacitlyDataPopup.isVisible = false;
                   gblFacilityInfo["cardDeliveryCode"] = frmFacilityInformation.segFacilityDataNew.selectedRowItems[0]["lblCode"]
            }

        }
        else if (frmFacilityInformation.flexPaymentMethodPopup.isVisible) {
            kony.print("skin 111-->" + frmFacilityInformation.btnSavePaymentMethod.skin)
            var segData = frmFacilityInformation.segPaymentMethod.data;
            for (var i = 0; i < segData.length; i++) {
                var alreadyselectedPaymentData = frmFacilityInformation.segPaymentMethod.data[i];
                if (alreadyselectedPaymentData["flexRow"]["skin"] == "flexGreyBGLoan") {
                    kony.print("--->" + alreadyselectedPaymentData["lblName"]["text"])
                    alreadyselectedPaymentData["imgTick"] = {
                        isVisible: false
                    };
                    alreadyselectedPaymentData["flexRow"] = {
                        "skin": "lFboxSegBrd0pxLoan"
                    };
                    frmFacilityInformation.segPaymentMethod.setDataAt(alreadyselectedPaymentData, i);
                    break;
                }
            }
            var selectedPaymentMethod = frmFacilityInformation.segPaymentMethod.selectedIndex[1];
            kony.print("--->" + selectedPaymentMethod)
            var selectedPaymentData = frmFacilityInformation.segPaymentMethod.data[selectedPaymentMethod];
            selectedPaymentData["imgTick"] = {
                isVisible: true
            };
            selectedPaymentData["flexRow"] = {
                "skin": "flexGreyBGLoan"
            };
            frmFacilityInformation.segPaymentMethod.setDataAt(selectedPaymentData, selectedPaymentMethod);
            if (frmFacilityInformation.btnSavePaymentMethod.skin != "btnBlueBGNoRound") {
                frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
                frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
                frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";
            }
        } else if (frmFacilityInformation.flxLoanRecievePopUp.isVisible) {
            frmFacilityInformation.lblLoanRecevie.isVisible = false;
            frmFacilityInformation.lblLoanRecevieHeader.isVisible = true;
            frmFacilityInformation.lblBankName.isVisible = false;
            frmFacilityInformation.lblAccNo.isVisible = true;
            frmFacilityInformation.lblAccName.isVisible = true;
            frmFacilityInformation.lblBankName.text = "TMB Bank"
            frmFacilityInformation.lblAccName.text = frmFacilityInformation.segLoanRecieve.selectedRowItems[0]["lblName"];
            frmFacilityInformation.lblAccNo.text = frmFacilityInformation.segLoanRecieve.selectedRowItems[0]["lblAccountNo"];
            frmFacilityInformation.flxLoanRecievePopUp.isVisible = false;
        }
    } catch (e) {
        //  alert(e);
    }
    enableDisableFacilitySaveBtn();
}


function togleCheck() {
    frmFacilityInformation.flxcircleAccept.skin = "slFboxgreenBGcircle";
    frmFacilityInformation.imgChkAccept.skin = "lblskinpiGreencheckLineLone";
    frmFacilityInformation.imgchkNotAccept.skin = "lblskinpigrayLineLone";
    enableDisableFacilitySaveBtn();
}

function togleUnCheck() {
    frmFacilityInformation.imgChkAccept.skin = "lblskinpigrayLineLone";
    frmFacilityInformation.imgchkNotAccept.skin = "lblskinpiGreencheckLineLone";
    frmFacilityInformation.flxcircleNotAccept.skin = "slFboxgreenBGcircle";
    enableDisableFacilitySaveBtn();
}

function enableDisableFacilitySaveBtn() {
    var count = 0;
    if (gblSelectedLoanProduct == "VP" || gblSelectedLoanProduct == "MS" || gblSelectedLoanProduct == "VM" || gblSelectedLoanProduct == "VT") { //  credit card
        var arryList = ["lblMailingPreDesc", "lblCardDeliveryDesc", "lblcriteriavalue", "lblCashCheque", "lblPaymentMethodAcctNo"];
        for (var i = 0; i < arryList.length; i++) {
            var val = frmFacilityInformation[arryList[i]].isVisible;
            if (val === true) {
                count = count + 1;
            }
        }
        kony.print("credit card count--->" + count);
      	var limitValue = 4;
       if(frmFacilityInformation.lblPmtCreteria.skin == "lblGrey171"){
         limitValue = 3;
       }
        if (count >= limitValue) {
            frmFacilityInformation.btnNext.skin = "btnBlue28pxLoan2";
            frmFacilityInformation.btnNext.focusSkin = "btnBlue28pxLoan2";
            frmFacilityInformation.lblcircle.skin = "skinpiGreenLineLone";
            frmFacilityInformation.flxcircle.skin = "flxGreenCircleLoan";
            frmFacilityInformation.btnNext.onClick = navigateToApllicationFromFacility;
        } else {
            frmFacilityInformation.btnNext.skin = "btnGreyBGNoRoundLoan";
            frmFacilityInformation.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
            frmFacilityInformation.flxcircle.skin = "slFbox";
            frmFacilityInformation.lblcircle.skin = "skinpiorangeLineLone";
            frmFacilityInformation.btnNext.onClick = "";
        }
    } else if (gblSelectedLoanProduct == "RC01") { // TMB TMB Ready Cash
        var arryList = ["lblcriteriavalue", "lblOtherLoanValue", "lblConsiderLoanValue", "lblMailingPreDesc", "lblCardDeliveryDesc", "lblCashCheque", "lblPaymentMethodAcctNo"];
        for (var i = 0; i < arryList.length; i++) {
            var val = frmFacilityInformation[arryList[i]].isVisible;
            if (val === true) {
                count = count + 1;
            }
        }
        kony.print("ready count--->" + count);
        var limitValue = 6;
         if(frmFacilityInformation.lblPmtCreteria.skin == "lblGrey171"){
           limitValue = 5;
         }
        if (count >= limitValue) {
            frmFacilityInformation.btnNext.skin = "btnBlue28pxLoan2";
            frmFacilityInformation.btnNext.focusSkin = "btnBlue28pxLoan2";
            frmFacilityInformation.lblcircle.skin = "skinpiGreenLineLone";
            frmFacilityInformation.flxcircle.skin = "flxGreenCircleLoan";
            frmFacilityInformation.btnNext.onClick = navigateToApllicationFromFacility;
        } else {
            frmFacilityInformation.btnNext.skin = "btnGreyBGNoRoundLoan";
            frmFacilityInformation.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
            frmFacilityInformation.flxcircle.skin = "slFbox";
            frmFacilityInformation.lblcircle.skin = "skinpiorangeLineLone";
            frmFacilityInformation.btnNext.onClick = "";
        }
    } else if (gblSelectedLoanProduct == "C2G01") { // TMB cash to go
        if (frmFacilityInformation.btnCash.skin == "") {

        }
        var arryList = ["txtReqCreditcardAmt", "lblTenureDesc", "lblOtherLoanValue", "lblConsiderLoanValue", "lblMailingPreDesc", "lblCashCheque", "lblPaymentMethodAcctNo","lblAccNo"];
        for (var i = 0; i < arryList.length; i++) {
            var val = frmFacilityInformation[arryList[i]].isVisible;
            if (val === true) {
                count = count + 1;
            }
        }
        kony.print("cash to go count--->" + count);
        if (count >= 7) {
            frmFacilityInformation.btnNext.skin = "btnBlue28pxLoan2";
            frmFacilityInformation.btnNext.focusSkin = "btnBlue28pxLoan2";
            frmFacilityInformation.lblcircle.skin = "skinpiGreenLineLone";
            frmFacilityInformation.flxcircle.skin = "flxGreenCircleLoan";
            frmFacilityInformation.btnNext.onClick = navigateToApllicationFromFacility;
        } else {
            frmFacilityInformation.btnNext.skin = "btnGreyBGNoRoundLoan";
            frmFacilityInformation.btnNext.focusSkin = "btnGreyBGNoRoundLoan";
            frmFacilityInformation.flxcircle.skin = "slFbox";
            frmFacilityInformation.lblcircle.skin = "skinpiorangeLineLone";
            frmFacilityInformation.btnNext.onClick = "";
        }
    }
}

function navigateToApllicationFromFacility() {
    //frmLoanApplicationForm.lblCircle4.skin = "skinpiGreenLineLone";
    //frmLoanApplicationForm.flxlblcircle4.skin = "flxGreenCircleLoan";
    showLoadingScreen();
    saveFI = true;
    var input_param = {};
 	var serviceName = "";
    // input_param["facid"] = "2018032201017652";
    input_param["caId"] = gblCaId;
    input_param["productCode"] = gblSelectedLoanProduct;
    input_param["employmentStatus"] = "01";
   
    if (gblSelectedLoanProduct == "VP" || gblSelectedLoanProduct == "MS" || gblSelectedLoanProduct == "VM" || gblSelectedLoanProduct == "VT") { //  credit card
          serviceName = "updateCreditCardInfo";
      	 var paymentMethodValue = 0;
      	 kony.print("payment method -->"+frmFacilityInformation.lblPaymentMethodAcctNo.isVisible)
       	 if (frmFacilityInformation.lblPaymentMethodAcctNo.isVisible) {
            paymentMethodValue = 1;
            input_param["debitAccountName"] = frmFacilityInformation.lblPaymentMethodAccountName.text;
            input_param["debitAccountNo"] = replaceAll(frmFacilityInformation.lblPaymentMethodAcctNo.text, "-", "");
        }
      	input_param["paymentMethod"] = paymentMethodValue;
        if(frmFacilityInformation.lblcriteriavalue.isVisible){ // MIB 14349
       	 input_param["paymentCriteria"] = gblFacilityInfo["paymentCriteriaCode"];
        }else {
          input_param["paymentCriteria"] = "";
        }
        input_param["mailPreference"] = gblFacilityInfo["mailPrefCode"];
        input_param["cardDeliveryAddress"] = gblFacilityInfo["cardDeliveryCode"];
      	input_param["creditcardSavedFlag"] = "Y";
    } else if (gblSelectedLoanProduct == "RC01") { // TMB TMB Ready Cash
      	serviceName = "updateFacilityInfo";
      	//input_param["caCampaignCode"] = "BB00";
      //	input_param["facilityCode"] = " RC";
         var paymentMethodValue = 0;
        if (frmFacilityInformation.lblPaymentMethodAcctNo.isVisible) {
            paymentMethodValue = 1;
            input_param["paymentAccountName"] = frmFacilityInformation.lblPaymentMethodAccountName.text;
            input_param["paymentAccountNo"] = replaceAll(frmFacilityInformation.lblPaymentMethodAcctNo.text, "-", "");
        }
      	input_param["paymentMethod"] = paymentMethodValue;
        //input_param["paymentAccountName"] = frmFacilityInformation.lblPaymentMethodAccountName.text;
        //input_param["paymentAccountNo"] = replaceAll(frmFacilityInformation.lblPaymentMethodAcctNo.text, "-", "");
        if(frmFacilityInformation.lblcriteriavalue.isVisible){ // MIB 14349
        	input_param["payMethodCriteria"] = gblFacilityInfo["paymentCriteriaCode"];  
        }else {
          input_param["paymentCriteria"] = "";
        }
        input_param["mailingPreference"] = gblFacilityInfo["mailPrefCode"];
        input_param["cardDelivery"] = gblFacilityInfo["cardDeliveryCode"];
      	//input_param["disburstAccountName"] = frmFacilityInformation.lblAccName.text;
         // input_param["disburstAccountNo"] = replaceAll(frmFacilityInformation.lblAccNo.text, "-", "");
      	input_param["considerLoanWithOtherBank"] = gblFacilityInfo["considerLoanCode"];
      	input_param["loanWithOtherBank"] = gblFacilityInfo["otherLoanCode"];
      	input_param["facilitySavedFlag"] = "Y";
    } else if (gblSelectedLoanProduct == "C2G01") { // TMB cash to go
      	serviceName = "updateFacilityInfo";
        kony.print("gblPersonalInfo empStatus->"+gblPersonalInfo.empStatus)
        if (isNotBlank(gblPersonalInfo.empStatus)) {
          if(gblPersonalInfo.empStatus == "01"){
            input_param["caCampaignCode"] = "DL00";
          }else if(gblPersonalInfo.empStatus == "02"){
            input_param["caCampaignCode"] = "EA00";
          }
        }
        input_param["facilityCode"] = " C2G";
        kony.print("Amount applied :"+frmFacilityInformation.txtReqCreditcardAmt.text)
        input_param["limitApplied"] = replaceAll(frmFacilityInformation.txtReqCreditcardAmt.text, ",", "");
        if(parseInt(input_param["limitApplied"]).toFixed(2) > 1000000 ){
          dismissLoadingScreen()  ;
          showAlertWithCallBack(kony.i18n.getLocalizedString("MaxRequestCreditLimit"), kony.i18n.getLocalizedString("info"), setFocusToFacilityAmount)
            return false;
    		}else{
             frmFacilityInformation.flxRequestCreditLimit.skin = "lFboxLoan"
           }
      	input_param["tenure"] = gblFacilityInfo["tenureCode"];
        var paymentMethodValue = 0;
        if (frmFacilityInformation.lblPaymentMethodAcctNo.isVisible) {
            paymentMethodValue = 1;
            input_param["paymentAccountName"] = frmFacilityInformation.lblPaymentMethodAccountName.text;
            input_param["paymentAccountNo"] = replaceAll(frmFacilityInformation.lblPaymentMethodAcctNo.text, "-", "");
        }
        input_param["paymentMethod"] = paymentMethodValue;
        input_param["disburstAccountName"] = frmFacilityInformation.lblAccName.text;
        input_param["disburstAccountNo"] = replaceAll(frmFacilityInformation.lblAccNo.text, "-", "");
        input_param["considerLoanWithOtherBank"] = gblFacilityInfo["considerLoanCode"];
      	input_param["loanWithOtherBank"] = gblFacilityInfo["otherLoanCode"];
        input_param["mailingPreference"] = gblFacilityInfo["mailPrefCode"];
      	input_param["facilitySavedFlag"] = "Y";
    }
  		kony.print("input_param in the update ---->" + input_param)
  	   invokeServiceSecureAsync(serviceName, input_param, saveFacilityInfoCallback);
  
}


function saveFacilityInfoCallback(status, result) {
    dismissLoadingScreen();
    if (status == 400) {
      	if (result["opstatus"] == 0) {
      	gblPersonalInfo.saveFacility = true;
        frmLoanApplicationForm.show();
    }else{
      showAlert(replaceAll(kony.i18n.getLocalizedString("Err_LoanApp"),"{errorCode}",result["responseCode"]), kony.i18n.getLocalizedString("info"));
    }
    }
}

function setFocusToFacilityAmount(){
  frmFacilityInformation.flxRequestCreditLimit.skin = "flexRedBorder1px"
  frmFacilityInformation.txtReqCreditcardAmt.setFocus(true); 
  return false;
}
function initFacilityForm() {
    setActionstoLoanFacilityInfoForm();
    frmFacilityInformation.preShow = preshowOfFacilityForm;
    frmFacilityInformation.onDeviceBack = doNothing;
}

function preshowOfFacilityForm() {
    try {
      	var locale_app = kony.i18n.getCurrentLocale();
        if (locale_app == "th_TH"){
          frmFacilityInformation.lblConsiderLoanHeader.height = "50dp"
          frmFacilityInformation.lblOtherLoanHeader.height = "50dp"  
        }
        
    } catch (e) {
        kony.print("Exception  " + e);
    }
}

function setDatatoRecieveLoan() {
    showLoadingScreen();
    var inputParam = {}
    if (gblRecievePaymentMethodResponse == "") {

        invokeServiceSecureAsync("GetLoanReceiveMethod", inputParam, GetLoanPaymentMethodCallback);
    } else {
        GetLoanPaymentMethodCallback(400, gblRecievePaymentMethodResponse);
    }
}

function GetLoanPaymentMethodCallback(status, result) {
    dismissLoadingScreen();
    if (status == 400) {
	if (result["opstatus"] == 0) {
        var facilityPaymentMethodData = [];
        var locale_app = kony.i18n.getCurrentLocale();
        var showTick = false;
        var flxSkin = "lFboxSegBrd0pxLoan";
      	if (result["custAcctRec"] != undefined) {
        if (result["custAcctRec"].length > 0) {
            for (var j = 0; j < result["custAcctRec"].length; j++) {
                gblRecievePaymentMethodResponse = result;
                var obj = "";
                showTick = false;
                flxSkin = "lFboxSegBrd0pxLoan";
                if (frmFacilityInformation.lblAccNo.text != "") {
                    if (frmFacilityInformation.lblAccNo.text == formatAccount(result["custAcctRec"][j].accId)) {
                        showTick = true;
                        flxSkin = "flexGreyBGLoan";
                    }

                }
                if ((result["custAcctRec"][j]["acctNickName"]) == null || (result["custAcctRec"][j]["acctNickName"]) == undefined) {
                    if (locale_app == "th_TH")
                        accountName = result["custAcctRec"][j]["defaultCurrentNickNameTH"];
                    else
                        accountName = result["custAcctRec"][j]["defaultCurrentNickNameEN"];
                } else {

                    accountName = result["custAcctRec"][j]["acctNickName"];
                }
                obj = {
                    "lblName": accountName,
                    "lblAccountNo": formatAccount(result["custAcctRec"][j].accId),
                    "lblSegRowLine": ".",
                    "imgTick": {
                        isVisible: showTick
                    },
                    "flexRow": {
                        "skin": flxSkin
                    }
                };
                facilityPaymentMethodData.push(obj);
            }
        }else{
           if (frmFacilityInformation.flxLoanRecievePopUp.isVisible) {
                showAlert(kony.i18n.getLocalizedString("LoanReceive_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
            } else if (frmFacilityInformation.flexPaymentMethodPopup.isVisible) {
                showAlert(kony.i18n.getLocalizedString("LoanPayment_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
            }
        }
    }else{
           if (frmFacilityInformation.flxLoanRecievePopUp.isVisible) {
                showAlert(kony.i18n.getLocalizedString("LoanReceive_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
            } else if (frmFacilityInformation.flexPaymentMethodPopup.isVisible) {
                showAlert(kony.i18n.getLocalizedString("LoanPayment_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
            }
        }
        //gblGetPaymentMethodResponse = result;
        frmFacilityInformation.segLoanRecieve.setData(facilityPaymentMethodData);
    }else{
      showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
      debitSaveFlag = false;
    }
    }
}



function setDatatoPaymentMethod() {

    var input_param = {}
    if (gblGetPaymentMethodResponse == "") {

        invokeServiceSecureAsync("GetLoanPaymentMethod", input_param, setDatatoPaymentMethodCallback);
    } else {
        setDatatoPaymentMethodCallback(400, gblGetPaymentMethodResponse);
    }
}


function setDatatoPaymentMethodCallback(status, result) {
    dismissLoadingScreen();
    if (status == 400) {
	if (result["opstatus"] == 0) {
        var facilityPaymentMethodData = [];
        frmFacilityInformation.segPaymentMethod.removeAll();
        var locale_app = kony.i18n.getCurrentLocale();
        var showTick = false;
        var flxSkin = "lFboxSegBrd0pxLoan";
        var atleastOneSlected = false;
      	if(!cashSaveFlag){
           frmFacilityInformation.btnSavePaymentMethod.skin = "btnGreyBGNoRound";
          frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnGreyBGNoRound";
          frmFacilityInformation.btnSavePaymentMethod.setEnabled(false);
        }
       
      	if (result["custAcctRec"] != undefined) {
        if (result["custAcctRec"].length > 0) {
            for (var j = 0; j < result["custAcctRec"].length; j++) {
                gblGetPaymentMethodResponse = result;
                var obj = "";
                showTick = false;
                flxSkin = "lFboxSegBrd0pxLoan";
                //frmFacilityInformation.btnSavePaymentMethod.setEnabled(false);

                if (frmFacilityInformation.lblPaymentMethodAcctNo.isVisible && frmFacilityInformation.lblPaymentMethodAcctNo.text == formatAccount(result["custAcctRec"][j].accId)) {
                    showTick = true;
                    flxSkin = "flexGreyBGLoan";
                    atleastOneSlected = true;
                    frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
                    frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
                    frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";

                }
                if ((result["custAcctRec"][j]["acctNickName"]) == null || (result["custAcctRec"][j]["acctNickName"]) == undefined) {
                    if (locale_app == "th_TH")
                        accountName = result["custAcctRec"][j]["defaultCurrentNickNameTH"];
                    else
                        accountName = result["custAcctRec"][j]["defaultCurrentNickNameEN"];
                } else {

                    accountName = result["custAcctRec"][j]["acctNickName"];
                }
                obj = {
                    "lblName": accountName,
                    "lblAccountNo": formatAccount(result["custAcctRec"][j].accId),
                    "lblSegRowLine": ".",
                    "imgTick": {
                        isVisible: showTick
                    },
                    "flexRow": {
                        "skin": flxSkin
                    }
                };
                facilityPaymentMethodData.push(obj);
            }
        } else {
            if (frmFacilityInformation.flxLoanRecievePopUp.isVisible) {
               // showAlertWithCallBack(kony.i18n.getLocalizedString("LoanReceive_msgNoEligibleAcc"), "Info", btnCloseCPopUpOnClick);
				showAlert(kony.i18n.getLocalizedString("LoanReceive_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
            } else if (frmFacilityInformation.flexPaymentMethodPopup.isVisible) {
               // showAlertWithCallBack(kony.i18n.getLocalizedString("LoanPayment_msgNoEligibleAcc"), "Info", btnCloseCPopUpOnClick);
			    if(!cashSaveFlag)
				showAlert(kony.i18n.getLocalizedString("LoanPayment_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
              	debitSaveFlag = false;
            }

        }
    }else{
       if (frmFacilityInformation.flxLoanRecievePopUp.isVisible) {
                showAlert(kony.i18n.getLocalizedString("LoanReceive_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
            } else if (frmFacilityInformation.flexPaymentMethodPopup.isVisible) {
                if(!cashSaveFlag)
				showAlert(kony.i18n.getLocalizedString("LoanPayment_msgNoEligibleAcc"), kony.i18n.getLocalizedString("info"));
              	debitSaveFlag = false;
            }
    }
        frmFacilityInformation.segPaymentMethod.setData(facilityPaymentMethodData);
        //  gblGetPaymentMethodResponse = result;
    }else{
			if(!cashSaveFlag)
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
      		debitSaveFlag = false;
    }

}
}

function setDatatoFacilityPopup() {
    try {
        kony.print("set data to tenure-->")
        frmFacilityInformation.segFacilityDataNew.removeAll();
        var selectedFacilityData = [];
        if (gblSelectedFacilityItem == "tenure") {
            frmFacilityInformation.lblPopupHeader.text = kony.i18n.getLocalizedString("Tenure_label");
            selectedFacilityData = gblPersonalInfo.TENURE;
        } else if (gblSelectedFacilityItem == "mailPref") {
            frmFacilityInformation.lblPopupHeader.text = kony.i18n.getLocalizedString("Mailing_Preference_label");
            var mailPrefData = gblPersonalInfo.MAIL_PREFERENCE;
            if(mailPrefData != undefined){
              	if(mailPrefData.length >0){
                if(isCreditCardProductSelected()){
                  for(var i=0;i<mailPrefData.length;i++){
                    if(mailPrefData[i].entrySource.indexOf("CC") > -1){
                        selectedFacilityData.push(mailPrefData[i]);
                    }
                }
                }else {
                  selectedFacilityData = gblPersonalInfo.MAIL_PREFERENCE;
                }
            }
            }
          
           // selectedFacilityData = gblPersonalInfo.MAIL_PREFERENCE;
        } else if (gblSelectedFacilityItem == "paymentCriteria") {
            frmFacilityInformation.lblPopupHeader.text = kony.i18n.getLocalizedString("Payment_Criteria_label");
            var paymentCriteriaData = gblPersonalInfo.PYMT_CRITERIA;
           if(paymentCriteriaData != undefined){
              	if(paymentCriteriaData.length >0){
            if(isCreditCardProductSelected()){
              	for(var i=0;i<paymentCriteriaData.length;i++){
                    if(paymentCriteriaData[i].entrySource.indexOf("CC") > -1){
                        selectedFacilityData.push(paymentCriteriaData[i]);
                    }
                }
            }else if(gblSelectedLoanProduct == "RC01"){
              for(var i=0;i<paymentCriteriaData.length;i++){
                    if(paymentCriteriaData[i].entrySource.indexOf("RC") > -1){
                        selectedFacilityData.push(paymentCriteriaData[i]);
                    }
                }
                }
            }
        }
           // selectedFacilityData = gblPersonalInfo.PYMT_CRITERIA;
        } else if (gblSelectedFacilityItem == "cardDelivery") {
            frmFacilityInformation.lblPopupHeader.text = kony.i18n.getLocalizedString("Card_Delivery_label");
            selectedFacilityData = gblPersonalInfo.CARD_SEND_PREFERENCE;
        } else if (gblSelectedFacilityItem == "otherLoan") {
            frmFacilityInformation.lblPopupHeader.text = kony.i18n.getLocalizedString("Other_Loan_Title")
            selectedFacilityData = [{
                "entryName": kony.i18n.getLocalizedString("Select_None"),
                "entryName2": kony.i18n.getLocalizedString("Select_None"),
                "entryCode": "1",
                "lblSegRowLine": "."
            }, {
                "entryName": kony.i18n.getLocalizedString("Select_One_label"),
                "entryName2": kony.i18n.getLocalizedString("Select_One_label"),
                "lblSegRowLine": ".",
                "entryCode": "2",
            }, {
                "entryName": kony.i18n.getLocalizedString("Select_More_than_three_label"),
                "entryName2": kony.i18n.getLocalizedString("Select_More_than_three_label"),
                "lblSegRowLine": ".",
                "entryCode": "3",
            }];
        } else if (gblSelectedFacilityItem == "considerLoan") {
            frmFacilityInformation.lblPopupHeader.text = kony.i18n.getLocalizedString("Consider_Loan_Title")
            selectedFacilityData = [{
                "entryName": kony.i18n.getLocalizedString("Select_None"),
                "entryName2": kony.i18n.getLocalizedString("Select_None"),
                "lblSegRowLine": ".",
                "entryCode": "1"
            }, {
                "entryName": kony.i18n.getLocalizedString("Select_One_label"),
                "entryName2": kony.i18n.getLocalizedString("Select_One_label"),
                "lblSegRowLine": ".",
                "entryCode": "2"
            }, {
                "entryName": kony.i18n.getLocalizedString("Select_More_than_three_label"),
                "entryName2": kony.i18n.getLocalizedString("Select_More_than_three_label"),
                "lblSegRowLine": ".",
                "entryCode": "3"
            }];
        }


        var segmentData = [];
        var showTick = false;
        var flxSkin = "lFboxSegBrd0pxLoan";
        for (var j = 0; j < selectedFacilityData.length; j++) {
            showTick = false;
            flxSkin = "lFboxSegBrd0pxLoan";
            if (gblSelectedFacilityItem == "tenure" && frmFacilityInformation.lblTenureDesc.text != "") {
                if (frmFacilityInformation.lblTenureDesc.text == selectedFacilityData[j].entryName) {
                    showTick = true;
                    flxSkin = "flexGreyBGLoan";
                }
            } else if (gblSelectedFacilityItem == "mailPref" && frmFacilityInformation.lblMailingPreDesc.isVisible) {
                if (frmFacilityInformation.lblMailingPreDesc.text == selectedFacilityData[j].entryName2 || frmFacilityInformation.lblMailingPreDesc.text == selectedFacilityData[j].entryName) {
                    showTick = true;
                    flxSkin = "flexGreyBGLoan";
                }

            } else if (gblSelectedFacilityItem == "otherLoan" && frmFacilityInformation.lblOtherLoanValue.isVisible) {
                if (frmFacilityInformation.lblOtherLoanValue.text == selectedFacilityData[j].entryName) {
                    showTick = true;
                    flxSkin = "flexGreyBGLoan";
                }
            } else if (gblSelectedFacilityItem == "considerLoan" && frmFacilityInformation.lblConsiderLoanValue.isVisible) {
                if (frmFacilityInformation.lblConsiderLoanValue.text == selectedFacilityData[j].entryName) {
                    showTick = true;
                    flxSkin = "flexGreyBGLoan";

                }
            } else if (gblSelectedFacilityItem == "paymentCriteria" && frmFacilityInformation.lblcriteriavalue.isVisible) {
                if (frmFacilityInformation.lblcriteriavalue.text == selectedFacilityData[j].entryName2 || frmFacilityInformation.lblcriteriavalue.text == selectedFacilityData[j].entryName) {
                    showTick = true;
                    flxSkin = "flexGreyBGLoan";
                }

            } else if (gblSelectedFacilityItem == "cardDelivery" && frmFacilityInformation.lblCardDeliveryDesc.isVisible) {
                if (frmFacilityInformation.lblCardDeliveryDesc.text == selectedFacilityData[j].entryName2 || frmFacilityInformation.lblCardDeliveryDesc.text == selectedFacilityData[j].entryName) {
                    showTick = true;
                    flxSkin = "flexGreyBGLoan";
                }

            }
            var obj = "";
            var entryName = "";
            if ("th_TH" == kony.i18n.getCurrentLocale()) {
                entryName = selectedFacilityData[j].entryName2;
            } else {
                entryName = selectedFacilityData[j].entryName;
            }
            obj = {
                "lblName": entryName,
                "lblCode": selectedFacilityData[j].entryCode,
                "lblSegRowLine": ".",
                "imgTick": {
                    isVisible: showTick
                },
                "flexRow": {
                    "skin": flxSkin
                }
            };
            segmentData.push(obj);
        }
        frmFacilityInformation.segFacilityDataNew.setData(segmentData);
        // frmLoanIncomeInfo.incomeBankList.onRowClick = frmLoanIncomeInfo_fxbankPopupseg_incomeBankList_selectedRowItems;
    } catch (e) {
        kony.print("e--->" + e)
    }
}



function preShowOfPaymentMethodPopup() {
    try {
        frmFacilityInformation.Label0ad0e5b1ef76740.text = kony.i18n.getLocalizedString("Payment_Method_Title");
        frmFacilityInformation.btnDebit.text = kony.i18n.getLocalizedString("DirectDebit_tab");
        frmFacilityInformation.btnCash.text = kony.i18n.getLocalizedString("CashCheque_tab");
        frmFacilityInformation.lbldebit.text = kony.i18n.getLocalizedString("DirectDebitAccountno_label");
        frmFacilityInformation.lbldebitname.text = kony.i18n.getLocalizedString("DirectDebitAccountName_label");
        frmFacilityInformation.lblDebitNo.text = kony.i18n.getLocalizedString("DirectDebitAccountno_label");
        frmFacilityInformation.lbldebitnametitle.text = kony.i18n.getLocalizedString("DirectDebitAccountName_label");
    } catch (e) {
        kony.print("Exception  " + e);
    }
}

function preShowOfPaymentCriteriaPopup() {
    try {
        frmFacilityInformation.CopyLabel0b78bb7b2272143.text = kony.i18n.getLocalizedString("Payment_Criteria_label");
        frmFacilityInformation.CopybtnDebit0cad0381dc6114f.text = kony.i18n.getLocalizedString("Full_Amount_Label");
        frmFacilityInformation.btnSaveCriteria.text = kony.i18n.getLocalizedString("MF_RDM_30");
    } catch (e) {
        kony.print("Exception  " + e);
    }
}


function onClickFacilityInfoCashCheque() {
    frmFacilityInformation.btnDebit.skin = "btnWhiteTabBGNoRound";
    frmFacilityInformation.btnDebit.focusSkin = "btnWhiteTabBGNoRound";
    frmFacilityInformation.btnCash.skin = "btnTabBlueRightRound";
    frmFacilityInformation.btnCash.focusSkin = "btnTabBlueRightRound";
    frmFacilityInformation.flexPaymentMethodPopupData.isVisible = false;
    frmFacilityInformation.flexPaymentMethodDirectDebit.isVisible = true;
    //frmFacilityInformation.lblPaymentMethodAcctNo.text = "";
    //setDatatoPaymentMethod();
    if (frmFacilityInformation.btnSavePaymentMethod.skin != "btnBlueBGNoRound") {
        frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
        frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
        frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";
    }
    cashSaveFlag = true;
    debitSaveFlag = false;
}

function onClickFacilityInfoDirectDebit() {
    frmFacilityInformation.btnDebit.skin = "btnBlueTabBGNoRound";
    frmFacilityInformation.btnDebit.focusSkin = "btnBlueTabBGNoRound";
    frmFacilityInformation.btnCash.skin = "btnTabWhiteRightRound";
    frmFacilityInformation.btnCash.focusSkin = "btnTabWhiteRightRound";
    frmFacilityInformation.flexPaymentMethodPopupData.isVisible = true;
    frmFacilityInformation.flexPaymentMethodDirectDebit.isVisible = false;
  	frmFacilityInformation.btnSavePaymentMethod.skin = "btnGreyBGNoRound";
    frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnGreyBGNoRound";
    frmFacilityInformation.btnSavePaymentMethod.setEnabled(false);
  	 var segData = frmFacilityInformation.segPaymentMethod.data;
            kony.print("segData length"+segData.length)
            for (var i = 0; i < segData.length; i++) {
                var alreadyselectedPaymentData = frmFacilityInformation.segPaymentMethod.data[i];
              if(alreadyselectedPaymentData["flexRow"] != undefined){
                if (alreadyselectedPaymentData["flexRow"]["skin"] == "flexGreyBGLoan") {
                  	 frmFacilityInformation.btnSavePaymentMethod.setEnabled(true);
                     frmFacilityInformation.btnSavePaymentMethod.skin = "btnBlueBGNoRound";
                     frmFacilityInformation.btnSavePaymentMethod.focusSkin = "btnBlueBGNoRound";
                    break;
                }
            }
            }
      
    cashSaveFlag = false;
    debitSaveFlag = true;
}

function checkAmountValue() {
    var haveAmount = true;
    if (frmFacilityInformation.txtReqCreditcardAmt.text == "") {
        frmFacilityInformation.lblRequestCreditLimit.isVisible = true;
        frmFacilityInformation.lblRequestCreditLimitNameHeader.isVisible = false;
        frmFacilityInformation.txtReqCreditcardAmt.isVisible = false;
        //frmFacilityInformation.txtReqCreditcardAmt.setFocus(false)
        frmFacilityInformation.flexFullBody.setFocus(true);
    }
    // return haveAmount;
}

function enableDisablePaymentCriteria(){
  if (gblSelectedLoanProduct == "VP" || gblSelectedLoanProduct == "MS" || gblSelectedLoanProduct == "VM" || gblSelectedLoanProduct == "VT" || gblSelectedLoanProduct == "RC01" || gblSelectedLoanProduct == "RC01") { //  for payment criteria products
    if(frmFacilityInformation.lblPmtCreteria.isVisible){
      if(cashSaveFlag){
      frmFacilityInformation.flexPmtCriteria.onClick = doNothing;
      frmFacilityInformation.flexPmtCriteria.isVisible = false;
      frmFacilityInformation.lblPmtCreteria.skin = "lblGrey171"
    }else{
      frmFacilityInformation.flexPmtCriteria.onClick = onClickFacilityFlexItems
      frmFacilityInformation.flexPmtCriteria.isVisible = true;
      frmFacilityInformation.lblPmtCreteria.skin = "lblblue24px007abc"
    }
    }else if(frmFacilityInformation.lblcriteriavalue.isVisible){
       if(cashSaveFlag){
      frmFacilityInformation.flexPmtCriteria.onClick = doNothing;
      frmFacilityInformation.flexPmtCriteria.isVisible = false;
      frmFacilityInformation.lblPmtCreteria.skin = "lblGrey171";
      frmFacilityInformation.lblcriteria.isVisible = false;
      //frmFacilityInformation.lblcriteria.text = ""
      frmFacilityInformation.lblcriteriavalue.isVisible = false;
      frmFacilityInformation.lblcriteriavalue.text = "";
      frmFacilityInformation.lblPmtCreteria.isVisible = true;
    }else{
      frmFacilityInformation.flexPmtCriteria.onClick = onClickFacilityFlexItems
      frmFacilityInformation.lblPmtCreteria.skin = "lblblue24px007abc";
      frmFacilityInformation.flexPmtCriteria.isVisible = true;
    }
    }
    
  }else{
    
  }
}

function isCreditCardProductSelected(){
  if (gblSelectedLoanProduct == "VP" || gblSelectedLoanProduct == "MS" || gblSelectedLoanProduct == "VM" || gblSelectedLoanProduct == "VT") { //  credit card
   return true;
  }else {
    return false;
  }
}

