//Type your code here

var mobileNumberFromUserDevice = "";
var networkProvider  =""; 
function initfrmMBEnterATMPinFunction(){
	frmMBEnterATMPinFlex.btn0.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn1.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn2.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn3.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn4.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn5.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn6.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn7.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn8.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btn9.onClick = btnPinClickATMPIN;
	frmMBEnterATMPinFlex.btnClr.onClick = btnPinClearActivation;
	frmMBEnterATMPinFlex.btnDel.onClick = btnPinDeleteActivation;
	frmMBEnterATMPinFlex.btnCancel1.onClick = showMBakingscreenNew;
	frmMBEnterATMPinFlex.btnNext.onClick = mbValidateAtmPin;
    frmMBEnterATMPinFlex.btnNext.skin="btnGreyBGNoRound"; 
  	frmMBEnterATMPinFlex.btnNext.setEnabled(false);
	//maxLenPin =4;
}


//PIN Setting Modified
function btnPinClickATMPIN(eventobject) {
    assignfrmMBEnterATMPin.call(this, eventobject["text"]);
}

//PIN Setting Modified
function assignfrmMBEnterATMPin(pin){
	
	if(isNotBlank(pin) && glbPin.length < maxLenPin){
			addPinMBActivation(pin,++glbIndex);
	}else{
		renderPINMBActivation();
	}
	
}


function cleardataATMPINMB(){
  	kony.print("Inside cleardataATMPINMB");
	if(maxLenPin == 6){
		frmMBEnterATMPinFlex.hbxAtmPin.isVisible = false;
		frmMBEnterATMPinFlex.hbxAtmPinConfirm.isVisible = false;
		frmMBEnterATMPinFlex.hbxAtmPinSix.isVisible = true;
	}else{
		frmMBEnterATMPinFlex.hbxAtmPin.isVisible = true;
		frmMBEnterATMPinFlex.hbxAtmPinConfirm.isVisible = false;
		frmMBEnterATMPinFlex.hbxAtmPinSix.isVisible = false;
	}
	glbPin = "";
	glbIndex = 0;
	glbPinArr = [];
	//toggleConfirmPINMBNew(false);
	
	assignATMPINRenderActivation("");

}

function addPinMBActivation(pin,position){

		setPinValueMBActivation(pin,position);
	
	if(glbPin.length == maxLenPin){
		
		
		glbPinArr[0] = glbPin;
		//glbPin = "";
		glbIndex = 0;
		//renderPINMBActivation();
		frmMBEnterATMPinFlex.btnNext.setEnabled(true);
	}
}

function setPinValueMBActivation(pin,position){
  	
		var pinVal = parseInt(pin.charCodeAt(0));
		pinVal += parseInt(glbRandomRang[position-1]);
		pinVal += parseInt(glbDigitWeight[position-1]);
		glbPin += String.fromCharCode(pinVal);
		renderPINMBActivation();
}

function renderPINMBActivation(){
	var btnObject = [];
	
	if(maxLenPin == 6){
		btnObject = [frmMBEnterATMPinFlex.imgPinSix1, frmMBEnterATMPinFlex.imgPinSix2, frmMBEnterATMPinFlex.imgPinSix3, frmMBEnterATMPinFlex.imgPinSix4, frmMBEnterATMPinFlex.imgPinSix5, frmMBEnterATMPinFlex.imgPinSix6];
		
	}else{
		btnObject = [frmMBEnterATMPinFlex.imgPin1, frmMBEnterATMPinFlex.imgPin2, frmMBEnterATMPinFlex.imgPin3, frmMBEnterATMPinFlex.imgPin4];
	}
	
	var emptyPinDis = "pin_input.png";
	var emptyPin = "pin_input_active.png";
	var fillPin = "pin_input_filled.png";
	for(var i=0; i < glbPin.length; i++){
			btnObject[i].src = fillPin;
			
	}
	for(var i=btnObject.length; i > glbPin.length; i--){
			btnObject[i-1].src = emptyPin;
			if(i > glbPin.length+1){
				btnObject[i-1].src = emptyPinDis;
			}
	}
  
  if(glbPin.length == maxLenPin){
		frmMBEnterATMPinFlex.btnNext.skin="btnBlueBGNoRound";
    	frmMBEnterATMPinFlex.btnNext.setEnabled(true);
    
	}else{
      frmMBEnterATMPinFlex.btnNext.skin="btnGreyBGNoRound";
      frmMBEnterATMPinFlex.btnNext.setEnabled(false);
    }
  
}


function assignATMPINRenderActivation(pin){
	
	if(isNotBlank(pin)){
		
			addPinMBActivation(pin,++glbIndex);
			
	}else{
		renderPINMBActivation();
	}
	
}


function btnPinClearActivation(eventobject) {
 
   resetPINwithInitOnlinePinActivation.call(this);
};

function resetPINwithInitOnlinePinActivation(){
	initialParam();
	cleardataATMPINMB();
}

function btnPinDeleteActivation(eventobject) {
    deleteATMPINActivation.call(this);
};

function deleteATMPINActivation(){

	
		if(glbPinArr.length > 0 && glbPinArr[0].length == maxLenPin){
				glbPin = glbPinArr[0];
				glbIndex = glbPinArr[0].length;
				glbPinArr = [];
			}
		if(isNotBlank(glbPin)){
			decreaseATMPINValActivation();
			//toggleConfirmPINMBNew(false);
				//isConfirmPin = false;
		}
}



function decreaseATMPINValActivation(){
			--glbIndex;
			glbPin = glbPin.substring(0, glbPin.length-1);
			assignATMPINRenderActivation("");
}


/**

	CREDEIT CARD ACTIVATION FLOW FUNCTIONS 

**/



//new CVV Function
function init_functionCVVDetailActivation(){
  
	//maxLenCVV = 3;
  	initialParam();
  	frmMBActivateEnterCVVNew.show();
    clearCVVMB_NewActivation();
}


function clearCVVMB_NewActivation(){
	glbPin = "";
	glbIndex = 0;
	glbPinArr = [];
	verifyCVVRenderMB_NewActivation("");
}

function initialFunctionCVVNumberActivation(){
	
	frmMBActivateEnterCVVNew.btn1.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn2.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn3.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn4.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn5.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn6.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn7.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn8.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn9.onClick = btnCVVPinClickActivation;
	frmMBActivateEnterCVVNew.btn11.onClick = btnCVVPinClickActivation;
	//BtnClear
	frmMBActivateEnterCVVNew.btn10.onClick = btnCVVPinClearActivation;
	//BtnDelete
	frmMBActivateEnterCVVNew.btn12.onClick = btnCVVPinDeleteActivation;
    frmMBActivateEnterCVVNew.btnCancel1.onClick=ActivationMBViaIBLogoutServiceCVV;
    frmMBActivateEnterCVVNew.btnNext.onClick = mbValidateCVV;
   frmMBActivateEnterCVVNew.btnNext.skin="btnGreyBGNoRound"; 
   frmMBActivateEnterCVVNew.btnNext.setEnabled(false);
}

function btnCVVPinClickActivation(eventobject) {
    verifyCVVRenderMB_NewActivation.call(this, eventobject["text"]);
};

function verifyCVVRenderMB_NewActivation(pin){
	
	if(isNotBlank(pin)){
		if(glbPin.length < maxLenCVV){
			addCVVMB_NewActivation(pin,++glbIndex);
		}
	}else{
		renderCVVMB_NewActivation();
	}
	
}



function addCVVMB_NewActivation(pin,position){

   var pinVal = parseInt(pin.charCodeAt(0));
	if(glbPin.length < maxLenCVV){
    pinVal += parseInt(glbRandomRang[position-1]);
	pinVal += parseInt(glbDigitWeight[position-1]);
	glbPin += String.fromCharCode(pinVal);
	renderCVVMB_NewActivation();
	}
	/**if(glbPin.length == maxLenCVV){
			mbValidateCardCVV();
	}**/
}

function renderCVVMB_NewActivation(){
	var btnObject = [];
	btnObject = [frmMBActivateEnterCVVNew.imgPin1, frmMBActivateEnterCVVNew.imgPin2, frmMBActivateEnterCVVNew.imgPin3];
	/**var emptyPinDis = "cvv_empty.png";
	var emptyPin = "cvv_empty.png";
	var fillPin = "cvv_filled.png";**/
  	var emptyPinDis = "pin_input.png";
	var emptyPin = "pin_input_active.png";
	var fillPin = "pin_input_filled.png";
	for(var i=0; i < glbPin.length; i++){
			btnObject[i].src = fillPin;
	}
	for(var i=btnObject.length; i > glbPin.length; i--){
			btnObject[i-1].src = emptyPin;
			if(i > glbPin.length+1){
				btnObject[i-1].src = emptyPinDis;
			}
	}
  
    if(glbPin.length == maxLenCVV){
      
      frmMBActivateEnterCVVNew.btnNext.skin="btnBlueBGNoRound"; 
      frmMBActivateEnterCVVNew.btnNext.setEnabled(true);
    }else{
      frmMBActivateEnterCVVNew.btnNext.skin="btnGreyBGNoRound"; 
      frmMBActivateEnterCVVNew.btnNext.setEnabled(false);
    }
  
}


function btnCVVPinClearActivation(eventobject) {
    clearCVVMB_NewActivation.call(this);
};



function btnCVVPinDeleteActivation(eventobject) {
	deleteCVVMB_NewActivation.call(this);
}


function deleteCVVMB_NewActivation(){
	if(glbPinArr.length > 0 && glbPinArr[0].length == maxLenCVV){
			glbPin = glbPinArr[0];
			glbIndex = glbPinArr[0].length;
			glbPinArr = [];
		}
	if(isNotBlank(glbPin)){
		--glbIndex;
		glbPin = glbPin.substring(0, glbPin.length-1);
		verifyCVVRenderMB_NewActivation("");
	}
}






function checkMobileNumberServlet() {
  kony.print("#### checkMobileNumberServlet");
  //"http://"+appConfig.serverIp+"/services/CheckUserStatus";
  var telco_url = kony.i18n.getLocalizedString("telcoURL");
  var request = new kony.net.HttpRequest();
  request.onReadyStateChange = checkMobileNumberServletStateChange;
  kony.print("#### telco_url URL =>" + telco_url);
  
  var randomnum = Math.floor((Math.random() * 10000) + 1);
  telco_url = telco_url+"?checkstatus="+randomnum;
  kony.print("#### telco_url URL after appending random =>" + telco_url);
  request.open(constants.HTTP_METHOD_POST, telco_url, true);
  
  
  //set safari user-agent if android device
  //#ifdef android
  request.setRequestHeader("User-Agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0_3 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Mobile/15A432 Safari Line/7.14.0");
  //#endif
   request.setRequestHeader('cache-control', 'no-cache, must-revalidate, post-check=0, pre-check=0');
  request.setRequestHeader('cache-control', 'max-age=0');
  request.setRequestHeader('expires', '0');
  request.setRequestHeader('expires', 'Tue, 01 Jan 1980 1:00:00 GMT');
  request.setRequestHeader('pragma', 'no-cache');

  request.setRequestHeader("Content-Type", "application/json");
  var postdata = {};
  request.send(postdata);
}

function checkMobileNumberServletStateChange() {
  if (this.readyState == constants.HTTP_READY_STATE_UNSENT) {
    kony.print("#### HTTP_READY_STATE_UNSENT " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_OPENED) {
    kony.print("#### HTTP_READY_STATE_OPENED " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_HEADERS_RECEIVED) {
    kony.print("#### HTTP_READY_STATE_HEADERS_RECEIVED " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_LOADING) {
    kony.print("#### HTTP_READY_STATE_LOADING " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_DONE) {
    kony.print("#### HTTP_READY_STATE_DONE " + new Date());
	
   kony.application.dismissLoadingScreen();
    
   if (this.status == 200 && this.response) {
		
      if (typeof(this.response) === 'string') {
        kony.print("#### HTTP_READY_STATE_DONE " +JSON.parse(this.response));
      }else{
        var responseDataMobileServlet = this.response;
       mobileNumberFromUserDevice = responseDataMobileServlet.userMobileNumberInDevice;
       networkProvider  = responseDataMobileServlet.operatorName;
        if(isNotBlank(mobileNumberFromUserDevice)){
          
          mobileNumberFromUserDevice = mobileNumberFromUserDevice.replace("66", "0");
          if(gbleKYCflow){
             kony.print("###"+mobileNumberFromUserDevice);
            frmMBeKYCVerifyMobile.show();
            return;
          }
          
          if(gblMBActivationVia == "2"){
            if(kony.i18n.getCurrentLocale() == "en_US"){
              setLocaleEng();
              frmMBActivationIBLogin.show();
            }else{
              setLocaleTH();	
              frmMBActivationIBLogin.show();
            }	
          }else if(gblMBActivationVia == "3"){
            atmCitizenIdVisibilityForConfirmation(false);
          }else{
            frmMBActivation.show();
          }

          
        }else{
          retryInActivationTelcoMobileNumber();
        }
        
        kony.print("#### HTTP_READY_STATE_DONE IN ELSE " +responseDataMobileServlet.userMobileNumberInDevice);
      }
      kony.print("#### jsonResponse = " + JSON.stringify(this.response));
    } else {
      kony.print("#### request.status = " + this.status);
     kony.application.dismissLoadingScreen();
    }
 
  }
}


function ActivationMBViaIBLogoutServiceCVV() {
	showLoadingScreen();
	inputParam = {};
	inputParam["channelId"] = "01";
	inputParam["timeOut"] = GBL_Time_Out;
	inputParam["deviceId"] = getDeviceID();	
	var locale = kony.i18n.getCurrentLocale();
			if (locale == "en_US") {
				inputParam["languageCd"] = "EN";
			} else {
				inputParam["languageCd"] = "TH";
			}
	invokeServiceSecureAsync("logOutTMB", inputParam, callBackActivationMBViaIBLogoutService);
}




function activationCompleteShow(){
  
  
  	if(GLOBAL_UV_STATUS_FLAG == "ON"){
      frmRegActivationSuccess.show();
    }else{
      	frmMBActiComplete.show();
    }
}


function checkMobileNumberServletCardLess() {
  kony.print("#### checkMobileNumberServlet");
  //"http://"+appConfig.serverIp+"/services/CheckUserStatus";
  var telco_url = kony.i18n.getLocalizedString("telcoURL");
  var request = new kony.net.HttpRequest();
  request.onReadyStateChange = checkMobileNumberServletStateChangeCardLess;
  kony.print("#### telco_url URL =>" + telco_url);
  var randomnum = Math.floor((Math.random() * 10000) + 1);
  telco_url = telco_url+"?checkstatus="+randomnum;
  request.open(constants.HTTP_METHOD_POST, telco_url, true);
  
  kony.print("#### telco_url URL after appending random =>" + telco_url);
  //set safari user-agent if android device
  //#ifdef android
  request.setRequestHeader("User-Agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0_3 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Mobile/15A432 Safari Line/7.14.0");
  //#endif
  
  request.setRequestHeader('cache-control', 'no-cache, must-revalidate, post-check=0, pre-check=0');
  request.setRequestHeader('cache-control', 'max-age=0');
  request.setRequestHeader('expires', '0');
  request.setRequestHeader('expires', 'Tue, 01 Jan 1980 1:00:00 GMT');
  request.setRequestHeader('pragma', 'no-cache');

  request.setRequestHeader("Content-Type", "application/json");
  var postdata = {};
  request.send(postdata);
}

function checkMobileNumberServletStateChangeCardLess() {
  if (this.readyState == constants.HTTP_READY_STATE_UNSENT) {
    kony.print("#### HTTP_READY_STATE_UNSENT " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_OPENED) {
    kony.print("#### HTTP_READY_STATE_OPENED " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_HEADERS_RECEIVED) {
    kony.print("#### HTTP_READY_STATE_HEADERS_RECEIVED " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_LOADING) {
    kony.print("#### HTTP_READY_STATE_LOADING " + new Date());
  } else if (this.readyState == constants.HTTP_READY_STATE_DONE) {
    kony.print("#### HTTP_READY_STATE_DONE " + new Date());
	
   if (this.status == 200 && this.response) {
		
      if (typeof(this.response) === 'string') {
        kony.print("#### HTTP_READY_STATE_DONE " +JSON.parse(this.response));
      }else{
        var responseDataMobileServlet = this.response;
       mobileNumberFromUserDevice = responseDataMobileServlet.userMobileNumberInDevice;
       networkProvider  = responseDataMobileServlet.operatorName;
        if(isNotBlank(mobileNumberFromUserDevice)){
          mobileNumberFromUserDevice = mobileNumberFromUserDevice.replace("66", "0");
          onClickUVAttentionNext();
        }else{
          retryInActivationTelcoMobileNumber();
        }
         
        kony.print("#### HTTP_READY_STATE_DONE IN ELSE " +responseDataMobileServlet.userMobileNumberInDevice);
      }
      kony.print("#### jsonResponse = " + JSON.stringify(this.response));
    } else {
      kony.print("#### request.status = " + this.status);
     
    }
 
  }
}