function onSelectFromAccntEDonation() {
  showLoadingScreen();
  var inputParam = {}
  inputParam["billPayInd"] = "billPayInd";
  invokeServiceSecureAsync("customerAccountInquiry", inputParam, eDonationCustomerAccountCallBack);
}

function eDonationCustomerAccountCallBack(status, resulttable) {
  var selectedIndex = 0;
  if (status == 400) //success responce
  {
    if (resulttable["opstatus"] == 0) {
      /** checking for Soap status below  */
      var StatusCode = resulttable["statusCode"];
      var fromData = []
      var j = 1
      var nonCASAAct = 0;
      gbltranFromSelIndex = [0, 0];

      for (var i = 0; i < resulttable.custAcctRec.length; i++) {
        var accountStatus = resulttable["custAcctRec"][i].acctStatus;
        if (accountStatus.indexOf("Active") == -1) {
          nonCASAAct = nonCASAAct + 1;
        }
        if (accountStatus.indexOf("Active") >= 0) {
          var icon = "";
          var iconcategory = "";
          if (resulttable.custAcctRec[i].personalisedAcctStatusCode == "02") {
            continue; //do not populate if account is deleted
          }
          icon = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + "NEW_" + resulttable.custAcctRec[i]["ICON_ID"] + "&modIdentifier=PRODICON";
          iconcategory = resulttable.custAcctRec[i]["ICON_ID"];
          if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
            var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew1, icon)
            } else if (iconcategory == "ICON-03") {
              var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew2, icon)
              } else if (iconcategory == "ICON-04") {
                var temp = createSegmentRecordBills(resulttable.custAcctRec[i], hbxSliderNew3, icon)

                }
          // added for removing the joint account and others for SPA and MB
          var jointActXfer = resulttable.custAcctRec[i].partyAcctRelDesc;
          if (jointActXfer == "PRIJNT" || jointActXfer == "SECJNT" || jointActXfer == "OTHJNT" || jointActXfer == "SECJAN") {

          } else {

            kony.table.insert(fromData, temp[0])
          }
          j++;
        }

      } //for
      gblNoOfFromAcs = fromData.length;
      for (var i = 0; i < gblNoOfFromAcs; i++) {
        if (glb_accId == fromData[i].accountNo) {
          selectedIndex = i;
          //glb_accId = 0;
          break;
        }
      }
      if (selectedIndex != 0)
        gbltranFromSelIndex = [0, selectedIndex];
      else
        gbltranFromSelIndex = [0, 0];


      frmSelAccntMutualFund.segTransFrm.widgetDataMap = [];
      frmSelAccntMutualFund.segTransFrm.widgetDataMap = {
        tdFlag: "tdFlag",
        lblAccountNickNameTH: "lblAccountNickNameTH",
        lblAccountNickNameEN: "lblAccountNickNameEN",
        accountNo : "accountNo",
        lblAcntType: "lblAcntType",
        img1: "img1",
        lblCustName: "lblCustName",
        lblBalance: "lblBalance",
        lblActNoval: "lblActNoval",
        lblDummy: "lblDummy",
        lblSliderAccN2: "lblSliderAccN2",
        lblRemainFee: "lblRemainFee",
        lblRemainFeeValue: "lblRemainFeeValue"
      }
      frmSelAccntMutualFund.segTransFrm.data = [];
      //frmBillPayment.segSlider.data=fromData;
      if (nonCASAAct == resulttable.custAcctRec.length) {
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
        return false;
      }
      if (fromData.length == 0) {
        frmSelAccntMutualFund.segTransFrm.setVisibility(false);
        showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"), onClickOfAccountDetailsBack);
        return false;
      } else {
        if (gblDeviceInfo["name"] == "android") {
          if (fromData.length == 1) {
            frmSelAccntMutualFund.segTransFrm.viewConfig = {
              "coverflowConfig": {
                "rowItemRotationAngle": 0,
                "isCircular": false,
                "spaceBetweenRowItems": 10,
                "projectionAngle": 90,
                "rowItemWidth": 80
              }
            };
          } else {
            frmSelAccntMutualFund.segTransFrm.viewConfig = {
              "coverflowConfig": {
                "rowItemRotationAngle": 0,
                "isCircular": true,
                "spaceBetweenRowItems": 10,
                "projectionAngle": 90,
                "rowItemWidth": 80
              }
            };
          }
        }
        frmSelAccntMutualFund.segTransFrm.data = fromData;
        frmSelAccntMutualFund.segTransFrm.selectedIndex = gbltranFromSelIndex;
      }
      dismissLoadingScreen();
      frmSelAccntMutualFund.LabelHeader.text = kony.i18n.getLocalizedString("MB_eDChangeAcctTitle"); 
      frmSelAccntMutualFund.btnTransCnfrmCancel.text = kony.i18n.getLocalizedString("MB_eDOKBtn"); 
	  //Extra code for E-Donation
      frmSelAccntMutualFund.btnHdrMenu.onClick = frmSelAccntEDonation_closeBtn_click;
      frmSelAccntMutualFund.btnTransCnfrmCancel.onClick = settingAccountEDonation; 
      checkAmountQRPay_donation();
      frmSelAccntMutualFund.show();
    } else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  } else {
    dismissLoadingScreen();
  }
}

function frmSelAccntEDonation_closeBtn_click(){
  frmEDonationPaymentConfirm.show();
}

function settingAccountEDonation() {
  var avaiLength = 0;
  var avaiAmount = "";
  gbltranFromSelIndex = frmSelAccntMutualFund.segTransFrm.selectedIndex;
  gblSelTransferFromAcctNo = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblActNoval;
  avaiLength = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance.length;
  avaiAmount = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance.substring(0, avaiLength - 2);
  gblEDonation["avaiAmount"] = parseFloat(removeCommos(avaiAmount));
  gblEDonation["fromAccountNo"] = gblSelTransferFromAcctNo;
  gblEDonation["acctNickname"] =frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblCustName;
  gblEDonation["acctType"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAcntType;
  
  //Saving Default Account
   frmEDonationPaymentOnClickSelAcctLink();
  
}

function frmEDonationPaymentOnClickSelAcctLink() {
  var inputParam = {};
  var fromAcctID = gblEDonation["fromAccountNo"];
  var frmAcct = fromAcctID.replace(/-/g, "");
  inputParam["accountNumber"] = frmAcct;
  inputParam["transType"] = "TRANSFER_PROMPTPAY";
  showLoadingScreen();
  invokeServiceSecureAsync("QRPaymentDefaultAccountSettings", inputParam, onClickSelAcctLinkEDonationCallBack)
}

function onClickSelAcctLinkEDonationCallBack(status, resulttable) {
  if (status == 400) {
    if (resulttable["opstatus"] == 0) {
      dismissLoadingScreen();
       gblQRPayData["accountName"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].custAcctName; //MKI, MIB-edonation
      var locale = kony.i18n.getCurrentLocale();
      if (locale == "th_TH") {
          gblQRPayData["custAcctName"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAccountNickNameTH;
          gblQRPayData["custName"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAccountNickNameTH;
          gblqrFromAccntNickName = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAccountNickNameTH;
      } else {
          gblQRPayData["custAcctName"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAccountNickNameEN;
          gblQRPayData["custName"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAccountNickNameEN;
          gblqrFromAccntNickName = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblAccountNickNameEN;
      }
      
      accountId = frmSelAccntMutualFund.segTransFrm.selectedItems[0].accountNo;
      glb_accId = accountId;
      gblqrFromAccntNum = frmSelAccntMutualFund.segTransFrm.selectedItems[0].accountNo;;
      gblQRPayDefaultAcct = gblqrFromAccntNum;

      gblqrToAccnt = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblActNoval;
      
      gblQRPayData["fromAcctID"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblActNoval;
	  gblQRPayData["accountNoFomatted"] = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblActNoval;
      
      gblqrToAccntBal = frmSelAccntMutualFund.segTransFrm.selectedItems[0].lblBalance;
      frmEDonationPaymentConfirm.lblFromAcctName.text = gblQRPayData["custAcctName"];
      gblQRPayData["acctNickName"] = gblQRPayData["custAcctName"];
      //
    } else {
      dismissLoadingScreen();
      alert("Cannot Set Default Account");
    }
  }
  billPaymentValidationForEDonation();
}

function prepareDataEDonationBiller() {
	gblQRPayData = [];
    kony.print("gblRef2ScanValue-->"+gblRef2ScanValue)
	if(gblRef2ScanValue == "0"){
		gblQRPayData["transferAmt"] = ""; 
		gblQRPayData["billerID"]=gblBillerTaxID;
		gblQRPayData["ref1"]=gblRef1ScanValue;
		gblQRPayData["ref2"]=gblRef2ScanValue;		
		gblQRPayData["QR_TransType"]="QR_Bill_Payment";
        kony.print("before calling default account-->")
		invokeEDonationDefaultAccountService();
	}
	else{
		dismissLoadingScreen();
		alert(kony.i18n.getLocalizedString("MB_eDErrorNotFound"));
	}
}


function invokeEDonationDefaultAccountService() {
 
  var inputParam = {};
  gblQRPaymentDeviceId = getDeviceID();
  inputParam["encryptDeviceId"] = getDeviceID();
	if(gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
    inputParam["promptPayId"] = gblQRPayData["billerID"];
    inputParam["promptPayType"] = "05";
  }

  if (gblQRPayData["transferAmt"] != undefined && gblQRPayData["transferAmt"] != "") {
    inputParam["transAmount"] = gblQRPayData["transferAmt"];
  } else {
    inputParam["transAmount"] = "1";
  }
	if(gblQRPayData["QR_TransType"]== "QR_Bill_Payment"){
      inputParam["transType"] = "BILLPAY_PROMPTPAY";
      inputParam["ref1"] = gblQRPayData["ref1"];
      inputParam["ref2"] = gblQRPayData["ref2"];
      inputParam["ref3"] = gblQRPayData["ref3"];
    }
  kony.print("before calling QRPaymentDefaultAccount-->"+inputParam)
  gblDonationNormalFlow = true;
  inputParam["eDonationType"] = "Barcode_Type";  // gbleDonationType 'QR_Type' or 'Barcode_Type'  
  invokeServiceSecureAsync("QRPaymentDefaultAccount", inputParam, callBackQRDefaultAccount);
}

// function callBackEDonationDefaultAccount(status, resulttable) {
//   try {
//     var locale_app = kony.i18n.getCurrentLocale();
//     var banckCode = "";
//     var usrAccntNickname = "";
//     var usrAcctNum = ""
//     var accountNickName = "";
//     var accountId = "";
//     var accountNoFomatted = "";
//     var availBalLabel = "";
//     var availBalValue = ""
//     var msgError = "";

//     if (status == 400) {
//       kony.print("Inside here 123");
//       if (resulttable["opstatus"] == "0" || resulttable["opstatus"] == 0) {
//         gblQRDefaultAccountResponse = resulttable;

//         if (resulttable["pp_promptPayFlag"] == "1") {

//           var errCode = resulttable["pp_errCode"];
//           var errorText = "";

//           if (errCode == 'XB240063') {
//             msgError = kony.i18n.getLocalizedString("MIB_P2PRecBankRejectAmt");
//           } else if (errCode == 'XB240048') {
//             kony.print("Inside here XB240048");
//             msgError = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
//           } else if (errCode == 'XB240066') {
//              kony.print("Inside here XB240066");
//             msgError = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
//           } else if (errCode == 'XB240088') {
//             msgError = kony.i18n.getLocalizedString("MIB_P2PAccInActive");          
//           } else if (errCode == 'XB240098') {
//             msgError = kony.i18n.getLocalizedString("MIB_P2PExceedeWal");
//           } else if (errCode == 'XB240067') {
//             msgError = kony.i18n.getLocalizedString("MIB_P2PCutOffTime");
//           } else if (errCode == 'XB240072') {
//             msgError = kony.i18n.getLocalizedString("MIB_P2PDestTimeout");

//           } else if (errCode == 'X8899') {
//             msgError = kony.i18n.getLocalizedString("MIB_P2PCloseBranchErr");
//           } else if (errCode == 'X1120') {
//             msgError = kony.i18n.getLocalizedString("billernotfoundTQRC");
//           } else if (errCode == '8002') {
//               msgError = kony.i18n.getLocalizedString("NoValidAccountsPayment");
//           } else {
//             var errorText = kony.i18n.getLocalizedString("ECGenericError") + " (" + errCode + ")";
//             msgError = errorText;
//           }
//           alert(msgError);
//           dismissLoadingScreen();
//           return;
//         } else if(resulttable["pp_destBankCode"] != undefined){
//           banckCode = resulttable["pp_destBankCode"];
//           gblisTMB = banckCode;
//           gblqrUsrName = resulttable["pp_toAccTitle"];
//           if(!isNotBlank(gblisTMB)){
//             alert(kony.i18n.getLocalizedString("ECGenericError"));
//             return;
//           }
//           if(gblisTMB != gblTMBBankCD){
//             gblqrUsrName = resulttable["pp_toAcctName"];
//             gblqrUsrAccnt = resulttable["pp_toAcctNo"];
//             gblqrTransFee = resulttable["pp_itmxfee"];
//             gblqrToUsrName = resulttable["pp_toAccTitle"];
            
//             gblQRPayData["banckCode"] = resulttable["pp_destBankCode"];
			
//             gblQRPayData["billerCompCode"] = resulttable["qrCompCode"];
//             gblQRPayData["toAccountName"] = resulttable["pp_toAccTitle"];
//             gblQRPayData["toAccountName"] = resulttable["pp_toAcctName"];
//             gblQRPayData["toAcctIDNoFormat"] = resulttable["pp_toAcctNo"];
//             //gblQRPayData["custAcctName"] = resulttable["pp_toAccTitle"];
//             if (resulttable["pp_itmxFee"] !== null || resulttable["pp_itmxFee"] !== undefined) {
//               gblQRPayData["fee"] = resulttable["pp_itmxFee"];
//             } else {
//               gblQRPayData["fee"] = "0.00";
//             }
//           } else {
//             gblqrUsrName = resulttable["pp_toAccTitle"];
//             gblqrUsrAccnt = resulttable["pp_toAcctNo"];
//             gblqrTransFee = resulttable["pp_itmxfee"];
//             gblqrToUsrName = resulttable["pp_toAccTitle"];
//             gblQRPayData["banckCode"] = resulttable["pp_destBankCode"];
// 			//commneted for bill pay
//             gblQRPayData["billerCompCode"] = resulttable["qrCompCode"];
//             gblQRPayData["toAccountName"] = resulttable["pp_toAccTitle"];
//             gblQRPayData["toAccountName"] = resulttable["pp_toAcctName"];
//             gblQRPayData["toAcctIDNoFormat"] = resulttable["pp_toAcctNo"];
//             //gblQRPayData["custAcctName"] = resulttable["pp_toAccTitle"];
//             if (resulttable["pp_itmxFee"] !== null || resulttable["pp_itmxFee"] !== undefined) {
//               gblQRPayData["fee"] = resulttable["pp_itmxFee"];
//             } else {
//               gblQRPayData["fee"] = "0.00";
//             }
//             kony.print("Values Before checking #########"+gblQRPayData["custAcctName"]+gblQRPayData["custName"]+gblQRPayData["toAccountName"])
//           }
//           if (resulttable["custAcctRec"] != undefined && resulttable["custAcctRec"].length > 0) {
//             kony.print("INSIDE the log custAcctRec $$$$$$$$$$$$$$");
//             kony.print("Values Before checking &&&&&&&&&"+gblQRPayData["custAcctName"]+gblQRPayData["custName"]+gblQRPayData["toAccountName"])
//             var collectionData = resulttable["custAcctRec"];
//             //alert("Testing"+resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"]);
// 			gblQRPayData["accountName"]= resulttable["custAcctRec"][0]["accountName"]; //MKI, MIB-9891
//             if(resulttable["custAcctRec"][0]["acctNickName"] != undefined 
//                && resulttable["custAcctRec"][0]["acctNickName"] != ""){
//               gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["acctNickName"];
//               gblQRPayData["custName"] = resulttable["custAcctRec"][0]["acctNickName"];
//              //gblQRPayData["accountName"]= resulttable["custAcctRec"][0]["accountName"]; //MKI, MIB-9891
//               gblqrFromAccntNickName = resulttable["custAcctRec"][0]["acctNickName"];
//             } else {
//               if (locale_app == "th_TH") {
//                 gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
//                 gblQRPayData["custName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
//                 gblqrFromAccntNickName = resulttable["custAcctRec"][0]["defaultCurrentNickNameTH"];
//               } else {
//                 gblQRPayData["custAcctName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
//                 gblQRPayData["custName"] = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
//                 gblqrFromAccntNickName = resulttable["custAcctRec"][0]["defaultCurrentNickNameEN"];
//               }
//             }

//             if ((resulttable["custAcctRec"][0]["accId"]) != null && (resulttable["custAcctRec"][0]["accId"]) != undefined) {
//               accountId = resulttable["custAcctRec"][0]["accId"];
//               gblqrFromAccntNum = resulttable["custAcctRec"][0]["accId"];
//               gblQRPayDefaultAcct = gblqrFromAccntNum;
//               glb_accId = gblQRPayDefaultAcct;
//             }

//             if ((resulttable["custAcctRec"][0]["accountNoFomatted"]) != null && (resulttable["custAcctRec"][0]["accountNoFomatted"]) != undefined) {
//               gblqrToAccnt = resulttable["custAcctRec"][0]["accountNoFomatted"];
//               gblQRPayData["fromAcctID"] = resulttable["custAcctRec"][0]["accountNoFomatted"];
//             }

//             if ((resulttable["custAcctRec"][0]["accType"]) == "CCA") {
//               gblqrToAccntBal = resulttable["custAcctRec"][0]["availableCreditBalDisplay"];
//             } else {
//               gblqrToAccntBal = resulttable["custAcctRec"][0]["availableBalDisplay"];
//             }
            
//             if(resulttable["pp_isOwn"] == 'Y'){
//               var defaultAccount = removeHyphenIB(resulttable["custAcctRec"][0]["accountNoFomatted"]);
//               gblpp_isOwnAccount = true; // MKI, MIB-9972
//               if (defaultAccount == resulttable["pp_toAcctNo"] && gblisTMB == gblTMBBankCD){
//                 alert(kony.i18n.getLocalizedString("msgErrOwnAcctQR"));
//                 dismissLoadingScreen();
//                 return false;
//               }
//             }
//             kony.print("Values Before checking !!$$$$$$$$$"+gblQRPayData["custAcctName"]+gblQRPayData["custName"]+gblQRPayData["toAccountName"]+" Testing:  "+gblQRPayData["toAccountName"])
//             if(gblQRPayData["toAccountName"]!= "" && gblQRPayData["toAccountName"]!= undefined){
//              gblQRPayData["selectAccName"] = gblQRPayData["toAccountName"];
//             }else {
//               gblQRPayData["selectAccName"] = "";
//             }
//             gblQRPayData["E_DONATION_CATEGORY"] = resulttable["E_DONATION_CATEGORY"];
//             gblQRPayData["billerCategoryID"] = resulttable["billerCategoryID"];
//             gblQRPayData["billerNameEN"] = resulttable["billerNameEN"];
//             gblQRPayData["qrCompCode"] = resulttable["qrCompCode"];
//             gblQRPayData["accountName"] = resulttable["custAcctRec"][0]["accountName"];
//             gblQRPayData["acctNickName"] = resulttable["custAcctRec"][0]["acctNickName"];
//             gblQRPayData["accountNoFomatted"] = resulttable["custAcctRec"][0]["accountNoFomatted"];
//             kony.print("QR_TransType"+gblQRPayData["QR_TransType"]);
//             if(gblQRPayData["E_DONATION_CATEGORY"] == gblQRPayData["billerCategoryID"]){
//             	dismissLoadingScreen();
//                 showeBillpaymentDonationAmount();
//             }else{
//             	dismissLoadingScreen();
//                //TODO frmQRSuccessPreShow();
//               	return false;
//             }           
//           } else {
//             alert(kony.i18n.getLocalizedString("keyNoEligibleAct"));
//             dismissLoadingScreen();
//             return false;
//           }
//         } else {
//           var errCode = resulttable["pp_errCode"];
//           var errorText = "";
//           if (isNotBlank(errCode)) {
//             if (errCode == 'X1120') {
//               msgError = kony.i18n.getLocalizedString("billernotfoundTQRC");
//             }else if (errCode == 'XB246072') {
//               msgError = kony.i18n.getLocalizedString("ErrorNotRegBillPay");
//             }else if (errCode == '8002') {
//               msgError = kony.i18n.getLocalizedString("NoValidAccountsPayment");
//             }else if (errCode == '8001'){
//               var errorText = kony.i18n.getLocalizedString("ErrorGenBillPay");
//               msgError = errorText;
//           	}else if (resulttable["pp_errB24Msg"] !== null || resulttable["pp_errB24Msg"] !== undefined) {
//               errorText = resulttable["pp_errB24Msg"];
//               msgError = errorText;
//             }else {
//               msgError = kony.i18n.getLocalizedString("ECGenericError");
//           	} 
//           }else{
//              var errorText = kony.i18n.getLocalizedString("ECGenericError");
//             msgError = errorText;
//           }          
//           alert(msgError);
//           dismissLoadingScreen();
//           return;
//         }
//       } else if (resulttable["opstatus"] == 8005) {
//         if (resulttable["errCode"] == 1002) {
//           isUserStatusActive = false;
//           alert(kony.i18n.getLocalizedString("VQB_MBStatus"));
//           dismissLoadingScreen();
//           frmMBPreLoginAccessesPin.show(); //MKI, MIB-9955
//         } else {
//           var errMsg = resulttable["errMsg"];
//           alert(errMsg);
//           dismissLoadingScreen();
//         }
//       } else if (resulttable["opstatus"] == 1011) {
//         alert(kony.i18n.getLocalizedString("genErrorWifiOff"));
//         if (resulttable["errCode"] == 1002) {
//           isUserStatusActive = false;
//           msgError = kony.i18n.getLocalizedString("VQB_MBStatus");
//           alert(msgError);
//           dismissLoadingScreen();
//         } else {
//           msgError = resulttable["errMsg"];
//           alert(msgError);
//           dismissLoadingScreen();
//         }
//       } else if (resulttable["opstatus"] == 1011) {
//         msgError = kony.i18n.getLocalizedString("genErrorWifiOff");
//         alert(msgError);
//         dismissLoadingScreen();
//       } else {
//         msgError = kony.i18n.getLocalizedString("VQB_ServiceDown");
//         alert(msgError);
//         dismissLoadingScreen();
//       }

//     } 
//   } catch (e) {
//     msgError = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
//     alert(msgError);
//     dismissLoadingScreen();
//   }
// }

function showeBillpaymentDonationAmount(){
  gbleDonationType = "Barcode_Type";
  kony.print("inside Show eDonation mode");
  getFoundationDetails(gblQRPayData["qrCompCode"]);
  frmDonationSelectAmount.show();
}