//Type your code here

function onclickCancelMF(){
  kony.print("in popup")
  var selectedindex=frmIBMFAcctOrderToBProceed.segOrder.selectedIndex[1];
  popUpCancelorder.skin="popUpBg";
  popUpCancelorder.HBoxImage.setVisibility(false);
  popUpCancelorder.hbxOtpBox.setVisibility(false);
  popUpCancelorder.hbxBut0otn.setVisibility(false);
  popUpCancelorder.HBoxOrderDetails.setVisibility(true);
  popUpCancelorder.HBoxCancelOrder.setVisibility(true);
  popUpCancelorder.HBoxHeader.skin="hboxWhite" ;
  popUpCancelorder.HBoxHeader.setVisibility(true);
 // popUpCancelorder.hbxBut0otn.skin="hboxWhite" ;
 // popUpCancelorder.HBoxImage.skin="hboxWhite" ;
 // popUpCancelorder.HBoxOrderDetails.skin="hboxWhite" ;
  //popUpCancelorder.HBoxCancelOrder.skin="hboxWhite" ;
 // popUpCancelorder.hbxOtpBox.skin="hboxWhite" ;
  
  dataOfSelectedIndex = frmIBMFAcctOrderToBProceed.segOrder.data[selectedindex];
  kony.print(JSON.stringify(dataOfSelectedIndex));
  OrderDateTime=dataOfSelectedIndex["lblOrderDateval"];
  var orderDate = dataOfSelectedIndex["lblOrderDateval"].split(" ");
  var date = orderDate[0];
  //popUpCancelorder.transparencyBehindThePopup="50";
  //popUpCancelorder.containerHeight="75";
  
   var locale = kony.i18n.getCurrentLocale();
     var tranTypeHub = "TranTypeHubEN";
     var statusHub = "StatusHubEN";
     var channelHub = "ChannelHubEN";
     if (locale == "th_TH") {
     	tranTypeHub = "TranTypeHubTH";
     	statusHub = "StatusHubTH";
     	channelHub = "ChannelHubTH";
     }
  	if(dataOfSelectedIndex["TranTypeHubEN"]=="BUY"){
      Trnsaction_Type="P";
    }else{
      Trnsaction_Type="R";
    }
    var amount = dataOfSelectedIndex["lblAmountval"];
    if (parseInt(dataOfSelectedIndex["lblAmountval"]) == 0 && parseInt(dataOfSelectedIndex["lblUnitval"]) != 0) {
        amount = verifyDisplayUnit(dataOfSelectedIndex["lblUnitval"]);
    }
  
  popUpCancelorder.lblTitle.text=kony.i18n.getLocalizedString("key_cancelorder");
  
  popUpCancelorder.lblkey1.text=kony.i18n.getLocalizedString("MF_lbl_Order_date");
  popUpCancelorder.lblValue1.text=date;
  
  popUpCancelorder.lblkey2.text=kony.i18n.getLocalizedString("key_fundCode")+":";
  if (isNotBlank(gblMFDetailsResulttable)) {
    if (locale == "th_TH") {
      popUpCancelorder.lblValue2.text=gblMFDetailsResulttable["FundNameTH"];
    }else{
      popUpCancelorder.lblValue2.text=gblMFDetailsResulttable["FundNameEN"];
    }
  }else{
    popUpCancelorder.lblValue2.text="";
  }
  
  popUpCancelorder.lblkey3.text=kony.i18n.getLocalizedString("MF_lbl_Amount");
  popUpCancelorder.lblValue3.text=amount;
  
  popUpCancelorder.lblkey4.text=kony.i18n.getLocalizedString("MF_lbl_Transaction_type");
  popUpCancelorder.lblValue4.text=dataOfSelectedIndex["lbltranTypeHubval"];
  
  //popUpCancelorder.lblkey4.text=kony.i18n.getLocalizedString("MF_thr_Status");
 // popUpCancelorder.lblTitle.text=kony.i18n.getLocalizedString("key_cancelorder");
  
 popUpCancelorder.lblkey5.text=kony.i18n.getLocalizedString("MF_thr_Status")+":";
  popUpCancelorder.lblValue5.text=dataOfSelectedIndex["lblstatusHubval"];
  
  popUpCancelorder.lblkey6.text=kony.i18n.getLocalizedString("MF_lbl_Channel");
  popUpCancelorder.lblValue6.text=dataOfSelectedIndex["lblchannelHubval"];
  
  popUpCancelorder.lblkey7.text=kony.i18n.getLocalizedString("MF_TransactionRefNo")+":";
  popUpCancelorder.lblValue7.text=dataOfSelectedIndex.OrderReference;
  
  popUpCancelorder.btnCancelOrder.text=kony.i18n.getLocalizedString("key_cancelorder");
  popUpCancelorder.btnCancelOrder.onClick=MBcallCancelValidation;
  popUpCancelorder.btnCancel.onClick=closePopUp;
  popUpCancelorder.btnClose.onClick=closePopUp;
  popUpCancelorder.btnConfirm.onClick=callCancelConfirmCompositeService;
  
  popUpCancelorder.HBoxImage.margin=[0,20,0,0];
  popUpCancelorder.HBoxCancelOrder.margin=[18,33,18,0];
  
  popUpCancelorder.show();
}
function closePopUp(){
  popUpCancelorder.dismiss();
}

function CancelValidationCallBackIB(status, resulttable) {
     kony.print(" in call back MBcallCancelValidation");
    if (status == 400) {
    	dismissLoadingScreen();
        if (resulttable["opstatus"] == 0 || resulttable["opstatus"] == "0") {
             kony.print(" in call back success MBcallCancelValidation");
            	//showCancelPwdPopup();
          		popUpCancelorder.hbxBut0otn.setVisibility(true);
          		popUpCancelorder.HBoxCancelOrder.setVisibility(false);
          		cancelMutualFundOTPRequestService();
        }else if(resulttable["opstatus"] == 1005 || resulttable["opstatus"] == "1005"){
          showAlert( kony.i18n.getLocalizedString("MF_Cancel_cuttoff"), kony.i18n.getLocalizedString("info"));
        }else{
          showAlert("status : " + status + " errMsg: " + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
        }

    }else{
    	dismissLoadingScreen();
    }

}


function cancelMutualFundOTPRequestService() {
	
    popUpCancelorder.txtBxOTP.text = "";
	var locale = kony.i18n.getCurrentLocale();
	var eventNotificationPolicy;
	var SMSSubject;
  	
	if(Trnsaction_Type=="R"){
		Channel = "MFCancelRedeem";
	} else {
		Channel = "MFCancelPurchase";
	}

    popUpCancelorder.lblOTPinCurr.text = " ";//kony.i18n.getLocalizedString("invalidOTP"); //
    popUpCancelorder.lblPlsReEnter.text = " "; 
    popUpCancelorder.hbxOTPincurrect.isVisible = false;
    popUpCancelorder.hbxOTPEntry.isVisible = true;
    popUpCancelorder.hbxOtpBox.isVisible = true;
    popUpCancelorder.txtBxOTP.setFocus(true); 
	var inputParams = {
		retryCounterRequestOTP: gblRetryCountRequestOTP,
		Channel: Channel,
      	FundCode:gblFundCode,
		locale: locale
	};
	showLoadingScreenPopup();
	try {
		invokeServiceSecureAsync("generateOTPWithUser", inputParams, cancelMutualFundOTPRequestServiceCallBack);
	} catch (e) {
		// todo: handle exception
		invokeCommonIBLogger("Exception in invoking service");
	}
}

function cancelMutualFundOTPRequestServiceCallBack(status, callBackResponse) {

	if (status == 400) {
	  dismissLoadingScreenPopup();
		if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
        	popUpCancelorder.hbxOTPsnt.setVisibility(false);
			//popUpCancelorder.hbox476047582127699.isVisible = false;
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            return false;
        }
        if (callBackResponse["errCode"] == "GenOTPRtyErr00002") {
        	popUpCancelorder.hbxOTPsnt.setVisibility(false);
			//popUpCancelorder.hbox476047582127699.isVisible = false;
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
            return false;
        }
 		if (callBackResponse["opstatus"] == 0) {
			if (callBackResponse["errCode"] == "GenOTPRtyErr00001") {
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			} else if (callBackResponse["errCode"] == "JavaErr00001") {
				showAlert(kony.i18n.getLocalizedString("ECJavaErr00001"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			//var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
			gblRetryCountRequestOTP = kony.os.toNumber(callBackResponse["retryCounterRequestOTP"]);
			gblOTPLENGTH = kony.os.toNumber(callBackResponse["otpLength"]);
			
			var reqOtpTimer = kony.os.toNumber(callBackResponse["requestOTPEnableTime"]);
            curr_form = kony.application.getCurrentForm();
            try {
                kony.timer.cancel("OTPTimer");
            } catch (e) {
                // todo: handle exception
            }
            if (gblOTPReqLimit < 3) {
                kony.timer.schedule("OtpTimer", OTPTimercallbackMFCancelOrder, reqOtpTimer, false);
            }
			
          	
			
			popUpCancelorder.hbox476047582127699.setVisibility(true);
			popUpCancelorder.hbxOTPsnt.setVisibility(true);
			popUpCancelorder.txtBxOTP.setFocus(true);
			popUpCancelorder.hbxBut0otn.setVisibility(true);
			popUpCancelorder.lblBankRef.text = kony.i18n.getLocalizedString("keyIBbankrefno");
			
			popUpCancelorder.lblOTPinCurr.text = kony.i18n.getLocalizedString("keyIBbankrefno");
			var refVal="";
			for(var d=0;d<callBackResponse["Collection1"].length;d++){
				if(callBackResponse["Collection1"][d]["keyName"] == "pac"){
					refVal=callBackResponse["Collection1"][d]["ValueString"];
					break;
				}
			}
			popUpCancelorder.lblBankRefVal.text = refVal//callBackResponse["Collection1"][8]["ValueString"];
			popUpCancelorder.lblKeyotpmg.text = kony.i18n.getLocalizedString("keyotpmsg");
			popUpCancelorder.label476047582115279.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			//frmIBMFConfirm.btnOTPReq.skin = btnIB158disabled;
			popUpCancelorder.btnOTPReq.skin = btnIBREQotp;
			popUpCancelorder.btnOTPReq.hoverSkin=btnIBREQotp;
			popUpCancelorder.btnOTPReq.setEnabled(false);
          
			
		} 
 		else if (callBackResponse["opstatus"] == 8005 || callBackResponse["code"] == 10403) {
 			popIBTransNowOTPLocked.show();
 		}
 		else {
          if(callBackResponse["errMsg"] != undefined)
          {
            alert(callBackResponse["errMsg"]);
          }
          else
          {
            alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
          }		
		}
	}  
}

function OTPTimercallbackMFCancelOrder() {
  popUpCancelorder.btnOTPReq.skin = "btnIBREQotpFocus";
  popUpCancelorder.btnOTPReq.setEnabled(true);
  popUpCancelorder.btnOTPReq.onClick = cancelMutualFundOTPRequestService;

  try {
    kony.timer.cancel("OtpTimer");
  } catch (e) {

  }
}

function mfCancelOrderCompositeCallBackIB(status, resulttable) {
    dismissLoadingScreen();
    if (status == 400) {
        if (resulttable["opstatus"] == 0 || resulttable["opstatus"] == "0") {
          	 popUpCancelorder.HBoxImage.margin=[0,38,0,0];
          	 popUpCancelorder.HBoxCancelOrder.margin=[18,45,18,0];
             popUpCancelorder.lblnotComplete.text="Transaction Cancelation Completed";
          	 popUpCancelorder.HBoxOrderDetails.setVisibility(false);
             popUpCancelorder.HBoxHeader.setVisibility(false);
             popUpCancelorder.hbxOtpBox.setVisibility(false);
             popUpCancelorder.hbxBut0otn.setVisibility(false);
          	 popUpCancelorder.HBoxCancelOrder.setVisibility(false);
          	callMFTimer();
        
          
        } else if (resulttable["opstatus"] == 1015 || resulttable["opstatus"] == "1015") {
          popUpCancelorder.HBoxImage.setVisibility(true);	  
          popUpCancelorder.HBoxImage.margin=[0,38,0,0];
          popUpCancelorder.HBoxCancelOrder.margin=[18,50,18,0];
          popUpCancelorder.ImgComplete.src="icon_alert.png";
          popUpCancelorder.lblnotComplete.text= kony.i18n.getLocalizedString("MF_Cancel_Warn");
          popUpCancelorder.HBoxCancelOrder.setVisibility(false);
          popUpCancelorder.HBoxOrderDetails.setVisibility(false);
          popUpCancelorder.HBoxHeader.setVisibility(true);
          popUpCancelorder.hbxOtpBox.setVisibility(false);
          popUpCancelorder.hbxBut0otn.setVisibility(false);
          popUpCancelorder.btnClose.onClick=callbackMFTimer;
        } 
      else	if(resulttable["opstatus"] == 1001 || resulttable["opstatus"] == "1001"){
        popUpCancelorder.HBoxImage.setVisibility(true);
        popUpCancelorder.HBoxImage.margin=[0,38,0,0];
        popUpCancelorder.HBoxCancelOrder.margin=[18,50,18,0];
        popUpCancelorder.ImgComplete.src="icon_failed.png";   
        popUpCancelorder.lblnotComplete.text= kony.i18n.getLocalizedString("MF_Cancel_Fail");
        popUpCancelorder.HBoxCancelOrder.setVisibility(true);
        popUpCancelorder.btnCancelOrder.text=kony.i18n.getLocalizedString("btnTryAgain");
        popUpCancelorder.btnCancelOrder.onClick=closePopUp;
        popUpCancelorder.HBoxOrderDetails.setVisibility(false);
        popUpCancelorder.HBoxHeader.setVisibility(true);
        popUpCancelorder.hbxOtpBox.setVisibility(false);
        popUpCancelorder.hbxBut0otn.setVisibility(false);
        popUpCancelorder.btnClose.onClick=closePopUp;

      }else if(resulttable["opstatus"] == 1005 || resulttable["opstatus"] == "1005"){
          showAlert( kony.i18n.getLocalizedString("MF_Cancel_cuttoff"), kony.i18n.getLocalizedString("info"));
        } else if (resulttable["opstatus"] == 8005) {
            dismissLoadingScreen();
           if (resulttable["errCode"] == "VrfyOTPErr00001") {
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                popUpCancelorder.hbxOTPincurrect.setVisibility(true);
                popUpCancelorder.lblOTPinCurr.text = kony.i18n.getLocalizedString("invalidotpone");//kony.i18n.getLocalizedString("invalidOTP"); //
                popUpCancelorder.lblPlsReEnter.text = kony.i18n.getLocalizedString("invalidotptwo");
                popUpCancelorder.lblOTPinCurr.setVisibility(true);
                popUpCancelorder.lblPlsReEnter.setVisibility(true);
                popUpCancelorder.hbox476047582127699.setVisibility(false);
                popUpCancelorder.hbxOTPsnt.setVisibility(false);
                popUpCancelorder.txtBxOTP.text="";
                popUpCancelorder.tbxToken.text="";
                //alert("" + kony.i18n.getLocalizedString("invalidOTP"));
                return false;
              } else if (resulttable["errCode"] == "VrfyOTPErr00002") {
                handleOTPLockedIB(resulttable);
                //startRcCrmUpdateProfilBPIB("04");
                return false;
      }  else if (resulttable["errCode"] == "VrfyOTPErr00005") {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("KeyTokenSerialNumError"), kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00006") {
                dismissLoadingScreen();
                gblVerifyOTPCounter = resulttable["retryCounterVerifyOTP"];
                showAlert("" + resulttable["errMessage"], kony.i18n.getLocalizedString("info"));
                return false;
            } else if (resulttable["errCode"] == "VrfyOTPErr00004") {
                dismissLoadingScreen();
                showAlert("" + resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
                return false;
            } else {
                dismissLoadingScreen();
                showAlert("" + kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    }
    
} 
function showIBLoadingAnimation(){
  	frmIBMFAcctOrderToBProceed.loadingBtnAnim1.src="tmb_loading.gif";
	 frmIBMFAcctOrderToBProceed.lblLoadingtext.text = kony.i18n.getLocalizedString("key_Loading");
  	 frmIBMFAcctOrderToBProceed.HboxLoading.setVisibility(true);
   kony.application.showLoadingScreen(null, "", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, false, null);
}
function dismissIBLoadingAnimation(){
   frmIBMFAcctOrderToBProceed.HboxLoading.setVisibility(false);
  dismissLoadingScreen();
}