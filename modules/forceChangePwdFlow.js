







var gblModifyUserErr = 0;
/*function looginflowPartyInqChangePwd() {
	showLoadingScreenPopup()
 	var inputparam = {};
 	invokeServiceSecureAsync("partyInquiry", inputparam, looginflowPartyInqChangePwdCallBack);
}

function looginflowPartyInqChangePwdCallBack(status, resulttable) {
	if (status == 400) {
		var isError = showCommonAlertIB(resulttable)
		if (isError) return false;
		if (resulttable["opstatus"] == 0) {
			for (var i = 0; i < resulttable["ContactNums"].length; i++) {
				if (resulttable["ContactNums"][i]["PhnType"] == 'Mobile')
					gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
			}
			IBcrmProfileInqForceChange();
			invokeRequestOtpForceChngPWD();
 		} else {
 			dismissLoadingScreenPopup();		
			invokeIBCRMProfileUpdateLogout();
			alert(kony.i18n.getLocalizedString("ECGenericError"));
		}
	}
}*/
/*function IBcrmProfileInqForceChange() {
	inputParam = {};
 	invokeServiceSecureAsync("crmProfileInq", inputParam, IBcrmProfileInqForcechangeCallback)
}*/
/*function IBcrmProfileInqForcechangeCallback(status, resulttable) {
	if (status == 400) {
		if (resulttable["opstatus"] == 0)
		{
			gblEmailId = resulttable["emailAddr"];
 		}else{
 			dismissLoadingScreenPopup();
 		}
	}
}*/

/**************************************************************************************
		Module	: invokeRequestOtpChngPWD
		Author  : Kony
		Date    : July 21, 2013
		Purpose : OTP while changePassword
*****************************************************************************************/

function invokeRequestOtpForceChngPWD() {
	if (gblOTPFlag) {
		frmIBForceChangePassword.txtOTP.text = "";
	var inputParams = {}
	/*if (kony.i18n.getCurrentLocale() == "en_US") {
		inputParams["eventNotificationPolicyId"] = "MIB_ChangeIBPWD_EN";
		inputParams["SMS_Subject"] = "MIB_ChangeIBPWD_EN";
	} else {
		inputParams["eventNotificationPolicyId"] = "MIB_ChangeIBPWD_TH";
		inputParams["SMS_Subject"] = "MIB_ChangeIBPWD_TH";
	}*/
	inputParams["Channel"] = "ChangeUserPassword";
	inputParams["locale"] = kony.i18n.getCurrentLocale();
	inputParams["retryCounterRequestOTP"] = gblRetryCountRequestOTP;
 	invokeServiceSecureAsync("generateOTPWithUser", inputParams, callBackReqOtpForceChngPWD);
 	}
}





/**************************************************************************************
		Module	: callBackReqOtpChngPWD
		Author  : Kony
		Date    : July 21, 2013
		Purpose : OTP Response while changePassword
*****************************************************************************************/

function callBackReqOtpForceChngPWD(status, resultable) {
	if (status == 400) {
		if (resultable["errCode"] == "GenOTPRtyErr00002") {
				dismissLoadingScreenPopup();		
				gblOTPFlag = false;	
				frmIBCMConfirmationPwd.btnOTPreq.skin = btnIB158disabled;
			    frmIBCMConfirmationPwd.btnOTPreq.setEnabled(false);
			    kony.timer.cancel("PwdOTPTimer");
				showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00002"), kony.i18n.getLocalizedString("info"));
				return false;
			}
			else if (resultable["errCode"] == "GenOTPRtyErr00001") {
                    dismissLoadingScreenPopup();
                    showAlertIB(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
                    return false;
           	}
		if (resultable["opstatus"] == 0) {
			
			var reqOtpTimer = kony.os.toNumber(resultable["requestOTPEnableTime"]);
			var uuid = kony.os.toNumber(resultable["uuid"]);
			var pac = kony.os.toNumber(resultable["pac"]);
			var expirydate = kony.os.toNumber(resultable["expirydate"]);
			gblOTPLENGTH = kony.os.toNumber(resultable["otpLength"]);
			frmIBForceChangePassword.txtOTP.maxTextLength = gblOTPLENGTH;
			gblRetryCountRequestOTP = resultable["retryCounterRequestOTP"];
			frmIBForceChangePassword.btnRequest.skin = btnIB158disabled;
			frmIBForceChangePassword.btnRequest.setEnabled(false);
			dismissLoadingScreenPopup();		
			frmIBForceChangePassword.lblOTPNumber.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
			frmIBForceChangePassword.hboxOTPButtons.setVisibility(true);
			frmIBForceChangePassword.hboxOTP.setVisibility(true);
			frmIBForceChangePassword.btnConfirm.setVisibility(false);
			var refVal="";
			for(var d=0;d<resultable["Collection1"].length;d++){
				if(resultable["Collection1"][d]["keyName"] == "pac"){
						refVal=resultable["Collection1"][d]["ValueString"];
						break;
					}
				}
		    kony.timer.schedule("PwdOTPTimer", forcePwdChngOTPTimerCallBack, reqOtpTimer, false);
			frmIBForceChangePassword.lblRefNo.text = refVal;
			gblOTPFlag = false;			
		} else {
			dismissLoadingScreenPopup();		
			gblRetryCountRequestOTP = resultable["retryCounterRequestOTP"];
			alert("Error " + resultable["errmsg"]);
		}
	} else {
		if (status == 300) {
			gblRetryCountRequestOTP = resultable["retryCounterRequestOTP"];
			dismissLoadingScreenPopup();
			alert("Error");
		}
	}
}
function forcePwdChngOTPTimerCallBack() {
	frmIBForceChangePassword.btnRequest.skin = btnIB158active;
	frmIBForceChangePassword.btnRequest.setEnabled(true);
	frmIBForceChangePassword.btnRequest.onClick = invokeRequestOtpForceChngPWD;
	//kony.timer.schedule("PwdOTPTimer", forcePwdChngOTPTimerCallBack, reqOtpTimer, false);
	gblOTPFlag = true;
	try {
		kony.timer.cancel("PwdOTPTimer")
	} catch (e) {
		
	}
}

/**************************************************************************************
		Module	: srvVerifyOTPIB
		Author  : Kony
		Date    : July 21, 2013
		Purpose : VerifyOTP  while changePassword
*****************************************************************************************/

/*function srvVerifyOTPForceChangeIB() {
	showLoadingScreenPopup();		
	var inputParams = {}
  	inputParams["password"] = frmIBForceChangePassword.txtOTP.text;
	//inputParams["userId"] = gblUserName;
	inputParams["retryCounterVerifyOTP"] = gblVerifyOTPCounter;
	inputParams["oldPassword"] = frmIBForceChangePassword.txtUserID.text;
	inputParams["newPassword"] = frmIBForceChangePassword.txtConfirmPassword.text;
	inputParams["OTP_TOKEN_FLAG"] = "OTP";
 	invokeServiceSecureAsync("ForceChangePasswordExecute", inputParams, callBackVerifyOTPForceChangeIB);
}*/
/**************************************************************************************
		Module	: callBackVerifyOTPIB
		Author  : Kony
		Date    : July 21, 2013
		Purpose : VerifyOTP Response  while changePassword
*****************************************************************************************/

//function callBackVerifyOTPForceChangeIB(status, resultable) {
//	if (status == 400) {
//	
//		if (resultable["opstatusVPX"] == 0) {
// 			var inputParam = {}
//			/*inputParam["loginModuleId"] = "IB_Pwd";
//			inputParam["oldPassword"] = frmIBForceChangePassword.txtUserID.text;
//			inputParam["newPassword"] = frmIBForceChangePassword.txtConfirmPassword.text;
//			gblVerifyOTPCounter = "0";
//			gblIBRetryCountRequestOTP["chngIBPwd"] = "0";
//			invokeServiceSecureAsync("changePassword", inputParam, callbackForceChangePassword);*/
//			callbackForceChangePassword(resultable);
//		} else if(resultable["errCode"]!= null && resultable["errCode"] == "10403") {
//				frmIBForceChangePassword.btnRequest.skin = btnIB158disabled;
//				frmIBForceChangePassword.btnRequest.setEnabled(false);
//				popIBBPOTPLocked.show();
//				gblVerifyOTPCounter = "0";
//				gblIBRetryCountRequestOTP["chngIBPwd"] = "0";
//				//var inputParam = {}
//				//inputParam["ibUserId"] = gblUserName;
//				//inputParam["ibUserStatusId"] = "04";
//				//inputParam["actionType"] = "32";
//				frmIBForceChangePassword.txtOTP.text = "";
//				dismissLoadingScreenPopup();
//				IBLogoutServiceForceChng();		
//				//doing crmprofilemod for otp lock in service postprocessor
//				//invokeServiceSecureAsync("crmProfileMod", inputParam, CallBackCrmProfilemodforceChangeLogout)
//		}else if(resultable["errCode"]!= null && resultable["errCode"] == "10020"){
//			dismissLoadingScreenPopup();		
//			frmIBForceChangePassword.label507353026213454.text = kony.i18n.getLocalizedString("wrongOTP");
//			frmIBForceChangePassword.txtOTP.text = "";
//			gblVerifyOTPCounter = resultable["retryCounterVerifyOTP"];
//		}
//		else{
//						alert(resultable["errmsg"]);
//						frmIBForceChangePassword.txtOTP.text = "";
//						dismissLoadingScreenPopup();		
//			}
//	}
//}
/**************************************************************************************
		Module	: callbackChangePassword
		Author  : Kony
		Date    : July 21, 2013
		Purpose : ECAS ChangePwd & notifying user once after changePassword
*****************************************************************************************/

/*function callbackForceChangePassword(resultable) {
		if (resultable["opstatusPWDC"] == 0) {
				IBLogoutServiceForceChng();
				dismissLoadingScreenPopup();		
				//frmIBPreLogin.show();
		} else {
			if (resultable["errCode"]!= null && resultable["errCode"] == "10403")	{
				frmIBForceChangePassword.txtOTP.text = "";
				popIBTransNowOTPLocked.btnClose.onClick = lockIBPwdChngOTpLockCrmUpdate;
				dismissLoadingScreenPopup();		
				popIBTransNowOTPLocked.show();
			}else{
				frmIBForceChangePassword.txtOTP.text = "";
				dismissLoadingScreenPopup();
				alert(resultable["errmsg"]);
			}
		}
}*/

function invokeMigrationUserComposite(currPwd,newPwd,userId){

 var inputParam = {};
 inputParam["currPwd"] = currPwd;
 inputParam["newPwd"] = newPwd;
 inputParam["newuserId"] = userId;
 inputParam["modifyUserErrorAttempt"] = gblModifyUserErr;
 showLoadingScreenPopup();
 invokeServiceSecureAsync("ForceChangePasswordExecute", inputParam, callBackCompCMigUser);

}


function callBackCompCMigUser(status,resulttable){

	if(status == 400){
		if(resulttable["opstatus"] == 0){
				frmIBFirstTimeActivationComplete.image2507382817236580.src = "iconcomplete.png";
				frmIBFirstTimeActivationComplete.label507382817236581.text = kony.i18n.getLocalizedString("keyIBFirstActiveSucces");
				frmIBFirstTimeActivationComplete.button507382817236587.setVisibility(true);
				gblIsFromMigration = true;
				gblActionCode = "04";
				IBsendTnCMailPartyInq();
				frmIBFirstTimeActivationComplete.show();
		}else if (resulttable["errCode"] == "validationErr") {
		  			
                    if(resulttable["errorKey"] == "keyUserIdUnderRestrictedList"){
                    	dismissLoadingScreenPopup();
                    	showAlertIB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
                    	frmIBForceChangePassword.txtUserId.text = "";
                    	frmIBForceChangePassword.txtUserId.setFocus(true);
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyRepetativeChars"){
                    	dismissLoadingScreenPopup();
                    	showAlertIB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
                    	frmIBForceChangePassword.txtUserId.setFocus(true);
                    	frmIBForceChangePassword.txtUserId.text = "";
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyWrongNewUserId"){
                    	dismissLoadingScreenPopup();
                    	showAlertIB(kony.i18n.getLocalizedString("keyUserIdMinRequirement"), kony.i18n.getLocalizedString("info"));
                    	frmIBForceChangePassword.txtUserId.setFocus(true);
                    	frmIBForceChangePassword.txtUserId.text = "";
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyPassSettingGuidelinesIB"){
                    	dismissLoadingScreenPopup();
                    	showAlertIB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
                    	frmIBForceChangePassword.txtPassword.text = "";
                    	frmIBForceChangePassword.txtConfirmPassword.text = "";
                    	frmIBForceChangePassword.txtPassword.setFocus(true);
                    	return false;
                    }
                    if(resulttable["errorKey"] == "keyWrongPwd"){
                    	dismissLoadingScreenPopup();
                    	showAlertIB(kony.i18n.getLocalizedString("invalidPasswordIB"), kony.i18n.getLocalizedString("info"));
                    	frmIBForceChangePassword.txtPassword.setFocus(true);
                    	frmIBForceChangePassword.txtPassword.text = "";
                    	frmIBForceChangePassword.txtConfirmPassword.text = "";
                    	return false;
                    }
                    
		}else if(resulttable["errCode"] == "VrfyAcPWDErr00001"){
			dismissLoadingScreenPopup();
			showAlertIB(kony.i18n.getLocalizedString("keyECUserNotFound"), kony.i18n.getLocalizedString("info"));
			frmIBForceChangePassword.txtCurrentPwd.setFocus(true);
			return false;
		}else if(resulttable["errCode"] == "VrfyAcPWDErr00002"){
			dismissLoadingScreenPopup();
			showAlertIB(kony.i18n.getLocalizedString("keyInCrtCurPwd"), kony.i18n.getLocalizedString("info"));
			frmIBForceChangePassword.txtCurrentPwd.text = "";
			frmIBForceChangePassword.txtCurrentPwd.setFocus(true);
			return false;
		}else if(resulttable["errCode"] == "VrfyTxPWDErr00003"){
			 dismissLoadingScreenPopup();
			 showAlertIB(kony.i18n.getLocalizedString("keyInCrtCurPwdLockd"), kony.i18n.getLocalizedString("info"));
			 IBLogoutService();
			 return false;
		}else if(resulttable["errCode"] == "verifyPwdOtherErr"){
			 dismissLoadingScreenPopup();
			 showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			 frmIBForceChangePassword.txtCurrentPwd.setFocus(true);
			 return false;
		}else if(resulttable["errCode"] == "modifyUserErr"){
			 dismissLoadingScreenPopup();
			 gblModifyUserErr = resulttable["retryModifyUser"]
				if(resulttable["errMsg"] != null && resulttable["errMsg"] != ""){
				showAlertIB(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
				}else{
				showAlertIB(kony.i18n.getLocalizedString("keyModifyUserMigErr"), kony.i18n.getLocalizedString("info"));
				}
			 
			 frmIBForceChangePassword.txtUserId.setFocus(true);
			 return false;
		}else if(resulttable["errCode"] == "modifyUserErrLimitReached"){
			 dismissLoadingScreenPopup();
			 showAlertIB(kony.i18n.getLocalizedString("keyIBLoginError003"), kony.i18n.getLocalizedString("info"));
			 IBLogoutService();
			 return false;
		}else if(resulttable["errCode"] == "resetPwdErr"){
			 dismissLoadingScreenPopup();
			 if(resulttable["errMsg"] != null && resulttable["errMsg"] != ""){
				showAlertIB(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
				}else{
				showAlertIB(kony.i18n.getLocalizedString("keyResetPwdMigUser"), kony.i18n.getLocalizedString("info"));
				}
			 frmIBForceChangePassword.txtPassword.setFocus(true);
			 return false;
		}else{
		 	dismissLoadingScreenPopup();
			 showAlertIB(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			 return false;
		}
		
	
	}

}