
function callloginekycService(){
    showLoadingScreen();
  var  inputParam = {};
  
  inputParam["deviceId"] = getDeviceID();
  kony.print("FPRINT: uniqueID is deviceId= " + getDeviceID());
  inputParam["osDeviceId"] = getDeviceID();
  channelId = "02";
  if (GLOBAL_MB_CHANNEL != null && GLOBAL_MB_CHANNEL != "") {
    channelId = GLOBAL_MB_CHANNEL;
  }
  inputParam["loginChannel"] = channelId;
  //glbAccessPin="010203";
  inputParam["password"] = encryptData(glbAccessPin);
 // gblLoginType = "Login";
  inputParam["deviceOS"] = getDeviceOS();
    var locale = kony.i18n.getCurrentLocale();
    kony.print("locale in LoginCompositeSingleService @@: "+locale);
    if (locale == "en_US") {
      inputParam["languageCd"] = "EN";
    } else {
      inputParam["languageCd"] = "TH";
    }
  invokeServiceSecureAsync("EKYCLoginJavaService", inputParam, callBackekycLogin)
}

function callBackekycLogin(status, resulttable) {
   kony.print("In New Login Optimisation Code callBackMBLoginAuthNew%%%%%%%%")
    kony.print("FPRINT: status=" + status);
    kony.print("FPRINT: resulttable[opstatus]=" + resulttable["opstatus"]);
    kony.print("FPRINT: resulttable[deviceStatus]=" + resulttable["deviceStatus"]);
    kony.print("FPRINT: resulttable[errMsg]=" + resulttable["errMsg"]);
    kony.print("FPRINT: resulttable[errCode]=" + resulttable["errCode"]);
   resetPINeKYC();
  gbleKYCAccessPINFlow="";
  kony.application.dismissLoadingScreen();
    if (status == 400) {
        if (isNotBlank(resulttable["errCode"])) {
            if (resulttable["errCode"] == "XPErr0001") {
                kony.print("FPRINT 1 SETTING isUserStatusActive TO FALSE");
                kony.print("FPRINT 1 setting isUserStatusActive to false");
                kony.store.setItem("isUserStatusActive", false);
                isUserStatusActive = false;
                onClickChangeTouchIcon();
                popupAccesesPinLocked.lblPopupConfText.text = kony.i18n.getLocalizedString("acclockmsg");
                popupAccesesPinLocked.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
                popupAccesesPinLocked.btnCallTMB.text = kony.i18n.getLocalizedString("keyCall");
                popupAccesesPinLocked.show();
                //showAlert(kony.i18n.getLocalizedString("accLock"), kony.i18n.getLocalizedString("info"));
                resetValues();
                kony.application.dismissLoadingScreen();
                return false;
            }
        }
        else if (resulttable["opstatus"] == 0) {
          
          	if(resulttable["flow_status"] == "EXPIRED"){
              	kony.store.removeItem("eKYCRegistrationFlag");
          		kony.store.setItem("eKYCRegistrationFlag", "N");
              	refresheKYCStartup();
                frmeKYCStartUp.show();
            }else if(resulttable["flow_status"] == "REG_USER"){
               gbleKYCStep="3";
               navigateKYCIntroduction();
            }else if(resulttable["flow_status"] == "SEL_IDP"){
               getIdentityproviderService();
            }else if(resulttable["flow_status"] == "IDP_REQ"){
               getIdentityproviderService();
            }else if(resulttable["flow_status"] == "FATCA"){
               emailVerificationMB("eKYC");
            }else if(resulttable["flow_status"] == "VER_EMAIL"){
              navigatefrmMBeKYCContactInfo();
            }else if(resulttable["flow_status"] == "CONTACT_INFO"){
             getPersonalInfoDatafromService();
            }else if(resulttable["flow_status"] == "PERSONAL_INFO" 
                     	|| resulttable["flow_status"] == "PARTYADD"
                    	|| resulttable["flow_status"] == "PARTYMOD"
                     	|| resulttable["flow_status"] == "CRM_RELATION"
                    	|| resulttable["flow_status"] == "FATCA_ADD"
                    	|| kony.string.startsWith(resulttable["flow_status"], "ACCTGEN")
                    	|| kony.string.startsWith(resulttable["flow_status"], "PARTYACCTREL")
                    	|| kony.string.startsWith(resulttable["flow_status"], "ACCTADD")
                    	|| kony.string.startsWith(resulttable["flow_status"], "OPEN_ACCT")
                    	|| kony.string.startsWith(resulttable["flow_status"], "ISSUE_CARD")){
            	frmMBeKycEmarketConsence.show();
            }
        }else {
           frmMBeKYCAccessPin.lblMessage.text=kony.i18n.getLocalizedString("ECVrfyAccPinErr00001");
           frmMBeKYCAccessPin.flxMsgNotification.setVisibility(true);
          animateIdAndSelfieImages(frmMBeKYCAccessPin.flxArrow,45);
          resetValues();
    	}
    } else {
            dismissLoadingScreen();
          showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
          return false;
    }
}

