//Starting frmMyRecipientAddAcc.kl
function frmMyRecipientAddAccMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientAddAcc.txtNickname.skin = txtNormalBG;
	    frmMyRecipientAddAcc.txtAccNo.skin = txtNormalBG;
	    frmMyRecipientAddAcc.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    //frmMyRecipientAddAcc.btnBanklist.setVisibility(true)
	    //frmMyRecipientAddAcc.btnBanklist.text="Bank";
	    frmMyRecipientAddAccPreShow.call(this);
	    //frmMyRecipientAddAcc.btnBanklist.text="Bank";
	    frmMyRecipientAddAcc.txtAccNo.text = "";
	    frmMyRecipientAddAcc.txtNickname.text = "";
	    frmMyRecipientAddAcc.tbxAccName.text = "";
	    frmMyRecipientAddAcc.hbxAccName.setVisibility(false);
	    gblSelectedBankAccLength = 10;
	    /* 
	 
	
	    */
	    frmMyRecipientAddAcc.btnBanklist.text = kony.i18n.getLocalizedString("keybtnPleaseSelect");
	    DisableFadingEdges.call(this, frmMyRecipientAddAcc);	
	}
}
function frmMyRecipientAddAccMenuPostshow(){
    if (gblCallPrePost) {
         frmMyRecipientAddAcc.btnBanklist.text = kony.i18n.getLocalizedString("keyBank");
	    frmMyRecipientAddAcc.txtAccNo.text = "";
	    frmMyRecipientAddAcc.txtNickname.text = "";

    }
    assignGlobalForMenuPostshow();
}


//Starting frmMyRecipientAddAccComplete.kl
function frmMyRecipientAddAccCompleteMenuPreshow(){
	if (gblCallPrePost) 
	{
	 	frmMyRecipientAddAccComplete.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    var setupTblTap = {
	        fingers: 1,
	        swipedistance: 50,
	        swipevelocity: 75
	    }
	    var tapGesture = frmMyRecipientAddAccComplete.vboxRight.setGestureRecognizer(2, setupTblTap, myTap);
	    if (flowSpa) {
	        swipeEnable = true;
	        var setupTblTap = {
	            fingers: 1,
	            swipedistance: 50,
	            swipevelocity: 75
	        }
	        swipeGesture = frmMyRecipientAddAccComplete.sboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
	        swipeGesture = frmMyRecipientAddAccComplete.scrollboxLeft.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
	    }
	    frmMyRecipientAddAccCompletePreShow.call(this);
	    DisableFadingEdges.call(this, frmMyRecipientAddAccComplete);	

	}
}
function frmMyRecipientAddAccCompleteMenuPostshow(){
    if (gblCallPrePost) {
	    frmMyRecipientAddAccComplete.scrollboxMain.scrollToEnd();
	    campaginService.call(this, "image2447443295186", "frmMyRecipientAddAccComplete", "M");
    }
    assignGlobalForMenuPostshow();
}


//Starting frmMyRecipientAddAccConf.kl
function frmMyRecipientAddAccConfMenuPreshow(){
	if (gblCallPrePost) 
	{
	 	frmMyRecipientAddAccConf.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMyRecipientAddAccConfPreShow.call(this);
	    /* 
	myRecAccAddConfLoad.call(this);
	
	 */
	    /* 
	var tempRecord = {
	 "lblBankName": {
	 "text": frmMyRecipientAddAcc.comboBank.selectedKey
	 },
	 "btnFav": {
	 "skin": "btnStar"
	 },
	 "lblAccountNo": {
	 "text": frmMyRecipientAddAcc.txtAccNo.text
	 },
	 "lblNick": {
	 "text": frmMyRecipientAddAcc.txtNickname.text
	 },
	 "lblAcctName": {
	 "text": ""
	 }
	 }
	
	AddAccountList.push(tempRecord);
	AddAccountList.sort(dynamicSort("lblNick"));
	
	frmMyRecipientAddAccConf.segMyRecipientDetail.removeAll();
	frmMyRecipientAddAccConf.segMyRecipientDetail.setData(AddAccountList);
	
	if(isRecipientNew){
	 frmMyRecipientAddAccConf.lblName.text = frmMyRecipientAddProfile.tbxRecipientName.text;
	 frmMyRecipientAddAccConf.lblMobile.text = frmMyRecipientAddProfile.tbxMobileNo.text;
	 frmMyRecipientAddAccConf.lblEmail.text = frmMyRecipientAddProfile.tbxEmail.text;
	 frmMyRecipientAddAccConf.lblFbIDstudio5.text = frmMyRecipientAddProfile.tbxFbID.text;//Modified by Studio Viz
	 frmMyRecipientAddAccConf.imgprofilepic.src = frmMyRecipientAddProfile.imgProfilePic.src;
	}else{
	 for(var i=0; i<(myRecipientsRs.length-1); i++){
	 if(myRecipientsRs[i]["lblName"] == currentRecipient){
	 frmMyRecipientAddAccConf.lblName.text = myRecipientsRs[i]["lblName"];
	 frmMyRecipientAddAccConf.lblMobile.text = myRecipientsRs[i]["lblMob"];
	 frmMyRecipientAddAccConf.lblEmail.text = myRecipientsRs[i]["lblEmail"];
	 frmMyRecipientAddAccConf.lblFbIDstudio5.text = myRecipientsRs[i]["lblFb"];//Modified by Studio Viz
	 frmMyRecipientAddAccConf.imgprofilepic.text = myRecipientsRs[i]["imgprofilepic"];
	 }
	 }
	}
	
	if(AddAccountList.length>=2){
	 frmMyRecipientAddAccConf.btnAddAcc.onClick=null;
	}else{
	 frmMyRecipientAddAccConf.btnAddAcc.onClick=btnAddAccOnclick;
	}
	
	function btnAddAccOnclick(){
	 frmMyRecipientAddAcc.show();
	}
	
	 */
	    DisableFadingEdges.call(this, frmMyRecipientAddAccConf);	
	}
}
function frmMyRecipientAddAccConfMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}


//Starting frmMyRecipientAddProfile.kl
function frmMyRecipientAddProfileMenuPreshow(){
	if (gblCallPrePost) 
	{
		   frmMyRecipientAddProfile.tbxRecipientName.skin = txtNormalBG;
		    frmMyRecipientAddProfile.tbxMobileNo.skin = txtNormalBG;
		    frmMyRecipientAddProfile.tbxEmail.skin = txtNormalBG;
		    frmMyRecipientAddProfile.tbxFbID.skin = txtNormalBG;
		    frmMyRecipientAddProfile.scrollboxMain.scrollToEnd();
		    /* 
		gblIndex = -1;
		isMenuShown = false;
		isSignedUser = true;
		
		
		 */
		    frmMyRecipientAddProfilePreShow.call(this);
		    /* 
		frmMyRecipientAddProfile.tbxEmail.text = ""
		frmMyRecipientAddProfile.tbxMobileNo.text = ""
		frmMyRecipientAddProfile.tbxRecipientName.text = ""
		frmMyRecipientAddProfile.tbxFbID.text = ""
		
		 */
		    dontChangeFBID = frmMyRecipientAddProfile.tbxFbID.text;
		    gblRcBase64List = "";
		    gblMixedCallsTrack1 = "";
		    if (flowSpa) {
		        frmMyRecipientAddProfile.btnMobileNumber.setVisibility(false);
		    } else {
		        frmMyRecipientAddProfile.btnMobileNumber.setVisibility(true);
		    }
		    if (gblRequestFromForm == "addprofile" || gblRequestFromForm == "addaccount") {
		        //DO NOTHING
		    } else {
		        frmMyRecipientAddProfile.tbxRecipientName.text = "";
		        frmMyRecipientAddProfile.tbxMobileNo.text = "";
		        frmMyRecipientAddProfile.tbxEmail.text = "";
		    }
		    DisableFadingEdges.call(this, frmMyRecipientAddProfile);	
	}
}
function frmMyRecipientAddProfileMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}


//Starting frmMyRecipientAddProfileComp.kl
function frmMyRecipientAddProfileCompMenuPreshow(){
	if (gblCallPrePost) 
	{
		frmMyRecipientAddProfileComp.scrollboxMain.scrollToEnd();
	    frmMyRecipientAddProfileCompPreShow.call(this);
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmMyRecipientAddProfileComp);	
	}
}
function frmMyRecipientAddProfileCompMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientDetail.kl
function frmMyRecipientDetailMenuPreshow(){
	if (gblCallPrePost) 
	{
		frmMyRecipientDetail.scrollboxMain.scrollToEnd();
		    gblIndex = -1;
		    isMenuShown = false;
		    isSignedUser = true;
		    frmMyRecipientDetail.lblName.text = "";
		    frmMyRecipientDetail.lblEmail.text = "";
		    frmMyRecipientDetail.lblMobile.text = "";
		    frmMyRecipientDetail.lblFbstudio3.text = "";//Modified by Studio Viz
		    frmMyRecipientDetail.lblFBstudio4.text = "";//Modified by Studio Viz
		    frmMyRecipientDetail.label47502979411852.text = "";
		    frmMyRecipientDetail.label47502979412689.text = "";
		    frmMyRecipientDetail.imgprofilepic.src = "avatar_dis.png";
		    frmMyRecipientDetail.segMyRecipientDetail.data = {};
		    /* 
		frmMyRecipientDetail.segMyRecipientDetail.data = [{
		 
		 "lblBank" : kony.i18n.getLocalizedString("keyBank")},
		 {"lblNumber" : kony.i18n.getLocalizedString("keyNumber")},
		 {"lblNickName" : kony.i18n.getLocalizedString("Nickname")},
		 {"lblAccountName" : kony.i18n.getLocalizedString("AccountName")}
		 
		 
		 ];
		
		 */
		    /* 
		var i = 0;
		if(originalName){
		 var name = frmMyRecipientEditProfile.tbxRecipientName.text;
		 frmMyRecipientDetail.lblName.text=name;
		 originalName=false;
		}
		else{
		 var name = frmMyRecipients["segMyRecipient"]["selectedItems"][0]["lblName"]["text"];
		 kony.print("1111111111111111111111111111111111111111111111111111111:"+name);
		 frmMyRecipientDetail.lblName.text = name;
		 //frmMyRecipientDetail.lblName.text=name;
		}
		frmMyRecipientDetail.hbox47502979411849.setVisibility(true)
		frmMyRecipientDetail.hbox47502979411850.setVisibility(true)
		frmMyRecipientDetail.hbox47502979411851.setVisibility(true)
		for (i in myRecipientsRs) {
		 if (myRecipientsRs[i]["lblName"] == name) {
		 //get current recipient details
		 currentRecipientDetails["lblName"] = myRecipientsRs[i]["lblName"];
		 currentRecipientDetails["imgprofilepic"] = myRecipientsRs[i]["imgprofilepic"];
		 currentRecipientDetails["lblAccountNum"] = myRecipientsRs[i]["lblAccountNum"];
		 currentRecipientDetails["lblMob"] = myRecipientsRs[i]["lblMob"];
		 currentRecipientDetails["lblEmail"] = myRecipientsRs[i]["lblEmail"];
		 currentRecipientDetails["lblFb"] = myRecipientsRs[i]["lblFb"];
		 kony.print("[][][][][][][][][][]:"+currentRecipientDetails["lblFb"]);
		 //to hide mobile field accordingly
		 if (!(myRecipientsRs[i]["lblMob"])) {
		 kony.print(myRecipientsRs[i]["lblMob"]);
		 frmMyRecipientDetail.hbox47502979411849.isVisible = false;
		 } else {
		 frmMyRecipientDetail.hbox47502979411849.isVisible = true;
		 var ph = myRecipientsRs[i]["lblMob"];
		 ph = addDashPh(ph);
		 //frmMyRecipientDetail.lblMobile.text = myRecipientsRs[i]["lblMob"];
		 frmMyRecipientDetail.lblMobile.text = ph;
		 }
		 //to hide email field accordingly
		 if (!(myRecipientsRs[i]["lblEmail"])) {
		 kony.print(myRecipientsRs[i]["lblEmail"]);
		 frmMyRecipientDetail.hbox47502979411850.isVisible = false;
		 } else {
		 frmMyRecipientDetail.hbox47502979411850.isVisible = true;
		 frmMyRecipientDetail.lblEmail.text = myRecipientsRs[i]["lblEmail"];
		 }
		 //to hide facebookID field accordingly
		 if (!(myRecipientsRs[i]["lblFb"])) {
		 kony.print(myRecipientsRs[i]["lblFb"]);
		 frmMyRecipientDetail.hbox47502979411851.isVisible = false;
		 } else {
		 frmMyRecipientDetail.hbox47502979411851.isVisible = true;
		 frmMyRecipientDetail.lblFbstudio3.text = myRecipientsRs[i]["lblFb"];//Modified by Studio Viz
		 }
		 if(myRecipientsRs[i]["imgprofilepic"]==""){
		 kony.print(myRecipientsRs[i]["imgprofilepic"]);
		 frmMyRecipientDetail.imgprofilepic.src = "avatar_dis.png";
		 } else {
		 frmMyRecipientDetail.imgprofilepic.src = myRecipientsRs[i]["imgprofilepic"];
		 }
		 }
		}
		
		 */
		    /* 
		populateRecipientAccounts.call(this,eventobject);
		
		 */
		    /* 
		populateRecipientDetails.call(this);
		
		 */
		    DisableFadingEdges.call(this, frmMyRecipientDetail);
      
      frmMyRecipientDetail.lblHdrTxt.text = kony.i18n.getLocalizedString("keyMyRecipients"); 
      frmMyRecipientDetail.backBtnrecpdetails.text = kony.i18n.getLocalizedString("Back");

	}
}
function frmMyRecipientDetailMenuPostshow(){
    if (gblCallPrePost) {
        frmMyRecipientsAccountListing.call(this);

    }
    assignGlobalForMenuPostshow();
}



//Starting frmMyRecipientEditAccComplete.kl
function frmMyRecipientEditAccCompleteMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientEditAccComplete.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmMyRecipientEditAccComplete);
	}
}
function frmMyRecipientEditAccCompleteMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}




//Starting frmMyRecipientEditAccount.kl
function frmMyRecipientEditAccountMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientEditAccount.txtAccountNickName.skin = tbxPopupBlue;
	    frmMyRecipientEditAccount.scrollboxMain.scrollToEnd();
	    frmMyRecipientEditAccountPreShow.call(this);
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmMyRecipientEditAccount);

	}
}
function frmMyRecipientEditAccountMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}


//Starting frmMyRecipientEditProfile.kl
function frmMyRecipientEditProfileMenuPreshow(){
	if (gblCallPrePost) 
	{
	 	frmMyRecipientEditProfile.tbxRecipientName.skin = txtNormalBG;
	    frmMyRecipientEditProfile.tbxEmail.skin = txtNormalBG;
	    frmMyRecipientEditProfile.tbxFbID.skin = txtNormalBG;
	    frmMyRecipientEditProfile.tbxMobileNo.skin = txtNormalBG;
	    frmMyRecipientEditProfile.scrollboxMain.scrollToEnd();
	    frmMyRecipientEditProfilePreShow.call(this);
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    originalName = frmMyRecipientEditProfile.tbxRecipientName.text;
	    dontChangeFBID = frmMyRecipientEditProfile.tbxFbID.text;
	    gblRcBase64List = "";
	    if (flowSpa) {
	        frmMyRecipientEditProfile.btnFaceBook.setVisibility(false);
	    } else {
	        frmMyRecipientEditProfile.btnFaceBook.setVisibility(true);
	    }
	    DisableFadingEdges.call(this, frmMyRecipientEditProfile);
	}
}
function frmMyRecipientEditProfileMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}


//Starting frmMyRecipientEditProfileComp.kl
function frmMyRecipientEditProfileCompMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientEditProfileComp.scrollboxMain.scrollToEnd();
	    frmMyRecipientEditProfileCompPreShow.call(this);
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmMyRecipientEditProfileComp);	
	}
}
function frmMyRecipientEditProfileCompMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}




//Starting frmMyRecipients.kl
function frmMyRecipientsMenuPreshow(){
  try{
    kony.print("@@@ In frmMyRecipientsMenuPreshow() @@@");
	if (gblCallPrePost) 
	{
	    frmMyRecipients.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMyRecipientsPreShow.call(this);
	    frmMyRecipients.textbox247327209467957.skin = "txtSearchbox";
	    //frmMyRecipients.textbox247327209467957.placeholder=kony.i18n.getLocalizedString('keySearch');
	    frmMyRecipients.segMyRecipient.data = {};
	    frmMyRecipients.hboxaddfblist.isVisible = true;
	    frmMyRecipients.btnRight.skin = "btnMyRecipient"
	    frmMyRecipients.imgHeaderMiddle.src = "arrowtopblue.png"
	    frmMyRecipients.imgHeaderRight.src = "empty.png"
	    gblMixedCallsTrack = "";
	    frmMyRecipients.hbox474969373109363.setVisibility(true);
	    frmMyRecipients.hbox86679448253627.setVisibility(false);
	    DisableFadingEdges.call(this, frmMyRecipients);	
	}
  }catch(e){
    kony.print("@@@ In frmMyRecipientsMenuPreshow() Exception:::"+e);
  }
}
function frmMyRecipientsMenuPostshow(){
  try{
    kony.print("@@@ In frmMyRecipientsMenuPostshow() @@@");
    if (gblCallPrePost) {
        frmMyRecipientsListing.call(this);
	    isChRecipientAccountsRs = true;
	    isSearched = false;
	    getMBMyRecepientBankListCache.call(this);
    }
    assignGlobalForMenuPostshow();
  }catch(e){
    kony.print("@@@ In frmMyRecipientsMenuPostshow() Exception::;"+e);
  }
}

//Starting frmMyRecipientSelectContacts.kl
function frmMyRecipientSelectContactsMenuPreshow(){
	if (gblCallPrePost) 
	{
	   frmMyRecipientSelectContactsPreShow.call(this);
	    frmMyRecipientSelectContacts.scrollboxMain.scrollToEnd();
	    /* 
	multipleSelectRecipientData.sort(dynamicSort("lblName"));
	
	frmMyRecipientMultipleSelect.segMyRecipient.widgetDataMap = { lblName:"lblName",lblMobile:"lblMobile",lblEmail:"lblEmail",imgprofilepic:"imgprofilepic",
	 imgchbxbtn:"imgchbxbtn" }
	
	frmMyRecipientMultipleSelect.segMyRecipient.setData(multipleSelectRecipientData);
	
	 */
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMyRecipientSelectContacts.textbox286685406430472.skin = "txtSearchbox";
	    if (gblEditContactList == false) frmMyRecipientSelectContacts.segMyRecipient.removeAll();
	    frmMyRecipientSelectContacts.textbox286685406430472.text = "";
	    frmMyRecipientSelectContacts.label475095112172022.setVisibility(false);
	    frmMyRecipientSelectContacts.segMyRecipient.setVisibility(true)
	        //frmMyRecipientSelectContacts.label475095112172022.text=kony.i18n.getLocalizedString("NoContacts");
	    DisableFadingEdges.call(this, frmMyRecipientSelectContacts);	
	}
}
function frmMyRecipientSelectContactsMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
	    /* 
	populateContacts.call(this);
	
	 */
	    kony.print("gblEditContactList " + gblEditContactList);
	    if (gblEditContactList == false) {
	        kony.print("not editing")
	        populateContacts();
	    } else {
	        kony.print("editing")
	    }
    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientSelectContactsComp.kl
function frmMyRecipientSelectContactsCompMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientSelectContactsComp.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMyRecipientSelectContactsCompPreShow.call(this);
	    DisableFadingEdges.call(this, frmMyRecipientSelectContactsComp);

	}
}
function frmMyRecipientSelectContactsCompMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
		campaginService.call(this, "image2447443295186", "frmMyRecipientSelectContactsComp", "M");
    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientSelectContactsConf.kl
function frmMyRecipientSelectContactsConfMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientSelectContactsConf.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    //selectedContactsfnComp();
	    //frmMyRecipientSelectContactsConf.segMyRecipientDetail.setData(multiSelectedContacts);
	    DisableFadingEdges.call(this, frmMyRecipientSelectContactsConf);

	}
}
function frmMyRecipientSelectContactsConfMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientSelectFacebook.kl
function frmMyRecipientSelectFacebookMenuPreshow(){
	if (gblCallPrePost) 
	{
	
    frmMyRecipientSelectFacebook.scrollboxMain.scrollToEnd();
    gblIndex = -1;
    isMenuShown = false;
    isSignedUser = true;
    if (!gblEditFacebookSelection) {
        frmMyRecipientSelectFacebook.segMyRecipient.removeAll();
    }
    frmMyRecipientSelectFacebook.textbox247327209467957.text = "";
    frmMyRecipientSelectFacebook.label47402263614029.setVisibility(false);
    /* 
multipleSelectRecipientData.sort(dynamicSort("lblName"));

frmMyRecipientMultipleSelect.segMyRecipient.widgetDataMap = { lblName:"lblName",lblMobile:"lblMobile",lblEmail:"lblEmail",imgprofilepic:"imgprofilepic",
 imgchbxbtn:"imgchbxbtn" }

frmMyRecipientMultipleSelect.segMyRecipient.setData(multipleSelectRecipientData);

 */
    DisableFadingEdges.call(this, frmMyRecipientSelectFacebook);
    frmMyRecipientSelectFacebookPreShow.call(this);	

	}
}
function frmMyRecipientSelectFacebookMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
	    if (gblEditFacebookSelection) {} else {
	        gblOffsetMB = "0";
	        gblLimitMB = "99";
	        gblSearch = "0";
	        gblStart = "0";
	        multiSelectedFacebook.length = 0;
	        requestFromForm = "addFacebook";
	        getFriendListFromFacebook.call(this, "0");
	    }
	    gblEditFacebookSelection = false;

    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientSelectFBComp.kl
function frmMyRecipientSelectFBCompMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientSelectFBComp.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    if (flowSpa) {
	        swipeEnable = true;
	        var setupTblTap = {
	            fingers: 1,
	            swipedistance: 50,
	            swipevelocity: 75
	        }
	        swipeGesture = frmMyRecipientSelectFBComp.sboxRight.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
	        swipeGesture = frmMyRecipientSelectFBComp.scrollboxLeft.setGestureRecognizer(2, setupTblTap, swipeEventMyAccount);
	    }
	    frmMyRecipientSelectFBCompPreShow.call(this);
	    DisableFadingEdges.call(this, frmMyRecipientSelectFBComp);
	}
}
function frmMyRecipientSelectFBCompMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
    	campaginService.call(this, "image2447443295186", "frmMyRecipientSelectFBComp", "M");
    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientSelectFBConf.kl
function frmMyRecipientSelectFBConfMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientSelectFBConf.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    DisableFadingEdges.call(this, frmMyRecipientSelectFBConf);

	}
}
function frmMyRecipientSelectFBConfMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientSelectFbID.kl
function frmMyRecipientSelectFbIDMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientSelectFbID.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMyRecipientSelectFbID.textbox247327209467957.text = "";
	    frmMyRecipientSelectFbID.label47402263614029.setVisibility(false);
	    /* 
	multipleSelectRecipientData.sort(dynamicSort("lblName"));
	
	frmMyRecipientMultipleSelect.segMyRecipient.widgetDataMap = { lblName:"lblName",lblMobile:"lblMobile",lblEmail:"lblEmail",imgprofilepic:"imgprofilepic",
	 imgchbxbtn:"imgchbxbtn" }
	
	frmMyRecipientMultipleSelect.segMyRecipient.setData(multipleSelectRecipientData);
	
	 */
	    DisableFadingEdges.call(this, frmMyRecipientSelectFbID);

	}
}
function frmMyRecipientSelectFbIDMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
	    gblOffsetMB = "0";
	    gblLimitMB = "99";
	    gblSearch = "0";
	    gblStart = "0";
	    multiSelectedFacebook.length = 0;
	    getFriendListFromFacebook.call(this, "0");
    }
    assignGlobalForMenuPostshow();
}


//Starting frmMyRecipientSelectMobile.kl
function frmMyRecipientSelectMobileMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientSelectMobile.scrollboxMain.scrollToEnd();
		    /* 
		multipleSelectRecipientData.sort(dynamicSort("lblName"));
		
		frmMyRecipientMultipleSelect.segMyRecipient.widgetDataMap = { lblName:"lblName",lblMobile:"lblMobile",lblEmail:"lblEmail",imgprofilepic:"imgprofilepic",
		 imgchbxbtn:"imgchbxbtn" }
		
		frmMyRecipientMultipleSelect.segMyRecipient.setData(multipleSelectRecipientData);
		
		 */
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMyRecipientSelectMobile.textbox247327209467957.skin = "txtSearchbox";
	    frmMyRecipientSelectMobile.textbox247327209467957.text = ""
	    frmMyRecipientSelectMobile.label475095112172022.text = kony.i18n.getLocalizedString("NoContacts");
	    frmMyRecipientSelectMobile.segMyRecipient.removeAll();
	    DisableFadingEdges.call(this, frmMyRecipientSelectMobile);

	}
}
function frmMyRecipientSelectMobileMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
		 selectMobileNumber.call(this);
    }
    assignGlobalForMenuPostshow();
}

//Starting frmMyRecipientViewAccount.kl
function frmMyRecipientViewAccountMenuPreshow(){
	if (gblCallPrePost) 
	{
	    frmMyRecipientViewAccount.scrollboxMain.scrollToEnd();
	    gblIndex = -1;
	    isMenuShown = false;
	    isSignedUser = true;
	    frmMyRecipientViewAccount.label47327209475446.text = "";
	    frmMyRecipientViewAccount.lblAccountName.text = "";
	    frmMyRecipientViewAccount.lblAcctName.text = "";
	    frmMyRecipientViewAccount.lblBankName.text = "";
	    frmMyRecipientViewAccount.lblNick.text = "";
	    frmMyRecipientViewAccount.lblNickName.text = "";
	    frmMyRecipientViewAccount.lblNumber.text = "";
	    frmMyRecipientViewAccount.lblAccountNo.text = "";
	    frmMyRecipientViewAccount.lblPersonalisedID.text = "";
	    frmMyRecipientViewAccount.lblBankCD.text = "";
	    DisableFadingEdges.call(this, frmMyRecipientViewAccount);	
	}
}
function frmMyRecipientViewAccountMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here
		 viewAccountShow.call(this);

    }
    assignGlobalForMenuPostshow();
}



//Starting frmMyTransferRecipient.kl
function frmMyTransferRecipientMenuPreshow(){
	if (gblCallPrePost) 
	{
		  segTransferRecipientData.call(this);
	}
}
function frmMyTransferRecipientMenuPostshow(){
    if (gblCallPrePost) {
        // Add the new code in the post show please add here

    }
    assignGlobalForMenuPostshow();
}


function checkContactPermissionsInAddProfile(){

	//#ifdef android
		checkContactPermForMyRecipientAddProfile();	
	//#else
		frmMyRecipientSelectMobile.show();
	//#endif
}


function checkContactPermForMyRecipientAddProfile(){
   var options = {};
   var result = kony.application.checkPermission(kony.os.RESOURCE_CONTACTS, options);	
   kony.print("checkContactPermForMyRecipientAddProfile result ###########  "+result.status);
   if (result.status == kony.application.PERMISSION_DENIED){
      kony.print("checkContactPermForMyRecipientAddProfile Permission denied  ");
	  if (result.canRequestPermission){
         kony.application.requestPermission(kony.os.RESOURCE_CONTACTS, reqContactPermissionCallback);
       }else{
         kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
		 var messageErr="App cannot proceed further unless the required permisssions are granted!"; 
		 alert(messageErr);
       }
   } else if (result.status == kony.application.PERMISSION_GRANTED) {
      //gblStoragePermissionAllowed = true;
      kony.print("checkContactPermForMyRecipientAddProfile Permission granted  ");
      kony.print("Permission allowed. Calling setUpVTapSDK 11111111");
      frmMyRecipientSelectMobile.show();    
   } else if (result.status == kony.application.PERMISSION_RESTRICTED) {
      kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
      var messageErr = "App cannot proceed further unless the required permisssions are granted!"; 
	  alert(messageErr);
   }
   
    function reqContactPermissionCallback(response) {
        kony.print("reqContactPermissionCallback "+result.status);
		if (response.status == kony.application.PERMISSION_GRANTED) {
			kony.print("reqContactPermissionCallback-PERMISSION_GRANTED: " + JSON.stringify(response));
			kony.print("Permission allowed.");
            frmMyRecipientSelectMobile.show();            
		} else if (response.status == kony.application.PERMISSION_DENIED) {
			kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
		    var messageErr="App cannot proceed further unless the required permisssions are granted!"; //Need to chk with customer on msg text
		    alert(messageErr);
		}
	}
}

/*
function chkRunTimePermissionsForContactMyRecipientAddProfile(){
      if(isAndroidM()){
            kony.print(" CALLIING PERMISSIONS FFI....");
            var permissionsArr=[gblPermissionsJSONObj.CONTACTS_GROUP];
            //Creates an object of class 'PermissionFFI'
            var RuntimePermissionsChkFFIObject = new MarshmallowPermissionChecks.RuntimePermissionsChkFFI();
            //Invokes method 'checkpermission' on the object
            RuntimePermissionsChkFFIObject.checkPermissions(permissionsArr,null,chkRunTimePermissionsForContactMyRecipientAddProfileCallBack);
      }else{
          frmMyRecipientSelectMobile.show();
      }
}

function chkRunTimePermissionsForContactMyRecipientAddProfileCallBack(result) {
	if(result == "1"){
		kony.print("IN FINAL CALLBACK PERMISSION IS AVAIALABLE");
		frmMyRecipientSelectMobile.show();
		
	}else{
		kony.print("IN FINAL CALLBACK PERMISSION IS UNAVAIALABLE");
		var messageErr="App cannot proceed further unless the required permisssions are granted!" //Need to chk with customer on msg text
		alert(messageErr);
	}
} */
