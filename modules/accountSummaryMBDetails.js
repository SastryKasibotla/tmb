
function accountDetailsMBPreShow(){
 changeStatusBarColor();
 var selectedRow = frmAccountSummary.segAccountDetails.selectedRowItems[0];
 if(isNotBlank(selectedRow)){
   var sbStr = selectedRow["accId"];
	   var length = sbStr.length;
	   if (selectedRow["accType"] == kony.i18n.getLocalizedString("Loan")){
		   sbStr = sbStr.substring(7, 11);
		}else{
		  sbStr = sbStr.substring(length - 4, length);
		}
		var viewName = selectedRow["viewName"];
        creditCardRedyCashDetails(selectedRow);
	    frmAccountDetailsMB.lblHead.text = kony.i18n.getLocalizedString('AccountDetails');
		frmAccountDetailsMB.btnMyActivities.text = kony.i18n.getLocalizedString('MyActivities');
		frmAccountDetailsMB.btnFullStatement.text = kony.i18n.getLocalizedString('FullStat');
		frmAccountDetailsMB.btnDreamSavings.text = kony.i18n.getLocalizedString("keyviewDream");
		frmAccountDetailsMB.btnEditBeneficiary.text=kony.i18n.getLocalizedString("editBenefit");
		frmAccountDetailsMB.btnApplySoGood.text = kony.i18n.getLocalizedString("keyApplySoGooODLink");
		frmAccountDetailsMB.btnPointRedeem.text = kony.i18n.getLocalizedString("keyPointRedemption");
		frmAccountDetailsMB.btnDreamSavings.text = kony.i18n.getLocalizedString("keyviewDream");
		frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString('keyAvailableBalanceMB')
		frmAccountDetailsMB.lblDepositDetails.text = kony.i18n.getLocalizedString('DepDetails')
		frmAccountDetailsMB.btnBack1.text = kony.i18n.getLocalizedString('Back');
		checkShowDebitCardMenu(selectedRow);
		displayIconsForAccountDetails(selectedRow);
  		if (viewName == "PRODUCT_CODE_DREAM_SAVING") {
			 dreamSavingAccDetails(selectedRow,sbStr);
		}else if (viewName == "PRODUCT_CODE_NOFIXED") {
			 noFixedAccDetails(selectedRow,sbStr);
		}else if(viewName == "PRODUCT_CODE_NOFEESAVING_TABLE"){
             noFeeSavingAccDetails(selectedRow,sbStr);
        } else if (viewName == "PRODUCT_CODE_SAVING_TABLE" || viewName == "PRODUCT_CODE_CURRENT_TABLE" || viewName ==
			"PRODUCT_CODE_SAVINGCARE" || viewName == "PRODUCT_CODE_NEWREADYCASH_TABLE") {
            savingAccountAccDetails(selectedRow,sbStr);
        }else if(viewName == "PRODUCT_CODE_CREDITCARD_TABLE"){
            creditCardAccDetails(selectedRow,sbStr);
        }else if(viewName == "PRODUCT_CODE_OLDREADYCASH_TABLE"){
             oldReadyCashAccDetails(selectedRow,sbStr);
        }else if(viewName == "PRODUCT_CODE_TD_TABLE"){
            tdAccountAccDetails(selectedRow,sbStr);
        }else if(viewName == "PRODUCT_CODE_LOAN_CASH2GO"){
            loanCashToGoAccDetails(selectedRow,sbStr);
        }else if(viewName == "PRODUCT_CODE_LOAN_HOMELOAN"){
            loanHomeLoanAccDetails(selectedRow,sbStr);
        }else if(viewName == "PRODUCT_CODE_READYCASH_TABLE"){
            readyCashAccDetails(selectedRow,sbStr);
        }
  }

}



function creditCardRedyCashDetails(selectedRow){
   var viewName = selectedRow["viewName"];
	if(viewName=="PRODUCT_CODE_CREDITCARD_TABLE" || viewName=="PRODUCT_CODE_READYCASH_TABLE")
	   	{
	   		frmAccountDetailsMB.flexNormalImage.setVisibility(false);
	   		frmAccountDetailsMB.flxCardImage.setVisibility(true);
	   		frmAccountDetailsMB.flxCardImage.top = "-85dp";
	   		if(isNotBlank(selectedRow["ProductImg"]))
	   		{
	   			var cardImage=selectedRow["ProductImg"]
	   		}
	   		else
	   		{
	   			var cardImage="";
	   		}
	   		var randomnum = Math.floor((Math.random() * 10000) + 1);
			var imageName = getImageSRC(cardImage);
			frmAccountDetailsMB.imgcard.src=imageName;
	   	}
	   	else
	   	{
	   		frmAccountDetailsMB.flexNormalImage.setVisibility(true);
			frmAccountDetailsMB.flxCardImage.setVisibility(false);
			var randomnum = Math.floor((Math.random() * 10000) + 1);
			var imageName = loadAccntProdImagesdetails(selectedRow["ICON_ID"]+"_ACCDET");
            frmAccountDetailsMB.imgAccountDetailsPic.src=imageName;
		
		}
	
	
}

function displayIconsForAccountDetails(selectedRow){
	
	
	var payBill = selectedRow["flxPayBill"].isVisible;
	var topup = selectedRow["flxTopUp"].isVisible;
	var withdraw = selectedRow["flxWithdrawl"].isVisible;
	var transfer = selectedRow["flxTransfer"].isVisible;
  
    var PayMyCreditCard = selectedRow["PayMyCreditCard"];
  	var PayMyLoan = selectedRow["PayMyLoan"];
   
     var applySoGood = selectedRow["allowApplySoGoood"];
     var redmtion = selectedRow["allowRedeemPoints"];
  
  	if(GLOBAL_UV_STATUS_FLAG != "ON"){
		withdraw = false;
	}
  
    if(transfer == false && payBill == false && topup == false && withdraw == false){
     frmAccountDetailsMB.flexbuttonfour.isVisible = false;
    }else{
      frmAccountDetailsMB.flexbuttonfour.isVisible = true;
    }
    var fwidth = setIconsSizeandValues("96",transfer,payBill,topup,withdraw);
    frmAccountDetailsMB.flxBtn41.width = fwidth;
    frmAccountDetailsMB.flxBtn42.width = fwidth;
  	frmAccountDetailsMB.flxBtn43.width = fwidth;
  	frmAccountDetailsMB.flxBtn44.width = fwidth;
	frmAccountDetailsMB.flxBtn41.isVisible = transfer;
	frmAccountDetailsMB.flxBtn42.isVisible = payBill;
	frmAccountDetailsMB.flxBtn43.isVisible = topup;
	frmAccountDetailsMB.flxBtn44.isVisible = withdraw;
	frmAccountDetailsMB.lb41.text = kony.i18n.getLocalizedString("Transfer");
	frmAccountDetailsMB.lb42.text = kony.i18n.getLocalizedString("PayBill");
	
	frmAccountDetailsMB.img41.src = "transfershortcut.png";
	frmAccountDetailsMB.img42.src = "paybillshortcut.png";
	frmAccountDetailsMB.img41.maxHeight = "50%";
    frmAccountDetailsMB.img42.maxHeight = "50%";
    frmAccountDetailsMB.img43.maxHeight = "50%";
    frmAccountDetailsMB.img44.maxHeight = "50%";
  
    frmAccountDetailsMB.img41.maxWidth = "50%";
    frmAccountDetailsMB.img42.maxWidth = "50%";
    frmAccountDetailsMB.img43.maxWidth = "50%";
    frmAccountDetailsMB.img44.maxWidth = "50%";
	
	frmAccountDetailsMB.flxBtn41.onClick = onclickgetTransferFromAccountsFromAccDetails;
	if(PayMyCreditCard == "Y"){
		 frmAccountDetailsMB.flxBtn42.onClick = onclickCreditfromDetails;
	}else{
		frmAccountDetailsMB.flxBtn42.onClick = onclickgetBillPaymentFromAccountsFromAccDetails;
    }
	if(PayMyLoan == "Y"){
		 frmAccountDetailsMB.flxBtn42.onClick = onclickLoanfromDetails;
	}else{
		frmAccountDetailsMB.flxBtn42.onClick = onclickgetBillPaymentFromAccountsFromAccDetails;
    }	
    frmAccountDetailsMB.img43.src = ""; 
    frmAccountDetailsMB.img44.src = ""; 
    if(applySoGood == "1"){
      frmAccountDetailsMB.img43.src = "sogoodshortcut.png"
      frmAccountDetailsMB.lb43.text = kony.i18n.getLocalizedString("applySoGood");
      frmAccountDetailsMB.flxBtn43.onClick = onClickApplySoGooODLink;
    }else{
      frmAccountDetailsMB.img43.src = "topupshortcut.png";
      frmAccountDetailsMB.lb43.text = kony.i18n.getLocalizedString("TopUp");
      frmAccountDetailsMB.flxBtn43.onClick = onclickgetTopUpFromAccountsFromAccDetails;
    }
   if(redmtion == "1"){
         frmAccountDetailsMB.img44.src = "redemptionshotrcut.png";
         frmAccountDetailsMB.lb44.text = kony.i18n.getLocalizedString("Redemtion");
         frmAccountDetailsMB.flxBtn44.onClick = checkMBCustStatus;
    }else {
        frmAccountDetailsMB.img44.src = "cardlessshortcut.png";
        frmAccountDetailsMB.lb44.text = kony.i18n.getLocalizedString("Withdraw");
        frmAccountDetailsMB.flxBtn44.onClick = onclickgetCardlessWithdrawFromAccountsFromAccDetails;
    }
	
	
}	

function dreamSavingAccDetails(selectedRow,sbStr){

   frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
   frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
	var locale = kony.i18n.getCurrentLocale();
	var branchName;
	var branchName;
	var accNickName;
	var accType;
	if (locale == "en_US") {
		productName = selectedRow["ProductNameEng"];
		branchName = selectedRow["BranchNameEh"];
		accNickName= selectedRow["ProductNameEng"]+" "+sbStr;
		accType = selectedRow["Type_EN"];
		
	} else {
		productName = selectedRow["ProductNameThai"];
		branchName = selectedRow["BranchNameTh"];
		accNickName = selectedRow["ProductNameThai"]+" "+sbStr;
		accType = selectedRow["Type_TH"];
	}
	if(isNotBlank(selectedRow["acctNickName"])){
		frmAccountDetailsMB.lblAccountNameHeader.text = selectedRow["acctNickName"];
	}
	else{
	 	frmAccountDetailsMB.lblAccountNameHeader.text = accNickName;
	}
	frmAccountDetailsMB.lblUsefulValue1.text = productName;
	frmAccountDetailsMB.lblUsefulValue10.text = branchName;
	frmAccountDetailsMB.lblUsefulValue6.text = accType;
	frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
	frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("DreamDes");
	frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("DreamTarAmt");
	frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("AccountType");
	frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("AccountNo");
	frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("AccountName");
	frmAccountDetailsMB.lblUseful10.text = kony.i18n.getLocalizedString("BranchName");
	frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("Status");
	frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("DreamMonTranAmt");
	frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("DreamMonTranDate");
	frmAccountDetailsMB.lblUseful13.text = kony.i18n.getLocalizedString("LinkedAcc");
	if((frmAccountDetailsMB.lblUsefulValue13.text).length!=13){
	  frmAccountDetailsMB.lblUsefulValue13.text="";
	 }
	if((frmAccountDetailsMB.lblUsefulValue13.text).length==0){
	 frmAccountDetailsMB.lblUsefulValue13.text=kony.i18n.getLocalizedString("lblNotFound");
	}
	if(gblAccountStatus!=null){
		var status=gblAccountStatus.split("|")
		if (locale == "en_US")
		frmAccountDetailsMB.lblUsefulValue9.text =status[0];
		else
		frmAccountDetailsMB.lblUsefulValue9.text =status[1];
	}
	
}


function noFixedAccDetails(selectedRow,sbStr){
	frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
	var accNickName;
	var productName;
	var branchName;
	var accType;
	var locale = kony.i18n.getCurrentLocale();
	if (locale == "en_US") {
		productName = selectedRow["ProductNameEng"];
		branchName = selectedRow["BranchNameEh"];
		accNickName = selectedRow["ProductNameEng"]+" "+sbStr;
		accType = selectedRow["Type_EN"];
	} else {
		productName = selectedRow["ProductNameThai"];
		branchName = selectedRow["BranchNameTh"];
		accNickName = selectedRow["ProductNameThai"]+" "+sbStr;
		accType= selectedRow["Type_TH"];
	}
	if(isNotBlank(selectedRow["acctNickName"])){
	frmAccountDetailsMB.lblAccountNameHeader.text = selectedRow["acctNickName"];
	}
	else{
	 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
	}
	
	frmAccountDetailsMB.lblUsefulValue1.text = productName;
	frmAccountDetailsMB.lblUsefulValue8.text = branchName;
	frmAccountDetailsMB.lblUsefulValue4.text = accType;
	frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
	frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountType");
	frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("AccountNo");
	frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("AccountName");
	frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("BranchName");
	frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("Status");
	frmAccountDetailsMB.lblUseful10.text = kony.i18n.getLocalizedString("IntrestAmount");
	frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("keyLblInterestRate");
	frmAccountDetailsMB.lblUseful11.text = kony.i18n.getLocalizedString("LinkedAcc")
	frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("SendToMaxPoint");
	frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("SendToMinPoint");
	if(gblAccountStatus!=null){
		var status=gblAccountStatus.split("|")
		if (locale == "en_US")
		frmAccountDetailsMB.lblUsefulValue7.text =status[0];
		else
		frmAccountDetailsMB.lblUsefulValue7.text =status[1];
	}

}

function  noFeeSavingAccDetails(selectedRow,sbStr){
	frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
	var locale = kony.i18n.getCurrentLocale();
	var branchName;
	var accNickName;
	var productName;
	var accType;
	if (locale == "en_US") {
		productName = selectedRow["ProductNameEng"];
		branchName = selectedRow["BranchNameEh"];
		accNickName= selectedRow["ProductNameEng"]+" "+sbStr;
		accType = selectedRow["Type_EN"];
	} else {
		branchName = selectedRow["BranchNameTh"];
		productName = selectedRow["ProductNameThai"];
		accNickName= selectedRow["ProductNameThai"]+" "+sbStr;
		accType = selectedRow["Type_TH"];
	}
	if(isNotBlank(selectedRow["acctNickName"])){
	frmAccountDetailsMB.lblAccountNameHeader.text = selectedRow["acctNickName"];
	}
	else{
	 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
	}
	frmAccountDetailsMB.lblUsefulValue7.text = branchName;
	frmAccountDetailsMB.lblUsefulValue3.text = accType;
	frmAccountDetailsMB.lblUsefulValue1.text = productName;
	frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
	frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("remFreeTran");
	frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("AccountType");
	frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountNo");
	frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("AccountName");
	frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("BranchName");
	frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("Status");
	if(gblAccountStatus!=null){
		var status=gblAccountStatus.split("|")
		if (locale == "en_US")
		frmAccountDetailsMB.lblUsefulValue6.text =status[0];
		else
		frmAccountDetailsMB.lblUsefulValue6.text =status[1];
	}	
}
	
function savingAccountAccDetails(selectedRow,sbStr){
	var locale = kony.i18n.getCurrentLocale();
	frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
	var branchName;
	var accNickName;
	var productName;
	var accType;
	if (locale == "en_US") {
		branchName = selectedRow["BranchNameEh"];
		productName = selectedRow["ProductNameEng"];
		accNickName = selectedRow["ProductNameEng"]+" "+sbStr;
		accType = selectedRow["Type_EN"];
	} else {
		branchName = selectedRow["BranchNameTh"];
		productName = selectedRow["ProductNameThai"];
		accNickName= selectedRow["ProductNameThai"]+" "+sbStr;
		accType=selectedRow["Type_TH"];
	}
	if(isNotBlank(selectedRow["acctNickName"])){
		frmAccountDetailsMB.lblAccountNameHeader.text = selectedRow["acctNickName"];
	}else{
		frmAccountDetailsMB.lblAccountNameHeader.text = accNickName;
	}
	if (selectedRow["accType"] == kony.i18n.getLocalizedString("Current"))
		frmAccountDetailsMB.lblUsefulValue2.text = accType;
	else
	frmAccountDetailsMB.lblUsefulValue2.text = accType;
	frmAccountDetailsMB.lblUsefulValue2.textCopyable=true;
	frmAccountDetailsMB.lblUsefulValue1.text = productName;
	frmAccountDetailsMB.lblUsefulValue6.text = branchName;
	frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("Status");
	frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
	frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("AccountType");
	frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("AccountNo");
	frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
	frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("BranchName");
	if(gblAccountStatus!=null){
		var status=gblAccountStatus.split("|")
		if (locale == "en_US")
		frmAccountDetailsMB.lblUsefulValue5.text =status[0];
		else
		frmAccountDetailsMB.lblUsefulValue5.text =status[1];
	}
	if(gblSavingsCareFlow=="AccountDetailsFlow"){
		kony.print("Saving care flow");
		frmAccountDetailsMB.flexLineEditBeneficiaries.setVisibility(true);
		frmAccountDetailsMB.flexLinkEditBeneficiaries.setVisibility(true);
		frmAccountDetailsMB.btnEditBeneficiary.text=kony.i18n.getLocalizedString("editBenefit");
		handleSavingCareMBDetails(frmAccountDetailsMB);			
	}
}	
	

function creditCardAccDetails(selectedRow,sbStr){
	frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString('keyMBCreditlimit');
	frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
	var locale = kony.i18n.getCurrentLocale();
	var accNickName;
	if (locale == "en_US") {
		productName = selectedRow["ProductNameEng"];
		accNickName = selectedRow["ProductNameEng"]+" "+sbStr;
		
	} else {
		productName = selectedRow["ProductNameThai"];
		accNickName = selectedRow["ProductNameThai"]+" "+sbStr;
	}
	if(isNotBlank(selectedRow["acctNickName"])){
	frmAccountDetailsMB.lblAccountNameHeader.text =selectedRow["acctNickName"];
	}
	else{
	 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
	}
	frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
	frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
	frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
	frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
	frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
	frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
	frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
	frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
	frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
	var allowCardTypeForRedeem = selectedRow["allowCardTypeForRedeem"];		
	if (allowCardTypeForRedeem == "1") {
		frmAccountDetailsMB.hbxUseful14.setVisibility(true);
		frmAccountDetailsMB.hbxUseful15.setVisibility(true);		
		frmAccountDetailsMB.lblUseful14.text = kony.i18n.getLocalizedString("keyMBAvailable");			
		frmAccountDetailsMB.lblUsefulValue14.text = frmAccountDetailsMB.lblUsefulValue14.text.split(" ")[0];
		frmAccountDetailsMB.lblUseful15.text = kony.i18n.getLocalizedString("keyPointExpiringOn") + " " + reformatDate(selectedRow["bonusPointExpiryDate"]) + ": ";
		frmAccountDetailsMB.lblUsefulValue15.text = frmAccountDetailsMB.lblUsefulValue15.text.split(" ")[0];
	}			
		
}	

function oldReadyCashAccDetails(selectedRow,sbStr){
	frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
	frmAccountDetailsMB.image24750595786995.setVisibility(false);
	frmAccountStatementMB.hbxMyactivities.setVisibility(false);
	frmAccountDetailsMB.lnkMyActivities.setVisibility(false);
	var locale = kony.i18n.getCurrentLocale();
	var accNickName;
	if (locale == "en_US") {
		productName = selectedRow["ProductNameEng"];
		accNickName= selectedRow["ProductNameEng"]+" "+sbStr;
		
	} else {
		productName = selectedRow["ProductNameThai"];
		accNickName = selectedRow["ProductNameThai"]+" "+sbStr;
	}
	if(isNotBlank(selectedRow["acctNickName"])){
	frmAccountDetailsMB.lblAccountNameHeader.text = selectedRow["acctNickName"];
	}
	else{
	 frmAccountDetailsMB.lblAccountNameHeader.text = accNickName;
	}
	frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
	frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
	frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
	frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
	frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
	frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
	frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
	frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
	frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
}


function tdAccountAccDetails(selectedRow,sbStr){
	frmAccountDetailsMB.flexledgerbalance.setVisibility(true);
	frmAccountDetailsMB.lblledgerbal.text = kony.i18n.getLocalizedString("LedgerBalance");
	var locale = kony.i18n.getCurrentLocale();
	var branchName;
	var productName;
	var accNickName;
	var accType;
	if (locale == "en_US") {
		branchName = selectedRow["BranchNameEh"];
		productName = selectedRow["ProductNameEng"];
		accNickName= selectedRow["ProductNameEng"]+" "+sbStr;
		accType=selectedRow["Type_EN"];
	} else {
		branchName = selectedRow["BranchNameTh"];
		productName = selectedRow["ProductNameThai"];
		accNickName= selectedRow["ProductNameThai"]+" "+sbStr;
		accType=selectedRow["Type_TH"];
	}
	if(isNotBlank(selectedRow["acctNickName"])){
	frmAccountDetailsMB.lblAccountNameHeader.text =selectedRow["acctNickName"];
	}
	else{
	 frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
	}
	frmAccountDetailsMB.lblUsefulValue1.text = productName;
	frmAccountDetailsMB.lblUsefulValue3.text = accType;
	frmAccountDetailsMB.lblUsefulValue3.textCopyable=true;
	frmAccountDetailsMB.lblUsefulValue7.text = branchName;
	frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
	frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("AccountType");
	frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountNo");
	frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("AccountName");
	frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("BranchName");
	frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("LinkedAcc");
	frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("tenor");
	frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("Status");
	frmAccountDetailsMB.flexBenefiListHeader.setVisibility(false);
	frmAccountDetailsMB.flexBenefiList.setVisibility(false);
	frmAccountDetailsMB.flexMaturityDisplayHeader.setVisibility(true);
	frmAccountDetailsMB.flexMaturityDisplay.setVisibility(true)
	frmAccountDetailsMB.lblDateH.text = kony.i18n.getLocalizedString("MatDate");
	frmAccountDetailsMB.lblBalanceH.text = kony.i18n.getLocalizedString("Balance");
	frmAccountDetailsMB.lblTransactionH.text = kony.i18n.getLocalizedString("InterestRate");
	frmAccountDetailsMB.lblDepositDetails.text = kony.i18n.getLocalizedString("DepDetails");
	if(gblAccountStatus!=null){
		var status=gblAccountStatus.split("|")
		if (locale == "en_US")
		frmAccountDetailsMB.lblUsefulValue6.text =status[0];
		else
		frmAccountDetailsMB.lblUsefulValue6.text =status[1];
	}
}
	

function loanCashToGoAccDetails(selectedRow,sbStr){
    frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
    var locale = kony.i18n.getCurrentLocale();
    var productName;
    var accNickName;
    if (locale == "en_US"){
      productName = selectedRow["ProductNameEng"];
      accNickName=selectedRow["ProductNameEng"]+" "+sbStr;
    }
    else{
      productName = selectedRow["ProductNameThai"];
      accNickName=selectedRow["ProductNameThai"]+" "+sbStr;
    }
    frmAccountDetailsMB.lblUsefulValue1.text = productName;
  	frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("keyMBOutstanding");
    if(isNotBlank(selectedRow["acctNickName"])){
      frmAccountDetailsMB.lblAccountNameHeader.text =selectedRow["acctNickName"];
    }
    else{
      frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
    }
    frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
    frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("AccountNo");
    frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("SuffixNo");
    frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
    frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("PayDueDate");
    frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("MonthlyInstallement");
    frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
  
}

function loanHomeLoanAccDetails(selectedRow,sbStr){
      frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
      var locale = kony.i18n.getCurrentLocale();
      var productName;
      var accNickName;
      if (locale == "en_US"){
        productName = selectedRow["ProductNameEng"];
        accNickName=selectedRow["ProductNameEng"]+" "+sbStr;
      }
      else{
        productName = selectedRow["ProductNameThai"];
        accNickName=selectedRow["ProductNameThai"]+" "+sbStr;
      }
      frmAccountDetailsMB.lblUsefulValue1.text = productName;
  		frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString("keyMBOutstanding");
      if(isNotBlank(selectedRow["acctNickName"])){
        frmAccountDetailsMB.lblAccountNameHeader.text =selectedRow["acctNickName"];
      }
      else{
        frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
      }
      frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("ProductName");
      frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("AccountNo");
      frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("SuffixNo");
      frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("AccountName");
      frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("PayDueDate");
      frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("MonthlyInstallement");
      frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("DirectCredit");
      frmAccountDetailsMB.lblDateH.text = kony.i18n.getLocalizedString("keyLoanLimit");
      frmAccountDetailsMB.lblTransactionH.text = kony.i18n.getLocalizedString('InterestRate');
      frmAccountDetailsMB.lblBalanceH.text = kony.i18n.getLocalizedString("keyHomeLoanExpiryDate");
      frmAccountDetailsMB.lblDepositDetails.text = kony.i18n.getLocalizedString("interestRateDetail");
}


function readyCashAccDetails(selectedRow,sbStr){
    frmAccountDetailsMB.lblHeaderBalance.text = kony.i18n.getLocalizedString('keyMBCreditlimit');
    frmAccountDetailsMB.flexledgerbalance.setVisibility(false);
    var locale = kony.i18n.getCurrentLocale();
    var accNickName;
    if (locale == "en_US") {
      productName = selectedRow["ProductNameEng"];
      accNickName= selectedRow["ProductNameEng"]+" "+sbStr;

    } else {
      productName = selectedRow["ProductNameThai"];
      accNickName=selectedRow["ProductNameThai"]+" "+sbStr;
    }
    if(isNotBlank(selectedRow["acctNickName"])){
      frmAccountDetailsMB.lblAccountNameHeader.text =selectedRow["acctNickName"];
    }
    else{
      frmAccountDetailsMB.lblAccountNameHeader.text =accNickName;
    }
    frmAccountDetailsMB.lblUseful2.text = kony.i18n.getLocalizedString("CardNo");
    frmAccountDetailsMB.lblUseful3.text = kony.i18n.getLocalizedString("CardHolderName");
    frmAccountDetailsMB.lblUseful4.text = kony.i18n.getLocalizedString("creditLimit");
    frmAccountDetailsMB.lblUseful5.text = kony.i18n.getLocalizedString("StatDate");
    frmAccountDetailsMB.lblUseful7.text = kony.i18n.getLocalizedString("TotalAmtDue");
    frmAccountDetailsMB.lblUseful8.text = kony.i18n.getLocalizedString("MinAmtDue");
    frmAccountDetailsMB.lblUseful1.text = kony.i18n.getLocalizedString("CardType");
    frmAccountDetailsMB.lblUseful6.text = kony.i18n.getLocalizedString("PayDueDate");
    frmAccountDetailsMB.lblUseful9.text = kony.i18n.getLocalizedString("DirectCredit");
}


function onClickOfAccountDetailsBack(){
  kony.print("@@@displayRTPAnnoucement:::"+displayRTPAnnoucement);
  if(displayRTPAnnoucement) {
    startUpRTPDisplay();
  } else {
    animation = true;
    //showAccountSummaryNew();
    showAccountSummaryNewWithRefresh();
    TMBUtil.DestroyForm(frmAccountDetailsMB);
  }
}