function MBcallBAPolicyListService() {

    //#ifdef iphone
    TMBUtil.DestroyForm(frmMBBankAssuranceSummary);
    //#endif

    var inputParam = {};
    showLoadingScreen();
    invokeServiceSecureAsync("BAGetPolicyList", inputParam, callBAPolicyListServiceCallBackMB);
}

function callBAPolicyListServiceCallBackMB(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            gblBASummaryData = resulttable;
            gblEmailId = resulttable["emailAddr"];
            //
            if (kony.i18n.getCurrentLocale() == "en_US") {
                frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerName;
            } else {
                frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerNameTh;
            }
            frmMBBankAssuranceSummary.label475124774164.text = kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
            frmMBBankAssuranceSummary.lblBalanceValue.text = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured");
            frmMBBankAssuranceSummary.btnBack1.text = kony.i18n.getLocalizedString("Back");
            if (isNotBlank(frmAccountSummary.imgProfile) && isNotBlank(frmAccountSummary.imgProfile.base64)) {
                frmMBBankAssuranceSummary.imgProfile.base64 = frmAccountSummary.imgProfile.base64;
            }
            var totalSumInsured = gblAccountTable["totalSumInsured"];
            if (totalSumInsured != "-") {
                totalSumInsured = commaFormatted(parseFloat(totalSumInsured).toFixed(2)) + " " + kony.i18n.getLocalizedString("currencyThaiBaht");
            }
            frmMBBankAssuranceSummary.lblInvestmentValue.text = totalSumInsured;
            frmMBBankAssuranceSummary.show();
            frmMBBankAssuranceSummary.scrollboxMain.scrollToEnd();
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
}

function populateDataBankAssuranceSummaryMB(dataBankAssurance) {

    var policyHeadersDS = dataBankAssurance["policyHeadersDS"];
    var policyDetailsDS = dataBankAssurance["policyDetailsDS"];
    var locale = kony.i18n.getCurrentLocale();
    var segmentData = [];
    for (var i = 0; i < policyDetailsDS.length; i++) {

        var companyName = "";
        var companynamePrev = "";
        var sumInsuredLabel = "";
        var policyName = "";
        var isFlag = "";
        var logo = "https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + "BA_" + policyDetailsDS[i]["DATA_SOURCE_VALUE_EN"] + "&modIdentifier=MFLOGOS";

        if (policyDetailsDS[i]["ALLOW_PAYMENT_VALUE_EN"] == "1") {
            isFlag = {
                "src": "ico_flag_active.png"
            };
        }

        if (locale == "en_US") {
            companyName = policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"];
            policyName = policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_EN"];
            sumInsuredLabel = policyHeadersDS[0]["SUM_INSURE_EN"]
            if (i != 0) companynamePrev = policyDetailsDS[i - 1]["COMPANY_NAME_VALUE_EN"];
        } else {
            companyName = policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"];
            policyName = policyDetailsDS[i]["POLICY_UNIQUE_NAME_VALUE_TH"];
            sumInsuredLabel = policyHeadersDS[0]["SUM_INSURE_TH"]
            if (i != 0) companynamePrev = policyDetailsDS[i - 1]["COMPANY_NAME_VALUE_TH"];
        }
        if (companynamePrev != companyName) {
            var dataObjectHeader = {
                lblHead: companyName,
                lblHeadEN: policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"],
                lblHeadTH: policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"],
                template: MBMFHeader
            };
            segmentData.push(dataObjectHeader);
        }

        var dataObject = {
            imgLogo: logo,
            imgPayFlag: isFlag,
            imageRightArrow: {
                "src": "navarrow.png"
            },
            companyNameEN: policyDetailsDS[i]["COMPANY_NAME_VALUE_EN"],
            companyNameTH: policyDetailsDS[i]["COMPANY_NAME_VALUE_TH"],
            policyNameEN: policyDetailsDS[i]["POLICY_NAME_VALUE_EN"],
            policyNameTH: policyDetailsDS[i]["POLICY_NAME_VALUE_TH"],
            lblPolicyName: policyName,
            lblPolicyNumber: policyDetailsDS[i]["POLICY_NO_VALUE_EN"],
            lblSumInsured: sumInsuredLabel + ":",
            sumInsuredEN: policyHeadersDS[0]["SUM_INSURE_EN"] + ":",
            sumInsuredTH: policyHeadersDS[0]["SUM_INSURE_TH"] + ":",
            lblSumInsuredValue: commaFormatted(policyDetailsDS[i]["SUM_INSURE_VALUE_EN"]) + " " + kony.i18n.getLocalizedString("currencyThaiBaht"),
            allowPayBill: policyDetailsDS[i]["ALLOW_PAYMENT_VALUE_EN"],
            allowTaxDoc: policyDetailsDS[i]["ALLOW_TAX_DOCUMENT_VALUE_EN"],
            //template: MBBAContent

        }

        segmentData.push(dataObject);
    }
    return segmentData;
}

function frmMBBankAssuranceSummaryPreShow() {
    changeStatusBarColor();
    if (kony.i18n.getCurrentLocale() == "en_US") {
        frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerName;
    } else {
        frmMBBankAssuranceSummary.lblAccntHolderName.text = gblCustomerNameTh;
    }
    frmMBBankAssuranceSummary.label475124774164.text = kony.i18n.getLocalizedString("BA_Acc_Summary_Title");
    frmMBBankAssuranceSummary.lblBalanceValue.text = kony.i18n.getLocalizedString("BA_lbl_Total_Sum_Insured");
    //MIB-1418  > MIB-1669
    frmMBBankAssuranceSummary.btnBack1.text = kony.i18n.getLocalizedString("Back");
    //MIB-1418  > MIB-1669
    var segmentData = populateDataBankAssuranceSummaryMB(gblBASummaryData);

    frmMBBankAssuranceSummary.segAccountDetails.widgetDataMap = {
        lblHead: "lblHead",
        imgLogo: "imgLogo",
        lblPolicyName: "lblPolicyName",
        lblPolicyNumber: "lblPolicyNumber",
        lblSumInsured: "lblSumInsured",
        lblSumInsuredValue: "lblSumInsuredValue",
        imageRightArrow: "imageRightArrow",
        imgPayFlag: "imgPayFlag"
    };
    frmMBBankAssuranceSummary.segAccountDetails.removeAll();
    frmMBBankAssuranceSummary.segAccountDetails.setData(segmentData);
}

function callBAPolicyDetailsServiceMB() {
  	var policynumber="";
  	var imgLogo="";
  //#ifdef android
  	 policynumber = frmMBBankAssuranceSummary.segAccountDetails.selectedItems[0]["lblPolicyNumber"];
     imgLogo = frmMBBankAssuranceSummary.segAccountDetails.selectedItems[0]["imgLogo"];
    //#else
  	 policynumber = frmMBBankAssuranceSummary.segAccountDetails.selectedRowItems[0]["lblPolicyNumber"];
     imgLogo = frmMBBankAssuranceSummary.segAccountDetails.selectedRowItems[0]["imgLogo"];
  	//#endif
  	var inputParam = {};
    showLoadingScreen();
    TMBUtil.DestroyForm(frmMBBankAssuranceDetails);
    inputParam["policyNumber"] = policynumber;
    frmMBBankAssuranceDetails.imgAccountDetailsPic.src = imgLogo;
    inputParam["dataSet"] = "0";
    invokeServiceSecureAsync("BAGetPolicyDetails", inputParam, callBAPolicyDetailsServiceCallBackMB);
}

function callBAPolicyDetailsServiceCallBackMB(status, resulttable) {
    if (status == 400) {
        dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            gblBAPolicyDetailsData = resulttable;
            frmMBBankAssuranceDetails.show();
        }
    }
}

function frmMBBankAssuranceDetailsInit() {
  	frmMBBankAssuranceDetails.onDeviceBack = disableBackButton;
    frmMBBankAssuranceDetails.preShow = frmMBBankAssuranceDetailsPreShow;
    frmMBBankAssuranceDetails.postShow = frmMBBankAssuranceDetailsPostShow;
    frmMBBankAssuranceDetails.btnMainMenu.onClick = frmMBBankAssuranceDetailsBtnMainMenuOnClick;
    frmMBBankAssuranceDetails.btnTaxDoc.onClick = showBATaxDocEmailConfirmPopup;
    frmMBBankAssuranceDetails.btnPayBill.onClick = moveToBillPaymentFromBAPolicyDetailsMB;
}
function frmMBBankAssuranceDetailsBtnMainMenuOnClick(){
	frmMBBankAssuranceSummary.show();
}
function frmMBBankAssuranceDetailsPostShow() {
    var trans100 = kony.ui.makeAffineTransform();
    trans100.rotate(45);
    var option1 = kony.ui.createAnimation({
        "100": {
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            },
            "transform": trans100
        }
    });
    var option2 = {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0
    };
    var option3 = {};
    frmMBBankAssuranceDetails.flxTaxDoc.animate(option1, option2, option3);
    assignGlobalForMenuPostshow();
}

function frmMBBankAssuranceDetailsPreShow() {
    try {
        changeStatusBarColor();

        var locale = kony.i18n.getCurrentLocale();
        var languageExtension = "EN";
        if (locale == "en_US") {
            languageExtension = "EN";
        } else {
            languageExtension = "TH";
        }

		frmMBBankAssuranceDetails.flxTaxDoc.width = "200dp";
    	frmMBBankAssuranceDetails.flxTaxDoc.left = "-100dp";
    	frmMBBankAssuranceDetails.flxTaxDoc.height = "150dp";
    	frmMBBankAssuranceDetails.flxTaxDoc.top = "-75dp";

        frmMBBankAssuranceDetails.lblHdrTxt.text = kony.i18n.getLocalizedString("BA_lbl_Policy_details");
        frmMBBankAssuranceDetails.btnPayBill.text = kony.i18n.getLocalizedString("PayBill");
        frmMBBankAssuranceDetails.btnTaxDoc.text = kony.i18n.getLocalizedString("BA_Tax_doc");

        var policyDetailsValueDS = gblBAPolicyDetailsData["policyDetailsValueDS"];
        var premiumPaymentValueDS = gblBAPolicyDetailsData["premiumPaymentValueDS"];
        var coverageValueDS = gblBAPolicyDetailsData["coverageValueDS"];
        var cashbackValueDS = gblBAPolicyDetailsData["cashbackValueDS"];
        var billPaymentValueDS = gblBAPolicyDetailsData["billPaymentValueDS"];

        kony.print("callmf xx policyDetailsValueDS" + JSON.stringify(policyDetailsValueDS));
        kony.print("callmf xx premiumPaymentValueDS" + JSON.stringify(premiumPaymentValueDS));
        kony.print("callmf xx coverageValueDS" + JSON.stringify(coverageValueDS));
        kony.print("callmf xx cashbackValueDS" + JSON.stringify(cashbackValueDS));
        kony.print("callmf xx billPaymentValueDS" + JSON.stringify(billPaymentValueDS));

        removeWidgetsfrmMBBankAssuranceDetails();

        var allowPaybill = false;
        if (policyDetailsValueDS.length > 0 && policyDetailsValueDS[0]["value_EN"] == "1" && billPaymentValueDS.length > 0) {
            allowPaybill = true;
        }
        var allowTaxDoc = false;
        if (policyDetailsValueDS.length > 0 && policyDetailsValueDS[1]["value_EN"] == "1") {
            allowTaxDoc = true;
        }

        if (allowPaybill) {
            frmMBBankAssuranceDetails.flxFooter.setVisibility(true);
            frmMBBankAssuranceDetails.flxFullBody.height = "81%";
        } else {
            frmMBBankAssuranceDetails.flxFooter.setVisibility(false);
            frmMBBankAssuranceDetails.flxFullBody.height = "92%";
        }
        if (allowTaxDoc) {
            frmMBBankAssuranceDetails.flxTaxDoc.setVisibility(true);
        } else {
            frmMBBankAssuranceDetails.flxTaxDoc.setVisibility(false);
        }


        frmMBBankAssuranceDetails.lblPolicyName.text = "";
        
        var currentHeight = 0;
        if (policyDetailsValueDS.length > 0) {

            if (isNotBlank(policyDetailsValueDS[3]["displayName_" + languageExtension])) {
            	frmMBBankAssuranceDetails.lblPolicyName.width = "85%";
            	frmMBBankAssuranceDetails.lblPolicyName.centerX = "50%";
            	frmMBBankAssuranceDetails.lblPolicyName.skin = 'lblGreyDBOzoMedium200';
                frmMBBankAssuranceDetails.lblPolicyName.text = policyDetailsValueDS[3]["value_" + languageExtension];
            }

            if (isNotBlank(policyDetailsValueDS[2]["displayName_" + languageExtension]) && isNotBlank(policyDetailsValueDS[2]["value_" + languageExtension])) {
                frmMBBankAssuranceDetails.lblPolicyNoValue.text = policyDetailsValueDS[2]["value_" + languageExtension];
                currentHeight = addWidgetsfrmMBBankAssuranceDetailsContent("policyDetails", policyDetailsValueDS[2]["displayName_" + languageExtension], policyDetailsValueDS[2]["value_" + languageExtension], currentHeight);
            }
            if (isNotBlank(policyDetailsValueDS[7]["displayName_" + languageExtension]) && isNotBlank(policyDetailsValueDS[7]["value_" + languageExtension])) {
                currentHeight = addWidgetsfrmMBBankAssuranceDetailsContent("policyDetails", policyDetailsValueDS[7]["displayName_" + languageExtension], policyDetailsValueDS[7]["value_" + languageExtension], currentHeight);
            }
            if (isNotBlank(policyDetailsValueDS[6]["displayName_" + languageExtension]) && isNotBlank(policyDetailsValueDS[6]["value_" + languageExtension])) {
            	var isActive =  false;
            	if ("Active" == policyDetailsValueDS[6]["value_EN"]) {
            		isActive = true;
            	}
                currentHeight = addWidgetsfrmMBBankAssuranceDetailsContent("policyDetails", policyDetailsValueDS[6]["displayName_" + languageExtension], policyDetailsValueDS[6]["value_" + languageExtension], currentHeight, isActive);
            }

            currentHeight = addWidgetsfrmMBBankAssuranceDetailsGroup("policyDetails", kony.i18n.getLocalizedString("BA_lbl_Policy_details"), currentHeight);
            for (var i = 9; i < policyDetailsValueDS.length; i++) {
                if (isNotBlank(policyDetailsValueDS[i]["displayName_" + languageExtension]) && isNotBlank(policyDetailsValueDS[i]["value_" + languageExtension])) {
                    currentHeight = addWidgetsfrmMBBankAssuranceDetailsContent("policyDetails", policyDetailsValueDS[i]["displayName_" + languageExtension], policyDetailsValueDS[i]["value_" + languageExtension], currentHeight);
                }
            }
        }

        if (coverageValueDS.length > 0) {
            currentHeight = addWidgetsfrmMBBankAssuranceDetailsGroup("coverageDetails", kony.i18n.getLocalizedString("BA_lbl_Coverage_info"), currentHeight);
            for (var i = 0; i < coverageValueDS.length; i++) {
                if (isNotBlank(coverageValueDS[i]["displayName_" + languageExtension]) && isNotBlank(coverageValueDS[i]["value_" + languageExtension])) {
                    currentHeight = addWidgetsfrmMBBankAssuranceDetailsContent("coverageDetails", coverageValueDS[i]["displayName_" + languageExtension], coverageValueDS[i]["value_" + languageExtension], currentHeight);
                }
            }
        }


        if (cashbackValueDS.length > 0) {
            currentHeight = addWidgetsfrmMBBankAssuranceDetailsGroup("cashbackDetails", kony.i18n.getLocalizedString("BA_lbl_Cash_back_dividend"), currentHeight);
            for (var i = 0; i < cashbackValueDS.length; i++) {
                if (isNotBlank(cashbackValueDS[i]["displayName_" + languageExtension]) && isNotBlank(cashbackValueDS[i]["value_" + languageExtension])) {
                    currentHeight = addWidgetsfrmMBBankAssuranceDetailsContent("cashbackDetails", cashbackValueDS[i]["displayName_" + languageExtension], cashbackValueDS[i]["value_" + languageExtension], currentHeight);
                }
            }
        }

        if (premiumPaymentValueDS.length > 0) {
            currentHeight = addWidgetsfrmMBBankAssuranceDetailsGroup("premiumPayment", kony.i18n.getLocalizedString("BA_lbl_Premium_payment_info"), currentHeight);
            for (var i = 0; i < premiumPaymentValueDS.length; i++) {
                if (isNotBlank(premiumPaymentValueDS[i]["displayName_" + languageExtension]) && isNotBlank(premiumPaymentValueDS[i]["value_" + languageExtension])) {
                    currentHeight = addWidgetsfrmMBBankAssuranceDetailsContent("premiumPayment", premiumPaymentValueDS[i]["displayName_" + languageExtension], premiumPaymentValueDS[i]["value_" + languageExtension], currentHeight);
                }
            }
        }

        //currentHeight = addWidgetsfrmMBBankAssuranceDetailsBottom(currentHeight);


    } catch (e) {
        kony.print(e.message + e.stack);
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
}

function removeWidgetsfrmMBBankAssuranceDetails() {
    frmMBBankAssuranceDetails.flxDynamic.removeAll();
}

function addWidgetsfrmMBBankAssuranceDetailsGroup(headerName, headerText, currentHeight) {
	var lineHeight = 18;
    var labelHeight = 70;
    var flexHeight = lineHeight + labelHeight;
    var flxGroupLine = new kony.ui.FlexContainer({
        "id": "flxGroupLine" + headerName,
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "height": lineHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "skin": "flexBlueLine",
        "focusSkin": "flexBlueLine",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxGroupLine.setDefaultUnit(kony.flex.DP)
    flxGroupLine.add();
    var lblGroupValue = new kony.ui.Label({
        "id": "lblGroupValue" + headerName,
        "top": lineHeight + "dp",
        "left": "0dp",
        "width": "85%",
        "height": labelHeight + "dp",
        "centerX": "50%",
        "zIndex": 1,
        "isVisible": true,
        "text": headerText,
        "skin": "lblGreyDBOzoMedium200"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });
    var flxGroup = new kony.ui.FlexContainer({
        "id": "flxGroup" + headerName,
        "top": currentHeight + "dp",
        "left": "0dp",
        "width": "100%",
        "height": flexHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxGroup.setDefaultUnit(kony.flex.DP)
    flxGroup.add(flxGroupLine, lblGroupValue);
    frmMBBankAssuranceDetails.flxDynamic.add(flxGroup);
    return currentHeight + flexHeight;
}

function addWidgetsfrmMBBankAssuranceDetailsContent(dataSetName, displayName, value, currentHeight) {
    addWidgetsfrmMBBankAssuranceDetailsContent(dataSetName, displayName, value, currentHeight, null);
}

function addWidgetsfrmMBBankAssuranceDetailsContent(dataSetName, displayName, value, currentHeight, isActive) {
    var lineHeight = 1;
    var labelHeight = 35;
    var valueHeight = 35; //default
    var flexHeight = 0;
    var flxContentLine = new kony.ui.FlexContainer({
        "id": "flxContentLine" + dataSetName + currentHeight,
        "top": "0dp",
        "left": "5%",
        "width": "90%",
        "height": lineHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "skin": "flexBlueLine",
        "focusSkin": "flexBlueLine",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxContentLine.setDefaultUnit(kony.flex.DP)
    flxContentLine.add();
    var lblContent = new kony.ui.Label({
        "id": "lblContent" + dataSetName + currentHeight,
        "top": lineHeight + "dp",
        "left": "0dp",
        "width": "100%",
        "height": labelHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "text": displayName,
        "skin": "lblGreyRegular142"
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });

    var currencyThaiBaht = kony.i18n.getLocalizedString("currencyThaiBaht");
    var charIndex = value.indexOf(currencyThaiBaht);
    var optionValue = "";

    if (charIndex != -1 && (charIndex + 1) != value.length) {
        optionValue = "(" + capitalizeWord(value.substring(charIndex + 1, value.length)) + ")";
        value = value.substring(0, charIndex + 1);
    }

    var countNewLine = (value.match(/\n/g) || []).length;
    if (countNewLine > 0) {
        valueHeight = valueHeight * (countNewLine + 1);
    }

    var lblContentValueSkin = "lblGrey48px";
    if (isActive != null) {
        if (isActive) {
            lblContentValueSkin = "lblGreen48px";
        }else{
        	lblContentValueSkin = "lblRed48px";
        }
    }
    var lblContentValue = new kony.ui.Label({
        "id": "lblContentValue" + dataSetName + currentHeight,
        "top": (lineHeight + labelHeight) + "dp",
        "left": "0dp",
        "width": "100%",
        "height": valueHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "text": value,
        "skin": lblContentValueSkin
    }, {
        "padding": [0, 0, 0, 0],
        "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
        "marginInPixel": false,
        "paddingInPixel": false,
        "containerWeight": 0
    }, {
        "textCopyable": false
    });

    flexHeight = lineHeight + labelHeight + valueHeight;
    var lblContentOptionValue = null;
    if (isNotBlank(optionValue)) {
        lblContentOptionValue = new kony.ui.Label({
            "id": "lblContentOptionValue" + dataSetName + currentHeight,
            "top": flexHeight + "dp",
            "left": "0dp",
            "width": "100%",
            "height": labelHeight + "dp",
            "zIndex": 1,
            "isVisible": true,
            "text": optionValue,
            "skin": "lblGreyRegular142"
        }, {
            "padding": [0, 0, 0, 0],
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "marginInPixel": false,
            "paddingInPixel": false,
            "containerWeight": 0
        }, {
            "textCopyable": false
        });
        flexHeight += labelHeight;
    }
    var flxContent = new kony.ui.FlexContainer({
        "id": "flxContent" + dataSetName + currentHeight,
        "top": currentHeight + "dp",
        "left": "0dp",
        "width": "100%",
        "height": flexHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxContent.setDefaultUnit(kony.flex.DP)
    if (isNotBlank(optionValue)) {
        flxContent.add(flxContentLine, lblContent, lblContentValue, lblContentOptionValue);
    } else {
        flxContent.add(flxContentLine, lblContent, lblContentValue);
    }
    frmMBBankAssuranceDetails.flxDynamic.add(flxContent);
    return currentHeight + flexHeight;
}

function addWidgetsfrmMBBankAssuranceDetailsBottom(currentHeight) {
    var lineHeight = 18;
    var flexHeight = lineHeight;
    var flxGroupBottomLine = new kony.ui.FlexContainer({
        "id": "flxGroupBottomLine",
        "top": "0dp",
        "left": "0dp",
        "width": "100%",
        "height": lineHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "skin": "flexBlueLine",
        "focusSkin": "flexBlueLine",
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxGroupBottomLine.setDefaultUnit(kony.flex.DP)
    flxGroupBottomLine.add();
    var flxGroupBottom = new kony.ui.FlexContainer({
        "id": "flxGroupBottom",
        "top": currentHeight + "dp",
        "left": "0dp",
        "width": "100%",
        "height": flexHeight + "dp",
        "zIndex": 1,
        "isVisible": true,
        "clipBounds": true,
        "layoutType": kony.flex.FREE_FORM
    }, {
        "padding": [0, 0, 0, 0]
    }, {});;
    flxGroupBottom.setDefaultUnit(kony.flex.DP)
    flxGroupBottom.add(flxGroupBottomLine);
    frmMBBankAssuranceDetails.flxDynamic.add(flxGroupBottom);
    return currentHeight + flexHeight;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function capitalizeWord(string) {
    var result = "";
    var words = string.split(" ");
    if (words.length > 1) {
        for (var index = 0; index < words.length; index++) {
            result += capitalizeFirstLetter(words[index]) + " ";
        }
        result = result.trim();
    } else {
        result = string;
    }
    return result;
}

function showBATaxDocEmailConfirmPopup() {
    if (validateEmail(gblEmailId)) {
        popAllowOrNot.imgIcon.setVisibility(false);
        popAllowOrNot.lblTitle.setVisibility(false);
        var message = kony.i18n.getLocalizedString("keyBATaxDocReqConfirmation");
        popAllowOrNot.lblMsgText.text = message.replace("[emailId]", gblEmailId);
        popAllowOrNot.btnDontAllow.text = kony.i18n.getLocalizedString("keyCancelButton");
        popAllowOrNot.btnAllow.text = kony.i18n.getLocalizedString("keyConfirm");
        popAllowOrNot.btnAllow.onClick = callBARequestTaxDocServiceMB;
        popAllowOrNot.show();
    } else {
        showAlert(kony.i18n.getLocalizedString("keyEmailInCorrect"), kony.i18n.getLocalizedString("info"));
    }
}

function callBARequestTaxDocServiceMB() {
    popAllowOrNot.imgIcon.setVisibility(true);
    popAllowOrNot.lblTitle.setVisibility(true);
    popAllowOrNot.dismiss();
    var locale = kony.i18n.getCurrentLocale();
    var languageExtension = "EN";
    if (locale == "en_US") {
        languageExtension = "EN";
    } else {
        languageExtension = "TH";
    }

    var policynumber = gblBAPolicyDetailsData["policyDetailsValueDS"][2]["value_" + languageExtension];
    var inputParam = {};
    showLoadingScreenPopup();
    inputParam["policyNumber"] = policynumber;
    inputParam["policyName"] = gblBAPolicyDetailsData["policyDetailsValueDS"][3]["value_EN"];

    invokeServiceSecureAsync("BAReqTaxDocument", inputParam, callBARequestTaxDocServiceCallBackMB);
}

function callBARequestTaxDocServiceCallBackMB(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0 && resulttable["StatusCode"] == "0000") {
            dismissLoadingScreenPopup();
            showAlert(kony.i18n.getLocalizedString("keyBATaxDocReqSuccess"), kony.i18n.getLocalizedString("info"));

        } else {
            dismissLoadingScreenPopup();
            if (resulttable["StatusCode"] == "BA3031") {
                showAlert(kony.i18n.getLocalizedString("keyBATaxDocReqAlready"), kony.i18n.getLocalizedString("info"));
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
        }
    }
}