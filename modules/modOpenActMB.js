/* 
Setting data for use accounts
*/
noofmnthLocale = "";
gblOpenProdAddress = false;
isVisibleEmailInput = true;
function setDataForUse() {
	gblSelProduct = "ForUse";

	frmOpenActSelProd.vbxOpenActSave.skin = vbxSaveFocus;
	frmOpenActSelProd.vbxOpenActUse.skin = vbxUse;
	frmOpenActSelProd.vbxOpenActTerm.skin = vbxTermFocus;
	
	frmOpenActSelProd.segOpenActSelProd.removeAll();
	frmOpenActSelProd.segOpenActSelProd.containerWeight = 100;
	frmOpenActSelProd.segOpenActSelProd.widgetDataMap = {
		imgOpnActSelProd: "imgOpnActSelProd",
		lblOpnActSelProd: "lblOpnActSelProd",
		imgOpnActSelProdRtArrow: "imgOpnActSelProdRtArrow"
	}
	
	var inputParam = {};
	inputParam["selectedCategory"] = gblSelProduct;
	//frmOpenActSelProd.lblUnderForUseTab.setVisibility(true);
	frmOpenActSelProd.line473302008978646.setVisibility(false);
	showLoadingScreen();
	invokeServiceSecureAsync("selectProdCategoryForOpenAcc", inputParam, callBackReceiveAckProdCategory);
}

function ProdDetails(){
    ProdDetails = new Object();
    this.add = function(key, value){
        ProdDetails[""+key+""] = value;
    };
    this.ProdDetails = ProdDetails;
}

/*****************************************************************
 *	Name    : callBackReceiveAckProdCategory
 *	Author  : Kony Solutions
 *	Purpose : getting prodcut codes from DB
 ******************************************************************/

function callBackReceiveAckProdCategory(status, callBackResponse) {
    if (status == 400) {
        if (callBackResponse["opstatus"] == 0) {
            if (callBackResponse["productList"] != undefined && callBackResponse["productList"].length > 0) {
                frmOpenActSelProd.lblUnderForUseTab.setVisibility(false);
                frmOpenActSelProd.line473302008978646.setVisibility(true); //Uncomment this due to DEF915
            } else {
                frmOpenActSelProd.lblUnderForUseTab.setVisibility(true);
                frmOpenActSelProd.line473302008978646.setVisibility(false);
            }
			//try {
				//gblProudctDetails = new ProdDetails();
			//}catch(e){
				//we are getting prodDetails is not contructor, need to look into this, but this is not stopping functionality
				//alert(e);
			//}
			gblProudctDetails = {};
            var segment_temp = [];
            var prodName;
            for (var i1 = 0; i1 < callBackResponse["productList"].length; i1++) {
                var locale = kony.i18n.getCurrentLocale();
                if (kony.i18n.getCurrentLocale() == "en_US") {
                    prodName = callBackResponse["productList"][i1]["PRODUCT_NAME_EN"];
                } else {
                    prodName = callBackResponse["productList"][i1]["PRODUCT_NAME_TH"];
                }
				
				kony.print("@@in open accounts flow@@");
                imageProduct = loadAccntProdImagesdetails(callBackResponse["productList"][i1]["ICON_ID"]);//"https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" + appConfig.middlewareContext + "/" + "ImageRender?crmId=&" + "&personalizedId=&billerId=" + callBackResponse["productList"][i1]["ICON_ID"] + "&modIdentifier=PRODICON";
                if (gblSelProduct == "ForUse") {
                    segment_temp.push({
                        "lblOpnActSelProd": prodName,
                        "productcode": callBackResponse["productList"][i1]["PROD_CODE"],
                        "imgOpnActSelProd": imageProduct,
                        hiddenProdNameEN: callBackResponse["productList"][i1]["PRODUCT_NAME_EN"],
                        hiddenProdNameTH: callBackResponse["productList"][i1]["PRODUCT_NAME_TH"],
                        hiddenActTypeEN: callBackResponse["productList"][i1]["TYPE"],
                        hiddenActTypeTH: callBackResponse["productList"][i1]["TYPE_TH"],
                        hiddenProdDes: callBackResponse["productList"][i1]["PROD_DESCRIPTION"],
                        hiddenIssueCardOnline: callBackResponse["productList"][i1]["ISSUE_CARD_ONLINE"],
                        imgOpnActSelProdRtArrow: "bg_arrow_right_grayb.png"
                    })
                } else if (gblSelProduct == "ForSave") {
                    segment_temp.push({
                        "lblOpnActSelProd": prodName,
                        "productcode": callBackResponse["productList"][i1]["PROD_CODE"],
                        "imgOpnActSelProd": imageProduct,
                        hiddenProdNameEN: callBackResponse["productList"][i1]["PRODUCT_NAME_EN"],
                        hiddenProdNameTH: callBackResponse["productList"][i1]["PRODUCT_NAME_TH"],
                        hiddenActTypeEN: callBackResponse["productList"][i1]["TYPE"],
                        hiddenActTypeTH: callBackResponse["productList"][i1]["TYPE_TH"],
                        hiddenProdDes: callBackResponse["productList"][i1]["PROD_DESCRIPTION"],
                        hiddenIssueCardOnline: callBackResponse["productList"][i1]["ISSUE_CARD_ONLINE"],
                        imgOpnActSelProdRtArrow: "bg_arrow_right_grayb.png"
                    })
                } else if (gblSelProduct == "ForTerm") {
                    segment_temp.push({
                        "lblOpnActSelProd": prodName,
                        "productcode": callBackResponse["productList"][i1]["PROD_CODE"],
                        "imgOpnActSelProd": imageProduct,
                        hiddenProdNameEN: callBackResponse["productList"][i1]["PRODUCT_NAME_EN"],
                        hiddenProdNameTH: callBackResponse["productList"][i1]["PRODUCT_NAME_TH"],
                        hiddenActTypeEN: callBackResponse["productList"][i1]["TYPE"],
                        hiddenActTypeTH: callBackResponse["productList"][i1]["TYPE_TH"],
                        hiddenProdDes: callBackResponse["productList"][i1]["PROD_DESCRIPTION"],
                        imgOpnActSelProdRtArrow: "bg_arrow_right_grayb.png"
                    })
                }
                
                var prodCode = callBackResponse["productList"][i1]["PROD_CODE"];
				// added below code for campaign link for product brief screen
				if(undefined != gblProdCode && "" != gblProdCode && gblProdCode == prodCode){
					//alert("found prod code : "+prodCode);
					var prodCode = prodCode;
					//alert("after cmp code");
					var obj1 = new Object();
					obj1.prodName = prodName;
					obj1.prodCode = prodCode;
					obj1.prodNameEN = callBackResponse["productList"][i1]["PRODUCT_NAME_EN"];
					obj1.prodNameTH = callBackResponse["productList"][i1]["PRODUCT_NAME_TH"];
					obj1.actTypeEN = callBackResponse["productList"][i1]["TYPE"];
					obj1.actTypeTH = callBackResponse["productList"][i1]["TYPE_TH"];
					obj1.prodDes = callBackResponse["productList"][i1]["PROD_DESCRIPTION"];
					obj1.issueCardOnline = callBackResponse["productList"][i1]["ISSUE_CARD_ONLINE"];
					obj1.imageProduct = imageProduct;
					//gblProudctDetails.add(prodCode, obj1);
					gblProudctDetails[prodCode] = obj1;
					//alert("test point 22");	
				}
            }
			dismissLoadingScreen();
            frmOpenActSelProd.segOpenActSelProd.setData(segment_temp);
                        
            if(isCmpFlow){
            	snippetCodeOnClickOfAcctSegmentMB();
			}
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;

        }

    }
}

/* 
Setting data for save accounts
*/
function setDataForSave() {
	gblSelProduct = "ForSave";
    frmOpenActSelProd.vbxOpenActSave.skin = vbxSave;
    frmOpenActSelProd.vbxOpenActUse.skin = vbxUseFocus;
    frmOpenActSelProd.vbxOpenActTerm.skin = vbxTermFocus;
	frmOpenActSelProd.segOpenActSelProd.removeAll();
	frmOpenActSelProd.segOpenActSelProd.containerWeight = 100;
	frmOpenActSelProd.segOpenActSelProd.widgetDataMap = {
		imgOpnActSelProd: "imgOpnActSelProd",
		lblOpnActSelProd: "lblOpnActSelProd",
		imgOpnActSelProdRtArrow: "imgOpnActSelProdRtArrow"
	}
	
	var inputParam = {};
	inputParam["selectedCategory"] = gblSelProduct;
	showLoadingScreen();
	invokeServiceSecureAsync("selectProdCategoryForOpenAcc", inputParam, callBackReceiveAckProdCategory);
}

/* 
Setting data for TD accounts
*/

function setDataForTD() {
	gblSelProduct = "ForTerm";

	frmOpenActSelProd.vbxOpenActSave.skin = vbxSaveFocus;
	frmOpenActSelProd.vbxOpenActUse.skin = vbxUseFocus;
	frmOpenActSelProd.vbxOpenActTerm.skin = vbxTerm;

	frmOpenActSelProd.segOpenActSelProd.removeAll();
	frmOpenActSelProd.segOpenActSelProd.containerWeight = 100;
	frmOpenActSelProd.segOpenActSelProd.widgetDataMap = {
		imgOpnActSelProd: "imgOpnActSelProd",
		lblOpnActSelProd: "lblOpnActSelProd",
		imgOpnActSelProdRtArrow: "imgOpnActSelProdRtArrow"
	}
	
	var inputParam = {};
	inputParam["selectedCategory"] = gblSelProduct;
	showLoadingScreen();
	invokeServiceSecureAsync("selectProdCategoryForOpenAcc", inputParam,callBackReceiveAckProdCategory);
}


function frmOpenProdDetnTnCInterestRate(){
	//var selectedProdList = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["productcode"];
	var inputParam = {};
	//gblFinActivityLogOpenAct = [];
	inputParam["selectedCategory"] = "";
	
	// added below if else to handle the campaign internal link to product breif screen, MIB-971
	if(isCmpFlow){
		//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
		var prodD = gblProudctDetails[gblProdCode];
		//alert("Product Name : "+prodD.prodName);
		//frmIBOpenProdDetnTnC.lblOpenActDescSubTitle.text = prodD.prodName
		inputParam["productCode"] = gblProdCode;
		gblFinActivityLogOpenAct["toProductName"] = prodD.prodName;
	} else {
		inputParam["productCode"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["productcode"];
		gblFinActivityLogOpenAct["toProductName"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["lblOpnActSelProd"];
	}
	isCmpFlow = false;
	
	showLoadingScreen();
	if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) < 0){
	//if(gblSelOpenActProdCode != "211")
	//{
	frmOpenProdDetnTnC.btnRight.setVisibility(false);
	frmOpenProdDetnTnC.hboxSaveCamEmail.setVisibility(false);
				 
	frmOpenProdDetnTnC.btnOpenProdAgree.text = kony.i18n.getLocalizedString('keyAgreeButton');
    frmOpenProdDetnTnC.btnOpenProdCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
	frmOpenProdDetnTnC.btnOpenProdAgree.setVisibility(false);
	frmOpenProdDetnTnC.btnOpenProdCancel.setVisibility(false);
	frmOpenProdDetnTnC.btnOpenProdContinue.setVisibility(true); 
	frmOpenProdDetnTnC.btnTermsBack.setVisibility(true);
	frmOpenProdDetnTnC.btnOpenProdContinue.text = kony.i18n.getLocalizedString("Next");
	frmOpenProdDetnTnC.btnTermsBack.text = kony.i18n.getLocalizedString("Back");

	
	frmOpenProdDetnTnC.richtext47425439023817.setVisibility(false);
	frmOpenProdDetnTnC.hbxOpnProdDet.setVisibility(true);
	frmOpenProdDetnTnC.hbxImgProdPackage.setVisibility(false);
				   
	var locale = kony.i18n.getCurrentLocale();
	if (kony.i18n.getCurrentLocale() == "en_US") {
		frmOpenProdDetnTnC.lblOpenActDescSubTitle.text  = gblFinActivityLogOpenAct["prodNameEN"];
	} else {
		frmOpenProdDetnTnC.lblOpenActDescSubTitle.text  = gblFinActivityLogOpenAct["prodNameTH"];
	}
	//frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["lblOpnActSelProd"];
	//gblFinActivityLogOpenAct["toProductName"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0]["lblOpnActSelProd"];
				 
	
	if(gblSelOpenActProdCode == "220"){
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNoFeeProdDet');
    }else if(gblSelOpenActProdCode == "221"){ 
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyNoFixedProdDet');
    } else if(gblSelOpenActProdCode == "222" ){
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyFreeFlowProdDet');
    }else if(gblSelOpenActProdCode == "206"){
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyDreamProdDet');
    }else if(gblSelOpenActProdCode == "203"){
      arrbenf1 = [];
      arrbenf2 = [];
      arrbenf3 = [];
      arrbenf4 = [];
      arrbenf5 = [];
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keySavingCareProdDet');
    }else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602"){
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermProdDet');
    } else if(gblSelOpenActProdCode == "659"){
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermUpProdDet');
    } else if(gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
      frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString('keyTermQuickProdDet');
    }
				
	frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
	frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
	frmOpenProdDetnTnC.btnRight.skin = "btnShare";
	frmOpenProdDetnTnC.lblOpenActDescSubTitle.setFocus(true);
	frmOpenProdDetnTnC.lblHdrTxt.text = kony.i18n.getLocalizedString('keyOpenAcc');
	}
	invokeServiceSecureAsync("getInterestRate", inputParam, callBackReceiveAckInterest);
	
}

/*****************************************************************
 *	Name    : callBackReceiveAckInterest
 *	Author  : Kony Solutions
 *	Purpose : getting interest rate from DB
 ******************************************************************/

function callBackReceiveAckInterest(status, callBackResponse) 
{
	if (status == 400) 
	{
			if (callBackResponse["opstatus"] == 0)
			{
				 var segment_temp = [];
				 //for (var i1 = 0; i1 < callBackResponse["SavingTargets"].length; i1++) 
				// {
					
					if(callBackResponse["SavingTargets"] != null && callBackResponse["SavingTargets"] != undefined && callBackResponse["SavingTargets"].length != 0 ){
					frmOpenProdDetnTnC.lblOpenAccInt.text = callBackResponse["SavingTargets"][0]["DATA_VALUE"] + "%";
					gblInterest = callBackResponse["SavingTargets"][0]["DATA_VALUE"] + "%";
					var locale = kony.i18n.getCurrentLocale();
						if (kony.i18n.getCurrentLocale() == "en_US") {
							frmOpenProdDetnTnC.lblOpenAccIntType.text = callBackResponse["SavingTargets"][0]["CATEGORY_EN"];
						} else {
							frmOpenProdDetnTnC.lblOpenAccIntType.text = callBackResponse["SavingTargets"][0]["CATEGORY_TH"];
						}
					
						gblFinActivityLogOpenAct["intersetRateLabelEN"] = callBackResponse["SavingTargets"][0]["CATEGORY_EN"];
						gblFinActivityLogOpenAct["intersetRateLabelTH"] = callBackResponse["SavingTargets"][0]["CATEGORY_TH"];
					}else{
						frmOpenProdDetnTnC.lblOpenAccInt.text = "0.00%";
					}					 
					 gblMinOpenAmt = callBackResponse["minLimitOpenAct"];
					 gblMaxOpenAmt = callBackResponse["maxLimitOpenAct"];
					 gblisDisToActTD = callBackResponse["isDisToActTD"];
					 gblCardImageForSave = callBackResponse["cardImg"];
					 gblproduct_details_i18n_key = callBackResponse["tPhraseContentId"];
					 gblFinActivityLogOpenAct["cardBinNo"] = callBackResponse["cardBinNo"];
					 gblFinActivityLogOpenAct["cardFee"] = callBackResponse["cardFee"];
					 gblFinActivityLogOpenAct["cardImg"] = callBackResponse["cardImg"];
					 gblFinActivityLogOpenAct["cardType"] = callBackResponse["cardType"];
					 gblFinActivityLogOpenAct["custType"] = callBackResponse["custType"];
				 //}
				 frmOpenProdDetnTnC.btnRight.setVisibility(false);
				 frmOpenProdDetnTnC.hboxSaveCamEmail.setVisibility(false);
				 frmOpenProdDetnTnC.hbxOpnProdDet.setVisibility(true);
				 
				   frmOpenProdDetnTnC.btnOpenProdAgree.setVisibility(false);
				   frmOpenProdDetnTnC.btnOpenProdCancel.setVisibility(false);
				   frmOpenProdDetnTnC.btnOpenProdContinue.setVisibility(true);
				   
				   if(gblOpenActList["FOR_USE_PRODUCT_CODES"] != null && gblOpenActList["FOR_USE_PRODUCT_CODES"] != undefined && gblOpenActList["FOR_USE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) > 0){ 
						frmOpenProdDetnTnC.richTextProdDetails.text = kony.i18n.getLocalizedString(gblproduct_details_i18n_key);
						var product_package_image_name = gblCardImageForSave;
						var product_package_image="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+product_package_image_name+"&modIdentifier=PRODUCTPACKAGEIMG";
						frmOpenProdDetnTnC.imgProductPackage.src = product_package_image;
						frmOpenProdDetnTnC.hbxImgProdPackage.setVisibility(true);
					}
              
				// frmOpenProdDetnTnC.lblOpenActDesc.text = kony.i18n.getLocalizedString('TCMessage');
				 frmOpenProdDetnTnC.imgHeaderMiddle.src = "arrowtop.png"
				 frmOpenProdDetnTnC.imgHeaderRight.src = "empty.png"
				 frmOpenProdDetnTnC.btnRight.skin = "btnShare";
				 frmOpenProdDetnTnC.lblOpenActDescSubTitle.setFocus(true);
				 dismissLoadingScreen();
				 //if(gblSelOpenActProdCode == "211")
              	frmOpenProdDetnTnC.richTextProdDetails.setVisibility(true);
				if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0){
					gblSavingsCareFlow = "openAccount";
				 	showSavingCareProductBrief();
				}else if(gblSelOpenActProdCode=="221"){
                  showSavingCareProductBrief();
                }else{
					 frmOpenProdDetnTnC.show();
				}	
			}else{
			dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;			
			}
	}
}


function onClickAgreeOpenDSActs(){
	
	showLoadingScreen();
	var inputparam = {};
	invokeServiceSecureAsync("getDreamSavingTargets", inputparam, callBackOpenDreamSavingTargets);
}

function callBackOpenDreamSavingTargets(status, resulttable){
	
	if (status == 400) {
	
		if (resulttable["opstatus"] == 0) {
				dismissLoadingScreen();	
				dsSelActSelIndex = [0,0];
				frmOpnActSelAct.imgNSProdName.src = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
				
				if (kony.i18n.getCurrentLocale() == "en_US") {
				frmOpnActSelAct.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
				}else{
				frmOpnActSelAct.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
				}
				
				
				var temp_seg = [];
					//frmOpenAcDreamSaving.txtODTargetAmt.text = gblMinOpenAmt;
					if (frmOpenAcDreamSaving.txtODTargetAmt.text == "0" || frmOpenAcDreamSaving.txtODTargetAmt.text == null || frmOpenAcDreamSaving.txtODTargetAmt.text == ("0.00" + kony.i18n.getLocalizedString("currencyThaiBaht"))){
						frmOpenAcDreamSaving.hbxODMnthSavAmt.setEnabled(false);
						frmOpenAcDreamSaving.hbxODMyDream.setEnabled(false);
					}else{
						frmOpenAcDreamSaving.hbxODMnthSavAmt.setEnabled(true);
						frmOpenAcDreamSaving.hbxODMyDream.setEnabled(true);
					}
					frmOpenAcDreamSaving.txtODTargetAmt.skin = txtNormalBG;
					frmOpenAcDreamSaving.txtODNickNameVal.skin = txtNormalBG;
					frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtFocusBG;
					frmOpenAcDreamSaving.txtODMyDream.skin = txtFocusBG;
					
                    frmOpenAcDreamSaving.segSliderOpenDream.removeAll();
                    frmOpenAcDreamSaving.segSliderOpenDream.containerWeight = 100;
                    frmOpenAcDreamSaving.segSliderOpenDream.widgetDataMap = {
                      imgDreamPic: "imgDreamPic",
                      imgDreamName: "imgDreamName"

                    }
					for (j=0; j< resulttable["SavingTargets"].length; j++){
					
							var dreamName; 
							var dreamimage;
							var locale = kony.i18n.getCurrentLocale();
							
							if (locale == "en_US") {
								dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"];
							} else {
								dreamName = resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_TH"];
							}
							
							if (resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Vacation"){
								dreamimage = "prod_lrg_vacation.png";
							}else if(resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Dream"){
								dreamimage = "prod_lrg_dream.png";
							}else if(resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Car"){
								dreamimage = "prod_lrg_car.png";
							}else if(resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Education"){
								dreamimage = "prod_lrg_education.png";
							}else if(resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Own House"){
								dreamimage = "prod_lrg_homeloan.png";
							}else if(resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Business"){
								dreamimage = "prod_lrg_business.png";
							}else if(resulttable["SavingTargets"][j]["DREAM_TARGET_DESC_EN"] == "My Saving"){
								dreamimage = "prod_lrg_saving.png";
							}
							
								
                            temp_seg.push({
                              "imgDreamName": dreamName,
                              "imgDreamPic": dreamimage,
                              hiddenDreamTargetId: resulttable["SavingTargets"][j]["DREAM_TARGET_ID"],
                              template: hbxSliderDream
                            })
								
					}
					frmOpenAcDreamSaving.segSliderOpenDream.setData(temp_seg);
					
					frmOpenAcDreamSaving.segSliderOpenDream.selectedIndex = dsSelActSelIndex;
					dsSelActLength = frmOpenAcDreamSaving.segSliderOpenDream.data.length - 1;
					frmOpenAcDreamSaving.btnDSNext.text = kony.i18n.getLocalizedString("Next");
          
					dismissLoadingScreen();
					frmOpenAcDreamSaving.label475124774164.text = kony.i18n.getLocalizedString("keyMBMydream");
					frmOpenAcDreamSaving.lblODPicIt.text = kony.i18n.getLocalizedString("keyPictureIt");
					frmOpenAcDreamSaving.lblODDreamDesc.text = kony.i18n.getLocalizedString("DreamDes");
					frmOpenAcDreamSaving.lblODTargetAmt.text = kony.i18n.getLocalizedString("keyTargetAmnt");
					frmOpenAcDreamSaving.lblBuildUpDream.text = kony.i18n.getLocalizedString("keyBuildMyDream");
					frmOpenAcDreamSaving.lblODMnthSavAmt.text = kony.i18n.getLocalizedString("keyMBMonthlySavingAmnt");
					frmOpenAcDreamSaving.lblODMyDream.text = kony.i18n.getLocalizedString("keyMBMonthToMyDream");
					frmOpenAcDreamSaving.txtODTargetAmt.placeholder = kony.i18n.getLocalizedString("keyTargetAmt");
							
					frmOpenAcDreamSaving.label47505874738694.text = kony.i18n.getLocalizedString("keyor");
					frmOpenAcDreamSaving.show();
		}else{
			dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;		
		}
	}
}
	


function showDateDreamSav(){
	var SelDate=[];			
		for(var i=1;i<=31;i++)
		{
			 
			 //#ifdef android
					i = i + "";
					//i = kony.string.split(i, ".")
					//i =i[0];	
			 //#endif
			   	var temp={
			   		"lblDreamSaveDate":i,"imgDreamSaveDate":"bg_arrow_right_grayb.png"
			   	}
			kony.table.insert(SelDate,temp)
		}
		popSelDate.segDreamSaveDate.data=SelDate;
		showPopSelDate(onclickSelDateOnPop);
	}

var dsSelActSelIndex = [0, 0];
var dsSelActLength = 0;

function setDreamSaveSetData() {
	var dreamMnthTextBaht = frmOpenAcDreamSaving.txtODMyDream.text;
	var dreamMnthText= dreamMnthTextBaht.replace(kony.i18n.getLocalizedString("keymonths"),"")
	frmOpenAcDreamSaving.txtODMyDream.text=dreamMnthText;  
	var myDreamAmtBaht = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
	myDreamAmt= myDreamAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
	var targetAmtBaht = frmOpenAcDreamSaving.txtODTargetAmt.text;
	targetAmt= targetAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
	var DreamDescOpen = frmOpenAcDreamSaving.txtODNickNameVal.text;
	var devNameFlag = validateLength(frmOpenAcDreamSaving.txtODNickNameVal.text, 3, 20);
	var monthlySavingNo= new Number(myDreamAmt.trim());
	var targetNo= new Number(targetAmt.trim())
	if (devNameFlag == false) {
			showAlert(kony.i18n.getLocalizedString("keyentercorrectdreamDesc"), kony.i18n.getLocalizedString("info"));
			  frmOpenAcDreamSaving.txtODNickNameVal.skin = txtErrorBG;
			return false;
		}
	 
	
	if (targetAmt == "" || targetAmt == null ||targetAmt ==0 ){
	showAlert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), kony.i18n.getLocalizedString("info"));
	frmOpenAcDreamSaving.txtODTargetAmt.skin = txtErrorBG;
	return false;
	}
	if(!kony.string.isNumeric(targetNo)){
		showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
			frmOpenAcDreamSaving.txtODTargetAmt.skin = txtErrorBG;
			return false;
		}
	if (myDreamAmt == "" ||  myDreamAmt == null ){
	showAlert(kony.i18n.getLocalizedString("keydreamMnthlySavingANdMnthToDream"), kony.i18n.getLocalizedString("info"));
	frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtErrorBG;
	return false;
	}
	if(!kony.string.isNumeric(monthlySavingNo)){
      showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
      frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtErrorBG;
      return false;
    }
	if (dreamMnthText == "" ||  dreamMnthText == null){
	showAlert(kony.i18n.getLocalizedString("keydreamMnthlySavingANdMnthToDream"), kony.i18n.getLocalizedString("info"));
	frmOpenAcDreamSaving.txtODMyDream.skin = txtErrorBG;
	return false;
	}
	
	if (!(amountValidationMBOpenAct(targetAmt))){
	showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), "Info");
	frmOpenAcDreamSaving.txtODTargetAmt.skin = txtErrorBG;
	return false;
	}	
	if ((kony.os.toNumber(myDreamAmt)) > (kony.os.toNumber(targetAmt))){
	showAlert(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
	frmOpenAcDreamSaving.txtODMyDream.skin = txtErrorBG;
	return false;
	}
	if(myDreamAmt != ""){
		var pat1 = /[A-Za-z]/g;
		var pat2 = /[0-9]/g;
		var isAlpha = pat1.test(myDreamAmt);
		var isNum = pat2.test(myDreamAmt);
	
		if((isAlpha==true && isNum==true) || (isAlpha == true)){
			showAlert(kony.i18n.getLocalizedString("keyMonthlyLimit"), kony.i18n.getLocalizedString("info"));
			frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtErrorBG;
			return false;
		}
	
	}	
			
	if ((kony.os.toNumber(targetAmt)) < (kony.os.toNumber(gblMinOpenAmt))){
			//showAlert("Please enter minimum target amount to proceed.", kony.i18n.getLocalizedString("info"));
	showAlert(kony.i18n.getLocalizedString("keyLessTargetAmt"), kony.i18n.getLocalizedString("info"));
			frmOpenAcDreamSaving.txtODTargetAmt.skin = txtErrorBG;
			 return false				
	}
	
	if ((kony.os.toNumber(myDreamAmt)) < (kony.os.toNumber(gblMinOpenAmt))){
			showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
			frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtErrorBG;
			 return false				
	}
				
	if (gblMaxOpenAmt != "No Limit")
		{
			if ((kony.os.toNumber(myDreamAmt)) > (kony.os.toNumber(gblMaxOpenAmt))){
			showAlert(kony.i18n.getLocalizedString("keydreamminimumAmount"), kony.i18n.getLocalizedString("info"));
			frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtErrorBG;
			return false				
			}
				
		}
	
	
	if (dreamMnthText == "0" || kony.string.containsChars(dreamMnthText, ["."])){
	showAlert(kony.i18n.getLocalizedString("keyValidMonths"), kony.i18n.getLocalizedString("info"));
	frmOpenAcDreamSaving.txtODMyDream.skin = txtErrorBG;
	return false;
	}
	
	frmOpenActDSConfirm.imgOADreamDetail.src = frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].imgDreamPic;
	frmOpenActDSConfirm.lblOADreamDetailTarAmtVal.text = commaFormattedOpenAct(targetAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
	
	dsSelActSelIndex = [0, 0];
	dsSelActLength = 0;
	frmOpenAcDreamSaving.txtODTargetAmt.skin = txtNormalBG;
	frmOpenAcDreamSaving.txtODNickNameVal.skin = txtNormalBG;
	frmOpenAcDreamSaving.txtODMnthSavAmt.skin = txtFocusBG;
	frmOpenAcDreamSaving.txtODMyDream.skin = txtFocusBG;
	var ifAccounts = true;
	
	frmOpnActSelAct.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
    ifAccounts = setDataforOpenFromActs(frmOpnActSelAct.segNSSlider);
    if (ifAccounts == false){
    	return false;
    }
    frmOpnActSelAct.segNSSlider.selectedIndex = dsSelActSelIndex;
    dsSelActLength = frmOpnActSelAct.segNSSlider.data.length - 1;
	frmOpnActSelAct.hbxSelActAmt.setVisibility(false);
	frmOpnActSelAct.hbxSelActSelNickName.setVisibility(true);
	frmOpnActSelAct.txtDSAmountSel.setVisibility(true);
	frmOpnActSelAct.line1.setVisibility(false);
	frmOpnActSelAct.line2.setVisibility(true);
	frmOpnActSelAct.line3.setVisibility(true);
	frmOpnActSelAct.line4.setVisibility(true);
		if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != ""){
		frmOpnActSelAct.txtOpenActNicNam.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
		}else{
		frmOpnActSelAct.txtOpenActNicNam.maxTextLength = 20;
		}
	frmOpnActSelAct.txtOpenActNicNam.text = "";
	frmOpnActSelAct.txtOpenActNicNam.skin = txtNormalBG;
	frmOpnActSelAct.btnDreamSavecombo.setVisibility(true);
	frmOpnActSelAct.lblSelectSlider.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
	frmOpnActSelAct.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
	frmOpnActSelAct.label45731775454460.text = kony.i18n.getLocalizedString("Nickname");
	frmOpnActSelAct.btnDreamSavecombo.text = kony.i18n.getLocalizedString("keyTransferEvryMnthOpen");
	//frmOpnActSelAct.txtDSAmountSel.text = commaFormattedOpenAct(myDreamAmt) + kony.i18n.getLocalizedString("currencyThaiBaht") +"/" + kony.i18n.getLocalizedString("keyCalendarMonth");
	frmOpenActDSConfirm.lblMyDreamDesVal.text = frmOpenAcDreamSaving.txtODNickNameVal.text;
	frmOpenActDSConfirm.lblOADSNickNameVal.text = frmOpnActSelAct.txtOpenActNicNam.text;
	
	calMonthlySavingCalculatorOpen();
	frmOpnActSelAct.txtDSAmountSel.text = frmOpenAcDreamSaving.txtODMnthSavAmt.text +"/" + kony.i18n.getLocalizedString("keyCalendarMonth");
	frmOpnActSelAct.show();
}	



function onClickLeftDS(currForm) {
	dsSelActSelIndex = currForm.selectedIndex;
	if(dsSelActLength<1){
		 return false;
		 }	
	if (dsSelActSelIndex[1] == 0) {
		currForm.selectedIndex = [0, dsSelActLength];
	} else {
		currForm.selectedIndex = [0, (dsSelActSelIndex[1] - 1)];
	}
	dsSelActSelIndex = currForm.selectedIndex;
}

function onClickRightDS(currForm) {
	dsSelActSelIndex = currForm.selectedIndex;
		if(dsSelActLength<1){
		 return false;
		 }
		
	if (dsSelActSelIndex[1] == dsSelActLength) {
		currForm.selectedIndex = [0, 0];
	} else {
		currForm.selectedIndex = [0, (dsSelActSelIndex[1] + 1)];
	}
	dsSelActSelIndex = currForm.selectedIndex;
}

function onClickConnectDSSeg(currForm) {
	//var currForm = kony.application.getCurrentForm()
	dsSelActSelIndex = currForm.selectedIndex;
}

function showTransPwdPopForOpenDS(){
	 gblVerifyOTP=0;
	 frmOpenActDSAck.btnOADSAckOpenMore.text = kony.i18n.getLocalizedString("btnOpenMore");
  
    if(gblAuthAccessPin == true){
      showAccesspinPopup();
    }else{
      	 //popupTractPwd.lblPopupTranscPwd.text=  kony.i18n.getLocalizedString("transPasswordSub ")
	    showOTPPopup(kony.i18n.getLocalizedString("TransactionPass")+":","","",onClickConfirmPopDS,3)
    }

}


function onClickConfirmPopDS(){
kony.print("inside onClickConfirmPopDS");

  if(gblAuthAccessPin == false){
    if (popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text==""){
		setTransPwdFailedError(kony.i18n.getLocalizedString("emptyMBTransPwd"))
		return false;
	}
  }
	
	//else {
		//checkVerifyPWDTransOpenActs();
				var fromActId;
				var fromActName;
				var fromActNickName;
				var fromFiident;
				var fromActType;
				var toActNum;
				var toActType;
				var toFiident;
				var dreamAmt;
				var transferAmount;
				var fromProdId;
		if (gblSelProduct == "TMBDreamSavings"){
					
					var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
					tarAmount = tarAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "");	
					fromActName = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActName; 
					fromActId = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblActNoval;
					
					
					gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
					
					fromActId = fromActId.toString().replace(/-/g, "");
					fromActNickName = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblCustName;
					fromFiident = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenFiident;
					fromActType = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActType;
					fromProdId = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenProductId;
					transferAmount = frmOpenActDSConfirm.lblOADSAmtVal.text;
					var bhat =kony.i18n.getLocalizedString("currencyThaiBaht");
					transferAmount = transferAmount.replace(bhat,"").replace(/,/g, "");
					dreamAmt = frmOpenActDSConfirm.lblOADSAmtVal.text;
					dreamAmt = dreamAmt.replace(bhat,"").replace(/,/g, "");
					var dreamMonth = frmOpenActDSConfirm.lblOADMnthVal.text;
					
					gblFinActivityLogOpenAct["fromActId"] = fromActId;
					gblFinActivityLogOpenAct["fromActName"] = fromActName;
					gblFinActivityLogOpenAct["fromActType"] = fromActType;
					gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
					gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
					gblFinActivityLogOpenAct["fromProdId"] = fromProdId;
					gblFinActivityLogOpenAct["toActType"] = "SDA";
					gblFinActivityLogOpenAct["toActName"] = fromActName;
					gblFinActivityLogOpenAct["toFiident"] = "";
					gblFinActivityLogOpenAct["toActId"] = "";
					gblFinActivityLogOpenAct["transferAmount"] = transferAmount;
					gblFinActivityLogOpenAct["dreamAmt"] = dreamAmt;
					gblFinActivityLogOpenAct["dreamMonth"] = dreamMonth;
					gblFinActivityLogOpenAct["channelName"] = "MB";
					gblFinActivityLogOpenAct["tdInterestRate"] = frmOpenProdDetnTnC.lblOpenAccInt.text;
					gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				   gblFinActivityLogOpenAct["affiliatedAcctId"] = "";
								
                if(gblAuthAccessPin == true){
                  validateOTPtextOpenAccountsJavaService(gblNum,dreamAmt,frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].imgDreamName,tarAmount,"",frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].hiddenDreamTargetId,"02",frmOpenAcDreamSaving.txtODNickNameVal.text);
                }else{
                  validateOTPtextOpenAccountsJavaService(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text,dreamAmt,frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].imgDreamName,tarAmount,"",frmOpenAcDreamSaving["segSliderOpenDream"]["selectedItems"][0].hiddenDreamTargetId,"02",frmOpenAcDreamSaving.txtODNickNameVal.text);  
                }
		 		
		
		
		}else if(gblSelProduct == "ForTerm"){
		 
		 
		 			if (gblisDisToActTD == "Y")
					{	
					toActNum = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].lblActNoval; 
					toActType = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenActType;
					toActNum = toActNum.toString().replace(/-/g, "");
					toFiident = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenFiident;
					gblFinActivityLogOpenAct["toActType"] = toActType;
					gblFinActivityLogOpenAct["affiliatedAcctNickname"] = frmOpenActTDConfirm.lblTDTransToNickName.text;
		    		gblFinActivityLogOpenAct["affiliatedAcctId"] = frmOpenActTDConfirm.lblTDTransToAccBal.text;
		    		gblFinActivityLogOpenAct["affiliatedAcctName"] = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenActName;
					}else{
					toActNum = ""; 
					toActType = "";
					toFiident = "";
					gblFinActivityLogOpenAct["toActType"] = "CDA";
					gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				    gblFinActivityLogOpenAct["affiliatedAcctId"] = "";
				    gblFinActivityLogOpenAct["affiliatedAcctName"] = "";
					}
					
					fromActName = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenActName; 
					fromActId = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblActNoval;
					fromActNickName = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblCustName;
					fromFiident = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenFiident;
					fromActType = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenActType;
					
					gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
					fromActId = fromActId.toString().replace(/-/g, "");
					
					gblFinActivityLogOpenAct["fromActId"] = fromActId;
					gblFinActivityLogOpenAct["fromActName"] = fromActName;
					gblFinActivityLogOpenAct["fromActType"] = fromActType;
					gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
					gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
					gblFinActivityLogOpenAct["toActName"] = fromActName;
					gblFinActivityLogOpenAct["toFiident"] = toFiident;
					gblFinActivityLogOpenAct["toActId"] = toActNum;
					gblFinActivityLogOpenAct["dreamAmt"] = "";
					gblFinActivityLogOpenAct["dreamMonth"] = "";
					gblFinActivityLogOpenAct["fromProdId"] = "";
					gblFinActivityLogOpenAct["channelName"] = "MB";
					gblFinActivityLogOpenAct["tdInterestRate"] = frmOpenActTDConfirm.lblOATDIntRateVal.text.replace("%", "");
		 
		   if(gblAuthAccessPin == true){
             validateOTPtextOpenAccountsJavaService(gblNum,"","","","","","02","");
           }else{
             validateOTPtextOpenAccountsJavaService(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text,"","","","","","02","");    
           }
		 
		
		}else if(gblSelProduct == "TMBSavingcare"){
					kony.print("Inside TMBSavingcare product---------->1111111111111");
					var fname = "";
					var lname = "";
					var relation = ""
					var percentage = "";
					var bName = "";
					for(var i = 0;i<arrayBenificiary.length;i++){
						fname = fname + arrayBenificiary[i]["firstName"] + arrayBenificiary[i]["lastName"]  + " " + arrayBenificiary[i]["relation"] + " " + arrayBenificiary[i]["percentage"] + "," ;
						relation = relation + arrayBenificiary[i]["relation"]+",";
						percentage = percentage + arrayBenificiary[i]["percentage"]+",";
						percentage = percentage.replace("%", "");
						bName = bName + arrayBenificiary[i]["firstName"]+ " " + arrayBenificiary[i]["lastName"] + ",";		
					}
					bName = bName.substring(bName, bName.length - 1);
					relation = relation.substring(relation, relation.length - 1);
					percentage = percentage.substring(percentage, percentage.length - 1);
					kony.print("fnames------>" + fname + "bName--->" + bName + "relation--->" + relation + "percentage--->" + percentage);	
					
					if(gblSavingsCareFlow=="MyAccountFlow" || gblSavingsCareFlow=="AccountDetailsFlow"){
					kony.print("***VIJAY***")
						fromActId = gblAccountId;
						fromProdId = gblSelOpenActProdCode;
						fromFiident =gblSCFromFiident; // CHeck this one
						fromActName = gblSCFromActName;
						fromActType=gblSCFromActType;
						fromActNickName=gblOpenSavingsCareNickName;
						kony.print("GOWRI123 IN fromActNickName="+fromActNickName);
					}else{
						fromActName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenActName; 
						fromActId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblActNoval;
						fromActId = fromActId.toString().replace(/-/g, "");
						fromActNickName = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].lblCustName;
						fromFiident = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenFiident;
						fromActType = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenActType;
						fromProdId = frmMBSavingsCareAddBal["segNSSlider"]["selectedItems"][0].hiddenProductId;					
					}
					kony.print("GOWRI123 OUT fromActNickName="+fromActNickName);
					gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
					fromActId = fromActId.toString().replace(/-/g, "");
					gblFinActivityLogOpenAct["affiliatedAcctId"]=fromActId;
					gblFinActivityLogOpenAct["fromActId"] = fromActId;
					gblFinActivityLogOpenAct["fromActName"] = fromActName;
					gblFinActivityLogOpenAct["fromActType"] = fromActType;
					gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
					gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
					gblFinActivityLogOpenAct["toActType"] = "SDA";					
					gblFinActivityLogOpenAct["toActName"] = fromActName;
					gblFinActivityLogOpenAct["toFiident"] = "";
					gblFinActivityLogOpenAct["toActId"] = "";
					gblFinActivityLogOpenAct["dreamAmt"] = "";
					gblFinActivityLogOpenAct["dreamMonth"] = "";
					gblFinActivityLogOpenAct["fromProdId"] = "";
					gblFinActivityLogOpenAct["channelName"] = "MB";
					gblFinActivityLogOpenAct["tdInterestRate"] = ""; // TO- DO - check with Shireesh
					gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				    gblFinActivityLogOpenAct["acctType"] = "SDA";
				    gblFinActivityLogOpenAct["toActNickName"] = frmMBSavingsCareAddNickName.txtNickName.text;
				    //kony.print("GOWRI123 OUT toActNickName="+toActNickName);
					
            if(gblAuthAccessPin == true){
              validateOTPtextOpenAccountsJavaService(gblNum,"","","",gblFinActivityLogOpenAct,"","02","");
            }else{
               validateOTPtextOpenAccountsJavaService(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text,"","","",gblFinActivityLogOpenAct,"","02","");  
            }
			
		
		
		}else if(gblSelProduct == "ForUse"){
		
					fromActName = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActName; 
					fromActId = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblActNoval;
					fromActNickName = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].lblCustName;
					fromFiident = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenFiident;
					fromActType = frmOpnActSelAct["segNSSlider"]["selectedItems"][0].hiddenActType;
					transferAmount = gblFinActivityLogOpenAct["transferAmount"];
					gblFinActivityLogOpenAct["fromActIdFormatted"] = fromActId;
					fromActId = fromActId.toString().replace(/-/g, "");
					
					gblFinActivityLogOpenAct["fromActId"] = fromActId;
					gblFinActivityLogOpenAct["fromActName"] = fromActName;
					gblFinActivityLogOpenAct["fromActType"] = fromActType;
					gblFinActivityLogOpenAct["fromActNickName"] = fromActNickName;
					gblFinActivityLogOpenAct["fromFiident"] = fromFiident;
					gblFinActivityLogOpenAct["toActType"] = "SDA";
					gblFinActivityLogOpenAct["toActName"] = fromActName;
					gblFinActivityLogOpenAct["toFiident"] = "";
					gblFinActivityLogOpenAct["toActId"] = "";
					gblFinActivityLogOpenAct["dreamAmt"] = "";
					gblFinActivityLogOpenAct["dreamMonth"] = "";
					gblFinActivityLogOpenAct["fromProdId"] = "";
					gblFinActivityLogOpenAct["channelName"] = "MB";
					gblFinActivityLogOpenAct["tdInterestRate"] = frmOpenProdDetnTnC.lblOpenAccInt.text;
					gblFinActivityLogOpenAct["affiliatedAcctNickname"] = "";
				    gblFinActivityLogOpenAct["affiliatedAcctId"] = "";
              if(gblAuthAccessPin == true){
                 validateOTPtextOpenAccountsJavaService(gblNum,"","","","","","02","");
              }else{
                validateOTPtextOpenAccountsJavaService(popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text,"","","","","","02","");         
              }
		 
		}
	//}
}

function showTDToActs(){
	
	setDataforOpenToActs(frmOpenAccTermDeposit.segToTDAct);
 
	selActTDToIndex = [0, 0];
	TDToActLen = 0;
	frmOpenAccTermDeposit.segToTDAct.selectedIndex = selActTDToIndex;
	TDToActLen = frmOpenAccTermDeposit.segToTDAct.data.length - 1;
	
}

function showTDFromActs(){
	
	var ifAccounts = true;
    selActTDFrmIndex = [0, 0];
    TDFrmActLen = 0;

    ifAccounts = setDataforOpenFromActs(frmOpenAccTermDeposit.segSliderTDFrom);
    if (ifAccounts == false){
      return false;
    }		
    frmOpenAccTermDeposit.segSliderTDFrom.selectedIndex = selActTDFrmIndex;
    TDFrmActLen = frmOpenAccTermDeposit.segSliderTDFrom.data.length - 1;
	frmOpenAccTermDeposit.show();
}


function showOpenTDAct(){
	//
	frmOpenAccTermDeposit.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
	frmOpenAccTermDeposit.btnTDSelAccNxt.text =  kony.i18n.getLocalizedString("Next");
	if (gblisDisToActTD == "Y")
	{	
		frmOpenAccTermDeposit.hbxSliderToTD.setVisibility(true);
		frmOpenAccTermDeposit.segToTDAct.setVisibility(true);
		//
		showTDToActs();
	}else{
		
			frmOpenAccTermDeposit.segToTDAct.widgetDataMap = {
			lblACno: "lblACno",
			lblAcntType: "lblAcntType",
			img1: "img1",
			lblCustName: "lblCustName",
			lblBalance: "lblBalance",
			lblActNoval: "lblActNoval",
			lblDummy: "lblDummy",
			lblSliderAccN1: "lblSliderAccN1",
			lblSliderAccN2: "lblSliderAccN2",
			lblSliderBranch: "lblSliderBranch",
			lblSliderBranchVal: "lblSliderBranchVal",
			imghidden:"imghidden"
			}
		
			frmOpenAccTermDeposit.hbxSliderToTD.setVisibility(false);
			frmOpenAccTermDeposit.segToTDAct.setVisibility(false);
	}

	try {
		if(null != frmOpenActSelProd["segOpenActSelProd"]["selectedItems"]){
			frmOpenAccTermDeposit.imgOATDProd.src = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
		}else {
			//when user come from open account brief screen from campaign
			//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
			var prodD = gblProudctDetails[gblProdCode];
			if(undefined != prodD && null != prodD){
				frmOpenAccTermDeposit.imgOATDProd.src = prodD.imageProduct;
			}
		}
	}catch(e){
		//
	}
	
	if (kony.i18n.getCurrentLocale() == "en_US") {
	frmOpenAccTermDeposit.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
	}else{
	frmOpenAccTermDeposit.lblOATDProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
	}
	frmOpenAccTermDeposit.txtTDamount.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
	frmOpenAccTermDeposit.txtTDamount.skin = txtNormalBG;
	frmOpenAccTermDeposit.txtDSNickName.skin = txtNormalBG;
	
		if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != ""){
		frmOpenAccTermDeposit.txtDSNickName.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
		}else{
		frmOpenAccTermDeposit.txtDSNickName.maxTextLength = 20;
		}	
		
	frmOpenAccTermDeposit.txtDSNickName.text = "";
	showTDFromActs();
	

}

var selActTDFrmIndex = [0, 0];
var TDFrmActLen = 0;
var selActTDToIndex = [0, 0];
var TDToActLen = 0;








function onClickConnectTDFrmSeg(){
	selActTDFrmIndex = frmOpenAccTermDeposit.segSliderTDFrom.selectedIndex;
}

function onClickConnectTDToSeg(){
	selActTDToIndex = frmOpenAccTermDeposit.segToTDAct.selectedIndex;
}

function onClickMoreSavings(){
	
	var isValid1  = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName1,frmOpenAccountSavingCareMB.txtBefSecName1,frmOpenAccountSavingCareMB.txtBefBen1,frmOpenAccountSavingCareMB.lblBenRefVal1);
	if (!isValid1){	
		return false;
	}
	
	if(frmOpenAccountSavingCareMB.hboxBenificiaryTwo.isVisible){
	var isValid2  = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName2,frmOpenAccountSavingCareMB.txtBefSecName2,frmOpenAccountSavingCareMB.txtBefBen2,frmOpenAccountSavingCareMB.lblBenRefVal2);
		if (!isValid2){	
		return false;
		}
	}	

	if(frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible){
		var isValid3 = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName3,frmOpenAccountSavingCareMB.txtBefSecName3,frmOpenAccountSavingCareMB.txtBefBen3,frmOpenAccountSavingCareMB.lblBenRefVal3);
		if (!isValid3){	
		return false;
		}
	}
	if(frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible){
		 var isValid4 = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName4,frmOpenAccountSavingCareMB.txtBefSecName4,frmOpenAccountSavingCareMB.txtBefBen4,frmOpenAccountSavingCareMB.lblBenRefVal4);
		if (!isValid4){	
		return false;
		}
	}

	if(frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible){
		var isValid5 = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName5,frmOpenAccountSavingCareMB.txtBefSecName5,frmOpenAccountSavingCareMB.txtBefBen5,frmOpenAccountSavingCareMB.lblBenRefVal5);
		if (!isValid5){	
		return false;
		}
	}
	
	if (!(frmOpenAccountSavingCareMB.hboxBenificiaryTwo.isVisible)){
		frmOpenAccountSavingCareMB.hboxBenificiaryTwo.setVisibility(true);
		frmOpenAccountSavingCareMB.hbxOASCBenDel2.setVisibility(true);
		if((frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible) &&(frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible) && (frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible)){
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(false);
		}else
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
		
	}else if (!(frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible)){
		frmOpenAccountSavingCareMB.hboxBenificiaryThree.setVisibility(true);
		frmOpenAccountSavingCareMB.hbxOASCBenDel3.setVisibility(true);
		if((frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible) &&(frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible)){
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(false);
		}else
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
		
	}else if (!(frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible)){
		frmOpenAccountSavingCareMB.hboxBenificiaryFour.setVisibility(true);
		frmOpenAccountSavingCareMB.hbxOASCBenDel4.setVisibility(true);
		if((frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible) &&(frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible)){
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(false);
		}else
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
	}else if (!(frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible)){
		frmOpenAccountSavingCareMB.hboxBenificiaryFive.setVisibility(true);
		frmOpenAccountSavingCareMB.hbxOASCBenDel5.setVisibility(true);
		if((frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible) &&(frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible) ){
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(false);
		}else
			frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
	}

}

function onClickDelBefOpenSavingAct(delBenf){

		if(delBenf == "1"){
		frmOpenAccountSavingCareMB.txtBefFisrtName1.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName1.text = "";
		frmOpenAccountSavingCareMB.txtBefBen1.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal1.text = "Please select";
		}else if(delBenf == "2"){
		frmOpenAccountSavingCareMB.txtBefFisrtName2.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName2.text = "";
		frmOpenAccountSavingCareMB.txtBefBen2.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal2.text = "Please select";
		}else if(delBenf == "3"){
		frmOpenAccountSavingCareMB.hboxBenificiaryThree.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxOASCBenDel3.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
		frmOpenAccountSavingCareMB.txtBefFisrtName3.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName3.text = "";
		frmOpenAccountSavingCareMB.txtBefBen3.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal3.text = "Please select";
		}else if(delBenf == "4"){
		frmOpenAccountSavingCareMB.hboxBenificiaryFour.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxOASCBenDel4.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
		frmOpenAccountSavingCareMB.txtBefFisrtName4.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName4.text = "";
		frmOpenAccountSavingCareMB.txtBefBen4.text = "";
		//frmOpenAccountSavingCareMB.lblBenRefVal4.text = "Please select";
		}else if(delBenf == "5"){
		frmOpenAccountSavingCareMB.hboxBenificiaryFive.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxOASCBenDel5.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
		frmOpenAccountSavingCareMB.txtBefFisrtName5.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName5.text = "";
		frmOpenAccountSavingCareMB.txtBefBen5.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal5.text = "Please select";
		}

}


function calBenefitPercent(){
	var benPer1 = frmOpenAccountSavingCareMB.txtBefBen1.text;
	var benPer2 = frmOpenAccountSavingCareMB.txtBefBen2.text;
	var benPer3 = "0";
	var benPer4 = "0";
	var benPer5 = "0";
	var totBenPer;
			
		if (frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible){
		benPer3 = frmOpenAccountSavingCareMB.txtBefBen3.text;
		}
		
		if (frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible){
		benPer4 = frmOpenAccountSavingCareMB.txtBefBen4.text;
		}
		
		if (frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible){
		benPer5 = frmOpenAccountSavingCareMB.txtBefBen5.text;
		}
		
	 benPer1 = kony.os.toNumber(benPer1);
	 benPer2 = kony.os.toNumber(benPer2);
	 benPer3 = kony.os.toNumber(benPer3);
	 benPer4 = kony.os.toNumber(benPer4);
	 benPer5 = kony.os.toNumber(benPer5);
	 
	 totBenPer = benPer1 + benPer2 + benPer3 + benPer4 + benPer5;
	 return totBenPer;
	
}

function validateBenfFields(firsTxtName,scndTxtName,befTxtName,relTxt){
	
	if (firsTxtName.text == "" || firsTxtName.text == null){
	
	showAlert(kony.i18n.getLocalizedString("keyInptCompBen") , kony.i18n.getLocalizedString("info"));
	firsTxtName.skin = txtErrorBG;
	return false;
	}else if (scndTxtName.text == "" || scndTxtName.text == null){
	showAlert(kony.i18n.getLocalizedString("keyInptCompBen") , kony.i18n.getLocalizedString("info"));
	scndTxtName.skin = txtErrorBG;
	return false;
	}else if (befTxtName.text == "" || befTxtName.text == null){
	showAlert(kony.i18n.getLocalizedString("keyInptCompBen") , kony.i18n.getLocalizedString("info"));
	befTxtName.skin = txtErrorBG;
	return false;
	}else if (relTxt.text == "Please select"){
	showAlert(kony.i18n.getLocalizedString("keyInptCompBen") , kony.i18n.getLocalizedString("info"));
	return false;
	}else if (!(benNameAlpNumValidation(firsTxtName.text))){
	
	showAlert(kony.i18n.getLocalizedString("keyBenFirstNameAlphaNum"), kony.i18n.getLocalizedString("info"));
	firsTxtName.skin = txtErrorBG;
	return false;
	}else if (!(benNameAlpNumValidation(scndTxtName.text))){
	showAlert(kony.i18n.getLocalizedString("keyBenSecondNameAlphNum") , kony.i18n.getLocalizedString("info"));
	scndTxtName.skin = txtErrorBG;
	return false;
	}else if (((kony.os.toNumber(befTxtName.text)) <= 0) && ((kony.os.toNumber(befTxtName.text)) >= 100)){
	showAlert(kony.i18n.getLocalizedString("keyBenpercentage") , kony.i18n.getLocalizedString("info"));
	befTxtName.skin = txtErrorBG;
	return false;
	}else
	return true;
	
}


function benNameAlpNumValidation(text){
	var pat1 = /[a-zA-Z0-9- ]+$/;
	//var pat2 = /[0-9]/g
	var pat2 = /[\u0E00-\u0E7F]+/;
	var isAlpha = pat1.test(text);
	//var isNum = pat2.test(text);
	var containsThai = pat2.test(text);
	if (containsThai) {
		
	} else {
	
		if (isAlpha == false ) {
			return false;
		}
	}
	return true;
}

function onClickSavingCareLanding(){
	if ((frmOpenAccountSavingCareMB.txtNickName.text == null) || (frmOpenAccountSavingCareMB.txtNickName.text == "")) {
	showAlert(kony.i18n.getLocalizedString("keyActNickNameEmpty"), kony.i18n.getLocalizedString("info"));
	frmOpenAccountSavingCareMB.txtNickName.skin = txtErrorBG;
	return false;
	}
	var nickName = NickNameValid(frmOpenAccountSavingCareMB.txtNickName.text);
	
	if (nickName == false) {
		showAlert(kony.i18n.getLocalizedString("keyOpenAccntNickname"), kony.i18n.getLocalizedString("info"));
		frmOpenAccountSavingCareMB.txtNickName.skin = txtErrorBG;
		return false;
	}
			
	var isValid = true;
	gblOpenActBenList = [];
		
		//gblFinActivityLogOpenAct = [];	
		frmOpenAccountSavingCareMB.txtAmount.text = autoFormatAmount(frmOpenAccountSavingCareMB.txtAmount.text);
		enteredAmount=frmOpenAccountSavingCareMB.txtAmount.text;
		enteredAmount = enteredAmount.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace(/,/g, "");
		enteredAmount = enteredAmount.trim();
		gblFinActivityLogOpenAct["transferAmount"] = enteredAmount;
		var isCrtFormt=amountValidationMBOpenAct(enteredAmount);		
				
				if(!isCrtFormt){
			   	showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
			   	frmOpenAccountSavingCareMB.txtAmount.text="";
			   	frmOpenAccountSavingCareMB.txtAmount.skin = txtErrorBG;
			   	return false
				}
				
				if ((kony.os.toNumber(enteredAmount)) < (kony.os.toNumber(gblMinOpenAmt))){
					showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
			   		frmOpenAccountSavingCareMB.txtAmount.skin = txtErrorBG;
			   		return false				
				}
				
				if (gblMaxOpenAmt != "No Limit")
				{
					if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(gblMaxOpenAmt))){
						showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
				   		frmOpenAccountSavingCareMB.txtAmount.skin = txtErrorBG;
				   		return false				
					}
				}	
			 
                if ((kony.os.toNumber(enteredAmount)) > (kony.os.toNumber(frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].hiddenActBal))) {
                  //	showAlert("Please enter amount less than the available balance.", kony.i18n.getLocalizedString("info"));
                  showAlert(kony.i18n.getLocalizedString("keyAmtLessThanAvaBal"), kony.i18n.getLocalizedString("info"));
                  frmOpenAccountSavingCareMB.txtAmount.skin = txtErrorBG;
                  return false
                }
		
			
			var nickNameUniq = openActNickNameUniq(frmOpenAccountSavingCareMB.txtNickName.text);
			 if(nickNameUniq == false){
			 showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
			 frmOpenAccountSavingCareMB.txtNickName.skin = txtErrorBG;
			 return false
			 }
						
	setSACnfWigFalse();
	frmOpenActSavingCareCnfNAck.lblOASCIntRateVal.text = frmOpenProdDetnTnC.lblOpenAccInt.text + "%";
	isValid = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName1,frmOpenAccountSavingCareMB.txtBefSecName1,frmOpenAccountSavingCareMB.txtBefBen1,frmOpenAccountSavingCareMB.lblBenRefVal1);
	if (!isValid){
		return false
	}else{
			frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm1.text = frmOpenAccountSavingCareMB.txtBefFisrtName1.text  + " " + frmOpenAccountSavingCareMB.txtBefSecName1.text;
			frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal1.text = frmOpenAccountSavingCareMB.lblBenRefVal1.text;
			frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal1.text = frmOpenAccountSavingCareMB.txtBefBen1.text + "%";
			gblOpenActBenList.push({
							"careBeneficiaryName": frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm1.text,
							"careBeneficiaryRelation":frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal1.text,
							"careBeneficiaryPct":frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal1.text
							})
			}

	
	if(frmOpenAccountSavingCareMB.hboxBenificiaryTwo.isVisible){
		
		if(!((frmOpenAccountSavingCareMB.txtBefFisrtName2.text == "" || frmOpenAccountSavingCareMB.txtBefFisrtName2.text == null) && (frmOpenAccountSavingCareMB.txtBefSecName2.text == "" || frmOpenAccountSavingCareMB.txtBefSecName2.text == null) && (frmOpenAccountSavingCareMB.txtBefBen2.text == "" || frmOpenAccountSavingCareMB.txtBefBen2.text == null) && frmOpenAccountSavingCareMB.lblBenRefVal2.text == "Please select")){
			isValid = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName2,frmOpenAccountSavingCareMB.txtBefSecName2,frmOpenAccountSavingCareMB.txtBefBen2,frmOpenAccountSavingCareMB.lblBenRefVal2);
			if (!isValid){
				return false
			}else{
				frmOpenActSavingCareCnfNAck.lineOASC1.setVisibility(true);
				frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm2.setVisibility(true);
				frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm2.setVisibility(true);
				frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm2.setVisibility(true);
				frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm2.text = frmOpenAccountSavingCareMB.txtBefFisrtName2.text  + " " + frmOpenAccountSavingCareMB.txtBefSecName2.text;
				frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal2.text = frmOpenAccountSavingCareMB.lblBenRefVal2.text;
				frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal2.text = frmOpenAccountSavingCareMB.txtBefBen2.text + "%";
				gblOpenActBenList.push({
							"careBeneficiaryName": frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm2.text,
							"careBeneficiaryRelation":frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal2.text,
							"careBeneficiaryPct":frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal2.text
							})
				}
		}	
	}
	
	
	if(frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible){
		
		isValid = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName3,frmOpenAccountSavingCareMB.txtBefSecName3,frmOpenAccountSavingCareMB.txtBefBen3,frmOpenAccountSavingCareMB.lblBenRefVal3);
		if (!isValid){
			return false
		}else{
			frmOpenActSavingCareCnfNAck.lineOASC2.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm3.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm3.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm3.setVisibility(true);
			frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm3.text = frmOpenAccountSavingCareMB.txtBefFisrtName3.text + " " + frmOpenAccountSavingCareMB.txtBefSecName3.text;
			frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal3.text = frmOpenAccountSavingCareMB.lblBenRefVal4.text;
			frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal3.text = frmOpenAccountSavingCareMB.txtBefBen3.text + "%";
			gblOpenActBenList.push({
							"careBeneficiaryName": frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm3.text,
							"careBeneficiaryRelation":frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal3.text,
							"careBeneficiaryPct":frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal3.text
							})
			}
	}
	
	
	if(frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible){
		isValid = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName4,frmOpenAccountSavingCareMB.txtBefSecName4,frmOpenAccountSavingCareMB.txtBefBen4,frmOpenAccountSavingCareMB.lblBenRefVal4);
		if (!isValid){
			return false;
		}else{
			frmOpenActSavingCareCnfNAck.lineOASC3.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm4.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm4.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm4.setVisibility(true);
			frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm4.text = frmOpenAccountSavingCareMB.txtBefFisrtName4.text + " " + frmOpenAccountSavingCareMB.txtBefSecName4.text;
			frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal4.text = frmOpenAccountSavingCareMB.lblBenRefVal4.text;
			frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal4.text = frmOpenAccountSavingCareMB.txtBefBen4.text + "%";
			gblOpenActBenList.push({
							"careBeneficiaryName": frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm4.text,
							"careBeneficiaryRelation":frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal4.text,
							"careBeneficiaryPct":frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal4.text
							})
			}
	}
	
	
	if(frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible){
		 isValid = validateBenfFields(frmOpenAccountSavingCareMB.txtBefFisrtName5,frmOpenAccountSavingCareMB.txtBefSecName5,frmOpenAccountSavingCareMB.txtBefBen5,frmOpenAccountSavingCareMB.lblBenRefVal5);
		 if (!isValid){
		 	return false;
		 }else{
			frmOpenActSavingCareCnfNAck.lineOASC4.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm5.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm5.setVisibility(true);
			frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm5.setVisibility(true);
			frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm5.text = frmOpenAccountSavingCareMB.txtBefFisrtName5.text + " " + frmOpenAccountSavingCareMB.txtBefSecName5.text;
			frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal5.text = frmOpenAccountSavingCareMB.lblBenRefVal5.text;
			frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text = frmOpenAccountSavingCareMB.txtBefBen5.text + "%";
			gblOpenActBenList.push({
							"careBeneficiaryName": frmOpenActSavingCareCnfNAck.lblBefNameValCnfrm5.text,
							"careBeneficiaryRelation":frmOpenActSavingCareCnfNAck.lblBefRsCnfrmVal5.text,
							"careBeneficiaryPct":frmOpenActSavingCareCnfNAck.lblBefPerCnfrmVal5.text
							})
			}
	}

	var nameUniq = checkNameUniquness();
	
	
	if(nameUniq == false){
	showAlert(kony.i18n.getLocalizedString("keyBenDiffNames"), kony.i18n.getLocalizedString("info"));
	frmOpenAccountSavingCareMB.txtNickName.skin = txtErrorBG;
	return false;
	}
	
	var totCalPer = calBenefitPercent();
	
	if (totCalPer != 100){
	showAlert(kony.i18n.getLocalizedString("keyBenTotalPer"), kony.i18n.getLocalizedString("info"));
	return false;
	}
	frmOpenAccountSavingCareMB.txtAmount.skin = txtNormalBG;
	frmOpenAccountSavingCareMB.txtNickName.skin = txtNormalBG;
	setSavingCareLandingTextBoxSkin();
  
    frmOpenActSavingCareCnfNAck.lblOASCBranchVal.text = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].lblRemainFeeValue;
    gblFinActivityLogOpenAct["toBranchName"] = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].lblRemainFeeValue;
    frmOpenActSavingCareCnfNAck.lblOASCActNameVal.text = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].hiddenActName;
    gblFinActivityLogOpenAct["branchEN"] = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].hiddenBranchNameEN;
    gblFinActivityLogOpenAct["branchTH"] = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].hiddenBranchNameTH;
    gblFinActivityLogOpenAct["nickTH"] = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].hiddenproductThaiAssign;
    gblFinActivityLogOpenAct["nickEN"] = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].hiddenprodENGAssign;
    gblFinActivityLogOpenAct["actTypeFromOpen"] = frmOpenAccountSavingCareMB["segSlider"]["selectedItems"][0].hiddenActType;
	
  	frmOpenActSavingCareCnfNAck.vbxHideSavingCare.setVisibility(false);
	if (kony.i18n.getCurrentLocale() == "en_US") {
		frmOpenActSavingCareCnfNAck.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":"
	} else {
		frmOpenActSavingCareCnfNAck.lblOASCIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":"
	}
	frmOpenActSavingCareCnfNAck.hboxaddfblist.setVisibility(true);
	frmOpenActSavingCareCnfNAck.hbxCannotTrans.setVisibility(false);
    frmOpenActSavingCareCnfNAck.lblNotAvailable.setVisibility(false);
    frmOpenActSavingCareCnfNAck.lblGreetingSavingCare.setVisibility(false);
    preShowiSavingCareConfirm();
	invokePartyInquiryOpenAct();
}

function setSavingCareLandingTextBoxSkin(){
	frmOpenAccountSavingCareMB.txtBefFisrtName1.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefSecName1.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefBen1.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefFisrtName2.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefSecName2.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefBen2.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefFisrtName3.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefSecName3.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefBen3.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefFisrtName4.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefSecName4.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefBen4.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefFisrtName5.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefSecName5.skin = txtGrey;
	frmOpenAccountSavingCareMB.txtBefBen5.skin = txtGrey;

}


function onClickSelRelation(){
	
	popSelDate.segDreamSaveDate.removeAll();
	popSelDate.segDreamSaveDate.containerWeight = 100;
	popSelDate.segDreamSaveDate.widgetDataMap = {
		lblDreamSaveDate: "lblDreamSaveDate",
		imgDreamSaveDate: "imgDreamSaveDate"
	}
	popSelDate.segDreamSaveDate.data = [{
		lblDreamSaveDate: kony.i18n.getLocalizedString("keyFather"),
		imgDreamSaveDate: "bg_arrow_right_grayb.png",
		hiddenRel: "F"
		}, {
		lblDreamSaveDate: kony.i18n.getLocalizedString("keyMother"),
		imgDreamSaveDate: "bg_arrow_right_grayb.png",
		hiddenRel: "M"
		}, {
		lblDreamSaveDate: kony.i18n.getLocalizedString("keySpouseReg"),
		imgDreamSaveDate: "bg_arrow_right_grayb.png",
		hiddenRel: "S"
	}, {
		lblDreamSaveDate: kony.i18n.getLocalizedString("keySpouseNL"),
		imgDreamSaveDate: "bg_arrow_right_grayb.png",
		hiddenRel: "L"
		}, {
		lblDreamSaveDate: kony.i18n.getLocalizedString("keyChild"),
		imgDreamSaveDate: "bg_arrow_right_grayb.png",
		hiddenRel: "C"
	}, {
		lblDreamSaveDate: kony.i18n.getLocalizedString("keyRelative"),
		imgDreamSaveDate: "bg_arrow_right_grayb.png",
		hiddenRel: "R"
		}, {
		lblDreamSaveDate: kony.i18n.getLocalizedString("keyOtherRelation"),
		imgDreamSaveDate: "bg_arrow_right_grayb.png",
		hiddenRel: "O"
	}]
	
	showPopSelDate(onclickSelRelationOnPop);

}

function showPopSelDate(onseleSeg){
	popSelDate.segDreamSaveDate.onRowClick = onseleSeg;
	popSelDate.show();
}

function onclickSelRelationOnPop(){
	if(gblSelRel == "1")
	{
	frmOpenAccountSavingCareMB.lblBenRefVal1.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
	arrbenf1=popSelDate.segDreamSaveDate.selectedItems[0].hiddenRel;
	}else if(gblSelRel == "2") 
	{frmOpenAccountSavingCareMB.lblBenRefVal2.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
	arrbenf2=popSelDate.segDreamSaveDate.selectedItems[0].hiddenRel;
	}else if(gblSelRel == "3") 
	{frmOpenAccountSavingCareMB.lblBenRefVal3.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
	arrbenf3=popSelDate.segDreamSaveDate.selectedItems[0].hiddenRel;
	}else if(gblSelRel == "4") 
	{frmOpenAccountSavingCareMB.lblBenRefVal4.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
	arrbenf4=popSelDate.segDreamSaveDate.selectedItems[0].hiddenRel;
	}else if(gblSelRel == "5") 
	{frmOpenAccountSavingCareMB.lblBenRefVal5.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
	arrbenf5=popSelDate.segDreamSaveDate.selectedItems[0].hiddenRel;
	}popSelDate.dismiss();
}


function onclickSelDateOnPop(){
	frmOpnActSelAct.btnDreamSavecombo.text = popSelDate.segDreamSaveDate.selectedItems[0].lblDreamSaveDate;
	popSelDate.dismiss();
}


function checkNameUniquness(){
	var usrName1 = frmOpenAccountSavingCareMB.txtBefFisrtName1.text +frmOpenAccountSavingCareMB.txtBefSecName1.text;
	var usrName2 = "usrName2";
	var usrName3 = "usrName3";
	var usrName4 = "usrName4";
	var usrName5 = "usrName5";
		
		if (frmOpenAccountSavingCareMB.hboxBenificiaryTwo.isVisible){
		usrName3 = frmOpenAccountSavingCareMB.txtBefFisrtName2.text + frmOpenAccountSavingCareMB.txtBefSecName2.text;
		}
		
		if (frmOpenAccountSavingCareMB.hboxBenificiaryThree.isVisible){
		usrName3 = frmOpenAccountSavingCareMB.txtBefFisrtName3.text + frmOpenAccountSavingCareMB.txtBefSecName3.text;
		}
		
		if (frmOpenAccountSavingCareMB.hboxBenificiaryFour.isVisible){
		usrName4 = frmOpenAccountSavingCareMB.txtBefFisrtName4.text + frmOpenAccountSavingCareMB.txtBefSecName4.text;
		}
		
		if (frmOpenAccountSavingCareMB.hboxBenificiaryFive.isVisible){
		usrName5 = frmOpenAccountSavingCareMB.txtBefFisrtName5.text + frmOpenAccountSavingCareMB.txtBefSecName2.text;
		}
		
		var uniqArr = [usrName1,usrName2,usrName3,usrName4,usrName5]
		
		for(i=0;i<5;i++){
			
			for(j=0;j<5;j++){
			 if (i != j){
				 
				 if (uniqArr[i] == uniqArr[j]){
				 return false;
				 }
			 }
			}
		}
	 
}


function setSavingCareData(){
	var ifAccounts = true;
	dsSelActSelIndex = [0, 0];
	dsSelActLength = 0;
	
    ifAccounts = setDataforOpenFromActs(frmOpenAccountSavingCareMB.segSlider);
    if (ifAccounts == false){
      return false;
    }
	
	frmOpenAccountSavingCareMB.segSlider.selectedIndex = dsSelActSelIndex;
	dsSelActLength = frmOpenAccountSavingCareMB.segSlider.data.length - 1;
	frmOpenAccountSavingCareMB.show();
}




function onClickPreShowSCAck(){
  frmOpenActSavingCareCnfNAck.btnRight.setVisibility(true);
  frmOpenActSavingCareCnfNAck.btnRight.skin = btnShare;
  frmOpenActSavingCareCnfNAck.imgOASCComplete.setVisibility(true);

  frmOpenActSavingCareCnfNAck.hbxOASCAck.isVisible = true;
  frmOpenActSavingCareCnfNAck.hbxOAScConfirmation.isVisible = false;
 
  frmOpenActSavingCareCnfNAck.hbxNofiSC.setVisibility(true);
  frmOpenActSavingCareCnfNAck.hbxAdv.setVisibility(true);
  frmOpenActSavingCareCnfNAck.vbxHideSavingCare.setVisibility(true);
  frmOpenActSavingCareCnfNAck.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
  frmOpenActSavingCareCnfNAck.hbxOASCActNumVal.setVisibility(true);
  frmOpenActSavingCareCnfNAck.hbxOASCActName.skin = hboxLightGrey;
  frmOpenActSavingCareCnfNAck.hbxOASCBranch.skin = hboxWhite;
  frmOpenActSavingCareCnfNAck.hbxOASCTranRefNo.skin = hboxWhite;
  frmOpenActSavingCareCnfNAck.hbxOASCOpenAct.skin = hboxLightGrey;

	if (grtFormattedAmount(gblFinActivityLogOpenAct["transferAmount"]) >0){
		frmOpenActSavingCareCnfNAck.vbxHideSavingCare.setVisibility(true);
		frmOpenActSavingCareCnfNAck.hbxOASCIntRate.skin = hboxWhite;
		frmOpenActSavingCareCnfNAck.hbxOASCOpenDate.skin = hboxLightGrey;
		frmOpenActSavingCareCnfNAck.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("keyBalanceAfterTrans");
		}else{
	    frmOpenActSavingCareCnfNAck.vbxHideSavingCare.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxOASCIntRate.skin = hboxLightGrey;
		frmOpenActSavingCareCnfNAck.hbxOASCOpenDate.skin = hboxWhite;
		}

//#ifdef android
frmOpenActSavingCareCnfNAck.scrollboxMain.containerWeight = 185 ;
//#endif
//Code change for IOS9
campaginService("imgOpnActSavingCareAdd","frmOpenActSavingCareCnfNAck","M");
frmOpenActSavingCareCnfNAck.show();
}

function setSACnfWigFalse(){
		frmOpenActSavingCareCnfNAck.lineOASC1.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm2.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm2.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm2.setVisibility(false);
		frmOpenActSavingCareCnfNAck.lineOASC2.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm3.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm3.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm3.setVisibility(false);
		frmOpenActSavingCareCnfNAck.lineOASC3.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm4.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm4.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm4.setVisibility(false);
		frmOpenActSavingCareCnfNAck.lineOASC4.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefNameCnfrm5.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefRsCnfrm5.setVisibility(false);
		frmOpenActSavingCareCnfNAck.hbxBefPerCnfrm5.setVisibility(false);
}


function setNormalSavingsData() {
	// added below if else condition to fix MIB-1601 
	if(undefined != frmOpenActSelProd["segOpenActSelProd"]["selectedItems"] && null != frmOpenActSelProd["segOpenActSelProd"]["selectedItems"]){
		frmOpnActSelAct.imgNSProdName.src = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].imgOpnActSelProd;
	}else {
		//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
		var prodD = gblProudctDetails[gblProdCode];
		if(undefined != prodD && null != prodD){
			kony.print("product Details : "+prodD);
			frmOpnActSelAct.imgNSProdName.src = prodD.imageProduct;
			}
	}
	
	
	if (kony.i18n.getCurrentLocale() == "en_US") {
	frmOpnActSelAct.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameEN"];
	}else{
	frmOpnActSelAct.lblNSProdName.text = gblFinActivityLogOpenAct["prodNameTH"];
	}

	 dsSelActSelIndex = [0, 0];
	 dsSelActLength = 0;
	 var ifAccounts = true;
	
    ifAccounts = setDataforOpenFromActs(frmOpnActSelAct.segNSSlider);
    if (ifAccounts == false){
      return false;
    }

    frmOpnActSelAct.segNSSlider.selectedIndex = dsSelActSelIndex;
    dsSelActLength = frmOpnActSelAct.segNSSlider.data.length - 1;
	
	frmOpnActSelAct.hbxSelActAmt.setVisibility(true);
		if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != ""){
		frmOpnActSelAct.txtOpenActNicNam.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
		}else{
		frmOpnActSelAct.txtOpenActNicNam.maxTextLength = 20;
		}
	frmOpnActSelAct.txtOpenActNicNam.text = "";
	frmOpnActSelAct.txtOpenActNicNam.skin = txtNormalBG;
	frmOpnActSelAct.txtOpenActSelAmt.skin = txtNormalBG;
	frmOpnActSelAct.txtOpenActSelAmt.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
	frmOpnActSelAct.hbxSelActSelNickName.setVisibility(true);
	frmOpnActSelAct.txtDSAmountSel.setVisibility(false);
	frmOpnActSelAct.line1.setVisibility(true);
	frmOpnActSelAct.line2.setVisibility(true);
	frmOpnActSelAct.line3.setVisibility(false);
	frmOpnActSelAct.line4.setVisibility(false);
	frmOpnActSelAct.btnDreamSavecombo.setVisibility(false);
	frmOpnActSelAct.show();
}	

function onClickAgreeOPenAcnt(){
	kony.print("inside onClickAgreeOPenAcnt gblSelProduct---->" + gblSelProduct);
	if(frmOpenProdDetnTnC.lblOpenAccInt.text == "0.00%")
	{
		showAlert(kony.i18n.getLocalizedString("keyOpenActGenErr"), kony.i18n.getLocalizedString("info"));
		return false;
	}

	frmOpenProdDetnTnC.lblOpenAccInt.text = frmOpenProdDetnTnC.lblOpenAccInt.text.replace("%", "");
	if(gblSelProduct == "ForTerm"){
		showOpenTDAct();
	}else if(gblSelProduct == "TMBDreamSavings"){
		frmOpenAcDreamSaving.txtODTargetAmt.text = "0";
		frmOpenAcDreamSaving.txtODMnthSavAmt.text = "";
		frmOpenAcDreamSaving.txtODMyDream.text = "";
		frmOpenAcDreamSaving.txtODNickNameVal.text = "";
		onClickAgreeOpenDSActs();
	}else if(gblSelProduct == "ForUse"){
	
		if(gblFinActivityLogOpenAct["issueCardOnline"] != undefined && gblFinActivityLogOpenAct["issueCardOnline"] == "1")
			showAddressForPackageProductMB();
		else
			gotoForUseOpenAccountForm();	
		
	}else if(gblSelProduct == "TMBSavingcare"){
		if (kony.i18n.getCurrentLocale() == "en_US") {
				frmOpenAccountSavingCareMB.lblSavinCareSelProdTxt.text  = gblFinActivityLogOpenAct["prodNameEN"];
			} else {
				frmOpenAccountSavingCareMB.lblSavinCareSelProdTxt.text  = gblFinActivityLogOpenAct["prodNameTH"];
			}
		
		frmOpenAccountSavingCareMB.hboxBenificiaryOne.setVisibility(true);
		frmOpenAccountSavingCareMB.hboxBenificiaryTwo.setVisibility(false);
		frmOpenAccountSavingCareMB.hboxBenificiaryThree.setVisibility(false);
		frmOpenAccountSavingCareMB.hboxBenificiaryFour.setVisibility(false);
		frmOpenAccountSavingCareMB.hboxBenificiaryFive.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxLink3.setVisibility(true);
		frmOpenAccountSavingCareMB.hbxOASCBenDel2.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxOASCBenDel3.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxOASCBenDel4.setVisibility(false);
		frmOpenAccountSavingCareMB.hbxOASCBenDel5.setVisibility(false);
		frmOpenAccountSavingCareMB.txtAmount.text = commaFormattedOpenAct(gblMinOpenAmt) + kony.i18n.getLocalizedString("currencyThaiBaht");
		if (GLOBAL_NICKNAME_LENGTH != null || GLOBAL_NICKNAME_LENGTH != ""){
			frmOpenAccountSavingCareMB.txtNickName.maxTextLength = kony.os.toNumber(GLOBAL_NICKNAME_LENGTH);
		}else{
			frmOpenAccountSavingCareMB.txtNickName.maxTextLength = 20;
		}		
		frmOpenAccountSavingCareMB.txtNickName.text = "";
		frmOpenAccountSavingCareMB.txtBefFisrtName1.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName1.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal1.text ="Please select"
		frmOpenAccountSavingCareMB.txtBefBen1.text = "";
		frmOpenAccountSavingCareMB.txtBefFisrtName2.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName2.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal2.text ="Please select"
		frmOpenAccountSavingCareMB.txtBefBen2.text = "";
		frmOpenAccountSavingCareMB.txtBefFisrtName3.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName3.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal3.text ="Please select"
		frmOpenAccountSavingCareMB.txtBefBen3.text = "";
		frmOpenAccountSavingCareMB.txtBefFisrtName4.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName4.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal4.text ="Please select"
		frmOpenAccountSavingCareMB.txtBefBen4.text = "";
		frmOpenAccountSavingCareMB.txtBefFisrtName5.text = "";
		frmOpenAccountSavingCareMB.txtBefSecName5.text = "";
	//	frmOpenAccountSavingCareMB.lblBenRefVal5.text ="Please select"
		frmOpenAccountSavingCareMB.txtBefBen5.text = "";
		frmOpenAccountSavingCareMB.txtAmount.skin = txtNormalBG;
		frmOpenAccountSavingCareMB.txtNickName.skin = txtNormalBG;
		setSavingCareLandingTextBoxSkin();
		preShowISavingCare();
		setSavingCareData();
	}
	var inputParam = {};
	invokeServiceSecureAsync("MyProfileViewCompositeServiceKYCOpenAcc", inputParam, kycCallBack);
	
}

function kycCallBack()
{
}
function enableAckList(frmName){
	var btnskin = "btnShare";
    var btnFocusSkin = "btnShareFoc";
    if (frmName.hboxaddfblist.isVisible) {
         frmName.hboxaddfblist.isVisible = true;
        frmName.btnRight.skin = btnskin;
        //frmName.btnRight.focusSkin = btnskin;
		frmName.imgHeaderMiddle.src = "arrowtop.png";
		frmName.imgHeaderRight.src = "empty.png";
	}
    else {
        frmName.hboxaddfblist.isVisible = true;
        frmName.btnRight.skin = btnFocusSkin;
		frmName.imgHeaderMiddle.src = "empty.png";
	    frmName.imgHeaderRight.src = "arrowtop.png";
	}
}

function checkTranPwdLocked(){
	kony.print("inside checkTranPwdLocked");
	var inputParam ={
		rqUUId: "",
		channelName: "MB-INQ",
		openActFlag: "true"
	}
	showLoadingScreen();
	invokeServiceSecureAsync("crmProfileInq", inputParam, callBackUserLockTransStatus)
}


function callBackUserLockTransStatus(status, callBackResponse){
	kony.print("inside callBackUserLockTransStatus" + JSON.stringify(callBackResponse));
	if(status == 400){
		if(callBackResponse["opstatus"] == 0)
		{
			
		 	var MBUserStatus = callBackResponse["mbFlowStatusIdRs"];
		 	kony.print("inside MBUserStatus---->" + MBUserStatus);
            //var SPAuserStatus = callBackResponse["ibUserStatusIdTr"];
            var key = kony.i18n.getLocalizedString("keyBusinessHours");
            var res = key.replace("<time>",callBackResponse["openActStartTime"] + "-" + callBackResponse["openActEndTime"]);
            
            
          if (MBUserStatus == "03") {
            gblUserLockStatusMB = "03";
            dismissLoadingScreen();
            alertUserStatusLocked();
          }
          else {
            gblUserLockStatusMB = MBUserStatus;
            gblEmailId = callBackResponse["emailAddr"];

            if(callBackResponse["openActBusinessHrsFlag"] == "true") {
              var ifCASAActs = checkTermDepositCount();
              if(ifCASAActs == 0){
                dismissLoadingScreen();
                showAlertWithCallBack(kony.i18n.getLocalizedString("msg_noActiveAct"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
                return false;
              }else{
                //dismissLoadingScreen();
                GBL_Fatca_Flow = "OAccounts";
                gblFATCAUpdateError = false;
                //callBackResponse["FatcaFlag"] = "0"	//Comment this line when service response comes
                if(callBackResponse["FatcaFlag"] == "0") {
                  dismissLoadingScreen();
                  gblFATCAUpdateFlag = "0";
                  showFACTAInfo();
                }
                else if(callBackResponse["FatcaFlag"] == "8" || callBackResponse["FatcaFlag"] == "9"
                        || callBackResponse["FatcaFlag"] == "P" || callBackResponse["FatcaFlag"] == "R" || callBackResponse["FatcaFlag"] == "X") {
                  dismissLoadingScreen();
                  gblFATCAUpdateFlag = "Z";
                  setFATCAMBGoToBranchInfoMessage();
                }
                else /*if(resulttable["FATCAFlag"] == "N" || resulttable["FATCAFlag"] == "I" || resulttable["FATCAFlag"] == "U") */{
                  ivokeCustActInqForOpenAct();
                }
              }						
            }else{
              dismissLoadingScreen();
              showAlert(res,kony.i18n.getLocalizedString("info"));
              return false;
            }

          }
		}
		else
		{
		 	gblUserLockStatusMB = "03";
		 	gblIBFlowStatus="04";
		 	dismissLoadingScreen();
		 	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
		} 
	}
	
}



function showTDConfirmation(){
	var textformat=NickNameValid(frmOpenAccTermDeposit.txtDSNickName.text);
	if(textformat== false) {
	showAlert(kony.i18n.getLocalizedString("keyOpenAccntNickname"), kony.i18n.getLocalizedString("info"));
	frmOpenAccTermDeposit.txtDSNickName.skin = txtErrorBG;
	return false;
	}
	
	if (frmOpenAccTermDeposit.txtTDamount.text == "" ||frmOpenAccTermDeposit.txtTDamount.text == null){
	showAlert(kony.i18n.getLocalizedString("keyPleaseEnterAmount"), kony.i18n.getLocalizedString("info"));
	frmOpenAccTermDeposit.txtTDamount.skin = txtErrorBG;
	return false
	}
	frmOpenAccTermDeposit.txtTDamount.text = autoFormatAmount(frmOpenAccTermDeposit.txtTDamount.text)
	var enteredTermDeposit = frmOpenAccTermDeposit.txtTDamount.text;
	enteredTermDeposit = enteredTermDeposit.replace(kony.i18n.getLocalizedString("currencyThaiBaht"),"").replace("/", "").replace(kony.i18n.getLocalizedString("keyCalendarMonth"), "").replace(/,/g, "");
	enteredTermDeposit = enteredTermDeposit.trim();
	gblFinActivityLogOpenAct["transferAmount"] = enteredTermDeposit;
	
	if (!(amountValidationMBOpenAct(gblFinActivityLogOpenAct["transferAmount"]))){
	showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
	frmOpenAccTermDeposit.txtTDamount.skin = txtErrorBG;
	return false
	}
	
	if ((kony.os.toNumber(gblFinActivityLogOpenAct["transferAmount"])) < (kony.os.toNumber(gblMinOpenAmt))){
	showAlert(kony.i18n.getLocalizedString("keyMinLimitAmt"), kony.i18n.getLocalizedString("info"));
	frmOpenAccTermDeposit.txtTDamount.skin = txtErrorBG;
	return false				
	}
				
	if (gblMaxOpenAmt != "No Limit")
	{
		if ((kony.os.toNumber(gblFinActivityLogOpenAct["transferAmount"])) > (kony.os.toNumber(gblMaxOpenAmt))){
		showAlert(kony.i18n.getLocalizedString("keyMaxLimitAmt"), kony.i18n.getLocalizedString("info"));
		frmOpenAccTermDeposit.txtTDamount.skin = txtErrorBG;
		return false				
		}
	}
	
    if ((kony.os.toNumber(gblFinActivityLogOpenAct["transferAmount"])) > (kony.os.toNumber(frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenActBal))){
      showAlert(kony.i18n.getLocalizedString("keyAmtLessThanAvaBal"), kony.i18n.getLocalizedString("info"));
      frmOpenAccTermDeposit.txtTDamount.skin = txtErrorBG;
      return false				
    }
	
	var nickName = openActNickNameUniq(frmOpenAccTermDeposit.txtDSNickName.text);
	 if(nickName == false){
	 showAlert(kony.i18n.getLocalizedString("keyuniqueNickName"), kony.i18n.getLocalizedString("info"));
	 frmOpenAccTermDeposit.txtDSNickName.skin = txtErrorBG;
	 return false
	 }
	
	frmOpenActTDConfirm.lblOATDIntRateVal.text = frmOpenProdDetnTnC.lblOpenAccInt.text + "%";
	frmOpenActTDConfirm.imgTDCnfmTitle.src = frmOpenAccTermDeposit.imgOATDProd.src;
	frmOpenActTDConfirm.lblTDCnfmTitle.text = frmOpenAccTermDeposit.lblOATDProdName.text;
	frmOpenActTDConfirm.lblOATDNickNameVal.text = frmOpenAccTermDeposit.txtDSNickName.text;
	frmOpenActTDConfirm.lblOATDAmtVal.text = frmOpenAccTermDeposit.txtTDamount.text;
	//gblFinActivityLogOpenAct["transferAmount"] = frmOpenAccTermDeposit.txtTDamount.text;
	gblFinActivityLogOpenAct["toActNickName"] = frmOpenAccTermDeposit.txtDSNickName.text;
	if (kony.i18n.getCurrentLocale() == "en_US") {
		frmOpenActTDConfirm.lblOATDIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelEN"] + ":";
		frmOpenActTDConfirm.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameEN"];
		} else {
		frmOpenActTDConfirm.lblOATDIntRate.text = gblFinActivityLogOpenAct["intersetRateLabelTH"] + ":";
		frmOpenActTDConfirm.lblTDCnfmTitle.text = gblFinActivityLogOpenAct["prodNameTH"];
		}
	
	if (gblisDisToActTD == "Y")
	{	
      frmOpenActTDConfirm.lblPayTDTo.setVisibility(true);
      frmOpenActTDConfirm.hbxOATDTransTo.setVisibility(true);
      frmOpenActTDConfirm.line47505874779284.setVisibility(true);
      
      frmOpenActTDConfirm.lblTDTransToNickName.text = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].lblCustName;
      frmOpenActTDConfirm.lblTDTransToActType.text = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].lblSliderAccN2;
      frmOpenActTDConfirm.lblTDTransToAccBal.text = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].lblActNoval;
      frmOpenActTDConfirm.imgOATDProTo.src = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].imgHidden;
      gblFinActivityLogOpenAct["branchENTO"] = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenBranchNameEN;
      gblFinActivityLogOpenAct["branchTHTO"] = frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenBranchNameTH;
      gblFinActivityLogOpenAct["nickTHTO"] =frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenproductThaiAssign;
      gblFinActivityLogOpenAct["nickENTO"] =frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenprodENGAssign;
      gblFinActivityLogOpenAct["actTypeToo"] =frmOpenAccTermDeposit["segToTDAct"]["selectedItems"][0].hiddenActType;
			
	}else{
      frmOpenActTDConfirm.line47505874779284.setVisibility(false);
      frmOpenActTDConfirm.lblPayTDTo.setVisibility(false);
      frmOpenActTDConfirm.hbxOATDTransTo.setVisibility(false);
	}
	
	frmOpenActTDConfirm.lblOATDActNameVal.text = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenActName;
	frmOpenAccTermDeposit.txtTDamount.skin = txtNormalBG;
	frmOpenAccTermDeposit.txtDSNickName.skin = txtNormalBG;
	
    frmOpenActTDConfirm.lblOATDBranchVal.text = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblRemainFeeValue;
    gblFinActivityLogOpenAct["toBranchName"] = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblRemainFeeValue;
    frmOpenActTDConfirm.lblTDTransFrmNickName.text = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblCustName;			
    frmOpenActTDConfirm.lblTDTransFrmActType.text = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblSliderAccN2;
    frmOpenActTDConfirm.lblTDTransFromAccBal.text = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].lblActNoval;
    frmOpenActTDConfirm.imgOAMnthlySavPic.src = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].imghidden;
    gblFinActivityLogOpenAct["branchEN"] = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenBranchNameEN;
    gblFinActivityLogOpenAct["branchTH"] = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenBranchNameTH;
    gblFinActivityLogOpenAct["nickTH"] =frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenproductThaiAssign;
    gblFinActivityLogOpenAct["nickEN"] =frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenprodENGAssign;
    gblFinActivityLogOpenAct["actTypeFromOpen"] = frmOpenAccTermDeposit["segSliderTDFrom"]["selectedItems"][0].hiddenActType;
	invokePartyInquiryOpenAct();

}

// ##TODO : implement upgrade skip counter

function ivokeCustActInqForOpenAct(upgradeSkip){
	showLoadingScreen();
	gblRetryCountRequestOTP = "0";
	var inputparam = {};
	inputparam["openActFlag"]= "true";
	if(upgradeSkip != null && upgradeSkip != undefined && upgradeSkip != "")
	{
		inputparam["upgradeSkip"] = upgradeSkip;
		clearFatcaGlobal();
	}
	invokeServiceSecureAsync("customerAccountInquiry", inputparam, callBackOpenAcntFromActs);
}

function callBackOpenAcntFromActs(status, resulttable){
	
	if (status == 400) {
	
		if (resulttable["opstatus"] == 0) {
			gblOpenActList = resulttable;
			var nonCASAAct=0;
			//getting all product code from tmb.property file.
			gblOpenAcctNormalSavings = resulttable["PROD_CODES_OPEN_ACCT_NORMALSAVINGS"];
			gblForUserProdCodes = resulttable["FOR_USE_PRODUCT_CODES"];
			gblOpenAcctSavingsCare = resulttable["SAVING_CARE_PRODUCT_CODES"];
			gblOpenAcctDreamSaving = resulttable["PROD_CODES_OPEN_ACCT_DREAMSAVINGS"];
			gblOpenAcctTermDeposit = resulttable["PROD_CODES_OPEN_ACCT_TERMDEPOSIT"];
			//alert(gblOpenAcctNormalSavings+gblForUserProdCodes+gblOpenAcctSavingsCare+gblOpenAcctDreamSaving+gblOpenAcctTermDeposit);
			for (var i = 0; i < resulttable.custAcctRec.length; i++) 
			{
				var accountStatus = resulttable["custAcctRec"][i].acctStatus;
				if(accountStatus.indexOf("Active") == -1){
					nonCASAAct = nonCASAAct + 1;
					//showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onclickActivationCompleteStart);
						//return false;
					}
			}
			dismissLoadingScreen();
			//checkIfUserHasActsWithProd();
			frmOpenActSelProdPreShowMin();
			//handling campaign flow here
			if(isCmpFlow){
				callProdCategoryBasedOnProdCodeInMB();
			} else {
				if(nonCASAAct==resulttable.custAcctRec.length)
				{
					showAlertWithCallBack(kony.i18n.getLocalizedString("MB_StatusNotEligible"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
					return false;
				}
				if(resulttable["custAcctFromRec"] == null || resulttable["custAcctFromRec"] == "" || resulttable["custAcctFromRec"] == undefined ){
	            	kony.application.dismissLoadingScreen();
	            	showAlertWithCallBack(kony.i18n.getLocalizedString("MB_CommonError_NoSA"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
					return false;
				}
                
                //Forcibly destroying account summary page
                TMBUtil.DestroyForm(frmAccountSummaryLanding);
                gblforcedCleanAS = true;
              
				if(openMoreSave){
					openMoreSave = false;
					setDataForSave();
				}else{
					setDataForUse();
				}
				frmOpenActSelProd.show();
			}
			
			//onClickAgreeOPenAcnt();
		}else{
			dismissLoadingScreen();	
			showAlert(resulttable["errMsg"], kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
	
}

/** 
 * added below function to implement campign internal link to open account prod breif screen.
 *  based on product code need to call appropriate product category.
 */
function callProdCategoryBasedOnProdCodeInMB(){

	if(undefined != gblProdCode && "" != gblProdCode) {
		if(gblOpenAcctNormalSavings.indexOf(gblProdCode) != -1 || gblForUserProdCodes.indexOf(gblProdCode) != -1){
	     	if(undefined != gblProdCode && "" != gblProdCode && gblProdCode == 221 ){//TMB No Fixed Account is a for save
				setDataForSave();
			}else {
				setDataForUse();
			}
	  	} else if(gblOpenAcctSavingsCare.indexOf(gblProdCode) != -1 || gblOpenAcctDreamSaving.indexOf(gblProdCode) != -1){
	     	setDataForSave();
	  	} else if(gblOpenAcctTermDeposit.indexOf(gblProdCode) != -1){
	     	setDataForTD();
	  	}else{
          	dismissLoadingScreen();
          	showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        }
	}
  	
}

function setDataforOpenFromActs(segName){

	segName.containerWeight = 100;
	segName.widgetDataMap = {
		lblACno: "lblACno",
		lblAcntType: "lblAcntType",
		img1: "img1",
		lblCustName: "lblCustName",
		lblBalance: "lblBalance",
		lblActNoval: "lblActNoval",
		lblDummy: "lblDummy",
		lblSliderAccN1: "lblSliderAccN1",
		lblSliderAccN2: "lblSliderAccN2",
		lblRemainFee : "lblRemainFee",
		lblRemainFeeValue: "lblRemainFeeValue",
		imghidden:"imghidden"
    };
	if(gblOpenActList["custAcctFromRec"] == null || gblOpenActList["custAcctFromRec"] == "" || gblOpenActList["custAcctFromRec"] == undefined ){
		            	kony.application.dismissLoadingScreen();
						showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
						return false;
	}
	
	var actResults = gblOpenActList["custAcctFromRec"];
	var length = actResults.length
	var count = 0;
	var imageName;
	var list = [];
	var temp = [];
	var iconcategory = "";
//
			for (j = 0; j < length; j++) {
				var locale = kony.i18n.getCurrentLocale();
				var fullProductType;
				var productType;
				var branchName;
				var accType = actResults[j]["accType"];
				
				if (locale == "en_US") {
					productType = actResults[j]["ProductNameEng"];
					fullProductType = productionNameTrun(actResults[j]["ProductNameEng"]);
					branchName = branchNameTrun(actResults[j]["BranchNameEh"]);
				} else {
					productType = actResults[j]["ProductNameThai"];
					fullProductType = productionNameTrun(actResults[j]["ProductNameThai"]);
					branchName = branchNameTrun(actResults[j]["BranchNameTh"]);
				}
				
				
				
				var accVal = actResults[j]["accId"];
				var accValLength = accVal.length;
				if (accType != "CCA"){
				accVal = accVal.substring((accValLength-10), (accValLength-7)) + "-" + accVal.substring((accValLength-7), (accValLength-6)) + "-" + accVal.substring((accValLength-6), (accValLength-1)) + "-" + accVal.substring((accValLength-1), accValLength);
				}
				else{
				accVal = accVal.substring((accValLength-16), (accValLength-12)) + "-" + accVal.substring((accValLength-12), (accValLength-8)) + "-" + accVal.substring((accValLength-8), (accValLength-4)) + "-" + accVal.substring((accValLength-4), accValLength);
				}
				
				if(accType == "SDA"){
				accType = kony.i18n.getLocalizedString("SavingMB");
				}else if(accType == "DDA"){
				accType = kony.i18n.getLocalizedString("CurrentMB");
				}
				var acctNickName;
				var productThaiAssign;
				var	prodENGAssign;
				
				
				var availableBalance = actResults[j]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
			
							if ((actResults[j]["acctNickName"]) == null || (actResults[j]["acctNickName"]) == ''){
							var sbStr = actResults[j]["accId"];
							var accLength = sbStr.length;
							var thaiProd = actResults[j]["ProductNameThai"];
							var engProd = actResults[j]["ProductNameEng"];
							
									sbStr = sbStr.substring(accLength - 4, accLength);
										
										if(productType.length > 15){
										productType = productType.substring(0,15);
										}
										if(thaiProd.length > 15){
										thaiProd = thaiProd.substring(0,15);
										}
										if(engProd.length > 15){
										engProd = engProd.substring(0,15);
										}
							
								acctNickName = productType + " " + sbStr;
								productThaiAssign = thaiProd + " " + sbStr;
								prodENGAssign = engProd + " " + sbStr;
							
							}
							else{
							acctNickName = actResults[j]["acctNickName"];
							productThaiAssign  = actResults[j]["acctNickName"];
							prodENGAssign  = actResults[j]["acctNickName"];
							}
							
							
							
							
							//Code for displaying image
				/*if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || actResults[j]["VIEW_NAME"]=="PRODUCT_CODE_NOFIXED"|| actResults[j]["VIEW_NAME"]=="PRODUCT_CODE_SAVINGCARE") 
		            imageName = "prod_savingd.png"
		         else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_CREDITCARD_TABLE") 
		            imageName = "prod_creditcard.png"
		           
		         else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE") 
		           
		            imageName = "prod_termdeposits.png"
		         else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") 
		            imageName = "prod_nofee.png"
		        
		        else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_HOMELOAN") 
		            imageName = "prod_homeloan.png"
		        else if (actResults["VIEW_NAME"] == "PRODUCT_CODE_OLDREADYCASH_TABLE"|| actResults["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE" || actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_CASH2GO") 
		            imageName = "prod_cash2go.png"
		        else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE"|| actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE") 
		            imageName = "prod_currentac.png" 
		        else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_READYCASH_TABLE") 
		            imageName = "prod_cash2go.png"  */ 
							
				
				 
				
				
				 var randomnum = Math.floor((Math.random() * 10000) + 1);
				 imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+ "NEW_" +actResults[j]["ICON_ID"]+"&modIdentifier=PRODICON&dummy="+randomnum;	
				 imageorginal="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+actResults[j]["ICON_ID"]+"&modIdentifier=PRODICON&dummy="+randomnum;
				 iconcategory=actResults[j]["ICON_ID"];	
					if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
						list.push({
							lblACno: kony.i18n.getLocalizedString("AccountNo") + " ",
							img1: imageName,
							lblCustName: acctNickName,
							lblBalance: availableBalance,
							lblActNoval: accVal,//resulttable["custAcctRec"][j]["accId"],
							lblSliderAccN1: kony.i18n.getLocalizedString("Product") + " ",
							lblSliderAccN2: fullProductType,
							lblDummy: " ",
							hiddenActVal:actResults[j]["accId"],
							hiddenActBal:actResults[j]["availableBal"],
							hiddenActName:actResults[j]["accountName"],
							hiddenFiident:actResults[j]["fiident"],
							hiddenProductId:actResults[j]["productID"],
							hiddenActType:actResults[j]["accType"],
							hiddenActPersId:actResults[j]["persionlizeAcctId"],
							lblRemainFee : kony.i18n.getLocalizedString("Branch"),
							lblRemainFeeValue: branchName,
							hiddenBranchNameEN:actResults[j]["BranchNameEh"],
							hiddenBranchNameTH:actResults[j]["BranchNameTh"],
							hiddenproductThaiAssign : productThaiAssign,
							hiddenprodENGAssign :prodENGAssign,
							imghidden:imageorginal,
							template: hbxSliderNew1
						})
						
					} else if (iconcategory == "ICON-03") {
						list.push({
							lblACno: kony.i18n.getLocalizedString("AccountNo") + " ",
							img1: imageName,
							lblCustName: acctNickName,
							lblBalance: availableBalance,
							lblActNoval: accVal,//resulttable["custAcctRec"][j]["accId"],
							lblSliderAccN1: kony.i18n.getLocalizedString("Product") + " ",
							lblSliderAccN2: fullProductType,
							lblDummy: " ",
							hiddenActVal:actResults[j]["accId"],
							hiddenActBal:actResults[j]["availableBal"],
							hiddenActName:actResults[j]["accountName"],
							hiddenFiident:actResults[j]["fiident"],
							hiddenProductId:actResults[j]["productID"],
							hiddenActType:actResults[j]["accType"],
							hiddenActPersId:actResults[j]["persionlizeAcctId"],
							lblRemainFee : kony.i18n.getLocalizedString("Branch"),
							lblRemainFeeValue: branchName,
							hiddenBranchNameEN:actResults[j]["BranchNameEh"],
							hiddenBranchNameTH:actResults[j]["BranchNameTh"],
							hiddenproductThaiAssign : productThaiAssign,
							hiddenprodENGAssign :prodENGAssign,
							imghidden:imageorginal,
							template: hbxSliderNew2
						})
						
					} else if (iconcategory == "ICON-04") {
						list.push({
							lblACno: kony.i18n.getLocalizedString("AccountNo") + " ",
							img1: imageName,
							lblCustName: acctNickName,
							lblBalance: availableBalance,
							lblActNoval: accVal,//resulttable["custAcctRec"][j]["accId"],
							lblSliderAccN1: kony.i18n.getLocalizedString("Product") + " ",
							lblSliderAccN2: fullProductType,
							lblDummy: " ",
							hiddenActVal:actResults[j]["accId"],
							hiddenActBal:actResults[j]["availableBal"],
							hiddenActName:actResults[j]["accountName"],
							hiddenFiident:actResults[j]["fiident"],
							hiddenProductId:actResults[j]["productID"],
							hiddenActPersId:actResults[j]["persionlizeAcctId"],
							hiddenActType:actResults[j]["accType"],
							lblRemainFee : kony.i18n.getLocalizedString("Branch"),
							lblRemainFeeValue: branchName,
							hiddenBranchNameEN:actResults[j]["BranchNameEh"],
							hiddenBranchNameTH:actResults[j]["BranchNameTh"],
							hiddenproductThaiAssign : productThaiAssign,
							hiddenprodENGAssign :prodENGAssign,
							imghidden:imageorginal,
							template: hbxSliderNew3
						})
						
					}
		
	}
    segName.setData(list)

    if (list.length == 1) {
      if(gblDeviceInfo["name"] == "android"){
        segName.viewConfig = {
          "coverflowConfig": {
            "rowItemRotationAngle": 0,
            "isCircular": false,
            "spaceBetweenRowItems": 10,
            "projectionAngle": 90,
            "rowItemWidth": 80
          }
        };
      }
    } else if (list.length > 1) {
      if(gblDeviceInfo["name"] == "android"){
        segName.viewConfig = {
          "coverflowConfig": {
            "rowItemRotationAngle": 0,
            "isCircular": true,
            "spaceBetweenRowItems": 10,
            "projectionAngle": 90,
            "rowItemWidth": 80
          }
        };
      }

    } else {
      kony.application.dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
      return false;
    }

}



function setDataforOpenToActs(segName,btnLeftArw,btnRgtArw){

	segName.containerWeight = 100;
	segName.widgetDataMap = {
		lblACno: "lblACno",
		lblAcntType: "lblAcntType",
		img1: "img1",
		lblCustName: "lblCustName",
		lblBalance: "lblBalance",
		lblActNoval: "lblActNoval",
		lblDummy: "lblDummy",
		lblSliderAccN1: "lblSliderAccN1",
		lblSliderAccN2: "lblSliderAccN2",
		lblRemainFee : "lblRemainFee",
		lblRemainFeeValue: "lblRemainFeeValue",
		imgHidden: "imgHidden"
	}
	var actResults = gblOpenActList["custAcctToRec"];
	
	var length = actResults.length
	
	var count = 0;
	var imageName;
	var list = [];
	var temp = [];
	var iconcategory="";
	 
//
			for (j = 0; j < length; j++) {
				var locale = kony.i18n.getCurrentLocale();
				var fullProductType;
				var productType;
				var branchName;
				
				var accType = actResults[j]["accType"];
				
				if (locale == "en_US") {
					productType = actResults[j]["ProductNameEng"];
					fullProductType = actResults[j]["ProductNameEng"];
					branchName = actResults[j]["BranchNameEh"];
				} else {
					productType = actResults[j]["ProductNameThai"];
					fullProductType = actResults[j]["ProductNameThai"];
					branchName = actResults[j]["BranchNameTh"];
				}
				
							
				
				var accVal = actResults[j]["accId"];
				var accValLength = accVal.length;
				if (accType != "CCA"){
				accVal = accVal.substring((accValLength-10), (accValLength-7)) + "-" + accVal.substring((accValLength-7), (accValLength-6)) + "-" + accVal.substring((accValLength-6), (accValLength-1)) + "-" + accVal.substring((accValLength-1), accValLength);
				}
				else{
				accVal = accVal.substring((accValLength-16), (accValLength-12)) + "-" + accVal.substring((accValLength-12), (accValLength-8)) + "-" + accVal.substring((accValLength-8), (accValLength-4)) + "-" + accVal.substring((accValLength-4), accValLength);
				}
				
				var acctNickName;
				var productThaiAssign;
				var prodENGAssign;
				
				if(accType == "SDA"){
				accType = kony.i18n.getLocalizedString("SavingMB");
				}else if(accType == "DDA"){
				accType = kony.i18n.getLocalizedString("CurrentMB");
				}
				
				
				
				var availableBalance = actResults[j]["availableBalDisplay"] + " " + kony.i18n.getLocalizedString("currencyThaiBaht")
			
							if ((actResults[j]["acctNickName"]) == null || (actResults[j]["acctNickName"]) == ''){
							var sbStr = actResults[j]["accId"];
							var accLength = sbStr.length;
							var thaiProd = actResults[j]["ProductNameThai"];
							var engProd = actResults[j]["ProductNameEng"];
							
							sbStr = sbStr.substring(accLength - 4, accLength);
							
										if(productType.length > 15){
										productType = productType.substring(0,15);
										}
										if(thaiProd.length > 15){
										thaiProd = thaiProd.substring(0,15);
										}
										if(engProd.length > 15){
										engProd = engProd.substring(0,15);
										}
							
							acctNickName = productType + " " + sbStr;
							productThaiAssign = thaiProd +" "+ sbStr; 
                            prodENGAssign = engProd+" "+ sbStr;
							}
							else{
							acctNickName = actResults[j]["acctNickName"];
							productThaiAssign = actResults[j]["acctNickName"]; 
                            prodENGAssign = actResults[j]["acctNickName"];
							}
							
							
							
							
							//Code for displaying image
				/*if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_DREAM_SAVING" || actResults[j]["VIEW_NAME"]=="PRODUCT_CODE_NOFIXED"|| actResults[j]["VIEW_NAME"]=="PRODUCT_CODE_SAVINGCARE") 
		            imageName = "prod_savingd.png"
		         else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_CREDITCARD_TABLE") 
		            imageName = "prod_creditcard.png"
		           
		         else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_TD_TABLE") 
		           
		            imageName = "prod_termdeposits.png"
		         else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_NOFEESAVING_TABLE") 
		            imageName = "prod_nofee.png"
		        
		        else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_HOMELOAN") 
		            imageName = "prod_homeloan.png"
		        else if (actResults["VIEW_NAME"] == "PRODUCT_CODE_OLDREADYCASH_TABLE"|| actResults["VIEW_NAME"] == "PRODUCT_CODE_NEWREADYCASH_TABLE" || actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_LOAN_CASH2GO") 
		            imageName = "prod_cash2go.png"
		        else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_CURRENT_TABLE"|| actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_SAVING_TABLE") 
		            imageName = "prod_currentac.png" 
		        else if (actResults[j]["VIEW_NAME"] == "PRODUCT_CODE_READYCASH_TABLE") 
		            imageName = "prod_cash2go.png"   */
				var randomnum = Math.floor((Math.random() * 10000) + 1);
				imageName="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+ "NEW_" +actResults[j]["ICON_ID"]+"&modIdentifier=PRODICON&dummy="+randomnum;
				imageorginal="https://" + appConfig.serverIp + ":" + appConfig.secureServerPort + "/" +appConfig.middlewareContext + "/" + "ImageRender?crmId=&"+ "&personalizedId=&billerId="+actResults[j]["ICON_ID"]+"&modIdentifier=PRODICON&dummy="+randomnum;
					
				 iconcategory=actResults[j]["ICON_ID"];	
					if (iconcategory == "ICON-01" || iconcategory == "ICON-02") {
						list.push({
							lblACno: kony.i18n.getLocalizedString("AccountNo"),
							img1: imageName,
							lblCustName: acctNickName,
							lblBalance: availableBalance,
							lblActNoval: accVal,//resulttable["custAcctRec"][j]["accId"],
							lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
							lblSliderAccN2: fullProductType,
							lblDummy: "-",
							hiddenActVal:actResults[j]["accId"],
							hiddenActBal:actResults[j]["availableBal"],
							hiddenActName:actResults[j]["accountName"],
							hiddenFiident:actResults[j]["fiident"],
							hiddenProductId:actResults[j]["productID"],
							hiddenActType:actResults[j]["accType"],
							hiddenActPersId:actResults[j]["persionlizeAcctId"],
							lblRemainFee : kony.i18n.getLocalizedString("Branch"),
							lblRemainFeeValue: branchName,
							hiddenBranchNameEN:actResults[j]["BranchNameEh"],
							hiddenBranchNameTH:actResults[j]["BranchNameTh"],
							hiddenproductThaiAssign : productThaiAssign,
							hiddenprodENGAssign :prodENGAssign,
							imgHidden: imageorginal,
							template: hbxSliderNew1
						})
						
					} else if (iconcategory == "ICON-03") {
						list.push({
							lblACno: kony.i18n.getLocalizedString("AccountNo"),
							img1: imageName,
							lblCustName: acctNickName,
							lblBalance: availableBalance,
							lblActNoval: accVal,//resulttable["custAcctRec"][j]["accId"],
							lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
							lblSliderAccN2: fullProductType,
							lblDummy: "-",
							hiddenActVal:actResults[j]["accId"],
							hiddenActBal:actResults[j]["availableBal"],
							hiddenActName:actResults[j]["accountName"],
							hiddenFiident:actResults[j]["fiident"],
							hiddenProductId:actResults[j]["productID"],
							hiddenActType:actResults[j]["accType"],
							hiddenActPersId:actResults[j]["persionlizeAcctId"],
							lblRemainFee : kony.i18n.getLocalizedString("Branch"),
							lblRemainFeeValue: branchName,
							hiddenBranchNameEN:actResults[j]["BranchNameEh"],
							hiddenBranchNameTH:actResults[j]["BranchNameTh"],
							hiddenproductThaiAssign : productThaiAssign,
							hiddenprodENGAssign :prodENGAssign,
							imgHidden: imageorginal,
							template: hbxSliderNew2
						})
					} else if (iconcategory == "ICON-04") {
						list.push({
							lblACno: kony.i18n.getLocalizedString("AccountNo"),
							img1: imageName,
							lblCustName: acctNickName,
							lblBalance: availableBalance,
							lblActNoval: accVal,//resulttable["custAcctRec"][j]["accId"],
							lblSliderAccN1: kony.i18n.getLocalizedString("Product"),
							lblSliderAccN2: fullProductType,
							lblDummy: "-",
							hiddenActVal:actResults[j]["accId"],
							hiddenActBal:actResults[j]["availableBal"],
							hiddenActName:actResults[j]["accountName"],
							hiddenFiident:actResults[j]["fiident"],
							hiddenProductId:actResults[j]["productID"],
							hiddenActPersId:actResults[j]["persionlizeAcctId"],
							hiddenActType:actResults[j]["accType"],
							lblRemainFee : kony.i18n.getLocalizedString("Branch"),
							lblRemainFeeValue: branchName,
							hiddenBranchNameEN:actResults[j]["BranchNameEh"],
							hiddenBranchNameTH:actResults[j]["BranchNameTh"],
							hiddenproductThaiAssign : productThaiAssign,
							hiddenprodENGAssign :prodENGAssign,
							imgHidden: imageorginal,
							template: hbxSliderNew3
						})
						
					}
		
	}
		segName.setData(list)
		
        if (list.length == 1) {
          /*
		   	//#ifdef android
		   		segName.viewConfig = {
      			coverflowConfig : {
	            projectionAngle : 60,
	            rowItemRotationAngle : 45,
	            spaceBetweenRowItems : 0,
	            rowItemWidth : 80,
	            isCircular : false
     			 }
				}   
			//#endif		   		

			*/
        } else if (list.length > 1) {
          /*
				//#ifdef android
		   		segName.viewConfig = {
      			coverflowConfig : {
	            projectionAngle : 60,
	            rowItemRotationAngle : 45,
	            spaceBetweenRowItems : 0,
	            rowItemWidth : 80,
	            isCircular : true
     			 }
				}   
				//#endif		 
				*/

        } else {
          kony.application.dismissLoadingScreen();
          showAlert(kony.i18n.getLocalizedString("keyMbActvNoAct"), kony.i18n.getLocalizedString("info"));
          return false;
        }
}

function openActNickNameUniq(uniqText){
		var nameFromService = "";
		for (i = 0; i < gblOpenActList["custAcctRec"].length ; i++) {		
		
			if ((gblOpenActList["custAcctRec"][i]["acctNickName"]) != null && (gblOpenActList["custAcctRec"][i]["acctNickName"]) != ""){
				//
				//
				nameFromService = gblOpenActList["custAcctRec"][i]["acctNickName"].toLowerCase()
			}
			else{
			if((gblOpenActList["custAcctRec"][i]["ProductNameEng"]) != null && 	(gblOpenActList["custAcctRec"][i]["ProductNameEng"]) != ""){
				var productType  = gblOpenActList["custAcctRec"][i]["ProductNameEng"];
				var sbStr = gblOpenActList["custAcctRec"][i]["accId"];
				var accLength = sbStr.length;
				sbStr = sbStr.substring(accLength - 4, accLength);
				nameFromService = productType.toLowerCase() + " " + sbStr;
			}
			
			}
			
			if (uniqText.toLowerCase() == nameFromService ){
				return false;
			}
			
		}
}

function OpenAcctEmailNotification() {
	var inputParam = {}

	var deliveryMethod = "Email";
	inputParam["notificationType"] = deliveryMethod;

	
	var currentLocale = getCurrentLocale();
	inputParam["Locale"] = currentLocale;
	
	if(gblSelOpenActProdCode == "200" || gblSelOpenActProdCode == "220"  || gblSelOpenActProdCode == "222" || gblSelOpenActProdCode == "203"){
		inputParam["source"] = "opennormalsaving";
		inputParam["accountNo"] = "xxx-x-" + gblFinActivityLogOpenAct["toActId"].substring(8, 13)+ "-x";
	}
	if(gblSelProduct == "TMBDreamSavings"){
		inputParam["source"] = "opendreamsaving";
		inputParam["accountNo"] = "xxx-x-" + gblFinActivityLogOpenAct["toActId"].substring(8, 13)+ "-x";
	}
	if(gblSelProduct == "ForTerm"){
		inputParam["source"] = "opentermDeposit";
		var termActNo = frmOpenActTDAck.lblOATDAckActNoVal.text.replace(/-/g, "");
		inputParam["accountNo"] = "xxx-x-" + termActNo.substring(4, 9)	+ "-x";
	}
	if(gblSelOpenActProdCode == "221"){
		inputParam["accountNo"] = "xxx-x-" + gblFinActivityLogOpenAct["toActId"].substring(8, 13)+ "-x";
		inputParam["source"] = "nofixed";
	}
	inputParam["nickname"] = gblFinActivityLogOpenAct["toActNickName"];
	
	inputParam["accountType"] = gblFinActivityLogOpenAct["accTypeValEN"];
	inputParam["accountTypeTH"] = gblFinActivityLogOpenAct["accTypeValTH"];
	if(currentLocale == "en_US"){
		inputParam["productName"] = gblFinActivityLogOpenAct["prodNameEN"];
	}else{
		inputParam["productName"] = gblFinActivityLogOpenAct["prodNameTH"];
	}
	inputParam["tenor"] = gblFinActivityLogOpenAct["tdTerm"];
	inputParam["amount"] = commaFormattedOpenAct(gblFinActivityLogOpenAct["transferAmount"]);
	inputParam["transrefno"] = gblTransferRefNo;	
	inputParam["notificationSubject"] = "";
	inputParam["notificationContent"] = "";
	
	inputParam["custName"] = gblCustomerName;
				
	invokeServiceSecureAsync("NotificationAdd", inputParam, callBackNotificationOpenAcct);
}

function callBackNotificationOpenAcct(status, resulttable) {
	
	if (status == 400) {
		
		if (resulttable["opstatus"] == 0) {
			
		}
	}
}


/* //commented as it is not used
function callBackNotificationAddMBForMail(status, resulttable) {
	
	if (status == 400) {
		
		
		if (resulttable["opstatus"] == 0) {
			
		}
	}
}
*/

function passingProdDescTnCEmailMB()
{
	prodDescTnC = gblFinActivityLogOpenAct["prodDesc"];
	prodDescTnC = prodDescTnC.replace(/\s+/g, '');
}




function loadTermsNConditionOpenAccountMB(){
	var input_param = {};
	var locale = kony.i18n.getCurrentLocale();
	    if (locale == "en_US") {
			input_param["localeCd"]="en_US";
		}else{
			input_param["localeCd"]="th_TH";
		}
//module key again is from tmb_tnc file
	var prodDescTnC = gblFinActivityLogOpenAct["prodDesc"];
	prodDescTnC = prodDescTnC.replace(/\s+/g, '');
	input_param["moduleKey"]= prodDescTnC;
	showLoadingScreen();
	invokeServiceSecureAsync("readUTFFile", input_param, loadTNCOpenActCallBackMB);
}

function loadTNCOpenActCallBackMB(status,result){
	if(status == 400){
		if(result["opstatus"] == 0){
			frmOpenProdDetnTnC.btnRight.setVisibility(true);
			frmOpenProdDetnTnC.btnOpenProdAgree.setVisibility(true);
			frmOpenProdDetnTnC.btnOpenProdCancel.setVisibility(true);
			frmOpenProdDetnTnC.btnOpenProdContinue.setVisibility(false);
			frmOpenProdDetnTnC.btnTermsBack.setVisibility(false);
			frmOpenProdDetnTnC.hbxOpnProdDet.setVisibility(false);
			
			frmOpenProdDetnTnC.hbxImgProdPackage.setVisibility(false);
				
			frmOpenProdDetnTnC.lblOpenActDescSubTitle.text = kony.i18n.getLocalizedString('keyTermsNConditions');
			frmOpenProdDetnTnC.btnOpenProdCancel.text = kony.i18n.getLocalizedString('keyCancelButton');
			frmOpenProdDetnTnC.btnOpenProdAgree.text = kony.i18n.getLocalizedString('keyAgreeButton');
			//frmOpenProdDetnTnC.lblOpenActDesc.text = result["fileContent"];
			frmOpenProdDetnTnC.richTextProdDetails.setVisibility(false);
			frmOpenProdDetnTnC.richtext47425439023817.setVisibility(true);
			frmOpenProdDetnTnC.richtext47425439023817.text = result["fileContent"];
			frmOpenProdDetnTnC.lblOpenActDescSubTitle.setFocus(true);
			dismissLoadingScreen();
			frmOpenProdDetnTnC.show();
		}else{
			dismissLoadingScreen();
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}

function checkDreamOpen(){
	
	var myDreamAmtBaht = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
	var tarAmount= myDreamAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
	var targetAmtBaht = frmOpenAcDreamSaving.txtODTargetAmt.text;
	var entAmount= targetAmtBaht.replace(kony.i18n.getLocalizedString("currencyThaiBaht"), "").replace(/,/g, "")
//var tarAmount = frmOpenAcDreamSaving.txtODTargetAmt.text;
	
//var entAmount = frmOpenAcDreamSaving.txtODMnthSavAmt.text;
	
	if (kony.string.isNumeric(tarAmount) ==  false){
	
	showAlert(kony.i18n.getLocalizedString("keydreamIncorrectMnthlySavingAmnt"), kony.i18n.getLocalizedString("info"));
	return false
	}else{
	var AddOpenBahtDreamMnth =frmOpenAcDreamSaving.txtODMnthSavAmt.text;
	frmOpenAcDreamSaving.txtODMnthSavAmt.text = commaFormattedOpenAct(AddOpenBahtDreamMnth)+ kony.i18n.getLocalizedString("currencyThaiBaht")
	}
	
	
	if (tarAmount.indexOf("0") == 0 ) {
	      showAlert(kony.i18n.getLocalizedString("keyPleaseentercorrectamount"), kony.i18n.getLocalizedString("info"));
	      frmOpenAcDreamSaving.txtODMyDream.text="";
		return false
	}
	
	
	var indexdot=tarAmount.indexOf(".");
			var decimal="";
			var remAmt="";
			
			if(indexdot > 0)
			{	decimal=tarAmount.substr(indexdot);
			
				 if (decimal.length > 3){
				 alert("Enter only 2 decimal values");
				 return false;
				 }
			}
	
	
	var noOfMnths;
	tarAmount = kony.os.toNumber(tarAmount);
	entAmount = kony.os.toNumber(entAmount);
	 
	
		if (tarAmount > entAmount ){
		frmOpenAcDreamSaving.txtODMyDream.text = "";
		showAlert(kony.i18n.getLocalizedString("keyDreamTargetAlert"), kony.i18n.getLocalizedString("info"));
		return false;
		}
		
	noOfMnths = entAmount/tarAmount;
	
	noOfMnths = Math.ceil(noOfMnths);
	
	
//#ifdef android
		noOfMnths = noOfMnths+ "";
//#endif
		noofmnthLocale = noOfMnths 
	
//if(frmOpenAcDreamSaving.txtODMyDream.text == "" || frmOpenAcDreamSaving.txtODMyDream.text == null){
	frmOpenAcDreamSaving.txtODMyDream.text = "";
	frmOpenAcDreamSaving.txtODMyDream.text = noOfMnths + kony.i18n.getLocalizedString("keymonths");
	gblDreamMnths = noOfMnths;
//}
}

function preShowiSavingCareConfirm(){

	if(frmOpenActSavingCareCnfNAck.hbxNofiSC.isVisible == true){
      frmOpenActSavingCareCnfNAck.lblHide.text = kony.i18n.getLocalizedString("Hide");
      frmOpenActSavingCareCnfNAck.lblHdrTxt.text = kony.i18n.getLocalizedString("keylblComplete");
      frmOpenActSavingCareCnfNAck.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("keyBalanceAfterTrans");

      frmOpenActSavingCareCnfNAck.label47505874741650.text = kony.i18n.getLocalizedString("keyOpenNotifyOne")
      frmOpenActSavingCareCnfNAck.label47505874741589.text = kony.i18n.getLocalizedString("keyOpenCompNotifi")

      frmOpenActSavingCareCnfNAck.btnOATDAckReturn.text = kony.i18n.getLocalizedString("keybtnReturn");
      frmOpenActSavingCareCnfNAck.btnOATDAckOpenMore.text = kony.i18n.getLocalizedString("btnOpenMore");
	}else{
      frmOpenActSavingCareCnfNAck.lblHdrTxt.text = kony.i18n.getLocalizedString("Confirmation");
      frmOpenActSavingCareCnfNAck.lblOpenActCnfmBal.text = kony.i18n.getLocalizedString("keyBalanceBeforTrans");

      frmOpenActSavingCareCnfNAck.btnOATDConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
      frmOpenActSavingCareCnfNAck.btnOATDConfConfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	}
	frmOpenActSavingCareCnfNAck.lblOASCNicNam.text = kony.i18n.getLocalizedString("keyNickNameIB");
	frmOpenActSavingCareCnfNAck.lblOASCActNum.text = kony.i18n.getLocalizedString("AccountNo");
	frmOpenActSavingCareCnfNAck.lblOASCActName.text = kony.i18n.getLocalizedString("AccountName");
	frmOpenActSavingCareCnfNAck.lblOASCBranch.text = kony.i18n.getLocalizedString("Branch");
	frmOpenActSavingCareCnfNAck.lblOASCOpenAct.text = kony.i18n.getLocalizedString("keyLblOpnAmount");
	//frmOpenActSavingCareCnfNAck.lblOASCIntRate.text = kony.i18n.getLocalizedString("keyNickNameIB");
	frmOpenActSavingCareCnfNAck.lblOASCOpenDate.text = kony.i18n.getLocalizedString("keyLblOpeningDate");
	frmOpenActSavingCareCnfNAck.lblOASCTranRefNo.text = kony.i18n.getLocalizedString("keyTransactionRefNo");
	frmOpenActSavingCareCnfNAck.lblOAPayIntAck.text = kony.i18n.getLocalizedString("keyBenificiaries");
	frmOpenActSavingCareCnfNAck.lblBefNameCnfrm1.text = kony.i18n.getLocalizedString("keyNameOpen");
	frmOpenActSavingCareCnfNAck.lblBefRsCnfrm1.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenActSavingCareCnfNAck.lblBefPerCnfrm1.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenActSavingCareCnfNAck.lblBefNameCnfrm2.text = kony.i18n.getLocalizedString("keyNameOpen");
	frmOpenActSavingCareCnfNAck.lblBefRsCnfrm2.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenActSavingCareCnfNAck.lblBefPerCnfrm2.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenActSavingCareCnfNAck.lblBefNameCnfrm3.text = kony.i18n.getLocalizedString("keyNameOpen");
	frmOpenActSavingCareCnfNAck.lblBefRsCnfrm3.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenActSavingCareCnfNAck.lblBefPerCnfrm3.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenActSavingCareCnfNAck.lblBefNameCnfrm4.text = kony.i18n.getLocalizedString("keyNameOpen");
	frmOpenActSavingCareCnfNAck.lblBefRsCnfrm4.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenActSavingCareCnfNAck.lblBefPerCnfrm4.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenActSavingCareCnfNAck.lblBefNameCnfrm5.text = kony.i18n.getLocalizedString("keyNameOpen");
	frmOpenActSavingCareCnfNAck.lblBefRsCnfrm5.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenActSavingCareCnfNAck.lblBefPerCnfrm5.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenActSavingCareCnfNAck.lblOATDActFrmAck.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
}

function preShowISavingCare(){
	frmOpenAccountSavingCareMB.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
	frmOpenAccountSavingCareMB.lblSelectSlider.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
	frmOpenAccountSavingCareMB.lblAmount.text = kony.i18n.getLocalizedString("keyXferAmt");
	frmOpenAccountSavingCareMB.lblNickName.text = kony.i18n.getLocalizedString("keyNickNameIB");
	frmOpenAccountSavingCareMB.lblBeneficiary.text = kony.i18n.getLocalizedString("keyBenificiaries");
	frmOpenAccountSavingCareMB.lblBefFisrtName1.text = kony.i18n.getLocalizedString("keyFirstName");
	frmOpenAccountSavingCareMB.lblBefSecName1.text = kony.i18n.getLocalizedString("keyLastName");
	frmOpenAccountSavingCareMB.lblBefRs1.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenAccountSavingCareMB.lblBefBen1.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenAccountSavingCareMB.lblBefFisrtName2.text = kony.i18n.getLocalizedString("keyFirstName");
	frmOpenAccountSavingCareMB.lblBefSecName2.text = kony.i18n.getLocalizedString("keyLastName");
	frmOpenAccountSavingCareMB.lblBefRs2.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenAccountSavingCareMB.lblBefBen2.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenAccountSavingCareMB.lblBefFisrtName3.text = kony.i18n.getLocalizedString("keyFirstName");
	frmOpenAccountSavingCareMB.lblBefSecName3.text = kony.i18n.getLocalizedString("keyLastName");
	frmOpenAccountSavingCareMB.lblBefRs3.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenAccountSavingCareMB.lblBefBen3.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenAccountSavingCareMB.lblBefFisrtName4.text = kony.i18n.getLocalizedString("keyFirstName");
	frmOpenAccountSavingCareMB.lblBefSecName4.text = kony.i18n.getLocalizedString("keyLastName");
	frmOpenAccountSavingCareMB.lblBefRs4.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenAccountSavingCareMB.lblBefBen4.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenAccountSavingCareMB.lblBefFisrtName5.text = kony.i18n.getLocalizedString("keyFirstName");
	frmOpenAccountSavingCareMB.lblBefSecName5.text = kony.i18n.getLocalizedString("keyLastName");
	frmOpenAccountSavingCareMB.lblBefRs5.text = kony.i18n.getLocalizedString("keyRelation");
	frmOpenAccountSavingCareMB.lblBefBen5.text = kony.i18n.getLocalizedString("keyBenfit");
	frmOpenAccountSavingCareMB.linkMore3.text = kony.i18n.getLocalizedString("keyAddMore");
	
	frmOpenAccountSavingCareMB.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
}


function editAddressInOpenProduct() {
	editbuttonflag="profile";
	gblOpenProdAddress = true;
	dropDownEditContactsStatePopulate();
}

function frmOpenProdEditAddressPreShow() 
{
	changeStatusBarColor();
    frmOpenProdEditAddress.btnCancelEdit.text = kony.i18n.getLocalizedString("keyCancelButton");
    frmOpenProdEditAddress.btnEditSave.text = kony.i18n.getLocalizedString("keysave");
	frmOpenProdEditAddress.lblOpenAccHeader.text = kony.i18n.getLocalizedString("keyAddressConfirmation");
	frmOpenProdEditAddress.lblcontadd.text = kony.i18n.getLocalizedString("keyAddressMailingCard");

	frmOpenProdEditAddress.hbox4758937266356.setEnabled(false);
	frmOpenProdEditAddress.hbox4758937266374.setEnabled(false);
	frmOpenProdEditAddress.hbox4758937266440.setEnabled(false);
	var ps = kony.i18n.getLocalizedString('keyIBPleaseSelect');
	var currentLocales = kony.i18n.getCurrentLocale();
	if(gblnotcountry)
	{
		frmOpenProdEditAddress.lblsubdistrict.text = ps;
        frmOpenProdEditAddress.lbldistrict.text = ps;
        frmOpenProdEditAddress.lblProvince.text = ps;
        frmOpenProdEditAddress.lblzipcode.text = ps;
	}
	else
	{
		if (gblStateValue == null || gblStateValue == "undefined" || gblStateValue == "" || gblStateEng == null || gblStateEng == "undefined" || gblStateEng == "") {
			
			frmOpenProdEditAddress.lblProvince.text = ps;
			frmOpenProdEditAddress.lbldistrict.text = ps;
			frmOpenProdEditAddress.lblsubdistrict.text = ps;
			frmOpenProdEditAddress.lblzipcode.text = ps;
		} 
		else
		{
		     if(currentLocales == "th_TH") {
				frmOpenProdEditAddress.lblProvince.text = gblStateValue;
		     } else {
		       	frmOpenProdEditAddress.lblProvince.text = gblStateEng;		
		     } 	
			if (gbldistrictValue == null || gbldistrictValue == "undefined" || gbldistrictValue == "" || gblDistEng == null || gblDistEng == "undefined" || gblDistEng == "") {
				frmOpenProdEditAddress.lbldistrict.text = ps;
				frmOpenProdEditAddress.lblsubdistrict.text = ps;
				frmOpenProdEditAddress.lblzipcode.text = ps;
			} else {
			    if(currentLocales == "th_TH") {
					frmOpenProdEditAddress.lbldistrict.text = gbldistrictValue;
			    } else {
				    frmOpenProdEditAddress.lbldistrict.text = gblDistEng;
			    } 	
				if (gblsubdistrictValue == null || gblsubdistrictValue == "undefined" || gblsubdistrictValue == "" || gblSubDistEng == null || gblSubDistEng == "undefined" || gblSubDistEng == "") {
					frmOpenProdEditAddress.lblsubdistrict.text = ps;
					frmOpenProdEditAddress.lblzipcode.text = ps;
				} else {
					    if(currentLocales == "th_TH"){
							frmOpenProdEditAddress.lblsubdistrict.text = gblsubdistrictValue;
					    } else {
							frmOpenProdEditAddress.lblsubdistrict.text = gblSubDistEng;
						} 	
						if (gblzipcodeValue == null || gblzipcodeValue == "undefined" || gblzipcodeValue == "" || gblZipEng == null || gblZipEng == "undefined" || gblZipEng == "") {
							frmOpenProdEditAddress.lblzipcode.text = ps;
						} else {
							frmOpenProdEditAddress.lblzipcode.text = gblzipcodeValue;
						}
					}
					
				}
				
		}
	}
	frmOpenProdEditAddress.txtAddress1.text = gblAddress1Value;
	frmOpenProdEditAddress.txtAddress2.text = gblAddress2Value;
	
}

function showAddressForPackageProductMB(){
	var inputParams = {};
    showLoadingScreen();
    inputParams["viewFlag"] = "OpenAccount";    
    invokeServiceSecureAsync("MyProfileViewCompositeService", inputParams, MBviewAddressServiceCallBack);
}

function MBviewAddressServiceCallBack(status,resulttable){
	if (status == 400) //success response
    {
      	if(resulttable["opstatus"] == 0) {
      		if(resulttable["Persondata"]!= null || resulttable["Persondata"]!= undefined){
				for (var i = 0; i < resulttable["Persondata"].length; i++) {
					var addressType = resulttable["Persondata"][i]["AddrType"];
					if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                        var adr3 = resulttable["Persondata"][i]["addr3"];
                        var reg = / {1,}/;
                        var tempArr = [];
                        tempArr = adr3.split(reg);
                        
                        var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
						var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
						var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
						var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";
						
						if (addressType == "Primary") {
							if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                if (isNotBlank(resulttable["Persondata"][i]["City"])) {
                                    gblStateValue = resulttable["Persondata"][i]["City"];
                                    if(tempArr[0].indexOf(subDistBan, 0) >= 0) {
										gblsubdistrictValue = tempArr[0].substring(4);
									} else if(tempArr[0].indexOf(subDistNotBan, 0) >= 0) {
											gblsubdistrictValue = tempArr[0].substring(2);
									} else {
										gblsubdistrictValue = "";
									}	
									if(tempArr[1].indexOf(distBan, 0) >= 0) {  	 
										gbldistrictValue = tempArr[1].substring(3);
									} else if(tempArr[1].indexOf(distNotBan, 0) >= 0) {
											gbldistrictValue = tempArr[1].substring(2); 
									} else 	{
										gbldistrictValue = "";
									}	
                                } 
                            } else {
                                gblsubdistrictValue = "";
                                gbldistrictValue = "";
                            }
                             
                             if(isNotBlank(resulttable["Persondata"][i]["City"])) {
        						gblStateValue = resulttable["Persondata"][i]["City"];
       						 } else {
       						 	gblStateValue = "";
       						 }
					       	
					       	if(isNotBlank(resulttable["Persondata"][i]["PostalCode"])) {
					        	gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
					       	} else {  
					        	gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
					       	}
					       
					       	if(isNotBlank(resulttable["Persondata"][i]["CountryCodeValue"])) { 
					        	gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
					       	} else {
					         	gblcountryCode = "";
					       	}
					       
					       	if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                gblnotcountry = true;
                            } else {
                                gblnotcountry = false;
                            }
                            
                            if(isNotBlank(resulttable["Persondata"][i]["addr1"])) {
                            	gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                            } else {
                             	gblAddress1Value = "";
                            }
                            
                            if(isNotBlank(resulttable["Persondata"][i]["addr2"])) {
                            	gblAddress2Value = resulttable["Persondata"][i]["addr2"];
                            } else {
                             	gblAddress2Value = "";
                            }
                            
							frmOpenProdViewAddress.lblHouseNo.text = resulttable["Persondata"][i]["addr1"];
							frmOpenProdViewAddress.lblStreet.text = resulttable["Persondata"][i]["addr2"];
							frmOpenProdViewAddress.lblSubDistrict.text = gblsubdistrictValue;
							frmOpenProdViewAddress.lblDistrict.text = gbldistrictValue;
							frmOpenProdViewAddress.lblProvince.text = resulttable["Persondata"][i]["City"];
							frmOpenProdViewAddress.lblPostalCode.text = resulttable["Persondata"][i]["PostalCode"];
						}
					}
					
				}
			
			}
			
			dismissLoadingScreen();
			frmOpenProdViewAddress.show();	
		}
	}
}

function openProductAddressValidation() {
    profileedit = true;
    profileAddrFlag = "";

        if ((frmOpenProdEditAddress.lblsubdistrict.text == gblsubdistrictValue || frmOpenProdEditAddress.lblsubdistrict.text == gblSubDistEng) && (frmOpenProdEditAddress.lbldistrict.text == gbldistrictValue || frmOpenProdEditAddress.lbldistrict.text == gblDistEng) && ((frmOpenProdEditAddress.lblzipcode.text) == gblzipcodeValue) && (frmOpenProdEditAddress.txtAddress1.text == gblAddress1Value) && (frmeditMyProfile.txtAddress2.text == gblAddress2Value) && (frmeditMyProfile.lblProvince.text == gblStateValue || frmeditMyProfile.lblProvince.text == gblStateEng)) {
            
            profileedit = false;
        }
        
    var saveaddrRes = saveEditedAddrOpenProduct(); //saveEditedAddressMyProfile(); 
    if (!saveaddrRes) {
        profileedit = false;
        return false;
    }
    gblSpaChannel = "ChangeProfile";
    
    checkMyProfileSaveAddrMB();
}

function saveEditedAddrOpenProduct() {

	var mess = kony.i18n.getLocalizedString("keyenterDetails");
        
        if ((frmOpenProdEditAddress.txtAddress1.text) == null || (frmOpenProdEditAddress.txtAddress1.text) == "") {
            frmOpenProdEditAddress.txtAddress1.skin = txtErrorBG;
            frmOpenProdEditAddress.txtAddress1.focusSkin = txtErrorBG;
            showAlert(mess, "Error");
        	return false;
        }
        if ((frmOpenProdEditAddress.txtAddress2.text) == null || (frmOpenProdEditAddress.txtAddress2.text) == "") {
            frmOpenProdEditAddress.txtAddress2.skin = txtErrorBG;
            frmOpenProdEditAddress.txtAddress2.focusSkin = txtErrorBG;
            showAlert(mess, "Error");
        	return false;
        }
        
	    if ((frmOpenProdEditAddress.lblsubdistrict.text == kony.i18n.getLocalizedString('keyIBPleaseSelect')) || (frmOpenProdEditAddress.lbldistrict.text == kony.i18n.getLocalizedString('keyIBPleaseSelect')) || (frmOpenProdEditAddress.lblzipcode.text == kony.i18n.getLocalizedString('keyIBPleaseSelect')) || (frmOpenProdEditAddress.lblProvince.text == kony.i18n.getLocalizedString('keyIBPleaseSelect'))) {
	        showAlert(mess, "Error");
	        return false;
	    } 
    
    	profileAddrFlag = "addr";
        return true;
}

function saveOpenProdAddrCallBack(status, result){
    if (status == 400) {
		
		if (result !=null && result["opstatus"] == 0) {
			gblAddress1Value = frmOpenProdEditAddress.txtAddress1.text;
			gblAddress2Value = frmOpenProdEditAddress.txtAddress2.text;
			frmOpenProdEditAddressConfirm.lblAddress1.text = result["Addr1"] + " " + result["Addr2"] + " " + result["subDistrict"];
        	frmOpenProdEditAddressConfirm.lblAddress2.text = result["district"] + " " + result["province"]  + " " + result["zipcode"];
        	dismissLoadingScreen();
			frmOpenProdEditAddressConfirm.show();
		}else{
			//do nothing
		}
	}
}

function frmOpenProdViewAddressPreShow(){
	changeStatusBarColor();
	frmOpenProdViewAddress.lblMailingAddress.text = kony.i18n.getLocalizedString("keyAddressMailingCard");
	frmOpenProdViewAddress.lblHdrTxt.text = kony.i18n.getLocalizedString("keyAddressConfirmation");
    frmOpenProdViewAddress.btnOpenProdAgree.text = kony.i18n.getLocalizedString("keyConfirm");
    frmOpenProdViewAddress.btnOpenProdCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
}

function frmOpenProdEditAddressConfirmPreShow(){
	changeStatusBarColor();
	frmOpenProdEditAddressConfirm.lblHdrConfirm.text = kony.i18n.getLocalizedString("keyAddressMailingCard");
	frmOpenProdEditAddressConfirm.lblHdrContactAddr.text = kony.i18n.getLocalizedString("keyAddressMailingCard");
	frmOpenProdEditAddressConfirm.lblHdrTxt.text = kony.i18n.getLocalizedString("keyAddressConfirmation");
	
    frmOpenProdEditAddressConfirm.btnOpenProdAgree.text = kony.i18n.getLocalizedString("keyConfirm");
    frmOpenProdEditAddressConfirm.btnOpenProdCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
}

function openProdSaveAddressOTP() {
	showOTPPopup(kony.i18n.getLocalizedString("TransactionPass") + ":", "", "", compositeEditAddressOpenProdMB, 3);
}

function compositeEditAddressOpenProdMB() {
    var trap;
    var password = null;
    var caseTrans = "profile";

    password = popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text;
    if (password == "" || password == null) {
      popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
      return;
    }
    trap = trassactionPwdValidatn(password);
    showLoadingScreen();
    if (trap == false) {
        
        dismissLoadingScreen();
        popupTractPwd.lblPopupTract7.skin = lblPopUpErr;
        popupTractPwd.lblPopupTract7.text = kony.i18n.getLocalizedString("invalidTxnPwd");
        popupTractPwd.lblPopupTract7.setVisibility(true);
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.skin = txtErrorBG;
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.focusSkin = txtErrorBG;
        popupTractPwd.show();
       if(popupTractPwd.hbxOTP.isVisible){
			kony.print("$$$$$$$$$$$$$$$$$"+popupTractPwd.hbxPopupTranscPwd.isVisible)
			popupTractPwd.txtOTP.setFocus(true)
	
	}else if(popupTractPwd.hbxPopupTranscPwd.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtTranscPwd.setFocus(true)
	}else if(popupTractPwd.hbxPoupAccesspin.isVisible){
		popupTractPwd.tbxPopupTractPwdtxtAccPin.setFocus(true)
	}
    } else {
        var compositeEditAddressMB_inputParam = {}
        // sending the global params 
        txt1 = gblAddress1Value;
        txt2 = gblAddress2Value;
        
        if(isNotBlank(StateValue)) {
        	gblStateValue = StateValue;
        }
        if(isNotBlank(districtValue)) {
        	gbldistrictValue = districtValue;
        }
        if(isNotBlank(subdistrictValue)) {
        	gblsubdistrictValue = subdistrictValue;
        }
        if(isNotBlank(zipcodeValue)) {
        	gblzipcodeValue = zipcodeValue;
        }
        
            province = gblStateValue;
            district = gbldistrictValue;
            subdistrict = gblsubdistrictValue;
            zipcode = gblzipcodeValue;
        
        
        //ending the global params
        compositeEditAddressMB_inputParam["verifyPswdMyProfile_loginModuleId"] = "MB_TxPwd";
        compositeEditAddressMB_inputParam["verifyPswdMyProfile_retryCounterVerifyAccessPin"] = gblRtyCtrVrfyAxPin;
        compositeEditAddressMB_inputParam["verifyPswdMyProfile_retryCounterVerifyTransPwd"] = gblRtyCtrVrfyTxPin;
        compositeEditAddressMB_inputParam["verifyPswdMyProfile_userStoreId"] = "DefaultStore";
        compositeEditAddressMB_inputParam["verifyPswdMyProfile_password"] = password;
        compositeEditAddressMB_inputParam["verifyPswdMyProfile_caseTrans"] = caseTrans;
		compositeEditAddressMB_inputParam["channelId"] = "MB";
        //address flag
        //sending addresschnage
        var locale = kony.i18n.getCurrentLocale();
        if (locale == "en_US") {
            compositeEditAddressMB_inputParam["partyUpdateInputMap_Language"] = "EN";
        } else {
            compositeEditAddressMB_inputParam["partyUpdateInputMap_Language"] = "TH";
        }
        compositeEditAddressMB_inputParam["globalvar_gblTokenSwitchFlag"] = gblTokenSwitchFlag;
        
        if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
        	compositeEditAddressMB_inputParam["globalvar_gblAddress"] = frmOpenProdViewAddress.lblHouseNo.text + " " + frmOpenProdViewAddress.lblStreet.text + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + frmOpenProdViewAddress.lblSubDistrict.text + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + frmOpenProdViewAddress.lblDistrict.text + " " + frmOpenProdViewAddress.lblProvince.text + " " + frmOpenProdViewAddress.lblPostalCode.text;
		} else { 
			compositeEditAddressMB_inputParam["globalvar_gblAddress"] = frmOpenProdViewAddress.lblHouseNo.text + " " + frmOpenProdViewAddress.lblStreet.text + " " + kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + frmOpenProdViewAddress.lblSubDistrict.text + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + frmOpenProdViewAddress.lblDistrict.text + " " + frmOpenProdViewAddress.lblProvince.text + " " + frmOpenProdViewAddress.lblPostalCode.text;
		}
    
        compositeEditAddressMB_inputParam["partyUpdateInputMap_Addr1"] = txt1;
        compositeEditAddressMB_inputParam["partyUpdateInputMap_Addr2"] = txt2;
        if (province == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
            compositeEditAddressMB_inputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB") + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThaiB") + district;
        } else {
            compositeEditAddressMB_inputParam["partyUpdateInputMap_Addr3"] = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + "." + subdistrict + " " + kony.i18n.getLocalizedString("gblDistPrefixThai") + "." + district;
        }
        compositeEditAddressMB_inputParam["partyUpdateInputMap_City"] = province; //kony.i18n.getLocalizedString("BangkokThaiValue");
        compositeEditAddressMB_inputParam["partyUpdateInputMap_PostalCode"] = zipcode;
        compositeEditAddressMB_inputParam["partyUpdateInputMap_CountryCodeValue"] = kony.i18n.getLocalizedString("Thailand");
       
        
        popupTractPwd.tbxPopupTractPwdtxtTranscPwd.text = "";
        invokeServiceSecureAsync("addressModifyCompositeJavaService", compositeEditAddressMB_inputParam, addressModifyCompositeJavaServiceCallBack)
    }
}

function addressModifyCompositeJavaServiceCallBack(status, resulttable) {
   var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
   var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
   var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
   var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";
    
    if (status == 400) {
        kony.application.dismissLoadingScreen();
        if (resulttable["opstatus"] == 0) {
            if (resulttable["statusCode"] == 0) {
                
				popupTractPwd.dismiss();
                completeicon = true;
                if(resulttable["errMsg_addr"] != null && resulttable["errMsg_addr"] != undefined )
				  {
				  	 
				  	 if(resulttable["errMsg_addr"].trim() == "Maximum Length Exceeded") {
				  	 	showAlert(kony.i18n.getLocalizedString("keyAdressMaxLengthExceeded"), kony.i18n.getLocalizedString("info"));
				  	 	return false;
				  	 } else{
				  	 	showAlert(kony.i18n.getLocalizedString("keyPartyUpdateFailure"), kony.i18n.getLocalizedString("info"));
				  	 	return false;
				  	 } 	
	    		     
	    		     //completeicon = false;
				  }
                
               
                 for (var i = 0; i < resulttable["Persondata"].length; i++) {
                    var tempAddrtype = resulttable["Persondata"][i]["AddrType"];
                    if (tempAddrtype == "Primary") {
                        if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                            //if(resulttable["Persondata"][i]["addr3"]){}
                            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                var adr3 = resulttable["Persondata"][i]["addr3"];
                                
                                var reg = / {1,}/;
                                var tempArr = [];
                                tempArr = adr3.split(reg);
                                
                                //if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) 
								//{
                                    if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                        if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                                            if(tempArr[0].indexOf(subDistBan, 0) >= 0)
												gblsubdistrictValue = tempArr[0].substring(4);
											else  if(tempArr[0].indexOf(subDistNotBan, 0) >= 0)   
												gblsubdistrictValue = tempArr[0].substring(2);
											else gblsubdistrictValue = "";	
											if(tempArr[1].indexOf(distBan, 0) >= 0)  	 
												gbldistrictValue = tempArr[1].substring(3);
											else   if(tempArr[1].indexOf(distNotBan, 0) >= 0)  
												gbldistrictValue = tempArr[1].substring(2); 
											else 	gbldistrictValue = "";
                                        } 
                                    } else {
                                        gblStateValue = "";
                                        gblsubdistrictValue = "";
                                        gbldistrictValue = "";
                                    }
                                    gblAddress1Value = resulttable["Persondata"][i]["addr1"];
                                    gblAddress2Value = resulttable["Persondata"][i]["addr2"];
									if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
										gblStateValue = resulttable["Persondata"][i]["City"];
									else	gblStateValue = "";
									if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
										gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
									else 	
										gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
									if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
										gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
									else	gblcountryCode = "";
                                    if (tempArr[0] == "" || tempArr[0] == undefined) {
                                        tempArr[0] = "";
                                    }
                                    if (tempArr[1] == "" || tempArr[1] == undefined) {
                                        tempArr[1] = "";
                                    }
                                    gblViewsubdistrictValue = tempArr[0];
                                    gblViewdistrictValue = tempArr[1];
                                    gblnotcountry = false;
                                    if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                        gblnotcountry = true;
                                    } else {
                                        gblnotcountry = false;
                                    }
                            }
                        }
                    }
                }
            }
            
            gotoForUseOpenAccountForm();
            
        } else {
            //MB handling
            if (resulttable["errCode"] == "VrfyTxPWDErr00003") {
              showTranPwdLockedPopup();
              popupTractPwd.dismiss();
              return;
            } else if (resulttable["errCode"] == "VrfyTxPWDErr00001" || resulttable["errCode"] == "VrfyTxPWDErr00002") {
              setTransPwdFailedError(kony.i18n.getLocalizedString("invalidTxnPwd"));
              return false;
            }
            if (resulttable["errMsg"] != undefined) {
				popupTractPwd.dismiss();
                alert(resulttable["errMsg"]);
            } else {
              	popupTractPwd.dismiss();
                alert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"));
            }
        }
        //InvalidPasswordFlow(resultTable)
        //popTransactionPwd.dismiss();
    } //status 400
}

function gotoForUseOpenAccountForm() {

  frmOpnActSelAct.btnPopUpTermination.text = kony.i18n.getLocalizedString("Next");
  frmOpnActSelAct.label475124774164.text = kony.i18n.getLocalizedString("keyOpenAcc");
  frmOpnActSelAct.lblSelectSlider.text = kony.i18n.getLocalizedString("keyOpenAmountFrom");
  frmOpnActSelAct.lblOpenActSelAmt.text = kony.i18n.getLocalizedString("keyXferAmt");
  frmOpnActSelAct.label45731775454460.text = kony.i18n.getLocalizedString("Nickname");
  setNormalSavingsData();
		
}

function checkRiskDataMB(){
	kony.print("inside checkRiskDataMB");
	showLoadingScreen();
	var inputparam = {};
	invokeServiceSecureAsync("CustomerRiskLevel", inputparam, customerRiskLevelCallBackMB);
}

function customerRiskLevelCallBackMB(status,resulttable){
	kony.print("inside customerRiskLevelCallBackMB" + JSON.stringify(resulttable));
	if(status == 400){
		dismissLoadingScreen();
		if(resulttable["opstatus"] == 0){
			if (resulttable["riskVerified"] == "Pass" ){
				checkTranPwdLocked();
			}else{
				showAlertWithCallBack(kony.i18n.getLocalizedString("MIB_Alert_CheckRisk"), kony.i18n.getLocalizedString("info"),onClickOfAccountDetailsBack);
				return false;
			}
		}else{
			showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
			return false;
		}
	}
}


// Added by Vijay to show the form 
function frmCheckContactInfoPreShow(){
	changeStatusBarColor();
	frmCheckContactInfo.scrollboxMain.scrollToEnd();
	frmCheckContactInfo.lblHdrTxt.text = kony.i18n.getLocalizedString("Kyc_ttl");
	frmCheckContactInfo.lblContactinfo.text = kony.i18n.getLocalizedString("Kyc_lbl_contactInfo");
	frmCheckContactInfo.lblName.text = kony.i18n.getLocalizedString("Kyc_lbl_thname");
	frmCheckContactInfo.lblName2.text = kony.i18n.getLocalizedString("Kyc_lbl_enname");
	frmCheckContactInfo.lblRegisterAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_registeraddr");
	frmCheckContactInfo.lblContactAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_contactaddr");
	frmCheckContactInfo.lblMobileNum.text = kony.i18n.getLocalizedString("Kyc_lbl_mobileno");
	frmCheckContactInfo.lblEmailAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_email");
	frmCheckContactInfo.lblbottomlabel.text = kony.i18n.getLocalizedString("Kyc_lbl_contactdesc");
    frmCheckContactInfo.lblMobile.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
    frmCheckContactInfo.btnnext.text  = kony.i18n.getLocalizedString("Next");
    frmCheckContactInfo.btnback.text  = kony.i18n.getLocalizedString("Back");
    
}

// Added by Vijay to show the form 
function onClickCheckContactInfo(){
      
      if(gblOpenActSavingCareEditCont==true){
            frmMBSavingsCareContactInfo.show();
	  }else if (gblCCDBCardFlow == "DEBIT_CARD_REISSUE"){
			frmMBBlockCardCCDBConfirm.show();
      }else{
            invokePartyInquiryService();
      }
}

function frmCheckOccupationInfoPreShow(){
	changeStatusBarColor();
	frmOccupationInfo.scrollboxMain.scrollToEnd();
	frmOccupationInfo.lblHdrTxt.text = kony.i18n.getLocalizedString("Kyc_ttl");
	frmOccupationInfo.lblOccupationinfo.text = kony.i18n.getLocalizedString("Kyc_lbl_occupationInfo");
	frmOccupationInfo.lblOccupationName.text = kony.i18n.getLocalizedString("Kyc_lbl_occupation");
	frmOccupationInfo.lblOfficeName.text = kony.i18n.getLocalizedString("Kyc_lbl_officename");
	frmOccupationInfo.lblOfficeAddr.text = kony.i18n.getLocalizedString("Kyc_lbl_officeaddress");
	frmOccupationInfo.lblincomesource.text = kony.i18n.getLocalizedString("Kyc_lbl_sourceofIncome");
	frmOccupationInfo.lblcountryincome.text = kony.i18n.getLocalizedString("Kyc_lbl_countryofIncome");
	frmOccupationInfo.btnconfirm.text = kony.i18n.getLocalizedString("keyConfirm");
	frmOccupationInfo.btnback.text = kony.i18n.getLocalizedString("Back");
	frmOccupationInfo.lblbottomlabel.text = kony.i18n.getLocalizedString("Kyc_lbl_occupdesc");
	if (locale == "en_US"){
		if(gblPartyInqOARes["OccupationEN"] != null && gblPartyInqOARes["OccupationEN"] != "" && gblPartyInqOARes["OccupationEN"] != undefined){
			frmOccupationInfo.lblName1.text = gblPartyInqOARes["OccupationEN"];
		}else{
			frmOccupationInfo.lblName1.text = "-";
		}
		if(gblPartyInqOARes["CompanyName"] != null && gblPartyInqOARes["CompanyName"] != "" && gblPartyInqOARes["CompanyName"] != undefined){
			frmOccupationInfo.officename.text = gblPartyInqOARes["CompanyName"];
		}else{
			frmOccupationInfo.officename.text = "-";
		}
		if(gblOfficeAddrOpenAc != null && gblOfficeAddrOpenAc != "" && gblOfficeAddrOpenAc != undefined){
			frmOccupationInfo.lblRegAddr.text = gblOfficeAddrOpenAc; 
		}else{
			frmOccupationInfo.lblRegAddr.text = "-";
		}
		if(gblPartyInqOARes["SourceOfIncomeEN"] != null && gblPartyInqOARes["SourceOfIncomeEN"] != "" && gblPartyInqOARes["SourceOfIncomeEN"] != undefined){
			frmOccupationInfo.incomesource.text = gblPartyInqOARes["SourceOfIncomeEN"];
		}else{
			frmOccupationInfo.incomesource.text = "-";
		}
		if(gblPartyInqOARes["CountryOfIncomeEN"] != null && gblPartyInqOARes["CountryOfIncomeEN"] != "" && gblPartyInqOARes["CountryOfIncomeEN"] != undefined){
			frmOccupationInfo.countryincome.text = gblPartyInqOARes["CountryOfIncomeEN"];
		}else{
			frmOccupationInfo.countryincome.text = "-";
		}
	}else{
		if(gblPartyInqOARes["OccupationTH"] != null && gblPartyInqOARes["OccupationTH"] != "" && gblPartyInqOARes["OccupationTH"] != undefined){
			frmOccupationInfo.lblName1.text = gblPartyInqOARes["OccupationTH"];
		}else{
			frmOccupationInfo.lblName1.text = "-";
		}
		if(gblPartyInqOARes["CompanyName"] != null && gblPartyInqOARes["CompanyName"] != "" && gblPartyInqOARes["CompanyName"] != undefined){
			frmOccupationInfo.officename.text = gblPartyInqOARes["CompanyName"];
		}else{
			frmOccupationInfo.officename.text = "-";
		}
		if(gblOfficeAddrOpenAc != null && gblOfficeAddrOpenAc != "" && gblOfficeAddrOpenAc != undefined){
			frmOccupationInfo.lblRegAddr.text = gblOfficeAddrOpenAc; 
		}else{
			frmOccupationInfo.lblRegAddr.text = "-";
		}
		if(gblPartyInqOARes["SourceOfIncomeTH"] != null && gblPartyInqOARes["SourceOfIncomeTH"] != "" && gblPartyInqOARes["SourceOfIncomeTH"] != undefined){
			frmOccupationInfo.incomesource.text = gblPartyInqOARes["SourceOfIncomeTH"];
		}else{
			frmOccupationInfo.incomesource.text = "-";
		}
		if(gblPartyInqOARes["CountryOfIncomeTH"] != null && gblPartyInqOARes["CountryOfIncomeTH"] != "" && gblPartyInqOARes["CountryOfIncomeTH"] != undefined){
			frmOccupationInfo.countryincome.text = gblPartyInqOARes["CountryOfIncomeTH"];
		}else{
			frmOccupationInfo.countryincome.text = "-";
		}
	}
	
}

function OnclickShowCheckContactInfo(){
//alert(gblPartyInqOARes["OccupationEN"]);
	if(gblPartyInqOARes["OccupationEN"] != null && gblPartyInqOARes["OccupationEN"] != "" && gblPartyInqOARes["OccupationEN"] != undefined){
		frmOccupationInfo.lblName1.text = gblPartyInqOARes["OccupationEN"];
	}else{
		frmOccupationInfo.lblName1.text = "-";
	}
	if(gblPartyInqOARes["CompanyName"] != null && gblPartyInqOARes["CompanyName"] != "" && gblPartyInqOARes["CompanyName"] != undefined){
		frmOccupationInfo.officename.text = gblPartyInqOARes["CompanyName"];
	}else{
		frmOccupationInfo.officename.text = "-";
	}
	if(gblOfficeAddrOpenAc != null && gblOfficeAddrOpenAc != "" && gblOfficeAddrOpenAc != undefined){
		frmOccupationInfo.lblRegAddr.text = gblOfficeAddrOpenAc; 
	}else{
		frmOccupationInfo.lblRegAddr.text = "-";
	}
	if(gblPartyInqOARes["SourceOfIncomeEN"] != null && gblPartyInqOARes["SourceOfIncomeEN"] != "" && gblPartyInqOARes["SourceOfIncomeEN"] != undefined){
		frmOccupationInfo.incomesource.text = gblPartyInqOARes["SourceOfIncomeEN"];
	}else{
		frmOccupationInfo.incomesource.text = "-";
	}
	if(gblPartyInqOARes["CountryOfIncomeEN"] != null && gblPartyInqOARes["CountryOfIncomeEN"] != "" && gblPartyInqOARes["CountryOfIncomeEN"] != undefined){
		frmOccupationInfo.countryincome.text = gblPartyInqOARes["CountryOfIncomeEN"];
	}else{
		frmOccupationInfo.countryincome.text = "-";
	}
	frmOccupationInfo.show();
}


function frmeditConactInfoPreShow() 
{
	changeStatusBarColor();
	//nulltheGlobalsEditContacts();
	//frmeditContactInfo.txtemailvalue.placeholder=gblEmailAddr;
	frmeditContactInfo.label475124774164.text = kony.i18n.getLocalizedString("Kyc_lbl_EditcontactInfo");
    //if(!(LocaleController.isFormUpdatedWithLocale(frmeditContactInfo.id))){	
		frmeditContactInfo.lblemail.text =  	kony.i18n.getLocalizedString("keyEmail");
		frmeditContactInfo.button474230331217991.text = kony.i18n.getLocalizedString("keyCancelButton");
		frmeditContactInfo.button474230331217989.text = kony.i18n.getLocalizedString("keysave");
		frmeditContactInfo.lblcontadd.text = kony.i18n.getLocalizedString("keyContactAddress");
		//LocaleController.updatedForm(frmeditContactInfo.id);
  	//}
   	frmeditContactInfo.hbox4758937266356.setEnabled(false);
	frmeditContactInfo.hbox4758937266374.setEnabled(false);
	frmeditContactInfo.hbox4758937266440.setEnabled(false);
	var ps = kony.i18n.getLocalizedString('keyIBPleaseSelect');
	var currentLocales = kony.i18n.getCurrentLocale();
	if(gblnotcountry)
	{
		frmeditContactInfo.lblsubdistrict.text = ps;
        frmeditContactInfo.lbldistrict.text = ps;
        frmeditContactInfo.lblProvince.text = ps;
        frmeditContactInfo.lblzipcode.text = ps;
	}
	else
	{
		if(mptemp != "locale" && confirmEdit != true)
		{
			if (gblStateValue == null || gblStateValue == "undefined" || gblStateValue == "" || gblStateEng == null || gblStateEng == "undefined" || gblStateEng == "") {
				
				frmeditContactInfo.lblProvince.text = ps;
				frmeditContactInfo.lbldistrict.text = ps;
				frmeditContactInfo.lblsubdistrict.text = ps;
				frmeditContactInfo.lblzipcode.text = ps;
			} 
			else
			{
			        if(currentLocales == "th_TH")
							frmeditContactInfo.lblProvince.text = gblStateValue;
						else 	frmeditContactInfo.lblProvince.text = gblStateEng;		
					if (gbldistrictValue == null || gbldistrictValue == "undefined" || gbldistrictValue == "" || gblDistEng == null || gblDistEng == "undefined" || gblDistEng == "") {
						frmeditContactInfo.lbldistrict.text = ps;
						frmeditContactInfo.lblsubdistrict.text = ps;
						frmeditContactInfo.lblzipcode.text = ps;
					} else {
					    if(currentLocales == "th_TH")
							frmeditContactInfo.lbldistrict.text = gbldistrictValue;
						else 	frmeditContactInfo.lbldistrict.text = gblDistEng;
						if (gblsubdistrictValue == null || gblsubdistrictValue == "undefined" || gblsubdistrictValue == "" || gblSubDistEng == null || gblSubDistEng == "undefined" || gblSubDistEng == "") {
								frmeditContactInfo.lblsubdistrict.text = ps;
								frmeditContactInfo.lblzipcode.text = ps;
							} 
						else {
						    if(currentLocales == "th_TH")
								frmeditContactInfo.lblsubdistrict.text = gblsubdistrictValue;
							else 	frmeditContactInfo.lblsubdistrict.text = gblSubDistEng;
							if (gblzipcodeValue == null || gblzipcodeValue == "undefined" || gblzipcodeValue == "" || gblZipEng == null || gblZipEng == "undefined" || gblZipEng == "") {
								frmeditContactInfo.lblzipcode.text = ps;
							} else {
								frmeditContactInfo.lblzipcode.text = gblzipcodeValue;
							}
						}
						
					}
					
			}
		}
		else
		{
			mptemp = "preshow";
			selectState = frmeditContactInfo.lblProvince.text;
			selectDist = frmeditContactInfo.lbldistrict.text;
			selectSubDist = frmeditContactInfo.lblsubdistrict.text;
			selectZip = frmeditContactInfo.lblzipcode.text;
	
			populateAddressLocaleConfirmEdit();
		}
	}
	
	gblDeviceInfo = kony.os.deviceInfo();
	if (gblDeviceInfo["name"] == "android") {
		if (kony.application.getCurrentForm().id != "frmMenu") {
			if (kony.application.getCurrentForm().id == "frmOpenProdViewAddress" || 
				kony.application.getCurrentForm().id == "frmMBBlockCardCCDBConfirm" ) {
				isVisibleEmailInput = false;
			}else{
				isVisibleEmailInput = true;
			}
		}
	}else{
		if (kony.application.getCurrentForm().id != "frmMenu" && kony.application.getPreviousForm().id != "frmMenu") {
			if (kony.application.getPreviousForm().id == "frmOpenProdViewAddress" || 
				kony.application.getPreviousForm().id == "frmMBBlockCardCCDBConfirm" ) {
				isVisibleEmailInput = false;
			}else{
				isVisibleEmailInput = true;
			}
		}
	}
	frmeditContactInfo.lblemail.isVisible = isVisibleEmailInput;
	frmeditContactInfo.txtemailvalue.isVisible = isVisibleEmailInput;
	frmeditContactInfo.line449146309336938.isVisible = isVisibleEmailInput;
	frmeditContactInfo.line447269476337926.isVisible = isVisibleEmailInput;
}

function populateAddressLocaleConfirmEdit(){
	
    var index = 0;
    var currentLocales = kony.i18n.getCurrentLocale();
    var psEn = kony.i18n.getLocalizedString('keyPleaseSelectEng');
    var psTh = kony.i18n.getLocalizedString('keyPleaseSelectTH');
    var key = selectState;
    for (; index < resulttableState.length; index++) {
        if (resulttableState[index].ProvinceNameTH == key || resulttableState[index].ProvinceNameEN == key) {
            if(currentLocales == "en_US")
        		frmeditContactInfo.lblProvince.text = resulttableState[index].ProvinceNameEN;
        	else if(currentLocales == "th_TH")
        		frmeditContactInfo.lblProvince.text = resulttableState[index].ProvinceNameTH;
        	break;	
        }
     }   
     if (key == psEn || key == psTh)
     {
       	if(currentLocales == "en_US")
       		frmeditContactInfo.lblProvince.text = psEn;
       	else if(currentLocales == "th_TH")
       		frmeditContactInfo.lblProvince.text = psTh;
      }			
    key = selectDist;
    index = 0;
    for (; index < resulttableDist.length; index++) {
        if (resulttableDist[index].DistrictNameTH == key || resulttableDist[index].DistrictNameEN == key) {
            if(currentLocales == "en_US")
        		frmeditContactInfo.lbldistrict.text = resulttableDist[index].DistrictNameEN;
        	else if(currentLocales == "th_TH")
        		frmeditContactInfo.lbldistrict.text = resulttableDist[index].DistrictNameTH;
        	break;	
        }
   }
   if (key == psEn || key == psTh) 
   {
       	if(currentLocales == "en_US")
       		frmeditContactInfo.lbldistrict.text = psEn;
       	else if(currentLocales == "th_TH")
       		frmeditContactInfo.lbldistrict.text = psTh;
   }		
   key = selectSubDist;
   index = 0;
   for (; index < resulttableSubDist.length; index++) {
        if (resulttableSubDist[index].SubDistrictNameTH == key || resulttableSubDist[index].SubDistrictNameEN == key) {
            if(currentLocales == "en_US")
        		frmeditContactInfo.lblsubdistrict.text = resulttableSubDist[index].SubDistrictNameEN;
        	else if(currentLocales == "th_TH")
        		frmeditContactInfo.lblsubdistrict.text = resulttableSubDist[index].SubDistrictNameTH;
        	break;	
        }
   }
   if (key == psEn || key == psTh)
   { 
       	if(currentLocales == "en_US")
       		frmeditContactInfo.lblsubdistrict.text = psEn;
       	else if(currentLocales == "th_TH")
       		frmeditContactInfo.lblsubdistrict.text = psTh;
   }		
   key = selectZip;
   index = 0;
   for (; index < resulttableStateZip.length; index++) {
        if (resulttableStateZip[index].ZipCode == key || resulttableStateZip[index].ZipCode == key) {
            if(currentLocales == "en_US")
        		frmeditContactInfo.lblzipcode.text = resulttableStateZip[index].ZipCode;
        	else if(currentLocales == "th_TH")
        		frmeditContactInfo.lblzipcode.text = resulttableStateZip[index].ZipCode;
        	break;	
        }
    }    
    if (key == psEn || key == psTh)
    {
       	if(currentLocales == "en_US")
       		frmeditContactInfo.lblzipcode.text = psEn;
       	else if(currentLocales == "th_TH")
       		frmeditContactInfo.lblzipcode.text = psTh;
    }			
    if(changeState == true)
    	frmeditContactInfo.hbox4758937266356.setEnabled(true); 
    if(changedist == true)
    	frmeditContactInfo.hbox4758937266374.setEnabled(true);
    if(changeSubDist == true)
    	frmeditContactInfo.hbox4758937266440.setEnabled(true);
}

function invokePartyInquiryService(){
	var input_param = {};
	//input_param["deviceID"] = GBL_UNIQ_ID ;
 	input_param["viewFlag"] = "OpenAccount";  
	showLoadingScreen();
	invokeServiceSecureAsync("MyProfileViewCompositeService", input_param, callBackOpenAccountContacts);	
}

function callBackOpenAccountContacts(status,resulttable){
	if (status == 400) //success response
    {
      	dismissLoadingScreenPopup();
      	if(resulttable["opstatus"] == 0) {
      		kony.print("inside callBackOpenAccountContacts------->" + JSON.stringify(resulttable));
      		gblOfficeAddrOpenAc = "";
      		gblPartyInqOARes = "";
			gblPartyInqOARes = resulttable;
			if (resulttable["emailAddr"] != null) {
                    gblEmailAddr = resulttable["emailAddr"];
                    gblEmailAddrOld = gblEmailAddr;
                    frmCheckContactInfo.lblEmailAddr1.text = gblEmailAddr;
            }
			for (var i = 0; i < resulttable["ContactNums"].length; i++) {
				var PhnType = resulttable["ContactNums"][i]["PhnType"];
							   
				if (PhnType != null && PhnType != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined) {
					if (PhnType == "Mobile") {
						if ((resulttable["ContactNums"][i]["PhnNum"] != null) && (resulttable["ContactNums"][i]["PhnNum"] != "" && resulttable["ContactNums"][i]["PhnNum"] != undefined)) {
							gblPHONENUMBER = resulttable["ContactNums"][i]["PhnNum"];
							gblPHONENUMBEROld = gblPHONENUMBER;
							GblMobileAL = HidePhnNum(gblPHONENUMBER);
							
							frmCheckContactInfo.lblMobile.text = "xxx-xxx-" + gblPHONENUMBER.substring(6, 10);
						}
					}
				}
            }
            if(resulttable["Persondata"]!= null || resulttable["Persondata"]!= undefined){
					for (var i = 0; i < resulttable["Persondata"].length; i++) {
					var addressType = resulttable["Persondata"][i]["AddrType"];
					if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                        var adr3 = resulttable["Persondata"][i]["addr3"];
                        var reg = / {1,}/;
                        var tempArr = [];
                        tempArr = adr3.split(reg);
                        
                        var subDistBan = kony.i18n.getLocalizedString("gblsubDtPrefixThaiB");
						var subDistNotBan = kony.i18n.getLocalizedString("gblsubDtPrefixThai") + ".";
						var distBan = kony.i18n.getLocalizedString("gblDistPrefixThaiB");
						var distNotBan = kony.i18n.getLocalizedString("gblDistPrefixThai") + ".";
						
						if (addressType == "Primary") {
							if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                if (resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined) {
                                    if(tempArr[0].indexOf(subDistBan, 0) >= 0)
										gblsubdistrictValue = tempArr[0].substring(4);
									else if(tempArr[0].indexOf(subDistNotBan, 0) >= 0) 
											gblsubdistrictValue = tempArr[0].substring(2);
										 else gblsubdistrictValue = "";	
									if(tempArr[1].indexOf(distBan, 0) >= 0)  	 
										gbldistrictValue = tempArr[1].substring(3);
									else if(tempArr[1].indexOf(distNotBan, 0) >= 0) 
											gbldistrictValue = tempArr[1].substring(2); 
										 else 	gbldistrictValue = "";
                                } 
                            } else {
                                gblsubdistrictValue = "";
                                gbldistrictValue = "";
                            }
                            if (tempArr[0] == undefined || tempArr[0] == "" || tempArr[0] == null || tempArr[0] == "undefined") gblViewsubdistrictValue = "";
                            else gblViewsubdistrictValue = tempArr[0];
                            if (tempArr[1] == undefined || tempArr[1] == "" || tempArr[1] == null || tempArr[1] == "undefined") gblViewdistrictValue = "";
                            else gblViewdistrictValue = tempArr[1];
                            if(resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != undefined)
								gblStateValue = resulttable["Persondata"][i]["City"];
							else	gblStateValue = "";
							if(resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != undefined)
								gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
							else 	
								gblzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
							if(resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)	
								gblcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
							else	gblcountryCode = "";
							if (resulttable["Persondata"][i]["CountryCodeValue"] != kony.i18n.getLocalizedString("Thailand")) {
                                gblnotcountry = true;
                            } else {
                                gblnotcountry = false;
                            } 
							gblAddress1Value = resulttable["Persondata"][i]["addr1"];
							gblAddress2Value = resulttable["Persondata"][i]["addr2"];
							frmCheckContactInfo.lblContactAddr1.text = gblAddress1Value + " " + gblAddress2Value + " " + gblViewsubdistrictValue + " " + gblViewdistrictValue + " " + gblStateValue + " " + gblzipcodeValue + " " + gblcountryCode;
							
						}else if (addressType == "Registered") {
                        
	                        //alert("registered address..."); 
	                        if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
	                            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
	                                var adr3 = resulttable["Persondata"][i]["addr3"];
	                                //alert(adr3);
	                                var reg = / {1,}/;
	                                var tempArr = [];
	                                tempArr = adr3.split(reg);
	                                //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 
									{
	                                    if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
	                                        if (resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != undefined && resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
	                                            gblregsubdistrictValue = tempArr[0] //.substring(3);
	                                            gblregdistrictValue = tempArr[1] //.substring(3);
	                                        } else {
	                                            gblregsubdistrictValue = tempArr[0] //.substring(2);
	                                            gblregdistrictValue = tempArr[1] //.substring(2);
	                                        }
	                                    }
	                                }
	                                gblregAddress1Value = resulttable["Persondata"][i]["addr1"];
	                                gblregAddress2Value = resulttable["Persondata"][i]["addr2"];
	                                if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "" || resulttable["Persondata"][i]["City"] != undefined) {
	                                    gblregStateValue = resulttable["Persondata"][i]["City"];
	                                } else {
	                                    gblregStateValue = "";
	                                }
	                                //gblregStateValue = resulttable["Persondata"][i]["City"];
	                                if(resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != undefined)
	                                	gblregzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
	                                else   gblregzipcodeValue = "";	
	                                if(resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)
	                                	gblregcountryCodeIB = resulttable["Persondata"][i]["CountryCodeValue"];
	                                else 	gblregcountryCodeIB = "";
	                                frmCheckContactInfo.lblRegAddr.text = gblregAddress1Value + " " + gblregAddress2Value + " " + gblregsubdistrictValue + " " + gblregdistrictValue + " " + " " + gblregStateValue + " " + " " + gblregzipcodeValue + " " + " " + gblregcountryCode;
	                                
	                            }
	                        }
	                    }else if (addressType == "Office") {
                        if (resulttable["Persondata"][i]["AddrType"] != null && resulttable["Persondata"][i]["AddrType"] != "" && resulttable["Persondata"][i]["AddrType"] != undefined) {
                            if (resulttable["Persondata"][i]["addr3"] != null || resulttable["Persondata"][i]["addr3"] != "" || resulttable["Persondata"][i]["addr3"] != undefined) {
                                var adr3 = resulttable["Persondata"][i]["addr3"];
                                var reg = / {1,}/;
                                var tempArr = [];
                                tempArr = adr3.split(reg);
                                var gblOffsubdistrictValue = "";
                                var gblOffdistrictValue = "";
	                            var gblOffAddress1Value = "";
								var	gblOffAddress2Value = "";
								var	gblOffStateValue = "";
								var	gblOffzipcodeValue = "";
								var	gblOffcountryCode = "";
                                //if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "") 
								{
                                    if (tempArr[0] != null && tempArr[1] != null && tempArr[0] != "" && tempArr[1] != "" && tempArr[0] != undefined && tempArr[1] != undefined) {
                                        if (resulttable["Persondata"][i]["City"] != "" && resulttable["Persondata"][i]["City"] != null && resulttable["Persondata"][i]["City"] != undefined && resulttable["Persondata"][i]["City"] == kony.i18n.getLocalizedString("BangkokThaiValueProfile")) {
                                            gblOffsubdistrictValue = tempArr[0] //.substring(3);
                                            gblOffdistrictValue = tempArr[1] //.substring(3);
                                        } else {
                                            gblOffsubdistrictValue = tempArr[0] //.substring(2);
                                            gblOffdistrictValue = tempArr[1] //.substring(2);
                                        }
                                    }
                                }
                                gblOffAddress1Value = resulttable["Persondata"][i]["addr1"];
                                gblOffAddress2Value = resulttable["Persondata"][i]["addr2"];
                                if (resulttable["Persondata"][i]["City"] != null || resulttable["Persondata"][i]["City"] != "" || resulttable["Persondata"][i]["City"] != undefined) {
                                    gblOffStateValue = resulttable["Persondata"][i]["City"];
                                } else {
                                    gblOffStateValue = "";
                                }
                                //gblregStateValue = resulttable["Persondata"][i]["City"];
                                if(resulttable["Persondata"][i]["PostalCode"] != "" && resulttable["Persondata"][i]["PostalCode"] != null && resulttable["Persondata"][i]["PostalCode"] != undefined)
                                	gblOffzipcodeValue = resulttable["Persondata"][i]["PostalCode"];
                                else   gblOffzipcodeValue = "";	
                                if(resulttable["Persondata"][i]["CountryCodeValue"] != "" && resulttable["Persondata"][i]["CountryCodeValue"] != null && resulttable["Persondata"][i]["CountryCodeValue"] != undefined)
                                	gblOffcountryCode = resulttable["Persondata"][i]["CountryCodeValue"];
                                else 	gblOffcountryCode = "";
                                gblOfficeAddrOpenAc = gblOffAddress1Value + " " + gblOffAddress2Value + " " + gblOffsubdistrictValue + " " + gblOffdistrictValue + " " + " " + gblOffStateValue + " " + " " + gblOffzipcodeValue + " " + " " + gblOffcountryCode;
                                
                            }
                        }
                    }
					}
					if(gblPartyInqOARes["customerNameTH"] != null && gblPartyInqOARes["customerNameTH"] != "" && gblPartyInqOARes["customerNameTH"] != undefined){
						frmCheckContactInfo.lblName1.text = gblPartyInqOARes["customerNameTH"];
					}else{
						frmCheckContactInfo.lblName1.text = "-";
					}
					if(gblPartyInqOARes["customerName"] != null && gblPartyInqOARes["customerName"] != "" && gblPartyInqOARes["customerName"] != undefined){
						frmCheckContactInfo.lblengname.text = gblPartyInqOARes["customerName"];
					}else{
						frmCheckContactInfo.lblengname.text = "-";
					}
					if(gblPartyInqOARes["emailAddr"] != null && gblPartyInqOARes["emailAddr"] != "" && gblPartyInqOARes["emailAddr"] != undefined){
						frmCheckContactInfo.lblEmailAddr1.text = gblPartyInqOARes["emailAddr"];
					}else{
						frmCheckContactInfo.lblEmailAddr1.text = "-";
					}
					kony.print("s2sBusinessHrsFlag in callBackOpenAccountContacts" + gblPartyInqOARes["s2sBusinessHrsFlag"]);
					if(gblPartyInqOARes["s2sBusinessHrsFlag"] != null && gblPartyInqOARes["s2sBusinessHrsFlag"] != "" && gblPartyInqOARes["s2sBusinessHrsFlag"] != undefined){
						gblOpenActBusinessHrs = gblPartyInqOARes["s2sBusinessHrsFlag"];
					}
					if(gblPartyInqOARes["s2sStartTime"] != null && gblPartyInqOARes["s2sStartTime"] != "" && gblPartyInqOARes["s2sStartTime"] != undefined){
						s2sStartTime = gblPartyInqOARes["s2sStartTime"];
					}
					if(gblPartyInqOARes["s2sEndTime"] != null && gblPartyInqOARes["s2sEndTime"] != "" && gblPartyInqOARes["s2sEndTime"] != undefined){
						s2sEndTime = gblPartyInqOARes["s2sEndTime"];
					}
					frmCheckContactInfo.show();	
				}
			}
		}
	}
}

 function dropDownEditContactsStatePopulate()
 {
    resetState = true;
    gblMyProfileAddressFlag = "state";
    showLoadingScreen();
    var inputParams = {};
    invokeServiceSecureAsync("MyprofileAddressProvinceJavaService", inputParams, dropDownPopulateAddrCommonCallbck); 
 }
 
 function dropDownEditContactsdistrictPopulate() 
 {
   	resetdistrict = true;
    gblMyProfileAddressFlag = "district";
    showLoadingScreen();
    var inputParams = {};
    inputParams["provinceCD"] = provinceCD;
    invokeServiceSecureAsync("MyprofileAddressDistrictJavaService", inputParams, dropDownPopulateAddrCommonCallbck);
 }
 
 function dropDownEditContactssubDistrictPopulate()
 {
   	resetsubdistrict = true;
    gblMyProfileAddressFlag = "subdistrict";
    showLoadingScreen();
    var inputParams = {};
    inputParams["districtCD"] = DistrictCD;
    invokeServiceSecureAsync("MyprofileAddressSubDistrictJavaService", inputParams, dropDownPopulateAddrCommonCallbck);
 }
 
 function dropDownEditContactszipCodetPopulate()
 {
    gblMyProfileAddressFlag = "zipcode";
    showLoadingScreen();
    var inputParams = {};
    inputParams["districtCD"] = DistrictCD;
    invokeServiceSecureAsync("MyprofileAddressZipcodeJavaService", inputParams, dropDownPopulateAddrCommonCallbck);
 }
 
 function dropDownPopulateAddrCommonCallbck(status, resulttable) {
    
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            // 
	            if (gblMyProfileAddressFlag == "state") {
	                    setDropdowninEngEditContacts(resulttable["state"]);
	            } else if (gblMyProfileAddressFlag == "district") {
	                    setDropdowninEngEditContacts(resulttable["district"]);
	            } else if (gblMyProfileAddressFlag == "subdistrict") {
	                    setDropdowninEngEditContacts(resulttable["subdistrict"]);
	            } 
	            else if (gblMyProfileAddressFlag == "zipcode") {
	                    setDropdowninEngEditContacts(resulttable["zipcode"]);
	            }
            else {
                
            }
        }
    } 
    
}

function setDropdowninEngEditContacts(resultDS) {
    //frmeditMyProfile.hbox4758937266440.setEnabled(true);
    //var resultDS = resulttable["state"];
    var currentLocales = kony.i18n.getCurrentLocale();
    
      	if(gblMyProfileAddressFlag == "state") {
      	   if(gblStateValue == "")
      	   {
      	   		dismissLoadingScreen();
      	   		gblMyProfileAddressFlag = "";
      	   		frmeditContactInfo.txtAddress1.text = frmCheckContactInfo.lblContactAddr1.text;
      	   		frmeditContactInfo.show();
      	   }
      	   else
      	   {
	      	   for (var j = 0; j < resultDS.length; j++)
	      	      if(resultDS[j].ProvinceNameTH == gblStateValue)
	      	      {
	      	        gblStateEng = resultDS[j].ProvinceNameEN;
	      	        provinceCD = resultDS[j].ProvinceCD;
	      	        break;
	      	      }
	      	      //dropDowndistrictPopulate();
	      	      dropDownEditContactsdistrictPopulate();
      	   }
      	      return;
      	}
    
      	if(gblMyProfileAddressFlag == "district") {
      	   if(gbldistrictValue!="")
      	   {
      	   for (var j = 0; j < resultDS.length; j++)
      	      if(resultDS[j].DistrictNameTH == gbldistrictValue)
      	      {
      	        gblDistEng = resultDS[j].DistrictNameEN;
      	        DistrictCD = resultDS[j].DistrictCD;
      	        break;
      	      }
      	      //dropDownsubDistrictPopulate();
				dropDownEditContactssubDistrictPopulate();
      	   }
      	   else
      	   {
      	   		frmeditContactInfo.show();
      	   		gblMyProfileAddressFlag = "";
      	   }
      	   return;
      	}
    
      	if(gblMyProfileAddressFlag == "subdistrict") {
      	   if(gblsubdistrictValue!="")
      	   {
      	   for (var j = 0; j < resultDS.length; j++)
      	      if(resultDS[j].SubDistrictNameTH == gblsubdistrictValue)
      	      {
      	        gblSubDistEng = resultDS[j].SubDistrictNameEN;
      	        break;
      	      }
      	      //dropDownzipCodetPopulate();
				dropDownEditContactszipCodetPopulate();
      	   }
      	   else
      	   {
      	   		dismissLoadingScreen();
      	   		frmeditContactInfo.show();
      	   		gblMyProfileAddressFlag = "";
      	   } 
      	      return;
      	}
      	if(gblMyProfileAddressFlag == "zipcode") {
      	   if(gblzipcodeValue!="")
      	   {
	      	   for (var j = 0; j < resultDS.length; j++)
	      	   {
	      	      if(resultDS[j].ZipCode == gblzipcodeValue)
	      	      {
	      	        gblZipEng = resultDS[j].ZipCode;
	      	        break;
	      	      }
	      	    }
      	   }
      	   else
      	   {
      	   		dismissLoadingScreen();
      	   		frmeditContactInfo.show();
      	   		gblMyProfileAddressFlag = "";
      	   } 
      	      
      	}
      	if(gblMyProfileAddressFlag == "zipcode")
      	{
      	   	dismissLoadingScreen();
      	   	frmeditContactInfo.show();
      	    gblMyProfileAddressFlag = "";
      	}
      	if(currentLocales == "th_TH")
        {
          return;
        }	
    
}

// paste here
var resetState = false;
var resetdistrict = false;
var resetsubdistrict = false;




function editMyContactsProvince() {
    resetState = true;
    gblMyProfileAddressFlag = "state";
    showLoadingScreen();
    var inputParams = {};
    invokeServiceSecureAsync("MyprofileAddressProvinceJavaService", inputParams, editMyContatcsAddressServiceCallBack);
}
// To get District List for editMyProfile 

function editMyContactsDistrict() {
    resetdistrict = true;
    gblMyProfileAddressFlag = "district";
    showLoadingScreen();
    var inputParams = {};
    inputParams["provinceCD"] = gblmyProfileAddrState;
    invokeServiceSecureAsync("MyprofileAddressDistrictJavaService", inputParams, editMyContatcsAddressServiceCallBack);
}
// To get SubDistrict List for editMyProfile 

function editMyContactsSubDistrict() {
    resetsubdistrict = true;
    gblMyProfileAddressFlag = "subdistrict";
    showLoadingScreen();
    var inputParams = {};
    inputParams["districtCD"] = gblmyProfileAddrDistrict;
    invokeServiceSecureAsync("MyprofileAddressSubDistrictJavaService", inputParams, editMyContatcsAddressServiceCallBack);
}
// To get Zipcode List for editMyProfile 

function editMyContactsZipcode() {
    gblMyProfileAddressFlag = "zipcode";
    showLoadingScreen();
    var inputParams = {};
    inputParams["districtCD"] = gblmyProfileAddrDistrict;
    invokeServiceSecureAsync("MyprofileAddressZipcodeJavaService", inputParams, editMyContatcsAddressServiceCallBack);
}

function editMyContatcsAddressServiceCallBack(status, resulttable) {
    
    var locale = kony.i18n.getCurrentLocale();
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            // 
            if (gblMyProfileAddressFlag == "state") {
                if (resulttable["state"][0].length != 0) {
                	if(locale == "en_US")
                    	populateContactsAddressFieldsData(kony.table.sort(resulttable["state"],"ProvinceNameEN"));
                    else 	populateContactsAddressFieldsData(resulttable["state"]);
                }
            } else if (gblMyProfileAddressFlag == "district") {
                if (resulttable["district"][0].length != 0) {
                	if(locale == "en_US")
                    	populateContactsAddressFieldsData(kony.table.sort(resulttable["district"],"DistrictNameEN"));
                    else	populateContactsAddressFieldsData(resulttable["district"]);
                }
            } else if (gblMyProfileAddressFlag == "subdistrict") {
                if (resulttable["subdistrict"][0].length != 0) {
                	if(locale == "en_US")
                    	populateContactsAddressFieldsData(kony.table.sort(resulttable["subdistrict"],"SubDistrictNameEN"));
                    else	populateContactsAddressFieldsData(resulttable["subdistrict"]);	
                }
            } else if (gblMyProfileAddressFlag == "zipcode") {
                if (resulttable["zipcode"][0].length != 0) {
                	if(locale == "en_US")
                    	populateContactsAddressFieldsData(kony.table.sort(resulttable["zipcode"],"ZipCode"));
                    else	populateContactsAddressFieldsData(resulttable["zipcode"]);	
                }
            } else {
                alert("gblMyProfileAddressFlag is not valid");
            }
        } else {
            alert("No address fields found opstatus is not 0");
        }
    } else {
        if (status == 300) {
            alert("No address fields found ststus 300");
        }
    }
    
}

function populateContactsAddressFieldsData(resultDS) {
    var resulttableRS = [];
    //frmeditMyProfile.hbox4758937266440.setEnabled(true);
    //var resultDS = resulttable["state"];
    var currentLocales = kony.i18n.getCurrentLocale();
    
    for (var j = 0; j < resultDS.length; j++) {
        try {
            var tempRec = [];
            if (gblMyProfileAddressFlag == "state") {
                if (currentLocales == "th_TH") {
                    tempRec = {
                        lblbankListThai: resultDS[j].ProvinceNameTH,
                        lblBanklist: resultDS[j].ProvinceNameTH,
                        idhiddenAddr: resultDS[j].ProvinceCD
                    }
                } else {
                    tempRec = {
                        lblbankListThai: resultDS[j].ProvinceNameTH,
                        lblBanklist: resultDS[j].ProvinceNameEN,
                        idhiddenAddr: resultDS[j].ProvinceCD
                    }
                }
            } else if (gblMyProfileAddressFlag == "district") {
                if (currentLocales == "th_TH") {
                    tempRec = {
                        lblbankListThai: resultDS[j].DistrictNameTH,
                        lblBanklist: resultDS[j].DistrictNameTH,
                        idhiddenAddr: resultDS[j].DistrictCD
                    }
                } else {
                    tempRec = {
                        lblbankListThai: resultDS[j].DistrictNameTH,
                        lblBanklist: resultDS[j].DistrictNameEN,
                        idhiddenAddr: resultDS[j].DistrictCD
                    }
                }
            } else if (gblMyProfileAddressFlag == "subdistrict") {
                //frmeditMyProfile.hbox4758937266440.setEnabled(true);
                if (currentLocales == "th_TH") {
                    tempRec = {
                        lblbankListThai: resultDS[j].SubDistrictNameTH,
                        lblBanklist: resultDS[j].SubDistrictNameTH,
                        idhiddenAddr: resultDS[j].SubDistrictCD
                    }
                } else {
                    tempRec = {
                        lblbankListThai: resultDS[j].SubDistrictNameTH,
                        lblBanklist: resultDS[j].SubDistrictNameEN,
                        idhiddenAddr: resultDS[j].SubDistrictCD
                    }
                }
            } else if (gblMyProfileAddressFlag == "zipcode") {
                //alert("resultDS.length "+resultDS.length);
                if (resultDS.length == 1) {
                    frmeditContactInfo.lblzipcode.text = resultDS[j].ZipCode;
                    zipcodeValue = resultDS[j].ZipCode;
                    gblmyProfileAddrZipCode = resultDS[j].ZipCode;
                    frmeditContactInfo.hbox4758937266440.setEnabled(false);
                    dismissLoadingScreen();
                    return false;
                } else {
                    tempRec = {
                        lblbankListThai: resultDS[j].ZipCode,
                        lblBanklist: resultDS[j].ZipCode,
                        idhiddenAddr: resultDS[j].ZipCode
                    }
                    frmeditContactInfo.hbox4758937266440.setEnabled(true);
                }
            } else {
                
            }
            resulttableRS.push(tempRec);
        } catch (i18nError) {
            alert("Exception While getting currentLocale  : " + i18nError);
        }
    }
    // if (currentLocales == "th_TH"){ 
    popContatcsAddrCmboBox.segBanklist.removeAll();
    popContatcsAddrCmboBox.segBanklist.setData(resulttableRS);
    if(gblMyProfileAddressFlag == "state")
    	resulttableState = resultDS;
    else	if(gblMyProfileAddressFlag == "district")
    	resulttableDist = resultDS;
    else	if(gblMyProfileAddressFlag == "subdistrict")
    	resulttableSubDist = resultDS;
    else	if(gblMyProfileAddressFlag == "zipcode")
    	resulttableStateZip = resultDS;
    
    dismissLoadingScreen();
    popContatcsAddrCmboBox.show();
    //popAddrCmboBox
}





function getMBEditContactStatus() {
	gblOpenProdAddress = false;
	if(gblOpenActSavingCareEditCont == true){
		frmeditContactInfo.txtemailvalue.text=frmMBSavingsCareContactInfo.lblEmailAddr1.text;
	}else if (gblCCDBCardFlow=="DEBIT_CARD_REISSUE") {
		frmeditContactInfo.txtemailvalue.text = gblEmailAddr;
	}else{
		frmeditContactInfo.txtemailvalue.text = frmCheckContactInfo.lblEmailAddr1.text;
	}
	
	if(gblOpenActBusinessHrs == "true"){
		//lblContactAddr1
      	if(gblEmailVerifyModuleName == "openAccount" ){// This condtion is know that user wants to edit email addess by new flow
          emailVerificationMB("openAccount");
        }else if(gblEmailVerifyModuleName == "openAccountSavingCare"){
          emailVerificationMB("openAccountSavingCare");
        }else{
              if(gblCCDBCardFlow=="DEBIT_CARD_REISSUE"){
              loadSearchAddress("issueCard");
            }else{
              loadSearchAddress("openAccount");
            }
        }
      	
		//dropDownEditContactsStatePopulate();
	}else{
		showAlert(getErrorMsgForBizHrs(), kony.i18n.getLocalizedString("info"));
	}
}


function onRowSelectContactsAddrPopUp() {
	kony.print("gblMyProfileAddressFlag---->" + gblMyProfileAddressFlag);
    lblAddrField = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
    idHiddenAddrField = popContatcsAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
    lblbankListThaiField = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
    dismissLoadingScreen();
    if (gblMyProfileAddressFlag == "state") {
    	confirmEdit = true;
    	changeState = true;
        frmeditContactInfo.hbox4758937266356.setEnabled(true);
        StateValue = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
        gblmyProfileAddrState = popContatcsAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
        frmeditContactInfo.lblProvince.text = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
        if (resetState) {
            frmeditContactInfo.lbldistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
            frmeditContactInfo.lblsubdistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
            frmeditContactInfo.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
        }
    } else if (gblMyProfileAddressFlag == "district") {
    	changedist = true;
        frmeditContactInfo.hbox4758937266374.setEnabled(true);
        districtValue = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
        gblmyProfileAddrDistrict = popContatcsAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
        frmeditContactInfo.lbldistrict.text = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
        if (resetdistrict) {
            frmeditContactInfo.lblsubdistrict.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
            frmeditContactInfo.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
        }
        resetState = false;
    } else if (gblMyProfileAddressFlag == "subdistrict") {
    	changeSubDist = true;
        frmeditContactInfo.hbox4758937266440.setEnabled(true);
        subdistrictValue = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
        gblmyProfileAddrSubDistrict = popContatcsAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
        frmeditContactInfo.lblsubdistrict.text = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
        if (resetsubdistrict) {
            frmeditContactInfo.lblzipcode.text = kony.i18n.getLocalizedString('keyIBPleaseSelect');
        }
        resetdistrict = false;
    } else if (gblMyProfileAddressFlag == "zipcode") {
    	changeZip = true;
        zipcodeValue = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblbankListThai;
        gblmyProfileAddrZipCode = popContatcsAddrCmboBox.segBanklist.selectedItems[0].idhiddenAddr;
        frmeditContactInfo.lblzipcode.text = popContatcsAddrCmboBox.segBanklist.selectedItems[0].lblBanklist;
        resetsubdistrict = false
    }
    popContatcsAddrCmboBox.dismiss();
}


function snippetCodeOnClickOfAcctSegmentMB(){
	gblTransferRefNo = "";
	
	if(isCmpFlow){
		//var prodD = gblProudctDetails.ProdDetails[gblProdCode];
		if(undefined !== gblProudctDetails[gblProdCode]){
			var prodD = gblProudctDetails[gblProdCode];
			gblSelOpenActProdCode = gblProdCode;
			gblFinActivityLogOpenAct["prodNameEN"] = prodD.prodNameEN;
			gblFinActivityLogOpenAct["prodNameTH"] = prodD.prodNameTH;
			gblFinActivityLogOpenAct["accTypeValTH"] = prodD.actTypeTH;
			gblFinActivityLogOpenAct["accTypeValEN"] = prodD.actTypeEN;
			gblFinActivityLogOpenAct["prodDesc"] = prodD.prodDes;
			gblFinActivityLogOpenAct["issueCardOnline"] = prodD.issueCardOnline;
		}else{
			alert(kony.i18n.getLocalizedString("keyCampaignAlreadyHaveAcct"));
			return;
		}
			
	}else {
		gblSelOpenActProdCode = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].productcode;
		gblFinActivityLogOpenAct["prodNameEN"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenProdNameEN;
		gblFinActivityLogOpenAct["prodNameTH"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenProdNameTH;
		gblFinActivityLogOpenAct["accTypeValTH"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenActTypeTH;
		gblFinActivityLogOpenAct["accTypeValEN"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenActTypeEN;
		gblFinActivityLogOpenAct["prodDesc"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenProdDes;
		if(frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenIssueCardOnline != undefined){
			gblFinActivityLogOpenAct["issueCardOnline"] = frmOpenActSelProd["segOpenActSelProd"]["selectedItems"][0].hiddenIssueCardOnline;
		}else{
			gblFinActivityLogOpenAct["issueCardOnline"]= "";
		}
	}
	
	kony.print("gblSelOpenActProdCode----->" +gblSelOpenActProdCode);
	/*if(gblSelOpenActProdCode == "211"){
		kony.print("Inside Savings Care----->");
		gblSelProduct = "TMBSavingcare";
		showSavingCareProductBrief();
	}else{*/
		if(gblSelOpenActProdCode == "225" || gblSelOpenActProdCode == "226" ){
			gblSelProduct = "ForUse";
		}else if(gblSelOpenActProdCode == "206"){
			gblSelProduct = "TMBDreamSavings";
		}else if(gblSelOpenActProdCode == "221"){
			gblSelProduct = "ForUse";
		}else if(gblAccountTable["SAVING_CARE_PRODUCT_CODES"].indexOf(gblSelOpenActProdCode) >= 0){
			gblSelProduct = "TMBSavingcare";
			//showSavingCareProductBrief();
		}else if(gblSelOpenActProdCode == "300" || gblSelOpenActProdCode == "301"  || gblSelOpenActProdCode == "302" || gblSelOpenActProdCode == "601" || gblSelOpenActProdCode == "602" || gblSelOpenActProdCode == "659" || gblSelOpenActProdCode == "664" || gblSelOpenActProdCode == "666"){
			gblSelProduct = "ForTerm";
		} 
		kony.print("eng name" +  gblFinActivityLogOpenAct["prodNameEN"] + "eng thai" + gblFinActivityLogOpenAct["prodNameTH"] + "gblSelProduct---->" + gblSelProduct);
		//calling get interest rate function below
		frmOpenProdDetnTnCInterestRate();
		}
	//}