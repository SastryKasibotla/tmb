//Type your code here
gblbilPayRefNum = "";

function billPayvalidationforQR() {
    kony.print("Biller Validation forQrPay");
    var inputParam = {};
    //gblSelTransferMode = 2;
    showLoadingScreen();
    gblStartBPDate = currentSystemDate();
    var scheduleDtBillPay = getFormattedDate(gblStartBPDate, "en_US");
    gblQRPayData["transferAmt"] = gblqrTransferAmnt;
    inputParam["BillerCompcode"] = gblQRPayData["billerID"];
    inputParam["ReferenceNumber1"] = gblQRPayData["ref1"];
    inputParam["ReferenceNumber2"] = gblQRPayData["ref2"];
    inputParam["Amount"] = gblQRPayData["transferAmt"];
    inputParam["ScheduleDate"] = scheduleDtBillPay;
    inputParam["billerCategoryID"] = "";
    inputParam["billerMethod"] = "0";
    inputParam["ModuleName"] = "BillPay";
    invokeServiceSecureAsync("billerValidation", inputParam, callBackbillerValidation);
}

function callBackbillerValidation(status, resulttable) {
    if (status == 400) //success responce
    {
        var validationBillPayMBFlag = "";
        var isValidOnlineBiller = result["isValidOnlineBiller"];
        if (result["opstatus"] == 0) {
            validationBillPayMBFlag = result["validationResult"];
        } else {
            validationBillPayMBFlag = result["validationResult"];

        }

        if (validationBillPayMBFlag == "true") {
            callGenerateTransferRefNoserviceqrBillPay();
        } else {
            dismissLoadingScreen();
            //ENH113
            if (result["isNotAllowedTime"] == "true") {
                //alert(kony.i18n.getLocalizedString("Valid_DuplicateNickname"));
                alert(result["errmsg"]);
                return false;
            }
            //ENH113


            if (isValidOnlineBiller != undefined && isValidOnlineBiller == "true") {
                dismissLoadingScreen();
                alert(kony.i18n.getLocalizedString("keyInvalidOnlineBillerDetails"));
            } else {
                var wrongRefMsg = kony.i18n.getLocalizedString("keyWrngRef1Val");
                wrongRefMsg = wrongRefMsg.replace("{ref_label}", removeColonFromEnd(frmBillPayment.lblRef1.text + ""));
                dismissLoadingScreen();
                showAlert(wrongRefMsg, kony.i18n.getLocalizedString("info"));
            }
            frmBillPayment.lblRef1Value.setFocus(true);
            return false;
        }
    }
}

function callGenerateTransferRefNoserviceqrBillPay() {
    inputParam = {};
    inputParam["transRefType"] = "NB";
    invokeServiceSecureAsync("generateTransferRefNo", inputParam, generateTransferRefNoserviceQRBillPayCallBack);
}


function generateTransferRefNoserviceQRBillPayCallBack(status, result) {


    if (status == 400) //success responce
    {



        if (result["opstatus"] == 0) {

            gblbilPayRefNum = result["transRefNum"] + "00";
			//alert("Ref number "+result["transRefNum"]);
            //frmIBBillPaymentConfirm.lblTRNVal.text = refNum + "00";
            // gotoBillPaymentConfirmationIB();
            saveToSessionQRBillPayment();
            //frmBillPaymentConfirmationFuture.lblTxnNumValue.text = refNum;   
        } else {
            dismissLoadingScreenPopup();
            alert(" " + result["errMsg"]);
        }
    }
}


function saveToSessionQRBillPayment() {
    inputParam = {};
    var frmAcct = "";
    var fromAcctID = gblQRPayData["fromAcctID"];

    frmAcct = fromAcctID.replace(/-/g, "");
  	if(gbleDonationType == "Barcode_Type"){
      	inputParam["EDonationFlag"] = "Y";
    }else{
    	inputParam["QRFlag"] = "Y";
    }
    inputParam["toAccountNo"] = gblQRPayData["toAcctIDNoFormat"];
    inputParam["billerMethod"] = "0";
    inputParam["fromAccountNo"] = frmAcct;
    inputParam["ref1"] = gblQRPayData["ref1"];
    
    kony.print("gblqrTransferAmnt"+gblqrTransferAmnt);
    kony.print("gblQRPayData[billerID]"+gblQRPayData["billerID"]);
    kony.print("gblQRPayData[Account Name]"+gblQRPayData["toAccountName"]);
    
    inputParam["amount"] = gblqrTransferAmnt;
    inputParam["compCode"] = gblQRPayData["billerID"];
    inputParam["flex1"] = gblQRPayData["toAccountName"];
    inputParam["flex2"] = gblQRPayData["fee"]; 
    if (gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type") {
        if (gblTaxDec == "T") {
            inputParam["ref2"] = gblCustomerIDValue;
        } else {
            inputParam["ref2"] = "0";
        }
    } else {
        inputParam["ref2"] = gblQRPayData["ref2"];
    }
    inputParam["typeID"] = "027"
    inputParam["billerId"] = gblQRPayData["billerID"];

  	if(gbleDonationType == "QR_Type"){
		verifyBillPaymentEdonation(gblNumApproval);
    } else {
		invokeServiceSecureAsync("billPaymentValidationService", inputParam, saveToSessionBillPaymentcallBackQR);
    }
}

function saveToSessionBillPaymentcallBackQR(status, resulttable) {
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            //alert("save in session success");
            //showAccessPinScreenKeypad();
            verifyBillPaymentQRPay(gblNumApproval);
        }
    }
}

function verifyBillPaymentQRPay(tranPassword) {
    var inputParam = {};

    showLoadingScreen();
    inputParam["channel"] = "rc";
  	//TODO make change for e-Donation billpayment flow
    inputParam["QRFlag"] = "Y";
    inputParam["verifyPwdMB_appID"] = appConfig.appId;
    inputParam["verifyPwdMB_channel"] = "MB";
    inputParam["password"] = tranPassword;

    inputParam["verifyPwdMB_appID"] = appConfig.appId;
    inputParam["billerMethod"] = "0";

    inputParam["isPayNow"] = "true";
    inputParam["compCode"] = gblQRPayData["billerCompCode"]; //gblQRPayData["billerID"];

    inputParam["isPayPremium"] = "";
    inputParam["policyNumber"] = "";
    inputParam["NotificationAdd_source"] = "billpaymentnow";
    kony.print("$$$$ Till Here1");
    //var transferFee = frmBillPaymentConfirmationFuture.lblPaymentFeeValue.text;
    var transferAmount = gblqrTransferAmnt;

    var fromAcctID = gblQRPayData["fromAcctID"];
    var frmAcct = fromAcctID.replace(/-/g, "");
    var toAcct = gblQRPayData["toAcctIDNoFormat"];
    gblBillerMethod = 0;
    gblreccuringDisablePay = "Y";
    if (gblBillerMethod == 0) {

        var invoice;
        if (gblreccuringDisablePay == "Y") {
            invoice = gblQRPayData["ref2"]; //no ref 2 for topup?
        } else {
            invoice = ""; //workaround
        }
        inputParam["promptPayETEAdd_fromAcctNo"] = frmAcct;
        inputParam["promptPayETEAdd_toAcctNo"] = toAcct;
        inputParam["promptPayETEAdd_fromFIIdent"] = "";
		//alert("Feee value is"+gblQRPayData["fee"])
        inputParam["promptPayETEAdd_transferAmt"] = transferAmount;
        inputParam["promptPayETEAdd_feeValue"] = gblQRPayData["fee"];
        inputParam["promptPayETEAdd_referenceNum1"] = gblQRPayData["ref1"];      
        if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
		    if(gblTaxDec == "T"){
			    inputParam["promptPayETEAdd_referenceNum2"] = gblCustomerIDValue;
			}else{
			    inputParam["promptPayETEAdd_referenceNum2"] = "0";
			}
		} else {
	        inputParam["promptPayETEAdd_referenceNum2"] = gblQRPayData["ref2"];
	    }        
        inputParam["promptPayETEAdd_EPAYCode"] = "";
        inputParam["promptPayETEAdd_CompCode"] = ""; //gblQRPayData["billerID"];
        inputParam["promptPayETEAdd_billPay"] = "billPay";
        inputParam["UsageLimit"] = "0";
        inputParam["promptPayETEAdd_appID"] = appConfig.appId;
        inputParam["promptPayETEAdd_channel"] = "MB";

    }

    kony.print("$$$$ Till Here2");
  if(gblQRPayData["fee"] == undefined || gblQRPayData["fee"] == "" || gblQRPayData["fee"] == "0.00"){
    var diplayFee = "";
   }else{
    var diplayFee = gblQRPayData["fee"];
   }
  var lblRefTH1 = gblQRDefaultAccountResponse["labelRef1TH"];
      if(lblRefTH1 == "" || lblRefTH1 == undefined){
        lblRefTH1 = kony.i18n.getLocalizedString("Reference1");
      }
      var lblsearchRefTH1 = lblRefTH1.search(":");
      if(lblsearchRefTH1 == -1){
        var gblQrRef1LblTH1 = lblRefTH1+": "+gblQRPayData["ref1"];
      }else{
        var gblQrRef1LblTH1 = lblRefTH1+" "+gblQRPayData["ref1"];
      }
  
  var lblRefEN1 = gblQRDefaultAccountResponse["labelRef1EN"];
      if(lblRefEN1 == "" || lblRefEN1 == undefined){
        lblRefEN1 = kony.i18n.getLocalizedString("Reference1");
      }
      var lblsearchRefEN1 = lblRefEN1.search(":");
      if(lblsearchRefEN1 == -1){
        var gblQrRef1LblEN1 = lblRefEN1+": "+gblQRPayData["ref1"];
      }else{
        var gblQrRef1LblEN1 = lblRefEN1+" "+gblQRPayData["ref1"];
      }
  if(gblQRPayData["ref2"] == undefined || gblQRPayData["ref2"] == ""){
    var gblQrRef2Lbl2 = "";
  }else{
    var lblRefTH2 = gblQRDefaultAccountResponse["labelRef2TH"];
      if(lblRefTH2 == "" || lblRefTH2 == undefined){
        lblRefTH2 = kony.i18n.getLocalizedString("Reference2");
      }
      var lblsearchRefTH2 = lblRefTH1.search(":");
      if(lblsearchRefTH2 == -1){
        var gblQrRef1LblTH2 = lblRefTH2+": "+gblQRPayData["ref2"];
      }else{
        var gblQrRef1LblTH2 = lblRefTH2+" "+gblQRPayData["ref2"];
      }
  
  var lblRefEN2 = gblQRDefaultAccountResponse["labelRef2EN"];
      if(lblRefEN2 == "" || lblRefEN2 == undefined){
        lblRefEN2 = kony.i18n.getLocalizedString("Reference2");
      }
      var lblsearchRefEN2 = lblRefEN2.search(":");
      if(lblsearchRefEN2 == -1){
        var gblQrRef1LblEN2 = lblRefEN2+": "+gblQRPayData["ref2"];
      }else{
        var gblQrRef1LblEN2 = lblRefEN2+" "+gblQRPayData["ref2"];
      }
  }
  kony.print("diplayFee  "+diplayFee);
if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
  if(gblQrRef1LblEN2 == "" || gblQrRef1LblEN2 == undefined){
    gblQrRef1LblEN2 = "";
    gblQrRef1LblTH2 = "";
  }
}
  kony.print("gblbilPayRefNum  "+gblbilPayRefNum);
  kony.print("gblQrRef1Lbl1  "+gblQrRef1LblEN2);
  kony.print("gblQrRef2Lbl2  "+gblQrRef1LblTH2);
  kony.print("gblQrRef1Lbl1  "+gblQrRef1LblEN1);
  kony.print("gblQrRef2Lbl2  "+gblQrRef1LblTH1);
    inputParam["NotificationAdd_channelId"] = "Mobile Banking";
    inputParam["NotificationAdd_customerName"] = gblQRPayData["custAcctName"];
    inputParam["NotificationAdd_fromAcctNick"] = gblQRPayData["custName"];
    inputParam["NotificationAdd_fromAcctName"] = gblQRPayData["custAcctName"];
    inputParam["NotificationAdd_billerNick"] = "";
    inputParam["NotificationAdd_billerName"] = gblQRPayData["toAccountName"];
    inputParam["NotificationAdd_billerNameTH"] = "";
    inputParam["NotificationAdd_ref1EN"] = gblQrRef1LblEN1;
    inputParam["NotificationAdd_ref1TH"] = gblQrRef1LblTH1;
    inputParam["NotificationAdd_ref2EN"] = gblQrRef1LblEN2;
    inputParam["NotificationAdd_ref2TH"] = gblQrRef1LblTH2;
    inputParam["NotificationAdd_amount"] = transferAmount;
    inputParam["NotificationAdd_fee"] = diplayFee;
    inputParam["NotificationAdd_refID"] = gblbilPayRefNum;
    inputParam["NotificationAdd_memo"] = "";
    inputParam["NotificationAdd_deliveryMethod"] = "Email";
    inputParam["NotificationAdd_noSendInd"] = "0";
    inputParam["NotificationAdd_notificationType"] = "Email";
    inputParam["NotificationAdd_Locale"] = kony.i18n.getCurrentLocale();
    inputParam["NotificationAdd_notificationSubject"] = "billpay";
    inputParam["NotificationAdd_notificationContent"] = "billPaymentDone";
    inputParam["NotificationAdd_appID"] = appConfig.appId;
    inputParam["NotificationAdd_channel"] = "MB";

    inputParam["crmProfileMod_appID"] = appConfig.appId;
    inputParam["crmProfileMod_channel"] = "MB";

    inputParam["source"] = "billpaymentnow";

    kony.print("$$$$ Till Here3");
    inputParam["finActivityLog_billerRef1"] = gblQRPayData["ref1"];
    inputParam["finActivityLog_fromAcctName"] = gblQRPayData["custAcctName"];
    inputParam["finActivityLog_fromAcctNickname"] = gblQRPayData["custName"];
    inputParam["finActivityLog_toAcctName"] = gblQRPayData["toAccountName"];
    inputParam["finActivityLog_toAcctNickname"] = gblQRPayData["toAccountName"].substr(0,20);
    if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
	    if(gblTaxDec == "T"){
		    inputParam["finActivityLog_billerRef2"] = gblCustomerIDValue;
		}else{
		    inputParam["finActivityLog_billerRef2"] = "0";
		}
	} else {
        inputParam["finActivityLog_billerRef2"] = gblQRPayData["ref2"];
    }      
    inputParam["finActivityLog_availableBal"] = "";
    inputParam["finActivityLog_finTxnRefId"] = gblbilPayRefNum;
    inputParam["finActivityLog_appID"] = appConfig.appId;
    inputParam["finActivityLog_channel"] = "MB";
    inputParam["finActivityLog_channelId"] = "02";
    inputParam["activityLog_appID"] = appConfig.appId;
    inputParam["activityLog_channel"] = "MB";
    inputParam["activityLog_channelId"] = "02";
    kony.print("$$$$ Till Here4");
    inputParam["activityLog_channelId"] = "02";
    inputParam["activityLog_activityFlexValues1"] = "";
    inputParam["activityLog_activityFlexValues2"] = "";
    inputParam["activityLog_activityFlexValues3"] = "";
    inputParam["activityLog_activityFlexValues4"] = gblqrTransferAmnt + "-" +gblQRPayData["fee"];
    inputParam["activityLog_activityFlexValues5"] = "";
    inputParam["serviceID"] = "ConfirmBillPaymentPromptPayService";
    inputParam["finActivityLog_billerCustomerName"] = gblQRPayData["toAccountName"];
    kony.print("$$$$ Till Here5");
  	if(gbleDonationType == "QR_Type" || gbleDonationType == "Barcode_Type"){
      invokeServiceSecureAsync("ConfirmBillPaymentPromptPayService", inputParam, callBackMBCompositeBillQRPay_donation);
    }else{
      invokeServiceSecureAsync("ConfirmBillPaymentPromptPayService", inputParam, callBackMBCompositeBillQRPay);
    }
    

}


function callBackMBCompositeBillQRPay(status, resultTable) {
    try {
        if (status == 400) {
            //alert("FInal Success");
            kony.print("Resultable" + JSON.stringify(resultTable));
            kony.print("isSignedUser" + isSignedUser);
          	var availableBalance=resultTable["availBal"];
            if (resultTable["opstatus"] == 0 && typeof(resultTable["paymentServerDate"]) != "undefined") {
                if (isSignedUser) {
                  	if(isNotBlank(availableBalance)){
                       dismissLoadingScreen();
                    //alert("Inside signed user");
                    closeApprovalKeypad();
                    //Start success case
                    frmQRPaymentSuccess.imagestatusicon.src = "greencomplete.png";
                    //frmQRPaymentSuccess.LabelRefId.setVisibility(false);
                    frmQRPaymentSuccess.hbxShareOption.setVisibility(true);
                    frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("msgQRPaymentSuccess");
                    //Start success case
                    if(gblref2Val)
                    {
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(true);
                    }else{
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(false);
                    }
                    frmQRPaymentSuccess.show();
                      
                    frmQRPaymentSuccess.flexReferenceNumber.setVisibility(true);
                    frmQRPaymentSuccess.flexNumberDetails.setVisibility(false);
                    frmQRPaymentSuccess.lblRefNumber2.text =  gblQRPayData["ref2"];
                    frmQRPaymentSuccess.lblRefNumber1.text =  gblQRPayData["ref1"];
                    var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
                    frmQRPaymentSuccess.lblTransNPbAckTotVal.text = formattedAmount[0] + formattedAmount[1];
                    //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custName"]; //gblQRPayData["custAcctName"];
                    var deviceInfo = kony.os.deviceInfo();
                    var deviceModel = deviceInfo.model;
                    var frmName = gblQRPayData["custName"]; // MKI, MIB-9931
                    if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                        frmName = frmName.substring(0,11);// MKI, MIB-9931
                    }//mki, MIB-9931 ====End
                    frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                    frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                    frmQRPaymentSuccess.lblTransNPbAckToName1.text = gblQRPayData["toAccountName"];
                    var formattedAmount1 = amountFormat("0.00");
                    frmQRPaymentSuccess.lblTransNPbAckTotVal2.text = formattedAmount[0] + formattedAmount[1] + " (" + formattedAmount1[0] + formattedAmount1[1] + ")";

                    frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " (" + formattedAmount1[0] + formattedAmount1[1] + ")";
                    frmQRPaymentSuccess.lblTransNPbAckFrmNum.text = "xxx-x-" + gblQRPayData["fromAcctID"].substring(6, 11) + "-x";
                      if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                        frmQRPaymentSuccess.LabelRefId.text=kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + gblbilPayRefNum;
                          }
                    //frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                    frmQRPaymentSuccess.lblTransNPbAckDateVal.text = resultTable["paymentServerDate"];
                    frmQRPaymentSuccess.lblSmartDateVal.text = resultTable["serverDate"]; //mki, MIB-13325
                    kony.timer.schedule("billpay", function(){onclickQRPaymentSaveImage();kony.timer.cancel("billpay");}, 1, false);
                    if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                    } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                    } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                    } else if (gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
                        frmQRPaymentSuccess.flexRefNumber.setVisibility(true);
                        frmQRPaymentSuccess.flexAccountPhone.setVisibility(false);
                      	if(kony.i18n.getCurrentLocale() == "th_TH") {
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1TH"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                           frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                           frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"];//MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2TH"]!= "" && gblQRDefaultAccountResponse["labelRef2TH"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2TH"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                             frmQRPaymentSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                           }
                    	}else{
                          
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1EN"];
                          kony.print("Inside english   "+lblRef1);
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          kony.print("Inside english   "+lblRef1);
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2EN"]!= "" && gblQRDefaultAccountResponse["labelRef2EN"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2EN"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                          }
                       }
                        if(gblref2Val)
                        {
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                        }else{
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                        } 
                    }
                    dismissLoadingScreen();
                    }else{
                       dismissLoadingScreen();
                    //alert("Inside signed user");
                    closeApprovalKeypad();
                    //Start success case
                    frmQRPaymentSuccess.imagestatusicon.src = "iconnotcomplete.png";
                    //frmQRPaymentSuccess.LabelRefId.setVisibility(false);
                    frmQRPaymentSuccess.hbxShareOption.setVisibility(false);
                    frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("keyServiceUnavailable");
                    //Start success case
                    if(gblref2Val)
                    {
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(true);
                    }else{
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(false);
                    }
                    frmQRPaymentSuccess.show();
                    frmQRPaymentSuccess.flexReferenceNumber.setVisibility(true);
                    frmQRPaymentSuccess.flexNumberDetails.setVisibility(false);
                    frmQRPaymentSuccess.lblRefNumber2.text =  gblQRPayData["ref2"];
                    frmQRPaymentSuccess.lblRefNumber1.text =  gblQRPayData["ref1"];
                    var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
                    frmQRPaymentSuccess.lblTransNPbAckTotVal.text = formattedAmount[0] + formattedAmount[1];
                    //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custName"]; //gblQRPayData["custAcctName"];
                    var deviceInfo = kony.os.deviceInfo();
                    var deviceModel = deviceInfo.model;
                    var frmName = gblQRPayData["custName"]; // MKI, MIB-9931
                    if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                        frmName = frmName.substring(0,11);// MKI, MIB-9931
                    }//mki, MIB-9931 ====End
                    frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                    frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                    frmQRPaymentSuccess.lblTransNPbAckToName1.text = gblQRPayData["toAccountName"];
                    var formattedAmount1 = amountFormat("0.00");
                    frmQRPaymentSuccess.lblTransNPbAckTotVal2.text = formattedAmount[0] + formattedAmount[1] + " (" + formattedAmount1[0] + formattedAmount1[1] + ")";

                    frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " (" + formattedAmount1[0] + formattedAmount1[1] + ")";
                    frmQRPaymentSuccess.lblTransNPbAckFrmNum.text = "xxx-x-" + gblQRPayData["fromAcctID"].substring(6, 11) + "-x";
                      if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                        frmQRPaymentSuccess.LabelRefId.text=kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + gblbilPayRefNum;
                          }
                    //frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                    frmQRPaymentSuccess.lblTransNPbAckDateVal.text = resultTable["paymentServerDate"];
                    frmQRPaymentSuccess.lblSmartDateVal.text = resultTable["serverDate"];
                    if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                    } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                    } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                    } else if (gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
                        frmQRPaymentSuccess.flexRefNumber.setVisibility(true);
                        frmQRPaymentSuccess.flexAccountPhone.setVisibility(false);
                      	if(kony.i18n.getCurrentLocale() == "th_TH") {
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1TH"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                           frmQRSelectAccount.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                           frmQRSelectAccount.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2TH"]!= "" && gblQRDefaultAccountResponse["labelRef2TH"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2TH"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                             frmQRSelectAccount.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                           }
                    	}else{
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1EN"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2EN"]!= "" && gblQRDefaultAccountResponse["labelRef2EN"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2EN"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                          }
                       }
                        if(gblref2Val){
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                        }else{
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                        }
                    }
                    dismissLoadingScreen();
                    }
                   
                } else {
					if(isNotBlank(availableBalance)){
                      //alert("Inside Unsigned");
                    closeApprovalKeypad();
                    if(gblref2Val)
                    {
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(true);
                    }else{
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(false);
                    }
                    frmQRPaymentSuccess.show();
                      
                    frmQRPaymentSuccess.flexReferenceNumber.setVisibility(true);
                    frmQRPaymentSuccess.flexNumberDetails.setVisibility(false);
                    frmQRPaymentSuccess.lblRefNumber2.text =  gblQRPayData["ref2"];
                    frmQRPaymentSuccess.lblRefNumber1.text =  gblQRPayData["ref1"];
                    //Start success case
                    frmQRPaymentSuccess.imagestatusicon.src = "greencomplete.png";
                    //frmQRPaymentSuccess.LabelRefId.setVisibility(false);
                    frmQRPaymentSuccess.hbxShareOption.setVisibility(true);
                    frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("msgQRPaymentSuccess");
                    //Start success case
                    var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
                    frmQRPaymentSuccess.lblTransNPbAckTotVal.text = formattedAmount[0] + formattedAmount[1];
                    //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custName"]; //gblQRPayData["custAcctName"];
                    var deviceInfo = kony.os.deviceInfo();
                    var deviceModel = deviceInfo.model;
                    var frmName = gblQRPayData["custName"]; // MKI, MIB-9931
                    if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                        frmName = frmName.substring(0,11);// MKI, MIB-9931
                    }//mki, MIB-9931 ====End
                    frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                    frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                    frmQRPaymentSuccess.lblTransNPbAckToName1.text = gblQRPayData["toAccountName"];
                    var formattedAmount1 = amountFormat("0.00");
                    frmQRPaymentSuccess.lblTransNPbAckTotVal2.text = formattedAmount[0] + formattedAmount[1] + " (" + formattedAmount1[0] + formattedAmount1[1] + ")";

                    frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " (" + formattedAmount1[0] + formattedAmount1[1] + ")";
                    frmQRPaymentSuccess.lblTransNPbAckFrmNum.text = "xxx-x-" + gblQRPayData["fromAcctID"].substring(6, 11) + "-x";
                      if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                        	frmQRPaymentSuccess.LabelRefId.text=kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + gblbilPayRefNum;
                          }
                    //frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                    frmQRPaymentSuccess.lblTransNPbAckDateVal.text = resultTable["paymentServerDate"];
                    frmQRPaymentSuccess.lblSmartDateVal.text = resultTable["serverDate"];
                    //loadvaluesforScreenshotqr(status, resultTable);
                    kony.timer.schedule("billpay", function(){onclickQRPaymentSaveImage();kony.timer.cancel("billpay");}, 1, false);
                    if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                    } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                    } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                    } else if (gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
                        frmQRPaymentSuccess.flexRefNumber.setVisibility(true);
                        frmQRPaymentSuccess.flexAccountPhone.setVisibility(false);
                        if(kony.i18n.getCurrentLocale() == "th_TH") {
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1TH"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                           frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                           frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2TH"]!= "" && gblQRDefaultAccountResponse["labelRef2TH"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2TH"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                             frmQRPaymentSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                           }
                    	}else{
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1EN"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2EN"]!= "" && gblQRDefaultAccountResponse["labelRef2EN"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2EN"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                          }
                       }
                        if(gblref2Val)
                        {
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                        }else{
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                        }
                    }
                    dismissLoadingScreen();
                    }else{
                      //alert("Inside Unsigned");
                    closeApprovalKeypad();
                    if(gblref2Val)
                    {
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(true);
                    }else{
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(false);
                    }
                    frmQRPaymentSuccess.show();
                    frmQRPaymentSuccess.flexReferenceNumber.setVisibility(true);
                    frmQRPaymentSuccess.flexNumberDetails.setVisibility(false);
                    frmQRPaymentSuccess.lblRefNumber2.text =  gblQRPayData["ref2"];
                    frmQRPaymentSuccess.lblRefNumber1.text =  gblQRPayData["ref1"];
                    //Start success case
                    frmQRPaymentSuccess.imagestatusicon.src = "iconnotcomplete.png";
                    //frmQRPaymentSuccess.LabelRefId.setVisibility(false);
                    frmQRPaymentSuccess.hbxShareOption.setVisibility(false);
                    frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("keyServiceUnavailable");
                    //Start success case
                    var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
                    frmQRPaymentSuccess.lblTransNPbAckTotVal.text = formattedAmount[0] + formattedAmount[1];
                    //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custName"]; //gblQRPayData["custAcctName"];
                    var deviceInfo = kony.os.deviceInfo();
                    var deviceModel = deviceInfo.model;
                    var frmName = gblQRPayData["custName"]; // MKI, MIB-9931
                    if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                        frmName = frmName.substring(0,11);// MKI, MIB-9931
                    }//mki, MIB-9931 ====End
                    frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                    frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                    frmQRPaymentSuccess.lblTransNPbAckToName1.text = gblQRPayData["toAccountName"];
                    var formattedAmount1 = amountFormat("0.00");
                    frmQRPaymentSuccess.lblTransNPbAckTotVal2.text = formattedAmount[0] + formattedAmount[1] + " (" + formattedAmount1[0] + formattedAmount1[1] + ")";

                    frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " (" + formattedAmount1[0] + formattedAmount1[1] + ")";
                    frmQRPaymentSuccess.lblTransNPbAckFrmNum.text = "xxx-x-" + gblQRPayData["fromAcctID"].substring(6, 11) + "-x";
                      if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                        frmQRPaymentSuccess.LabelRefId.text=kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + gblbilPayRefNum;
                          }
                    //frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                    frmQRPaymentSuccess.lblTransNPbAckDateVal.text = resultTable["paymentServerDate"];
                    frmQRPaymentSuccess.lblSmartDateVal.text = resultTable["serverDate"];
                    //loadvaluesforScreenshotqr(status, resultTable);
                    if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                    } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                    } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                    } else if (gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
                        frmQRPaymentSuccess.flexRefNumber.setVisibility(true);
                        frmQRPaymentSuccess.flexAccountPhone.setVisibility(false);
                        if(kony.i18n.getCurrentLocale() == "th_TH") {
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1TH"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                           frmQRSelectAccount.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                           frmQRSelectAccount.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2TH"]!= "" && gblQRDefaultAccountResponse["labelRef2TH"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2TH"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                             frmQRSelectAccount.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                           }
                    	}else{
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1EN"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2EN"]!= "" && gblQRDefaultAccountResponse["labelRef2EN"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2EN"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                          }
                       }
                        if(gblref2Val)
                        {
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                        }else{
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                        }
                    }
                    dismissLoadingScreen();
                    }
                    
                }

            } else if(resultTable["opstatus"] == -1){
                    dismissLoadingScreen();
                    closeApprovalKeypad();
                    frmQRPaymentSuccess.imagestatusicon.src = "iconnotcomplete.png";
                    frmQRPaymentSuccess.hbxShareOption.setVisibility(false);
					if(resultTable["errMsg"] == "Insufficient funds"){
						frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("keyInSuffBalance");
					}else{
						frmQRPaymentSuccess.LabelSuccessDesc.text = kony.i18n.getLocalizedString("keyServiceUnavailable");
					}
                    //Start success case
                    if(gblref2Val)
                    {
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(true);
                    }else{
                      frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                      frmQRPaymentSuccess.lblRefNumber2.setVisibility(false);
                    }
                    frmQRPaymentSuccess.show();
                    frmQRPaymentSuccess.flexReferenceNumber.setVisibility(true);
                    frmQRPaymentSuccess.flexNumberDetails.setVisibility(false);
                    frmQRPaymentSuccess.lblRefNumber2.text =  gblQRPayData["ref2"];
                    frmQRPaymentSuccess.lblRefNumber1.text =  gblQRPayData["ref1"];
                    var formattedAmount = amountFormat(gblQRPayData["transferAmt"]);
                    frmQRPaymentSuccess.lblTransNPbAckTotVal.text = formattedAmount[0] + formattedAmount[1];
                    //frmQRPaymentSuccess.lblTransNPbAckFrmName.text = gblQRPayData["custName"]; //gblQRPayData["custAcctName"];
                    var deviceInfo = kony.os.deviceInfo();
                    var deviceModel = deviceInfo.model;
                    var frmName = gblQRPayData["custName"]; // MKI, MIB-9931
                    if (deviceModel == "iPhone 5S" || deviceModel == "iPhone 5"|| deviceModel == "iPhone SE"|| deviceModel == "iPhone 5C") {
                        frmName = frmName.substring(0,11);// MKI, MIB-9931
                    }//mki, MIB-9931 ====End
                    frmQRPaymentSuccess.lblTransNPbAckFrmName.text = frmName;
                    frmQRPaymentSuccess.lblTransNPbAckFrmCustomerName.text = gblQRPayData["accountName"];
                    frmQRPaymentSuccess.lblTransNPbAckToName1.text = gblQRPayData["toAccountName"];
                    var formattedAmount1 = amountFormat("0.00");
                    frmQRPaymentSuccess.lblTransNPbAckTotVal2.text = formattedAmount[0] + formattedAmount[1] + " (" + formattedAmount1[0] + formattedAmount1[1] + ")";

                    frmQRPaymentSuccess.lblTransNPbAckTotFeeVal.text = " (" + formattedAmount1[0] + formattedAmount1[1] + ")";
                    frmQRPaymentSuccess.lblTransNPbAckFrmNum.text = "xxx-x-" + gblQRPayData["fromAcctID"].substring(6, 11) + "-x";
                      if(gblQRPayData["QR_TransType"]=="QR_Bill_Payment"){
                            frmQRPaymentSuccess.lblTransNPbAckRefDes.text=gblbilPayRefNum;
                        frmQRPaymentSuccess.LabelRefId.text=kony.i18n.getLocalizedString("TRConfirm_RefNo") + ": " + gblbilPayRefNum;
                          }
                    //frmQRPaymentSuccess.lblTransNPbAckRefDes.text=resultTable["Transfer"][0]["refId"];
                    frmQRPaymentSuccess.lblTransNPbAckDateVal.text = resultTable["paymentServerDate"];
                    frmQRPaymentSuccess.lblSmartDateVal.text = resultTable["serverDate"];
                    if (gblQRPayData["mobileNo"] != undefined && gblQRPayData["mobileNo"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskMobileNumber(gblQRPayData["mobileNo"]);
                    } else if (gblQRPayData["cId"] != undefined && gblQRPayData["cId"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskCitizenID(gblQRPayData["cId"]);
                    } else if (gblQRPayData["eWallet"] != undefined && gblQRPayData["eWallet"] != "") {
                        frmQRPaymentSuccess.lblMobileNumber.text = maskeWalletID(gblQRPayData["eWallet"]);
                    } else if (gblQRPayData["billerID"] != undefined && gblQRPayData["billerID"] != "") {
                        frmQRPaymentSuccess.flexRefNumber.setVisibility(true);
                        frmQRPaymentSuccess.flexAccountPhone.setVisibility(false);
                      	if(kony.i18n.getCurrentLocale() == "th_TH") {
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1TH"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                           frmQRSelectAccount.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                           frmQRSelectAccount.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2TH"]!= "" && gblQRDefaultAccountResponse["labelRef2TH"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2TH"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRSelectAccount.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2TH"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                             frmQRSelectAccount.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                           }
                    	}else{
                          var lblRef1 = gblQRDefaultAccountResponse["labelRef1EN"];
                          if(lblRef1 == "" || lblRef1 == undefined){
                            lblRef1 = kony.i18n.getLocalizedString("Reference1");
                          }
                          var lblSearchRef1 = lblRef1.search(":");
                          if(lblSearchRef1 == -1){
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + ": " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum1.text = lblRef1 + " " + gblQRPayData["ref1"]; //MKI, MIB-10538
                          }
                          if(gblQRDefaultAccountResponse["labelRef2EN"]!= "" && gblQRDefaultAccountResponse["labelRef2EN"]!= undefined){
                            var lblRef2 = gblQRDefaultAccountResponse["labelRef2EN"];
                            lblRef2 = lblRef2.search(":");
                            if(lblRef2 == -1){
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+": "+gblQRPayData["ref2"];
                            }else{
                              frmQRPaymentSuccess.lblReferenceNum2.text =gblQRDefaultAccountResponse["labelRef2EN"]+" "+gblQRPayData["ref2"];
                            }
                          }else{
                            frmQRPaymentSuccess.lblReferenceNum2.text = kony.i18n.getLocalizedString("Reference2")+": "+gblQRPayData["ref2"];
                          }
                       }
                        if(gblref2Val){
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(true);
                        }else{
                          frmQRPaymentSuccess.lblReferenceNum2.setVisibility(false);
                        }
                    }
                    dismissLoadingScreen();
            }else{
              kony.print("Inside else error");
              var currentForm = kony.application.getCurrentForm();
              kony.print("currentForm = "+currentForm.id);
                if(resultTable["errCode"] == "VrfyAcPWDErr00001" || resultTable["errCode"] == "VrfyAcPWDErr00002"){
                  resetKeypadApproval();
                  dismissLoadingScreen();
                  var badLoginCount = resultTable["badLoginCount"];
                  var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
                  incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttemptsTransfers - badLoginCount); 
				  if(currentForm == frmQRSelectAccount){
                    frmQRSelectAccount.lblForgotPin.text = incorrectPinText; //kony.i18n.getLocalizedString("invalidCurrPIN"); //MKI, MIB-9892
                    frmQRSelectAccount.lblForgotPin.skin = "lblBlackMed150NewRed";
                    frmQRSelectAccount.lblForgotPin.onTouchEnd = doNothing;
                  }else if(currentForm == frmQRSuccess){
                    frmQRSuccess.lblForgotPin.text = incorrectPinText; //kony.i18n.getLocalizedString("invalidCurrPIN"); //MKI, MIB-9892
                    frmQRSuccess.lblForgotPin.skin = "lblBlackMed150NewRed";
                    frmQRSuccess.lblForgotPin.onTouchEnd = doNothing;
                  }
                } else if (resultTable["errCode"] == "VrfyAcPWDErr00003") {
                  closeApprovalKeypad();
                  //showTranPwdLockedPopup();
                  gotoUVPINLockedScreenPopUp();
                  return false;
                } else {
                  dismissLoadingScreen();
                  closeApprovalKeypad();
                  var errorText = kony.i18n.getLocalizedString("ECGenOTPRtyErr00001");
                  frmQRSuccess.lblerrormsg1.text = errorText;
                  frmQRSuccess.flxErrorBody.height = "94%"
                  frmQRSuccess.flxErrorBody.setVisibility(true);
                  frmQRSuccess.flxPayFooter.setVisibility(false);
                  frmQRSuccess.flexChooseAccntFooter.setVisibility(false);
                  frmQRSuccess.flxLoginFooter.setVisibility(false);
                  return;
                }   
            }

        }
    } catch (ex) {
        kony.print("callBackMBCompositeBillQRPay ex=" + ex.message);
    }
}

function promptpayETEInquiryModule() {
  	//alert("in ete inq module");
    inputParam = {};
    var inputParam = {};
    var fromData = "";
    var i = gbltranFromSelIndex[1];
    fromData = frmQRSelectAccount.segTransFrm.data;
    var enteredAmount = gblQRPayData["transferAmt"];
    var frmID = fromData[i].lblActNoval;
    var prodCode = fromData[i].prodCode;
    var AccntType = fromData[i].accType;
    var fromAcctID = kony.string.replace(frmID, "-", "");
    var toAcctID = "";
    inputParam["compCode"] = "";
    inputParam["toAcctNo"] = gblQRPayData["billerID"];
    inputParam["fromAcctNo"] = fromAcctID;
    inputParam["transferAmt"] = enteredAmount;
    inputParam["tranCode"] = "";
    inputParam["fromAcctTypeValue"] = AccntType;
    inputParam["ref1"] = gblQRPayData["ref1"];
    inputParam["ref2"] = gblQRPayData["ref2"];
    inputParam["ref3"] = gblQRPayData["ref3"];
    inputParam["ref4"] = "";

    inputParam["postedDate"] = getFormattedDate(currentSystemDate(), kony.i18n.getCurrentLocale());
    inputParam["ePayCode"] = "";
    //inputParam["compCode"] = gblCompCode;
    inputParam["waiveCode"] = "I";
    inputParam["fIIdent"] = "11";
    inputParam["mobileOrCI"] = "05";

    invokeServiceSecureAsync("promptPayInq", inputParam, callBackCheckETEPromptPayinqServiceQRBillPay);
    //invokeServiceSecureAsync("billPaymentInquiry", inputParam, BillPaymentInquiryServiceCallBackIB);
}


function callBackCheckETEPromptPayinqServiceQRBillPay(status, resulttable) {
  try{
    var ToAccountName = "";
    if (status == 400) {
        if (resulttable["opstatus"] == 0) {
            if (kony.application.getCurrentForm().id != "frmQRSelectAccount") {
                frmQRSelectAccount.show();
            }
            if (resulttable["WaiveFlag"] == "Y") {
                FeeAmnt = "0.00";
            } else {
                FeeAmnt = resulttable["FeeAmt"];
            }
            frmQRSelectAccount.LabelFee.text = kony.i18n.getLocalizedString("keyFee") + FeeAmnt + " " + kony.i18n.getLocalizedString("keyBaht");
            //checkCrmProfileInqForQRPay();

            dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            if (isNotBlank(resulttable["errMsg"])) {
                var errCode = resulttable["errMsg"];
                var errorText = "";
                
                frmQRSelectAccount.HBoxError.setVisibility(true);
                frmQRSelectAccount.LabelCaution.text = errCode;
                frmQRSelectAccount.LabelFee.setVisibility(false);
                frmQRSelectAccount.ButtonPayNow.setVisibility(false);
                
            } else {
                var errorText = kony.i18n.getLocalizedString("msgErrRecipientNotRegisterdPromptpay");
                frmQRSelectAccount.HBoxError.setVisibility(true);
                frmQRSelectAccount.LabelCaution.text = errorText;
                frmQRSelectAccount.LabelFee.setVisibility(false);
                frmQRSelectAccount.ButtonPayNow.setVisibility(false);
            }
        }

	}
  } catch (ex) {
        kony.print("callBackCheckETEPromptPayinqServiceQRBillPay ex=" + ex.message);
    }

}