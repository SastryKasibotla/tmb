function frmMBSetEmailInit() {
    frmMBSetEmail.preShow = frmMBSetEmailPreshow;
    //frmMBSetEmail.btnNext.onClick = isEmailVerifiedinECAS;
    frmMBSetEmail.btnNext.onClick = CheckEmailStatusMB;
    frmMBSetEmail.txtEmail.onDone = isValidEmailCheck;
    frmMBSetEmail.txtEmail.onEndEditing = isValidEmailCheck;
    frmMBSetEmail.postShow = frmMBSetEmailPosshow;
    frmMBSetEmail.onDeviceBack = doNothing;

}

// This method is to validate whether the email id format is correct or not and apply skin
function isValidEmailCheck() {
    var isValidEmail = validateEmail(frmMBSetEmail.txtEmail.text);
    if (isValidEmail) {
        frmMBSetEmail.lblInvalidEmail.isVisible = false;
        //frmMBSetEmail.btnNext.onClick  = isEmailVerifiedinECAS;
        frmMBSetEmail.btnNext.onClick = CheckEmailStatusMB;
        frmMBSetEmail.flexEmail.skin = "lFboxLoan";
        frmMBSetEmail.btnNext.skin = "btnBlueBGNoRound";
        //frmMBSetEmail.txtEmail.setFocus(false);
    } else {
        frmMBSetEmail.lblInvalidEmail.isVisible = true;
        frmMBSetEmail.btnNext.onClick = doNothing;
        frmMBSetEmail.flexEmail.skin = "flexRedBorder1px";
        frmMBSetEmail.btnNext.skin = "blueLightBlueDisbaled";
        //frmMBSetEmail.txtEmail.setFocus(false);
        //frmMBSetEmail.flxHeader.setFocus(true);
    }
}

function frmMBVerifyEmailInit() {
    try {
        frmMBVerifyEmail.preShow = frmMBVerifyEmailPreshow;
        frmMBVerifyEmail.postShow = frmMBVerifyEmailPostshow;
        frmMBVerifyEmail.btnChangeEmail.onClick = showSetEmailForm;
        frmMBVerifyEmail.txtPin1.onTextChange = enterOTPEmailVerify;
        frmMBVerifyEmail.txtPin2.onTextChange = enterOTPEmailVerify;
        frmMBVerifyEmail.txtPin3.onTextChange = enterOTPEmailVerify;
        frmMBVerifyEmail.txtPin4.onTextChange = enterOTPEmailVerify;
        frmMBVerifyEmail.flexResend.onClick = onClickResendEmail;
        frmMBVerifyEmail.lblResentOption.skin = lblbluemedium160;
        frmMBVerifyEmail.btnBack.onClick = goToSetEmailScreen;
        frmMBVerifyEmail.onDeviceBack = doNothing;
    } catch (e) {
        kony.print("exception-->" + e)
    }
}

function goToSetEmailScreen() {
    frmMBSetEmail.show();
}

function frmMBVerifyEmailPostshow() {
    // addAccessPinKeypad(frmMBVerifyEmail);
}

function frmMBVerifyEmailPreshow() {
    gblEmailIncorrectOTP = 0;
    frmMBVerifyEmail.lblEmail.text = frmMBSetEmail.txtEmail.text;
    frmMBVerifyEmail.lblHeader.text = kony.i18n.getLocalizedString("VRF_txtTitle");
    frmMBVerifyEmail.lblResentOption.text = kony.i18n.getLocalizedString("VRF_linkResend");
    frmMBVerifyEmail.lblResend.text = kony.i18n.getLocalizedString("VRF_txtDesc2");
    frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
    //frmMBVerifyEmail.lblResendTime.isVisible = false;
    frmMBVerifyEmail.btnChangeEmail.text = kony.i18n.getLocalizedString("VRF_btnChange");
    if (gblEmailVerifyModuleName == "activationMB") {
        frmMBVerifyEmail.btnBack.isVisible = false;
    } else {
        frmMBVerifyEmail.btnBack.isVisible = true;
    }
    resetEmailOTPPins();
}

function resetEmailOTPPins() {
    frmMBVerifyEmail.txtPin1.text = "";
    frmMBVerifyEmail.txtPin2.text = "";
    frmMBVerifyEmail.txtPin3.text = "";
    frmMBVerifyEmail.txtPin4.text = "";
    frmMBVerifyEmail.flexPin1.skin = "lFboxLoan";
    frmMBVerifyEmail.flexPin2.skin = "lFboxLoan";
    frmMBVerifyEmail.flexPin3.skin = "lFboxLoan";
    frmMBVerifyEmail.flexPin4.skin = "lFboxLoan";
    frmMBVerifyEmail.lblInvalidPin.isVisible = false;
}

function frmMBSetEmailPosshow() {
    addAccessPinKeypad(frmMBSetEmail);
}

function frmMBSetEmailPreshow() {
    gblResendOTPOptionValue = 0;
    frmMBSetEmail.lblInvalidEmail.isVisible = false;
    //frmMBSetEmail.btnNext.onClick  = isEmailVerifiedinECAS;
    frmMBSetEmail.btnNext.onClick = CheckEmailStatusMB;
    //frmMBSetEmail.btnNext.skin = "txtBlueFontNoBorder";
    frmMBSetEmail.flexEmail.skin = "lFboxLoan";
    frmMBSetEmail.btnNext.skin = "blueLightBlueDisbaled";
    frmMBSetEmail.btnNext.onClick = doNothing;
    if (frmMBSetEmail.txtEmail.text !== "" && frmMBSetEmail.txtEmail.text != null) {
        isValidEmailCheck();
    }
    frmMBSetEmail.txtEmail.placeholder = kony.i18n.getLocalizedString("SET_phdExample");
    frmMBSetEmail.btnNext.text = kony.i18n.getLocalizedString("SET_btnNext");
    //frmMBSetEmail.lblEmailDrescription.text=kony.i18n.getLocalizedString("SET_txtDesc");
    //frmMBSetEmail.lblHeader.text=kony.i18n.getLocalizedString("SET_txtTitle");
    frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark")
    frmMBVerifyEmail.flexResend.isVisible = true;
    frmMBVerifyEmail.flexResend.onClick = onClickResendEmail;
    frmMBVerifyEmail.lblResentOption.skin = lblbluemedium160;
    //frmMBSetEmail.txtEmail.text=kony.i18n.getLocalizedString("SET_phdExample");
}

function onClickResendEmail() {
    var inputParam = {};
    var locale = kony.i18n.getCurrentLocale();
    inputParam["ChannelId"] = "OTP_EMAIL";
    inputParam["toEmail"] = frmMBSetEmail.txtEmail.text;
    inputParam["retryCounterRequestOTP"] = gblResendOTPOptionValue;
    invokeServiceSecureAsync("RequestOTPEmail", inputParam, startCountingEMAILOTPTimer);
}

function startCountingEMAILOTPTimer(status, result) {
    try {
        dismissLoadingScreen()
        if (status == 400) {
            if (result["opstatus"] == "0") {
                showAlert(kony.i18n.getLocalizedString("VRF_msgSentCode").replace("{email}", frmMBVerifyEmail.lblEmail.text), kony.i18n.getLocalizedString("info"))
                frmMBVerifyEmail.flexResend.onClick = doNothing;
                frmMBVerifyEmail.lblResendTime.isVisible = true;
                frmMBVerifyEmail.lblResentOption.skin = lblLightGrey160;
                frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
                if (gblResendOTPOptionValue < 3) {
                    gblResendOTPOptionValue++;
                    var timer = kony.timer.schedule("emailOTPimer", setTimetoEmailCountDownText, 1, true);
                } else {
                    frmMBVerifyEmail.lblResendTime.isVisible = true;
                    frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgNotReceived");
                    frmMBVerifyEmail.flexResend.isVisible = false;
                    frmMBVerifyEmail.flexResend.onClick = doNothing;
                }
                frmMBVerifyEmail.lblEmailDrescription.text = kony.i18n.getLocalizedString("VRF_txtDesc1").replace("emailRefNo", result['bankRefNo']);
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            }
        }
    } catch (e) {
        kony.print("Exception in email verification" + e);
    }
}

function setTimetoEmailCountDownText() {
    if (gblEmailOTPTime > 0) {
        frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark").replace("60", gblEmailOTPTime);
        gblEmailOTPTime--;
    } else {
        kony.timer.cancel("emailOTPimer");
        gblEmailOTPTime = 59;
        frmMBVerifyEmail.lblResendTime.isVisible = false;
        frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
        frmMBVerifyEmail.flexResend.onClick = onClickResendEmail;
        frmMBVerifyEmail.lblResentOption.skin = lblbluemedium160;
        if (gblResendOTPOptionValue >= 3) {
            frmMBVerifyEmail.lblResendTime.isVisible = true;
            frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgNotReceived");
            frmMBVerifyEmail.flexResend.onClick = doNothing;
            frmMBVerifyEmail.lblResentOption.skin = lblLightGrey160;
        }
    }
}

function isEmailVerifiedinECAS() {
    try {
        var prevForm = kony.application.getPreviousForm().id;
        kony.print("prevForm in isEmailVerifiedinECAS---->" + prevForm);
        var isValidEmail = validateEmail(frmMBSetEmail.txtEmail.text);
        if (!isValidEmail) {
            isValidEmailCheck();
            return false;
        }
        if (gblisEmailVerifiedinECAS && (frmMBSetEmail.lbOriginalEmail.text == frmMBSetEmail.txtEmail.text)) {
            if (gblEmailVerifyModuleName == "activationMB") {
                mbActivationEmailDeviceNameValidatn();
            } else if (gblEmailVerifyModuleName == "myProfile") {
                menuMyProfileonClick();
            } else if (gblEmailVerifyModuleName == "openAccount") {
                frmCheckContactInfo.show();
            } else if (gblEmailVerifyModuleName == "openAccountSavingCare") {
                frmMBSavingsCareContactInfo.show();
            } else if (gblEmailVerifyModuleName == "loans") {
                frmLoanPersonalInfo.show();
            } else if (gblEmailVerifyModuleName == "eStatement") {
                frmMBEStatementLanding.show();
            }
            //frmMBVerifyEmail.show();
        } else {
            if (gblEmailVerifyModuleName == "activationMB" || gblEmailVerifyModuleName == "eKYC") { // these flows dont require to have access pin
                showEmailVerifyPage();
            } else if (gblEmailVerifyModuleName == "myProfile" || gblEmailVerifyModuleName == "openAccountSavingCare" || gblEmailVerifyModuleName == "openAccount" || gblEmailVerifyModuleName == "loans" || gblEmailVerifyModuleName == "eStatement") {
                showAccessPinScreenKeypad();
            }
        }

    } catch (e) {
        kony.print("Exception in email verification" + e);
    }
}

function showEmailVerifyPage() {
    var inputParam = {};
    var locale = kony.i18n.getCurrentLocale();
    inputParam["ChannelId"] = "OTP_EMAIL";
    inputParam["toEmail"] = frmMBSetEmail.txtEmail.text;
    inputParam["retryCounterRequestOTP"] = gblResendOTPOptionValue;
    invokeServiceSecureAsync("RequestOTPEmail", inputParam, RequestOTPEmailVerificationCallBack);
}


function RequestOTPEmailVerificationCallBack(status, result) {
    dismissLoadingScreen()
    if (status == 400) {
        if (result["opstatus"] == "0") {
            frmMBVerifyEmail.lblEmailDrescription.text = kony.i18n.getLocalizedString("VRF_txtDesc1").replace("emailRefNo", result['bankRefNo']);
            frmMBVerifyEmail.flexResend.onClick = doNothing;
            frmMBVerifyEmail.lblResendTime.isVisible = true;
            frmMBVerifyEmail.lblResentOption.skin = lblLightGrey160;
            frmMBVerifyEmail.lblResendTime.text = kony.i18n.getLocalizedString("VRF_msgRemark");
            // frmMBVerifyEmail.lblResendTime.isVisible = true;
            frmMBVerifyEmail.show();
            var timer = kony.timer.schedule("emailOTPimer", setTimetoEmailCountDownText, 1, true);
        } else {
            showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
        }
    }
}

function showSetEmailForm() {
    frmMBSetEmail.show();
    frmMBSetEmail.txtEmail.text = "";
    frmMBSetEmail.txtEmail.setFocus(true)
}

function enterOTPEmailVerify(eventObject) {
    //var id = eventObject["id"];
    frmMBVerifyEmail.flexPin1.skin = "lFboxLoan";
    frmMBVerifyEmail.flexPin2.skin = "lFboxLoan";
    frmMBVerifyEmail.flexPin3.skin = "lFboxLoan";
    frmMBVerifyEmail.flexPin4.skin = "lFboxLoan";
    frmMBVerifyEmail.lblInvalidPin.isVisible = false;
    if (frmMBVerifyEmail.txtPin1.text.length > 0 && frmMBVerifyEmail.txtPin2.text.length > 0 && frmMBVerifyEmail.txtPin3.text.length > 0 && frmMBVerifyEmail.txtPin4.text.length > 0) {
        validateEmailOTP();
        if (eventObject["id"] == "txtPin4") {
            frmMBVerifyEmail.txtPin4.setFocus(true);
        } else if (eventObject["id"] == "txtPin3") {
            frmMBVerifyEmail.txtPin3.setFocus(true);
        } else if (eventObject["id"] == "txtPin2") {
            frmMBVerifyEmail.txtPin2.setFocus(true);
        } else if (eventObject["id"] == "txtPin1") {
            frmMBVerifyEmail.txtPin1.setFocus(true);
        }
        return false;
        //break;
    }
    if (eventObject["id"] == "txtPin1") {
        if (frmMBVerifyEmail.txtPin1.text.length > 0) {
            frmMBVerifyEmail.txtPin2.setFocus(true);
        }
    } else if (eventObject["id"] == "txtPin2") {
        if (frmMBVerifyEmail.txtPin2.text.length > 0) {
            frmMBVerifyEmail.txtPin3.setFocus(true);
        } else {
            frmMBVerifyEmail.txtPin1.setFocus(true);
        }
    } else if (eventObject["id"] == "txtPin3") {
        if (frmMBVerifyEmail.txtPin3.text.length > 0) {
            frmMBVerifyEmail.txtPin4.setFocus(true);
        } else {
            frmMBVerifyEmail.txtPin2.setFocus(true);
        }
    } else if (eventObject["id"] == "txtPin4") {
        if (frmMBVerifyEmail.txtPin3.text.length > 0) {
            //frmMBVerifyEmail.txtPin4.setFocus(true);
        } else {
            frmMBVerifyEmail.txtPin3.setFocus(true);
        }
    }
}

function validateEmailOTP() {
    showLoadingScreen();
    var inputParam = {};
    var enteredOTP = frmMBVerifyEmail.txtPin1.text + frmMBVerifyEmail.txtPin2.text + frmMBVerifyEmail.txtPin3.text + frmMBVerifyEmail.txtPin4.text
    var locale = kony.i18n.getCurrentLocale();
    inputParam["otp"] = enteredOTP;
    inputParam["serviceID"] = "verifyOTPEmail";
    inputParam["retryCounterVerifyOTP"] = gblResendOTPOptionValue;
    invokeServiceSecureAsync("verifyOTPEmail", inputParam, validateEmailOTPCallback);
    //alert("length is 4")
}


function validateEmailOTPCallback(status, result) {
    dismissLoadingScreen()
    if (status == 400) {
        if (result["opstatus"] == "0") {
            gblEmailAddr = frmMBVerifyEmail.lblEmail.text;
            updateEmailMB(frmMBVerifyEmail.lblEmail.text)
            if (gblEmailVerifyModuleName == "activationMB") {
                mbActivationEmailDeviceNameValidatn();
            }
        } else {
            enterWrongEmailOTP();
        }
    }

}

function enterWrongEmailOTP() {
    gblEmailIncorrectOTP++;
    if (gblEmailIncorrectOTP >= 3) {
        frmMBVerifyEmail.txtPin4.setFocus(false);
        showAlertWithCallBack(kony.i18n.getLocalizedString("VRF_msgIncorrectCode"), kony.i18n.getLocalizedString("info"), goToSetEmailScreen);
        return false;
    }
    frmMBVerifyEmail.txtPin4.text = "";
    frmMBVerifyEmail.txtPin2.text = "";
    frmMBVerifyEmail.txtPin3.text = "";
    frmMBVerifyEmail.txtPin1.text = "";
    frmMBVerifyEmail.flexPin1.skin = "flexRedBorder1px";
    frmMBVerifyEmail.flexPin2.skin = "flexRedBorder1px";
    frmMBVerifyEmail.flexPin3.skin = "flexRedBorder1px";
    frmMBVerifyEmail.flexPin4.skin = "flexRedBorder1px";
    frmMBVerifyEmail.lblInvalidPin.isVisible = true;
    frmMBVerifyEmail.txtPin1.setFocus(true);
}


function emailVerificationMB(module) {
    try {
        gblEmailVerifyModuleName = "";
        frmMBSetEmail.lblHeader.text = kony.i18n.getLocalizedString("CHM_txtTitle");
        frmMBSetEmail.lblEmailDrescription.text = kony.i18n.getLocalizedString("CHM_txtDesc");
        frmMBSetEmail.btnBack.isVisible = true;
        frmMBSetEmail.btnBack.onClick = onClickBackEmailVerifyMB;
        if (module == "myProfile") {
            gblEmailVerifyModuleName = "myProfile";
            frmMBSetEmail.txtEmail.text = frmMyProfiles.lblEmailVal.text;
            frmMBSetEmail.lbOriginalEmail.text = frmMyProfiles.lblEmailVal.text;

        } else if (module == "loans") {
            gblEmailVerifyModuleName = "loans";
            frmMBSetEmail.txtEmail.text = frmLoanPersonalInfo.lblEmailWithData.text;
            frmMBSetEmail.lbOriginalEmail.text = frmLoanPersonalInfo.lblEmailWithData.text;
        } else if (module == "openAccount") {
            gblEmailVerifyModuleName = "openAccount";
            frmMBSetEmail.txtEmail.text = frmCheckContactInfo.lblEmailAddr1.text;
            frmMBSetEmail.lbOriginalEmail.text = frmCheckContactInfo.lblEmailAddr1.text;
        } else if (module == "openAccountSavingCare") {
            gblEmailVerifyModuleName = "openAccountSavingCare";
            frmMBSetEmail.txtEmail.text = frmMBSavingsCareContactInfo.lblEmailAddr1.text;
            frmMBSetEmail.lbOriginalEmail.text = frmMBSavingsCareContactInfo.lblEmailAddr1.text;
        } else if (module == "activationMB") {
            gblEmailVerifyModuleName = "activationMB";
            frmMBSetEmail.btnBack.isVisible = false;
            frmMBSetEmail.lblHeader.text = kony.i18n.getLocalizedString("SET_txtTitle");
            frmMBSetEmail.lblEmailDrescription.text = kony.i18n.getLocalizedString("SET_txtDesc");
            //frmMBSetEmail.show();
        } else if (module == "eKYC") {
            gblEmailVerifyModuleName = "eKYC";
            frmMBSetEmail.btnBack.isVisible = false;
            //frmMBSetEmail.show();
        } else if (module == "eStatement") {
            gblEmailVerifyModuleName = "eStatement";
        }
        frmMBSetEmail.show();
    } catch (e) {
        kony.print("Exception in email verification" + e);
    }
}


function onClickBackEmailVerifyMB() {
    if (gblEmailVerifyModuleName == "myProfile") {
        frmMyProfiles.show()
    } else if (gblEmailVerifyModuleName == "openAccountSavingCare") {
        frmMBSavingsCareContactInfo.show();
    } else if (gblEmailVerifyModuleName == "openAccount") {
        frmCheckContactInfo.show();
    } else if (gblEmailVerifyModuleName == "loans") {
        frmLoanPersonalInfo.show();
    } else if (gblEmailVerifyModuleName == "eStatement") {
        frmMBEStatementLanding.show();
    }
    gblEmailVerifyModuleName = "";
}

function verifyAccessForEmailChange(pin) {
    showLoadingScreen();
    var inputParam = {};
    inputParam["emailId"] = frmMBSetEmail.txtEmail.text;
    inputParam["actionId"] = "verifyAccessPIN";
    inputParam["password"] = encryptData(pin);
    inputParam["moduleName"] = getModuleNameEMailVerity();
    invokeServiceSecureAsync("emailVerification", inputParam, emailVerifyAccesspinVerification);
    //showEmailVerifyPage();		
}

function getModuleNameEMailVerity() {
    var moduleName = ""
    if (gblEmailVerifyModuleName == "myProfile") {
        moduleName = "myProfile"
    } else if (gblEmailVerifyModuleName == "openAccountSavingCare") {
        moduleName = "openAccountSavingCare"
    } else if (gblEmailVerifyModuleName == "openAccount") {
        moduleName = "openAccount"
    } else if (gblEmailVerifyModuleName == "loans") {
        moduleName = "loans"
    } else if (gblEmailVerifyModuleName == "activationMB") {
        moduleName = "activationMB"
    } else if (gblEmailVerifyModuleName == "eKYC") {
        moduleName = "eKYC"
    } else if (gblEmailVerifyModuleName == "eStatement") {
        moduleName = "eStatement"
    }
    return moduleName;
}

function updateEmailMB(emailID) {
    showLoadingScreen();
    var inputParam = {};
    inputParam["actionId"] = "updateEmail";
    inputParam["moduleName"] = getModuleNameEMailVerity();
    inputParam["emailId"] = emailID;
    invokeServiceSecureAsync("emailVerification", inputParam, updateEmailMBCallback);
}

function CheckEmailStatusMB() {
    showLoadingScreen();
    var inputParam = {};
    inputParam["actionId"] = "getEmailStatus";
    inputParam["moduleName"] = getModuleNameEMailVerity();
    inputParam["emailId"] = frmMBSetEmail.txtEmail.text;
    invokeServiceSecureAsync("emailVerification", inputParam, CheckEmailStatusMBCallback);
}

function emailVerificationService(inputParam, callback) {
    invokeServiceSecureAsync("emailVerification", inputParam, callback);
}

function emailVerifyAccesspinVerification(status, result) {
    try {
        dismissLoadingScreen()
        if (status == 400) {
            if (result["opstatus"] == "0") {
                closeApprovalKeypad();
                showEmailVerifyPage();
            } else if (result["opstatus"] === 1 || result["opstatus"] == "1") {
                if (result["errCode"] == "E10020" || result["errMsg"] == "Wrong Password") {
                    //Wrong password Entered 
                    resetKeypadApproval();
                    var badLoginCount = result["badLoginCount"];
                    var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
                    incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
                    frmMBSetEmail.lblForgotPin.text = incorrectPinText;
                    frmMBSetEmail.lblForgotPin.skin = "lblBlackMed150NewRed";
                    frmMBSetEmail.lblForgotPin.onTouchEnd = doNothing;
                    dismissLoadingScreen();
                } else if (result["errCode"] == "E10403" || result["errMsg"] == "Password Locked") {
                    closeApprovalKeypad();
                    dismissLoadingScreen();
                    gotoUVPINLockedScreenPopUp();
                } else {
                    dismissLoadingScreen();
                    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                    return false;
                }
            } else {
                dismissLoadingScreen();
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
                return false;
            }
        }
    } catch (e) {
        kony.print("Exception in email verification" + e);
    }
}

function updateEmailMBCallback(status, result) {
    try {
        dismissLoadingScreen()
        if (status == 400) {
            if (result["opstatus"] == "0") {
                if (gblEmailVerifyModuleName == "activationMB") {
                    return false;
                } else if (gblEmailVerifyModuleName == "myProfile") {
                    resetEmailOTPPins();
                    menuMyProfileonClick();
                } else if (gblEmailVerifyModuleName == "openAccountSavingCare") {
                    frmMBSavingsCareContactInfo.lblEmailAddr1.text = frmMBVerifyEmail.lblEmail.text
                    frmMBSavingsCareContactInfo.show();
                    gblEmailAddr = frmMBVerifyEmail.lblEmail.text;
                    gblEmailAddrOld = frmMBSetEmail.lbOriginalEmail.text;
                } else if (gblEmailVerifyModuleName == "openAccount") {
                    //  frmCheckContactInfo.lblEmailAddr1.text = frmMBVerifyEmail.lblEmail.text
                    //  frmCheckContactInfo.show();
                    //  gblEmailAddr = frmMBVerifyEmail.lblEmail.text;
                    // gblEmailAddrOld = frmMBSetEmail.lbOriginalEmail.text;
                    onClickCheckContactInfo();
                } else if (gblEmailVerifyModuleName == "loans") {
                    frmLoanPersonalInfo.lblEmailWithData.text = frmMBVerifyEmail.lblEmail.text
                    frmLoanPersonalInfo.show();
                    gblEmailAddr = frmMBVerifyEmail.lblEmail.text;
                    gblEmailAddrOld = frmMBSetEmail.lbOriginalEmail.text;
                } else if (gblEmailVerifyModuleName == "eStatement") {
                    onClickNextTnCEStatement();
                } else if (gblEmailVerifyModuleName == "eKYC") {
                	navigatefrmMBeKYCContactInfo();
            	}
                gblEmailVerifyModuleName = "";
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            }
        }
    } catch (e) {
        kony.print("Exception in email verification" + e);
    }
}

function CheckEmailStatusMBCallback(status, result) {
    try {
        dismissLoadingScreen()
        if (status == 400) {
            if (result["opstatus"] == "0") {
                if (result["verifyEmailStatus"] == "Y") {
                    gblisEmailVerifiedinECAS = true;
                } else {
                    gblisEmailVerifiedinECAS = false;
                }
                isEmailVerifiedinECAS();
            } else {
                showAlert(kony.i18n.getLocalizedString("ECGenOTPRtyErr00001"), kony.i18n.getLocalizedString("info"));
            }
        }
    } catch (e) {
        kony.print("Exception in email verification" + e);
    }

}