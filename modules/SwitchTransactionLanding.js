

function frmMFSwitchLandingMBInitiatization(){
  frmMFSwitchLanding.preShow = frmMFSwitchLandingPreShow; 
  frmMFSwitchLanding.postShow = frmMFSwitchLandingPostShow; 
  frmMFSwitchLanding.btnSelectFundSource.onClick = onClickSelectFundSource;
  frmMFSwitchLanding.btnFundDetails.onClick = onClickSelectFundSource;
  frmMFSwitchLanding.btnSelectTargetFund.onClick = onClickSelectTargetFund;
  frmMFSwitchLanding.btnAll.onClick = onClickSwitchbtnAll;
  frmMFSwitchLanding.btnUnits.onClick = onClickSwitchbtnUnits;
  frmMFSwitchLanding.btnBaht.onClick = onClickSwitchbtnBaht;
  frmMFSwitchLanding.btnCancel.onClick = onClickSwitchBack;
  frmMFSwitchLanding.btnConfirm.onClick = checkValidSwitchAmountMB;
  frmMFSwitchLanding.btnBack.onClick = onClickSwitchBack;
  frmMFSwitchLanding.ButtonClose.onClick = onClickClose;
  frmMFSwitchLanding.txtSwitchBalance.onTextChange = frmMFSwitchLandingOnTextChangeAmount;
  frmMFSwitchLanding.SegmentFundList.onRowClick = onRowClickOfSegmentFundList;
   frmMFSwitchLanding.SegmentTargetFundList.onRowClick = onRowClickOfSegmentTargetFundList;
  frmMFSwitchLanding.txtSwitchBalance.onDone = frmMFSwitchLandingOnDoneEditingAmount;
 


}

function frmMFSwitchLandingPreShow(){

  var locale = kony.i18n.getCurrentLocale();
  var prevForm = kony.application.getPreviousForm();
  var investmentValue = "";
  var unitF = 0.0;
  var ordUnitF = 0.0;
  var redeemableUnitF = 0.0;
  var fundHouseCode = "";
  var randomnum = 0;
  var logo = "";
  
  
    frmMFSwitchLanding.lblSwitchHeader.text = kony.i18n.getLocalizedString("MF_SW_Title");
    frmMFSwitchLanding.lblSelectFundSource.text = kony.i18n.getLocalizedString("MF_SW_Source_Fund");
    frmMFSwitchLanding.LabelSelectFund.text = kony.i18n.getLocalizedString("MF_SW_Select_Source");
    frmMFSwitchLanding.lblFundSourceTxt.text = kony.i18n.getLocalizedString("MF_SW_02-FundSource");
    frmMFSwitchLanding.lblSelectTargetFund.text = kony.i18n.getLocalizedString("MF_SW_02-Target");
    frmMFSwitchLanding.lblTargetFund.text = kony.i18n.getLocalizedString("MF_SW_03-TargetFund");
    frmMFSwitchLanding.lblSwitchMethodtext.text = kony.i18n.getLocalizedString("MF_SW_03-SwitcMethod");
    frmMFSwitchLanding.btnAll.text = kony.i18n.getLocalizedString("MF_SW_03-All");
    frmMFSwitchLanding.btnUnits.text = kony.i18n.getLocalizedString("MF_SW_03-Units");
    frmMFSwitchLanding.btnBaht.text = kony.i18n.getLocalizedString("MF_SW_03-Baht");
  
  frmMFSwitchLanding.btnCancel.text = kony.i18n.getLocalizedString("MF_SW_03-Cancel");
  frmMFSwitchLanding.btnConfirm.text = kony.i18n.getLocalizedString("MF_SW_03-Next");
  
  frmMFSwitchLanding.flxFundList.left = "100%";
 
  
   kony.print("MF_FUNDDETAIL_FLOW "+ MF_FUNDDETAIL_FLOW + "MF_FUNDSUMMARY_FLOW " +MF_FUNDSUMMARY_FLOW + "orderFlow  "+ gblMFOrder["orderFlow"])
  if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW) {

    frmMFSwitchLanding.flxSelectFundSource.isVisible = false;
    frmMFSwitchLanding.flxSelectTargetFund.isVisible = true;
    frmMFSwitchLanding.lblSelectTargetFund.isVisible = true;
    frmMFSwitchLanding.FlexFundDetails.isVisible = true;
    frmMFSwitchLanding.lblBalance.text = "Balance " + frmMFFullStatementNFundSummary.lblUnitval.text + " units";
    frmMFSwitchLanding.flxTargetFund.isVisible = false;
    frmMFSwitchLanding.lblSwitchMethodtext.isVisible = false;
    frmMFSwitchLanding.flxSwitchMain.isVisible = false;
    frmMFSwitchLanding.lbltransactionDate.isVisible = false;
    frmMFSwitchLanding.flexFooter.isVisible = false;

  }else if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){

    frmMFSwitchLanding.flxSelectFundSource.isVisible = true;
    frmMFSwitchLanding.FlexFundDetails.isVisible = false;
    frmMFSwitchLanding.flxSelectTargetFund.isVisible = false;
    frmMFSwitchLanding.flxTargetFund.isVisible = false;
    frmMFSwitchLanding.lblSelectTargetFund.isVisible = false;
    frmMFSwitchLanding.lblSwitchMethodtext.isVisible = false;
    frmMFSwitchLanding.flxSwitchMain.isVisible = false;
    frmMFSwitchLanding.lbltransactionDate.isVisible = false;
    frmMFSwitchLanding.flexFooter.isVisible = false;

  }
  

 

}

function frmMFSwitchLandingPostShow(){

 

}


function onClickSwitchCancel(){
  
  if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    MBcallMutualFundsSummary();
  } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
    kony.print("mki gblMFOrder blank 4");
    gblMFOrder = {};
    frmMFFullStatementNFundSummary.show();
  }
  
}
function frmMFSwitchLandingOnTextChangeAmount(){
  var enteredAmount = frmMFSwitchLanding.txtSwitchBalance.text;
  if(isNotBlank(enteredAmount)) {
    enteredAmount = kony.string.replace(enteredAmount, ",", "");
    if(!(isNotBlank(enteredAmount) && enteredAmount.length > 0 && parseInt(enteredAmount, 10) == 0)){
      frmMFSwitchLanding.txtSwitchBalance.text = commaFormattedTransfer(enteredAmount);
    }
  }
}

function frmMFSwitchLandingOnDoneEditingAmount(){
  if(gblMFOrder["switchUnit"] == ORD_UNIT_BAHT){
    if (kony.string.containsChars(frmMFSwitchLanding.txtSwitchBalance.text, ["."]))
      frmMFSwitchLanding.txtSwitchBalance.text = frmMFSwitchLanding.txtSwitchBalance.text;
    else
      frmMFSwitchLanding.txtSwitchBalance.text = frmMFSwitchLanding.txtSwitchBalance.text + ".00";
    frmMFSwitchLanding.txtSwitchBalance.text = onDoneEditingAmountValue(frmMFSwitchLanding.txtSwitchBalance.text);
  } else if(gblMFOrder["switchUnit"] == ORD_UNIT_UNIT){
    if (kony.string.containsChars(frmMFSwitchLanding.txtSwitchBalance.text, ["."]))
      frmMFSwitchLanding.txtSwitchBalance.text = frmMFSwitchLanding.txtSwitchBalance.text;
    else
      frmMFSwitchLanding.txtSwitchBalance.text = frmMFSwitchLanding.txtSwitchBalance.text + ".0000";
    frmMFSwitchLanding.txtSwitchBalance.text = onDoneEditingUnitValue(frmMFSwitchLanding.txtSwitchBalance.text);
  }

}

function onClickSwitchbtnAll(){

  gblMFOrder["redeemUnit"] = ORD_UNIT_ALL;
  gblMFSwitchOrder["redeemUnit"] = ORD_UNIT_ALL;
  
  frmMFSwitchLanding.txtSwitchBalance.text = commaFormatted(gblMFSwitchOrder["redeemableUnit"]);
  frmMFSwitchLanding.txtSwitchBalance.setEnabled(false);
  frmMFSwitchLanding.btnAll.skin = "btnBlueBGMF";
  frmMFSwitchLanding.btnUnits.skin = "btnWhiteBGMF";
  frmMFSwitchLanding.btnBaht.skin = "btnWhiteBGMF";

}

function onClickSwitchbtnBaht(){

  gblMFOrder["redeemUnit"] = ORD_UNIT_BAHT;
  gblMFSwitchOrder["redeemUnit"] = ORD_UNIT_BAHT;
  frmMFSwitchLanding.txtSwitchBalance.setEnabled(true);
  frmMFSwitchLanding.txtSwitchBalance.placeholder = "0.00";
  frmMFSwitchLanding.txtSwitchBalance.text = "";
  frmMFSwitchLanding.txtSwitchBalance.setFocus(true);

  frmMFSwitchLanding.btnAll.skin = "btnWhiteBGMF";
  frmMFSwitchLanding.btnUnits.skin = "btnWhiteBGMF";
  frmMFSwitchLanding.btnBaht.skin = "btnBlueBGMF";

}

function onClickSwitchbtnUnits(){

  gblMFOrder["redeemUnit"] = ORD_UNIT_UNIT;
  gblMFSwitchOrder["redeemUnit"] = ORD_UNIT_UNIT;
  frmMFSwitchLanding.txtSwitchBalance.setEnabled(true);
  frmMFSwitchLanding.txtSwitchBalance.placeholder = "0.0000";
  frmMFSwitchLanding.txtSwitchBalance.text = "";
  frmMFSwitchLanding.txtSwitchBalance.setFocus(true);

  frmMFSwitchLanding.btnAll.skin = "btnWhiteBGMF";
  frmMFSwitchLanding.btnUnits.skin = "btnBlueBGMF";
  frmMFSwitchLanding.btnBaht.skin = "btnWhiteBGMF";


}


function onClickClose(){

  leftAnimation("flxFundList", "100%", 0.3, 0);

}

function onClickSelectFundSource(){
 frmMFSwitchLanding.LabelSelectFund.text = kony.i18n.getLocalizedString("MF_SW_Select_Source");
  kony.print(" Inside onClickSelectFundSource ");
  gblMFOrder = {};
  gblFundRulesData = {};
  gblMFEventFlag = MF_EVENT_SWO;
  gblMFOrder["orderType"] = MF_ORD_TYPE_SWITCH; 
  gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
  MBcallGetFundList();

 // leftAnimation("flxFundList", "0%", 0.3, 0);

}

function onClickSelectTargetFund(){
  frmMFSwitchLanding.LabelSelectFund.text = kony.i18n.getLocalizedString("MF_SW_02-SelectTarget");
  leftAnimation("flxFundList", "0%", 0.3, 0);

}

function onClickFundDetails(){

  leftAnimation("flxFundList", "0%", 0.3, 0);

}

function onRowClickOfSegmentFundList(){

//   kony.print("selectedItems  ",frmMFSwitchLanding.SegmentFundList.selectedItems[0]);
//   var segData = frmMFSwitchLanding.SegmentFundList.selectedItems[0];
//     gblMFSwitchOrder["sourceUnitHolderNo"] = segData["LabelUnitHolderNo"];
//   	gblMFSwitchOrder["sourceFundCode"] = segData["FundCode"];
//   	gblMFSwitchOrder["sourceFundClassCode"] = segData["LabelFundClaseCode"];
//   	gblMFSwitchOrder["sourceFundHouseCode"] = segData["LabelFundHouseCode"];
  
//     frmMFSwitchLanding.flxSelectFundSource.isVisible = false;
//     frmMFSwitchLanding.FlexFundDetails.isVisible = true;
//     frmMFSwitchLanding.lblPortfolioName.text = segData.LabelFundName;
//     frmMFSwitchLanding.lblPortfolioCode.text = segData.LabelUnitHolderNo;
//     frmMFSwitchLanding.flxSelectTargetFund.isVisible = true;
//     frmMFSwitchLanding.flxTargetFund.isVisible = false;
//     frmMFSwitchLanding.lblSelectTargetFund.isVisible = true;
//     frmMFSwitchLanding.lblSwitchMethodtext.isVisible = false;
//     frmMFSwitchLanding.flxSwitchMain.isVisible = false;
//     frmMFSwitchLanding.lbltransactionDate.isVisible = false;
//     frmMFSwitchLanding.flexFooter.isVisible = false;
    gblMFEventFlag = MF_EVENT_SWO;
    getSwitchFundDetailsAndTargetFunds();


}


function onRowClickOfSegmentTargetFundList(){
  
   var segData = frmMFSwitchLanding.SegmentTargetFundList.selectedItems[0];
  
    kony.print("selectedItems  ",frmMFSwitchLanding.SegmentTargetFundList.selectedItems[0]);
    gblMFSwitchOrder["targetFundCode"] = segData["fundCode"];
  	gblMFSwitchOrder["targetFundHouseCode"] = segData["fundHouseCode"];
    gblMFSwitchOrder["fxRisk"] = segData["fxRisk"];
    gblMFSwitchOrder["riskRate"] = segData["riskRate"];
  
    frmMFSwitchLanding.lblSelectTargetFund.isVisible = false;
    frmMFSwitchLanding.lblTargetFundType.text = segData.LabelUnitHolderNo;
    frmMFSwitchLanding.flxTargetFund.isVisible = true;
    frmMFSwitchLanding.lblSwitchMethodtext.isVisible = true;
    frmMFSwitchLanding.flxSwitchMain.isVisible = true;
    frmMFSwitchLanding.lbltransactionDate.isVisible = true;
    frmMFSwitchLanding.flexFooter.isVisible = true;
  

   
    leftAnimation("flxFundList", "100%", 0.3, 0);
  
}

function leftAnimation(widgetId, left, duration, delay) {

  kony.print("Inside Left Animation");

  var currentForm = kony.application.getCurrentForm();
  if (null == currentForm || undefined === currentForm) {
    return;
  }




  if (!isBlankOrNull(currentForm[widgetId])) {
    currentForm[widgetId].animate(
      kony.ui.createAnimation({
        100: {
          left: left
        }
      }),{
        duration: "0.8",
        delay: delay,
        fillMode: kony.anim.FILL_MODE_FORWARDS
      },{
        animationEnd: function() {
          kony.print("func end ");


        }
      }
    )
  }


}


function isBlankOrNull(argValue) {
  var bresult = false;
  if (argValue == undefined || argValue == "" || argValue == null || argValue == "null" || argValue == "nil" || kony.string.trim(argValue) == "" || argValue == [] || argValue == {} || argValue == "[]" || argValue == "{}" || argValue == false || argValue == "false") {
    bresult = true;
  }
  return bresult;
}

var gblMFSwitchData= {};
var gblMFSwitchOrder = {};

function getSwitchFundDetailsAndTargetFunds(){
  var inputParam = {};
  inputParam["channel"] = "rc";
  var currFrm = kony.application.getCurrentForm();
  var unitHolderNuber = "";
  var fundcode = "";
  var fundClassCode = "";
  var fundHouseCode = "";
  kony.print("gblMBMFDetailsResulttable is "+ JSON.stringify(gblMBMFDetailsResulttable));
 
 
 
  if(currFrm.id == "frmMFSwitchLanding"){
    unitHolderNuber = frmMFSwitchLanding.SegmentFundList.selectedRowItems[0].LabelUnitHolderNo;
    fundcode = frmMFSwitchLanding.SegmentFundList.selectedRowItems[0].FundCode;
    fundClassCode = frmMFSwitchLanding.SegmentFundList.selectedRowItems[0].LabelFundClaseCode;
    fundHouseCode = frmMFSwitchLanding.SegmentFundList.selectedRowItems[0].LabelFundHouseCode;
  }else{
     kony.print("frmMFSummary selected row data is ", JSON.stringify(frmMFSummary.segAccountDetails.selectedItems[0]));
    unitHolderNuber = gblMBMFDetailsResulttable["UnitHolderNo"].replace(/-/gi, "");
    fundHouseCode = gblMBMFDetailsResulttable["FundHouseCode"];
    fundcode = frmMFSummary.segAccountDetails.selectedItems[0].fundCode;
    fundClassCode = frmMFSummary.segAccountDetails.selectedItems[0].fundClass;
  }
 
 
  /// MFUHAccountDetailInq Service
  if(unitHolderNuber == null || unitHolderNuber == undefined) {
    return;
  }
 
  gblUnitHolderNumber = unitHolderNuber;
  gblFundCode = fundcode;
 
  var inputParam = {};
  inputParam["unitHolderNo"] = unitHolderNuber;
  inputParam["funCode"] = fundcode;
  inputParam["fundClassCode"] = fundClassCode;
  inputParam["serviceType"] = "1";
 
  // MFSwitchFundValidation Service
  inputParam["unitHolderNo"] = unitHolderNuber;
  inputParam["orderType"] = gblMFEventFlag; //MF_EVENT_SWITCH 3
  inputParam["fundHouseCode"] = fundHouseCode;
  inputParam["fundCode"] = fundcode;
  inputParam["orderDate"] = "";
  inputParam["fromAccountNo"] = "";
  inputParam["redeemType"] = "";
  inputParam["amount"] = "";
  inputParam["fundShortName"] = "";
  inputParam["TCConfirm"] = "";
  inputParam["cardRefId"] = "";
 
  // GetMFFundFamily
  inputParam["fundCodeSWO"] = fundcode;
 
  showLoadingScreen();
  invokeServiceSecureAsync("validateSourceAndGetTargetFunds", inputParam, getSwitchFundDetailsAndTargetFundsServiceCallBack)
}


function getSwitchFundDetailsAndTargetFundsServiceCallBack(status, resulttable){
  if (status == 400) {
    dismissLoadingScreen();

    var currFrm = kony.application.getCurrentForm();
    kony.print("in getSwitchFundDetailsAndTargetFundsServiceCallBack resulttable ", JSON.stringify(resulttable))
    if (resulttable["opstatus"] == 0) {

      if(resulttable["opstatus_GetMFFundFamily"] != 0  || resulttable["opstatus_MFUHAccountDetailInq"] != 0 || resulttable["opstatus_MFSwitchFundValidation"] != 0){

        return;

      }
      var unitF = 0.0;
      var ordUnitF = 0.0;
      var redeemableUnitF = 0.0;
      var targetFundData = [];
      gblMBMFDetailsResulttable = resulttable;
      gblFundRulesData = resulttable;
      gblMFSwitchData = resulttable;
      
//        if(gblMFSwitchData["effectiveDate"] != undefined &&    gblMFSwitchData["transactionDate"] != undefined) {
//         effectiveDate = gblMFSwitchData["effectiveDate"].substring(0,10);
//         transactionDate = gblMFSwitchData["transactionDate"].substring(0,10);
//       }
      

//       if(gblMFSwitchData["allowTransFlag"] == '3'){ //MKI, MIB-12551
//         showAlert(kony.i18n.getLocalizedString("MF_ERR_Overcutofftime"), kony.i18n.getLocalizedString("info")); //MKI, MIB-12551
  	    
//       }else if(gblMFSwitchData["allowTransFlag"] != '0'){
//         showAlert(kony.i18n.getLocalizedString("MF_ERR_cutofftime"), kony.i18n.getLocalizedString("info"));
//       } else if(gblMFEventFlag == MF_EVENT_REDEEM && 
//                 (gblMFSwitchData["allotType"] == '2' || gblMFSwitchData["allotType"] == '3')){
//         showAlert(kony.i18n.getLocalizedString("MF_ERR_notallow_order"), kony.i18n.getLocalizedString("info"));
//       } else if(gblMFSwitchData["fundCode"] == "") {
//         showAlert("Cannot check fund suitability", kony.i18n.getLocalizedString("info"));
//       } else if(effectiveDate != transactionDate){ 
//         popupConfirmMutualFundOrder(callBackOnConfirmOverTimeOrder);
//       }
      
      
     
    kony.print("in getSwitchFundDetailsAndTargetFundsServiceCallBack transactionDate ", gblMFSwitchData["transactionDate"])
   kony.print("in getSwitchFundDetailsAndTargetFundsServiceCallBack allowTransFlag ", gblMFSwitchData["allowTransFlag"])
      // cut of time
      frmMFSwitchLanding.lblCutOffTime.text = kony.i18n.getLocalizedString("MF_SW_02-Cutoff") + " "+ gblMFSwitchData["timeNotAllow"].substring(0,5) //+ ":00";
      
      // transaction dates
      frmMFSwitchLanding.lbltransactionDate.text =  kony.i18n.getLocalizedString("MF_SW_03-TranDate") + " "+ gblMFSwitchData["transactionDate"].substring(0,10);
      // avaliable units to switch
      gblMFSwitchOrder["InvestmentValue"] = gblMFSwitchData["InvestmentValue"];
      gblMFSwitchOrder["Nav"] = gblMFSwitchData["Nav"];
      unitF = parseFloat(numberFormat(gblMFSwitchData["Unit"], 4));
      if(gblMFSwitchData["totalRedeemInProcess"] == undefined){
        ordUnitF = 0.00;
      } else {
        ordUnitF = numberFormat(gblMFSwitchData["totalRedeemInProcess"], 4);
      }

      redeemableUnitF = unitF - ordUnitF;
      gblMFSwitchOrder["redeemableUnit"] = numberFormat(redeemableUnitF, 4);
      frmMFSwitchLanding.lblBalance.text = kony.i18n.getLocalizedString("Balance") + " " +commaFormatted(numberFormat(unitF, 4)) + " " + kony.i18n.getLocalizedString("MF_lbl_Units");
      if(gblMFSwitchOrder["redeemUnit"] == undefined || gblMFSwitchOrder["redeemUnit"] == ORD_UNIT_ALL){
        onClickSwitchbtnAll();
        frmMFSwitchLanding.txtSwitchBalance.text = commaFormatted(numberFormat(redeemableUnitF, 4));//MKI,MIB-12562
      } else {
        if(gblMFSwitchOrder["redeemUnit"] == ORD_UNIT_UNIT){
          onClickSwitchbtnUnits();
          frmMFSwitchLanding.txtSwitchBalance.text = onDoneEditingUnitValue(gblMFSwitchOrder["amount"]); 
        } else if(gblMFSwitchOrder["redeemUnit"] == ORD_UNIT_BAHT){
          onClickSwitchbtnBaht();
          frmMFSwitchLanding.txtSwitchBalance.text = onDoneEditingAmountValue(gblMFSwitchOrder["amount"]); 
        }
      }

      
      if(currFrm.id != "frmMFSwitchLanding"){
        gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;
        frmMFSwitchLanding.show();
       
      }else if(currFrm.id == "frmMFSwitchLanding"){
        
        kony.print("selectedItems  ",frmMFSwitchLanding.SegmentFundList.selectedItems[0]);
        var segData = frmMFSwitchLanding.SegmentFundList.selectedItems[0];
          gblMFSwitchOrder["sourceUnitHolderNo"] = segData["LabelUnitHolderNo"];
          gblMFSwitchOrder["sourceFundCode"] = segData["FundCode"];
          gblMFSwitchOrder["sourceFundClassCode"] = segData["LabelFundClaseCode"];
          gblMFSwitchOrder["sourceFundHouseCode"] = segData["LabelFundHouseCode"];

          frmMFSwitchLanding.flxSelectFundSource.isVisible = false;
          frmMFSwitchLanding.FlexFundDetails.isVisible = true;
          frmMFSwitchLanding.lblPortfolioName.text = segData.LabelFundName;
          frmMFSwitchLanding.lblPortfolioCode.text = segData.LabelUnitHolderNo;
          frmMFSwitchLanding.flxSelectTargetFund.isVisible = true;
          frmMFSwitchLanding.flxTargetFund.isVisible = false;
          frmMFSwitchLanding.lblSelectTargetFund.isVisible = true;
          frmMFSwitchLanding.lblSwitchMethodtext.isVisible = false;
          frmMFSwitchLanding.flxSwitchMain.isVisible = false;
          frmMFSwitchLanding.lbltransactionDate.isVisible = false;
          frmMFSwitchLanding.flexFooter.isVisible = false;
           gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
          leftAnimation("flxFundList", "100%", 0.3, 0);
      }
      
      for(var i = 0 ; i < gblMFSwitchData["Funds"].length ; i++){
        var tempRecord = { 
        
          "LabelUnitHolderNo": gblMFSwitchData["Funds"][i]["fundCode"],
          "fundCode": gblMFSwitchData["Funds"][i]["fundCode"], 
          "fundHouseCode": gblMFSwitchData["Funds"][i]["fundHouseCode"],
          "fxRisk":gblMFSwitchData["Funds"][i]["fxRisk"],
          "riskRate":gblMFSwitchData["Funds"][i]["riskRate"],
          "riskRate":gblMFSwitchData["Funds"][i]["riskRate"]
          
        };
        targetFundData.push(tempRecord);
      }
        frmMFSwitchLanding.SegmentFundList.isVisible = false;
        frmMFSwitchLanding.SegmentTargetFundList.removeAll();
        frmMFSwitchLanding.SegmentTargetFundList.setData(targetFundData);
        frmMFSwitchLanding.SegmentTargetFundList.isVisible = true;
      
       
    } else{
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }
  } else{
    dismissLoadingScreen();
    showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    return false;
  }
}


// function onClickSwitchMenu(){

//   if(gblUserLockStatusMB == "03"){
//     showTranPwdLockedPopup();
//   }else{
//     if(gblMFSummaryData["MF_BusinessHrsFlag"]!=null && gblMFSummaryData["MF_BusinessHrsFlag"]=="false"){
//       kony.print("1512 Inside If Condition, Other flow");
//       var endTime = gblMFSummaryData["MF_EndTime"];
//       var startTime = gblMFSummaryData["MF_StartTime"]; 
//       var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
//       messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
//       messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
//       showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
//     } else {
//       kony.print("1512 Inside else Condition, Normal flow");
//       gblOrderFlow = true;
//       gblMFOrder = {};
//       gblFundRulesData = {};
//       gblMFEventFlag = MF_EVENT_SWO;
//       gblMFOrder["orderType"] = MF_ORD_TYPE_SWITCH; 
//       gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
//       //  getMFSuitibilityReviewDetails(); 
//       checkSuitabilityExpireMB();
//     }
//   }


// }


function onClickSwitchMBMenu(){

  var currfrm = kony.application.getCurrentForm();
  kony.print("inside onClickSwitchMBMenu currfrm id is  " + currfrm.id);
  
  if(gblUserLockStatusMB == "03"){
    showTranPwdLockedPopup();
  }else if(currfrm.id == "frmMFFullStatementNFundSummary"){
    onClickSwitchInFundStmt();
  }else{
    if(gblMFSummaryData["MF_BusinessHrsFlag"]!=null && gblMFSummaryData["MF_BusinessHrsFlag"]=="false"){
      kony.print("Inside Switch summary flow ");
      var endTime = gblMFSummaryData["MF_EndTime"];
      var startTime = gblMFSummaryData["MF_StartTime"]; 
      var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
      messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
      messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
      showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
    } else {
      kony.print("Calling checkSuitabilityExpireMB");
      gblMFSwitchOrder = {};
      gblFundRulesData = {};
      gblMFEventFlag = MF_EVENT_SWO;
      gblMFSwitchOrder["orderType"] = MF_ORD_TYPE_SWITCH; 
      gblMFSwitchOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
      gblMFOrder["orderFlow"] = MF_FUNDSUMMARY_FLOW;
      checkSuitabilityExpireMB();
    }
  }
}


function onClickSwitchBack(){
  gblMFEventFlag = ' ';
  onClickSwitchCancel();
}

function setSwitchData(){

  var locale = kony.i18n.getCurrentLocale();
  var prevForm = kony.application.getPreviousForm();
  var investmentValue = "";
  var unitF = 0.0;
  var ordUnitF = 0.0;
  var redeemableUnitF = 0.0;
  var fundHouseCode = "";
  var randomnum = 0;
  var logo = "";

  unitF = parseFloat(numberFormat(gblMBMFDetailsResulttable["Unit"], 4));
  if(gblFundRulesData["totalRedeemInProcess"] == undefined){
    ordUnitF = 0.00;
  } else {
    ordUnitF = numberFormat(gblFundRulesData["totalRedeemInProcess"], 4);
  }

  redeemableUnitF = unitF - ordUnitF;
  gblMFOrder["redeemableUnit"] = numberFormat(redeemableUnitF, 4);

  if(gblMFOrder["redeemUnit"] == undefined || gblMFOrder["redeemUnit"] == ORD_UNIT_ALL){
    onClickSwitchbtnAll();
    frmMFSwitchLanding.txtSwitchBalance.text = commaFormatted(numberFormat(redeemableUnitF, 4));//MKI,MIB-12562
  } else {
    if(gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT){
      onClickSwitchbtnUnits();
      frmMFSwitchLanding.txtSwitchBalance.text = onDoneEditingUnitValue(gblMFOrder["amount"]); 
    } else if(gblMFOrder["redeemUnit"] == ORD_UNIT_BAHT){
      onClickSwitchbtnBaht();
      frmMFSwitchLanding.txtSwitchBalance.text = onDoneEditingAmountValue(gblMFOrder["amount"]); 
    }
  }


}


function onClickSwitchInFundStmt(){
  var APchar = gblUnitHolderNumber.substring(0, 2);
  kony.print("APchar  "+APchar);
  kony.print(" AS Prefix "+gblMFSummaryData["MF_UHO_ASP_PREFIX"]);
  var AS_prefix  = gblMFSummaryData["MF_UHO_ASP_PREFIX"];
  if(AS_prefix == APchar){
    alert(kony.i18n.getLocalizedString("MF_AS_Prefix_ERR"));
    return;
  }
  if(gblUserLockStatusMB == "03"){
    showTranPwdLockedPopup();
  }else{
    if( gblMBMFDetailsResulttable["MF_BusinessHrsFlag"]!=null && gblMBMFDetailsResulttable["MF_BusinessHrsFlag"]=="false"){
      var endTime = gblMBMFDetailsResulttable["MF_EndTime"];
      var startTime = gblMBMFDetailsResulttable["MF_StartTime"];
      var messageUnavailable = kony.i18n.getLocalizedString("ECGenericErrorNew");
      messageUnavailable = messageUnavailable.replace("{start_time}", startTime);
      messageUnavailable = messageUnavailable.replace("{end_time}", endTime);
      showAlert(messageUnavailable, kony.i18n.getLocalizedString("info"));
    } else {
      var tempfundTypeB = gblMFOrder["fundTypeB"];
      gblMFSwitchOrder = {};
      gblMFSwitchOrder["fundTypeB"] = tempfundTypeB ;
      kony.print("mki onClickSwitch gblMFOrder blank 3");
      gblMFEventFlag = MF_EVENT_SWO;
      //MBcallMutualFundRulesValidation();
      gblMFSwitchOrder["orderType"] = MF_EVENT_SWO;
      gblMFSwitchOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;
      gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW;
      gblMFSwitchOrder["fundNameTH"] = gblMBMFDetailsResulttable["FundNameTH"];
      gblMFSwitchOrder["fundNameEN"] = gblMBMFDetailsResulttable["FundNameEN"];
      gblMFSwitchOrder["fundShortName"] = gblFundShort;
      getSwitchFundDetailsAndTargetFunds();
    }
  }
}

function callBackOnSwitchConfirmOverTimeOrder(){
   popupConfirmation.dismiss();
   getMutualFundTnC();
}