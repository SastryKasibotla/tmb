gblFundRulesData = {};
//{"fundAllowOtx":"Y","minUnit":"0.0001","allotType":"1","tranEndDate":"2030-12-30","httpStatusCode":200,"fundCode":"VFOCUS-D","orderType":"2","suitabilityExpireDate":"","opstatus":0,"statusCode":"0","minAmount":"0.01","suitabilityScore":"4","timeStart":"0830","timeEnd":"1500","riskRate":"06","fundHouseCode":"UOBAMTH","trnAllowOtx":"Y","suitabilityExpireFlag":"","tranStartDate":"2012-09-12"}
function MBcallMutualFundRulesValidation(){
  var inputParam = {};
  showLoadingScreen();

  var unitholder = gblMBMFDetailsResulttable["UnitHolderNo"].replace(/-/gi, "");
  inputParam["unitHolderNo"] = unitholder;
  inputParam["orderType"] = gblMFEventFlag;
  inputParam["fundHouseCode"] = gblMBMFDetailsResulttable["FundHouseCode"]; 
  inputParam["fundCode"] = gblFundCode; 
  inputParam["orderDate"] = "";

  invokeServiceSecureAsync("MFFundValidation", inputParam, MBcallMutualFundRulesValidationCallBack);
}

function IBcallMutualFundRulesValidation(){
  var inputParam = {};
  //showLoadingScreen();

  var unitholder = gblUnitHolderNumber.replace(/-/gi, "");
  inputParam["unitHolderNo"] = unitholder;
  inputParam["orderType"] = gblMFEventFlag;
  inputParam["fundHouseCode"] = gblMFDetailsResulttable["FundHouseCode"]; 
  inputParam["fundCode"] = gblFundCode; 
  inputParam["orderDate"] = "";

  invokeServiceSecureAsync("MFFundValidation", inputParam, IBcallMutualFundRulesValidationCallBack);
}

function MBcallMutualFundRulesValidationForAcceptTnc(){
  var inputParam = {};
  showLoadingScreen();

  var unitholder = gblMFOrder["unitHolderNo"];
  inputParam["unitHolderNo"] = unitholder;
  inputParam["orderType"] = gblMFEventFlag;
  inputParam["fundHouseCode"] = gblMFOrder["fundHouseCode"];
  inputParam["fundCode"] = gblMFOrder["fundCode"]; 
  inputParam["TCConfirm"] = "Y";
  
  invokeServiceSecureAsync("MFFundValidation", inputParam, MBcallMutualFundRulesValidationForAcceptTncCallBack);
}

function MBcallMutualFundRulesValidationForAcceptTncCallBack(status,resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    if ( (resulttable["opstatus"] == 0) ) {
      frmMFConfirmMB.show();
    }
  }
}

function IBcallMutualFundRulesValidationForAcceptTnc(){
  var inputParam = {};
  showLoadingScreen();

  var unitholder = gblMFOrder["unitHolderNo"];
  inputParam["unitHolderNo"] = unitholder;
  inputParam["orderType"] = gblMFEventFlag;
  inputParam["fundHouseCode"] = gblMFOrder["fundHouseCode"];
  inputParam["fundCode"] = gblMFOrder["fundCode"];
  inputParam["TCConfirm"] = "Y";

  invokeServiceSecureAsync("MFFundValidation", inputParam, IBcallMutualFundRulesValidationForAcceptTncCallBack);
}

function IBcallMutualFundRulesValidationForAcceptTncCallBack(status,resulttable){
  if (status == 400) {
    dismissLoadingScreenPopup();
    if ( (resulttable["opstatus"] == 0) ) {
      if(gblMFEventFlag == MF_EVENT_PURCHASE){
        masterBillerInqMF();
      } else if(gblMFEventFlag == MF_EVENT_REDEEM){
        saveToSessionMFOrder();
      }
    }
  }
}

function MBcallMutualFundRulesValidationCallBack(status,resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    if ( (resulttable["opstatus"] == 0) ) {
      gblFundRulesData = resulttable;
      if(gblFundRulesData["effectiveDate"] != undefined &&    gblFundRulesData["transactionDate"] != undefined) {
        effectiveDate = gblFundRulesData["effectiveDate"].substring(0,10);
        transactionDate = gblFundRulesData["transactionDate"].substring(0,10);
      }
      if((gblMFEventFlag == MF_EVENT_PURCHASE||gblMFEventFlag == MF_EVENT_SWI || gblMFEventFlag == MF_EVENT_SWO) && gblFundRulesData["suitabilityExpireFlag"] == 'Y' ) {
        gblIsExpiry = true;
        var yyyyMMdd = gblFundRulesData["suitabilityExpireDate"].split("-");
        var expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
        getMFSuitibilityReviewExpiry(expireDate);
      } else if(gblFundRulesData["allowTransFlag"] == '3'){ //MKI, MIB-12551
        showAlert(kony.i18n.getLocalizedString("MF_ERR_Overcutofftime"), kony.i18n.getLocalizedString("info")); //MKI, MIB-12551
  	  }else if(gblFundRulesData["allowTransFlag"] != '0'){
        showAlert(kony.i18n.getLocalizedString("MF_ERR_cutofftime"), kony.i18n.getLocalizedString("info"));
      } else if(gblMFEventFlag == MF_EVENT_REDEEM && 
                (gblFundRulesData["allotType"] == '2' || gblFundRulesData["allotType"] == '3')){
        showAlert(kony.i18n.getLocalizedString("MF_ERR_notallow_order"), kony.i18n.getLocalizedString("info"));
      } else if(gblFundRulesData["fundCode"] == "") {
        showAlert("Cannot check fund suitability", kony.i18n.getLocalizedString("info"));
      } else if(effectiveDate != transactionDate){ 
        popupConfirmMutualFundOrder(callBackOnConfirmOverTimeOrder);
      } else if(gblMFEventFlag == MF_EVENT_SWO){ 
         gblMFOrder["orderFlow"] = MF_FUNDDETAIL_FLOW
         frmMFSwitchLanding.show();
      } else {
        gblMFOrder["fundHouseCode"] = gblMBMFDetailsResulttable["FundHouseCode"]; 
        gblMFOrder["fundCode"] = gblFundCode;
        gblMFOrder["unitHolderNo"] = gblMBMFDetailsResulttable["UnitHolderNo"].replace(/-/gi, "");
        MBorderMutualFund();
      }
    } 
    else {
       if ( (resulttable["errCode"] == 21) ) { //MKI, MIB - 10144 start
        dismissLoadingScreen();
      	showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01004"), kony.i18n.getLocalizedString("info"));
      }
      else if ( (resulttable["errCode"] == 22) ) { //MKI,MIB-11357 start
        dismissLoadingScreen();
      	showAlert(kony.i18n.getLocalizedString("MF_ERR_UHNO_NOTALLOW"), kony.i18n.getLocalizedString("info"));
      } //MKI,MIB-11357 End
      else if ( (resulttable["errCode"] == 1) ) {
        dismissLoadingScreen();
      	showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
      }
      else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      } //MKI, MIB - 10144 End
    }
  } else {
    dismissLoadingScreen();
  }	

}

function IBcallMutualFundRulesValidationCallBack(status,resulttable){
  if (status == 400) {
    dismissLoadingScreenPopup();
    if ( (resulttable["opstatus"] == 0) ) {
      gblFundRulesData = resulttable;
      if(gblFundRulesData["effectiveDate"] != undefined && gblFundRulesData["transactionDate"] != undefined) {
        effectiveDate = gblFundRulesData["effectiveDate"].substring(0,10);
        transactionDate = gblFundRulesData["transactionDate"].substring(0,10);
      }
      if(gblMFEventFlag == MF_EVENT_PURCHASE && gblFundRulesData["suitabilityExpireFlag"] == 'Y') {
        //showAlert(kony.i18n.getLocalizedString("MF01_001"), kony.i18n.getLocalizedString("info"));
        gblIsExpiry = true;
        var yyyyMMdd = gblFundRulesData["suitabilityExpireDate"].split("-");
        var expireDate = yyyyMMdd[2] + "/" + yyyyMMdd[1] + "/" + yyyyMMdd[0];
        getMFSuitibilityReviewExpiryIB(expireDate);
      } else if(gblFundRulesData["allowTransFlag"] == '3'){ //MKI, MIB-12551
        showAlert(kony.i18n.getLocalizedString("MF_ERR_Overcutofftime"), kony.i18n.getLocalizedString("info")); //MKI, MIB-12551
      }else if(gblFundRulesData["allowTransFlag"] != '0'){
        showAlert(kony.i18n.getLocalizedString("MF_ERR_cutofftime"), kony.i18n.getLocalizedString("info"));
      } else if(gblMFEventFlag == MF_EVENT_REDEEM && 
                (gblFundRulesData["allotType"] == '2' || gblFundRulesData["allotType"] == '3')){
        showAlert(kony.i18n.getLocalizedString("MF_ERR_notallow_order"), kony.i18n.getLocalizedString("info"));
      } else if(gblFundRulesData["fundCode"] == "") {
        showAlert("Cannot check fund suitability", kony.i18n.getLocalizedString("info"));
      } else if(effectiveDate != transactionDate){ 
        popupConfirmMutualFundOrderIB();
      } else {
        gblMFOrder["fundHouseCode"] = gblMFDetailsResulttable["FundHouseCode"]; 
        gblMFOrder["fundCode"] = gblFundCode;
        gblMFOrder["unitHolderNo"] = gblMFDetailsResulttable["UnitHolderNo"].replace(/-/gi, "");
        IBorderMutualFund();
      }
    } 
    else {
      if ( (resulttable["errCode"] == 21) ) { //MKI, MIB - 10144 start
        dismissLoadingScreenPopup();
      	showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01004"), kony.i18n.getLocalizedString("info"));
      }
      else if ( (resulttable["errCode"] == 22) ) { //MKI,MIB-11357 start
        dismissLoadingScreenPopup();
      	showAlert(kony.i18n.getLocalizedString("MF_ERR_UHNO_NOTALLOW"), kony.i18n.getLocalizedString("info"));
      } //MKI,MIB-11357 End
      else if ( (resulttable["errCode"] == 1) ) {
        dismissLoadingScreen();
      	showAlert(kony.i18n.getLocalizedString("MF_P_ERR_01005"), kony.i18n.getLocalizedString("info"));
      }
      else{
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      } //MKI, MIB - 10144 End
    }
  } else {
    dismissLoadingScreenPopup();
  }	
}

function MBorderMutualFund(){

  if(gblMFEventFlag == MF_EVENT_PURCHASE) {
    //frmMFSelectPurchaseFundMB.show();//MKI,MIB-11684 start
    if(gblFundRulesData["requireSuitabilityUpdated"] != undefined  && gblFundRulesData["requireSuitabilityUpdated"] == 1) {
       //Suitability flow MB
       showAlertMFSuitWarnMB();
    }
    else
    {
      // Purchase flow MB
      frmMFSelectPurchaseFundMB.show();
    }//MKI,MIB-11684 End
  } else if(gblMFEventFlag == MF_EVENT_REDEEM) {
    frmMFSelectRedeemFundMB.show();
  }else if(gblMFEventFlag == MF_EVENT_SWO ) {
    if(gblFundRulesData["requireSuitabilityUpdated"] != undefined  && gblFundRulesData["requireSuitabilityUpdated"] == 1) {
      //Suitability flow MB
      showAlertMFSuitWarnMB();
    }
    else
    {
      var curFrm = kony.application.getCurrentForm();
        if(curFrm.id != "frmMFSwitchLanding"){
          frmMFSwitchLanding.show();
        } 
    }
  }
}
function showAlertMFSuitWarnMB() {
  	var KeyTitle = "Info"//kony.i18n.getLocalizedString("keyOK");
   // var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage");
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
  	var keyMsg = kony.i18n.getLocalizedString("MF_Suitablity_ComplexFundWarn");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: onClickSuitabilityScoreMBMenu
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}
function showAlertMFSuitWarnIB() {
  	var KeyTitle = "Info"//kony.i18n.getLocalizedString("keyOK");
   // var keyMsg = kony.i18n.getLocalizedString("keyTouchMessage");
    var okk = kony.i18n.getLocalizedString("keyOK");
    
	//Defining basicConf parameter for alert
  	var keyMsg = kony.i18n.getLocalizedString("MF_Suitablity_ComplexFundWarn");
	var basicConf = {
		message: keyMsg,
		alertType: constants.ALERT_TYPE_INFO,
		alertTitle: KeyTitle,
		yesLabel: okk,
		noLabel: "",
		alertHandler: SuitibilityReviewDetailsIB
	};
	//Defining pspConf parameter for alert
	var pspConf = {};
	//Alert definition
	var infoAlert = kony.ui.Alert(basicConf, pspConf);
    function handleok(response){}
    
	return;
}
function SuitibilityReviewDetailsIB()
{
   gblOrderFlow = false;
   getMFSuitibilityReviewDetailsIB();
}
function IBorderMutualFund(){
  if(gblMFEventFlag == MF_EVENT_PURCHASE) {
    //onSelectFromAccntMufutalFundIB(); //MKI,MIB-11684 start
    if(gblFundRulesData["requireSuitabilityUpdated"] != undefined  && gblFundRulesData["requireSuitabilityUpdated"] == 1) {
       //Suitability flow IB
       showAlertMFSuitWarnIB();
    }
    else
    {
      // Purchase flow IB
      onSelectFromAccntMufutalFundIB();
    }//MKI,MIB-11684 End
  } else if(gblMFEventFlag == MF_EVENT_REDEEM) {
    frmIBMFSelRedeemFund.show();
  }
}

function getMutualFundTnC() {
   if(gblMFEventFlag == MF_EVENT_SWI || gblMFEventFlag == MF_EVENT_SWO) {
     kony.print("getMutualFundTnC ::: start");
    MFFundValidationFOrTargetFund();
     
     //MutualFundTnC()
  } else  {
   MFOrderActivityLogInitial();
  }
  
}

function MutualFundTnC(){
  kony.print("MutualFundTnC ::: start");
  var inputparam = {};
  var currentLocale = kony.i18n.getCurrentLocale();

   inputparam["localeCd"] = currentLocale;

  if(gblMFEventFlag == MF_EVENT_PURCHASE) {
    inputparam["moduleKey"] = 'TMBMFPurchase';
  } else if(gblMFEventFlag == MF_EVENT_REDEEM) {
    inputparam["moduleKey"] = 'TMBMFRedeem';
  }else if(gblMFEventFlag == MF_EVENT_SWI || gblMFEventFlag == MF_EVENT_SWO) {
    inputparam["moduleKey"] = 'TMBMFSwitch';
     //inputparam["moduleKey"] = 'TMBMFPurchase';
  }
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", inputparam, MutualFundTnCforCallBack);
}

function MutualFundTnCforCallBack(status,resulttable){
  if(status == 400){
    dismissLoadingScreen();
          
   kony.print("MutualFundTnCforCallBack ::: start");
    if(resulttable["opstatus"]==0){
      if(gblChannel == "IB"){
        frmIBMFTnC.lblTnC.text = resulttable["fileContent"];
        //frmIBMFTnC.show();
      } else {
         kony.print("MutualFundTnCforCallBack in else");
        frmMFTnc.lblTandC.text = resulttable["fileContent"];
        frmMFTncPreShow();
        frmMFTnc.show();
      }
    }              
    else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
    }
  } else {
    dismissLoadingScreen();
  }	     
}

function MFOrderActivityLogInitial(){
	var inputParam = {};
	showLoadingScreen();

	inputParam["unitHolderNo"] = gblMFOrder["unitHolderNo"];

    if (gblMFOrder["orderType"] == MF_ORD_TYPE_REDEEM) {
        inputParam["orderType"] = MF_EVENT_REDEEM;   //  1 Purchase 2 Redeem
        if (gblMFOrder["redeemUnit"] == ORD_UNIT_ALL || gblMFOrder["redeemUnit"] == ORD_UNIT_UNIT) {
            inputParam["redeemType"] = ORD_UNIT_UNIT;     // M U
        } else {
            inputParam["redeemType"] = ORD_UNIT_BAHT;
        }
    } else if (gblMFOrder["orderType"] == MF_ORD_TYPE_PURCHASE) {
    	inputParam["orderType"] = MF_EVENT_PURCHASE;
    	if(gblChannel == "IB") {
    		inputParam["fromAccountNo"] = removeHyphenIB(gblAccountNoval);
    	} else {
            if(gbltdFlag == "CCA"){
              inputParam["cardRefId"] = gblMFOrder["fromAccountNo"].split("-").join("");
              inputParam["fromAccountNo"] = gblMFOrder["fromAccountNo"].split("-").join("");
            }else{
              inputParam["fromAccountNo"] = gblMFOrder["fromAccountNo"].split("-").join("")
            }
    		
    	}
    }
    
  	inputParam["fundHouseCode"] = gblMFOrder["fundHouseCode"];
 	inputParam["fundCode"] = gblMFOrder["fundCode"];
	inputParam["amount"] = gblMFOrder["amount"];
	inputParam["fundShortName"] = gblMFOrder["fundShortName"];
	
	inputParam["TCConfirm"] = "I";

  	invokeServiceSecureAsync("MFFundValidation", inputParam, MFOrderActivityLogInitialCallBack);
}

function MFOrderActivityLogInitialCallBack(status,resulttable){
  if (status == 400) {
    dismissLoadingScreen();
    if(resulttable["opstatus"] == 0) {
      kony.print("Inside MFOrderActivityLogInitialCallBack");
       var MF_CC_StartTime = resulttable["ccStartTime"];
       var MF_CC_EndTime = resulttable["ccEndTime"];
       var MF_CC_AllowTime = resulttable["ccAllowTime"];
      //var MF_CC_AllowTime = "N";
       var errorMsg = kony.i18n.getLocalizedString("MF_PUR_CC"); 
       errorMsg = errorMsg.replace("{service_hours}", MF_CC_EndTime + " - " + MF_CC_StartTime);
		
      if(gbltdFlag == "CCA" && MF_CC_AllowTime == "N"){
        kony.print("Inside CC alow time");
          gblCCAllowTime = "N";
          showAlert(errorMsg, kony.i18n.getLocalizedString("info"));
        }else{
          MutualFundTnC();
        }

    }
  }
}

function formatDateMF(date) {
  //var montharrayfull = [];
  //montharrayfull = [kony.i18n.getLocalizedString("keyCalendarJanuary"), kony.i18n.getLocalizedString("keyCalendarFebruary"), kony.i18n.getLocalizedString("keyCalendarMarch"), 
  //                  kony.i18n.getLocalizedString("keyCalendarApril"),kony.i18n.getLocalizedString("keyCalendarMayFull"), kony.i18n.getLocalizedString("keyCalendarJune"),
  //                  kony.i18n.getLocalizedString("keyCalendarJuly"), kony.i18n.getLocalizedString("keyCalendarAugust"),kony.i18n.getLocalizedString("keyCalendarSeptember"),
  //                  kony.i18n.getLocalizedString("keyCalendarOctober"), kony.i18n.getLocalizedString("keyCalendarNovember"), kony.i18n.getLocalizedString("keyCalendarDecember")];

  var d = new Date(date),
      month = '' + (d.getMonth()+1),
      day = '' + d.getDate(),
      year = d.getFullYear();
  if (day.length < 2) day = '0' + day;
  if (month.length < 2) month = '0' + month;
  
  return [day, month, year].join('/');
}

function checkAvailableBalance(ordAmount, availableBalance){
  if(availableBalance < ordAmount){
    return false;
  }
  return true;
}
function checkHoldingUnit(unitOrdAmount, unitHolder){
  var tempRemainUnit = 0.0000;

  //var unitOrdAmount = parseFloat(unitOrder);
  var minUnitHolderUnit = parseFloat(gblFundRulesData["minHoldingUnit"]);
  var errText = "";

  //alert("Current Unit:" + unitHolder + " Order Unit:" + unitOrdAmount);

  tempRemainUnit = unitHolder - unitOrdAmount;
  if(tempRemainUnit > 0.0000 && tempRemainUnit  < minUnitHolderUnit){
    return false;
  }
  return true;
}

function checkHoldingInvestmentValue(ordAmount, investmentVal){
  var tempRemainAmount = 0.00;

  //var ordAmount = parseFloat(bahtOrder);
  var minUnitHolderAmount = parseFloat(gblFundRulesData["minHoldingAmount"]);
  var errText = "";

  //alert("Investment:" + investmentVal + " Order Amount:" + ordAmount);

  tempRemainAmount = investmentVal - ordAmount;
  if(tempRemainAmount > 0.0000 && tempRemainAmount  < minUnitHolderAmount){
    return false;
  }
  return true;
}

function checkHoldingValue(unitType, bahtOrdAmount, unitOrdAmount, holdingUnit, holdingAmount){
  var errMsg = "";
  //alert("min holding unit:" + gblFundRulesData["minHoldingCon"]);
  if(gblFundRulesData["minHoldingCon"] == '1'){ // case and
    if(checkHoldingInvestmentValue(bahtOrdAmount, holdingAmount)){
      if(checkHoldingUnit(unitOrdAmount, holdingUnit)){
        // do nothing no alert
      } else {
        errMsg = kony.i18n.getLocalizedString("MF_ERR_MINHOLDING_UNIT");
        errMsg = errMsg.replace("{x}",gblFundRulesData["minHoldingUnit"])
      }
    } else {
      errMsg = kony.i18n.getLocalizedString("MF_ERR_MINHOLDING_AMOUNT");
      errMsg = errMsg.replace("{x}",gblFundRulesData["minHoldingAmount"])
    }
  } else if(gblFundRulesData["minHoldingCon"] == '0') { // case or
    if(checkHoldingInvestmentValue(bahtOrdAmount, holdingAmount)){
      // do nothing no alert
    } else {
      if(checkHoldingUnit(unitOrdAmount, holdingUnit)){
        // do nothing no alert
      } else {
        if(unitType == ORD_UNIT_UNIT){
          errMsg = kony.i18n.getLocalizedString("MF_ERR_MINHOLDING_UNIT");
          errMsg = errMsg.replace("{x}",gblFundRulesData["minHoldingUnit"])
        } else if(unitType == ORD_UNIT_BAHT){
          errMsg = kony.i18n.getLocalizedString("MF_ERR_MINHOLDING_AMOUNT");
          errMsg = errMsg.replace("{x}",gblFundRulesData["minHoldingAmount"])
        }
      }
    }
  }
  return errMsg;
}

function checkMinUnit(unitOrdAmount){
  var minUnit = parseFloat(gblFundRulesData["minUnit"]);

  if(unitOrdAmount < minUnit){
    return false;
  }
  return true;
}

function checkMaxUnit(unitOrdAmount){
  var maxUnit = parseFloat(gblFundRulesData["maxUnit"]);

  if(unitOrdAmount > maxUnit){
    return false;
  }
  return true;
}

function checkMinAmount(unitOrdAmount){
  var minAmount = parseFloat(gblFundRulesData["minAmount"]);

  if(unitOrdAmount < minAmount){
    return false;
  }
  return true;
}

function checkMaxAmount(unitOrdAmount){
  var maxAmount = parseFloat(gblFundRulesData["maxAmount"]);

  if(unitOrdAmount > maxAmount){
    return false;
  }
  return true;
}

function validatePurchaseOrder(unitOrder, availableBalance){
  var unitOrdAmount = parseFloat(unitOrder);
  var errMsg = "";
  if(!checkAvailableBalance(unitOrdAmount, availableBalance)){
    if(gbltdFlag == "CCA"){
      errMsg = kony.i18n.getLocalizedString("MF01_009");
    }else{
      errMsg = kony.i18n.getLocalizedString("MF01_008");
    }
  } else if(!checkMinAmount(unitOrdAmount)){
    errMsg = kony.i18n.getLocalizedString("MF01_004");
    errMsg = errMsg.replace("{x}",autoFormatAmount(gblFundRulesData["minAmount"])); //mki, MIB 9746
    errMsg = errMsg.replace("{y}",kony.i18n.getLocalizedString("key_MF_Purchase"));
  } else if(!checkMaxAmount(unitOrdAmount)){
    errMsg = kony.i18n.getLocalizedString("MF01_005");
    errMsg = errMsg.replace("{x}",autoFormatAmount(gblFundRulesData["maxAmount"])); //mki, MIB 9746
    errMsg = errMsg.replace("{y}",kony.i18n.getLocalizedString("key_MF_Purchase"));
  } 

  return errMsg;
}

function calulateAmountWithNAV(nav, amount,unitType){
  //var nav = 0.0000;
  var retAmount = 0.00;
  if(unitType == ORD_UNIT_BAHT){ 
    retAmount = amount / nav;
  }else if(unitType == ORD_UNIT_UNIT){
    retAmount = amount * nav;
  }
  return retAmount;

}

function validateRedeemOrder(avaiUnit, avaiAmount, amount,unitType, nav){
  var unitOrdAmount = 0.0;
  var bahtOrdAmount = 0.0;
  var errMsg = "";

  if(avaiUnit <= 0.00){
    errMsg = kony.i18n.getLocalizedString("RD_01_003");
  } else {
    if(unitType == ORD_UNIT_BAHT){
      bahtOrdAmount = parseFloat(amount);
      unitOrdAmount = calulateAmountWithNAV(nav, bahtOrdAmount, ORD_UNIT_BAHT);
      //alert("BAHT:" + bahtOrdAmount + " UNIT:" + unitOrdAmount);
      if(avaiAmount < bahtOrdAmount){
        errMsg = kony.i18n.getLocalizedString("RD_01_003");
      } else if(!checkMinAmount(bahtOrdAmount)){
        errMsg = kony.i18n.getLocalizedString("MF01_004");
        errMsg = errMsg.replace("{x}",autoFormatAmount(gblFundRulesData["minAmount"])); //mki, MIB 9746
        errMsg = errMsg.replace("{y}",kony.i18n.getLocalizedString("key_MF_Redeem"));
      } else {
        errMsg = checkHoldingValue(unitType, bahtOrdAmount, unitOrdAmount, avaiUnit, avaiAmount);
      }
    } else if(unitType == ORD_UNIT_UNIT){
      unitOrdAmount = parseFloat(amount);
      bahtOrdAmount = calulateAmountWithNAV(nav, unitOrdAmount, ORD_UNIT_UNIT);
      //alert("BAHT:" + bahtOrdAmount + " UNIT:" + unitOrdAmount);

      if(avaiUnit < unitOrdAmount){
        errMsg = kony.i18n.getLocalizedString("RD_01_003");
      } else if(!checkMinUnit(unitOrdAmount)){
        errMsg = kony.i18n.getLocalizedString("MF01_006");
        errMsg = errMsg.replace("{x}",gblFundRulesData["minUnit"])
        errMsg = errMsg.replace("{y}",kony.i18n.getLocalizedString("key_MF_Redeem"));
      } else {
        errMsg = checkHoldingValue(unitType, bahtOrdAmount, unitOrdAmount, avaiUnit, avaiAmount);
      }
    } else if(unitType == ORD_UNIT_ALL){
      unitOrdAmount = parseFloat(amount);
      if(!checkMinUnit(unitOrdAmount)){
        errMsg = kony.i18n.getLocalizedString("MF01_006");
        errMsg = errMsg.replace("{x}",gblFundRulesData["minUnit"])
        errMsg = errMsg.replace("{y}",kony.i18n.getLocalizedString("key_MF_Redeem"));
      }
    }
  }


  return errMsg;
}

function popupConfirmMutualFundOrder(callBackOnConfirm) {
  popupConfirmation.lblPopupConfText.text = kony.i18n.getLocalizedString("MF_ERR_005");
  popupConfirmation.imgPopConfirmIcon.setVisibility(false);
  popupConfirmation.btnpopConfConfirm.onClick = callBackOnConfirm;
  popupConfirmation.btnPopupConfCancel.onClick = callBackCancel;
  popupConfirmation.btnPopupConfCancel.text = kony.i18n.getLocalizedString("keyCancelButton");
  popupConfirmation.btnpopConfConfirm.text = kony.i18n.getLocalizedString("keyYes");
  popupConfirmation.show();
}

function callBackOnConfirmOverTimeOrder(){
  popupConfirmation.dismiss();
  //frmMFCompleteMB.show();
  gblMFOrder["fundHouseCode"] = gblMBMFDetailsResulttable["FundHouseCode"]; 
  gblMFOrder["fundCode"] = gblFundCode;
  gblMFOrder["unitHolderNo"] = gblMBMFDetailsResulttable["UnitHolderNo"].replace(/-/gi, "");
  MBorderMutualFund();
}
function callBackCancel(){
  popupConfirmation.dismiss();
}

function popupConfirmMutualFundOrderIB() {


  popupIBCommonConfirmCancel.lblPopUpMessage.text = kony.i18n.getLocalizedString("MF_ERR_005");
  
  popupIBCommonConfirmCancel.btnPopUpConfirm.text = kony.i18n.getLocalizedString("keyYes");
  popupIBCommonConfirmCancel.btnPopUpCancel.text = kony.i18n.getLocalizedString("keyCancelButton");

  if(gblMFOrder["orderFlow"] == MF_FUNDSUMMARY_FLOW){
    popupIBCommonConfirmCancel.btnPopUpConfirm.onClick = callBackOnConfirmOverTimeOrderIBOrdSumFlow;
  } else if(gblMFOrder["orderFlow"] == MF_FUNDDETAIL_FLOW){
	    gblMFOrder["fundHouseCode"] = gblMFDetailsResulttable["FundHouseCode"]; 
  gblMFOrder["fundCode"] = gblFundCode;
  gblMFOrder["unitHolderNo"] = gblMFDetailsResulttable["UnitHolderNo"].replace(/-/gi, "");
    popupIBCommonConfirmCancel.btnPopUpConfirm.onClick = callBackOnConfirmOverTimeOrderIB;
  }

  popupIBCommonConfirmCancel.btnPopUpCancel.onClick = callBackOnCancelOverTimeOrderIB;
  popupIBCommonConfirmCancel.show();
}

function callBackOnConfirmOverTimeOrderIB(){
  popupIBCommonConfirmCancel.dismiss();
  IBorderMutualFund();
}
function callBackOnConfirmOverTimeOrderIBOrdSumFlow(){
  popupIBCommonConfirmCancel.dismiss();
  gblChannel = "IB";
    frmIBMFTnC.show();
  

}
function callBackOnCancelOverTimeOrderIB() {
  popupIBCommonConfirmCancel.dismiss();
}

function callProcessOrdMFCompositeService(tranPassword){
  dismissLoadingScreen();
  popTransactionPwd.dismiss();
}

function getAccountScoreMapping(actureScore){
  var accountRiskLevel = 1;
  switch(actureScore) {
    case 1: accountRiskLevel = 1; break; // 1
    case 2: accountRiskLevel = 4; break; // 1-4
    case 3: accountRiskLevel = 5; break; // 1-5
    case 4: accountRiskLevel = 7; break; // 1-7
    case 5: accountRiskLevel = 8; break; // 1-8
  }
  return accountRiskLevel;
}