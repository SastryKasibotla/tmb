/*
************************************************************************
            Name    : Handle Letter of Consent
            Author  : Brajesh Kumar
            Date    : Feb 02, 2018
            Purpose : Loan application user Consent verification
************************************************************************
 */
gblconsentApproved=false;

function InitfrmLetterConsent(){
  frmLetterConsent.preShow=preshowfrmLetterConsent;
  frmLetterConsent.postShow=postshowfrmLetterConsent;
  frmLetterConsent.onDeviceBack=onDeviceSpaback;
}
function postshowfrmLetterConsent(){
   if (gblCallPrePost) {
        //..
    }
  	addAccessPinKeypad(frmLetterConsent);
    assignGlobalForMenuPostshow();
}
function preshowfrmLetterConsent(){
  if(!gblconsentApproved){
      frmLetterConsent.lblProductDetailsHeader.text = kony.i18n.getLocalizedString("NCBTitle");
      frmLetterConsent.btnCancel.text = kony.i18n.getLocalizedString("MF_RDM_33");
      frmLetterConsent.btnNext.text = kony.i18n.getLocalizedString("MF_RDM_40");
      frmLetterConsent.lblNCBAccept.text = kony.i18n.getLocalizedString("AcceptNCB");
      frmLetterConsent.lblNCBNotAccept.text = kony.i18n.getLocalizedString("NotAcceptNCB");
      frmLetterConsent.lblTitleNCB.text = kony.i18n.getLocalizedString("SuggestTitle");
      frmLetterConsent.lblContentNCB.text = kony.i18n.getLocalizedString("SuggestContent");
      frmLetterConsent.lblContentConfirm.text = kony.i18n.getLocalizedString("SuggestConfirm");
      frmLetterConsent.btnBackProDetl.setVisibility(true);
      frmLetterConsent.flxNCBChecklist.setVisibility(true);
      frmLetterConsent.btnBackProDetl.onClick=onclickLetterofConsentBack;
      //frmLetterConsent.btnCancel.onClick=onclickLetterofConsentBack;
      ////frmLetterConsent.btnNext.onClick =onclickLetterConsentNext;
      frmLetterConsent.flxNotAcceptNCBPopUp.setVisibility(false);
      frmLetterConsent.btnNext.onClick = showAcceptNotAccept;
      frmLetterConsent.btnNext.skin = "btnGreyBGNoRound";
      frmLetterConsent.btnNext.focusSkin = "btnGreyBGNoRound";
      frmLetterConsent.ImageCheckList.skin = "skinpiblueLineLone";
      frmLetterConsent.ImageCheckList1.skin = "skinpiblueLineLone";
      frmLetterConsent.flxNCBAccept.onClick = selectNCB;
      frmLetterConsent.flxNCBNotAccept.onClick = selectNotNCB;
      frmLetterConsent.btnCancel.onClick = navigateBackToSubProducts;
  } 
}

function showAcceptNotAccept(){
     frmLetterConsent.Flexbody.scrollToEnd();
}

function selectNCB(){
  var selectswitch = false;
  selectswitch = !selectswitch;
  if(selectswitch === true){
  frmLetterConsent.ImageCheckList.skin = "skinpibluechkedLineLone";
    }else{
      frmLetterConsent.ImageCheckList.skin = "skinpiblueLineLone";
    }
  if(frmLetterConsent.ImageCheckList.skin == skinpibluechkedLineLone){
     frmLetterConsent.ImageCheckList1.skin = "skinpiblueLineLone";
  }
    frmLetterConsent.btnNext.onClick = showAccessPinScreenKeypad;
    frmLetterConsent.btnNext.skin = "btnBlueBGNoRound";
    frmLetterConsent.btnNext.focusSkin = "btnBlueBGNoRound150pxLoan";
}

function selectNotNCB(){
  var selectswitch = false;
  selectswitch = !selectswitch;
  if(selectswitch === true){
  frmLetterConsent.ImageCheckList1.skin = "skinpibluechkedLineLone";
    }else{
      frmLetterConsent.ImageCheckList1.skin = "skinpiblueLineLone";
    }
  if(frmLetterConsent.ImageCheckList1.skin == skinpibluechkedLineLone){
    frmLetterConsent.ImageCheckList.skin = "skinpiblueLineLone";
  }
    frmLetterConsent.btnNext.onClick = ncbWarningPopup;
    frmLetterConsent.btnNext.skin = "btnBlueBGNoRound";
    frmLetterConsent.btnNext.focusSkin = "btnBlueBGNoRound150pxLoan";
}

function ncbWarningPopup(){
  frmLetterConsent.flxNotAcceptNCBPopUp.setVisibility(true);
  frmLetterConsent.btnCancel.text = kony.i18n.getLocalizedString("Back");
  frmLetterConsent.btnNext.onClick = showAccessPinScreenKeypad;
  frmLetterConsent.btnCancel.onClick = closeWarningPopup;
  frmLetterConsent.btnClosePopUp.onClick = closeWarningPopup;
}

function closeWarningPopup(){
   frmLetterConsent.flxNotAcceptNCBPopUp.setVisibility(false);
   frmLetterConsent.btnCancel.text = kony.i18n.getLocalizedString("MF_RDM_33");
   frmLetterConsent.btnCancel.onClick = navigateBackToSubProducts;
   frmLetterConsent.btnNext.onClick = ncbWarningPopup;
}

function onlickBackCompleteConsent(){
  gblconsentApproved=false;
  callServicegetLetterofContent();
}
function onclickLetterConsentNext(){
  //uploadLetterofConsentScreenshot();
    kony.print("Test point");
    if(gblconsentApproved){
      ////frmLetterConsent.btnNext.onClick = uploadLetterofConsentScreenshot;
    }else{
      ////frmLetterConsent.btnNext.onClick = showAccessPinScreenKeypad;
    }
  
}
function verifyLetterofConsent(tranPassword){
  var inputParam={};
  inputParam["loginModuleId"] = "MB_TxPwd";
  inputParam["password"] =  encryptData(tranPassword);  
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        inputParam["localeCd"] = "en_US";
    } else {
        inputParam["localeCd"] = "th_TH";
    }
   inputParam["productType"] = frmCheckList.lblSubProductName.text;
   //inputParam["TransPassword"] = tranPassword;
   inputParam["consentApproved"] = "Y";
   inputParam["productCode"]=gblSelectedLoanProduct;
   inputParam["idType"]=gblCustomerIDType;
   if(frmLetterConsent.ImageCheckList.skin == skinpibluechkedLineLone){
   inputParam["ncbConsentFlag"] = "Y";
   }else if(frmLetterConsent.ImageCheckList1.skin == "skinpibluechkedLineLone"){
     inputParam["ncbConsentFlag"] = "N";
   }
  //inputParam["idIssueCtry"]="TH";
   if (gblCustomerIDType == "CI") {
    inputParam["idNo"] = gblCustomerIDValue;
  }else{
    inputParam["idNo"] =  gblCustomerPassport;
  }   
  
   inputParam["categoryCode"]=gblLoanAppType;
  inputParam["moduleKey"] ="ReadLetterofContent";
  showLoadingScreen();
  invokeServiceSecureAsync("verifyLetterofConsentService", inputParam, callbackVerifyLetterofConsent);
}
function preshowOfLetterOfComplete(){
  kony.print("preshowOfLetterOfComplete");
 //frmLetterConsent.lblHeader.setVisibility(false);
  frmLetterConsent.richtxtPersonalInfo.setVisibility(true);
  frmLetterConsent.lblProductDetailsHeader.text = kony.i18n.getLocalizedString("NCBTitle");
  frmLetterConsent.btnCancel.text = kony.i18n.getLocalizedString("MF_RDM_33");
  frmLetterConsent.btnNext.text = kony.i18n.getLocalizedString("MF_RDM_6");
  frmLetterConsent.btnBackProDetl.setVisibility(false);
  //frmLetterConsent.btnNext.onClick =showfrmLoanApplication; //--need to update method after service inegration
  //frmLetterConsent.lblHeader.setVisibility(true);
}
function callbackVerifyLetterofConsent(status,result){
  if (status == 400) {
    if (result["opstatus"] === 0 ||result["opstatus"] == "0") {
      dismissLoadingScreen();
      frmLetterConsent.richtxtPersonalInfo.text="";
      frmLetterConsent.richtxtPersonalInfo.setVisibility(false);
      frmLetterConsent.flxNotAcceptNCBPopUp.setVisibility(false);
      gblconsentApproved=true;
      closeApprovalKeypad();
      frmLetterConsent.flxNCBChecklist.setVisibility(false);
      frmLetterConsent.btnNext.onClick =showfrmLoanApplication; //uploadLetterofConsentScreenshot;
      frmLetterConsent.richtxtPersonalInfo.text=result["fileContent"];
      gblCaId=result["caId"];
      gblLoanAppRefNo=result["appRefNo"];
      preshowOfLetterOfComplete();
      //postshowfrmLetterConsent();
      //frmLetterConsent.show();
      dismissLoadingScreen();
    }else if (result["opstatus"] === 1 || result["opstatus"] == "1") {
      if(result["errCode"] == "E10020" || result["errMsg"] == "Wrong Password" ){
        //Wrong password Entered 

        resetKeypadApproval();
        var badLoginCount = result["badLoginCount"];
        var incorrectPinText = kony.i18n.getLocalizedString("PIN_Incorrect");
        incorrectPinText = incorrectPinText.replace("{rem_attempt}", gblTotalPinAttempts - badLoginCount);
        frmLetterConsent.lblForgotPin.text = incorrectPinText;
        frmLetterConsent.lblForgotPin.skin = "lblBlackMed150NewRed";
        frmLetterConsent.lblForgotPin.onTouchEnd = doNothing;
        dismissLoadingScreen();

      }else if(result["errCode"] == "E10403" || result["errMsg"] == "Password Locked" ){
        // password Locked 
        closeApprovalKeypad();
        dismissLoadingScreen();
        //frmMBManageDebitCardFailure.rchAddress.text = kony.i18n.getLocalizedString("ChangeDBL_Failed");
        gotoUVPINLockedScreenPopUp();

      }else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
    } else {
        dismissLoadingScreen();
        showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
        return false;
      }
  }else {
      dismissLoadingScreen();
      showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
      return false;
    }

  }

// function uploadLetterofConsentScreenshot(){
//    showLoadingScreen();
//   	var snapshot=takeSnapshot(frmLetterConsent.Flexbody);
//   	 var input_param = {};
//     var locale = kony.i18n.getCurrentLocale();
//     if (locale == "en_US") {
//         input_param["localeCd"] = "en_US";
//     } else {
//         input_param["localeCd"] = "th_TH";
//     }
//     input_param["Base64Img"] =snapshot;
//   	invokeServiceSecureAsync("uploadImageFiletoFTP", input_param, callbackuploadImage);
// }
// function callbackuploadImage(status,result){
//   if (status == 400) {
//         if (result["opstatus"] == 0) {
//        		dismissLoadingScreen();
//           	showfrmLoanApplication();
//         } else {
//             dismissLoadingScreen();
//             showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
//             return false;
//         }
//     }
// }

// function takeSnapshot(widget){
  
//     var imgFlight = kony.image.createImageFromSnapShot(widget);  
//         var filterObj = kony.filter.createFilter();
//   	filterData = {
//         "filterName": kony.filter.BOX_BLUR,
//         "inputImage": imgFlight,
//         "inputRadius": 10
//     };
//    	filterObj.applyFilter(filterData);
//    	var imageObj = filterObj.getOutputImage();
//   	//var capturedImg = kony.image.createImage(imgFlight.getImageAsRawBytes());
//  	 //capturedImg.compress(0.7);
// 	var base64 = kony.convertToBase64(imgFlight.getImageAsRawBytes());
//   	kony.print("screenshot "+base64);
// 	return base64;
  
//   //var image= kony.image.createImageFromSnapShot(frmScreenshotCapture.flexmain);

  
  
//   //frmHome.img.im=image;
//   //frmHome.show();
//   //alert("screenshot taken"+JSON.stringify(image));
// }

function onclickTncLoan(){
  gblMBNewTncFlow= "TMB_NEW_LOAN";
  callServiceReadUTFFileLoan();
}
function onclickTnCLoanBtnNext(){
  	gblconsentApproved = false;
	callServicegetLetterofContent();
}

function onclickTnCLoanBtnBack(){
 showfrmCheckList ();
}
function onclickLetterofConsentBack(){
  frmMBNewTnCLoan.show();
}

function callServicegetLetterofContent() {
    var input_param = {};
    var locale = kony.i18n.getCurrentLocale();
    if (locale == "en_US") {
        input_param["localeCd"] = "en_US";
    } else {
        input_param["localeCd"] = "th_TH";
    }
   input_param["productType"] = frmCheckList.lblSubProductName.text;
   if (gblCustomerIDType == "CI") {
    input_param["CI"] = formatCitizenID(gblCustomerIDValue);
  }else{
    input_param["CI"] =  gblCustomerPassport;
  } 
	input_param["productCode"]=gblSelectedLoanProduct;
    input_param["moduleKey"] ="ReadLetterofContent";//"ReadLetterofContent";
    showLoadingScreen();
    invokeServiceSecureAsync("readUTFFile", input_param, callbackshowLetterofConsent);
}

function callbackshowLetterofConsent(status, result){
   if (status == 400) {
        if (result["opstatus"] == 0) {
          	frmLetterConsent.richtxtPersonalInfo.text=result["fileContent"];
          	gblconsentApproved=false;
          	frmLetterConsent.btnNext.onClick = showAccessPinScreenKeypad;
          	frmLetterConsent.show();
          	dismissLoadingScreen();
        } else {
            dismissLoadingScreen();
            showAlert(kony.i18n.getLocalizedString("ECGenericError"), kony.i18n.getLocalizedString("info"));
            return false;
        }
    }
  
}