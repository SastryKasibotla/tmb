package com.kone.TMB;

import com.konylabs.android.KonyMain;


import com.konylabs.search.SearchDataAdapter;
       import android.provider.SearchRecentSuggestions;
       import android.content.Intent;
       import android.app.SearchManager;


import sdk.insert.io.*;
import android.os.Bundle;

public class TMBUI extends KonyMain  {
	private static TMBUI context;
    public void onCreate(Bundle savedInstanceState) {
		context = this;
		try{
		if(null == Insert.getInstance()){
			Insert.InsertInitParams insertParams = new  Insert.InsertInitParams();
			insertParams.setCallbackInterface(new InsertPhasesCallbackInterface() {
			 @Override public void onInitComplete() { }
			 @Override public void onInitFailed() { }
			 @Override public void onInitStarted() { }
			});
			Insert.initSDK(
			this.getApplication(),"bf3ccd119656c347931892a63ec4be84ecf351144d024f76806db1bcecfa3a31ef416d97502180f5662eaceea06285167adbe595538103746b054c5f1d0811b3d4bbf1ca1ed660557f5cbf9f6dce9879.2db78fc5a92be73e1a7a047562537f53.7d24f12500992783532875e292095d4183f216f6edc8d2235b4e25ec7bf44d04", 
			"tmbbankthailand",insertParams);
		}
		}catch(Exception e){

		}
		super.onCreate(savedInstanceState);

    }
		
	public static TMBUI getActivityContext() {
		return context;
	}
	
	public int getAppSourceLocation(){
		return 1;
	}

	public void onNewIntent(Intent intent) {
       if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
        // handles a search query
        String query = intent.getStringExtra(SearchManager.QUERY);
        String extraData = intent.getStringExtra("intent_extra_data_key");
        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
          TMBUISearchSuggestionProvider.AUTHORITY, TMBUISearchSuggestionProvider.MODE);
        suggestions.saveRecentQuery(query, extraData);
        SearchDataAdapter.getInstance().raiseOnDoneCallback(query,extraData);
       }
       else
        super.onNewIntent(intent);
      }

	
	
}
